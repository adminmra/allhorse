<?php	 		 	 
  // Set page title
  require_javascript("og/CustomPropertyFunctions.js");
  set_page_title(lang('custom properties'));  
  $genid = gen_id();
?>

<div style="height:100%;background-color:white">
  <div class="adminHeader">
  	<div class="adminTitle"><?php	 	 echo lang('custom properties') ?></div>
  </div>
  <div class="adminSeparator"></div>
  <div class="adminMainBlock">
  
  <?php	 	?>
  <?php	 	')) ?>
  <hr/>
  
  <form class="internalForm" style='height:100%;background-color:white' action="<?php	 	">
  	<input type="hidden" name="objectType" id="objectType"/>
  	<div id="<?php	 	 echo $genid?>">
	</div>
	<br/>
	<div id="CPactions<?php	 	">
	<a href="#" class="link-ico ico-add" onclick="og.addCustomProperty('<?php	 	 echo lang('add custom property')?></a>
	<br/>
	<?php	 	 echo submit_button(lang('save changes')) ?>
	</div>
  </form>  
  
  <script>
  	og.loadCustomPropertyFlags();
  </script>
  
  </div>
</div>