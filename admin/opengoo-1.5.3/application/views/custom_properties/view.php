<?php	 		 	
	$properties = $__properties_object->getCustomProperties();	
	$cpvCount = CustomPropertyValues::getCustomPropertyValueCount($__properties_object->getId(), get_class($__properties_object->manager()));
	if ((!is_array($properties) || count($properties) == 0) && $cpvCount == 0) 
		return "";
?>
<div class="commentsTitle"><?php	 	 echo lang('custom properties')?></div>
<?php	 	 if($cpvCount > 0){?>
<table class="og-custom-properties">
<?php	 	 
	$alt = true;
	$cps = CustomProperties::getAllCustomPropertiesByObjectType(get_class($__properties_object->manager()));
	foreach($cps as $customProp){ 
		$alt = !$alt;
		$cpv = CustomPropertyValues::getCustomPropertyValue($__properties_object->getId(), $customProp->getId());
		if($cpv instanceof CustomPropertyValue && ($customProp->getIsRequired() || $cpv->getValue() != '')){?>
			<tr class="<?php	 	 echo $alt ? 'altRow' : ''?>">
				<td class="name" title="<?php	 	</td>
				<?php	 	
					// dates are in standard format "Y-m-d H:i:s", must be formatted
					if ($customProp->getType() == 'date') {
						$dtv = DateTimeValueLib::dateFromFormatAndString("Y-m-d H:i:s", $cpv->getValue());
						$value = $dtv->format(user_config_option('date_format'));
					} else {
						$value = $cpv->getValue();
					}
					
					$title = '';
					$style = '';
					if ($customProp->getType() == 'boolean'){
						$htmlValue = '<div class="db-ico ico-'.($value?'complete':'delete').'">&nbsp;</div>';
					} else if ($customProp->getIsMultipleValues()) {
						$multValues = CustomPropertyValues::getCustomPropertyValues($__properties_object->getId(), $customProp->getId());
						$htmlValue = '<table style="width:100%;margin-bottom:2px">';
						$newAlt = $alt;
						foreach ($multValues as $mv){
							$value = str_replace('|', ',', $mv->getValue());
							$title =  (strlen($value) > 100 && $customProp->getType() != 'memo') ? clean(str_replace('|', ',', $value)) : '';
							$showValue = $customProp->getType() == 'memo' ? $value : truncate($value,100);
							$htmlValue .= '<tr class="' . ($newAlt ? 'altRow' : 'row') . '"><td style="padding:0px 5px" title="' . $title . '">' . clean($showValue) . '</td></tr>';
							$newAlt = !$newAlt; 
						}
						$htmlValue .= '</table>';
						$style = 'style="padding:1px 0px"';
					} else {
						$title =  (strlen($value) > 100 && $customProp->getType() != 'memo') ? clean($value) : '';
						$htmlValue = nl2br(clean($customProp->getType() == 'memo' ? $value : truncate($value,100)));
					}
				?>
				<td class="value" <?php	 	 echo $htmlValue ?></td>
			</tr>
		<?php	 	 } // if
	} // foreach ?>
</table>
<?php	 	 } // if

// Draw flexible custom properties
if (is_array($properties) && count($properties) > 0){ ?>
	<table class="og-custom-properties">
	<?php	 	 foreach ($properties as $prop) {?>
		<tr>
			<td class="name" title="<?php	 	</td>
			<td title="' . $prop->getPropertyValue() . '"><?php	 	 echo $prop->getPropertyValue() ?></td>
		</tr>
	<?php	 	 } // foreach ?>
	</table>
<?php	 	 } // if ?>
</div>