<?php	 		 	
	$submit_url = get_url('event', 'icalendar_export');
	$genid = gen_id();
?>

<script>
og.download_exported_cal = function() {
	window.open(og.getUrl('event', 'download_exported_file'));
}
</script>

<form style="height:100%;background-color:white" id="<?php	 	 echo $submit_url ?>" method="post" enctype="multipart/form-data">

<div class="file">
<div class="coInputHeader">
<div class="coInputHeaderUpperRow">
<div class="coInputTitle">
	<table style="width:535px"><tr><td><?php	 	?></td>
	<?php	 	 if (!isset($result_msg)) { ?>
	<td style="text-align:right"><?php	 	")) ?></td>
	<?php	 	 } //if ?>
	</tr></table>
</div>
</div>
</div>
<div class="coInputMainBlock adminMainBlock">
<?php	 	 if (!isset($result_msg)) { ?>
<span><b><?php	 	 echo lang('calendar will be exported in icalendar format') ?></b></span>
<?php	 	 
	$from_date = DateTimeValueLib::now();
	$from_date->add('M', -6);
	$to_date = DateTimeValueLib::now();
	$to_date->add('M', 6);
?>
<fieldset><legend><?php	 	 echo lang('range of events') ?></legend>
	<table><tr style="padding-bottom:4px;">
		<td align="right" style="padding-right:10px;padding-bottom:6px;padding-top:2px"><?php	 	 echo lang('from date') ?></td>
		<td><?php	 	 ?></td></tr>
	<tr style="padding-bottom:4px;">
		<td align="right" style="padding-right:10px;padding-bottom:6px;padding-top:2px"><?php	 	 echo lang('to date') ?></td>
		<td><?php	 	  ?></td></tr>
	<tr style="padding-bottom:4px;">
		<td align="right" style="padding-right:10px;padding-bottom:6px;padding-top:2px"><?php	 	 echo lang('name') ?></td>
		<td><?php	 	 echo lang('calendar name desc') ?></span></td></tr>
	</table>
</fieldset>
	<table style="width:535px">
	<tr><td><?php	 	")) ?></td></tr></table>
</div>
<?php	 	 } else { ?>
	<div><b><?php	 	 echo $result_msg ?></b></div>
<?php	 	 } ?>

</div>
</form>
<script>
	btn = Ext.get('<?php	 	
	if (btn != null) btn.focus()
</script>