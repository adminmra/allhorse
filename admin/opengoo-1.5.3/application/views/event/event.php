<?php	 		 	
require_javascript('og/modules/addMessageForm.js'); 
$genid = gen_id(); 
?>
<script>

	og.cal_hide = function(id) {
		document.getElementById(id).style.display = "none";
	}

	og.cal_show = function(id) {
		document.getElementById(id).style.display = "block";
	}
	
	og.toggleDiv = function(div_id){
		var theDiv = document.getElementById(div_id);
		dis = !theDiv.disabled;
	    var theFields = theDiv.getElementsByTagName('*');
	    for (var i=0; i < theFields.length;i++) theFields[i].disabled=dis;
	    theDiv.disabled=dis;
	}
	
	og.changeRepeat = function() {
		og.cal_hide("cal_extra1");
		og.cal_hide("cal_extra2");
		og.cal_hide("cal_extra3");
		og.cal_hide("<?php	 	
		if(document.getElementById("daily").selected){
			document.getElementById("word").innerHTML = '<?php	 	
			og.cal_show("cal_extra1");
			og.cal_show("cal_extra2");
			og.cal_show("<?php	 	
		} else if(document.getElementById("weekly").selected){
			document.getElementById("word").innerHTML =  '<?php	 	
			og.cal_show("cal_extra1");
			og.cal_show("cal_extra2");
			og.cal_show("<?php	 	
		} else if(document.getElementById("monthly").selected){
			document.getElementById("word").innerHTML =  '<?php	 	
			og.cal_show("cal_extra1");
			og.cal_show("cal_extra2");
			og.cal_show("<?php	 	
		} else if(document.getElementById("yearly").selected){
			document.getElementById("word").innerHTML =  '<?php	 	
			og.cal_show("cal_extra1");
			og.cal_show("cal_extra2");
			og.cal_show("<?php	 	
		} else if(document.getElementById("holiday").selected){
			og.cal_show("cal_extra3");
			og.cal_show("<?php	 	
		}
	}
	
	og.confirmEditRepEvent = function(ev_id, is_repetitive) {
		if (is_repetitive) {
			return confirm(lang('confirm repeating event edition'));
		}
		return true;
	}
	
</script>


<?php	 	
/*
	Copyright (c) Reece Pegues
	sitetheory.com

    Reece PHP Calendar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or 
	any later version if you wish.

    You should have received a copy of the GNU General Public License
    along with this file; if not, write to the Free Software
    Foundation Inc, 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
$object = $event;

$active_projects = logged_user()->getActiveProjects();
$project = active_or_personal_project();

$day =  array_var($event_data, 'day');
$month =  array_var($event_data, 'month');
$year =  array_var($event_data, 'year');

$filter_user = isset($_GET['user_id']) ? $_GET['user_id'] : logged_user()->getId();

$use_24_hours = user_config_option('time_format_use_24');

	// get dates
	$setlastweek='';
	$pm = 0;
	if($event->isNew()) { 
			
		
		$username = '';
		$desc = '';
		
		// if adding event to today, make the time current time.  Else just make it 6PM (you can change that)
		if( "$year-$month-$day" == date("Y-m-d") ) $hour = date('G') + 1;
		else $hour = 18;
		// organize time by 24-hour or 12-hour clock.
		$pm = 0;
		if(!$use_24_hours) {
			if($hour >= 12) {
				$hour = $hour - 12;
				$pm = 1;
			}
		}
		// set default minute and duration times.
		$minute = 0;
		$durhr = 1;
		$durday = 0;
		$durmin = 0;
		// set other defaults
		$rjump = 1;
		// set type of event to default of 1 (nothing)
		$typeofevent = 1;
	}
	?>

	<?php	 	 if($event->isNew()) { ?>
	<form style="height:100%;background-color:white" class="internalForm" action="<?php	 	 ?>" method="post">
	<?php	 	 } else { ?>
	<form style="height:100%;background-color:white" class="internalForm" action="<?php	 	 ?>" method="post">
	<?php	 	 } // if ?>

	<input type="hidden" id="event[pm]" name="event[pm]" value="<?php	 	 echo $pm?>">
	<div class="event">	
	<div class="coInputHeader">
		<div class="coInputHeaderUpperRow">
			<div class="coInputTitle">
				<table style="width:535px">
				<tr>
					<td>
					<?php	 	 echo $event->isNew() ? lang('new event') : lang('edit event') ?></td>
					<td style="text-align:right">
					<?php	 	
						$is_repetitive = ($event->getRepeatD() > 0 || $event->getRepeatM() > 0 || $event->getRepeatY() > 0 || $event->getRepeatH() > 0) ? 'true' : 'false'; 
						echo submit_button($event->isNew() ? lang('add event') : lang('save changes'),'e',array('style'=>'margin-top:0px;margin-left:10px', 'tabindex' => 200, 'onclick' => (!$event->isNew() ? "javascript:if(!og.confirmEditRepEvent('".$event->getId()."',$is_repetitive)) return false;" : '')));
					?>
					</td>
				</tr>
				</table>
			</div>		
		</div>
		<div style="text-align:left;"><?php	 	 echo label_tag(lang('subject'), 'taskListFormName', true) . text_field('event[subject]', array_var($event_data, 'subject'), 
	    		array('class' => 'title', 'id' => 'eventSubject', 'tabindex' => '1', 'maxlength' => '100', 'tabindex' => '10')) ?>
	    </div>
	 
	 	<?php	 	 ?>
	 	
	 	<div style="padding-top:5px;text-align:left;">
		<a href='#' class='option' onclick="og.ToggleTrap('trap1', 'fs1');og.toggleAndBolden('<?php	 	 echo lang('workspace')?></a> - 
		<a href='#' class='option' onclick="og.ToggleTrap('trap2', 'fs2');og.toggleAndBolden('<?php	 	 echo lang('tags')?></a> - 
		<a href='#' class='option' onclick="og.ToggleTrap('trap3', 'fs3');og.toggleAndBolden('<?php	 	 echo lang('description')?></a> - 
		<a href='#' class='option' onclick="og.ToggleTrap('trap4', 'fs4');og.toggleAndBolden('<?php	 	 echo lang('CAL_REPEATING_EVENT')?></a> -
		<a href='#' class='option' onclick="og.ToggleTrap('trap5', 'fs5');og.toggleAndBolden('<?php	 	 echo lang('object reminders')?></a> - 
		<a href='#' class='option' onclick="og.ToggleTrap('trap6', 'fs6');og.toggleAndBolden('<?php	 	 echo lang('custom properties')?></a> - 
		<a href="#" class="option" onclick="og.ToggleTrap('trap7', 'fs7');og.toggleAndBolden('<?php	 	 echo lang('object subscribers') ?></a>
		<?php	 	 if($object->isNew() || $object->canLinkObject(logged_user(), $project)) { ?> - 
			<a href="#" class="option" onclick="og.ToggleTrap('trap8', 'fs8');og.toggleAndBolden('<?php	 	 echo lang('linked objects') ?></a>
		<?php	 	 } ?> -
		<a href="#" class="option" onclick="og.ToggleTrap('trap9', 'fs9');og.toggleAndBolden('<?php	 	 echo lang('event invitations') ?></a>
		<?php	 	 foreach ($categories as $category) { ?>
			- <a href="#" class="option" <?php	 	 echo lang($category['name'])?></a>
		<?php	 	 } ?>
		</div>
		</div>
	
		<div class="coInputSeparator"></div>
		<div class="coInputMainBlock">	
		
		<?php	 	 
			$show_help_option = user_config_option('show_context_help'); 
			if ($show_help_option == 'always' || ($show_help_option == 'until_close')&& user_config_option('show_add_event_context_help', true, logged_user()->getId())) {?>
			<div id="addEventPanelContextHelp" class="contextHelpStyle">
				<?php	 	 ?>
			</div>
		<?php	 	 }?>
		
		<div id="<?php	 	 echo $genid ?>add_event_select_workspace_div" style="display:none">
		<fieldset>
		<?php	 	 
			$show_help_option = user_config_option('show_context_help'); 
			if ($show_help_option == 'always' || ($show_help_option == 'until_close')&& user_config_option('show_add_event_workspace_context_help', true, logged_user()->getId())) {?>
			<div id="addEventPanelContextHelp" class="contextHelpStyle">
				<?php	 	 ?>
			</div>
		<?php	 	 }?>
		<legend><?php	 	 echo lang('workspace') ?></legend>
			<?php	 	 if ($object->isNew()) {
				echo select_workspaces('ws_ids', null, array($project), $genid.'ws_ids');
			} else {
				echo select_workspaces('ws_ids', null, $object->getWorkspaces(), $genid.'ws_ids');
			} ?>
		</fieldset>
		</div>
		<div id="trap1"><fieldset id="fs1" style="height:0px;border:0px;padding:0px;display:none"><span style="color:#FFFFFF;"></span></fieldset></div>
		
		<div id="<?php	 	 echo $genid ?>add_event_tags_div" style="display:none">
		<fieldset>
		<?php	 	 
			$show_help_option = user_config_option('show_context_help'); 
			if ($show_help_option == 'always' || ($show_help_option == 'until_close')&& user_config_option('show_add_event_tag_context_help', true, logged_user()->getId())) {?>
			<div id="addEventPanelContextHelp" class="contextHelpStyle">
				<?php	 	 ?>
			</div>
		<?php	 	 }?>
			<legend><?php	 	 echo lang('tags')?></legend>
			<?php	 	 ?>
		</fieldset>
		</div>
		<div id="trap2"><fieldset id="fs2" style="height:0px;border:0px;padding:0px;display:none"><span style="color:#FFFFFF;"></span></fieldset></div>
		
		<div id="<?php	 	 echo $genid ?>add_event_description_div" style="display:none">
			<fieldset>
			<?php	 	 
			$show_help_option = user_config_option('show_context_help'); 
			if ($show_help_option == 'always' || ($show_help_option == 'until_close')&& user_config_option('show_add_event_description_context_help', true, logged_user()->getId())) {?>
			<div id="addEventPanelContextHelp" class="contextHelpStyle">
				<?php	 	 ?>
			</div>
		<?php	 	 }?>
			<legend><?php	 	 echo lang('description')?></legend>
				<?php	 	?>
			</fieldset>
		</div>
		<div id="trap3"><fieldset id="fs3" style="height:0px;border:0px;padding:0px;display:none"><span style="color:#FFFFFF;"></span></fieldset></div>
		
<?php	 	 
	$rsel1 = array_var($event_data, 'rsel1'); 
	$rsel2 = array_var($event_data, 'rsel2'); 
	$rsel3 = array_var($event_data, 'rsel3'); 
	$rnum = array_var($event_data, 'rnum'); 
	$rend = array_var($event_data, 'rend');?>
		
	<div id="<?php	 	 echo $genid ?>event_repeat_options_div" style="display:none">
		<fieldset>
			<?php	 	 
				$show_help_option = user_config_option('show_context_help'); 
				if ($show_help_option == 'always' || ($show_help_option == 'until_close')&& user_config_option('show_add_event_repeat_options_context_help', true, logged_user()->getId())) {?>
				<div id="addEventPanelContextHelp" class="contextHelpStyle">
					<?php	 	 ?>
				</div>
			<?php	 	 }?>
				<legend><?php	 	 echo lang('CAL_REPEATING_EVENT')?></legend>
			<?php	 	
			// calculate what is visible given the repeating options
			$hide = '';
			$hide2 = (isset($occ) && $occ == 6)? '' : "display: none;";
			if((!isset($occ)) OR $occ == 1 OR $occ=="6" OR $occ=="") $hide = "display: none;";
			// print out repeating options for daily/weekly/monthly/yearly repeating.
			if(!isset($rsel1)) $rsel1=true;
			if(!isset($rsel2)) $rsel2="";
			if(!isset($rsel3)) $rsel3="";
			if(!isset($rnum) || $rsel2=='') $rnum="";
			if(!isset($rend) || $rsel3=='') $rend="";
			if(!isset($hide2) ) $hide2="";?>
			
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" valign="top" style="padding-bottom:6px">
						<table>
							<tr>
								<td align="left" valign="top" style="padding-bottom:6px">
									<?php	 	 echo lang('CAL_REPEAT')?> 
										<select name="event[occurance]" onChange="og.changeRepeat()" tabindex="40">
											<option value="1" id="today"<?php	 	 echo lang('CAL_ONLY_TODAY')?></option>
											<option value="2" id="daily"<?php	 	 echo lang('CAL_DAILY_EVENT')?></option>
											<option value="3" id="weekly"<?php	 	 echo lang('CAL_WEEKLY_EVENT')?></option>
											<option value="4" id="monthly"<?php	 	 echo lang('CAL_MONTHLY_EVENT') ?></option>
											<option value="5" id="yearly"<?php	 	 echo lang('CAL_YEARLY_EVENT') ?></option>
											<!-- option value="6" id="holiday"<?php	 	 echo lang('CAL_HOLIDAY_EVENT') ?></option -->
										</select>
									<?php	 	 if (isset($occ) && $occ > 1 && $occ < 6){ ?>
									<script>
										og.changeRepeat();
									</script>
									<?php	 	 } ?>
								</td>
							</tr>
						</table>
					</td>
					</tr><tr>
					<td>
						<div id="cal_extra2" style="width: 400px; align: center; text-align: left; <?php	 	 echo $hide ?>">
							<div id="cal_extra1" style="<?php	 	 echo $hide ?>">
								<?php	 	". text_field('event[occurance_jump]',array_var($event_data, 'rjump'), array('class' => 'title','size' => '2', 'id' => 'eventSubject', 'tabindex' => '50', 'maxlength' => '100', 'style'=>'width:25px')) ?>
								<span id="word"></span>
							</div>
							<table>
								<tr><td colspan="2" style="vertical-align:middle; height: 22px;">
									<?php	 	". lang('CAL_REPEAT_FOREVER')?>
								</td></tr>
								<tr><td colspan="2" style="vertical-align:middle">
									<?php	 	
									echo "&nbsp;" . text_field('event[repeat_num]', $rnum, array('size' => '3', 'id' => 'repeat_num', 'maxlength' => '3', 'style'=>'width:25px', 'tabindex' => '80')) ."&nbsp;" . lang('CAL_TIMES') ?>
								</td></tr>
								<tr><td style="vertical-align:middle">
									<?php	 	?>
								</td><td style="padding-left:8px;">
									<?php	 	?>
								</td></tr>
							</table>
						</div>
						<div id="cal_extra3" style="width: 300px; align: center; text-align: left; <?php	 	 echo $hide2 ?>'">
							<?php	 	
							// get the week number
							$tmp = 1;
							$week = 0;
							while($week < 5 AND $tmp <= $day){
								$week++;
								$tmp += 7;
							}
							// get days in month and day name
							$daysinmonth = date("t",mktime(0,0,1,$month,$day,$year));
							$dayname = date("l",mktime(0,0,1,$month,$day,$year));
							$dayname = "CAL_" .strtoupper  ($dayname);
							// use week number, and days in month to calculate if it's on the last week.
							if($day > $daysinmonth - 7) $lastweek = true;
							else $lastweek = false;
							// calculate the correct number endings
							if($week==1) $weekname = "1st";
							elseif($week==2) $weekname = "2nd";
							elseif($week==3) $weekname = "3rd";
							else $weekname = $week."th";
							// print out the data for holiday repeating
					
							echo lang('CAL_HOLIDAY_EXPLAIN'). $weekname." ". lang($dayname) ." ".lang('CAL_DURING')." ".cal_month_name($month)." ".lang('CAL_EVERY_YEAR');
					
							if($lastweek){// if it's the last week, add option to have event repeat on LAST week every month (holiday repeating only)
								echo "<br/><br/>". checkbox_field('event[cal_holiday_lastweek]',$setlastweek, array('value' => '1', 'id' => 'cal_holiday_lastweek', 'maxlength' => '10')) .lang('CAL_HOLIDAY_EXTRAOPTION') ." " . lang($dayname)." ".lang('CAL_IN')." ".cal_month_name($month)." ".lang('CAL_EVERY_YEAR');
							}
							?>
						</div>
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
	<div id="trap4"><fieldset id="fs4" style="height:0px;border:0px;padding:0px;display:none"><span style="color:#FFFFFF;"></span></fieldset></div>

	<div id="<?php	 	 echo $genid ?>add_reminders_div" style="display:none">
	<fieldset>
	<?php	 	 
			$show_help_option = user_config_option('show_context_help'); 
			if ($show_help_option == 'always' || ($show_help_option == 'until_close')&& user_config_option('show_add_event_reminders_context_help', true, logged_user()->getId())) {?>
			<div id="addEventPanelContextHelp" class="contextHelpStyle">
				<?php	 	 ?>
			</div>
		<?php	 	 }?>
	<legend><?php	 	 echo lang('object reminders')?></legend>
		<div id="<?php	 	">
			<?php	 	 echo lang('reminders will not apply to repeating events') ?>
		</div>
		<?php	 	?>
	</fieldset>
	</div>
	<div id="trap5"><fieldset id="fs5" style="height:0px;border:0px;padding:0px;display:none"><span style="color:#FFFFFF;"></span></fieldset></div>

	<div id="<?php	 	 echo $genid ?>add_custom_properties_div" style="display:none">
	<fieldset>
	<?php	 	 
			$show_help_option = user_config_option('show_context_help'); 
			if ($show_help_option == 'always' || ($show_help_option == 'until_close')&& user_config_option('show_add_event_custom_properties_context_help', true, logged_user()->getId())) {?>
			<div id="addEventPanelContextHelp" class="contextHelpStyle">
				<?php	 	 ?>
			</div>
		<?php	 	 }?>
	<legend><?php	 	 echo lang('custom properties')?></legend>
		<?php	 	 echo render_object_custom_properties($object, 'ProjectEvents', false) ?><br/><br/>
		<?php	 	?>
	</fieldset>
	</div>
	<div id="trap6"><fieldset id="fs6" style="height:0px;border:0px;padding:0px;display:none"><span style="color:#FFFFFF;"></span></fieldset></div>

	<div id="<?php	 	 echo $genid ?>add_subscribers_div" style="display:none">
		<fieldset>
		<?php	 	 
			$show_help_option = user_config_option('show_context_help'); 
			if ($show_help_option == 'always' || ($show_help_option == 'until_close')&& user_config_option('show_add_event_subscribers_context_help', true, logged_user()->getId())) {?>
			<div id="addEventPanelContextHelp" class="contextHelpStyle">
				<?php	 	 ?>
			</div>
		<?php	 	 }?>
		<legend><?php	 	 echo lang('object subscribers') ?></legend>
		<div id="<?php	 	 echo $genid ?>add_subscribers_content">
			<?php	 	 ?>
		</div>
		</fieldset>
	</div>
	
	<script>
	var wsch = Ext.getCmp('<?php	 	
	wsch.on("wschecked", function(arguments) {
		var uids = App.modules.addMessageForm.getCheckedUsers('<?php	 	
		Ext.get('<?php	 	 echo $genid ?>add_subscribers_content').load({
			url: og.getUrl('object', 'render_add_subscribers', {
				workspaces: this.getValue(),
				users: uids,
				genid: '<?php	 	 echo $genid ?>',
				object_type: '<?php	 	 echo get_class($object->manager()) ?>'
			}),
			scripts: true
		});
	}, wsch);
	</script>
	<div id="trap7"><fieldset id="fs7" style="height:0px;border:0px;padding:0px;display:none"><span style="color:#FFFFFF;"></span></fieldset></div>

	<?php	 	 if($object->isNew() || $object->canLinkObject(logged_user(), $project)) { ?>

	<div style="display:none" id="<?php	 	 echo $genid ?>add_linked_objects_div">
	<fieldset>
	<?php	 	 
			$show_help_option = user_config_option('show_context_help'); 
			if ($show_help_option == 'always' || ($show_help_option == 'until_close')&& user_config_option('show_add_event_linked_objects_context_help', true, logged_user()->getId())) {?>
			<div id="addEventPanelContextHelp" class="contextHelpStyle">
				<?php	 	 ?>
			</div>
		<?php	 	 }?>
		<legend><?php	 	 echo lang('linked objects') ?></legend>
		<?php	 	 echo render_object_link_form($object) ?>
	</fieldset>	
	</div>
	<div id="trap8"><fieldset id="fs8" style="height:0px;border:0px;padding:0px;display:none"><span style="color:#FFFFFF;"></span></fieldset></div>
	<?php	 	 } // if ?>

	<div id="<?php	 	 echo $genid ?>add_event_invitation_div" style="display:none" class="og-add-subscribers">
	<fieldset id="emailNotification">
	<?php	 	 
			$show_help_option = user_config_option('show_context_help'); 
			if ($show_help_option == 'always' || ($show_help_option == 'until_close')&& user_config_option('show_add_event_invitation_context_help', true, logged_user()->getId())) {?>
			<div id="addEventPanelContextHelp" class="contextHelpStyle">
				<?php	 	 ?>
			</div>
		<?php	 	 }?>
		<legend><?php	 	 echo lang('event invitations') ?></legend>
		<?php	 	 // ComboBox for Assistance confirmation 
			if (!$event->isNew()) {
				$event_invs = $event->getInvitations();
				if (isset($event_invs[$filter_user])) {
					$event_inv_state = $event_invs[$filter_user]->getInvitationState();
				} else {
					$event_inv_state = -1;
				}
				
				if ($event_inv_state != -1) {
					$options = array(
						option_tag(lang('yes'), 1, ($event_inv_state == 1)?array('selected' => 'selected'):null),
						option_tag(lang('no'), 2, ($event_inv_state == 2)?array('selected' => 'selected'):null),
						option_tag(lang('maybe'), 3, ($event_inv_state == 3)?array('selected' => 'selected'):null)
					);
					if ($event_inv_state == 0) {
						$options[] = option_tag(lang('decide later'), 0, ($event_inv_state == 0) ? array('selected' => 'selected'):null);
					}
					?>
					<table><tr><td style="padding-right: 6px;"><label for="eventFormComboAttendance" class="combobox"><?php	 	 echo lang('confirm attendance') ?></label></td><td>
					<?php	 	?>
					</td></tr></table>	
			<?php	 		} //if			
			} // if ?>

			<p><?php	 	 echo lang('event invitations desc') ?></p>
			<?php	 	 echo checkbox_field('event[send_notification]', array_var($event_data, 'send_notification', $event->isNew()), array('id' => 'eventFormSendNotification', 'tabindex' => '110')) ?>
			<label for="eventFormSendNotification" class="checkbox"><?php	 	 echo lang('send new event notification') ?></label>
			
	</fieldset>
	</div>	
	<div id="trap9"><fieldset id="fs9" style="height:0px;border:0px;padding:0px;display:none"><span style="color:#FFFFFF;"></span></fieldset></div>

<div>
<fieldset><legend><?php	 	 echo lang('CAL_TIME_AND_DURATION') ?></legend>
<table>
	<tr style="padding-bottom:4px">
		<td align="right" style="padding-right:10px;padding-bottom:6px;padding-top:2px"><?php	 	 echo lang('CAL_DATE') ?></td>
		<td align='left'><?php	 	
				$dv_start = DateTimeValueLib::make(array_var($event_data, 'hour'), array_var($event_data, 'minute'), 0, $month, $day, $year);
				$event->setStart($dv_start);
				echo pick_date_widget2('event[start_value]', $event->getStart(), $genid, 120); ?>
		</td>
	</tr>

	<tr style="padding-bottom:4px">
		<td align="right" style="padding-right:10px;padding-bottom:6px;padding-top:2px">
			<?php	 	 echo lang('CAL_TIME') ?>
		</td>
		<td>
		<?php	 	
			$hr = array_var($event_data, 'hour');
		 	$minute = array_var($event_data, 'minute');
			$is_pm = array_var($event_data, 'pm');
			$time_val = "$hr:" . str_pad($minute, 2, '0') . ($use_24_hours ? '' : ' '.($is_pm ? 'PM' : 'AM'));
			echo pick_time_widget2('event[start_time]', $time_val, $genid, 130);
		?>
		</td>
	</tr>
	<!--   begin printing the duration options-->
	<tr>
		<td align="right" style="padding-right:10px;padding-bottom:6px;padding-top:2px"><?php	 	 echo lang('CAL_DURATION') ?></td>
		<td align="left">
		<div id="<?php	 	 echo $genid ?>ev_duration_div">
			<select name="event[durationhour]" size="1" tabindex="150">
			<?php	 	
			for($i = 0; $i < 24; $i++) {
				echo "<option value='$i'";
				if(array_var($event_data, 'durationhour')== $i) echo ' selected="selected"';
				echo ">$i</option>\n";
			}
			?>
			</select> <?php	 	 echo lang('CAL_HOURS') ?> <select
				name="event[durationmin]" size="1" tabindex="160">
				<?php	 	
				// print out the duration minutes drop down
				$durmin = array_var($event_data, 'durationmin');
				for($i = 0; $i <= 59; $i = $i + 15) {
					echo "<option value='$i'";
					if($durmin >= $i && $i > $durmin - 15) echo ' selected="selected"';
					echo sprintf(">%02d</option>\n", $i);
				}
				?>
			</select> 
		</div>
		</td>
	</tr>
	<tr style="padding-bottom:4px">
		<td align="right" style="padding-right:10px;padding-bottom:6px;padding-top:2px">&nbsp;</td>
		<td align='left'>
			<?php	 	
			echo checkbox_field('event[type_id]',array_var($event_data, 'typeofevent') == 2, array('id' => 'format_html','value' => '2', 'tabindex' => '170', 'onchange' => 'og.toggleDiv(\''.$genid.'event[start_time]\'); og.toggleDiv(\''.$genid.'ev_duration_div\');'));
			echo lang('CAL_FULL_DAY');
			?>
		</td>
	</tr>

	<!--   print extra time options-->
	
</table>
</fieldset>
</div>

<?php	 	 foreach ($categories as $category) { ?>
	<div <?php	 	 echo $genid . $category['name'] ?>">
	<fieldset>
		<legend><?php	 	 ?></legend>
		<?php	 	 echo $category['content'] ?>
	</fieldset>
	</div>
	<?php	 	 } ?>

	<input type="hidden" name="cal_origday" value="<?php	 	 echo $day?>">
	<input type="hidden" name="cal_origmonth" value="<?php	 	 echo $month?>">
	<input type="hidden" name="cal_origyear" value="<?php	 	 echo $year?>">
	
	<div>
		<?php	 	 echo render_object_custom_properties($object, 'ProjectEvents', true) ?>
	</div><br/>
	
	<?php	 	 ?>
	</div></div>
</form>

<script>

var wsch = Ext.getCmp('<?php	 	
og.eventInvitationsUserFilter = '<?php	 	
og.eventInvitationsPrevWsVal = -1;

og.drawInnerHtml = function(companies) {
	var htmlStr = '';
	var script = "";
	htmlStr += '<div id="<?php	 	
	htmlStr += '&nbsp;';
	script += 'var div = Ext.getDom(\'<?php	 	
	script += 'div.invite_companies = {};';
	script += 'var cos = div.invite_companies;';
	htmlStr += '<div class="company-users">';
	if (companies != null) {
		for (i = 0; i < companies.length; i++) {
			comp_id = companies[i].object_id;
			comp_name = companies[i].name;
			comp_img = companies[i].logo_url;			
			script += 'cos.company_' + comp_id + ' = {id:\'<?php	 	
			htmlStr += '<div onclick="App.modules.addMessageForm.emailNotifyClickCompany('+comp_id+',\'<?php	 	
					htmlStr += '<input type="checkbox" style="display:none;" name="event[invite_company_'+comp_id+']" id="<?php	 	
					htmlStr += '<label style="background: transparent url('+comp_img+') no-repeat; scroll 0% -5px;" ><span class="link-ico ico-company">'+og.clean(comp_name)+'</span></label>';
			htmlStr += '</div>';
			
			htmlStr += '<div class="company-users" style="padding-left:10px;">';
						
					for (j = 0; j < companies[i].users.length; j++) {

						usr = companies[i].users[j];
						htmlStr += '<div id="div<?php	 	" onmouseover="og.rollOver(this)" onmouseout="og.rollOut(this,false ,true)" onclick="og.checkUser(this)">'
						htmlStr += '<input style="display:none;" type="checkbox" class="checkbox" name="event[invite_user_'+usr.id+']" id="<?php	 	
						htmlStr += '<label style="overflow:hidden; background: transparent url('+usr.avatar_url+') no-repeat;" ><span class="link-ico ico-user" >'+og.clean(usr.name)+'</span> <br> <span style="color:#888888;font-size:90%;font-weight:normal;">'+ usr.mail+ ' </span></label>';
						script += 'cos.company_' + comp_id + '.users.push({ id:'+usr.id+', checkbox_id : \'inviteUser' + usr.id + '\'});';
						if (usr.invited)
							script += 'og.checkUser(document.getElementById(\'div<?php	 	'
						htmlStr += '</div>';
	
					}
				htmlStr += '</div>';
			
		}
		htmlStr += '</div>';
	}
	Ext.lib.Event.onAvailable('<?php	 	 echo $genid ?>invite_companies', function() {
		eval(script);
	});
	return htmlStr;
};

og.drawUserList = function(success, data) {
	var companies = data.companies;

	var inv_div = Ext.get('<?php	 	
	if (inv_div != null) inv_div.remove();
	inv_div = Ext.get('emailNotification');
	
	if (inv_div != null) {
		inv_div.insertHtml('beforeEnd', '<div id="<?php	 		
		if (Ext.isIE) inv_div.update(Ext.getDom("emailNotification").innerHTML, true);
	}
};

og.redrawUserList = function(wsVal){
	if (wsVal != og.eventInvitationsPrevWsVal) {
		og.openLink(og.getUrl('event', 'allowed_users_view_events', {ws_id:wsVal, user:og.eventInvitationsUserFilter, evid:<?php	 	
		og.eventInvitationsPrevWsVal = wsVal;
	}
};
wsch.on("wschecked", function() {
	og.redrawUserList(this.getValue());
}, wsch);
<?php	 	 if ($object->isNew()) {
	$ws_ids = $project->getId();
} else {
	$ws_ids = "";
	foreach ($object->getWorkspaces() as $w) {
		if ($ws_ids != "") $ws_ids .= ",";
		$ws_ids .= $w->getId();
	}
}
?>
og.redrawUserList('<?php	 	

Ext.get('eventSubject').focus();
<?php	 	 ?>

</script>