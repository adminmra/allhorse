<?php	 		 	
    set_page_title(lang('update avatar'));
?>


<form target="_blank" style='height:100%;background-color:white' action="<?php	 	 echo $user->getUpdateAvatarUrl($redirect_to) ?>" method="post" enctype="multipart/form-data" onsubmit="og.submit(this, {callback:{type:'back'}})">

<div class="avatar">
<div class="coInputSeparator"></div>
<div class="coInputMainBlock">
  
  <fieldset>
    <legend><?php	 	 echo lang('current avatar') ?></legend>
<?php	 	 if($user->hasAvatar()) { ?>
    <img src="<?php	 	 echo clean($user->getDisplayName()) ?> avatar" />
    <p><a class="internalLink" href="<?php	 	 echo lang('delete current avatar') ?></a></p>
<?php	 	 } else { ?>
    <?php	 	 echo lang('no current avatar') ?>
<?php	 	 } // if ?>
  </fieldset>
  
  <div>
    <?php	 	 echo label_tag(lang('new avatar'), 'avatarFormAvatar', true) ?>
    <?php	 	 echo file_field('new avatar', null, array('id' => 'avatarFormAvatar')) ?>
<?php	 	 if($user->hasAvatar()) { ?>
    <p class="desc"><?php	 	 echo lang('new avatar notice') ?></p>
<?php	 	 } // if ?>
  </div>
  
  <?php	 	 echo submit_button(lang('update avatar')) ?>
  
</div>
</div>
</form>