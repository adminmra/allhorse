<?php	 		 	
require_javascript("og/modules/addTaskForm.js");
$task_list = $object;
?>
<script>
  if(App.modules.addTaskForm) {
    App.modules.addTaskForm.task_lists[<?php	 	 echo $task_list->getId() ?>] = {
      id               : <?php	 	 echo $task_list->getId() ?>,
	      can_add_task     : <?php	 	 echo ($task_list->canAddSubTask(logged_user()) && !$task_list->isTrashed()) ? 'true' : 'false' ?>,
      add_task_link_id : 'addTaskForm<?php	 	 echo $task_list->getId() ?>ShowLink',
      task_form_id     : 'addTaskForm<?php	 	 echo $task_list->getId() ?>',
      text_id          : 'addTaskText<?php	 	 echo $task_list->getId() ?>',
      assign_to_id     : 'addTaskAssignTo<?php	 	 echo $task_list->getId() ?>',
      submit_id        : 'addTaskSubmit<?php	 	 echo $task_list->getId() ?>'
    };
  } // if
</script>

<?php	 	 if ($task_list->getStartDate() instanceof DateTimeValue) { ?>
	<?php	 	 if ($task_list->getStartDate()->getYear() > DateTimeValueLib::now()->getYear()) { ?> 
	  <div class="startDate"><b><?php	 	 echo format_date($task_list->getStartDate(), null, 0) ?></div>
	<?php	 	 } else { ?> 
	  <div class="startDate"><b><?php	 	 echo format_descriptive_date($task_list->getStartDate(), 0) ?></div>
	<?php	 	 } // if ?>
<?php	 	 } // if ?>

<?php	 	 if ($task_list->getDueDate() instanceof DateTimeValue) { ?>
	<?php	 	 if ($task_list->getDueDate()->getYear() > DateTimeValueLib::now()->getYear()) { ?> 
	  <div class="dueDate"><b><?php	 	 echo format_date($task_list->getDueDate(), null, 0) ?></div>
	<?php	 	 } else { ?> 
	  <div class="dueDate"><b><?php	 	 echo format_descriptive_date($task_list->getDueDate(), 0) ?></div>
	<?php	 	 } // if ?>
<?php	 	 } // if ?>
		<?php	 	 
				$show_help_option = user_config_option('show_context_help');
			if ($show_help_option == 'always' || ($show_help_option == 'until_close' && user_config_option('show_list_task_context_help', true, logged_user()->getId()))) {?>
					<div id="tasksCardContextHelp" class="contextHelpStyle">
						<?php	 	 ?>
					</div>
		<?php	 	 }?>

<?php	 	 if($task_list->getText()) { ?>
  <fieldset><legend><?php	 	 echo lang('description') ?></legend>
  	<?php	 	 echo escape_html_whitespace(convert_to_links(clean($task_list->getText())))?>
  </fieldset>
<?php	 	 } // if ?>


  <table style="border:1px solid #717FA1;width:100%; padding-left:10px;"><tr><th style="padding-left:10px;padding-top:4px;padding-bottom:4px;background-color:#E8EDF7;font-size:120%;font-weight:bolder;color:#717FA1;width:100%;"><?php	 	">
  <div class="openTasks">
<?php	 	 if(is_array($task_list->getOpenSubTasks())) { ?>
    <table class="blank">
<?php	 	 foreach($task_list->getOpenSubTasks() as $task) { ?>
      <tr>
      
<!-- Checkbox -->
<?php	 	 if($task->canChangeStatus(logged_user()) && !$task_list->isTrashed()) { ?>
    <td class="taskCheckbox"><?php	 	 echo checkbox_link($task->getCompleteUrl(rawurlencode(get_url('task', 'view_task', array('id' => $task_list->getId())))), false, lang('mark task as completed')) ?></td>
<?php	 	 } else { ?>
        <td class="taskCheckbox"><img src="<?php	 	 echo lang('open task') ?>" /></td>
<?php	 	 } // if?>

<!-- Task text and options -->
        <td class="taskText">
<?php	 	 if($task->getAssignedTo()) { ?>
          <span class="assignedTo"><?php	 	 echo clean($task->getAssignedTo()->getObjectName()) ?>:</span> 
<?php	 	 } // if{ ?>
          <a class="internalLink" href="<?php	 	 echo ($task->getTitle() && $task->getTitle()!='' )?clean($task->getTitle()):clean($task->getText()) ?></a> 
          <?php	 	 if($task->canEdit(logged_user()) && !$task->isTrashed()) { ?>
          	<a class="internalLink blank" href="<?php	 	 echo lang('edit task') ?>">
          	<img src="<?php	 	 echo icon_url('edit.gif') ?>" alt="" /></a>
          <?php	 	 } // if ?>
          <?php	 	 if($task->canDelete(logged_user()) && !$task->isTrashed()) { ?>
          	<a class="internalLink blank" href="<?php	 	 echo lang('delete task') ?>">
          	<img src="<?php	 	 echo icon_url('cancel_gray.gif') ?>" alt="" /></a>
          <?php	 	 } // if ?>
        </td>
      </tr>
<?php	 	 } // foreach ?>
   </table>
<?php	 	 } else { ?>
  <?php	 	 echo lang('no open task in task list') ?>
<?php	 	 } // if ?></div>
  
  
    
  <div class="addTask">
<?php	 	 if($task_list->canAddSubTask(logged_user()) && !$task_list->isTrashed()) { ?>
    <div id="addTaskForm<?php	 	 echo lang('add sub task') ?></a></div>
  
    <div id="addTaskForm<?php	 	 echo $task_list->getId() ?>" style="display:none">
      <form class="internalForm" action="<?php	 	 echo $task_list->getAddTaskUrl(false) ?>" method="post">
        <div class="taskListAddTaskText">
          <label for="addTaskText<?php	 	 echo lang('name') ?>:</label>
          <?php	 	 echo textarea_field("task[title]", null, array('class' => 'short', 'id' => 'addTaskText' . $task_list->getId())) ?>
        </div>
        <div class="taskListAddTaskAssignedTo">
          <label for="addTaskAssignTo<?php	 	 echo lang('assign to') ?>:</label>
          <?php	 	 echo assign_to_select_box("task[assigned_to]", $task_list->getProject(), null, array('id' => 'addTaskAssignTo' . $task_list->getId())) ?>
        </div>
		<input type="hidden" id="addTaskMilestoneId<?php	 	 echo $task_list->getMilestoneId() ?>"/>
		<input type="hidden" id="addTaskProjectId<?php	 	 echo $task_list->getProjectId() ?>"/>
		<input type="hidden" id="addTaskTags<?php	 	 echo implode(',',$task_list->getTagNames()) ?>"/>
		<input type="hidden" id="addTaskPriority<?php	 	 echo $task_list->getPriority() ?>"/>
		<input type="hidden" id="addTaskInputType<?php	 	 echo $task_list->getId() ?>" name="task[inputtype]" value="taskview"/>
        <?php	 	 echo lang('cancel') ?></a>
      </form>
    </div>
<?php	 	 } else { ?>
	<?php	 	 if($on_list_page) { ?>
	<?php	 	 echo lang('completed tasks') ?>:
	<?php	 	 } else { ?>
	<?php	 	 echo lang('recently completed tasks') ?>:
	<?php	 	 } // if ?>
<?php	 	 } // if ?>
  </div>
</td></tr></table>

<br/>


  
<?php	 	 if(is_array($task_list->getCompletedSubTasks())) { ?>
  <table style="border:1px solid #717FA1;width:100%; padding-left:10px;"><tr><th style="padding-left:10px;padding-top:4px;padding-bottom:4px;background-color:#E8EDF7;font-size:120%;font-weight:bolder;color:#717FA1;width:100%;"><?php	 	">
  
  <div class="completedTasks">
    <table class="blank">
<?php	 	 ?>
<?php	 	 foreach($task_list->getCompletedSubTasks() as $task) { ?>
<?php	 	 ?>
<?php	 	 if($on_list_page || ($counter <= 5)) { ?>
      <tr>
<?php	 	 if($task->canChangeStatus(logged_user()) && !$task->isTrashed()) { ?>
    <td class="taskCheckbox"><?php	 	 echo checkbox_link($task->getOpenUrl(rawurlencode(get_url('task', 'view_task', array('id' => $task_list->getId())))), true, lang('mark task as open')) ?></td>
<?php	 	 } else { ?>
        <td class="taskCheckbox"><img src="<?php	 	 echo lang('completed task') ?>" /></td>
<?php	 	 } // if ?>
        <td class="taskText">
        	<a class="internalLink" href="<?php	 	 echo clean($task->getTitle()) ?></a> 
          <?php	 	 if($task->canEdit(logged_user()) && !$task->isTrashed()) { ?>
          	<a class="internalLink" href="<?php	 	 echo lang('edit task') ?>">
          	<img src="<?php	 	 echo icon_url('edit.gif') ?>" alt="" /></a>
          <?php	 	 } // if ?> 
          <?php	 	 if($task->canDelete(logged_user()) && !$task->isTrashed()) { ?>
          	<a href="<?php	 	 echo lang('delete task') ?>">
          	<img src="<?php	 	 echo icon_url('cancel_gray.gif') ?>" alt="" /></a>
          <?php	 	 } // if ?>
          <br />
          <?php	 	 if ($task->getCompletedBy() instanceof User) {?>
          	<span class="taskCompletedOnBy">(<?php	 	 echo lang('completed on by', format_date($task->getCompletedOn()), $task->getCompletedBy()->getCardUrl(), clean($task->getCompletedBy()->getDisplayName())) ?>)</span>
          <?php	 	 } else { ?>
          <span class="taskCompletedOnBy">(<?php	 	 echo lang('completed on by', format_date($task->getCompletedOn()), "#", lang("n/a")) ?>)</span>
          <?php	 	 } ?>
        </td>
        <td></td>
      </tr>
<?php	 	 } // if ?>
<?php	 	 } // foreach ?>
<?php	 	 if(!$on_list_page && $counter > 5) { ?>
      <tr>
        <td colspan="2"><a class="internalLink" href="<?php	 	 echo lang('view all completed tasks', $counter) ?></a></td>
      </tr>
<?php	 	 } // if ?>
    </table>
  </div>
</td></tr></table>
<?php	 	 } // if ?>


<?php	 	
	$time_estimate = $task_list->getTimeEstimate();
	$total_minutes = $task_list->getTotalMinutes();

	if ($time_estimate > 0 || $total_minutes > 0){?>
<br/>
<table>
<?php	 	 if ($time_estimate > 0) {?>
<tr><td>
	<div style="font-weight:bold"><?php	 	'?></div></td><td> 
		<?php	 	 echo DateTimeValue::FormatTimeDiff(new DateTimeValue(0), new DateTimeValue($time_estimate * 60), 'hm', 60) ?></td></tr>
<?php	 	 } ?>

<?php	 	 if ($total_minutes > 0 && can_manage_time(logged_user())) {?>
	<tr><td><div style="font-weight:bold"><?php	 	' ?></div></td><td>
		<span style="font-size:120%;font-weight:bold;<?php	 	 echo ($time_estimate > 0 && $total_minutes > $time_estimate) ? 'color:#FF0000':'' ?>">
			<?php	 	 echo DateTimeValue::FormatTimeDiff(new DateTimeValue(0), new DateTimeValue($total_minutes * 60), 'hm', 60) ?>
		</span></td></tr>
<?php	 	 } ?>
</table>
<?php	 	 } ?>

<?php	 	 if ($task_list->isRepetitive()) { ?>
	<div style="font-weight:bold">
	<?php	 	 
		echo '<br>' . lang('this task repeats'). '&nbsp;';
		if ($task_list->getRepeatForever()) echo lang('forever');
		else if ($task_list->getRepeatNum()) echo lang('n times', $task_list->getRepeatNum());
		else if ($task_list->getRepeatEnd()) echo lang('until x', format_date($task_list->getRepeatEnd()));
		echo ", " . lang ('every') . " ";
		if ($task_list->getRepeatD() > 0) echo lang('n days', $task_list->getRepeatD()) . ".";
		else if ($task_list->getRepeatM() > 0) echo lang('n months', $task_list->getRepeatM()) . ".";
		else if ($task_list->getRepeatY() > 0) echo lang('n years', $task_list->getRepeatY()) . ".";
	?>
	</div>
<?php	 	 } ?>
  