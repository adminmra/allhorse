<div style="padding:10px">
<?php	 		 	
  require_javascript('og/modules/updatePermissionsForm.js');
  set_page_title(lang('permissions'));
  project_tabbed_navigation(PROJECT_TAB_PEOPLE);
  project_crumbs(array(
    array(lang('people'), get_url('project', 'people')),
    array(lang('permissions'))
  ));
	//add_stylesheet_to_page('project/permissions.css');
?>
<?php	 	
  $quoted_permissions = array();
  foreach($permissions as $permission_id => $permission_text) {
    $quoted_permissions[] = "'$permission_id'";
  } // foreach
?>
<script>
  App.modules.updatePermissionsForm.owner_company_id = <?php	 	
  App.modules.updatePermissionsForm.project_permissions = new Array(<?php	 	
</script>

<label><b><?php	 	 echo lang('edit permissions') ?></b></label>
<label><k><?php	 	 echo lang('edit permissions explanation') ?></k></label>
<?php	 	 if(isset($companies) && is_array($companies) && count($companies)) { ?>
<form class="internalForm" action="<?php	 	 echo get_url('project', 'permissions') ?>" method="post">
<div id="projectCompanies">
<?php	 	 foreach($companies as $company) { ?>
<?php	 	 if($company->countUsers() > 0) { ?>
<fieldset>
<legend><?php	 	 echo clean($company->getName()) ?></legend>
  <div class="projectCompany" style="border:0">
    <div class="projectCompanyLogo"><img src="<?php	 	 echo clean($company->getName()) ?>" /></div>
    <div class="projectCompanyMeta">
      <div class="projectCompanyTitle">
<?php	 	 if($company->isOwner()) { ?>
<!--        <label><?php	 	 //echo clean($compangetName()) ?></label>-->
        <input type="hidden" name="project_company_<?php	 	 echo $company->getId() ?>" value="checked" />
<?php	 	 } else { ?>
        <?php	 	 echo clean($company->getName()) ?></label>
<?php	 	 } // if ?>
      </div>
      <div class="projectCompanyUsers" id="project_company_users_<?php	 	 echo $company->getId() ?>">
        <table class="blank">
<?php	 	 if($users = $company->getUsers()) { ?>
<?php	 	 foreach($users as $user) { ?>
          <tr class="user">
            <td>
<?php	 	 if($user->isAccountOwner()) { ?>
              <img src="<?php	 	 echo clean($user->getDisplayName()) ?></label>
              <input type="hidden" name="<?php	 	 echo 'project_user_' . $user->getId() ?>" value="checked" />
<?php	 	 } else { ?>
              <?php	 	 echo clean($user->getDisplayName()) ?></label>
<?php	 	 } // if ?>
  
<?php	 	 if($user->isAdministrator()) { ?>
              <span class="desc">(<?php	 	 echo lang('administrator') ?>)</span>
<?php	 	 } // if ?>
            </td>
            <td>
<?php	 	 if(!$company->isOwner()) { ?>
              <div class="projectUserPermissions" id="user_<?php	 	 echo $user->getId() ?>_permissions">
                <div><?php	 	 echo lang('all permissions') ?></label></div>
<?php	 	 foreach($permissions as $permission_id => $permission_text) { ?>            
                <div><?php	 	 echo $permission_text ?></label></div>
<?php	 	 } // foreach ?>
              </div>
<?php	 	 } // if ?>
            </td>
          </tr>
<?php	 	 if(!$company->isOwner()) { ?>
          <script>
            if(!document.getElementById('project_user_<?php	 	 echo $user->getId() ?>').checked) {
              document.getElementById('user_<?php	 	
            } // if
          </script>
<?php	 	 } // if ?>
<?php	 	 } // foreach ?>
<?php	 	 } else { ?>
          <tr>
            <td colspan="2"><?php	 	 echo lang('no users in company') ?></td>
          </tr>
<?php	 	 } // if ?>
        </table>
      </div>
      <div class="clear"></div>
    </div>
  </div>
<?php	 	 if(!$company->isOwner()) { ?>
  <script>
    if(!document.getElementById('project_company_<?php	 	 echo $company->getId() ?>').checked) {
      document.getElementById('project_company_users_<?php	 	
    } // if
  </script>
<?php	 	 } // if ?>
</fieldset>
<?php	 	 } // if ?>
<?php	 	 } // foreach ?>

<?php	 	 echo submit_button(lang('update people')) ?>
  <input type="hidden" name="process" value="process" />
</div>
</form>
<?php	 	 } // if ?>
</div>
