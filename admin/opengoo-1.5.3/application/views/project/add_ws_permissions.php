
	<!-- permissions -->

		<?php	 		 	 if (isset($companies) && is_array($companies) && count($companies)) { ?>
			<div id="projectCompaniesContainer"><div id="projectCompanies">
			<?php	 	 foreach ($companies as $company) { ?>
				<?php	 	 if ($company->countUsers() > 0) { ?>
				<fieldset>
				<legend><?php	 	 echo clean($company->getName()) ?></legend>
					<div class="projectCompany" style="border:0">
					<div class="projectCompanyLogo"><img src="<?php	 	 echo clean($company->getName()) ?>" /></div>
					<div class="projectCompanyMeta">
						<div class="projectCompanyTitle">
					<?php	 	 //if($company->isOwner()) { ?>
						<!-- <label><?php	 	 //echo clean($compangetName()) ?></label>-->
							<!-- input type="hidden" name="project_company_<?php	 	 echo $company->getId() ?>" value="checked" / -->
					<?php	 	 //} else {
						$has_checked_users = false;
						foreach ($company->getUsers() as $user) {
							if ($user->isProjectUser($project)) {
								$has_checked_users = true;
								break;
							}
						}
						echo checkbox_field('project_company_' . $company->getId(), $has_checked_users, array('id' => $genid . 'project_company_' . $company->getId(), 'tabindex' => $field_tab++, 'onclick' => "App.modules.updatePermissionsForm.companyCheckboxClick(" . $company->getId() . ",'".$genid."')")) ?> <label for="<?php	 	 echo clean($company->getName()) ?></label>
					<?php	 	 //} // if ?>
						</div>
						<div class="projectCompanyUsers" id="<?php	 	 echo $company->getId() ?>">
							<table class="blank">
					<?php	 	 if ($users = $company->getUsers()) { ?>
<?php	 							 foreach ($users as $user) { ?>
								<tr class="user">
									<td>
								<?php	 	 echo clean($user->getDisplayName()) ?></label>
<?php	 	 //							 } // if ?>
<?php	 								 if($user->isAdministrator()) {?> 
										<span class="desc">(<?php	 	 echo lang('administrator') ?>)</span>
<?php	 								 } // if ?>
									</td>
									<td>
							<?php	 	 //if(!$company->isOwner()) { ?>
										<div class="projectUserPermissions" id="<?php	 	 echo $genid ."user_".$user->getId() ?>_permissions">
										<div><?php	 	 echo lang('all permissions') ?></label></div>
								<?php	 	 foreach ($permissions as $permission_id => $permission_text) { ?>						
										<div><?php	 	 echo $permission_text ?></label></div>
								<?php	 	 } // foreach ?>
										</div>
									<script>
										if (!document.getElementById( '<?php	 	 echo $user->getId() ?>').checked) {
											document.getElementById( '<?php	 	
										} // if
									</script>
							<?php	 	 //} // if ?>
									</td>
								</tr>
						<?php	 	 } // foreach ?>
					<?php	 	 } else { ?>
								<tr>
								<td colspan="2"><?php	 	 echo lang('no users in company') ?></td>
								</tr>
					<?php	 	 } // if ?>
							</table>
						</div>
						<div class="clear"></div>
					</div>
					</div>
					<?php	 	 //if (!$company->isOwner()) { ?>
						<script>
							if(!document.getElementById( '<?php	 	 echo $company->getId() ?>').checked) {
								document.getElementById( '<?php	 	
							} // if
						</script>
					<?php	 	 //} // if ?>
				</fieldset>
				<?php	 	 } // if ?>
			<?php	 	 } // foreach ?>
			</div></div>
		<?php	 	 } // if ?>
	
	<!-- /permissions -->
	
