<?php	 		 	
require_javascript('og/modules/addMessageForm.js');
set_page_title($webpage->isNew() ? lang('add webpage') : lang('edit webpage'));
if ($webpage->isNew()) {
	$project = active_or_personal_project();
} else {
	$project = $webpage->getProject();
}
$genid = gen_id();
$object = $webpage;
?>

<form style='height: 100%; background-color: white' class="internalForm"
	action="<?php	 	 echo $webpage->isNew() ? get_url('webpage', 'add') : $webpage->getEditUrl() ?>"
	method="post">

<div class="webpage">
<div class="coInputHeader">
<div class="coInputHeaderUpperRow">
<div class="coInputTitle">
<table style="width: 535px">
	<tr>
		<td><?php	 	 echo $webpage->isNew() ? lang('new webpage') : lang('edit webpage') ?>
		</td>
		<td style="text-align: right"><?php	 	margin-left:10px', 'tabindex' => '20')) ?></td>
	</tr>
</table>
</div>

</div>
<div><?php	 	 echo text_field('webpage[title]', array_var($webpage_data, 'title'), array('class' => 'title', 'tabindex' => '1', 'id' => 'webpageFormTitle')) ?>
</div>

<?php	 	 ?>

<div style="padding-top: 5px"><a href="#" class="option" tabindex=0
	onclick="og.toggleAndBolden('add_webpage_select_workspace_div', this)"><?php	 	 echo lang('workspace') ?></a>
- <a href="#" class="option" tabindex=0
	onclick="og.toggleAndBolden('add_webpage_tags_div', this)"><?php	 	 echo lang('tags') ?></a>
- <a href="#" class="option" tabindex=0
	onclick="og.toggleAndBolden('add_webpage_description_div', this)"><?php	 	 echo lang('description') ?></a>
- <a href="#" class="option" tabindex=0
	onclick="og.toggleAndBolden('add_custom_properties_div', this)"><?php	 	 echo lang('custom properties') ?></a>
- <a href="#" class="option"
	onclick="og.toggleAndBolden('<?php	 	 echo lang('object subscribers') ?></a>
<?php	 	 if($object->isNew() || $object->canLinkObject(logged_user(), $project)) { ?>
- <a href="#" class="option"
	onclick="og.toggleAndBolden('<?php	 	 echo lang('linked objects') ?></a>
<?php	 	 foreach ($categories as $category) { ?> - <a href="#"
	class="option"
	<?php	 	 ?>
	onclick="og.toggleAndBolden('<?php	 	 echo lang($category['name'])?></a>
	<?php	 	 } ?></div>
</div>
<div class="coInputSeparator"></div>
<div class="coInputMainBlock"><?php	 	 
$show_help_option = user_config_option('show_context_help');
if ($show_help_option == 'always' || ($show_help_option == 'until_close' && user_config_option('show_add_webpage_context_help', true, logged_user()->getId()))) {?>
<div id="webpagePanelContextHelp"
	class="contextHelpStyle"><?php	 	 ?>
</div>
<?php	 	 }?>
<div id="add_webpage_select_workspace_div" style="display: none">
<fieldset><?php	 	 
$show_help_option = user_config_option('show_context_help');
if ($show_help_option == 'always' || ($show_help_option == 'until_close' && user_config_option('show_add_webpage_workspace_context_help', true, logged_user()->getId()))) {?>
<div id="webpagePanelContextHelp"
	class="contextHelpStyle"><?php	 	 ?>
</div>
<?php	 	 echo lang('workspace') ?></legend>
	<?php	 	 if ($object->isNew()) {
			echo select_workspaces('ws_ids', null, array($project), $genid.'ws_ids');
		} else {
			echo select_workspaces('ws_ids', null, $object->getWorkspaces(), $genid.'ws_ids');
		} ?>
</fieldset>
</div>

<div id="add_webpage_tags_div" style="display: none">
<fieldset><?php	 	 
$show_help_option = user_config_option('show_context_help');
if ($show_help_option == 'always' || ($show_help_option == 'until_close' && user_config_option('show_add_webpage_tags_context_help', true, logged_user()->getId()))) {?>
<div id="webpagePanelContextHelp"
	class="contextHelpStyle"><?php	 	 ?>
</div>
<?php	 	 ?>
</fieldset>
</div>

<div id="add_webpage_description_div" style="display: none">
<fieldset><?php	 	 
$show_help_option = user_config_option('show_context_help');
if ($show_help_option == 'always' || ($show_help_option == 'until_close' && user_config_option('show_add_webpage_description_context_help', true, logged_user()->getId()))) {?>
<div id="webpagePanelContextHelp"
	class="contextHelpStyle"><?php	 	 ?>
</div>
<?php	 	 echo label_tag(lang('description'), 'webpageFormDesc') ?>
</legend> <?php	 	 echo textarea_field('webpage[description]', array_var($webpage_data, 'description'), array('class' => 'long', 'id' => 'webpageFormDesc', 'tabindex' => '40')) ?>
</fieldset>
</div>

<div id='add_custom_properties_div' style="display: none">
<fieldset><?php	 	 
$show_help_option = user_config_option('show_context_help');
if ($show_help_option == 'always' || ($show_help_option == 'until_close' && user_config_option('show_add_webpage_custom_properties_context_help', true, logged_user()->getId()))) {?>
<div id="webpagePanelContextHelp"
	class="contextHelpStyle"><?php	 	 ?>
</div>
<?php	 	 echo render_object_custom_properties($object, 'ProjectWebPages', false) ?><br />
<br />
<?php	 	 ?></fieldset>
</div>


<div id="<?php	 	 echo $genid ?>add_subscribers_div" style="display: none">
<fieldset><?php	 	 
$show_help_option = user_config_option('show_context_help');
if ($show_help_option == 'always' || ($show_help_option == 'until_close' && user_config_option('show_add_webpage_subscribers_context_help', true, logged_user()->getId()))) {?>
<div id="webpagePanelContextHelp"
	class="contextHelpStyle"><?php	 	 ?>
</div>
<?php	 	 echo lang('object subscribers') ?></legend>
<div id="<?php	 	 ?>
</div>
</fieldset>
</div>

<script>
var wsch = Ext.getCmp('<?php	 	
wsch.on("wschecked", function(arguments) {
	var uids = App.modules.addMessageForm.getCheckedUsers('<?php	 	
	Ext.get('<?php	 	 echo $genid ?>add_subscribers_content').load({
		url: og.getUrl('object', 'render_add_subscribers', {
			workspaces: this.getValue(),
			users: uids,
			genid: '<?php	 	 echo $genid ?>',
			object_type: '<?php	 	 echo get_class($object->manager()) ?>'
		}),
		scripts: true
	});
}, wsch);
</script>

<?php	 	 if($object->isNew() || $object->canLinkObject(logged_user(), $project)) { ?>
<div style="display: none"
	id="<?php	 	 echo $genid ?>add_linked_objects_div">
<fieldset><?php	 	 
$show_help_option = user_config_option('show_context_help');
if ($show_help_option == 'always' || ($show_help_option == 'until_close' && user_config_option('show_add_webpage_linked_objects_context_help', true, logged_user()->getId()))) {?>
<div id="webpagePanelContextHelp"
	class="contextHelpStyle"><?php	 	 ?>
</div>
<?php	 	 echo render_object_link_form($object) ?>
</fieldset>
</div>
<?php	 	 } // if ?>

<div><?php	 	 echo text_field('webpage[url]', array_var($webpage_data, 'url'), array('class' => 'title', 'tabindex' => '10', 'id' => 'webpageFormURL')) ?>
</div>

<?php	 	 foreach ($categories as $category) { ?>
<div <?php	 	 if (!$category['visible']) echo 'style="display:none"' ?>
	id="<?php	 	 echo $genid . $category['name'] ?>">
<fieldset><legend><?php	 	 ?></legend>
<?php	 	 echo $category['content'] ?></fieldset>
</div>
<?php	 	 } ?>

<div><?php	 	 echo render_object_custom_properties($object, 'ProjectWebPages', true) ?>
</div>
<br />

<?php	 	 echo submit_button($webpage->isNew() ? lang('add webpage') : lang('save changes'), 's',
array('tabindex' => '200')) ?></div>
</div>
</form>

<script>
	Ext.get('webpageFormTitle').focus();
</script>
