<div id="userbox">
	<?php	 	 echo lang('logout') ?></a>) :
	<?php	 	 foreach ($_userbox_extra_crumbs as $crumb) {
		echo '<a class="internalLink"';
		if (isset($crumb['target'])) echo ' target="' . $crumb['target'] .'"';
		echo ' href="' . $crumb['url'] . '">';
		echo $crumb['text'];
		echo '</a> | ';
	} ?> 
	<?php	 	 if (logged_user()->isMemberOfOwnerCompany() && logged_user()->isAdministrator()) { ?>
		<a class="internalLink" target="administration" href="<?php	 	 echo lang('administration') ?></a> |
	<?php	 	 } ?>
	<a class="internalLink" target="account" href="<?php	 	 echo lang('account') ?></a> |
	<a target="_blank" href="<?php	 	 echo lang('help') ?></a>
</div>