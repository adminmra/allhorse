<?php	 		 	
	$genid = gen_id();
?>
<form style='height:100%;background-color:white' class="internalForm" action="<?php	 	 echo get_url('reporting', 'total_task_times_vs_estimate_comparison') ?>" method="post" enctype="multipart/form-data">

<div class="reportTotalTimeParams">
<div class="coInputHeader">
	<div class="coInputHeaderUpperRow">
		<div class="coInputTitle"><?php	 	 echo lang('task time report') ?></div>
	</div>
</div>
<div class="coInputSeparator"></div>
<div class="coInputMainBlock">

	<table>
		<tr>
			<td><b><?php	 	</b></td>
			<td align='left'><?php	 	 
				echo pick_date_widget2('report[start]',DateTimeValueLib::now(), $genid);
			?></td>
		</tr>
		<tr style='height:30px;'>
			<td ><b><?php	 	</b></td>
			<td align='left'><?php	 	 
				echo pick_date_widget2('report[end]',DateTimeValueLib::now(), $genid);
			?></td>
		</tr>
		<tr style='height:30px;'>
			<td><b><?php	 	</b></td>
			<td align='left'><?php	 	 
				$options = array();
				$options[] = option_tag('-- ' . lang('anyone') . ' --', 0, array('selected' => 'selected'));
				foreach($users as $user){
					$options[] = option_tag($user->getDisplayName(),$user->getId());
				}
				echo select_box('report[user]', $options);
			?></td>
		</tr>
		<tr style='height:30px;'>
			<td><b><?php	 	</b></td>
			<td align='left'>
				<?php	 	
					echo checkbox_field('report[include_subworkspaces]', true, array('id' => 'report[include_subworkspaces]' )) ?> 
	      <label for="<?php	 	 echo lang('include subworkspaces') ?></label>
			</td>
		</tr>
		<tr style='height:30px;'>
			<td colspan=2 align='left'>
				<?php	 	 echo checkbox_field('report[show_details]', false, array('id' => 'report[show_details]' )) ?> 
	      <label for="<?php	 	 echo lang('show details') ?></label>
			</td>
		</tr>
	</table>
	
<br/>
<?php	 	margin-left:10px')) ?>
</div>
</div>

</form>