<?php	 	 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
	<!-- script src="http://www.savethedevelopers.org/say.no.to.ie.6.js"></script -->
	<title><?php	 	 echo clean(CompanyWebsite::instance()->getCompany()->getName()) . ' - ' . PRODUCT_NAME ?></title>
	<?php	 	 echo link_tag(with_slash(ROOT_URL)."favicon.ico", "rel", "shortcut icon") ?>
	<?php	 	 echo add_javascript_to_page("og/app.js") // loaded first because it's needed for translating?>
	<?php	 	 ?>
	<?php	 	 //echo add_javascript_to_page(with_slash(ROOT_URL) . 'language/' . Localization::instance()->getLocale() . "/lang.js") ?>
	<?php	 	 charset=utf-8', true) ?>
<?php	 	

	$version = include "version.php";
	if (defined('COMPRESSED_CSS') && COMPRESSED_CSS) {
		echo stylesheet_tag("ogmin.css");
	} else {
		echo stylesheet_tag('website.css');
	}
	$theme = config_option('theme', DEFAULT_THEME);
	if (is_file(PUBLIC_FOLDER . "/assets/themes/$theme/stylesheets/custom.css")) {
		echo stylesheet_tag('custom.css');
	}
	$css = array();
	Hook::fire('autoload_stylesheets', null, $css);
	foreach ($css as $c) {
		echo stylesheet_tag($c);
	}

	if (defined('COMPRESSED_JS') && COMPRESSED_JS) {
		$jss = array("ogmin.js");
	} else {
		$jss = include "javascripts.php";
	}
	Hook::fire('autoload_javascripts', null, $jss);
	if (defined('USE_JS_CACHE') && USE_JS_CACHE) {
		echo add_javascript_to_page(with_slash(ROOT_URL)."public/tools/combine.php?version=$version&type=javascript&files=".implode(',', $jss));
	} else {
		foreach ($jss as $onejs) {
			echo add_javascript_to_page($onejs);
		}
	}
	$ext_lang_file = get_ext_language_file(get_locale());
	if ($ext_lang_file)	{
		echo add_javascript_to_page("extjs/locale/$ext_lang_file");
	}
	?>
	<?php	 	 if (config_option("show_feed_links")) { ?>
		<link rel="alternate" type="application/rss+xml" title="<?php	 	 echo logged_user()->getRecentActivitiesFeedUrl() ?>" />
	<?php	 	 } ?>
</head>
<body id="body" <?php	 	 echo render_body_events() ?>>

<div id="loading">
	<img src="<?php	 	 echo lang("loading") ?>...
</div>

<div id="subWsExpander" onmouseover="clearTimeout(og.eventTimeouts['swst']);" onmouseout="og.eventTimeouts['swst'] = setTimeout('og.HideSubWsTooltip()', 2000);" style="display:none;top:10px;"></div>

<?php	 	 echo render_page_javascript() ?>
<?php	 	 echo render_page_inline_js() ?>

<!-- header -->
<div id="header">
	<div id="headerContent">
	    <table class="headerLogoAndWorkspace"><tr><td style="width:60px">
			<div id="logodiv"></div>
		</td><td>
			<div id="wsCrumbsWrapper">
				<table><tr><td>
					<div id="wsCrumbsDiv">
						<div style="font-size:150%;display:inline;">
							<a href="#" style="display:inline;line-height:28px" onclick="og.expandSubWsCrumbs(0)"><?php	 	 echo lang('all') ?></a>
						</div>
					</div>
				</td><td>
					<div id="wsTagCrumbs"></div>
				</td></tr></table>
			</div>
		</td></tr></table>
		<div id="userboxWrapper"><?php	 	 echo render_user_box(logged_user()) ?></div>
		<div id="searchbox">
			<form class="internalForm" action="<?php	 	 echo ROOT_URL . '/index.php' ?>" method="get">
				<table><tr><td>
				<?php	 	
				$search_field_default_value = lang('search') . '...';
				$search_field_attrs = array(
				'onfocus' => 'if (value == \'' . $search_field_default_value . '\') value = \'\'',
				'onblur' => 'if (value == \'\') value = \'' . $search_field_default_value . '\''); ?>
				<?php	 	 echo input_field('search_for', $search_field_default_value, $search_field_attrs) ?>
				</td><td id="searchboxSearch">
				<button type="submit"><?php	 	 echo lang('search button caption') ?></button>
				</td><td style="padding-left:10px"><div id="quickAdd" style="padding-top:1px"></div></td></tr></table>
				<input type="hidden" name="c" value="search" />
				<input type="hidden" name="a" value="search" />
				<input type="hidden" name="current" value="search" />
				<input type="hidden" id="hfVars" name="vars" value="dashboard" />
			</form>
		</div>
		<?php	 	 Hook::fire('render_page_header', null, $ret) ?>
	</div>
</div>
<!-- /header -->

<!-- footer -->
<div id="footer">
	<div id="copy">
		<?php	 	 if(is_valid_url($owner_company_homepage = owner_company()->getHomepage())) { ?>
			<?php	 	 echo lang('footer copy with homepage', date('Y'), $owner_company_homepage, clean(owner_company()->getName())) ?>
		<?php	 	 } else { ?>
			<?php	 	 echo lang('footer copy without homepage', date('Y'), clean(owner_company()->getName())) ?>
		<?php	 	 } // if ?>
	</div>
	<div id="productSignature"><?php	 	 echo product_signature() ?></div>
</div>
<!-- /footer -->

<script>
// OG config options
og.pageSize = <?php	 	
og.timeFormat24 = <?php	 	
og.hostName = '<?php	 	
og.maxUploadSize = <?php	 	
og.rememberGUIState = <?php	 	
<?php	 	 if (user_config_option("rememberGUIState")) { ?>
og.initialGUIState = <?php	 	
<?php	 	 } ?>
<?php	 	
if ($initialWS === "remember") {
	$initialWS = user_config_option('lastAccessedWorkspace', 0);
}
?>
<?php	 	 if (user_config_option('show_unread_on_title')) { ?>
og.showUnreadEmailsOnTitle = true;
<?php	 	 } ?>
og.pollForEmail = <?php	 	
og.initialWorkspace = '<?php	 	
og.initialURL = '<?php	 	
og.loggedUser = {
	id: <?php	 	 echo logged_user()->getId() ?>,
	username: <?php	 	 echo json_encode(logged_user()->getUsername()) ?>,
	displayName: <?php	 	 echo json_encode(logged_user()->getDisplayName()) ?>,
	isAdmin: <?php	 	 echo logged_user()->isAdministrator() ? 'true' : 'false' ?>
};
og.hasNewVersions = <?php	 	
	if (config_option('upgrade_last_check_new_version', false) && logged_user()->isAdministrator()) {
		echo json_encode(lang('new OpenGoo version available', "#", "og.openLink(og.getUrl('administration', 'upgrade'))"));
	} else {
		echo "false";
	}
?>;
og.enableNotesModule = <?php	 	
og.enableEmailModule = <?php	 	
og.enableContactsModule = <?php	 	
og.enableCalendarModule = <?php	 	
og.enableDocumentsModule = <?php	 	
og.enableTasksModule = <?php	 	
og.enableWeblinksModule = <?php	 	
og.enableTimeModule = <?php	 	
og.enableReportingModule = <?php	 	
og.daysOnTrash = <?php	 	
og.showCheckoutNotification  = <?php	 	
Ext.Ajax.timeout = <?php	 	
og.musicSound = new Sound();
og.systemSound = new Sound();

var quickAdd = new og.QuickAdd({renderTo:'quickAdd'});

<?php	 	 if (!defined('DISABLE_JS_POLLING') || !DISABLE_JS_POLLING) { ?>
setInterval(function() {
	og.openLink(og.getUrl('object', 'popup_reminders'), {
		hideLoading: true,
		hideErrors: true,
		preventPanelLoad: true
	});
}, 60000);
<?php	 	 } ?>

og.date_format = '<?php	 	
og.calendar_start_day = <?php	 	
og.draftAutosaveTimeout = <?php	 	

og.loadEmailAccounts('view');
og.loadEmailAccounts('edit');

</script>
<?php	 	?>
</body>
</html>
