<?php	 		 	 return array(
	'new message' => 'Nuovo messaggio',
	'new comment' => 'Nuovo commento',
	'your account created' => 'Account creato',
	'your password' => 'Password',
	'milestone assigned to you' => 'Ti è stata assegnata un traguardo',
	'task assigned to you' => 'Ti è stata assegnata una attività',
	'hi john doe' => 'Ciao {0}',
	'user password reseted' => 'Vecchia password annullata. La nuova password è \'{0}\'.',
	'dont reply wraning' => 'QUESTA È UNA NOTIFICA AUTOMATICA. NON RISPONDERE A QUESTA EMAIL!',
	'new message posted' => 'Nuovo messaggio "{0}" inviato',
	'new task' => 'Nuova attività',
	'new task created' => 'La nuova attività "{0}" è stata creata nel Progetto "{1}"',
	'view new message' => 'Vedi questo messaggio',
	'view new task' => 'Vedi questa attività',
	'new comment posted' => 'È stato spedito un nuovo commento su "{0}"',
	'view new comment' => 'Vedi questo commento',
	'user created your account' => '{0} ha creato un nuovo account per te',
	'visit and login' => 'Visita {0} ed entra con',
	'milestone assigned' => 'Ti è stata assegnata il traguardo "{0}"',
	'task assigned' => 'Ti è stata assegnata l\'attività "{0}"',
	'view assigned milestones' => 'Vedi traguardi',
	'view assigned tasks' => 'Vedi attività',
	'reply mail' => 'Rispondi',
	'write mail' => 'Scrivi mail',
	'mail to' => 'A',
	'mail account' => 'Account',
	'mail account desc' => ' Account dal quale sarà spedita la mail',
	'mail subject' => 'Oggetto',
	'mail CC' => 'CC',
	'mail CC desc' => ' I destinatari Carbon Copy riceveranno una mail identica',
	'mail BCC' => 'BCC',
	'mail BCC desc' => ' I destinatari Blind  Carbon Copy riceveranno una mail identica senza che gli altri destinatari lo sappiano',
	'mail from' => 'Da',
	'mail body' => 'Corpo',
	'send mail' => 'Spedisci',
	'smtp server' => 'Server SMTP',
	'mail account smtp server description' => ' Il server SMTP serve a spedire le email',
	'smtp use auth' => 'Autenticazione server SMTP',
	'mail account smtp use auth description' => ' Modo in cui sarai autenticato dal server SMTP. Normalmente \'Usa ricevente\' è l\'opzione corretta.',
	'no smtp auth' => 'Nessuna autenticazione',
	'same as incoming' => 'Usa ricevente',
	'smtp specific' => 'Inserisci nome utente e password SMTP',
	'smtp username' => 'Nome utente SMTP',
	'mail account smtp username description' => ' Nome utente server SMTP',
	'smtp password' => 'Password SMTP',
	'mail account smtp password description' => ' Password  server SMTP',
	'smtp port' => 'Porta server SMTP',
	'mail account smtp port description' => ' Si tratta della porta su cui è in ascolto il servizio SMTP. Normalmente il valore di default è 25.',
	'format plain' => 'Formato Testo in chiaro',
	'format html' => 'Formato HTML',
	'mail options' => 'Opzioni',
	'forward mail' => 'Inoltra',
	'reply to all mail' => 'Rispondi a tutti',
	'switch format warn' => 'Scegliendo il modo testo verrà persa la formattazione HTML. Continuo?',
	'success mark objects' => 'Mail aggiornata',
	'error mark email' => 'Errore aggiornando lo stato email',
	'draft' => 'Bozza',
	'success save mail' => 'Bozza salvata',
	'discard' => 'Rinuncia',
	'mail address' => 'Indirizzo Email',
	'mail address description' => 'L\'indirizzo Email è quello che le persone usano per inviarti email',
	'unread emails' => 'Email da leggere',
	'view due task' => 'Vedi attività obbligatorie',
	'due task email' => 'L\'attività "{0}" è obbligatoria su "{1}" ed è ancora pendente',
	'due task' => 'Attività obbligatoria presto',
	'due task reminder' => 'Attività obbligatoria presto',
	'task has been modified' => 'L\'attività "{0}" nel Progetto "{1}" è stata modificata',
	'task modified' => 'Attività modificata',
	'view task' => 'Vedi attività',
	'original message' => 'Messaggio originale',
	'mail sent' => 'Inviato',
	'new notification task' => 'Nuova attività \'{0}\'',
	'new notification task desc' => 'È stata aggiunta una nuova attività denominata \'{0}\' ',
	'new notification event' => 'Nuovo evento \'{0}\'',
	'new notification event desc' => 'È stato aggiunto un nuovo evento denominato \'{0}\'',
	'new notification company' => 'Nuova Società \'{0}\'',
	'new notification company desc' => 'È stata aggiunta una nuova Società denominata \'{0}\'',
	'new notification contact' => 'Nuovo Contatto \'{0}\'',
	'new notification contact desc' => '{1} ha aggiunto un nuovo contatto denominato \'{0}\' ',
	'new notification message' => 'Nuova Nota \'{0}\'',
	'new notification message desc' => '{1} ha aggiunto una nuova Nota denominata \'{0}\'',
	'new notification file' => 'Nuovo file \'{0}\'',
	'new notification file desc' => '{1} ha aggiunto un nuovo file denominato \'{0}\'',
	'new notification webpage' => 'Nuovo Collegamento web \'{0}\'',
	'new notification webpage desc' => '{1} ha aggiunto un nuovo Collegamento web denominato \'{0}\'',
	'new notification milestone' => 'Nuovo Traguardo \'{0}\'',
	'new notification milestone desc' => '{1} ha aggiunto un nuovo Traguardo denominato \'{0}\'',
	'modified notification task' => 'Attività \'{0}\' modificata',
	'modified notification task desc' => '{1} ha modificato l\'Attività \'{0}\'',
	'modified notification event' => 'Evento \'{0}\' modificato',
	'modified notification event desc' => '{1} ha modificato l\'Evento \'{0}\'',
	'modified notification company' => 'Società \'{0}\' modificata',
	'modified notification company desc' => '{1} ha modificato la Società \'{0}\'',
	'modified notification contact' => 'Contatto \'{0}\' modificato',
	'modified notification contact desc' => '{1} ha modificato il Contatto \'{0}\'',
	'modified notification message' => 'Nota \'{0}\' modificata',
	'modified notification message desc' => '{1} ha modificato la Nota \'{0}\'',
	'modified notification file' => 'File \'{0}\' modificato',
	'modified notification file desc' => '{1} ha modificato il file \'{0}\'',
	'modified notification webpage' => 'Collegamento web \'{0}\' modificato',
	'modified notification webpage desc' => '{1} ha modificato il Collegamento web \'{0}\'',
	'modified notification milestone' => 'Traguardo \'{0}\' modificato',
	'modified notification milestone desc' => '{1} ha modificato il traguardo \'{0}\'',
	'deleted notification task' => 'Attività \'{0}\' cestinata',
	'deleted notification task desc' => '{1} ha cestinato l\'Attività \'{0}\'',
	'deleted notification event' => 'Evento \'{0}\' cestinato',
	'deleted notification event desc' => '{1} ha cestinato l\'Attività \'{0}\'',
	'deleted notification company' => 'Società \'{0}\' cestinata',
	'deleted notification company desc' => '{1} ha cestinato la Società \'{0}\'',
	'deleted notification contact' => 'Contatto \'{0}\' cestinato',
	'deleted notification contact desc' => '{1} ha cestinato il Contatto \'{0}\'',
	'deleted notification message' => 'Nota \'{0}\' cestinata',
	'deleted notification message desc' => '{1} ha cestinato la Nota \'{0}\'',
	'deleted notification file' => 'File \'{0}\' cestinato',
	'deleted notification file desc' => '{1} ha cestinato il File \'{0}\'',
	'deleted notification webpage' => 'Collegamento web \'{0}\' cestinato',
	'deleted notification webpage desc' => '{1} ha cestinato il collegamento web \'{0}\'',
	'deleted notification milestone' => 'Traguardo \'{0}\' cestinato',
	'deleted notification milestone desc' => '{1} ha cestinato il Traguardo \'{0}\'',
	'user password reseted desc' => 'Password azzerata. Adesso puoi entrare in {0} a {1} con le credenziali seguenti:',
	'user password reseted username' => 'Nome utente: {0}',
	'user password reseted password' => 'Password: {0}',
	'outgoing transport type' => 'Connessione sicura',
	'mail account outgoing transport type description' => 'Scegli "ssl" o "tls" per una connessione sicura',
	'smtp settings' => 'Preferenze SMTP',
	'due_date reminder notification task' => 'Promemoria attività obbligatorie',
	'due_date task reminder desc' => 'L\'attività "{0}" è obbligatoria in "{1}"  ed è ancora pendente',
	'due_date reminder notification milestone' => 'Promemoria traguardo obbligatorio',
	'due_date milestone reminder desc' => 'Il traguardo "{0}" è obbligatorio in "{1}"  ed è ancora pendente',
	'start reminder notification event' => 'Promemoria evento',
	'start event reminder desc' => 'L\'efento "{0}" inizia in "{1}"',
	'view file' => 'Vedi file',
	'view message' => 'Vedi messaggio',
	'view comment' => 'Vedi commento',
	'view company' => 'Vedi Società',
	'view contact' => 'Vedi contatto',
	'view mail account' => 'Vedi mail account',
	'view email' => 'Vedi email',
	'view emailunclassified' => 'Vedi email',
	'view chart' => 'Vedi grafico',
	'view event' => 'Vedi eventi',
	'view file_revision' => 'Vedi revisione file',
	'view form' => 'Vedi modulo',
	'view milestone' => 'Vedi traguardo',
	'view webpage' => 'Vedi collegamento web',
	'view project' => 'Cedi progetto',
	'view tag' => 'Vedi etichetta',
	'view template' => 'Vedi modello',
	'view timeslot' => 'Vedi compito',
	'view user' => 'Vedi utente',
	'closed notification task' => 'L\'attività \'{0}\' è stata completata',
	'closed notification task desc' => 'L\'attività \'{0}\' è stata completata da {1}.',
	'closed notification milestone' => 'Il traguardo \'{0}\' è stato raggiunto',
	'closed notification milestone desc' => 'Il traguardo \'{0}\' è stato raggiunto da {1}.',
	'password expiration reminder' => 'Promemoria scadenza password',
	'password expires days' => 'La tua password scadrà tra {0} giorni',
	'mail attachments' => 'Allegati',
	'select attachment' => 'Sceglie carica un allegato',
	'attach from workspace' => 'Scegli l\'allegato dal progetto',
); ?>
