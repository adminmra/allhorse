<?php	 		 	

// Return array of langs
return array(
	'chelp pending tasks widget' => 'This widget displays your incomplete tasks. You can view only incomplete tasks assigned to you, or you can change this setting by accessing your dashboard options.',
	'chelp documents widget' => 'This widget displays the latest documents in the selected workspace.',
 	'chelp active tasks widget' => 'This widget displays your active and paused tasks.',
 	'chelp late tasks widget' => 'This widget displays your late tasks and milestones, as the ones due today, ordered by due date.',
 	'chelp calendar widget' => 'This widget displays the current week\'s events, due tasks and milestones. You can create a new event by clicking on a day.',
 	'chelp comments widget' => 'This widget displays the latest comments for objects in the current workspace.',
 	'chelp dashboard info widget' => 'This widget displays information for the current workspace, including users with access, assigned contacts, etc...',
 	'chelp emails widget' => 'This widget displays the latest emails classified in the current workspace.',
 	'chelp messages widget' => 'This widget shows the latest notes for the selected workspace.',
 	
 	'chelp active tasks panel' => 'All your active tasks are displayed below, regardless of the current workspace. You can pause, resume and stop them or mark them as completed.',
	'chelp general timeslots panel' => 'This panel displays timeslots directly assigned to a workspace. This is a quick way to assign and count time for users. You can also print time reports by clicking on the \'Print report\' link.',

	'chelp personal account' => 'This is your personal account.<br/>In this view you can update your profile and avatar, change your password and edit your personal preferences.',
	'chelp user account' => 'This is a user account.',
	'chelp user account admin' => 'Being an administrator, you may update the user\'s profile and avatar, change his or her password, and edit his or her preferences through this view.',

 	'remove context help' => 'Remove this help message',
	
); // array

?>