<?php	 		 	

  return array(
  
    // ---------------------------------------------------
    //  Administration tools
    // ---------------------------------------------------
    
    'administration tool name test_mail_settings' => 'Nastavení zkušebního e-mailu',
    'administration tool desc test_mail_settings' => 'Použijte tento jednoduchý nástroj k odeslání testovací zprávy k ověření zda je OpenGoo mailer správně nakonfigurován',
    'administration tool name mass_mailer' => 'Hromadná zpráva',
    'administration tool desc mass_mailer' => 'Tento jednoduchý nástroj vám umožní odesílat e-mailové zprávy v prostém textu jakékoliv skupině uživatelů registrovaných v systému',
  
    // ---------------------------------------------------
    //  Configuration categories and options
    // ---------------------------------------------------
  
    'configuration' => 'Nastavení',
    
    'mail transport mail()' => 'Výchozí nastavení PHP',
    'mail transport smtp' => 'SMTP server',
    
    'secure smtp connection no'  => 'Ne',
    'secure smtp connection ssl' => 'Ano, použít SSL',
    'secure smtp connection tls' => 'Ano, použít TLS',
    
    'file storage file system' => 'Souborový systém',
    'file storage mysql' => 'Databáze (MySQL)',
    
    // Categories
    'config category name general' => 'Hlavní',
    'config category desc general' => 'Hlavní nastavení OpenGoo',
    'config category name mailing' => 'Pošta',
    'config category desc mailing' => 'Pomocí tohoto souboru nastavení zadejte, jak by měla aplikace OpenGoo odesílat poštu. Můžete použít konfigurační možnosti poskytované v php.ini nebo aplikaci nastavit tak, aby využívala jakýkoliv jiný SMTP server',
  	'config category name modules' => 'Moduly',
    'config category desc modules' => 'Use these settings to enable or disable OpenGoo modules. Disabling a module only hides it from the graphical interface. It doesn\'t remove permissions from users to create or edit content objects.',
	'config category name passwords' => 'Hesla',
    'config category desc passwords' => 'Pomocí tohoto nastavení spravujete hesla.',
	
    
    // ---------------------------------------------------
    //  Options
    // ---------------------------------------------------
    
    // General
    'config option name site_name' => 'Název stránky',
    'config option desc site_name' => 'Tato hodnota bude zobrazena jako název stránky na nástěnce',
    'config option name file_storage_adapter' => 'Úložiště souborů',
    'config option desc file_storage_adapter' => 'Vyberte kde chcete ukládat nahrané dokumenty. <strong>Změna nastavení úložiště učiní všechny dříve nahrané soubory nedostupné </strong>.',
    'config option name default_project_folders' => 'Výchozí adrsáře',
    'config option desc default_project_folders' => 'Adresáře, které budou vytvořeny současně s vytvořením pracovní plochy projektu. Každý název souboru musí být na novém řádku. Zdvojená zadání budou ignorována',
    'config option name theme' => 'Téma',
    'config option desc theme' => 'Použitím témat můžete změnit výchozí vzhled OpenGoo',
  	'config option name days_on_trash' => 'Dny v koši',
    'config option desc days_on_trash' => 'Kolik dnů je objektový obsah zachován v koši před tím, než je automaticky vymazán. Pokud je zvolena 0, objekty nemohou být smazány z koše. Varování: Modul pro poštu je v beta stádiu a může obsahovat chyby pro které se zatím nedoporučuje ho používat v produkčním prostřední.',
	'config option name checkout_notification_dialog' => 'Checkout notification dialog for documents',
    'config option desc checkout_notification_dialog' => 'If enabled, the user will be prompted when downloading a file to select between editing or read only',
  	'config option name time_format_use_24' => 'Use 24 hour format',
  	'config option desc time_format_use_24' => 'If set, the 24 hour format will be used instead of the 12 hour format',
  	'config option name file_revision_comments_required' => 'File revision comments required',
    'config option desc file_revision_comments_required' => 'If set, adding new file revisions requires users to provide a new comment for each revision.',

  	'config option name enable_notes_module' => 'Enable Notes Module',
  	'config option name enable_email_module' => 'Enable Email Module',
  	'config option name enable_contacts_module' => 'Enable Contacts Module',
  	'config option name enable_calendar_module' => 'Enable Calendar Module',
  	'config option name enable_documents_module' => 'Enable Documents Module',
  	'config option name enable_tasks_module' => 'Enable Tasks Module',
  	'config option name enable_weblinks_module' => 'Enable Web Links Module',
  	'config option name enable_time_module' => 'Enable Time Module',
  	'config option name enable_reporting_module' => 'Enable Reporting Module',
  
    'config option name upgrade_check_enabled' => 'Enable upgrade check',
    'config option desc upgrade_check_enabled' => 'If Yes system will once a day check if there are new versions of OpenGoo available for download',
	'config option name work_day_start_time' => 'Work day start time',
  	'config option desc work_day_start_time' => 'Specifies the time when work day starts',
  
  	'config option name use_minified_resources' => 'Use minified resources',
  	'config option desc use_minified_resources' => 'Uses compressed Javascript and CSS to improve performance. You need to recompress JS and CSS if you modify them, by using the public/tools.',
    
  	'config option name currency_code' => 'Currency',
  	'config option desc currency_code' => 'Currency symbol',
    
    // Mailing
    'config option name exchange_compatible' => 'Microsoft Exchange compatibility mode',
    'config option desc exchange_compatible' => 'If you are using Microsoft Exchange Server set this option to yes to avoid some known mailing problems.',
    'config option name mail_transport' => 'Mail transport',
    'config option desc mail_transport' => 'You can use default PHP settings for sending emails or specify SMTP server',
    'config option name smtp_server' => 'SMTP server',
    'config option name smtp_port' => 'SMTP port',
    'config option name smtp_authenticate' => 'Use SMTP authentication',
    'config option name smtp_username' => 'SMTP username',
    'config option name smtp_password' => 'SMTP password',
    'config option name smtp_secure_connection' => 'Use secure SMTP connection',
  
  	// Passwords
  	'config option name min_password_length' => 'Minimum password length',
  	'config option desc min_password_length' => 'Minimum number of characters required for the password',
  	'config option name password_numbers' => 'Password numbers',
  	'config option desc password_numbers' => 'Amount of numerical characters required for the password',
  	'config option name password_uppercase_characters' => 'Password uppercase characters',
  	'config option desc password_uppercase_characters' => 'Amount of uppercase characters required for the password',
  	'config option name password_metacharacters' => 'Password metacharacters',
  	'config option desc password_metacharacters' => 'Amount of metacharacters required for the password (e.g.: ., $, *)',
  	'config option name password_expiration' => 'Password expiration (days)',
  	'config option desc password_expiration' => 'Number of days in which a new password is valid (0 to disable this option)',
  	'config option name password_expiration_notification' => 'Password expiration notification (days before)',
  	'config option desc password_expiration_notification' => 'Number of days to notifify user before password expiration (0 to disable this option)',
  	'config option name account_block' => 'Block account on password expiration',
  	'config option desc account_block' => 'Block user account when password expires (requires admin to enable user account again)',
	'config option name new_password_char_difference' => 'Validate new password character difference against history',
  	'config option desc new_password_char_difference' => 'Validate that a new password differs in at least 3 characters against the last 10 passwords used by the user',
    'config option name validate_password_history' => 'Validate password history',
  	'config option desc validate_password_history' => 'Validate that a new password doesn\'t match any of the last 10 passwords used by the user',
  	'config option name checkout_for_editing_online' => 'Automatically checkout when editing online',
  	'config option desc checkout_for_editing_online' => 'When a user edits a document online it will be checkout so that no one else can edit it at the same time',
  
 	'can edit company data' => 'Can edit company data',
  	'can manage security' => 'Can manage security',
  	'can manage workspaces' => 'Can manage workspaces',
  	'can manage configuration' => 'Can manage configuration',
  	'can manage contacts' => 'Can manage contacts',
  	'can manage reports' => 'Can manage reports',
  	'group users' => 'Group users',
    
  	
  	'user ws config category name dashboard' => 'Dashboard options',
  	'user ws config category name task panel' => 'Task options',
  	'user ws config category name general' => 'General',
	'user ws config category name calendar panel' => 'Calendar options',
  	'user ws config option name show pending tasks widget' => 'Show pending tasks widget',
  	'user ws config option name pending tasks widget assigned to filter' => 'Show tasks assigned to',
  	'user ws config option name show late tasks and milestones widget' => 'Show late tasks and milestones widget',
  	'user ws config option name show messages widget' => 'Show notes widget',
  	'user ws config option name show comments widget' => 'Show comments widget',
  	'user ws config option name show documents widget' => 'Show documents widget',
  	'user ws config option name show calendar widget' => 'Show mini calendar widget',
  	'user ws config option name show charts widget' => 'Show charts widget',
  	'user ws config option name show emails widget' => 'Show emails widget',
  	'user ws config option name show dashboard info widget' => 'Show dashboard info widget',
  	'user ws config option name show getting started widget' => 'Show getting started widget',
  	'user ws config option name localization' => 'Localization',
  	'user ws config option desc localization' => 'Labels and dates will be displayed according to this locale. Need to refresh for it to take effect.',
  	'user ws config option name initialWorkspace' => 'Initial workspace',
  	'user ws config option desc initialWorkspace' => 'This setting lets you choose which workspace you will be selected when you login, or you can choose to remember the last workspace you were viewing.',
  	'user ws config option name rememberGUIState' => 'Remember the user interface state',
  	'user ws config option desc rememberGUIState' => 'This allows you to save the state of the graphical interface (size of panels, expanded/collapsed state, etc) for the next time that you log in. Warning: This feature is in BETA status.',
  	'user ws config option name time_format_use_24' => 'Use 24 hours format for time descriptions',
  	'user ws config option desc time_format_use_24' => 'If enabled time descriptions will be shown as \'hh:mm\' from 00:00 to 23:59, if not hours will go from 1 to 12 using AM or PM.',
  	'user ws config option name work_day_start_time' => 'Work day start time',
	'user ws config option desc work_day_start_time' => 'Specifies the time when work day starts',
  	'user ws config option name show dashboard info widget' => 'Show workspace description widget',
  	
  	'user ws config option name my tasks is default view' => 'Tasks assigned to me is the default view',
  	'user ws config option desc my tasks is default view' => 'If no is selected, the default view of the task panel will show all tasks',
  	'user ws config option name show tasks in progress widget' => 'Show \'Tasks in progress\' widget',
  	'user ws config option name can notify from quick add' => 'Task notification is checked by default',
  	'user ws config option desc can notify from quick add' => 'The notification checkbox enables the option to notify assigned users after a task is added or updated',
	'user ws config option name show_tasks_context_help' => 'Show context help for tasks',
  	'user ws config option desc show_tasks_context_help' => 'If enabled, a context help box will be displayed on the tasks panel',
 	'user ws config option name start_monday' => 'Start week on monday',
	'user ws config option desc start_monday' => 'Will show the calendar starting weeks on monday',
  
  	'user ws config option name date_format' => 'Date format',
  	'user ws config option desc date_format' => 'Template format to be applied to date values.',
  	'user ws config option name descriptive_date_format' => 'Descriptive date format',
  	'user ws config option desc descriptive_date_format' => 'Template format to be applied to descriptive date values.',
     
  	'backup process desc' => 'A backup saves the current state of the whole application into a compressed folder. It can be used to easily backup an OpenGoo installation. <br> Generating a backup of the database and filesystem can last more than a couple of seconds, so making a backup is a process consisting on three steps: <br>1.- Launch a backup process, <br>2.- Download the backup. <br> 3.- Optionally, a backup can be manually deleted so that it is not available in the future. <br> ',
    'backup config warning' => 'Warning: Your config and tmp folders won\'t be backed up.',
  	'start backup' => 'Launch backup process',
    'start backup desc' => 'Launching a backup process implies deleting previous backups, and generating a new one.',
  	'download backup' => 'Download backup',
    'download backup desc' => 'To be able to download a backup you must first generate a backup.',
  	'delete backup' => 'Delete backup',
    'delete backup desc' => 'Deletes the last backup so that it is not available for download. Deleting backups after download is highly recommended.',
    'backup' => 'Backup',
    'backup menu' => 'Backup Menu',
   	'last backup' => 'Last backup was created on',
   	'no backups' => 'There are no backups to download',
   	
   	'user ws config option name always show unread mail in dashboard' => 'Always show unread email in dashboard',
   	'user ws config option desc always show unread mail in dashboard' => 'When NO is chosen emails from the active workspace will be shown',
   	'workspace emails' => 'Workspace Mails',
  	'user ws config option name tasksShowWorkspaces' => 'Show workspaces',
  	'user ws config option name tasksShowTime' => 'Show time',
  	'user ws config option name tasksShowDates' => 'Show dates',
  	'user ws config option name tasksShowTags' => 'Show tags',
  	'user ws config option name tasksGroupBy' => 'Group by',
  	'user ws config option name tasksOrderBy' => 'Order by',
  	'user ws config option name task panel status' => 'Status',
  	'user ws config option name task panel filter' => 'Filter by',
  	'user ws config option name task panel filter value' => 'Filter value',
  
  	'templates' => 'Templates',
	'add template' => 'Add template',
	'confirm delete template' => 'Are you sure you want to delete this template?',
	'no templates' => 'There are no templates',
	'template name required' => 'The template\'s name is required',
	'can manage templates' => 'Can manage templates',
	'new template' => 'New template',
	'edit template' => 'Edit template',
	'template dnx' => 'The template does not exist',
	'success edit template' => 'Template modified successfully',
	'log add cotemplates' => '{0} added',
	'log edit cotemplates' => '{0} modified',
	'success delete template' => 'Template deleted successfully',
	'error delete template' => 'Error deleting the template',
	'objects' => 'Objects',
	'objects in template' => 'Objects in template',
	'no objects in template' => 'There are no objects in this template',
	'add to a template' => 'Add to a template',
  	'add an object to template' => 'Add an object to this template',
	'you are adding object to template' => 'You are adding {0} \'{1}\' to a template. Choose a template below or create a new one for this {0}.',
	'success add object to template' => 'Object added to template successfully',
	'object type not supported' => 'This object type is not supported for templates',
  	'assign template to workspace' => 'Assign template to workspace',
  
  	'cron events' => 'Cron events',
  	'about cron events' => 'Learn about cron events...',
  	'cron events info' => 'Cron events let you execute tasks in OpenGoo periodically, without having to login to the system. To enable cron events you need to configure a cron job to periodically execute the "cron.php" file, located at the root of OpenGoo. The periodicity at which you run the cron job will determine the granularity at which you can run these cron events. For example, if you configure a cron job to run every five minutes, and you configure the cron event to check for upgrades every one minute, it will only be able to check for upgrades every five minutes. To learn about how to configure a cron job ask your system administrator or hosting provider.',
  	'cron event name check_mail' => 'Check mail',
  	'cron event desc check_mail' => 'This cron event will check for new email in all email accounts in the system.',
  	'cron event name purge_trash' => 'Purge trash',
  	'cron event desc purge_trash' => 'This cron event will delete objects older than the amount of days specified in the \'Days in trash\' configuration.',
  	'cron event name send_reminders' => 'Send reminders',
  	'cron event desc send_reminders' => 'This cron event will send email reminders.',
  	'cron event name check_upgrade' => 'Check upgrade',
  	'cron event desc check_upgrade' => 'This cron event will check for new versions of OpenGoo.',
  	'cron event name create_backup' => 'Create backup',
  	'cron event desc create_backup' => 'Creates a backup that you can download from the Backup section of Administration.',
  	'cron event name send_notifications_through_cron' => 'Send notifications through cron',
  	'cron event desc send_notifications_through_cron' => 'If this event is enabled email notifications will be sent through cron and not when generated by OpenGoo.',
  	'cron event name backup' => 'Backup OpenGoo',
  	'cron event desc backup' => 'If this event is enabled OpenGoo will be backed up periodically. The installation owner will be able to download backups through the Administration panel. OpenGoo backups are kept as a zip file in the \'tmp/backup\' directory',
  	'next execution' => 'Next execution',
  	'delay between executions' => 'Delay between executions',
  	'enabled' => 'Enabled',
  	'no cron events to display' => 'There are no cron events to display',
  	'success update cron events' => 'Cron events updated successfully',
  
  	'manual upgrade' => 'Manual upgrade',
  	'manual upgrade desc' => 'To manually upgrade OpenGoo you have to download the new version of OpenGoo, extract it to the root of your installation and then go to <a href="public/upgrade">\'public/upgrade\'</a> in your browser to run the upgrade process.',
  	'automatic upgrade' => 'Automatic upgrade',
  	'automatic upgrade desc' => 'The automatic upgrade will automatically download and extract the new version, and will run the upgrade process for you. The webserver needs write access to all folders.',
  	'start automatic upgrade' => 'Start automatic upgrade',
  
  	'select object type' => 'Select object type',
  	'select one' => 'Select one',
  	'email type' => 'Email',
  	'custom properties updated' => 'Custom properties updated'
  ); // array

?>