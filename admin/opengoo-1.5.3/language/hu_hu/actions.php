<?php	 		 	
	/*
	* Első magyar fordítás 1.3 verzióig: Váczy Attila <vaczy.a.m@gmail.com>
	* Magyar fordítás 1.4.2 verzióig (az 1.3 bázis részbeni újrafordításával, módosításával): Lukács Péter <programozo@lukacspeter.hu>
	*/
	
  // Actions
  return array(
  
    // Registration
    'register' => 'Regisztráció',
    'login' => 'Belépés',
    'logout' => 'Kilépés',
    'hide welcome info' => '&raquo; Rejtse el ezt az információt',
    
    // Companies
    'add company' => 'Cég létrehozása',
    'edit company' => 'Cég szerkesztése',
    'delete company' => 'Cég törlése',
    'edit company logo' => 'Logó frissítés',
    'delete company logo' => 'Logó törlés',
    
    // Clients
    'add client' => 'Ügyfél hozzáadás',
    'edit client' => 'Ügyfél szerkesztés',
    'delete client' => 'Ügyfél törlés',
    
    // Users
    'add user' => 'Felhasználó hozzáadás',
    'edit user' => 'Felhasználó szerkesztés',
    'delete user' => 'Felhsználó törlés',
    'create user from contact' => 'Ügyfélből felhasználó',
    
    // Group
    'add group' => 'Csoport hozzáadása',
    'edit group' => 'Csoport szerkesztése',
    'delete group' => 'Csoport törlése',
    
    // Project
    'add project' => 'Projekt hozzáadása',
    'edit project' => 'Projekt,szerkesztése',
    'delete project' => 'Projekt törlése',
    'mark project as finished' => 'Projekt megjelölése befejezettnek',
    'mark project as active' => 'Projekt megjelölése aktívnak',
    
    // Workspace
    'add workspace' => 'Munkahely hozzáadása',
    'edit workspace' => 'Munkahely szerkesztése',
    'delete workspace' => 'Munkahely törlése',
    'mark workspace as finished' => 'Munkahely megjelölése 
befejezettnek',
    'mark workspace as active' => 'Munkahely megjelölése aktívnak',
    
    // Messages
    'add message' => 'Üzenet hozzáadása',
    'add new message' => 'Új üzenet',
    'edit message' => 'Üzenet szerkesztése',
    'delete message' => 'Üzenet törlése',
    'view message' => 'Üzenet megtekintése',
    'update message options' => 'Üzenet tulajdonságok',
    'subscribe to object' => 'Feliratkozás',
    'unsubscribe from object' => 'Feliratkozás törlése',
    
    // Comments
    'add comment' => 'Megjegyzés hozzáfűzése',
    'edit comment' => 'Megjegyzés szerkesztése',
    
    // Task list
    'add task list' => 'Feladat hozzáadása',
    'edit task list' => 'Feladat szerkesztése',
    'delete task list' => 'Feladat törlése',
    'reorder tasks' => 'Feladatok új sorrendbe',
	'reorder sub tasks' => 'Alfeladatok új sorrendbe',
  	'copy task' => 'Másolat készítése e feladatról',
  	'copy milestone' => 'Másolat készítése erről a mérföldkőről',
    
    // Task
    'add task' => 'Feladat hozzáadása',
	'add sub task' => 'Alfeladat hozzáadása',
    'edit task' => 'Feladat szerkesztése',
    'delete task' => 'Feladat törlése',
    'mark task as completed' => 'Feladat készre jelölése',
    'mark task as open' => 'Feladat újbóli megnyitása',
    
    // Milestone
    'add milestone' => 'Mérföldkő hozzáadása',
    'edit milestone' => 'Mérföldkő szerkesztése',
    'delete milestone' => 'Mérföldkő törlése',
    
    // Events
    'add event' => 'Esemény hozzáadása',
    'edit event' => 'Esemény szerkesztése',
    'delete event' => 'Esemény törlése',
    'previous month' => 'Előző hónap',
    'next month' => 'Következő hónap',
    'previous week' => 'Előző hét',
    'next week' => 'Következő hét',
    'previous day' => 'Tegnap',
    'next day' => 'Holnap',
    'back to calendar' => 'Vissza a naptárhoz',
    'back to day' => 'Vissza a naphoz',
    'pick a date' => 'Dátum választása',
    'month' => 'Hónap',
    'week' => 'Hét',
    
    //Groups
    'can edit company data' => 'Szerkesztheti a cég adatokat',
    'can manage security' => 'Megadhat biztonsági beállításokat',
    'can manage workspaces' => 'Szerkesztheti a munkafelületeket/projeteket',
    'can manage configuration' => 'Szerkesztheti a konfigurációt',
    'can manage contacts' => 'Szerkesztheti az ügyfeleket',
    'group users' => 'Csoport felhasználók',
    
    
    // People
    'update people' => 'Frissít',
    'remove user from project' => 'Törlés a projektből',
    'remove company from project' => 'Törlés a projektből',
    
    // Password
    'update profile' => 'Profil frissítés',
    'change password' => 'Jelszó változtatás',
    'update avatar' => 'Fénykép frissítés',
    'delete current avatar' => 'Jelenlegi fénykép törlése',
    
    // Forms
    'add form' => 'Űrlap létrehozása',
    'edit form' => 'Űrlap szerkesztése',
    'delete form' => 'Űrlap törlése',
    'submit project form' => 'Mentés',
    
    // Files
    'add file' => 'Fájl hozzáadása',
    'edit file properties' => 'Fájl tulajdonságok szerkesztése',
    'upload file' => 'Fájl feltöltése',
    'create new revision' => 'Új verzió készítése',

    'add document' => 'Dokumentum hozzáadása',
    'save document' => 'Dokumentum mentése',
    'add spreadsheet' => 'Táblázat hozzáadása',
    'add presentation' => 'Bemutató hozzáadása',
    'document' => 'Dokumentum',
    'spreadsheet' => 'Táblázat',
    'presentation' => 'Bemutató',

    'new' => 'Új',
    'upload' => 'Feltöltés',
    'hide' => 'Elrejtés',
    'new document' => 'Új dokumentum',
    'new spreadsheet' => 'Új táblázat',
    'new presentation' => 'Új bemutató',

    'slideshow' => 'Bemutató',
    'revisions and comments' =>'Verziók, megjegyzések',
        
    'Save' => 'Mentés',
    'all elements' => 'Minden elemet',
    'collapse all' => 'Összes bezárása',
    'expand all' => 'Összes kinyitása',

    'properties' => 'Tulajdonságok',
    'edit file' => 'Fájl szerkesztése',
    'edit document' => 'Dokumentum szerkesztése',
    'edit spreadsheet' => 'Táblázat szerkesztése',
    'edit presentation' => 'Edit presentation',

  	'play' => 'Lejátszás',
  	'queue' => 'Várólista',
  
    'delete file' => 'Fájl törlése',
    
    'add folder' => 'Mappa hozzáadása',
    'edit folder' => 'Mappa szerkesztése',
    'delete folder' => 'Mappa törlése',
    
    'edit file revisions' => 'Verzió szerkesztése',
    'version' => 'ver',
    'last modification' => 'Utolsó módosítás',
    
    'link object' => 'Elem csatolása',
    'link objects' => 'Elemek csatolása',
    'link more objects' => 'Több elem csatolása',
    'unlink' => 'Leválasztás',
    'unlink object' => 'Elem leválasztása',
    'unlink objects' => 'Elemek leválasztása',
	'extract' => 'Kicsomagolás',
  	'add files to zip' => 'Fájl hozzáadása zip-hez',
  	
    // Tags
    'delete tag'  => 'Címke törlése',
    
    // Permissions
    'update permissions' => 'Engedélyek frissítése',
    'edit permissions' => 'Engedélyek szerkesztése',
    'edit permissions explanation'  => 'Jelölje meg, hogy miket engedélyez!',
  
  	'save as new revision' => 'Mentés új verzióként',
	'save as' => 'Átnevezés',
	'details' => 'Részletek',
	'view history for' => 'Előzmények megtekintése',
	'view history' => 'Előzmények megtekintése',    
	'edit preferences' => 'Beállítások szerkesztése',
	'view milestone' => 'Mérföldkövek megtekintése',
  	'custom properties' => 'Egyedi beállítások',
  	'move to trash' => 'Kukába dobás',
  	'restore from trash' => 'Visszaállítás kukából',
  	'delete permanently' => 'Végleges törlés',
  	'copy file' => 'Másolat létrehozása a fájlról',
  	'open weblink' => 'Web link megnyitása',
  ); // array

?>
