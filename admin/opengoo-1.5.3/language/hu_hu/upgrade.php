<?php	 		 	
	
	/*
	* Első magyar fordítás 1.4.2 verzióig: Lukács Péter <programozo@lukacspeter.hu>
	*/
	
	return array(
		'upgrade' => 'Szoftverfrissítés',
		'upgrade from' => 'Frissítés miről',
		'upgrade to' => 'Frissítés mire',
		'already upgraded' => 'Ön már rendelkezik a legfrissebb verzióval.',
		'back to opengoo' => 'Vissza az OpenGoo-ba',
		'all rights reserved' => 'Minden jog fenntartva',
		'upgrade process log' => 'Frissítési folyamat napló',
		'upgrade opengoo' => 'Az OpenGoo frissítése',
		'upgrade your opengoo installation' => 'Az Ön telepített OpenGoo verziójának frissítése'
	);
?>