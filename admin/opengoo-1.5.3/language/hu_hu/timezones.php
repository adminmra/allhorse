<?php	 		 	
	/*
	* Els� magyar ford�t�s 1.4.2 verzi�ig: Luk�cs P�ter <programozo@lukacspeter.hu>
	*/
	
  // This array has been taken from the XOOPS project
  return array(
    'timezone gmt -12'  => '(GMT-12:00) Eniwetok, Kwajalein',
    'timezone gmt -11'  => '(GMT-11:00) Midway-szigetek, Szamoa',
    'timezone gmt -10'  => '(GMT-10:00) Hawaii',
    'timezone gmt -9'   => '(GMT-9:00) Alaszka',
    'timezone gmt -8'   => '(GMT-8:00) Csendes-�ce�ni id� (Egyes�lt �llamok �s Kanada)',
    'timezone gmt -7'   => '(GMT-7:00) Hegyi id� (Egyes�lt �llamok �s Kanada)',
    'timezone gmt -6'   => '(GMT-6:00) K�z�p-amerikai id� (Egyes�lt �llamok �s Kanada), Mexik� V�ros',
    'timezone gmt -5'   => '(GMT-5:00) Keleti id� (Egyes�lt �llamok �s Kanada), Bogota, Lima, Quito',
    'timezone gmt -4.5' => '(GMT-4:30) Venezuela (VET)',
    'timezone gmt -4'   => '(GMT-4:00) Atlanti-�ce�ni id� (Kanada), Caracas, La Paz',
    'timezone gmt -3.5' => '(GMT-3:30) �j-foundland',
    'timezone gmt -3'   => '(GMT-3:00) Braz�lia, Buenos Aires, Georgetown, Gr�nland',
    'timezone gmt -2'   => '(GMT-2:00) K�z�p-atlanti',
    'timezone gmt -1'   => '(GMT-1:00) Azori-szigetek, Z�ld-foki-szigetek',
    'timezone gmt 0'    => '(GMT) Greenwichi k�z�pid�, London, Dublin, Lisszabon, Casablanca, Monrovia',
    'timezone gmt +1'   => '(GMT+1:00) Amszterdam, Berlin, R�ma, Koppenh�ga, Br�sszel, Madrid, P�rizs, B�cs, Budapest',
    'timezone gmt +2'   => '(GMT+2:00) Ath�n, Isztanbul, Minszk, Helsinki, Jeruzs�lem, D�l-Afrika',
    'timezone gmt +3'   => '(GMT+3:00) Bagdad, Kuvait, Rij�d, Moszkva, Szentp�terv�r',
    'timezone gmt +3.5' => '(GMT+3:30) Teher�n',
    'timezone gmt +4'   => '(GMT+4:00) Abu-Dzabi, Maszkat, Baku, Jerev�n',
    'timezone gmt +4.5' => '(GMT+4:30) Kabul',
    'timezone gmt +5'   => '(GMT+5:00) Jekatyerinburg, Iszlamab�d, Karacsi, Taskent',
    'timezone gmt +5.5' => '(GMT+5:30) Bombay, Kalkutta, Madras, �j-Delhi',
    'timezone gmt +6'   => '(GMT+6:00) Almaty, Dhaka, Novoszibirszk',
    'timezone gmt +7'   => '(GMT+7:00) Bangkok, Hanoi, Dzsakarta',
    'timezone gmt +8'   => '(GMT+8:00) Peking, Perth, Szingap�r, Hong Kong, Kuala Lumpur',
    'timezone gmt +9'   => '(GMT+9:00) Toki�, Sz�ul, Oszaka, Szapporo, Yakutsk',
    'timezone gmt +9.5' => '(GMT+9:30) Adelaide, Darwin',
    'timezone gmt +10'  => '(GMT+10:00) Brisbane, Canberra, Melbourne, Sydney, Guam, Vlagyivosztok',
    'timezone gmt +11'  => '(GMT+11:00) Magad�n, Salamon-szigetek, �j-Kaled�nia',
    'timezone gmt +12'  => '(GMT+12:00) Auckland, Wellington, Fidzsi-szigetek, Kamcsatka, Marshall-Szigetek',    
    'timezone gmt +13'  => '(GMT+13:00) Nuku\'alofa'
  ); // array

?>