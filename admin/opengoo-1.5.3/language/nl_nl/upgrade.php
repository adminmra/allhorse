<?php	 		 	 return array(
	'upgrade' => 'upgrade',
	'upgrade from' => 'upgrade van ',
	'upgrade to' => 'upgrade naar',
	'already upgraded' => 'U werkt reeds met de laatst mogelijke versie.',
	'back to opengoo' => 'terug naar OpenGoo',
	'all rights reserved' => 'Alle rechten voorbehouden',
	'upgrade process log' => 'upgrade proces log',
	'upgrade opengoo' => 'upgrade OpenGoo',
	'upgrade your opengoo installation' => 'Upgrade uw OpenGoo installatie.',
); ?>
