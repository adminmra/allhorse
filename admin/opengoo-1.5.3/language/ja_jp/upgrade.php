<?php	 		 	 return array(
	'upgrade' => 'アップグレード',
	'upgrade from' => 'アップグレード前',
	'upgrade to' => 'アップグレード後',
	'already upgraded' => '最新の可能なバージョンにアップグレードしました。',
	'back to opengoo' => 'OpenGooに戻る',
	'all rights reserved' => 'All rights reserved',
	'upgrade process log' => 'アップグレードの処理のログ',
	'upgrade opengoo' => 'OpenGooをアップグレード',
	'upgrade your opengoo installation' => 'インストールしてあるOpenGooをアップグレード',
); ?>
