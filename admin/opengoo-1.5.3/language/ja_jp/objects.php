<?php	 		 	 return array(
	'user' => 'ユーザ',
	'group' => 'グループ',
	'groups' => 'グループ',
	'users' => 'ユーザ',
	'member' => 'メンバー',
	'members' => 'メンバー',
	'company' => '会社',
	'companies' => '会社',
	'client' => '顧客',
	'clients' => '顧客',
	'package' => 'パッケージ',
	'project' => 'ワークスペース',
	'projects' => 'ワークスペース',
	'message' => 'ノート',
	'messages' => 'ノート',
	'comment' => 'コメント',
	'comments' => 'コメント',
	'milestone' => 'マイルストーン',
	'milestones' => 'マイルストーン',
	'task' => 'タスク',
	'subtasks' => 'サブタスク',
	'tasks' => 'タスク',
	'task list' => 'タスク',
	'task lists' => 'タスク',
	'tag' => 'タグ',
	'tags' => 'タグ',
	'attachment' => '添付ファイル',
	'attachments' => '添付ファイル',
	'object subscribers' => '購読者',
	'form' => 'フォーム',
	'forms' => 'フォーム',
	'file' => 'ファイル',
	'files' => 'ファイル',
	'file revision' => 'ファイルのリビジョン',
	'file revisions' => 'ファイルのリビジョン',
	'file contents' => 'ファイルの内容',
	'revision' => 'リビジョン',
	'revisions' => 'リビジョン',
	'folder' => 'フォルダ',
	'folders' => 'フォルダ',
	'configuration category' => '環境設定のカテゴリ',
	'configuration categories' => '環境設定のカテゴリ',
	'handin' => 'ハンドイン',
	'handins' => 'ハンドイン',
	'owner company' => 'オーナ会社',
	'client companies' => '顧客企業',
	'open milestone' => '未完のマイルストーン',
	'open milestones' => '未完のマイルストーン',
	'completed milestone' => '完了したマイルストーン',
	'completed milestones' => '完了したマイルストーン',
	'late milestones' => '遅れているマイルストーン',
	'late milestones and tasks' => '遅れているマイルストーンとタスク',
	'today milestones' => '今日のマイルストーン',
	'upcoming milestones' => '次回のマイルストーン',
	'completed task list' => '完了したタスク',
	'completed task lists' => '完了したタスク',
	'open task list' => '未完のタスク',
	'open task lists' => '未完のタスク',
	'active project' => '活動状態にあるワークスペース',
	'active projects' => '活動状態にあるワークスペース',
	'finished project' => '終了状態のワークスペース',
	'finished projects' => '終了状態のワークスペース',
	'linked object' => 'リンクされたオブジェクト',
	'linked objects' => 'リンクされたオブジェクト',
	'important message' => '重要なノート',
	'important messages' => '重要なノート',
	'important file' => '重要なファイル',
	'important files' => '重要なファイル',
	'parent task' => '親のタスク',
	'private message' => '親のノート',
	'private milestone' => 'プライベートなマイルストーン',
	'private task list' => 'プライベートなタスク',
	'private comment' => 'プライベートなコメント',
	'private file' => 'プライベートなファイル',
	'emailunclassified' => 'メール',
	'load file' => 'ファイルを読み込み',
	'time' => '時刻',
	'reporting' => 'レポート',
	'object reminders' => 'リマインダー',
	'add object reminder' => 'リマインダーを追加',
	'create document' => 'ドキュメントを作成',
	'create presentation' => 'プレゼンテーションを作成',
	'modify object subscribers' => '購読者を変更',
	'subscription modified successfully' => '購読者の変更に成功しました。',
); ?>
