<?php	 		 	
	return array(
		'upgrade' => 'Upgrade',
		'upgrade from' => 'Upgrade from',
		'upgrade to' => 'Upgrade to',
		'already upgraded' => 'You already have upgraded to the latest possible version.',
		'back to opengoo' => 'Back to OpenGoo',
		'all rights reserved' => 'All rights reserved',
		'upgrade process log' => 'Upgrade process log',
		'upgrade opengoo' => 'Upgrade OpenGoo',
		'upgrade your opengoo installation' => 'Upgrade your OpenGoo installation',
	
		'error upgrade version must be specified' => 'No version specified. Cannot continue with automatic upgrade. Please, try again later or try the manual upgrade instead.',
		'error upgrade version not found' => 'Invalid version specified ({0}). Cannot continue with automatic upgrade. Please, try again later or try the manual upgrade instead.',
		'error upgrade invalid zip url' => 'Invalid download URL for specified version. Cannot continue with automatic upgrade. Please, try again later or try the manual upgrade instead.',
		'error upgrade cannot open zip file' => 'Cannot open new version\'s zip file. Cannot continue with automatic upgrade. Please, try again later or try the manual upgrade instead.',
	);
?>