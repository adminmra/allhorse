<?php	 		 	

  // Return array of langs
  return array(
    'username' => 'Username',
    'password' => 'Password',
    'password again' => 'Repeat password',
    'old password' => 'Old password',
  	'new password' => 'New password',
    'remember me' => 'Remember me for 14 days',
    'email address' => 'Email address',
    'display name' => 'Display name',
    'overview' => 'Overview',
    'search' => 'Search',
    'search results' => 'Search results',
    'account' => 'Account',
    'settings' => 'Settings',
    'index' => 'Index',
    'view' => 'View',
    'edit' => 'Edit',
    'delete' => 'Delete',
  	'empty trash can' => 'Empty trash can',
    'save' => 'Save',
  	'add' => 'Add',
    'update' => 'Update',
    'submit' => 'Submit',
    'reset' => 'Reset',
  	'change' => 'Change',
    'name' => 'Name',
    'title' => 'Title',
    'description' => 'Description',
    'text' => 'Text',
    'additional text' => 'Additional text',
    'due date' => 'Due date',
    'use due date' => 'Use due date',
    'use start date' => 'Use start date',
    'assign to' => 'Assign to',
    'late' => 'Late',
    'upcoming' => 'Upcoming',
    'today' => 'Today',
    'yesterday' => 'Yesterday',
    'tomorrow' => 'Tomorrow',
    'n/a' => 'n/a',
    'anyone' => 'Anyone',
    'nobody' => 'Nobody',
    'none' => '-- None --',
    'please select' => '-- Please select --',
    'reorder' => 'Reorder',
    'cancel' => 'Cancel',
    'size' => 'Size',
    'type' => 'Type',
    'status' => 'Status',
    'options' => 'Options',
    'active' => 'Active',
    'completed' => 'Completed',
    'administrator' => 'Administrator',
    'error' => 'Error',
    'yes' => 'Yes',
    'no' => 'No',
    'all' => 'All',
    'or' => 'Or',
    'by' => 'By',
    'on' => 'On',
    'in' => 'In',
    'people' => 'People',
    'dates' => 'Dates',
    'permission' => 'Permission',
    'permissions' => 'Permissions',
    'reset' => 'Reset',
    'owner' => 'Owner',
    'instant messengers' => 'Instant Messengers',
    'value' => 'Value',
    'phone number' => 'Phone number',
    'phone numbers' => 'Phone numbers',
    'office phone number' => 'Office',
    'fax number' => 'Fax',
    'mobile phone number' => 'Mobile',
    'home phone number' => 'Home',
    'settings' => 'Settings',
    'homepage' => 'Homepage',
    'address' => 'Address',
    'address2' => 'Address 2',
    'city' => 'City',
    'state' => 'State',
    'zipcode' => 'Zipcode',
    'country' => 'Country',
    'contact' => 'Contact',
    'pagination page' => 'Page:',
    'pagination first' => 'First page',
    'pagination previous' => 'Previous page',
    'pagination next' => 'Next page',
    'pagination last' => 'Last page',
    'pagination current page' => 'Page: {0}',
    'download' => 'Download',
    'downloads' => 'Downloads',
    'replace' => 'Replace',
    'expand' => 'Expand',
    'collapse' => 'Collapse',
    'author' => 'Author',
    'user title' => 'Title',
    'more' => 'More',
    'order by' => 'Order by',
    'filename' => 'File name',
    'permalink' => 'Permalink',
    'timezone' => 'Timezone',
    'upgrade' => 'Upgrade',
    'changelog' => 'Changelog',
    'hint' => 'Hint',
    'order' => 'Order',
    'read' => 'Read',
    'workspace' => 'Workspace',
    'event' => 'Event',
    'events' => 'Events',
    'Event' => 'Event',
    'Events' => 'Events',
    
    'project calendar' => '{0} calendar',
    'user calendar' => '{0}\'s calendar',
    
    'month 1' => 'January',
    'month 2' => 'February',
    'month 3' => 'March',
    'month 4' => 'April',
    'month 5' => 'May',
    'month 6' => 'June',
    'month 7' => 'July',
    'month 8' => 'August',
    'month 9' => 'September',
    'month 10' => 'October',
    'month 11' => 'November',
    'month 12' => 'December',
  
  	'monday' => 'Monday',
  	'tuesday' => 'Tuesday',
  	'wednesday' => 'Wednesday',
  	'thursday' => 'Thursday',
    'friday' => 'Friday',
  	'saturday' => 'Saturday',
  	'sunday' => 'Sunday',
  
  	'monday short' => 'Mon',
  	'tuesday short' => 'Tue',
  	'wednesday short' => 'Wed',
  	'thursday short' => 'Thu',
    'friday short' => 'Fri',
  	'saturday short' => 'Sat',
  	'sunday short' => 'Sun',
  
  	'copy of' => 'Copy of {0}',
  	'task templates' => 'Task Templates',
  	'add task template' => 'Add Task Template',
  	'template' => 'Template',
  	'workspaces' => 'Workspaces',
  	'assign to workspace' => 'Assign to Workspace',
  	'assign'=>'Assign',
  	'include subworkspaces'=>'Include Subworkspaces',
  	'assign task template to workspace' => 'Assign task template \'{0}\' to workspaces',
  	'confirm delete task template' => 'Are you sure you want to delete this task template permanently? It will not longer be available throughout the system.',
  	'no task templates' => 'No task templates',
  	'create contact from user' => 'Create contact with user data',
  	'confirm create contact from user' => 'Are you sure you want to create a contact with user data?',
  	'create contact from user desc' => 'If yes is selected, the user will be stored in the system as a contact',
  	'use previous personal workspace' =>'Use an existing workspace as user\'s personal workspace?',
  	'use previous personal workspace desc' =>'If yes is selected you can determine which workspace assing as personal for this user',
  	'select personal workspace' => 'Select a Workspace',
    'create personal workspace' => 'Create a new one',
    'use an existing workspace' => 'Use an existing Workspace',
  	'go to contact' => 'Go to contact data',
  	'show assigned to' => 'Show assigned to',
  	'timeslot on object' => 'Timeslot on {0}',
  	'show by status' => 'Show by status',
  	'show by priority' => 'Show by priority',
  	'task report' => 'Task Report',
  	'priority' => 'Priority',
  	'anybody' => 'Anybody',
  
  	'read and write' => 'Read &amp; Write',
  	'read only' => 'Read only',
  	'none no bars' => 'None',
  	'apply to all subworkspaces' => 'Apply these permissions to all subworkspaces',
  	'weblinks' => 'Web links',
	'add work' => 'Add work',
  
  	'date format' => 'm/d/Y',
  	'date format description' => '(mm/dd/yyyy)',
  	'confirm move to trash' => 'Are you sure you want to move the selected object to the trash?',
  
  	'remove' => 'Remove',
  	'mime type' => 'MIME type',
  	
  	'default' => 'Default',
  	'language' => 'Language',
  	'before' => 'before',
  	'pages' => 'pages',
  
	'repeat by' => 'Repeat by',
	'repeating task' => 'Repeating task',
  	'this task repeats' => 'This task repeats',
  	'forever' => 'forever',
	'n times' => '{0} times',
	'until x' => 'until {0}',
	'n days' => '{0} days',
	'n months' => '{0} months',
	'n years' => '{0} years',
  	'every' => 'every',
	'generate repetitition' => 'Generate task repetition',
	'end of task' => 'End of Task',
	'start of task' => 'Start of Task',
  	'new task repetition generated' => 'New task repetition successfully generated',

	'reminders will not apply to repeating events' => 'Warning: reminders will not apply to repeating events',
  ); // array

?>