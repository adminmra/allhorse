<?php	 		 	

  // Reporting
  return array(
  
  	'custom' => 'Custom',
    'custom reports' => 'Custom reports',
  	'no custom reports' => 'There are no custom reports',
  	'add custom report' => 'Add a custom report',
  	'edit custom report' => 'Edit custom report',
  	'new custom report' => 'New custom report',
  	'add report' => 'Add report',
  	'object type' => 'Object Type',
  	'add condition' => 'Add condition',
  	'custom report created' => 'Custom report created',
  	'custom report updated' => 'Custom report updated',
  	'conditions' => 'Conditions',
  	'columns and order' => 'Columns & Order',
  	'true' => 'True',
  	'false' => 'False',
  	'field' => 'Field',
  	'condition' => 'Condition',
  	'ends with' => 'ends with',
  	'select unselect all' => 'Select/Unselect all',
  	'ascending' => 'Ascending',
  	'descending' => 'Descending',
    'parametrizable'=> 'Paramenter?',
  );
  
?>