<?php	 		 	 return array(
	'upgrade' => 'Aktualisierung',
	'upgrade from' => 'Aktualisierung von',
	'upgrade to' => 'Aktualisierung auf',
	'back to opengoo' => 'Zurück zu OpenGoo',
	'upgrade opengoo' => 'Aktualisierung OpenGoo',
	'all rights reserved' => 'Alle Rechte vorbehalten',
	'upgrade process log' => 'Protokoll Aktualisierungsprozess ',
	'upgrade your opengoo installation' => 'Aktualisierung der OpenGoo Installation',
	'already upgraded' => 'OpenGoo bereits auf die neueste mögliche Version aktualisiert.',
); ?>
