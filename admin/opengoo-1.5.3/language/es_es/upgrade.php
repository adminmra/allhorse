<?php	 		 	 return array(
	'upgrade' => 'Actualizar',
	'upgrade from' => 'Actualizar de',
	'upgrade to' => 'Actualizar a',
	'already upgraded' => 'Ha actualizado a la última versión posible.',
	'back to opengoo' => 'Volver a OpenGoo',
	'all rights reserved' => 'Todos los derechos reservados',
	'upgrade process log' => 'Registro del proceso de actualización',
	'upgrade opengoo' => 'Actualizar OpenGoo',
	'upgrade your opengoo installation' => 'Actualice su instalación de OpenGoo',

	'error upgrade version must be specified' => 'No se ha especificado la versión. No se puede seguir con la actualizacón automática. Por favor, intente más tarde o pruebe realizar una actualización manual.',
	'error upgrade version not found' => 'Versión inválida ({0}). No se puede seguir con la actualizacón automática. Por favor, intente más tarde o pruebe realizar una actualización manual.',
	'error upgrade invalid zip url' => 'Dirección de descarga inválida. No se puede seguir con la actualizacón automática. Por favor, intente más tarde o pruebe realizar una actualización manual.',
	'error upgrade cannot open zip file' => 'No se puede abrir el archivo de la nueva versión. No se puede seguir con la actualizacón automática. Por favor, intente más tarde o pruebe realizar una actualización manual.',
); ?>
