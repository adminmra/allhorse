<?php	 		 	 return array(
	'upgrade' => 'Atualizar',
	'upgrade from' => 'Atualizar de',
	'upgrade to' => 'Atualizar para',
	'already upgraded' => 'Você atualizou para a última versão possível',
	'back to opengoo' => 'Retornar a OpenGoo',
	'all rights reserved' => 'Todos os direitos reservados',
	'upgrade process log' => 'Log do processo de atualização',
	'upgrade opengoo' => 'Atualizar OpenGoo',
	'upgrade your opengoo installation' => 'Atualizar sua instalação do OpenGoo',
); ?>
