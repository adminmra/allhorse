<?php	 		 	 return array(
	'CAL_QUERY_GETEVENT_ERROR' => 'Database Error: Failed fetching event by ID',
	'CAL_QUERY_SETEVENT_ERROR' => 'Database Error: Failed to Set Event Data',
	'CAL_SUBM_LOGOUT' => 'Log ud',
	'CAL_SUBM_LOGIN' => 'Log Ind',
	'CAL_SUBM_ADMINPAGE' => 'Admin Page',
	'CAL_SUBM_SEARCH' => 'Søg',
	'CAL_SUBM_BACK_CALENDAR' => 'Tilbage til kalander',
	'CAL_SUBM_VIEW_TODAY' => 'Dagens kampe',
	'CAL_SUBM_ADD' => 'Tilføj kamp idag',
	'CAL_MENU_BACK_CALENDAR' => 'Tilbage til kalander',
	'CAL_MENU_NEWEVENT' => 'Ny kamp',
	'CAL_MENU_BACK_EVENTS' => 'Tilbage til kampe',
	'CAL_MENU_GO' => 'Klik',
	'CAL_MENU_TODAY' => 'I dag',
	'CAL_NO_READ_PERMISSION' => 'Du har ikkke tilladelse til at se denne kamp',
	'CAL_NO_WRITE_PERMISSION' => 'Du har ikkke tilladelse til at lave eller redigere kampe.',
	'CAL_NO_EDITOTHERS_PERMISSION' => 'Du skal have tilladelse til at rette andre spilleres kampe',
	'CAL_NO_EDITPAST_PERMISSION' => 'Du kan ikke redigere tilbage i tiden.',
	'CAL_NO_ACCOUNTS' => 'This calendar does not allow accounts; only root can log on.',
	'CAL_NO_MODIFY' => 'Kan ikke ændre',
	'CAL_NO_ANYTHING' => 'Du har ikke lov til en pind på denne side',
	'0' => 'CAL_NO_WRITE',
	'1' => 'Du har ikke tilladelse til at oprette kampe',
	'CAL_MONDAY' => 'Mandag',
	'CAL_TUESDAY' => 'Tirsdag',
	'CAL_WEDNESDAY' => 'Onsdag',
	'CAL_THURSDAY' => 'Torsdag',
	'CAL_FRIDAY' => 'Fredag',
	'CAL_SATURDAY' => 'Lørdag',
	'CAL_SUNDAY' => 'Søndag',
	'CAL_SHORT_MONDAY' => 'M',
	'CAL_SHORT_TUESDAY' => 'T',
	'CAL_SHORT_WEDNESDAY' => 'O',
	'CAL_SHORT_THURSDAY' => 'T',
	'CAL_SHORT_FRIDAY' => 'F',
	'CAL_SHORT_SATURDAY' => 'L',
	'CAL_SHORT_SUNDAY' => 'S',
	'CAL_JANUARY' => 'Januar',
	'CAL_FEBRUARY' => 'Februar',
	'CAL_MARCH' => 'marts',
	'CAL_APRIL' => 'April',
	'CAL_MAY' => 'Maj',
	'CAL_JUNE' => 'Juni',
	'CAL_JULY' => 'Juli',
	'CAL_AUGUST' => 'August',
	'CAL_SEPTEMBER' => 'September',
	'CAL_OCTOBER' => 'Oktober',
	'CAL_NOVEMBER' => 'November',
	'CAL_DECEMBER' => 'December',
	'CAL_MORE_TIME_OPTIONS' => 'Flere muligheder',
	'CAL_REPEAT' => 'Gentag',
	'CAL_EVERY' => 'Hver',
	'CAL_REPEAT_FOREVER' => 'Gentag uendeligt',
	'CAL_REPEAT_UNTIL' => 'Gentag indtil den',
	'CAL_TIMES' => 'antal gange',
	'CAL_HOLIDAY_EXPLAIN' => 'Dette vil kampen til at gentage sig den',
	'CAL_DURING' => 'Under',
	'CAL_EVERY_YEAR' => 'Hvert år',
	'CAL_HOLIDAY_EXTRAOPTION' => 'Or, since this falls on the last week of the month, Check here to make the event fall on the LAST',
	'CAL_IN' => 'i',
	'CAL_PRIVATE_EVENT_EXPLAIN' => 'Dette er en privat kamp',
	'CAL_SUBMIT_ITEM' => 'Send',
	'CAL_MINUTES' => 'Minutter',
	'CAL_MINUTES_SHORT' => 'min',
	'CAL_TIME_AND_DURATION' => 'Dato, klokkeslæt og varighed',
	'CAL_REPEATING_EVENT' => 'Gentag kampe',
	'CAL_EXTRA_OPTIONS' => 'Ekstra muligheder',
	'CAL_ONLY_TODAY' => 'Kun denne dag',
	'CAL_DAILY_EVENT' => 'Gentag dagligt',
	'CAL_WEEKLY_EVENT' => 'Gentag ugenligt',
	'CAL_MONTHLY_EVENT' => 'Gentag hver måned',
	'CAL_YEARLY_EVENT' => 'Gentag årligt',
	'CAL_HOLIDAY_EVENT' => 'Gentag hver ferie',
	'CAL_UNKNOWN_TIME' => 'Ukendt start tidspunkt',
	'CAL_ADDING_TO' => 'Tilføj til',
	'CAL_ANON_ALIAS' => 'Alias Navn',
	'CAL_EVENT_TYPE' => 'Kamp type',
	'CAL_DESCRIPTION' => 'Beskrivelse',
	'CAL_DURATION' => 'Varighed',
	'CAL_DATE' => 'Dato',
	'CAL_NO_EVENTS_FOUND' => 'Ingen kampe fundet',
	'CAL_NO_SUBJECT' => 'Intet emne',
	'CAL_PRIVATE_EVENT' => 'Privat kamp',
	'CAL_DELETE' => 'Slet',
	'CAL_MODIFY' => 'Rediger',
	'CAL_NOT_SPECIFIED' => 'Ikke specificeret',
	'CAL_FULL_DAY' => 'Hele dagen',
	'CAL_HACKING_ATTEMPT' => 'Hacking Attempt - IP address logged',
	'CAL_TIME' => 'Tid',
	'CAL_HOURS' => 'Timer',
	'CAL_HOUR' => 'Time',
	'CAL_ANONYMOUS' => 'Anonym',
	'CAL_SELECT_TIME' => 'Vælg start tidspunkt',
	'event invitations' => 'Kamp invitationer',
	'event invitations desc' => 'Send kampkort ud til valgte spillere',
	'send new event notification' => 'Send email beskeder',
	'new event notification' => 'Ny kamp er blevet tilføjet kalenderen',
	'change event notification' => 'Kampen er blevet ændret',
	'deleted event notification' => 'En kamp er blevet slettet',
	'attendance' => 'Vil du spille med?',
	'confirm attendance' => 'Bekræft at du spiller med',
	'maybe' => 'Måske (sløve padde)',
	'decide later' => 'Beslut dig senere (Ikke for sent)',
	'view event' => 'Vis kamp',
	'new event created' => 'Ny kamp er oprettet',
	'event changed' => 'Kampen er ændret',
	'event deleted' => 'Kampen er slettet',
	'calendar of' => '{0}\'s kalender',
	'all users' => 'Alle spillere',
	'error delete event' => 'Fejl ved sletning',
	'days' => 'dage',
	'weeks' => 'uger',
	'months' => 'måneder',
	'years' => 'år',
	'invitations' => 'Kampkort',
	'pending response' => 'Afventer svar',
	'participate' => 'Vil gerne spille',
	'no invitations to this event' => 'Ingen kampkort er sendt til denne kamp',
	'duration must be at least 15 minutes' => 'Varighed skal være mindst 15 minutter',
	'event dnx' => 'Kampen du søger findes ikke',
	'view date title' => 'l, m/d/Y',
); ?>
