<?php	 		 	 return array(
	'custom' => 'Własne',
	'custom reports' => 'Własne raporty',
	'no custom reports' => 'Brak własnych raportów',
	'add custom report' => 'Dodaj własny raport',
	'edit custom report' => 'Edytuj własny raport',
	'new custom report' => 'Nowy własny raport',
	'add report' => 'Dodaj raport',
	'object type' => 'Typ obiektu',
	'add condition' => 'Dodaj warunek',
	'custom report created' => 'Własny raport utworzony',
	'custom report updated' => 'Własny raport zmodyfikowany',
	'conditions' => 'Warunki',
	'columns and order' => 'Kolumny i kolejność',
	'true' => 'Prawda',
	'false' => 'Fałsz',
	'field' => 'Pole',
	'condition' => 'Warunek',
	'ends with' => 'kończy się na',
	'select unselect all' => 'Zaznacz/Odznacz wszystko',
	'ascending' => 'Rosnąco',
	'descending' => 'Malejąco',
	'parametrizable' => 'Parametr?',
); ?>
