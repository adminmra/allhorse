<?php	 		 	 return array(
	'upgrade' => 'Aktualizacja',
	'upgrade from' => 'Aktualizacja z',
	'upgrade to' => 'Aktualizacja do',
	'already upgraded' => 'Zaktualizowałeś/aś oprogramowanie do najnowszej dostępnej wersji',
	'back to opengoo' => 'Powrót do OpenGoo',
	'all rights reserved' => 'Wszystkie Prawa Zastrzeżone',
	'upgrade process log' => 'Dziennik procesu aktualizacji',
	'upgrade opengoo' => 'Aktualizuj OpenGoo',
	'upgrade your opengoo installation' => 'Aktualizuj swoją instalację OpenGoo',
); ?>
