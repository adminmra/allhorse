<?php	 		 	
/* 
Translated into Russian for OpenGoo
Last update:  see readme_rus.txt

http://code.google.com/p/opengooru/
   
*/

	return array(
		'upgrade' => 'Обновление',
		'upgrade from' => 'Обновление',
		'upgrade to' => 'Обновить',
		'already upgraded' => 'У вас установлена последняя версия.',
		'back to opengoo' => 'Перейти к',
		'all rights reserved' => 'Все права защищены',
		'upgrade process log' => 'История процесса обновления',
		'upgrade opengoo' => 'Обновление ',
		'upgrade your opengoo installation' => 'Обновите вашу систему',
	);
?>
