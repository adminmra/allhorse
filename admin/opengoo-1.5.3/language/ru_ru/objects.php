<?php	 		 	
/* 
Translated into Russian for OpenGoo
Last update:  see readme_rus.txt

http://code.google.com/p/opengooru/
   
*/

 return array(
  // Objects
	'user' => 'Пользователь',
	'group' => 'Группа',
	'groups' => 'Группы',
	'users' => 'Пользователи',
	'member' => 'Участник',
	'members' => 'Участники',
	'company' => 'Компания',
	'companies' => 'Компании',
	'client' => 'Клиент',
	'clients' => 'Компании',
	'package' => 'Пакет',
	'project' => 'Проект',
	'projects' => 'Проекты',
	'message' => 'Заметка',
	'messages' => 'Заметки',
	'comment' => 'Комментарий',
	'comments' => 'Комментарии',
	'milestone' => 'Этап',
	'milestones' => 'Этапы',
	'task' => 'Задача',
	'subtasks' => 'Подзадачи',
	'tasks' => 'Задачи',
	'task list' => 'Задача',
	'task lists' => 'Задачи',
	'tag' => 'Тег',
	'tags' => 'Теги',
	'attachment' => 'Вложение',
	'attachments' => 'Вложения',
	'object subscribers' => 'Подписчики',
	'form' => 'Форма',
	'forms' => 'Формы',
	'file' => 'Файл',
	'files' => 'Файлы',
	'file revision' => 'Версия файла',
	'file revisions' => 'Версии файла',
	'file contents' => 'Содержимое файла',
	'revision' => 'Версия',
	'revisions' => 'Версии',
	'folder' => 'Папка',
	'folders' => 'Папки',
	'configuration category' => 'Раздел настройки',
	'configuration categories' => 'Разделы настройки',
	'handin' => 'Ручная правка',
	'handins' => 'Ручные правки',
	'emailunclassified' => 'Email',

    'load file'=> 'Файл загружен',
    'time' => 'Время',
  	'reporting' => 'Отчет',
	
  // Variations
  'modify object subscribers' => 'Изменить подписчиков',
  'subscription modified successfully' => 'Подписчики успешно изменены',  
	'owner company' => 'Моя организация',
	'client companies' => 'Компании',
	
	'open milestone' => 'Открыть этап',
	'open milestones' => 'Открыть этап',
	'completed milestone' => 'Завершенный этап',
	'completed milestones' => 'Завершенные этапы',
	'late milestones' => 'Последние этапы',
	'late milestones and tasks' => 'Последние этапы и задачи',
	'today milestones' => 'Сегодняшние этапы',
	'upcoming milestones' => 'Грядущие этапы',
	'completed task list' => 'Завершенная задача',
	'completed task lists' => 'Завершенные задачи',
	'open task list' => 'Открыть задачу',
	'open task lists' => 'Открыть задачи',
	'active project' => 'Активный проект',
	'active projects' => 'Активные проекты',
	'finished project' => 'Закрытый проект',
	'finished projects' => 'Закрытый проекты',
	'linked object' => 'Связанный объект',
	'linked objects' => 'Связанные объекты',
	
	'important message' => 'Важное сообщение',
	'important messages' => 'Важные сообщения',
	'important file' => 'Важный файл',
	'important files' => 'Важные файлы',
	'parent task' => 'Исходная задача',
	
	'private message' => 'Личное сообщение',
	'private milestone' => 'Личный этап',
	'private task list' => 'Личные задачи',
	'private comment' => 'Частный комментарий',
	'private file' => 'Личный файл',
	
	'object reminders' => 'Напоминания',
	'add object reminder' => 'Добавить напоминание',
	
  	'create document' => 'Создать документ',
  	'create presentation' => 'Создать презентацию',	
); ?>
