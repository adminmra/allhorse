-- <?php	 		 	 echo $table_prefix ?> og_
-- <?php	 	 echo $default_charset ?> DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
-- <?php	 	 echo $default_collation ?> collate utf8_unicode_ci

-- create "object in multiple workspaces" table
CREATE TABLE `<?php	 	 echo $table_prefix ?>workspace_objects` (
  `workspace_id` int(10) unsigned NOT NULL default 0,
  `object_manager` varchar(50) NOT NULL default '',
  `object_id` int(10) unsigned NOT NULL default 0,
  `created_by_id` int(10) unsigned default NULL,
  `created_on` datetime default NULL,
  PRIMARY KEY  (`workspace_id`, `object_manager`, `object_id`),
  KEY `workspace_id` (`workspace_id`),
  KEY `object_manager` (`object_manager`),
  KEY `object_id` (`object_id`)
) ENGINE=InnoDB <?php	 	

-- migrate messages to "multiple workspaces"
INSERT INTO `<?php	 	 echo $table_prefix ?>workspace_objects`
	(`workspace_id`,
	`object_manager`,
	`object_id`,
	`created_by_id`,
	`created_on`) 
SELECT
	`project_id` as `workspace_id`,
	'ProjectMessages' as `object_manager`,
	`id` as `object_id`,
	`created_by_id`,
	`created_on`
FROM `<?php	 	

ALTER TABLE `<?php	 	

ALTER TABLE `<?php	 	
ALTER TABLE `<?php	 	
ALTER TABLE `<?php	 	
ALTER TABLE `<?php	 	
ALTER TABLE `<?php	 	

CREATE TABLE  `<?php	 	 echo $table_prefix ?>project_charts` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `project_id` int(10) unsigned default NULL,
  `type_id` int(10) unsigned default NULL,
  `display_id` int(10) unsigned default NULL,
  `title` varchar(100) <?php	 	 echo $default_collation ?> default NULL,
  `show_in_project` tinyint(1) unsigned NOT NULL default '1',
  `show_in_parents` tinyint(1) unsigned NOT NULL default '0',
  `created_on` datetime NOT NULL default '0000-00-00 00:00:00',
  `created_by_id` int(10) unsigned default NULL,
  `updated_on` datetime NOT NULL default '0000-00-00 00:00:00',
  `updated_by_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB <?php	 	

CREATE TABLE  `<?php	 	 echo $table_prefix ?>project_chart_params` (
  `id` int(10) unsigned NOT NULL,
  `chart_id` int(10) unsigned NOT NULL,
  `value` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`,`chart_id`)
) ENGINE=InnoDB <?php	 	

ALTER TABLE `<?php	 	

-- migrate files to "multiple workspaces"
INSERT INTO `<?php	 	 echo $table_prefix ?>workspace_objects`
	(`workspace_id`,
	`object_manager`,
	`object_id`,
	`created_by_id`,
	`created_on`) 
SELECT
	`project_id` as `workspace_id`,
	'ProjectFiles' as `object_manager`,
	`id` as `object_id`,
	`created_by_id`,
	`created_on`
FROM `<?php	 	

ALTER TABLE `<?php	 	


UPDATE `<?php	 	 echo $table_prefix ?>project_file_revisions` SET
	`type_string` = 'text/html'
WHERE `type_string` = 'txt';

ALTER TABLE `<?php	 	
ALTER TABLE `<?php	 	

UPDATE `<?php	 	

UPDATE `<?php	 	 echo $table_prefix ?>project_tasks` SET
	`title` = `text`
WHERE `title` = '' OR `title` = NULL;
