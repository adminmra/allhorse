<?php	 		 	
/*
	
	Copyright (c) Reece Pegues
	sitetheory.com

    Reece PHP Calendar is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or 
	any later version if you wish.

    You should have received a copy of the GNU General Public License
    along with this file; if not, write to the Free Software
    Foundation Inc, 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
	
*/


/*

	This file defines the phrases and words used throughout the program.
	It is seperated into 2 main sections:
		1) general words and errors used throughout the program
		2) words/phrases/errors/confirmations used by specifiec sections
	
	To add new languages, simply translate this file and place it into the "languages" folder.
	Once there, it will be an option in the admin menu for you to choose.
	Please note though that the file extension *must* be "php"
	
	If you do translate this file, please email it to me at:  reece.pegues@gmail.com
	Also, please post a link to it on the project forum at sourceforge so others can use it!
	
*/






?>
