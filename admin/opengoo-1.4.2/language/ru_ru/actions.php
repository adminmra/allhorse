<?php	 		 	 return array(
	'register' => 'Регистрация',
	'login' => 'Вход',
	'logout' => 'Выход',
	'hide welcome info' => '» Скрыть информацию',
	'add company' => 'Добавить компанию',
	'edit company' => 'Обновить информацию о компании',
	'delete company' => 'Удалить компанию',
	'edit company logo' => 'Обновить логотип',
	'delete company logo' => 'Удалить логотип',
	'add client' => 'Добавить дочернюю компанию',
	'edit client' => 'Редактировать информацию о компании',
	'delete client' => 'Удалить дочернюю компанию',
	'add user' => 'Добавить пользователя',
	'edit user' => 'Редактировать пользователя',
	'delete user' => 'Удалить пользователя',
	'create user from contact' => 'Создать пользователя из контакта',
	'add group' => 'Добавить группу',
	'edit group' => 'Редактировать группу',
	'delete group' => 'Удалить группу',
	'add project' => 'Добавить проект',
	'edit project' => 'Редактировать свойства проекта',
	'delete project' => 'Удалить проект',
	'mark project as finished' => 'Пометить проект как закрытый',
	'mark project as active' => 'Пометить проект как активный',
	'add workspace' => 'Добавить новый проект',
	'edit workspace' => 'Редактировать проект',
	'delete workspace' => 'Удалить проект',
	'mark workspace as finished' => 'Пометить проект как законченный',
	'mark workspace as active' => 'Пометить проект как активный',
	'add message' => 'Добавить сообщение',
	'add new message' => 'Добавить новое сообщение',
	'edit message' => 'Редактировать сообщение',
	'delete message' => 'Удалить сообщение',
	'view message' => 'Просмотреть сообщение',
	'update message options' => 'Обновить настройки сообщения',
	'subscribe to object' => 'Подписаться',
	'unsubscribe from object' => 'Отписаться',
	'add comment' => 'Добавить комментарий',
	'edit comment' => 'Редактировать комментарий',
	'add task list' => 'Добавить задачу',
	'edit task list' => 'Редактировать',
	'delete task list' => 'Удалить',
	'reorder tasks' => 'Упорядочить',
	'reorder sub tasks' => 'Упорядочить подзадачи',
	'copy task' => 'Создать копию задачи',
	'copy milestone' => 'Создать копию этапа',
	'add task' => 'Добавить задачу',
	'add sub task' => 'Добавить подзадачу',
	'edit task' => 'Редактировать',
	'delete task' => 'Удалить',
	'mark task as completed' => 'Пометить задачу выполненной',
	'mark task as open' => 'Пометить задачу открытой',
	'add milestone' => 'Добавить этап',
	'edit milestone' => 'Редактировать',
	'delete milestone' => 'Удалить этап',
	'add event' => 'Добавить событие',
	'edit event' => 'Редактировать событие',
	'delete event' => 'Удалить событие',
	'previous month' => 'Предыдущий месяц',
	'next month' => 'Следующий месяц',
	'previous week' => 'Предыдущая неделя',
	'next week' => 'Следующая неделя',
	'previous day' => 'Предыдущий день',
	'next day' => 'Следующий день',
	'back to calendar' => 'К календарю',
	'back to day' => 'К выбору по дням',
	'pick a date' => 'Выбрать дату',
	'month' => 'Месяц',
	'week' => 'Неделя',
	'can edit company data' => 'Разрешить редактировать информацию о компании',
	'can manage security' => 'Разрешить управление безопасностью',
	'can manage workspaces' => 'Разрешить управление проектами',
	'can manage configuration' => 'Разрешить изменять конфигурацию ',
	'can manage contacts' => 'Разрешить управление контактами',
	'group users' => 'Группы пользователей',
	'update people' => 'Обновить',
	'remove user from project' => 'Отстранить пользователя от проекта',
	'remove company from project' => 'Отстранить компанию от проекта',
	'update profile' => 'Обновить профиль',
	'change password' => 'Изменить пароль',
	'update avatar' => 'Обновить аватар',
	'delete current avatar' => 'Удалить текущий аватар',
	'add form' => 'Добавить форму',
	'edit form' => 'Редактировать форму',
	'delete form' => 'Удалить форму',
	'submit project form' => 'Подтвердить',
	'add file' => 'Добавить файл',
	'edit file properties' => 'Свойства файла',
	'upload file' => 'Загрузить файл',
	'create new revision' => 'Создать новую версию',
	'add document' => 'Добавить документ',
	'save document' => 'Сохранить документ',
	'add spreadsheet' => 'Добавить таблицу',
	'add presentation' => 'Добавить презентацию',
	'document' => 'Документ',
	'spreadsheet' => 'Таблица',
	'presentation' => 'Презентация',
	'new' => 'Новый',
	'upload' => 'Загрузка',
	'hide' => 'Скрыть',
	'new document' => 'Новый документ',
	'new spreadsheet' => 'Новая таблица',
	'new presentation' => 'Новая презентация',
	'slideshow' => 'Слайдшоу',
	'revisions and comments' => 'Версии и комментарии',
	'Save' => 'Сохранить',
	'all elements' => 'Все элементы',
	'collapse all' => 'Свернуть всё',
	'expand all' => 'Развернуть всё',
	'properties' => 'Свойства',
	'edit file' => 'Редактировать файл',
	'edit document' => 'Редактировать документ',
	'edit spreadsheet' => 'Редактировать таблицу',
	'edit presentation' => 'Редактировать презентацию',
	'play' => 'Воспроизвести',
	'queue' => 'Поставить в очередь',
	'delete file' => 'Удалить файл',
	'add folder' => 'Добавить папку',
	'edit folder' => 'Редактировать папку',
	'delete folder' => 'Удалить папку',
	'edit file revisions' => 'Редактировать версию',
	'version' => 'версия',
	'last modification' => 'Последнее изменение',
	'link object' => 'Прикрепить объект',
	'link objects' => 'Прикрепить объекты',
	'link more objects' => 'Больше объектов',
	'unlink' => 'Удалить связь',
	'unlink object' => 'Удалить связь с объектом',
	'unlink objects' => 'Удалить связи с объектами',
	'extract' => 'Извлечь',
	'add files to zip' => 'Добавить в архив ZIP',
	'delete tag' => 'Удалить тег',
	'update permissions' => 'Обновить права',
	'edit permissions' => 'Редактировать права',
	'edit permissions explanation' => 'Пометьте нужные поля, чтобы подтвердить доступ пользователя к недавно созданному проекту.',
	'save as new revision' => 'Сохранить как новую версию',
	'save as' => 'Переименовать',
	'details' => 'Подробнее',
	'view history for' => 'Просмотреть историю для',
	'view history' => 'Просмотреть историю',
	'edit preferences' => 'Редактировать настройки',
	'view milestone' => 'Посмотреть этап',
	'custom properties' => 'Пользовательские настройки',
	'move to trash' => 'Переместить в корзину',
	'restore from trash' => 'Восстановить из корзины',
	'delete permanently' => 'Удалить навсегда',
	'copy file' => 'Копировать файл',
	'open weblink' => 'Открыть ссылку',
); ?>
