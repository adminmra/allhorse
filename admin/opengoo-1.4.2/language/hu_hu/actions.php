<?php	 		 	

  // Actions
  return array(
  
    // Registration
    'register' => 'Regisztráció',
    'login' => 'Belépés',
    'logout' => 'Kilépés',
    'hide welcome info' => '&raquo; Rejtsd el ezt az információt',
    
    // Companies
    'add company' => 'Adj hozzá céget',
    'edit company' => 'Szerkeszd a céget',
    'delete company' => 'Töröld a céget',
    'edit company logo' => 'Logó frissítés',
    'delete company logo' => 'Logó törlés',
    
    // Clients
    'add client' => 'Ügyfél hozzáadás',
    'edit client' => 'Ügyfél szerkesztés',
    'delete client' => 'Ügyfél törlés',
    
    // Users
    'add user' => 'Felhasználó hozzáadás',
    'edit user' => 'Felhasználó szerkesztés',
    'delete user' => 'Felhsználó törlés',
    'create user from contact' => 'Ügyfélből felhasználó',
    
    // Group
    'add group' => 'Csoport hozzáadása',
    'edit group' => 'Csoport szerkesztése',
    'delete group' => 'Csoport törlése',
    
    // Project
    'add project' => 'Projekt hozzáadása',
    'edit project' => 'Projekt,szerkesztése',
    'delete project' => 'Projekt törlése',
    'mark project as finished' => 'Projekt megjelölése befejezettnek',
    'mark project as active' => 'Projekt megjelölése aktívnak',
    
    // Workspace
    'add workspace' => 'Munkahely hozzáadása',
    'edit workspace' => 'Munkahely szerkesztése',
    'delete workspace' => 'Munkahely törlése',
    'mark workspace as finished' => 'Munkahely megjelölése 
befejezettnek',
    'mark workspace as active' => 'Munkahely megjelölése aktívnak',
    
    // Messages
    'add message' => 'Üzenet hozzáadása',
    'add new message' => 'Új üzenet',
    'edit message' => 'Üzenet szerkesztése',
    'delete message' => 'Üzenet törlése',
    'view message' => 'Üzenet megtekintése',
    'update message options' => 'Üzenet tulajdonságok',
    'subscribe to object' => 'Feliratkozás',
    'unsubscribe from object' => 'Feliratkozás törlése',
    
    // Comments
    'add comment' => 'Megjegyzés hozzáfűzése',
    'edit comment' => 'Megjegyzés szerkesztése',
    
    // Task list
    'add task list' => 'Feladat hozzáadása',
    'edit task list' => 'Feladat szerkesztése',
    'delete task list' => 'Feladat törlése',
    'reorder tasks' => 'Feladatok új sorrendbe',
	'reorder sub tasks' => 'Alfeladatok új sorrendbe',
  	'copy task' => 'Másolat készítése e feladatról',
  	'copy milestone' => 'Create a copy of this milestone',
    
    // Task
    'add task' => 'Feladat hozzáadása',
	'add sub task' => 'Alfeladat hozzáadása',
    'edit task' => 'Feladat szerkesztése',
    'delete task' => 'Feladat törlése',
    'mark task as completed' => 'Feladat készre jelölése',
    'mark task as open' => 'Feladat újbóli megnyitása',
    
    // Milestone
    'add milestone' => 'Add milestone',
    'edit milestone' => 'Edit milestone',
    'delete milestone' => 'Delete milestone',
    
    // Events
    'add event' => 'Esemény hozzáadása',
    'edit event' => 'Esemény szerkesztése',
    'delete event' => 'Esemény törlése',
    'previous month' => 'Előző hónap',
    'next month' => 'Következő hónap',
    'previous week' => 'Előző hét',
    'next week' => 'Következő hét',
    'previous day' => 'Tegnap',
    'next day' => 'Holnap',
    'back to calendar' => 'Vissza a naptárhoz',
    'back to day' => 'Vissza a naphoz',
    'pick a date' => 'Dátum választása',
    'month' => 'Hónap',
    'week' => 'Hét',
    
    //Groups
    'can edit company data' => 'Szerkesztheti a cég adatokat',
    'can manage security' => 'Megadhat biztonsági beállításokat',
    'can manage workspaces' => 'Szerkesztheti a 
munkahelyeket/projeteket',
    'can manage configuration' => 'Szerkesztheti a konfigurációt',
    'can manage contacts' => 'Szerkesztheti az ügyfeleket',
    'group users' => 'Csoport felhasználók',
    
    
    // People
    'update people' => 'Frissít',
    'remove user from project' => 'Törlés a projektből',
    'remove company from project' => 'Törlés a projektből',
    
    // Password
    'update profile' => 'Profil frissítés',
    'change password' => 'Jelszó változtatás',
    'update avatar' => 'Avatar frissítés',
    'delete current avatar' => 'Delete current avatar',
    
    // Forms
    'add form' => 'Add form',
    'edit form' => 'Edit form',
    'delete form' => 'Delete form',
    'submit project form' => 'Submit',
    
    // Files
    'add file' => 'File hozzáadása',
    'edit file properties' => 'File tulajdonságok szerkesztése',
    'upload file' => 'File feltöltése',
    'create new revision' => 'Új verzió készítése',

    'add document' => 'Dokumentum hozzáadása',
    'save document' => 'Dokumentum mentése',
    'add spreadsheet' => 'Számoló tábla hozzáadása',
    'add presentation' => 'Bemutató hozzáadása',
    'document' => 'Dokumentum',
    'spreadsheet' => 'Számoló tábla',
    'presentation' => 'Bemutató',

    'new' => 'Új',
    'upload' => 'Feltöltés',
    'hide' => 'Elrejtés',
    'new document' => 'Új dokumentum',
    'new spreadsheet' => 'Új számolótábla',
    'new presentation' => 'Új bemutató',

    'slideshow' => 'Vetítés',
    'revisions and comments' =>'Verziók & Megjegyzések',
        
    'Save' => 'Mentés',
    'all elements' => 'Minden elemet',
    'collapse all' => 'Becsukás Összes',
    'expand all' => 'Kinyitás Összes',

    'properties' => 'Tulajdonságok',
    'edit file' => 'File szerkesztése',
    'edit document' => 'Dokumentum szerkesztése',
    'edit spreadsheet' => 'Számolótábla szerkesztése',
    'edit presentation' => 'Edit presentation',

  	'play' => 'Lejátszás',
  	'queue' => 'Queue',
  
    'delete file' => 'File törlése',
    
    'add folder' => 'Mappa hozzáadása',
    'edit folder' => 'Mappa szerkesztése',
    'delete folder' => 'Mappa törlése',
    
    'edit file revisions' => 'Verzió szerkesztése',
    'version' => 'ver',
    'last modification' => 'Utolsó módosítás',
    
    'link object' => 'Elem csatolása',
    'link objects' => 'Elemek csatolása',
    'link more objects' => 'Több elem csatolása',
    'unlink' => 'Leválasztás',
    'unlink object' => 'Elem leválasztása',
    'unlink objects' => 'Elemek leválasztása',
	'extract' => 'Szétbontás',
  	'add files to zip' => 'File hozzáadása zip-hez',
  	
    // Tags
    'delete tag'  => 'Tag törlése',
    
    // Permissions
    'update permissions' => 'Engedélyek frissítése',
    'edit permissions' => 'Engedélyek szerkesztése',
    'edit permissions explanation'  => 'Jelöld meg, hogy miket engedélyezel!',
  
  	'save as new revision' => 'Mentés új verzióként',
	'save as' => 'Átnevezés',
	'details' => 'Részletek',
	'view history for' => 'Előzmények megtekintése',
	'view history' => 'Előzmény megtekintése',    
	'edit preferences' => 'Beállítások szerkesztése',
	'view milestone' => 'Mérföldkövek megtekintése',
  	'custom properties' => 'Egyedi beállítások',
  	'move to trash' => 'Kukába dobás',
  	'restore from trash' => 'Visszaállítás kukából',
  	'delete permanently' => 'Végleges törlés',
  	'copy file' => 'E file másolása',
  	'open weblink' => 'Web link megnyitása',
  ); // array

?>
