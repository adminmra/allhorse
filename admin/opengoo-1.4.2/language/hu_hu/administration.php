<?php	 		 	 return array(
	'administration tool name test_mail_settings' => 'Teszt email beállítások',
	'administration tool desc test_mail_settings' => 'Ezzel az egyszerű eszközzel küldhetsz teszt email üzenetet, hogy ellenőrizd az OpenGoo email beállításait.',
	'administration tool name mass_mailer' => 'Tömeges email küldése',
	'administration tool desc mass_mailer' => 'Ez egy egyszerű eszköz, amellyel egyszerű szöveges üzenetet küldhetsz a rendszer valamennyi felhasználójának.',
	'configuration' => 'Beállítások',
	'mail transport mail()' => 'PHP beállításokkal',
	'mail transport smtp' => 'SMTP szerver',
	'secure smtp connection no' => 'Nem',
	'secure smtp connection ssl' => 'Igen, SSL használatával',
	'secure smtp connection tls' => 'Igen, TLS használatával',
	'file storage file system' => 'Fájl rendszer',
	'file storage mysql' => 'Adatbázis (MySQL)',
	'config category name general' => 'Általános',
	'config category desc general' => 'Általános OpenGoo beállítások.',
	'config category name mailing' => 'Levelezés',
	'config category desc mailing' => 'Ezekkel a beállításokkal adhatod meg, hogyan kezelje az OpenGoo a levelezést. Használhatod a php.ini-ben meghatározott beállításokat vagy megadhatod bármelyik SMTP szerver elérését',
	'config category name modules' => 'Modulok',
	'config category desc modules' => 'Itt engedélyezheted az OpenGoo moduljait. A nem engedélyezéssel csak elrejted a kezelő felületről. Ettől még más felhasználók létrehozhatnak és szerkeszthetnek elemeket benne.',
	'config option name site_name' => 'A hely neve',
	'config option desc site_name' => 'Ez fog megjelenni a hely neveként a műszerfal oldalon',
	'config option name file_storage_adapter' => 'Fájl tárolás',
	'config option desc file_storage_adapter' => 'Válaszd ki, hol akarod tárolni a feltöltött dokumentumokat. Figyelem! A tárolás helyének megváltoztatása hozzáférhetetlenné teszi az előzőleg feltöltött állományokat! Mentsd le azokat először!',
	'config option name default_project_folders' => 'Alap mappák',
	'config option desc default_project_folders' => 'Ezeket a mappákat hozza létre a rendszer egy projekt létrehozásakor. Minden nevet új sorba kell írni! Az ismétlődő vagy üres sorok figyelmen kívül lesznek hagyva.',
	'config option name theme' => 'Téma',
	'config option desc theme' => 'Témák használatával meg tudod változtatni az OpenGoo megjelenését. Frissíteni kell a böngészőben a lapot a hatás eléréséhez.',
	'config option name days_on_trash' => 'Napok a kukában',
	'config option desc days_on_trash' => 'Hány nap elteltével törlődnek automatikusan a kukába dobott elemek. "0" beállítása esetén nem kerülnek törlésre az elemek.',
	'config option name enable_notes_module' => 'Feljegyzés Modul engedélyezése',
	'config option name enable_email_module' => 'Email Modul engedélyezése',
	'config option name enable_contacts_module' => 'Ügyfelek Modul engedélyezése',
	'config option name enable_calendar_module' => 'Naptár Modul engedélyezése',
	'config option name enable_documents_module' => 'Dokumentumok Modul engedélyezése',
	'config option name enable_tasks_module' => 'Feladat Modul engedélyezése',
	'config option name enable_weblinks_module' => 'Honlapok Modul engedélyezése',
	'config option name enable_time_module' => 'Idő Modul engedélyezése',
	'config option name enable_reporting_module' => 'Jelentés Modul engedélyezése',
	'config option name upgrade_check_enabled' => 'Frissítés ellenőrzésének engedélyezése',
	'config option desc upgrade_check_enabled' => 'Ha "igen" a beállítás, az OpenGoo naponta ellenőrzi az új verziók elérhetőségét',
	'config option name work_day_start_time' => 'A munkanap kezdő időpontja',
	'config option desc work_day_start_time' => 'Annak meghatározása, hánykor kezdődik a munkanapod',
	'config option name use_minified_resources' => 'Tömörített erőforrások használata',
	'config option desc use_minified_resources' => 'Tömörített Javascript és CSS használatával növelheted a rendszer teljesítményét. Újra tömörítened kell a JS és CSS állományokat, ha módosítottad azokat, a "http(s)://OpenGooWebhelyed/public/tools" használatával.',
	'config option name exchange_compatible' => 'Microsoft Exchange kompatibilis mód',
	'config option desc exchange_compatible' => 'Ha Microsoft Exchange Szervert használsz, állítsd ezt az értéket "igen"-re a néhány ismert levelezési probléma elkerülése érdekében.',
	'config option name mail_transport' => 'Email továbbítás',
	'config option desc mail_transport' => 'Beállíthatod a PHP beállításainak használatára, vagy megadhatsz más SMTP szervert.',
	'config option name smtp_server' => 'SMTP szerver',
	'config option name smtp_port' => 'SMTP port',
	'config option name smtp_authenticate' => 'SMTP hitelesítés használata',
	'config option name smtp_username' => 'SMTP felhasználói név',
	'config option name smtp_password' => 'SMTP jelszó',
	'config option name smtp_secure_connection' => 'Biztonságos SMTP kapcsolat használata',
	'can edit company data' => 'Szerkesztheti a cég adatait',
	'can manage security' => 'Biztonsági beállításokat adhat meg',
	'can manage workspaces' => 'Szerkeszthet projekteket',
	'can manage configuration' => 'Szerkesztheti a beállításokat',
	'can manage contacts' => 'Szerkeszthet ügyfeleket',
	'group users' => 'Csoport tagok',
	'user ws config category name dashboard' => 'Műszerfal beállítások',
	'user ws config category name task panel' => 'Feladat beállítások',
	'user ws config category name general' => 'Általános',
	'user ws config option name show pending tasks widget' => 'Mutasd a függő feladatok kisalkalmazást',
	'user ws config option name pending tasks widget assigned to filter' => 'Mutasd a hozzá rendelt feladatokat',
	'user ws config option name show late tasks and milestones widget' => 'Mutasd a késében lévő feladatok és mérföldkövek kisalkalmazást',
	'user ws config option name show messages widget' => 'Mutasd a feljegyzések kisalkalmazást',
	'user ws config option name show comments widget' => 'Mutasd a megjegyzések kisalkalmazást',
	'user ws config option name show documents widget' => 'Mutasd a dokumentumok kisalkalmazást',
	'user ws config option name show calendar widget' => 'Mutasd a mini naptár kisalkalmazást',
	'user ws config option name show charts widget' => 'Mutasd a grafikonok kisalkalmazást',
	'user ws config option name show emails widget' => 'Mutasd a email üzenetek kisalkalmazást',
	'user ws config option name localization' => 'Nyelv megadása',
	'user ws config option desc localization' => 'A feliratok és dátumok a megadott nyelven fognak megjelenni. Frissítsd a böngésző lapját!',
	'user ws config option name initialWorkspace' => 'Kezdő Projekt',
	'user ws config option desc initialWorkspace' => 'A belépésed után ez a projekt lesz kiválasztva, vagy beállíthatod, hogy a legutóbb használt legyen kiválasztva.',
	'user ws config option name rememberGUIState' => 'Emlékezzen a kezelői felület helyzetére',
	'user ws config option desc rememberGUIState' => 'Beállíthatod, hogy emlékezzen a kezelői felület beállításaira (panel méretek, becsukott/kinyitott voltuk, stb). Figyelem! Ez még BÉTA állapotú!',
	'user ws config option name time_format_use_24' => '24 órás idő megjelenítés',
	'user ws config option desc time_format_use_24' => 'Ha engedélyezed: \'hh:mm\' formában 00:00-tól 23:59-ig fogja az időt megjeleníteni, ha nem; 1-től 12-ig.',
	'user ws config option name work_day_start_time' => 'Munkanap kezdete',
	'user ws config option desc work_day_start_time' => 'Hány órától kezdesz dolgozni..',
	'user ws config option name my tasks is default view' => 'A hozzám rendelt feladatok az alap beállítás',
	'user ws config option desc my tasks is default view' => 'Ha nem állítod ezt be, alapból az összes feladatot látni fogod',
	'user ws config option name show tasks in progress widget' => 'Mutasd a \'Végrehajtás alatt lévő feladatok\' kisalkalmazást',
	'user ws config option name can notify from quick add' => 'A feladat értesítő legyen bekapcsolva alapból',
	'user ws config option desc can notify from quick add' => 'Az értesítő lehetővé teszi, hogy értesítést küldj a felhasználónak a feladat hozzárendelése vagy módosítása esetére',
	'backup process desc' => 'A mentéssel a teljes rendszer egy tömörített állományba kerül. <br>A mentési állomány létrehozása több percbe is beletelhet, az alábbi lépésekben:<br>1.- Indítsd el a mentési folyamatot,<br>2.- Töltsd le a tömörített állományt. <br>3.- A mentés kézzel is törölheted, így nem lesz hozzáférhető többé. <br>',
	'start backup' => 'Mentési folyamat elindítása',
	'start backup desc' => 'A mentési folyamat elindítása magába foglalja az előző mentett állomány törlését, és új állomány létrehozását.',
	'download backup' => 'Mentés letöltése',
	'download backup desc' => 'A mentés letöltéséhez előbb létre kell hozni egyet!',
	'delete backup' => 'Mentés törlése',
	'delete backup desc' => 'Ha törlöd, nem tudod letölteni! Letöltés után viszont javasolt azt törölni a szerverről!',
	'backup' => 'Mentés',
	'backup menu' => 'Mentés Menű',
	'last backup' => 'Az utolsó mentés létrehozásának ideje',
	'no backups' => 'Nincs letölthető mentés',
	'user ws config option name always show unread mail in dashboard' => 'Mindig mutassa a műszerfalon a még nem olvasott leveleket',
	'user ws config option desc always show unread mail in dashboard' => 'Ha a "Nem" van kiválasztva, az aktív projekt üzeneteit láthatod',
	'workspace emails' => 'Projekt levelek',
	'user ws config option name tasksShowWorkspaces' => 'Projekt megmutatása',
	'user ws config option name tasksShowTime' => 'Idő megmutatása',
	'user ws config option name tasksShowDates' => 'Dátum megmutatása',
	'user ws config option name tasksShowTags' => 'Címkék megmutatása',
	'user ws config option name tasksGroupBy' => 'Csoport szerint',
	'user ws config option name tasksOrderBy' => 'Sorrend',
	'user ws config option name task panel status' => 'Státusz',
	'user ws config option name task panel filter' => 'Szűrési feltétel',
	'user ws config option name task panel filter value' => 'Szűrő érték',
	'templates' => 'Sablonok',
	'add template' => 'Sablon hozzáadása',
	'confirm delete template' => 'Biztosan törlöd ezt a sablont?',
	'no templates' => 'Nincsenek sablonok',
	'template name required' => 'A sablon nevének megadása kötelező',
	'can manage templates' => 'Kezelhet sablonokat',
	'new template' => 'Új sablon',
	'edit template' => 'Sablon szerkesztése',
	'template dnx' => 'A sablon nem létezik',
	'success edit template' => 'A sablon sikeresen módosítva',
	'log add cotemplates' => '{0} hozzáadva',
	'log edit cotemplates' => '{0} módosítva',
	'success delete template' => 'A sablon sikeresen törölve',
	'error delete template' => 'Hiba a sablon törlése során',
	'objects' => 'Elemek',
	'objects in template' => 'Elemek a sablonban',
	'no objects in template' => 'Nincsenek elemek a sablonban',
	'add to a template' => 'Hozzáadás sablonhoz',
	'add an object to template' => 'Elem hozzáadása ehhez a sablonhoz',
	'you are adding object to template' => '{0} \'{1}\' hozzáadása sablonhoz. Válassz az alábbi sablonokból, vagy hozz létre egy újat ennek: {0}.',
	'success add object to template' => 'Az elem sikeresen hozzáadva a sablonhoz',
	'object type not supported' => 'Ezt az elemet nem támogatja a sablon',
	'assign template to workspace' => 'A sablon hozzárendelése projekthez',
	'cron events' => 'Ismétlődő események',
	'about cron events' => 'Az ismétlődő eseményekről...',
	'cron events info' => 'Az ismétlődő események lehetővé teszik, hogy az OpenGoo a rendszerbe történő bejelentkezés nélkül is végrehajtson feladatokat. Az engedélyezéséhez meg kell határozz egy ismétlődő feladatot a "cron.php" nevű fájlban, ami az OpenGoo telepítésed gyökerében található. A cron futási rendszeressége (milyen időközönként futtatod) meghatározza, milyen időközöket állíthatsz be; pl. ha a cron 5 percenként fut hiába állítod be, hogy 1 percenként ellenőrizze új OpenGoo verzió meglétét akkor is csak 5 percenként tudja elvégezni. A cron beállításokról kérdezd meg a rendszered adminisztrátorát.',
	'cron event name check_mail' => 'Email ellenőrzése',
	'cron event desc check_mail' => 'Ez az ismétlődő esemény a rendszerben beállított valamennyi email postafiókot ellenőrzi.',
	'cron event name purge_trash' => 'Kuka ürítése',
	'cron event desc purge_trash' => 'Ez az ismétlődő esemény kitöröl minden a \'Nap a kukában\' beállításnál régebben a kukába dobott elemet.',
	'cron event name send_reminders' => 'Emlékeztető küldése',
	'cron event desc send_reminders' => 'Ez az ismétlődő esemény emlékeztetőket küld.',
	'cron event name check_upgrade' => 'Rendszer frissítés ellenőrzése',
	'cron event desc check_upgrade' => 'Ez az ismétlődő esemény ellenőrzi új OpenGoo verzió elérhetőségét',
	'cron event name create_backup' => 'Mentés készítése',
	'cron event desc create_backup' => 'Mentést készít, amit letölthetsz az Adminisztrációs lapról.',
	'next execution' => 'Következő végrehajtása',
	'delay between executions' => 'Várakozás a végrehajtások között',
	'enabled' => 'Megengedve',
	'no cron events to display' => 'Nincs ismétlődő esemény',
	'success update cron events' => 'Az ismétlődő események frissítése sikeres',
	'manual upgrade' => 'Kézi frissítés',
	'manual upgrade desc' => 'az OpenGoo kézi frissítése is elvégezhető az új verzió letöltésével. Bontsd ki a letöltött állomány a telepítés gyökerébe, majd állítsd a böngésződet a <a href="public/upgrade">\'public/upgrade\'</a> címre a frissítés végrehajtásához',
	'automatic upgrade' => 'Automatikus frissítés',
	'automatic upgrade desc' => 'Az automatikus frissítés automatikusan letölti és kibontja a friss verziót és lefuttatja a szükséges folyamatot. A web szervernek hozzáférésének kell lennie az összes könyvtárhoz!',
	'start automatic upgrade' => 'Automatikus frissítés megkezdése',
	'user ws config option name show dashboard info widget' => 'Mutasd a projekt leírás kisalkalmazást',
); ?>
