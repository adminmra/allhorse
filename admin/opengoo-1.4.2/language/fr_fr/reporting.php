<?php	 		 	 return array(
	'custom' => 'Personnalisé',
	'custom reports' => 'Rapports personnalisés',
	'no custom reports' => 'Il n\'y a pas de Rapports personnalisés',
	'add custom report' => 'Ajouter un Rapport personnalisé',
	'edit custom report' => 'Éditer un Rapport personnalisé',
	'new custom report' => 'Nouveau Rapport personnalisé',
	'add report' => 'Ajouter un rapport',
	'object type' => 'Type d\'objet',
	'add condition' => 'Ajouter une condition',
	'custom report created' => 'Rapport personnalisé créé',
	'custom report updated' => 'Rapport personnalisé mis-à-jour\n',
	'conditions' => 'Conditions',
	'columns and order' => 'Colonnes et tri',
	'true' => 'Vrai',
	'false' => 'Faux',
	'field' => 'Champs',
	'condition' => 'Condition',
	'ends with' => 'Finit par',
	'select unselect all' => 'Sélectionner/déselectionner tout',
	'ascending' => 'Croissant',
	'descending' => 'Décroissant',
); ?>
