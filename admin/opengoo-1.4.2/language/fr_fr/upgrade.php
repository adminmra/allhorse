<?php	 		 	 return array(
	'upgrade' => 'Mise à jour',
	'upgrade from' => 'Mise à jour depuis',
	'upgrade to' => 'Mise à jour vers',
	'already upgraded' => 'Vous avez déjà la toute dernière mise à jour.',
	'back to opengoo' => 'Retour à OpenGoo',
	'all rights reserved' => 'Tous droits réservés',
	'upgrade process log' => 'Log du processus de mise à jour',
	'upgrade opengoo' => 'Mise à jour d\'OpenGoo',
	'upgrade your opengoo installation' => 'Mettre à jour votre installation d\'OpenGoo',
); ?>
