<?php	 		 	 return array(
	'upgrade' => 'Aktualisiere',
	'upgrade from' => 'Aktualisiere von',
	'upgrade to' => 'Aktualisiere auf',
	'back to opengoo' => 'Zurück zu OpenGoo',
	'upgrade opengoo' => 'Aktualisiere OpenGoo',
	'all rights reserved' => 'Alle Rechte vorbehalten',
	'upgrade process log' => 'Protokoll Aktualisierungsprozess ',
	'upgrade your opengoo installation' => 'Aktualisiere deine OpenGoo Installation',
	'already upgraded' => 'Du hast bereits auf die neueste mögliche Version aktualisiert.',
); ?>
