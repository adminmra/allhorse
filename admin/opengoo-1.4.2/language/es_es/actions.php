<?php	 		 	

  // Actions
  return array(
  
    // Registration
    'register' => 'Registrar',
    'login' => 'Conexión',
    'logout' => 'Desconexión',
    'hide welcome info' => '&raquo; Ocultar información de forma permanente',
    
    // Companies
    'add company' => 'Añadir empresa',
    'edit company' => 'Editar datos de la compañía',
    'delete company' => 'Eliminar compañía',
    'edit company logo' => 'Guardar logo',
    'delete company logo' => 'Eliminar logo',
    
    // Clients
    'add client' => 'Añadir cliente',
    'edit client' => 'Editar cliente',
    'delete client' => 'Eliminar cliente',
    
    // Users
    'add user' => 'Añadir usuario',
    'edit user' => 'Editar usuario',
    'delete user' => 'Eliminar usuario',
    'create user from contact' => 'Crear usuario a partir de un contacto existente',
    
    // Group
    'add group' => 'Añadir grupo',
    'edit group' => 'Editar grupo',
    'delete group' => 'Eliminar grupo',
    
    // Project
    'add project' => 'Añadir área de trabajo',
    'edit project' => 'Editar área de trabajo',
    'delete project' => 'Eliminar área de trabajo',
    'mark project as finished' => 'Marcar área de trabajo como cerrada',
    'mark project as active' => 'Marcar área de trabajo como activa',
    
    // Workspace
    'add workspace' => 'Añadir nueva área de trabajo',
    'edit workspace' => 'Editar área de trabajo',
    'delete workspace' => 'Eliminar área de trabajo',
    'mark workspace as finished' => 'Marcar área de trabajo como finalizada',
    'mark workspace as active' => 'Marcar área de trabajo como activa',
    
    // Messages
    'add message' => 'Añadir nota',
    'edit message' => 'Editar nota',
    'delete message' => 'Eliminar nota',
    'view message' => 'Mostrar nota',
    'update message options' => 'Actializar opciones de nota',
    'subscribe to object' => 'Suscribirse al objeto',
    'unsubscribe from object' => 'Borrar suscripción al objeto',
    
    // Comments
    'add comment' => 'Añadir comentario',
    'edit comment' => 'Editar comentario',
    
    // Task list
    'add task list' => 'Añadir tarea',
    'edit task list' => 'Editar tarea',
    'delete task list' => 'Eliminar tarea',
    'reorder tasks' => 'Reordenar tareas',
	'reorder sub tasks' => 'Reordenar sub-tareas',
  	'copy task' => 'Copiar tarea',
  	'copy milestone' => 'Copiar hito',
    
    // Task
    'add task' => 'Añadir tarea',
	'add sub task' => 'Añadir subtarea',
    'edit task' => 'Editar tarea',
    'delete task' => 'Eliminar tarea',
    'mark task as completed' => 'Marcar tarea como completada',
    'mark task as open' => 'Marcar tarea como abierta',
    
    // Milestone
    'add milestone' => 'Añadir hito',
    'edit milestone' => 'Editar hito',
    'delete milestone' => 'Eliminar hito',
    
    // Events
    'add event' => 'Añadir evento',
    'edit event' => 'Editar evento',
    'delete event' => 'Eliminar evento',
    'previous month' => 'Mes anterior',
    'next month' => 'Próximo mes',
  	'previous week' => 'Semana anterior',
    'next week' => 'Próxima semana',
    'previous day' => 'Día anterior',
    'next day' => 'Próximo día',
    'back to calendar' => 'Volver al calendario',
    'back to day' => 'Volver al día',
    'pick a date' => 'Elegir fecha',
    'month' => 'Mes',
    'week' => 'Semana',
    
    //Groups
    'can edit company data' => 'Permitir la edición de los datos de la compañía',
    'can manage security' => 'Permitir la modificación de las configuraciones de seguridad',
    'can manage workspaces' => 'Permitir la modificación de las configuraciones de áreas de trabajo',
    'can manage configuration' => 'Permitir la modificación de las configuraciones',
    'can manage contacts' => 'Permitirl la modificación de las configuraciones de contactos',
    'group users' => 'Agrupar usuarios',
    
    
    // People
    'update people' => 'Poner al día',
    'remove user from project' => 'Eliminar usuario de esta área de trabajo',
    'remove company from project' => 'Eliminar compañía de esta área de trabajo',
    
    // Password
    'update profile' => 'Editar datos de mi cuenta',
    'change password' => 'Modificar contraseña',
    'update avatar' => 'Guardar nueva imagen',
    'delete current avatar' => 'Eliminar la imagen actual',
    
    // Forms
    'add form' => 'Añadir formulario',
    'edit form' => 'Editar formulario',
    'delete form' => 'Eliminar formulario',
    'submit project form' => 'Enviar',
    
    // Files
    'add file' => 'Añadir archivo',
    'file properties' => 'Propiedades del archivo',
    'upload file' => 'Cargar archivo',
    'create new revision' => 'Crear nueva revisión',

    'add document' => 'Añadir documento',
    'save document' => 'Guardar documento',
    'add spreadsheet' => 'Añadir hoja de cálculo',
    'add presentation' => 'Añadir presentación',
    'document' => 'Documento',
    'spreadsheet' => 'Hoja de cálculo',
    'presentation' => 'Presentación',

    'new' => 'Nuevo',
    'upload' => 'Cargar',
    'hide' => 'Ocultar',
    'new document' => 'Nuevo documento',
    'new spreadsheet' => 'Nueva hoja de cálculo',
    'new presentation' => 'Nueva presentación',

    'slideshow' => 'Presentación',
    'revisions and comments' =>'Revisiones y comentarios',
        
    'Save' => 'Guardar',
    'all elements' => 'Todos los elementos',
    'collapse all' => 'Contraer todos',
    'expand all' => 'Expandir todos',

    'properties' => 'Propiedades',
    'edit file' => 'Editar archivo',
    'edit document' => 'Editar documento',
    'edit spreadsheet' => 'Editar hoja de cálculo',
    'edit presentation' => 'Editar presentación',

  	'play' => 'Reproducir',
  	'queue' => 'Encolar',
  
    'delete file' => 'Eliminar archivo',
    
    'add folder' => 'Añadir carpeta',
    'edit folder' => 'Editar carpeta',
    'delete folder' => 'Eliminar carpeta',
    
    'edit file revisions' => 'Editar revisiones',
    'version' => 'ver',
    'last modification' => 'Última modificación',
    
    'link object' => 'Vincular este objecto',
    'link objects' => 'Vincular estos objectos',
    'link more objects' => 'Vincular a más objectos',
    'unlink' => 'Eliminar vínculo',
    'unlink object' => 'Eliminar vínculo con este objecto',
    'unlink objects' => 'Eliminar vínculo con estos objectos',
	'extract' => 'Extraer',
  	'add files to zip' => 'Agregar archivos',
    	
    // Tags
    'delete tag'  => 'Eliminar etiqueta',
    
    // Permissions
    'update permissions' => 'Actualizar los permisos',
    'edit permissions' => 'Editar permisos',
    'edit permissions explanation'  => 'Seleccione los usuarios habilitados para acceder al área de trabajo creada.',
  
  	'save as new revision' => 'Guardar como nueva revisión',
	'save as' => 'Guardar como',
	'details' => 'Detalles',
	'view history for' => 'Ver historial',
	'view history' => 'Ver historial',
	'edit preferences' => 'Editar preferencias',
  	'add new message' => 'Añadir nueva nota',  
    'view milestone' => 'Ver hito',
    'custom properties' => 'Propiedades Personalizadas',
  	'edit file properties' => 'Editar propiedades del archivo',
    'move to trash' => 'Enviar a la papelera',
    'restore from trash' => 'Restaurar de la papelera',
    'delete permanently' => 'Eliminar permanentemente',
  
  	'copy file' => 'Copiar este archivo',
  	'open weblink' => 'Abrir enlace web',
  
  
  ); // array

?>