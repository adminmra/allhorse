<?php	 		 	
	return array(
		'upgrade' => 'Upgrade',
		'upgrade from' => 'Upgrade from',
		'upgrade to' => 'Upgrade to',
		'already upgraded' => 'You have upgraded to the latest possible version.',
		'back to opengoo' => 'Back to OpenGoo',
		'all rights reserved' => 'All rights reserved',
		'upgrade process log' => 'Upgrade process log',
		'upgrade opengoo' => 'Upgrade OpenGoo',
		'upgrade your opengoo installation' => 'Upgrade your OpenGoo installation',
	);
?>