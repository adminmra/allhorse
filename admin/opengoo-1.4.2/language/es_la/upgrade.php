<?php	 		 	 return array(
	'upgrade' => 'Actualizar',
	'upgrade from' => 'Actualizar de',
	'upgrade to' => 'Actualizar a',
	'already upgraded' => 'Ha actualizado a la última versión posible.',
	'back to opengoo' => 'Volver a OpenGoo',
	'all rights reserved' => 'Todos los derechos reservados',
	'upgrade process log' => 'Registro del proceso de actualización',
	'upgrade opengoo' => 'Actualizar OpenGoo',
	'upgrade your opengoo installation' => 'Actualice su instalación de OpenGoo',
); ?>
