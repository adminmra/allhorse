<?php	 		 	
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:		function
 * Name:		gradient
 * Version:		1.0
 * Date:		July 1, 2003
 * Author:		Lucas Baltes <lucas at thebobo dot com>
 * License:		GPL (http://www.gnu.org/)
 * Credits:		<kristoffer at caveo dot se> for his posting in 
 *				the	php manual (dechex function, user notes)
 * Purpose:		apply a color gradient to a string of text.
 * Install:		drop into the plugin directory
 * Required input:
 *				text = string of text to colorize
 *
 * Optional input: 
 *				font = the typeface to use (Helvetica, etc)
 *				size = the font size to use
 *				start = rgb colors to start with, 
 *						 separated by comma's (ie '255,255,0')
 *				end = rgb colors to end with, 
 *						 separated by comma's (ie '255,255,0')
 * 
 * Examples:	{gradient text="this is a gradient"}
 *				{gradient text="this is a gradient" font="Verdana"}
 *				{gradient text="this is a gradient" font="Verdana" size="3" start="255,255,0" end="0,0,255"}
 * -------------------------------------------------------------
 */

function smarty_function_gradient($params, &$smarty)
{
    extract($params);

    if (empty($text)) {
        $smarty->trigger_error("gradient: missing 'text' parameter");
        return;
    } else {
		$text = str_replace("\n", "", $text);
		$levels = strlen($text);
	}
	
	if (empty($font)) {
		$font = "";
	} else {
		$font = " face=\"" . $font . "\"";
	}

	if (empty($size)) {
		$size = "";
	} else {
		$size = " size=\"" . $size . "\"";
	}

    if (empty($start)) {
		$start = array(0,0,255);
    } else {
		$start = explode(",", $start);
	}

    if (empty($end)) {
		$end = array(255,0,0);
    } else {
		$end = explode(",", $end);
	}
	
	$output = "";
	
	for ($i=1;$i<=$levels;$i++)
	{
		for ($j=0;$j<3;$j++)
		{
			$buffer[$j] = $start[$j] - $end[$j];
			$buffer[$j] = floor($buffer[$j] / $levels);
			$rgb[$j] = $start[$j] - ($buffer[$j] * $i);

			if ($rgb[$j] > 255) $rgb[$j] = 255;

			$rgb[$j] = dechex($rgb[$j]);
			$rgb[$j] = strtoupper($rgb[$j]);

			if (strlen($rgb[$j]) < 2) $rgb[$j] = "0$rgb[$j]";
		}
		
		$color = "color=\"#" . $rgb[0] . $rgb[1] . $rgb[2] . "\"";
		$output .= "<font $color$font$size>" . $text[$i -1 ] . "</font>";
	}
	
	echo $output;

}

/* vim: set expandtab: */
?>