<?
	class utils
	{
		
		// Function for the Pagination
		function pagination($num_rows_all, $start, $limit, $urlstr)
		{
			$c = 1;
			$max = 5;
			if($start == 0)
			{
				$from = 1;
				$to = $limit;
			}
			else
			{
				$from = $start+1;
				$to = $from + $limit;
				if($to > $start + $limit)
					$to = $to - 1;
			}
			if($to > $num_rows_all)
			{
				$to = $num_rows_all;
			}
			if($to != 0 && $num_rows_all !=0)
			{
				//$countmsg = "Showing $from to $to of $num_rows_all";
			}
	
			$display .=  $countmsg."<br><br>";
	
			@$pages = intval($num_rows_all/$limit);
	
			if ($num_rows_all % $limit) 
			{
				$pages++;
			}
	
			$sets = intval($pages/$max);
	
			if($pages % $max)
			{
				$sets++;
			}		
	
			@$currentpage = intval($start/$limit);
	
			$currentpage++;
	
			if($currentpage > $max)
			{
				$set = intval($currentpage/$max);
				if($currentpage % $max)
				{
					$set++;
				}
	
				$end_page = $max * $set;
				$start_page = $end_page - $max;
	
				if($start_page > 1)
				{
					$start_page++; 
				}
			}
			else
			{
				$start_page = 1;
				$end_page = $max; 
			}
			if($end_page > $pages)
			{
				$end_page = $pages;
			}
			if($start_page > $max)
			{
				$previousdisplay = "...";
				$previousset = ($start_page - 2) * $limit;
			}
			if($end_page < $pages)
			{
				$nextdisplay = "...";
				$nextset = ($end_page) * $limit;
			}
	
			$display .=  '<div align="center">';
	
			if($previousset > 0) 
				 $display .= "<a href=\"" . $PHP_SELF . $urlstr ."&start=".$previousset."\" class=\"AA1\">&lt;&lt;</a>";
	
			if($start > 0) 
				 $display .= " <a href=\"" . $PHP_SELF . $urlstr ."&start=" .($start - $limit)."\" class=\"AA1\">Previous</a>  ".$previousdisplay."";
	
			if($num_rows_all > $limit)
			{
				while($start_page <= $end_page)
				{
					$subpage = $start_page - 1;
					$snext = $subpage * $limit;		
					if($start_page != $currentpage)
					{
						$display .= "<a href='".$PHP_SELF. $urlstr ."&start=".$snext."' class=\"AA1\">".$start_page."</a>";
					}
					else
					{
						$display .= "<font color='#000000' class=\"NOTLINK1\"><strong>".$start_page."</strong></font>";
					}		
	
					if($start_page != $end_page)
					{
						$display .= "&nbsp; ";
					}
	
					$scale++;
					$start_page++;
				}
			}
	
			if($num_rows_all > ($start + $limit)) 
			{
				 $display .=  "".$nextdisplay."  <a href=\"" . $PHP_SELF . $urlstr ."&start=".($start + $limit)."\" class=\"AA1\">Next</a> ";
			}
	
			if($nextset > 0) 
			{
				 $display .= " <a href=\"" . $PHP_SELF . $urlstr ."&start=".$nextset."\" class=\"AA1\">&gt;&gt;</a>";
			}
	
			$display .= '</div>';
	
			return $display;
	
		}//end function
		
		// Function for the Mail
		function send_mail($to,$from,$subject,$message)
		{
			$c_type = "text/html";
			
			$additional_headers = "MIME-Version : 1.0\r\n"."Content-type : ".$c_type.";Charset = iso-8859-1\r\n"."From : <".$from.">\r\n"."X-Mailer : PHP".phpversion(); 
			
			if(mail($to,$subject,$message,$additional_headers))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	

	} // End Class
?>