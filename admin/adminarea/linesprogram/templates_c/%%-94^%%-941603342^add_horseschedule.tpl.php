<?php	 		 	 /* Smarty version 2.6.1, created on 2008-02-18 12:54:12
         compiled from add_horseschedule.tpl */ ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "left.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-------------------- Body Section ------------------------>
<?php	 	 echo '
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
<script language="javascript" src="cal/cal.js"></script>
<SCRIPT LANGUAGE="JavaScript">
function showColor(val) {
document.web_form.lines_back_col.value = val;
}
function showColor2(val) {
document.web_form.lines_col.value = val;
}
function showColor3(val) {
document.web_form.specific_col.value = val;
}
function showColor4(val) {
document.web_form.specific_back_col.value = val;
}
</script>
'; ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0958A6" style="padding-left:10px;"><strong><font color="#FFFFFF"><?php	 	 echo $this->_tpl_vars['headingdata']; ?>
</font></strong></td>
          <td align="right" bgcolor="#0958A6" style="padding-right:10px;">
		  <a href="site_view.php" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="horse_schedule.php" method="post" enctype="multipart/form-data" name="web_form" >
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      
                      <tr>
            <td class="fieldname" colspan="2" align="left">
              <?php	 	 echo $this->_tpl_vars['warning']; ?>
            </td>
          </tr>
		  
								  
	 <!--<tr>
            <td width='401' class="fieldname">Class = sports</td>
            <td align="left" class="columnHeaderStatic">
              <input type=radio name="lines_back_col"  value="sports" checked="checked"></td>
          </tr>
		   -->
		 <?php	 	 if (isset($this->_sections['l'])) unset($this->_sections['l']);
$this->_sections['l']['name'] = 'l';
$this->_sections['l']['loop'] = is_array($_loop=$this->_tpl_vars['sched']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['l']['show'] = true;
$this->_sections['l']['max'] = $this->_sections['l']['loop'];
$this->_sections['l']['step'] = 1;
$this->_sections['l']['start'] = $this->_sections['l']['step'] > 0 ? 0 : $this->_sections['l']['loop']-1;
if ($this->_sections['l']['show']) {
    $this->_sections['l']['total'] = $this->_sections['l']['loop'];
    if ($this->_sections['l']['total'] == 0)
        $this->_sections['l']['show'] = false;
} else
    $this->_sections['l']['total'] = 0;
if ($this->_sections['l']['show']):

            for ($this->_sections['l']['index'] = $this->_sections['l']['start'], $this->_sections['l']['iteration'] = 1;
                 $this->_sections['l']['iteration'] <= $this->_sections['l']['total'];
                 $this->_sections['l']['index'] += $this->_sections['l']['step'], $this->_sections['l']['iteration']++):
$this->_sections['l']['rownum'] = $this->_sections['l']['iteration'];
$this->_sections['l']['index_prev'] = $this->_sections['l']['index'] - $this->_sections['l']['step'];
$this->_sections['l']['index_next'] = $this->_sections['l']['index'] + $this->_sections['l']['step'];
$this->_sections['l']['first']      = ($this->_sections['l']['iteration'] == 1);
$this->_sections['l']['last']       = ($this->_sections['l']['iteration'] == $this->_sections['l']['total']);
?>
		 <?php	 	 if ($this->_sections['l']['index']%3 == 0): ?>
		<tr>  
		<?php	 	 endif; ?>
            <td align="left" class="columnHeaderStatic" >
	    <input type="checkbox" name="check_<?php	 	 echo $this->_tpl_vars['sched'][$this->_sections['l']['index']]['id']; ?>
"  value='Y' <?php	 	 if ($this->_tpl_vars['sched'][$this->_sections['l']['index']]['display'] == 'Y'): ?> checked <?php	 	 endif; ?>>
	    <input  type="text" name="date_<?php	 	 echo $this->_tpl_vars['sched'][$this->_sections['l']['index']]['id']; ?>
"  value="<?php	 	 echo $this->_tpl_vars['sched'][$this->_sections['l']['index']]['addedon']; ?>
" readonly="readonly"><a onClick='popUpCalendar(this,document.web_form.date_<?php	 	 echo $this->_tpl_vars['sched'][$this->_sections['l']['index']]['id']; ?>
,"yyyy-mm-dd")' border="0"  ><img src="cal/cal.gif"></a></br>
        <!--</td> </tr>  
		<tr>          
            <td align="left" class="columnHeaderStatic" colspan="2"> -->
	    <textarea name="data_<?php	 	 echo $this->_tpl_vars['sched'][$this->_sections['l']['index']]['id']; ?>
"   cols="25" rows="10"><?php	 	 echo $this->_tpl_vars['sched'][$this->_sections['l']['index']]['data']; ?>
</textarea></td>
          <?php	 	 if ($this->_sections['l']['index']%3 == 2): ?>
		</tr>  
		<?php	 	 endif; ?>
		 <?php	 	 endfor; endif; ?>
								  
								 
		<!--						  <tr>
            <td width='401' class="fieldname">Class = sportsheader</td>
            <td align="left" class="columnHeaderStatic">
              <input type=radio name="specific_back_col"  value="sportsheader" checked="checked"></td>
          </tr> -->
		 
                      <tr>
                        <td colspan="3" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="web_id" value="<?php	 	 echo $this->_tpl_vars['web_id']; ?>
">
						<input type="hidden" name="t" value="<?php	 	 echo $this->_tpl_vars['t']; ?>
">
						<?php	 	 if ($this->_tpl_vars['web_id'] != NULL): ?>
						<input type="hidden" name="mode" value="edit_data">
						<?php	 	 else: ?>
						 <input type="hidden" name="mode" value="add_data">
						 <?php	 	 endif; ?>
                           <?php	 	 if ($this->_tpl_vars['web_id'] == NULL): ?>  <!--<input name="select-all2" type="image" src="images/submit-button.gif" style="width:60px"> --><input type="submit" value="Submit" name="submit" style="width:100px; color:#FFFFFF; background-color:#0958A6; font-weight:bold;"/>
						   <?php	 	 else: ?><!--<input name="submit21" type="image" src="images/edit-but.gif" style="width:60px"> --><input type="submit" value="Edit" style="width:100px; color:#FFFFFF; background-color:#0958A6; font-weight:bold;"/><?php	 	 endif; ?>

                        </div></td>
                      </tr>
					 
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
          </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
