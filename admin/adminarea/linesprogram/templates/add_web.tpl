{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
<SCRIPT LANGUAGE="JavaScript">
function showColor(val) {
document.web_form.lines_back_col.value = val;
}
function showColor2(val) {
document.web_form.lines_col.value = val;
}
function showColor3(val) {
document.web_form.specific_col.value = val;
}
function showColor4(val) {
document.web_form.specific_back_col.value = val;
}
</script>
{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="50%" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0958A6" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $web_id neq NULL}EDIT{else}ADD {/if}
            WEBSITE</font></strong></td>
          <td align="right" bgcolor="#0958A6" style="padding-right:10px;">
		  <a href="site_view.php" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="web_form" >
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      
                      <tr>
            <td class="fieldname" colspan="2" align="left">
              {$warning}            </td>
          </tr>
		  <tr>
            <td width='401' class="fieldname">
             Website Name           </td>
            <td width="362" class="columnHeaderStatic">
              <input name="web_name" type="text" value="{$web_name}"> </td>
          </tr>
		  <tr>
            <td width='401' class="fieldname">
             Status          </td>
            <td class="columnHeaderStatic"><select name="status">
              <option value="1">Enable</option>
              <option value="0">Disable</option>
            </select></td>
          </tr>
		  <!--<tr>
		    <td class="fieldname">Write Image Path </td>
		    <td class="columnHeaderStatic"><input type="text" name="web_image_path"  value="{$web_image_path}"/></td>
		    </tr> -->
		  <tr>
            <td width='401' class="fieldname">
             Write Website css URL            </td>
            <td class="columnHeaderStatic">
              <input type=text name="web_css"  value="{$web_css}"> </td>
          </tr>
		  <tr>
            <td width='401' class="fieldname"> Hot Lines </td>
            <td align="left" class="columnHeaderStatic">
              <input type=checkbox name="lines_back_col"  value="sportshot" {if $lines_back_col eq "sportshot"} checked="checked" {/if}></td>
          </tr>
		  
		  
								  
								  
	 <!--<tr>
            <td width='401' class="fieldname">Class = sports</td>
            <td align="left" class="columnHeaderStatic">
              <input type=radio name="lines_back_col"  value="sports" checked="checked"></td>
          </tr>
		   -->
								  
		<tr>
            <td width='401' class="fieldname">Hot Specific Sports </td>
            <td align="left" class="columnHeaderStatic">
              <input type=checkbox name="specific_back_col"  value="sportsheaderhot" {if $specific_back_col eq "sportsheaderhot"} checked="checked" {/if} ></td>
          </tr>
		 
								  
								 
		<!--						  <tr>
            <td width='401' class="fieldname">Class = sportsheader</td>
            <td align="left" class="columnHeaderStatic">
              <input type=radio name="specific_back_col"  value="sportsheader" checked="checked"></td>
          </tr> -->
		 
                      <tr>
                        <td colspan="2" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="web_id" value="{$web_id}">
						{if $web_id neq NULL}
						<input type="hidden" name="act" value="edit">
						{else}
						 <input type="hidden" name="act" value="add">
						 {/if}
                           {if $web_id eq NULL}  <!--<input name="select-all2" type="image" src="images/submit-button.gif" style="width:60px"> --><input type="submit" value="Submit" style="width:100px; color:#FFFFFF; background-color:#0958A6; font-weight:bold;"/>
						   {else}<!--<input name="submit21" type="image" src="images/edit-but.gif" style="width:60px"> --><input type="submit" value="Edit" style="width:100px; color:#FFFFFF; background-color:#0958A6; font-weight:bold;"/>{/if}

                        </div></td>
                      </tr>
					 
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
          </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

