{include file="header.tpl"}
{include file="left.tpl"}

<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><? //include "patient_manage_top.php"; ?></td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <!--<tr>
    <td height="30" align="right" bgcolor="#FFFFFF" style="padding-right:10px;"><strong class="bodyb">&#8249; </strong><a href="#" class="one"><b>Previous</b></a> | <a href="#" class="one"><b>Next</b></a> <strong class="one">&#8250;</strong></td>
  </tr> -->
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0958A6" style="padding-left:10px;"><strong><font color="#FFFFFF">{$schedules}</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22" ><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" bgcolor="#E4EBF6"><a href="schedule.php?mode=add&t={$t}" class="one">Add</a></td>
                
				<td height="22" bgcolor="#E4EBF6">Total Records : {$record_count}</td>
				<td height="22" bgcolor="#E4EBF6" align="right">{$paging}</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" rowspan="2" bgcolor="#0958A6"><font style="color:#FFFFFF;"><b>Added On</b></font></td>
			 <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Data</b></font></td>
             <td bgcolor="#0958A6" rowspan="2" class="bodyb"><font style="color:#FFFFFF;"><b>Edit</b></font></td>
            <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Delete</b></font></td>
		 
		  <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>see_preview</b></font></td>
		  </tr>
          <tr align="center">
           
            </tr>
			{section name=web loop=$sched}
          <tr align="center">
            <td height="18" bgcolor="#0958A6"><font style="color:#FFFFFF;">
             {$sched[web].addedon}</font></td>
            <td height="18">{$sched[web].data|truncate:15:"---"}</td>
            <td><a href="schedule.php?web_id={$sched[web].id}&t={$t}&mode=edit" class="seven">Edit</a></td>
            <td><a href="schedule.php?web_id={$sched[web].id}&t={$t}&mode=delete" onClick="return confirm('Are you sure?');" class="seven">Delete</a></td>
			<td><img src="images/see_preview.gif" border="0" onclick="javascript: window.open('newwin.php?web_id={$sched[web].id}','_blank','status=no,toolbar=no,menubar=no,location=no,scrollbar=yes');" style="cursor:pointer; "/></td>
          </tr>
		  {sectionelse}
		  <tr><td height="50" bgcolor="#0958A6" colspan="10" align="center"><font style="color:#FFFFFF;"><strong>
             No Data available</strong></font></td></tr>
			 {/section}
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
{include file="footer.php"}