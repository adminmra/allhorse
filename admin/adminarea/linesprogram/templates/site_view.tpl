{include file="header.tpl"}
{include file="left.tpl"}

<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><? //include "patient_manage_top.php"; ?></td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <!--<tr>
    <td height="30" align="right" bgcolor="#FFFFFF" style="padding-right:10px;"><strong class="bodyb">&#8249; </strong><a href="#" class="one"><b>Previous</b></a> | <a href="#" class="one"><b>Next</b></a> <strong class="one">&#8250;</strong></td>
  </tr> -->
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0958A6" style="padding-left:10px;"><strong><font color="#FFFFFF">WEBSITES</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="20%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" bgcolor="#E4EBF6"><a href="add_web.php" class="one">Add</a></td>
                
				<td height="22" bgcolor="#E4EBF6">Total Records : {$record_count}</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" rowspan="2" bgcolor="#0958A6"><font style="color:#FFFFFF;"><b> Website Name</b></font></td>
			 <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Created On</b></font></td>
            <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Status</b></font></td>
            <td bgcolor="#0958A6" rowspan="2" class="bodyb"><font style="color:#FFFFFF;"><b>Edit</b></font></td>
            <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Delete</b></font></td>
		 
		  <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>see_preview</b></font></td>
		 <!--  <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Genarate File</b></font></td> -->
		  </tr>
          <tr align="center">
           
            </tr>
			{section name=web loop=$web_id}
          <tr align="center">
            <td height="18" bgcolor="#0958A6"><font style="color:#FFFFFF;">
             {$web_name[web]}</font></td>
            <td height="18">{$create[web]}</td>
            <td height="18">{if $status[web] eq 1}<img src="images/active.gif" />{else}<img src="images/deactive.gif" />{/if}</td>
            <td><a href="add_web.php?web_id={$web_id[web]}" class="seven">Edit</a></td>
            <td><a href="?web_id={$web_id[web]}&act=delete" onClick="return confirm('Are you sure?');" class="seven">Delete</a></td>
			<td><a href="check.php?web_id={$web_id[web]}" class="seven" target="_blank"><img src="images/see_preview.gif" border="0" /></a></td>
          <!-- <td><a href="genfile.php?web_id={$web_id[web]}" class="seven" target="_blank">Genarate File</a></td> -->

		  </tr>
		  {sectionelse}
		  <tr><td height="50" bgcolor="#0958A6" colspan="10" align="center"><font style="color:#FFFFFF;"><strong>
             No Website available</strong></font></td></tr>
			 {/section}
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
{include file="footer.php"}