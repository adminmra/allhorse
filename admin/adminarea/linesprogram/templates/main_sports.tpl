{include file="header.tpl"}
{include file="left.tpl"}

<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><? //include "patient_manage_top.php"; ?></td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <!--<td height="30" align="right" bgcolor="#FFFFFF" style="padding-right:10px;"><strong class="bodyb">&#8249; </strong><a href="#" class="one"><b>Previous</b></a> | <a href="#" class="one"><b>Next</b></a> <strong class="one">&#8250;</strong></td> -->
	<td height="30" width="100%" align="right" bgcolor="#FFFFFF" style="padding-left:600px;"><strong >{$paging}</strong></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0958A6" style="padding-left:10px;"><strong><font color="#FFFFFF">MAIN SPORTS DETAIL</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="20%" border="0" cellpadding="0" cellspacing="0" bgcolor="">
          <tr>
            <td height="22"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" bgcolor="#E4EBF6"><a href="add_mainsports.php" class="one">Add</a></td>
                
				<td height="22" bgcolor="#E4EBF6">Total Records : {$record_count}</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" rowspan="2" bgcolor="#0958A6"><font style="color:#FFFFFF;"><b>Main Sports Name</b></font></td>
			<td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Website name</b></font></td>
			 <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Column No</b></font></td>
             <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Position in Column  </b></font></td>
			 <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Status </b></font></td>
			 
			 <!--<td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Specific Sports</b></font></td>-->
			 <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>All Specific Sports</b></font></td>
			  <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Lines</b></font></td>
            <td bgcolor="#0958A6" rowspan="2" class="bodyb"><font style="color:#FFFFFF;"><b>Edit</b></font></td>
            <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Delete</b></font></td>
		  </tr>
          <tr align="center">
           
            </tr>
			{section name=sports loop=$sports_id}
          <tr align="center">
            <td height="18" bgcolor="#0958A6"><font style="color:#FFFFFF;"><strong>
             {$main_sports_name[sports]}</strong></font></td>
            <td height="18">{if $website_name[sports] neq NULL}{$website_name[sports]}{else}All websites{/if}</td>
			<td height="18">{$column_no[sports]}</td>
            <td height="18">{$arrange[sports]}</td>
			<td height="18">{if $status[sports] eq 1}<img src="images/active.gif" />{else}<img src="images/deactive.gif" />{/if}</td>
			<!--<td height="18" bgcolor="#B5DE97"><select name="specific_sports" style="width:120;">
			{section name=specific loop=$specific_id[sports]}
			<option value="">{$specific_sports[sports][specific]}</option>
			{/section}
			</select></td>-->
			<td height="18"><a href="specific_sports_view.php?main_id={$sports_id[sports]}" class="seven">Specific sports</a></td>
			<td height="18"><a href="lines_view.php?main_id={$sports_id[sports]}&check=main" class="seven">See the lines directly exists under {$main_sports_name[sports]}</a></td>
            <td><a href="add_mainsports.php?sports_id={$sports_id[sports]}" class="seven">Edit</a></td>
            <td><a href="?sports_id={$sports_id[sports]}&act=delete" onClick="return confirm('Are you sure?');" class="seven">Delete</a></td>
          </tr>
		  {sectionelse}
		  <tr><td height="50" bgcolor="#0958A6" colspan="10" align="center"><font style="color:#FFFFFF;"><strong>
             No main sports available</strong></font></td></tr>
			 {/section}
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
{include file="footer.php"}