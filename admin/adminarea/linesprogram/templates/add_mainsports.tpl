{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
<SCRIPT LANGUAGE="JavaScript">
function showColor(val) {
document.mainsports_form.hexval.value = val;
}
function showColor2(val) {
document.mainsports_form.textcolor.value = val;
}
</script>
{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0958A6" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $mainsports_id neq NULL}EDIT{else}ADD {/if}
            MAIN SPORTS</font></strong></td>
          <td align="right" bgcolor="#0958A6" style="padding-right:10px;">
	<!-- 	  <a href="main_sports.php" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>
	 -->	  <a href="index.php" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="mainsports_form" >
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      
                      <tr>
            <td class="fieldname" colspan="5" align="left">
              {if $warning neq NULL}<img src="images/Ball_2.gif" />{$warning}{/if}           </td>
          </tr>
		  <tr>
            <td class="fieldname" colspan="5" align="left"><font color="#990000">
              *  If you will not choose the website name this main sports type will be applicable to all of you sites<br>
			  ** If you will not upload graphics only main sports name will appear with text and background color<br> 
			  </font>			  </td>
          </tr>
		  <tr>
            <td class="fieldname" colspan="5" align="left">          </tr>
		 {if $mainsports_img neq NULL} 
		 <tr>
		  <td width='181' class="fieldname" align="left">
		  Main Sports Image		  </td>
            <td colspan="5" align="left" class="fieldname">
			 <!--<img src="uploaded_images/main_sports_images/thumb{$mainsports_img}">	 -->
             <img src="uploaded_images/main_sports_images/{$mainsports_img}" height="52"></td>
          </tr> {/if}
		  <tr>
            <td width='181' class="fieldname">
             Mainsports Name           </td>
            <td width="219" class="columnHeaderStatic">
              <input name="mainsports_name" type="text" id="affiliateFirstName" value="{$mainsports_name}" style="width:150px;"> </td>
            <td width="23" class="columnHeaderStatic">&nbsp;</td>
            <td width="116" class="fieldname">Hot</td>
            <td width="292" class="columnHeaderStatic"><input type=checkbox name="hexval"  value="sportshot" {if $color eq "sportshot"} checked="checked"{/if} ></td>
		  </tr>
		 
		  
		  <tr>
            <td width='181' class="fieldname">
             Column Number           </td>
            <td class="columnHeaderStatic">
              <input name="column_no" type="text" id="affiliateFirstName" value="{$column}" style="width:150px;">  </td>
            <td class="columnHeaderStatic">&nbsp;</td>
            <td class="fieldname">Start Date</td>
            <td class="columnHeaderStatic"><input name="start_date" type="text" value="{$start_date}" style="width:150px;"/><script language="javascript" src="js/cal.js"></script>
							<a onClick='popUpCalendar(this,document.mainsports_form.start_date,"yyyy-mm-dd")' border="0" ><img src="images/cal.gif"></a></td>
		  </tr>
		  <tr>
            <td width='181' class="fieldname">
             Position in column           </td>
            <td class="columnHeaderStatic">
              <input name="arrange" type="text" id="affiliateFirstName" value="{$arrange}" style="width:150px;"><!-- &nbsp Recommended <font color="#990000">{$recomend+1}</font>--></td>
            <td class="columnHeaderStatic">&nbsp;</td>
            <td class="fieldname">End Date</td>
            <td class="columnHeaderStatic"><input name="stop_date" type="text" value="{$stop_date}" style="width:150px;"/><script language="javascript" src="js/cal.js"></script>
							<a onClick='popUpCalendar(this,document.mainsports_form.stop_date,"yyyy-mm-dd")' border="0" ><img src="images/cal.gif"></a></td>
		  </tr>
		  <tr>
            <td width='181' class="fieldname">
             Status           </td>
            <td class="columnHeaderStatic" >
              <select name="status" style="width:150px;">
			  <option value="1" {if $status eq 1} selected="selected"{/if}>Enable</option>
			  <option value="0" {if $status eq 0} selected="selected"{/if}>Disable</option>
			  </select> </td>
            <td class="columnHeaderStatic" >&nbsp;</td>
            <td class="fieldname" ><!-- Add Anchor --></td>
            <td class="columnHeaderStatic" ><!-- <input name="add_anchor" type="text"  value="{$add_anchor}" style="width:150px;"> --></td>
		  </tr>
		  <!-- <tr>
            <td width='181' class="fieldname">
             Upload Image           </td>
            <td class="columnHeaderStatic">
			<input type="hidden" name="MAX_FILE_SIZE" value="2000000">
              <input name="mainsports_img" type="file" id="affiliateFirstName" value="{$mainsports_img}" style="width:150px;"> </td>
            <td class="columnHeaderStatic">&nbsp;</td>
            <td class="fieldname"> Website Name</td>
            <td class="columnHeaderStatic">{section name=web loop=$web_id}<input type="checkbox" name="webs[]" checked="checked"value="{$web_id[web]}" style="width:20px; height:20px;" />&nbsp;{$web_name[web]}<br />{/section}</td>
		  </tr> -->
		  <!--<tr>
            <td width='114' class="fieldname">Hot</td>
            <td colspan="4" class="columnHeaderStatic">
			<input type=checkbox name="hexval"  value="sportshot" {if $color eq "sportshot"} checked="checked"{/if} ></td>
          </tr>
		  <tr>
            <td width='114' class="fieldname">Start Date</td>
            <td colspan="4" class="columnHeaderStatic"><input name="start_date" type="text" value="{$start_date}" style="width:150px;"/><script language="javascript" src="js/cal.js"></script>
							<a onClick='popUpCalendar(this,document.mainsports_form.start_date,"yyyy-mm-dd")' border="0" ><img src="images/cal.gif"></a></td>
          </tr>
		  <tr>
            <td width='114' class="fieldname">End Date</td>
            <td colspan="4" class="columnHeaderStatic"><input name="stop_date" type="text" value="{$stop_date}" style="width:150px;"/><script language="javascript" src="js/cal.js"></script>
							<a onClick='popUpCalendar(this,document.mainsports_form.stop_date,"yyyy-mm-dd")' border="0" ><img src="images/cal.gif"></a></td>
          </tr>
		   <tr>
            <td width='114' class="fieldname">Add Anchor</td>
            <td colspan="4" class="columnHeaderStatic"><input name="add_anchor" type="text"  value="{$add_anchor}" style="width:150px;"></td>
          </tr>
		  <tr>
            <td width='114' class="fieldname">
             Website Name           </td>
            <td colspan="4" class="columnHeaderStatic">
              {section name=web loop=$web_id}<input type="checkbox" name="webs[]" checked="checked" style="width:20px; height:20px;" />&nbsp;{$web_name[web]}<br />{/section}</td>
          </tr> -->
		   <!-- <tr><td colspan="5" align="center" class="fieldname">Or</td></tr> -->
		{* <tr>
            <td width='181' class="fieldname">
             Add HTML           </td>
            <td colspan="4" class="columnHeaderStatic">
              {$variable} </td>
          </tr> *}  
								  <!------------------>
								  <!------------------>
                      <tr>
                        <td colspan="5" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="column" value="{$columnrecomend}" />
						<input type="hidden" name="mainsports_id" value="{$mainsports_id}">
						<input type="hidden" name="refer_check" value="{$main_refer}">
						{if $mainsports_id neq NULL}
						<input type="hidden" name="act" value="edit">
						<input type="hidden" name="image_del" value="{$mainsports_img}">
						<input type="hidden" name="main_sportsid" value="{$mainsports_id}">
						{else}
						 <input type="hidden" name="act" value="add">
						 {/if}
                           {if $mainsports_id eq NULL}  <!--<input name="select-all2" type="image" src="images/submit-button.gif" style="width:60px"> --><input type="submit" value="Submit" style="width:100px; color:#FFFFFF; background-color:#0958A6; font-weight:bold;"/>
						   {else}<!--<input name="submit21" type="image" src="images/edit-but.gif" style="width:60px"> --><input type="submit" value="Edit" style="width:100px; color:#FFFFFF; background-color:#0958A6; font-weight:bold;"/>{/if}

                        </div></td>
                      </tr>
					 
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
          </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

