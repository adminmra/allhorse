{include file="header.tpl"}
{include file="left.tpl"}

<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><? //include "patient_manage_top.php"; ?></td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <!--<td height="30" align="right" bgcolor="#FFFFFF" style="padding-right:10px;"><strong class="bodyb">&#8249; </strong><a href="#" class="one"><b>Previous</b></a> | <a href="#" class="one"><b>Next</b></a> <strong class="one">&#8250;</strong></td> -->
	 
    <td height="30" width="100%" align="right" bgcolor="#FFFFFF" style="padding-left:600px;"><strong >{$paging}</strong></td>
  
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0958A6" style="padding-left:10px;"><strong><font color="#FFFFFF">LINES</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="35%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr align="center">
			  {if $check neq main}
                <td height="22" bgcolor="#E4EBF6"><a href="add_lines.php?main_id={$main_id}&sp_id={$sp_id}" class="one">Add</a></td>
			  {/if}
				<td height="22" bgcolor="#E4EBF6"><a href="javascript:history.back();" class="one">Back</a></td>
                
				<td height="22" bgcolor="#E4EBF6">Total Records : {$record_count}</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" rowspan="2" bgcolor="#0958A6"><font style="color:#FFFFFF;"><b>Lines Name</b></font></td>
			<td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>{if $main_id neq NULL}Main Sports Name{else}Specific Sports name{/if}</b></font></td>
			 <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Position</b></font></td>
			 <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Modify</b></font></td> 
            <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Status</b></font></td>
            <td bgcolor="#0958A6" rowspan="2" class="bodyb"><font style="color:#FFFFFF;"><b>Edit</b></font></td>
            <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Delete</b></font></td>
		  </tr>
          <tr align="center">
           
            </tr>
			{section name=lines loop=$lines_id}
			<form name="update_form" method="post" action="?act=update">
          <tr align="center">
            <td height="18" bgcolor="#0958A6"><font style="color:#FFFFFF;">
             <input type="text" name="update_lines[]" value="{$lines_name[lines]}" style="width:150px;" /></font></td>
            <td height="18" >{if $specific_name[lines] neq NULL}{$specific_name[lines]}{elseif $main_name[lines] neq NULL}{$main_name[lines]}{/if}</td>
			<td height="18"><input type="text" name="arrange[]" value="{$arrange[lines]}" style="width:25px;" /></td>
			<td height="18"><input type="submit" value="Change" name="submit[]" style="width:80px; color:#FFFFFF; background-color:#0958A6; font-weight:bold;"/></td><input type="hidden" name="update_id[]" value="{$lines_id[lines]}" />
			<input type="hidden" name="main_id" value="{$main_id}" />
			<input type="hidden" name="update_sp_id" value="{$sp_id}" />
            <td height="18">{if $status[lines] eq 1}<img src="images/active.gif" />{else}<img src="images/deactive.gif" />{/if}</td>
            <td ><a href="add_lines.php?lines_id={$lines_id[lines]}&sp_id={$sp_id}&main_id={$main_id}&check=main" class="seven">Edit</a></td>
            <td><a href="?lines_id={$lines_id[lines]}&act=delete&sp_id={$sp_id}&main_id={$main_id}" onClick="return confirm('Are you sure?');" class="seven">Delete</a></td>
          </tr>
		  </form>
		  {sectionelse}
		  <tr><td height="50" bgcolor="#0958A6" colspan="10" align="center"><font style="color:#FFFFFF;"><strong>
             No lines available</strong></font></td></tr>
			 {/section}
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
{include file="footer.php"}