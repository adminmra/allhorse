{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
<SCRIPT LANGUAGE="JavaScript">
function showColor(val) {
document.specificsports_form.hexval.value = val;
}
function showColor2(val) {
document.specificsports_form.textcolor.value = val;
}
</script>
{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0958A6" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $specificsports_id neq NULL}EDIT{else}ADD {/if}
            SPECIFIC SPORTS</font></strong></td>
          <td align="right" bgcolor="#0958A6" style="padding-right:10px;">
		  <a href="specific_sports_view.php?main_id={$main_id}" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="specificsports_form" >
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      
                      <tr>
            <td class="fieldname" colspan="6" align="left">
              {if $warning neq NULL}<img src="images/Ball_2.gif" />{$warning}{/if}           </td>
          </tr>
		  <tr>
            <td class="fieldname" colspan="6" align="left"><font color="#990000">
              
			  * If you will not upload graphics only specific sports name will appear with text and background color<br> 
			  </font>			  </td>
          </tr>
		  <tr>
            <td class="fieldname" colspan="6" align="left">          </tr>
		  {if $specificsports_img neq NULL}
		  <tr>
		  <td colspan="2" align="left" class="fieldname">
		  Specificsports Image		  </td>
            <td colspan="5" align="left" class="fieldname">
			
             <img src="uploaded_images/specific_sports_images/{$specificsports_img}">			 			 </td>
          </tr>
		  {/if}
		  <tr>
            <td colspan="2" class="fieldname">
             Specificsports Name           </td>
            <td width="220" class="columnHeaderStatic">
              <input name="specificsports_name" type="text" id="affiliateFirstName" value="{$specificsports_name}" style="width:150px;"> </td>
            <td width="14" class="fieldname">&nbsp;</td>
            <td width="116" class="fieldname">Hot</td>
            <td width="186" class="fieldname"> <input type=checkbox name="hexval"  value="sportsheaderhot" {if $color eq "sportsheaderhot"} checked="checked"{/if} ></td>
		  </tr>
		  
		  <tr>
            <td colspan="2" class="fieldname">
             Position in main sports           </td>
            <td class="columnHeaderStatic">
              <input name="arrange" type="text" id="affiliateFirstName" value="{$arrange}" style="width:150px;"> </td>
            <td class="fieldname">&nbsp;</td>
            <td class="fieldname">{*Select Websites*}</td>
            <td class="fieldname">{*{section name=web loop=$web_id}<input type="checkbox" name="webs[]" checked="checked" style="width:20px; height:20px;" value="{$web_id[web]}" />{$web_name[web]}<br />{/section}*}</td>
		  </tr>
		  <tr>
            <td colspan="2" class="fieldname">
             Status           </td>
            <td class="columnHeaderStatic">
              <select name="status" style="width:150px;">
			  <option value="1" {if $status eq 1} selected="selected"{/if}>Enable</option>
			  <option value="0" {if $status eq 0} selected="selected"{/if}>Disable</option>
			  </select> </td>
            <td class="columnHeaderStatic">&nbsp;</td>
            <td class="columnHeaderStatic"><span class="fieldname">Start Date</span></td>
			
            <td class="columnHeaderStatic"><input name="start_date" type="text" value="{$start_date}" style="width:150px;"/><script language="javascript" src="js/cal.js"></script>
							<a onClick='popUpCalendar(this,document.specificsports_form.start_date,"yyyy-mm-dd")' border="0" ><img src="images/cal.gif"></a></td>
		  </tr>
		  <tr>
            <td colspan="2" class="fieldname">
             <!--Upload Image -->  Add Anchor</td>
            <td class="columnHeaderStatic">
              <!--<input name="specificsports_img" type="file" id="affiliateFirstName" value="{$specificsports_img}" style="width:150px;">  --><input name="add_anchor" type="text"  value="{$add_anchor}" style="width:150px;"></td>
            <td class="columnHeaderStatic">&nbsp;</td>
            <td class="fieldname">End Date</td>
            <td class="columnHeaderStatic"><input name="stop_date" type="text" value="{$stop_date}" style="width:150px;"/><script language="javascript" src="js/cal.js"></script>
							<a onClick='popUpCalendar(this,document.specificsports_form.stop_date,"yyyy-mm-dd")' border="0" ><img src="images/cal.gif"></a></td>
		  </tr>
		  <!-- <tr>
            <td colspan="2" class="fieldname">Add Anchor</td>
            <td class="columnHeaderStatic">
              <input name="add_anchor" type="text"  value="{$add_anchor}" style="width:150px;"> </td>
            <td class="columnHeaderStatic">&nbsp;</td>
            <td class="columnHeaderStatic">&nbsp;</td>
            <td class="columnHeaderStatic">&nbsp;</td>
		   </tr> -->
		  <!--<tr>
            <td colspan="2" class="fieldname">Start Date </td>
            <td colspan="4" class="columnHeaderStatic"><input name="start_date" type="text" value="{$start_date}" style="width:150px;"/><script language="javascript" src="js/cal.js"></script>
							<a onClick='popUpCalendar(this,document.specificsports_form.start_date,"yyyy-mm-dd")' border="0" ><img src="images/cal.gif"></a></td>
          </tr>
		  <tr>
            <td colspan="2" class="fieldname">End Date</td>
            <td colspan="4" class="columnHeaderStatic"><input name="stop_date" type="text" value="{$stop_date}" style="width:150px;"/><script language="javascript" src="js/cal.js"></script>
							<a onClick='popUpCalendar(this,document.specificsports_form.stop_date,"yyyy-mm-dd")' border="0" ><img src="images/cal.gif"></a></td>
          </tr> -->
		  	  
                      <tr>
                        <td colspan="6" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="specificsports_id" value="{$specificsports_id}">
						{if $specificsports_id neq NULL}
						<input type="hidden" name="act" value="edit">
						<input type="hidden" name="image_del" value="{$specificsports_img}">
						<input type="hidden" name="specific_sportsid" value="{$specificsports_id}">
						<input type="hidden" name="refer_check" value="{$main_refer}">
						{else}
						 <input type="hidden" name="act" value="add">
						 {/if}
                           {if $specificsports_id eq NULL}  <!--<input name="select-all2" type="image" src="images/submit-button.gif" style="width:60px"> -->
						   <input type="submit" value="Submit" style="width:100px; color:#FFFFFF; background-color:#0958A6; font-weight:bold;"/>
						   {else}<!--<input name="submit21" type="image" src="images/edit-but.gif" style="width:60px"> --><input type="submit" value="Edit" style="width:100px; color:#FFFFFF; background-color:#0958A6; font-weight:bold;"/>{/if}

                        </div></td>
                      </tr>
					 
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
          </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

