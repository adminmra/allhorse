{include file="header.tpl"}
{include file="left.tpl"}

<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><? //include "patient_manage_top.php"; ?></td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <!--<td height="30" align="right" bgcolor="#FFFFFF" style="padding-right:10px;"><strong class="bodyb">&#8249; </strong><a href="#" class="one"><b>Previous</b></a> | <a href="#" class="one"><b>Next</b></a> <strong class="one">&#8250;</strong></td> -->
	<td height="30" width="100%" align="right" bgcolor="#FFFFFF" style="padding-left:600px;"><strong >{$paging}</strong></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0958A6" style="padding-left:10px;"><strong><font color="#FFFFFF">SPECIFIC SPORTS</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="35%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" bgcolor="#E4EBF6"><a href="add_specificsports.php?main_id={$main_id}" class="one">Add</a></td>
                <td height="22" bgcolor="#E4EBF6"><a href="before_main.php" class="one">Back</a></td>
				<td height="22" bgcolor="#E4EBF6">Total Records : {$record_count}</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" rowspan="2" bgcolor="#0958A6"><font style="color:#FFFFFF;"><b>specific Sports Name</b></font></td>
			<td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Main Sports name</b></font></td>
			 <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Position</b></font></td>
			<td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Modify</b></font></td> 
            <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Status</b></font></td>
			<td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>See Lines</b></font></td>
            <td bgcolor="#0958A6" rowspan="2" class="bodyb"><font style="color:#FFFFFF;"><b>Edit</b></font></td>
            <td height="20" bgcolor="#0958A6" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Delete</b></font></td>
		  </tr>
          <tr align="center">
           
            </tr>
			{section name=sports loop=$sports_id}
			<form name="specific_sports[]" method="post" action="?act=update">
          <tr align="center">
            <td height="18"><font style="color:#FFFFFF;">
             <input type="text" name="specific_sp[]" value="{$specific_sports_name[sports]}" style="width:150px;" /></font></td>
            <td height="18">{if $mainsports_name[sports] neq NULL}{$mainsports_name[sports]}{else}All Main Sports{/if}</td>
			<td height="18"><input type="text" name="arrange[]" value="{$position[sports]}" style="width:50px;" /></td>
			<td height="18"><input type="submit" value="Change" name="submit[]" style="width:80px; color:#FFFFFF; background-color:#0958A6; font-weight:bold;"/></td>
            <td height="18">{if $status[sports] eq 1}<img src="images/active.gif" />{else}<img src="images/deactive.gif" />{/if}</td>
			<td height="18"><a href="lines_view.php?sp_id={$sports_id[sports]}&main_id={$main_id}" class="seven">See Lines</a></td>
            <td ><a href="add_specificsports.php?sports_id={$sports_id[sports]}&main_id={$main_id}" class="seven">Edit</a></td>
            <td ><a href="?sports_id={$sports_id[sports]}&act=delete&main_id={$main_id}" onClick="return confirm('Are you sure?');" class="seven">Delete</a></td>
          </tr><input type="hidden" name="update_id[]" value="{$sports_id[sports]}" />
		  <input type="hidden" name="main_id2" value="{$main_id}" />
		  
		   </form>
		  {sectionelse}
		  <tr><td height="50" bgcolor="#0958A6" colspan="10" align="center"><font style="color:#FFFFFF;"><strong>
             No specific sports available</strong></font></td></tr>
			 
			
			 {/section}
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
{include file="footer.php"}