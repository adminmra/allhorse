var tsaveState = 1;

var tpressedFontColor = "#AA0000";

var tpathPrefix_img = "js/img/";

var tlevelDX = 10;
var ttoggleMode = 1;

var texpanded = 0;
var tcloseExpanded   = 0;
var tcloseExpandedXP = 0;

var tblankImage      = "js/img/blank.gif";
var tmenuWidth       = 200;
var tmenuHeight      = 0;

var tabsolute        = 1;
var tleft            = 5;
var ttop             = 50;

var tfloatable       = 0;
var tfloatIterations = 10;

var tmoveable        = 0;
var tmoveImage       = "js/img/movepic.gif";
var tmoveImageHeight = 12;

var tfontStyle       = "normal 8pt Tahoma";
var tfontColor       = ["#215DC6","#428EFF"];
var tfontDecoration  = ["none","underline"];

var titemBackColor   = ["#D6DFF7","#D6DFF7"];
var titemAlign       = "left";
var titemBackImage   = ["",""];
var titemCursor      = "pointer";
var titemHeight      = 22;
var titemTarget      = "";

var ticonWidth       = 21;
var ticonHeight      = 15;
var ticonAlign       = "left";

var tmenuBackImage   = "";
var tmenuBackColor   = "";
var tmenuBorderColor = "#FFFFFF";
var tmenuBorderStyle = "solid";
var tmenuBorderWidth = 0;

var texpandBtn       =["expandbtn2.gif","expandbtn2.gif","collapsebtn2.gif"];
var texpandBtnW      = 9;
var texpandBtnH      = 9;
var texpandBtnAlign  = "left"

var tpoints       = 0;
var tpointsImage  = "";
var tpointsVImage = "";
var tpointsCImage = "";

// XP-Style Parameters
var tXPStyle = 1;
var tXPIterations = 10;                  // expand/collapse speed
var tXPTitleBackColor    = "#265BCC";
var tXPExpandBtn    = ["xpexpand1.gif","xpexpand2.gif","xpcollapse1.gif","xpcollapse2.gif"];
var tXPTitleBackImg = "xptitle.gif";

var tXPTitleLeft      = "xptitleleft.gif";
var tXPTitleLeftWidth = 4;

var tXPBtnWidth  = 25;
var tXPBtnHeight = 25;

var tXPIconWidth  = 31;
var tXPIconHeight = 32;

var tXPFilter=1;

var tXPBorderWidth = 1;
var tXPBorderColor = '#FFFFFF';



var tstyles =
[
    ["tfontStyle=bold 8pt Tahoma","titemBackColor=#265BCC,#265BCC","tfontColor=#FFFFFF,#428EFF", "tfontDecoration=none,none"],
    ["tfontStyle=bold 8pt Tahoma","titemBackColor=#265BCC,#265BCC","tfontColor=#215DC6,#428EFF", "tfontDecoration=none,none"],
    ["tfontDecoration=none,none"],
    ["tfontStyle=bold 8pt Tahoma","tfontColor=#444444,#5555FF"],
];

var tXPStyles =
[
    ["tXPTitleBackColor=#D0DAF8", "tXPExpandBtn=xpexpand3.gif,xpexpand4.gif,xpcollapse3.gif,xpcollapse4.gif", "tXPTitleBackImg=xptitle2.gif"]
];

var tmenuItems =
[
    ["+Admin Control Arena", "", "xpicon1.gif","","", "Admin Control Arena","","0"],
		["|Home", "home.php", "icon1.gif", "icon1o.gif", "", "Home"],
		["|Logout", "logout.php", "icon1.gif", "icon1o.gif", "", "Logout"],
	
	["Lines Management", "", "","","", "Lines Management",,"1","0"],
		["|Website Management Area", "site_view.php", "icon1.gif", "icon1o.gif", "", "Manage website"],
	
		["|Main Management Area", "before_main.php", "icon1.gif", "icon1o.gif", "", "Manage Main Sports"],
		
		["|Advanced option", "main_sports.php", "icon1.gif", "icon1o.gif", "", "Advanced option"],
				
		["|In-Running Schedule", "schedule.php?web_id=1&t=1&mode=edit", "icon1.gif", "icon1o.gif", "", "In-Running Schedule"],
	
		["|NASCAR Schedule", "schedule.php?web_id=3&t=2&mode=edit", "icon1.gif", "icon1o.gif", "", "NASCAR Schedule"],

		["|Horse Racing Schedule", "horse_schedule.php?t=3", "icon1.gif", "icon1o.gif", "", "Horse Racing Schedule"],
		
		["|Weekly Stakes Schedule", "horse_schedule.php?t=4", "icon1.gif", "icon1o.gif", "", "Weekly Stakes Schedule"],

		
	
	
		/*["|Specific Sports management", "specific_sports_view.php", "icon1.gif", "icon1o.gif", "", "Manage Specific Sports"],
	
		
	*/
		
	
			

];

apy_tmenuInit();
