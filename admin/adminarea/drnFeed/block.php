<?php	 		 	
//include_once "/home/allhorse/admin/adminarea/dbconnect_rw.inc";
  function ShortenText($text) {
        // Change to the number of characters you want to display
        $chars = 40;

        $text = $text." ";
        $text = substr($text,0,$chars);
        $text = substr($text,0,strrpos($text,' '));
        $text = $text."...";

        return $text;
    }


					   $sql_schedule = "select * from schedule where type = '4' and display = 'Y' AND addedon >= '".date('Y-m-d')."' order by addedon ";
					   $res_schedule=mysql_query($sql_schedule);
					   $counter = 0;
?>


<div class="contentPanel" style="margin:13px 0 0; width:372px;"><!-- start:block -->
<div class="strip"></div>
<div class="padded leaders">

<div class="tabs">
	<ul class="tabNavigation">
	<li><a href="#first"><h2>Graded Stakes Races</h2></a></li>
	<li><a href="#second"><h2>Top Leaders</h2></a></li>
	</ul>

<div id="first"><!-- first tab contents -->
<div id="graded">
<div id="graded-content">
<?php	 	
 while($data_schedule = mysql_fetch_object($res_schedule))
					   {
						if($counter%2 == 0)
						{
						 $ClassOddEven="even";
						}
						else
						{
						 $ClassOddEven="odd";
						}
?>
<div class="<?php	 	 echo $ClassOddEven;?>" id="grade-stake-<?php	 	 echo $counter;?>">
<div class="graded-date"><span class="bold"><?php	 	 $scdate = explode(" ",$data_schedule->addedon); $fscdate =explode("-",$scdate[0]); echo date('M',mktime(0,0,0,$fscdate[1],$fscdate[2],$fscdate[0]))." ".date('d',mktime(0,0,0,$fscdate[1],$fscdate[2],$fscdate[0])); ?></span></div>
<div class="graded-race">
<?php	 	 
						 $datamain = explode("*",$data_schedule->data);
						 for($j=0;$j<=count($datamain)-1;$j++)
						 {
						 	if(strlen(trim($datamain[$j])) > 0)
							{
								$Finaldata=explode(":",trim(stripslashes($datamain[$j])));
							echo "<p><span class=\"bold\">".$Finaldata[0].":</span>".ShortenText($Finaldata[1])."</p>"; 
							}
						 }
						  ?>
</div>
</div>
<?php	 	
$counter++;
}
?>

</div><!-- end: graded-content -->
<!--   <span class="more" style="display:block; margin-top:20px;">&#187; <a href="/results/">More Graded Stakes Races</a></span> 
 --></div><!-- end: graded -->	

</div><!-- end: first -->
            
<div id="second"><!-- second tab contents -->
<div class="leaderboard-mini">
<h3>Horses</h3>
<?php	 	
$sql_top= "SELECT * FROM top_leader where type = '1' ORDER BY position limit 0 , 1";
$result_top=mysql_query($sql_top);
$data_top=mysql_fetch_object($result_top);
?>
<table cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<th class="left" width="35%">Name</th>
<th>Starts</th>
<th>1sts</th>
<th>2nds</th>
<th>3rds</th>
<th class="right">Purses</th>
</tr>
<tr>
<td class="left"><span class="num">1.</span> <span class="name"><?php	 	 echo stripslashes($data_top->name); ?></span></td>
<td><?php	 	 echo $data_top->starts; ?></td>
<td><?php	 	 echo $data_top->first; ?></td>
<td><?php	 	 echo $data_top->second; ?></td>
<td><?php	 	 echo $data_top->third; ?></td>
<td class="right"><?php	 	 echo $data_top->purse; ?></td>
</tr>
</tbody>
</table>
<?php	 	
$sql_top= "SELECT * FROM top_leader where type = '2' ORDER BY position limit 0 , 1";
$result_top=mysql_query($sql_top);
$data_top=mysql_fetch_object($result_top);
?>
<h3>Jockeys</h3>
<table cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<th class="left" width="35%">Name</th>
<th>Starts</th>
<th>1sts</th>
<th>2nds</th>
<th>3rds</th>
<th class="right">Purses</th>
</tr>
<tr>
<td class="left"><span class="num">1.</span> <span class="name"><?php	 	 echo stripslashes($data_top->name); ?></span></td>
<td><?php	 	 echo $data_top->starts; ?></td>
<td><?php	 	 echo $data_top->first; ?></td>
<td><?php	 	 echo $data_top->second; ?></td>
<td><?php	 	 echo $data_top->third; ?></td>
<td class="right"><?php	 	 echo $data_top->purse; ?></td>
</tr>
</tbody>
</table>
<?php	 	
$sql_top= "SELECT * FROM top_leader where type = '3' ORDER BY position limit 0 , 1";
$result_top=mysql_query($sql_top);
$data_top=mysql_fetch_object($result_top);
?>

<h3>Trainers</h3>
<table cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<th class="left" width="35%">Name</th>
<th>Starts</th>
<th>1sts</th>
<th>2nds</th>
<th>3rds</th>
<th class="right">Purses</th>
</tr>
<tr>
<td class="left"><span class="num">1.</span> <span class="name"><?php	 	 echo stripslashes($data_top->name); ?></span></td>
<td><?php	 	 echo $data_top->starts; ?></td>
<td><?php	 	 echo $data_top->first; ?></td>
<td><?php	 	 echo $data_top->second; ?></td>
<td><?php	 	 echo $data_top->third; ?></td>
<td class="right"><?php	 	 echo $data_top->purse; ?></td>
</tr>
</tbody>
</table>
</div><!-- end: leaderboard-mini -->
<span class="more" style="display:block; margin-top:20px;">&#187; <a href="/leaderboard">Top Ten Leaders</a></span> 
</div><!-- end: second -->

</div><!-- end: Tabs -->
<script type="text/javascript" charset="utf-8">
$(function () {
var tabContainers = $('div.tabs > div');
tabContainers.hide().filter(':first').show();
			
$('div.tabs ul.tabNavigation a').click(function () {
	tabContainers.hide();
	tabContainers.filter(this.hash).show();
	$('div.tabs ul.tabNavigation a').removeClass('selected');
	$(this).addClass('selected');
	return false;
	}).filter(':first').click();
});
</script>

</div><!-- end: padded -->
</div><!-- end: contentPanel -->
<!-- end: BLOCK -->
