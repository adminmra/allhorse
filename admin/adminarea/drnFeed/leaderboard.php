<?php	 		 	 //include_once "/home/allhorse/admin/adminarea/dbconnect_rw.inc";
$sql_top= " SELECT * FROM top_leader where type = '1' ORDER BY position limit 0,1 ";
$result_top=mysql_query($sql_top);
$data_top=mysql_fetch_object($result_top);
$updatedate=explode("-",$data_top->updatedas);
$updateas=date("F jS",mktime(0,0,0,$updatedate[1],$updatedate[2],$updatedate[0]));

?>
<div class="leaderboard">
<div style="float: left; width: 100%;">
<h2 style="float: left;">Leaderboard 2011</h2>
<span style="float: right; padding-right: 10px;"><a href="/leaderboard/leaders-2009">2009</a> | <a href="/leaderboard/leaders-2010">2010</a> | <span style="color: red; font-weight: bold; font-size: 12px;">2011</span></span>

</div>
<span class="postedDate" style="float: right;">Updated <?php	 	 echo $updateas; ?></span>
<h3 style="padding-left: 10px;">2011 Leaders in Thoroughbred Racing</h3>
<?php	 	
$sql_top= "SELECT * FROM top_leader where type = '1' ORDER BY position ";
$result_top=mysql_query($sql_top);
?>

<table style="width: 100%; margin-top: 10px;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr class="header">
<td colspan="7">LEADING HORSES</td>
</tr>

<tr>
<th width="3%"></th>
<th>Name</th>
<th width="9%">Starts</th>
<th width="9%">1sts</th>
<th width="9%">2nds</th>
<th width="9%">3rds</th>
<th width="14%">Purses</th>
</tr>

<?php	 	
$counter=0;
while($data_top=mysql_fetch_object($result_top))
{
if($counter%2 == 1)
{ echo '<tr  class="alt">';}
else
{ echo '<tr>'; }
?>
<td class="num"><?php	 	 echo $data_top->position; ?>.</td>
<td><?php	 	 echo stripslashes($data_top->name); ?></td>
<td><?php	 	 echo $data_top->starts; ?></td>
<td><?php	 	 echo $data_top->first; ?></td>
<td><?php	 	 echo $data_top->second; ?></td>
<td><?php	 	 echo $data_top->third; ?></td>
<td><?php	 	 echo $data_top->purse; ?></td>
</tr>
<?php	 	 $counter++; }
?>
</tbody>
</table>
<?php	 	
$sql_top= "SELECT * FROM top_leader where type = '2' ORDER BY position ";
$result_top=mysql_query($sql_top);
?>

<table style="width: 100%; margin-top: 10px;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr class="header">
<td colspan="7">LEADING JOCKEYS</td>
</tr>
<tr>
<th width="3%"></th>
<th>Name</th>
<th width="9%">Mounts</th>
<th width="9%">1sts</th>
<th width="9%">2nds</th>
<th width="9%">3rds</th>
<th width="14%">Purses</th>
</tr>
<?php	 	
$counter=0;
while($data_top=mysql_fetch_object($result_top))
{
if($counter%2 == 1)
{ echo '<tr  class="alt">';}
else
{ echo '<tr>'; }
?>
<td class="num"><?php	 	 echo $data_top->position; ?>.</td>
<td><?php	 	 echo stripslashes($data_top->name); ?></td>
<td><?php	 	 echo $data_top->starts; ?></td>
<td><?php	 	 echo $data_top->first; ?></td>
<td><?php	 	 echo $data_top->second; ?></td>
<td><?php	 	 echo $data_top->third; ?></td>
<td><?php	 	 echo $data_top->purse; ?></td>
</tr>
<?php	 	 $counter++; }
?>
</tbody>
</table>
<?php	 	
$sql_top= "SELECT * FROM top_leader where type = '3' ORDER BY position ";
$result_top=mysql_query($sql_top);
?>

<table style="width: 100%; margin-top: 10px;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr class="header">
<td colspan="7">LEADING TRAINERS</td>
</tr>
<tr>
<th width="3%"></th>
<th>Name</th>
<th width="9%">Starts</th>
<th width="9%">1sts</th>
<th width="9%">2nds</th>
<th width="9%">3rds</th>
<th width="14%">Purses</th>
</tr>
<?php	 	
$counter=0;
while($data_top=mysql_fetch_object($result_top))
{
if($counter%2 == 1)
{ echo '<tr class="alt">';}
else
{ echo '<tr>'; }
?>
<td class="num"><?php	 	 echo $data_top->position; ?>.</td>
<td><?php	 	 echo stripslashes($data_top->name); ?></td>
<td><?php	 	 echo $data_top->starts; ?></td>
<td><?php	 	 echo $data_top->first; ?></td>
<td><?php	 	 echo $data_top->second; ?></td>
<td><?php	 	 echo $data_top->third; ?></td>
<td><?php	 	 echo $data_top->purse; ?></td>
</tr>
<?php	 	 $counter++; }
?>
</tbody>
</table>
</div>