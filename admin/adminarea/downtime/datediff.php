<?Php
function unix_diff($first_date,$first_time,$second_date,$second_time)
{

   // Author: Tone.
   // Date    : 15-12-2003.
  
   // Ref: Dates go in "2003-12-31".
   // Ref: Times go in "12:59:13".
   // Ref: mktime(HOUR,MIN,SEC,MONTH,DAY,YEAR).
  
   // Splits the dates into parts, to be reformatted for mktime.
   $first_date_ex = explode("-",$first_date);
   $first_time_ex = explode(":",$first_time);
   $second_date_ex = explode("-",$second_date);
   $second_time_ex = explode(":",$second_time);
  
   // makes the dates and times into unix timestamps.
   $first_unix  = mktime($first_time_ex[0], $first_time_ex[1], $first_time_ex[2], $first_date_ex[1], $first_date_ex[2], $first_date_ex[0]);
   $second_unix  = mktime($second_time_ex[0], $second_time_ex[1], $second_time_ex[2], $second_date_ex[1], $second_date_ex[2], $second_date_ex[0]);

   // Gets the difference between the two unix timestamps.
   $timediff = $first_unix-$second_unix;
              
   // Works out the days, hours, mins and secs.
   $days=intval($timediff/86400);
   $remain=$timediff%86400;
   $hours=intval($remain/3600);
   $remain=$remain%3600;
   $mins=intval($remain/60);
   $secs=intval($remain%60);

   // Returns a pre-formatted string. Can be chagned to an array.
   //return $days . " days " . $hours . " hours<br> " . $mins . " mins";
   return $timediff;
} 

?>