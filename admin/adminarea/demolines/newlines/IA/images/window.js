function createWindow(url,windowname,w,h,l,t,mb,sb,tb,rb) 
{
	if (!w){w=600;}//width
	if (!h){h=400;}//height
	if (!l){l=100;}//left
	if (!t){t=100;}//top
	if (!mb){mb=0;}//menubar
	if (!sb){sb=1;}//scrollbar
	if (!tb){tb=0;}//toolbar
	if (!rb){rb='yes';}//resizeable
    var features ='width=' + w+
        ',height='      + h +
        ',left=' + l +
        ',top='    + t +
        ',menubar='     + mb +
        ',scrollbars='  + sb +
        ',toolbar='     + tb +
        ',resizable='   + rb;
		newwindow = open (url, windowname, features);
		//window.open (url, windowname, features);
		newwindow.focus();
	
}
function fnloadAffiliateGroups(){
if (fieldexists("merchantid")){
 	var selmerchantid = getObj("merchantid").options[getObj("merchantid").selectedIndex].value;
 	params ="list=affiliategroup&merchantid=" + selmerchantid;
	var mytypeAjax = new Ajax.Updater('RPaffgroups', '../includes/scripts/ajax_selectlist.asp', { parameters: params} );
}
return true;
}
function fnloadPromoGroups(){
if (fieldexists("merchantid")){
 	var selmerchantid = getObj("merchantid").options[getObj("merchantid").selectedIndex].value;
 	params ="list=promogroup&merchantid=" + selmerchantid;
	var mytypeAjax = new Ajax.Updater('RPpromogroups', '../includes/scripts/ajax_selectlist.asp', { parameters: params} );
}
return true;
}
function fnloadPromoGroupsALL(){
if (fieldexists("merchantid")){
 	var selmerchantid = getObj("merchantid").options[getObj("merchantid").selectedIndex].value;
 	params ="list=promogroup&allowall=true&merchantid=" + selmerchantid;
	var mytypeAjax = new Ajax.Updater('RPpromogroups', '../includes/scripts/ajax_selectlist.asp', { parameters: params} );
}
return true;
}
function fnselrow(curclassname,classid){
	if (curclassname == 'reportrowsel'){ return 'reportrow'+ classid;}
	else {return 'reportrowsel';}
}
function toggletable(tableid,rowid){
	thistbl = document.getElementById(tableid);
	thistbllnk= document.getElementById(tableid + "link");
	var trs = thistbl.getElementsByTagName("tr");
    for (var i = 0; i < trs.length; i++) {
	if (rowid == trs[i].id){	
		showMode = 	trs[i].style.display;
		if (showMode=="none") {trs[i].style.display = "";thistbllnk.innerHTML ="[Hide]";}
		else {trs[i].style.display = "none";thistbllnk.innerHTML ="[Show]";	}
	}
	}
}
