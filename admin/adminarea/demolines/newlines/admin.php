<?php	 		 	
include "common.php";
$sql= "select * from sportscat order by position ";
$result=mysql_query($sql);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Lines Program Demo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="./css/dragable-boxes.css" type="text/css">

	<style type="text/css">
	/* CSS NEEDED ONLY IN THE DEMO */
	/*html{
		width:100%;
		overflow-x:hidden;
	}
	body{
		font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;
		width:100%;
		margin:0px;
		padding:0px;
		text-align:center;
		background-color:#E2EBED;
				background-color:#FFFFFF;	
		font-size:0.7em;	
		overflow-x:hidden;
	}*/
		
	#mainContainer{
		width:510px;
		margin:0 auto;
		text-align:left;
		background-color:#FFF;
	}
	h4{
		margin:0px;
		background-color:#666666;
		padding:0px 1px 0px 5px;
		
	}
	p{
		margin-top:5px;
		margin-left:5px;
		margin-right:5px;
	}
	
	.odd
	{
		background-color:#CCCCCC;
	}
	
	
	
	/* This is the box that is parent to the dragable items */
	#dragableElementsParentBox{
		padding:10px;	/* Air */
	}
	
	.smallArticle,.bigArticle{
		float:left;
		border:1px solid #000;
		background-color:#ffffff;
		/*padding:2px;
		margin-right:10px;*/
		margin-bottom:2px;
	}
	/*.smallArticle img,.bigArticle img{
		float:left;
		padding:5px;
	}*/
	.smallArticle .rightImage,.bigArticle .rightImage{
		float:right;
	}
	.smallArticle{
		width:500px;		
	}
	.bigArticle{
		width:564px;
	}
	.clear{
		clear:both;
	}
	
	/* END DEMO CSS */
	
	/* REQUIRED CSS */
	
	#rectangle{
		float:left;
		border:1px dotted #F00;	/* Red border */
		background-color:#FFF;
	}
	#insertionMarker{	/* Don't change the rules for the insertionMarker */
		width:6px;
		position:absolute;
		display:none;
	}
	#insertionMarker img{	/* Don't change the rules for the insertionMarker */
		float:left;
	}		
	#dragDropMoveLayer{	/* Dragable layer - Not need if you're using 'rectangle' mode */
		position:absolute;
		display:none;
		border:1px solid #000;
		filter:alpha(opacity=50);	/* 50% opacity , i.e. transparency */
		opacity:0.5;	/* 50% opacity , i.e. transparency */

	}
	
	/* END REQUIRED CSS */
	</style>
	<script type="text/javascript" src="js/request.js"></script>
	<script type="text/javascript" src="js/arrange_content.js"></script>
	<script type="text/javascript" src="js/extra_admin.js"></script>

	</head>

<body>
<br>
<div id="mainContainer">
	<!-- START DRAGABLE CONTENT -->
	<div id="dragableElementsParentBox">
	<?php	 	
	$i=0;
	while($data=mysql_fetch_object($result))
	{
	?>
	<div class="smallArticle" dragableBox="true" id="article_<?php	 	 echo $data->id; ?>">
	<p ><div  style="width:200px; float:left; padding-left: 5px; "><?php	 	 echo $data->title; ?></div><div  style="padding-left:0px; float:left; " >
	<?php	 	
	if($data->active == 'Y')
	{
	?>
	&nbsp;<img src="images/active.gif" onClick="actadmin(<?php	 	 echo $data->id; ?>,'N');" style="cursor:pointer; ">
	<?php	 		
	}
	else
	{
	?>
	&nbsp;<img src="images/deactive.gif" onClick="actadmin(<?php	 	 echo $data->id; ?>,'Y');" style="cursor:pointer; ">
	<?php	 		
	}
	?>|&nbsp;<img src="images/no.png" style="cursor:pointer; " onClick="javascript:confirmDelete(<?php	 	 echo $data->id; ?>)">&nbsp;|&nbsp;<img src="images/b_edit.png" onClick="showeditsp(<?php	 	 echo $data->id; ?>,'n');" style="cursor:pointer; ">&nbsp;|&nbsp;<a href="newlines.php?id=<?php	 	 echo $data->id; ?>"  >Arrange sports</a></div></p>
	</div>
	<?php	 	
	}
	?> 
	<div class="clear"></div>
</div>
<input type="button" value="Save Arrangement" onclick="saveData()"> <input type="button" value="Add New Tab" onClick="showeditsp(0,'n');" >
</div>
<!-- REQUIRED DIVS -->
<div id="insertionMarker">
	<img src="images/marker_top.gif">
	<img src="images/marker_middle.gif" id="insertionMarkerLine">
	<img src="images/marker_bottom.gif">
</div>
<!-- END REQUIRED DIVS -->
<div class="loadingBox" id="loading" align="center">
<img src="images/ajax-loader.gif" style="margin:25px 25px 25px 25px " ><br>

</div>


<div  class="editbox" id="editdiv" style="height:150px; top:150px; ">
<div align="right"><a href="#" onClick="hideedit();return false;" >Close[X]</a></div>
<div id="innercontent">

</div>
</div>

<br>
</body>
</html>
