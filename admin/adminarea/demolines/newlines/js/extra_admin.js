// JavaScript Document
function showeditsp(id,up)
		{
			//alert(id);
			document.getElementById("loading").style.display="block";
			Http.get({
				url: "getadmin.php?id="+id+"&up="+up,
				callback: gettingadmin,
				cache: Http.Cache.Get
			});
			return false;
			
		}
		
		function gettingadmin(xmlreply)
		{
			document.getElementById("loading").style.display="none";
				var request = xmlreply.responseText;
			document.getElementById("innercontent").innerHTML=request;
			document.getElementById("editdiv").style.display="block";
		}
		
		function hideedit()
		{
			//alert("ok");
			location.reload(true);
			document.getElementById("editdiv").style.display="none";
		}
		
		function confirmDelete(id) {
		  if (confirm("Are you sure you want to delete")) {
			document.getElementById("loading").style.display="block";
			Http.get({
				url: "deleteadmin.php?id="+id,
				callback: hideedit,
				cache: Http.Cache.Get
			});
			return false;
		  }
		}
		
		function actadmin(id,status)
		{
			document.getElementById("loading").style.display="block";
			Http.get({
				url: "updatestatad.php?id="+id+"&status="+status,
				callback: hideedit,
				cache: Http.Cache.Get
			});
			return false;
		}
		
		
		   var http_request = false;
   function makePOSTRequest(url, parameters) {
   document.getElementById("loading").style.display="block";
      http_request = false;
      if (window.XMLHttpRequest) { // Mozilla, Safari,...
         http_request = new XMLHttpRequest();
         if (http_request.overrideMimeType) {
         	// set type accordingly to anticipated content type
            //http_request.overrideMimeType('text/xml');
            http_request.overrideMimeType('text/html');
         }
      } else if (window.ActiveXObject) { // IE
         try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }
      if (!http_request) {
         alert('Cannot create XMLHTTP instance');
         return false;
      }
      
      http_request.onreadystatechange = alertContents;
      http_request.open('POST', url, true);
      http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      http_request.setRequestHeader("Content-length", parameters.length);
      http_request.setRequestHeader("Connection", "close");
      http_request.send(parameters);
   }

   function alertContents() {
   	//alert(http_request.readyState);
      if (http_request.readyState == 4) {
         if (http_request.status == 200) {
            //alert(http_request.responseText);
            result = http_request.responseText;
            document.getElementById('myspan').innerHTML = result;  
			showeditsp(result,'y');          
         } else {
            alert('There was a problem with the request.');
         }
      }
   }
   
   function get(obj) {
     var poststr = "id=" + encodeURI( document.getElementById("id").value );
   		var els = document.myform.elements;
		for(var no=0;no<els.length;no++){
			//alert(els[no]);
			poststr = poststr+"&"+els[no].name+"=" + encodeURI(els[no].value );
		}  
                   
      makePOSTRequest('form_Submit.php', poststr);
   }
