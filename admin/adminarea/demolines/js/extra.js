// JavaScript Document
function showedit(id,up)
		{
			//alert(id);
			document.getElementById("loading").style.display="block";
			Http.get({
				url: "getform.php?id="+id+"&up="+up,
				callback: gettingpro,
				cache: Http.Cache.Get
			});
			return false;
			
		}
		
		function lishowedit(id,up)
		{
			alert(id);
			document.getElementById("loading").style.display="block";
			/*Http.get({
				url: "getform.php?id="+id+"&up="+up,
				callback: gettingpro,
				cache: Http.Cache.Get
			});
			return false;*/
			document.getElementById("lieditdiv").style.display="block";
			
		}
		
		function showadd(id,up)
		{
			//alert(id);
			document.getElementById("loading").style.display="block";
			Http.get({
				url: "getform.php?id=0&up="+up+"&main_id="+id,
				callback: gettingpro,
				cache: Http.Cache.Get
			});
			return false;
			
		}

		function gettingpro(xmlreply)
		{
			document.getElementById("loading").style.display="none";
				var request = xmlreply.responseText;
			document.getElementById("innercontent").innerHTML=request;
			document.getElementById("editdiv").style.display="block";
		}
		
		function act(id,status,re)
		{
			document.getElementById("loading").style.display="block";
			Http.get({
				url: "updatestat.php?id="+id+"&status="+status+"&action="+re,
				callback: gettingproact,
				cache: Http.Cache.Get
			});
			return false;
		}
		
		function actmain(id,status)
		{
			document.getElementById("loading").style.display="block";
			Http.get({
				url: "updatemain.php?id="+id+"&status="+status,
				callback: gettingproact,
				cache: Http.Cache.Get
			});
			return false;
		}
		function gettingproact(xmlreply)
		{
			document.getElementById("loading").style.display="none";
				var request = xmlreply.responseText;
			showedit(request,'y');          
			
		}
		
		function change(id,status)
		{
			document.getElementById("loading").style.display="block";
			Http.get({
				url: "change.php?id="+id+"&action="+status,
				callback: gettingproch,
				cache: Http.Cache.Get
			});
			return false;
		}
		function gettingproch(xmlreply)
		{
			document.getElementById("loading").style.display="none";
				var request = xmlreply.responseText;
			showedit(request,'y');          
			
		}
		function confirmDelete(id,re) {
		  if (confirm("Are you sure you want to delete")) {
			document.getElementById("loading").style.display="block";
			Http.get({
				url: "delete.php?id="+id+"&action="+re,
				callback: gettingproch,
				cache: Http.Cache.Get
			});
			return false;
		  }
		}
		
		function confirmDeletemain(id,re) {
		  if (confirm("Are you sure you want to delete? It will delete all its lines also.")) {
			document.getElementById("loading").style.display="block";
			Http.get({
				url: "deletemain.php?id="+id+"&action="+re,
				callback: hideedit,
				cache: Http.Cache.Get
			});
			return false;
		  }
		}
		

		function hideedit()
		{
			//alert("ok");
			location.reload(true);
			document.getElementById("editdiv").style.display="none";
		}
	
   var http_request = false;
   function makePOSTRequest(url, parameters) {
   document.getElementById("loading").style.display="block";
      http_request = false;
      if (window.XMLHttpRequest) { // Mozilla, Safari,...
         http_request = new XMLHttpRequest();
         if (http_request.overrideMimeType) {
         	// set type accordingly to anticipated content type
            //http_request.overrideMimeType('text/xml');
            http_request.overrideMimeType('text/html');
         }
      } else if (window.ActiveXObject) { // IE
         try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }
      if (!http_request) {
         alert('Cannot create XMLHTTP instance');
         return false;
      }
      
      http_request.onreadystatechange = alertContents;
      http_request.open('POST', url, true);
      http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      http_request.setRequestHeader("Content-length", parameters.length);
      http_request.setRequestHeader("Connection", "close");
      http_request.send(parameters);
   }

   function alertContents() {
   	//alert(http_request.readyState);
      if (http_request.readyState == 4) {
         if (http_request.status == 200) {
            //alert(http_request.responseText);
            result = http_request.responseText;
            document.getElementById('myspan').innerHTML = result;  
			showedit(result,'y');          
         } else {
            alert('There was a problem with the request.');
         }
      }
   }
   
   function get(obj) {
     var poststr = "id=" + encodeURI( document.getElementById("id").value );
   		var els = document.myform.elements;
		for(var no=0;no<els.length;no++){
			//alert(els[no]);
			poststr = poststr+"&"+els[no].name+"=" + encodeURI(els[no].value );
		}  
                   
      makePOSTRequest('formSubmit.php', poststr);
   }
