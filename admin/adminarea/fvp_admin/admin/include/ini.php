<?
// REQUIRED CLASSES AND FILES FOR PEAR PACKAGES //
require_once (AT_INCLUDE_PATH.'include/dbconnect.php');
//require_once (AT_INCLUDE_PATH.'classes/paging.php');
require_once (AT_INCLUDE_PATH.'classes/Tbl_admin.php');
require_once (AT_INCLUDE_PATH.'classes/Tbl_categories.php');
require_once (AT_INCLUDE_PATH.'classes/Tbl_subcategory.php');
require_once (AT_INCLUDE_PATH.'classes/Tbl_product.php');
require_once (AT_INCLUDE_PATH.'classes/Tbl_orders.php');
require_once (AT_INCLUDE_PATH.'classes/Tbl_customer.php');
require_once (AT_INCLUDE_PATH.'classes/Tbl_cart.php');
//require_once (AT_INCLUDE_PATH.'classes/Dummy.php');
require_once (AT_INCLUDE_PATH.'classes/Tbl_faq.php');
 
// REQUIRED CLASSES FOR SMARTY PACKAGES //
require (AT_INCLUDE_PATH.'libs/Smarty.class.php');
require (AT_INCLUDE_PATH.'include/iniSmarty.php');
require_once (AT_INCLUDE_PATH.'classes/Tbl_cart.php');
// REQUIRED CLASSES FOR THUMBNAIL IMAGES //
require (AT_INCLUDE_PATH.'lib/ImageResizeClass.php');
require (AT_INCLUDE_PATH.'lib/ImageResizeFactory.php');
?>