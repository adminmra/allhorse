<?php	 		 	
include("libs/Smarty.class.php");
include("configs/mysql.php");
include("configs/db_info.php");



global $smarty;
$smarty = new Smarty;
$smarty->compile_check = true;
$smarty->debugging = false;
$smarty->template_dir ="templates";
$smarty->compile_dir = 'templates_c';
$smarty->config_dir = 'configs/';
$smarty->cache_dir = 'cache/'; 
$smarty->caching = false;

global $db_obj;
$db_obj = new sql_db($db_host,$db_user,$db_pass,$db_name);

?>