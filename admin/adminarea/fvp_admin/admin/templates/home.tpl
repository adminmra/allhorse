<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="style1.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="100" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="115">{include file="adminheader.tpl"}</td>
  </tr>
  <tr>
    <td height="31" bgcolor="#000000" style="padding-left:12px; padding-right:7px; "><table width="100%" height="31"  border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td><img src="images/admin-panel-txt.gif" alt="" title="" width="113" height="31"></td>
        <td align="right"><img src="images/log-out.gif" width="87" height="31" border="0" usemap="#Map"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><img src="images/blank.gif" alt="" title="" width="1" height="1"></td>
  </tr>
  <tr>
    <td align="left" valign="top" bgcolor="#E8EBDE"><table width="100%" height="100%"  border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="190" align="left" valign="top" style=" padding-left:6px; padding-top:30px; padding-bottom:25px; ">{include file="left-menu.tpl"}</td>
        <td width="1" valign="top" background="images/divider.gif"><img src="images/divider.gif" width="1" height="3"></td>
        <td width="580" align="center" valign="top" style="padding-top:50px; padding-bottom:50px; "><table width="436" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td><table width="158" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><img src="images/label-left.gif" alt="" title="" width="8" height="30"></td>
                      <td width="142" align="center" bgcolor="#7F8690" class="label">Welcome 
                        to Admin</td>
                  <td><img src="images/label-right.gif" alt="" title="" width="8" height="30"></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td><img src="images/blank.gif" alt="" title="" width="1" height="1"></td>
          </tr>
          <tr>
            <td bgcolor="#BBBBBB" style="border-left:1px solid #BBBBBB; border-right:1px solid #BBBBBB; "><img src="images/blank.gif" alt="" title="" width="1" height="3"></td>
          </tr>
          <tr>
            <td height="148" valign="top" bgcolor="#EDEDED" style="border-left:1px solid #BBBBBB; border-right:1px solid #BBBBBB; padding-top:11px; padding-left:18px; padding-right:23px; ">
              <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td valign="top" class="txt"><div align="justify">
                    <p>Welcome Administrator. </p>
                  </div></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="12"></td>
                </tr>                
                <tr>
                  <td valign="top">&nbsp;</td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td bgcolor="#BBBBBB" style="border-left:1px solid #BBBBBB; border-right:1px solid #BBBBBB; "><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
          </tr>
        </table>
          <map name="Map5">
            <area shape="rect" coords="20,11,68,83" href="#">
          </map>
          <map name="Map6">
            <area shape="rect" coords="25,10,71,82" href="#">
          </map>
          <map name="Map7">
            <area shape="rect" coords="11,8,78,82" href="#">
          </map></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="56">{include file="adminfooter.tpl"}</td>
  </tr>
</table>
<map name="Map">
  <area shape="rect" coords="0,9,69,25" href="logout.php">
</map>
</body>
</html>
