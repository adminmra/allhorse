<?php	 		 	
session_start();
include("configs/admin_global.php");
include("left.php");
include("classes/breadcrumb.php");


if($_REQUEST['mode']=="listvideo")
{
	if($_REQUEST['cat_id']!="")
	{
		$parent_id = $_REQUEST['cat_id'];		
		//$parent_id = $_REQUEST['set'];
        //$parent_id = ($_REQUEST['set']) ? $_REQUEST['set'] : $_REQUEST['cat_id'];
		$brdcrmb=new breadcrumb();
		$home="video_manage.php?mode=listvideo";
		$other="video_manage.php";
		$breadcrumb_ar=$brdcrmb->show_breadcrumb($parent_id,$home,$other,$_REQUEST['set']);
		$b="";
		for($i=0;$i<count($breadcrumb_ar);$i++)
		{
			$b .=$breadcrumb_ar[$i];			
			$smarty->assign('bread',$b);
		}
		
	}
	else
	{
		$parent_id = 0;
	}


	$res = $db_obj->sql_query("SELECT * FROM tbl_video WHERE category_id='$parent_id'");

	while($pos = $db_obj->sql_fetchrow($res))
	{
		$video_list[] = array("id"=>$pos['id'],
							  "label"=>$pos['label'],
							  "info"=>$pos['info']);
	}
	if(!empty($video_list))
	{
		$smarty->assign("video_list",$video_list);
	}
	else
	{
		$smarty->assign("emptyvideo","YES");
	}
	if($_REQUEST['genmsg']==1)
	{
		$smarty->assign("genmsg","Xml file generated successfully.");
	}
	$smarty->assign("mode","listvideo");
}


//............... Add video file .............//
if($_REQUEST['mode']=="addvideo")
{
	$smarty->assign("mode","addvideo");
}

if(isset($_REQUEST['submit_video']))
{
	if($_REQUEST['catdrop']!="select")
	{
		$catdrop = $_REQUEST['catdrop'];
	}

	$txt_label = addslashes(trim($_REQUEST['txt_label']));
	$txt_info = addslashes(trim($_REQUEST['txt_info']));
	$txt_value = $_REQUEST['txt_value'];
	$chk_player1 = $_REQUEST['chk_player1'];
	$chk_player2 = $_REQUEST['chk_player2'];
	$chk_player3 = $_REQUEST['chk_player3'];
	
	if($chk_player1 == TRUE)
	{
		$player1 = 1;
	}
	else
	{
		$player1 = 0;
	}
	
	if($chk_player2 == TRUE)
	{
		$player2 = 1;
	}
	else
	{
		$player2 = 0;
	}
	
	if($chk_player3 == TRUE)
	{
		$player3 = 1;
	}
	else
	{
		$player3 = 0;
	}
	
	
	if($_FILES['file_image']['name']!="")
	{
		$img_filename = $_FILES['file_image']['name'];
		$img_dest = "../video/".$_FILES['file_image']['name'];
		move_uploaded_file($_FILES['file_image']['tmp_name'],$img_dest);
	}
	if($_FILES['file_thumb']['name']!="")
	{
		$thmb_filename = $_FILES['file_thumb']['name'];
		$thmb_dest = "../video/".$_FILES['file_thumb']['name'];
		move_uploaded_file($_FILES['file_thumb']['tmp_name'],$thmb_dest);
	}
	
	$res = $db_obj->sql_query("SELECT * FROM tbl_video WHERE label='$txt_label' AND image='$img_filename' AND thmb='$thmb_filename' AND info='$txt_info' AND category_id='$catdrop'");
	$cnt = $db_obj->sql_numrows($res);

	
	if($cnt==0)
	{
		$db_obj->sql_query("INSERT INTO tbl_video(category_id,label,image,thmb,info,valu,player1,player2,player3) VALUES('$catdrop','$txt_label','$img_filename','$thmb_filename','$txt_info','$txt_value','$player1','$player2','$player3')");

		$msg = "Added to database successfully.";
	}
	else
	{
		$msg = "Duplicate values are not allowed.";
	}

	$smarty->assign("msg",$msg);
	$smarty->assign("mode","addvideo");
}

//.......... generate xml file .............//
if(isset($_REQUEST['generate_xml']))
{
	$xmlstr = "<?xml version='1.0' ?>\n\t<tree>\n\n";
	
	/*$res = $db_obj->sql_query("SELECT * FROM tbl_video");
	while($pos = $db_obj->sql_fetchrow($res))
	{
		$xmlstr .= "<node label ='".$pos['label']."' image='video/".$pos['image']."' thmb='video/".$pos['thmb']."' info='".$pos['info']."' valu='".$pos['valu']."' />\n\n";
	}*/
	
	
	/////////////////////////////////
	
	$res = $db_obj->sql_query("SELECT * FROM tbl_video where player1=1");
	$xmlstr .="\t\t<player1>\n\n";
	while($pos = $db_obj->sql_fetchrow($res))
	{
		$xmlstr .= "\t\t\t<node label ='".$pos['label']."' image='video/".$pos['image']."' thmb='video/".$pos['thmb']."' info='".$pos['info']."' valu='".$pos['valu']."' />\n\n";
	}
	$xmlstr .="\t\t</player1>\n\n";
	
	$res = $db_obj->sql_query("SELECT * FROM tbl_video where player2=1");
	$xmlstr .="\t\t<player2>\n\n";
	while($pos = $db_obj->sql_fetchrow($res))
	{
		$xmlstr .= "\t\t\t<node label ='".$pos['label']."' image='video/".$pos['image']."' thmb='video/".$pos['thmb']."' info='".$pos['info']."' valu='".$pos['valu']."' />\n\n";
	}
	$xmlstr .="\t\t</player2>\n\n";
	
	$res = $db_obj->sql_query("SELECT * FROM tbl_video where player3=1");
	$xmlstr .="\t\t<player3>\n\n";
	while($pos = $db_obj->sql_fetchrow($res))
	{
		$xmlstr .= "\t\t\t<node label ='".$pos['label']."' image='video/".$pos['image']."' thmb='video/".$pos['thmb']."' info='".$pos['info']."' valu='".$pos['valu']."' />\n\n";
	}
	$xmlstr .="\t\t</player3>\n\n";
	/////////////////////////////////////////
	$xmlstr .="\t</tree>";
	$fp = fopen("../video.xml","w");
	if($fp)
	{
		fwrite($fp,$xmlstr);
		fclose($fp);
	}

	header("location:video_manage.php?mode=listvideo&genmsg=1");
}

//........ edit video ...........//
if($_REQUEST['mode']=="edit")
{
	$id = $_REQUEST['id'];
	$res = $db_obj->sql_query("SELECT * FROM tbl_video WHERE id='$id'");
	$pos =  $db_obj->sql_fetchrow($res);
	$listvideo = array("id"=>$pos['id'],
				  "category_id"=>$pos['category_id'],
				  "label"=>$pos['label'],
				  "info"=>$pos['info'],
				  "value"=>$pos['valu'],
				  "player1"=>$pos['player1'],
				  "player2"=>$pos['player2'],
				  "player3"=>$pos['player3'],
				  );

	$smarty->assign("listvideo",$listvideo);
	$smarty->assign("mode","addvideo");
	$smarty->assign("modetype","edit");
}
if(isset($_REQUEST['update_video']))
{
	$id = $_REQUEST['id'];
	$txt_label = addslashes(trim($_REQUEST['txt_label']));
	$txt_info = addslashes(trim($_REQUEST['txt_info']));
	$txt_value = $_REQUEST['txt_value'];
	$chk_player1 = $_REQUEST['chk_player1'];
	$chk_player2 = $_REQUEST['chk_player2'];
	$chk_player3 = $_REQUEST['chk_player3'];
	
	if($chk_player1 == TRUE)
	{
		$player1 = 1;
	}
	else
	{
		$player1 = 0;
	}
	
	if($chk_player2 == TRUE)
	{
		$player2 = 1;
	}
	else
	{
		$player2 = 0;
	}
	
	if($chk_player3 == TRUE)
	{
		$player3 = 1;
	}
	else
	{
		$player3 = 0;
	}
	
	if($_FILES['file_image']['name']!="")
	{
		$img_filename = $_FILES['file_image']['name'];
		$img_dest = "../video/".$_FILES['file_image']['name'];
		move_uploaded_file($_FILES['file_image']['tmp_name'],$img_dest);
	}
	if($_FILES['file_thumb']['name']!="")
	{
		$thmb_filename = $_FILES['file_thumb']['name'];
		$thmb_dest = "../video/".$_FILES['file_thumb']['name'];
		move_uploaded_file($_FILES['file_thumb']['tmp_name'],$thmb_dest);
	}
	
	if($img_filename!="" && $thmb_filename!="")
	{
		$sql = "UPDATE tbl_video SET label='$txt_label',image='$img_filename', thmb='$thmb_filename' , info='$txt_info', player1='$player1',player2='$player2',player3='$player3', valu='$txt_value' WHERE id='$id'";
	}
	elseif($img_filename!="" && $thmb_filename=="")
	{
		$sql = "UPDATE tbl_video SET label='$txt_label',image='$img_filename', info='$txt_info',player1='$player1',player2='$player2',player3='$player3', valu='$txt_value' WHERE id='$id'";
	}
	elseif($img_filename=="" && $thmb_filename!="")
	{
		$sql = "UPDATE tbl_video SET label='$txt_label',thmb='$thmb_filename' , info='$txt_info',player1='$player1',player2='$player2',player3='$player3', valu='$txt_value' WHERE id='$id'";
	}
	else
	{
		$sql = "UPDATE tbl_video SET label='$txt_label',info='$txt_info',player1='$player1',player2='$player2',player3='$player3', valu='$txt_value' WHERE id='$id'";
	}

	$db_obj->sql_query($sql);
	header("location:video_manage.php?mode=listvideo");

}

//........ delete video ........//
if($_REQUEST['mode']=="delete")
{
	$id = $_REQUEST['id'];
	$db_obj->sql_query("DELETE FROM tbl_video WHERE id='$id'");

	header("location:video_manage.php?mode=listvideo");
}

//........... list category in drop down ..........//
$cat_list_drpdwn = list_cat_dropdwn_new(1);	
if(!empty($cat_list_drpdwn))
{
	$smarty->assign("cat_list_drpdwn",$cat_list_drpdwn);
}
else
{
	$smarty->assign("emptycat","YES");
}

$smarty->display('video_manage.tpl'); 
?>