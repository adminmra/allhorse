<?php	 		 	

function list_category($parent_id,$status)
{
	
	global $db_obj;

	if($status == "")
	{
		
		$res = $db_obj->sql_query("SELECT * FROM tbl_category WHERE parent_id = '$parent_id' ORDER BY cat_id");
	}
	else
	{
		$res = $db_obj->sql_query("SELECT * FROM tbl_category WHERE parent_id = '$parent_id' AND status = '$status' ORDER BY cat_id");
	}

	while($pos = $db_obj->sql_fetchrow($res))
	{
		$list[] = array("cat_id"=>$pos['cat_id'], "parent_id"=>$pos['parent_id'], "cat_name"=>stripslashes($pos['category_name']),"status"=>$pos['status']);
	}
	return $list;
}

function get_parentcatname($parent_id)
{	
	global $db_obj;
	$res = $db_obj->sql_query("SELECT * FROM tbl_category WHERE cat_id='$parent_id'");

	$pos = $db_obj->sql_fetchrow($res);			
	
	if($pos['parent_id']!=0)
	{	
		$pos['category_name'].="<=".get_parentcatname($pos['parent_id']);	
	}
	return $pos['category_name'];
}

function list_cat_dropdwn($status="")
{	
	global $db_obj;
	if($status=="")
	{
		$res = $db_obj->sql_query("SELECT * FROM tbl_category ORDER BY parent_id");
	}
	else
	{
		$res = $db_obj->sql_query("SELECT * FROM tbl_category WHERE status=1 ORDER BY parent_id");
	}
	$i = 0;

	while($pos = $db_obj->sql_fetchrow($res))
	{
		$cat_set[$i]['cat_id']=$pos['cat_id'];
		$cat_set[$i]['parent_id']=$pos['parent_id'];	
		
		if($pos['parent_id'] == 0)
		{
			$cat_set[$i]['cat_name']=stripslashes($pos['category_name']);
		}
		else
		{
			$parentcatname= get_parentcatname($pos['parent_id']);
			$cat_set[$i]['cat_name']=stripslashes($pos['category_name'])."<=".$parentcatname;
								
		}
		$cat_set[$i]['status']=$pos['status'];
		$i++;		
	}

	return $cat_set;
}

function get_subcat_drpdwn($ar,$cat_id,$status="")
{
	$j=0;
	global $db_obj;
	if($status=="")
	{
		$res = $db_obj->sql_query("SELECT * FROM tbl_category WHERE parent_id='$cat_id' ORDER BY cat_id");
	}
	else
	{
		$res = $db_obj->sql_query("SELECT * FROM tbl_category WHERE status=1 AND parent_id='$cat_id' ORDER BY cat_id");
	}
	while($pos = $db_obj->sql_fetchrow($res))
	{
		$cat_set['cat_id']=$pos['cat_id'];
		$cat_set['parent_id']=$pos['parent_id'];		
		$cat_set['status']=$pos['status'];
		$cat_set['cat_name']=stripslashes($pos['category_name']);
		array_push($ar,$cat_set);
		
	}
	
	return $ar;

	
}

function list_cat_dropdwn_new($status="")
{	
	global $db_obj;
	$ar = array();
	if($status=="")
	{
		$res = $db_obj->sql_query("SELECT * FROM tbl_category ORDER BY parent_id");
	}
	else
	{
		$res = $db_obj->sql_query("SELECT * FROM tbl_category WHERE status=1 ORDER BY parent_id");
	}
	$i = 0;
	$j = 0;
	while($pos = $db_obj->sql_fetchrow($res))
	{
		if($pos['parent_id']==0)
		{
			$cat_set['cat_id']=$pos['cat_id'];
			$cat_set['parent_id']=$pos['parent_id'];		
			$cat_set['status']=$pos['status'];
			$cat_set['cat_name']=stripslashes($pos['category_name']);
			array_push($ar,$cat_set);
			$ar = get_subcat_drpdwn($ar,$pos['cat_id'],$status="");
		}			
	}
	return $ar;
	
}

?>