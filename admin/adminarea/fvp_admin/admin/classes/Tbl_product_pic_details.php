<?php	 		 	
/**
 * Table Definition for tbl_product_pic_details
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_product_pic_details extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'tbl_product_pic_details';         // table name
    var $pic_id;                          // int(11)  not_null primary_key auto_increment
    var $company_id;                      // int(11)  not_null
    var $p_id;                            // int(11)  not_null
    var $image_one;                       // string(45)  not_null
    var $image_one_default_status;        // int(11)  not_null
    var $image_one_url;                   // string(45)  not_null
    var $image_two;                       // string(45)  not_null
    var $image_two_default_status;        // int(11)  not_null
    var $image_two_url;                   // string(45)  not_null
    var $image_three;                     // string(45)  not_null
    var $image_three_default_status;      // int(11)  not_null
    var $image_three_url;                 // string(45)  not_null
    var $image_four;                      // string(45)  not_null
    var $image_four_default_status;       // int(11)  not_null
    var $image_four_url;                  // string(45)  not_null

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_product_pic_details',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
