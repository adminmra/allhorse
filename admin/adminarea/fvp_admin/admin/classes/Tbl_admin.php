<?php	 		 	
/**
 * Table Definition for tbl_admin
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_admin extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'tbl_admin';                       // table name
    var $id;                              // int(11)  not_null primary_key auto_increment
    var $user_name;                       // string(50)  not_null
    var $password;                        // string(50)  not_null
    var $email;                           // string(96)  not_null
    var $status;                          // string(1)  not_null

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_admin',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
