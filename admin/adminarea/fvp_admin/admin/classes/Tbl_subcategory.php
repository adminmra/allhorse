<?php	 		 	
/**
 * Table Definition for tbl_subcategory
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_subcategory extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'tbl_subcategory';                 // table name
    var $id;                              // int(5)  not_null primary_key auto_increment
    var $cat_id;                          // int(5)  not_null
    var $sub_cat_name;                    // string(80)  not_null
    var $status_flag;                     // int(1)  not_null

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_subcategory',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
