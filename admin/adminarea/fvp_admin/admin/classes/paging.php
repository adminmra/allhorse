<?

class paging

{

	function pagination($num_rows_all, $start, $limit, $urlstr)

	{

		$c = 1;

		$max = 10;

		if($start == 0)

		{

			$from = 1;

			$to = $limit;

		}

		else

		{

			$from = $start+1;

			$to = $from + $limit;

		}

		if($to > $num_rows_all)

		{

			$to = $num_rows_all;

		}

		if($to != 0 && $num_rows_all !=0)

		{

			$countmsg = "Showing $from to $to of $num_rows_all";

		}

		$display .=  $countmsg;

		@$pages = intval($num_rows_all/$limit);

		if ($num_rows_all % $limit) 

		{

			$pages++;

		}

		$sets = intval($pages/$max);

		if($pages % $max)

		{

			$sets++;

		}		

		@$currentpage = intval($start/$limit);

		$currentpage++;

		if($currentpage > $max)

		{

			$set = intval($currentpage/$max);

			if($currentpage % $max)

			{

				$set++;

			}

			$end_page = $max * $set;

			$start_page = $end_page - $max;

			if($start_page > 1)

			{

				$start_page++; 

			}

		}

		else

		{

			$start_page = 1;

			$end_page = $max; 

		}

		if($end_page > $pages)

		{

			$end_page = $pages;

		}

		if($start_page > $max)

		{

			$previousdisplay = "...";

			$previousset = ($start_page - 2) * $limit;

		}

		if($end_page < $pages)

		{

			$nextdisplay = "...";

			$nextset = ($end_page) * $limit;

		}

		$display .=  '<div align="center">';

		if($previousset > 0) 

			 $display .= "[ <a href=\"" . $PHP_SELF . $urlstr ."&start=".$previousset."\" class=\"nomal-text\">&lt;&lt;</a> ]";

		if($start > 0) 

			 $display .= "[ <a href=\"" . $PHP_SELF . $urlstr ."&start=" .($start - $limit)."\" class=\"nomal-text\">Previous</a> ] ".$previousdisplay."";

		if($num_rows_all > $limit)

		{

			while($start_page <= $end_page)

			{

				$subpage = $start_page - 1;

				$snext = $subpage * $limit;		

				if($start_page != $currentpage)

				{

					$display .= "<a href='".$PHP_SELF. $urlstr ."&start=".$snext."' class=\"nomal-text\">".$start_page."</a>";

				}

				else

				{

					$display .= "<span style='font-size:12px;'>".$start_page."</span>";

				}		

				if($start_page != $end_page)

				{

					$display .= "&nbsp; ";

				}

				$scale++;

				$start_page++;

			}

		}

		if($num_rows_all > ($start + $limit)) 

		{

			 $display .=  "".$nextdisplay." [ <a href=\"" . $PHP_SELF . $urlstr ."&start=".($start + $limit)."\" class=\"nomal-text\">Next</a> ]";

		

		}

		if($nextset > 0) 

		{

			 $display .= "[ <a href=\"" . $PHP_SELF . $urlstr ."&start=".$nextset."\" class=\"nomal-text\">&gt;&gt;</a> ]";

		

		}

		$display .= '</div>';

		return $display;

	}//end function
}