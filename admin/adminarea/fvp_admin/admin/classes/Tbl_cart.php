<?php	 		 	
/**
 * Table Definition for tbl_cart
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_cart extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'tbl_cart';                        // table name
    var $cart_id;                         // int(11)  not_null primary_key auto_increment
    var $product_id;                      // int(10)  not_null
    var $session_id;                      // string(255)  not_null
    var $quentity;                        // int(10)  not_null

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_cart',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
