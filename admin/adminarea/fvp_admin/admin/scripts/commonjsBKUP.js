function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}




// TEXTBOX EMPTY CHECKING
function emptyValidation(frmName, fieldName) {
	if(frmName[fieldName].value==null || frmName[fieldName].value==''){
		return false;
	}else{
		return true;
	}
}

// LOGIN VALIDATION
function loginValidation(){
	var Messages='';
	if(!emptyValidation(frmLogin, 'Username'))
		Messages+='* The "Username" can not be left blank.\n';
	if(!emptyValidation(frmLogin, 'Password'))
		Messages+='* The "Password" can not be left blank.\n';	
	if(Messages!=''){
		alert(Messages);	
		return false;
	}	
}

// MY ACCOUNT VALIDATION
function myaccountValidation(){
	var Messages='';
	if(!emptyValidation(frmMyAccount, 'Username'))
		Messages+='* The "Username" can not be left blank.\n';
	if(!emptyValidation(frmMyAccount, 'Email'))
		Messages+='* The "Email" can not be left blank.\n';	
	if(Messages!=''){
		alert(Messages);	
		return false;
	}	
}

// CHANGE PASSWORD
function changePassword(){
	var Messages='';
	if(!emptyValidation(frmChangePassword, 'OldPwd'))
		Messages+='* The "Old Password" can not be left blank.\n';
	if(!emptyValidation(frmChangePassword, 'NewPwd'))
		Messages+='* The "New Password" can not be left blank.\n';
	if(!emptyValidation(frmChangePassword, 'ConfirmPwd'))
		Messages+='* The "Confirm Password" can not be left blank.\n';
	if(frmChangePassword.NewPwd.value!=frmChangePassword.ConfirmPwd.value)			
		Messages+='* Inconsistent Password.\n';
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
}

// JOB VALIDATION    
function jobValidation(){
	var Messages='';
	if(!emptyValidation(frmJob, 'JobTitle'))
		Messages+='* The "Title" can not be left blank.\n';
	if(!emptyValidation(frmJob, 'Post'))
		Messages+='* The "Post" can not be left blank.\n';	
	if(!emptyValidation(frmJob, 'Experience'))
		Messages+='* The "Experience" can not be left blank.\n';
	if(!emptyValidation(frmJob, 'KeySkills'))
		Messages+='* The "Key Skills" can not be left blank.\n';
	if(!emptyValidation(frmJob, 'Location'))
		Messages+='* The "Location" can not be left blank.\n';
	if(!emptyValidation(frmJob, 'SalaryRange'))
		Messages+='* The "Salary Range" can not be left blank.\n';
	if(!emptyValidation(frmJob, 'LastDate'))
		Messages+='* The "Last Date" can not be left blank.\n';						
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
	// STORE DESCRIPTION
	frmJob.Description.value = oEdit1.getHTMLBody();
}

// TIME VALIDATION
function timeValidation(frmName, fieldName){
	var val = frmName[fieldName].value;
	if(val.length < 5)
		return false;
	else if(val.indexOf(':')!='2')
		return false;
	else if(parseInt(val.substr(0, 2))>23)
		return false;
	else if(parseInt(val.substr(3, 2))>59)		
		return false;
	else	
		return true;
}

// EVENT VALIDATION
function eventValidation(){
	var Messages='';
	if(!emptyValidation(frmEvent, 'EventName'))
		Messages+='* The "Name" can not be left blank.\n';
	if(!emptyValidation(frmEvent, 'StartDate'))
		Messages+='* The "Start Date" can not be left blank.\n';	
	if(!emptyValidation(frmEvent, 'EndDate'))
		Messages+='* The "End Date" can not be left blank.\n';
	if(!emptyValidation(frmEvent, 'Time')){
		Messages+='* The "Time" can not be left blank.\n';
	}else if(!timeValidation(frmEvent, 'Time')){
		Messages+='* Invalid "Time".\n';
	}	
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
	// STORE DESCRIPTION
	frmEvent.Description.value = oEdit1.getHTMLBody();	
}

