<?php	 		 	 /* Smarty version 2.6.1, created on 2011-04-25 00:20:56
         compiled from add_dubai.tpl */ ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "left.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-------------------- Body Section ------------------------>
<?php	 	 echo '
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
'; ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%"  align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  ">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF"><?php	 	 if ($this->_tpl_vars['horse_id'] == NULL): ?>ADD 
            DUBAI WORLD CUP<?php	 	 else: ?> EDIT  DUBAI WORLD CUP<?php	 	 endif; ?></font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  align="center" cellpadding="0" cellspacing="0" >
              <form action="<?php	 	 echo $this->_tpl_vars['action']; ?>
" method="post" enctype="multipart/form-data" name="breeders_form" >
                <tr>
                <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
				  <!--------------------->
				  <tr>
				  <td colspan="4">
				  <table><tr>
            <td  class="fieldname" align="center" valign="top" >
             <?php	 	 if ($this->_tpl_vars['horse_pic'] != NULL):  if ($this->_tpl_vars['img_chk'] == 1): ?>
			 <img src="<?php	 	 echo $this->_tpl_vars['horse_pic']; ?>
" border="1">
			 <?php	 	 else: ?>
			 <img src="../uploaded_images/horse_images/thumb<?php	 	 echo $this->_tpl_vars['horse_pic']; ?>
" border="1"><?php	 	 endif;  endif; ?>            </td>
            <td   align="center" valign="top">
              <?php	 	 if ($this->_tpl_vars['horse_silkpic'] != NULL): ?>
			  <?php	 	 if ($this->_tpl_vars['silkimg_chk'] == 1): ?>
			 <img src="<?php	 	 echo $this->_tpl_vars['horse_silkpic']; ?>
" border="1">
			 <?php	 	 else: ?>
			  <img src="../uploaded_images/horse_images/thumb<?php	 	 echo $this->_tpl_vars['horse_silkpic']; ?>
" border="1"><?php	 	 endif;  endif; ?>            </td>
			</tr>
			</table></td>
          </tr>
				  
				  <!---------------------->
                      
                      <tr>
            <td width='150' class="fieldname">
              Year            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="year" value="<?php	 	 echo $this->_tpl_vars['year']; ?>
" >            </td>
          </tr>
		   <tr>
            <td class="fieldname">
              Winner            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="winner" value="<?php	 	 echo $this->_tpl_vars['winner']; ?>
" >             </td>
          </tr><tr>
            <td width='150' class="fieldname">
              Jockey            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="jockey" value="<?php	 	 echo $this->_tpl_vars['jockey']; ?>
" > <!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php	 	 if ($this->_tpl_vars['horse_id'] != NULL): ?><a href="trainer_view.php?trainer=<?php	 	 echo $this->_tpl_vars['horse_trainer']; ?>
" class="link">See the trainer</a><?php	 	 endif; ?>-->            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Age            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="age" value="<?php	 	 echo $this->_tpl_vars['age']; ?>
" > <!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php	 	 if ($this->_tpl_vars['horse_id'] != NULL): ?><a href="owner_view.php?owner=<?php	 	 echo $this->_tpl_vars['horse_owner']; ?>
" class="link">See the owner</a><?php	 	 endif; ?>-->            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Trainer            </td>
            <td width="244" class="columnHeaderStatic">
              <input type="text" name="trainer" value="<?php	 	 echo $this->_tpl_vars['trainer']; ?>
" >           
          </tr><tr>
            <td width='150' class="fieldname">
              Owner            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="owner" value="<?php	 	 echo $this->_tpl_vars['owner']; ?>
" >            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Time            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="timer" value="<?php	 	 echo $this->_tpl_vars['timer']; ?>
" >            </td>

          </tr>
                      <tr>
                        <td colspan="5" class="fieldname"><div align="center">
						<input type="hidden" name="horse_id" value="<?php	 	 echo $this->_tpl_vars['horse_id']; ?>
">
						 <input type="hidden" name="horse_pic" value="<?php	 	 echo $this->_tpl_vars['horse_pic']; ?>
">
						 <input type="hidden" name="horse_silkpic" value="<?php	 	 echo $this->_tpl_vars['horse_silkpic']; ?>
">
						 <input type="hidden" name="start" value="<?php	 	 echo $this->_tpl_vars['start']; ?>
" />
						  <input type="hidden" name="group_stakes" value="<?php	 	 echo $this->_tpl_vars['group_stakes']; ?>
" />
						   <input type="hidden" name="act_BreedersCup" value="<?php	 	 echo $this->_tpl_vars['act_BreedersCup']; ?>
" />
						   <input type="hidden" name="search_BreedersCup" value="<?php	 	 echo $this->_tpl_vars['search_BreedersCup']; ?>
" />
                           <?php	 	 if ($this->_tpl_vars['horse_id'] == NULL): ?> <input type="image" name="Submit" src="images/submit-button.gif"  style="width:62px;">
						   <?php	 	 else: ?><input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;"><?php	 	 endif; ?>

                        </div></td>
                      </tr>
					  
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td></tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
