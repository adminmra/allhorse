<?php	 		 	 /* Smarty version 2.6.1, created on 2010-04-17 05:27:38
         compiled from stake_view.tpl */ ?>
<?php	 	 require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'stake_view.tpl', 50, false),)), $this); ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "left.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 echo '
<style type="text/css">

</style>
<script type="text/javascript">
function LoadStakes(a)
{
	//alert(a.value);
	if(a.value == 0)
	{
	window.location="stake_view.php";
	}else
	{
	window.location="?act_stakes=track&code="+a.value;
	}
}
</script>
'; ?>

<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><?php	 	 echo '<?'; ?>
 //include "patient_manage_top.php"; <?php	 	 echo '?>'; ?>
</td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <td height="30" align="right" bgcolor="#FFFFFF" style="padding-right:10px;"><strong class="bodyb"><?php	 	 echo $this->_tpl_vars['paging']; ?>
</strong></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">STAKES</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22" align="left"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" bgcolor="#E4EBF6" align="center" width="10%" ><a href="add_stakes.php" class="one">Add</a>&nbsp;|&nbsp;<a href="stake_view.php" class="one">View List</a></td><td bgcolor="#FFFFFF" align="right">
				<select name="racetrack"  onChange="return LoadStakes(this);">
				<option value="0">Select Racetrack</option>
				<?php	 	 echo smarty_function_html_options(array('options' => $this->_tpl_vars['racetracks'],'selected' => $this->_tpl_vars['mySelect']), $this);?>

				</select>
				</td>
               
                
              </tr>
            </table></td>
          </tr>
		  <!-------------------------------->
		  <tr align="center">
			  <td colspan="2" width="100%">
			  <table width="100%" bgcolor="#FFFFFF">
			  <tr>
			  <form name="stakes_search" method="post" action="?act_stakes=search">
                <td height="22" width="35%" bgcolor="#FFFFFF" colspan="1"> Search by Name : <input type="text" name="search_stakes" style="width:100px; " value="<?php	 	 echo $this->_tpl_vars['search_value']; ?>
" ></td>
			   
			   <td height="22" width="65%" bgcolor="#FFFFFF" colspan="4" align="left"><input type="image" src="images/botton-seeresults.gif" style="width:85px; height:18px;" />&nbsp; <strong style="padding-bottom:5px;">Total records : <?php	 	 echo $this->_tpl_vars['record_count']; ?>
</strong>
				                                            </td>
				
                  
				 </form>          
              </tr>
			  </table>
			  </td>
			  </tr>
		  <!--------------------------------->
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" rowspan="2" bgcolor="#0D4686"><a href="?sort_by=name" class="seven"><font style="color:#FFFFFF;"><b>Stakes name</b></font></a></td>
            
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Trackcode</b></font></td>
			 <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><a href="?sort_by=date" class="seven"><font style="color:#FFFFFF;"><b>Date</b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Purse</b></font></td>
			<td height="20" bgcolor="#0D4686" rowspan="2" class="header"><a href="?sort_by=horseage" class="seven"><font style="color:#FFFFFF;"><b>Horse Age</b></font></a></td>
			<td bgcolor="#0D4686" rowspan="2" class="bodyb"><font style="color:#FFFFFF;"><b>Stake Sex</b></font></td>
			<td height="20" bgcolor="#0D4686" rowspan="2"><a href="?sort_by=grade" class="seven"><font style="color:#FFFFFF;"><b>Grade</b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><a href="?sort_by=distance" class="seven"><font style="color:#FFFFFF;"><b>Distance</b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Turf</b></font></td>
            	
            <td height="20" bgcolor="#0D4686" rowspan="2" colspan="2" class="header"><font style="color:#FFFFFF;"><b>Action</b></font></td>
           <!-- <td height="40" rowspan="2" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">Action</font></td>-->
		  </tr>
          <tr align="center">
           
            </tr>
			<?php	 	 $this->assign('indexvalue', 0); ?>
					  <?php	 	 if (isset($this->_sections['stake'])) unset($this->_sections['stake']);
$this->_sections['stake']['name'] = 'stake';
$this->_sections['stake']['loop'] = is_array($_loop=$this->_tpl_vars['stake_id']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['stake']['show'] = true;
$this->_sections['stake']['max'] = $this->_sections['stake']['loop'];
$this->_sections['stake']['step'] = 1;
$this->_sections['stake']['start'] = $this->_sections['stake']['step'] > 0 ? 0 : $this->_sections['stake']['loop']-1;
if ($this->_sections['stake']['show']) {
    $this->_sections['stake']['total'] = $this->_sections['stake']['loop'];
    if ($this->_sections['stake']['total'] == 0)
        $this->_sections['stake']['show'] = false;
} else
    $this->_sections['stake']['total'] = 0;
if ($this->_sections['stake']['show']):

            for ($this->_sections['stake']['index'] = $this->_sections['stake']['start'], $this->_sections['stake']['iteration'] = 1;
                 $this->_sections['stake']['iteration'] <= $this->_sections['stake']['total'];
                 $this->_sections['stake']['index'] += $this->_sections['stake']['step'], $this->_sections['stake']['iteration']++):
$this->_sections['stake']['rownum'] = $this->_sections['stake']['iteration'];
$this->_sections['stake']['index_prev'] = $this->_sections['stake']['index'] - $this->_sections['stake']['step'];
$this->_sections['stake']['index_next'] = $this->_sections['stake']['index'] + $this->_sections['stake']['step'];
$this->_sections['stake']['first']      = ($this->_sections['stake']['iteration'] == 1);
$this->_sections['stake']['last']       = ($this->_sections['stake']['iteration'] == $this->_sections['stake']['total']);
?>
          <tr align="center">
            <td height="18" bgcolor="#3D73B8" align="left"><a href="add_stakes.php?stake_id=<?php	 	 echo $this->_tpl_vars['stake_id'][$this->_sections['stake']['index']]; ?>
&start=<?php	 	 echo $this->_tpl_vars['start']; ?>
" class="seven2">
             <?php	 	 echo $this->_tpl_vars['stake_name'][$this->_sections['stake']['index']]; ?>
</a></td>
            <td height="18" bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['stake_trackcode'][$this->_sections['stake']['index']]; ?>
</td>
			<td height="18" bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['stake_date'][$this->_sections['stake']['index']]; ?>
</td>
            <td height="18" bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['stake_purse'][$this->_sections['stake']['index']]; ?>
</td>
			<td height="18" bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['stake_horseage'][$this->_sections['stake']['index']]; ?>
</td>
			<td height="18" bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['stake_stakessex'][$this->_sections['stake']['index']]; ?>
</td>
			<td height="18" bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['stake_grade'][$this->_sections['stake']['index']]; ?>
</td>
			<td height="18" bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['stake_distance'][$this->_sections['stake']['index']]; ?>
</td>
			<td height="18" bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['stake_turf'][$this->_sections['stake']['index']]; ?>
</td>
            
            <td bgcolor="#B5DE97"><a href="add_stakes.php?stake_id=<?php	 	 echo $this->_tpl_vars['stake_id'][$this->_sections['stake']['index']]; ?>
&start=<?php	 	 echo $this->_tpl_vars['start']; ?>
" class="seven">[Edit]</a></td>
            <td bgcolor="#B5DE97"><a href="?act=delete&stake_id=<?php	 	 echo $this->_tpl_vars['stake_id'][$this->_sections['stake']['index']]; ?>
" onClick="return confirm('Are you sure to delete?')" class="seven">[Delete]</a><!--<input type="checkbox" name="checkbox" value="checkbox" style="border-color:#B5DE97;" />--></td>
          </tr>
		 <?php	 	 $this->assign('indexvalue', $this->_tpl_vars['indexvalue']+1); ?>
		 <?php	 	 endfor; else: ?>
		  <tr align="center">
            <td height="30" bgcolor="#3D73B8" colspan="11"><font style="color:#FFFFFF;"><strong>No data available</strong></font></td>
			</tr>
                    <?php	 	 endif; ?>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.php", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>