<?php	 		 	 /* Smarty version 2.6.1, created on 2010-11-18 05:30:20
         compiled from add_trainer.tpl */ ?>
<?php	 	 require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'add_trainer.tpl', 63, false),)), $this); ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "left.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-------------------- Body Section ------------------------>
<?php	 	 echo '
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
<style type="text/css">

</style>
'; ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  " align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF"><?php	 	 if ($this->_tpl_vars['trainer_id'] == NULL): ?>ADD 
            <?php	 	 else: ?> EDIT<?php	 	 endif; ?> TRAINER</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>&nbsp;&nbsp;&nbsp;
		  <a href="trainer_view.php"><strong><font color="#FFFFFF">&laquo; Back to Trainers</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="trainers_form" onSubmit="return chk_trainer('<?php	 	 echo $this->_tpl_vars['trainer_id']; ?>
');">
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      <tr>
				  <td colspan="2">
				  <table><tr>
            <td  align="left" valign="top">
             <?php	 	 if ($this->_tpl_vars['trainer_pic'] != NULL): ?>
			 <?php	 	 if ($this->_tpl_vars['img_chk'] == 1): ?>
			 <img src="<?php	 	 echo $this->_tpl_vars['trainer_pic']; ?>
" border="1">
			 <?php	 	 else: ?>
			 <img src="../uploaded_images/trainer_images/thumb<?php	 	 echo $this->_tpl_vars['trainer_pic']; ?>
" border="1"><?php	 	 endif;  endif; ?>            </td>
            
			</tr>
			</table></td>
          </tr>
                      <tr>
            <td width='150' class="fieldname">
              Name            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="trainer_name" value="<?php	 	 echo $this->_tpl_vars['trainer_name']; ?>
" id="affiliateFirstName">            </td>
          </tr>
		   <tr>
            <td class="fieldname">
              Country            </td>
            <td class="columnHeaderStatic">

			<select name="trainer_country" >
			<?php	 	 echo smarty_function_html_options(array('values' => $this->_tpl_vars['country_name'],'output' => $this->_tpl_vars['country_name'],'selected' => $this->_tpl_vars['trainer_country']), $this);?>

			</select>            </td>
          </tr><tr>
            <td width='150' class="fieldname">
              State            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="trainer_state" value="<?php	 	 echo $this->_tpl_vars['trainer_state']; ?>
" id="affiliateFirstName">            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              City            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="trainer_city" value="<?php	 	 echo $this->_tpl_vars['trainer_city']; ?>
" id="affiliateFirstName">            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Family            </td>
            <td class="columnHeaderStatic">
              <textarea name="trainer_family" id="affiliateFirstName"><?php	 	 echo $this->_tpl_vars['trainer_family']; ?>
</textarea>            </td>

          </tr> 
          <tr>
            <td class="fieldname" valign="top">AHR Notes</td>
            <td class="columnHeaderStatic"><textarea name="trainer_notes" id="textarea"><?php	 	 echo $this->_tpl_vars['trainer_notes']; ?>
</textarea></td>
          </tr>
		  <tr>
            <td class="fieldname" valign="top">GHB Notes</td>
            <td class="columnHeaderStatic"><textarea name="trainer_notes_GHB" id="textarea"><?php	 	 echo $this->_tpl_vars['trainer_notes_GHB']; ?>
</textarea></td>
          </tr>
		  <tr>
            <td class="fieldname" valign="top">HB Notes</td>
            <td class="columnHeaderStatic"><textarea name="trainer_notes_HB" id="textarea"><?php	 	 echo $this->_tpl_vars['trainer_notes_HB']; ?>
</textarea></td>
          </tr>
          <tr>
            <td width='150' class="fieldname">
              Url            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="trainer_url" value="<?php	 	 echo $this->_tpl_vars['trainer_url']; ?>
" id="affiliateFirstName">            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Birth Date            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="trainer_birthdate" value="<?php	 	 echo $this->_tpl_vars['trainer_birthdate']; ?>
" id="affiliateFirstName">            </td>
          </tr>
		   <tr>
            <td width='150' class="fieldname">
              Birth Country            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="trainer_birth_country" value="<?php	 	 echo $this->_tpl_vars['trainer_birth_country']; ?>
" id="affiliateFirstName">            </td>
          </tr> 
		  
		  <tr>
            <td width='150' class="fieldname">
              Birth State            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="trainer_birth_state" value="<?php	 	 echo $this->_tpl_vars['trainer_birthstate']; ?>
" id="">            </td>
          </tr> <tr>
            <td width='150' class="fieldname">
              Birth City            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="trainer_birth_city" value="<?php	 	 echo $this->_tpl_vars['trainer_birthcity']; ?>
" id="affiliateFirstName">            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Image            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="trainer_image" value="<?php	 	 echo $this->_tpl_vars['trainer_pic']; ?>
" id="affiliateFirstName">            </td>

          </tr>
                      <tr>
                        <td colspan="2" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="trainer_id" value="<?php	 	 echo $this->_tpl_vars['trainer_id']; ?>
">
						 <input type="hidden" name="trainer_pic" value="<?php	 	 echo $this->_tpl_vars['trainer_pic']; ?>
">
						 <input type="hidden" name="start" value="<?php	 	 echo $this->_tpl_vars['start']; ?>
" />
						 <input type="hidden" name="act_trainer" value="<?php	 	 echo $this->_tpl_vars['act_trainer']; ?>
">
						 <input type="hidden" name="search_trainer" value="<?php	 	 echo $this->_tpl_vars['search_trainer']; ?>
">
                           <?php	 	 if ($this->_tpl_vars['trainer_id'] == NULL): ?><input type="image" name="Submit" src="images/submit-button.gif"  style="width:62px;">
						   <?php	 	 else: ?><input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;"><?php	 	 endif; ?>

                        </div></td>
                      </tr>
					  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import all records from .CSV file 
                        </div></td>
                      </tr><tr>
                        <td colspan="3" class="fieldname"><div align="center" style="padding-right:100px ">
						<input type="file" name="import_trainer">
                        </div></td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
