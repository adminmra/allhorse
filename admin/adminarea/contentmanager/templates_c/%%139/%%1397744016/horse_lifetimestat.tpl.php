<?php	 		 	 /* Smarty version 2.6.1, created on 2009-04-29 22:16:04
         compiled from horse_lifetimestat.tpl */ ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "left.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><?php	 	 echo '<?'; ?>
 //include "patient_manage_top.php"; <?php	 	 echo '?>'; ?>
</td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <td height="30" align="right" bgcolor="#FFFFFF" style="padding-right:10px;"><strong class="bodyb">&#8249; </strong><a href="#" class="one"><b>Previous</b></a> | <a href="#" class="one"><b>Next</b></a> <strong class="one">&#8250;</strong></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">HORSE LIFETIME STATISTICS</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="40%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" bgcolor="#E4EBF6"><a href="add_horse_statslifetime.php?horse_id=<?php	 	 echo $this->_tpl_vars['horse_id']; ?>
" class="one">Add</a></td>
                <td height="22" bgcolor="#E4EBF6"><a href="horse_view.php" class="one">Back to Horse </a></td>
				<td height="22" bgcolor="#E4EBF6">Total Records : <?php	 	 echo $this->_tpl_vars['total_records']; ?>
</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" rowspan="2" bgcolor="#0D4686"><font style="color:#FFFFFF;"><b>Name</b></font></td>
			<!-- <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Year</b></font></td>-->
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Earning</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Races run</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Wins</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Places</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Shows</b></font></td>
            <td bgcolor="#0D4686" rowspan="2" class="bodyb"><font style="color:#FFFFFF;"><b>Edit</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Delete</b></font></td>
		  </tr>
          <tr align="center">
           
            </tr>
			<?php	 	 if (isset($this->_sections['horse_statslifetime'])) unset($this->_sections['horse_statslifetime']);
$this->_sections['horse_statslifetime']['name'] = 'horse_statslifetime';
$this->_sections['horse_statslifetime']['loop'] = is_array($_loop=$this->_tpl_vars['horsestatslifetime_id']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['horse_statslifetime']['show'] = true;
$this->_sections['horse_statslifetime']['max'] = $this->_sections['horse_statslifetime']['loop'];
$this->_sections['horse_statslifetime']['step'] = 1;
$this->_sections['horse_statslifetime']['start'] = $this->_sections['horse_statslifetime']['step'] > 0 ? 0 : $this->_sections['horse_statslifetime']['loop']-1;
if ($this->_sections['horse_statslifetime']['show']) {
    $this->_sections['horse_statslifetime']['total'] = $this->_sections['horse_statslifetime']['loop'];
    if ($this->_sections['horse_statslifetime']['total'] == 0)
        $this->_sections['horse_statslifetime']['show'] = false;
} else
    $this->_sections['horse_statslifetime']['total'] = 0;
if ($this->_sections['horse_statslifetime']['show']):

            for ($this->_sections['horse_statslifetime']['index'] = $this->_sections['horse_statslifetime']['start'], $this->_sections['horse_statslifetime']['iteration'] = 1;
                 $this->_sections['horse_statslifetime']['iteration'] <= $this->_sections['horse_statslifetime']['total'];
                 $this->_sections['horse_statslifetime']['index'] += $this->_sections['horse_statslifetime']['step'], $this->_sections['horse_statslifetime']['iteration']++):
$this->_sections['horse_statslifetime']['rownum'] = $this->_sections['horse_statslifetime']['iteration'];
$this->_sections['horse_statslifetime']['index_prev'] = $this->_sections['horse_statslifetime']['index'] - $this->_sections['horse_statslifetime']['step'];
$this->_sections['horse_statslifetime']['index_next'] = $this->_sections['horse_statslifetime']['index'] + $this->_sections['horse_statslifetime']['step'];
$this->_sections['horse_statslifetime']['first']      = ($this->_sections['horse_statslifetime']['iteration'] == 1);
$this->_sections['horse_statslifetime']['last']       = ($this->_sections['horse_statslifetime']['iteration'] == $this->_sections['horse_statslifetime']['total']);
?>
          <tr align="center">
            <td height="18" bgcolor="#3D73B8"><font style="color:#FFFFFF;">
             <?php	 	 echo $this->_tpl_vars['horse_name']; ?>
</font></td>
           <!-- <td height="18" bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['year_stat'][$this->_sections['horse_statslifetime']['index']]; ?>
</td>-->
            <td height="18" bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['earning_stat'][$this->_sections['horse_statslifetime']['index']]; ?>
</td>
            <td bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['starts_stat'][$this->_sections['horse_statslifetime']['index']]; ?>
</td>
            <td bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['win_stat'][$this->_sections['horse_statslifetime']['index']]; ?>
</td>
            <td bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['places_stat'][$this->_sections['horse_statslifetime']['index']]; ?>
</td>
            <td bgcolor="#B5DE97"><?php	 	 echo $this->_tpl_vars['show_stat'][$this->_sections['horse_statslifetime']['index']]; ?>
</td>
            <td bgcolor="#B5DE97"><a href="add_horse_statslifetime.php?horsestatslifetime_id=<?php	 	 echo $this->_tpl_vars['horsestatslifetime_id'][$this->_sections['horse_statslifetime']['index']]; ?>
&horse_id=<?php	 	 echo $this->_tpl_vars['horse_id']; ?>
" class="seven">Edit</a></td>
            <td bgcolor="#B5DE97"><a href="?horsestatslifetime_id=<?php	 	 echo $this->_tpl_vars['horsestatslifetime_id'][$this->_sections['horse_statslifetime']['index']]; ?>
&horse_id=<?php	 	 echo $this->_tpl_vars['horse_id']; ?>
&act=del" onClick="return confirm('Are you sure?');" class="seven">Delete</a></td>
          </tr>
		  <?php	 	 endfor; else: ?>
		  <tr><td height="50" bgcolor="#3D73B8" colspan="10" align="center"><font style="color:#FFFFFF;"><strong>
             No lifetime statistics available</strong></font></td></tr>
			 <?php	 	 endif; ?>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.php", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>