<?php	 		 	 /* Smarty version 2.6.1, created on 2009-04-27 22:42:11
         compiled from add_owner.tpl */ ?>
<?php	 	 require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count_characters', 'add_owner.tpl', 26, false),array('function', 'html_options', 'add_owner.tpl', 59, false),)), $this); ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "left.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-------------------- Body Section ------------------------>
<?php	 	 echo '
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
'; ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%"  align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  ">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF"><?php	 	 if ($this->_tpl_vars['owner_id'] == NULL): ?>ADD 
            OWNER<?php	 	 else: ?> EDIT OWNER<?php	 	 endif; ?></font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>&nbsp;
		  <a href="owner_view.php" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back to owners</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
		<?php	 	 if (((is_array($_tmp=$this->_tpl_vars['pic'])) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp)) > 3): ?>
		<tr> 
          <td height="10" colspan="2" align="center"><img src="<?php	 	 echo $this->_tpl_vars['pic']; ?>
" ></td>
        </tr>
		<?php	 	 endif; ?>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="owners_form" onSubmit="return chk_owner('<?php	 	 echo $this->_tpl_vars['owner_id']; ?>
');">
                <tr>
                <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
				  <!--------------------->
				  
				  
				  <!---------------------->
                      
                      <tr>
            <td width='150' class="fieldname">
              Name
            </td>
            <td class="columnHeaderStatic">
              <input type="text"  name="owner_name" value="<?php	 	 echo $this->_tpl_vars['owner_name']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		   <tr>
            <td class="fieldname">
              Country</td>
            <td class="columnHeaderStatic">
             <select name="owner_country" >
			<?php	 	 echo smarty_function_html_options(array('values' => $this->_tpl_vars['country_name'],'output' => $this->_tpl_vars['country_name'],'selected' => $this->_tpl_vars['owner_country']), $this);?>

			</select>
             </td>
          </tr><tr>
            <td width='150' class="fieldname">
              State</td>
            <td class="columnHeaderStatic">
              <input type="text" name="owner_state" value="<?php	 	 echo $this->_tpl_vars['owner_state']; ?>
" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              City</td>
            <td class="columnHeaderStatic">
              <input type="text" name="owner_city" value="<?php	 	 echo $this->_tpl_vars['owner_city']; ?>
" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Stable</td>
            <td class="columnHeaderStatic">
              <input type="text" name="owner_stable" value="<?php	 	 echo $this->_tpl_vars['owner_stable']; ?>
" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              family</td>
            <td class="columnHeaderStatic">
              <input type="text" name="owner_family" value="<?php	 	 echo $this->_tpl_vars['owner_family']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Url</td>
            <td class="columnHeaderStatic">
              <input type="text" name="owner_url" value="<?php	 	 echo $this->_tpl_vars['owner_url']; ?>
" id="affiliateFirstName">
            </td>
          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Notes
            </td>
            <td class="columnHeaderStatic">
              <textarea name="owner_notes" rows="3"><?php	 	 echo $this->_tpl_vars['owner_notes']; ?>
</textarea>
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Image
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="pic" value="<?php	 	 echo $this->_tpl_vars['pic']; ?>
">
            </td>

          </tr>
		  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import all records from .CSV file 
                        </div></td>
                      </tr>
		  <tr>
                        <td colspan="3" class="fieldname"><div align="center" style="padding-right:100px ">
						<input type="file" name="import_owner">
                        </div></td>
                      </tr>
                      <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						<input type="hidden" name="owner_id" value="<?php	 	 echo $this->_tpl_vars['owner_id']; ?>
">
						 <input type="hidden" name="owner_pic" value="<?php	 	 echo $this->_tpl_vars['owner_pic']; ?>
">
						 <input type="hidden" name="owner_silkpic" value="<?php	 	 echo $this->_tpl_vars['owner_silkpic']; ?>
">
						 <input type="hidden" name="start" value="<?php	 	 echo $this->_tpl_vars['start']; ?>
" />
                           <?php	 	 if ($this->_tpl_vars['owner_id'] == NULL): ?><input type="image" name="Submit" src="images/submit-button.gif"  style="width:62px;">
						   <?php	 	 else: ?><input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;"><?php	 	 endif; ?>

                        </div></td>
                      </tr>
					 
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td></tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
