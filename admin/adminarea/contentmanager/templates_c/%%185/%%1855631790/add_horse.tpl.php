<?php	 		 	 /* Smarty version 2.6.1, created on 2010-11-17 05:50:47
         compiled from add_horse.tpl */ ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "left.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-------------------- Body Section ------------------------>
<?php	 	 echo '
<script language="javascript" src="js/validation.js" type="text/javascript"></script>


'; ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%"  align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  ">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF"><?php	 	 if ($this->_tpl_vars['horse_id'] == NULL): ?>ADD 
            HORSE<?php	 	 else: ?> EDIT HORSE<?php	 	 endif; ?></font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">&nbsp;
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>
		  <!--</td>
		  <td align="right" bgcolor="#0D4686" style="padding-right:10px;">-->
		  <a href="horse_view.php" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back to horses</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  align="center" cellpadding="0" cellspacing="0" >
              <form action="" method="post" enctype="multipart/form-data" name="horses_form" onSubmit="return chk_horse('<?php	 	 echo $this->_tpl_vars['horse_id']; ?>
');">
                <tr>
                <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
				  <!--------------------->
				  <tr>
				  <td colspan="2">
				  <table><tr>
            <td  class="fieldname" align="center" valign="top" >
             <?php	 	 if ($this->_tpl_vars['horse_pic'] != NULL):  if ($this->_tpl_vars['img_chk'] == 1): ?>
			 <img src="<?php	 	 echo $this->_tpl_vars['horse_pic']; ?>
" border="1">
			 <?php	 	 else: ?>
			 <img src="../uploaded_images/horse_images/thumb<?php	 	 echo $this->_tpl_vars['horse_pic']; ?>
" border="1"><?php	 	 endif;  endif; ?>
            </td>
            <td   align="center" valign="top">
              <?php	 	 if ($this->_tpl_vars['horse_silkpic'] != NULL): ?>
			  <?php	 	 if ($this->_tpl_vars['silkimg_chk'] == 1): ?>
			 <img src="<?php	 	 echo $this->_tpl_vars['horse_silkpic']; ?>
" border="1">
			 <?php	 	 else: ?>
			  <img src="../uploaded_images/horse_images/thumb<?php	 	 echo $this->_tpl_vars['horse_silkpic']; ?>
" border="1"><?php	 	 endif;  endif; ?>
            </td>
			</tr>
			</table>
</td>
          </tr>
				  
				  <!---------------------->
                      
                      <tr>
            <td width='150' class="fieldname">
              Name
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_name" value="<?php	 	 echo $this->_tpl_vars['horse_name']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		   <tr>
            <td class="fieldname">
              Status
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_status" value="<?php	 	 echo $this->_tpl_vars['horse_status']; ?>
" id="affiliateFirstName">
             </td>
          </tr><tr>
            <td width='150' class="fieldname">
              Trainer
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_trainer" value="<?php	 	 echo $this->_tpl_vars['horse_trainer']; ?>
" id="affiliateFirstName"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php	 	 if ($this->_tpl_vars['horse_id'] != NULL): ?><a href="trainer_view.php?trainer=<?php	 	 echo $this->_tpl_vars['horse_trainer']; ?>
" class="link">See the trainer</a><?php	 	 endif; ?>
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Owner
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_owner" value="<?php	 	 echo $this->_tpl_vars['horse_owner']; ?>
" id="affiliateFirstName"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php	 	 if ($this->_tpl_vars['horse_id'] != NULL): ?><a href="owner_view.php?owner=<?php	 	 echo $this->_tpl_vars['horse_owner']; ?>
" class="link">See the owner</a><?php	 	 endif; ?>
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Foaling
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_foaling" value="<?php	 	 echo $this->_tpl_vars['horse_foaling']; ?>
" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Sire
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_sire" value="<?php	 	 echo $this->_tpl_vars['horse_sire']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Dam
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_dam" value="<?php	 	 echo $this->_tpl_vars['horse_dam']; ?>
" id="affiliateFirstName">
            </td>
          </tr>
		   <tr>
            <td width='150' class="fieldname">
              Damesire
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_damsire" value="<?php	 	 echo $this->_tpl_vars['horse_damsire']; ?>
" id="affiliateFirstName">
            </td>

          </tr> 
		  
		  <tr>
            <td width='150' class="fieldname">
              Dosage
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_dosage" value="<?php	 	 echo $this->_tpl_vars['horse_dosage']; ?>
" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Dosage Index
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_dosage_index" value="<?php	 	 echo $this->_tpl_vars['horse_dosageindex']; ?>
" id="affiliateFirstName">
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              CD
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_cd" value="<?php	 	 echo $this->_tpl_vars['horse_cd']; ?>
" id="affiliateFirstName">
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname" valign="top">
             AHR Notes
            </td>
            <td class="columnHeaderStatic">
              <textarea name="horse_notes" rows="3"><?php	 	 echo $this->_tpl_vars['horse_notes']; ?>
</textarea>
            </td>

          </tr> 
		   <tr>
            <td width='150' class="fieldname" valign="top">
              GHB Notes
            </td>
            <td class="columnHeaderStatic">
              <textarea name="horse_notes_GHB" rows="3"><?php	 	 echo $this->_tpl_vars['horse_notes_GHB']; ?>
</textarea>
            </td>

          </tr> 
		   <tr>
            <td width='150' class="fieldname" valign="top">
              HB Notes
            </td>
            <td class="columnHeaderStatic">
              <textarea name="horse_notes_HB" rows="3"><?php	 	 echo $this->_tpl_vars['horse_notes_HB']; ?>
</textarea>
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Best Beyer Figure
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_bestbuyer" value="<?php	 	 echo $this->_tpl_vars['horse_bestbuyer']; ?>
" id="affiliateFirstName">
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Stable
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_stable" value="<?php	 	 echo $this->_tpl_vars['horse_stable']; ?>
" id="affiliateFirstName">
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Picture
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_image" value="<?php	 	 echo $this->_tpl_vars['horse_pic']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Silk Picture
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_silkimage" value="<?php	 	 echo $this->_tpl_vars['horse_silkpic']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import all records from .CSV file
                        </div></td>
                      </tr><tr>
                        <td colspan="2" class="fieldname">
						<div align="center" style="padding-right:100px ">
						<input type="file" name="import_horse">
                        </div></td>
                      </tr>
                      <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						<input type="hidden" name="horse_id" value="<?php	 	 echo $this->_tpl_vars['horse_id']; ?>
">
						 <input type="hidden" name="horse_pic" value="<?php	 	 echo $this->_tpl_vars['horse_pic']; ?>
">
						 <input type="hidden" name="horse_silkpic" value="<?php	 	 echo $this->_tpl_vars['horse_silkpic']; ?>
">
						 <input type="hidden" name="start" value="<?php	 	 echo $this->_tpl_vars['start']; ?>
" />
						 <input type="hidden" name="act_horse" value="<?php	 	 echo $this->_tpl_vars['act_horse']; ?>
">
						 <input type="hidden" name="search_horse" value="<?php	 	 echo $this->_tpl_vars['search_horse']; ?>
">
                           <?php	 	 if ($this->_tpl_vars['horse_id'] == NULL): ?> <input type="image" name="Submit" src="images/submit-button.gif"  style="width:62px;">
						   <?php	 	 else: ?><input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;"><?php	 	 endif; ?>
							
                        </div></td>
                      </tr>
					  
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td></tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
