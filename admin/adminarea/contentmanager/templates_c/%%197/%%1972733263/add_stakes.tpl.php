<?php	 		 	 /* Smarty version 2.6.1, created on 2010-11-18 05:21:30
         compiled from add_stakes.tpl */ ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "left.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-------------------- Body Section ------------------------>
<?php	 	 echo '
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
<style type="text/css">

</style>
'; ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  " align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">ADD 
            STAKES</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>&nbsp;
		  <a href="stake_view.php"><strong><font color="#FFFFFF">&laquo; Back to Stakes</font></strong></a> <a href="horse_history.php?stakes=<?php	 	 echo $this->_tpl_vars['stake_id']; ?>
"><strong><font color="#FFFFFF">&laquo; Add Historical Data</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="stakes_form" onSubmit="return chk_stake('<?php	 	 echo $this->_tpl_vars['stake_id']; ?>
');">
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      
                      <tr>
            <td width='150' class="fieldname">
              Stakes Name            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_name" value="<?php	 	 echo $this->_tpl_vars['stake_name']; ?>
" id="affiliateFirstName">            </td>
          </tr><tr>
            <td width='150' class="fieldname">
              Trackcode            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_trackcode" value="<?php	 	 echo $this->_tpl_vars['stake_trackcode']; ?>
" id="affiliateFirstName">            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Date            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_date" value="<?php	 	 echo $this->_tpl_vars['stake_date']; ?>
" id="affiliateFirstName">            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Grade            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_grade" value="<?php	 	 echo $this->_tpl_vars['stake_grade']; ?>
" id="affiliateFirstName">            </td>
          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Purse            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_purse" value="<?php	 	 echo $this->_tpl_vars['stake_purse']; ?>
" id="affiliateFirstName">            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              HTA</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_hta" value="<?php	 	 echo $this->_tpl_vars['stake_hta']; ?>
" id="affiliateFirstName">            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Distance            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_distance" value="<?php	 	 echo $this->_tpl_vars['stake_distance']; ?>
" id="affiliateFirstName">            </td>
          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Horse Age</td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_age" value="<?php	 	 echo $this->_tpl_vars['horse_age']; ?>
" id="affiliateFirstName">            </td>
          </tr>
		   <tr>
            <td width='150' class="fieldname">
              Stake sex</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_sex" value="<?php	 	 echo $this->_tpl_vars['stake_sex']; ?>
" id="affiliateFirstName">            </td>
          </tr> 
		  
		  <tr>
            <td width='150' class="fieldname">
              Turf</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_turf" value="<?php	 	 echo $this->_tpl_vars['stake_turf']; ?>
" id="">            </td>
          </tr> 
		  <tr>
		    <td class="fieldname">Post Time </td>
		    <td class="columnHeaderStatic"><input type="text" name="stake_posttime" value="<?php	 	 echo $this->_tpl_vars['stake_posttime']; ?>
" id="stake_posttime" /></td>
		    </tr>
		  <tr>
            <td width='150' class="fieldname" valign="top">
             AHR Notes            </td>
            <td class="columnHeaderStatic">
               <textarea name="stake_notes"  id="affiliateFirstName" rows="4"><?php	 	 echo $this->_tpl_vars['stake_notes']; ?>
</textarea>            </td>
          </tr>
		   <tr>
            <td width='150' class="fieldname" valign="top">
            GHB Notes            </td>
            <td class="columnHeaderStatic">
               <textarea name="stake_notes_GHB"  id="affiliateFirstName" rows="4"><?php	 	 echo $this->_tpl_vars['stake_notes_GHB']; ?>
</textarea>            </td>
          </tr>
		  		  <tr>
            <td width='150' class="fieldname" valign="top">
             HB Notes            </td>
            <td class="columnHeaderStatic">
               <textarea name="stake_notes_HB"  id="affiliateFirstName" rows="4"><?php	 	 echo $this->_tpl_vars['stake_notes_HB']; ?>
</textarea>            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Logo</td>
            <td class="columnHeaderStatic">
               <input type="file" name="logo" >            </td>
          </tr>
		  
		  
                      <tr>
                        <td colspan="2" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="stake_id" value="<?php	 	 echo $this->_tpl_vars['stake_id']; ?>
">
						 <input type="hidden" name="stake_pic" value="<?php	 	 echo $this->_tpl_vars['stake_pic']; ?>
">
						 <input type="hidden" name="start" value="<?php	 	 echo $this->_tpl_vars['start']; ?>
" />
                           <?php	 	 if ($this->_tpl_vars['stake_id'] == NULL): ?> <input name="submit21" type="image" src="images/submit-button.gif" style="width:62px;">
						   <?php	 	 else: ?><input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;"><?php	 	 endif; ?>

                        </div></td>
                      </tr>
					   <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import all records from .CSV file
                        </div></td>
                      </tr><tr>
                        <td colspan="3" class="fieldname"><div align="center" style="padding-right:100px ">
						<input type="file" name="import_stake">
                        </div></td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
