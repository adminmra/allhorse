<?php	 		 	 /* Smarty version 2.6.1, created on 2008-02-25 02:05:33
         compiled from add_stable.tpl */ ?>
<?php	 	 require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'add_stable.tpl', 55, false),)), $this); ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "left.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-------------------- Body Section ------------------------>
<?php	 	 echo '
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
<style type="text/css">

</style>
'; ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  " align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF"><?php	 	 if ($this->_tpl_vars['stable_id'] == NULL): ?>ADD 
            <?php	 	 else: ?>EDIT <?php	 	 endif; ?>STABLE</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>&nbsp;
		  <a href="stable_view.php"><strong><font color="#FFFFFF">&laquo; Back to Stable</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="stables_form" onSubmit="return chk_stable('<?php	 	 echo $this->_tpl_vars['stable_id']; ?>
');">
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      
                      <tr>
            <td width='150' class="fieldname">
              Name
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stable_name" value="<?php	 	 echo $this->_tpl_vars['stable_name']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		   <tr>
            <td class="fieldname">
              Country
            </td>
            <td class="columnHeaderStatic">

<select name="stable_country" >
<?php	 	 echo smarty_function_html_options(array('values' => $this->_tpl_vars['country_name'],'output' => $this->_tpl_vars['country_name'],'selected' => $this->_tpl_vars['stable_country']), $this);?>
</select>
            </td>
          </tr><tr>
            <td width='150' class="fieldname">
              State
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stable_state" value="<?php	 	 echo $this->_tpl_vars['stable_state']; ?>
" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              City
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stable_city" value="<?php	 	 echo $this->_tpl_vars['stable_city']; ?>
" id="affiliateFirstName">
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Zip
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stable_zip" value="<?php	 	 echo $this->_tpl_vars['stable_zip']; ?>
" id="affiliateFirstName">
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Url
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stable_url" value="<?php	 	 echo $this->_tpl_vars['stable_url']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              email</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stable_email" value="<?php	 	 echo $this->_tpl_vars['stable_email']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Family
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stable_family" value="<?php	 	 echo $this->_tpl_vars['stable_family']; ?>
" id="affiliateFirstName">
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Street1</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stable_street1" value="<?php	 	 echo $this->_tpl_vars['stable_street1']; ?>
" id="affiliateFirstName">
            </td>
          </tr>
		   <tr>
            <td width='150' class="fieldname">
              Street2</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stable_street2" value="<?php	 	 echo $this->_tpl_vars['stable_street2']; ?>
" id="affiliateFirstName">
            </td>

          </tr> 
		  
		  <tr>
            <td width='150' class="fieldname">
              Phone1</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stable_phone1" value="<?php	 	 echo $this->_tpl_vars['stable_phone1']; ?>
" id="">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Phone2</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stable_phone2" value="<?php	 	 echo $this->_tpl_vars['stable_phone2']; ?>
" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Services
            </td>
            <td class="columnHeaderStatic">
               <textarea name="stable_services"  id="affiliateFirstName" rows="4"><?php	 	 echo $this->_tpl_vars['stable_services']; ?>
</textarea>
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Notes
            </td>
            <td class="columnHeaderStatic">
               <textarea name="stable_notes"  id="affiliateFirstName" rows="4"><?php	 	 echo $this->_tpl_vars['stable_notes']; ?>
</textarea>
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Contact
            </td>
            <td class="columnHeaderStatic">
               <textarea name="stable_contact"  id="affiliateFirstName" rows="4"><?php	 	 echo $this->_tpl_vars['stable_contact']; ?>
</textarea>
            </td>

          </tr>
		  
                      <tr>
                        <td colspan="2" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="stable_id" value="<?php	 	 echo $this->_tpl_vars['stable_id']; ?>
">
						 <input type="hidden" name="stable_pic" value="<?php	 	 echo $this->_tpl_vars['stable_pic']; ?>
">
						 <input type="hidden" name="start" value="<?php	 	 echo $this->_tpl_vars['start']; ?>
" />
                           <?php	 	 if ($this->_tpl_vars['stable_id'] == NULL): ?> <input name="submit21" type="image" src="images/submit-button.gif" style="width:62px;">
						   <?php	 	 else: ?><input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;"><?php	 	 endif; ?>

                        </div></td>
                      </tr>
					  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import all records from .CSV file
                        </div></td>
                      </tr><tr>
                        <td colspan="3" class="fieldname"><div align="center" style="padding-right:100px ">
						<input type="file" name="import_stable">
                        </div></td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
