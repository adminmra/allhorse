<?php	 		 	 /* Smarty version 2.6.1, created on 2008-02-07 03:29:43
         compiled from equine_charity_view.tpl */ ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "left.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 echo '
<script language="javascript" type="text/javascript">
function search_equine_charity()
{
document.equine_charity_search.action="?act=search_equine_charity";
document.equine_charity_search.submit();
}

</script>
'; ?>

<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><?php	 	 echo '<?'; ?>
 //include "patient_manage_top.php"; <?php	 	 echo '?>'; ?>
</td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <td height="30" width="100%" align="right" bgcolor="#FFFFFF" style="padding-left:600px;"><strong ><?php	 	 echo $this->_tpl_vars['paging']; ?>
</strong></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">EQUINE CHARITIES</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" width="10%" bgcolor="#E4EBF6"><a href="add_equine_charity.php" class="one" >Add</a></td>
               <td width="75%" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="15%" height="22" bgcolor="#E4EBF6" align="center"><a href="import_equine_charity_records.php"><img src="images/import.gif" border="0"></a></td>
               
                
              </tr>
			  <tr align="center">
			  <td colspan="2" width="100%">
			  <table width="100%" bgcolor="#FFFFFF">
			  <tr>
			  <form name="equine_charity_search" method="post" action="?act_equine_charities=search">
               <td height="22" width="35%" bgcolor="#FFFFFF" colspan="1"> Search by Name : <input type="text" name="search_equine_charity" style="width:100px; " value="<?php	 	 echo $this->_tpl_vars['search_value']; ?>
" ></td>
			   
			   <td height="22" width="65%" bgcolor="#FFFFFF" colspan="4" align="left" valign="middle"><input type="image" src="images/botton-seeresults.gif" style="width:85px; height:18px;" /> &nbsp; <strong style="padding-bottom:5px;">Total records : <?php	 	 echo $this->_tpl_vars['record_count']; ?>
</strong>
				                                            </td>
				
                  
				 </form>          
              </tr>
			  </table>
			  </td>
			  </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" rowspan="2" bgcolor="#0D4686"><a href="?sort_by=name" class="seven"><font style="color:#FFFFFF;"><b>Name</b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2"><a href="?sort_by=url" class="seven"><font style="color:#FFFFFF;"><b>Url
               </b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><a href="?sort_by=country" class="seven"><font style="color:#FFFFFF;"><b>Country</b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><a href="?sort_by=states" class="seven"><font style="color:#FFFFFF;"><b>State</b></font></a></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><a href="?sort_by=city" class="seven"><font style="color:#FFFFFF;"><b>City</b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" colspan="2" class="header"><font style="color:#FFFFFF;"><b>Action</b></font></td>
          		  </tr>
          <tr align="center">
            </tr>
			<?php	 	 $this->assign('indexvalue', 0); ?>
					  <?php	 	 if (isset($this->_sections['equine_charity'])) unset($this->_sections['equine_charity']);
$this->_sections['equine_charity']['name'] = 'equine_charity';
$this->_sections['equine_charity']['loop'] = is_array($_loop=$this->_tpl_vars['equine_charity_id']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['equine_charity']['show'] = true;
$this->_sections['equine_charity']['max'] = $this->_sections['equine_charity']['loop'];
$this->_sections['equine_charity']['step'] = 1;
$this->_sections['equine_charity']['start'] = $this->_sections['equine_charity']['step'] > 0 ? 0 : $this->_sections['equine_charity']['loop']-1;
if ($this->_sections['equine_charity']['show']) {
    $this->_sections['equine_charity']['total'] = $this->_sections['equine_charity']['loop'];
    if ($this->_sections['equine_charity']['total'] == 0)
        $this->_sections['equine_charity']['show'] = false;
} else
    $this->_sections['equine_charity']['total'] = 0;
if ($this->_sections['equine_charity']['show']):

            for ($this->_sections['equine_charity']['index'] = $this->_sections['equine_charity']['start'], $this->_sections['equine_charity']['iteration'] = 1;
                 $this->_sections['equine_charity']['iteration'] <= $this->_sections['equine_charity']['total'];
                 $this->_sections['equine_charity']['index'] += $this->_sections['equine_charity']['step'], $this->_sections['equine_charity']['iteration']++):
$this->_sections['equine_charity']['rownum'] = $this->_sections['equine_charity']['iteration'];
$this->_sections['equine_charity']['index_prev'] = $this->_sections['equine_charity']['index'] - $this->_sections['equine_charity']['step'];
$this->_sections['equine_charity']['index_next'] = $this->_sections['equine_charity']['index'] + $this->_sections['equine_charity']['step'];
$this->_sections['equine_charity']['first']      = ($this->_sections['equine_charity']['iteration'] == 1);
$this->_sections['equine_charity']['last']       = ($this->_sections['equine_charity']['iteration'] == $this->_sections['equine_charity']['total']);
?>
          <tr align="center">
            <td height="18" bgcolor="#3D73B8" align="left"><a href="add_equine_charity.php?equine_charity_id=<?php	 	 echo $this->_tpl_vars['equine_charity_id'][$this->_sections['equine_charity']['index']]; ?>
&start=<?php	 	 echo $this->_tpl_vars['start']; ?>
" class="seven2">
             <?php	 	 echo $this->_tpl_vars['name'][$this->_sections['equine_charity']['index']]; ?>
</a></td>
            <td height="18" bgcolor="#B5DE97" align="left"><a href="<?php	 	 echo $this->_tpl_vars['url'][$this->_sections['equine_charity']['index']]; ?>
" class="seven" target="_blank"><?php	 	 echo $this->_tpl_vars['url'][$this->_sections['equine_charity']['index']]; ?>
</a></td>
            <td height="18" bgcolor="#B5DE97" align="left"><?php	 	 echo $this->_tpl_vars['equine_country'][$this->_sections['equine_charity']['index']]; ?>
</td>
            <td bgcolor="#B5DE97" align="left"><?php	 	 echo $this->_tpl_vars['state'][$this->_sections['equine_charity']['index']]; ?>
</td>
            <td bgcolor="#B5DE97" align="left"><?php	 	 echo $this->_tpl_vars['city'][$this->_sections['equine_charity']['index']]; ?>
</td>
            <td bgcolor="#B5DE97"><a href="add_equine_charity.php?equine_charity_id=<?php	 	 echo $this->_tpl_vars['equine_charity_id'][$this->_sections['equine_charity']['index']]; ?>
&start=<?php	 	 echo $this->_tpl_vars['start']; ?>
" class="seven">[Edit]</a></td>
            <td bgcolor="#B5DE97"><a href="?equine_charity_id=<?php	 	 echo $this->_tpl_vars['equine_charity_id'][$this->_sections['equine_charity']['index']]; ?>
&act=delete" class="seven" onClick="return confirm('Are you sure to delete?')">[Delete]</a><!--<input type="checkbox" name="checkbox" value="checkbox" style="border-color:#B5DE97;" />--></td>
          </tr>
		 <?php	 	 $this->assign('indexvalue', $this->_tpl_vars['indexvalue']+1); ?>
		 <?php	 	 endfor; else: ?>
		  <tr align="center">
            <td height="30" bgcolor="#3D73B8" colspan="11"><font style="color:#FFFFFF;"><strong>No data available</strong></td>
			</tr>
         
                    <?php	 	 endif; ?>
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.php", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>