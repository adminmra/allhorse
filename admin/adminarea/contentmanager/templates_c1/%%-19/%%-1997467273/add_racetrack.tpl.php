<?php	 		 	 /* Smarty version 2.6.1, created on 2008-10-03 08:57:50
         compiled from add_racetrack.tpl */ ?>
<?php	 	 require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'add_racetrack.tpl', 153, false),)), $this); ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "left.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!-------------------- Body Section ------------------------>
<?php	 	 echo '
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
<style type="text/css">

</style>
'; ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  " align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF"><?php	 	 if ($this->_tpl_vars['racetrack_id'] == NULL): ?>ADD 
            RACETRACK<?php	 	 else: ?> EDIT RACETRACK<?php	 	 endif; ?></font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>&nbsp;
		  <a href="racetrack_view.php" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back to Racetracks</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="racetracks_form" onSubmit="return chk_racetrack('<?php	 	 echo $this->_tpl_vars['racetrack_id']; ?>
');">
                <tr>
                <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
				  <!--------------------->
				  <tr>
				  <td colspan="3" width="100%">
				  <table >
				  <tr><td class="fieldname" width="50%" align="center">Logo</td><td class="fieldname" width="50%" align="center">Driving Picture</td></tr>
				  <tr>
            <td  class="fieldname" align="center" valign="top" width="50%" >
			 <?php	 	 if ($this->_tpl_vars['logo'] != NULL):  if ($this->_tpl_vars['img_chk'] == 1): ?>
			 <img src="../../images/cm/<?php	 	 echo $this->_tpl_vars['logo']; ?>
" border="1" width="200" height="200" >
			 <?php	 	 else: ?>
             <!-- <img src="../uploaded_images/racetrack_images/thumb<?php	 	 echo $this->_tpl_vars['logo']; ?>
" border="1"> -->
			 <img src="../../images/racetracks/<?php	 	 echo $this->_tpl_vars['logo']; ?>
" border="1"><?php	 	 endif;  endif; ?>
            </td>
            <td   align="center" valign="top" width="50%">
			<?php	 	 if ($this->_tpl_vars['driving_pic'] != NULL):  if ($this->_tpl_vars['imgdriving_chk'] == 1): ?>
			 <img src="../../images/cm/<?php	 	 echo $this->_tpl_vars['driving_pic']; ?>
" border="1" width="300" height="300">
			 <?php	 	 else: ?>
              <img src="../uploaded_images/racetrack_images/thumb<?php	 	 echo $this->_tpl_vars['driving_pic']; ?>
" border="1"><?php	 	 endif;  endif; ?>
            </td>
			</tr>
			</table>
</td>
          </tr>
				  
				  <!---------------------->
                      
                      <tr>
            <td width='150' class="fieldname">
              Name
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_name" value="<?php	 	 echo $this->_tpl_vars['racetrack_name']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Url
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_url" value="<?php	 	 echo $this->_tpl_vars['racetrack_url']; ?>
" id="affiliateFirstName" style="width:450px; ">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Track Code
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_track_code" value="<?php	 	 echo $this->_tpl_vars['racetrack_track_code']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname" valign="top">
              History
            </td>
            <td class="columnHeaderStatic">
              <!-- <input type="text" name="racetrack_history" value="<?php	 	 echo $this->_tpl_vars['racetrack_history']; ?>
" id="affiliateFirstName" > -->
			  <textarea name="racetrack_history" style="width:450px; "><?php	 	 echo $this->_tpl_vars['racetrack_history']; ?>
</textarea>
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Opening Date
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_opening_date" value="<?php	 	 echo $this->_tpl_vars['racetrack_opening_date']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Closing Day
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_closing_date" value="<?php	 	 echo $this->_tpl_vars['racetrack_closing_date']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Racing days
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_racing_days" value="<?php	 	 if ($this->_tpl_vars['racetrack_racing_days'] != '0'):  echo $this->_tpl_vars['racetrack_racing_days'];  endif; ?>" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Street1
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_street1" value="<?php	 	 echo $this->_tpl_vars['racetrack_street1']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Street2
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_street2" value="<?php	 	 echo $this->_tpl_vars['racetrack_street2']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		   <tr>
            <td class="fieldname">
              Country
            </td>
            <td class="columnHeaderStatic">

              <select name="racetrack_country">
<?php	 	 echo smarty_function_html_options(array('values' => $this->_tpl_vars['country_name'],'output' => $this->_tpl_vars['country_name'],'selected' => $this->_tpl_vars['racetrack_country']), $this);?>
</select>
            </td>
          </tr><tr>
            <td width='150' class="fieldname">
              State
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_state" value="<?php	 	 echo $this->_tpl_vars['racetrack_state']; ?>
" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              City
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_city" value="<?php	 	 echo $this->_tpl_vars['racetrack_city']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Zip
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_zip" value="<?php	 	 echo $this->_tpl_vars['racetrack_zip']; ?>
" id="affiliateFirstName">
            </td>

          </tr><tr>
            <td width='150' class="fieldname" valign="top">
              Driving
            </td>
            <td class="columnHeaderStatic">
			<textarea name="racetrack_driving" style="width:450px; " ><?php	 	 echo $this->_tpl_vars['racetrack_driving']; ?>
</textarea>
              <!-- <input type="text" name="racetrack_driving" value="<?php	 	 echo $this->_tpl_vars['racetrack_driving']; ?>
" id="affiliateFirstName"> -->
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Phone1
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_phone1" value="<?php	 	 echo $this->_tpl_vars['racetrack_phone1']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Phone2
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_phone2" value="<?php	 	 echo $this->_tpl_vars['racetrack_phone2']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Hta
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="hta" value="<?php	 	 echo $this->_tpl_vars['racetrack_hta']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Weather
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_weather" value="<?php	 	 echo $this->_tpl_vars['racetrack_weather']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Start Date
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_start_date" value="<?php	 	 echo $this->_tpl_vars['racetrack_start_date']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Track Length
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_track_lenth" value="<?php	 	 echo $this->_tpl_vars['racetrack_track_lenth']; ?>
" id="affiliateFirstName">
            </td>

          </tr><tr>
            <td width='150' class="fieldname">
              Stretch Length
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_strech_lenth" value="<?php	 	 echo $this->_tpl_vars['racetrack_strech_lenth']; ?>
" id="">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Stretch Width
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_strech_width" value="<?php	 	 echo $this->_tpl_vars['racetrack_stretch_width']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
             Speed Rating
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_speed_rating" value="<?php	 	 echo $this->_tpl_vars['racetrack_speed_rating']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Infield Capacity
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_cap" value="<?php	 	 if ($this->_tpl_vars['racetrack_cap'] != '0'):  echo $this->_tpl_vars['racetrack_cap'];  endif; ?>" id="">
            </td>

          </tr>
		     <tr>
            <td width='150' class="fieldname">
              Clubhouse Capacity
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_cap_club" value="<?php	 	 if ($this->_tpl_vars['racetrack_cap_club'] != '0'):  echo $this->_tpl_vars['racetrack_cap_club'];  endif; ?>" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Grand Stand Capacity
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_cap_ground" value="<?php	 	 if ($this->_tpl_vars['racetrack_cap_ground'] != '0'):  echo $this->_tpl_vars['racetrack_cap_ground'];  endif; ?>" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
             Parking Capacity
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_cap_parking" value="<?php	 	 if ($this->_tpl_vars['racetrack_cap_parking'] != '0'):  echo $this->_tpl_vars['racetrack_cap_parking'];  endif; ?>" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Price General Admission
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="pricega" value="<?php	 	 echo $this->_tpl_vars['pricega']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		     <tr>
            <td width='150' class="fieldname">
              Price Clubhouse
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="pricech" value="<?php	 	 echo $this->_tpl_vars['pricech']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Price Turf Club
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="pricetc" value="<?php	 	 echo $this->_tpl_vars['pricetc']; ?>
" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname" valign="top">
              Points of Interest
            </td>
            <td class="columnHeaderStatic">
			<textarea name="point_interest" style="width:450px; "><?php	 	 echo $this->_tpl_vars['point_interest']; ?>
</textarea>
            <!--   <input type="text" name="point_interest" value="<?php	 	 echo $this->_tpl_vars['point_interest']; ?>
" id="affiliateFirstName"> -->
            </td>

          </tr>
		  
		   <tr>
            <td width='150' class="fieldname">
             Logo 
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="logo" value="<?php	 	 echo $this->_tpl_vars['logo']; ?>
" />
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
             Live
            </td>
            <td class="columnHeaderStatic">
              <input  type="checkbox" name="chk_box" value="yes" <?php	 	 if ($this->_tpl_vars['live'] == 'Y'): ?> checked <?php	 	 endif; ?>/>
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
             Driving Picture
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="driving_pic" value="<?php	 	 echo $this->_tpl_vars['driving_pic']; ?>
" />
            </td>

          </tr>
		   <tr>
            <td width='150' class="fieldname">
            Timezone
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="timezone" value="<?php	 	 echo $this->_tpl_vars['timezone']; ?>
" />
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
            Live Video Url
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="uvideourl" value="<?php	 	 echo $this->_tpl_vars['uvideourl']; ?>
"  style="width:450px; "/>
            </td>

          </tr>
		  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import all records from .CSV file 
                        </div></td>
                      </tr><tr>
                        <td colspan="3" class="fieldname"><div align="center" style="padding-right:0px ">
						<input type="file" name="import_racetrack">
                        </div></td>
                      </tr>
                      <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						<input type="hidden" name="racetrack_id" value="<?php	 	 echo $this->_tpl_vars['racetrack_id']; ?>
">
						 <input type="hidden" name="logo1" value="<?php	 	 echo $this->_tpl_vars['logo']; ?>
">
						 <input type="hidden" name="driving_pic1" value="<?php	 	 echo $this->_tpl_vars['driving_pic']; ?>
">
						 <input type="hidden" name="start" value="<?php	 	 echo $this->_tpl_vars['start']; ?>
" />
                           <?php	 	 if ($this->_tpl_vars['racetrack_id'] == NULL): ?> <input name="select-all2" type="image" src="images/submit-button.gif" style="width:62px;">
						   <?php	 	 else: ?><input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;"><?php	 	 endif; ?>

                        </div></td>
                      </tr>
					  
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td></tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
