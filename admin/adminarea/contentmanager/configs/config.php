<?
define('TITLE', 'Horse Racing');
define('NO_OF_RECORDS', 10);
///////////for image resizing
define('TRAINER_IMG_H', 250);
define('TRAINER_IMG_W', 250);
define('LOGO_IMG_H', 100);
define('LOGO_IMG_W', 100);
define('HORSE_IMG_H', 300);
define('HORSE_IMG_W', 300);
define('JOCKEY_IMG_H', 300);
define('JOCKEY_IMG_W', 250);
define('RACETRACK_IMG_H', 250);
define('RACETRACK_IMG_W', 250);
define('STABLE_IMG_H', 250);
define('STABLE_IMG_W', 250);
//////////image resizing ends here

?>