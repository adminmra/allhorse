<?php	 		 	 /* Smarty version 2.6.1, created on 2009-04-24 01:59:41
         compiled from horse_history.tpl */ ?>
<?php	 	 require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'horse_history.tpl', 85, false),array('modifier', 'count_characters', 'horse_history.tpl', 170, false),)), $this); ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "left.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php	 	 echo '
<script language="javascript" type="text/javascript">
function search_horse()
{
document.horse_search.action="?act=search_breeders";
document.horse_search.submit();
}

</script>
'; ?>


<?php	 	 echo '
<script>
function check(stakes)
{ document.horse_search.action="horse_history.php?act_BreedersCup=search";
document.horse_search.submit();
 
}

function formsubmit()
{
	document.asd.submit();
}

function uploadcheck()
{
	if(document.asd.stakes.value == 0)
	{
		alert("Please at first select a stakes");
		return false;
	}
	if(document.fileup.xmlfile.value == "")
	{
		alert("Please Upload an xml file");
		return false;
	}
	
	//return false;

}
</script>
<style type="text/css">
.smalltext
{
width:40px;
}
.mediumtext
{
width:140px;
}
</style>
'; ?>

<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><?php	 	 echo '<?'; ?>
 //include "patient_manage_top.php"; <?php	 	 echo '?>'; ?>
</td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <td height="30" width="100%" align="right" bgcolor="#FFFFFF" style="padding-left:600px;"><strong ><?php	 	 echo $this->_tpl_vars['paging']; ?>
</strong></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">Add Historical Data</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" width="10%" bgcolor="#E4EBF6">
				<form name="asd" action="" method="post">
				<select name=stakes onChange="return formsubmit();">
				<option value="0">Please select an stakes</option>
					<?php	 	 echo smarty_function_html_options(array('options' => $this->_tpl_vars['allstakes'],'selected' => $this->_tpl_vars['allstakesselected']), $this);?>

				</select>
				</form>

				<!-- <a href="add_derby.php" class="one" >Add</a> --></td>
               <td width="75%" bgcolor="#FFFFFF">
			   <form name="fileup" action="" method="post"  onSubmit="return uploadcheck();"  enctype="multipart/form-data">
			   <input type="hidden" name="stakes" value="<?php	 	 echo $this->_tpl_vars['allstakesselected']; ?>
">
				<input type="file" name="xmlfile">&nbsp;<input   type="submit" value="UPLOAD XML FILE" name="Submitxml">
				</form>
			   </td>
                <td width="15%" height="22" bgcolor="#FFFFFF" align="center"><a href="#"><!--<img src="images/import.gif" border="0">-->&nbsp;</a></td>
               
                
              </tr>
			  <tr align="center">
			  <td colspan="2" width="100%">
			  <table width="100%" bgcolor="#FFFFFF">
			   <form name="horse_search" method="post" action="?act_BreedersCup=search">
			   <input type="hidden" name="stakes" value="<?php	 	 echo $this->_tpl_vars['allstakesselected']; ?>
">
			  <tr>
			 
               <td height="22" width="35%" bgcolor="#FFFFFF" colspan="1"><!--  Search by Name : <input type="text" name="search_BreedersCup" style="width:100px; " value="<?php	 	 echo $this->_tpl_vars['search_value']; ?>
" > --></td>
			   
			   <td height="22" width="65%" bgcolor="#FFFFFF" colspan="4" align="left" valign="middle"><!-- <input type="image" src="images/botton-seeresults.gif" style="width:85px; height:18px;" /> --> 
			     &nbsp; <strong style="padding-bottom:5px;">Total records : <?php	 	 echo $this->_tpl_vars['record_count']; ?>
</td>
                  
				         
              </tr> 
<!-- 			  <tr>
			 
            <td height="22" width="35%" bgcolor="#FFFFFF" colspan="2">Group By Stakes : 
			     <select name="group_stakes" onchange="check(this.value)">
				 <option value="">Select Stakes</option>
			   <?php	 	 echo smarty_function_html_options(array('values' => $this->_tpl_vars['horse_stakes_group'],'output' => $this->_tpl_vars['horse_stakes_group'],'selected' => $this->_tpl_vars['group_stakes']), $this);?>

                  </select></td>
                  
				         
              </tr> -->
			  </form> 
			  </table>
			  </td>
			  </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top">
		<?php	 	 if ($this->_tpl_vars['showtable'] == 'yes'): ?>
		<form action="horse_history.php" name="frmnew" method="post">
		 <input type="hidden" name="stakes" value="<?php	 	 echo $this->_tpl_vars['allstakesselected']; ?>
">
		  <input type="hidden" name="mode" value="add">
		<table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="20" rowspan="2" bgcolor="#0D4686"><font style="color:#FFFFFF;"><b>Year</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2"><font style="color:#FFFFFF;"><b>Winner</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Age</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Jockey</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Trainer</b></font></td>
           <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Owner</b></font></td> 
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Time</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2"  class="header"><font style="color:#FFFFFF;"><b>Action</b></font></td>
          
		  </tr>
          <tr align="center">
           <!-- <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb">&nbsp;</td>-->
            </tr>
			<?php	 	 $this->assign('indexvalue', 0); ?>
					  <?php	 	 if (isset($this->_sections['horse'])) unset($this->_sections['horse']);
$this->_sections['horse']['name'] = 'horse';
$this->_sections['horse']['loop'] = is_array($_loop=$this->_tpl_vars['2006BreedersCup_id']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['horse']['show'] = true;
$this->_sections['horse']['max'] = $this->_sections['horse']['loop'];
$this->_sections['horse']['step'] = 1;
$this->_sections['horse']['start'] = $this->_sections['horse']['step'] > 0 ? 0 : $this->_sections['horse']['loop']-1;
if ($this->_sections['horse']['show']) {
    $this->_sections['horse']['total'] = $this->_sections['horse']['loop'];
    if ($this->_sections['horse']['total'] == 0)
        $this->_sections['horse']['show'] = false;
} else
    $this->_sections['horse']['total'] = 0;
if ($this->_sections['horse']['show']):

            for ($this->_sections['horse']['index'] = $this->_sections['horse']['start'], $this->_sections['horse']['iteration'] = 1;
                 $this->_sections['horse']['iteration'] <= $this->_sections['horse']['total'];
                 $this->_sections['horse']['index'] += $this->_sections['horse']['step'], $this->_sections['horse']['iteration']++):
$this->_sections['horse']['rownum'] = $this->_sections['horse']['iteration'];
$this->_sections['horse']['index_prev'] = $this->_sections['horse']['index'] - $this->_sections['horse']['step'];
$this->_sections['horse']['index_next'] = $this->_sections['horse']['index'] + $this->_sections['horse']['step'];
$this->_sections['horse']['first']      = ($this->_sections['horse']['iteration'] == 1);
$this->_sections['horse']['last']       = ($this->_sections['horse']['iteration'] == $this->_sections['horse']['total']);
?>
          <tr align="center">
            <td height="18" bgcolor="#3D73B8" align="left"><font style="color:#FFFFFF;"><input type="text" value="<?php	 	 echo $this->_tpl_vars['horse_name'][$this->_sections['horse']['index']]; ?>
" name="year[]" class="smalltext"></font></td>
            <td height="18" bgcolor="#B5DE97" align="left"><input type="text" value="<?php	 	 echo $this->_tpl_vars['horse_odds'][$this->_sections['horse']['index']]; ?>
" name="winner[]" class="mediumtext"></td>
            <td height="18" bgcolor="#B5DE97" align="left"><input type="text" value="<?php	 	 echo $this->_tpl_vars['horse_jockey'][$this->_sections['horse']['index']]; ?>
" name="age[]" class="smalltext"></td>
            <td bgcolor="#B5DE97"><input type="text" value="<?php	 	 echo $this->_tpl_vars['horse_post'][$this->_sections['horse']['index']]; ?>
" name="jockey[]" class="mediumtext"></td>
            <td bgcolor="#B5DE97"><input type="text" value="<?php	 	 echo $this->_tpl_vars['horse_result'][$this->_sections['horse']['index']]; ?>
" name="trainer[]"   class="mediumtext"></td>
           <td bgcolor="#B5DE97"><input type="text" value="<?php	 	 echo $this->_tpl_vars['horse_stakes'][$this->_sections['horse']['index']]; ?>
" name="owner[]" class="mediumtext"></td>
             <td bgcolor="#B5DE97"><input type="text" value="<?php	 	 echo $this->_tpl_vars['trainer'][$this->_sections['horse']['index']]; ?>
" name="racetime[]" class="smalltext"></td>
             <td bgcolor="#B5DE97"><?php	 	 if (((is_array($_tmp=$this->_tpl_vars['2006BreedersCup_id'][$this->_sections['horse']['index']])) ? $this->_run_mod_handler('count_characters', true, $_tmp, true) : smarty_modifier_count_characters($_tmp, true)) > 0): ?><a href="?act=delete&BreedersCup_id=<?php	 	 echo $this->_tpl_vars['2006BreedersCup_id'][$this->_sections['horse']['index']]; ?>
&stakes=<?php	 	 echo $this->_tpl_vars['allstakesselected']; ?>
" class="seven" onClick="return confirm('Are you sure to delete?')">[Delete]</a><?php	 	 endif; ?></td>
          </tr>
		  <?php	 	 $this->assign('indexvalue', $this->_tpl_vars['indexvalue']+1); ?>
		 <?php	 	 endfor; else: ?>
		  <tr align="center">
            <td height="18" bgcolor="#3D73B8" colspan="11"><strong>No data available</strong></td>
			</tr>
                    <?php	 	 endif; ?>
			
         <tr><td colspan="7" align="center"><input type="image" src="images/submit-button.gif" style="width:85px; height:18px;" name="submitodds" value="submitodds"/></td></tr>
		 
        </table>
		</form>
		<?php	 	 else: ?>
		
		<?php	 	 endif; ?>
		</td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->