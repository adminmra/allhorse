{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>

{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $stablepics_id eq NULL}ADD {else} EDIT{/if} 
            STABLE PICTURE</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="stable_pics.php?stable_id={$stable_id}" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="stable_pics_add" onSubmit="return chk_stable_pic('{$stablepics_id}');">
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      
                     {if $stablepics_id neq NULL} <tr>
            <td width='150' class="fieldname">
              Picture           </td>
            <td class="columnHeaderStatic">
              {if $stablepics neq null} 
			  {if $img_chk eq 1}
			 <img src="{$stablepics}" border="1">
			 {else}
			 <img src="../uploaded_images/stable_images/stable_pics/thumb{$stablepics}" border="1">{/if}{/if}           </td>
          </tr>{/if}
		  <tr>
            <td width='150' class="fieldname">
             upload picture           </td>
            <td class="columnHeaderStatic">
             <input type="file" name="stable_image" /> </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname" colspan="2" align="center">
             Or insert URL           </td>
            
          </tr>
		  <tr>
            <td width='150' class="fieldname">
             Url           </td>
            <td class="columnHeaderStatic">
             <input type="text" name="stable_url" value="{$stablepics}"/> </td>
          </tr>
                      <tr>
                        <td colspan="2" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="stable_id" value="{$stable_id}">
						<input type="hidden" name="stablepics_id" value="{$stablepics_id}">
						 <input type="hidden" name="stable_pic" value="{$stablepics}">
                           {if $stablepicss_id eq NULL}  <input name="select-all2" type="image" src="images/submit-button.gif" style="width:60px">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:60px">{/if}

                        </div></td>
                      </tr>
					  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import 
                        </div></td>
                      </tr><tr>
                        <td colspan="3" class="fieldname"><div align="center">
						<input type="file" name="import_stable_pic">
                        </div></td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

