{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
<style type="text/css">

</style>
{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  " align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">ADD 
            HISTORY</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>&nbsp;
		  <a href="stake_view.php"><strong><font color="#FFFFFF">&laquo; Back to Stakes</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="stakes_form" onSubmit="return chk_stake('{$stake_id}');">
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      
                      <tr>
            <td width='150' class="fieldname"> Year              </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_name" value="{$stake_name}" id="affiliateFirstName">            </td>
          </tr><tr>
            <td width='150' class="fieldname">
              Winner</td>
            <td class="columnHeaderStatic">
              <select name=horse>
			<option  value="0">Please selec an horse</option>
	{html_options options=$horsesname}
</select>
Or <a href="#" onClick="window.open('#')">Add new horse here</a>            </td>

          </tr> <tr>
            <td width='150' class="fieldname"> Age</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_date" value="{$stake_date}" id="affiliateFirstName">            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname"> Jockey              </td>
            <td class="columnHeaderStatic"><select name=jockey>
			<option  value="0">Please selec an jockey</option>
	{html_options options=$jockeyname}
</select>
Or <a href="#" onClick="window.open('add_jockey.php', height=)">Add jockey Here</a></td>
          </tr> 
		  <tr>
            <td width='150' class="fieldname"> Trainer              </td>
            <td class="columnHeaderStatic">
              <select name=trainer>
			<option  value="0">Please selec an trainer</option>
	{html_options options=$trainersname}
</select>
Or <a href="#" onClick="window.open('#')">Add new trainer here</a>       </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname"> Owner              </td>
            <td class="columnHeaderStatic">
              <select name=owner>
			<option  value="0">Please selec an owner</option>
	{html_options options=$ownersname}
</select>
Or <a href="#" onClick="window.open('#')">Add new owner here</a>           </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Time</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_distance" value="{$stake_distance}" id="affiliateFirstName">            </td>
          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Place</td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_age" value="{$horse_age}" id="affiliateFirstName">            </td>
          </tr>
		   <tr>
            <td width='150' class="fieldname">
              Race Name </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_sex" value="{$stake_sex}" id="affiliateFirstName">            </td>
          </tr> 
		  
<!-- 		  <tr>
            <td width='150' class="fieldname">
              Turf</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_turf" value="{$stake_turf}" id="">            </td>
          </tr> 
		  <tr>
		    <td class="fieldname">Post Time </td>
		    <td class="columnHeaderStatic"><input type="text" name="stake_posttime" value="{$stake_posttime}" id="stake_posttime" /></td>
		    </tr> -->
		  <tr>
            <td width='150' class="fieldname">
              Notes            </td>
            <td class="columnHeaderStatic">
               <textarea name="stake_notes"  id="affiliateFirstName" rows="4">{$stake_notes}</textarea>            </td>
          </tr>
		  
		  
                      <tr>
                        <td colspan="2" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="stake_id" value="{$stake_id}">
						 <input type="hidden" name="stake_pic" value="{$stake_pic}">
						 <input type="hidden" name="start" value="{$start}" />
                           {if $stake_id eq NULL} <input name="submit21" type="image" src="images/submit-button.gif" style="width:62px;">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;">{/if}

                        </div></td>
                      </tr>
					  <!--  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import all records from .CSV file
                        </div></td>
                      </tr><tr>
                        <td colspan="3" class="fieldname"><div align="center" style="padding-right:100px ">
						<input type="file" name="import_stake">
                        </div></td>
                      </tr> -->
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

