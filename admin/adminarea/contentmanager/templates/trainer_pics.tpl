{include file="header.tpl"}
{include file="left.tpl"}
{literal}
<style type="text/css">
</style>
{/literal}
<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><? //include "patient_manage_top.php"; ?></td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <td height="30" align="right" bgcolor="#FFFFFF" style="padding-right:10px;"><strong class="bodyb">&#8249; </strong><a href="#" class="one"><b>Previous</b></a> | <a href="#" class="one"><b>Next</b></a> <strong class="one">&#8250;</strong></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">trainer PICTURES </font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="40%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" bgcolor="#E4EBF6"><a href="add_trainer_pics.php?trainer_id={$trainer_id}" class="one">Add New</a></td>
                <td height="22" bgcolor="#E4EBF6"><a href="trainer_view.php" class="one">Back to trainer</a></td>
				<td height="22" bgcolor="#E4EBF6">Total Records : {$total_records}</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" bgcolor="#0D4686"><font style="color:#FFFFFF;"><b>trainer name</b></font></td>
            <td height="40" bgcolor="#0D4686"><font style="color:#FFFFFF;"><b>trainer Pictures </b></font></td>
            <td height="40" bgcolor="#0D4686" class="header"><font style="color:#FFFFFF;"><b>Edit</b></font></td>
            <td height="40" bgcolor="#0D4686" class="header"><font style="color:#FFFFFF;"><b>Delete</b></font></td>
           
		  </tr>
		  {assign var=index value=1}
		  {section name=trainerpics loop=$trainer_pics_id}
		  <tr align="center">
            <td height="18" bgcolor="#3D73B8"><font style="color:#FFFFFF;">
             {$trainer_name}</font></td>
            <td height="18" bgcolor="#B5DE97"><a href="#" onClick="javascript:window.open('show_pics_trainer.php?pic_id={$trainer_pics_id[trainerpics]}', 
'URLcheck','width=400,height=400,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=0');newWindow.moveTo(500,300); 
" class="seven">Picture{$index}</a></td>
            <td bgcolor="#B5DE97"><a href="add_trainer_pics.php?trainerpics_id={$trainer_pics_id[trainerpics]}&trainer_id={$trainer_id}" class="seven">Edit</a></td>
            <td bgcolor="#B5DE97"><a href="?trainerpics_id={$trainer_pics_id[trainerpics]}&trainer_id={$trainer_id}&act=del" onClick="return confirm('Are you sure?');"class="seven">Delete</a></td>
            
          </tr>
		  {assign var=index value=$index+1}
		  {sectionelse}
		   <td height="38" bgcolor="#3D73B8" colspan="4" align="center"><font style="color:#FFFFFF;">
            <b> No picture available</b></font></td>
		 {/section}
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
{include file="footer.php"}