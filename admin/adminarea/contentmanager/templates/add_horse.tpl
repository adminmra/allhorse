{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>


{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%"  align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  ">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $horse_id eq NULL}ADD 
            HORSE{else} EDIT HORSE{/if}</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">&nbsp;
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>
		  <!--</td>
		  <td align="right" bgcolor="#0D4686" style="padding-right:10px;">-->
		  <a href="horse_view.php" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back to horses</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  align="center" cellpadding="0" cellspacing="0" >
              <form action="" method="post" enctype="multipart/form-data" name="horses_form" onSubmit="return chk_horse('{$horse_id}');">
                <tr>
                <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
				  <!--------------------->
				  <tr>
				  <td colspan="2">
				  <table><tr>
            <td  class="fieldname" align="center" valign="top" >
             {if $horse_pic neq NULL}{if $img_chk eq 1}
			 <img src="{$horse_pic}" border="1">
			 {else}
			 <img src="../uploaded_images/horse_images/thumb{$horse_pic}" border="1">{/if}{/if}
            </td>
            <td   align="center" valign="top">
              {if $horse_silkpic neq NULL}
			  {if $silkimg_chk eq 1}
			 <img src="{$horse_silkpic}" border="1">
			 {else}
			  <img src="../uploaded_images/horse_images/thumb{$horse_silkpic}" border="1">{/if}{/if}
            </td>
			</tr>
			</table>
</td>
          </tr>
				  
				  <!---------------------->
                      
                      <tr>
            <td width='150' class="fieldname">
              Name
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_name" value="{$horse_name}" id="affiliateFirstName">
            </td>

          </tr>
		   <tr>
            <td class="fieldname">
              Status
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_status" value="{$horse_status}" id="affiliateFirstName">
             </td>
          </tr><tr>
            <td width='150' class="fieldname">
              Trainer
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_trainer" value="{$horse_trainer}" id="affiliateFirstName"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{if $horse_id neq NULL}<a href="trainer_view.php?trainer={$horse_trainer}" class="link">See the trainer</a>{/if}
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Owner
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_owner" value="{$horse_owner}" id="affiliateFirstName"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{if $horse_id neq NULL}<a href="owner_view.php?owner={$horse_owner}" class="link">See the owner</a>{/if}
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Foaling
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_foaling" value="{$horse_foaling}" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Sire
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_sire" value="{$horse_sire}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Dam
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_dam" value="{$horse_dam}" id="affiliateFirstName">
            </td>
          </tr>
		   <tr>
            <td width='150' class="fieldname">
              Damesire
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_damsire" value="{$horse_damsire}" id="affiliateFirstName">
            </td>

          </tr> 
		  
		  <tr>
            <td width='150' class="fieldname">
              Dosage
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_dosage" value="{$horse_dosage}" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Dosage Index
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_dosage_index" value="{$horse_dosageindex}" id="affiliateFirstName">
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              CD
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_cd" value="{$horse_cd}" id="affiliateFirstName">
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname" valign="top">
             AHR Notes
            </td>
            <td class="columnHeaderStatic">
              <textarea name="horse_notes" rows="3">{$horse_notes}</textarea>
            </td>

          </tr> 
		   <tr>
            <td width='150' class="fieldname" valign="top">
              GHB Notes
            </td>
            <td class="columnHeaderStatic">
              <textarea name="horse_notes_GHB" rows="3">{$horse_notes_GHB}</textarea>
            </td>

          </tr> 
		   <tr>
            <td width='150' class="fieldname" valign="top">
              HB Notes
            </td>
            <td class="columnHeaderStatic">
              <textarea name="horse_notes_HB" rows="3">{$horse_notes_HB}</textarea>
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Best Beyer Figure
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_bestbuyer" value="{$horse_bestbuyer}" id="affiliateFirstName">
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Stable
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_stable" value="{$horse_stable}" id="affiliateFirstName">
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Picture
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_image" value="{$horse_pic}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Silk Picture
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_silkimage" value="{$horse_silkpic}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import all records from .CSV file
                        </div></td>
                      </tr><tr>
                        <td colspan="2" class="fieldname">
						<div align="center" style="padding-right:100px ">
						<input type="file" name="import_horse">
                        </div></td>
                      </tr>
                      <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						<input type="hidden" name="horse_id" value="{$horse_id}">
						 <input type="hidden" name="horse_pic" value="{$horse_pic}">
						 <input type="hidden" name="horse_silkpic" value="{$horse_silkpic}">
						 <input type="hidden" name="start" value="{$start}" />
						 <input type="hidden" name="act_horse" value="{$act_horse}">
						 <input type="hidden" name="search_horse" value="{$search_horse}">
                           {if $horse_id eq NULL} <input type="image" name="Submit" src="images/submit-button.gif"  style="width:62px;">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;">{/if}
							
                        </div></td>
                      </tr>
					  
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td></tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

