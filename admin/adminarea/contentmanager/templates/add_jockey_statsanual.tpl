{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>

{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $jockeystatsanual_id eq NULL}ADD {else}EDIT {/if}
            JOCKEY ANUAL STATISTICS</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="jockey_annualstat.php?jockey_id={$jockey_id}" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="jockey_statsanual_add" onSubmit="return chk_jockey_statsanual('{$jockeystatsanual_id}');">
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      
                      
		   <tr>
            <td width='150' class="fieldname">
              Earnings            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="earning_stat" value="{$earning_stat}" id="affiliateFirstName">            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Year            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="year_stat" value="{$year_stat}" id="affiliateFirstName">            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Starts            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="starts_stat" value="{$starts_stat}" id="affiliateFirstName">            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Wins            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="win_stat" value="{$win_stat}" id="affiliateFirstName">            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Places            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="places_stat" value="{$places_stat}" id="affiliateFirstName">            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Shows			              </td>
            <td class="columnHeaderStatic">
              <input type="text" name="show_stat" value="{$show_stat}" id="affiliateFirstName">            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Win percentage            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="w_stat" value="{$w_stat}" id="affiliateFirstName">            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Win per show percentage            </td>


            <td class="columnHeaderStatic">
              <input type="text" name="wpc_stat" value="{$wpc_stat}" id="affiliateFirstName">            </td>
          </tr>
		   
		  
                      <tr>
                        <td colspan="2" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="jockey_id" value="{$jockey_id}">
						<input type="hidden" name="jockeystatsanual_id" value="{$jockeystatsanual_id}">
						 <input type="hidden" name="image" value="{$value_image}">
                           {if $jockeystatsanual_id eq NULL}  <input name="select-all2" type="image" src="images/submit-button.gif" style="width:60px">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:60px">{/if}

                        </div></td>
                      </tr>
					  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import 
                        </div></td>
                      </tr><tr>
                        <td colspan="3" class="fieldname"><div align="center">
						<input type="file" name="import_jockey_annualstat">
                        </div></td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

