{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>

{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $jockeyvid_id eq NULL}ADD {else} EDIT{/if} 
            JOCKEY VIDEO</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="jockey_videos.php?jockey_id={$jockey_id}" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="jockey_vid_add" onSubmit="return chk_jockey_vid('{$jockeyvid_id}');">
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      
                     {if $jockeyvid_id neq NULL} <tr>
            <td width='150' class="fieldname">
              Video           </td>
            <td class="columnHeaderStatic">
              {if $jockeyvid neq null} 
			  {if $img_chk eq 1}
			 <object id="RP2" classid="clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA" width="220" height="30">
										<param name="CONTROLS" value="ControlPanel">
										<param name="AUTOSTART" value="false">
										<param name="RESET" value="false">
										<param name="CONSOLE" value="clip1">
										<param name="NOJAVA" value="true">
										<param name="SRC" value="{$jockeyvid}">
										<embed id="embdID" type="video/x-pn-realaudio-plugin" width="220" height="200" controls="ControlPanel" autostart="true" console="clip1" labels="true" nojava="true" src="{$jockeyvid}"></embed>
										</object>
			 {else}
			 <object id="RP2" classid="clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA" width="220" height="30">
										<param name="CONTROLS" value="ControlPanel">
										<param name="AUTOSTART" value="false">
										<param name="RESET" value="false">
										<param name="CONSOLE" value="clip1">
										<param name="NOJAVA" value="true">
										<param name="SRC" value="../uploaded_video/jockey_video/{$jockeyvid}">
										<embed id="embdID" type="video/x-pn-realaudio-plugin" width="220" height="200" controls="ControlPanel" autostart="true" console="clip1" labels="true" nojava="true" src="../uploaded_video/jockey_video/{$jockeyvid}"></embed>
										</object>{/if}{/if}           </td>
          </tr>{/if}
		  <tr>
            <td width='150' class="fieldname">
             upload video           </td>
            <td class="columnHeaderStatic">
             <input type="file" name="jockey_video" /> </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname" colspan="2" align="center">
             Or insert URL           </td>
            
          </tr>
		  <tr>
            <td width='150' class="fieldname">
             Url           </td>
            <td class="columnHeaderStatic">
             <input type="text" name="jockey_url" /> </td>
          </tr>
                      <tr>
                        <td colspan="2" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="jockey_id" value="{$jockey_id}">
						<input type="hidden" name="jockeyvid_id" value="{$jockeyvid_id}">
						 <input type="hidden" name="jockey_vid" value="{$jockeyvid}">
                           {if $jockeyvid_id eq NULL}  <input name="select-all2" type="image" src="images/submit-button.gif" style="width:60px">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:60px">{/if}

                        </div></td>
                      </tr>
					  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import 
                        </div></td>
                      </tr><tr>
                        <td colspan="3" class="fieldname"><div align="center">
						<input type="file" name="import_jockey_vid">
                        </div></td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

