{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%"  align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  ">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $horse_id eq NULL}ADD 
            {$title} DUBAI WORLD CUP{else} EDIT {$title} DUBAI WORLD CUP{/if}</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  align="center" cellpadding="0" cellspacing="0" >
              <form action="{$action}" method="post" enctype="multipart/form-data" name="breeders_form" >
                <tr>
                <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
				  <!--------------------->
				  <tr>
				  <td colspan="4">
				  <table><tr>
            <td  class="fieldname" align="center" valign="top" >
             {if $horse_pic neq NULL}{if $img_chk eq 1}
			 <img src="{$horse_pic}" border="1">
			 {else}
			 <img src="../uploaded_images/horse_images/thumb{$horse_pic}" border="1">{/if}{/if}            </td>
            <td   align="center" valign="top">
              {if $horse_silkpic neq NULL}
			  {if $silkimg_chk eq 1}
			 <img src="{$horse_silkpic}" border="1">
			 {else}
			  <img src="../uploaded_images/horse_images/thumb{$horse_silkpic}" border="1">{/if}{/if}            </td>
			</tr>
			</table></td>
          </tr>
				  
				  <!---------------------->
                      
                      <tr>
            <td width='150' class="fieldname">
            Name          </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="name" value="{$name}" > </td>
          </tr>
		   <tr>
            <td class="fieldname">
              Age/weight            </td>
            <td colspan="3" class="columnHeaderStatic">
              <!-- <input type="text" name="age" value="{$age}" >     -->   
			  <textarea name="age">{$age}</textarea></td>
          </tr><tr>
            <td width='150' class="fieldname">
              Purse            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="purse" value="{$purse}" > <!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{if $horse_id neq NULL}<a href="trainer_view.php?trainer={$horse_trainer}" class="link">See the trainer</a>{/if}-->            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Distance            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="distance" value="{$distance}" > <!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{if $horse_id neq NULL}<a href="owner_view.php?owner={$horse_owner}" class="link">See the owner</a>{/if}-->            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Suraface            </td>
            <td width="244" class="columnHeaderStatic">
              <input type="text" name="surface" value="{$surface}" >           
          </tr><tr>
            <td width='150' class="fieldname">
              Post            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="post" value="{$post}" >            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Grade            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="grade" value="{$grade}" >            </td>

          </tr>
		   <tr>
            <td width='150' class="fieldname">
              Turf            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="turf" value="{$turf}" >            </td>

          </tr>
                      <tr>
                        <td colspan="5" class="fieldname"><div align="center">
						<input type="hidden" name="horse_id" value="{$horse_id}">
						 <input type="hidden" name="horse_pic" value="{$horse_pic}">
						 <input type="hidden" name="horse_silkpic" value="{$horse_silkpic}">
						 <input type="hidden" name="start" value="{$start}" />
						  <input type="hidden" name="group_stakes" value="{$group_stakes}" />
						   <input type="hidden" name="act_BreedersCup" value="{$act_BreedersCup}" />
						   <input type="hidden" name="search_BreedersCup" value="{$search_BreedersCup}" />
                           {if $horse_id eq NULL} <input type="image" name="Submit" src="images/submit-button.gif"  style="width:62px;">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;">{/if}

                        </div></td>
                      </tr>
					  
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td></tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

