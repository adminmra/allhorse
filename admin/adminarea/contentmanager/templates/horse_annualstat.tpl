{include file="header.tpl"}
{include file="left.tpl"}

<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><? //include "patient_manage_top.php"; ?></td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <td height="30" align="right" bgcolor="#FFFFFF" style="padding-right:10px;"><strong class="bodyb">&#8249; </strong><a href="#" class="one"><b>Previous</b></a> | <a href="#" class="one"><b>Next</b></a> <strong class="one">&#8250;</strong></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">HORSE ANNUAL STATISTICS</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="40%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" bgcolor="#E4EBF6"><a href="add_horse_statsannual.php?horse_id={$horse_id}" class="one">Add</a></td>
                <td height="22" bgcolor="#E4EBF6"><a href="horse_view.php" class="one">Back to Horse </a></td>
				<td height="22" bgcolor="#E4EBF6">Total Records : {$total_records}</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" rowspan="2" bgcolor="#0D4686"><font style="color:#FFFFFF;"><b>Name</b></font></td>
			 <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Year</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Earning</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Races run</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Wins</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Places</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Shows</b></font></td>
            <td bgcolor="#0D4686" rowspan="2" class="bodyb"><font style="color:#FFFFFF;"><b>Edit</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Delete</b></font></td>
		  </tr>
          <tr align="center">
           
            </tr>
			{section name=horse_statsanual loop=$horsestatsanual_id}
          <tr align="center">
            <td height="18" bgcolor="#3D73B8"><font style="color:#FFFFFF;">
             {$horse_name}</font></td>
            <td height="18" bgcolor="#B5DE97">{$year_stat[horse_statsanual]}</td>
            <td height="18" bgcolor="#B5DE97">{$earning_stat[horse_statsanual]}</td>
            <td bgcolor="#B5DE97">{$starts_stat[horse_statsanual]}</td>
            <td bgcolor="#B5DE97">{$win_stat[horse_statsanual]}</td>
            <td bgcolor="#B5DE97">{$places_stat[horse_statsanual]}</td>
            <td bgcolor="#B5DE97">{$show_stat[horse_statsanual]}</td>
            <td bgcolor="#B5DE97"><a href="add_horse_statsannual.php?horsestatsanual_id={$horsestatsanual_id[horse_statsanual]}&horse_id={$horse_id}" class="seven">Edit</a></td>
            <td bgcolor="#B5DE97"><a href="?horsestatsanual_id={$horsestatsanual_id[horse_statsanual]}&act=del&horse_id={$horse_id}" onClick="return confirm('Are you sure?');" class="seven">Delete</a></td>
          </tr>
		  {sectionelse}
		  <tr><td height="50" bgcolor="#3D73B8" colspan="10" align="center"><font style="color:#FFFFFF;"><strong>
             No anual statistics available</strong></font></td></tr>
			 {/section}
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
{include file="footer.php"}