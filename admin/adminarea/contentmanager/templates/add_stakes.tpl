{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
<style type="text/css">

</style>
{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  " align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">ADD 
            STAKES</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>&nbsp;
		  <a href="stake_view.php"><strong><font color="#FFFFFF">&laquo; Back to Stakes</font></strong></a> <a href="horse_history.php?stakes={$stake_id}"><strong><font color="#FFFFFF">&laquo; Add Historical Data</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="stakes_form" onSubmit="return chk_stake('{$stake_id}');">
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      
                      <tr>
            <td width='150' class="fieldname">
              Stakes Name            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_name" value="{$stake_name}" id="affiliateFirstName">            </td>
          </tr><tr>
            <td width='150' class="fieldname">
              Trackcode            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_trackcode" value="{$stake_trackcode}" id="affiliateFirstName">            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Date            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_date" value="{$stake_date}" id="affiliateFirstName">            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Grade            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_grade" value="{$stake_grade}" id="affiliateFirstName">            </td>
          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Purse            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_purse" value="{$stake_purse}" id="affiliateFirstName">            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              HTA</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_hta" value="{$stake_hta}" id="affiliateFirstName">            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Distance            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_distance" value="{$stake_distance}" id="affiliateFirstName">            </td>
          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Horse Age</td>
            <td class="columnHeaderStatic">
              <input type="text" name="horse_age" value="{$horse_age}" id="affiliateFirstName">            </td>
          </tr>
		   <tr>
            <td width='150' class="fieldname">
              Stake sex</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_sex" value="{$stake_sex}" id="affiliateFirstName">            </td>
          </tr> 
		  
		  <tr>
            <td width='150' class="fieldname">
              Turf</td>
            <td class="columnHeaderStatic">
              <input type="text" name="stake_turf" value="{$stake_turf}" id="">            </td>
          </tr> 
		  <tr>
		    <td class="fieldname">Post Time </td>
		    <td class="columnHeaderStatic"><input type="text" name="stake_posttime" value="{$stake_posttime}" id="stake_posttime" /></td>
		    </tr>
		  <tr>
            <td width='150' class="fieldname" valign="top">
             AHR Notes            </td>
            <td class="columnHeaderStatic">
               <textarea name="stake_notes"  id="affiliateFirstName" rows="4">{$stake_notes}</textarea>            </td>
          </tr>
		   <tr>
            <td width='150' class="fieldname" valign="top">
            GHB Notes            </td>
            <td class="columnHeaderStatic">
               <textarea name="stake_notes_GHB"  id="affiliateFirstName" rows="4">{$stake_notes_GHB}</textarea>            </td>
          </tr>
		  		  <tr>
            <td width='150' class="fieldname" valign="top">
             HB Notes            </td>
            <td class="columnHeaderStatic">
               <textarea name="stake_notes_HB"  id="affiliateFirstName" rows="4">{$stake_notes_HB}</textarea>            </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Logo</td>
            <td class="columnHeaderStatic">
               <input type="file" name="logo" >            </td>
          </tr>
		  
		  
                      <tr>
                        <td colspan="2" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="stake_id" value="{$stake_id}">
						 <input type="hidden" name="stake_pic" value="{$stake_pic}">
						 <input type="hidden" name="start" value="{$start}" />
                           {if $stake_id eq NULL} <input name="submit21" type="image" src="images/submit-button.gif" style="width:62px;">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;">{/if}

                        </div></td>
                      </tr>
					   <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import all records from .CSV file
                        </div></td>
                      </tr><tr>
                        <td colspan="3" class="fieldname"><div align="center" style="padding-right:100px ">
						<input type="file" name="import_stake">
                        </div></td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

