{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%"  align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  ">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $owner_id eq NULL}ADD 
            OWNER{else} EDIT OWNER{/if}</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>&nbsp;
		  <a href="owner_view.php" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back to owners</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
		{if $pic|count_characters > 3}
		<tr> 
          <td height="10" colspan="2" align="center"><img src="{$pic}" ></td>
        </tr>
		{/if}
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="owners_form" onSubmit="return chk_owner('{$owner_id}');">
                <tr>
                <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
				  <!--------------------->
				  
				  
				  <!---------------------->
                      
                      <tr>
            <td width='150' class="fieldname">
              Name
            </td>
            <td class="columnHeaderStatic">
              <input type="text"  name="owner_name" value="{$owner_name}" id="affiliateFirstName">
            </td>

          </tr>
		   <tr>
            <td class="fieldname">
              Country</td>
            <td class="columnHeaderStatic">
             <select name="owner_country" >
			{html_options values=$country_name output=$country_name selected=$owner_country}
			</select>
             </td>
          </tr><tr>
            <td width='150' class="fieldname">
              State</td>
            <td class="columnHeaderStatic">
              <input type="text" name="owner_state" value="{$owner_state}" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              City</td>
            <td class="columnHeaderStatic">
              <input type="text" name="owner_city" value="{$owner_city}" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Stable</td>
            <td class="columnHeaderStatic">
              <input type="text" name="owner_stable" value="{$owner_stable}" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              family</td>
            <td class="columnHeaderStatic">
              <input type="text" name="owner_family" value="{$owner_family}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Url</td>
            <td class="columnHeaderStatic">
              <input type="text" name="owner_url" value="{$owner_url}" id="affiliateFirstName">
            </td>
          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Notes
            </td>
            <td class="columnHeaderStatic">
              <textarea name="owner_notes" rows="3">{$owner_notes}</textarea>
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Image
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="pic" value="{$pic}">
            </td>

          </tr>
		  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import all records from .CSV file 
                        </div></td>
                      </tr>
		  <tr>
                        <td colspan="3" class="fieldname"><div align="center" style="padding-right:100px ">
						<input type="file" name="import_owner">
                        </div></td>
                      </tr>
                      <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						<input type="hidden" name="owner_id" value="{$owner_id}">
						 <input type="hidden" name="owner_pic" value="{$owner_pic}">
						 <input type="hidden" name="owner_silkpic" value="{$owner_silkpic}">
						 <input type="hidden" name="start" value="{$start}" />
                           {if $owner_id eq NULL}<input type="image" name="Submit" src="images/submit-button.gif"  style="width:62px;">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;">{/if}

                        </div></td>
                      </tr>
					 
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td></tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

