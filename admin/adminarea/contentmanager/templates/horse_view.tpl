{include file="header.tpl"}
{include file="left.tpl"}
{literal}
<script language="javascript" type="text/javascript">
function search_horse()
{
document.horse_search.action="?act=search_horse";
document.horse_search.submit();
}

</script>
{/literal}
<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><? //include "patient_manage_top.php"; ?></td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <td height="30" width="100%" align="right" bgcolor="#FFFFFF" style="padding-left:600px;"><strong >{$paging}</strong></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">HORSES</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" width="10%" bgcolor="#E4EBF6"><a href="add_horse.php" class="one" >Add</a></td>
               <td width="75%" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="15%" height="22" bgcolor="#E4EBF6" align="center"><a href="import_horse_records.php"><img src="images/import.gif" border="0"></a></td>
               
                
              </tr>
			  <tr align="center">
			  <td colspan="2" width="100%">
			  <table width="100%" bgcolor="#FFFFFF">
			  <tr>
			  <form name="horse_search" method="post" action="?act_horse=search">
               <td height="22" width="35%" bgcolor="#FFFFFF" colspan="1"> Search by Name : <input type="text" name="search_horse" style="width:100px; " value="{$search_value}" ></td>
			   
			   <td height="22" width="65%" bgcolor="#FFFFFF" colspan="4" align="left" valign="middle"><input type="image" src="images/botton-seeresults.gif" style="width:85px; height:18px;" /> &nbsp; <strong style="padding-bottom:5px;">Total records : {$record_count}</strong>
				                                            </td>
				
                  
				 </form>          
              </tr>
			  </table>
			  </td>
			  </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" rowspan="2" bgcolor="#0D4686"><a href="?sort_by=horse" class="seven"><font style="color:#FFFFFF;"><b>Horse name</b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2"><a href="?sort_by=status" class="seven"><font style="color:#FFFFFF;"><b>Status
               </b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><a href="?sort_by=owner" class="seven"><font style="color:#FFFFFF;"><b>Owner</b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Horse fact</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Horse Picture</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Horse Video</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Annual Statistics</b></font></td>
            <td bgcolor="#0D4686" rowspan="2" class="bodyb"><font style="color:#FFFFFF;"><b>Lifetime Statistics</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" colspan="2" class="header"><font style="color:#FFFFFF;"><b>Action</b></font></td>
           <!-- <td height="40" rowspan="2" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">Action</font></td>-->
		  </tr>
          <tr align="center">
           
            </tr>
			{assign var="indexvalue" value=0}
					  {section name=horse loop=$horse_id}
          <tr align="center">
            <td height="18" bgcolor="#3D73B8" align="left"><a href="add_horse.php?horse_id={$horse_id[horse]}&start={$start}&act_horse=search&search_horse={$search_value}" class="seven2">
             {$name[horse]}</a></td>
            <td height="18" bgcolor="#B5DE97" align="left">{$status[horse]}</td>
            <td height="18" bgcolor="#B5DE97" align="left">{$owner[horse]}</td>
            <td bgcolor="#B5DE97"><a href="horse_fact.php?horse_id={$horse_id[horse]}" class="seven">Facts</a></td>
            <td bgcolor="#B5DE97"><a href="horse_pics.php?horse_id={$horse_id[horse]}" class="seven">Pictures</a></td>
            <td bgcolor="#B5DE97"><a href="horse_videos.php?horse_id={$horse_id[horse]}" class="seven">Videos</a></td>
            <td bgcolor="#B5DE97"><a href="horse_annualstat.php?horse_id={$horse_id[horse]}" class="seven">Annual Statistics</a></td>
            <td bgcolor="#B5DE97"><a href="horse_lifetimestat.php?horse_id={$horse_id[horse]}" class="seven">Lifetime Statistics</a></td>
            <td bgcolor="#B5DE97"><a href="add_horse.php?horse_id={$horse_id[horse]}&start={$start}&act_horse=search&search_horse={$search_value}" class="seven">[Edit]</a></td>
            <td bgcolor="#B5DE97"><a href="?act=del&horse_id={$horse_id[horse]}" class="seven" onClick="return confirm('Are you sure to delete?')">[Delete]</a><!--<input type="checkbox" name="checkbox" value="checkbox" style="border-color:#B5DE97;" />--></td>
          </tr>
		 {assign var="indexvalue" value=$indexvalue+1}
		 {sectionelse}
		  <tr align="center">
            <td height="30" bgcolor="#3D73B8" colspan="11"><font style="color:#FFFFFF;"><strong>No data available</strong></font></td>
			</tr>
         
                    {/section}
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
{include file="footer.php"}