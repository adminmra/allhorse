{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
<style type="text/css">

</style>
{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  " align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $racetrack_id eq NULL}ADD 
            RACETRACK{else} EDIT RACETRACK{/if}</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>&nbsp;
		  <a href="racetrack_view.php" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back to Racetracks</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="racetracks_form" onSubmit="return chk_racetrack('{$racetrack_id}');">
                <tr>
                <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
				  <!--------------------->
				  <tr>
				  <td colspan="3" width="100%">
				  <table >
				  <tr><td class="fieldname" width="50%" align="center">Logo</td><td class="fieldname" width="50%" align="center">Driving Picture</td></tr>
				  <tr>
            <td  class="fieldname" align="center" valign="top" width="50%" >
			 {if $logo neq NULL}{if $img_chk eq 1}
			 <img src="../../images/cm/{$logo}" border="1" width="200" height="200" >
			 {else}
             <!-- <img src="../uploaded_images/racetrack_images/thumb{$logo}" border="1"> -->
			 <img src="../../images/racetracks/{$logo}" border="1">{/if}{/if}
            </td>
            <td   align="center" valign="top" width="50%">
			{if $driving_pic neq NULL}{if $imgdriving_chk eq 1}
			 <img src="../../images/cm/{$driving_pic}" border="1" width="300" height="300">
			 {else}
              <img src="../uploaded_images/racetrack_images/thumb{$driving_pic}" border="1">{/if}{/if}
            </td>
			</tr>
			</table>
</td>
          </tr>
				  
				  <!---------------------->
                      
                      <tr>
            <td width='150' class="fieldname">
              Name
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_name" value="{$racetrack_name}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Url
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_url" value="{$racetrack_url}" id="affiliateFirstName" style="width:450px; ">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Track Code
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_track_code" value="{$racetrack_track_code}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname" valign="top">
              History
            </td>
            <td class="columnHeaderStatic">
              <!-- <input type="text" name="racetrack_history" value="{$racetrack_history}" id="affiliateFirstName" > -->
			  <textarea name="racetrack_history" style="width:450px; ">{$racetrack_history}</textarea>
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Opening Date
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_opening_date" value="{$racetrack_opening_date}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Closing Day
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_closing_date" value="{$racetrack_closing_date}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Racing days
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_racing_days" value="{if $racetrack_racing_days != '0'}{$racetrack_racing_days}{/if}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Street1
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_street1" value="{$racetrack_street1}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Street2
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_street2" value="{$racetrack_street2}" id="affiliateFirstName">
            </td>

          </tr>
		   <tr>
            <td class="fieldname">
              Country
            </td>
            <td class="columnHeaderStatic">

              <select name="racetrack_country">
{html_options values=$country_name output=$country_name selected=$racetrack_country}</select>
            </td>
          </tr><tr>
            <td width='150' class="fieldname">
              State
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_state" value="{$racetrack_state}" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              City
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_city" value="{$racetrack_city}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Zip
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_zip" value="{$racetrack_zip}" id="affiliateFirstName">
            </td>

          </tr><tr>
            <td width='150' class="fieldname" valign="top">
              Driving
            </td>
            <td class="columnHeaderStatic">
			<textarea name="racetrack_driving" style="width:450px; " >{$racetrack_driving}</textarea>
              <!-- <input type="text" name="racetrack_driving" value="{$racetrack_driving}" id="affiliateFirstName"> -->
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Phone1
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_phone1" value="{$racetrack_phone1}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Phone2
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_phone2" value="{$racetrack_phone2}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Hta
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="hta" value="{$racetrack_hta}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Weather
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_weather" value="{$racetrack_weather}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Start Date
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_start_date" value="{$racetrack_start_date}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Track Length
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_track_lenth" value="{$racetrack_track_lenth}" id="affiliateFirstName">
            </td>

          </tr><tr>
            <td width='150' class="fieldname">
              Stretch Length
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_strech_lenth" value="{$racetrack_strech_lenth}" id="">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Stretch Width
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_strech_width" value="{$racetrack_stretch_width}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
             Speed Rating
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_speed_rating" value="{$racetrack_speed_rating}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Infield Capacity
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_cap" value="{if $racetrack_cap != '0'}{$racetrack_cap}{/if}" id="">
            </td>

          </tr>
		     <tr>
            <td width='150' class="fieldname">
              Clubhouse Capacity
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_cap_club" value="{if $racetrack_cap_club != '0'}{$racetrack_cap_club}{/if}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Grand Stand Capacity
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_cap_ground" value="{if $racetrack_cap_ground != '0'}{$racetrack_cap_ground}{/if}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
             Parking Capacity
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="racetrack_cap_parking" value="{if $racetrack_cap_parking != '0'}{$racetrack_cap_parking}{/if}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Price General Admission
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="pricega" value="{$pricega}" id="affiliateFirstName">
            </td>

          </tr>
		     <tr>
            <td width='150' class="fieldname">
              Price Clubhouse
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="pricech" value="{$pricech}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Price Turf Club
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="pricetc" value="{$pricetc}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname" valign="top">
              Points of Interest
            </td>
            <td class="columnHeaderStatic">
			<textarea name="point_interest" style="width:450px; ">{$point_interest}</textarea>
            <!--   <input type="text" name="point_interest" value="{$point_interest}" id="affiliateFirstName"> -->
            </td>

          </tr>
		  
		   <tr>
            <td width='150' class="fieldname">
             Logo 
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="logo" value="{$logo}" />
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
             Live
            </td>
            <td class="columnHeaderStatic">
              <input  type="checkbox" name="chk_box" value="yes" {if $live == 'Y'} checked {/if}/>
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
             Driving Picture
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="driving_pic" value="{$driving_pic}" />
            </td>

          </tr>
		   <tr>
            <td width='150' class="fieldname">
            Timezone
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="timezone" value="{$timezone}" />
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
            Live Video Url
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="uvideourl" value="{$uvideourl}"  style="width:450px; "/>
            </td>

          </tr>
		  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import all records from .CSV file 
                        </div></td>
                      </tr><tr>
                        <td colspan="3" class="fieldname"><div align="center" style="padding-right:0px ">
						<input type="file" name="import_racetrack">
                        </div></td>
                      </tr>
                      <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						<input type="hidden" name="racetrack_id" value="{$racetrack_id}">
						 <input type="hidden" name="logo1" value="{$logo}">
						 <input type="hidden" name="driving_pic1" value="{$driving_pic}">
						 <input type="hidden" name="start" value="{$start}" />
                           {if $racetrack_id eq NULL} <input name="select-all2" type="image" src="images/submit-button.gif" style="width:62px;">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;">{/if}

                        </div></td>
                      </tr>
					  
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td></tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

