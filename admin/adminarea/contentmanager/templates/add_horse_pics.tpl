{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>

{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">ADD 
            HORSE PICTURES</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="horse_pics.php?horse_id={$horse_id}" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="horse_pics_add" onSubmit="return chk_horse_pic('{$horsepics_id}');">
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      
                     {if $horsepics_id neq NULL} <tr>
            <td width='150' class="fieldname">
              Picture           </td>
            <td class="columnHeaderStatic">
              {if $horsepics neq null} 
			  {if $img_chk eq 1}
			 <img src="{$horsepics}" border="1" width="300" height="250">
			 {else}
			 <img src="../uploaded_images/horse_images/horse_pics/thumb{$horsepics}" border="1">{/if}{/if}           </td>
          </tr>{/if}
		  <tr>
            <td width='150' class="fieldname">
             upload picture           </td>
            <td class="columnHeaderStatic">
             <input type="file" name="horse_image" /> </td>
          </tr>
		  <tr>
            <td width='150' class="fieldname" colspan="2" align="center">
             Or insert URL           </td>
            
          </tr>
		  <tr>
            <td width='150' class="fieldname">
             Url           </td>
            <td class="columnHeaderStatic">
             <input type="text" name="horse_url" value="{$horsepics}"/> </td>
          </tr>
                      <tr>
                        <td colspan="2" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="horse_id" value="{$horse_id}">
						<input type="hidden" name="horsepics_id" value="{$horsepics_id}">
						 <input type="hidden" name="horse_pic" value="{$horsepics}">
                           {if $horsepicss_id eq NULL}  <input name="select-all2" type="image" src="images/submit-button.gif" style="width:60px">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:60px">{/if}

                        </div></td>
                      </tr>
					  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import 
                        </div></td>
                      </tr><tr>
                        <td colspan="2" class="fieldname">
						<div align="center" style="padding-right:100px ">
						<input type="file" name="import_horse_pic">
                        </div></td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

