



<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" bgcolor="#FFFFFF"><? //include "patient_manage_top.php"; ?></td>
  </tr>
  
  
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      
      <tr>
        <td height="25"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
         
        </table></td>
      </tr>
	  
      <tr>
        <td height="25" valign="top"><table width="100%" border="1">
          <tr>
            <td width="16%" align="left" valign="middle">Connections</td>
            <td width="4%" align="center" valign="middle"><strong>:</strong></td>
            <td width="80%">&nbsp;</td>
          </tr>
		  {foreach from=$horse_record_array key=horseId item=i}
          <tr>
            <td align="left" valign="middle"><strong>Trainer</strong></td>
            <td align="center" valign="middle"><strong>:</strong></td>
            <td align="left" valign="middle">{if $i.0 eq ""}
				-
				{else}
				{$i.0}
				{/if}</td>
          </tr>
          <tr>
            <td align="left" valign="middle"><strong>Jockey</strong></td>
            <td align="center" valign="middle"><strong>:</strong></td>
            <td align="left" valign="middle">{if $i.1 eq ""}
				-
				{else}
				{$i.1}
				{/if}				</td>
          </tr>
          <tr>
            <td align="left" valign="middle"><strong>Owner</strong></td>
            <td align="center" valign="middle"><strong>:</strong></td>
            <td align="left" valign="middle">{if $i.2 eq ""}
				-
				{else}
				{$i.2}
				{/if}</td>
          </tr>
          <tr>
            <td align="left" valign="middle">&nbsp;</td>
            <td align="center" valign="middle">&nbsp;</td>
            <td align="left" valign="middle">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="middle">Stats</td>
            <td align="center" valign="middle"><strong>:</strong></td>
            <td align="left" valign="middle">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="middle"><strong>Foaling Date</strong></td>
            <td align="center" valign="middle"><strong>:</strong></td>
            <td align="left" valign="middle">{if $i.3 eq ""}
				-
				{else}
				{$i.3}
				{/if}</td>
          </tr>
          <tr>
            <td align="left" valign="middle"><strong>Dosage Profile</strong></td>
            <td align="center" valign="middle"><strong>:</strong></td>
            <td align="left" valign="middle">{if $i.4 eq ""}
				-
				{else}
				{$i.4}
				{/if}</td>
          </tr>
          <tr>
            <td align="left" valign="middle"><strong>Dosage Index/CD</strong> </td>
            <td align="center" valign="middle"><strong>:</strong></td>
            <td align="left" valign="middle">{if $i.5 eq ""}
				-
				{else}
				{$i.5}
				{/if}</td>
          </tr>
          <tr>
            <td align="left" valign="middle"><strong>Pedigree</strong></td>
            <td align="center" valign="middle"><strong>:</strong></td>
            <td align="left" valign="middle">{if $i.6 eq ""}
				-
				{else}
				{$i.6}
				{/if}</td>
          </tr>
          <tr>
            <td align="left" valign="middle"><strong>Supplement</strong></td>
            <td align="center" valign="middle"><strong>:</strong></td>
            <td align="left" valign="middle">{if $i.7 eq ""}
				-
				{else}
				{$i.7}
				{/if}</td>
          </tr>
		  {/foreach}
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
