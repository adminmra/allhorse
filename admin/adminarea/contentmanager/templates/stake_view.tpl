{include file="header.tpl"}
{include file="left.tpl"}
{literal}
<style type="text/css">

</style>
<script type="text/javascript">
function LoadStakes(a)
{
	//alert(a.value);
	if(a.value == 0)
	{
	window.location="stake_view.php";
	}else
	{
	window.location="?act_stakes=track&code="+a.value;
	}
}
</script>
{/literal}
<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><? //include "patient_manage_top.php"; ?></td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <td height="30" align="right" bgcolor="#FFFFFF" style="padding-right:10px;"><strong class="bodyb">{$paging}</strong></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">STAKES</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22" align="left"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" bgcolor="#E4EBF6" align="center" width="10%" ><a href="add_stakes.php" class="one">Add</a>&nbsp;|&nbsp;<a href="stake_view.php" class="one">View List</a></td><td bgcolor="#FFFFFF" align="right">
				<select name="racetrack"  onChange="return LoadStakes(this);">
				<option value="0">Select Racetrack</option>
				{html_options options=$racetracks selected=$mySelect}
				</select>
				</td>
               
                
              </tr>
            </table></td>
          </tr>
		  <!-------------------------------->
		  <tr align="center">
			  <td colspan="2" width="100%">
			  <table width="100%" bgcolor="#FFFFFF">
			  <tr>
			  <form name="stakes_search" method="post" action="?act_stakes=search">
                <td height="22" width="35%" bgcolor="#FFFFFF" colspan="1"> Search by Name : <input type="text" name="search_stakes" style="width:100px; " value="{$search_value}" ></td>
			   
			   <td height="22" width="65%" bgcolor="#FFFFFF" colspan="4" align="left"><input type="image" src="images/botton-seeresults.gif" style="width:85px; height:18px;" />&nbsp; <strong style="padding-bottom:5px;">Total records : {$record_count}</strong>
				                                            </td>
				
                  
				 </form>          
              </tr>
			  </table>
			  </td>
			  </tr>
		  <!--------------------------------->
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" rowspan="2" bgcolor="#0D4686"><a href="?sort_by=name" class="seven"><font style="color:#FFFFFF;"><b>Stakes name</b></font></a></td>
            
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Trackcode</b></font></td>
			 <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><a href="?sort_by=date" class="seven"><font style="color:#FFFFFF;"><b>Date</b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Purse</b></font></td>
			<td height="20" bgcolor="#0D4686" rowspan="2" class="header"><a href="?sort_by=horseage" class="seven"><font style="color:#FFFFFF;"><b>Horse Age</b></font></a></td>
			<td bgcolor="#0D4686" rowspan="2" class="bodyb"><font style="color:#FFFFFF;"><b>Stake Sex</b></font></td>
			<td height="20" bgcolor="#0D4686" rowspan="2"><a href="?sort_by=grade" class="seven"><font style="color:#FFFFFF;"><b>Grade</b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><a href="?sort_by=distance" class="seven"><font style="color:#FFFFFF;"><b>Distance</b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Turf</b></font></td>
            	
            <td height="20" bgcolor="#0D4686" rowspan="2" colspan="2" class="header"><font style="color:#FFFFFF;"><b>Action</b></font></td>
           <!-- <td height="40" rowspan="2" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">Action</font></td>-->
		  </tr>
          <tr align="center">
           
            </tr>
			{assign var="indexvalue" value=0}
					  {section name=stake loop=$stake_id}
          <tr align="center">
            <td height="18" bgcolor="#3D73B8" align="left"><a href="add_stakes.php?stake_id={$stake_id[stake]}&start={$start}" class="seven2">
             {$stake_name[stake]}</a></td>
            <td height="18" bgcolor="#B5DE97">{$stake_trackcode[stake]}</td>
			<td height="18" bgcolor="#B5DE97">{$stake_date[stake]}</td>
            <td height="18" bgcolor="#B5DE97">{$stake_purse[stake]}</td>
			<td height="18" bgcolor="#B5DE97">{$stake_horseage[stake]}</td>
			<td height="18" bgcolor="#B5DE97">{$stake_stakessex[stake]}</td>
			<td height="18" bgcolor="#B5DE97">{$stake_grade[stake]}</td>
			<td height="18" bgcolor="#B5DE97">{$stake_distance[stake]}</td>
			<td height="18" bgcolor="#B5DE97">{$stake_turf[stake]}</td>
            
            <td bgcolor="#B5DE97"><a href="add_stakes.php?stake_id={$stake_id[stake]}&start={$start}" class="seven">[Edit]</a></td>
            <td bgcolor="#B5DE97"><a href="?act=delete&stake_id={$stake_id[stake]}" onClick="return confirm('Are you sure to delete?')" class="seven">[Delete]</a><!--<input type="checkbox" name="checkbox" value="checkbox" style="border-color:#B5DE97;" />--></td>
          </tr>
		 {assign var="indexvalue" value=$indexvalue+1}
		 {sectionelse}
		  <tr align="center">
            <td height="30" bgcolor="#3D73B8" colspan="11"><font style="color:#FFFFFF;"><strong>No data available</strong></font></td>
			</tr>
                    {/section}
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
{include file="footer.php"}