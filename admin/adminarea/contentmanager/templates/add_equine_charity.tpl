{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>


{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%"  align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  ">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $equine_charity_id eq NULL}ADD 
            EQUINE CHARITY{else} EDIT EQUINE CHARITY{/if}</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="equine_charities_view.php" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  align="center" cellpadding="0" cellspacing="0" >
              <form action="" method="post" enctype="multipart/form-data" name="equine_charitys_form" onSubmit="return chk_equine_charity('{$equine_charity_id}');">
                <tr>
                <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
				  <!--------------------->
				  
				  
				  <!---------------------->
                      
                      <tr>
            <td width='150' class="fieldname">
              Name
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="equine_charity_name" value="{$equine_charity_name}" id="affiliateFirstName">
            </td>

          </tr>
		   <tr>
            <td class="fieldname">
              Url
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="equine_charity_url" value="{$equine_charity_url}" id="affiliateFirstName">
             </td>
          </tr><tr>
            <td width='150' class="fieldname">
              Country
            </td>
            <td class="columnHeaderStatic">
			<select name="equine_charity_country">
              {html_options values=$country_name output=$country_name selected=$equine_charity_country}
			  </select>
			  </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              State
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="equine_charity_state" value="{$equine_charity_state}" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              City
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="equine_charity_city" value="{$equine_charity_city}" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Notes
            </td>
            <td class="columnHeaderStatic">
              <textarea name="equine_charity_notes" id="affiliateFirstName">{$equine_charity_notes}</textarea>
            </td>

          </tr>
		  
		  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import 
                        </div></td>
                      </tr><tr>
                        <td colspan="2" class="fieldname">
						<div align="center" style="padding-right:100px ">
						<input type="file" name="import_equine_charity">
                        </div></td>
                      </tr>
                      <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						<input type="hidden" name="equine_charity_id" value="{$equine_charity_id}">
						 <input type="hidden" name="equine_charity_pic" value="{$equine_charity_pic}">
						 <input type="hidden" name="equine_charity_silkpic" value="{$equine_charity_silkpic}">
						 <input type="hidden" name="start" value="{$start}" />
                           {if $equine_charity_id eq NULL} <input type="image" name="Submit" src="images/submit-button.gif"  style="width:62px;">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;">{/if}

                        </div></td>
                      </tr>
					  
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td></tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

