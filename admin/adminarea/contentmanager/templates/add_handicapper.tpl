{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
<style type="text/css">

</style>
{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  " align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $handicapper_id eq NULL}ADD 
            {else}EDIT {/if}HANDICAPPER</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>&nbsp;
		  <a href="handicapper_view.php"><strong><font color="#FFFFFF">&laquo; Back to handicappers</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="handicappers_form" onSubmit="return chk_handicapper('{$handicapper_id}');">
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                      
                      <tr>
            <td width='150' class="fieldname">
              Company Name
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="handicapper_name" value="{$handicapper_name}" id="affiliateFirstName">
            </td>

          </tr>
		   <tr>
            <td class="fieldname">
              Country
            </td>
            <td class="columnHeaderStatic">

             <select name="handicapper_country" >
           {html_options values=$country_name output=$country_name selected=$handicapper_country}</select>
            </td>
          </tr><tr>
            <td width='150' class="fieldname">
              State
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="handicapper_state" value="{$handicapper_state}" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              City
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="handicapper_city" value="{$handicapper_city}" id="affiliateFirstName">
            </td>

          </tr> 
		  <tr>
            <td width='150' class="fieldname">
              Url
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="handicapper_url" value="{$handicapper_url}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              email</td>
            <td class="columnHeaderStatic">
              <input type="text" name="handicapper_email" value="{$handicapper_email}" id="affiliateFirstName">
            </td>

          </tr>
		  
		  <tr>
            <td width='150' class="fieldname">
              Street1</td>
            <td class="columnHeaderStatic">
              <input type="text" name="handicapper_street1" value="{$handicapper_street1}" id="affiliateFirstName">
            </td>
          </tr>
		   <tr>
            <td width='150' class="fieldname">
              Street2</td>
            <td class="columnHeaderStatic">
              <input type="text" name="handicapper_street2" value="{$handicapper_street2}" id="affiliateFirstName">
            </td>

          </tr> 
		  
		  <tr>
            <td width='150' class="fieldname">
              Phone1</td>
            <td class="columnHeaderStatic">
              <input type="text" name="handicapper_phone1" value="{$handicapper_phone1}" id="">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Phone2</td>
            <td class="columnHeaderStatic">
              <input type="text" name="handicapper_phone2" value="{$handicapper_phone2}" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Information
            </td>
            <td class="columnHeaderStatic">
               <textarea name="handicapper_info"  id="affiliateFirstName" rows="4">{$handicapper_info}</textarea>
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Comments
            </td>
            <td class="columnHeaderStatic">
               <textarea name="handicapper_comments"  id="affiliateFirstName" rows="4">{$handicapper_comments}</textarea>
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Contact
            </td>
            <td class="columnHeaderStatic">
               <textarea name="handicapper_contact"  id="affiliateFirstName" rows="4">{$handicapper_contact}</textarea>
            </td>

          </tr>
		  
                      <tr>
                        <td colspan="2" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="handicapper_id" value="{$handicapper_id}">
						 <input type="hidden" name="handicapper_pic" value="{$handicapper_pic}">
						 <input type="hidden" name="start" value="{$start}" />
                           {if $handicapper_id eq NULL} <input name="submit21" type="image" src="images/submit-button.gif" style="width:62px;">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;">{/if}

                        </div></td>
                      </tr>
					  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import 
                        </div></td>
                      </tr><tr>
                        <td colspan="3" class="fieldname"><div align="center" style="padding-right:100px ">
						<input type="file" name="import_handicapper">
                        </div></td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

