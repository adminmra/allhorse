{include file="header.tpl"}
{include file="left.tpl"}
{literal}
<script language="javascript" type="text/javascript">
function search_horse()
{
document.horse_search.action="?act=search_breeders";
document.horse_search.submit();
}

</script>
{/literal}

{literal}
<script>
function check(stakes)
{ document.horse_search.action="belmont.php?act_BreedersCup=search";
document.horse_search.submit();
 
}
</script>
<style type="text/css">
.smalltext
{
width:40px;
}
.mediumtext
{
width:120px;
}
</style>
{/literal}
<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><? //include "patient_manage_top.php"; ?></td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <td height="30" width="100%" align="right" bgcolor="#FFFFFF" style="padding-left:600px;"><strong >{$paging}</strong></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">2009 Belmont Stakes</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <!-- <tr>
        <td height="25"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" width="10%" bgcolor="#E4EBF6"><a href="add_derby.php" class="one" >Add</a></td>
               <td width="75%" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="15%" height="22" bgcolor="#FFFFFF" align="center"><a href="#"><img src="images/import.gif" border="0">&nbsp;</a></td>
               
                
              </tr>
			  <tr align="center">
			  <td colspan="2" width="100%">
			  <table width="100%" bgcolor="#FFFFFF">
			   <form name="horse_search" method="post" action="?act_BreedersCup=search">
			  <tr>
			 
               <td height="22" width="35%" bgcolor="#FFFFFF" colspan="1"> Search by Name : <input type="text" name="search_BreedersCup" style="width:100px; " value="{$search_value}" ></td>
			   
			   <td height="22" width="65%" bgcolor="#FFFFFF" colspan="4" align="left" valign="middle"><input type="image" src="images/botton-seeresults.gif" style="width:85px; height:18px;" /> 
			     &nbsp; <strong style="padding-bottom:5px;">Total records : {$record_count}</td>
                  
				         
              </tr> 
			<tr>
			 
            <td height="22" width="35%" bgcolor="#FFFFFF" colspan="2">Group By Stakes : 
			     <select name="group_stakes" onchange="check(this.value)">
				 <option value="">Select Stakes</option>
			   {html_options values=$horse_stakes_group output=$horse_stakes_group selected=$group_stakes}
                  </select></td>
                  
				         
              </tr> 			  </form> 
			  </table>
			  </td>
			  </tr>
            </table></td>
          </tr>
        </table></td>
      </tr> -->
      <tr>
        <td height="25" valign="top">
		
		<form action="" name="frmnew" method="post">
		<table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" rowspan="2" bgcolor="#0D4686"><font style="color:#FFFFFF;"><b>Horse</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2"><font style="color:#FFFFFF;"><b>Odds</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Jockey</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Post</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Result</b></font></td>
           <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Result Time</b></font></td> 
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Trainer</b></font></td>
			 <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Earnings</b></font></td> 
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Breeder</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" colspan="2" class="header"><font style="color:#FFFFFF;"><b>Action</b></font></td>
          
		  </tr>
          <tr align="center">
           <!-- <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb">&nbsp;</td>-->
            </tr>
			{assign var="indexvalue" value=0}
					  {section name=horse loop=$2006BreedersCup_id}
          <tr align="center">
            <td height="18" bgcolor="#3D73B8" align="left"><font style="color:#FFFFFF;"><input type="text" value="{$horse_name[horse]}" name="horse[]" class="mediumtext"></font></td>
            <td height="18" bgcolor="#B5DE97" align="left"><input type="text" value="{$horse_odds[horse]}" name="odds[]" class="smalltext"></td>
            <td height="18" bgcolor="#B5DE97" align="left"><input type="text" value="{$horse_jockey[horse]}" name="jockey[]" class="mediumtext"></td>
            <td bgcolor="#B5DE97"><input type="text" value="{$horse_post[horse]}" name="post[]" class="smalltext"></td>
            <td bgcolor="#B5DE97"><input type="text" value="{$horse_result[horse]}" name="result[]"   class="smalltext"></td>
           <td bgcolor="#B5DE97"><input type="text" value="{$horse_stakes[horse]}" name="stakes[]" class="smalltext"></td>
             <td bgcolor="#B5DE97"><input type="text" value="{$trainer[horse]}" name="trainer[]" class="mediumtext"></td>
           <td bgcolor="#B5DE97"><input type="text" value="{$earnings[horse]}" name="earnings[]"  style="width:75px; "></td>
             <td bgcolor="#B5DE97"><input type="text" value="{$breeder[horse]}" name="breeder[]" class="mediumtext"></td>
            <td bgcolor="#B5DE97">{if $2006BreedersCup_id[horse]|count_characters:true > 0}<a href="?act=delete&BreedersCup_id={$2006BreedersCup_id[horse]}" class="seven" onClick="return confirm('Are you sure to delete?')">[Delete]</a>{/if}</td>
          </tr>
		  {assign var="indexvalue" value=$indexvalue+1}
		 {sectionelse}
		  <tr align="center">
            <td height="18" bgcolor="#3D73B8" colspan="11"><strong>No data available</strong></td>
			</tr>
                    {/section}
			
         <tr><td colspan="7" align="center"><input type="image" src="images/submit-button.gif" style="width:85px; height:18px;" name="submitodds" value="submitodds"/></td></tr>
		 
        </table>
		</form>
		
		</td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
