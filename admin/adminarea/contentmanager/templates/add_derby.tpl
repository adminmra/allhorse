{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>


{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%"  align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  ">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $horse_id eq NULL}ADD 
            2007 Kentucky Derby {else} EDIT 2007 Kentucky Derby{/if}</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>&nbsp;
		  <a href="2007_Kentucky_derby.php" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back to 2007 Kentucky Derby</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  align="center" cellpadding="0" cellspacing="0" >
              <form action="" method="post" enctype="multipart/form-data" name="breeders_form" onSubmit="return chk_breeders('{$horse_id}');">
                <tr>
                <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
				  <!--------------------->
				  <tr>
				  <td colspan="4">
				  <table><tr>
            <td  class="fieldname" align="center" valign="top" >
             {if $horse_pic neq NULL}{if $img_chk eq 1}
			 <img src="{$horse_pic}" border="1">
			 {else}
			 <img src="../uploaded_images/horse_images/thumb{$horse_pic}" border="1">{/if}{/if}            </td>
            <td   align="center" valign="top">
              {if $horse_silkpic neq NULL}
			  {if $silkimg_chk eq 1}
			 <img src="{$horse_silkpic}" border="1">
			 {else}
			  <img src="../uploaded_images/horse_images/thumb{$horse_silkpic}" border="1">{/if}{/if}            </td>
			</tr>
			</table></td>
          </tr>
				  
				  <!---------------------->
                      
                      <tr>
            <td width='150' class="fieldname">
              Name            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="horse_name" value="{$horse_name}" id="affiliateFirstName">            </td>
          </tr>
		   <tr>
            <td class="fieldname">
              Odds            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="horse_odds" value="{$horse_odds}" id="affiliateFirstName">             </td>
          </tr><tr>
            <td width='150' class="fieldname">
              Jockey            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="horse_jockey" value="{$horse_jockey}" id="affiliateFirstName"> <!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{if $horse_id neq NULL}<a href="trainer_view.php?trainer={$horse_trainer}" class="link">See the trainer</a>{/if}-->            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Post            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="horse_post" value="{$horse_post}" id="affiliateFirstName"> <!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{if $horse_id neq NULL}<a href="owner_view.php?owner={$horse_owner}" class="link">See the owner</a>{/if}-->            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              result            </td>
            <td width="244" class="columnHeaderStatic">
              <input type="text" name="horse_result" value="{$horse_result}" id="affiliateFirstName">            </td>

            <td width="95" class="columnHeaderStatic"><span class="fieldname">result time</span></td>
            <td width="202" class="columnHeaderStatic"><input type="text" name="horse_resulttime" value="{$horse_resulttime}" id="affiliateFirstName" /></td>
          </tr>
		  <!-- <tr>
            <td width='150' class="fieldname">
              Stakes            </td>
            <td colspan="3" class="columnHeaderStatic">
              <input type="text" name="horse_stakes" value="{$horse_stakes}" id="affiliateFirstName">            </td>

          </tr> -->
		  <tr>
                        <td colspan="5" class="fieldname"><div align="center">
						Or Import all records from .CSV file
                        </div></td>
                      </tr><tr>
                        <td colspan="4" class="fieldname">
						<div align="center" style="padding-right:100px ">
						<input type="file" name="import_breeders">
                        </div></td>
                      </tr>
                      <tr>
                        <td colspan="5" class="fieldname"><div align="center">
						<input type="hidden" name="horse_id" value="{$horse_id}">
						 <input type="hidden" name="horse_pic" value="{$horse_pic}">
						 <input type="hidden" name="horse_silkpic" value="{$horse_silkpic}">
						 <input type="hidden" name="start" value="{$start}" />
						  <input type="hidden" name="group_stakes" value="{$group_stakes}" />
						   <input type="hidden" name="act_BreedersCup" value="{$act_BreedersCup}" />
						   <input type="hidden" name="search_BreedersCup" value="{$search_BreedersCup}" />
                           {if $horse_id eq NULL} <input type="image" name="Submit" src="images/submit-button.gif"  style="width:62px;">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;">{/if}

                        </div></td>
                      </tr>
					  
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td></tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

