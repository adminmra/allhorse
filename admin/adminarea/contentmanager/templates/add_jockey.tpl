{include file="header.tpl"}
{include file="left.tpl"}
<!-------------------- Body Section ------------------------>
{literal}
<script language="javascript" src="js/validation.js" type="text/javascript"></script>
<style type="text/css">

</style>
{/literal}
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"><br>
      <table width="90%" style="border: 1px solid #0D4686 ;
 border-collapse: collapse;  " align="center" cellpadding="0" cellspacing="0" bgcolor="#B5DE97" class="border">
        <tr> 
          <td width="50%" height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">{if $jockey_id eq NULL}ADD{else}
		  EDIT{/if} JOCKEY</font></strong></td>
          <td align="right" bgcolor="#0D4686" style="padding-right:10px;">
		  <a href="javascript:history.back();" style="text-decoration:none;"><strong><font color="#FFFFFF">&laquo; Back</font></strong></a>&nbsp;
		  <a href="jockey_view.php"><strong><font color="#FFFFFF">&laquo; Back to jockey</font></strong></a>
		  </td>
        </tr>
        <tr> 
          <td height="10" colspan="2"> </td>
        </tr>
        <tr> 
          <td height="25" colspan="2" align="center">
              <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
              <form action="" method="post" enctype="multipart/form-data" name="jockeys_form" onSubmit="return chk_jockey('{$jockey_id}');">
                <tr>
                  <td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%"  border="0" cellspacing="2" cellpadding="2">
                     
				  <!--------------------->
				  <tr>
				  <td colspan="2">
				  <table><tr>
            <td  align="left" >
             {if $jockey_pic neq NULL}
			 {if $img_chk eq 1}
			 <img src="{$jockey_pic}" border="1">
			 {else}
			 <img src="uploaded_images/jockey_images/thumb{$jockey_pic}" border="1">{/if}{/if}
            </td>
            
			</tr>
			</table>
</td>
          </tr>
				  
				  <!---------------------->
                  
                      <tr>
            <td width='150' class="fieldname">
              Name
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="jockey_name" value="{$jockey_name}" id="affiliateFirstName">
            </td>

          </tr>
		   <tr>
            <td class="fieldname">
              Country
            </td>
            <td class="columnHeaderStatic">

			<select name="jockey_country" >
			{html_options values=$country_name output=$country_name selected=$jockey_country}
			</select>
            </td>
          </tr><tr>
            <td width='150' class="fieldname">
              State
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="jockey_state" value="{$jockey_state}" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              City
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="jockey_city" value="{$jockey_city}" id="affiliateFirstName">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Url
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="jockey_url" value="{$jockey_url}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Birth Date
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="jockey_birthdate" value="{$jockey_birthdate}" id="affiliateFirstName">
            </td>
          </tr>
		   <tr>
            <td width='150' class="fieldname">
              Birth Country
            </td>
            <td class="columnHeaderStatic">
              <select name="jockey_birth_country" >
              {html_options values=$country_name output=$country_name selected=$jockey_birthcountry}
              </select>

            </td>

          </tr> 
		  
		  <tr>
            <td width='150' class="fieldname">
              Birth State
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="jockey_birth_state" value="{$jockey_birthstate}" id="">
            </td>

          </tr> <tr>
            <td width='150' class="fieldname">
              Birth City
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="jockey_birth_city" value="{$jockey_birthcity}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              Height
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="jockey_height" value="{$jockey_height}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname">
              weight
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="jockey_weight" value="{$jockey_weight}" id="affiliateFirstName">
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname"  valign="top">
              Family
            </td>
            <td class="columnHeaderStatic">
              <textarea name="jockey_family"  id="affiliateFirstName" rows="4">{$jockey_family}</textarea>
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname" valign="top">
             AHR Notes
            </td>
            <td class="columnHeaderStatic">
              <textarea name="jockey_notes"  id="affiliateFirstName" rows="4">{$jockey_notes}</textarea>
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname" valign="top">
             GHB Notes
            </td>
            <td class="columnHeaderStatic">
              <textarea name="jockey_notes_GHB"  id="affiliateFirstName" rows="4">{$jockey_notes_GHB}</textarea>
            </td>

          </tr>
		  <tr>
            <td width='150' class="fieldname" valign="top">
             HB Notes
            </td>
            <td class="columnHeaderStatic">
              <textarea name="jockey_notes_HB"  id="affiliateFirstName" rows="4">{$jockey_notes_HB}</textarea>
            </td>

          </tr>
		   <tr>
            <td width='150' class="fieldname">
              Image
            </td>
            <td class="columnHeaderStatic">
              <input type="text" name="jockey_image" value="{$jockey_pic}" id="affiliateFirstName">
            </td>

          </tr>
                      <tr>
                        <td colspan="2" class="fieldname" align="left"><div align="center">
						<input type="hidden" name="jockey_id" value="{$jockey_id}">
						 <input type="hidden" name="jockey_pic" value="{$jockey_pic}">
						 <input type="hidden" name="start" value="{$start}" />
                           {if $jockey_id eq NULL} <input type="image" name="Submit" src="images/submit-button.gif"  style="width:62px; ">
						   {else}<input name="submit21" type="image" src="images/edit-but.gif" style="width:62px;">{/if}

                        </div></td>
                      </tr>
					  <tr>
                        <td colspan="3" class="fieldname"><div align="center">
						Or Import all records from .CSV file
                        </div></td>
                      </tr><tr>
                        <td colspan="3" class="fieldname"><div align="center" style="padding-right:100px ">
						<input type="file" name="import_jockey">
                        </div></td>
                      </tr>
                  </table></td>
                </tr>
                <tr>
                  <td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
                </tr>
              </form>
            </table>
            </td>
        </tr>
      </table>
      <br>
    </td>
  </tr>
  <tr> 
    <td height="6" bgcolor="#FFFFFF"> </td>
  </tr>
</table> 
<!------------------ End of Body Section ------------------->
{include file="footer.tpl"}

