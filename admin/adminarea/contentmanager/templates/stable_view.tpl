{include file="header.tpl"}
{include file="left.tpl"}
{literal}
<style type="text/css">

</style>
{/literal}
<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><? //include "patient_manage_top.php"; ?></td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <td height="30" align="right" bgcolor="#FFFFFF" style="padding-right:10px;"><strong class="bodyb">{$paging}</strong></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#F6F9FE" class="border">
      <tr>
        <td height="25" bgcolor="#0D4686" style="padding-left:10px;"><strong><font color="#FFFFFF">STABLES</font></strong></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
      <tr>
        <td height="25"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#89ADD7">
          <tr>
            <td height="22"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="1">
              <tr align="center">
                <td height="22" bgcolor="#E4EBF6" width="10%"><a href="add_stable.php" class="one">Add</a></td>
           <td width="75%" bgcolor="#FFFFFF">&nbsp;</td><td width="15%" height="22" bgcolor="#E4EBF6" align="center"><a href="import_stable_records.php"><img src="images/import.gif" border="0"></a></td>    
                
              </tr>
			  <!------------------------------->
			  <tr align="center">
			  <td colspan="2" width="100%">
			  <table width="100%" bgcolor="#FFFFFF">
			  <tr>
			  <form name="stable_search" method="post" action="?act_stable=search">
                <td height="22" width="35%" bgcolor="#FFFFFF" colspan="1"> Search by Name : <input type="text" name="search_stable" style="width:100px; " value="{$search_value}" ></td>
			   
			   <td height="22" width="65%" bgcolor="#FFFFFF" colspan="4" align="left"><input type="image" src="images/botton-seeresults.gif" style="width:85px; height:18px;" />&nbsp; <strong style="padding-bottom:5px;">Total records : {$record_count}</strong>
				                                            </td>
                  
				 </form>          
              </tr>
			  </table>
			  </td>
			  </tr>
			  <!------------------------------->
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="25" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr align="center">
            <td height="40" rowspan="2" bgcolor="#0D4686"><a href="?sort_by=name" class="seven"><font style="color:#FFFFFF;"><b>stable name</b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2"><a href="?sort_by=country" class="seven"><font style="color:#FFFFFF;"><b>Country
               </b></font></a></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><a href="?sort_by=state" class="seven"><font style="color:#FFFFFF;"><b>State</b></font></a></td>
			<td height="20" bgcolor="#0D4686" rowspan="2" class="header"><a href="?sort_by=url" class="seven"><font style="color:#FFFFFF;"><b>Url</b></font></a></td>
			<td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>email</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Stable fact</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Stable Picture</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" class="header"><font style="color:#FFFFFF;"><b>Stable Video</b></font></td>
            <td height="20" bgcolor="#0D4686" rowspan="2" colspan="2" class="header"><font style="color:#FFFFFF;"><b>Action</b></font></td>
           <!-- <td height="40" rowspan="2" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">Action</font></td>-->
		  </tr>
          <tr align="center">
           <!-- <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">&nbsp;</font></td>
            <td height="20" bgcolor="#0D4686" class="bodyb">&nbsp;</td>-->
            </tr>
			{assign var="indexvalue" value=0}
					  {section name=stable loop=$stable_id}
          <tr align="center">
            <td height="18" bgcolor="#3D73B8" align="left"><a href="add_stable.php?stable_id={$stable_id[stable]}&start={$start}" class="seven2">
             {$stable_name[stable]}</a></td>
            <td height="18" bgcolor="#B5DE97" align="left">{$stable_country[stable]}</td>
            <td height="18" bgcolor="#B5DE97" align="left">{$stable_state[stable]}</td>
			<td height="18" bgcolor="#B5DE97" align="left"><a href="{$stable_url[stable]}" class="seven" target="_blank">{$stable_url[stable]}</a></td>
			<td height="18" bgcolor="#B5DE97"><a href="mailto:{$stable_email[stable]}" class="seven">{$stable_email[stable]}</a></td>
            <td bgcolor="#B5DE97"><a href="stable_fact.php?stable_id={$stable_id[stable]}" class="seven">Facts</a></td>
            <td bgcolor="#B5DE97"><a href="stable_pics.php?stable_id={$stable_id[stable]}" class="seven">Pictures</a></td>
            <td bgcolor="#B5DE97"><a href="stable_videos.php?stable_id={$stable_id[stable]}" class="seven">Videos</a></td>
            <td bgcolor="#B5DE97"><a href="add_stable.php?stable_id={$stable_id[stable]}&start={$start}" class="seven">[Edit]</a></td>
            <td bgcolor="#B5DE97"><a href="?act=delete&stable_id={$stable_id[stable]}" onClick="return confirm('Are you sure to delete?')" class="seven">[Delete]</a><!--<input type="checkbox" name="checkbox" value="checkbox" style="border-color:#B5DE97;" />--></td>
          </tr>
		 {assign var="indexvalue" value=$indexvalue+1}
		 {sectionelse}
		  <tr align="center">
            <td height="30" bgcolor="#3D73B8" colspan="10"><font style="color:#FFFFFF;">No data available</td>
			</tr>
                    {/section}
        </table></td>
      </tr>
      <tr>
        <td height="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
{include file="footer.php"}