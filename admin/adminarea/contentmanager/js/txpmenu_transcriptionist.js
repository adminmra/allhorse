var tsaveState = 1;

var tpressedFontColor = "#AA0000";

var tpathPrefix_img = "js/img/";

var tlevelDX = 10;
var ttoggleMode = 1;

var texpanded = 0;
var tcloseExpanded   = 0;
var tcloseExpandedXP = 0;

var tblankImage      = "js/img/blank.gif";
var tmenuWidth       = 200;
var tmenuHeight      = 0;

var tabsolute        = 1;
var tleft            = 5;
var ttop             = 50;

var tfloatable       = 0;
var tfloatIterations = 10;

var tmoveable        = 0;
var tmoveImage       = "js/img/movepic.gif";
var tmoveImageHeight = 12;

var tfontStyle       = "normal 8pt Tahoma";
var tfontColor       = ["#215DC6","#428EFF"];
var tfontDecoration  = ["none","underline"];

var titemBackColor   = ["#D6DFF7","#D6DFF7"];
var titemAlign       = "left";
var titemBackImage   = ["",""];
var titemCursor      = "pointer";
var titemHeight      = 22;
var titemTarget      = "";

var ticonWidth       = 21;
var ticonHeight      = 15;
var ticonAlign       = "left";

var tmenuBackImage   = "";
var tmenuBackColor   = "";
var tmenuBorderColor = "#FFFFFF";
var tmenuBorderStyle = "solid";
var tmenuBorderWidth = 0;

var texpandBtn       =["expandbtn2.gif","expandbtn2.gif","collapsebtn2.gif"];
var texpandBtnW      = 9;
var texpandBtnH      = 9;
var texpandBtnAlign  = "left"

var tpoints       = 0;
var tpointsImage  = "";
var tpointsVImage = "";
var tpointsCImage = "";

// XP-Style Parameters
var tXPStyle = 1;
var tXPIterations = 10;                  // expand/collapse speed
var tXPTitleBackColor    = "#265BCC";
var tXPExpandBtn    = ["xpexpand1.gif","xpexpand2.gif","xpcollapse1.gif","xpcollapse2.gif"];
var tXPTitleBackImg = "xptitle.gif";

var tXPTitleLeft      = "xptitleleft.gif";
var tXPTitleLeftWidth = 4;

var tXPBtnWidth  = 25;
var tXPBtnHeight = 25;

var tXPIconWidth  = 31;
var tXPIconHeight = 32;

var tXPFilter=1;

var tXPBorderWidth = 1;
var tXPBorderColor = '#FFFFFF';



var tstyles =
[
    ["tfontStyle=bold 8pt Tahoma","titemBackColor=#265BCC,#265BCC","tfontColor=#FFFFFF,#428EFF", "tfontDecoration=none,none"],
    ["tfontStyle=bold 8pt Tahoma","titemBackColor=#265BCC,#265BCC","tfontColor=#215DC6,#428EFF", "tfontDecoration=none,none"],
    ["tfontDecoration=none,none"],
    ["tfontStyle=bold 8pt Tahoma","tfontColor=#444444,#5555FF"],
];

var tXPStyles =
[
    ["tXPTitleBackColor=#D0DAF8", "tXPExpandBtn=xpexpand3.gif,xpexpand4.gif,xpcollapse3.gif,xpcollapse4.gif", "tXPTitleBackImg=xptitle2.gif"]
];

var tmenuItems =
[
    ["+Transcriptionist", "", "xpicon1.gif","","", "Transcriptionist","","0"],
		["|Change Login Info", "changelogininfo.php", "icon1.gif", "icon1o.gif", "", "Change Login Info"],
		["|Logout", "logout.php", "icon1.gif", "icon1o.gif", "", "Logout"],
		
	["Manage Notes", "", "","","", "Manage Notes",,"1","0"],
		["|Manage Transcription Notes", "transcription_notes.php", "icon1.gif", "icon1o.gif", "", "Manage Transcription Notes"],
	
	["Internal Messaging", "", "","","", "Internal Messaging",,"1","0"],
		["|Create Message", "compose.php", "icon1.gif", "icon1o.gif", "", "Create Message"],
		["|Inbox", "inbox.php", "icon1.gif", "icon1o.gif", "", "Inbox"],
		["|Outbox", "outbox.php", "icon1.gif", "icon1o.gif", "", "Outbox"],
	/*["Account Management", "", "","","Account Management", "",,"1","0"],
		["|Physicians", "", "icon1.gif", "icon1o.gif", "", "Manage Physicians"],
			["||Add", "physician_add.php", "icon2.gif", "icon2o.gif", "", "Add"],
			["||Edit", "physician_edit.php", "icon2.gif", "icon2o.gif", "", "Edit"],
			["||Delete", "physician_delete.php", "icon2.gif", "icon2o.gif", "", "Delete"],
			["||Activate/Deactivate", "physician_status.php", "icon2.gif", "icon2o.gif", "", "Activate/Deactivate"],
		["|Nurses", "", "icon1.gif", "icon1o.gif", "", "Manage Nurses"],
			["||Add", "nurse_add.php", "icon2.gif", "icon2o.gif", "", "Add"],
			["||Edit", "nurse_edit.php", "icon2.gif", "icon2o.gif", "", "Edit"],
			["||Delete", "nurse_delete.php", "icon2.gif", "icon2o.gif", "", "Delete"],
			["||Activate/Deactivate", "nurse_status.php", "icon2.gif", "icon2o.gif", "", "Activate/Deactivate"],
		["|Transcriptionist", "", "icon1.gif", "icon1o.gif", "", "Manage Transcriptionist"],
			["||Add", "transcriptionist_add.php", "icon2.gif", "icon2o.gif", "", "Add"],
			["||Edit", "transcriptionist_edit.php", "icon2.gif", "icon2o.gif", "", "Edit"],
			["||Delete", "transcriptionist_delete.php", "icon2.gif", "icon2o.gif", "", "Delete"],
			["||Activate/Deactivate", "transcriptionist_status.php", "icon2.gif", "icon2o.gif", "", "Activate/Deactivate"],
			["||Assign Physician", "transcriptionist_assignphysician.php", "icon2.gif", "icon2o.gif", "", "Assign Physician"],
		["|Receptionist", "", "icon1.gif", "icon1o.gif", "", "Manage Receptionist"],
			["||Add", "receptionaist_add.php", "icon2.gif", "icon2o.gif", "", "Add"],
			["||Edit", "receptionaist_edit.php", "icon2.gif", "icon2o.gif", "", "Edit"],
			["||Delete", "receptionaist_delete.php", "icon2.gif", "icon2o.gif", "", "Delete"],
			["||Activate/Deactivate", "receptionaist_status.php", "icon2.gif", "icon2o.gif", "", "Activate/Deactivate"],
            
    ["Diagnosis Codes", "", "","","Manage Diagnosis Codes", "",,"1","0"],
		["|Add", "diagnosiscodes_add.php", "icon1.gif", "icon1o.gif", "", "Add"],
		["|Edit", "diagnosiscodes_edit.php", "icon1.gif", "icon1o.gif", "", "Edit"],
		["|Delete", "diagnosiscodes_delete.php", "icon1.gif", "icon1o.gif", "", "Delete"],
		["|Activate/Deactivate", "diagnosiscodes_status.php", "icon1.gif", "icon1o.gif", "", "Activate/Deactivate"],
		
	["Patients Appointments", "", "","","Manage Patients Appointments", "",,"1","0"],
		["|Add", "patientsappointments_add.php", "icon1.gif", "icon1o.gif", "", "Add"],
		["|Edit", "patientsappointments_edit.php", "icon1.gif", "icon1o.gif", "", "Edit"],
		["|Delete", "patientsappointments_delete.php", "icon1.gif", "icon1o.gif", "", "Delete"],
		
    ["Insurance Module", "", "","","Manage Insurance Module", "",,"1","0"],
		["|Add", "insurancemodule_add.php", "icon1.gif", "icon1o.gif", "", "Add"],
		["|Edit", "insurancemodule_edit.php", "icon1.gif", "icon1o.gif", "", "Edit"],
		["|Delete", "insurancemodule_delete.php", "icon1.gif", "icon1o.gif", "", "Delete"],
		["|Activate/Deactivate", "insurancemodule_status.php", "icon1.gif", "icon1o.gif", "", "Activate/Deactivate"],
		
	["Patients History Category", "", "","","Patients History Category", "",,"1","0"],
		["|Add/Edit", "patientshistorycategory_manage.php", "icon1.gif", "icon1o.gif", "", "Add/Edit"],
		["|Billing Patient", "patientshistorycategory_billingpatient.php", "icon1.gif", "icon1o.gif", "", "Billing Patient"],
		["|Payment Received", "patientshistorycategory_paymentreceived.php", "icon1.gif", "icon1o.gif", "", "Payment Received"],
		
	["Reports", "", "","","Reports", "",,"1","0"],
		["|Patient Billing", "reports_patientbilling.php", "icon1.gif", "icon1o.gif", "", "Patient Billing"],
		["|Patient Transaction", "reports_patienttransaction.php", "icon1.gif", "icon1o.gif", "", "Patient Transaction"],
		
    ["Appointments", "", "","","Appointments", "",,"1","0"],
		["|Approve/Disapprove", "appointments_status.php", "icon1.gif", "icon1o.gif", "", "Approve/Disapprove"],

    ["Complexity Level", "", "","","Complexity Level", "",,"1","0"],
		["|Assign diagnosis codes based on it", "complexitylevel_assigndiagnosiscodes.php", "icon1.gif", "icon1o.gif", "", "Assign diagnosis codes based on it"],
		
	["FAQ", "", "","","FAQ", "",,"1","0"],
		["|Approve Question", "faq_approvequestion.php", "icon1.gif", "icon1o.gif", "", "Approve Question"],
		["|Post Question", "faq_postquestion.php", "icon1.gif", "icon1o.gif", "", "Post Question"],
		["|Post Answer", "faq_postanswer.php", "icon1.gif", "icon1o.gif", "", "Post Answer"],
	
	["Complaints", "", "","","Complaints", "",,"1","0"],
		["|Manage Complaints", "complaints_manage.php", "icon1.gif", "icon1o.gif", "", "Manage Complaints"],*/

];

apy_tmenuInit();
