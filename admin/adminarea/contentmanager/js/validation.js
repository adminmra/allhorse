function dropdown(drop,lmt)

{
var d = drop.options[drop.selectedIndex].value;


        if(d=="send_all")
		{
			send_all(document.frm1,lmt);
			}
		else if(d=="send_all2")
		{
			send_all2(document.frm1,lmt);
			}
		else if(d=="send_all_admin")
		{
			
		send_all_admin(document.frm1,lmt);
				
		}	
		else if(d=="send_all_employer")
		{
			send_all_employer(document.frm1,lmt);
		}
		else if(d=="compose-all-artist")
		{
			send_all_artist(document.frm1,lmt);
		}
		else if(d=="delete"){
		
				del_selected(document.frm1,lmt);
				
			}else if(d=="send_newsletter"){
			
				send_newsletter(document.frm1,lmt);
				
			}else if(d=="activate"){
			
				activate_all(document.frm1,lmt);
				
			}else if(d=="deactivate"){
			
				deactivate_all(document.frm1,lmt);
				
			}else if(d=="featured"){
			
				featured_all(document.frm1,lmt);
				
			}else if(d=="reset"){
			
				reset_all(document.frm1,lmt);
				
			}
			else if(d=="send-newsletter-artist"){
			
				send_newsletter_artist(document.frm1,lmt);
				
			}
			else if(d=="approve-all"){
			
				approve_all(document.frm1,lmt);
				
			}
			
			else{
				window.top.location.href = d;
				drop.selectedindex = 0;
		}
}
//-->
function dropdown1(drop,val)

{
   var d = drop.options[drop.selectedIndex].value;
	if(d=="delete"){
		var alt=confirm("This will delete the selected elements and related elements from other tables in database.\nAre you sure to Delete?");
		if(alt)
             {
			  document.frm1.action="?act=del&id="+val;
              document.frm1.submit();
             }
	}else{
			window.top.location.href = d;
			drop.selectedindex = 0;
	}
}

function drop(dr)
{
	var d = dr.options[dr.selectedIndex].value;
	window.top.location.href = d;
	dr.selectedindex = 0;
}
function drop_adm(dr)
{ 
   	var d = dr.options[dr.selectedIndex].value;
	window.location.href = d;
	dr.selectedindex = 0;
}
// ADMIN HOME 
function drop_city(dr)
{ 
    document.frm1.action="?act=search";	
	document.frm1.submit();
}
/////////////////////////////
function chk(){                                // Add/Edit country

  if(document.f1.country.value==""){
     alert('Please enter Country');
	 document.f1.country.focus();
	 return false;
  }
  return true;
}
function chk_city(){                           // Add/Edit city
if(document.frm.country.value==""){
	alert('Please Select Country');
	 document.frm.country.focus();
	 return false;
}
  if(document.frm.city.value==""){
     alert('Please enter City');
	 document.frm.city.focus();
	 return false;
  }
  return true;
}
//-------------------------- For select/deselect function
function del_selected(frm,i)
       {
             var flag=0;
			 var strng = "";
			 for(j=0;j<i;j++)
             {
                if(document.getElementById('checkbox'+j).checked)
                {
                    flag=1;
					if(j == 0){
					 strng=document.getElementById('checkbox'+j).value;
					 
					 }else{
					  strng=strng+","+document.getElementById('checkbox'+j).value;
					  
					 }
                }
             }
			
             if(flag==0)
             {
                  alert("Please Check the item !!!");
             }
             else
             {
             var d=confirm("This will delete the selected elements and related elements from other tables in database.\nAre you sure to Delete?");
				 if(d)
				 {
					  
					  frm.action="?del=all&str="+strng;
					  frm.submit();
				 }
             }
       }

function selectAll(i,frm)
       {
           for(j=0;j<i;j++)
           {
              document.getElementById('checkbox'+j).checked =!(document.getElementById('checkbox'+j).checked);
           }
		    return false;
			
		}
 
 
//FOR HORSE SECTION
function chk_horse(val){                                
 var con=document.horses_form.horse_image.value;
 //alert(document.horses_form.image_file.height);
 if(document.horses_form.import_horse.value=="")
 {
  if(document.horses_form.horse_name.value==""){
     alert('Please Select name');
	 document.horses_form.horse_name.focus();
	 return false;
  }
  if(document.horses_form.horse_status.value==""){
     alert('Please Select horse status');
	 document.horses_form.horse_status.focus();
	 return false;
  }
  if(document.horses_form.horse_trainer.value==""){
     alert('Please Select horse trainer');
	 document.horses_form.horse_trainer.focus();
	 return false;
  }
  if(document.horses_form.horse_owner.value==""){
     alert('Please Select horse owner');
	 document.horses_form.horse_owner.focus();
	 return false;
  }

  	/*if(val==""){
		  if(document.horses_form.horse_image.value==""){
			 alert('Please upload image');
			 document.horses_form.horse_image.focus();
			 return false;
		  }
			  
    }*/
}
	if(con!="")
		  {
			var con1=con.charAt(con.length-3)+con.charAt(con.length-2)+con.charAt(con.length-1);  
			if(con1!='gif' && con1!='GIF' && con1!='jpg' && con1!='JPG' && con1!='png' && con1!='PNG')
					{  
						alert("Invalid Image File. Your file type must be jpeg or gif or png");
						document.horses_form.horse_image.focus();
						return false;
					  
		}
		  }
    if(val!=""){
		document.horses_form.action="?act=edit";
		document.horses_form.submit();
	  }else{
		 document.horses_form.action="?act=add";
		document.horses_form.submit();
	  }
  return true;
}
///////////for horsefacts\\\\\\\\\\\
function chk_horse_fact(val){                                
 if(document.horse_fact_add.import_horse_fact.value=="")
 {
  if(document.horse_fact_add.horse_fact.value==""){
     alert('Please Select fact');
	 document.horse_fact_add.horse_fact.focus();
	 return false;
  }
 }
    if(val!=""){
		document.horse_fact_add.action="?act=edit";
		document.horse_fact_add.submit();
	  }else{
		 document.horse_fact_add.action="?act=add";
		document.horse_fact_add.submit();
	  }
  return true;
}
///////////for horsefacts\\\\\\\\\\\
function chk_trainer_fact(val){                                
 if(document.trainer_fact_add.import_trainer_fact.value=="")
 {
  if(document.trainer_fact_add.trainer_fact.value==""){
     alert('Please Select fact');
	 document.trainer_fact_add.trainer_fact.focus();
	 return false;
  }
 }
    if(val!=""){
		document.trainer_fact_add.action="?act=edit";
		document.trainer_fact_add.submit();
	  }else{
		 document.trainer_fact_add.action="?act=add";
		document.trainer_fact_add.submit();
	  }
  return true;
}
///////////for ownerfacts\\\\\\\\\\\
function chk_owner_fact(val){                                
 if(document.owner_fact_add.import_owner_fact.value=="")
 {
  if(document.owner_fact_add.owner_fact.value==""){
     alert('Please Select fact');
	 document.owner_fact_add.owner_fact.focus();
	 return false;
  }
 }
    if(val!=""){
		document.owner_fact_add.action="?act=edit";
		document.owner_fact_add.submit();
	  }else{
		 document.owner_fact_add.action="?act=add";
		document.owner_fact_add.submit();
	  }
  return true;
}
///////////for stablefacts\\\\\\\\\\\
function chk_stable_fact(val){                                
 if(document.stable_fact_add.import_stable_fact.value=="")
 {
  if(document.stable_fact_add.stable_fact.value==""){
     alert('Please Select fact');
	 document.stable_fact_add.stable_fact.focus();
	 return false;
  }
 }
    if(val!=""){
		document.stable_fact_add.action="?act=edit";
		document.stable_fact_add.submit();
	  }else{
		 document.stable_fact_add.action="?act=add";
		document.stable_fact_add.submit();
	  }
  return true;
}
///////////for jockeyfacts\\\\\\\\\\\
function chk_jockey_fact(val){                                
 if(document.jockey_fact_add.import_jockey_fact.value=="")
 {
  if(document.jockey_fact_add.jockey_fact.value==""){
     alert('Please Select fact');
	 document.jockey_fact_add.jockey_fact.focus();
	 return false;
  }
 }
    if(val!=""){
		document.jockey_fact_add.action="?act=edit";
		document.jockey_fact_add.submit();
	  }else{
		 document.jockey_fact_add.action="?act=add";
		document.jockey_fact_add.submit();
	  }
  return true;
}
//////////FOR HORSE PICS
function chk_horse_pic(val){ 
if(document.horse_pics_add.import_horse_pic.value=="")
{
 var con=document.horse_pics_add.horse_image.value;
  if(val==""){
		  if(document.horse_pics_add.horse_image.value=="" && document.horse_pics_add.horse_url.value==""){
			 alert('Please upload image or insert some url');
			 document.horse_pics_add.horse_image.focus();
			 return false;
		  }
			  
    }
  if(con!="")
		  {
			var con1=con.charAt(con.length-3)+con.charAt(con.length-2)+con.charAt(con.length-1);  
			if(con1!='gif' && con1!='GIF' && con1!='jpg' && con1!='JPG' && con1!='png' && con1!='PNG')
					{  
						alert("Invalid Image File. Your file type must be jpeg or gif or png ");
						document.horse_pics_add.horse_image.focus();
						return false;
					  
		}
		  }
}
    if(val!=""){
		document.horse_pics_add.action="?act=edit";
		document.horse_pics_add.submit();
	  }else{
		 document.horse_pics_add.action="?act=add";
		document.horse_pics_add.submit();
	  }
  return true;
}
function chk_horse_vid(val){   
if(document.horse_vid_add.import_horse_vid.value=="")
{
	 var con=document.horse_vid_add.horse_video.value;
	  if(val==""){
			  if(document.horse_vid_add.horse_video.value=="" && document.horse_vid_add.horse_url.value==""){
				 alert('Please upload video or insert some url');
				 document.horse_vid_add.horse_video.focus();
				 return false;
			  }
				  
		}
}
    if(val!=""){
		document.horse_vid_add.action="?act=edit";
		document.horse_vid_add.submit();
	  }else{
		 document.horse_vid_add.action="?act=add";
		document.horse_vid_add.submit();
	  }
  return true;
}
function chk_horse_statsanual(val){                                
 
  if(document.horse_statsanual_add.import_horse_annualstat.value=="" )
  {
		  if(document.horse_statsanual_add.earning_stat.value=="" ){
			 alert('Please specify earning.');
			 document.horse_statsanual_add.earning_stat.focus();
			 return false;
		  }
			if(document.horse_statsanual_add.year_stat.value=="" ){
			 alert('Please specify year');
			 document.horse_statsanual_add.year_stat.focus();
			 return false;
		  }
		  if(document.horse_statsanual_add.starts_stat.value=="" ){
			 alert('Please specify starts');
			 document.horse_statsanual_add.starts_stat.focus();
			 return false;
		  }
		  if(document.horse_statsanual_add.w_stat.value=="" ){
			 alert('Please specify win percentage');
			 document.horse_statsanual_add.w_stat.focus();
			 return false;
		  }
		  if(document.horse_statsanual_add.win_stat.value=="" ){
			 alert('Please specify wins');
			 document.horse_statsanual_add.win_stat.focus();
			 return false;
		  }
		  if(document.horse_statsanual_add.places_stat.value=="" ){
			 alert('Please specify places');
			 document.horse_statsanual_add.places_stat.focus();
			 return false;
		  }
		  if(document.horse_statsanual_add.show_stat.value=="" ){
			 alert('Please specify earning');
			 document.horse_statsanual_add.show_stat.focus();
			 return false;
		  }
  }
    if(val!=""){
		document.horse_statsanual_add.action="?act=edit";
		document.horse_statsanual_add.submit();
	  }else{
		 document.horse_statsanual_add.action="?act=add";
		document.horse_statsanual_add.submit();
	  }
  return true;
}
//////////horse lifetime statistics\\\\\\\

function chk_horse_statslifetime(val)                               
{  
  if(document.horse_statslifetime_add.import_horse_lifetimestat.value=="" )
  {
		  if(document.horse_statslifetime_add.earning_stat.value=="" ){
			 alert('Please specify earning.');
			 document.horse_statslifetime_add.earning_stat.focus();
			 return false;
		  }
			/*if(document.horse_statslifetime_add.year_stat.value=="" ){
			 alert('Please specify year');
			 document.horse_statslifetime_add.year_stat.focus();
			 return false;
		  }*/
		  if(document.horse_statslifetime_add.starts_stat.value=="" ){
			 alert('Please specify starts');
			 document.horse_statslifetime_add.starts_stat.focus();
			 return false;
		  }
		  if(document.horse_statslifetime_add.w_stat.value=="" ){
			 alert('Please specify win percentage');
			 document.horse_statslifetime_add.w_stat.focus();
			 return false;
		  }
		  if(document.horse_statslifetime_add.win_stat.value=="" ){
			 alert('Please specify wins');
			 document.horse_statslifetime_add.win_stat.focus();
			 return false;
		  }
		  if(document.horse_statslifetime_add.places_stat.value=="" ){
			 alert('Please specify places');
			 document.horse_statslifetime_add.places_stat.focus();
			 return false;
		  }
		  if(document.horse_statslifetime_add.show_stat.value=="" ){
			 alert('Please specify shows');
			 document.horse_statslifetime_add.show_stat.focus();
			 return false;
		  }
  }
    if(val!=""){
		document.horse_statslifetime_add.action="?act=edit";
		document.horse_statslifetime_add.submit();
	  }else{
		 document.horse_statslifetime_add.action="?act=add";
		document.horse_statslifetime_add.submit();
	  }
  return true;
}

//////////trainer anual statistics \\\\\\\

function chk_trainer_statsanual(val){                                
 
  if(document.trainer_statsanual_add.import_trainer_annualstat.value=="" )
  {
		  if(document.trainer_statsanual_add.earning_stat.value=="" ){
			 alert('Please specify earning.');
			 document.trainer_statsanual_add.earning_stat.focus();
			 return false;
		  }
			if(document.trainer_statsanual_add.year_stat.value=="" ){
			 alert('Please specify year');
			 document.trainer_statsanual_add.year_stat.focus();
			 return false;
		  }
		  if(document.trainer_statsanual_add.races_stat.value=="" ){
			 alert('Please specify starts');
			 document.trainer_statsanual_add.races_stat.focus();
			 return false;
		  }
		  if(document.trainer_statsanual_add.w_stat.value=="" ){
			 alert('Please specify win percentage');
			 document.trainer_statsanual_add.w_stat.focus();
			 return false;
		  }
		  if(document.trainer_statsanual_add.win_stat.value=="" ){
			 alert('Please specify wins');
			 document.trainer_statsanual_add.win_stat.focus();
			 return false;
		  }
		  if(document.trainer_statsanual_add.places_stat.value=="" ){
			 alert('Please specify places');
			 document.trainer_statsanual_add.places_stat.focus();
			 return false;
		  }
		  if(document.trainer_statsanual_add.show_stat.value=="" ){
			 alert('Please specify shows');
			 document.trainer_statsanual_add.show_stat.focus();
			 return false;
		  }
  }
    if(val!=""){
		document.trainer_statsanual_add.action="?act=edit";
		document.trainer_statsanual_add.submit();
	  }else{
		 document.trainer_statsanual_add.action="?act=add";
		document.trainer_statsanual_add.submit();
	  }
  return true;
}
////////////////trainer lifetime stat\\\\\\\\

function chk_trainer_statslifetime(val){                                
 
  if(document.trainer_statslifetime_add.import_trainer_lifetimestat.value=="" )
  {
		  if(document.trainer_statslifetime_add.earning_stat.value=="" ){
			 alert('Please specify earning.');
			 document.trainer_statslifetime_add.earning_stat.focus();
			 return false;
		  }
			/*if(document.trainer_statslifetime_add.year_stat.value=="" ){
			 alert('Please specify year');
			 document.trainer_statslifetime_add.year_stat.focus();
			 return false;
		  }*/
		  if(document.trainer_statslifetime_add.starts_stat.value=="" ){
			 alert('Please specify starts');
			 document.trainer_statslifetime_add.starts_stat.focus();
			 return false;
		  }
		  if(document.trainer_statslifetime_add.w_stat.value=="" ){
			 alert('Please specify win percentage');
			 document.trainer_statslifetime_add.w_stat.focus();
			 return false;
		  }
		  if(document.trainer_statslifetime_add.win_stat.value=="" ){
			 alert('Please specify wins');
			 document.trainer_statslifetime_add.win_stat.focus();
			 return false;
		  }
		  if(document.trainer_statslifetime_add.places_stat.value=="" ){
			 alert('Please specify places');
			 document.trainer_statslifetime_add.places_stat.focus();
			 return false;
		  }
		  if(document.trainer_statslifetime_add.show_stat.value=="" ){
			 alert('Please specify shows');
			 document.trainer_statslifetime_add.show_stat.focus();
			 return false;
		  }
  }
    if(val!=""){
		document.trainer_statslifetime_add.action="?act=edit";
		document.trainer_statslifetime_add.submit();
	  }else{
		 document.trainer_statslifetime_add.action="?act=add";
		document.trainer_statslifetime_add.submit();
	  }
  return true;
}

/////////////////owner anual statistics\\\\\\\

function chk_owner_statsanual(val){                                
 
  if(document.owner_statsanual_add.import_owner_annualstat.value=="" )
  {
		  if(document.owner_statsanual_add.earning_stat.value=="" ){
			 alert('Please specify earning.');
			 document.owner_statsanual_add.earning_stat.focus();
			 return false;
		  }
			if(document.owner_statsanual_add.year_stat.value=="" ){
			 alert('Please specify year');
			 document.owner_statsanual_add.year_stat.focus();
			 return false;
		  }
		  if(document.owner_statsanual_add.starts_stat.value=="" ){
			 alert('Please specify starts');
			 document.owner_statsanual_add.starts_stat.focus();
			 return false;
		  }
		  if(document.owner_statsanual_add.w_stat.value=="" ){
			 alert('Please specify win percentage');
			 document.owner_statsanual_add.w_stat.focus();
			 return false;
		  }
		  if(document.owner_statsanual_add.win_stat.value=="" ){
			 alert('Please specify wins');
			 document.owner_statsanual_add.win_stat.focus();
			 return false;
		  }
		  if(document.owner_statsanual_add.places_stat.value=="" ){
			 alert('Please specify places');
			 document.owner_statsanual_add.places_stat.focus();
			 return false;
		  }
		  if(document.owner_statsanual_add.show_stat.value=="" ){
			 alert('Please specify shows');
			 document.owner_statsanual_add.show_stat.focus();
			 return false;
		  }
  }
    if(val!=""){
		document.owner_statsanual_add.action="?act=edit";
		document.owner_statsanual_add.submit();
	  }else{
		 document.owner_statsanual_add.action="?act=add";
		document.owner_statsanual_add.submit();
	  }
  return true;
}
///////////////
/////////////////owner lifetime statistics\\\\\\\

function chk_owner_statslifetime(val){                                
 
  if(document.owner_statslifetime_add.import_owner_lifetimestat.value=="" )
  {
		  if(document.owner_statslifetime_add.earning_stat.value=="" ){
			 alert('Please specify earning.');
			 document.owner_statslifetime_add.earning_stat.focus();
			 return false;
		  }
			/*if(document.owner_statslifetime_add.year_stat.value=="" ){
			 alert('Please specify year');
			 document.owner_statslifetime_add.year_stat.focus();
			 return false;
		  }*/
		  if(document.owner_statslifetime_add.starts_stat.value=="" ){
			 alert('Please specify races run');
			 document.owner_statslifetime_add.starts_stat.focus();
			 return false;
		  }
		  if(document.owner_statslifetime_add.w_stat.value=="" ){
			 alert('Please specify win percentage');
			 document.owner_statslifetime_add.w_stat.focus();
			 return false;
		  }
		  if(document.owner_statslifetime_add.win_stat.value=="" ){
			 alert('Please specify wins');
			 document.owner_statslifetime_add.win_stat.focus();
			 return false;
		  }
		  if(document.owner_statslifetime_add.places_stat.value=="" ){
			 alert('Please specify places');
			 document.owner_statslifetime_add.places_stat.focus();
			 return false;
		  }
		  if(document.owner_statslifetime_add.show_stat.value=="" ){
			 alert('Please specify shows');
			 document.owner_statslifetime_add.show_stat.focus();
			 return false;
		  }
  }
    if(val!=""){
		document.owner_statslifetime_add.action="?act=edit";
		document.owner_statslifetime_add.submit();
	  }else{
		 document.owner_statslifetime_add.action="?act=add";
		document.owner_statslifetime_add.submit();
	  }
  return true;
}
//////////////2006 breeder cup \\\\\\\\\\\\\\\\\\\\\\\\
function chk_breeders(val){                                
 if(document.breeders_form.import_breeders.value=="")
 {
  if(document.breeders_form.horse_name.value==""){
     alert('Please Select horse name');
	 document.breeders_form.horse_name.focus();
	 return false;
  }
 }
    if(val!=""){
		document.breeders_form.action="?act=edit";
		document.breeders_form.submit();
	  }else{
		 document.breeders_form.action="?act=add";
		document.breeders_form.submit();
	  }
  return true;
}
///////////jockey anual stat
function chk_jockey_statsanual(val){                                
 
  if(document.jockey_statsanual_add.import_jockey_annualstat.value=="" )
  {
	
		  if(document.jockey_statsanual_add.earning_stat.value=="" ){
			 alert('Please specify earning.');
			 document.jockey_statsanual_add.earning_stat.focus();
			 return false;
		  }
			if(document.jockey_statsanual_add.year_stat.value=="" ){
			 alert('Please specify year');
			 document.jockey_statsanual_add.year_stat.focus();
			 return false;
		  }
		  if(document.jockey_statsanual_add.starts_stat.value=="" ){
			 alert('Please specify starts');
			 document.jockey_statsanual_add.starts_stat.focus();
			 return false;
		  }
		  if(document.jockey_statsanual_add.w_stat.value=="" ){
			 alert('Please specify win percentage');
			 document.jockey_statsanual_add.w_stat.focus();
			 return false;
		  }
		  if(document.jockey_statsanual_add.win_stat.value=="" ){
			 alert('Please specify wins');
			 document.jockey_statsanual_add.win_stat.focus();
			 return false;
		  }
		  if(document.jockey_statsanual_add.places_stat.value=="" ){
			 alert('Please specify places');
			 document.jockey_statsanual_add.places_stat.focus();
			 return false;
		  }
		  if(document.jockey_statsanual_add.show_stat.value=="" ){
			 alert('Please specify earning');
			 document.jockey_statsanual_add.show_stat.focus();
			 return false;
		  }
  }
    if(val!=""){
		document.jockey_statsanual_add.action="?act=edit";
		document.jockey_statsanual_add.submit();
	  }else{
		 document.jockey_statsanual_add.action="?act=add";
		document.jockey_statsanual_add.submit();
	  }
  return true;
}
///////////jockey lifetime stat
function chk_jockey_statslifetime(val){                                
 
  if(document.jockey_statslifetime_add.import_jockey_lifetimestat.value=="" )
  {
		  if(document.jockey_statslifetime_add.earning_stat.value=="" ){
			 alert('Please specify earning.');
			 document.jockey_statslifetime_add.earning_stat.focus();
			 return false;
		  }
			/*if(document.jockey_statslifetime_add.year_stat.value=="" ){
			 alert('Please specify year');
			 document.jockey_statslifetime_add.year_stat.focus();
			 return false;
		  }*/
		  if(document.jockey_statslifetime_add.starts_stat.value=="" ){
			 alert('Please specify starts');
			 document.jockey_statslifetime_add.starts_stat.focus();
			 return false;
		  }
		  if(document.jockey_statslifetime_add.w_stat.value=="" ){
			 alert('Please specify win percentage');
			 document.jockey_statslifetime_add.w_stat.focus();
			 return false;
		  }
		  if(document.jockey_statslifetime_add.win_stat.value=="" ){
			 alert('Please specify wins');
			 document.jockey_statslifetime_add.win_stat.focus();
			 return false;
		  }
		  if(document.jockey_statslifetime_add.places_stat.value=="" ){
			 alert('Please specify places');
			 document.jockey_statslifetime_add.places_stat.focus();
			 return false;
		  }
		  if(document.jockey_statslifetime_add.show_stat.value=="" ){
			 alert('Please specify earning');
			 document.jockey_statslifetime_add.show_stat.focus();
			 return false;
		  }
  }
    if(val!=""){
		document.jockey_statslifetime_add.action="?act=edit";
		document.jockey_statslifetime_add.submit();
	  }else{
		 document.jockey_statslifetime_add.action="?act=add";
		document.jockey_statslifetime_add.submit();
	  }
  return true;
}

//////////FOR TRAINER SECTION
function chk_trainer_vid(val){  
if(document.trainer_vid_add.import_trainer_vid.value=="")
{
 var con=document.trainer_vid_add.trainer_video.value;
  if(val==""){
		  if(document.trainer_vid_add.trainer_video.value=="" && document.trainer_vid_add.trainer_url.value==""){
			 alert('Please upload video or insert some url');
			 document.trainer_vid_add.trainer_video.focus();
			 return false;
		  }
			  
    }
}
    if(val!=""){
		document.trainer_vid_add.action="?act=edit";
		document.trainer_vid_add.submit();
	  }else{
		 document.trainer_vid_add.action="?act=add";
		document.trainer_vid_add.submit();
	  }
  return true;
}
//////STABLE VIDEO
function chk_stable_vid(val){       
if(document.stable_vid_add.import_stable_vid.value=="")
{
 var con=document.stable_vid_add.stable_video.value;
  if(val==""){
		  if(document.stable_vid_add.stable_video.value=="" && document.stable_vid_add.stable_url.value==""){
			 alert('Please upload video or insert some url');
			 document.stable_vid_add.stable_video.focus();
			 return false;
		  }
			  
    }
}
    if(val!=""){
		document.stable_vid_add.action="?act=edit";
		document.stable_vid_add.submit();
	  }else{
		 document.stable_vid_add.action="?act=add";
		document.stable_vid_add.submit();
	  }
  return true;
}
//////OWNER VIDEO
function chk_owner_vid(val){   
if(document.owner_vid_add.import_owner_vid.value=="")
{
 var con=document.owner_vid_add.owner_video.value;
  if(val==""){
		  if(document.owner_vid_add.owner_video.value=="" && document.owner_vid_add.owner_url.value==""){
			 alert('Please upload video or insert some url');
			 document.owner_vid_add.owner_video.focus();
			 return false;
		  }
			  
    }
}
    if(val!=""){
		document.owner_vid_add.action="?act=edit";
		document.owner_vid_add.submit();
	  }else{
		 document.owner_vid_add.action="?act=add";
		document.owner_vid_add.submit();
	  }
  return true;
}
//////JOCKEY VIDEO
function chk_jockey_vid(val){     
if(document.jockey_vid_add.import_jockey_vid.value=="")
{
 var con=document.jockey_vid_add.jockey_video.value;
  if(val==""){
		  if(document.jockey_vid_add.jockey_video.value=="" && document.jockey_vid_add.jockey_url.value==""){
			 alert('Please upload video or insert some url');
			 document.jockey_vid_add.jockey_video.focus();
			 return false;
		  }
			  
    }
}
    if(val!=""){
		document.jockey_vid_add.action="?act=edit";
		document.jockey_vid_add.submit();
	  }else{
		 document.jockey_vid_add.action="?act=add";
		document.jockey_vid_add.submit();
	  }
  return true;
}
//////////FOR TRAINER PICS
function chk_trainer_pic(val){  
if(document.trainer_pics_add.import_trainer_pic.value=="")
{
 var con=document.trainer_pics_add.trainer_image.value;
  if(val==""){
		  if(document.trainer_pics_add.trainer_image.value=="" && document.trainer_pics_add.trainer_url.value==""){
			 alert('Please upload image or insert some url');
			 document.trainer_pics_add.trainer_image.focus();
			 return false;
		  }
			  
    }
  if(con!="")
		  {
			var con1=con.charAt(con.length-3)+con.charAt(con.length-2)+con.charAt(con.length-1);  
			if(con1!='gif' && con1!='GIF' && con1!='jpg' && con1!='JPG')
					{  
						alert("Invalid Image File. Your file type must be jpeg or gif ");
						document.trainer_pics_add.trainer_image.focus();
						return false;
					  
		}
		  }
}
    if(val!=""){
		document.trainer_pics_add.action="?act=edit";
		document.trainer_pics_add.submit();
	  }else{
		 document.trainer_pics_add.action="?act=add";
		document.trainer_pics_add.submit();
	  }
  return true;
}
//FOR TRAIER SECTION
function chk_trainer(val){    
var imp_trainer=document.trainers_form.import_trainer.value;
if(imp_trainer=="")
{

 var con=document.trainers_form.trainer_image.value;
 //alert(document.trainers_form.image_file.height);
  if(document.trainers_form.trainer_name.value==""){
     alert('Please Select name');
	 document.trainers_form.trainer_name.focus();
	 return false;
  }
  if(document.trainers_form.trainer_country.value==""){
     alert('Please Select trainer country');
	 document.trainers_form.trainer_country.focus();
	 return false;
  }
  if(document.trainers_form.trainer_state.value==""){
     alert('Please Select trainer state');
	 document.trainers_form.trainer_state.focus();
	 return false;
  }
  if(document.trainers_form.trainer_city.value==""){
     alert('Please Select trainer city');
	 document.trainers_form.trainer_city.focus();
	 return false;
  }
  
  	/*if(val==""){
		  if(document.trainers_form.trainer_image.value==""){
			 alert('Please upload image');
			 document.trainers_form.trainer_image.focus();
			 return false;
		  }
			  
    }
	if(con!="")
		  {
			var con1=con.charAt(con.length-3)+con.charAt(con.length-2)+con.charAt(con.length-1);  
			if(con1!='gif' && con1!='GIF' && con1!='jpg' && con1!='JPG')
					{  
						alert("Invalid Image File. Your file type must be jpeg or gif ");
						document.trainers_form.trainer_image.focus();
						return false;
					  
		}
		  }*/
}
    if(val!=""){
		document.trainers_form.action="?act=edit";
		document.trainers_form.submit();
	  }else{
		 document.trainers_form.action="?act=add";
		document.trainers_form.submit();
	  }
  return true;
}
///////////FOR OWNER PICS\\\\\\\\\\
function chk_owner_pic(val){  
if(document.owner_pics_add.import_owner_pic.value=="")
{
 var con=document.owner_pics_add.owner_image.value;
  if(val==""){
		  if(document.owner_pics_add.owner_image.value=="" && document.owner_pics_add.owner_url.value==""){
			 alert('Please upload image or insert some url');
			 document.owner_pics_add.owner_image.focus();
			 return false;
		  }
			  
    }
  /*if(con!="")
		  {
			var con1=con.charAt(con.length-3)+con.charAt(con.length-2)+con.charAt(con.length-1);  
			if(con1!='gif' && con1!='GIF' && con1!='jpg' && con1!='JPG')
					{  
						alert("Invalid Image File. Your file type must be jpeg or gif ");
						document.owner_pics_add.owner_image.focus();
						return false;
					  
		}
		  }*/
}
    if(val!=""){
		document.owner_pics_add.action="?act=edit";
		document.owner_pics_add.submit();
	  }else{
		 document.owner_pics_add.action="?act=add";
		document.owner_pics_add.submit();
	  }
  return true;
}
/////////////////////check jockey picture \\\\\\\\\\\

function chk_jockey_pic(val){   
if(document.jockey_pics_add.import_jockey_pic.value=="")
{
 var con=document.jockey_pics_add.jockey_image.value;
  if(val==""){
		  if(document.jockey_pics_add.jockey_image.value=="" && document.jockey_pics_add.jockey_url.value==""){
			 alert('Please upload image or insert some url');
			 document.jockey_pics_add.jockey_image.focus();
			 return false;
		  }
			  
    }
  /*if(con!="")
		  {
			var con1=con.charAt(con.length-3)+con.charAt(con.length-2)+con.charAt(con.length-1);  
			if(con1!='gif' && con1!='GIF' && con1!='jpg' && con1!='JPG')
					{  
						alert("Invalid Image File. Your file type must be jpeg or gif ");
						document.jockey_pics_add.jockey_image.focus();
						return false;
					  
		}
		  }*/
}
    if(val!=""){
		document.jockey_pics_add.action="?act=edit";
		document.jockey_pics_add.submit();
	  }else{
		 document.jockey_pics_add.action="?act=add";
		document.jockey_pics_add.submit();
	  }
  return true;
}
/////////////////////check stable picture \\\\\\\\\\\

function chk_stable_pic(val){  
if(document.stable_pics_add.import_stable_pic.value=="")
{
 var con=document.stable_pics_add.stable_image.value;
  if(val==""){
		  if(document.stable_pics_add.stable_image.value=="" && document.stable_pics_add.stable_url.value==""){
			 alert('Please upload image or insert some url');
			 document.stable_pics_add.stable_image.focus();
			 return false;
		  }
			  
    }
  /*if(con!="")
		  {
			var con1=con.charAt(con.length-3)+con.charAt(con.length-2)+con.charAt(con.length-1);  
			if(con1!='gif' && con1!='GIF' && con1!='jpg' && con1!='JPG')
					{  
						alert("Invalid Image File. Your file type must be jpeg or gif ");
						document.stable_pics_add.stable_image.focus();
						return false;
					  
		}
		  }*/
}
    if(val!=""){
		document.stable_pics_add.action="?act=edit";
		document.stable_pics_add.submit();
	  }else{
		 document.stable_pics_add.action="?act=add";
		document.stable_pics_add.submit();
	  }
  return true;
}
////////////////////for owner section
function chk_owner(val){                                
 var con=document.owners_form.import_owner.value;
 //alert(document.owners_form.image_file.height);
 if(con=="")
 {
  if(document.owners_form.owner_name.value==""){
     alert('Please Select name');
	 document.owners_form.owner_name.focus();
	 return false;
  }
  if(document.owners_form.owner_country.value==""){
     alert('Please Select owner country');
	 document.owners_form.owner_country.focus();
	 return false;
  }
  if(document.owners_form.owner_city.value==""){
     alert('Please Select owner city');
	 document.owners_form.owner_city.focus();
	 return false;
  }
  if(document.owners_form.owner_state.value==""){
     alert('Please Select owner state');
	 document.owners_form.owner_state.focus();
	 return false;
  }
  	
 }
    if(val!=""){
		document.owners_form.action="?act=edit";
		document.owners_form.submit();
	  }else{
		 document.owners_form.action="?act=add";
		document.owners_form.submit();
	  }
  return true;
}
//FOR RACINGCHANNEL SECTION

function chk_racingchannel(val){                                
 var con=document.racingchannels_form.import_racingchannel.value;
 //alert(document.racingchannels_form.image_file.height);
 if(con=="")
 {
  if(document.racingchannels_form.racingchannel_url.value==""){
     alert('Please enter url');
	 document.racingchannels_form.racingchannel_url.focus();
	 return false;
  }
  if(document.racingchannels_form.racingchannel_race_track.value==""){
     alert('Please enter track name');
	 document.racingchannels_form.racingchannel_race_track.focus();
	 return false;
  }
  if(document.racingchannels_form.racingchannel_race_date.value==""){
     alert('Please enter race date');
	 document.racingchannels_form.racingchannel_race_date.focus();
	 return false;
  }
  if(document.racingchannels_form.racingchannel_race_number.value==""){
     alert('Please enter race number');
	 document.racingchannels_form.racingchannel_race_number.focus();
	 return false;
  }
  	 if(document.racingchannels_form.racingchannel_horse_number1.value==""){
     alert('Please enter horse number 1');
	 document.racingchannels_form.racingchannel_horse_number1.focus();
	 return false;
  }
   if(document.racingchannels_form.racingchannel_horse_name1.value==""){
     alert('Please enter horse name 1');
	 document.racingchannels_form.racingchannel_horse_name1.focus();
	 return false;
  }
   if(document.racingchannels_form.racingchannel_win1.value==""){
     alert('Please enter win1 ');
	 document.racingchannels_form.racingchannel_win1.focus();
	 return false;
  }
 }
    if(val!=""){
		document.racingchannels_form.action="?act=edit";
		document.racingchannels_form.submit();
	  }else{
		 document.racingchannels_form.action="?act=add";
		document.racingchannels_form.submit();
	  }
  return true;
}

//////////////////////////

function chk_equibase_TopLeaders(val){                                
 var con=document.equibase_TopLeaders_form.import_equibase_TopLeaders.value;
 //alert(document.racingchannels_form.image_file.height);
 if(con=="")
 {
  if(document.equibase_TopLeaders_form.Leader_Type.value==""){
     alert('Please enter leader type');
	 document.equibase_TopLeaders_form.Leader_Type.focus();
	 return false;
  }
  if(document.equibase_TopLeaders_form.Track.value==""){
     alert('Please enter track name');
	 document.equibase_TopLeaders_form.Track.focus();
	 return false;
  }
  if(document.equibase_TopLeaders_form.Name.value==""){
     alert('Please enter name');
	 document.equibase_TopLeaders_form.Name.focus();
	 return false;
  }
  if(document.equibase_TopLeaders_form.Starting_Date.value==""){
     alert('Please enter starting date');
	 document.equibase_TopLeaders_form.Starting_Date.focus();
	 return false;
  }
  	 if(document.equibase_TopLeaders_form.Ending_Date.value==""){
     alert('Please enter ending date');
	 document.equibase_TopLeaders_form.Ending_Date.focus();
	 return false;
  }
   if(document.equibase_TopLeaders_form.Earnings.value==""){
     alert('Please enter win earnings');
	 document.equibase_TopLeaders_form.Earnings.focus();
	 return false;
  }
   
 }
    if(val!=""){
		document.equibase_TopLeaders_form.action="?act=edit";
		document.equibase_TopLeaders_form.submit();
	  }else{
		 document.equibase_TopLeaders_form.action="?act=add";
		document.equibase_TopLeaders_form.submit();
	  }
  return true;
}

/////////RACING LEADER \\\\\\

function chk_equibase_RacingLeader(val){                                
 var con=document.equibase_RacingLeaders_form.import_equibase_RacingLeader.value;
 //alert(document.racingchannels_form.image_file.height);
 if(con=="")
 {
  if(document.equibase_RacingLeaders_form.List_name.value==""){
     alert('Please enter List name');
	 document.equibase_RacingLeaders_form.List_name.focus();
	 return false;
  }
  if(document.equibase_RacingLeaders_form.order.value==""){
     alert('Please enter track order');
	 document.equibase_RacingLeaders_form.order.focus();
	 return false;
  }
  if(document.equibase_RacingLeaders_form.Name.value==""){
     alert('Please enter name');
	 document.equibase_RacingLeaders_form.Name.focus();
	 return false;
  }
  /*if(document.equibase_RacingLeaders_form.Starting_Date.value==""){
     alert('Please enter starting date');
	 document.equibase_RacingLeaders_form.Starting_Date.focus();
	 return false;
  }
  	 if(document.equibase_RacingLeaders_form.Ending_Date.value==""){
     alert('Please enter ending date');
	 document.equibase_RacingLeaders_form.Ending_Date.focus();
	 return false;
  }*/
   if(document.equibase_RacingLeaders_form.Earnings.value==""){
     alert('Please enter earnings');
	 document.equibase_RacingLeaders_form.Earnings.focus();
	 return false;
  }
   
 }
    if(val!=""){
		document.equibase_RacingLeaders_form.action="?act=edit";
		document.equibase_RacingLeaders_form.submit();
	  }else{
		 document.equibase_RacingLeaders_form.action="?act=add";
		document.equibase_RacingLeaders_form.submit();
	  }
  return true;
}


/////FOR ENTRA SECTION \\\\\\\

function chk_ntra(val){                                
 var con=document.ntra_form.import_ntra.value;
 //alert(document.racingchannels_form.image_file.height);
 if(con=="")
 {
  if(document.ntra_form.ntra_url.value==""){
     alert('Please enter url type');
	 document.ntra_form.ntra_url.focus();
	 return false;
  }
  if(document.ntra_form.ntra_race_date.value==""){
     alert('Please enter race date');
	 document.ntra_form.ntra_race_date.focus();
	 return false;
  }
  if(document.ntra_form.ntra_track_name.value==""){
     alert('Please enter track name');
	 document.ntra_form.ntra_track_name.focus();
	 return false;
  }
  if(document.ntra_form.ntra_location.value==""){
     alert('Please enter location date');
	 document.ntra_form.ntra_location.focus();
	 return false;
  }
  	 if(document.ntra_form.ntra_entry_draw.value==""){
     alert('Please enter entry draw date');
	 document.ntra_form.ntra_entry_draw.focus();
	 return false;
  }
   if(document.ntra_form.ntra_time_zone.value==""){
     alert('Please enter time zone');
	 document.ntra_form.ntra_time_zone.focus();
	 return false;
  }
   
 }
    if(val!=""){
		document.ntra_form.action="?act=edit";
		document.ntra_form.submit();
	  }else{
		 document.ntra_form.action="?act=add";
		document.ntra_form.submit();
	  }
  return true;
}



//FOR JOCKEY SECTION
function chk_jockey(val){   
 var imp_chk=document.jockeys_form.import_jockey.value;
 if(imp_chk=="")
 {
 var con=document.jockeys_form.jockey_image.value;
 //alert(document.jockeys_form.image_file.height);
  if(document.jockeys_form.jockey_name.value==""){
     alert('Please Select name');
	 document.jockeys_form.jockey_name.focus();
	 return false;
  }
  if(document.jockeys_form.jockey_country.value==""){
     alert('Please Select jockey country');
	 document.jockeys_form.jockey_country.focus();
	 return false;
  }
  if(document.jockeys_form.jockey_city.value==""){
     alert('Please Select jockey city');
	 document.jockeys_form.jockey_city.focus();
	 return false;
  }
  /*
  	if(val==""){
		  if(document.jockeys_form.jockey_image.value==""){
			 alert('Please upload image');
			 document.jockeys_form.jockey_image.focus();
			 return false;
		  }
			  
    }*/
	if(con!="")
		  {
			var con1=con.charAt(con.length-3)+con.charAt(con.length-2)+con.charAt(con.length-1);  
			if(con1!='gif' && con1!='GIF' && con1!='jpg' && con1!='JPG')
					{  
						alert("Invalid Image File. Your file type must be jpeg or gif ");
						document.jockeys_form.jockey_image.focus();
						return false;
					  
		}
		  }
 }
    if(val!=""){
		document.jockeys_form.action="?act=edit";
		document.jockeys_form.submit();
	  }else{
		 document.jockeys_form.action="?act=add";
		document.jockeys_form.submit();
	  }
  return true;
}

////////////////////for stable section
function chk_stable(val){    
// alert(val);
// alert("ok");
 var imp_stable=document.stables_form.import_stable.value;

 if(imp_stable=="")
 {
  if(document.stables_form.stable_name.value==""){
     alert('Please Select name');
	 document.stables_form.stable_name.focus();
	 return false;
  }
  if(document.stables_form.stable_country.value==""){
     alert('Please Select stable country');
	 document.stables_form.stable_country.focus();
	 return false;
  }
  if(document.stables_form.stable_city.value==""){
     alert('Please Select stable city');
	 document.stables_form.stable_city.focus();
	 return false;
  }
  if(document.stables_form.stable_state.value==""){
     alert('Please Select stable state');
	 document.stables_form.stable_state.focus();
	 return false;
  }
  if(document.stables_form.stable_phone1.value==""){
     alert('Please Select stable phone1');
	 document.stables_form.stable_phone1.focus();
	 return false;
  }
  if(document.stables_form.stable_url.value==""){
     alert('Please Select stable url');
	 document.stables_form.stable_url.focus();
	 return false;
  }
  if(document.stables_form.stable_email.value==""){
     alert('Please Select stable email');
	 document.stables_form.stable_email.focus();
	 return false;
  }
  	
}
    if(val!=""){
		document.stables_form.action="?act=edit";
		document.stables_form.submit();
	  }else{
		 document.stables_form.action="?act=add";
		document.stables_form.submit();
	  }
  return true;
}
////////////////////for racetrack section
function chk_racetrack(val){                                
 var imp_racetrack=document.racetracks_form.import_racetrack.value;
 //alert(document.racetracks_form.image_file.height);
 if(imp_racetrack=="")
 {
  if(document.racetracks_form.racetrack_name.value==""){
     alert('Please Select name');
	 document.racetracks_form.racetrack_name.focus();
	 return false;
  }
  /*if(document.racetracks_form.racetrack_track_code.value==""){
     alert('Please Select track code');
	 document.racetracks_form.racetrack_track_code.focus();
	 return false;
  }*/
 /* if(document.racetracks_form.racetrack_phone1.value==""){
     alert('Please Select racetrack phone1');
	 document.racetracks_form.racetrack_phone1.focus();
	 return false;
  }*/
  /*if(document.racetracks_form.racetrack_street1.value==""){
     alert('Please Select street1');
	 document.racetracks_form.racetrack_street1.focus();
	 return false;
  }*/
  if(document.racetracks_form.racetrack_country.value==""){
     alert('Please Select racetrack country');
	 document.racetracks_form.racetrack_country.focus();
	 return false;
  }
  /*if(document.racetracks_form.racetrack_city.value==""){
     alert('Please Select racetrack city');
	 document.racetracks_form.racetrack_city.focus();
	 return false;
  }
  if(document.racetracks_form.racetrack_state.value==""){
     alert('Please Select racetrack state');
	 document.racetracks_form.racetrack_state.focus();
	 return false;
  }*/
 /* if(document.racetracks_form.racetrack_driving.value==""){
     alert('Please Select racetracks driving');
	 document.racetracks_form.racetrack_driving.focus();
	 return false;
  }
  if(document.racetracks_form.racetrack_weather.value==""){
     alert('Please Select racetracks weather');
	 document.racetracks_form.racetrack_weather.focus();
	 return false;
  }
  if(document.racetracks_form.racetrack_track_lenth.value==""){
     alert('Please Select racetracks track lenth');
	 document.racetracks_form.racetrack_track_lenth.focus();
	 return false;
  }
  if(document.racetracks_form.racetrack_strech_lenth.value==""){
     alert('Please Select racetracks strech lenth');
	 document.racetracks_form.racetrack_strech_lenth.focus();
	 return false;
  }
  if(document.racetracks_form.racetrack_speed_rating.value==""){
     alert('Please Select racetracks speed rating');
	 document.racetracks_form.racetrack_speed_rating.focus();
	 return false;
  }
  if(document.racetracks_form.point_interest.value==""){
     alert('Please Select racetracks point of interest');
	 document.racetracks_form.point_interest.focus();
	 return false;
  }
 */
 }
    if(val!=""){
		document.racetracks_form.action="?act=edit";
		document.racetracks_form.submit();
	  }else{
		 document.racetracks_form.action="?act=add";
		document.racetracks_form.submit();
	  }
  return true;
}
//////////////for stakes section

function chk_stake(val){                                
 var imp_stake=document.stakes_form.import_stake.value;
 //alert(document.stakes_form.image_file.height);
 if(imp_stake=="")
 {
  if(document.stakes_form.stake_name.value==""){
     alert('Please Select name');
	 document.stakes_form.stake_name.focus();
	 return false;
  }
  if(document.stakes_form.stake_trackcode.value==""){
     alert('Please Select stake trackcode');
	 document.stakes_form.stake_trackcode.focus();
	 return false;
  }
  if(document.stakes_form.stake_date.value==""){
     alert('Please Select stake date');
	 document.stakes_form.stake_date.focus();
	 return false;
  }
  if(document.stakes_form.stake_grade.value==""){
     alert('Please Select stake grade');
	 document.stakes_form.stake_grade.focus();
	 return false;
  }
  if(document.stakes_form.stake_purse.value==""){
     alert('Please Select stake purse');
	 document.stakes_form.stake_purse.focus();
	 return false;
  }
  if(document.stakes_form.stake_hta.value==""){
     alert('Please Select stake hta');
	 document.stakes_form.stake_hta.focus();
	 return false;
  }
  if(document.stakes_form.stake_distance.value==""){
     alert('Please Select stake distance');
	 document.stakes_form.stake_distance.focus();
	 return false;
  }
  	
 }
    if(val!=""){
		document.stakes_form.action="?act=edit";
		document.stakes_form.submit();
	  }else{
		 document.stakes_form.action="?act=add";
		document.stakes_form.submit();
	  }
  return true;
}
///////check handicapper\\\\\\\\\\\\
function chk_handicapper(val){    
// alert(val);
// alert("ok");
 var imp_handicapper=document.handicappers_form.import_handicapper.value;

 if(imp_handicapper=="")
 {
  if(document.handicappers_form.handicapper_name.value==""){
     alert('Please Select name');
	 document.handicappers_form.handicapper_name.focus();
	 return false;
  }
  if(document.handicappers_form.handicapper_country.value==""){
     alert('Please Select handicapper country');
	 document.handicappers_form.handicapper_country.focus();
	 return false;
  }
  if(document.handicappers_form.handicapper_city.value==""){
     alert('Please Select handicapper city');
	 document.handicappers_form.handicapper_city.focus();
	 return false;
  }
  if(document.handicappers_form.handicapper_state.value==""){
     alert('Please Select handicapper state');
	 document.handicappers_form.handicapper_state.focus();
	 return false;
  }
  if(document.handicappers_form.handicapper_phone1.value==""){
     alert('Please Select handicapper phone1');
	 document.handicappers_form.handicapper_phone1.focus();
	 return false;
  }
  if(document.handicappers_form.handicapper_url.value==""){
     alert('Please Select handicapper url');
	 document.handicappers_form.handicapper_url.focus();
	 return false;
  }
  if(document.handicappers_form.handicapper_email.value==""){
     alert('Please Select handicapper email');
	 document.handicappers_form.handicapper_email.focus();
	 return false;
  }
  if(document.handicappers_form.handicapper_street1.value==""){
     alert('Please Select handicapper street1');
	 document.handicappers_form.handicapper_street1.focus();
	 return false;
  }
  if(document.handicappers_form.handicapper_contact.value==""){
     alert('Please Select handicapper contact');
	 document.handicappers_form.handicapper_contact.focus();
	 return false;
  }
  	
}
    if(val!=""){
		document.handicappers_form.action="?act=edit";
		document.handicappers_form.submit();
	  }else{
		 document.handicappers_form.action="?act=add";
		document.handicappers_form.submit();
	  }
  return true;
}

///  equine charities\\\\\\\\\\\

function chk_equine_charity(val){                                
 var con=document.equine_charitys_form.import_equine_charity.value;
 //alert(document.equine_charitys_form.image_file.height);
 if(con=="")
 {
  if(document.equine_charitys_form.equine_charity_name.value==""){
     alert('Please enter name');
	 document.equine_charitys_form.equine_charity_name.focus();
	 return false;
  }
  if(document.equine_charitys_form.equine_charity_url.value==""){
     alert('Please enter url');
	 document.equine_charitys_form.equine_charity_url.focus();
	 return false;
  }
  if(document.equine_charitys_form.equine_charity_state.value==""){
     alert('Please enter state');
	 document.equine_charitys_form.equine_charity_state.focus();
	 return false;
  }
  if(document.equine_charitys_form.equine_charity_city.value==""){
     alert('Please enter city');
	 document.equine_charitys_form.equine_charity_city.focus();
	 return false;
  }
  	 
 }
    if(val!=""){
		document.equine_charitys_form.action="?act=edit";
		document.equine_charitys_form.submit();
	  }else{
		 document.equine_charitys_form.action="?act=add";
		document.equine_charitys_form.submit();
	  }
  return true;
}

// for PORTFOLIO  BACKGROUND SETTING
function drop1(dr)
{

   
	var d = dr.options[dr.selectedIndex].value;
	window.location.href = d;
	dr.selectedindex = 0;
}

function portform(){

  document.colorform.action="?act=md";
 document.colorform.submit();

}

//FOR SUPPORT
function support_check()
{
	if(document.frm1.to.value=="")
	{
		alert("Please specify the recipient");
		document.frm1.to.focus();
		return false;
	}
	if(document.frm1.sub.value=="")
	{
		alert("Please specify the subject");
		document.frm1.sub.focus();
		return false;
	}
	return true;
}

 function reset_all(frm,i)
       {
             var flag=0;
			 var strng = "";
			 for(j=0;j<i;j++)
             {
                if(document.getElementById('checkbox'+j).checked)
                {
                    flag=1;
					if(j == 0){
					 strng=document.getElementById('checkbox'+j).value;
					 
					 }else{
					  strng=strng+","+document.getElementById('checkbox'+j).value;
					  
					 }
                }
             }
			
             if(flag==0)
             {
                  alert("Please Check the item !!!");
             }
             else
             {
                var d=confirm("Are you sure to reset all selected artist?");
				 if(d)
				 {     
					   frm.action="?activate=none&str="+strng;
					  frm.submit();
				 }	  
				
             }
       } 

// ADMIN UPDATE ACCOUNT

function emailValidation(frmName, fieldName) {
		var Messages='';
		if(!(frmName[fieldName].value.indexOf('\@') > -1)) {
		// Email must contain '@'
		Messages+='* Invalid "Email".\n';
		}else if(frmName[fieldName].value.substr(frmName[fieldName].value.indexOf('\@')+1,1) == ".") {
		// @ and '.' (dot) in consecutive position!
		Messages+='* Invalid "Email".\n';
		}else if(!(frmName[fieldName].value.indexOf('.') > -1)) {
		// Email must contain '.'
		Messages+='* Invalid "Email".\n';
		}else if(frmName[fieldName].value.length < frmName[fieldName].value.indexOf('.')+3){
		// Email must contain alleast 2 character after '.'
		Messages+='* Invalid "Email".\n';
		}
		return Messages;
	}

