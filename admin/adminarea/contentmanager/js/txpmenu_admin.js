var tsaveState = 1;

var tpressedFontColor = "#AA0000";

var tpathPrefix_img = "js/img/";

var tlevelDX = 10;
var ttoggleMode = 1;

var texpanded = 0;
var tcloseExpanded   = 0;
var tcloseExpandedXP = 0;

var tblankImage      = "js/img/blank.gif";
var tmenuWidth       = 200;
var tmenuHeight      = 0;

var tabsolute        = 1;
var tleft            = 5;
var ttop             = 50;

var tfloatable       = 0;
var tfloatIterations = 10;

var tmoveable        = 0;
var tmoveImage       = "js/img/movepic.gif";
var tmoveImageHeight = 12;

var tfontStyle       = "normal 8pt Tahoma";
var tfontColor       = ["#215DC6","#428EFF"];
var tfontDecoration  = ["none","underline"];

var titemBackColor   = ["#D6DFF7","#D6DFF7"];
var titemAlign       = "left";
var titemBackImage   = ["",""];
var titemCursor      = "pointer";
var titemHeight      = 22;
var titemTarget      = "";

var ticonWidth       = 21;
var ticonHeight      = 15;
var ticonAlign       = "left";

var tmenuBackImage   = "";
var tmenuBackColor   = "";
var tmenuBorderColor = "#FFFFFF";
var tmenuBorderStyle = "solid";
var tmenuBorderWidth = 0;

var texpandBtn       =["expandbtn2.gif","expandbtn2.gif","collapsebtn2.gif"];
var texpandBtnW      = 9;
var texpandBtnH      = 9;
var texpandBtnAlign  = "left"

var tpoints       = 0;
var tpointsImage  = "";
var tpointsVImage = "";
var tpointsCImage = "";

// XP-Style Parameters
var tXPStyle = 1;
var tXPIterations = 10;                  // expand/collapse speed
var tXPTitleBackColor    = "#265BCC";
var tXPExpandBtn    = ["xpexpand1.gif","xpexpand2.gif","xpcollapse1.gif","xpcollapse2.gif"];
var tXPTitleBackImg = "xptitle.gif";

var tXPTitleLeft      = "xptitleleft.gif";
var tXPTitleLeftWidth = 4;

var tXPBtnWidth  = 25;
var tXPBtnHeight = 25;

var tXPIconWidth  = 31;
var tXPIconHeight = 32;

var tXPFilter=1;

var tXPBorderWidth = 1;
var tXPBorderColor = '#FFFFFF';



var tstyles =
[
    ["tfontStyle=bold 8pt Tahoma","titemBackColor=#265BCC,#265BCC","tfontColor=#FFFFFF,#428EFF", "tfontDecoration=none,none"],
    ["tfontStyle=bold 8pt Tahoma","titemBackColor=#265BCC,#265BCC","tfontColor=#215DC6,#428EFF", "tfontDecoration=none,none"],
    ["tfontDecoration=none,none"],
    ["tfontStyle=bold 8pt Tahoma","tfontColor=#444444,#5555FF"],
];

var tXPStyles =
[
    ["tXPTitleBackColor=#D0DAF8", "tXPExpandBtn=xpexpand3.gif,xpexpand4.gif,xpcollapse3.gif,xpcollapse4.gif", "tXPTitleBackImg=xptitle2.gif"]
];

var tmenuItems =
[
    ["+Admin Control Arena", "", "xpicon1.gif","","", "Admin Control Arena","","0"],
		["|Home", "home.php", "icon1.gif", "icon1o.gif", "", "Home"],
		["|Logout", "index.php", "icon1.gif", "icon1o.gif", "", "Logout"],
	
	["Horse Racing Management", "", "","","", "Horse Racing Management",,"1","0"],
		
		["|Add Historical Race", "horse_history.php", "icon1.gif", "icon1o.gif", "", "Add Historical Race"],
		["|Equine Charity management", "equine_charities_view.php", "icon1.gif", "icon1o.gif", "", "Manage Equine Charity"],
		["|Handicapper management", "handicapper_view.php", "icon1.gif", "icon1o.gif", "", "Manage Handicapper"],
		["|History Race Timezone", "timezone.php", "icon1.gif", "icon1o.gif", "", "History Race Timezone"],
		
		["|Horse management", "horse_view.php", "icon1.gif", "icon1o.gif", "", "Manage Horse"],
		
		["|Jockey management", "jockey_view.php", "icon1.gif", "icon1o.gif", "", "Manage Jockey"],
		["|Ntra management", "ntra_view.php", "icon1.gif", "icon1o.gif", "", "Manage Ntra"],
		["|Owner management", "owner_view.php", "icon1.gif", "icon1o.gif", "", "Manage Owner"],
		["|Racing Channel management", "racingchannel_view.php", "icon1.gif", "icon1o.gif", "", "Manage Racing Channel"],
		["|Racingleader management", "racingleader_view.php", "icon1.gif", "icon1o.gif", "", "Manage Racingleader"],
		["|Racetrack management", "racetrack_view.php", "icon1.gif", "icon1o.gif", "", "Manage Racetrack"],
		["|Stable management", "stable_view.php", "icon1.gif", "icon1o.gif", "", "Manage Stable"],
		["|Stakes management", "stake_view.php", "icon1.gif", "icon1o.gif", "", "Manage Stakes"],
		["|Trainer management", "trainer_view.php", "icon1.gif", "icon1o.gif", "", "Manage Trainer"],
		["|Topleader management", "topleader_view.php", "icon1.gif", "icon1o.gif", "", "Manage Topleader"],
		
		["2012 Racing Management", "", "","","", "2012 Racing Management",,"1","0"],
		["|2012 Kentucky Derby Odds", "2012_kentucky_derby.php", "icon1.gif", "icon1o.gif", "", "2012 Kentucky Derby Odds"],	
		["|2012 Kentucky Prep Races", "kentucky_prep_race_2011.php", "icon1.gif", "icon1o.gif", "", "2012 Kentucky Prep Races"],

		
		["2011 Racing Management", "", "","","", "2011 Racing Management",,"1","0"],
		["|2011 Belmont Stakes", "2011_belmont.php", "icon1.gif", "icon1o.gif", "", "2011 Belmont Stakes"],
		["|2011 Breeders Cup ", "2011_breeders_cup.php", "icon1.gif", "icon1o.gif", "", "2011 Breeders Cup"],
		["|2011 Breeders Cup Odds ", "2011_breeder_odds.php", "icon1.gif", "icon1o.gif", "", "2011 Breeders Cup Classic Odds"],
		["|2011 Breeders Challenge", "2011_breeders_challenge.php", "icon1.gif", "icon1o.gif", "", "2011 Breeders Challenge"],
		["|2011 KD Contenders", "2011_Kentucky_derby_contenders.php", "icon1.gif", "icon1o.gif", "", "2011 Kentucky Derby Contenders"],				
		["|2011 Kentucky Derby Odds", "2011_Kentucky_derby.php", "icon1.gif", "icon1o.gif", "", "2011 Kentucky Derby Odds"],			
		["|2011 Kentucky Prep Races", "kentucky_prep_race_2010.php", "icon1.gif", "icon1o.gif", "", "2011 Kentucky Prep Races"],
		["|2011 Kentucky Oaks", "2011_Kentucky_oaks.php", "icon1.gif", "icon1o.gif", "", "2011 Kentucky Oaks"],
		["|2011 Kentucky Oaks Odds", "2011_Kentucky_oaks_odds.php", "icon1.gif", "icon1o.gif", "", "2011 Kentucky Oaks Odds"],	
		["|2011 Preakness Stakes", "2011_preakness.php", "icon1.gif", "icon1o.gif", "", "2011 Preakness Stakes"],
		
		
		["2010 Racing Management", "", "","","", "2010 Racing Management",,"1","0"],	
		["|2010 Belmont Stakes", "2010_belmont.php", "icon1.gif", "icon1o.gif", "", "2010 Belmont Stakes"],	
		["|2010 Breeders Cup ", "2010_breeders_cup.php", "icon1.gif", "icon1o.gif", "", "2010 Breeders Cup"],
		["|2010 Breeders Challenge", "2010_breeders_challenge.php", "icon1.gif", "icon1o.gif", "", "2010 Breeders Challenge"],
		["|2010 Kentucky Derby", "2010_Kentucky_derby.php", "icon1.gif", "icon1o.gif", "", "2010 Kentucky Derby"],		
		["|2010 Kentucky Oaks", "2010_Kentucky_oaks.php", "icon1.gif", "icon1o.gif", "", "2010 Kentucky Oaks"],
		["|2010 Preakness Stakes", "2010_preakness.php", "icon1.gif", "icon1o.gif", "", "2010 Preakness Stakes"],
		
		["2009 Racing Management", "", "","","", "2009 Racing Management",,"1","0"],	
		["|2009 Belmont Stakes", "2009_belmont.php", "icon1.gif", "icon1o.gif", "", "2009 Belmont Stakes"],	
		["|2009 Breeders Cup ", "2009_breeders_cup.php", "icon1.gif", "icon1o.gif", "", "2009 Breeders Cup"],
		["|2009 Breeders Challenge", "2009_breeders_challenge.php", "icon1.gif", "icon1o.gif", "", "2009 Breeders Challenge"],
		["|2009 Dubai World Cup", "2009_dubai_world_cup.php", "icon1.gif", "icon1o.gif", "", "2009 Dubai World Cup"],
		["|2009 Kentucky Prep Races", "kentucky_prep_race_2009.php", "icon1.gif", "icon1o.gif", "", "2009 Kentucky Prep Races"],
		["|2009 Kentucky Derby", "2009_Kentucky_derby.php", "icon1.gif", "icon1o.gif", "", "2009 Kentucky Derby"],		
		["|2009 Kentucky Oaks", "2009_Kentucky_oaks.php", "icon1.gif", "icon1o.gif", "", "2009 Kentucky Oaks"],
		["|2009 Melbourne Cup", "2009_melbourne_result.php", "icon1.gif", "icon1o.gif", "", "2009 Melbourne Cup"],
		["|2009 Preakness Stakes", "2009_preakness.php", "icon1.gif", "icon1o.gif", "", "2009 Preakness Stakes"],	
			
		
		
	["2008 Racing Management", "", "","","", "2008 Racing Management",,"1","0"],		
		["|2008 Belmont Stakes", "2008_belmont.php", "icon1.gif", "icon1o.gif", "", "2008 Belmont Stakes"],	
		["|2008 Breeders Cup ", "2008_breeders_cup.php", "icon1.gif", "icon1o.gif", "", "2008 Breeders Cup"],
		["|2008 Breeders Challenge", "breeders_challenge.php", "icon1.gif", "icon1o.gif", "", "2008 Breeders Challenge"],
		["|2008 Dubai World Cup", "2008_dubai_world_cup.php", "icon1.gif", "icon1o.gif", "", "2008 Dubai World Cup"],	
		["|2008 Kentucky Derby", "2008_Kentucky_derby.php", "icon1.gif", "icon1o.gif", "", "2008 Kentucky Derby"],		
		["|2008 Kentucky Oaks", "2008_Kentucky_oaks.php", "icon1.gif", "icon1o.gif", "", "2008 Kentucky Oaks"],
		["|2008 Melbourne Cup", "2008_melbourne_result.php", "icon1.gif", "icon1o.gif", "", "2008 Melbourne Cup"],
		["|2008 Preakness Stakes", "2008_preakness.php", "icon1.gif", "icon1o.gif", "", "2008 Preakness Stakes"],	
		
	["2007 Racing Management", "", "","","", "2007 Racing Management",,"1","0"],
		["|2007 Belmont Stakes", "belmont.php", "icon1.gif", "icon1o.gif", "", "2007 Belmont Stakes"],	
		["|2007 Breeders management", "2007_breeders_cup.php", "icon1.gif", "icon1o.gif", "", "Manage 2007 Breeders"],
		["|2007 Dubai World", "2007_dubai_world_cup.php", "icon1.gif", "icon1o.gif", "", "2007 Dubai World Cup"],
		["|2007 Kentucky Derby", "2007_Kentucky_derby.php", "icon1.gif", "icon1o.gif", "", "2007 Kentucky Derby"],
		["|2007 Kentucky Oaks", "2007_Kentucky_oaks.php", "icon1.gif", "icon1o.gif", "", "2007 Kentucky Oaks"],
		["|2007 Preakness Stakes", "Preakness.php", "icon1.gif", "icon1o.gif", "", "2007 Preakness Stakes"],
		
	["2006 Racing Management", "", "","","", "2006 Racing Management",,"1","0"],		
		["|2006 Breeders management", "breeders_view.php", "icon1.gif", "icon1o.gif", "", "Manage 2006 Breeders"],
		["|2006 Dubai World", "2006_dubai_world_cup.php", "icon1.gif", "icon1o.gif", "", "2006 Dubai World Cup"],
	
	["Past Winner Management", "", "","","", "Past Winner Management",,"1","0"],		
		["|Belmont Stakes Past Winners", "belmont_result.php", "icon1.gif", "icon1o.gif", "", "Belmont Stakes Past Winners"],
		["|Breeders Cup Past Winners", "breeders_past_result.php", "icon1.gif", "icon1o.gif", "", "Breeders Cup Past Winners"],
		["|Dubai World Past Winners", "dubai_world_cup.php", "icon1.gif", "icon1o.gif", "", "Dubai World Past Winners"],
		["|Kentucky Derby Past Winners", "kentucky_derby_result.php", "icon1.gif", "icon1o.gif", "", "Kentucky Derby Past Winner"],
		["|Melbourne Cup Past Winners", "melbourne_result.php", "icon1.gif", "icon1o.gif", "", "Melbourne Cup Past Winner"],
		["|Preakness Stakes Past Winners", "preakness_result.php", "icon1.gif", "icon1o.gif", "", "Preakness Stakes Past Winners"],
		
		
	["Other Management", "", "","","", "Other Management",,"1","0"],
	
		["|About Us", "#", "icon1.gif", "icon1o.gif", "", "About Us"],
		["|Contact Us", "#", "icon1.gif", "icon1o.gif", "", "Contact Us"],
		["|Faq management", "#", "icon1.gif", "icon1o.gif", "", "Manage FAQ"],
		["|Flash video management", "fvp_admin/admin/index.php", "icon1.gif", "icon1o.gif", "", "Manage FLash video"],
		["|Horse Matchups", "Horse-Matchups.php", "icon1.gif", "icon1o.gif", "", "Weekly Horse Matchups"],
		["|Race of the Week", "race_promo.php", "icon1.gif", "icon1o.gif", "", "Race of the Week Promotion"],
		["|You Tube video management", "utube_view.php", "icon1.gif", "icon1o.gif", "", "Manage FLash video"],
		
	["Top Leaders Management", "", "","","", "Other Management",,"1","0"],
	
		["|Top Horse", "top_horse.php", "icon1.gif", "icon1o.gif", "", "Top Horse"],
		["|Top Jockey", "top_jockey.php", "icon1.gif", "icon1o.gif", "", "Top Jockey"],
		["|Top Trainer", "top_trainer.php", "icon1.gif", "icon1o.gif", "", "Top Trainer"],
		
		
			//["||Add", "patient_manage_vitals.php", "icon2.gif", "icon2o.gif", "", "Add"],				
			//["||Edit", "physician_edit.php", "icon2.gif", "icon2o.gif", "", "Edit"],
			//["||Delete", "physician_delete.php", "icon2.gif", "icon2o.gif", "", "Delete"],
			//["||Activate/Deactivate", "physician_status.php", "icon2.gif", "icon2o.gif", "", "Activate/Deactivate"],
		//["|Nurses", "#", "icon1.gif", "icon1o.gif", "", "Manage Nurses"],
			//["||Add", "nurse_add.php", "icon2.gif", "icon2o.gif", "", "Add"],
			//["||Edit", "nurse_edit.php", "icon2.gif", "icon2o.gif", "", "Edit"],
			//["||Delete", "nurse_delete.php", "icon2.gif", "icon2o.gif", "", "Delete"],
			//["||Activate/Deactivate", "nurse_status.php", "icon2.gif", "icon2o.gif", "", "Activate/Deactivate"],
		//["|Transcriptionist", "#", "icon1.gif", "icon1o.gif", "", "Manage Transcriptionist"],
			//["||Add", "transcriptionist_add.php", "icon2.gif", "icon2o.gif", "", "Add"],
			//["||Edit", "transcriptionist_edit.php", "icon2.gif", "icon2o.gif", "", "Edit"],
			//["||Delete", "transcriptionist_delete.php", "icon2.gif", "icon2o.gif", "", "Delete"],
			//["||Activate/Deactivate", "transcriptionist_status.php", "icon2.gif", "icon2o.gif", "", "Activate/Deactivate"],
			//["||Assign Physician", "transcriptionist_assignphysician.php", "icon2.gif", "icon2o.gif", "", "Assign Physician"],
		//["|Receptionist", "#", "icon1.gif", "icon1o.gif", "", "Manage Receptionist"],
			//["||Add", "receptionaist_add.php", "icon2.gif", "icon2o.gif", "", "Add"],
			//["||Edit", "receptionaist_edit.php", "icon2.gif", "icon2o.gif", "", "Edit"],
			//["||Delete", "receptionaist_delete.php", "icon2.gif", "icon2o.gif", "", "Delete"],
			//["||Activate/Deactivate", "receptionaist_status.php", "icon2.gif", "icon2o.gif", "", "Activate/Deactivate"],
            
    //["Configuration", "", "","","", "Configure Setup",,"1","0"],
		//["|Procedure Codes", "", "icon1.gif","icon1o.gif","", "Procedure Codes"],
		//["|Procedure Codes", "#", "icon1.gif", "icon1o.gif", "", "Procedure Codes"],
			//["||Availability Chart", "add_avail_template.php", "icon2.gif", "icon2o.gif", "", "Availability Chart Template"],
		//["Diagnosis Codes", "", "","","Manage Diagnosis Codes", "",,"1","0"],
		//["|Diagnosis Codes", "#", "icon1.gif", "icon1o.gif", "", "Diagnosis Codes"],
		//["Code Types", "", "","","Code Types", "",,"1","0"],
		//["|Code Types", "#", "icon1.gif", "icon1o.gif", "", "Code Types"],
	//["Set Relations", "", "","","Set Relations", "",,"1","0"],
		//["|Profiles", "profiles.php", "icon1.gif", "icon1o.gif", "", "Profiles"],
		//["|Insurers", "#", "icon1.gif", "icon1o.gif", "", "Insurance Module"],
		//["|Classification", "#", "icon1.gif", "icon1o.gif", "", "Insurance Module Classification"],
		//["|HCFA Types of Services", "#", "icon1.gif", "icon1o.gif", "", "HCFA Types of Services"],
		//["|Referrals", "#", "icon1.gif", "icon1o.gif", "", "Referrals"],
		//["|Insurer Classification", "#", "icon1.gif", "icon1o.gif", "", "Insurer Classification"],
		//["|Provider Vs Insurers", "#", "icon1.gif", "icon1o.gif", "", "Provider Vs Insurers"],
		//["|Referral Vs Insurers", "#", "icon1.gif", "icon1o.gif", "", "Referral Vs Insurers"],
		//["|Facility Vs Procedures", "#", "icon1.gif", "icon1o.gif", "", "Facility Vs Procedures"],
		//["|Diagnosis Vs Procedures", "#", "icon1.gif", "icon1o.gif", "", "Diagnosis Vs Procedures"],
		//["|Diagnosis Keywords", "#", "icon1.gif", "icon1o.gif", "", "Diagnosis Keywords/Phrases"],
	//["Insurance Module", "", "","","Manage Insurance Module", "",,"1","0"],	
		//["|Edit", "insurancemodule_edit.php", "icon1.gif", "icon1o.gif", "", "Edit"],
		//["|Delete", "insurancemodule_delete.php", "icon1.gif", "icon1o.gif", "", "Delete"],
		//["|Activate/Deactivate", "insurancemodule_status.php", "icon1.gif", "icon1o.gif", "", "Activate/Deactivate"],
	
	//["Appointments", "", "","","", "Manage Patients Appointments",,"1","0"],
		//["|Add", "appointments_add.php", "icon1.gif", "icon1o.gif", "", "Add"],
		//["|Edit", "appointments_edit.php", "icon1.gif", "icon1o.gif", "", "Edit"],
		//["|Delete", "appointments_delete.php", "icon1.gif", "icon1o.gif", "", "Delete"],
		//["|Activate/Deactivate", "appointments_status.php", "icon1.gif", "icon1o.gif", "", "Delete"],
		//["|Physician Visiting Hours", "#", "icon1.gif", "icon1o.gif", "", "Physician Availability"],
		//["|Set Appointment Time Range", "#", "icon1.gif", "icon1o.gif", "", "Set Appointment Time Range"],
		//["|Set Appointment Location", "#", "icon1.gif", "icon1o.gif", "", "Set Location of Appointments"],
		//["|Time Schedule", "#", "icon1.gif", "icon1o.gif", "", "Time Schedule"],
		//["|Block Physician Availability", "#", "icon1.gif", "icon1o.gif", "", "Manage/Block Physician Availability"],
		
	//["Patients Info", "", "","","", "Patients Info",,"1","0"],
		//["|Add New Patient", "#", "icon1.gif", "icon1o.gif", "", "Add New Patient"],
		//["|Manage Patient", "#", "icon1.gif", "icon1o.gif", "", "Manage Patient"],
		//["|Billing Patient", "#", "icon1.gif", "icon1o.gif", "", "Billing Patient"],
		//["|Payment Received", "#", "icon1.gif", "icon1o.gif", "", "Payment Received"],
		
	//["Reports", "", "","","", "Reports",,"1","0"],
		/*["|AR By Physician/Patient", "#", "icon1.gif", "icon1o.gif", "", "Doctor/Patient Account Receivable Report"],
		["|AR By Insurance", "#", "icon1.gif", "icon1o.gif", "", "Insurance Account Receivable Report"],
		["|Account Aging", "#", "icon1.gif", "icon1o.gif", "", "Account Aging Report"],
		["|Incomplete Claim", "#", "icon1.gif", "icon1o.gif", "", "Incomplete Claim Report"],
		["|Code Utilization", "#", "icon1.gif", "icon1o.gif", "", "Code Utilization Report"],
		["|Balance Patient Payment", "#", "icon1.gif", "icon1o.gif", "", "Balance Patient Payment"],
		["|Balance Insurance Payment", "#", "icon1.gif", "icon1o.gif", "", "Balance Insurance Payment"],
		["|Overdue Accounts", "#", "icon1.gif", "icon1o.gif", "", "Overdue Account"],
		["|Print Statements", "#", "icon1.gif", "icon1o.gif", "", "Print Statements"],*/
		
    //["Internal Messaging", "", "","","", "Internal Messaging",,"1","0"],
		/*["|Create Message", "#", "icon1.gif", "icon1o.gif", "", "Create Message"],
		["|Inbox", "#", "icon1.gif", "icon1o.gif", "", "Inbox"],
		["|Outbox", "#", "icon1.gif", "icon1o.gif", "", "Outbox"],*/
		
	//["Complexity Level", "", "","","Complexity Level", "",,"1","0"],
	//	["|Assign diagnosis codes based on it", "complexitylevel_assigndiagnosiscodes.php", "icon1.gif", "icon1o.gif", "", "Assign diagnosis codes based on it"],
		
	//["FAQ", "", "","","FAQ", "",,"1","0"],
		//["|Approve Question", "faq_approvequestion.php", "icon1.gif", "icon1o.gif", "", "Approve Question"],
		//["|Post Question", "faq_postquestion.php", "icon1.gif", "icon1o.gif", "", "Post Question"],
		//["|Post Answer", "faq_postanswer.php", "icon1.gif", "icon1o.gif", "", "Post Answer"],
	
	//["Complaints", "", "","","Complaints", "",,"1","0"],
		//["|Manage Complaints", "complaints_manage.php", "icon1.gif", "icon1o.gif", "", "Manage Complaints"],

];

apy_tmenuInit();
