<?php	 		 	

/**
 * test if a date is equal or later than another
 *
 * @param string $value the value being tested
 * @param boolean $empty if field can be empty
 * @param array params validate parameter values
 * @param array formvars form var values
 */
function smarty_validate_criteria_isDateOnOrAfter($value, $empty, &$params, &$formvars) { 

        if(strlen($value) == 0) 
            return $empty; 

        if(!isset($params['field2'])) { 
                trigger_error("SmartyValidate: [isDateAfter] parameter 'field2' is missing.");            
                return false; 
        } 
		
		$value=preg_replace("/(\w{2})\/(\d{2})\/(\d{4})/i", "\${2}/\$1/\$3", $value);
        
        $_date1 = strtotime($value); 
        $_date2 = array_key_exists ($params['field2'], $formvars) ? strtotime($formvars[$params['field2']]) : strtotime($params['field2']); 

		
        
        if($_date1 == -1) { 
                trigger_error("SmartyValidate: [isDateAfter] parameter 'field' is not a valid date.");            
                return false; 
        } 
        if($_date2 == -1) { 
                trigger_error("SmartyValidate: [isDateAfter] parameter 'field2' is not a valid date.");            
                return false; 
        } 
		//echo "<br>".$_date1."<br>".$_date2 ;
                
        return $_date1 >= $_date2; 
} 


?>
