<?php	 		 	
require_once('header.php');
require_once (AT_INCLUDE_PATH.'include/ini.php');
/////////////coding part\\\\\\\\\\\\\\\\\\\
$countries = new DataObjects_Countries;
$trainer = new DataObjects_Trainers;
$trainerRecord = new DataObjects_Trainers;
$trainerRecord2 = new DataObjects_Trainers;
if($_REQUEST['act']=='add')
{    
    if($_FILES['import_trainer']['name']!="")
	 {
	 move_uploaded_file($_FILES['import_trainer']['tmp_name'],"temp/trainer_import.csv");
	 $rows = 0;
     $handle = fopen("temp/trainer_import.csv", "r");
     while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
     list($name,$city, $state, $country, $family, $pic, $url, $birthdate, $birthcity, $birthstate, $birthcountry) = $data;
    
    // 3) parse each row and add to the database
    $trainerRecord2->query("select count(*) c from {$trainerRecord2->__table} where name='$name' and city='$city'");
	$trainerRecord2->fetch();
	if($trainerRecord2->c==0)
	{
		$sql = "INSERT INTO {$trainerRecord->__table} (name, city, state, country, family, pic, url, birthdate, birthcity, birthstate, birthcountry) VALUES('".trim($name)."', '$city', '$state', '$country', '$family', '$pic', '$url', '$birthdate', '$birthcity', '$birthstate','$birthcountry')";
		// echo $sql;exit;
		if($rows!=0)
		{
			 if(!($result2 = $trainerRecord->query($sql))) {
			 print("Invalid query:\n");
			 print("SQL: $sql\n");
			 die();
			 }
		 }
	 }
     $rows++;
    
    }
	
   fclose($handle);
   unlink("temp/trainer_import.csv");
	 }
else
	{    
    $trainer->name   = ucwords(trim($_REQUEST['trainer_name']));
	$trainer->country = $_REQUEST['trainer_country'];
	$trainer->state = $_REQUEST['trainer_state'];
	$trainer->city = $_REQUEST['trainer_city'];
	$trainer->family = $_REQUEST['trainer_family'];
	$trainer->url = $_REQUEST['trainer_url'];
	$trainer->birthdate = $_REQUEST['trainer_birthdate'];
	$trainer->birthcity   = $_REQUEST['trainer_birth_city'];
	$trainer->birthstate = $_REQUEST['trainer_birth_state'];
	$trainer->birthcountry = $_REQUEST['trainer_birth_country'];
	$trainer->notes = $_REQUEST['trainer_notes'];
	$trainer->GHBnotes = $_REQUEST['trainer_notes_GHB'];
	$trainer->HBnotes = $_REQUEST['trainer_notes_HB'];
	
	//$trainer->insert();
	$fname = $HTTP_POST_FILES['trainer_image']['name'];
	if($fname!="")
	 {
	 				    $ext=stristr($fname,".");
						$ext=strtolower($ext);
						$ImageName=md5(microtime());
						$FilePath= "../uploaded_images/trainer_images/".$ImageName.$ext;
						$ThumbFilePath =  "../uploaded_images/trainer_images/thumb".$ImageName.$ext;
						$trainer->pic  		= $ImageName.$ext;
						if(move_uploaded_file($_FILES['trainer_image']['tmp_name'], $FilePath)){
							   // MAKING THUMBNAIL IMAGE STARTS
								// Instantiate the correct object depending on type of image i.e jpg or png
								$objResize = ImageResizeFactory::getInstanceOf($FilePath, $ThumbFilePath, TRAINER_IMG_W, TRAINER_IMG_H);
								// Call the method to resize the image
								$objResize->getResizedImage();
								unset($objResize);
								// MAKING THUMBNAIL IMAGE ENDS
										
						}else{
							   $smarty->assign('message','Image not uploaded !');
							   header('Location:add_trainer.php');
							   exit();
						}
	}
	else
	{
		$trainer->pic = 'http://64.40.114.205/images/cm/default/trainer.gif';
	}
	 
	
	$trainer->insert();
	}
	//$splash_img->insert();
	header("Location: trainer_view.php");	
	exit;
	
	
}
//////////////EDIT RECORD\\\\\\\\\\\\\\\\\\\\\

if($_REQUEST['act']=="edit"){
    
	$trainer->get($_REQUEST['trainer_id']);
	 $trainer->name   = ucwords(trim($_REQUEST['trainer_name']));
	$trainer->country = $_REQUEST['trainer_country'];
	$trainer->state = $_REQUEST['trainer_state'];
	$trainer->city = $_REQUEST['trainer_city'];
	$trainer->family = $_REQUEST['trainer_family'];
	$trainer->url = $_REQUEST['trainer_url'];
	$trainer->birthdate = $_REQUEST['trainer_birthdate'];
	$trainer->birthcity   = $_REQUEST['trainer_birth_city'];
	$trainer->birthstate = $_REQUEST['trainer_birth_state'];
	$trainer->birthcountry  = $_REQUEST['trainer_birth_country'];
	$trainer->pic = $_REQUEST['trainer_image'];
	$trainer->notes = $_REQUEST['trainer_notes'];
	$trainer->GHBnotes = $_REQUEST['trainer_notes_GHB'];
	$trainer->HBnotes = $_REQUEST['trainer_notes_HB'];
		
		/*if($fname!=""){
								$ext=stristr($fname,".");
								$ext=strtolower($ext);
								$ImageName=md5(microtime());
								$FilePath= "../uploaded_images/trainer_images/".$ImageName.$ext;
								$ThumbFilePath =  "../uploaded_images/trainer_images/thumb".$ImageName.$ext;
									$file="../uploaded_images/trainer_images/".$_REQUEST['trainer_pic'];
									$thumbfile="../uploaded_images/trainer_images/thumb".$_REQUEST['trainer_pic'];
									if(file_exists($file))
										@unlink($file);
									if(file_exists($thumbfile))
										@unlink($thumbfile);
									move_uploaded_file($_FILES['trainer_image']['tmp_name'], $FilePath);
									$trainer->pic 		= $ImageName.$ext;
								     // MAKING THUMBNAIL IMAGE STARTS
								// Instantiate the correct object depending on type of image i.e jpg or png
								$objResize = ImageResizeFactory::getInstanceOf($FilePath, $ThumbFilePath, TRAINER_IMG_W, TRAINER_IMG_H);
								// Call the method to resize the image
								$objResize->getResizedImage();
								unset($objResize);
								// MAKING THUMBNAIL IMAGE ENDS
								
				
		}*/
	
	
	$trainer->update();
	if($_REQUEST['start']==0)
	$start_page="";
	else
	$start_page=$_REQUEST['start'];
	header("Location: trainer_view.php?start=".$start_page."&act_trainer=".$_REQUEST['act_trainer']."&search_trainer=".$_REQUEST['search_trainer']);	
	exit;
	
}

//////////////////////////////////////////////////////////////
if($_REQUEST['trainer_id']!=""){
  $trainer->query("SELECT * FROM {$trainer->__table} WHERE id='".$_REQUEST['trainer_id']."'");
  $trainer->fetch();
    $smarty->assign('trainer_id',$trainer->id);
	$smarty->assign('trainer_name',stripslashes($trainer->name));
	if(preg_match("/www/",$trainer->pic)||preg_match("/http/",$trainer->pic))
	{
	$img_raw=$trainer->pic;
	$exhash=explode('#',$img_raw);
	//print_r($exhash);
	$image=implode("",$exhash);
	$smarty->assign('img_chk',1);
	$smarty->assign('trainer_pic', $image);
	}
	else
	{
	$smarty->assign('trainer_pic', $trainer->pic);
	}
	$smarty->assign('trainer_city',$trainer->city) ;
	$smarty->assign('trainer_state',$trainer->state) ;
	$smarty->assign('trainer_country',$trainer->country);
	$smarty->assign('trainer_family',$trainer->family);
	$smarty->assign('trainer_url',$trainer->url) ;
	$smarty->assign('trainer_birthdate',$trainer->birthdate);
	$smarty->assign('trainer_birth_country',$trainer->birthcountry) ;
	$smarty->assign('trainer_birthstate',$trainer->birthstate) ;
	$smarty->assign('trainer_birthcity',$trainer->birthcity);
	$smarty->assign('trainer_notes',stripslashes($trainer->notes)) ;
	$smarty->assign('trainer_notes_GHB',stripslashes($trainer->GHBnotes)) ;
	$smarty->assign('trainer_notes_HB',stripslashes($trainer->HBnotes)) ;
	
	
}
  $countries->query("select * from {$countries->__table} order by sort_order desc");
  while($countries->fetch())
  {
  $country_name[]=$countries->countries_name;
  $country_id[]=$countries->countries_id;
  $country_code[]=$countries->countries_iso_code_2;
  
  }
  $smarty->assign('country_id',$country_id) ;
  $smarty->assign('country_code',$country_code) ;
  $smarty->assign('country_name',$country_name);
  $smarty->assign('start',$_REQUEST['start']);
  $smarty->assign('act_trainer',$_REQUEST['act_trainer']);
  $smarty->assign('search_trainer',$_REQUEST['search_trainer']) ;

$smarty->display('add_trainer.tpl');
?>