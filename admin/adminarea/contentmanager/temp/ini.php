<?
//define('AT_INCLUDE_PATH','../');
// REQUIRED CLASSES AND FILES FOR PEAR PACKAGES //
error_reporting(E_ALL ^ E_NOTICE);
require_once (AT_INCLUDE_PATH.'include/dbconnect.php');
require_once (AT_INCLUDE_PATH.'classes/Tbl_admin.php');
require_once (AT_INCLUDE_PATH.'classes/Tbl_text_setting.php');
require_once (AT_INCLUDE_PATH.'classes/Tbl_faq.php');
require_once (AT_INCLUDE_PATH.'classes/Horse.php');
require_once (AT_INCLUDE_PATH.'classes/Horsepics.php');
require_once (AT_INCLUDE_PATH.'classes/Horsefacts.php');
require_once (AT_INCLUDE_PATH.'classes/Horsestatsanual.php');
require_once (AT_INCLUDE_PATH.'classes/Horsestatslifetime.php');
require_once (AT_INCLUDE_PATH.'classes/Horsevideo.php');
require_once (AT_INCLUDE_PATH.'classes/Trainers.php');
require_once (AT_INCLUDE_PATH.'classes/Trainerfacts.php');
require_once (AT_INCLUDE_PATH.'classes/Trainerpics.php');
require_once (AT_INCLUDE_PATH.'classes/Trainerstatsannual.php');
require_once (AT_INCLUDE_PATH.'classes/Trainerstatslifetime.php');
require_once (AT_INCLUDE_PATH.'classes/Trainervideo.php');
require_once (AT_INCLUDE_PATH.'classes/Owners.php');
require_once (AT_INCLUDE_PATH.'classes/Ownerfacts.php');
require_once (AT_INCLUDE_PATH.'classes/Ownerpics.php');
require_once (AT_INCLUDE_PATH.'classes/Ownerstatsannual.php');
require_once (AT_INCLUDE_PATH.'classes/Ownerstatslifetime.php');
require_once (AT_INCLUDE_PATH.'classes/Ownervideo.php');
require_once (AT_INCLUDE_PATH.'classes/Jockey.php');
require_once (AT_INCLUDE_PATH.'classes/Jockeyfacts.php');
require_once (AT_INCLUDE_PATH.'classes/Jockeypics.php');
require_once (AT_INCLUDE_PATH.'classes/Jockeystatsannual.php');
require_once (AT_INCLUDE_PATH.'classes/Jockeystatslifetime.php');
require_once (AT_INCLUDE_PATH.'classes/Jockeyvideo.php');
require_once (AT_INCLUDE_PATH.'classes/Racetracks.php');
require_once (AT_INCLUDE_PATH.'classes/Stakes.php');
require_once (AT_INCLUDE_PATH.'classes/Stables.php');
require_once (AT_INCLUDE_PATH.'classes/Stablefacts.php');
require_once (AT_INCLUDE_PATH.'classes/Stablepics.php');
require_once (AT_INCLUDE_PATH.'classes/Stablesvideo.php');
require_once (AT_INCLUDE_PATH.'classes/Racetracks.php');
require_once (AT_INCLUDE_PATH.'classes/Countries.php');
require_once (AT_INCLUDE_PATH.'classes/Equibase_RacingLeader.php');
require_once (AT_INCLUDE_PATH.'classes/Equibase_TopLeaders.php');
require_once (AT_INCLUDE_PATH.'classes/Ntra.php');
require_once (AT_INCLUDE_PATH.'classes/Racingchannel.php');
require_once (AT_INCLUDE_PATH.'classes/Handicappers.php');

// REQUIRED CLASSES FOR SMARTY PACKAGES //
require (AT_INCLUDE_PATH.'libs/Smarty.class.php');
require (AT_INCLUDE_PATH.'include/iniSmarty.php');
//config file
require (AT_INCLUDE_PATH.'configs/config.php');
require(AT_INCLUDE_PATH.'configs/front_config.php');
// REQUIRED CLASSES FOR THUMBNAIL IMAGES //
require (AT_INCLUDE_PATH.'lib/ImageResizeClass.php');
require (AT_INCLUDE_PATH.'lib/ImageResizeFactory.php');


?>