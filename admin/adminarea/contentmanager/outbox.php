<?php	 		 	
include "header.php";
include "left.php";
?>
<!-------------------- Body Section ------------------------>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr> 
    <td><br> 
      <table width="90%" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#F6F9FE" class="border">
        <tr bgcolor="#0D4686"> 
          <td width="50%" height="25" style="padding-left:10px;"><strong><font color="#FFFFFF">OUTBOX</font></strong></td>
          <td height="25" align="right" style="padding-right:10px;"> <a href="inbox.php"><strong><font color="#FFFFFF">&laquo; 
            Back to Inbox</font></strong></a> </td>
        </tr>
        <tr> 
          <td height="166" colspan="2"><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
                <td height="25" align="left"><strong></strong></td>
                <td height="25" align="right"> <strong class="bodyb">&#8249; </strong><a href="#" class="bodyb">previous</a> 
                  | <a href="#" class="bodyb">next</a> <strong class="bodyb">&#8250;</strong></td>
              </tr>
              <tr> 
                <td height="25" colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:solid 1px #0D4686;">
                    <form action="" method="post" name="f1">
                      <tr> 
                        <td> <table width="100%" border="0" cellspacing="1" cellpadding="1">
                            <tr align="center"> 
                              <td width="35%" height="25" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">To</font></td>
                              <td width="35%" height="25" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">Patient</font></td>
                              <td width="20%" height="25" bgcolor="#0D4686" class="bodyb"><font style="color:#FFFFFF;">Date 
                                and Time</font></td>
                            </tr>
                            <tr align="center"> 
                              <td height="25" bgcolor="#3D73B8"><a href="message.php"><font style="color:#FFFFFF;">Demo
                                   User</font></a></td>
                              <td height="25" bgcolor="#B5DE97"><a href="message.php">Patient Name</a></td>
                              <td height="25" bgcolor="#B5DE97"><a href="message.php">21 April, 06
                                at  2.30pm</a></td>
                            </tr>
                            <tr align="center"> 
                              <td height="25" bgcolor="#3D73B8"><a href="message.php"><font style="color:#FFFFFF;">Demo
                              User</font></a></td>
                              <td height="25" bgcolor="#B5DE97"><a href="message.php">Patient
                              Name</a></td>
                              <td height="25" bgcolor="#B5DE97"><a href="message.php">21
                              April, 06 at 2.00pm</a></td>
                            </tr>
                            <tr align="center"> 
                              <td height="25" bgcolor="#3D73B8"><a href="message.php"><font style="color:#FFFFFF;">Demo
                              User</font></a></td>
                              <td height="25" bgcolor="#B5DE97"><a href="message.php">Patient
                              Name</a></td>
                              <td height="25" bgcolor="#B5DE97"><a href="message.php">21
                              April, 06 at 1.30pm</a></td>
                            </tr>
                            <tr align="center"> 
                              <td height="25" bgcolor="#3D73B8"><a href="message.php"><font style="color:#FFFFFF;">Demo
                              User</font></a></td>
                              <td height="25" bgcolor="#B5DE97"><a href="message.php">Patient
                              Name</a></td>
                              <td height="25" bgcolor="#B5DE97"><a href="message.php">21
                              April, 06 at 12.30pm</a></td>
                            </tr>
                          </table></td>
                      </tr>
                    </form>
                  </table></td>
              </tr>
              <tr align="right"> 
                <td height="25" colspan="2"> <strong class="bodyb">&#8249; </strong><a href="#" class="bodyb">previous</a> 
                  | <a href="#" class="bodyb">next</a> <strong class="bodyb">&#8250;</strong></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <br> </td>
  </tr>
</table>
<!------------------ End of Body Section ------------------->
<?php	 	
include "footer.php";
?>
