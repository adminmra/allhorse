<?
define('AT_INCLUDE_PATH2','');
// REQUIRED CLASSES AND FILES FOR PEAR PACKAGES //
require_once (AT_INCLUDE_PATH2.'include/dbconnect.php');
require_once (AT_INCLUDE_PATH2.'classes/Tbl_admin.php');
//require_once (AT_INCLUDE_PATH2.'classes/Tbl_text_setting.php');
//require_once (AT_INCLUDE_PATH2.'classes/Tbl_faq.php');
require_once (AT_INCLUDE_PATH2.'classes/Horse.php');
require_once (AT_INCLUDE_PATH2.'classes/Horsepics.php');
require_once (AT_INCLUDE_PATH2.'classes/Horsefacts.php');
require_once (AT_INCLUDE_PATH2.'classes/Horsestatsanual.php');
require_once (AT_INCLUDE_PATH2.'classes/Horsestatslifetime.php');
require_once (AT_INCLUDE_PATH2.'classes/Horsevideo.php');
require_once (AT_INCLUDE_PATH2.'classes/Trainers.php');
require_once (AT_INCLUDE_PATH2.'classes/Trainerfacts.php');
require_once (AT_INCLUDE_PATH2.'classes/Trainerpics.php');
require_once (AT_INCLUDE_PATH2.'classes/Trainerstatsannual.php');
require_once (AT_INCLUDE_PATH2.'classes/Trainerstatslifetime.php');
require_once (AT_INCLUDE_PATH2.'classes/Trainervideo.php');
require_once (AT_INCLUDE_PATH2.'classes/Owners.php');
require_once (AT_INCLUDE_PATH2.'classes/Ownerfacts.php');
require_once (AT_INCLUDE_PATH2.'classes/Ownerpics.php');
require_once (AT_INCLUDE_PATH2.'classes/Ownerstatsannual.php');
require_once (AT_INCLUDE_PATH2.'classes/Ownerstatslifetime.php');
require_once (AT_INCLUDE_PATH2.'classes/Ownervideo.php');
require_once (AT_INCLUDE_PATH2.'classes/Jockey.php');
require_once (AT_INCLUDE_PATH2.'classes/Jockeyfacts.php');
require_once (AT_INCLUDE_PATH2.'classes/Jockeypics.php');
require_once (AT_INCLUDE_PATH2.'classes/Jockeystatsannual.php');
require_once (AT_INCLUDE_PATH2.'classes/Jockeystatslifetime.php');
require_once (AT_INCLUDE_PATH2.'classes/Jockeyvideo.php');
require_once (AT_INCLUDE_PATH2.'classes/Racetracks.php');
require_once (AT_INCLUDE_PATH2.'classes/Stakes.php');
require_once (AT_INCLUDE_PATH2.'classes/Stables.php');
require_once (AT_INCLUDE_PATH2.'classes/Stablefacts.php');
require_once (AT_INCLUDE_PATH2.'classes/Stablepics.php');
require_once (AT_INCLUDE_PATH2.'classes/Stablesvideo.php');
require_once (AT_INCLUDE_PATH2.'classes/Racetracks.php');
require_once (AT_INCLUDE_PATH2.'classes/Countries.php');
require_once (AT_INCLUDE_PATH2.'classes/Equibase_RacingLeader.php');
require_once (AT_INCLUDE_PATH2.'classes/Equibase_TopLeaders.php');
require_once (AT_INCLUDE_PATH2.'classes/Ntra.php');
require_once (AT_INCLUDE_PATH2.'classes/Racingchannel.php');
require_once (AT_INCLUDE_PATH2.'classes/Handicappers.php');
require_once (AT_INCLUDE_PATH2.'classes/Equine_charities.php');
require_once (AT_INCLUDE_PATH2.'classes/2006BreedersCup.php');
require_once (AT_INCLUDE_PATH2.'classes/Past_winners_dubai.php');
require_once (AT_INCLUDE_PATH2.'classes/2006dubaiworldcup.php');
require_once (AT_INCLUDE_PATH2.'classes/2006dubaiworldcupresults.php');
require_once (AT_INCLUDE_PATH2.'classes/2007_Kentucky_derby.php');
require_once (AT_INCLUDE_PATH2.'classes/2007_Kentucky_oaks.php');
require_once (AT_INCLUDE_PATH2.'classes/Preakness.php');
require_once (AT_INCLUDE_PATH2.'classes/Belmont.php');
require_once (AT_INCLUDE_PATH2.'classes/Historyrace.php');
require_once (AT_INCLUDE_PATH2.'classes/2007BreedersCup.php');
require_once (AT_INCLUDE_PATH2.'classes/2008_preakness.php');
require_once (AT_INCLUDE_PATH2.'classes/2008_belmont.php');
require_once (AT_INCLUDE_PATH2.'classes/2008_Kentucky_oaks.php');
require_once (AT_INCLUDE_PATH2.'classes/2008BreedersCup.php');
require_once (AT_INCLUDE_PATH2.'classes/2008dubaiworldcupresults.php');
require_once (AT_INCLUDE_PATH2.'classes/Breeders_challenge.php');
require_once (AT_INCLUDE_PATH2.'classes/Utube.php');
require_once (AT_INCLUDE_PATH2.'classes/2009_kentucky_prep.php');
require_once (AT_INCLUDE_PATH2.'classes/2009_Kentucky_oaks.php');
require_once (AT_INCLUDE_PATH2.'classes/2009_kentucky_derby.php');
require_once (AT_INCLUDE_PATH2.'classes/2009_preakness.php');
require_once (AT_INCLUDE_PATH2.'classes/2009_belmont.php');
require_once (AT_INCLUDE_PATH2.'classes/2009_breeders_challenge.php');
require_once (AT_INCLUDE_PATH2.'classes/2009BreedersCup.php');
require_once (AT_INCLUDE_PATH2.'classes/2009dubaiworldcupresults.php');
require_once (AT_INCLUDE_PATH2.'classes/2010_kentucky_prep.php');
require_once (AT_INCLUDE_PATH2.'classes/2010_kentucky_prep_new.php');
require_once (AT_INCLUDE_PATH2.'classes/2010_Kentucky_oaks.php');
require_once (AT_INCLUDE_PATH2.'classes/2010_preakness.php');
require_once (AT_INCLUDE_PATH2.'classes/2010_belmont.php');
require_once (AT_INCLUDE_PATH2.'classes/2010BreedersCup.php');
require_once (AT_INCLUDE_PATH2.'classes/2010dubaiworldcupresults.php');
require_once (AT_INCLUDE_PATH2.'classes/2010_breeders_challenge.php');
require_once (AT_INCLUDE_PATH2.'classes/Top_leader.php');
require_once (AT_INCLUDE_PATH2.'classes/Horse_matchups.php');
require_once (AT_INCLUDE_PATH2.'classes/2011_Kentucky_oaks.php');
require_once (AT_INCLUDE_PATH2.'classes/2011_preakness.php');
require_once (AT_INCLUDE_PATH2.'classes/2011_belmont.php');
require_once (AT_INCLUDE_PATH2.'classes/2011_kentucky_prep_new.php');
require_once (AT_INCLUDE_PATH2.'classes/2011BreedersCup.php');
require_once (AT_INCLUDE_PATH2.'classes/2011dubaiworldcupresults.php');
require_once (AT_INCLUDE_PATH2.'classes/2011_breeders_challenge.php');




// REQUIRED CLASSES FOR SMARTY PACKAGES //
require(AT_INCLUDE_PATH2.'libs/Smarty.class.php');
require('iniSmarty.php');
//config file
require (AT_INCLUDE_PATH2.'configs/config.php');
require(AT_INCLUDE_PATH2.'configs/front_config.php');
// REQUIRED CLASSES FOR THUMBNAIL IMAGES //
require (AT_INCLUDE_PATH2.'lib/ImageResizeClass.php');
require (AT_INCLUDE_PATH2.'lib/ImageResizeFactory.php');


?>