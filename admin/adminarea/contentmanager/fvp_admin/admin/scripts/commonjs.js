function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

// INTEGER CHECKING FUNCTIONS STARTS
function getkey(e)
{
	if (window.event)
		return window.event.keyCode;
	else if (e)
		return e.which;
	else
		return null;
}
function goodchars(e, goods)
{
	var key, keychar;
	key = getkey(e);
	if (key == null) return true;
	// get character
	keychar = String.fromCharCode(key);
	keychar = keychar.toLowerCase();
	goods = goods.toLowerCase();
	// check goodkeys
	if (goods.indexOf(keychar) != -1)
		return true;
	// control keys
	if ( key==null || key==0 || key==8 || key==9 || key==13 || key==27 )
		return true;
	// else return false
	return false;
}
// INTEGER CHECKING FUNCTIONS ENDS


// TEXTBOX EMPTY CHECKING
function emptyValidation(frmName, fieldName) {
	if(frmName[fieldName].value==null || frmName[fieldName].value==''){
		return false;
	}else{
		return true;
	}
}


// LOGIN VALIDATION
function loginValidation(){
	var Messages='';
	if(!emptyValidation(frmLogin, 'Username'))
		Messages+='* The "Username" can not be left blank.\n';
	if(!emptyValidation(frmLogin, 'Password'))
		Messages+='* The "Password" can not be left blank.\n';	
	if(Messages!=''){
		alert(Messages);	
		return false;
	}	
}

// MY ACCOUNT VALIDATION
function myaccountValidation(){
	var Messages='';
	if(!emptyValidation(frmMyAccount, 'Username'))
		Messages+='* The "Username" can not be left blank.\n';
	if(!emptyValidation(frmMyAccount, 'Email'))
		Messages+='* The "Email" can not be left blank.\n';	
	if(Messages!=''){
		alert(Messages);	
		return false;
	}	
}

// CHANGE PASSWORD
function changePassword(){
	var Messages='';
	if(!emptyValidation(frmChangePassword, 'OldPwd'))
		Messages+='* The "Old Password" can not be left blank.\n';
	if(!emptyValidation(frmChangePassword, 'NewPwd'))
		Messages+='* The "New Password" can not be left blank.\n';
	if(!emptyValidation(frmChangePassword, 'ConfirmPwd'))
		Messages+='* The "Confirm Password" can not be left blank.\n';
	if(frmChangePassword.NewPwd.value!=frmChangePassword.ConfirmPwd.value)			
		Messages+='* Inconsistent Password.\n';
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
}

// JOB VALIDATION    
function jobValidation(){
	var Messages='';
	if(!emptyValidation(frmJob, 'JobTitle'))
		Messages+='* The "Title" can not be left blank.\n';
	if(!emptyValidation(frmJob, 'Post'))
		Messages+='* The "Post" can not be left blank.\n';	
	if(!emptyValidation(frmJob, 'Experience'))
		Messages+='* The "Experience" can not be left blank.\n';
	if(!emptyValidation(frmJob, 'KeySkills'))
		Messages+='* The "Key Skills" can not be left blank.\n';
	if(!emptyValidation(frmJob, 'Location'))
		Messages+='* The "Location" can not be left blank.\n';
	if(!emptyValidation(frmJob, 'SalaryRange'))
		Messages+='* The "Salary Range" can not be left blank.\n';
	if(!emptyValidation(frmJob, 'LastDate'))
		Messages+='* The "Last Date" can not be left blank.\n';						
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
	// STORE DESCRIPTION
	frmJob.Description.value = oEdit1.getHTMLBody();
}

// TIME VALIDATION
function timeValidation(frmName, fieldName){
	var val = frmName[fieldName].value;
	if(val.length < 5)
		return false;
	else if(val.indexOf(':')!='2')
		return false;
	else if(parseInt(val.substr(0, 2))>23)
		return false;
	else if(parseInt(val.substr(3, 2))>59)		
		return false;
	else	
		return true;
}

// EVENT VALIDATION
function eventValidation(){
	var Messages='';
	if(!emptyValidation(frmEvent, 'Date'))
		Messages+='* The "Date" can not be left blank.\n';	
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
	// STORE DESCRIPTION
	frmEvent.Event.value = oEdit1.getHTMLBody();	
}

// OPPORTUNITIES VALIDATION
function opportunitiesValidation(){
	frmOpportunities.Description.value = oEdit1.getHTMLBody();	
}

// GOLF JAZZ VALIDATION
function jazzgolffriendValidation(){
	frmJazzGolfFriend.Description.value = oEdit1.getHTMLBody();	
}

// NEW WINDOW OPEN
function openWin( windowURL, windowName, windowFeatures ) { 
	return window.open( windowURL, windowName, windowFeatures ) ; 
} 

// IMAGE VALIDATION
function imageValidation() {
	var Messages='';
	if(!emptyValidation(frmUploadImages, 'Caption'))
		Messages+='* The "Caption" can not be left blank.\n';	
	if(!emptyValidation(frmUploadImages, 'Image'))
		Messages+='* The "Image" can not be left blank.\n';
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
}

// DOCUMENT VALIDATION
function documentValidation() {
	var Messages='';
	if(!emptyValidation(frmDocument, 'DocTitle'))
		Messages+='* The "Title" can not be left blank.\n';	
	if(!emptyValidation(frmDocument, 'CatID'))
		Messages+='* Please choose "Category".\n';
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
}

// INVESTMENT VALIDATION
function investmentValidation() {
	var Messages='';
	if(!emptyValidation(frmInvestment, 'Heading'))
		Messages+='* The "Heading" can not be left blank.\n';	
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
	frmInvestment.Description.value = oEdit1.getHTMLBody();	
}

// ABOUT VALIDATION
function aboutValidation() {
	frmAbout.About.value = oEdit1.getHTMLBody();	
}

// ABOUT VALIDATION
function missionValidation() {
	frmMission.Mission.value = oEdit1.getHTMLBody();	
}

// BOARD OF DIRECTORS
function boardValidation() {
	var Messages='';
	if(!emptyValidation(frmBoard, 'Name'))
		Messages+='* The "Name" can not be left blank.\n';	
	if(!emptyValidation(frmBoard, 'Title'))
		Messages+='* The "Title" can not be left blank.\n';		
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
}

// STAFF LIST
function staffValidation() {
	var Messages='';
	if(!emptyValidation(frmStaff, 'Name'))
		Messages+='* The "Name" can not be left blank.\n';	
	if(!emptyValidation(frmStaff, 'Title'))
		Messages+='* The "Title" can not be left blank.\n';		
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
}

// ANNUAL WISH LIST VALIDATION 
function annualWishValidation() {
	var Messages='';
	if(!emptyValidation(frmAnnualWish, 'Category'))
		Messages+='* The "Category" can not be left blank.\n';	
	if(!emptyValidation(frmAnnualWish, 'Cost'))
		Messages+='* The "Cost" can not be left blank.\n';		
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
	frmAnnualWish.Description.value = oEdit1.getHTMLBody();	
}

// PUBLICATION VALIDATION
function publicationValidation() {
	var Messages='';
	if(!emptyValidation(frmPublication, 'Date'))
		Messages+='* The "Date" can not be left blank.\n';	
	if(Messages!=''){
		alert(Messages);	
		return false;
	}	
}

// ADVISOR VALIDATION
function advisorValidation() {
	var Messages='';
	if(!emptyValidation(frmAdvisor, 'Name'))
		Messages+='* The "Name" can not be left blank.\n';	
	if(!emptyValidation(frmAdvisor, 'Address'))
		Messages+='* The "Address" can not be left blank.\n';	
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
}

// EMAIL CHECKING
function emailValidation(frmName, fieldName) {
	var Messages='';
	if(!(frmName[fieldName].value.indexOf('\@') > -1)) {
		// Email must contain '@'
		Messages+='* Invalid "Email".\n';
	}else if(frmName[fieldName].value.substr(frmName[fieldName].value.indexOf('\@')+1,1) == ".") {
		// @ and '.' (dot) in consecutive position!
		Messages+='* Invalid "Email".\n';
	}else if(!(frmName[fieldName].value.indexOf('.') > -1)) {
		// Email must contain '.'
		Messages+='* Invalid "Email".\n';
	}else if(frmName[fieldName].value.length < frmName[fieldName].value.indexOf('.')+3){
		// Email must contain alleast 2 character after '.'
		Messages+='* Invalid "Email".\n';
	}
	return Messages;
}


// DOUBLE INTEGER CHECKING
function isDouble(frmName, fieldName){
	var elmstr = frmName[fieldName].value + "";
	var pattern="^([0-9])*\.{0,1}([0-9])+$";
	if (elmstr == "") 
		return false;
	if(!elmstr.match(pattern)) 
		return false;
	return true;
}

// AMOUNT CHECKING
function amountValidation(frmName, fieldName){
	var Messages='';
	if(!emptyValidation(frmName, fieldName)){
		Messages+='* The "Amount" can not be left blank.\n';	
	}else if(!isDouble(frmName, fieldName)){
		Messages+='* Invalid "Amount".\n';	
	}
	return Messages;
}

// EXPIRY DATE VALIDATION
function expDateValidation(frmName, fieldName) {
	var Messages='';
	if(!emptyValidation(frmName, fieldName))
		Messages+='* The "Expiry Date" can not be left blank.\n';			
	else if(frmName[fieldName].value.length<5)	
		Messages+='* Invalid "Expiry Date".\n';			
	else if(frmName[fieldName].value.indexOf('/')<2)
		Messages+='* Invalid "Expiry Date".\n';
	else if(parseInt(frmName[fieldName].value.substring(0,2))<1 || parseInt(frmName[fieldName].value.substring(0,2))>12)
		Messages+='* Invalid "Expiry Date".\n';
	return Messages;
}

// COMMON DONATION VALIDATION
function donationValidation(frmName) {
	var Messages='';
	if(!emptyValidation(frmName, 'FirstName'))
		Messages+='* The "First Name" can not be left blank.\n';	
	if(!emptyValidation(frmName, 'LastName'))
		Messages+='* The "Last Name" can not be left blank.\n';		
	if(!emptyValidation(frmName, 'Address'))
		Messages+='* The "Address" can not be left blank.\n';		
	if(!emptyValidation(frmName, 'City'))
		Messages+='* The "City" can not be left blank.\n';		
	if(!emptyValidation(frmName, 'State'))
		Messages+='* The "State" can not be left blank.\n';
	if(!emptyValidation(frmName, 'Zip'))
		Messages+='* The "Postal Code" can not be left blank.\n';			
	Messages+=amountValidation(frmName, 'Amount');
	if(!emptyValidation(frmName, 'CreditCard'))
		Messages+='* The "Credit Card" can not be left blank.\n';			
	if(!emptyValidation(frmName, 'CardNum1') || !emptyValidation(frmName, 'CardNum2') || !emptyValidation(frmName, 'CardNum3') || !emptyValidation(frmName, 'CardNum4'))
		Messages+='* The "Card Number" can not be left blank.\n';		
	Messages+=expDateValidation(frmName, 'ExpDate');
	if(!emptyValidation(frmName, 'HolderName'))
		Messages+='* The "Card Holder Name" can not be left blank.\n';			
	return Messages;
}

// COMMON GOLF VALIDATION
function golfValidation(frmName) {
	var Messages='';
	if(!emptyValidation(frmName, 'FirstName'))
		Messages+='* The "First Name" can not be left blank.\n';	
	if(!emptyValidation(frmName, 'LastName'))
		Messages+='* The "Last Name" can not be left blank.\n';		
	if(!emptyValidation(frmName, 'Address'))
		Messages+='* The "Address" can not be left blank.\n';		
	if(!emptyValidation(frmName, 'City'))
		Messages+='* The "City" can not be left blank.\n';		
	if(!emptyValidation(frmName, 'State'))
		Messages+='* The "State" can not be left blank.\n';
	if(!emptyValidation(frmName, 'Zip'))
		Messages+='* The "Postal Code" can not be left blank.\n';			
	if(!emptyValidation(frmName, 'Email')) {
		Messages+='* The "Email" can not be left blank.\n';
	}else{ 
		Messages+=emailValidation(frmName, 'Email');
	}	
	if(!emptyValidation(frmName, 'ISD') || !emptyValidation(frmName, 'STD') || !emptyValidation(frmName, 'Number'))
		Messages+='* The "Telephone" can not be left blank.\n';	
	Messages+=amountValidation(frmName, 'Amount');
	if(!emptyValidation(frmName, 'CreditCard'))
		Messages+='* The "Credit Card" can not be left blank.\n';			
	if(!emptyValidation(frmName, 'CardNum1') || !emptyValidation(frmName, 'CardNum2') || !emptyValidation(frmName, 'CardNum3') || !emptyValidation(frmName, 'CardNum4'))
		Messages+='* The "Card Number" can not be left blank.\n';		
	Messages+=expDateValidation(frmName, 'ExpDate');
	if(!emptyValidation(frmName, 'HolderName'))
		Messages+='* The "Card Holder Name" can not be left blank.\n';			
	return Messages;
}

// ANNUAL WISH LIST VALIDATION
function annWishValidation() {
	var Messages='';
	Messages = donationValidation(frmAnnualWishList);
	if(Messages!=''){
		alert(Messages);
		return false;
	}	
}

// CAPITAL CAMPAIGN VALIDATION
function capCampaignValidation() {
	var Messages='';
	Messages = donationValidation(frmCapitalCampaign);
	if(emptyValidation(frmCapitalCampaign, 'PledgePayment')){
		if(!isDouble(frmCapitalCampaign, 'PledgePayment')) {
			Messages+='* Invalid "Pledge Payment".\n';				
		}
	}
	if(Messages!=''){
		alert(Messages);
		return false;
	}	
}

// A STAR IS BORN VALIDATION
function starisbornValidation() {
	var Messages='';
	Messages = donationValidation(frmStarisborn);
	if(!emptyValidation(frmStarisborn, 'NewbornName'))
		Messages+='* The "Newborn Name" can not be left blank.\n';				
	if(!emptyValidation(frmStarisborn, 'DOB'))
		Messages+='* The "Date of Birth" can not be left blank.\n';						
	if(!emptyValidation(frmStarisborn, 'PFirstName'))
		Messages+='* The "Parent First Name" can not be left blank.\n';	
	if(!emptyValidation(frmStarisborn, 'PLastName'))
		Messages+='* The "Parent Last Name" can not be left blank.\n';		
	if(!emptyValidation(frmStarisborn, 'PAddress'))
		Messages+='* The "Parent Address" can not be left blank.\n';		
	if(!emptyValidation(frmStarisborn, 'PCity'))
		Messages+='* The "Parent City" can not be left blank.\n';		
	if(!emptyValidation(frmStarisborn, 'PState'))
		Messages+='* The "Parent State" can not be left blank.\n';
	if(!emptyValidation(frmStarisborn, 'PZip'))
		Messages+='* The "Parent Postal Code" can not be left blank.\n';	

	if(Messages!=''){
		alert(Messages);
		return false;
	}	
}

// TREES OF HEALTHY WISHES VALIDATION
function treesHealthyWishesValidation() {
	var Messages='';
	Messages = donationValidation(frmTreesHealthyWishes);
	if(Messages!=''){
		alert(Messages);
		return false;
	}	
}

// TREES OF HEALTHY WISHES VALIDATION
function treesHealthyWishesValidation() {
	var Messages='';
	Messages = donationValidation(frmTreesHealthyWishes);
	if(Messages!=''){
		alert(Messages);
		return false;
	}	
}

// GENERAL DONATION VALIDATION
function generalDonationValidation() {
	var Messages='';
	Messages = donationValidation(frmGeneralDonation);
	if(Messages!=''){
		alert(Messages);
		return false;
	}	
}

// HONOUR DONATION VALIDATION
function tributeHonourValidation() {
	var Messages='';
	Messages = donationValidation(frmTributeHonour);
	if(!emptyValidation(frmTributeHonour, 'TFirstName'))
		Messages+='* The "Greets First Name" can not be left blank.\n';	
	if(!emptyValidation(frmTributeHonour, 'TLastName'))
		Messages+='* The "Greets Last Name" can not be left blank.\n';		
	if(!emptyValidation(frmTributeHonour, 'TAddress'))
		Messages+='* The "Greets Address" can not be left blank.\n';		
	if(!emptyValidation(frmTributeHonour, 'TCity'))
		Messages+='* The "Greets City" can not be left blank.\n';		
	if(!emptyValidation(frmTributeHonour, 'TState'))
		Messages+='* The "Greets State" can not be left blank.\n';
	if(!emptyValidation(frmTributeHonour, 'TZip'))
		Messages+='* The "Greets Postal Code" can not be left blank.\n';			
	if(Messages!=''){
		alert(Messages);
		return false;
	}	
}

// MEMORY DONATION VALIDATION
function tributeMemoryValidation() {
	var Messages='';
	Messages = donationValidation(frmTributeMemory);
	if(!emptyValidation(frmTributeMemory, 'TFirstName'))
		Messages+='* The "Greets First Name" can not be left blank.\n';	
	if(!emptyValidation(frmTributeMemory, 'TLastName'))
		Messages+='* The "Greets Last Name" can not be left blank.\n';		
	if(!emptyValidation(frmTributeMemory, 'TAddress'))
		Messages+='* The "Greets Address" can not be left blank.\n';		
	if(!emptyValidation(frmTributeMemory, 'TCity'))
		Messages+='* The "Greets City" can not be left blank.\n';		
	if(!emptyValidation(frmTributeMemory, 'TState'))
		Messages+='* The "Greets State" can not be left blank.\n';
	if(!emptyValidation(frmTributeMemory, 'TZip'))
		Messages+='* The "Greets Postal Code" can not be left blank.\n';
	if(Messages!=''){
		alert(Messages);
		return false;
	}	
}

// GOLF TOURNAMENT SPONSORSHIP VALIDATION
function golfSponValidation() {
	var Messages='';
	Messages = golfValidation(frmGolfSponsorship);
	if(Messages!=''){
		alert(Messages);
		return false;		
	}	
}


// GOLF TOURNAMENT REGISTRATION VALIDATION
function golfRegValidation() {
	var Messages='';
	Messages = golfValidation(frmGolfRegistration);
	if(!emptyValidation(frmGolfRegistration, 'ShoeSize'))
		Messages+='* The "Shoe Size" can not be left blank.\n';	
	if(Messages!=''){
		alert(Messages);
		return false;		
	}	
}

// CATEGORY VALIDATION
function categoryValidation() {
	var Messages='';
	if(!emptyValidation(frmCategory, 'Category'))
		Messages+='* The "Category" can not be left blank.\n';	
	if(Messages!=''){
		alert(Messages);
		return false;		
	}
}

// MEMBER VALIDATION
function memberValidation() {
	var Messages='';
	if(!emptyValidation(frmMember, 'DocID'))
		Messages+='* Please choose "Document".\n';	
	if(!emptyValidation(frmMember, 'Username'))
		Messages+='* The "Username" can not be left blank.\n';	
	if(!emptyValidation(frmMember, 'Password'))
		Messages+='* The "Password" can not be left blank.\n';	
	if(Messages!=''){
		alert(Messages);
		return false;		
	}
}

// MEMBER LOGIN VALIDATION
function memLoginValidate() {
	var Messages='';
	if(!emptyValidation(frmMemberLogin, 'MemUsername'))
		Messages+='* The "Username" can not be left blank.\n';
	if(!emptyValidation(frmMemberLogin, 'MemPassword'))
		Messages+='* The "Password" can not be left blank.\n';
	if(Messages!=''){
		alert(Messages);	
		return false;
	}
}