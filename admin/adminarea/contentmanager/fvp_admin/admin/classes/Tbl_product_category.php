<?php	 		 	
/**
 * Table Definition for tbl_product_category
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_product_category extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'tbl_product_category';            // table name
    var $cat_id;                          // int(11)  not_null primary_key auto_increment
    var $company_id;                      // int(20)  not_null
    var $cat_order_id;                    // int(11)  not_null
    var $cat_name;                        // string(255)  not_null
    var $cat_add_date;                    // date(10)  not_null binary
    var $status_flag;                     // int(11)  not_null

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_product_category',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
