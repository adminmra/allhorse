<?php	 		 	
/**
 * Table Definition for orders_products
 */
require_once 'DB/DataObject.php';

class DataObjects_Orders_products extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'orders_products';                 // table name
    var $orders_products_id;              // int(11)  not_null primary_key auto_increment
    var $orders_id;                       // int(11)  not_null
    var $products_id;                     // int(11)  not_null
    var $products_model;                  // string(12)  
    var $products_name;                   // string(64)  not_null
    var $products_price;                  // real(17)  not_null
    var $final_price;                     // real(17)  not_null
    var $products_tax;                    // real(9)  not_null
    var $products_quantity;               // int(2)  not_null

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Orders_products',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
