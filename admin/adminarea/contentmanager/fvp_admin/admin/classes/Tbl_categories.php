<?php	 		 	
/**
 * Table Definition for tbl_categories
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_categories extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'tbl_categories';                  // table name
    var $id;                              // int(11)  not_null primary_key auto_increment
    var $cat_name;                        // string(64)  not_null
    var $status_flag;                     // int(1)  not_null

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_categories',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
