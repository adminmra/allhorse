<?php	 		 	
/**
 * Table Definition for tbl_customer
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_customer extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'tbl_customer';                    // table name
    var $id;                              // int(5)  not_null primary_key auto_increment
    var $userid;                          // string(50)  not_null
    var $password;                        // string(50)  not_null
    var $firstname;                       // string(80)  not_null
    var $lastname;                        // string(32)  not_null
    var $address;                         // blob(65535)  not_null blob
    var $phone;                           // string(20)  not_null
    var $fax;                             // string(20)  not_null
    var $mobile;                          // string(20)  not_null
    var $email;                           // string(96)  not_null
    var $status;                          // string(1)  not_null

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_customer',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
