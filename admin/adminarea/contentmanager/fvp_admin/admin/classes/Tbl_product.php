<?php	 		 	
/**
 * Table Definition for tbl_product
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_product extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'tbl_product';                     // table name
    var $id;                              // int(5)  not_null primary_key auto_increment
    var $cat_id;                          // int(5)  not_null unsigned
    var $subcat_id;                       // int(5)  not_null
    var $p_name;                          // string(80)  not_null
    var $keywords;                        // blob(65535)  not_null blob
    var $price;                           // string(255)  not_null
    var $intro;                           // blob(65535)  not_null blob
    var $p_full_desc;                     // blob(65535)  not_null blob
    var $image;                           // string(80)  not_null
    var $new_image;                       // string(1)  not_null enum
    var $status;                          // int(1)  not_null

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_product',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
