<?php	 		 	
/**
 * Table Definition for tbl_orders
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_orders extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'tbl_orders';                      // table name
    var $id;                              // int(20)  not_null primary_key auto_increment
    var $orders_id;                       // int(20)  not_null
    var $product_id;                      // int(20)  not_null
    var $quentity;                        // int(10)  not_null
    var $customers_name;                  // string(64)  not_null
    var $customers_street_address;        // string(64)  not_null
    var $customers_town;                  // string(32)  
    var $customers_district;              // string(32)  not_null
    var $customers_postcode;              // string(10)  not_null
    var $customers_state;                 // string(32)  
    var $customers_country;               // string(32)  not_null
    var $customers_daytime_telephone;     // string(32)  not_null
    var $customer_evening_telephone;      // string(32)  not_null
    var $customer_mobile_telephone;       // string(32)  not_null
    var $customers_email_address;         // string(96)  not_null
    var $delivery_name;                   // string(64)  not_null
    var $delivery_street_address;         // string(64)  not_null
    var $delivery_town;                   // string(32)  
    var $delivery_postcode;               // string(10)  not_null
    var $delivery_district;               // string(32)  
    var $delivery_country;                // string(32)  not_null
    var $payment_method;                  // string(32)  not_null
    var $cc_type;                         // string(20)  
    var $cc_owner;                        // string(64)  
    var $cc_number;                       // string(32)  
    var $cc_issue_date;                   // string(32)  not_null
    var $cc_expires;                      // string(4)  
    var $cc_security_number;              // string(80)  not_null
    var $last_modified;                   // datetime(19)  binary
    var $date_purchased;                  // datetime(19)  binary
    var $miscellaneous;                   // blob(65535)  not_null blob
    var $order_total;                     // real(11)  not_null
    var $orders_status;                   // int(5)  not_null
    var $status;                          // int(5)  not_null

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_orders',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
