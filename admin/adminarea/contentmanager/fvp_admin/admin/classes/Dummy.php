<?php	 		 	
/**
 * Table Definition for dummy
 */
require_once 'DB/DataObject.php';

class DataObjects_Dummy extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'dummy';                           // table name
    var $id;                              // int(5)  not_null primary_key auto_increment
    var $desc1;                           // blob(65535)  not_null blob

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Dummy',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
