<?php	 		 	
/**
 * Table Definition for tbl_faq
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_faq extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    var $__table = 'tbl_faq';                         // table name
    var $id;                              // int(11)  not_null primary_key auto_increment
    var $faq_desc;                        // blob(65535)  not_null blob

    /* ZE2 compatibility trick*/
    function __clone() { return $this;}

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_faq',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
