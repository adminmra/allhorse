<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="style1.css" rel="stylesheet" type="text/css">
</head>

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>

    <td>
    {include file="adminheader.tpl"}
    </td>
  
  </tr>
  <tr>
    <td height="31" bgcolor="#000000" style="padding-left:6px; "><img src="images/admin-panel-txt.gif" alt="" title="" width="113" height="31"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF"><img src="images/blank.gif" alt="" title="" width="1" height="1"></td>
  </tr>
  <tr>
    <td height="356" valign="top" bgcolor="#E8EBDE" style="padding-top:50px; "><table width="325" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
          <td class="txt"><font color="RED" face="verdana">{$errMsg}</font></td>
</tr>
<tr><td class="txt">&nbsp;</td></tr>

      <tr>
        <td><table width="141" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/label-left.gif" alt="" title="" width="8" height="30"></td>
            <td width="127" bgcolor="#7F8690" class="label" style=" padding-left:15px; ">Admin Login</td>
            <td><img src="images/label-right.gif" alt="" title="" width="8" height="30"></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><img src="images/blank.gif" alt="" title="" width="1" height="1"></td>
      </tr>
      <tr>
        <td bgcolor="#BBBBBB" style="border-left:1px solid #BBBBBB; border-right:1px solid #BBBBBB; "><img src="images/blank.gif" alt="" title="" width="1" height="3"></td>
      </tr>
      <tr>
        <td height="148" valign="top" bgcolor="#EDEDED" style="border-left:1px solid #BBBBBB; border-right:1px solid #BBBBBB; "><table width="100%"  border="0" cellspacing="0" cellpadding="0">


          <tr>
            <td width="27%" valign="top"><img src="images/log-in-icon.gif" width="92" height="98"></td>
            <td width="73%" valign="top" style="padding-top:36px; ">
			  <table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
             <form name="f1" method="post" action="?"  onSubmit="return go();"> 
			 <tr>
                <td><div align="right" class="fieldname">User Name</div></td>
                <td style="padding-left:12px; ">
                  <input name="userid" type="text" id="userid" style="width:125px; height:18px; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:9px; color:#333399;">
                </td>
              </tr>
              <tr>
                <td colspan="2"><img src="images/blank.gif" alt="" title="" width="1" height="5"></td>
                </tr>
              <tr>
                <td><div align="right" class="fieldname">Password</div></td>
                <td style="padding-left:12px; "><input name="password" type="password" id="password" style="width:125px; height:18px; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:9px; color:#333399;"></td>
              </tr>
              <tr>
                <td colspan="2"><img src="images/blank.gif" alt="" title="" width="1" height="9"></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="right" style="padding-right:8px; "><input name="Submit" type="image" src="images/log-in-button.gif" width="62" height="17"></td>
              </tr></form>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td bgcolor="#BBBBBB" style="border-left:1px solid #BBBBBB; border-right:1px solid #BBBBBB; "><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
  
    <td>
    {include file="adminfooter.tpl"}
    </td>
  
  
  </tr>
</table>
</body>
</html>

{literal}
<script language="Javascript">
function go()
{
	if(document.f1.userid.value == "")
	{
		alert("Please enter an User ID !");
		document.f1.userid.focus();
		return false;
	}

	if(document.f1.password.value == "")
	{
		alert("Please enter the Password !");
		document.f1.password.focus();
		return false;
	}
	return true;

}
</script>
{/literal}