<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="style1.css" rel="stylesheet" type="text/css">
{literal}
<script>
/*function del(text)
{	
	if(!confirm(text))
	{
		return false;
	}
	else
	{
		return true;
	}
}*/

</script>
{/literal}
</head>

<body>
<table width="780" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
	<td height="115">
	<!--header part start -->
	{include file="adminheader.tpl"}
	<!--header part end -->
	</td>
</tr>
<tr>
	<td height="31" bgcolor="#000000" style="padding-left:12px; padding-right:7px; ">
	<table width="100%" height="31"  border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><img src="images/admin-panel-txt.gif" alt="" title="" width="113" height="31"></td>
		<td align="right"><img src="images/log-out.gif" width="87" height="31" border="0" usemap="#Map"></td>
	</tr>
	</table>
	</td>
</tr>
<tr>
	<td bgcolor="#FFFFFF"><img src="images/blank.gif" alt="" title="" width="1" height="1"></td>
</tr>
<tr>
	<td align="left" valign="top" bgcolor="#E8EBDE">
	<table width="100%" height="100%"  border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="150" align="left" valign="top" style=" padding-left:12px; padding-top:30px; padding-bottom:25px; ">{include file="left-menu.tpl"}</td>
		<td width="1" valign="top" background="images/divider.gif"><img src="images/divider.gif" width="1" height="3"></td>
		<td width="558" align="center" valign="top" style="padding-top:50px; padding-bottom:50px; "> 
		<!--body starts here-->
		{if $mode eq "addvideo"}
		<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td>
			<table width="158" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><img src="images/label-left.gif" alt="" title="" width="8" height="30"></td>
				<td width="142" align="left" bgcolor="#7F8690" class="label">Add</td>
				<td><img src="images/label-right.gif" alt="" title="" width="8" height="30"></td>
			</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td><img src="images/blank.gif" alt="" title="" width="1" height="1"></td>
		</tr>
		<tr>
			<td bgcolor="#BBBBBB" style="border-left:1px solid #BBBBBB; border-right:1px solid #BBBBBB; "><img src="images/blank.gif" alt="" title="" width="1" height="3"></td>
		</tr>
		<tr>
			<td height="148" valign="top" bgcolor="#EDEDED" style="border-left:1px solid #BBBBBB; border-right:1px solid #BBBBBB; padding-top:11px; padding-bottom:11px; padding-left:12px; padding-right:12px; ">
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
			</tr>
			<tr>
				<td valign="top">
				<form name="add_videofrm" action="video_manage.php" method="post" enctype="multipart/form-data">
				<table width="100%"  border="0" cellpadding="2" cellspacing="2" bordercolor="1">
				<tr>
					<td colspan="2"><span style="color:red;">{$msg}</span></td>
				</tr>
				<tr>
					<td class="fieldname1" width="25%">Select category: </td>
					<td>
					<select name="catdrop">
					<option value="select">-- Select --</option>
					{section name="ct" loop=$cat_list_drpdwn}
					{if $listvideo.category_id eq $cat_list_drpdwn[ct].cat_id}
						{if $cat_list_drpdwn[ct].parent_id eq 0}
						<option value="{$cat_list_drpdwn[ct].cat_id}" selected>{$cat_list_drpdwn[ct].cat_name}</option>
						{else}
						<option value="{$cat_list_drpdwn[ct].cat_id}" selected>&nbsp;&nbsp;&nbsp;&nbsp;{$cat_list_drpdwn[ct].cat_name}</option>
						{/if}
					{else}
						{if $cat_list_drpdwn[ct].parent_id eq 0}
						<option value="{$cat_list_drpdwn[ct].cat_id}">{$cat_list_drpdwn[ct].cat_name}</option>
						{else}
						<option value="{$cat_list_drpdwn[ct].cat_id}">&nbsp;&nbsp;&nbsp;&nbsp;{$cat_list_drpdwn[ct].cat_name}</option>
						{/if}
					{/if}
					{/section}
					</select>
					</td>
				</tr>
				<tr> 
					<td class="fieldname1" width="25%">Label : </td>
					<td><input type="text" name="txt_label" class="field" value="{$listvideo.label}"></td>
				</tr>
				<tr> 
					<td class="fieldname1" width="25%">Upload Video : </td>
					<td><input type="file" name="file_image" ></td>
				</tr>
				<tr> 
					<td class="fieldname1" width="25%">Upload Thumbnail : </td>
					<td><input type="file" name="file_thumb" ></td>
				</tr>
				<tr> 
					<td class="fieldname1" width="25%">Description : </td>
					<td><input type="text" name="txt_info" class="field" value="{$listvideo.info}"></td>
				</tr>
				<tr> 
					<td class="fieldname1" width="25%">Value : </td>
					<td><input type="text" name="txt_value" class="field" value="{$listvideo.value}"></td>
				</tr>
				<tr> 
					<td class="fieldname1" width="25%">All Horse : </td>
					<td><input type="checkbox" name="chk_player1"{ if $listvideo.player1 eq "1" } checked="checked" {/if}></td>
				</tr>
				<tr> 
					<td class="fieldname1" width="25%">Go Horse Betting : </td>
					<td><input type="checkbox" name="chk_player2"{ if $listvideo.player2 eq "1" } checked="checked" {/if}></td>
				</tr>
				<tr> 
					<td class="fieldname1" width="25%">HorseBetting.com : </td>
					<td><input type="checkbox" name="chk_player3"{ if $listvideo.player3 eq "1" } checked="checked" {/if}></td>
				</tr>
				<tr> 
					<td class="fieldname1" width="25%">&nbsp; </td>
					<td>
					{if $modetype neq "edit"}
					<input type="submit" name="submit_video" value=" Add ">
					{else}
					<input type="hidden" name="id" value="{$listvideo.id}">
					<input type="submit" name="update_video" value=" Update ">
					{/if}
					</td>
				</tr>
				</table>
				</form>
				</td>
			</tr>
			<tr>
				<td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
			</tr>
			</table>
			</td>
		</tr>
		</table>
		{/if}
		{if $mode eq "listvideo"}
		<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td>
			<table width="158" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><img src="images/label-left.gif" alt="" title="" width="8" height="30"></td>
				<td width="142" align="left" bgcolor="#7F8690" class="label">{$bread}</td>
				<td><img src="images/label-right.gif" alt="" title="" width="8" height="30"></td>
			</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td><img src="images/blank.gif" alt="" title="" width="1" height="1"></td>
		</tr>
		<tr>
			<td bgcolor="#BBBBBB" style="border-left:1px solid #BBBBBB; border-right:1px solid #BBBBBB; "><img src="images/blank.gif" alt="" title="" width="1" height="3"></td>
		</tr>
		<tr>
			<td height="148" valign="top" bgcolor="#EDEDED" style="border-left:1px solid #BBBBBB; border-right:1px solid #BBBBBB; padding-top:11px; padding-bottom:11px; padding-left:12px; padding-right:12px; ">
			<table width="100%"  border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
			</tr>
			<tr>
				<td><span style="color:red;">{$genmsg}</span></td>
			</tr>
			<tr>
				<td valign="top">
				<table width="100%"  border="0" cellpadding="2" cellspacing="2" bordercolor="1">
				<tr> 
					<td width="20%" class="label1" style="border:1px solid #D9D7D7; "><div align="center">Label </div></td>
					<td  class="label1" style="border:1px solid #D9D7D7; "><div align="center">Info</div></td>
					<td width="10%" class="label1" style="border:1px solid #D9D7D7; "><div align="center">Action</div></td>
				</tr>
				{if $emptyvideo neq "YES"}
				{section name="lst" loop=$video_list}
				<tr> 
					<td class="txt1" style="border:1px solid #D9D7D7; "><div align="center">{$video_list[lst].label}</div></td>
					<td class="txt1" style="border:1px solid #D9D7D7; "><div align="center">{$video_list[lst].info}</div></td>
					<td style="border:1px solid #D9D7D7; ">
					<table width="90%"  border="0" align="center" cellpadding="0" cellspacing="0">
					<tr> 
						<td align="center"><a href="video_manage.php?mode=edit&id={$video_list[lst].id}"><img src="images/edit.gif" alt="Edit" title="Edit" width="16" height="16" border="0"></a></td>
						<td align="center"><a href="video_manage.php?mode=delete&id={$video_list[lst].id}" onClick="return del('Do you really want to delete the video?');"><img src="images/delete.gif" title="Delete" width="16" height="16" border="0"></a></td>
					</tr>
					</table>
					</td>
				</tr>
				{/section}
				{else}
				<tr>
					<td colspan="3" align="center"><span style="color:red;">Sorry!! No video is available.</span></td>
				</tr>
				{/if}
				</table>
				</td>
			</tr>
			<tr>
				<td valign="top"><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
			</tr>
			<tr>
				<td valign="top">
				<form name="genxmlfrm" method="post" action="video_manage.php">
				<table width="45%"  border="0" cellspacing="0" cellpadding="0">
				<tr>					
					<td width="29%"><input type="submit" name="generate_xml" value="Generate XML"></td>
				</tr>
				</table>
				</td>
			</tr>			
			</table>
			<!--<br>{include file="paging.tpl"}-->
			</td>
		</tr>
		<tr>
			<td bgcolor="#BBBBBB" style="border-left:1px solid #BBBBBB; border-right:1px solid #BBBBBB; "><img src="images/blank.gif" alt="" title="" width="1" height="4"></td>
		</tr>
		</table>
		{/if}
		<!--body ends here-->
		</td>
	</tr>
	</table>
	</td>
</tr>
<tr>
	<td height="56">
	<!--footer part start -->
	{include file="adminfooter.tpl"}
	<!--footer part end -->
	</td>
</tr>
</table>

<map name="Map">
<area shape="rect" coords="0,9,69,25" href="logout.php">
</map>
<map name="Map2">
<area shape="rect" coords="22,9,68,83" href="#">
</map>
<map name="Map3">
<area shape="rect" coords="23,9,68,83" href="#">
</map>
<map name="Map4">
<area shape="rect" coords="24,12,67,83" href="#">
</map>
<map name="Map5">
<area shape="rect" coords="20,11,68,83" href="#">
</map>
<map name="Map6">
<area shape="rect" coords="25,10,71,82" href="#">
</map>
<map name="Map7">
<area shape="rect" coords="11,8,78,82" href="#">
</map>

</body>
</html>

