<?php	 		 	
/**
 * Table Definition for node_comment_statistics
 */
require_once 'DB/DataObject.php';

class DataObjects_Node_comment_statistics extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'node_comment_statistics';         // table name
    public $nid;                             // int(10)  not_null primary_key unsigned
    public $last_comment_timestamp;          // int(11)  not_null multiple_key
    public $last_comment_name;               // string(60)  
    public $last_comment_uid;                // int(11)  not_null
    public $comment_count;                   // int(10)  not_null unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Node_comment_statistics',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
