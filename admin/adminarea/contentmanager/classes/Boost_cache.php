<?php	 		 	
/**
 * Table Definition for boost_cache
 */
require_once 'DB/DataObject.php';

class DataObjects_Boost_cache extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'boost_cache';                     // table name
    public $hash;                            // string(32)  not_null primary_key
    public $filename;                        // blob(65535)  not_null blob
    public $base_dir;                        // string(128)  not_null multiple_key
    public $expire;                          // int(10)  not_null multiple_key unsigned
    public $lifetime;                        // int(11)  not_null
    public $push;                            // int(6)  not_null multiple_key
    public $page_callback;                   // string(255)  not_null multiple_key
    public $page_type;                       // string(255)  not_null multiple_key
    public $page_id;                         // string(64)  not_null multiple_key
    public $extension;                       // string(8)  not_null multiple_key
    public $timer;                           // int(10)  not_null multiple_key unsigned
    public $timer_average;                   // real(12)  not_null multiple_key
    public $hash_url;                        // string(32)  not_null
    public $url;                             // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Boost_cache',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
