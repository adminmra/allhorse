<?php	 		 	
/**
 * Table Definition for ad_clicks
 */
require_once 'DB/DataObject.php';

class DataObjects_Ad_clicks extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ad_clicks';                       // table name
    public $cid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $aid;                             // int(10)  not_null multiple_key unsigned
    public $uid;                             // int(10)  not_null unsigned
    public $status;                          // int(3)  not_null multiple_key unsigned
    public $hostname;                        // string(128)  not_null multiple_key
    public $user_agent;                      // string(255)  not_null multiple_key
    public $adgroup;                         // string(255)  not_null multiple_key
    public $hostid;                          // string(32)  not_null multiple_key
    public $url;                             // string(255)  multiple_key
    public $timestamp;                       // int(10)  not_null unsigned
    public $extra;                           // string(255)  not_null multiple_key

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ad_clicks',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
