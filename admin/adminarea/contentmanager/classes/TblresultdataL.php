<?php	 		 	
/**
 * Table Definition for tblresultdataL
 */
require_once 'DB/DataObject.php';

class DataObjects_TblresultdataL extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tblresultdataL';                  // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $duration;                        // string(100)  not_null
    public $favorite;                        // string(100)  not_null
    public $finisher;                        // string(100)  not_null
    public $perform_id;                      // int(11)  not_null
    public $cotestid;                        // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_TblresultdataL',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
