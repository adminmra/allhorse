<?php	 		 	
/**
 * Table Definition for views_object_cache
 */
require_once 'DB/DataObject.php';

class DataObjects_Views_object_cache extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'views_object_cache';              // table name
    public $sid;                             // string(64)  multiple_key
    public $name;                            // string(32)  
    public $obj;                             // string(32)  
    public $updated;                         // int(10)  not_null multiple_key unsigned
    public $data;                            // blob(4294967295)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Views_object_cache',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
