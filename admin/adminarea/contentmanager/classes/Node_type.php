<?php	 		 	
/**
 * Table Definition for node_type
 */
require_once 'DB/DataObject.php';

class DataObjects_Node_type extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'node_type';                       // table name
    public $type;                            // string(32)  not_null primary_key
    public $name;                            // string(255)  not_null
    public $module;                          // string(255)  not_null
    public $description;                     // blob(16777215)  not_null blob
    public $help;                            // blob(16777215)  not_null blob
    public $has_title;                       // int(3)  not_null unsigned
    public $title_label;                     // string(255)  not_null
    public $has_body;                        // int(3)  not_null unsigned
    public $body_label;                      // string(255)  not_null
    public $min_word_count;                  // int(5)  not_null unsigned
    public $custom;                          // int(4)  not_null
    public $modified;                        // int(4)  not_null
    public $locked;                          // int(4)  not_null
    public $orig_type;                       // string(255)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Node_type',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
