<?php	 		 	
/**
 * Table Definition for ntra
 */
require_once 'DB/DataObject.php';

class DataObjects_Ntra extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ntra';                            // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $URL;                             // string(255)  
    public $Race_Date;                       // string(255)  
    public $Track_Name;                      // string(255)  
    public $Location;                        // string(255)  
    public $Entry_draw;                      // string(255)  
    public $Scratch;                         // string(255)  
    public $Time_Zone;                       // string(255)  
    public $Race1;                           // string(255)  
    public $Race2;                           // string(255)  
    public $Race3;                           // string(255)  
    public $Race4;                           // string(255)  
    public $Race5;                           // string(255)  
    public $Race6;                           // string(255)  
    public $Race7;                           // string(255)  
    public $Race8;                           // string(255)  
    public $Race9;                           // string(255)  
    public $Race10;                          // string(255)  
    public $Race11;                          // string(255)  
    public $Race12;                          // string(255)  
    public $Race13;                          // string(255)  
    public $Race14;                          // string(255)  
    public $Race15;                          // string(255)  
    public $Stakes;                          // string(255)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ntra',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
