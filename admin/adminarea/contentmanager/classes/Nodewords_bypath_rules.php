<?php	 		 	
/**
 * Table Definition for nodewords_bypath_rules
 */
require_once 'DB/DataObject.php';

class DataObjects_Nodewords_bypath_rules extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'nodewords_bypath_rules';          // table name
    public $id;                              // int(10)  not_null primary_key unsigned auto_increment
    public $name;                            // string(128)  not_null
    public $type;                            // int(4)  not_null
    public $path_expr;                       // string(255)  not_null
    public $weight;                          // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Nodewords_bypath_rules',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
