<?php	 		 	
/**
 * Table Definition for vocabulary
 */
require_once 'DB/DataObject.php';

class DataObjects_Vocabulary extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'vocabulary';                      // table name
    public $vid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $name;                            // string(255)  not_null
    public $description;                     // blob(4294967295)  blob
    public $help;                            // string(255)  not_null
    public $relations;                       // int(3)  not_null unsigned
    public $hierarchy;                       // int(3)  not_null unsigned
    public $multiple;                        // int(3)  not_null unsigned
    public $required;                        // int(3)  not_null unsigned
    public $tags;                            // int(3)  not_null unsigned
    public $module;                          // string(255)  not_null
    public $weight;                          // int(4)  not_null multiple_key

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Vocabulary',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
