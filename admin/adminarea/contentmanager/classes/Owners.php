<?php	 		 	
/**
 * Table Definition for owners
 */
require_once 'DB/DataObject.php';

class DataObjects_Owners extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'owners';                          // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $name;                            // string(50)  not_null
    public $city;                            // string(50)  
    public $state;                           // string(50)  
    public $country;                         // string(50)  
    public $notes;                           // blob(4294967295)  blob
    public $stable;                          // string(50)  
    public $family;                          // blob(4294967295)  blob
    public $url;                             // blob(4294967295)  blob
    public $pic;                             // string(100)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Owners',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
