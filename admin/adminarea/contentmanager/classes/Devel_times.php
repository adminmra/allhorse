<?php	 		 	
/**
 * Table Definition for devel_times
 */
require_once 'DB/DataObject.php';

class DataObjects_Devel_times extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'devel_times';                     // table name
    public $tid;                             // int(11)  not_null primary_key auto_increment
    public $qid;                             // int(11)  not_null multiple_key
    public $time;                            // real(12)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Devel_times',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
