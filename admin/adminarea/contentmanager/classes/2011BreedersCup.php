<?php	 		 	
/**
 * Table Definition for 2011BreedersCup
 */
require_once 'DB/DataObject.php';

class DataObjects_2011BreedersCup extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = '2011BreedersCup';                 // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $name;                            // string(150)  not_null
    public $age_weight;                      // string(60)  not_null
    public $purse;                           // string(60)  not_null
    public $distance;                        // string(60)  not_null
    public $turf;                            // string(60)  not_null
    public $surface;                         // string(60)  not_null
    public $post;                            // string(60)  not_null
    public $grade;                           // string(60)  not_null
    public $racedate;                        // date(10)  binary
    public $arrorder;                        // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_2011BreedersCup',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
