<?php	 		 	
/**
 * Table Definition for main_sports
 */
require_once 'DB/DataObject.php';

class DataObjects_Main_sports extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'main_sports';                     // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $web_id;                          // int(11)  not_null
    public $name;                            // string(80)  not_null
    public $image;                           // string(100)  not_null
    public $own_html;                        // blob(4294967295)  not_null blob
    public $color;                           // string(50)  not_null
    public $backcolor;                       // string(30)  not_null
    public $arrange;                         // int(11)  not_null
    public $column_no;                       // int(11)  not_null
    public $created_on;                      // datetime(19)  not_null binary
    public $start_date;                      // date(10)  not_null binary
    public $stop_date;                       // date(10)  not_null binary
    public $status;                          // string(1)  not_null
    public $add_anchor;                      // string(20)  not_null
    public $img_id;                          // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Main_sports',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
