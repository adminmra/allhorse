<?php	 		 	
/**
 * Table Definition for backup_migrate_profiles
 */
require_once 'DB/DataObject.php';

class DataObjects_Backup_migrate_profiles extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'backup_migrate_profiles';         // table name
    public $profile_id;                      // string(32)  not_null primary_key
    public $name;                            // string(255)  not_null
    public $filename;                        // string(50)  not_null
    public $append_timestamp;                // int(3)  not_null unsigned
    public $timestamp_format;                // string(14)  not_null
    public $filters;                         // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Backup_migrate_profiles',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
