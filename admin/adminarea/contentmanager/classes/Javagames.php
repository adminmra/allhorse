<?php	 		 	
/**
 * Table Definition for javagames
 */
require_once 'DB/DataObject.php';

class DataObjects_Javagames extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'javagames';                       // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $name;                            // blob(16777215)  not_null blob
    public $type;                            // blob(16777215)  not_null blob
    public $var;                             // blob(16777215)  not_null blob
    public $sub;                             // blob(16777215)  not_null blob
    public $desc;                            // blob(16777215)  not_null blob
    public $kind;                            // blob(16777215)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Javagames',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
