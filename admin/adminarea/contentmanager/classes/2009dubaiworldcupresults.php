<?php	 		 	
/**
 * Table Definition for 2009dubaiworldcupresults
 */
require_once 'DB/DataObject.php';

class DataObjects_2009dubaiworldcupresults extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = '2009dubaiworldcupresults';        // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $race_id;                         // int(11)  not_null
    public $horse;                           // string(75)  not_null
    public $trainer;                         // string(75)  not_null
    public $country;                         // string(75)  not_null
    public $jockey;                          // string(75)  not_null
    public $distance;                        // real(8)  not_null
    public $age_weight;                      // string(50)  not_null
    public $post;                            // int(11)  not_null
    public $result;                          // int(11)  not_null
    public $time1;                           // string(100)  not_null
    public $odds;                            // string(100)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_2009dubaiworldcupresults',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
