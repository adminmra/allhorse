<?php	 		 	
/**
 * Table Definition for promotiondemo
 */
require_once 'DB/DataObject.php';

class DataObjects_Promotiondemo extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'promotiondemo';                   // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $additionallink;                  // blob(65535)  not_null blob
    public $promotion;                       // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Promotiondemo',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
