<?php	 		 	
/**
 * Table Definition for authmap
 */
require_once 'DB/DataObject.php';

class DataObjects_Authmap extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'authmap';                         // table name
    public $aid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $uid;                             // int(11)  not_null
    public $authname;                        // string(128)  not_null unique_key
    public $module;                          // string(128)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Authmap',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
