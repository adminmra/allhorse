<?php	 		 	
/**
 * Table Definition for search_node_links
 */
require_once 'DB/DataObject.php';

class DataObjects_Search_node_links extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'search_node_links';               // table name
    public $sid;                             // int(10)  not_null primary_key unsigned
    public $type;                            // string(16)  not_null primary_key
    public $nid;                             // int(10)  not_null primary_key multiple_key unsigned
    public $caption;                         // blob(4294967295)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Search_node_links',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
