<?php	 		 	
/**
 * Table Definition for ad_permissions
 */
require_once 'DB/DataObject.php';

class DataObjects_Ad_permissions extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ad_permissions';                  // table name
    public $oid;                             // int(10)  not_null primary_key unsigned
    public $permissions;                     // blob(4294967295)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ad_permissions',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
