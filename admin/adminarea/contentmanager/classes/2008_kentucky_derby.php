<?php	 		 	
/**
 * Table Definition for 2008_kentucky_derby
 */
require_once 'DB/DataObject.php';

class DataObjects_2008_kentucky_derby extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = '2008_kentucky_derby';             // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $dat;                             // string(50)  not_null
    public $race;                            // string(50)  not_null
    public $track;                           // string(50)  not_null
    public $grade;                           // string(50)  not_null
    public $dist;                            // string(50)  not_null
    public $purse;                           // string(50)  not_null
    public $earnings;                        // string(100)  not_null
    public $breeder;                         // string(100)  not_null
    public $odd;                             // string(25)  not_null
    public $resulttime;                      // string(50)  not_null
    public $result;                          // int(11)  
    public $post;                            // int(11)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_2008_kentucky_derby',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
