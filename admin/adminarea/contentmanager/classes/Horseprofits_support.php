<?php	 		 	
/**
 * Table Definition for horseprofits_support
 */
require_once 'DB/DataObject.php';

class DataObjects_Horseprofits_support extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'horseprofits_support';            // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $name;                            // string(50)  not_null
    public $email;                           // string(50)  not_null
    public $affiliatelogin;                  // string(50)  not_null
    public $comment;                         // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Horseprofits_support',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
