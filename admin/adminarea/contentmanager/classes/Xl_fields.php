<?php	 		 	
/**
 * Table Definition for xl_fields
 */
require_once 'DB/DataObject.php';

class DataObjects_Xl_fields extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'xl_fields';                       // table name
    public $CONTENDER;                       // string(250)  
    public $Trainer;                         // string(250)  
    public $Sire;                            // string(250)  
    public $Jockey;                          // string(250)  
    public $Triple Crown nominee;            // string(250)  
    public $Dosage index;                    // string(250)  
    public $Center of distribution;          // string(250)  
    public $Raise a Native in male line;     // string(250)  
    public $Highest dosage points in "Classic" wing;    // string(250)  
    public $Experimental free handicap;      // string(250)  
    public $Dual qualifier;                  // string(250)  
    public $Trainer has prior Derby horses;    // string(250)  
    public $Raced as a 2-year-old;           // string(250)  
    public $Won as a 2-year-old;             // string(250)  
    public $Raced a mile or more as a 2-year-old;    // string(250)  
    public $Won at a mile or more as a 2-year-old;    // string(250)  
    public $Top BRIS 2-year-old Speed Rating;    // string(250)  
    public $Top Beyer 2-year-old Speed Figure;    // string(250)  
    public  or higher Beyer as a 2-year-old;    // string(250)  
    public 
-year-old performance rate;     // string(250)  
    public $Three to seven races as a 2-year-old;    // string(250)  
    public $Stakes win as a 2-year-old;      // string(250)  
    public $Won in first three starts;       // string(250)  
    public $"Sharp" race at Churchill Downs;    // string(250)  
    public $Raced at a Kentucky track;       // string(250)  
    public $Raced at 1 1/8 miles;            // string(250)  
    public $Won or "sharp" in 1 1/8-mile race;    // string(250)  
    public $Won as a 3-year-old;             // string(250)  
    public $Stakes win as a 3-year-old;      // string(250)  
    public $Three or more races as a 3-year-old;    // string(250)  
    public $Six or more career starts;       // string(250)  
    public $Faced field size of 10 or more;    // string(250)  
    public $Top 3-year-old Beyer Speed Figure;    // string(250)  
    public 5 or higher Beyer as a 3-year-old;    // string(250)  
    public $Two 100 or higher BRIS Speed Ratings;    // string(250)  
    public $Top BRIS Speed Rating;           // string(250)  
    public 4 or higher BRIS speed rating;    // string(250)  
    public $Best Performance Figure;         // string(250)  
    public $Last Performance Figure;         // string(250)  
    public $First or second in key prep;     // string(250)  
    public $"Sharp" race in key prep;        // string(250)  
    public $Gained in stretch in key prep;    // string(250)  
    public $In-the-money in final prep;      // string(250)  
    public $"Sharp" race in final prep;      // string(250)  
    public $Earned or matched highest Beyer in one of final two preps;    // string(250)  
    public $Final time in last 1 1/8-mile prep;    // string(250)  
    public $Final 1/8 in last 1 1/8-mile prep;    // string(250)  
    public $Final 3/8s in last 1 1/8-mile prep;    // string(250)  
    public $Final quarter-mile in last 1 1/8-mile prep;    // string(250)  
    public $Final prep 21-28 days prior;     // string(250)  
    public $Lowest prior three-race total of finishing positions;    // string(250)  
    public $Proven ability to handle traffic;    // string(250)  
    public $Possesses stalking or closing style;    // string(250)  
    public $BRIS final prep late pace rating;    // string(250)  
    public $BRIS final prep speed rating;    // string(250)  
    public $BRIS racing speed average;       // string(250)  
    public $Jockey has prior Derby mount;    // string(250)  
    public $Jockey rode horse in a previous race;    // string(250)  
    public $Two works at Churchill;          // string(250)  
    public $Workout of five furlongs or longer at Churchill;    // string(250)  
    public $"Bullet" work at Churchill before Derby;    // string(250)  
    public $Post position 1-12;              // string(250)  
    public $id;                              // int(10)  not_null primary_key unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Xl_fields',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
