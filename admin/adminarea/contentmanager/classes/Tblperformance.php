<?php	 		 	
/**
 * Table Definition for tblperformance
 */
require_once 'DB/DataObject.php';

class DataObjects_Tblperformance extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tblperformance';                  // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $performance_id;                  // string(10)  not_null
    public $date;                            // date(10)  not_null binary
    public $description;                     // string(100)  not_null
    public $flag;                            // string(100)  not_null
    public $host;                            // string(50)  not_null
    public $total_contest;                   // int(11)  not_null
    public $contestno;                       // int(11)  not_null
    public $posttime;                        // string(50)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tblperformance',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
