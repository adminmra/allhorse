<?php	 		 	
/**
 * Table Definition for tbl_video
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_video extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tbl_video';                       // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $category_id;                     // int(11)  not_null
    public $label;                           // string(250)  not_null
    public $image;                           // string(250)  not_null
    public $thmb;                            // string(250)  not_null
    public $info;                            // string(250)  not_null
    public $valu;                            // string(100)  not_null
    public $player1;                         // int(1)  not_null
    public $player2;                         // int(1)  not_null
    public $player3;                         // int(1)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_video',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
