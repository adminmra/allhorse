<?php	 		 	
/**
 * Table Definition for page_url
 */
require_once 'DB/DataObject.php';

class DataObjects_Page_url extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'page_url';                        // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $pagename;                        // string(150)  not_null
    public $page;                            // string(75)  not_null
    public $entity_id;                       // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Page_url',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
