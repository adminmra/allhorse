<?php	 		 	
/**
 * Table Definition for greg_horsestowatch
 */
require_once 'DB/DataObject.php';

class DataObjects_Greg_horsestowatch extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'greg_horsestowatch';              // table name
    public $story_id;                        // int(11)  not_null primary_key auto_increment
    public $story_title;                     // blob(16777215)  not_null blob
    public $story_content;                   // blob(16777215)  not_null blob
    public $story_posted;                    // datetime(19)  not_null binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Greg_horsestowatch',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
