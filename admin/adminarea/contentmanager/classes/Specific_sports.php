<?php	 		 	
/**
 * Table Definition for specific_sports
 */
require_once 'DB/DataObject.php';

class DataObjects_Specific_sports extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'specific_sports';                 // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $main_id;                         // int(11)  not_null
    public $name;                            // string(60)  not_null
    public $image;                           // string(100)  not_null
    public $color;                           // string(50)  not_null
    public $backcolor;                       // string(30)  not_null
    public $arrange;                         // int(11)  not_null
    public $column_no;                       // int(11)  not_null
    public $created_on;                      // datetime(19)  not_null binary
    public $status;                          // string(1)  not_null
    public $lines_status;                    // string(1)  not_null
    public $start_date;                      // date(10)  not_null binary
    public $stop_date;                       // date(10)  not_null binary
    public $add_anchor;                      // string(20)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Specific_sports',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
