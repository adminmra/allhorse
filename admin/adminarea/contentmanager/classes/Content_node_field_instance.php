<?php	 		 	
/**
 * Table Definition for content_node_field_instance
 */
require_once 'DB/DataObject.php';

class DataObjects_Content_node_field_instance extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'content_node_field_instance';     // table name
    public $field_name;                      // string(32)  not_null primary_key
    public $type_name;                       // string(32)  not_null primary_key
    public $weight;                          // int(11)  not_null
    public $label;                           // string(255)  not_null
    public $widget_type;                     // string(32)  not_null
    public $widget_settings;                 // blob(16777215)  not_null blob
    public $display_settings;                // blob(16777215)  not_null blob
    public $description;                     // blob(16777215)  not_null blob
    public $widget_module;                   // string(127)  not_null
    public $widget_active;                   // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Content_node_field_instance',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
