<?php	 		 	
/**
 * Table Definition for sportlinesdemo
 */
require_once 'DB/DataObject.php';

class DataObjects_Sportlinesdemo extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'sportlinesdemo';                  // table name
    public $linesid;                         // int(11)  not_null primary_key auto_increment
    public $name;                            // string(75)  not_null
    public $main_id;                         // int(11)  not_null
    public $active;                          // string(1)  not_null enum
    public $position;                        // int(4)  not_null
    public $mark;                            // string(1)  not_null enum
    public $sportid;                         // string(25)  not_null
    public $notes;                           // blob(65535)  not_null blob
    public $addedon;                         // date(10)  not_null binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Sportlinesdemo',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
