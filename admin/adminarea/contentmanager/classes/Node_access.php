<?php	 		 	
/**
 * Table Definition for node_access
 */
require_once 'DB/DataObject.php';

class DataObjects_Node_access extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'node_access';                     // table name
    public $nid;                             // int(10)  not_null primary_key unsigned
    public $gid;                             // int(10)  not_null primary_key unsigned
    public $realm;                           // string(255)  not_null primary_key
    public $grant_view;                      // int(3)  not_null unsigned
    public $grant_update;                    // int(3)  not_null unsigned
    public $grant_delete;                    // int(3)  not_null unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Node_access',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
