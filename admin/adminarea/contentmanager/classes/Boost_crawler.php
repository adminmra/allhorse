<?php	 		 	
/**
 * Table Definition for boost_crawler
 */
require_once 'DB/DataObject.php';

class DataObjects_Boost_crawler extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'boost_crawler';                   // table name
    public $id;                              // int(10)  not_null primary_key unsigned auto_increment
    public $hash;                            // string(32)  not_null unique_key
    public $url;                             // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Boost_crawler',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
