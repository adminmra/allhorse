<?php	 		 	
/**
 * Table Definition for themekey_properties
 */
require_once 'DB/DataObject.php';

class DataObjects_Themekey_properties extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'themekey_properties';             // table name
    public $id;                              // int(10)  not_null primary_key unsigned auto_increment
    public $property;                        // string(255)  not_null multiple_key
    public $value;                           // string(255)  not_null
    public $weight;                          // int(11)  not_null multiple_key
    public $conditions;                      // blob(4294967295)  not_null blob
    public $theme;                           // string(255)  not_null
    public $callbacks;                       // blob(4294967295)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Themekey_properties',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
