<?php	 		 	
/**
 * Table Definition for create_message
 */
require_once 'DB/DataObject.php';

class DataObjects_Create_message extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'create_message';                  // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $web_name;                        // string(100)  not_null
    public $default_msg;                     // blob(65535)  not_null blob
    public $user_conf_msg;                   // blob(65535)  not_null blob
    public $start_time;                      // datetime(19)  not_null binary
    public $stop_time;                       // datetime(19)  not_null binary
    public $created_on;                      // datetime(19)  not_null binary
    public $status;                          // string(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Create_message',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
