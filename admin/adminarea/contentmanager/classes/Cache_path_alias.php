<?php	 		 	
/**
 * Table Definition for cache_path_alias
 */
require_once 'DB/DataObject.php';

class DataObjects_Cache_path_alias extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'cache_path_alias';                // table name
    public $cid;                             // string(255)  not_null primary_key
    public $data;                            // blob(4294967295)  blob binary
    public $expire;                          // int(11)  not_null multiple_key
    public $created;                         // int(11)  not_null
    public $headers;                         // blob(65535)  blob
    public $serialized;                      // int(6)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Cache_path_alias',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
