<?php	 		 	
/**
 * Table Definition for accesslog
 */
require_once 'DB/DataObject.php';

class DataObjects_Accesslog extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'accesslog';                       // table name
    public $aid;                             // int(11)  not_null primary_key auto_increment
    public $sid;                             // string(64)  not_null
    public $title;                           // string(255)  
    public $path;                            // string(255)  
    public $url;                             // blob(65535)  blob
    public $hostname;                        // string(128)  
    public $uid;                             // int(10)  multiple_key unsigned
    public $timer;                           // int(10)  not_null unsigned
    public $timestamp;                       // int(10)  not_null multiple_key unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Accesslog',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
