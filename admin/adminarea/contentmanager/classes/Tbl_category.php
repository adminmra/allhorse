<?php	 		 	
/**
 * Table Definition for tbl_category
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_category extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tbl_category';                    // table name
    public $cat_id;                          // int(11)  not_null primary_key auto_increment
    public $parent_id;                       // int(11)  not_null
    public $category_name;                   // string(250)  not_null
    public $cat_add_date;                    // date(10)  not_null binary
    public $status;                          // int(2)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_category',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
