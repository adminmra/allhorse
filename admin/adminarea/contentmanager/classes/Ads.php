<?php	 		 	
/**
 * Table Definition for ads
 */
require_once 'DB/DataObject.php';

class DataObjects_Ads extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ads';                             // table name
    public $aid;                             // int(10)  not_null primary_key unsigned
    public $uid;                             // int(10)  not_null multiple_key unsigned
    public $adstatus;                        // string(255)  not_null
    public $adtype;                          // string(255)  not_null
    public $redirect;                        // string(255)  not_null
    public $autoactivate;                    // int(10)  not_null multiple_key unsigned
    public $autoactivated;                   // int(10)  not_null unsigned
    public $autoexpire;                      // int(10)  not_null unsigned
    public $autoexpired;                     // int(10)  not_null unsigned
    public $activated;                       // int(10)  not_null unsigned
    public $maxviews;                        // int(10)  not_null unsigned
    public $maxclicks;                       // int(10)  not_null unsigned
    public $expired;                         // int(10)  not_null unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ads',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
