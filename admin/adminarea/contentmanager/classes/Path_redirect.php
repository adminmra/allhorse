<?php	 		 	
/**
 * Table Definition for path_redirect
 */
require_once 'DB/DataObject.php';

class DataObjects_Path_redirect extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'path_redirect';                   // table name
    public $rid;                             // int(11)  not_null primary_key auto_increment
    public $path;                            // string(255)  not_null multiple_key
    public $redirect;                        // string(255)  not_null
    public $query;                           // string(255)  
    public $fragment;                        // string(50)  
    public $language;                        // string(12)  not_null
    public $type;                            // int(6)  not_null
    public $last_used;                       // int(10)  not_null unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Path_redirect',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
