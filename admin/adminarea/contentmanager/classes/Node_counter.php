<?php	 		 	
/**
 * Table Definition for node_counter
 */
require_once 'DB/DataObject.php';

class DataObjects_Node_counter extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'node_counter';                    // table name
    public $nid;                             // int(11)  not_null primary_key
    public $totalcount;                      // int(20)  not_null unsigned
    public $daycount;                        // int(8)  not_null unsigned
    public $timestamp;                       // int(10)  not_null unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Node_counter',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
