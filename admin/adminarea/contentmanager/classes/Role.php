<?php	 		 	
/**
 * Table Definition for role
 */
require_once 'DB/DataObject.php';

class DataObjects_Role extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'role';                            // table name
    public $rid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $name;                            // string(64)  not_null unique_key

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Role',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
