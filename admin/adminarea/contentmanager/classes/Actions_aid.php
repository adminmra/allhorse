<?php	 		 	
/**
 * Table Definition for actions_aid
 */
require_once 'DB/DataObject.php';

class DataObjects_Actions_aid extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'actions_aid';                     // table name
    public $aid;                             // int(10)  not_null primary_key unsigned auto_increment

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Actions_aid',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
