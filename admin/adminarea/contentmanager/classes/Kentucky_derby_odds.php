<?php	 		 	
/**
 * Table Definition for kentucky_derby_odds
 */
require_once 'DB/DataObject.php';

class DataObjects_Kentucky_derby_odds extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'kentucky_derby_odds';             // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $creat;                           // date(10)  not_null binary
    public $team;                            // string(50)  not_null
    public $odd;                             // string(50)  not_null
    public $description;                     // string(100)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Kentucky_derby_odds',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
