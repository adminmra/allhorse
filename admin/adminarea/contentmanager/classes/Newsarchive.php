<?php	 		 	
/**
 * Table Definition for newsarchive
 */
require_once 'DB/DataObject.php';

class DataObjects_Newsarchive extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'newsarchive';                     // table name
    public $ID;                              // int(11)  not_null primary_key auto_increment
    public $ContentId;                       // int(11)  not_null
    public $Title;                           // blob(65535)  not_null blob
    public $Teaser;                          // blob(65535)  not_null blob
    public $DateCreated;                     // string(64)  not_null
    public $timecreate;                      // datetime(19)  not_null binary
    public $doc-id;                          // string(84)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Newsarchive',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
