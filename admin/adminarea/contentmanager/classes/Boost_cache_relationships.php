<?php	 		 	
/**
 * Table Definition for boost_cache_relationships
 */
require_once 'DB/DataObject.php';

class DataObjects_Boost_cache_relationships extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'boost_cache_relationships';       // table name
    public $hash;                            // string(32)  not_null primary_key
    public $base_dir;                        // string(128)  not_null
    public $page_callback;                   // string(255)  not_null
    public $page_type;                       // string(255)  not_null
    public $page_id;                         // string(64)  not_null
    public $child_page_callback;             // string(255)  not_null
    public $child_page_type;                 // string(255)  not_null
    public $child_page_id;                   // string(64)  not_null
    public $hash_url;                        // string(32)  not_null
    public $timestamp;                       // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Boost_cache_relationships',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
