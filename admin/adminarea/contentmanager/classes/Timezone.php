<?php	 		 	
/**
 * Table Definition for timezone
 */
require_once 'DB/DataObject.php';

class DataObjects_Timezone extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'timezone';                        // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $name;                            // string(75)  not_null
    public $track;                           // string(75)  not_null
    public $zone;                            // string(75)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Timezone',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
