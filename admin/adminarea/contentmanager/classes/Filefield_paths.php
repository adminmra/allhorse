<?php	 		 	
/**
 * Table Definition for filefield_paths
 */
require_once 'DB/DataObject.php';

class DataObjects_Filefield_paths extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'filefield_paths';                 // table name
    public $type;                            // string(32)  not_null primary_key
    public $field;                           // string(32)  not_null primary_key
    public $filename;                        // blob(16777215)  not_null blob
    public $filepath;                        // blob(16777215)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Filefield_paths',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
