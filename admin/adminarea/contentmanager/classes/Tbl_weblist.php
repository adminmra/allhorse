<?php	 		 	
/**
 * Table Definition for tbl_weblist
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_weblist extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tbl_weblist';                     // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $web_name;                        // string(100)  not_null
    public $created_on;                      // datetime(19)  not_null binary
    public $status;                          // string(1)  not_null
    public $lines_color;                     // string(20)  not_null
    public $lines_back_color;                // string(20)  not_null
    public $specific_color;                  // string(20)  not_null
    public $specific_back_color;             // string(20)  not_null
    public $web_css;                         // string(200)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_weblist',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
