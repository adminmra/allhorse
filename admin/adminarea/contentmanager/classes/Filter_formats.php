<?php	 		 	
/**
 * Table Definition for filter_formats
 */
require_once 'DB/DataObject.php';

class DataObjects_Filter_formats extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'filter_formats';                  // table name
    public $format;                          // int(11)  not_null primary_key auto_increment
    public $name;                            // string(255)  not_null unique_key
    public $roles;                           // string(255)  not_null
    public $cache;                           // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Filter_formats',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
