<?php	 		 	
/**
 * Table Definition for term_hierarchy
 */
require_once 'DB/DataObject.php';

class DataObjects_Term_hierarchy extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'term_hierarchy';                  // table name
    public $tid;                             // int(10)  not_null primary_key unsigned
    public $parent;                          // int(10)  not_null primary_key multiple_key unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Term_hierarchy',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
