<?php	 		 	
/**
 * Table Definition for lines_name
 */
require_once 'DB/DataObject.php';

class DataObjects_Lines_name extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'lines_name';                      // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $main_id;                         // int(11)  not_null
    public $sp_id;                           // int(11)  not_null
    public $name;                            // string(60)  not_null
    public $sports_type;                     // int(11)  not_null
    public $feature;                         // string(15)  not_null
    public $color;                           // string(50)  not_null
    public $backcolor;                       // string(30)  not_null
    public $arrange;                         // int(11)  not_null
    public $created_on;                      // datetime(19)  not_null binary
    public $status;                          // string(1)  not_null
    public $specific_status;                 // string(1)  not_null
    public $start_date;                      // date(10)  not_null binary
    public $stop_date;                       // date(10)  not_null binary
    public $add_rules;                       // string(15)  not_null
    public $rules_text;                      // blob(65535)  not_null blob
    public $bold;                            // string(10)  not_null
    public $new_window;                      // string(25)  not_null
    public $new_window_link;                 // string(200)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Lines_name',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
