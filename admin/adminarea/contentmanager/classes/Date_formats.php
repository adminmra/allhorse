<?php	 		 	
/**
 * Table Definition for date_formats
 */
require_once 'DB/DataObject.php';

class DataObjects_Date_formats extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'date_formats';                    // table name
    public $dfid;                            // int(10)  not_null primary_key unsigned auto_increment
    public $format;                          // string(100)  not_null multiple_key binary
    public $type;                            // string(200)  not_null
    public $locked;                          // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Date_formats',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
