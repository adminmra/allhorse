<?php	 		 	
/**
 * Table Definition for menu_links
 */
require_once 'DB/DataObject.php';

class DataObjects_Menu_links extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'menu_links';                      // table name
    public $menu_name;                       // string(32)  not_null multiple_key
    public $mlid;                            // int(10)  not_null primary_key unsigned auto_increment
    public $plid;                            // int(10)  not_null unsigned
    public $link_path;                       // string(255)  not_null multiple_key
    public $router_path;                     // string(255)  not_null multiple_key
    public $link_title;                      // string(255)  not_null
    public $options;                         // blob(65535)  blob
    public $module;                          // string(255)  not_null
    public $hidden;                          // int(6)  not_null
    public $external;                        // int(6)  not_null
    public $has_children;                    // int(6)  not_null
    public $expanded;                        // int(6)  not_null
    public $weight;                          // int(11)  not_null
    public $depth;                           // int(6)  not_null
    public $customized;                      // int(6)  not_null
    public $p1;                              // int(10)  not_null unsigned
    public $p2;                              // int(10)  not_null unsigned
    public $p3;                              // int(10)  not_null unsigned
    public $p4;                              // int(10)  not_null unsigned
    public $p5;                              // int(10)  not_null unsigned
    public $p6;                              // int(10)  not_null unsigned
    public $p7;                              // int(10)  not_null unsigned
    public $p8;                              // int(10)  not_null unsigned
    public $p9;                              // int(10)  not_null unsigned
    public $updated;                         // int(6)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Menu_links',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
