<?php	 		 	
/**
 * Table Definition for ahr_marketing_new
 */
require_once 'DB/DataObject.php';

class DataObjects_Ahr_marketing_new extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ahr_marketing_new';               // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $name;                            // string(100)  not_null
    public $email;                           // string(100)  not_null primary_key
    public $state;                           // string(100)  not_null
    public $update;                          // timestamp(19)  not_null unsigned zerofill binary timestamp

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ahr_marketing_new',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
