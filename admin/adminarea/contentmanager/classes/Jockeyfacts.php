<?php	 		 	
/**
 * Table Definition for jockeyfacts
 */
require_once 'DB/DataObject.php';

class DataObjects_Jockeyfacts extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'jockeyfacts';                     // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $jockey_id;                       // int(20)  not_null
    public $fact;                            // blob(4294967295)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Jockeyfacts',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
