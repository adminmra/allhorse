<?php	 		 	
/**
 * Table Definition for ad_hosts
 */
require_once 'DB/DataObject.php';

class DataObjects_Ad_hosts extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ad_hosts';                        // table name
    public $uid;                             // int(10)  not_null primary_key unsigned
    public $hostid;                          // string(32)  not_null multiple_key
    public $status;                          // int(3)  not_null multiple_key unsigned
    public $description;                     // blob(65535)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ad_hosts',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
