<?php	 		 	
/**
 * Table Definition for history
 */
require_once 'DB/DataObject.php';

class DataObjects_History extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'history';                         // table name
    public $uid;                             // int(11)  not_null primary_key
    public $nid;                             // int(11)  not_null primary_key multiple_key
    public $timestamp;                       // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_History',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
