<?php	 		 	
/**
 * Table Definition for splash
 */
require_once 'DB/DataObject.php';

class DataObjects_Splash extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'splash';                          // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $site;                            // string(100)  not_null
    public $landingurl;                      // string(100)  not_null
    public $status;                          // string(1)  not_null enum
    public $host;                            // string(20)  not_null
    public $present;                         // string(50)  not_null
    public $splashstat;                      // string(1)  not_null enum

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Splash',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
