<?php	 		 	
/**
 * Table Definition for equine_charities
 */
require_once 'DB/DataObject.php';

class DataObjects_Equine_charities extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'equine_charities';                // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $name;                            // string(60)  not_null
    public $country;                         // string(100)  not_null
    public $state;                           // string(70)  not_null
    public $city;                            // string(70)  not_null
    public $url;                             // string(150)  not_null
    public $notes;                           // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Equine_charities',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
