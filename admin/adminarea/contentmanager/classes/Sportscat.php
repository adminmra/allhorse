<?php	 		 	
/**
 * Table Definition for sportscat
 */
require_once 'DB/DataObject.php';

class DataObjects_Sportscat extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'sportscat';                       // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $title;                           // string(75)  not_null
    public $active;                          // string(1)  not_null enum
    public $position;                        // int(4)  not_null
    public $defaulttab;                      // string(1)  not_null enum

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Sportscat',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
