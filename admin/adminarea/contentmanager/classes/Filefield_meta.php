<?php	 		 	
/**
 * Table Definition for filefield_meta
 */
require_once 'DB/DataObject.php';

class DataObjects_Filefield_meta extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'filefield_meta';                  // table name
    public $fid;                             // int(10)  not_null primary_key unsigned
    public $width;                           // int(10)  unsigned
    public $height;                          // int(10)  unsigned
    public $duration;                        // real(12)  
    public $audio_format;                    // string(10)  not_null
    public $audio_sample_rate;               // int(9)  not_null
    public $audio_channel_mode;              // string(10)  not_null
    public $audio_bitrate;                   // real(12)  not_null
    public $audio_bitrate_mode;              // string(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Filefield_meta',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
