<?php	 		 	
/**
 * Table Definition for horsepics
 */
require_once 'DB/DataObject.php';

class DataObjects_Horsepics extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'horsepics';                       // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $horse_id;                        // int(20)  not_null
    public $url;                             // blob(4294967295)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Horsepics',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
