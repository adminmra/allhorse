<?php	 		 	
/**
 * Table Definition for hbhotlines
 */
require_once 'DB/DataObject.php';

class DataObjects_Hbhotlines extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'hbhotlines';                      // table name
    public $linesid;                         // int(11)  not_null primary_key auto_increment
    public $landurl;                         // string(150)  not_null
    public $tag;                             // string(50)  not_null
    public $sportscat;                       // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Hbhotlines',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
