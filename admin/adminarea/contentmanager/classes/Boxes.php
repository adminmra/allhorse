<?php	 		 	
/**
 * Table Definition for boxes
 */
require_once 'DB/DataObject.php';

class DataObjects_Boxes extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'boxes';                           // table name
    public $bid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $body;                            // blob(4294967295)  blob
    public $info;                            // string(128)  not_null unique_key
    public $format;                          // int(6)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Boxes',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
