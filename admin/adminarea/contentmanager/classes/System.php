<?php	 		 	
/**
 * Table Definition for system
 */
require_once 'DB/DataObject.php';

class DataObjects_System extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'system';                          // table name
    public $filename;                        // string(255)  not_null primary_key
    public $name;                            // string(255)  not_null
    public $type;                            // string(255)  not_null multiple_key
    public $owner;                           // string(255)  not_null
    public $status;                          // int(11)  not_null
    public $throttle;                        // int(4)  not_null
    public $bootstrap;                       // int(11)  not_null
    public $schema_version;                  // int(6)  not_null
    public $weight;                          // int(11)  not_null
    public $info;                            // blob(65535)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_System',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
