<?php	 		 	
/**
 * Table Definition for tbl_admin
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_admin extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tbl_admin';                       // table name
    public $admin_id;                        // int(11)  not_null primary_key auto_increment
    public $admin_username;                  // string(250)  
    public $admin_password;                  // string(250)  
    public $mailid;                          // string(250)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_admin',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
