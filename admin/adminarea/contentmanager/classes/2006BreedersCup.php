<?php	 		 	
/**
 * Table Definition for 2006BreedersCup
 */
require_once 'DB/DataObject.php';

class DataObjects_2006BreedersCup extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = '2006BreedersCup';                 // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $horse;                           // string(50)  not_null
    public $odds;                            // string(60)  not_null
    public $jockey;                          // string(60)  not_null
    public $result;                          // int(11)  
    public $post;                            // int(60)  
    public $stakes;                          // blob(65535)  not_null blob
    public $resulttime;                      // string(150)  not_null
    public $posttime;                        // string(150)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_2006BreedersCup',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
