<?php	 		 	
/**
 * Table Definition for ad_html
 */
require_once 'DB/DataObject.php';

class DataObjects_Ad_html extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ad_html';                         // table name
    public $aid;                             // int(10)  not_null primary_key unsigned
    public $html;                            // blob(65535)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ad_html',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
