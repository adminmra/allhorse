<?php	 		 	
/**
 * Table Definition for batch
 */
require_once 'DB/DataObject.php';

class DataObjects_Batch extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'batch';                           // table name
    public $bid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $token;                           // string(64)  not_null multiple_key
    public $timestamp;                       // int(11)  not_null
    public $batch;                           // blob(4294967295)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Batch',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
