<?php	 		 	
/**
 * Table Definition for menu_custom
 */
require_once 'DB/DataObject.php';

class DataObjects_Menu_custom extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'menu_custom';                     // table name
    public $menu_name;                       // string(32)  not_null primary_key
    public $title;                           // string(255)  not_null
    public $description;                     // blob(65535)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Menu_custom',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
