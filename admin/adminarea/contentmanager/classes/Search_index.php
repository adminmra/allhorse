<?php	 		 	
/**
 * Table Definition for search_index
 */
require_once 'DB/DataObject.php';

class DataObjects_Search_index extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'search_index';                    // table name
    public $word;                            // string(50)  not_null multiple_key
    public $sid;                             // int(10)  not_null multiple_key unsigned
    public $type;                            // string(16)  
    public $score;                           // real(12)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Search_index',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
