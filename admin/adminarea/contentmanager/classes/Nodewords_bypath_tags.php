<?php	 		 	
/**
 * Table Definition for nodewords_bypath_tags
 */
require_once 'DB/DataObject.php';

class DataObjects_Nodewords_bypath_tags extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'nodewords_bypath_tags';           // table name
    public $rule_id;                         // int(11)  not_null primary_key
    public $meta_tag;                        // string(32)  not_null primary_key
    public $meta_value;                      // string(255)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Nodewords_bypath_tags',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
