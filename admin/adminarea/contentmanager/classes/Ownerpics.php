<?php	 		 	
/**
 * Table Definition for ownerpics
 */
require_once 'DB/DataObject.php';

class DataObjects_Ownerpics extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ownerpics';                       // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $owner_id;                        // int(20)  not_null
    public $name;                            // string(50)  multiple_key
    public $url;                             // blob(4294967295)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ownerpics',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
