<?php	 		 	
/**
 * Table Definition for ad_countdown_format
 */
require_once 'DB/DataObject.php';

class DataObjects_Ad_countdown_format extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ad_countdown_format';             // table name
    public $gid;                             // int(10)  not_null primary_key unsigned
    public $min_width;                       // int(10)  not_null unsigned
    public $max_width;                       // int(10)  not_null unsigned
    public $min_height;                      // int(10)  not_null unsigned
    public $max_height;                      // int(10)  not_null unsigned
    public $max_size;                        // int(10)  not_null unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ad_countdown_format',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
