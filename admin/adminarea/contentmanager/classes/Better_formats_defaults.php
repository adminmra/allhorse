<?php	 		 	
/**
 * Table Definition for better_formats_defaults
 */
require_once 'DB/DataObject.php';

class DataObjects_Better_formats_defaults extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'better_formats_defaults';         // table name
    public $rid;                             // int(10)  not_null primary_key unsigned
    public $type;                            // string(255)  not_null primary_key
    public $format;                          // int(8)  not_null unsigned
    public $type_weight;                     // int(3)  not_null unsigned
    public $weight;                          // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Better_formats_defaults',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
