<?php	 		 	
/**
 * Table Definition for content_type_page
 */
require_once 'DB/DataObject.php';

class DataObjects_Content_type_page extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'content_type_page';               // table name
    public $vid;                             // int(10)  not_null primary_key unsigned
    public $nid;                             // int(10)  not_null multiple_key unsigned
    public $field_body_logged_out_value;     // blob(4294967295)  blob
    public $field_body_logged_out_format;    // int(10)  unsigned
    public $field_body_logged_in_value;      // blob(4294967295)  blob
    public $field_body_logged_in_format;     // int(10)  unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Content_type_page',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
