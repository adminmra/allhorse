<?php	 		 	
/**
 * Table Definition for ad_weight_probability
 */
require_once 'DB/DataObject.php';

class DataObjects_Ad_weight_probability extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ad_weight_probability';           // table name
    public $aid;                             // int(11)  not_null primary_key
    public $probability;                     // int(3)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ad_weight_probability',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
