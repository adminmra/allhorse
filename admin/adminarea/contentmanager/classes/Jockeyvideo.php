<?php	 		 	
/**
 * Table Definition for jockeyvideo
 */
require_once 'DB/DataObject.php';

class DataObjects_Jockeyvideo extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'jockeyvideo';                     // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $jockey_id;                       // int(20)  not_null
    public $name;                            // string(50)  multiple_key
    public $url;                             // blob(4294967295)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Jockeyvideo',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
