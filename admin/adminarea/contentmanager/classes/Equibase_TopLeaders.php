<?php	 		 	
/**
 * Table Definition for equibase_TopLeaders
 */
require_once 'DB/DataObject.php';

class DataObjects_Equibase_TopLeaders extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'equibase_TopLeaders';             // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $Leader_Type;                     // string(255)  
    public $Track;                           // string(255)  
    public $Starting_Date;                   // string(255)  
    public $Ending_Date;                     // string(255)  
    public $Name;                            // string(255)  
    public $Starts;                          // string(255)  
    public $first;                           // string(255)  
    public $second;                          // string(255)  
    public $third;                           // string(255)  
    public $Earnings;                        // string(255)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Equibase_TopLeaders',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
