<?php	 		 	
/**
 * Table Definition for phpmysqlautobackup
 */
require_once 'DB/DataObject.php';

class DataObjects_Phpmysqlautobackup extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'phpmysqlautobackup';              // table name
    public $id;                              // int(11)  not_null primary_key
    public $version;                         // string(6)  
    public $time_last_run;                   // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Phpmysqlautobackup',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
