<?php	 		 	
/**
 * Table Definition for horse
 */
require_once 'DB/DataObject.php';

class DataObjects_Horse extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'horse';                           // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $horse;                           // string(50)  not_null
    public $status;                          // string(50)  
    public $trainer;                         // string(50)  
    public $pic;                             // string(90)  
    public $silkpic;                         // string(90)  
    public $owner;                           // string(50)  
    public $foaling;                         // string(50)  
    public $sire;                            // string(50)  
    public $dam;                             // string(50)  
    public $damsire;                         // string(50)  
    public $dosage;                          // string(50)  
    public $dosageindex;                     // string(11)  
    public $cd;                              // string(11)  
    public $notes;                           // blob(4294967295)  blob
    public $bestbeyer;                       // string(25)  
    public $stable;                          // string(50)  
    public $GHBnotes;                        // blob(4294967295)  not_null blob
    public $HBnotes;                         // blob(4294967295)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Horse',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
