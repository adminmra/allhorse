<?php	 		 	
/**
 * Table Definition for racetrack
 */
require_once 'DB/DataObject.php';

class DataObjects_Racetrack extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'racetrack';                       // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $racetrack;                       // string(50)  not_null
    public $type;                            // string(1)  not_null enum

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Racetrack',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
