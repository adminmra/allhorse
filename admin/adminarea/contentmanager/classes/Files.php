<?php	 		 	
/**
 * Table Definition for files
 */
require_once 'DB/DataObject.php';

class DataObjects_Files extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'files';                           // table name
    public $fid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $uid;                             // int(10)  not_null multiple_key unsigned
    public $filename;                        // string(255)  not_null
    public $filepath;                        // string(255)  not_null
    public $filemime;                        // string(255)  not_null
    public $filesize;                        // int(10)  not_null unsigned
    public $status;                          // int(11)  not_null multiple_key
    public $timestamp;                       // int(10)  not_null multiple_key unsigned
    public $origname;                        // string(255)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Files',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
