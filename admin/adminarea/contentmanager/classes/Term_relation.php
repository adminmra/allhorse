<?php	 		 	
/**
 * Table Definition for term_relation
 */
require_once 'DB/DataObject.php';

class DataObjects_Term_relation extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'term_relation';                   // table name
    public $trid;                            // int(11)  not_null primary_key auto_increment
    public $tid1;                            // int(10)  not_null multiple_key unsigned
    public $tid2;                            // int(10)  not_null multiple_key unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Term_relation',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
