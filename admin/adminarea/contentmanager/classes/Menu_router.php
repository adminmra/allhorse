<?php	 		 	
/**
 * Table Definition for menu_router
 */
require_once 'DB/DataObject.php';

class DataObjects_Menu_router extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'menu_router';                     // table name
    public $path;                            // string(255)  not_null primary_key
    public $load_functions;                  // blob(65535)  not_null blob
    public $to_arg_functions;                // blob(65535)  not_null blob
    public $access_callback;                 // string(255)  not_null
    public $access_arguments;                // blob(65535)  blob
    public $page_callback;                   // string(255)  not_null
    public $page_arguments;                  // blob(65535)  blob
    public $fit;                             // int(11)  not_null multiple_key
    public $number_parts;                    // int(6)  not_null
    public $tab_parent;                      // string(255)  not_null multiple_key
    public $tab_root;                        // string(255)  not_null multiple_key
    public $title;                           // string(255)  not_null
    public $title_callback;                  // string(255)  not_null
    public $title_arguments;                 // string(255)  not_null
    public $type;                            // int(11)  not_null
    public $block_callback;                  // string(255)  not_null
    public $description;                     // blob(65535)  not_null blob
    public $position;                        // string(255)  not_null
    public $weight;                          // int(11)  not_null
    public $file;                            // blob(16777215)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Menu_router',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
