<?php	 		 	
/**
 * Table Definition for tasks
 */
require_once 'DB/DataObject.php';

class DataObjects_Tasks extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tasks';                           // table name
    public $ID;                              // int(11)  not_null primary_key auto_increment
    public $parent;                          // int(11)  
    public $title;                           // blob(65535)  blob
    public $notes;                           // blob(65535)  blob
    public $status;                          // int(11)  
    public $priority;                        // int(11)  
    public $URL_1;                           // blob(65535)  blob
    public $URL_2;                           // blob(65535)  blob
    public $URL_3;                           // blob(65535)  blob
    public $date_due;                        // date(10)  binary
    public $date_modified;                   // timestamp(19)  not_null unsigned zerofill binary timestamp
    public $date_entered;                    // timestamp(19)  not_null unsigned zerofill binary
    public $obsolete;                        // int(6)  
    public $container;                       // int(6)  
    public $type;                            // int(6)  not_null
    public $user_date_1;                     // datetime(19)  not_null binary
    public $user_date_2;                     // datetime(19)  not_null binary
    public $user_text_1;                     // blob(65535)  not_null blob
    public $user_text_2;                     // blob(65535)  not_null blob
    public $user_int_1;                      // int(11)  not_null
    public $user_int_2;                      // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tasks',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
