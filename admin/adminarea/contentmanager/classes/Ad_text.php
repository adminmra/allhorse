<?php	 		 	
/**
 * Table Definition for ad_text
 */
require_once 'DB/DataObject.php';

class DataObjects_Ad_text extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ad_text';                         // table name
    public $aid;                             // int(10)  not_null primary_key unsigned
    public $url;                             // string(255)  not_null
    public $adheader;                        // string(255)  not_null
    public $adbody;                          // blob(65535)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ad_text',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
