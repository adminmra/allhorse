<?php	 		 	
/**
 * Table Definition for tbloddsL
 */
require_once 'DB/DataObject.php';

class DataObjects_TbloddsL extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tbloddsL';                        // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $perform_id;                      // int(11)  not_null
    public $cotestid;                        // int(11)  not_null
    public $poolcode;                        // string(10)  not_null
    public $totalrows;                       // string(50)  not_null
    public $totalcolumn;                     // string(50)  not_null
    public $morningdata;                     // string(150)  not_null
    public $finaldata;                       // string(150)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_TbloddsL',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
