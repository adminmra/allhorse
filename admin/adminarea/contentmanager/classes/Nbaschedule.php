<?php	 		 	
/**
 * Table Definition for nbaschedule
 */
require_once 'DB/DataObject.php';

class DataObjects_Nbaschedule extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'nbaschedule';                     // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $matchups;                        // string(150)  
    public $time;                            // string(50)  
    public $racedate;                        // date(10)  not_null binary
    public $weekselect;                      // string(50)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Nbaschedule',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
