<?php	 		 	
/**
 * Table Definition for jockey_backup
 */
require_once 'DB/DataObject.php';

class DataObjects_Jockey_backup extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'jockey_backup';                   // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $name;                            // string(50)  
    public $sex;                             // string(50)  
    public $birthdate;                       // date(10)  binary
    public $birthcity;                       // string(50)  
    public $birthstate;                      // string(50)  
    public $birthcountry;                    // string(50)  
    public $residentcity;                    // string(50)  
    public $residentstate;                   // string(50)  
    public $residentcountry;                 // string(50)  
    public $height;                          // string(50)  
    public $weight;                          // int(11)  
    public $family;                          // blob(4294967295)  blob
    public $pic;                             // string(90)  
    public $url;                             // string(50)  
    public $notes;                           // blob(4294967295)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Jockey_backup',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
