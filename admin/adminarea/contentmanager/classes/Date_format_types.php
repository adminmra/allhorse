<?php	 		 	
/**
 * Table Definition for date_format_types
 */
require_once 'DB/DataObject.php';

class DataObjects_Date_format_types extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'date_format_types';               // table name
    public $type;                            // string(200)  not_null primary_key
    public $title;                           // string(255)  not_null
    public $locked;                          // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Date_format_types',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
