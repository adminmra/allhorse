<?php	 		 	
/**
 * Table Definition for term_synonym
 */
require_once 'DB/DataObject.php';

class DataObjects_Term_synonym extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'term_synonym';                    // table name
    public $tsid;                            // int(11)  not_null primary_key auto_increment
    public $tid;                             // int(10)  not_null multiple_key unsigned
    public $name;                            // string(255)  not_null multiple_key

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Term_synonym',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
