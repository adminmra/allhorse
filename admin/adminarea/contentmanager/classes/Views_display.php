<?php	 		 	
/**
 * Table Definition for views_display
 */
require_once 'DB/DataObject.php';

class DataObjects_Views_display extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'views_display';                   // table name
    public $vid;                             // int(10)  not_null multiple_key unsigned
    public $id;                              // string(64)  not_null
    public $display_title;                   // string(64)  not_null
    public $display_plugin;                  // string(64)  not_null
    public $position;                        // int(11)  
    public $display_options;                 // blob(65535)  blob binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Views_display',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
