<?php	 		 	
/**
 * Table Definition for wysiwyg
 */
require_once 'DB/DataObject.php';

class DataObjects_Wysiwyg extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'wysiwyg';                         // table name
    public $format;                          // int(11)  not_null primary_key
    public $editor;                          // string(128)  not_null
    public $settings;                        // blob(65535)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Wysiwyg',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
