<?php	 		 	
/**
 * Table Definition for search_total
 */
require_once 'DB/DataObject.php';

class DataObjects_Search_total extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'search_total';                    // table name
    public $word;                            // string(50)  not_null primary_key
    public $count;                           // real(12)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Search_total',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
