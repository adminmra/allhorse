<?php	 		 	
/**
 * Table Definition for melbourne_result
 */
require_once 'DB/DataObject.php';

class DataObjects_Melbourne_result extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'melbourne_result';                // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $year;                            // int(11)  not_null
    public $winner;                          // string(50)  not_null
    public $jockey;                          // string(50)  not_null
    public $trainer;                         // string(50)  not_null
    public $owner;                           // string(150)  not_null
    public $wintime;                         // string(50)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Melbourne_result',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
