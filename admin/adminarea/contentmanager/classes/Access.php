<?php	 		 	
/**
 * Table Definition for access
 */
require_once 'DB/DataObject.php';

class DataObjects_Access extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'access';                          // table name
    public $aid;                             // int(11)  not_null primary_key auto_increment
    public $mask;                            // string(255)  not_null
    public $type;                            // string(255)  not_null
    public $status;                          // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Access',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
