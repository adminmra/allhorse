<?php	 		 	
/**
 * Table Definition for devel_queries
 */
require_once 'DB/DataObject.php';

class DataObjects_Devel_queries extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'devel_queries';                   // table name
    public $qid;                             // int(11)  not_null multiple_key auto_increment
    public $function;                        // string(255)  not_null
    public $query;                           // blob(65535)  not_null blob
    public $hash;                            // string(255)  not_null primary_key

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Devel_queries',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
