<?php	 		 	
/**
 * Table Definition for xmlsitemap
 */
require_once 'DB/DataObject.php';

class DataObjects_Xmlsitemap extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'xmlsitemap';                      // table name
    public $type;                            // string(12)  not_null primary_key
    public $id;                              // int(10)  not_null primary_key unsigned
    public $status;                          // int(3)  not_null unsigned
    public $loc;                             // string(255)  not_null unique_key
    public $lastmod;                         // int(10)  unsigned
    public $priority;                        // real(12)  
    public $changefreq;                      // int(10)  unsigned
    public $changecount;                     // int(10)  unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Xmlsitemap',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
