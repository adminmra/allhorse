<?php	 		 	
/**
 * Table Definition for racepromo
 */
require_once 'DB/DataObject.php';

class DataObjects_Racepromo extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'racepromo';                       // table name
    public $racepromoid;                     // int(11)  not_null primary_key auto_increment
    public $promo;                           // blob(65535)  not_null blob
    public $lastupdate;                      // date(10)  not_null binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Racepromo',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
