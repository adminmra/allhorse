<?php	 		 	
/**
 * Table Definition for race_date
 */
require_once 'DB/DataObject.php';

class DataObjects_Race_date extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'race_date';                       // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $race_date;                       // date(10)  not_null binary
    public $racetrack_id;                    // int(11)  not_null
    public $abb;                             // string(50)  not_null
    public $performance;                     // string(100)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Race_date',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
