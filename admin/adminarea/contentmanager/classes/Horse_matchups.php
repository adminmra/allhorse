<?php	 		 	
/**
 * Table Definition for horse_matchups
 */
require_once 'DB/DataObject.php';

class DataObjects_Horse_matchups extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'horse_matchups';                  // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $racedate;                        // date(10)  not_null binary
    public $race;                            // string(200)  not_null
    public $track;                           // string(150)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Horse_matchups',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
