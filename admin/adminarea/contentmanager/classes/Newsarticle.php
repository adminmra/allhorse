<?php	 		 	
/**
 * Table Definition for newsarticle
 */
require_once 'DB/DataObject.php';

class DataObjects_Newsarticle extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'newsarticle';                     // table name
    public $ID;                              // int(11)  not_null primary_key auto_increment
    public $ContentIdART;                    // int(11)  not_null
    public $TitleART;                        // blob(65535)  not_null blob
    public $HtmlART;                         // blob(65535)  not_null blob
    public $DateCreatedART;                  // string(64)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Newsarticle',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
