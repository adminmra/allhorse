<?php	 		 	
/**
 * Table Definition for ad_owners
 */
require_once 'DB/DataObject.php';

class DataObjects_Ad_owners extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ad_owners';                       // table name
    public $oid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $aid;                             // int(10)  not_null multiple_key unsigned
    public $uid;                             // int(10)  not_null multiple_key unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ad_owners',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
