<?php	 		 	
/**
 * Table Definition for nodewords_custom
 */
require_once 'DB/DataObject.php';

class DataObjects_Nodewords_custom extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'nodewords_custom';                // table name
    public $pid;                             // int(11)  not_null primary_key auto_increment
    public $path;                            // string(255)  not_null unique_key
    public $weight;                          // int(4)  not_null
    public $enabled;                         // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Nodewords_custom',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
