<?php	 		 	
/**
 * Table Definition for backup_migrate_destinations
 */
require_once 'DB/DataObject.php';

class DataObjects_Backup_migrate_destinations extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'backup_migrate_destinations';     // table name
    public $destination_id;                  // string(32)  not_null primary_key
    public $name;                            // string(255)  not_null
    public $type;                            // string(32)  not_null
    public $location;                        // blob(65535)  not_null blob
    public $settings;                        // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Backup_migrate_destinations',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
