<?php	 		 	
/**
 * Table Definition for emailcatch
 */
require_once 'DB/DataObject.php';

class DataObjects_Emailcatch extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'emailcatch';                      // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $first;                           // string(75)  not_null
    public $last;                            // string(75)  not_null
    public $email;                           // string(75)  not_null
    public $site;                            // string(75)  not_null
    public $timeadd;                         // datetime(19)  not_null binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Emailcatch',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
