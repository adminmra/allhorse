<?php	 		 	
/**
 * Table Definition for content_group_fields
 */
require_once 'DB/DataObject.php';

class DataObjects_Content_group_fields extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'content_group_fields';            // table name
    public $type_name;                       // string(32)  not_null primary_key
    public $group_name;                      // string(32)  not_null primary_key
    public $field_name;                      // string(32)  not_null primary_key

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Content_group_fields',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
