<?php	 		 	
/**
 * Table Definition for term_data
 */
require_once 'DB/DataObject.php';

class DataObjects_Term_data extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'term_data';                       // table name
    public $tid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $vid;                             // int(10)  not_null multiple_key unsigned
    public $name;                            // string(255)  not_null
    public $description;                     // blob(4294967295)  blob
    public $weight;                          // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Term_data',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
