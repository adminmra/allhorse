<?php	 		 	
/**
 * Table Definition for handicappingcontest
 */
require_once 'DB/DataObject.php';

class DataObjects_Handicappingcontest extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'handicappingcontest';             // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $cid;                             // string(100)  not_null
    public $email;                           // string(100)  not_null
    public $dropdown1;                       // string(100)  not_null
    public $dropdown2;                       // string(100)  not_null
    public $dropdown3;                       // string(100)  not_null
    public $dropdown4;                       // string(100)  not_null
    public $dropdown5;                       // string(100)  not_null
    public $dropdown6;                       // string(100)  not_null
    public $dropdown7;                       // string(100)  not_null
    public $dropdown8;                       // string(100)  not_null
    public $dropdown9;                       // string(100)  not_null
    public $dropdown10;                      // string(100)  not_null
    public $updatetime;                      // timestamp(19)  not_null unsigned zerofill binary timestamp

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Handicappingcontest',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
