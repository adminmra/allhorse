<?php	 		 	
/**
 * Table Definition for contest
 */
require_once 'DB/DataObject.php';

class DataObjects_Contest extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'contest';                         // table name
    public $contest_id;                      // int(11)  not_null primary_key auto_increment
    public $performance_id;                  // int(11)  not_null
    public $time;                            // string(50)  not_null
    public $purse;                           // real(8)  not_null
    public $info;                            // string(150)  not_null
    public $contest_no;                      // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Contest',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
