<?php	 		 	
/**
 * Table Definition for racetracks
 */
require_once 'DB/DataObject.php';

class DataObjects_Racetracks extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'racetracks';                      // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $trackcode;                       // string(50)  multiple_key
    public $name;                            // string(50)  
    public $url;                             // blob(4294967295)  blob
    public $urlahr;                          // blob(4294967295)  blob
    public $piclogo;                         // string(90)  
    public $history;                         // blob(4294967295)  blob
    public $dateopen;                        // date(10)  binary
    public $dateclosed;                      // date(10)  binary
    public $racingdays;                      // int(11)  
    public $street;                          // string(50)  
    public $street2;                         // string(50)  
    public $city;                            // string(50)  
    public $state;                           // string(50)  
    public $zip;                             // string(50)  
    public $country;                         // string(50)  
    public $driving;                         // blob(4294967295)  blob
    public $picdriving;                      // string(90)  
    public $phone;                           // string(50)  
    public $phone2;                          // string(50)  
    public $hta;                             // string(50)  
    public $weather;                         // string(50)  
    public $datestart;                       // date(10)  binary
    public $tracklength;                     // string(50)  
    public $stretchlength;                   // string(50)  
    public $stretchwidth;                    // string(50)  
    public $speedrating;                     // string(50)  
    public $capinfield;                      // int(11)  
    public $capclubhouse;                    // int(11)  
    public $capgrandstand;                   // int(11)  
    public $capparking;                      // int(11)  
    public $pricega;                         // string(50)  
    public $pricech;                         // string(50)  
    public $pricetc;                         // string(50)  
    public $pointsofinterest;                // blob(4294967295)  blob
    public $live;                            // string(1)  not_null enum
    public $timezone;                        // string(50)  not_null
    public $isDST;                           // string(1)  not_null enum
    public $uvideourl;                       // string(100)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Racetracks',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
