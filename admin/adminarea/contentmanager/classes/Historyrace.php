<?php	 		 	
/**
 * Table Definition for historyrace
 */
require_once 'DB/DataObject.php';

class DataObjects_Historyrace extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'historyrace';                     // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $year;                            // string(75)  not_null
    public $winner;                          // string(75)  not_null
    public $age;                             // int(11)  
    public $jockey;                          // string(75)  not_null
    public $trainer;                         // string(75)  not_null
    public $owner;                           // string(75)  not_null
    public $racetime;                        // string(75)  not_null
    public $stake_id;                        // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Historyrace',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
