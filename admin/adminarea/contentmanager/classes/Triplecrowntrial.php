<?php	 		 	
/**
 * Table Definition for triplecrowntrial
 */
require_once 'DB/DataObject.php';

class DataObjects_Triplecrowntrial extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'triplecrowntrial';                // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $racedate;                        // date(10)  not_null binary
    public $horse;                           // string(100)  not_null
    public $PF;                              // string(100)  not_null
    public $sire;                            // string(100)  not_null
    public $maresire;                        // string(100)  not_null
    public $race;                            // string(100)  not_null
    public $gr;                              // string(100)  not_null
    public $track;                           // string(100)  not_null
    public $surface;                         // string(100)  not_null
    public $distance;                        // string(100)  not_null
    public $dp;                              // string(100)  not_null
    public $di;                              // string(100)  not_null
    public $cd;                              // string(100)  not_null
    public $points;                          // string(100)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Triplecrowntrial',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
