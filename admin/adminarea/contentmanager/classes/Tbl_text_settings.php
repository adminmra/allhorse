<?php	 		 	
/**
 * Table Definition for tbl_text_settings
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_text_settings extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tbl_text_settings';               // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $text;                            // blob(65535)  not_null blob
    public $section_name;                    // string(50)  not_null
    public $created_on;                      // datetime(19)  not_null binary
    public $status;                          // string(1)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_text_settings',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
