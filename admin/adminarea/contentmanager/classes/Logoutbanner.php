<?php	 		 	
/**
 * Table Definition for logoutbanner
 */
require_once 'DB/DataObject.php';

class DataObjects_Logoutbanner extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'logoutbanner';                    // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $racedate;                        // date(10)  binary
    public $hour;                            // int(11)  not_null
    public $mint;                            // int(11)  
    public $ahrimage;                        // string(100)  not_null
    public $hbimage;                         // string(100)  not_null
    public $timeet;                          // int(11)  not_null
    public $alt;                             // string(150)  not_null
    public $landing;                         // string(150)  not_null
    public $ghbimage;                        // string(75)  not_null
    public $ghbminiimage;                    // string(75)  not_null
    public $altahr;                          // string(100)  not_null
    public $altghb;                          // string(100)  not_null
    public $AHRland;                         // string(200)  not_null
    public $GHBland;                         // string(200)  not_null
    public $HBland;                          // string(200)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Logoutbanner',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
