<?php	 		 	
/**
 * Table Definition for nflschedule
 */
require_once 'DB/DataObject.php';

class DataObjects_Nflschedule extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'nflschedule';                     // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $nfldate;                         // date(10)  not_null binary
    public $weekselect;                      // string(75)  not_null
    public $timeet;                          // int(6)  
    public $visitingteam;                    // string(100)  not_null
    public $hometeam;                        // string(100)  not_null
    public $tv;                              // string(10)  not_null
    public $weather;                         // string(15)  not_null
    public $timestate;                       // string(2)  not_null enum
    public $timese;                          // int(6)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Nflschedule',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
