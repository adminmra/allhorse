<?php	 		 	
/**
 * Table Definition for odds
 */
require_once 'DB/DataObject.php';

class DataObjects_Odds extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'odds';                            // table name
    public $odds_id;                         // int(11)  not_null primary_key auto_increment
    public $contest_id;                      // int(11)  not_null
    public $morning_odds;                    // string(200)  not_null
    public $current_odds;                    // string(200)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Odds',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
