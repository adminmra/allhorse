<?php	 		 	
/**
 * Table Definition for upload
 */
require_once 'DB/DataObject.php';

class DataObjects_Upload extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'upload';                          // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $name;                            // string(30)  not_null
    public $type;                            // string(30)  not_null
    public $size;                            // int(11)  not_null
    public $content;                         // blob(16777215)  not_null blob binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Upload',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
