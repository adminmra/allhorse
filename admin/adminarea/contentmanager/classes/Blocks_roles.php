<?php	 		 	
/**
 * Table Definition for blocks_roles
 */
require_once 'DB/DataObject.php';

class DataObjects_Blocks_roles extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'blocks_roles';                    // table name
    public $module;                          // string(64)  not_null primary_key
    public $delta;                           // string(32)  not_null primary_key
    public $rid;                             // int(10)  not_null primary_key multiple_key unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Blocks_roles',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
