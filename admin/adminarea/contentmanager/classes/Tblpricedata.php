<?php	 		 	
/**
 * Table Definition for tblpricedata
 */
require_once 'DB/DataObject.php';

class DataObjects_Tblpricedata extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tblpricedata';                    // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $pool;                            // string(60)  not_null
    public $pricetype;                       // string(60)  not_null
    public $totallegs;                       // string(60)  not_null
    public $contestants;                     // string(60)  not_null
    public $baseamount;                      // string(60)  not_null
    public $price;                           // string(60)  not_null
    public $cap;                             // string(60)  not_null
    public $def;                             // string(60)  not_null
    public $shadow;                          // string(60)  not_null
    public $perform_id;                      // int(11)  not_null multiple_key
    public $cotestid;                        // int(11)  not_null multiple_key

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tblpricedata',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
