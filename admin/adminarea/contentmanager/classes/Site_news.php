<?php	 		 	
/**
 * Table Definition for site_news
 */
require_once 'DB/DataObject.php';

class DataObjects_Site_news extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'site_news';                       // table name
    public $news_id;                         // int(11)  not_null multiple_key auto_increment
    public $news_title;                      // blob(16777215)  not_null blob
    public $news_copy;                       // blob(16777215)  not_null blob
    public $news_date;                       // datetime(19)  not_null binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Site_news',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
