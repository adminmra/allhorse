<?php	 		 	
/**
 * Table Definition for horsevideo
 */
require_once 'DB/DataObject.php';

class DataObjects_Horsevideo extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'horsevideo';                      // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $horse_id;                        // int(20)  not_null
    public $url;                             // string(90)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Horsevideo',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
