<?php	 		 	
/**
 * Table Definition for hp_signup
 */
require_once 'DB/DataObject.php';

class DataObjects_Hp_signup extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'hp_signup';                       // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $license_id;                      // int(20)  not_null
    public $first_name;                      // string(100)  not_null
    public $last_name;                       // string(100)  not_null
    public $street1;                         // string(250)  not_null
    public $street2;                         // string(250)  not_null
    public $city;                            // string(100)  not_null
    public $state;                           // string(100)  not_null
    public $country;                         // string(100)  not_null
    public $zipcode;                         // string(30)  not_null
    public $phone_no;                        // string(50)  not_null
    public $email_id;                        // string(150)  not_null
    public $primary_site;                    // blob(65535)  not_null blob
    public $other_sites;                     // blob(65535)  not_null blob
    public $username;                        // string(100)  not_null
    public $password;                        // string(100)  not_null
    public $commission;                      // string(200)  not_null
    public $rcv_email;                       // string(10)  not_null
    public $pay_method;                      // string(50)  not_null
    public $pmGamingAccount;                 // string(50)  not_null
    public $pmGamingSitename;                // string(200)  not_null
    public $pmNetellerAccountNumber;         // string(50)  not_null
    public $pmNetTellerEmail;                // string(200)  not_null
    public $pmAchBankname;                   // string(200)  not_null
    public $pmAchBankAddress;                // blob(65535)  not_null blob
    public $pmAchAccountNumber;              // string(50)  not_null
    public $pmAchRoutingNumber;              // string(200)  not_null
    public $pmAchAccountType;                // string(100)  not_null
    public $pmAchAccountHolderName;          // string(100)  not_null
    public $pmCheckPayableTo;                // string(100)  not_null
    public $pmCheckPaymentAddress;           // blob(65535)  not_null blob
    public $pmBankName;                      // string(200)  not_null
    public $pmBankPaymentAddress;            // blob(65535)  not_null blob
    public $pmBankAccountNumber;             // string(100)  not_null
    public $pmBankRoutingNumber;             // string(100)  not_null
    public $pmBankAccountName;               // string(200)  not_null
    public $payment_concern;                 // blob(65535)  not_null blob
    public $how_dou_know;                    // string(200)  not_null
    public $detail;                          // blob(65535)  not_null blob
    public $parent_id;                       // int(11)  not_null
    public $created_on;                      // datetime(19)  not_null binary
    public $status;                          // string(1)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Hp_signup',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
