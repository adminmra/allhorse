<?php	 		 	
/**
 * Table Definition for imagecache_preset
 */
require_once 'DB/DataObject.php';

class DataObjects_Imagecache_preset extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'imagecache_preset';               // table name
    public $presetid;                        // int(10)  not_null primary_key unsigned auto_increment
    public $presetname;                      // string(255)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Imagecache_preset',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
