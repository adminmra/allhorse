<?php	 		 	
/**
 * Table Definition for color_manage_lines
 */
require_once 'DB/DataObject.php';

class DataObjects_Color_manage_lines extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'color_manage_lines';              // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $web_id;                          // int(11)  not_null
    public $lines_id;                        // int(11)  not_null
    public $l_back_color;                    // string(50)  not_null
    public $l_front_color;                   // string(50)  not_null
    public $status;                          // string(1)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Color_manage_lines',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
