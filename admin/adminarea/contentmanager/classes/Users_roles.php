<?php	 		 	
/**
 * Table Definition for users_roles
 */
require_once 'DB/DataObject.php';

class DataObjects_Users_roles extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'users_roles';                     // table name
    public $uid;                             // int(10)  not_null primary_key unsigned
    public $rid;                             // int(10)  not_null primary_key multiple_key unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Users_roles',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
