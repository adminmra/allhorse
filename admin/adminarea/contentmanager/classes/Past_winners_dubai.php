<?php	 		 	
/**
 * Table Definition for past_winners_dubai
 */
require_once 'DB/DataObject.php';

class DataObjects_Past_winners_dubai extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'past_winners_dubai';              // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $year;                            // int(11)  not_null
    public $winner;                          // string(50)  not_null
    public $age;                             // int(11)  not_null
    public $jockey;                          // string(50)  not_null
    public $traineer;                        // string(50)  not_null
    public $owner;                           // string(50)  not_null
    public $time;                            // string(50)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Past_winners_dubai',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
