<?php	 		 	
/**
 * Table Definition for taxonomy_breadcrumb_vocabulary
 */
require_once 'DB/DataObject.php';

class DataObjects_Taxonomy_breadcrumb_vocabulary extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'taxonomy_breadcrumb_vocabulary';    // table name
    public $vid;                             // int(11)  not_null primary_key
    public $path;                            // string(128)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Taxonomy_breadcrumb_vocabulary',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
