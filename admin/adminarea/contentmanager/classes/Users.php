<?php	 		 	
/**
 * Table Definition for users
 */
require_once 'DB/DataObject.php';

class DataObjects_Users extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'users';                           // table name
    public $uid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $name;                            // string(60)  not_null unique_key
    public $pass;                            // string(32)  not_null
    public $mail;                            // string(64)  multiple_key
    public $mode;                            // int(4)  not_null
    public $sort;                            // int(4)  
    public $threshold;                       // int(4)  
    public $theme;                           // string(255)  not_null
    public $signature;                       // string(255)  not_null
    public $signature_format;                // int(6)  not_null
    public $created;                         // int(11)  not_null multiple_key
    public $access;                          // int(11)  not_null multiple_key
    public $login;                           // int(11)  not_null
    public $status;                          // int(4)  not_null
    public $timezone;                        // string(8)  
    public $language;                        // string(12)  not_null
    public $picture;                         // string(255)  not_null
    public $init;                            // string(64)  
    public $data;                            // blob(4294967295)  blob
    public $timezone_name;                   // string(50)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Users',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
