<?php	 		 	
/**
 * Table Definition for search_dataset
 */
require_once 'DB/DataObject.php';

class DataObjects_Search_dataset extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'search_dataset';                  // table name
    public $sid;                             // int(10)  not_null multiple_key unsigned
    public $type;                            // string(16)  
    public $data;                            // blob(4294967295)  not_null blob
    public $reindex;                         // int(10)  not_null unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Search_dataset',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
