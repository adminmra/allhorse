<?php	 		 	
/**
 * Table Definition for multiblock
 */
require_once 'DB/DataObject.php';

class DataObjects_Multiblock extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'multiblock';                      // table name
    public $delta;                           // int(11)  not_null primary_key auto_increment
    public $title;                           // string(64)  not_null
    public $module;                          // string(64)  not_null
    public $orig_delta;                      // string(32)  not_null
    public $multi_settings;                  // int(3)  not_null unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Multiblock',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
