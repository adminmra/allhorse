<?php	 		 	
/**
 * Table Definition for ad_countdown
 */
require_once 'DB/DataObject.php';

class DataObjects_Ad_countdown extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ad_countdown';                    // table name
    public $aid;                             // int(10)  not_null primary_key unsigned
    public $fid;                             // int(10)  not_null unsigned
    public $url;                             // string(255)  not_null
    public $tooltip;                         // string(255)  not_null
    public $remote_image;                    // string(255)  not_null
    public $width;                           // int(10)  not_null unsigned
    public $height;                          // int(10)  not_null unsigned
    public $countdown_ts;                    // datetime(19)  binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ad_countdown',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
