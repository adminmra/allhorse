<?php	 		 	
/**
 * Table Definition for schedule
 */
require_once 'DB/DataObject.php';

class DataObjects_Schedule extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'schedule';                        // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $data;                            // blob(65535)  not_null blob
    public $addedon;                         // datetime(19)  not_null binary
    public $type;                            // int(6)  not_null
    public $display;                         // string(1)  not_null enum

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Schedule',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
