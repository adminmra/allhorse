<?php	 		 	
/**
 * Table Definition for racingchannel
 */
require_once 'DB/DataObject.php';

class DataObjects_Racingchannel extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'racingchannel';                   // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $URL;                             // string(100)  
    public $RaceTrack;                       // string(50)  
    public $RaceDate;                        // string(50)  
    public $RaceNumber;                      // string(50)  
    public $HorseNumber1;                    // string(50)  
    public $HorseName1;                      // string(50)  
    public $Win1;                            // string(50)  
    public $Place1;                          // string(50)  
    public $Show1;                           // string(50)  
    public $HorseNumber2;                    // string(50)  
    public $HorseName2;                      // string(50)  
    public $Win2;                            // string(50)  
    public $Place2;                          // string(50)  
    public $Show2;                           // string(50)  
    public $HorseNumber3;                    // string(50)  
    public $HorseName3;                      // string(50)  
    public $Win3;                            // string(50)  
    public $Place3;                          // string(50)  
    public $Show3;                           // string(50)  
    public $HorseNumber4;                    // string(50)  
    public $HorseName4;                      // string(50)  
    public $Win4;                            // string(50)  
    public $Place4;                          // string(50)  
    public $Show4;                           // string(50)  
    public $D_Double;                        // string(50)  
    public $D_Double_Amount;                 // string(50)  
    public $Exacta;                          // string(50)  
    public $Exacta_Amount;                   // string(50)  
    public $Trifecta;                        // string(50)  
    public $Trifecta_Amount;                 // string(50)  
    public $Superfecta;                      // string(50)  
    public $Superfecta_Amount;               // string(50)  
    public $Quinella;                        // string(50)  
    public $Quinella_Amount;                 // string(50)  
    public $Refunds;                         // string(50)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Racingchannel',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
