<?php	 		 	
/**
 * Table Definition for utube
 */
require_once 'DB/DataObject.php';

class DataObjects_Utube extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'utube';                           // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $entity_id;                       // int(11)  not_null
    public $type;                            // string(1)  not_null enum
    public $url;                             // string(100)  not_null
    public $title;                           // string(100)  not_null
    public $stakes;                          // int(11)  
    public $year;                            // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Utube',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
