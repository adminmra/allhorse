<?php	 		 	
/**
 * Table Definition for tblcontestL
 */
require_once 'DB/DataObject.php';

class DataObjects_TblcontestL extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tblcontestL';                     // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $perfomance_id;                   // string(10)  not_null
    public $contestno;                       // int(11)  not_null
    public $contestents;                     // int(11)  not_null
    public $live;                            // string(100)  not_null
    public $scratched;                       // string(100)  not_null
    public $pools;                           // string(100)  not_null
    public $posttime;                        // string(25)  not_null
    public $purse;                           // string(100)  not_null
    public $info;                            // blob(65535)  not_null blob
    public $perform_id;                      // int(11)  not_null
    public $status;                          // string(50)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_TblcontestL',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
