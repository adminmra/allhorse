<?php	 		 	
/**
 * Table Definition for url_alias
 */
require_once 'DB/DataObject.php';

class DataObjects_Url_alias extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'url_alias';                       // table name
    public $pid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $src;                             // string(128)  not_null multiple_key
    public $dst;                             // string(128)  not_null multiple_key
    public $language;                        // string(12)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Url_alias',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
