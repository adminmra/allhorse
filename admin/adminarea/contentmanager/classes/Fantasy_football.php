<?php	 		 	
/**
 * Table Definition for fantasy_football
 */
require_once 'DB/DataObject.php';

class DataObjects_Fantasy_football extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'fantasy_football';                // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $week;                            // int(4)  not_null
    public $place;                           // int(4)  not_null
    public $name;                            // string(100)  not_null
    public $points;                          // int(9)  not_null
    public $prize;                           // int(9)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Fantasy_football',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
