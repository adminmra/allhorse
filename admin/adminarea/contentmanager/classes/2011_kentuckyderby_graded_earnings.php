<?php	 		 	
/**
 * Table Definition for 2011_kentuckyderby_graded_earnings
 */
require_once 'DB/DataObject.php';

class DataObjects_2011_kentuckyderby_graded_earnings extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = '2011_kentuckyderby_graded_earnings';    // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $rank;                            // string(25)  not_null
    public $lastweek;                        // string(25)  not_null
    public $horse;                           // string(75)  not_null
    public $earnings;                        // string(75)  not_null
    public $owner;                           // string(75)  not_null
    public $trainer;                         // string(75)  not_null
    public $updatedate;                      // date(10)  not_null binary
    public $ranksort;                        // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_2011_kentuckyderby_graded_earnings',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
