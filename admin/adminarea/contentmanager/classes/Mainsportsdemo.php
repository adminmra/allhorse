<?php	 		 	
/**
 * Table Definition for mainsportsdemo
 */
require_once 'DB/DataObject.php';

class DataObjects_Mainsportsdemo extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'mainsportsdemo';                  // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $name;                            // string(75)  not_null
    public $active;                          // string(1)  not_null enum
    public $sportscat;                       // int(4)  not_null
    public $position;                        // int(4)  not_null
    public $columnno;                        // int(4)  not_null
    public $notes;                           // blob(65535)  not_null blob
    public $extra;                           // string(1)  not_null enum
    public $typeof;                          // string(1)  not_null enum
    public $isModule;                        // string(1)  not_null enum
    public $textdata;                        // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Mainsportsdemo',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
