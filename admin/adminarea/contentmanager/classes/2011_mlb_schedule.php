<?php	 		 	
/**
 * Table Definition for 2011_mlb_schedule
 */
require_once 'DB/DataObject.php';

class DataObjects_2011_mlb_schedule extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = '2011_mlb_schedule';               // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $race;                            // string(255)  not_null
    public $time;                            // string(50)  not_null
    public $dates;                           // date(10)  not_null binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_2011_mlb_schedule',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
