<?php	 		 	
/**
 * Table Definition for tbl_variables_databaserules
 */
require_once 'DB/DataObject.php';

class DataObjects_Tbl_variables_databaserules extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tbl_variables_databaserules';     // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $text_id;                         // int(11)  not_null
    public $AHR;                             // string(100)  not_null
    public $GHB;                             // string(100)  not_null
    public $text_var;                        // string(100)  not_null
    public $harness;                         // string(100)  not_null
    public $created_on;                      // datetime(19)  not_null binary
    public $status;                          // string(1)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tbl_variables_databaserules',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
