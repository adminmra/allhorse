<?php	 		 	
/**
 * Table Definition for 2008_melbourne_result
 */
require_once 'DB/DataObject.php';

class DataObjects_2008_melbourne_result extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = '2008_melbourne_result';           // table name
    public $id;                              // int(4)  not_null primary_key auto_increment
    public $result;                          // int(11)  
    public $horse;                           // string(75)  not_null
    public $jockey;                          // string(75)  not_null
    public $trainer;                         // string(75)  not_null
    public $wg;                              // string(50)  not_null
    public $margin;                          // string(50)  not_null
    public $odds;                            // string(50)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_2008_melbourne_result',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
