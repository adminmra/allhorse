<?php	 		 	
/**
 * Table Definition for comments
 */
require_once 'DB/DataObject.php';

class DataObjects_Comments extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'comments';                        // table name
    public $cid;                             // int(11)  not_null primary_key auto_increment
    public $pid;                             // int(11)  not_null multiple_key
    public $nid;                             // int(11)  not_null multiple_key
    public $uid;                             // int(11)  not_null
    public $subject;                         // string(64)  not_null
    public $comment;                         // blob(4294967295)  not_null blob
    public $hostname;                        // string(128)  not_null
    public $timestamp;                       // int(11)  not_null
    public $status;                          // int(3)  not_null multiple_key unsigned
    public $format;                          // int(6)  not_null
    public $thread;                          // string(255)  not_null
    public $name;                            // string(60)  
    public $mail;                            // string(64)  
    public $homepage;                        // string(255)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Comments',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
