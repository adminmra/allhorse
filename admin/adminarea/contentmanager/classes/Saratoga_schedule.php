<?php	 		 	
/**
 * Table Definition for saratoga_schedule
 */
require_once 'DB/DataObject.php';

class DataObjects_Saratoga_schedule extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'saratoga_schedule';               // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $eventorder;                      // int(11)  not_null
    public $dates;                           // string(150)  not_null
    public $name;                            // string(175)  not_null
    public $grade;                           // string(15)  not_null
    public $event;                           // string(45)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Saratoga_schedule',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
