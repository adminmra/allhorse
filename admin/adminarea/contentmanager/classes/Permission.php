<?php	 		 	
/**
 * Table Definition for permission
 */
require_once 'DB/DataObject.php';

class DataObjects_Permission extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'permission';                      // table name
    public $pid;                             // int(11)  not_null primary_key auto_increment
    public $rid;                             // int(10)  not_null multiple_key unsigned
    public $perm;                            // blob(4294967295)  blob
    public $tid;                             // int(10)  not_null unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Permission',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
