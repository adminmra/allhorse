<?php	 		 	
/**
 * Table Definition for race_schedule
 */
require_once 'DB/DataObject.php';

class DataObjects_Race_schedule extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'race_schedule';                   // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $racedate;                        // date(10)  not_null binary
    public $race;                            // string(150)  not_null
    public $grade;                           // string(15)  not_null
    public $distance;                        // string(125)  not_null
    public $age;                             // string(25)  not_null
    public $track;                           // string(125)  not_null
    public $timeet;                          // string(25)  not_null
    public $purse;                           // string(150)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Race_schedule',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
