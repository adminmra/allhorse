<?php	 		 	
/**
 * Table Definition for themekey_paths
 */
require_once 'DB/DataObject.php';

class DataObjects_Themekey_paths extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'themekey_paths';                  // table name
    public $id;                              // int(10)  not_null primary_key unsigned auto_increment
    public $path;                            // string(255)  not_null multiple_key
    public $fit;                             // int(11)  not_null multiple_key
    public $weight;                          // int(11)  not_null multiple_key
    public $wildcards;                       // blob(4294967295)  not_null blob
    public $conditions;                      // blob(4294967295)  not_null blob
    public $custom;                          // int(11)  not_null multiple_key
    public $theme;                           // string(255)  not_null
    public $callbacks;                       // blob(4294967295)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Themekey_paths',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
