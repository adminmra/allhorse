<?php	 		 	
/**
 * Table Definition for watchdog
 */
require_once 'DB/DataObject.php';

class DataObjects_Watchdog extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'watchdog';                        // table name
    public $wid;                             // int(11)  not_null primary_key auto_increment
    public $uid;                             // int(11)  not_null
    public $type;                            // string(16)  not_null multiple_key
    public $message;                         // blob(4294967295)  not_null blob
    public $variables;                       // blob(4294967295)  not_null blob
    public $severity;                        // int(3)  not_null unsigned
    public $link;                            // string(255)  not_null
    public $location;                        // blob(65535)  not_null blob
    public $referer;                         // blob(65535)  blob
    public $hostname;                        // string(128)  not_null
    public $timestamp;                       // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Watchdog',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
