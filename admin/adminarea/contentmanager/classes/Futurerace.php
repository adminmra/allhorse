<?php	 		 	
/**
 * Table Definition for futurerace
 */
require_once 'DB/DataObject.php';

class DataObjects_Futurerace extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'futurerace';                      // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $url;                             // string(250)  not_null
    public $racedate;                        // date(10)  not_null multiple_key binary
    public $track;                           // string(100)  not_null
    public $location;                        // string(100)  not_null
    public $entry_draw;                      // string(75)  not_null
    public $scratch;                         // string(100)  not_null
    public $zonetime;                        // string(100)  not_null
    public $race1;                           // string(50)  not_null
    public $race2;                           // string(50)  not_null
    public $race3;                           // string(50)  not_null
    public $race4;                           // string(50)  not_null
    public $race5;                           // string(50)  not_null
    public $race6;                           // string(50)  not_null
    public $race7;                           // string(50)  not_null
    public $race8;                           // string(50)  not_null
    public $race9;                           // string(50)  not_null
    public $race10;                          // string(50)  not_null
    public $race11;                          // string(50)  not_null
    public $race12;                          // string(50)  not_null
    public $race13;                          // string(50)  not_null
    public $race14;                          // string(50)  not_null
    public $race15;                          // string(50)  not_null
    public $stakes;                          // string(120)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Futurerace',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
