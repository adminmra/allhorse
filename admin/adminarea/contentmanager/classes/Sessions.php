<?php	 		 	
/**
 * Table Definition for sessions
 */
require_once 'DB/DataObject.php';

class DataObjects_Sessions extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'sessions';                        // table name
    public $uid;                             // int(10)  not_null multiple_key unsigned
    public $sid;                             // string(64)  not_null primary_key
    public $hostname;                        // string(128)  not_null
    public $timestamp;                       // int(11)  not_null multiple_key
    public $cache;                           // int(11)  not_null
    public $session;                         // blob(4294967295)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Sessions',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
