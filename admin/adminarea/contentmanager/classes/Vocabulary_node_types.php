<?php	 		 	
/**
 * Table Definition for vocabulary_node_types
 */
require_once 'DB/DataObject.php';

class DataObjects_Vocabulary_node_types extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'vocabulary_node_types';           // table name
    public $vid;                             // int(10)  not_null primary_key multiple_key unsigned
    public $type;                            // string(32)  not_null primary_key

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Vocabulary_node_types',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
