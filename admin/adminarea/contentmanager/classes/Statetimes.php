<?php	 		 	
/**
 * Table Definition for statetimes
 */
require_once 'DB/DataObject.php';

class DataObjects_Statetimes extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'statetimes';                      // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $state;                           // string(50)  not_null
    public $abb;                             // string(50)  not_null
    public $diff;                            // int(11)  not_null
    public $daylight_diff;                   // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Statetimes',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
