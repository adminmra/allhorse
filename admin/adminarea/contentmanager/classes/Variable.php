<?php	 		 	
/**
 * Table Definition for variable
 */
require_once 'DB/DataObject.php';

class DataObjects_Variable extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'variable';                        // table name
    public $name;                            // string(128)  not_null primary_key
    public $value;                           // blob(4294967295)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Variable',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
