<?php	 		 	
/**
 * Table Definition for ipademail
 */
require_once 'DB/DataObject.php';

class DataObjects_Ipademail extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ipademail';                       // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $name;                            // string(75)  not_null
    public $email;                           // string(75)  not_null
    public $site;                            // string(75)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ipademail',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
