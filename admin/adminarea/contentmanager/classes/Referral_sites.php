<?php	 		 	
/**
 * Table Definition for referral_sites
 */
require_once 'DB/DataObject.php';

class DataObjects_Referral_sites extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'referral_sites';                  // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $sitename;                        // string(150)  not_null
    public $host;                            // string(100)  not_null
    public $timecreate;                      // timestamp(19)  not_null unsigned zerofill binary timestamp
    public $referfrom;                       // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Referral_sites',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
