<?php	 		 	
/**
 * Table Definition for views_view
 */
require_once 'DB/DataObject.php';

class DataObjects_Views_view extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'views_view';                      // table name
    public $vid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $name;                            // string(32)  not_null unique_key
    public $description;                     // string(255)  
    public $tag;                             // string(255)  
    public $view_php;                        // blob(65535)  blob binary
    public $base_table;                      // string(64)  not_null
    public $is_cacheable;                    // int(4)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Views_view',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
