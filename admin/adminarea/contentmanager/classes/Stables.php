<?php	 		 	
/**
 * Table Definition for stables
 */
require_once 'DB/DataObject.php';

class DataObjects_Stables extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'stables';                         // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $name;                            // string(50)  not_null
    public $street1;                         // string(50)  
    public $street2;                         // string(50)  
    public $city;                            // string(50)  
    public $state;                           // string(50)  
    public $zip;                             // string(50)  
    public $country;                         // string(50)  
    public $phone1;                          // string(50)  
    public $phone2;                          // string(50)  
    public $url;                             // blob(4294967295)  blob
    public $email;                           // string(50)  
    public $services;                        // blob(4294967295)  blob
    public $contact;                         // string(50)  
    public $notes;                           // blob(4294967295)  blob
    public $family;                          // blob(4294967295)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Stables',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
