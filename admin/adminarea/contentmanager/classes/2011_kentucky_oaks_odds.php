<?php	 		 	
/**
 * Table Definition for 2011_kentucky_oaks_odds
 */
require_once 'DB/DataObject.php';

class DataObjects_2011_kentucky_oaks_odds extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = '2011_kentucky_oaks_odds';         // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $dat;                             // string(50)  not_null
    public $race;                            // string(50)  not_null
    public $track;                           // string(50)  not_null
    public $grade;                           // string(50)  not_null
    public $dist;                            // string(50)  not_null
    public $purse;                           // string(50)  not_null
    public $earnings;                        // string(100)  not_null
    public $breeder;                         // string(100)  not_null
    public $odd;                             // string(25)  not_null
    public $resulttime;                      // string(50)  not_null
    public $result;                          // int(11)  
    public $post;                            // int(11)  
    public $updatedate;                      // date(10)  not_null binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_2011_kentucky_oaks_odds',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
