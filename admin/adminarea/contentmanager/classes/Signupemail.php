<?php	 		 	
/**
 * Table Definition for signupemail
 */
require_once 'DB/DataObject.php';

class DataObjects_Signupemail extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'signupemail';                     // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $subject;                         // string(250)  not_null
    public $body;                            // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Signupemail',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
