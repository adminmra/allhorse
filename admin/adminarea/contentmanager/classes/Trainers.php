<?php	 		 	
/**
 * Table Definition for trainers
 */
require_once 'DB/DataObject.php';

class DataObjects_Trainers extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'trainers';                        // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $name;                            // string(50)  
    public $city;                            // string(50)  
    public $state;                           // string(50)  
    public $country;                         // string(50)  
    public $family;                          // blob(4294967295)  blob
    public $notes;                           // blob(65535)  not_null blob
    public $pic;                             // string(90)  
    public $url;                             // string(50)  
    public $birthdate;                       // date(10)  binary
    public $birthcity;                       // string(50)  
    public $birthstate;                      // string(50)  
    public $birthcountry;                    // string(50)  
    public $GHBnotes;                        // blob(4294967295)  not_null blob
    public $HBnotes;                         // blob(4294967295)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Trainers',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
