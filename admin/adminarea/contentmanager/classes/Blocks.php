<?php	 		 	
/**
 * Table Definition for blocks
 */
require_once 'DB/DataObject.php';

class DataObjects_Blocks extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'blocks';                          // table name
    public $bid;                             // int(11)  not_null primary_key auto_increment
    public $module;                          // string(64)  not_null
    public $delta;                           // string(32)  not_null
    public $theme;                           // string(64)  not_null multiple_key
    public $status;                          // int(4)  not_null
    public $weight;                          // int(4)  not_null
    public $region;                          // string(64)  not_null
    public $custom;                          // int(4)  not_null
    public $throttle;                        // int(4)  not_null
    public $visibility;                      // int(4)  not_null
    public $pages;                           // blob(65535)  not_null blob
    public $title;                           // string(64)  not_null
    public $cache;                           // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Blocks',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
