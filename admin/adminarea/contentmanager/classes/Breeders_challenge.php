<?php	 		 	
/**
 * Table Definition for breeders_challenge
 */
require_once 'DB/DataObject.php';

class DataObjects_Breeders_challenge extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'breeders_challenge';              // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $racedate;                        // date(10)  not_null binary
    public $race;                            // string(75)  not_null
    public $noms;                            // string(75)  not_null
    public $track;                           // string(75)  not_null
    public $division;                        // string(75)  not_null
    public $tv;                              // string(75)  not_null
    public $winner;                          // string(75)  not_null
    public $distance;                        // string(75)  not_null
    public $breeders_id;                     // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Breeders_challenge',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
