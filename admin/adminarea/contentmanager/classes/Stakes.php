<?php	 		 	
/**
 * Table Definition for stakes
 */
require_once 'DB/DataObject.php';

class DataObjects_Stakes extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'stakes';                          // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $trackcode;                       // string(50)  multiple_key
    public $date;                            // date(10)  binary
    public $name;                            // string(50)  not_null
    public $grade;                           // string(50)  
    public $purse;                           // string(50)  
    public $hta;                             // string(50)  
    public $distance;                        // string(50)  
    public $horseage;                        // string(50)  
    public $stakessex;                       // string(50)  
    public $turf;                            // string(50)  
    public $notes;                           // blob(4294967295)  blob
    public $posttime;                        // string(50)  
    public $logo;                            // string(200)  not_null
    public $GHBnotes;                        // blob(4294967295)  not_null blob
    public $HBnotes;                         // blob(4294967295)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Stakes',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
