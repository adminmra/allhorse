<?php	 		 	
/**
 * Table Definition for 2006dubaiworldcup
 */
require_once 'DB/DataObject.php';

class DataObjects_2006dubaiworldcup extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = '2006dubaiworldcup';               // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $name;                            // string(75)  not_null
    public $age_weight;                      // string(75)  not_null
    public $purse;                           // int(11)  not_null
    public $distance;                        // string(75)  not_null
    public $surface;                         // string(75)  not_null
    public $post;                            // string(75)  not_null
    public $grade;                           // string(75)  not_null
    public $turf;                            // string(75)  not_null
    public $type;                            // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_2006dubaiworldcup',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
