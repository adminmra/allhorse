<?php	 		 	
/**
 * Table Definition for contestant
 */
require_once 'DB/DataObject.php';

class DataObjects_Contestant extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'contestant';                      // table name
    public $contestant_id;                   // int(11)  not_null primary_key auto_increment
    public $contest_id;                      // int(11)  not_null
    public $horse;                           // string(50)  not_null
    public $jockey;                          // string(50)  not_null
    public $trainer;                         // string(50)  not_null
    public $morning_odds;                    // string(50)  not_null
    public $current_odds;                    // string(50)  not_null
    public $equip;                           // string(50)  not_null
    public $med;                             // string(50)  not_null
    public $climingprice;                    // real(8)  not_null
    public $post_position;                   // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Contestant',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
