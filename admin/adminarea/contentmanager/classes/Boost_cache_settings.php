<?php	 		 	
/**
 * Table Definition for boost_cache_settings
 */
require_once 'DB/DataObject.php';

class DataObjects_Boost_cache_settings extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'boost_cache_settings';            // table name
    public $csid;                            // int(10)  not_null primary_key unsigned auto_increment
    public $base_dir;                        // string(128)  not_null multiple_key
    public $page_callback;                   // string(255)  not_null multiple_key
    public $page_type;                       // string(255)  not_null multiple_key
    public $page_id;                         // string(64)  not_null multiple_key
    public $extension;                       // string(8)  not_null multiple_key
    public $lifetime;                        // int(11)  not_null
    public $push;                            // int(6)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Boost_cache_settings',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
