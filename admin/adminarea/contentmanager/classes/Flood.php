<?php	 		 	
/**
 * Table Definition for flood
 */
require_once 'DB/DataObject.php';

class DataObjects_Flood extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'flood';                           // table name
    public $fid;                             // int(11)  not_null primary_key auto_increment
    public $event;                           // string(64)  not_null multiple_key
    public $hostname;                        // string(128)  not_null
    public $timestamp;                       // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Flood',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
