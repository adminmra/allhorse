<?php	 		 	
/**
 * Table Definition for morning_odds
 */
require_once 'DB/DataObject.php';

class DataObjects_Morning_odds extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'morning_odds';                    // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $contest;                         // string(50)  not_null
    public $raceno;                          // int(11)  not_null
    public $odds;                            // string(200)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Morning_odds',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
