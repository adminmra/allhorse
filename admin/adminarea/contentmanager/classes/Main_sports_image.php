<?php	 		 	
/**
 * Table Definition for main_sports_image
 */
require_once 'DB/DataObject.php';

class DataObjects_Main_sports_image extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'main_sports_image';               // table name
    public $id;                              // int(10)  not_null primary_key auto_increment
    public $main_id;                         // int(11)  not_null
    public $web_id;                          // int(10)  not_null
    public $img_path;                        // string(100)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Main_sports_image',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
