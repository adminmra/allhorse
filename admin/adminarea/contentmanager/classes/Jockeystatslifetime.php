<?php	 		 	
/**
 * Table Definition for jockeystatslifetime
 */
require_once 'DB/DataObject.php';

class DataObjects_Jockeystatslifetime extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'jockeystatslifetime';             // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $jockey_id;                       // int(20)  not_null
    public $earnings;                        // string(50)  
    public $racesrun;                        // int(11)  
    public $wins;                            // int(11)  
    public $places;                          // int(11)  
    public $shows;                           // int(11)  
    public $notes;                           // blob(4294967295)  blob
    public $winperc;                         // real(22)  
    public $wpsperc;                         // real(22)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Jockeystatslifetime',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
