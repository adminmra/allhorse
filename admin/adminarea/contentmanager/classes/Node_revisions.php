<?php	 		 	
/**
 * Table Definition for node_revisions
 */
require_once 'DB/DataObject.php';

class DataObjects_Node_revisions extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'node_revisions';                  // table name
    public $nid;                             // int(10)  not_null multiple_key unsigned
    public $vid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $uid;                             // int(11)  not_null multiple_key
    public $title;                           // string(255)  not_null
    public $body;                            // blob(4294967295)  not_null blob
    public $teaser;                          // blob(4294967295)  not_null blob
    public $log;                             // blob(4294967295)  not_null blob
    public $timestamp;                       // int(11)  not_null
    public $format;                          // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Node_revisions',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
