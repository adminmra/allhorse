<?php	 		 	
/**
 * Table Definition for es_calendar2
 */
require_once 'DB/DataObject.php';

class DataObjects_Es_calendar2 extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'es_calendar2';                    // table name
    public $id;                              // int(7)  not_null primary_key auto_increment
    public $startDate;                       // date(10)  not_null binary
    public $endDate;                         // date(10)  not_null binary
    public $startTime;                       // time(8)  not_null binary
    public $endTime;                         // time(8)  not_null binary
    public $type;                            // blob(65535)  not_null blob
    public $repeat;                          // int(7)  not_null
    public $title;                           // blob(65535)  not_null blob
    public $descr;                           // blob(65535)  not_null blob
    public $days;                            // int(7)  not_null
    public $stop;                            // int(7)  not_null
    public $month;                           // int(2)  not_null
    public $weekDay;                         // int(1)  not_null
    public $weekNumber;                      // int(1)  not_null
    public $colorCode;                       // int(4)  not_null
    public $altLink;                         // blob(65535)  not_null blob
    public $eventKey;                        // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Es_calendar2',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
