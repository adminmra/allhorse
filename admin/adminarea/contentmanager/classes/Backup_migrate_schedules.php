<?php	 		 	
/**
 * Table Definition for backup_migrate_schedules
 */
require_once 'DB/DataObject.php';

class DataObjects_Backup_migrate_schedules extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'backup_migrate_schedules';        // table name
    public $schedule_id;                     // string(32)  not_null primary_key
    public $name;                            // string(255)  not_null
    public $source_id;                       // string(32)  not_null
    public $destination_id;                  // string(32)  not_null
    public $profile_id;                      // string(32)  not_null
    public $keep;                            // int(11)  not_null
    public $period;                          // int(11)  not_null
    public $last_run;                        // int(11)  not_null
    public $enabled;                         // int(3)  not_null unsigned
    public $cron;                            // int(3)  not_null unsigned

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Backup_migrate_schedules',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
