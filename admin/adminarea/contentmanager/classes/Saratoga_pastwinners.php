<?php	 		 	
/**
 * Table Definition for saratoga_pastwinners
 */
require_once 'DB/DataObject.php';

class DataObjects_Saratoga_pastwinners extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'saratoga_pastwinners';            // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $race_id;                         // int(11)  not_null
    public $year;                            // int(11)  not_null
    public $horse;                           // string(150)  not_null
    public $wt;                              // int(11)  not_null
    public $time;                            // string(25)  not_null
    public $owner;                           // string(150)  not_null
    public $trainer;                         // string(150)  not_null
    public $jockey;                          // string(150)  not_null
    public $value;                           // string(50)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Saratoga_pastwinners',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
