<?php	 		 	
/**
 * Table Definition for horsestatslifetime
 */
require_once 'DB/DataObject.php';

class DataObjects_Horsestatslifetime extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'horsestatslifetime';              // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $horse_id;                        // int(20)  not_null
    public $earnings;                        // string(50)  
    public $starts;                          // int(11)  
    public $wins;                            // int(11)  
    public $places;                          // int(11)  
    public $shows;                           // int(11)  
    public $notes;                           // blob(4294967295)  blob
    public $winperc;                         // real(22)  
    public $wpsperc;                         // real(22)  

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Horsestatslifetime',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
