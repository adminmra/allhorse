<?php	 		 	
/**
 * Table Definition for handicappers
 */
require_once 'DB/DataObject.php';

class DataObjects_Handicappers extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'handicappers';                    // table name
    public $id;                              // int(20)  not_null primary_key auto_increment
    public $company_name;                    // string(100)  
    public $contact;                         // blob(65535)  blob
    public $phone;                           // string(50)  
    public $phone2;                          // string(50)  
    public $street;                          // string(50)  
    public $street1;                         // string(50)  
    public $city;                            // string(50)  
    public $state;                           // string(50)  
    public $country;                         // string(50)  
    public $email;                           // string(50)  
    public $url;                             // string(50)  
    public $info;                            // blob(65535)  blob
    public $coments;                         // blob(65535)  blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Handicappers',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
