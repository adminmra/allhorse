<?php	 		 	
/**
 * Table Definition for actions
 */
require_once 'DB/DataObject.php';

class DataObjects_Actions extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'actions';                         // table name
    public $aid;                             // string(255)  not_null primary_key
    public $type;                            // string(32)  not_null
    public $callback;                        // string(255)  not_null
    public $parameters;                      // blob(4294967295)  not_null blob
    public $description;                     // string(255)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Actions',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
