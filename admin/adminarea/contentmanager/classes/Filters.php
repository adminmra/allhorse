<?php	 		 	
/**
 * Table Definition for filters
 */
require_once 'DB/DataObject.php';

class DataObjects_Filters extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'filters';                         // table name
    public $fid;                             // int(11)  not_null primary_key auto_increment
    public $format;                          // int(11)  not_null multiple_key
    public $module;                          // string(64)  not_null
    public $delta;                           // int(4)  not_null
    public $weight;                          // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Filters',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
