<?php	 		 	
/**
 * Table Definition for contact
 */
require_once 'DB/DataObject.php';

class DataObjects_Contact extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'contact';                         // table name
    public $cid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $category;                        // string(255)  not_null unique_key
    public $recipients;                      // blob(4294967295)  not_null blob
    public $reply;                           // blob(4294967295)  not_null blob
    public $weight;                          // int(4)  not_null multiple_key
    public $selected;                        // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Contact',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
