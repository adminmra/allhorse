<?php	 		 	
/**
 * Table Definition for node
 */
require_once 'DB/DataObject.php';

class DataObjects_Node extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'node';                            // table name
    public $nid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $vid;                             // int(10)  not_null unique_key unsigned
    public $type;                            // string(32)  not_null multiple_key
    public $language;                        // string(12)  not_null
    public $title;                           // string(255)  not_null multiple_key
    public $uid;                             // int(11)  not_null multiple_key
    public $status;                          // int(11)  not_null multiple_key
    public $created;                         // int(11)  not_null multiple_key
    public $changed;                         // int(11)  not_null multiple_key
    public $comment;                         // int(11)  not_null
    public $promote;                         // int(11)  not_null multiple_key
    public $moderate;                        // int(11)  not_null multiple_key
    public $sticky;                          // int(11)  not_null
    public $tnid;                            // int(10)  not_null multiple_key unsigned
    public $translate;                       // int(11)  not_null multiple_key

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Node',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
