<?php	 		 	
/**
 * Table Definition for 2010_kentucky_prep
 */
require_once 'DB/DataObject.php';

class DataObjects_2010_kentucky_prep extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = '2010_kentucky_prep';              // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $racedate;                        // string(50)  not_null
    public $race;                            // string(50)  not_null
    public $racetrack;                       // string(50)  not_null
    public $grade;                           // string(50)  not_null
    public $purse;                           // string(50)  not_null
    public $distance;                        // string(50)  not_null
    public $horse;                           // string(50)  not_null
    public $jockey;                          // string(50)  not_null
    public $coverage;                        // string(50)  not_null
    public $win;                             // string(100)  not_null
    public $place;                           // string(100)  not_null
    public $showwin;                         // string(100)  not_null
    public $racetime;                        // string(75)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_2010_kentucky_prep',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
