<?php	 		 	
/**
 * Table Definition for Breederscuprace
 */
require_once 'DB/DataObject.php';

class DataObjects_Breederscuprace extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'Breederscuprace';                 // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $race;                            // string(150)  not_null
    public $race_id;                         // int(11)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Breederscuprace',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
