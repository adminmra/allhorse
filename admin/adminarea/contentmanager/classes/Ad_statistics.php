<?php	 		 	
/**
 * Table Definition for ad_statistics
 */
require_once 'DB/DataObject.php';

class DataObjects_Ad_statistics extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'ad_statistics';                   // table name
    public $sid;                             // int(10)  not_null primary_key unsigned auto_increment
    public $aid;                             // int(10)  not_null multiple_key unsigned
    public $date;                            // int(10)  not_null multiple_key unsigned
    public $action;                          // string(255)  not_null multiple_key
    public $adgroup;                         // string(255)  multiple_key
    public $hostid;                          // string(32)  not_null multiple_key
    public $count;                           // int(10)  not_null unsigned
    public $extra;                           // string(255)  not_null multiple_key

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Ad_statistics',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
