<?php	 		 	
/**
 * Table Definition for content_node_field
 */
require_once 'DB/DataObject.php';

class DataObjects_Content_node_field extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'content_node_field';              // table name
    public $field_name;                      // string(32)  not_null primary_key
    public $type;                            // string(127)  not_null
    public $global_settings;                 // blob(16777215)  not_null blob
    public $required;                        // int(4)  not_null
    public $multiple;                        // int(4)  not_null
    public $db_storage;                      // int(4)  not_null
    public $module;                          // string(127)  not_null
    public $db_columns;                      // blob(16777215)  not_null blob
    public $active;                          // int(4)  not_null
    public $locked;                          // int(4)  not_null

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Content_node_field',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
