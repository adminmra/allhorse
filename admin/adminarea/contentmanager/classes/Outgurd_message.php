<?php	 		 	
/**
 * Table Definition for outgurd_message
 */
require_once 'DB/DataObject.php';

class DataObjects_Outgurd_message extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'outgurd_message';                 // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $default_msg;                     // blob(65535)  not_null blob
    public $user_conf_msg;                   // blob(65535)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Outgurd_message',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
