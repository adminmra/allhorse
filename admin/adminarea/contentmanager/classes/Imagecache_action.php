<?php	 		 	
/**
 * Table Definition for imagecache_action
 */
require_once 'DB/DataObject.php';

class DataObjects_Imagecache_action extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'imagecache_action';               // table name
    public $actionid;                        // int(10)  not_null primary_key unsigned auto_increment
    public $presetid;                        // int(10)  not_null multiple_key unsigned
    public $weight;                          // int(11)  not_null
    public $module;                          // string(255)  not_null
    public $action;                          // string(255)  not_null
    public $data;                            // blob(4294967295)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Imagecache_action',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
