<?php	 		 	
/**
 * Table Definition for tblcontestant
 */
require_once 'DB/DataObject.php';

class DataObjects_Tblcontestant extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'tblcontestant';                   // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $perfomance_id;                   // string(10)  not_null
    public $contestno;                       // int(11)  not_null
    public $contestentsno;                   // int(11)  not_null
    public $contestkey;                      // string(50)  not_null
    public $claim;                           // string(50)  not_null
    public $display;                         // string(75)  not_null
    public $equip;                           // string(75)  not_null
    public $jockey;                          // string(75)  not_null
    public $med;                             // string(75)  not_null
    public $name;                            // string(75)  not_null
    public $odds;                            // string(75)  not_null
    public $post;                            // string(50)  not_null
    public $own;                             // string(75)  not_null
    public $shadow;                          // string(75)  not_null
    public $train;                           // string(75)  not_null
    public $weight;                          // string(75)  not_null
    public $perform_id;                      // int(11)  not_null multiple_key
    public $cotestid;                        // int(11)  not_null multiple_key

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Tblcontestant',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
