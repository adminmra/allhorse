<?php	 		 	
/**
 * Table Definition for nodewords
 */
require_once 'DB/DataObject.php';

class DataObjects_Nodewords extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'nodewords';                       // table name
    public $mtid;                            // int(11)  not_null primary_key auto_increment
    public $type;                            // string(16)  not_null multiple_key
    public $id;                              // string(255)  not_null
    public $name;                            // string(32)  not_null
    public $content;                         // blob(4294967295)  not_null blob

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Nodewords',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
