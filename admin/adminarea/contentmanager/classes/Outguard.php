<?php	 		 	
/**
 * Table Definition for outguard
 */
require_once 'DB/DataObject.php';

class DataObjects_Outguard extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'outguard';                        // table name
    public $id;                              // int(6)  not_null primary_key auto_increment
    public $status;                          // string(4)  not_null enum

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Outguard',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
