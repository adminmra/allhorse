<?php	 		 	
/**
 * Table Definition for top_leader
 */
require_once 'DB/DataObject.php';

class DataObjects_Top_leader extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'top_leader';                      // table name
    public $id;                              // int(11)  not_null primary_key auto_increment
    public $position;                        // int(4)  not_null
    public $name;                            // string(250)  not_null
    public $starts;                          // int(11)  not_null
    public $first;                           // int(11)  
    public $second;                          // int(11)  
    public $third;                           // int(11)  
    public $purse;                           // string(250)  not_null
    public $type;                            // int(4)  not_null
    public $updatedas;                       // date(10)  not_null binary

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Top_leader',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
