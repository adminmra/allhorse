<?php	 		 	
/**
 * Table Definition for date_format_locale
 */
require_once 'DB/DataObject.php';

class DataObjects_Date_format_locale extends DB_DataObject 
{
    ###START_AUTOCODE
    /* the code below is auto generated do not remove the above tag */

    public $__table = 'date_format_locale';              // table name
    public $format;                          // string(100)  not_null binary
    public $type;                            // string(200)  not_null primary_key
    public $language;                        // string(12)  not_null primary_key

    /* Static get */
    function staticGet($k,$v=NULL) { return DB_DataObject::staticGet('DataObjects_Date_format_locale',$k,$v); }

    /* the code above is auto generated do not remove the tag below */
    ###END_AUTOCODE
}
