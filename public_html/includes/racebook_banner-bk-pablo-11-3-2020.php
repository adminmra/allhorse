<!--<div class="leagueIdtable container row w-100 p-0 mb-3">
    <div class="row col-lg-12 p-0">
        <div class="elem one col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=998&breadcrumbId=30_420" target="_top">KENTUCKY DERBY</a></div>
        <div class="elem two col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=88&breadcrumbId=30_480" target="_top">BELMONT STAKES</a></div>    
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=993&breadcrumbId=4107_424" target="_top">TRIPLE CROWN SPECIALS</a></div>  
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=926&breadcrumbId=4107_421" target="_top">KENTUCKY DERBY PROPS</a></div>
    </div>
     <div class="row col-lg-12 p-0">
        <div class="elem five col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1006&breadcrumbId=30_463" target="_top">KENTUCKY OAKS</a> </div>   
        <div class="elem six col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1012&breadcrumbId=30_466" target="_top">PRIX DE L'ARC DE TRIOMPHE</a></div>    
    </div>
    <div class="row col-lg-12 p-0">                
        <div class="elem seven col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=3447&breadcrumbId=4107_461" target="_top">COMMOMWEALTH CUP</a> </div>       
        <div class="elem eight col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=982&breadcrumbId=4107_456" target="_top">MELBOURNE CUP</a></div>       
    </div>
</div>-->
<?php 
    $now = new DateTime("now", new DateTimeZone('America/New_York') );
    $now = $now->format('Y-m-d H:i:s'); //echo $now;
?>
<?php if($now <= '2020-07-13 18:10:00') : ?>
<div class="leagueIdtable container row w-100 p-0 mb-3">
    <div class="row col-lg-12 p-0">
        <div class="elem one col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=998&breadcrumbId=30_420" target="_top">KENTUCKY DERBY</a></div> 
        <div class="elem two col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=926&breadcrumbId=4107_421" target="_top">KENTUCKY DERBY PROPS</a></div>    
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=993&breadcrumbId=4107_424" target="_top">TRIPLE CROWN SPECIALS</a></div>   
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1012&breadcrumbId=30_466" target="_top">PRIX DE L'ARC DE TRIOMPHE</a></div>   
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1006&breadcrumbId=30_463" target="_top">KENTUCKY OAKS</a></div>        
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=982&breadcrumbId=4107_456" target="_top">MELBOURNE CUP</a></div>
    </div>
    <!--
    <div class="row col-lg-12 p-0">                
        <div class="elem seven col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=3447&breadcrumbId=4107_461" target="_top">COMMOMWEALTH CUP</a> </div>          
        <div class="elem eight col-6 col-sm-6 col-md-6 col-lg-6"></div>     
    </div>
-->
</div>
<?php elseif($now <= '2020-10-03 17:00:00') : ?>
<div class="leagueIdtable container row w-100 p-0 mb-3">
    <div class="row col-lg-12 p-0">
     <div class="elem seven col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1012&breadcrumbId=30_466" target="_top">PRIX DE L'ARC DE TRIOMPHE</a></div>   
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=927&breadcrumbId=4107_456" target="_top">Breeders' Cup Classic</a></div>  

    </div>
<?php /*
    <div class="row col-lg-12 p-0">                
               <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=983&breadcrumbId=4542_482" target="_top">PREAKNESS STAKES</a></div>  
        <div class="elem eight col-6 col-sm-6 col-md-6 col-lg-6"></div>     
    </div> */ ?>
</div> 
<?php else : ?>
<div class="leagueIdtable container row w-100 p-0 mb-3">
    <!--<div class="row col-lg-12 p-0">
        <div class="elem one col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=998&breadcrumbId=30_420" target="_top">KENTUCKY DERBY</a></div>   
        <div class="elem two col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=926&breadcrumbId=4107_421" target="_top">KENTUCKY DERBY PROPS</a></div>    
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=993&breadcrumbId=4107_424" target="_top">TRIPLE CROWN SPECIALS</a></div> 
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1012&breadcrumbId=30_466" target="_top">PRIX DE L'ARC DE TRIOMPHE</a></div>   
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1006&breadcrumbId=30_463" target="_top">KENTUCKY OAKS</a> </div>      
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=982&breadcrumbId=4107_456" target="_top">MELBOURNE CUP</a></div>
    </div>

    <div class="row col-lg-12 p-0">
        <div class="elem one col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=927&breadcrumbId=4107_456" target="_top">BREEDERS' CUP CLASSIC</a></div> 
        <div class="elem two col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=998&breadcrumbId=30_420" target="_top">KENTUCKY DERBY</a></div> 
    </div>

    <div class="row col-lg-12 p-0">                
        <div class="elem seven col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1012&breadcrumbId=30_466" target="_top">PRIX DE L'ARC DE TRIOMPHE</a></div>          
        <div class="elem eight col-6 col-sm-6 col-md-6 col-lg-6"></div>     
    </div>-->
    <div class="row col-lg-12 p-0">
        <div class="elem one col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=998&breadcrumbId=4107_420" target="_top">2021 KENTUCKY DERBY</a></div>   
        <div class="elem two col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=3521&breadcrumbId=4107_419" target="_top">HORSE RACING SPECIAL PROPS</a></div>    
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=927&breadcrumbId=4107_429" target="_top">BREEDERS CUP CLASSIC</a></div> 
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=990&breadcrumbId=4107_439" target="_top">BREEDERS CUP JUVENILE</a></div>   
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1014&breadcrumbId=4107_427" target="_top">BREEDERS CUP JUVENILE TURF SPRINT</a> </div>      
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1439&breadcrumbId=4107_432" target="_top">BREEDERS CUP JUVENILE FILLIES TURF</a></div>
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1011&breadcrumbId=4107_434" target="_top">BREEDERS CUP JUVENILE FILLIES</a></div> 
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1438&breadcrumbId=4107_437" target="_top">BREEDERS CUP JUVENILE TURF</a></div>
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1366&breadcrumbId=4107_441" target="_top">BREEDERS CUP FILLY AND MARE SPRINT</a></div> 
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1365&breadcrumbId=4107_444" target="_top">BREEDERS CUP TURF SPRINT</a></div>
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1362&breadcrumbId=4107_447" target="_top">BREEDERS CUP DIRT MILE</a></div> 
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1367&breadcrumbId=4107_452" target="_top">BREEDERS CUP MILE</a></div>
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1368&breadcrumbId=4107_450" target="_top">BREEDERS CUP FILLY AND MARE TURF</a></div> 
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1364&breadcrumbId=4107_451" target="_top">BREEDERS CUP SPRINT</a></div>
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1010&breadcrumbId=4107_453" target="_top">BREEDERS CUP TURF</a></div> 
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=928&breadcrumbId=4107_454" target="_top">BREEDERS CUP DISTAFF</a></div>
    </div>
    <!--<div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=982&breadcrumbId=4107_456" target="_top">Melbourne Cup</a></div> 
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6">&nbsp;</div>
    </div>-->
    <!--<noscript>test</noscript>-->
</div>
<?php endif; ?>