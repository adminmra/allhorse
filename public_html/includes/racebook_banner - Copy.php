<div class="leagueIdtable container row w-100 p-0 mb-3">
    
<div class="row col-lg-12 p-0">
        <div class="elem one col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=3521&breadcrumbId=4107_419" target="_top">TODAY'S MATCH RACES</a></div>
        <div class="elem two col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=998&breadcrumbId=30_420" target="_top">KENTUCKY DERBY</a></div>    
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=993&breadcrumbId=4107_424" target="_top">TRIPLE CROWN SPECIALS</a></div>
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=926&breadcrumbId=4107_421" target="_top">KENTUCKY DERBY PROPS</a></div>       
    </div>
    <div class="row col-lg-12 p-0">
                <div class="elem five col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1006&breadcrumbId=30_463" target="_top">KENTUCKY OAKS</a></div>
                <div class="elem six col-6 col-sm-6 col-md-6 col-lg-6"> <a href="https://engine.betusracing.ag/sports?leagueId=1012&breadcrumbId=30_466" target="_top">PRIX DE L'ARC DE TRIOMPHE</a></div>       
    </div>
    <div class="row col-lg-12 p-0">
                <div class="elem seven col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=3447&breadcrumbId=4107_461" target="_top">COMMOMWEALTH CUP</a></div>
                <div class="elem eight col-6 col-sm-6 col-md-6 col-lg-6"> <a href="https://engine.betusracing.ag/sports?leagueId=982&breadcrumbId=4107_456" target="_top">MELBOURNE CUP</a></div>       
    </div>
	   <!-- <div class="row col-lg-12 p-0">

		<div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1279" target="_top">GRAND NATIONAL</a></div>
		<div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=993&breadcrumbId=30_424" target="_top">TRIPLE CROWN SPECIALS</a></div>
		<div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"> <a href="https://engine.betusracing.ag/sports?leagueId=1012&breadcrumbId=30_466" target="_top">PRIX DE L'ARC DE TRIOMPHE</a></div>
    </div>
	<!--
    <div class="row col-lg-12 p-0">
		<div class="elem five col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=3447" target="_top">COMMONWEALTH CUP</a></div>
		<div class="elem seven col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1012&breadcrumbId=30_466" target="_top">PRIX DE L'ARC DE TRIOMPHE</a></div>
        <div class="elem six col-6 col-sm-6 col-md-6 col-lg-6"> <a href="https://engine.betusracing.ag/sports?leagueId=1450" target="_top">QIPCO 1000
                                GUINEAS</a> </div>        
    </div>
	
    <div class="row col-lg-12 p-0">

		<div class="elem seven col-6 col-sm-6 col-md-6 col-lg-6"> <a href="https://engine.betusracing.ag/sports?leagueId=1012" target="_top">PRIX DE L'ARC DE TRIOMPHE</a></div>	
    	<div class="elem eight col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.betusracing.ag/sports?leagueId=1448" target="_top">QIPCO 2000
                                GUINEAS</a></div>					
    </div>
	 -->

</div>
