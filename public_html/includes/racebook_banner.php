<!--<div class="leagueIdtable container row w-100 p-0 mb-3">
    <div class="row col-lg-12 p-0">
        <div class="elem one col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=998&breadcrumbId=30_420" target="_top">KENTUCKY DERBY</a></div>
        <div class="elem two col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=88&breadcrumbId=30_480" target="_top">BELMONT STAKES</a></div>    
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=993&breadcrumbId=4107_424" target="_top">TRIPLE CROWN SPECIALS</a></div>  
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=926&breadcrumbId=4107_421" target="_top">KENTUCKY DERBY PROPS</a></div>
    </div>
     <div class="row col-lg-12 p-0">
        <div class="elem five col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=1006&breadcrumbId=30_463" target="_top">KENTUCKY OAKS</a> </div>   
        <div class="elem six col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=1012&breadcrumbId=30_466" target="_top">PRIX DE L'ARC DE TRIOMPHE</a></div>    
    </div>
    <div class="row col-lg-12 p-0">                
        <div class="elem seven col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=3447&breadcrumbId=4107_461" target="_top">COMMOMWEALTH CUP</a> </div>       
        <div class="elem eight col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=982&breadcrumbId=4107_456" target="_top">MELBOURNE CUP</a></div>       
    </div>
</div>-->
<?php 
    $now = new DateTime("now", new DateTimeZone('America/New_York') );
    $now = $now->format('Y-m-d H:i:s'); //echo $now;
?>
<?php if($now <= '2020-07-13 18:10:00') : ?>
<div class="leagueIdtable container row w-100 p-0 mb-3">
    <div class="row col-lg-12 p-0">
        <div class="elem one col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=998&breadcrumbId=30_420" target="_top">KENTUCKY DERBY</a></div> 
        <div class="elem two col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=926&breadcrumbId=4107_421" target="_top">KENTUCKY DERBY PROPS</a></div>    
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=993&breadcrumbId=4107_424" target="_top">TRIPLE CROWN SPECIALS</a></div>   
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=1012&breadcrumbId=30_466" target="_top">PRIX DE L'ARC DE TRIOMPHE</a></div>   
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=1006&breadcrumbId=30_463" target="_top">KENTUCKY OAKS</a></div>        
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=982&breadcrumbId=4107_456" target="_top">MELBOURNE CUP</a></div>
    </div>
    <!--
    <div class="row col-lg-12 p-0">                
        <div class="elem seven col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=3447&breadcrumbId=4107_461" target="_top">COMMOMWEALTH CUP</a> </div>          
        <div class="elem eight col-6 col-sm-6 col-md-6 col-lg-6"></div>     
    </div>
-->
</div>
<?php elseif($now <= '2020-10-03 17:00:00') : ?>
<div class="leagueIdtable container row w-100 p-0 mb-3">
    <div class="row col-lg-12 p-0">
     <div class="elem seven col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=1012&breadcrumbId=30_466" target="_top">PRIX DE L'ARC DE TRIOMPHE</a></div>   
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=927&breadcrumbId=4107_456" target="_top">Breeders' Cup Classic</a></div>  

    </div>
<?php /*
    <div class="row col-lg-12 p-0">                
               <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=983&breadcrumbId=4542_482" target="_top">PREAKNESS STAKES</a></div>  
        <div class="elem eight col-6 col-sm-6 col-md-6 col-lg-6"></div>     
    </div> */ ?>
</div> 
<?php elseif($now <= '2021-01-23 17:44:00') : ?>
<div class="leagueIdtable container row w-100 p-0 mb-3">
    <div class="row col-lg-12 p-0">
        <div class="elem one col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/to/racebook?meetingRace=GP*2021/01/23*12" target="_top">PEGASUS WORLD CUP</a></div>   
        <div class="elem two col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=3521&breadcrumbId=4107_419" target="_top">HORSE RACING SPECIAL PROPS</a></div>    
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=998&breadcrumbId=4107_420" target="_top">2021 KENTUCKY DERBY</a></div> 
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=1279&breadcrumbId=4542_457&utm_source=Racebook&utm_medium=Grand-National&utm_campaign=Future-Wagers-Odds" target="_top">2021 GRAND NATIONAL</a></div>   
    </div>
    
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=1005&breadcrumbId=4542_458&utm_source=Racebook&utm_medium=Dubai-World-Cup&utm_campaign=Future-Wagers-Odds" target="_top">2021 DUBAI WORLD CUP</a></div> 
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"></div>   
    </div>
</div> 
<?php elseif($now <= '2021-04-30 17:50:00') : ?>
<div class="leagueIdtable container row w-100 p-0 mb-3">
<div class="row col-lg-12 p-0">
        <div class="elem one col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=998&breadcrumbId=4107_420" target="_top">2021 KENTUCKY DERBY</a></div>
        <div class="elem two col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=1006&breadcrumbId=4542_463" target="_top">2021 KENTUCKY OAKS</a></div>  
    </div>
</div> 
<?php elseif($now <= '2021-05-01 18:49:00') : ?>
<div class="leagueIdtable container row w-100 p-0 mb-3">
<div class="row col-lg-12 p-0">
        <div class="elem one col-6 col-sm-6 col-md-6 col-lg-6"><a href="https://engine.busr.ag/sports?leagueId=998&breadcrumbId=4107_420" target="_top">2021 KENTUCKY DERBY</a></div>
        <div class="elem two col-6 col-sm-6 col-md-6 col-lg-6"></div>  
    </div>
</div> 
<?php else : ?>
<div class="leagueIdtable container row w-100 p-0 mb-3">
    <div class="row col-lg-12 p-0">
        <div class="elem one col-6 col-sm-6 col-md-6 col-lg-6"></div>
        <div class="elem two col-6 col-sm-6 col-md-6 col-lg-6"></div>  
    </div>
    <div class="row col-lg-12 p-0">
        <div class="elem three col-6 col-sm-6 col-md-6 col-lg-6"></div> 
        <div class="elem four col-6 col-sm-6 col-md-6 col-lg-6"></div>   
    </div>
</div>
<?php endif; ?>
