<?php

$FileXML=$_SERVER['DOCUMENT_ROOT'].'/switch-control/libcontrol.xml';
	function writexml($xmldata, $FileXML)
	{
		$file= $FileXML;
			if (!$fp = fopen($file, 'wb')) {
				return "Could not write to ".$file;
				}
		//echo "successfully write to file".$file."<br>";
		fwrite($fp, $xmldata);
		fclose($fp);
	}
	
	function is_between_times( $start = null, $end = null ) {
		$currentTime=strtotime(date("Y-m-d H:i:s"));
        return ( $start <= $currentTime && $currentTime <= $end );
		
    }
	
	
	function is_between_times_validate( $start = null, $end = null, $tochk= null) {
		$currentTime=strtotime(date("Y-m-d H:i:s"));
        return ( $start <= $tochk && $tochk <= $end );
		
    }

if($_POST['submit']== 'Save'){
	//print_r($_POST);
$xmldata .= '<?xml version="1.0" encoding="utf-8"?>';
$xmldata .= '<libcontrol>';
$i=1;
for($i=1;$i<=$_POST['varcounter'];$i++){
	$namePost= $i."_name";
	$stakesPost= $i."_varname";
	$starttimePost= $i."_starttime";
	$endtimePost= $i."_endtime";
	$switchforPost= $i."_switchfor";
	
	$err=0;
	$errcode=0;


$xmldata .= '<libfiles>';
$xmldata .= '<id>'.$i.'</id>';
$xmldata .= '<name>'.$_POST[$namePost].'</name>';
$xmldata .= '<varname>'.$_POST[$stakesPost].'</varname>';
$xmldata .= '<starttime>'.$_POST[$starttimePost].'</starttime>';
$xmldata .= '<endtime>'.$_POST[$endtimePost].'</endtime>';
$xmldata .= '</libfiles>';

}

$xmldata .= '</libcontrol>';
writexml($xmldata, $FileXML);
			?>
            <script type="text/javascript" >
			window.location = "/switch-control/admin.php"
			</script>
            <?php

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin</title>
 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<!--<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-prA7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>-->
<script
  src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
  integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="
  crossorigin="anonymous"></script>
  
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<script src="js/jquery-ui-timepicker-addon.js"></script>
<script src="https://rawgithub.com/trentrichardson/jQuery-Timepicker-Addon/master/jquery-ui-sliderAccess.js"></script>

<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/smoothness/jquery-ui.css" type="text/css" media="all" />
    <style>
.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 40%; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
.ui-timepicker-div .ui_tpicker_unit_hide{ display: none; }

.ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input { background: none; color: inherit; border: none; outline: none; border-bottom: solid 1px #555; width: 95%; }
.ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input:focus { border-bottom-color: #aaa; }

.ui-timepicker-rtl{ direction: rtl; }
.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
.ui-timepicker-rtl dl dt{ float: right; clear: right; }
.ui-timepicker-rtl dl dd { margin: 0 40% 10px 10px; }

/* Shortened version style */
.ui-timepicker-div.ui-timepicker-oneLine { padding-right: 2px; }
.ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time, 
.ui-timepicker-div.ui-timepicker-oneLine dt { display: none; }
.ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time_label { display: block; padding-top: 2px; }
.ui-timepicker-div.ui-timepicker-oneLine dl { text-align: right; }
.ui-timepicker-div.ui-timepicker-oneLine dl dd, 
.ui-timepicker-div.ui-timepicker-oneLine dl dd > div { display:inline-block; margin:0; }
.ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_minute:before,
.ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_second:before { content:':'; display:inline-block; }
.ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_millisec:before,
.ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_microsec:before { content:'.'; display:inline-block; }
.ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide,
.ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide:before{ display: none; }
.ui-datepicker-current{ display: none; }
.errcl{ text-align:center; color: #F00; }
.headrow{ font-weight:bolder; }
    </style>
</head>

<body>
<br />
<?php
date_default_timezone_set('America/Kentucky/Louisville');
echo '<center><h1>Server TimeZone:'.date_default_timezone_get().', <br /> ';
echo 'Server Time:'.date("F j, Y, g:i a").'</h1></center>'; 
$xmlString = file_get_contents($FileXML);
$xml = simplexml_load_string($xmlString);
 
?>
<br />
<?php 
$cssError= '';
if(isset($_REQUEST['err'])){ if($_REQUEST['err']=="bcjerr"){ echo '<div class="errcl">Breeders\' Cup Juvenile dates, time must be between Breeders\' Cup dates.</div>';}}
?>
<br />
<div  style=" text-align:right; margin-right: 8px; margin-bottom: 10px; " ><a href="/switch-control/add.php" class="btn btn-primary" >Add New Variables</a></div>
<form name="frm" action="" method="post" >
<table border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0">
<tr >
<td><strong>Name</strong></td>
<td><strong>Variable</strong></td>
<td><strong>Start Time</strong></td>
<td><strong>End Time</strong></td>
<td><strong>Status</strong></td>

</tr>
<?php
$group="";
foreach($xml->libfiles as $item)
{ 
$curdate=date('Y-m-d');
$curtime=date('H:m');
$css='';
$bcmenu='';
$liveNow='';
	$id=(string)$item->id;
	$name=(string)$item->name;
	$varname=(string)$item->varname;
	$starttime=(string)$item->starttime;
	$endtime=(string)$item->endtime;
	$start=strtotime($starttime);
	$end=strtotime($endtime);
	$switchfor=(string)$item->switchfor;
	if (is_between_times($start, $end)) {
		 $Filetoload=(string)$item->stakes;
		 $css='style=" background-color:#F00; color: #fff; font-weight: bold;"';
		 $liveNow= 'Live';  $bcmenu.="::BC=Y";
	 }
	 else{ $liveNow= 'Not Live';  $bcmenu.="::BCj=Y";}
	 

?>

<?php // echo $cssError; ?>
<tr>
<td><?php echo $name; ?></td>
<td>$<?php echo $varname; ?></td>
<td>
<input type="text" name="<?php echo $id."_starttime"; ?>" id="<?php echo $id."_starttime"; ?>" value="<?php echo $item->starttime; ?>" readonly /></td>
<td><input type="text" name="<?php echo $id."_endtime"; ?>" id="<?php echo $id."_endtime"; ?>" value="<?php echo $item->endtime; ?>" readonly /></td>
<td  <?php echo $css; ?>> <?php echo $liveNow; ?></td>
</tr>
<input type="hidden" name="<?php echo $id."_name"; ?>" value="<?php echo $name; ?>" />
<input type="hidden" name="<?php echo $id."_varname"; ?>" value="<?php echo $varname; ?>" />
<script type="text/javascript" >
$('#<?php echo $id."_starttime"; ?>').datetimepicker({
	dateFormat: "yy-mm-dd",
	hourGrid: 4,
	minuteGrid: 10,
	minDate: "<?php echo $curdate; ?>",
	timeFormat: "HH:mm"
});

$('#<?php echo $id."_endtime"; ?>').datetimepicker({
	dateFormat: "yy-mm-dd",
	hourGrid: 4,
	minuteGrid: 10,
	minDate: "<?php echo $curdate; ?>",
	timeFormat: "HH:mm"
});

$('#<?php echo $id."_starttime"; ?>') .on( "change", function() {
          //getDate( this );
		  var datti= this.value;
		  var splitedDate= datti.split(" ");
		  $('#<?php echo $id."_endtime"; ?>').datepicker( "option", "minDate", splitedDate[0]);
		  $('#<?php echo $id."_endtime"; ?>').datepicker( "option", "minTime", splitedDate[1]);
        });




</script>
<?php
}
?>
<input type="hidden" name="varcounter" value="<?php echo $id; ?>" />
<tr><td colspan="4" align="center"><input type="submit" value="Save" name="submit" /></td></tr>
</table>

</form>

</body>
</html>