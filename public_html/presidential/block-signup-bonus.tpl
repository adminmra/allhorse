<section class="card">
    <div class="card_wrap">
        <div class="card_half card_content">
            <a href="/promos/cash-bonus-10">
                {* <img src="/img/index-kd/icon-kentucky-derby-betting.png" alt="Bet on Kentucky Derby"
                    class="card_icon"> *}
                <img class="card_icon" src="/img/presidential/offer-icon.png">
            </a>
            <h2 class="card_heading">Get a Sign Up Bonus up to $500 Cash</h2>
            <h3 class="card_subheading"><p>At <a href="/signup?ref={$ref15}">BUSR</a>, you are entitled to exceptional new member bonuses.</p>
            <p>For your first deposit with <a href="/signup?ref={$ref16}">BUSR</a>, you&#39;ll get an additional 20% bonus to your deposit. No Limits! Deposit a minimum of $100 and you could qualify to earn an additional $150! You can only win with <a href="/signup?ref={$ref17}">BUSR</a>!</p>
            <a href="/promos/cash-bonus-20" class="card_btn">Learn More</a>
        </div>
        <div class="card_half card_hide-mobile">
            <a href="/promos/cash-bonus-20"><img src="/img/presidential/sigup-offer.jpg" class="card_img" alt='U.S. Presidential Election Odds Offer'></a>
        </div>
    </div>
</section>
