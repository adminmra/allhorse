{* {if $ShowPegasusOddS}

<!-- ON Pegasus -->
BUSR and the Pegasus World Cup are a great combination. When you sign up you can receive up to
$625 in cash bonus. Use the promocode <strong>PEGASUS</strong> when you make your deposit and receive an extra 25% cash
up to $625. Take advantage of this great offer for the Pegasus World Cup. Who is going to win the Pegasus World Cup?
Place your bet now! The action doesn't stop with BUSR. Join today so you can place your wager on
the Pegasus World Cup! <br>
<br>

{else}

<!-- After Pegasus -->
BUSR and the Pegasus World Cup are a great combination. When you sign up you receive a 10% cash
bonus. Take advantage of this great offer for the Pegasus World Cup along with all the other great horse racing action
throughout the year. Who do you think will win the Pegasus World Cup? Place your bet now! The action doesn't stop with
BUSR. Join today so you can place your wager on the Pegasus World Cup!


{/if} *}
<h1 class="kd_subheading">Who will win the U.S. Presidential election in
{include_php file='/home/ah/allhorse/public_html/presidential/year.php'}?</h1>
<p><a href="/signup?ref={$ref3}">BUSR</a> is the leading site where you can bet on politics, including the U.S. Presidential Election! What are <a href="/signup?ref={$ref4}">Ivanka Trump's Odds</a> to follow her father Donald and become President? Will President <a href="/signup?ref={$ref5}">Joe Biden</a> step aside and let Kamala Harris run as the Democratic nominee for President in 2024?
	Don't count out some other candidates like Mark Cuban, Tom Cotton, Ted Cruz and Kanye West!</p>
<p>No matter your political affiliation, you can always <a href="/signup?ref={$ref6}">bet on politics</a> and get in on the exiting U.S. Presidential Election Action.
At <a href="/signup?ref={$ref7}">BUSR</a> you can always find the latest available <a href="/signup?ref={$ref8}">US Politics Odds</a>. </p>
<p>The {include_php file='/home/ah/allhorse/public_html/presidential/year.php'} U.S. <a href="/signup?ref={$ref9}">Presidential Election Odds</a>, Props and Specials are live. </p>

<a href="/signup?ref={$ref10}" rel="nofollow" class="btn-xlrg ">Bet on the U.S. Presidential Elections</a>
