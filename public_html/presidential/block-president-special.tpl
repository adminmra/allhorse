<section class="card">
      <div class="card_wrap">
        <div class="card_half card_hide-mobile">
                <a href="/signup?ref={$ref18}">
                        <img src="/img/presidential/usr-pe-sm.jpg" data-src-img="/img/presidential/usr-pe.jpg" alt="U.S. Presidential Election Odds"  class="card_img"></a></div>
        <div class="card_half card_content">
                <a href="/signup?ref={$ref18}">
          <img class=" card_icon" alt="U.S. Presidential Election Odds" src="/img/presidential/offer-icon.png">
                </a>
          <h2 class="card_heading">BET ON U.S. POLITICS TODAY!</h2>
          <!--<h3 class="card_subheading">And Other Exclusive U.S. Presidential Elections Props</h3>-->
          <p style="margin-bottom: 10px">The election may be over, but there’s still lots to bet on.</p>
          <p>Check out the exclusive Political Props at BUSR. Wager on current affairs, upcoming events and more!</p>
         <a href="/signup?ref={$ref18}" class="card_btn">Bet on the U.S. Presidential Election Specials</a>
        </div>
      </div>
    </section>
