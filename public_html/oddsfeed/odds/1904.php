	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--switzerland Challenge League">
			<caption>Live--switzerland Challenge League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated April 28, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--switzerland Challenge League  - Apr 28</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Rapperswil-jona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( +160 )</div>
								 <div class="boxdata" >220</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Wil 1900</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -222 )</div>
								 <div class="boxdata" >195</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Chiasso</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >265</div>
								 <div class="boxdata" >+&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Kriens</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	