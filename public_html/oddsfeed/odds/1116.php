	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Rugby - Futures">
			<caption>Rugby - Futures</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Rugby - Futures  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Rugby World Cup - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>New Zealand</td><td>5/4</td><td>+125</td></tr><tr><td>Ireland</td><td>9/1</td><td>+900</td></tr><tr><td>England</td><td>4/1</td><td>+400</td></tr><tr><td>Australia</td><td>12/1</td><td>+1200</td></tr><tr><td>South Africa</td><td>4/1</td><td>+400</td></tr><tr><td>Wales</td><td>9/1</td><td>+900</td></tr><tr><td>France</td><td>33/1</td><td>+3300</td></tr><tr><td>Scotland</td><td>40/1</td><td>+4000</td></tr><tr><td>Argentina</td><td>40/1</td><td>+4000</td></tr><tr><td>Fiji</td><td>350/1</td><td>+35000</td></tr><tr><td>Japan</td><td>150/1</td><td>+15000</td></tr><tr><td>Georgia</td><td>1000/1</td><td>+100000</td></tr><tr><td>Samoa</td><td>750/1</td><td>+75000</td></tr><tr><td>Tonga</td><td>1000/1</td><td>+100000</td></tr><tr><td>Italy</td><td>750/1</td><td>+75000</td></tr><tr><td>Canada</td><td>5000/1</td><td>+500000</td></tr><tr><td>Namibia</td><td>7500/1</td><td>+750000</td></tr><tr><td>Russia</td><td>5000/1</td><td>+500000</td></tr><tr><td>Usa</td><td>2000/1</td><td>+200000</td></tr><tr><td>Uruguay</td><td>5000/1</td><td>+500000</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					Rugby - Futures  - Sep 27					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Rugby Union - Pro 14 - Odds To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Leinster</td><td>6/5</td><td>+120</td></tr><tr><td>Glasgow</td><td>7/2</td><td>+350</td></tr><tr><td>Munster</td><td>7/2</td><td>+350</td></tr><tr><td>Scarlets</td><td>20/1</td><td>+2000</td></tr><tr><td>Connacht</td><td>20/1</td><td>+2000</td></tr><tr><td>Ulster</td><td>14/1</td><td>+1400</td></tr><tr><td>Ospreys</td><td>25/1</td><td>+2500</td></tr><tr><td>Edinburgh</td><td>20/1</td><td>+2000</td></tr><tr><td>Cardiff Blues</td><td>25/1</td><td>+2500</td></tr><tr><td>Benetton Treviso</td><td>50/1</td><td>+5000</td></tr><tr><td>Cheetahs</td><td>66/1</td><td>+6600</td></tr><tr><td>Newport Rfc</td><td>250/1</td><td>+25000</td></tr><tr><td>Zebre</td><td>250/1</td><td>+25000</td></tr><tr><td>Southern Kings</td><td>250/1</td><td>+25000</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					Rugby - Futures  - Feb 01					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Six Nations 2020 - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>England</td><td>8/5</td><td>+160</td></tr><tr><td>Ireland</td><td>2/1</td><td>+200</td></tr><tr><td>Wales</td><td>5/2</td><td>+250</td></tr><tr><td>France</td><td>16/1</td><td>+1600</td></tr><tr><td>Scotland</td><td>16/1</td><td>+1600</td></tr><tr><td>Italy</td><td>1000/1</td><td>+100000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');</script>
{/literal}	
	