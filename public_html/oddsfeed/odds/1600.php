	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--japan Professional Baseball">
			<caption>Live--japan Professional Baseball</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 21, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--japan Professional Baseball  - May 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fukuoka Softbank Hawks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o9&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >260</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -181 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Saitama Seibu Lions</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u9&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-500</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +114 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	