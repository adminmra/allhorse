	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nfl">
			<caption>Nfl</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Nfl  - Sep 19</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tennessee Titans</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o38&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-130</div>
								 <div class="boxdata" >-2<br /> ( -115)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jacksonville Jaguars</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u38&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >+2<br /> ( -105)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Nfl  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Denver Broncos</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o42&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >305</div>
								 <div class="boxdata" >+8<br /> ( -110)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Green Bay Packers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u42&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-365</div>
								 <div class="boxdata" >-8<br /> ( -110)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Detroit Lions</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o46&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+6&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Philadelphia Eagles</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u46&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-300</div>
								 <div class="boxdata" >-6&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Baltimore Ravens</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o54&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >245</div>
								 <div class="boxdata" >+6&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kansas City Chiefs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u54&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-290</div>
								 <div class="boxdata" >-6&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cincinnati Bengals</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o44<br /> ( -110)</div>
								 <div class="boxdata" >210</div>
								 <div class="boxdata" >+6<br /> ( -110)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Buffalo Bills</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u44<br /> ( -110)</div>
								 <div class="boxdata" >-250</div>
								 <div class="boxdata" >-6<br /> ( -110)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atlanta Falcons</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o47<br /> ( -110)</div>
								 <div class="boxdata" >105</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Indianapolis Colts</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u47<br /> ( -110)</div>
								 <div class="boxdata" >-125</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Oakland Raiders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o43&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >310</div>
								 <div class="boxdata" >+8&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Minnesota Vikings</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u43&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-370</div>
								 <div class="boxdata" >-8&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New York Jets</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o43&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+23&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New England Patriots</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u43&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-23&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Miami Dolphins</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o47<br /> ( -110)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+22<br /> ( -110)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dallas Cowboys</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u47<br /> ( -110)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-22<br /> ( -110)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New York Giants</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o48<br /> ( -105)</div>
								 <div class="boxdata" >240</div>
								 <div class="boxdata" >+7<br /> ( -120)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tampa Bay Buccaneers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u48<br /> ( -115)</div>
								 <div class="boxdata" >-280</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New Orleans Saints</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o44&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" >+4&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Seattle Seahawks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u44&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-210</div>
								 <div class="boxdata" >-4&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Houston Texans</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o47&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >155</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Los Angeles Chargers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u47&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-175</div>
								 <div class="boxdata" >-3<br /> ( -120)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pittsburgh Steelers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o43&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >235</div>
								 <div class="boxdata" >+7<br /> ( -115)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >San Francisco 49ers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u43&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-275</div>
								 <div class="boxdata" >-7<br /> ( -105)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Los Angeles Rams</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o49<br /> ( -110)</div>
								 <div class="boxdata" >-155</div>
								 <div class="boxdata" >-3<br /> ( -110)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cleveland Browns</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u49<br /> ( -110)</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" >+3<br /> ( -110)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Nfl  - Sep 23</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chicago Bears</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o41&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-200</div>
								 <div class="boxdata" >-4<br /> ( -110)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Washington Redskins</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u41&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >170</div>
								 <div class="boxdata" >+4<br /> ( -110)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	