	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Finland Liiga">
			<caption>Finland Liiga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Finland Liiga  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vaasan Sport</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kookoo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-&frac12;<br /> ( +105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lukko</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5<br /> ( -125)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pelicans</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5<br /> ( +105)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-&frac12;<br /> ( +115 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jyp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5<br /> ( -110)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+1<br /> ( -125)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ifk Helsinki</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5<br /> ( -110)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-1<br /> ( +105)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Saipa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5<br /> ( -105)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hameenlinna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-1<br /> ( -120)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tps Turku</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5<br /> ( -130)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+1<br /> ( -120)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ilves</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5<br /> ( +110)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tappara</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5<br /> ( +105)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-&frac12;<br /> ( +120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Assat</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5<br /> ( -125)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	