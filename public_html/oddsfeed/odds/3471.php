	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - Lpga Tour 3 Balls">
			<caption>Golf - Lpga Tour 3 Balls</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - Lpga Tour 3 Balls  - Jul 04					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Thornberry Creek Lpga Classic - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Jennifer Song</td><td>11/4</td><td>+275</td></tr><tr><td>Ally Mcdonald</td><td>11/5</td><td>+220</td></tr><tr><td>Carlota Ciganda</td><td>20/21</td><td>-105</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Thornberry Creek Lpga Classic - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Maria Fernanda Torres</td><td>3/1</td><td>+300</td></tr><tr><td>Hyo Joo Kim</td><td>21/20</td><td>+105</td></tr><tr><td>Kristen Gillman</td><td>37/20</td><td>+185</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	