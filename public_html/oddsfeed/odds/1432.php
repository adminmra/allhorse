	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - Wgc">
			<caption>Golf - Wgc</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 29, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - Wgc  - Mar 29					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Wgc Dell Matchplay - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Rory Mcilroy</td><td>4/1</td><td>+400</td></tr><tr><td>Francesco Molinari</td><td>15/2</td><td>+750</td></tr><tr><td>Sergio Garcia</td><td>16/1</td><td>+1600</td></tr><tr><td>Justin Rose</td><td>20/1</td><td>+2000</td></tr><tr><td>Matt Kuchar</td><td>25/1</td><td>+2500</td></tr><tr><td>Tommy Fleetwood</td><td>25/1</td><td>+2500</td></tr><tr><td>Branden Grace</td><td>25/1</td><td>+2500</td></tr><tr><td>Marc Leishman</td><td>25/1</td><td>+2500</td></tr><tr><td>Paul Casey</td><td>28/1</td><td>+2800</td></tr><tr><td>Gary Woodland</td><td>30/1</td><td>+3000</td></tr><tr><td>Xander Schauffele</td><td>33/1</td><td>+3300</td></tr><tr><td>Henrik Stenson</td><td>33/1</td><td>+3300</td></tr><tr><td>Hao Tong Li</td><td>33/1</td><td>+3300</td></tr><tr><td>Justin Thomas</td><td>33/1</td><td>+3300</td></tr><tr><td>Jordan Spieth</td><td>35/1</td><td>+3500</td></tr><tr><td>Jim Furyk</td><td>35/1</td><td>+3500</td></tr><tr><td>Jon Rahm</td><td>40/1</td><td>+4000</td></tr><tr><td>Tyrrell Hatton</td><td>40/1</td><td>+4000</td></tr><tr><td>Brandt Snedeker</td><td>40/1</td><td>+4000</td></tr><tr><td>Ian Poulter</td><td>45/1</td><td>+4500</td></tr><tr><td>Billy Horschel</td><td>50/1</td><td>+5000</td></tr><tr><td>Tony Finau</td><td>50/1</td><td>+5000</td></tr><tr><td>Dustin Johnson</td><td>50/1</td><td>+5000</td></tr><tr><td>Patrick Cantlay</td><td>50/1</td><td>+5000</td></tr><tr><td>Lucas Bjerregaard</td><td>50/1</td><td>+5000</td></tr><tr><td>Kevin Kisner</td><td>55/1</td><td>+5500</td></tr><tr><td>Charles Howell</td><td>66/1</td><td>+6600</td></tr><tr><td>Bryson Dechambeau</td><td>66/1</td><td>+6600</td></tr><tr><td>Kyle Stanley</td><td>66/1</td><td>+6600</td></tr><tr><td>Tiger Woods</td><td>80/1</td><td>+8000</td></tr><tr><td>Louis Oosthuizen</td><td>80/1</td><td>+8000</td></tr><tr><td>Keith Mitchell</td><td>80/1</td><td>+8000</td></tr><tr><td>Kiradech Aphibarnrat</td><td>90/1</td><td>+9000</td></tr><tr><td>Matt Wallace</td><td>100/1</td><td>+10000</td></tr><tr><td>Alex Noren</td><td>100/1</td><td>+10000</td></tr><tr><td>Abraham Ancer</td><td>100/1</td><td>+10000</td></tr><tr><td>J B Holmes</td><td>150/1</td><td>+15000</td></tr><tr><td>Thorbjorn Olesen</td><td>200/1</td><td>+20000</td></tr><tr><td>Andrew Putnam</td><td>250/1</td><td>+25000</td></tr><tr><td>Keegan Bradley</td><td>250/1</td><td>+25000</td></tr><tr><td>Kevin Na</td><td>300/1</td><td>+30000</td></tr><tr><td>Luke List</td><td>300/1</td><td>+30000</td></tr><tr><td>Rafael Cabrera Bello</td><td>300/1</td><td>+30000</td></tr><tr><td>Justin Harding</td><td>300/1</td><td>+30000</td></tr><tr><td>Lee Westwood</td><td>400/1</td><td>+40000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	