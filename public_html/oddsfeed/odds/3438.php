	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Spanish Copa Del Rey">
			<caption>Soccer - Spanish Copa Del Rey</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Spanish Copa Del Rey  - Dec 18					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Spain Copa Del Rey - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Barcelona</td><td>2/1</td><td>+200</td></tr><tr><td>Real Madrid</td><td>11/4</td><td>+275</td></tr><tr><td>Atletico Madrid</td><td>6/1</td><td>+600</td></tr><tr><td>Valencia</td><td>10/1</td><td>+1000</td></tr><tr><td>Sevilla</td><td>16/1</td><td>+1600</td></tr><tr><td>Villarreal</td><td>20/1</td><td>+2000</td></tr><tr><td>Athletic Bilbao</td><td>20/1</td><td>+2000</td></tr><tr><td>Getafe</td><td>25/1</td><td>+2500</td></tr><tr><td>Real Betis</td><td>33/1</td><td>+3300</td></tr><tr><td>Eibar</td><td>33/1</td><td>+3300</td></tr><tr><td>Celta Vigo</td><td>50/1</td><td>+5000</td></tr><tr><td>Espanyol</td><td>50/1</td><td>+5000</td></tr><tr><td>Real Sociedad</td><td>66/1</td><td>+6600</td></tr><tr><td>Cd Alaves</td><td>100/1</td><td>+10000</td></tr><tr><td>Levante</td><td>100/1</td><td>+10000</td></tr><tr><td>Leganes</td><td>150/1</td><td>+15000</td></tr><tr><td>Osasuna</td><td>150/1</td><td>+15000</td></tr><tr><td>Granada</td><td>200/1</td><td>+20000</td></tr><tr><td>Valladolid</td><td>200/1</td><td>+20000</td></tr><tr><td>Girona</td><td>250/1</td><td>+25000</td></tr><tr><td>Malaga</td><td>250/1</td><td>+25000</td></tr><tr><td>Rayo Vallecano</td><td>250/1</td><td>+25000</td></tr><tr><td>Huesca</td><td>250/1</td><td>+25000</td></tr><tr><td>Albacete</td><td>250/1</td><td>+25000</td></tr><tr><td>Deportivo La Coruna</td><td>250/1</td><td>+25000</td></tr><tr><td>Mallorca</td><td>250/1</td><td>+25000</td></tr><tr><td>Oviedo</td><td>500/1</td><td>+50000</td></tr><tr><td>Sporting Gijon</td><td>500/1</td><td>+50000</td></tr><tr><td>Almeria</td><td>500/1</td><td>+50000</td></tr><tr><td>Cadiz</td><td>500/1</td><td>+50000</td></tr><tr><td>Las Palmas</td><td>1000/1</td><td>+100000</td></tr><tr><td>Extremadura</td><td>1000/1</td><td>+100000</td></tr><tr><td>Alcorcon</td><td>1000/1</td><td>+100000</td></tr><tr><td>Real Zaragoza</td><td>1000/1</td><td>+100000</td></tr><tr><td>Tenerife</td><td>1000/1</td><td>+100000</td></tr><tr><td>Elche</td><td>1000/1</td><td>+100000</td></tr><tr><td>Lugo</td><td>1500/1</td><td>+150000</td></tr><tr><td>Numancia</td><td>1500/1</td><td>+150000</td></tr><tr><td>Mirandes</td><td>2000/1</td><td>+200000</td></tr><tr><td>Ponferradina</td><td>2000/1</td><td>+200000</td></tr><tr><td>Racing Santander</td><td>2000/1</td><td>+200000</td></tr><tr><td>Fuenlabrada</td><td>2000/1</td><td>+200000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	