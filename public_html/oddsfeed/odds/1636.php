	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--turkey Super Lig">
			<caption>Live--turkey Super Lig</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 20, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--turkey Super Lig  - May 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sivasspor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( -181 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+2<br /> ( +104)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ankaragucu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( +104 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-2<br /> ( -166)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fenerbahce</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -200 )</div>
								 <div class="boxdata" >-400</div>
								 <div class="boxdata" >-1<br /> ( -125)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Buyuksehir Belediye Erzurumspor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" >1225</div>
								 <div class="boxdata" >+1<br /> ( -125)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	