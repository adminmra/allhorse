	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Audl">
			<caption>Audl</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Audl  - Jul 04</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ottawa Outlaws</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o40&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+1&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Montreal Royal</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u40&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-1&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Audl  - Jul 05</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dallas Roughnecks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o43&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >-110</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Raleigh Flyers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u43&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >-130</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Audl  - Jul 06</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pittsburgh Thunderbirds</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o37&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-6&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Detroit Mechanix</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u37&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+6&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dc Breeze</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o40&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-1&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Toronto Rush</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u40&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+1&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Los Angeles Aviators</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o45&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-5<br /> ( -120)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Seattle Cascades</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u45&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+5<br /> ( -120)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Minnesota Wind Chill</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o41&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-2&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Madison Radicals</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u41&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+2&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New York Empire</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o40&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-5&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Philadelphia Phoenix</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u40&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+5&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >San Diego Growlers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o46&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-4<br /> ( -120)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >San Jose Spiders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u46&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+4<br /> ( -120)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chicago Wildfire</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o39&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-1<br /> ( -120)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Indianapolis Alleycats</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u39&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+1<br /> ( -120)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dallas Roughnecks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o40&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-2&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atlanta Hustle</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u40&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+2&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	