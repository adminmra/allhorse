	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Dominican Republic Baseball">
			<caption>Dominican Republic Baseball</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated January 22, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Dominican Republic Baseball  - Jan 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Toros Del Este</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o9<br /> ( -122)</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -159 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Estrellas De Oriente</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u9<br /> ( -106)</div>
								 <div class="boxdata" >-192</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +127 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	