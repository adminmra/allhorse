	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--turkey Super Lig">
			<caption>Pre--turkey Super Lig</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--turkey Super Lig  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atiker Konyaspor 1922</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >216</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Goztepe Sk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--turkey Super Lig  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Antalyaspor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >194</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kasimpasa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Denizlispor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >187</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kayserispor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >138</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mke Ankaragucu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >822</div>
								 <div class="boxdata" >+1&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fenerbahce</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >-370</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--turkey Super Lig  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gazisehir Gaziantep Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >264</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Caykur Rizespor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -126 )</div>
								 <div class="boxdata" >-107</div>
								 <div class="boxdata" >-&frac12;<br /> ( -108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Galatasaray</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >-123</div>
								 <div class="boxdata" >-&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yeni Malatyaspor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >297</div>
								 <div class="boxdata" >+&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Alanyaspor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >-&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Genclerbirligi Sk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >254</div>
								 <div class="boxdata" >+&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--turkey Super Lig  - Sep 23</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Basaksehir Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Besiktas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Trabzonspor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >163</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sivasspor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -103 )</div>
								 <div class="boxdata" >142</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	