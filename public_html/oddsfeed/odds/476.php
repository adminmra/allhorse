	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mlb - World Series - To Win">
			<caption>Mlb - World Series - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Mlb - World Series - To Win  - Sep 22					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 World Series - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Los Angeles Dodgers</td><td>5/2</td><td>+250</td></tr><tr><td>New York Yankees</td><td>7/2</td><td>+350</td></tr><tr><td>Houston Astros</td><td>21/10</td><td>+210</td></tr><tr><td>Atlanta Braves</td><td>5/1</td><td>+500</td></tr><tr><td>Minnesota Twins</td><td>14/1</td><td>+1400</td></tr><tr><td>Chicago Cubs</td><td>20/1</td><td>+2000</td></tr><tr><td>Tampa Bay Rays</td><td>18/1</td><td>+1800</td></tr><tr><td>Milwaukee Brewers</td><td>28/1</td><td>+2800</td></tr><tr><td>Boston Red Sox</td><td>400/1</td><td>+40000</td></tr><tr><td>Philadelphia Phillies</td><td>55/1</td><td>+5500</td></tr><tr><td>St Louis Cardinals</td><td>12/1</td><td>+1200</td></tr><tr><td>Cleveland Indians</td><td>20/1</td><td>+2000</td></tr><tr><td>Washington Nationals</td><td>16/1</td><td>+1600</td></tr><tr><td>Oakland Athletics</td><td>16/1</td><td>+1600</td></tr><tr><td>Arizona Diamondbacks</td><td>100/1</td><td>+10000</td></tr><tr><td>New York Mets</td><td>45/1</td><td>+4500</td></tr><tr><td>San Francisco Giants</td><td>4500/1</td><td>+450000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	