	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nfl - Draft Props">
			<caption>Nfl - Draft Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nfl - Draft Props  - Jan 01					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Be The #1 Pick In 2020 Draft</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tua Tagovailoa</td><td>1/2</td><td>-200</td></tr><tr><td>Justin Herbert</td><td>7/2</td><td>+350</td></tr><tr><td>Jake Fromm</td><td>5/1</td><td>+500</td></tr><tr><td>Chase Young</td><td>9/1</td><td>+900</td></tr><tr><td>Derrick Brown</td><td>9/1</td><td>+900</td></tr><tr><td>Jerry Jeudy</td><td>10/1</td><td>+1000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	