	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--india Indian Super League">
			<caption>Pre--india Indian Super League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 14, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Pre--india Indian Super League  - Dec 14</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Northeast United Fc<br /> - vs - <br /> Fc Goa</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-116)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+266</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac34;-107 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (-108)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-106</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac34;-128</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Pre--india Indian Super League  - Dec 15</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Delhi Dynamos Fc<br /> - vs - <br /> Chennaiyin Fc</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.25<br /> (-104)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+231</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac34;-106 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.25<br /> (-121)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+110</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac34;-129</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	