	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Grand National">
			<caption>Horses - Grand National</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Grand National  - Apr 04					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Grand National - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Kildisart</td><td>50/1</td><td>+5000</td></tr><tr><td>Tiger Roll</td><td>5/1</td><td>+500</td></tr><tr><td>Burrows Saint</td><td>16/1</td><td>+1600</td></tr><tr><td>Rathvinden</td><td>25/1</td><td>+2500</td></tr><tr><td>Magic Of Light</td><td>25/1</td><td>+2500</td></tr><tr><td>Native River</td><td>25/1</td><td>+2500</td></tr><tr><td>Frodon</td><td>25/1</td><td>+2500</td></tr><tr><td>Walk In The Mill</td><td>33/1</td><td>+3300</td></tr><tr><td>Anibale Fly</td><td>33/1</td><td>+3300</td></tr><tr><td>Blaklion</td><td>33/1</td><td>+3300</td></tr><tr><td>Any Second Now</td><td>33/1</td><td>+3300</td></tr><tr><td>Calett Mad</td><td>33/1</td><td>+3300</td></tr><tr><td>Pleasant Company</td><td>33/1</td><td>+3300</td></tr><tr><td>Talkischeap</td><td>33/1</td><td>+3300</td></tr><tr><td>Vintage Clouds</td><td>40/1</td><td>+4000</td></tr><tr><td>Lake View Lad</td><td>40/1</td><td>+4000</td></tr><tr><td>Cadmium</td><td>40/1</td><td>+4000</td></tr><tr><td>Le Breuil</td><td>40/1</td><td>+4000</td></tr><tr><td>Mister Malarky</td><td>40/1</td><td>+4000</td></tr><tr><td>Beware The Bear</td><td>40/1</td><td>+4000</td></tr><tr><td>Elegant Escape</td><td>40/1</td><td>+4000</td></tr><tr><td>Missed Approach</td><td>40/1</td><td>+4000</td></tr><tr><td>Glen Rocco</td><td>40/1</td><td>+4000</td></tr><tr><td>One For Arthur</td><td>50/1</td><td>+5000</td></tr><tr><td>Livelovelaugh</td><td>50/1</td><td>+5000</td></tr><tr><td>Discorama</td><td>50/1</td><td>+5000</td></tr><tr><td>Ramses De Teillee</td><td>50/1</td><td>+5000</td></tr><tr><td>Jerrysback</td><td>66/1</td><td>+6600</td></tr><tr><td>Milansbar</td><td>66/1</td><td>+6600</td></tr><tr><td>Woods Well</td><td>66/1</td><td>+6600</td></tr><tr><td>Big River</td><td>66/1</td><td>+6600</td></tr><tr><td>Takingrisks</td><td>66/1</td><td>+6600</td></tr><tr><td>Blue Flight</td><td>66/1</td><td>+6600</td></tr><tr><td>Cloth Cap</td><td>66/1</td><td>+6600</td></tr><tr><td>Potters Corner</td><td>66/1</td><td>+6600</td></tr><tr><td>Molly The Dolly</td><td>66/1</td><td>+6600</td></tr><tr><td>Morney Wing</td><td>100/1</td><td>+10000</td></tr><tr><td>Crievehill</td><td>66/1</td><td>+6600</td></tr><tr><td>Top Ville Ben</td><td>40/1</td><td>+4000</td></tr><tr><td>Borice</td><td>33/1</td><td>+3300</td></tr><tr><td>Bristol De Mai</td><td>33/1</td><td>+3300</td></tr><tr><td>Snugsborough Benny</td><td>50/1</td><td>+5000</td></tr><tr><td>Kilfilum Cross</td><td>66/1</td><td>+6600</td></tr><tr><td>Yala Enki</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	