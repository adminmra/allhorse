	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Ncaa Football - Adjusted Lines">
			<caption>Ncaa Football - Adjusted Lines</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 21, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Ncaa Football - Adjusted Lines  - Dec 21</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Florida Intl<br /> - vs - <br /> Toledo</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-3.5<br /> (350)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o51&frac12;-215 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">3.5<br /> (-500)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u51&frac12;+175</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Florida Intl<br /> - vs - <br /> Toledo</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">3.5<br /> (155)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o62&frac12;+180 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-3.5<br /> (-190)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u62&frac12;-220</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Byu<br /> - vs - <br /> Western Michigan</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-7.5<br /> (-200)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o48&frac12;-215 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">7.5<br /> (160)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u48&frac12;+175</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Byu<br /> - vs - <br /> Western Michigan</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-3.5<br /> (-450)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o58&frac12;+185 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">3.5<br /> (325)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u58&frac12;-225</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	