	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Ent - Music">
			<caption>Ent - Music</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Ent - Music  - Feb 10					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Grammy Awards - Album Of The Year</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Black Panther: the Album</td><td>8/5</td><td>+160</td></tr><tr><td>Golden Hour - Kacey Musgraves</td><td>6/5</td><td>+120</td></tr><tr><td>Dirty Computer - Janelle Monae</td><td>7/2</td><td>+350</td></tr><tr><td>Scorpion - Drake</td><td>6/1</td><td>+600</td></tr><tr><td>Invasion Of Privacy - Cardi B</td><td>14/1</td><td>+1400</td></tr><tr><td>By The Way, I Forgive You - Brandi Carlile</td><td>16/1</td><td>+1600</td></tr><tr><td>Beerbongs & Bentleys - Post Malone</td><td>16/1</td><td>+1600</td></tr><tr><td>H.e.r - H.e.r</td><td>25/1</td><td>+2500</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Grammy Awards - Best New Artist</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>H.e.r</td><td>EV</td><td>EV</td></tr><tr><td>Dua Lipa</td><td>13/5</td><td>+260</td></tr><tr><td>Greta Van Fleet</td><td>8/1</td><td>+800</td></tr><tr><td>Bebe Rexha</td><td>14/1</td><td>+1400</td></tr><tr><td>Luke Combs</td><td>20/1</td><td>+2000</td></tr><tr><td>Margo Price</td><td>20/1</td><td>+2000</td></tr><tr><td>Jorja Smith</td><td>20/1</td><td>+2000</td></tr><tr><td>Chole X Halle</td><td>6/1</td><td>+600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Grammy Awards - Record Of The Year</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>This Is America - Childish Gambino</td><td>5/9</td><td>-180</td></tr><tr><td>Shallow - Lady Gaga And Bradley Cooper</td><td>2/1</td><td>+200</td></tr><tr><td>All The Stars - Kendrick Lamar & Sza</td><td>6/1</td><td>+600</td></tr><tr><td>I Like It - Cardi B, Bad Bunny And J Balvin</td><td>12/1</td><td>+1200</td></tr><tr><td>God’s Plan - Drake</td><td>14/1</td><td>+1400</td></tr><tr><td>The Middle - Zeed, Maren Morris And Grey</td><td>16/1</td><td>+1600</td></tr><tr><td>The Joke - Brandi Carlile</td><td>25/1</td><td>+2500</td></tr><tr><td>Rockstar - Post Malone Featuring 21 Savage</td><td>25/1</td><td>+2500</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Grammy Awards - Song Of The Year</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Shallow - Lady Gaga & Bradley Cooper</td><td>10/27</td><td>-270</td></tr><tr><td>This Is America - Childish Gambino</td><td>5/2</td><td>+250</td></tr><tr><td>All The Stars - Kendrick Lamar & Sza</td><td>9/1</td><td>+900</td></tr><tr><td>The Joke - Brandi Carlile</td><td>13/1</td><td>+1300</td></tr><tr><td>God’s Plan - Drake</td><td>12/1</td><td>+1200</td></tr><tr><td>In My Blood - Shawn Mendes</td><td>16/1</td><td>+1600</td></tr><tr><td>Boo’d Up - Ella Mai</td><td>25/1</td><td>+2500</td></tr><tr><td>The Middle - Zeed, Maren Morris & Grey</td><td>25/1</td><td>+2500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');</script>
{/literal}	
	