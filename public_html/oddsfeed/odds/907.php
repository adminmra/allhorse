	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Ent - Tv Shows">
			<caption>Ent - Tv Shows</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Ent - Tv Shows  - Sep 16					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Fall Dancing With The Stars - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Karamo Brown </td><td>3/1</td><td>+300</td></tr><tr><td>Ray Lewis </td><td>7/2</td><td>+350</td></tr><tr><td>Ally Brooke </td><td>4/1</td><td>+400</td></tr><tr><td>Kel Mitchell </td><td>9/2</td><td>+450</td></tr><tr><td>James Van Der Beek </td><td>5/1</td><td>+500</td></tr><tr><td>Lauren Alaina </td><td>13/2</td><td>+650</td></tr><tr><td>Hannah Brown </td><td>9/1</td><td>+900</td></tr><tr><td>Christie Brinkley </td><td>11/1</td><td>+1100</td></tr><tr><td>Mary Wilson </td><td>11/1</td><td>+1100</td></tr><tr><td>Lamar Odom </td><td>15/1</td><td>+1500</td></tr><tr><td>Kate Flannery  </td><td>20/1</td><td>+2000</td></tr><tr><td>Sean Spicer </td><td>40/1</td><td>+4000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Be Eliminated First</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Lamar Odom</td><td>5/14</td><td>-280</td></tr><tr><td>Ray Lewis</td><td>11/5</td><td>+220</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Be Eliminated First</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Kate Flannery</td><td>3/1</td><td>+300</td></tr><tr><td>Sean Spicer</td><td>1/4</td><td>-400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Be Eliminated First</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Ally Brooke</td><td>10/13</td><td>-130</td></tr><tr><td>Kel Mitchell</td><td>EV</td><td>EV</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Be Eliminated First</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>James Van Der Beek</td><td>1/2</td><td>-200</td></tr><tr><td>Lauren Alaina</td><td>8/5</td><td>+160</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');</script>
{/literal}	
	