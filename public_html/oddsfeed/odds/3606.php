	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Cricket - Man Of The Match">
			<caption>Cricket - Man Of The Match</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 27, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Cricket - Man Of The Match  - Jun 28					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">South Africa Vs Sri Lanka - Man Of The Match</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Q De Kock</td><td>15/2</td><td>+750</td></tr><tr><td>F Du Plessis</td><td>9/1</td><td>+900</td></tr><tr><td>K Rabada</td><td>9/1</td><td>+900</td></tr><tr><td>Hm Amla</td><td>9/1</td><td>+900</td></tr><tr><td>Ak Markram</td><td>11/1</td><td>+1100</td></tr><tr><td>Da Miller</td><td>11/1</td><td>+1100</td></tr><tr><td>L Ngidi</td><td>11/1</td><td>+1100</td></tr><tr><td>Kusal Perera</td><td>11/1</td><td>+1100</td></tr><tr><td>I Tahir</td><td>11/1</td><td>+1100</td></tr><tr><td>Thisara Perera</td><td>11/1</td><td>+1100</td></tr><tr><td>Sl Malinga</td><td>13/1</td><td>+1300</td></tr><tr><td>Ch Morris</td><td>13/1</td><td>+1300</td></tr><tr><td>A Mathews</td><td>13/1</td><td>+1300</td></tr><tr><td>Al Phehlukwayo</td><td>15/1</td><td>+1500</td></tr><tr><td>Avishka Fernando</td><td>15/1</td><td>+1500</td></tr><tr><td>Fdm Karunaratne</td><td>15/1</td><td>+1500</td></tr><tr><td>He Van Der Dussen</td><td>15/1</td><td>+1500</td></tr><tr><td>Kusal Mendis</td><td>15/1</td><td>+1500</td></tr><tr><td>Dm De Silva</td><td>19/1</td><td>+1900</td></tr><tr><td>Tam Siriwardana</td><td>19/1</td><td>+1900</td></tr><tr><td>Jp Duminy</td><td>19/1</td><td>+1900</td></tr><tr><td>Hdrl Thirimanne</td><td>19/1</td><td>+1900</td></tr><tr><td>I Udana</td><td>19/1</td><td>+1900</td></tr><tr><td>Ras Lakmal</td><td>19/1</td><td>+1900</td></tr><tr><td>Be Hendricks</td><td>24/1</td><td>+2400</td></tr><tr><td>T Shamsi</td><td>24/1</td><td>+2400</td></tr><tr><td>Nuwan Pradeep</td><td>24/1</td><td>+2400</td></tr><tr><td>Jeevan Mendis</td><td>24/1</td><td>+2400</td></tr><tr><td>D Petrorius</td><td>24/1</td><td>+2400</td></tr><tr><td>Jdf Vandersay</td><td>27/1</td><td>+2700</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	