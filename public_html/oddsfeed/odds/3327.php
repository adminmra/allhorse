	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--bk - Italy (serie A)">
			<caption>Live--bk - Italy (serie A)</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 20, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--bk - Italy (serie A)  - May 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New Basket Brindisi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o168&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >160</div>
								 <div class="boxdata" >+5&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dinamo Sassari</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u168&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-285</div>
								 <div class="boxdata" >-5&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Felice Scandone</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o163&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >265</div>
								 <div class="boxdata" >+8&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olimpia Milano</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u163&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-625</div>
								 <div class="boxdata" >-8&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	