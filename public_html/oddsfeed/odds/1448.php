	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - 2000 Guineas">
			<caption>Horses - 2000 Guineas</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - 2000 Guineas  - May 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2000 Guineas - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Pinatubo</td><td>3/1</td><td>+300</td></tr><tr><td>Arizona</td><td>16/1</td><td>+1600</td></tr><tr><td>Lope Y Fernandez</td><td>16/1</td><td>+1600</td></tr><tr><td>Threat</td><td>20/1</td><td>+2000</td></tr><tr><td>Monarch Of Egypt</td><td>16/1</td><td>+1600</td></tr><tr><td>Year Of The Tiger</td><td>40/1</td><td>+4000</td></tr><tr><td>Siskin</td><td>8/1</td><td>+800</td></tr><tr><td>Guildsman</td><td>33/1</td><td>+3300</td></tr><tr><td>Hong Kong</td><td>33/1</td><td>+3300</td></tr><tr><td>Southern Hills</td><td>33/1</td><td>+3300</td></tr><tr><td>King`s Command</td><td>66/1</td><td>+6600</td></tr><tr><td>Fort Myers</td><td>50/1</td><td>+5000</td></tr><tr><td>Visinari</td><td>50/1</td><td>+5000</td></tr><tr><td>Cormorant</td><td>40/1</td><td>+4000</td></tr><tr><td>Vatican City</td><td>40/1</td><td>+4000</td></tr><tr><td>Paradiso</td><td>40/1</td><td>+4000</td></tr><tr><td>Royal Lytham</td><td>16/1</td><td>+1600</td></tr><tr><td>Sunday Sovereign</td><td>50/1</td><td>+5000</td></tr><tr><td>Well Of Wisdom</td><td>40/1</td><td>+4000</td></tr><tr><td>Monoski</td><td>66/1</td><td>+6600</td></tr><tr><td>Armory</td><td>12/1</td><td>+1200</td></tr><tr><td>Man Of The Night</td><td>25/1</td><td>+2500</td></tr><tr><td>Al Suhail</td><td>33/1</td><td>+3300</td></tr><tr><td>Al Madhar</td><td>25/1</td><td>+2500</td></tr><tr><td>Mystery Power</td><td>25/1</td><td>+2500</td></tr><tr><td>Juan Elcano</td><td>16/1</td><td>+1600</td></tr><tr><td>Earthlight</td><td>12/1</td><td>+1200</td></tr><tr><td>Toronto</td><td>33/1</td><td>+3300</td></tr><tr><td>Tsar</td><td>33/1</td><td>+3300</td></tr><tr><td>Russian Emperor</td><td>33/1</td><td>+3300</td></tr><tr><td>Positive</td><td>20/1</td><td>+2000</td></tr><tr><td>Native Tribe</td><td>66/1</td><td>+6600</td></tr><tr><td>Malvern</td><td>100/1</td><td>+10000</td></tr><tr><td>Mogul</td><td>16/1</td><td>+1600</td></tr><tr><td>Cepheus</td><td>50/1</td><td>+5000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	