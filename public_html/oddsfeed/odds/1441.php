	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Us Presidential Election - Winning Party">
			<caption>Us Presidential Election - Winning Party</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Us Presidential Election - Winning Party  - Nov 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Us Presidential Election - Winning Party</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Democratic Party</td><td>8/5</td><td>+160</td></tr><tr><td>Republican Party</td><td>1/2</td><td>-200</td></tr><tr><td>Any Other Party</td><td>50/1</td><td>+5000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	