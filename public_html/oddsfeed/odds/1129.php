	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nfl - Superbowl - To Win">
			<caption>Nfl - Superbowl - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nfl - Superbowl - To Win  - Sep 05					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Superbowl 54 - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>New England Patriots</td><td>7/1</td><td>+700</td></tr><tr><td>Los Angeles Rams</td><td>15/2</td><td>+750</td></tr><tr><td>Kansas City Chiefs</td><td>8/1</td><td>+800</td></tr><tr><td>New Orleans Saints</td><td>8/1</td><td>+800</td></tr><tr><td>Los Angeles Chargers</td><td>14/1</td><td>+1400</td></tr><tr><td>Philadelphia Eagles</td><td>14/1</td><td>+1400</td></tr><tr><td>Cleveland Browns</td><td>14/1</td><td>+1400</td></tr><tr><td>Chicago Bears</td><td>14/1</td><td>+1400</td></tr><tr><td>Indianapolis Colts</td><td>16/1</td><td>+1600</td></tr><tr><td>Green Bay Packers</td><td>20/1</td><td>+2000</td></tr><tr><td>Pittsburgh Steelers</td><td>20/1</td><td>+2000</td></tr><tr><td>Dallas Cowboys</td><td>22/1</td><td>+2200</td></tr><tr><td>Minnesota Vikings</td><td>28/1</td><td>+2800</td></tr><tr><td>Seattle Seahawks</td><td>28/1</td><td>+2800</td></tr><tr><td>Atlanta Falcons</td><td>33/1</td><td>+3300</td></tr><tr><td>Baltimore Ravens</td><td>33/1</td><td>+3300</td></tr><tr><td>Houston Texans</td><td>33/1</td><td>+3300</td></tr><tr><td>San Francisco 49ers</td><td>33/1</td><td>+3300</td></tr><tr><td>Jacksonville Jaguars</td><td>33/1</td><td>+3300</td></tr><tr><td>Carolina Panthers</td><td>50/1</td><td>+5000</td></tr><tr><td>Tennessee Titans</td><td>60/1</td><td>+6000</td></tr><tr><td>Tampa Bay Buccaneers</td><td>60/1</td><td>+6000</td></tr><tr><td>Oakland Raiders</td><td>66/1</td><td>+6600</td></tr><tr><td>New York Giants</td><td>66/1</td><td>+6600</td></tr><tr><td>Denver Broncos</td><td>66/1</td><td>+6600</td></tr><tr><td>New York Jets</td><td>80/1</td><td>+8000</td></tr><tr><td>Detroit Lions</td><td>80/1</td><td>+8000</td></tr><tr><td>Arizona Cardinals</td><td>80/1</td><td>+8000</td></tr><tr><td>Washington Redskins</td><td>80/1</td><td>+8000</td></tr><tr><td>Buffalo Bills</td><td>100/1</td><td>+10000</td></tr><tr><td>Cincinnati Bengals</td><td>125/1</td><td>+12500</td></tr><tr><td>Miami Dolphins</td><td>125/1</td><td>+12500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	