	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Racing - Formula 1">
			<caption>Racing - Formula 1</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Racing - Formula 1  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Singapore Grand Prix - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Lewis Hamilton</td><td>27/20</td><td>+135</td></tr><tr><td>Max Verstappen</td><td>3/2</td><td>+150</td></tr><tr><td>Valtteri Bottas</td><td>6/1</td><td>+600</td></tr><tr><td>Alexander Albon</td><td>7/1</td><td>+700</td></tr><tr><td>Charles Leclerc</td><td>12/1</td><td>+1200</td></tr><tr><td>Sebastian Vettel</td><td>14/1</td><td>+1400</td></tr><tr><td>Lando Norris</td><td>500/1</td><td>+50000</td></tr><tr><td>Carlos Sainz</td><td>500/1</td><td>+50000</td></tr><tr><td>Kimi Raikkonen</td><td>750/1</td><td>+75000</td></tr><tr><td>Daniel Ricciardo</td><td>1000/1</td><td>+100000</td></tr><tr><td>Nico Hulkenberg</td><td>1000/1</td><td>+100000</td></tr><tr><td>Lance Stroll</td><td>1500/1</td><td>+150000</td></tr><tr><td>Sergio Perez</td><td>1500/1</td><td>+150000</td></tr><tr><td>Daniil Kvyat</td><td>1500/1</td><td>+150000</td></tr><tr><td>Pierre Gasly</td><td>1500/1</td><td>+150000</td></tr><tr><td>Antonio Giovinazzi</td><td>2000/1</td><td>+200000</td></tr><tr><td>Romain Grosjean</td><td>2000/1</td><td>+200000</td></tr><tr><td>Kevin Magnussen</td><td>2000/1</td><td>+200000</td></tr><tr><td>George Russell</td><td>4000/1</td><td>+400000</td></tr><tr><td>Robert Kubica</td><td>4000/1</td><td>+400000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	