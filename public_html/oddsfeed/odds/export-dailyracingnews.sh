#!/bin/bash

php /home/ah/news.usracing.com/wp-cli.phar export --path='/home/ah/news.usracing.com/' --dir='/home/ah/news.usracing.com/exports_xml' --user=admin --post_type=post --start_date=`date -d '2 day ago' '+%Y-%m-%d'` --end_date=`date +%Y-%m-%d` --filename_format='wp_export.xml'

#php /home/ah/news.usracing.com/wp-cli.phar export --dir='./exports_xml' --user=admin --post_type=attachment --start_date=2016-09-01 --end_date=2016-10-30

sed -Ei 's/http:\/\/news.usracing.com/https:\/\/www.dailyracingnews.com/' /home/ah/news.usracing.com/exports_xml/wp_export.xml

php /home/ah/news.usracing.com/wp-cli.phar export --path='/home/ah/news.usracing.com/' --dir='/home/ah/news.usracing.com/exports_xml' --user=admin --post_type=attachment --start_date=`date -d '2 day ago' '+%Y-%m-%d'` --end_date=`date +%Y-%m-%d` --filename_format='wp_export_attachment.xml'

sed -Ei 's/http:\/\/news.usracing.com/https:\/\/www.dailyracingnews.com/' /home/ah/news.usracing.com/exports_xml/wp_export_attachment.xml


#rsync -avz /home/ah/news.usracing.com/wp-content/uploads /home/ah/dailyracingnews.com/wp-content/uploads

/usr/bin/rsync -avz /home/ah/news.usracing.com/wp-content/uploads/ /home/ah/dailyracingnews.com/wp-content/uploads/

#http://news.usracing.com
#https://www.dailyracingnews.com/
php /home/ah/dailyracingnews.com/wp-cli.phar import --path='/home/ah/dailyracingnews.com/' /home/ah/news.usracing.com/exports_xml/wp_export.xml --authors=create
php /home/ah/dailyracingnews.com/wp-cli.phar import --path='/home/ah/dailyracingnews.com/' /home/ah/news.usracing.com/exports_xml/wp_export_attachment.xml --authors=create


# wp media regenerate --yes
# seq 1000 2000 | xargs php wp-cli.phar media regenerate
