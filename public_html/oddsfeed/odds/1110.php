	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Kentucky Derby Trainer">
			<caption>Horses - Kentucky Derby Trainer</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Kentucky Derby Trainer  - May 04					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Kenrucky Derby Trainer - Odds To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Mark E. Casse</td><td>15/1</td><td>+1500</td></tr><tr><td>Danny Gargan</td><td>22/1</td><td>+2200</td></tr><tr><td>W. Bret Calhoun</td><td>15/1</td><td>+1500</td></tr><tr><td>Peter Miller</td><td>45/1</td><td>+4500</td></tr><tr><td>Bob Baffert</td><td>20/27</td><td>-135</td></tr><tr><td>George Weaver</td><td>22/1</td><td>+2200</td></tr><tr><td>Jason Servis</td><td>8/1</td><td>+800</td></tr><tr><td>William I. Mott</td><td>5/1</td><td>+500</td></tr><tr><td>Brendan P. Walsh</td><td>45/1</td><td>+4500</td></tr><tr><td>Todd A. Pletcher</td><td>16/1</td><td>+1600</td></tr><tr><td>Claude R. Mcgaughey Iii</td><td>12/1</td><td>+1200</td></tr><tr><td>Michael J. Trombetta</td><td>20/1</td><td>+2000</td></tr><tr><td>Koichi Tsunoda</td><td>50/1</td><td>+5000</td></tr><tr><td>Steven M. Asmussen</td><td>45/1</td><td>+4500</td></tr><tr><td>Gustavo Delgado</td><td>30/1</td><td>+3000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	