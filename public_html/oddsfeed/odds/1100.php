	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Donald Trump - Specials">
			<caption>Donald Trump - Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Donald Trump - Specials  - Dec 31					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Trump Resign Before The End Of His First Term</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>12/1</td><td>+1200</td></tr><tr><td>No</td><td>1/90</td><td>-9000</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					Donald Trump - Specials  - Dec 31					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Proof Of Trump Visiting Parlour Before Presidency</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>12/1</td><td>+1200</td></tr><tr><td>No</td><td>1/50</td><td>-5000</td></tr></tbody></table></td></tr>        <tr><th colspan="3"  class="datahead">Donald Trump - Specials  - Dec 31</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" ></div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Proof Of Trump Visiting Parlour After Become Potus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	