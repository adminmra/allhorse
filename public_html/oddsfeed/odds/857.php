	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Portugal Primeira Liga - To Win">
			<caption>Soccer - Portugal Primeira Liga - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Portugal Primeira Liga - To Win  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-2020 Portugal Primeira Liga - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Benfica</td><td>EV</td><td>EV</td></tr><tr><td>Fc Porto</td><td>11/10</td><td>+110</td></tr><tr><td>Sporting</td><td>10/1</td><td>+1000</td></tr><tr><td>Braga</td><td>125/1</td><td>+12500</td></tr><tr><td>Guimaraes</td><td>500/1</td><td>+50000</td></tr><tr><td>Rio Ave</td><td>200/1</td><td>+20000</td></tr><tr><td>Moreirense</td><td>750/1</td><td>+75000</td></tr><tr><td>Boavista</td><td>250/1</td><td>+25000</td></tr><tr><td>Vitoria Setubal</td><td>750/1</td><td>+75000</td></tr><tr><td>Pacos Ferreira</td><td>4500/1</td><td>+450000</td></tr><tr><td>Santa Clara</td><td>1000/1</td><td>+100000</td></tr><tr><td>Belenenses</td><td>2000/1</td><td>+200000</td></tr><tr><td>Portimonense</td><td>3000/1</td><td>+300000</td></tr><tr><td>Maritimo</td><td>4000/1</td><td>+400000</td></tr><tr><td>Cd Aves</td><td>2500/1</td><td>+250000</td></tr><tr><td>Tondela</td><td>2000/1</td><td>+200000</td></tr><tr><td>Famalicao</td><td>125/1</td><td>+12500</td></tr><tr><td>Gil Vicente</td><td>4500/1</td><td>+450000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	