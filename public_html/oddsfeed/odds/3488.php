	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Odds To Win The West Bracket">
			<caption>Odds To Win The West Bracket</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 28, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Odds To Win The West Bracket  - Mar 28					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Odds To Win West Region</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Gonzaga</td><td>4/5</td><td>-125</td></tr><tr><td>Michigan</td><td>17/5</td><td>+340</td></tr><tr><td>Texas Tech</td><td>39/10</td><td>+390</td></tr><tr><td>Florida State</td><td>73/10</td><td>+730</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	