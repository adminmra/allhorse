	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - Champions Tour - Odds To Win">
			<caption>Golf - Champions Tour - Odds To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - Champions Tour - Odds To Win  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Sanford International - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Bernhard Langer</td><td>15/2</td><td>+750</td></tr><tr><td>Jerry Kelly</td><td>8/1</td><td>+800</td></tr><tr><td>Scott Mccarron</td><td>17/2</td><td>+850</td></tr><tr><td>Woody Austin</td><td>10/1</td><td>+1000</td></tr><tr><td>Retief Goosen</td><td>11/1</td><td>+1100</td></tr><tr><td>David Toms</td><td>14/1</td><td>+1400</td></tr><tr><td>Brandt Jobe</td><td>16/1</td><td>+1600</td></tr><tr><td>Tim Petrovic</td><td>18/1</td><td>+1800</td></tr><tr><td>Doug Barron</td><td>22/1</td><td>+2200</td></tr><tr><td>Ken Duke</td><td>22/1</td><td>+2200</td></tr><tr><td>Tom Lehman</td><td>25/1</td><td>+2500</td></tr><tr><td>Steve Flesch</td><td>25/1</td><td>+2500</td></tr><tr><td>Paul Broadhurst</td><td>30/1</td><td>+3000</td></tr><tr><td>Colin Montgomerie</td><td>40/1</td><td>+4000</td></tr><tr><td>Joe Durant</td><td>40/1</td><td>+4000</td></tr><tr><td>Tom Gillis</td><td>40/1</td><td>+4000</td></tr><tr><td>Davis Love Iii</td><td>40/1</td><td>+4000</td></tr><tr><td>Doug Garwood</td><td>40/1</td><td>+4000</td></tr><tr><td>Vijay Singh</td><td>45/1</td><td>+4500</td></tr><tr><td>Wes Short Jr</td><td>45/1</td><td>+4500</td></tr><tr><td>Marco Dawson</td><td>45/1</td><td>+4500</td></tr><tr><td>Kirk Triplett</td><td>45/1</td><td>+4500</td></tr><tr><td>Stephen Ames</td><td>50/1</td><td>+5000</td></tr><tr><td>Paul Goydos</td><td>50/1</td><td>+5000</td></tr><tr><td>Bob Estes</td><td>50/1</td><td>+5000</td></tr><tr><td>Jay Haas</td><td>66/1</td><td>+6600</td></tr><tr><td>David Mckenzie</td><td>66/1</td><td>+6600</td></tr><tr><td>Stephen Leaney</td><td>70/1</td><td>+7000</td></tr><tr><td>Ken Tanigawa</td><td>70/1</td><td>+7000</td></tr><tr><td>Duffy Waldorf</td><td>70/1</td><td>+7000</td></tr><tr><td>Kent Jones</td><td>75/1</td><td>+7500</td></tr><tr><td>Angel Cabrera</td><td>80/1</td><td>+8000</td></tr><tr><td>Lee Janzen</td><td>100/1</td><td>+10000</td></tr><tr><td>Tom Byrum</td><td>125/1</td><td>+12500</td></tr><tr><td>Glen Day</td><td>125/1</td><td>+12500</td></tr><tr><td>Rocco Mediate</td><td>125/1</td><td>+12500</td></tr><tr><td>John Daly</td><td>125/1</td><td>+12500</td></tr><tr><td>Gene Sauers</td><td>140/1</td><td>+14000</td></tr><tr><td>Tommy Tolles</td><td>150/1</td><td>+15000</td></tr><tr><td>Jerry Smith</td><td>160/1</td><td>+16000</td></tr><tr><td>Billy Mayfair</td><td>160/1</td><td>+16000</td></tr><tr><td>Michael Bradley</td><td>160/1</td><td>+16000</td></tr><tr><td>Chris Dimarco</td><td>160/1</td><td>+16000</td></tr><tr><td>Jesper Parnevik</td><td>175/1</td><td>+17500</td></tr><tr><td>Corey Pavin</td><td>175/1</td><td>+17500</td></tr><tr><td>Jeff Sluman</td><td>200/1</td><td>+20000</td></tr><tr><td>Olin Browne</td><td>200/1</td><td>+20000</td></tr><tr><td>Tom Pernice Jr</td><td>200/1</td><td>+20000</td></tr><tr><td>David Frost</td><td>200/1</td><td>+20000</td></tr><tr><td>Gibby Gilbert</td><td>200/1</td><td>+20000</td></tr><tr><td>Shaun Micheel</td><td>200/1</td><td>+20000</td></tr><tr><td>Fred Funk</td><td>300/1</td><td>+30000</td></tr><tr><td>John Huston</td><td>300/1</td><td>+30000</td></tr><tr><td>Michael Allen</td><td>350/1</td><td>+35000</td></tr><tr><td>Tommy Armour Iii</td><td>400/1</td><td>+40000</td></tr><tr><td>Joey Sindelar</td><td>400/1</td><td>+40000</td></tr><tr><td>Greg Kraft</td><td>400/1</td><td>+40000</td></tr><tr><td>Cliff Kresge</td><td>500/1</td><td>+50000</td></tr><tr><td>Skip Kendall</td><td>600/1</td><td>+60000</td></tr><tr><td>Dudley Hart</td><td>600/1</td><td>+60000</td></tr><tr><td>Steve Jones</td><td>600/1</td><td>+60000</td></tr><tr><td>Sandy Lyle</td><td>600/1</td><td>+60000</td></tr><tr><td>Mark Calcavecchia</td><td>1000/1</td><td>+100000</td></tr><tr><td>Russ Cochran</td><td>1000/1</td><td>+100000</td></tr><tr><td>Robert Gamez</td><td>1000/1</td><td>+100000</td></tr><tr><td>Scott Simpson</td><td>1000/1</td><td>+100000</td></tr><tr><td>Larry Mize</td><td>1500/1</td><td>+150000</td></tr><tr><td>Dan Forsman</td><td>2000/1</td><td>+200000</td></tr><tr><td>John Harris</td><td>2000/1</td><td>+200000</td></tr><tr><td>Gary Nicklaus</td><td>2000/1</td><td>+200000</td></tr><tr><td>Dave Stockton</td><td>2000/1</td><td>+200000</td></tr><tr><td>Mark Brooks</td><td>2500/1</td><td>+250000</td></tr><tr><td>John Jacobs</td><td>2500/1</td><td>+250000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	