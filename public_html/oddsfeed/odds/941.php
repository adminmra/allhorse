	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Racing - Nascar Xfinity Series">
			<caption>Racing - Nascar Xfinity Series</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Racing - Nascar Xfinity Series  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Go Bowling 250 - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Christopher Bell</td><td>5/4</td><td>+125</td></tr><tr><td>Cole Custer</td><td>13/4</td><td>+325</td></tr><tr><td>Justin Allgaier</td><td>6/1</td><td>+600</td></tr><tr><td>Tyler Reddick</td><td>6/1</td><td>+600</td></tr><tr><td>Chase Briscoe</td><td>10/1</td><td>+1000</td></tr><tr><td>Austin Cindric</td><td>28/1</td><td>+2800</td></tr><tr><td>Noah Gragson</td><td>40/1</td><td>+4000</td></tr><tr><td>Brandon Jones</td><td>40/1</td><td>+4000</td></tr><tr><td>Harrison Burton</td><td>66/1</td><td>+6600</td></tr><tr><td>Zane Smith</td><td>66/1</td><td>+6600</td></tr><tr><td>Michael Annett</td><td>80/1</td><td>+8000</td></tr><tr><td>John Hunter Nemechek</td><td>80/1</td><td>+8000</td></tr><tr><td>Justin Haley</td><td>100/1</td><td>+10000</td></tr><tr><td>Ryan Sieg</td><td>250/1</td><td>+25000</td></tr><tr><td>Joe Graf Jr</td><td>500/1</td><td>+50000</td></tr><tr><td>Field</td><td>22/1</td><td>+2200</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	