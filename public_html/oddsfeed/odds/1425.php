	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Russia Vtb United League">
			<caption>Russia Vtb United League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Russia Vtb United League  - Jun 10</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cska Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o170&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-390</div>
								 <div class="boxdata" >-8<br /> ( -110)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Khimki M.</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u170&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >305</div>
								 <div class="boxdata" >+8<br /> ( -110)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	