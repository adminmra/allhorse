	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba - Specials">
			<caption>Nba - Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nba - Specials  - Dec 31					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Nba Season Lonzo Ball Assists Per Game</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 6 Apg - Regular Season</td><td>5/6</td><td>-120</td></tr><tr><td>Under 6 Apg - Regular Season</td><td>10/11</td><td>-110</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Nba Season Anthony Davis Points Per Game</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 25.5 Ppg - Regular Season</td><td>5/6</td><td>-120</td></tr><tr><td>Under 25.5 Ppg - Regular Season</td><td>10/11</td><td>-110</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Nba Season Anthony Davis Rebounds Per Game</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 10.5 Rpg - Regular Season</td><td>5/6</td><td>-120</td></tr><tr><td>Under 10.5 Rpg - Regular Season</td><td>10/11</td><td>-110</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Nba Season Anthony Davis Missed Games</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 11.5 Missed Games - Regular Season</td><td>5/7</td><td>-140</td></tr><tr><td>Under 11.5 Missed Games - Regular Season</td><td>11/10</td><td>+110</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Nba Season Lonzo Ball Points Per Game</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 11 Ppg - Regular Season</td><td>20/23</td><td>-115</td></tr><tr><td>Under 11 Ppg - Regular Season</td><td>20/23</td><td>-115</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');</script>
{/literal}	
	