	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--slovakia 2. Liga">
			<caption>Pre--slovakia 2. Liga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--slovakia 2. Liga  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Slavoj Trebisov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >869</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Poprad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >-416</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Petrzalka 1898</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >403</div>
								 <div class="boxdata" >+1<br /> ( -129)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mfk Skalica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >-175</div>
								 <div class="boxdata" >-1<br /> ( -102)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mfk Ruzomberok B</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" >+&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Kosice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac12;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Partizan Bardejov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >295</div>
								 <div class="boxdata" >+&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Zeleziarne Podbrezova</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >-128</div>
								 <div class="boxdata" >-&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--slovakia 2. Liga  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Stk 1914 Samorin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >271</div>
								 <div class="boxdata" >+&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Puchov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >-125</div>
								 <div class="boxdata" >-&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mfk Dukla Banska Bystrica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >148</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kfc Komarno</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Dubnica Nad Vahom</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >160</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mfk Tatran Liptovsky Mikulas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -116 )</div>
								 <div class="boxdata" >139</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Msk Zilina B</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >116</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Slovan Bratislava B</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	