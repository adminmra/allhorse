	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba -  All Star Game Mvp">
			<caption>Nba -  All Star Game Mvp</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 17, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nba -  All Star Game Mvp  - Feb 17					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Nba All Star Game Mvp - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Anthony Davis</td><td>10/1</td><td>+1000</td></tr><tr><td>Ben Simmons</td><td>33/1</td><td>+3300</td></tr><tr><td>Blake Griffin</td><td>33/1</td><td>+3300</td></tr><tr><td>Bradley Beal</td><td>40/1</td><td>+4000</td></tr><tr><td>D'angelo Russell</td><td>100/1</td><td>+10000</td></tr><tr><td>Damian Lillard</td><td>40/1</td><td>+4000</td></tr><tr><td>Dirk Nowitzki</td><td>50/1</td><td>+5000</td></tr><tr><td>Dwyane Wade</td><td>20/1</td><td>+2000</td></tr><tr><td>Giannis Antetokounmpo</td><td>6/1</td><td>+600</td></tr><tr><td>James Harden</td><td>8/1</td><td>+800</td></tr><tr><td>Joel Embiid</td><td>10/1</td><td>+1000</td></tr><tr><td>Karl-anthony Towns</td><td>40/1</td><td>+4000</td></tr><tr><td>Kawhi Leonard</td><td>14/1</td><td>+1400</td></tr><tr><td>Kemba Walker</td><td>15/2</td><td>+750</td></tr><tr><td>Kevin Durant</td><td>6/1</td><td>+600</td></tr><tr><td>Khris Middleton</td><td>80/1</td><td>+8000</td></tr><tr><td>Klay Thompson</td><td>40/1</td><td>+4000</td></tr><tr><td>Kyle Lowry</td><td>66/1</td><td>+6600</td></tr><tr><td>Kyrie Irving</td><td>9/1</td><td>+900</td></tr><tr><td>Lamarcus Aldridge</td><td>50/1</td><td>+5000</td></tr><tr><td>Nikola Jokic</td><td>66/1</td><td>+6600</td></tr><tr><td>Nikola Vucevic</td><td>100/1</td><td>+10000</td></tr><tr><td>Paul George</td><td>8/1</td><td>+800</td></tr><tr><td>Russell Westbrook</td><td>12/1</td><td>+1200</td></tr><tr><td>Stephen Curry  </td><td>5/1</td><td>+500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	