	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--france Ligue 2">
			<caption>Pre--france Ligue 2</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--france Ligue 2  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Troyes Ac</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -117)</div>
								 <div class="boxdata" >244</div>
								 <div class="boxdata" >+&frac12;<br /> ( -158 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aj Auxerre</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -112)</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" >-&frac12;<br /> ( +119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ea Guingamp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -133 )</div>
								 <div class="boxdata" >168</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sochaux</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( EV )</div>
								 <div class="boxdata" >185</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Clermont Foot</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Paris Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Grenoble Foot</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -135)</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lb Chateauroux</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( +102)</div>
								 <div class="boxdata" >163</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Ajaccio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >167</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Le Mans Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >171</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chamois Niort Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >237</div>
								 <div class="boxdata" >+&frac12;<br /> ( -151 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Valenciennes Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >117</div>
								 <div class="boxdata" >-&frac12;<br /> ( +114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Chambly Oise</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -106)</div>
								 <div class="boxdata" >219</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Nancy</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -125)</div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Havre</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Orleans 45</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >211</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--france Ligue 2  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rc Lens</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -117)</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac12;<br /> ( +106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sm Caen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -113)</div>
								 <div class="boxdata" >271</div>
								 <div class="boxdata" >+&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--france Ligue 2  - Sep 23</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rodez Aveyron</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -120)</div>
								 <div class="boxdata" >391</div>
								 <div class="boxdata" >+&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Lorient</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -113)</div>
								 <div class="boxdata" >-140</div>
								 <div class="boxdata" >-&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	