	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--international Friendlies">
			<caption>Pre--international Friendlies</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--international Friendlies  - Sep 10</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Peru</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >617</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brazil</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -109 )</div>
								 <div class="boxdata" >-263</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bulgaria</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >334</div>
								 <div class="boxdata" >+&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ireland</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Venezuela</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >499</div>
								 <div class="boxdata" >+1<br /> ( -123)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Colombia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" >-1<br /> ( -107)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Uruguay</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac12;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Usa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >263</div>
								 <div class="boxdata" >+&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bolivia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >592</div>
								 <div class="boxdata" >+1<br /> ( -104)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ecuador</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >-217</div>
								 <div class="boxdata" >-1<br /> ( -128)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nigeria</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >433</div>
								 <div class="boxdata" >+&frac12;<br /> ( +114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ukraine</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >-151</div>
								 <div class="boxdata" >-&frac12;<br /> ( -151 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Paraguay</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >-102</div>
								 <div class="boxdata" >-&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jordan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >285</div>
								 <div class="boxdata" >+&frac12;<br /> ( -126 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mexico</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >158</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Argentina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >167</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chile</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" >-&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Honduras</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >365</div>
								 <div class="boxdata" >+&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ivory Coast</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -107)</div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tunisia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -123)</div>
								 <div class="boxdata" >159</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Niger</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >1109</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Morocco</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-384</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lebanon</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -121)</div>
								 <div class="boxdata" >291</div>
								 <div class="boxdata" >+&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Oman</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -108)</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac12;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	