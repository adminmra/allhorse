	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--denmark Landspokal Cup">
			<caption>Pre--denmark Landspokal Cup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--denmark Landspokal Cup  - Sep 10</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hvidovre If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -117)</div>
								 <div class="boxdata" >-238</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B 1908 Amager</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -112)</div>
								 <div class="boxdata" >535</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -147 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vejle Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >118</div>
								 <div class="boxdata" >-&frac12;<br /> ( +114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vendsyssel Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" >+&frac12;<br /> ( -153 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Skive Ik</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >142</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Middelfart Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Thisted Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >-169</div>
								 <div class="boxdata" >-1<br /> ( -107)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Varde If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >362</div>
								 <div class="boxdata" >+1<br /> ( -123)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Viborg Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >-9999</div>
								 <div class="boxdata" >-5&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Soenderborg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >4900</div>
								 <div class="boxdata" >+5&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--denmark Landspokal Cup  - Sep 11</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lyngby Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -104)</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bk Fremad Amager</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -126)</div>
								 <div class="boxdata" >229</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Agf Aarhus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6<br /> ( -114)</div>
								 <div class="boxdata" >-9999</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Marstal/rise</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6<br /> ( -114)</div>
								 <div class="boxdata" >3564</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aalborg Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5<br /> ( -109)</div>
								 <div class="boxdata" >-9999</div>
								 <div class="boxdata" >-4&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Norresundby Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5<br /> ( -120)</div>
								 <div class="boxdata" >4900</div>
								 <div class="boxdata" >+4&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	