	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nfl - Defensive Rookie Of The Year">
			<caption>Nfl - Defensive Rookie Of The Year</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nfl - Defensive Rookie Of The Year  - Sep 05					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Nfl Defensive Rookie Of The Year</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Nick Bosa</td><td>4/1</td><td>+400</td></tr><tr><td>Josh Allen</td><td>6/1</td><td>+600</td></tr><tr><td>Quinnen Williams</td><td>4/1</td><td>+400</td></tr><tr><td>Clelin Ferrell</td><td>10/1</td><td>+1000</td></tr><tr><td>Devin White</td><td>7/1</td><td>+700</td></tr><tr><td>Ed Oliver</td><td>8/1</td><td>+800</td></tr><tr><td>Devin Bush</td><td>7/1</td><td>+700</td></tr><tr><td>Rashan Gary</td><td>20/1</td><td>+2000</td></tr><tr><td>Christian Wilkins</td><td>25/1</td><td>+2500</td></tr><tr><td>Brian Burns</td><td>30/1</td><td>+3000</td></tr><tr><td>Dexter Lawrence</td><td>30/1</td><td>+3000</td></tr><tr><td>Jeffrey Simmons</td><td>25/1</td><td>+2500</td></tr><tr><td>Darnell Savage Jr</td><td>25/1</td><td>+2500</td></tr><tr><td>Montez Sweat</td><td>15/1</td><td>+1500</td></tr><tr><td>Johnathan Abram</td><td>35/1</td><td>+3500</td></tr><tr><td>Jerry Tillery</td><td>35/1</td><td>+3500</td></tr><tr><td>Deandre Baker</td><td>35/1</td><td>+3500</td></tr><tr><td>Byron Murphy</td><td>35/1</td><td>+3500</td></tr><tr><td>Rock Ya-sin</td><td>40/1</td><td>+4000</td></tr><tr><td>Sean Bunting</td><td>40/1</td><td>+4000</td></tr><tr><td>Trayvon Mullen</td><td>40/1</td><td>+4000</td></tr><tr><td>Jahlani Tavai</td><td>40/1</td><td>+4000</td></tr><tr><td>Joejuan Williams</td><td>50/1</td><td>+5000</td></tr><tr><td>Greedy Williams</td><td>25/1</td><td>+2500</td></tr><tr><td>Marquise Blair</td><td>80/1</td><td>+8000</td></tr><tr><td>Ben Banogu</td><td>60/1</td><td>+6000</td></tr><tr><td>Lonnie Johnson Jr</td><td>50/1</td><td>+5000</td></tr><tr><td>Trysten Hill</td><td>50/1</td><td>+5000</td></tr><tr><td>Nasir Adderly</td><td>80/1</td><td>+8000</td></tr><tr><td>Taylor Rapp</td><td>50/1</td><td>+5000</td></tr><tr><td>Juan Thornhill</td><td>50/1</td><td>+5000</td></tr><tr><td>The Field</td><td>8/1</td><td>+800</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	