	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mlb - Home Run Derby">
			<caption>Mlb - Home Run Derby</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Mlb - Home Run Derby  - Jul 08					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Home Run Derby - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Pete Alonso</td><td>4/1</td><td>+400</td></tr><tr><td>Vladimir Guerrero</td><td>16/5</td><td>+320</td></tr><tr><td>Joc Pederson</td><td>6/1</td><td>+600</td></tr><tr><td>Ronald Acuna</td><td>7/1</td><td>+700</td></tr><tr><td>Josh Bell</td><td>3/1</td><td>+300</td></tr><tr><td>Alex Bregman</td><td>8/1</td><td>+800</td></tr><tr><td>Carlos Santana</td><td>9/1</td><td>+900</td></tr><tr><td>Matt Chapman</td><td>11/2</td><td>+550</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	