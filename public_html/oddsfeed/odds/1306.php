	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Romania - Liga 1">
			<caption>Romania - Liga 1</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 3, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Romania - Liga 1  - Dec 03</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Botosani<br /> - vs - <br /> Astra</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-130)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+270</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+129 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (100)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+103</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-164</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Romania - Liga 1  - Dec 04</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Cfr Cluj<br /> - vs - <br /> Dinamo Bucuresti</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-101</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+129 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+279</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-164</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Hermannstadt<br /> - vs - <br /> Voluntari</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+157</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2-130 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+179</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2EV</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Romania - Liga 1  - Dec 05</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Steaua Bucuresti<br /> - vs - <br /> Sepsi</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (-105)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-102</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+120 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-125)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+283</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-150</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Viitorul<br /> - vs - <br /> Gaz Metan Medias</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+167</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+129 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+166</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-164</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Romania - Liga 1  - Dec 06</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Cs U Craiova<br /> - vs - <br /> Concordia Chiajna</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-133</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"> </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+348</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Csm Iasi<br /> - vs - <br /> Botosani</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+208</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"> </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+128</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Astra<br /> - vs - <br /> Dunarea Calarasi</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+149</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"> </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+193</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	