	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Belmont Stakes - To Win">
			<caption>Horses - Belmont Stakes - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 5, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Belmont Stakes - To Win  - Jun 08					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Belmont Stakes - Odds To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Joevia</td><td>31/1</td><td>+3100</td></tr><tr><td>Everfast</td><td>11/1</td><td>+1100</td></tr><tr><td>Master Fencer</td><td>9/1</td><td>+900</td></tr><tr><td>Tax</td><td>16/1</td><td>+1600</td></tr><tr><td>Bourbon War</td><td>14/1</td><td>+1400</td></tr><tr><td>Spinoff</td><td>15/1</td><td>+1500</td></tr><tr><td>Sir Winston</td><td>16/1</td><td>+1600</td></tr><tr><td>Intrepid Heart</td><td>11/1</td><td>+1100</td></tr><tr><td>War Of Will</td><td>9/4</td><td>+225</td></tr><tr><td>Tacitus</td><td>3/2</td><td>+150</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	