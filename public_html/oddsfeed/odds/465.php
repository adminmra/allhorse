	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Denmark - Superliga">
			<caption>Denmark - Superliga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 3, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Denmark - Superliga  - Dec 03</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Vejle Boldklub<br /> - vs - <br /> Aarhus Gf</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-105)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+309</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+115 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-113</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-135</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Denmark - Superliga  - Dec 07</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Odense Bk<br /> - vs - <br /> Fc Midtjylland</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (110)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+318</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-145 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (-130)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-130</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+125</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Denmark - Superliga  - Dec 08</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Sonderjyske<br /> - vs - <br /> Hobro Ik</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (125)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+125</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-110 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-145)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+201</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-110</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Denmark - Superliga  - Dec 09</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Esbjerg Fb<br /> - vs - <br /> Fc Copenhagen</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1.5<br /> (-110)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+800</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o3+105 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1.5<br /> (-110)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-337</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u3-125</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Horsens<br /> - vs - <br /> Aalborg Bk</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+382</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-110 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-150</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-110</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Randers Fc<br /> - vs - <br /> Vendsyssel Ff</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+111</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+115 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+234</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-135</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Brondby If<br /> - vs - <br /> Vejle Boldklub</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-176</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-145 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+417</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+125</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Denmark - Superliga  - Dec 10</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Aarhus Gf<br /> - vs - <br /> Fc Nordsjaelland</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+280</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-135 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-118</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+115</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	