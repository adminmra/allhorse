	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--mexico Copa Mx">
			<caption>Live--mexico Copa Mx</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 7, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--mexico Copa Mx  - Feb 07</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Lobos B.u.a.p.</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -250 )</div>
								 <div class="boxdata" >380</div>
								 <div class="boxdata" >+&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Mineros De Zacatecas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +165 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Cafetaleros De Tapachula</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -142 )</div>
								 <div class="boxdata" >1525</div>
								 <div class="boxdata" >+1<br /> ( -142)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cimarrones De Sonora Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-400</div>
								 <div class="boxdata" >-1<br /> ( -111)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	