	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Iran Pro League">
			<caption>Iran Pro League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated November 30, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Iran Pro League  - Nov 30</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Esteghlal Khuzestan<br /> - vs - <br /> Saipa</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+493</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2-120 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-163</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2EV</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Padideh Khorasan<br /> - vs - <br /> Sepahan</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+309</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2+105 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (100)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+101</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2-125</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Paykan<br /> - vs - <br /> Naft Masjed Soleyman</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+176</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2+115 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (100)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+196</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2-135</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	