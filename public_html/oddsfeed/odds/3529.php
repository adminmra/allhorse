	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Churchill Down Stakes - To Win">
			<caption>Horses - Churchill Down Stakes - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Churchill Down Stakes - To Win  - May 04					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Churchill Downs Stakes - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Do Share</td><td>10/1</td><td>+1000</td></tr><tr><td>Limousine Liberal</td><td>10/1</td><td>+1000</td></tr><tr><td>Majestic Dunhill</td><td>20/1</td><td>+2000</td></tr><tr><td>Bobby's Wicked One</td><td>9/1</td><td>+900</td></tr><tr><td>Wild Shot</td><td>40/1</td><td>+4000</td></tr><tr><td>Promises Fulfilled</td><td>7/2</td><td>+350</td></tr><tr><td>Warrior's Club</td><td>14/1</td><td>+1400</td></tr><tr><td>Mitole</td><td>11/4</td><td>+275</td></tr><tr><td>Uncontested</td><td>12/1</td><td>+1200</td></tr><tr><td>Still Having Fun</td><td>50/1</td><td>+5000</td></tr><tr><td>Uno Mas Modelo</td><td>33/1</td><td>+3300</td></tr><tr><td>Whitmore</td><td>7/2</td><td>+350</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	