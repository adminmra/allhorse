	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--switzerland Challenge League">
			<caption>Pre--switzerland Challenge League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 23, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--switzerland Challenge League  - May 23</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Wil 1900</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >467</div>
								 <div class="boxdata" >+1<br /> ( -119)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Schaffhausen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >-188</div>
								 <div class="boxdata" >-1<br /> ( -111)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Lausanne-sport</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -101)</div>
								 <div class="boxdata" >-188</div>
								 <div class="boxdata" >-1<br /> ( -116)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Kriens</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -131)</div>
								 <div class="boxdata" >428</div>
								 <div class="boxdata" >+1<br /> ( -113)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Winterthur</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -104)</div>
								 <div class="boxdata" >279</div>
								 <div class="boxdata" >+&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Rapperswil-jona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -128)</div>
								 <div class="boxdata" >-121</div>
								 <div class="boxdata" >-&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Servette Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" >-&frac34;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Vaduz</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >315</div>
								 <div class="boxdata" >+&frac34;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Aarau</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >-169</div>
								 <div class="boxdata" >-1<br /> ( -107)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Chiasso</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >353</div>
								 <div class="boxdata" >+1<br /> ( -123)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	