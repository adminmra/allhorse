	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Wnba - 1h">
			<caption>Wnba - 1h</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 17, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Wnba - 1h First Half Lines - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Los Angeles Sparks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o80&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >+&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Connecticut Sun</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u80&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-122</div>
								 <div class="boxdata" >-&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Las Vegas Aces</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o88<br /> ( -110)</div>
								 <div class="boxdata" >260</div>
								 <div class="boxdata" >+6<br /> ( -110)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Washington Mystics</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u88<br /> ( -110)</div>
								 <div class="boxdata" >-320</div>
								 <div class="boxdata" >-6<br /> ( -110)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	