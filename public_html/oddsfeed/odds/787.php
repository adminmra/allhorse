	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Superbowl 53 - Gatorade Shower">
			<caption>Superbowl 53 - Gatorade Shower</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Superbowl 53 - Gatorade Shower  - Feb 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Color Of The Liquid Poured On The Winning Coach</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Lime/green/yellow</td><td>9/4</td><td>+225</td></tr><tr><td>Clear/water</td><td>EV</td><td>EV</td></tr><tr><td>Orange</td><td>4/1</td><td>+400</td></tr><tr><td>Blue</td><td>4/1</td><td>+400</td></tr><tr><td>Red</td><td>6/1</td><td>+600</td></tr><tr><td>Purple</td><td>10/1</td><td>+1000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	