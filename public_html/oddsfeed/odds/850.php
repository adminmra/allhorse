	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Cfb - Contest Props">
			<caption>Cfb - Contest Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated January 7, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center">
					Cfb - Contest Props  - Jan 07					</th>
			</tr>
            
	<tr><th colspan="3" class="center">Clemson Vs Alabama - Double Result</th></tr><tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Clem 1h/clem Ft</td><td>3/1</td><td>+300</td></tr><tr><td>Clem 1h/ala Ft</td><td>11/2</td><td>+550</td></tr><tr><td>Ala 1h/clem Ft</td><td>67/10</td><td>+670</td></tr><tr><td>Ala 1h/ala Ft</td><td>10/13</td><td>-130</td></tr><tr><td>Tie 1h/clem Ft</td><td>18/1</td><td>+1800</td></tr><tr><td>Tie 1h/ala Ft</td><td>16/1</td><td>+1600</td></tr><tr><th colspan="3" class="center">Clemson Vs Alabama - Winning Margin</th></tr><tr><td>Clem By 1-3 Pts</td><td>10/1</td><td>+1000</td></tr><tr><td>Clem By 4-6 Pts</td><td>14/1</td><td>+1400</td></tr><tr><td>Clem By 7-10 Pts</td><td>11/1</td><td>+1100</td></tr><tr><td>Clem By 11-13 Pts</td><td>25/1</td><td>+2500</td></tr><tr><td>Clem By 14-17 Pts</td><td>17/1</td><td>+1700</td></tr><tr><td>Clem By 18-21 Pts</td><td>24/1</td><td>+2400</td></tr><tr><td>Clem By 22 Or More Pts</td><td>11/1</td><td>+1100</td></tr><tr><td>Ala By 1-3 Pts</td><td>6/1</td><td>+600</td></tr><tr><td>Ala By 4-6 Pts</td><td>15/2</td><td>+750</td></tr><tr><td>Ala By 7-10 Pts</td><td>6/1</td><td>+600</td></tr><tr><td>Ala By 11-13 Pts</td><td>13/1</td><td>+1300</td></tr><tr><td>Ala By 14-17 Pts</td><td>17/2</td><td>+850</td></tr><tr><td>Ala By 18-21 Pts</td><td>11/1</td><td>+1100</td></tr><tr><td>Ala By 22 Or More Pts</td><td>4/1</td><td>+400</td></tr><tr><th colspan="3" class="center">Clemson Vs Alabama - First Scoring Play</th></tr><tr><td>Clem Touchdown</td><td>8/5</td><td>+160</td></tr><tr><td>Clem Field Goal</td><td>13/2</td><td>+650</td></tr><tr><td>Clem Any Other Score</td><td>50/1</td><td>+5000</td></tr><tr><td>Ala Touchdown</td><td>11/10</td><td>+110</td></tr><tr><td>Ala Field Goal</td><td>6/1</td><td>+600</td></tr><tr><td>Ala Any Other Score</td><td>50/1</td><td>+5000</td></tr><tr><th colspan="3" class="center">Clemson Vs Alabama -total Pts Scored By Both Teams</th></tr><tr><td>0-14 Pts</td><td>240/1</td><td>+24000</td></tr><tr><td>15-21 Pts</td><td>65/1</td><td>+6500</td></tr><tr><td>22-28 Pts</td><td>24/1</td><td>+2400</td></tr><tr><td>29-35 Pts</td><td>11/1</td><td>+1100</td></tr><tr><td>36-42 Pts</td><td>13/2</td><td>+650</td></tr><tr><td>43-49 Pts</td><td>9/2</td><td>+450</td></tr><tr><td>50-56 Pts</td><td>37/10</td><td>+370</td></tr><tr><td>57-63 Pts</td><td>37/10</td><td>+370</td></tr><tr><td>64-70 Pts</td><td>9/2</td><td>+450</td></tr><tr><td>71-77 Pts</td><td>6/1</td><td>+600</td></tr><tr><td>78-84 Points</td><td>17/2</td><td>+850</td></tr><tr><td>85 Or More Points</td><td>6/1</td><td>+600</td></tr><tr><th colspan="3" class="center">Clem Vs Ala - 1st Half Winning Margin</th></tr><tr><td>Clem Win 1h By 1- 3 Points</td><td>14/1</td><td>+1400</td></tr><tr><td>Clem Win 1h By 4-6 Points</td><td>16/1</td><td>+1600</td></tr><tr><td>Clem Win 1h By 7-9 Points</td><td>9/1</td><td>+900</td></tr><tr><td>Clem Win 1h By 10-12 Points</td><td>14/1</td><td>+1400</td></tr><tr><td>Clem Win 1h By 13 Or More</td><td>6/1</td><td>+600</td></tr><tr><td>Ala Win 1h By 1- 3 Points</td><td>12/1</td><td>+1200</td></tr><tr><td>Ala Win 1h By 4-6 Points</td><td>12/1</td><td>+1200</td></tr><tr><td>Ala Win 1h By 7-9 Points</td><td>13/2</td><td>+650</td></tr><tr><td>Ala Win 1h By 10-12 Points</td><td>10/1</td><td>+1000</td></tr><tr><td>Ala Win 1h By 13 Or More</td><td>23/10</td><td>+230</td></tr><tr><td>Tie</td><td>7/1</td><td>+700</td></tr>			</tbody>
		</table>
	</div>

	