	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Breeders Cup Distaff - To Win">
			<caption>Horses - Breeders Cup Distaff - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Breeders Cup Distaff - To Win  - Nov 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Breeders Cup Distaff - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Bellafina</td><td>18/1</td><td>+1800</td></tr><tr><td>Midnight Bisou</td><td>3/1</td><td>+300</td></tr><tr><td>Elate</td><td>5/1</td><td>+500</td></tr><tr><td>Wow Cat</td><td>25/1</td><td>+2500</td></tr><tr><td>Restless Rider</td><td>25/1</td><td>+2500</td></tr><tr><td>Jaywalk</td><td>25/1</td><td>+2500</td></tr><tr><td>Escape Clause</td><td>33/1</td><td>+3300</td></tr><tr><td>Blue Prize</td><td>25/1</td><td>+2500</td></tr><tr><td>Guarana</td><td>3/1</td><td>+300</td></tr><tr><td>Dunbar Road</td><td>6/1</td><td>+600</td></tr><tr><td>Serengeti Empress</td><td>16/1</td><td>+1600</td></tr><tr><td>Ollie S Candy</td><td>16/1</td><td>+1600</td></tr><tr><td>Vexatious</td><td>100/1</td><td>+10000</td></tr><tr><td>Covfefe</td><td>16/1</td><td>+1600</td></tr><tr><td>La Force</td><td>50/1</td><td>+5000</td></tr><tr><td>Paradise Woods</td><td>50/1</td><td>+5000</td></tr><tr><td>Secret Spice</td><td>20/1</td><td>+2000</td></tr><tr><td>She's A Julie</td><td>40/1</td><td>+4000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	