	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--lebanon Premier League">
			<caption>Pre--lebanon Premier League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 14, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Pre--lebanon Premier League  - Dec 14</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Tripoli Ac<br /> - vs - <br /> Salam Zgharta</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+175</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"> </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+164</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Pre--lebanon Premier League  - Dec 15</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Al Tadamon Sour<br /> - vs - <br /> Al Ahed</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+1172</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"> </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-454</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Safa Sc<br /> - vs - <br /> Racing Beirut</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+121</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"> </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+202</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Pre--lebanon Premier League  - Dec 16</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Shabab Al Sahel<br /> - vs - <br /> Al Ansar</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1<br /> (-114)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+461</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac34;-111 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1<br /> (-109)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-178</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac34;-125</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	