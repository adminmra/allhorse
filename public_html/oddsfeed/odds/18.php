	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Canadian Football">
			<caption>Canadian Football</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Canadian Football  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Calgary Stampeders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o53&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-250</div>
								 <div class="boxdata" >-6&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Toronto Argonauts</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u53&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >210</div>
								 <div class="boxdata" >+6&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Canadian Football  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Winnipeg Blue Bombers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o47<br /> ( -110)</div>
								 <div class="boxdata" >-138</div>
								 <div class="boxdata" >-2&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Montreal Alouettes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u47<br /> ( -110)</div>
								 <div class="boxdata" >118</div>
								 <div class="boxdata" >+2&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B.c. Lions</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o47&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-155</div>
								 <div class="boxdata" >-3<br /> ( -110)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ottawa Redblacks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u47&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" >+3<br /> ( -110)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	