	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nfl - Regular Season Mvp">
			<caption>Nfl - Regular Season Mvp</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nfl - Regular Season Mvp  - Sep 08					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Regular Season Mvp 2019/20 - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Patrick Mahomes</td><td>4/1</td><td>+400</td></tr><tr><td>Drew Brees</td><td>7/1</td><td>+700</td></tr><tr><td>Aaron Rodgers</td><td>8/1</td><td>+800</td></tr><tr><td>Andrew Luck</td><td>8/1</td><td>+800</td></tr><tr><td>Tom Brady</td><td>10/1</td><td>+1000</td></tr><tr><td>Baker Mayfield</td><td>14/1</td><td>+1400</td></tr><tr><td>Russell Wilson</td><td>16/1</td><td>+1600</td></tr><tr><td>Carson Wentz</td><td>19/1</td><td>+1900</td></tr><tr><td>Philip Rivers</td><td>22/1</td><td>+2200</td></tr><tr><td>Deshaun Watson</td><td>25/1</td><td>+2500</td></tr><tr><td>Matt Ryan</td><td>28/1</td><td>+2800</td></tr><tr><td>Jared Goff</td><td>33/1</td><td>+3300</td></tr><tr><td>Alvin Kamara</td><td>33/1</td><td>+3300</td></tr><tr><td>Todd Gurley</td><td>40/1</td><td>+4000</td></tr><tr><td>Ben Roethlisberger</td><td>40/1</td><td>+4000</td></tr><tr><td>Cam Newton</td><td>40/1</td><td>+4000</td></tr><tr><td>Aaron Donald</td><td>40/1</td><td>+4000</td></tr><tr><td>Jimmy Garoppolo</td><td>40/1</td><td>+4000</td></tr><tr><td>Saquon Barkley</td><td>40/1</td><td>+4000</td></tr><tr><td>Leveon Bell</td><td>50/1</td><td>+5000</td></tr><tr><td>Christian Mccaffrey</td><td>50/1</td><td>+5000</td></tr><tr><td>Ezekiel Elliott</td><td>50/1</td><td>+5000</td></tr><tr><td>Kirk Cousins</td><td>50/1</td><td>+5000</td></tr><tr><td>David Johnson</td><td>66/1</td><td>+6600</td></tr><tr><td>Mitch Trubisky</td><td>66/1</td><td>+6600</td></tr><tr><td>Dak Prescott</td><td>66/1</td><td>+6600</td></tr><tr><td>Odell Beckham</td><td>80/1</td><td>+8000</td></tr><tr><td>Lamar Jackson</td><td>80/1</td><td>+8000</td></tr><tr><td>Julio Jones</td><td>80/1</td><td>+8000</td></tr><tr><td>Nick Foles</td><td>80/1</td><td>+8000</td></tr><tr><td>Adam Thielen</td><td>80/1</td><td>+8000</td></tr><tr><td>Josh Allen</td><td>80/1</td><td>+8000</td></tr><tr><td>Melvin Gordon</td><td>80/1</td><td>+8000</td></tr><tr><td>Jameis Winston</td><td>100/1</td><td>+10000</td></tr><tr><td>Kyler Murray</td><td>100/1</td><td>+10000</td></tr><tr><td>Juju Smith-schuster</td><td>100/1</td><td>+10000</td></tr><tr><td>Antonio Brown</td><td>100/1</td><td>+10000</td></tr><tr><td>Matthew Stafford</td><td>100/1</td><td>+10000</td></tr><tr><td>Sam Darnold</td><td>100/1</td><td>+10000</td></tr><tr><td>Nick Chubb</td><td>125/1</td><td>+12500</td></tr><tr><td>Marcus Mariota</td><td>125/1</td><td>+12500</td></tr><tr><td>Deandre Hopkins</td><td>125/1</td><td>+12500</td></tr><tr><td>Devonta Freeman</td><td>125/1</td><td>+12500</td></tr><tr><td>Mike Evans</td><td>125/1</td><td>+12500</td></tr><tr><td>Aj Green</td><td>125/1</td><td>+12500</td></tr><tr><td>Amari Cooper</td><td>125/1</td><td>+12500</td></tr><tr><td>Dalvin Cook</td><td>125/1</td><td>+12500</td></tr><tr><td>James Conner</td><td>125/1</td><td>+12500</td></tr><tr><td>Derek Carr</td><td>125/1</td><td>+12500</td></tr><tr><td>Eli Manning</td><td>125/1</td><td>+12500</td></tr><tr><td>Andy Dalton</td><td>150/1</td><td>+15000</td></tr><tr><td>Dwayne Haskins</td><td>150/1</td><td>+15000</td></tr><tr><td>Leonard Fournette</td><td>150/1</td><td>+15000</td></tr><tr><td>Ty Hilton</td><td>150/1</td><td>+15000</td></tr><tr><td>Brandin Cooks</td><td>150/1</td><td>+15000</td></tr><tr><td>Cooper Kupp</td><td>150/1</td><td>+15000</td></tr><tr><td>Davante Adams</td><td>150/1</td><td>+15000</td></tr><tr><td>Joe Mixon</td><td>150/1</td><td>+15000</td></tr><tr><td>Emmanuel Sanders</td><td>150/1</td><td>+15000</td></tr><tr><td>Damien Williams</td><td>150/1</td><td>+15000</td></tr><tr><td>Josh Rosen</td><td>150/1</td><td>+15000</td></tr><tr><td>Joe Flacco</td><td>150/1</td><td>+15000</td></tr><tr><td>Michael Thomas</td><td>80/1</td><td>+8000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	