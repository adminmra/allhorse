	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--slovenia Slovenian Cup">
			<caption>Pre--slovenia Slovenian Cup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--slovenia Slovenian Cup  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ns Mura</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -114)</div>
								 <div class="boxdata" >-138</div>
								 <div class="boxdata" >-&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Triglav Kranj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -114)</div>
								 <div class="boxdata" >313</div>
								 <div class="boxdata" >+&frac12;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Maribor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -126 )</div>
								 <div class="boxdata" >-243</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Koper</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >570</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -147 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Domzale</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >-416</div>
								 <div class="boxdata" >-2<br /> ( +104)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Brda</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >809</div>
								 <div class="boxdata" >+2<br /> ( -138)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Tabor Sezana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >370</div>
								 <div class="boxdata" >+&frac12;<br /> ( +108 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Aluminij Kidricevo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >-142</div>
								 <div class="boxdata" >-&frac12;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--slovenia Slovenian Cup  - Sep 19</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Rudar Velenje</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >667</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Olimpija Ljubljana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >-285</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	