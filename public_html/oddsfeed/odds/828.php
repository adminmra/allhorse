	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba - Divisions - Odds To Win">
			<caption>Nba - Divisions - Odds To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nba - Divisions - Odds To Win  - Oct 22					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Atlantic Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Philadelphia 76ers</td><td>4/7</td><td>-175</td></tr><tr><td>Boston Celtics</td><td>9/2</td><td>+450</td></tr><tr><td>Brooklyn Nets</td><td>5/1</td><td>+500</td></tr><tr><td>Toronto Raptors</td><td>6/1</td><td>+600</td></tr><tr><td>New York Knicks</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Central Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Milwaukee Bucks</td><td>1/12</td><td>-1200</td></tr><tr><td>Indiana Pacers</td><td>7/1</td><td>+700</td></tr><tr><td>Detroit Pistons</td><td>33/1</td><td>+3300</td></tr><tr><td>Chicago Bulls</td><td>60/1</td><td>+6000</td></tr><tr><td>Cleveland Cavaliers</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Northwest Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Utah Jazz</td><td>11/10</td><td>+110</td></tr><tr><td>Denver Nuggets</td><td>3/2</td><td>+150</td></tr><tr><td>Portland Trail Blazers</td><td>7/2</td><td>+350</td></tr><tr><td>Minnesota Timberwolves</td><td>80/1</td><td>+8000</td></tr><tr><td>Oklahoma City Thunder</td><td>150/1</td><td>+15000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Pacific Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Los Angeles Clippers</td><td>5/8</td><td>-160</td></tr><tr><td>Los Angeles Lakers</td><td>9/4</td><td>+225</td></tr><tr><td>Golden State Warriors</td><td>11/2</td><td>+550</td></tr><tr><td>Sacramento Kings</td><td>33/1</td><td>+3300</td></tr><tr><td>Phoenix Suns</td><td>150/1</td><td>+15000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Southeast Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Miami Heat</td><td>10/13</td><td>-130</td></tr><tr><td>Orlando Magic</td><td>3/2</td><td>+150</td></tr><tr><td>Atlanta Hawks</td><td>15/2</td><td>+750</td></tr><tr><td>Washington Wizards</td><td>80/1</td><td>+8000</td></tr><tr><td>Charlotte Hornets</td><td>150/1</td><td>+15000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Southwest Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Houston Rockets</td><td>5/13</td><td>-260</td></tr><tr><td>San Antonio Spurs</td><td>4/1</td><td>+400</td></tr><tr><td>Dallas Mavericks</td><td>17/2</td><td>+850</td></tr><tr><td>New Orleans Pelicans</td><td>17/2</td><td>+850</td></tr><tr><td>Memphis Grizzlies</td><td>150/1</td><td>+15000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');</script>
{/literal}	
	