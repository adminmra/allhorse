	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Afc Champions League - To Win">
			<caption>Soccer - Afc Champions League - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Afc Champions League - To Win  - Sep 16					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Afc Champions League - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Guangzhou Evergrande</td><td>4/1</td><td>+400</td></tr><tr><td>Urawa Red Diamonds</td><td>7/1</td><td>+700</td></tr><tr><td>Shanghai Sipg</td><td>5/1</td><td>+500</td></tr><tr><td>Al-nassr Riyadh</td><td>6/1</td><td>+600</td></tr><tr><td>Al-sadd Sc</td><td>6/1</td><td>+600</td></tr><tr><td>Kashima Antlers</td><td>9/1</td><td>+900</td></tr><tr><td>Al Hilal Riyadh</td><td>9/2</td><td>+450</td></tr><tr><td>Al Ittihad Jeddah</td><td>14/1</td><td>+1400</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	