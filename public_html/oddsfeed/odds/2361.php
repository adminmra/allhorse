	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--serbia Prva Liga">
			<caption>Pre--serbia Prva Liga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 14, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Pre--serbia Prva Liga  - Dec 15</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Fk Bezanija<br /> - vs - <br /> Fk Buducnost Dobanovci</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1.5<br /> (-114)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+903</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-123 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1.5<br /> (-111)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-344</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-112</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Fk Indija<br /> - vs - <br /> Fk Becej 1918</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.25<br /> (-117)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+118</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac14;-102 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.25<br /> (-108)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+242</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac14;-135</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Fk Novi Pazar<br /> - vs - <br /> Fk Sindelic Beograd</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">2<br /> (-120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+1594</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac34;-120 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-2<br /> (-106)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-625</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac34;-113</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Fk Radnicki 1923<br /> - vs - <br /> Fk Zlatibor</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.75<br /> (-106)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+429</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac14;-106 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.75<br /> (-119)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-151</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac14;-129</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	