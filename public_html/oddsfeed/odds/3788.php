	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--brazil Copa Paulista">
			<caption>Pre--brazil Copa Paulista</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--brazil Copa Paulista  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ec Taubate Sp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -117)</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" >-&frac12;<br /> ( +123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Atibaia Sp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -112)</div>
								 <div class="boxdata" >229</div>
								 <div class="boxdata" >+&frac12;<br /> ( -163 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rio Claro Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -131)</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Desportivo Brasil Sp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -101)</div>
								 <div class="boxdata" >191</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ferroviaria</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -119)</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" >-&frac12;<br /> ( +123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ec Sao Bernardo Sp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -111)</div>
								 <div class="boxdata" >231</div>
								 <div class="boxdata" >+&frac12;<br /> ( -163 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Comercial Fc Sp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -129 )</div>
								 <div class="boxdata" >143</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ec Santo Andre Sp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -102 )</div>
								 <div class="boxdata" >221</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ec Xv De Novembro Sp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >-163</div>
								 <div class="boxdata" >-1<br /> ( +108)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nacional Ac Sp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >440</div>
								 <div class="boxdata" >+1<br /> ( -144)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	