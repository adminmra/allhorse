	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pinball">
			<caption>Pinball</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Pinball  - Oct 11					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">European Pinball Championship - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Daniele Acciari</td><td>4/1</td><td>+400</td></tr><tr><td>Johannes Ostermeier</td><td>4/1</td><td>+400</td></tr><tr><td>Jorian Engelbrektsson</td><td>6/1</td><td>+600</td></tr><tr><td>Peter Andersen</td><td>6/1</td><td>+600</td></tr><tr><td>Julio Vicario Soriano</td><td>7/1</td><td>+700</td></tr><tr><td>Colin Mcalpine</td><td>8/1</td><td>+800</td></tr><tr><td>Franck Bona</td><td>9/1</td><td>+900</td></tr><tr><td>Jorgen Holm</td><td>10/1</td><td>+1000</td></tr><tr><td>Roberto Pedroni</td><td>12/1</td><td>+1200</td></tr><tr><td>Mats Runsten</td><td>13/1</td><td>+1300</td></tr><tr><td>Albert Nomden</td><td>15/1</td><td>+1500</td></tr><tr><td>Johan Genberg</td><td>15/1</td><td>+1500</td></tr><tr><td>Jonas Valstrom</td><td>20/1</td><td>+2000</td></tr><tr><td>Levi Nayman</td><td>20/1</td><td>+2000</td></tr><tr><td>Mads Kristiansen</td><td>20/1</td><td>+2000</td></tr><tr><td>Paul Jongma</td><td>20/1</td><td>+2000</td></tr><tr><td>Marcus Hugosson</td><td>25/1</td><td>+2500</td></tr><tr><td>Olli-mikko Ojamies</td><td>25/1</td><td>+2500</td></tr><tr><td>Sebastian Bobbio</td><td>40/1</td><td>+4000</td></tr><tr><td>Rich Mallet</td><td>40/1</td><td>+4000</td></tr><tr><td>Greg Poverelli</td><td>50/1</td><td>+5000</td></tr><tr><td>Ben Grabeldinger</td><td>60/1</td><td>+6000</td></tr><tr><td>Jeff Teolis</td><td>60/1</td><td>+6000</td></tr><tr><td>David Dahl-hansson</td><td>60/1</td><td>+6000</td></tr><tr><td>Field </td><td>5/1</td><td>+500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	