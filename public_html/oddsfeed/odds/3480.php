	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Racing - Rally - World Championship - To Win">
			<caption>Racing - Rally - World Championship - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Racing - Rally - World Championship - To Win  - Aug 01					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">World Rally Championship 2019</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Ott Tanak</td><td>2/7</td><td>-350</td></tr><tr><td>Sebastien Ogier</td><td>7/2</td><td>+350</td></tr><tr><td>Thierry Neuville</td><td>7/2</td><td>+350</td></tr><tr><td>Jari-matti Latvala</td><td>150/1</td><td>+15000</td></tr><tr><td>Esapekka Lappi</td><td>150/1</td><td>+15000</td></tr><tr><td>Kris Meeke</td><td>80/1</td><td>+8000</td></tr><tr><td>Andreas Mikkelsen</td><td>400/1</td><td>+40000</td></tr><tr><td>Sebastien Loeb</td><td>300/1</td><td>+30000</td></tr><tr><td>Elfyn Evans</td><td>66/1</td><td>+6600</td></tr><tr><td>Dani Sordo</td><td>200/1</td><td>+20000</td></tr><tr><td>Teemu Suninen</td><td>200/1</td><td>+20000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	