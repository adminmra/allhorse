	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Commonwealth Cup">
			<caption>Horses - Commonwealth Cup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Commonwealth Cup  - Jun 19					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Commonwealth Cup - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Siskin</td><td>12/1</td><td>+1200</td></tr><tr><td>A`ali</td><td>12/1</td><td>+1200</td></tr><tr><td>Raffle Prize</td><td>12/1</td><td>+1200</td></tr><tr><td>Sunday Sovereign</td><td>16/1</td><td>+1600</td></tr><tr><td>Threat</td><td>10/1</td><td>+1000</td></tr><tr><td>Arizona</td><td>20/1</td><td>+2000</td></tr><tr><td>Daahyeh</td><td>20/1</td><td>+2000</td></tr><tr><td>Kimari</td><td>20/1</td><td>+2000</td></tr><tr><td>Monarch Of Egypt</td><td>16/1</td><td>+1600</td></tr><tr><td>Royal Lytham</td><td>16/1</td><td>+1600</td></tr><tr><td>Ventura Rebel</td><td>20/1</td><td>+2000</td></tr><tr><td>Pinatubo</td><td>12/1</td><td>+1200</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	