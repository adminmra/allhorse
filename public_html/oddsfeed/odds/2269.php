	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--lithuania A Lyga">
			<caption>Pre--lithuania A Lyga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--lithuania A Lyga  - Jul 04</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Panevezys</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >-263</div>
								 <div class="boxdata" >-1&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Palanga</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >524</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--lithuania A Lyga  - Jul 05</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vilniaus Riteriai</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -129 )</div>
								 <div class="boxdata" >487</div>
								 <div class="boxdata" >+1<br /> ( -102)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Zalgiris Vilnius</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -102 )</div>
								 <div class="boxdata" >-204</div>
								 <div class="boxdata" >-1<br /> ( -129)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	