	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Connor Mcgregor Specials">
			<caption>Connor Mcgregor Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated April 2, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Connor Mcgregor Specials  - Apr 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">What Will Happen Next For Conor Mcgregor</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>He Fights In 2019</td><td>9/5</td><td>+180</td></tr><tr><td>Fights In 2020 </td><td>5/2</td><td>+250</td></tr><tr><td>Arrested Before He Fights Again  </td><td>3/1</td><td>+300</td></tr><tr><td>Rematch With Mayweather In Boxing  </td><td>4/1</td><td>+400</td></tr><tr><td>Trilogy Fight With Nate Diaz In 2021 </td><td>9/1</td><td>+900</td></tr><tr><td>Never Fights For Ufc Again </td><td>EV</td><td>EV</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	