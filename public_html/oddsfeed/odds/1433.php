	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - Wgc 1st Round Leader">
			<caption>Golf - Wgc 1st Round Leader</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 21, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - Wgc 1st Round Leader  - Feb 21					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Wgc Mexico Championship - First Round Leader</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Dustin Johnson</td><td>18/1</td><td>+1800</td></tr><tr><td>Justin Thomas</td><td>17/1</td><td>+1700</td></tr><tr><td>Rory Mcilroy</td><td>20/1</td><td>+2000</td></tr><tr><td>Jon Rahm</td><td>22/1</td><td>+2200</td></tr><tr><td>Rickie Fowler</td><td>22/1</td><td>+2200</td></tr><tr><td>Tiger Woods</td><td>25/1</td><td>+2500</td></tr><tr><td>Bryson Dechambeau</td><td>25/1</td><td>+2500</td></tr><tr><td>Xander Schauffele</td><td>25/1</td><td>+2500</td></tr><tr><td>Hideki Matsuyama</td><td>28/1</td><td>+2800</td></tr><tr><td>Brooks Koepka</td><td>28/1</td><td>+2800</td></tr><tr><td>Phil Mickelson</td><td>28/1</td><td>+2800</td></tr><tr><td>Tony Finau</td><td>30/1</td><td>+3000</td></tr><tr><td>Paul Casey</td><td>30/1</td><td>+3000</td></tr><tr><td>Jordan Spieth</td><td>30/1</td><td>+3000</td></tr><tr><td>Patrick Cantlay</td><td>30/1</td><td>+3000</td></tr><tr><td>Marc Leishman</td><td>30/1</td><td>+3000</td></tr><tr><td>Tommy Fleetwood</td><td>33/1</td><td>+3300</td></tr><tr><td>Webb Simpson</td><td>33/1</td><td>+3300</td></tr><tr><td>Bubba Watson</td><td>33/1</td><td>+3300</td></tr><tr><td>Gary Woodland</td><td>33/1</td><td>+3300</td></tr><tr><td>Sergio Garcia</td><td>40/1</td><td>+4000</td></tr><tr><td>Patrick Reed</td><td>40/1</td><td>+4000</td></tr><tr><td>Rafael Cabrera Bello</td><td>40/1</td><td>+4000</td></tr><tr><td>Charles Howell</td><td>40/1</td><td>+4000</td></tr><tr><td>Francesco Molinari</td><td>40/1</td><td>+4000</td></tr><tr><td>Ian Poulter</td><td>40/1</td><td>+4000</td></tr><tr><td>Matt Kuchar</td><td>45/1</td><td>+4500</td></tr><tr><td>Hao-tong Li</td><td>45/1</td><td>+4500</td></tr><tr><td>Louis Oosthuizen</td><td>50/1</td><td>+5000</td></tr><tr><td>Cameron Smith</td><td>50/1</td><td>+5000</td></tr><tr><td>Henrik Stenson</td><td>55/1</td><td>+5500</td></tr><tr><td>Tyrrell Hatton</td><td>55/1</td><td>+5500</td></tr><tr><td>Matt Wallace</td><td>55/1</td><td>+5500</td></tr><tr><td>Matthew Fitzpatrick</td><td>55/1</td><td>+5500</td></tr><tr><td>Byeong Hun An</td><td>55/1</td><td>+5500</td></tr><tr><td>Billy Horschel</td><td>60/1</td><td>+6000</td></tr><tr><td>Shane Lowry</td><td>60/1</td><td>+6000</td></tr><tr><td>Lee Westwood</td><td>60/1</td><td>+6000</td></tr><tr><td>Joost Luiten</td><td>66/1</td><td>+6600</td></tr><tr><td>Russell Knox</td><td>70/1</td><td>+7000</td></tr><tr><td>Brandt Snedeker</td><td>75/1</td><td>+7500</td></tr><tr><td>Thorbjorn Olesen</td><td>75/1</td><td>+7500</td></tr><tr><td>Emiliano Grillo</td><td>75/1</td><td>+7500</td></tr><tr><td>Alex Noren</td><td>80/1</td><td>+8000</td></tr><tr><td>Keegan Bradley</td><td>80/1</td><td>+8000</td></tr><tr><td>Branden Grace</td><td>80/1</td><td>+8000</td></tr><tr><td>Chez Reavie</td><td>80/1</td><td>+8000</td></tr><tr><td>Tom Lewis</td><td>90/1</td><td>+9000</td></tr><tr><td>Kevin Kisner</td><td>90/1</td><td>+9000</td></tr><tr><td>Lucas Bjerregaard</td><td>100/1</td><td>+10000</td></tr><tr><td>Danny Willett</td><td>100/1</td><td>+10000</td></tr><tr><td>Shubhankar Sharma</td><td>100/1</td><td>+10000</td></tr><tr><td>Aaron Rai</td><td>100/1</td><td>+10000</td></tr><tr><td>Patton Kizzire</td><td>110/1</td><td>+11000</td></tr><tr><td>Aaron Wise</td><td>110/1</td><td>+11000</td></tr><tr><td>Eddie Pepperell</td><td>110/1</td><td>+11000</td></tr><tr><td>Kiradech Aphibarnrat</td><td>110/1</td><td>+11000</td></tr><tr><td>Abraham Ancer</td><td>110/1</td><td>+11000</td></tr><tr><td>Kevin Na</td><td>110/1</td><td>+11000</td></tr><tr><td>Alexander Bjork</td><td>110/1</td><td>+11000</td></tr><tr><td>Richard Sterne</td><td>110/1</td><td>+11000</td></tr><tr><td>Kyle Stanley</td><td>140/1</td><td>+14000</td></tr><tr><td>Adrian Otaegui</td><td>140/1</td><td>+14000</td></tr><tr><td>Erik Van Rooyen</td><td>150/1</td><td>+15000</td></tr><tr><td>David Lipsky</td><td>150/1</td><td>+15000</td></tr><tr><td>George Coetzee</td><td>160/1</td><td>+16000</td></tr><tr><td>Shugo Imahira</td><td>160/1</td><td>+16000</td></tr><tr><td>Satoshi Kodaira</td><td>160/1</td><td>+16000</td></tr><tr><td>Shaun Norris</td><td>175/1</td><td>+17500</td></tr><tr><td>Matthew Millar</td><td>175/1</td><td>+17500</td></tr><tr><td>Sanghyun Park</td><td>175/1</td><td>+17500</td></tr><tr><td>Jake Mcleod</td><td>175/1</td><td>+17500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	