	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--denmark 2nd Division">
			<caption>Pre--denmark 2nd Division</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--denmark 2nd Division  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B 93 Copenhagen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >891</div>
								 <div class="boxdata" >+2<br /> ( -140)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Helsingoer</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >-416</div>
								 <div class="boxdata" >-2<br /> ( +104)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--denmark 2nd Division  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fa 2000</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -125 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Skovshoved If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -105 )</div>
								 <div class="boxdata" >146</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hik Hellerup</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >138</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hillerod Fodbold</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >161</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vanloese If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >199</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bk Frem</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brabrand If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -117 )</div>
								 <div class="boxdata" >217</div>
								 <div class="boxdata" >+&frac12;<br /> ( -144 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Middelfart Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac12;<br /> ( +108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aarhus Fremad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -120)</div>
								 <div class="boxdata" >-188</div>
								 <div class="boxdata" >-1<br /> ( -120)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vejgaard Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -109)</div>
								 <div class="boxdata" >453</div>
								 <div class="boxdata" >+1<br /> ( -109)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Thisted Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >-109</div>
								 <div class="boxdata" >-&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Naesby Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >244</div>
								 <div class="boxdata" >+&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Sydvest 05</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -108)</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vejlby Skovbakken Aarhus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -121)</div>
								 <div class="boxdata" >148</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dalum If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >312</div>
								 <div class="boxdata" >+&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jammerbugt Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >-131</div>
								 <div class="boxdata" >-&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--denmark 2nd Division  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Holbaek B&i</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >217</div>
								 <div class="boxdata" >+&frac12;<br /> ( -149 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bk Avarta</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac12;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bronshoj Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" >-&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Slagelse Bi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >315</div>
								 <div class="boxdata" >+&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ringkobing If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -131)</div>
								 <div class="boxdata" >575</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -138 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ab Gladsaxe</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -105)</div>
								 <div class="boxdata" >-263</div>
								 <div class="boxdata" >-1&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	