	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Superbowl 53 - Mvp">
			<caption>Superbowl 53 - Mvp</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Superbowl 53 - Mvp  - Feb 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Superbowl Liii Mvp - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tom Brady</td><td>4/5</td><td>-125</td></tr><tr><td>Jared Goff</td><td>11/5</td><td>+220</td></tr><tr><td>Todd Gurley</td><td>8/1</td><td>+800</td></tr><tr><td>Sony Michel</td><td>10/1</td><td>+1000</td></tr><tr><td>Cj Anderson</td><td>25/1</td><td>+2500</td></tr><tr><td>Aaron Donald</td><td>15/1</td><td>+1500</td></tr><tr><td>James White</td><td>22/1</td><td>+2200</td></tr><tr><td>Julian Edelman</td><td>25/1</td><td>+2500</td></tr><tr><td>Brandin Cooks</td><td>40/1</td><td>+4000</td></tr><tr><td>Robert Woods</td><td>40/1</td><td>+4000</td></tr><tr><td>Rob Gronkowski</td><td>25/1</td><td>+2500</td></tr><tr><td>Stephen Gostkowski</td><td>60/1</td><td>+6000</td></tr><tr><td>Dante Fowler Jr</td><td>100/1</td><td>+10000</td></tr><tr><td>Greg Zuerlein</td><td>85/1</td><td>+8500</td></tr><tr><td>Rex Burkhead</td><td>85/1</td><td>+8500</td></tr><tr><td>Aqib Talib</td><td>100/1</td><td>+10000</td></tr><tr><td>Chris Hogan</td><td>100/1</td><td>+10000</td></tr><tr><td>Cory Littleton</td><td>100/1</td><td>+10000</td></tr><tr><td>Josh Reynolds</td><td>100/1</td><td>+10000</td></tr><tr><td>Trey Flowers</td><td>100/1</td><td>+10000</td></tr><tr><td>Ndamukong Suh</td><td>100/1</td><td>+10000</td></tr><tr><td>Cordarelle Patterson</td><td>125/1</td><td>+12500</td></tr><tr><td>Donta Hightower</td><td>150/1</td><td>+15000</td></tr><tr><td>Gerald Everett</td><td>200/1</td><td>+20000</td></tr><tr><td>John Johnson</td><td>200/1</td><td>+20000</td></tr><tr><td>Kyle Van Noy</td><td>100/1</td><td>+10000</td></tr><tr><td>Marcus Peters</td><td>125/1</td><td>+12500</td></tr><tr><td>Michael Brookers</td><td>150/1</td><td>+15000</td></tr><tr><td>Stephon Gilmore</td><td>150/1</td><td>+15000</td></tr><tr><td>Devin Mccourty</td><td>250/1</td><td>+25000</td></tr><tr><td>Elandon Roberts</td><td>250/1</td><td>+25000</td></tr><tr><td>Lamarcus Joyner</td><td>250/1</td><td>+25000</td></tr><tr><td>Mark Barron</td><td>250/1</td><td>+25000</td></tr><tr><td>Patrick Chung</td><td>250/1</td><td>+25000</td></tr><tr><td>Phillip Dorsett</td><td>250/1</td><td>+25000</td></tr><tr><td>Tyler Higbee</td><td>250/1</td><td>+25000</td></tr><tr><td>James Develin</td><td>500/1</td><td>+50000</td></tr><tr><td>Matthew Slater</td><td>500/1</td><td>+50000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will The Super Bowl Mvp Thank First</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Teammates</td><td>3/2</td><td>+150</td></tr><tr><td>God</td><td>8/5</td><td>+160</td></tr><tr><td>Does Not Thank Anyone Or Mention Any On List</td><td>37/10</td><td>+370</td></tr><tr><td>Family</td><td>21/5</td><td>+420</td></tr><tr><td>Coach</td><td>11/2</td><td>+550</td></tr><tr><td>Fans/city</td><td>15/2</td><td>+750</td></tr><tr><td>Owner</td><td>15/2</td><td>+750</td></tr><tr><td>Referees</td><td>35/1</td><td>+3500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	