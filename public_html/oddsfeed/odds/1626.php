	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--italy Serie B">
			<caption>Live--italy Serie B</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 11, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--italy Serie B  - May 11</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Livorno</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >-555</div>
								 <div class="boxdata" >-1&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Calcio Padova</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >1450</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Cremonese</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >285</div>
								 <div class="boxdata" >+&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Perugia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Spezia Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >2800</div>
								 <div class="boxdata" >+2&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Lecce</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-1999</div>
								 <div class="boxdata" >-2&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sportiva Salernitana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +104 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pescara Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -142 )</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Benevento Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -166 )</div>
								 <div class="boxdata" >270</div>
								 <div class="boxdata" >+&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brescia Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ascoli</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >2000</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Crotone</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-833</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Foggia Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >275</div>
								 <div class="boxdata" >+&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hellas Verona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Cittadella</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+2&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Palermo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-2&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Venezia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -166 )</div>
								 <div class="boxdata" >295</div>
								 <div class="boxdata" >+&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Carpi Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	