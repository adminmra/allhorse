	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Aaf Outrights">
			<caption>Aaf Outrights</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 16, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Aaf Outrights  - Feb 16					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Aaf Championship - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Arizona Hotshots</td><td>9/4</td><td>+225</td></tr><tr><td>Salt Lake Stallions</td><td>7/1</td><td>+700</td></tr><tr><td>San Antonio Commanders</td><td>5/1</td><td>+500</td></tr><tr><td>Orlando Apollos</td><td>3/1</td><td>+300</td></tr><tr><td>Atlanta Legends</td><td>14/1</td><td>+1400</td></tr><tr><td>San Diego Fleet</td><td>10/1</td><td>+1000</td></tr><tr><td>Memphis Express</td><td>20/1</td><td>+2000</td></tr><tr><td>Birmingham Iron</td><td>9/2</td><td>+450</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	