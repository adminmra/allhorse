	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Ent - The Next James Bond">
			<caption>Ent - The Next James Bond</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Ent - The Next James Bond  - Dec 31					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Celebrity - Who Will Play The Next James Bond?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>James Norton</td><td>6/1</td><td>+600</td></tr><tr><td>Tom Hardy</td><td>6/1</td><td>+600</td></tr><tr><td>Jack Huston</td><td>40/1</td><td>+4000</td></tr><tr><td>Aidan Turner</td><td>12/1</td><td>+1200</td></tr><tr><td>Idris Elba</td><td>4/1</td><td>+400</td></tr><tr><td>Tom Hiddleston</td><td>2/1</td><td>+200</td></tr><tr><td>Michael Fassbender</td><td>33/1</td><td>+3300</td></tr><tr><td>Jaime Bell</td><td>60/1</td><td>+6000</td></tr><tr><td>Jude Law</td><td>66/1</td><td>+6600</td></tr><tr><td>Jack Oconnell</td><td>100/1</td><td>+10000</td></tr><tr><td>Henry Cavil</td><td>16/1</td><td>+1600</td></tr><tr><td>Cillian Murphy</td><td>5/1</td><td>+500</td></tr><tr><td>Andrew Lincoln</td><td>100/1</td><td>+10000</td></tr><tr><td>Jack Lowden</td><td>33/1</td><td>+3300</td></tr><tr><td>Luke Evans</td><td>33/1</td><td>+3300</td></tr><tr><td>Taron Egerton</td><td>100/1</td><td>+10000</td></tr><tr><td>Damian Lewis</td><td>2/1</td><td>+200</td></tr><tr><td>Matthew Goode</td><td>200/1</td><td>+20000</td></tr><tr><td>Dan Stevens</td><td>66/1</td><td>+6600</td></tr><tr><td>Gillian Anderson</td><td>100/1</td><td>+10000</td></tr><tr><td>Colin Salmon</td><td>100/1</td><td>+10000</td></tr><tr><td>Matthew Macfadyen</td><td>500/1</td><td>+50000</td></tr><tr><td>Christian Bale</td><td>100/1</td><td>+10000</td></tr><tr><td>Ryan Gosling</td><td>200/1</td><td>+20000</td></tr><tr><td>Sam Riley</td><td>500/1</td><td>+50000</td></tr><tr><td>Gerard Butler</td><td>500/1</td><td>+50000</td></tr><tr><td>Charlie Hunnam</td><td>50/1</td><td>+5000</td></tr><tr><td>David Oyelowo</td><td>125/1</td><td>+12500</td></tr><tr><td>Benedict Cumberbatch</td><td>100/1</td><td>+10000</td></tr><tr><td>Adrian Lester</td><td>200/1</td><td>+20000</td></tr><tr><td>John Boyega</td><td>100/1</td><td>+10000</td></tr><tr><td>Richard Madden</td><td>3/1</td><td>+300</td></tr><tr><td>Richard Armitage</td><td>100/1</td><td>+10000</td></tr><tr><td>Rupert Friend</td><td>200/1</td><td>+20000</td></tr><tr><td>James Mcavoy</td><td>200/1</td><td>+20000</td></tr><tr><td>Martin Freeman</td><td>500/1</td><td>+50000</td></tr><tr><td>Barry Sloan</td><td>500/1</td><td>+50000</td></tr><tr><td>Tom Ellis</td><td>500/1</td><td>+50000</td></tr><tr><td>Eddie Redmayne</td><td>500/1</td><td>+50000</td></tr><tr><td>Alex Oloughlin</td><td>500/1</td><td>+50000</td></tr><tr><td>Chiwetel Ejiofor</td><td>125/1</td><td>+12500</td></tr><tr><td>Orlando Bloom</td><td>100/1</td><td>+10000</td></tr><tr><td>Chris Hemsworth</td><td>20/1</td><td>+2000</td></tr><tr><td>Ewan Mcgregor</td><td>33/1</td><td>+3300</td></tr><tr><td>Clive Owen</td><td>200/1</td><td>+20000</td></tr><tr><td>Tim Roth</td><td>500/1</td><td>+50000</td></tr><tr><td>Dominic West</td><td>500/1</td><td>+50000</td></tr><tr><td>Dougray Scott</td><td>500/1</td><td>+50000</td></tr><tr><td>Liev Schreiber</td><td>500/1</td><td>+50000</td></tr><tr><td>James Purefoy</td><td>500/1</td><td>+50000</td></tr><tr><td>Kristen Stewart</td><td>500/1</td><td>+50000</td></tr><tr><td>Colin Farrell</td><td>500/1</td><td>+50000</td></tr><tr><td>Jason Statham</td><td>200/1</td><td>+20000</td></tr><tr><td>Sam Heughan</td><td>66/1</td><td>+6600</td></tr><tr><td>Kit Harington</td><td>100/1</td><td>+10000</td></tr><tr><td>Nicholas Hoult</td><td>100/1</td><td>+10000</td></tr><tr><td>Rupert Penry-jones</td><td>500/1</td><td>+50000</td></tr><tr><td>O.t. Fagbenle</td><td>500/1</td><td>+50000</td></tr><tr><td>Sam Worthington</td><td>500/1</td><td>+50000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	