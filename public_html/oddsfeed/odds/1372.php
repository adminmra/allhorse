	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="International - Basketball Futures">
			<caption>International - Basketball Futures</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					International - Basketball Futures  - Aug 31					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Fiba World Cup - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Usa</td><td>5/28</td><td>-560</td></tr><tr><td>Serbia</td><td>9/1</td><td>+900</td></tr><tr><td>Spain</td><td>18/1</td><td>+1800</td></tr><tr><td>France</td><td>33/1</td><td>+3300</td></tr><tr><td>Greece</td><td>33/1</td><td>+3300</td></tr><tr><td>Australia</td><td>40/1</td><td>+4000</td></tr><tr><td>Lithuania</td><td>50/1</td><td>+5000</td></tr><tr><td>Canada</td><td>66/1</td><td>+6600</td></tr><tr><td>Argentina</td><td>80/1</td><td>+8000</td></tr><tr><td>Russia</td><td>90/1</td><td>+9000</td></tr><tr><td>Italy</td><td>100/1</td><td>+10000</td></tr><tr><td>Germany</td><td>150/1</td><td>+15000</td></tr><tr><td>Turkey</td><td>150/1</td><td>+15000</td></tr><tr><td>Brazil</td><td>150/1</td><td>+15000</td></tr><tr><td>Poland</td><td>200/1</td><td>+20000</td></tr><tr><td>Puerto Rico</td><td>250/1</td><td>+25000</td></tr><tr><td>Montenegro</td><td>250/1</td><td>+25000</td></tr><tr><td>New Zealand</td><td>300/1</td><td>+30000</td></tr><tr><td>Czech Republic</td><td>400/1</td><td>+40000</td></tr><tr><td>China</td><td>500/1</td><td>+50000</td></tr><tr><td>Nigeria</td><td>500/1</td><td>+50000</td></tr><tr><td>Dominican Republic</td><td>500/1</td><td>+50000</td></tr><tr><td>Iran</td><td>750/1</td><td>+75000</td></tr><tr><td>South Korea</td><td>1250/1</td><td>+125000</td></tr><tr><td>Angola</td><td>1250/1</td><td>+125000</td></tr><tr><td>Jordan</td><td>1250/1</td><td>+125000</td></tr><tr><td>Japan</td><td>1250/1</td><td>+125000</td></tr><tr><td>Phillippines</td><td>1250/1</td><td>+125000</td></tr><tr><td>Venezuela</td><td>12500/1</td><td>+1250000</td></tr><tr><td>Tunisia</td><td>1250/1</td><td>+125000</td></tr><tr><td>Ivory Coast</td><td>1500/1</td><td>+150000</td></tr><tr><td>Senegal</td><td>1500/1</td><td>+150000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Fiba World Cup Group D - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Serbia</td><td>4/25</td><td>-625</td></tr><tr><td>Italy</td><td>7/2</td><td>+350</td></tr><tr><td>Angola</td><td>150/1</td><td>+15000</td></tr><tr><td>Phillippines</td><td>150/1</td><td>+15000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Fiba World Cup Group A - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Poland</td><td>11/10</td><td>+110</td></tr><tr><td>China</td><td>8/5</td><td>+160</td></tr><tr><td>Venezuela</td><td>4/1</td><td>+400</td></tr><tr><td>Ivory Coast</td><td>33/1</td><td>+3300</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Fiba World Cup Group B - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Argentina</td><td>5/6</td><td>-120</td></tr><tr><td>Russia</td><td>29/20</td><td>+145</td></tr><tr><td>Nigeria</td><td>6/1</td><td>+600</td></tr><tr><td>South Korea</td><td>150/1</td><td>+15000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Fiba World Cup Group C - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Spain</td><td>1/100</td><td>-10000</td></tr><tr><td>Puerto Rico</td><td>11/1</td><td>+1100</td></tr><tr><td>Iran</td><td>80/1</td><td>+8000</td></tr><tr><td>Tunisia</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					International - Basketball Futures  - Sep 01					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Fiba World Cup Group H - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Australia</td><td>8/5</td><td>+160</td></tr><tr><td>Lithuania</td><td>9/5</td><td>+180</td></tr><tr><td>Canada</td><td>19/10</td><td>+190</td></tr><tr><td>Senegal</td><td>150/1</td><td>+15000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Fiba World Cup Group F - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Greece</td><td>5/17</td><td>-340</td></tr><tr><td>Brazil</td><td>9/2</td><td>+450</td></tr><tr><td>Montenegro</td><td>9/1</td><td>+900</td></tr><tr><td>New Zealand</td><td>20/1</td><td>+2000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Fiba World Cup Group E - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Usa</td><td>1/100</td><td>-10000</td></tr><tr><td>Turkey</td><td>14/1</td><td>+1400</td></tr><tr><td>Czech Republic</td><td>33/1</td><td>+3300</td></tr><tr><td>Japan</td><td>150/1</td><td>+15000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o8t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Fiba World Cup Group G - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>France</td><td>5/18</td><td>-360</td></tr><tr><td>Germany </td><td>15/4</td><td>+375</td></tr><tr><td>Dominican Republic</td><td>10/1</td><td>+1000</td></tr><tr><td>Jordan</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');addSort('o6t');addSort('o7t');addSort('o8t');</script>
{/literal}	
	