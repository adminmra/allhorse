	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - France Ligue 1 - To Win">
			<caption>Soccer - France Ligue 1 - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - France Ligue 1 - To Win  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 France Ligue 1 - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Psg</td><td>1/14</td><td>-1400</td></tr><tr><td>Lyon</td><td>10/1</td><td>+1000</td></tr><tr><td>Marseille</td><td>100/1</td><td>+10000</td></tr><tr><td>Monaco</td><td>80/1</td><td>+8000</td></tr><tr><td>Lille</td><td>66/1</td><td>+6600</td></tr><tr><td>St Etienne</td><td>100/1</td><td>+10000</td></tr><tr><td>Nice</td><td>200/1</td><td>+20000</td></tr><tr><td>Montpellier</td><td>150/1</td><td>+15000</td></tr><tr><td>Rennes</td><td>66/1</td><td>+6600</td></tr><tr><td>Bordeaux</td><td>250/1</td><td>+25000</td></tr><tr><td>Nantes</td><td>100/1</td><td>+10000</td></tr><tr><td>Reims</td><td>500/1</td><td>+50000</td></tr><tr><td>Strasbourg</td><td>500/1</td><td>+50000</td></tr><tr><td>Nimes</td><td>500/1</td><td>+50000</td></tr><tr><td>Metz</td><td>1000/1</td><td>+100000</td></tr><tr><td>Toulouse</td><td>750/1</td><td>+75000</td></tr><tr><td>Angers</td><td>500/1</td><td>+50000</td></tr><tr><td>Amiens</td><td>1000/1</td><td>+100000</td></tr><tr><td>Brest</td><td>1500/1</td><td>+150000</td></tr><tr><td>Dijon</td><td>1500/1</td><td>+150000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	