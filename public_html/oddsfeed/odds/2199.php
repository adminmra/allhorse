	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--cyprus 1st Division">
			<caption>Pre--cyprus 1st Division</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--cyprus 1st Division  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aek Larnaca Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -105 )</div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" >-1<br /> ( -111)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olympiakos Nicosia Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -125 )</div>
								 <div class="boxdata" >459</div>
								 <div class="boxdata" >+1<br /> ( -119)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--cyprus 1st Division  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Apollon Limassol Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Omonia Nicosia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Doxa Katokopias Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >371</div>
								 <div class="boxdata" >+&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ael Limassol Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >-151</div>
								 <div class="boxdata" >-&frac12;<br /> ( -151 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--cyprus 1st Division  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nea Salamina Famagusta Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ethnikos Achnas Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >193</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pafos Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >286</div>
								 <div class="boxdata" >+&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Anorthosis Famagusta Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >-126</div>
								 <div class="boxdata" >-&frac12;<br /> ( -126 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--cyprus 1st Division  - Sep 23</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Apoel Nicosia Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >-400</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -138 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Enosis Neon Paralimni Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >797</div>
								 <div class="boxdata" >+1&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	