	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Racing - F1- Drivers Championship - To Win">
			<caption>Racing - F1- Drivers Championship - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Racing - F1- Drivers Championship - To Win  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Formula 1 Drivers Championship - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Lewis Hamilton</td><td>1/100</td><td>-10000</td></tr><tr><td>Sebastian Vettel</td><td>200/1</td><td>+20000</td></tr><tr><td>Max Verstappen</td><td>100/1</td><td>+10000</td></tr><tr><td>Charles Leclerc</td><td>100/1</td><td>+10000</td></tr><tr><td>Valtteri Bottas</td><td>25/1</td><td>+2500</td></tr><tr><td>Pierre Gasly</td><td>4000/1</td><td>+400000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	