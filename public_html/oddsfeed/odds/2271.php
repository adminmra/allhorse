	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--malta Premier League">
			<caption>Pre--malta Premier League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 17, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Pre--malta Premier League  - Dec 17</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Hibernians Fc<br /> - vs - <br /> Balzan Youths</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-126)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+150</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-111 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (100)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+179</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-123</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Hamrun Spartans Fc<br /> - vs - <br /> Valletta Fc</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1<br /> (-112)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+516</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-116 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1<br /> (-113)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-188</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-119</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	