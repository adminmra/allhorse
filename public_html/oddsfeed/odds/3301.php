	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Cbb - Ncaa Bk Specials">
			<caption>Cbb - Ncaa Bk Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 31, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Cbb - Ncaa Bk Specials  - Apr 08					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Shoe Brand Zion Williamson Signs With In 2019</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Nike</td><td>11/10</td><td>+110</td></tr><tr><td>Adidas</td><td>3/2</td><td>+150</td></tr><tr><td>Puma</td><td>9/2</td><td>+450</td></tr><tr><td>New Balance</td><td>18/1</td><td>+1800</td></tr><tr><td>Under Armour</td><td>18/1</td><td>+1800</td></tr><tr><td>Big Baller</td><td>100/1</td><td>+10000</td></tr><tr><td>Starbury</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Zion Williamson Be Drafted 1st Overall In 2019</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>1/10</td><td>-1000</td></tr><tr><td>No</td><td>5/1</td><td>+500</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">How Many Years Will Zion Williamsons Shoe Deal Be</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 6½ Years</td><td>1/2</td><td>-200</td></tr><tr><td>Under 6½ Years </td><td>3/2</td><td>+150</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Total Value Of Zion Williamsons Shoe Deal</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 55 Million</td><td>10/19</td><td>-190</td></tr><tr><td>Under 55 Million</td><td>29/20</td><td>+145</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');</script>
{/literal}	
	