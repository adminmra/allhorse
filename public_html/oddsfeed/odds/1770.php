	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--macedonia 1 Mfl">
			<caption>Live--macedonia 1 Mfl</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated April 24, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--macedonia 1 Mfl  - Apr 24</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Vardar Skopje</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >235</div>
								 <div class="boxdata" >+&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gjorce Petrov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Sileks Kratovo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" >950</div>
								 <div class="boxdata" >+1<br /> ( -105)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Renova</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -200 )</div>
								 <div class="boxdata" >-333</div>
								 <div class="boxdata" >-1<br /> ( -133)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Akademija Pandev</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" >265</div>
								 <div class="boxdata" >+&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Shkupi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -181 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Pobeda Ad Prilep</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( +104 )</div>
								 <div class="boxdata" >2300</div>
								 <div class="boxdata" >+2&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Rabotnicki Skopje</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -142 )</div>
								 <div class="boxdata" >-1428</div>
								 <div class="boxdata" >-2&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	