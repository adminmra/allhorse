	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--italy Serie A">
			<caption>Pre--italy Serie A</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--italy Serie A  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Genoa Cfc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cagliari Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--italy Serie A  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brescia Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >293</div>
								 <div class="boxdata" >+&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Udinese Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >-113</div>
								 <div class="boxdata" >-&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Inter Milano</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Milan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >202</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hellas Verona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -116)</div>
								 <div class="boxdata" >1650</div>
								 <div class="boxdata" >+2<br /> ( -102)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Juventus Turin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -113)</div>
								 <div class="boxdata" >-714</div>
								 <div class="boxdata" >-2<br /> ( -129)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--italy Serie A  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Roma</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -105)</div>
								 <div class="boxdata" >134</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bologna Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -126)</div>
								 <div class="boxdata" >174</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Spal 2013</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >258</div>
								 <div class="boxdata" >+&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Sassuolo Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >-104</div>
								 <div class="boxdata" >-&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ssc Napoli</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -108)</div>
								 <div class="boxdata" >-277</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Lecce</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -121)</div>
								 <div class="boxdata" >639</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Torino</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Uc Sampdoria</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >148</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Parma Calcio 1913 S.r.l.</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -105)</div>
								 <div class="boxdata" >735</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Lazio Roma</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -125)</div>
								 <div class="boxdata" >-322</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acf Fiorentina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -108)</div>
								 <div class="boxdata" >268</div>
								 <div class="boxdata" >+&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atalanta Bergamasca Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -120)</div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" >-&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--italy Serie A  - Sep 24</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Juventus Turin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >-277</div>
								 <div class="boxdata" >-1&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brescia Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -103 )</div>
								 <div class="boxdata" >646</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Udinese Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hellas Verona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >167</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--italy Serie A  - Sep 25</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bologna Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Genoa Cfc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >163</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Lecce</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >298</div>
								 <div class="boxdata" >+&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Spal 2013</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >-131</div>
								 <div class="boxdata" >-&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Uc Sampdoria</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -114 )</div>
								 <div class="boxdata" >286</div>
								 <div class="boxdata" >+&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acf Fiorentina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >-128</div>
								 <div class="boxdata" >-&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Lazio Roma</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >221</div>
								 <div class="boxdata" >+&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Inter Milano</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac12;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Sassuolo Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >202</div>
								 <div class="boxdata" >+&frac12;<br /> ( -156 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Parma Calcio 1913 S.r.l.</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac12;<br /> ( +112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atalanta Bergamasca Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >218</div>
								 <div class="boxdata" >+&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Roma</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >-106</div>
								 <div class="boxdata" >-&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cagliari Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >771</div>
								 <div class="boxdata" >+1&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ssc Napoli</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >-400</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--italy Serie A  - Sep 26</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Milan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >173</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Torino</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >142</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	