	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--hockey - Germany (del)">
			<caption>Live--hockey - Germany (del)</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated April 14, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--hockey - Germany (del)  - Apr 14</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ehc Red Bull München</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >-166</div>
								 <div class="boxdata" >-1<br /> ( +125)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Augsburger Panther</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >+1<br /> ( -200)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	