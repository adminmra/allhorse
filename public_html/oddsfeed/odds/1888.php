	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--poland Polish Cup">
			<caption>Live--poland Polish Cup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 6, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Live--poland Polish Cup  - Dec 06</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Ks Gornik Zabrze<br /> - vs - <br /> Rozwoj Katowice</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.25<br /> (-105)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+165</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-117 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.25<br /> (-133)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+625</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-117</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	