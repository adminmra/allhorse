	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mma - Combate Americas">
			<caption>Mma - Combate Americas</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Mma - Combate Americas  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Erick Conzalez</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -180 )</div>
								 <div class="boxdata" >744</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rafa Garcia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +150 )</div>
								 <div class="boxdata" >-1478</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aaron Brink</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +171 )</div>
								 <div class="boxdata" >575</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Oscar Cota</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -211 )</div>
								 <div class="boxdata" >-950</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yajaira Romo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >500</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yasmine Jauregui</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-800</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Albert Trujillo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Edgar Chairez</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" >-215</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lj Torres</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" >260</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Eliezer Ortega</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" >-335</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cristian Perez</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" >113</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jair Perez</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" >-143</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Karla Almaguer</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -151 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Laura Huziar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +121 )</div>
								 <div class="boxdata" >-180</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Carlos Ernesto Ochoa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >118</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gilberto Santos</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-148</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gabriel Valdez</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -260 )</div>
								 <div class="boxdata" >-180</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Santiago Monreal</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +200 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	