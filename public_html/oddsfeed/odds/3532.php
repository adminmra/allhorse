	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Old Forester Turf Classic - To Win">
			<caption>Horses - Old Forester Turf Classic - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Old Forester Turf Classic - To Win  - May 04					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Old Forester Turf Classic - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Qurbaan</td><td>15/2</td><td>+750</td></tr><tr><td>Raging Bull</td><td>9/2</td><td>+450</td></tr><tr><td>Multiplier</td><td>20/1</td><td>+2000</td></tr><tr><td>Sabador</td><td>16/1</td><td>+1600</td></tr><tr><td>Markitoff</td><td>14/1</td><td>+1400</td></tr><tr><td>Clyde's Image</td><td>22/1</td><td>+2200</td></tr><tr><td>Prime Attraction</td><td>14/1</td><td>+1400</td></tr><tr><td>Breaking The Rules</td><td>14/1</td><td>+1400</td></tr><tr><td>Synchrony</td><td>11/2</td><td>+550</td></tr><tr><td>Ticonderoga</td><td>11/1</td><td>+1100</td></tr><tr><td>March To The Arch</td><td>28/1</td><td>+2800</td></tr><tr><td>Bricks And Mortar</td><td>9/4</td><td>+225</td></tr><tr><td>Next Shares</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	