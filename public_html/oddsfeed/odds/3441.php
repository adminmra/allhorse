	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba - Three Point Contest">
			<caption>Nba - Three Point Contest</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 16, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nba - Three Point Contest  - Feb 16					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Nba All Star Three Point Contest - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Buddy Hield (sac)</td><td>4/1</td><td>+400</td></tr><tr><td>Damian Lillard (por)</td><td>8/1</td><td>+800</td></tr><tr><td>Danny Green (tor)</td><td>11/1</td><td>+1100</td></tr><tr><td>Devin Booker (pho)</td><td>9/2</td><td>+450</td></tr><tr><td>Dirk Nowitzki (dal)</td><td>15/1</td><td>+1500</td></tr><tr><td>Joe Harris (bkn)</td><td>7/1</td><td>+700</td></tr><tr><td>Kemba Walker (cha)</td><td>9/1</td><td>+900</td></tr><tr><td>Khris Middleton (mil)</td><td>12/1</td><td>+1200</td></tr><tr><td>Seth Curry (por)</td><td>5/1</td><td>+500</td></tr><tr><td>Stephen Curry (gs)</td><td>13/10</td><td>+130</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	