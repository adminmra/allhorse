	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--japan J League 2">
			<caption>Pre--japan J League 2</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--japan J League 2  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kyoto Sanga Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >137</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >V-varen Nagasaki</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Montedio Yamagata</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -109 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Ryukyu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >212</div>
								 <div class="boxdata" >+&frac12;<br /> ( -147 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Niigata Albirex</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >219</div>
								 <div class="boxdata" >+&frac12;<br /> ( -158 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ventforet Kofu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" >-&frac12;<br /> ( +119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Renofa Yamaguchi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -106)</div>
								 <div class="boxdata" >249</div>
								 <div class="boxdata" >+&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Zweigen Kanazawa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -125)</div>
								 <div class="boxdata" >-111</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fagiano Okayama</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >161</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Avispa Fukuoka</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--japan J League 2  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tokyo Verdy</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >276</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Omiya Ardija</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >-112</div>
								 <div class="boxdata" >-&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tokushima Vortis</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jef United Ichihara Chiba</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >227</div>
								 <div class="boxdata" >+&frac12;<br /> ( -138 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yokohama Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >-102</div>
								 <div class="boxdata" >-&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Machida Zelvia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >243</div>
								 <div class="boxdata" >+&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ehime Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >476</div>
								 <div class="boxdata" >+1<br /> ( -112)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kashiwa Reysol</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >-204</div>
								 <div class="boxdata" >-1<br /> ( -123)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kagoshima United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tochigi Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >168</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Gifu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >424</div>
								 <div class="boxdata" >+1<br /> ( -128)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mito Hollyhock</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >-181</div>
								 <div class="boxdata" >-1<br /> ( -107)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	