	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Liga Mx Apertura">
			<caption>Soccer - Liga Mx Apertura</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Liga Mx Apertura  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Torneo De Apertura Liga Mx - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tigres</td><td>6/1</td><td>+600</td></tr><tr><td>Monterrey</td><td>9/1</td><td>+900</td></tr><tr><td>Club America</td><td>4/1</td><td>+400</td></tr><tr><td>Leon</td><td>8/1</td><td>+800</td></tr><tr><td>Toluca</td><td>200/1</td><td>+20000</td></tr><tr><td>Cruz Azul</td><td>20/1</td><td>+2000</td></tr><tr><td>Chivas Guadalajara</td><td>25/1</td><td>+2500</td></tr><tr><td>Pachuca</td><td>100/1</td><td>+10000</td></tr><tr><td>Santos Laguna</td><td>11/2</td><td>+550</td></tr><tr><td>Pumas Unam</td><td>16/1</td><td>+1600</td></tr><tr><td>Tijuana</td><td>16/1</td><td>+1600</td></tr><tr><td>Atlas</td><td>25/1</td><td>+2500</td></tr><tr><td>Necaxa</td><td>12/1</td><td>+1200</td></tr><tr><td>Queretaro Fc</td><td>10/1</td><td>+1000</td></tr><tr><td>Monarcas Morelia</td><td>33/1</td><td>+3300</td></tr><tr><td>Puebla Fc</td><td>500/1</td><td>+50000</td></tr><tr><td>Atletico San Luis</td><td>25/1</td><td>+2500</td></tr><tr><td>Fc Juarez</td><td>250/1</td><td>+25000</td></tr><tr><td>Veracruz</td><td>2500/1</td><td>+250000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	