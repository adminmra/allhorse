	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Fifa World Cup Props">
			<caption>Soccer - Fifa World Cup Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Fifa World Cup Props  - Nov 21					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will The 2022 Wc Final Be A Rematch Of 2018 Final?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2022 Wc Rematch Of 2018 Final - Yes </td><td>25/1</td><td>+2500</td></tr><tr><td>2022 Wc Rematch Of 2018 Final - No</td><td>1/50</td><td>-5000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2018 Quarter Finalists Make 2022 Wc Quarter Final?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2018 Q.f. Makes 2022 Q.f. - Yes</td><td>50/1</td><td>+5000</td></tr><tr><td>2018 Q.f. Makes 2022 Q.f. - No</td><td>1/100</td><td>-10000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Odds Harry Kane Wins The 2022 Wc Golden Boot?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>H. Kane Wins 2022 Wc Golden Boot - Yes</td><td>10/1</td><td>+1000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Team Usa Qualify For The 2022 World Cup?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Team Usa Qualifys For 2022 Wc - Yes</td><td>1/5</td><td>-500</td></tr><tr><td>Team Usa Qualifys For 2022 Wc - No</td><td>3/1</td><td>+300</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Stage Team Usa Will Be Eliminated At The 2022 Wc?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Team Usa - Do Not Qualify</td><td>3/1</td><td>+300</td></tr><tr><td>Team Usa Eliminated - Group Stage</td><td>2/1</td><td>+200</td></tr><tr><td>Team Usa Eliminated - Round Of 16</td><td>2/1</td><td>+200</td></tr><tr><td>Team Usa Eliminated - Quarterfinals</td><td>6/1</td><td>+600</td></tr><tr><td>Team Usa Eliminated - Semi Finals</td><td>12/1</td><td>+1200</td></tr><tr><td>Team Usa Eliminated - Finals</td><td>26/1</td><td>+2600</td></tr><tr><td>Team Usa - Winner</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">D. Sarachan Coach Of Team Usa For The 2022 Wc?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>D. Sarachan Coach Team Usa 2022 Wc - Yes</td><td>4/1</td><td>+400</td></tr><tr><td>D. Sarachan Coach Team Usa 2022 Wc - No</td><td>1/7</td><td>-700</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Team Italy Qualify For The 2022 World Cup?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Italy Qualifys For 2022 Wc - Yes</td><td>1/10</td><td>-1000</td></tr><tr><td>Italy Qualifys For 2022 Wc - No</td><td>6/1</td><td>+600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Team Canada Qualify For The 2022 World Cup?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Canada Qualifys For 2022 Wc - Yes</td><td>6/1</td><td>+600</td></tr><tr><td>Canada Qualifys For 2022 Wc - No</td><td>1/10</td><td>-1000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o8t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Temperature At Kickoff Of The 2022 Wc Final?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Kickoff Temp 2022 Wc - Over 72.5f</td><td>5/6</td><td>-120</td></tr><tr><td>Kickoff Temp 2022 Wc - Under 72.5f</td><td>5/6</td><td>-120</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o9t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Qatar Make The 2022 Wc Final?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Qatar Makes The 2022 Wc Final - Yes</td><td>250/1</td><td>+25000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o10t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Ronaldo Play In The 2022 World Cup?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Ronaldo Plays 2022 World Cup - Yes</td><td>2/3</td><td>-150</td></tr><tr><td>Ronaldo Plays 2022 World Cup - No</td><td>13/10</td><td>+130</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');addSort('o6t');addSort('o7t');addSort('o8t');addSort('o9t');addSort('o10t');</script>
{/literal}	
	