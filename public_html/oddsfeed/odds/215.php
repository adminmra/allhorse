	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mlb - National League Pennant - To Win">
			<caption>Mlb - National League Pennant - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Mlb - National League Pennant - To Win  - Sep 22					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 National League Pennant - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Los Angeles Dodgers</td><td>5/7</td><td>-140</td></tr><tr><td>Atlanta Braves</td><td>2/1</td><td>+200</td></tr><tr><td>Chicago Cubs</td><td>10/1</td><td>+1000</td></tr><tr><td>Milwaukee Brewers</td><td>14/1</td><td>+1400</td></tr><tr><td>Philadelphia Phillies</td><td>22/1</td><td>+2200</td></tr><tr><td>St Louis Cardinals</td><td>11/2</td><td>+550</td></tr><tr><td>Washington Nationals</td><td>15/2</td><td>+750</td></tr><tr><td>Arizona Diamondbacks</td><td>33/1</td><td>+3300</td></tr><tr><td>New York Mets</td><td>22/1</td><td>+2200</td></tr><tr><td>San Francisco Giants</td><td>600/1</td><td>+60000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	