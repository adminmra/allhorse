	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mlb - American League Pennant - To Win">
			<caption>Mlb - American League Pennant - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Mlb - American League Pennant - To Win  - Sep 22					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 American League Pennant - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>New York Yankees</td><td>9/5</td><td>+180</td></tr><tr><td>Houston Astros</td><td>10/13</td><td>-130</td></tr><tr><td>Minnesota Twins</td><td>7/1</td><td>+700</td></tr><tr><td>Tampa Bay Rays</td><td>19/2</td><td>+950</td></tr><tr><td>Boston Red Sox</td><td>300/1</td><td>+30000</td></tr><tr><td>Cleveland Indians</td><td>13/1</td><td>+1300</td></tr><tr><td>Oakland Athletics</td><td>17/2</td><td>+850</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	