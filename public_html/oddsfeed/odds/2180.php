	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--italy Serie B">
			<caption>Pre--italy Serie B</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--italy Serie B  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Venezia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >449</div>
								 <div class="boxdata" >+1<br /> ( -147)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Frosinone Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >-161</div>
								 <div class="boxdata" >-1<br /> ( +110)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--italy Serie B  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Perugia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >199</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Spezia Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >136</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Crotone</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >234</div>
								 <div class="boxdata" >+&frac12;<br /> ( -156 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Cremonese</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac12;<br /> ( +117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ascoli Calcio 1898 Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >217</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Juve Stabia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cosenza Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >448</div>
								 <div class="boxdata" >+1<br /> ( -142)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Benevento Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >-161</div>
								 <div class="boxdata" >-1<br /> ( +108)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pordenone Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >269</div>
								 <div class="boxdata" >+&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Livorno</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Cittadella</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >409</div>
								 <div class="boxdata" >+1<br /> ( -151)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Empoli</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" >-1<br /> ( +112)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Pisa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >269</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Chievo Verona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >-102</div>
								 <div class="boxdata" >-&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Virtus Entella</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >185</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pescara Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >145</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--italy Serie B  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sportiva Salernitana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >165</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Trapani Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >158</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	