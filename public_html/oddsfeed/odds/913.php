	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nhl - Contest Game Props">
			<caption>Nhl - Contest Game Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nhl - Contest Game Props  - Jun 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Bruins Vs Blues - Winning Margin</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Bruins Win By 1 Goal</td><td>13/5</td><td>+260</td></tr><tr><td>Bruins Win By 2 Goals</td><td>13/2</td><td>+650</td></tr><tr><td>Bruins Win By 3 Goals</td><td>10/1</td><td>+1000</td></tr><tr><td>Bruins Win By 4 Goals</td><td>16/1</td><td>+1600</td></tr><tr><td>Bruins Win By 5 Or More Goals</td><td>35/1</td><td>+3500</td></tr><tr><td>Blues Win By 1 Goal</td><td>12/5</td><td>+240</td></tr><tr><td>Blues Win By 2 Goals</td><td>11/2</td><td>+550</td></tr><tr><td>Blues Win By 3 Goals</td><td>8/1</td><td>+800</td></tr><tr><td>Blues Win By 4 Goals</td><td>12/1</td><td>+1200</td></tr><tr><td>Blues Win By 5 Or More Goals</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Bruins Vs Blues - Race To 3 Goals</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Bruins</td><td>17/10</td><td>+170</td></tr><tr><td>Blues</td><td>27/20</td><td>+135</td></tr><tr><td>Neither</td><td>23/10</td><td>+230</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	