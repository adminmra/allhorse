	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Ecuador Serie A">
			<caption>Ecuador Serie A</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated November 28, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
            <tr class="header"><td>TEAM</td><td>SPREAD</td><td>MONEYLINE</td><td>TOTAL</td></tr>
	        <tr><th colspan="4">Ecuador Serie A  - Nov 28</th></tr>
         <tr><td colspan="4"><table class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0"><tbody>
         <tr><td class="teams"><br />Deportivo Cuenca<br /> - vs - <br /> Barcelona Sc</td><td class="spmoto"><span class="box" >1.5<br /> (-130) </span><br /><hr /><br/><span class="box" > -1.5<br /> (110)</span></td><td class="spmoto"><span class="box" >+742</span> <br /><hr /><br/> <span class="box" >-291</span></td><td class="spmoto"><span class="box" >o2&frac12;-115 </span><br /><hr /><br/> <span class="box" >u2&frac12;-105</span></td></tr><tr><td class="teams"><br />Independiente Del Valle<br /> - vs - <br /> Emelec</td><td class="spmoto"><span class="box" >0.5<br /> (125) </span><br /><hr /><br/><span class="box" > -0.5<br /> (-145)</span></td><td class="spmoto"><span class="box" >+365</span> <br /><hr /><br/> <span class="box" >-146</span></td><td class="spmoto"><span class="box" >o2&frac12;-110 </span><br /><hr /><br/> <span class="box" >u2&frac12;-110</span></td></tr><tr><td class="teams"><br />Universidad Catolica<br /> - vs - <br /> Macara</td><td class="spmoto"><span class="box" >0<br /> (105) </span><br /><hr /><br/><span class="box" > 0<br /> (-125)</span></td><td class="spmoto"><span class="box" >+184</span> <br /><hr /><br/> <span class="box" >+140</span></td><td class="spmoto"><span class="box" >o2&frac12;-105 </span><br /><hr /><br/> <span class="box" >u2&frac12;-115</span></td></tr><tr><td class="teams"><br />Guayaquil City Fc<br /> - vs - <br /> Delfin</td><td class="spmoto"><span class="box" >1.5<br /> (120) </span><br /><hr /><br/><span class="box" > -1.5<br /> (-140)</span></td><td class="spmoto"><span class="box" >+1224</span> <br /><hr /><br/> <span class="box" >-524</span></td><td class="spmoto"><span class="box" >o2&frac12;-135 </span><br /><hr /><br/> <span class="box" >u2&frac12;+115</span></td></tr><tr><td class="teams"><br />Ldu Quito<br /> - vs - <br /> Aucas</td><td class="spmoto"><span class="box" >0<br /> (-120) </span><br /><hr /><br/><span class="box" > 0<br /> (100)</span></td><td class="spmoto"><span class="box" >+157</span> <br /><hr /><br/> <span class="box" >+176</span></td><td class="spmoto"><span class="box" >o2&frac12;+130 </span><br /><hr /><br/> <span class="box" >u2&frac12;-150</span></td></tr><tr><td class="teams"><br />Tecnico Universitario<br /> - vs - <br /> El Nacional</td><td class="spmoto"><span class="box" >0.5<br /> (125) </span><br /><hr /><br/><span class="box" > -0.5<br /> (-145)</span></td><td class="spmoto"><span class="box" >+322</span> <br /><hr /><br/> <span class="box" >-146</span></td><td class="spmoto"><span class="box" >o3-110 </span><br /><hr /><br/> <span class="box" >u3-110</span></td></tr>                  </tbody>
               </table>
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	