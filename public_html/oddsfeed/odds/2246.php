	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--norway 1st Division">
			<caption>Pre--norway 1st Division</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--norway 1st Division  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kfum Oslo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >350</div>
								 <div class="boxdata" >+1<br /> ( -131)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aalesunds Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >-161</div>
								 <div class="boxdata" >-1<br /> ( -101)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kongsvinger Il Fotball</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >-107</div>
								 <div class="boxdata" >-&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Notodden Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sogndal Il</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -113 )</div>
								 <div class="boxdata" >-101</div>
								 <div class="boxdata" >-&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Skeid Fotball</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -116 )</div>
								 <div class="boxdata" >238</div>
								 <div class="boxdata" >+&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stroemmen If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -125 )</div>
								 <div class="boxdata" >401</div>
								 <div class="boxdata" >+1<br /> ( -126)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sandefjord Fotball</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -106 )</div>
								 <div class="boxdata" >-172</div>
								 <div class="boxdata" >-1<br /> ( -104)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jerv Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >222</div>
								 <div class="boxdata" >+&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ullensaker/kisa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >-102</div>
								 <div class="boxdata" >-&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nest-sotra</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >395</div>
								 <div class="boxdata" >+1<br /> ( -140)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ik Start</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-161</div>
								 <div class="boxdata" >-1<br /> ( +106)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	