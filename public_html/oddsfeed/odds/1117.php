	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Cfb - Contest Player Props">
			<caption>Cfb - Contest Player Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated January 7, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center">
					Cfb - Contest Player Props  - Jan 07					</th>
			</tr>
            
	<tr><th colspan="3" class="center">Clemson Vs Alabama - Player To Score 1st Touchdown</th></tr><tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Damien Harris (alabama)</td><td>6/1</td><td>+600</td></tr><tr><td>Travis Etienne (clemson)</td><td>13/2</td><td>+650</td></tr><tr><td>Jerry Jeudy (alabama)</td><td>8/1</td><td>+800</td></tr><tr><td>Josh Jacobs (alabama)</td><td>8/1</td><td>+800</td></tr><tr><td>Henry Ruggs Iii (alabama)</td><td>10/1</td><td>+1000</td></tr><tr><td>Justyn Ross (clemson)</td><td>12/1</td><td>+1200</td></tr><tr><td>Tee Higgins (clemson)</td><td>12/1</td><td>+1200</td></tr><tr><td>Irv Smith Jr (alabama)</td><td>18/1</td><td>+1800</td></tr><tr><td>Jaylen Waddle (alabama)</td><td>18/1</td><td>+1800</td></tr><tr><td>Adam Choice (clemson)</td><td>20/1</td><td>+2000</td></tr><tr><td>Amari Rodgers (clemson)</td><td>20/1</td><td>+2000</td></tr><tr><td>Devonta Smith (alabama)</td><td>20/1</td><td>+2000</td></tr><tr><td>Hunter Renfrow (clemson)</td><td>20/1</td><td>+2000</td></tr><tr><td>Najee Harris (alabama)</td><td>20/1</td><td>+2000</td></tr><tr><td>Tua Tagovailoa (alabama) - Rushing Td</td><td>20/1</td><td>+2000</td></tr><tr><td>Tavien Feaster (clemson)</td><td>25/1</td><td>+2500</td></tr><tr><td>Lyn-j Dixon (clemson)</td><td>40/1</td><td>+4000</td></tr><tr><td>Trevor Lawrence (clemson) - Rushing Td</td><td>40/1</td><td>+4000</td></tr><tr><td>No Td Scored In The Game</td><td>100/1</td><td>+10000</td></tr><tr><td>Field (any Other Player)</td><td>5/1</td><td>+500</td></tr>			</tbody>
		</table>
	</div>

	