	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Futures">
			<caption>Soccer - Futures</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Futures  - Nov 30					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Ballon D`or Winner</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Virgil Van Dijk</td><td>1/3</td><td>-300</td></tr><tr><td>Leo Messi</td><td>3/1</td><td>+300</td></tr><tr><td>Cristiano Ronaldo</td><td>25/1</td><td>+2500</td></tr><tr><td>Alisson Becker</td><td>33/1</td><td>+3300</td></tr><tr><td>Sadio Mane</td><td>33/1</td><td>+3300</td></tr><tr><td>Mohamed Salah</td><td>33/1</td><td>+3300</td></tr><tr><td>Raheem Sterling</td><td>50/1</td><td>+5000</td></tr><tr><td>Kylian Mbappe</td><td>66/1</td><td>+6600</td></tr><tr><td>Bernardo Silva</td><td>80/1</td><td>+8000</td></tr><tr><td>Eden Hazard</td><td>100/1</td><td>+10000</td></tr><tr><td>Antoine Griezmann</td><td>100/1</td><td>+10000</td></tr><tr><td>Kevin De Bruyne</td><td>100/1</td><td>+10000</td></tr><tr><td>Son Heung Min</td><td>150/1</td><td>+15000</td></tr><tr><td>Sergio Aguero</td><td>150/1</td><td>+15000</td></tr><tr><td>Christian Eriksen</td><td>175/1</td><td>+17500</td></tr><tr><td>Dusan Tadic</td><td>175/1</td><td>+17500</td></tr><tr><td>Harry Kane</td><td>175/1</td><td>+17500</td></tr><tr><td>Neymar</td><td>225/1</td><td>+22500</td></tr><tr><td>Philippe Coutinho</td><td>275/1</td><td>+27500</td></tr><tr><td>Raphael Varane</td><td>275/1</td><td>+27500</td></tr><tr><td>Roberto Firmino</td><td>275/1</td><td>+27500</td></tr><tr><td>Jadon Sancho</td><td>275/1</td><td>+27500</td></tr><tr><td>Luka Modric</td><td>275/1</td><td>+27500</td></tr><tr><td>Sergio Ramos</td><td>275/1</td><td>+27500</td></tr><tr><td>Leroy Sane</td><td>275/1</td><td>+27500</td></tr><tr><td>Robert Lewandowski</td><td>275/1</td><td>+27500</td></tr><tr><td>Paul Pogba</td><td>500/1</td><td>+50000</td></tr><tr><td>David De Gea</td><td>500/1</td><td>+50000</td></tr><tr><td>Pierre Emerick Aubameyang</td><td>500/1</td><td>+50000</td></tr><tr><td>Ousman Dembele</td><td>500/1</td><td>+50000</td></tr><tr><td>Marco Verratti</td><td>500/1</td><td>+50000</td></tr><tr><td>Manuel Neuer</td><td>500/1</td><td>+50000</td></tr><tr><td>Paulo Dybala</td><td>500/1</td><td>+50000</td></tr><tr><td>Edinson Cavani</td><td>500/1</td><td>+50000</td></tr><tr><td>Marquinhos</td><td>500/1</td><td>+50000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	