	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Cricket - Futures">
			<caption>Cricket - Futures</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Cricket - Futures  - Jun 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Cricket - Icc World Cup 2019 Winner</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Australia</td><td>7/2</td><td>+350</td></tr><tr><td>South Africa</td><td>20/1</td><td>+2000</td></tr><tr><td>India</td><td>33/10</td><td>+330</td></tr><tr><td>England</td><td>7/4</td><td>+175</td></tr><tr><td>New Zealand</td><td>8/1</td><td>+800</td></tr><tr><td>Pakistan</td><td>28/1</td><td>+2800</td></tr><tr><td>Sri Lanka</td><td>66/1</td><td>+6600</td></tr><tr><td>West Indies</td><td>9/1</td><td>+900</td></tr><tr><td>Bangladesh</td><td>33/1</td><td>+3300</td></tr><tr><td>Afghanistan</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	