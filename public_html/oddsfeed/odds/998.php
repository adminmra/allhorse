	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Kentucky Derby">
			<caption>Horses - Kentucky Derby</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Kentucky Derby  - May 05					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Kentucky Derby - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Cezanne</td><td>25/1</td><td>+2500</td></tr><tr><td>Green Light Go</td><td>25/1</td><td>+2500</td></tr><tr><td>Mo Hawk</td><td>33/1</td><td>+3300</td></tr><tr><td>Eight Rings</td><td>25/1</td><td>+2500</td></tr><tr><td>Garth</td><td>66/1</td><td>+6600</td></tr><tr><td>Dennis` Moment</td><td>33/1</td><td>+3300</td></tr><tr><td>Gouverneur Morris</td><td>25/1</td><td>+2500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	