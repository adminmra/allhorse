	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--netherlands Eerste Divisie">
			<caption>Pre--netherlands Eerste Divisie</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--netherlands Eerste Divisie  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Volendam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -131 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Telstar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -101 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jong Ajax Amsterdam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac34;<br /> ( -113 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Go Ahead Eagles</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac34;<br /> ( -116 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mvv Maastricht</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -113)</div>
								 <div class="boxdata" >172</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Den Bosch</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -117)</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Eindhoven</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -119)</div>
								 <div class="boxdata" >382</div>
								 <div class="boxdata" >+1<br /> ( -125)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >De Graafschap</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -111)</div>
								 <div class="boxdata" >-169</div>
								 <div class="boxdata" >-1<br /> ( -106)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sbv Excelsior</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -105)</div>
								 <div class="boxdata" >222</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nac Breda</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -125)</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nec Nijmegen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >-103</div>
								 <div class="boxdata" >-&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dordrecht</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >216</div>
								 <div class="boxdata" >+&frac12;<br /> ( -126 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jong Fc Utrecht</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -117)</div>
								 <div class="boxdata" >355</div>
								 <div class="boxdata" >+1<br /> ( -133)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Roda Jc Kerkrade</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -112)</div>
								 <div class="boxdata" >-161</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jong Az Alkmaar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -126)</div>
								 <div class="boxdata" >334</div>
								 <div class="boxdata" >+1<br /> ( -144)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Almere City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -105)</div>
								 <div class="boxdata" >-149</div>
								 <div class="boxdata" >-1<br /> ( +108)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--netherlands Eerste Divisie  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Top Oss</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -116 )</div>
								 <div class="boxdata" >165</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Helmond Sport</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >136</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--netherlands Eerste Divisie  - Sep 23</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Cambuur</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -125)</div>
								 <div class="boxdata" >146</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jong Psv Eindhoven</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -109)</div>
								 <div class="boxdata" >144</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	