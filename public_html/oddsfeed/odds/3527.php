	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Humana Distaff - To Win">
			<caption>Horses - Humana Distaff - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Humana Distaff - To Win  - May 04					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Humana Distaff - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Spring In The Wind</td><td>20/1</td><td>+2000</td></tr><tr><td>Spiced Perfection</td><td>33/10</td><td>+330</td></tr><tr><td>Mia Mischief</td><td>14/1</td><td>+1400</td></tr><tr><td>Talk Veuve To Me</td><td>17/2</td><td>+850</td></tr><tr><td>Amy's Challenge</td><td>9/2</td><td>+450</td></tr><tr><td>Emboldened</td><td>15/2</td><td>+750</td></tr><tr><td>Marley's Freedom</td><td>3/2</td><td>+150</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	