	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--romania Liga 2">
			<caption>Pre--romania Liga 2</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--romania Liga 2  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acs Viitorul Pandurii Targu Jiu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >451</div>
								 <div class="boxdata" >+1<br /> ( -129)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Turris-oltul Turnu Magurele</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >-175</div>
								 <div class="boxdata" >-1<br /> ( -102)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Csikszereda Miercurea Ciuc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -131 )</div>
								 <div class="boxdata" >-384</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Sportul Snagov Sa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -101 )</div>
								 <div class="boxdata" >898</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Uta Arad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >-178</div>
								 <div class="boxdata" >-1<br /> ( -108)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Pandurii Lignitul Targu Jiu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >437</div>
								 <div class="boxdata" >+1<br /> ( -121)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Dunarea 2005 Calarasi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >198</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Mioveni</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >121</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Metaloglobus Bucuresti</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac12;<br /> ( +108 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Asc Daco-getica Bucuresti</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >213</div>
								 <div class="boxdata" >+&frac12;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acs Champions Fc Arges</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >142</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Concordia Chiajna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >168</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Csm Resita</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Asu Politehnica Timisoara</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Petrolul Ploiesti</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >142</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Fc Universitatea Cluj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--romania Liga 2  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Scm Gloria Buzau</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Rapid 1923</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >-112</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--romania Liga 2  - Sep 23</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ripensia Timisoara</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >195</div>
								 <div class="boxdata" >+&frac12;<br /> ( -158 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Farul Constanta 1920</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >116</div>
								 <div class="boxdata" >-&frac12;<br /> ( +114 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	