	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Netherlands Eredivisie - To Win">
			<caption>Soccer - Netherlands Eredivisie - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Netherlands Eredivisie - To Win  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-2020 Holland Eredivisie - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Ajax</td><td>5/7</td><td>-140</td></tr><tr><td>Psv</td><td>5/4</td><td>+125</td></tr><tr><td>Feyenoord</td><td>20/1</td><td>+2000</td></tr><tr><td>Az</td><td>66/1</td><td>+6600</td></tr><tr><td>Fc Utrecht</td><td>100/1</td><td>+10000</td></tr><tr><td>Vitesse</td><td>50/1</td><td>+5000</td></tr><tr><td>Fc Groningen</td><td>500/1</td><td>+50000</td></tr><tr><td>Willem Ii</td><td>250/1</td><td>+25000</td></tr><tr><td>Heerenveen</td><td>1000/1</td><td>+100000</td></tr><tr><td>Fc Twente</td><td>100/1</td><td>+10000</td></tr><tr><td>Vvv</td><td>750/1</td><td>+75000</td></tr><tr><td>Heracles</td><td>400/1</td><td>+40000</td></tr><tr><td>Ado Den Haag</td><td>1000/1</td><td>+100000</td></tr><tr><td>Pec Zwolle</td><td>400/1</td><td>+40000</td></tr><tr><td>Fortuna Sittard</td><td>2500/1</td><td>+250000</td></tr><tr><td>Sparta Rotterdam</td><td>1000/1</td><td>+100000</td></tr><tr><td>Rkc</td><td>4500/1</td><td>+450000</td></tr><tr><td>Fc Emmen</td><td>2500/1</td><td>+250000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	