	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--montenegro 1 Cfl">
			<caption>Pre--montenegro 1 Cfl</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--montenegro 1 Cfl  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ofk Titograd</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >301</div>
								 <div class="boxdata" >+&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ofk Grbalj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >-112</div>
								 <div class="boxdata" >-&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Mladost Ljeskopolje</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >518</div>
								 <div class="boxdata" >+1<br /> ( -113)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Zeta Golubovci</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >-196</div>
								 <div class="boxdata" >-1<br /> ( -116)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Rudar Pljevlja</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >371</div>
								 <div class="boxdata" >+&frac12;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Kom</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >-136</div>
								 <div class="boxdata" >-&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Iskra Danilovgrad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >218</div>
								 <div class="boxdata" >+&frac12;<br /> ( -161 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ofk Petrovac</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >123</div>
								 <div class="boxdata" >-&frac12;<br /> ( +120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Sutjeska</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -128)</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Buducnost</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -103)</div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	