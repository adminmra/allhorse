	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Turkey - Super Lig">
			<caption>Turkey - Super Lig</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 3, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Turkey - Super Lig  - Dec 03</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Akhisar Belediye<br /> - vs - <br /> Yeni Malatyaspor</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (100)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+319</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+110 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (-120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-119</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-130</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Kasimpasa<br /> - vs - <br /> Fenerbahce</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (100)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+289</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o3-125 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (-120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-121</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u3+105</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	