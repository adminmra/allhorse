	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Callen & Delia Props">
			<caption>Callen & Delia Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Callen & Delia Props  - Sep 19					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Can Undress The Fastest With The Same Wardrobe</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Bryan Callen (Competition 4)</td><td>EV</td><td>EV</td></tr><tr><td>Chris D'elia (Competition 4)</td><td>5/7</td><td>-140</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					Callen & Delia Props  - Sep 26					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Can Whip Milk Into Cream Faster Competition 5</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Bryan Callen (Competition 5)</td><td>7/5</td><td>+140</td></tr><tr><td>Chris D’elia (Competition 5)</td><td>1/2</td><td>-200</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	