	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mlb - Regular Season Wins">
			<caption>Mlb - Regular Season Wins</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 28, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Mlb - Regular Season Wins  - Mar 28</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Arizona Diamondbacks Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o75&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Arizona Diamondbacks Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u75&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atlanta Braves Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o86&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atlanta Braves Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u86&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Baltimore Orioles Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o58&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Baltimore Orioles Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u58&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Boston Red Sox Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o94&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Boston Red Sox Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u94&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chicago Cubs Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o87&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chicago Cubs Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u87&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chicago White Sox Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o73&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chicago White Sox Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u73&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cincinnati Reds Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o78&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cincinnati Reds Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u78&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cleveland Indians Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o90&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cleveland Indians Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u90&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Colorado Rockies Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o84&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Colorado Rockies Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u84&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Detroit Tigers Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o68&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Detroit Tigers Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u68&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Houston Astros Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o96&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Houston Astros Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u96&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kansas City Royals Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o69&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kansas City Royals Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u69&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Los Angeles Angels Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o81&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Los Angeles Angels Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u81&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Los Angeles Dodgers Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o93&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Los Angeles Dodgers Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u93&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Miami Marlins Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o63&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Miami Marlins Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u63&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Milwaukee Brewers Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o86&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Milwaukee Brewers Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u86&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Minnesota Twins Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o83&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Minnesota Twins Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u83&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New York Mets Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o85&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New York Mets Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u85&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New York Yankees Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o96&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New York Yankees Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u96&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Oakland Athletics Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o83&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Oakland Athletics Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u83&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Philadelphia Phillies Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o89&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Philadelphia Phillies Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u89&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pittsburgh Pirates Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o77&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pittsburgh Pirates Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u77&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >San Diego Padres Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o78&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >San Diego Padres Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u78&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >San Francisco Giants Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o73&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >San Francisco Giants Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u73&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >St Louis Cardinals Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o88&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >St Louis Cardinals Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u88&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Seattle Mariners Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o71&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Seattle Mariners Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u71&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tampa Bay Rays Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o84&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tampa Bay Rays Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u84&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Texas Rangers Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o71&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Texas Rangers Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u71&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Toronto Blue Jays Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o75&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Toronto Blue Jays Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u75&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Washington Nationals Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o88&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Washington Nationals Rsw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u88&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	