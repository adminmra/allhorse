	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Spanish La Liga - To Win">
			<caption>Soccer - Spanish La Liga - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Spanish La Liga - To Win  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-2020 Primera Liga - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Barcelona</td><td>20/33</td><td>-165</td></tr><tr><td>Real Madrid</td><td>9/5</td><td>+180</td></tr><tr><td>Atletico Madrid</td><td>9/1</td><td>+900</td></tr><tr><td>Valencia</td><td>200/1</td><td>+20000</td></tr><tr><td>Sevilla</td><td>66/1</td><td>+6600</td></tr><tr><td>Villarreal</td><td>350/1</td><td>+35000</td></tr><tr><td>Getafe</td><td>500/1</td><td>+50000</td></tr><tr><td>Real Betis</td><td>500/1</td><td>+50000</td></tr><tr><td>Athletic Bilbao</td><td>150/1</td><td>+15000</td></tr><tr><td>Real Sociedad</td><td>750/1</td><td>+75000</td></tr><tr><td>Espanyol</td><td>1000/1</td><td>+100000</td></tr><tr><td>Celta Vigo</td><td>1000/1</td><td>+100000</td></tr><tr><td>Eibar</td><td>1000/1</td><td>+100000</td></tr><tr><td>Cd Alaves</td><td>2000/1</td><td>+200000</td></tr><tr><td>Valladolid</td><td>2500/1</td><td>+250000</td></tr><tr><td>Leganes</td><td>2500/1</td><td>+250000</td></tr><tr><td>Osasuna</td><td>2500/1</td><td>+250000</td></tr><tr><td>Levante</td><td>1500/1</td><td>+150000</td></tr><tr><td>Granada</td><td>3000/1</td><td>+300000</td></tr><tr><td>Mallorca</td><td>4500/1</td><td>+450000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	