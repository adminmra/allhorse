	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="England - Premier League - 1h">
			<caption>England - Premier League - 1h</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated November 30, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">England - Premier League - 1h First Half Lines - Nov 30</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">1h Wolverhampton Wanderers<br /> - vs - <br /> 1h Cardiff City</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-170)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+184</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o1+125 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (145)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+336</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u1-145</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	