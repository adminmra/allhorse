	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Aaf 1h">
			<caption>Aaf 1h</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 31, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Aaf 1h First Half Lines - Mar 31</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Atlanta Legends</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o19<br /> ( -110)</div>
								 <div class="boxdata" >155</div>
								 <div class="boxdata" >+3<br /> ( -105)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Birmingham Iron</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u19<br /> ( -110)</div>
								 <div class="boxdata" >-195</div>
								 <div class="boxdata" >-3<br /> ( -115)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Arizona Hotshots</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o21&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >+&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h San Antonio Commanders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u21&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-125</div>
								 <div class="boxdata" >-&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	