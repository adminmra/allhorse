	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Canadian Football - 1h">
			<caption>Canadian Football - 1h</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Canadian Football - 1h First Half Lines - Jul 04</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Hamilton Tigercats</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o28&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-340</div>
								 <div class="boxdata" >-7<br /> ( -110)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Montreal Alouettes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u28&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >270</div>
								 <div class="boxdata" >+7<br /> ( -110)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Canadian Football - 1h First Half Lines - Jul 05</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Winnipeg Blue Bombers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o25&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" >+2&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Ottawa Redblacks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u25&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-160</div>
								 <div class="boxdata" >-2&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Canadian Football - 1h First Half Lines - Jul 06</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h B.c. Lions</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o27&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-200</div>
								 <div class="boxdata" >-3&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Toronto Argonauts</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u27&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >170</div>
								 <div class="boxdata" >+3&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Calgary Stampeders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o24&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" >+2&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Saskatchewan Roughriders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u24&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-165</div>
								 <div class="boxdata" >-2&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	