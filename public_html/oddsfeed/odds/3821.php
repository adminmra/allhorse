	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--estonia Esiliiga">
			<caption>Pre--estonia Esiliiga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--estonia Esiliiga  - Sep 19</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tallinna Fc Flora U21</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac34;<br /> ( -125 )</div>
								 <div class="boxdata" >453</div>
								 <div class="boxdata" >+1<br /> ( +108)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tallinna Jk Legion</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac34;<br /> ( -106 )</div>
								 <div class="boxdata" >-208</div>
								 <div class="boxdata" >-1<br /> ( -144)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kohtla-jarve Jk Jarve</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >501</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tartu Jk Tammeka U21</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >-277</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jk Tallinna Kalev U21</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >421</div>
								 <div class="boxdata" >+1<br /> ( +106)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fci Levadia Tallinn U21</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac34;<br /> ( -117 )</div>
								 <div class="boxdata" >-208</div>
								 <div class="boxdata" >-1<br /> ( -140)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Elva</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -111)</div>
								 <div class="boxdata" >163</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Parnu Jk Vaprus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -119)</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	