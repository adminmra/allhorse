	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Sb 53 - Coin Toss">
			<caption>Sb 53 - Coin Toss</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Sb 53 - Coin Toss  - Feb 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Sb 53 - Coin Toss</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Coin Will Land Heads</td><td>50/51</td><td>-102</td></tr><tr><td>Coin Will Land Tails</td><td>50/51</td><td>-102</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Sb 53 - Team To Win The Opening Coin Toss</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Patriots Win Coin Toss</td><td>20/21</td><td>-105</td></tr><tr><td>Rams Win Coin Toss</td><td>20/21</td><td>-105</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Sb 53 - Will The Winner Of The Coin Toss Win Sb 53</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Winner Coin Toss Win Sb - Yes</td><td>20/21</td><td>-105</td></tr><tr><td>Winner Coin Toss Win Sb - No</td><td>20/21</td><td>-105</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Sb 53 - Team Or Player To Call Coin Toss Will Be</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Correct Call</td><td>20/21</td><td>-105</td></tr><tr><td>Wrong Call</td><td>20/21</td><td>-105</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');</script>
{/literal}	
	