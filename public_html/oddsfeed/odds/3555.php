	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--tennis Player To Win A Set">
			<caption>Pre--tennis Player To Win A Set</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--tennis Player To Win A Set  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >227</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-344</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >160</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-227</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >323</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-526</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-158</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-138</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >488</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-999</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >258</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-400</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-222</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-108</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-126</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >384</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-714</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >544</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-1249</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-196</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >256</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-400</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >209</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-312</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >167</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-238</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >212</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-312</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >195</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-285</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >403</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-769</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >448</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-909</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-128</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-106</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-212</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >254</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-400</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-125</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-114</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >224</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-357</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-1428</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >548</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >403</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-833</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >166</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-250</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >300</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-526</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-181</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >133</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-196</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >237</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-384</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >189</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-294</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-102</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-140</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >183</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-277</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >429</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-909</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-178</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >725</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-3333</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >184</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-285</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >334</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-625</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >270</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-454</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-256</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >171</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >188</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-285</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-114</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-125</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >256</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-400</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >159</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-227</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-384</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-217</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >276</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-434</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-212</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-158</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >547</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-1428</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >263</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-416</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-196</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >388</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-769</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >133</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-196</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-106</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >281</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-476</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >397</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-769</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-108</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-131</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >295</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-500</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-178</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-243</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >213</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-312</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-188</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-121</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >277</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-476</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >411</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-833</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >237</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-384</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >696</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-2499</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-222</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >148</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-232</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >661</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-2499</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-196</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >262</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-434</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-188</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-123</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >436</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-909</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-129</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-111</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >421</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-909</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >147</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-217</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >227</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-357</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-192</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >238</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-384</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-188</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >244</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-400</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >170</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-243</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >212</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-312</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-212</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >238</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-357</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-263</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >211</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-333</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >295</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-555</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-163</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-312</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >212</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >865</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-4999</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-322</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >824</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-4999</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--tennis Player To Win A Set  - Sep 19</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >248</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-434</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-196</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >471</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-999</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-102</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >311</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-500</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-131</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >450</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-909</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >209</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-312</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >165</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-238</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-277</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >191</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-277</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >189</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-303</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >171</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-270</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >291</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-476</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-250</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >161</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-151</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >711</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-4999</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >511</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-1428</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >216</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-344</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-256</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >560</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-1428</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-163</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >379</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-833</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-142</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-384</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-270</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >296</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-555</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >703</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-2499</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >428</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-909</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-217</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >146</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-104</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-138</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >133</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >260</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-454</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-196</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >262</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-476</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-250</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-303</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-416</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-129</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-111</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >454</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-999</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >338</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-666</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >143</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-222</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-384</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >354</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-666</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-151</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >121</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >300</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-526</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >396</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-909</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-104</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-142</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >117</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >275</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-500</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >293</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-500</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >209</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-333</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >282</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-476</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >224</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-384</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-270</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-192</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-256</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-172</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-270</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >304</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-526</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >208</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-322</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-232</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >305</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-588</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-434</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >121</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >278</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-500</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-172</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >487</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-1249</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >136</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-208</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >188</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-285</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-149</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >246</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-434</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-294</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	