	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - The Open Championship">
			<caption>Golf - The Open Championship</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - The Open Championship  - Jul 16					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 The Open Championship - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Brooks Koepka</td><td>10/1</td><td>+1000</td></tr><tr><td>Rory Mcilroy</td><td>11/1</td><td>+1100</td></tr><tr><td>Dustin Johnson</td><td>14/1</td><td>+1400</td></tr><tr><td>Jon Rahm</td><td>18/1</td><td>+1800</td></tr><tr><td>Justin Rose</td><td>18/1</td><td>+1800</td></tr><tr><td>Tommy Fleetwood</td><td>22/1</td><td>+2200</td></tr><tr><td>Francesco Molinari</td><td>25/1</td><td>+2500</td></tr><tr><td>Justin Thomas</td><td>25/1</td><td>+2500</td></tr><tr><td>Rickie Fowler</td><td>25/1</td><td>+2500</td></tr><tr><td>Xander Schauffele</td><td>28/1</td><td>+2800</td></tr><tr><td>Jordan Spieth</td><td>28/1</td><td>+2800</td></tr><tr><td>Tiger Woods</td><td>30/1</td><td>+3000</td></tr><tr><td>Patrick Cantlay</td><td>33/1</td><td>+3300</td></tr><tr><td>Adam Scott</td><td>33/1</td><td>+3300</td></tr><tr><td>Henrik Stenson</td><td>33/1</td><td>+3300</td></tr><tr><td>Jason Day</td><td>35/1</td><td>+3500</td></tr><tr><td>Hideki Matsuyama</td><td>35/1</td><td>+3500</td></tr><tr><td>Bryson Dechambeau</td><td>40/1</td><td>+4000</td></tr><tr><td>Shane Lowry</td><td>45/1</td><td>+4500</td></tr><tr><td>Louis Oosthuizen</td><td>50/1</td><td>+5000</td></tr><tr><td>Paul Casey</td><td>50/1</td><td>+5000</td></tr><tr><td>Matthew Fitzpatrick</td><td>55/1</td><td>+5500</td></tr><tr><td>Sergio Garcia</td><td>55/1</td><td>+5500</td></tr><tr><td>Danny Willett</td><td>66/1</td><td>+6600</td></tr><tr><td>Tony Finau</td><td>66/1</td><td>+6600</td></tr><tr><td>Tyrrell Hatton</td><td>66/1</td><td>+6600</td></tr><tr><td>Ian Poulter</td><td>66/1</td><td>+6600</td></tr><tr><td>Matt Kuchar</td><td>66/1</td><td>+6600</td></tr><tr><td>Eddie Pepperell</td><td>70/1</td><td>+7000</td></tr><tr><td>Gary Woodland</td><td>80/1</td><td>+8000</td></tr><tr><td>Rafael Cabrera Bello</td><td>80/1</td><td>+8000</td></tr><tr><td>Marc Leishman</td><td>80/1</td><td>+8000</td></tr><tr><td>Webb Simpson</td><td>80/1</td><td>+8000</td></tr><tr><td>Lee Westwood</td><td>80/1</td><td>+8000</td></tr><tr><td>Phil Mickelson</td><td>100/1</td><td>+10000</td></tr><tr><td>Hao Tong Li</td><td>100/1</td><td>+10000</td></tr><tr><td>Alex Noren</td><td>100/1</td><td>+10000</td></tr><tr><td>Thomas Pieters</td><td>125/1</td><td>+12500</td></tr><tr><td>Thorbjorn Olesen</td><td>125/1</td><td>+12500</td></tr><tr><td>Bubba Watson</td><td>125/1</td><td>+12500</td></tr><tr><td>Erik Van Rooyen</td><td>125/1</td><td>+12500</td></tr><tr><td>Zach Johnson</td><td>125/1</td><td>+12500</td></tr><tr><td>Branden Grace</td><td>125/1</td><td>+12500</td></tr><tr><td>Brandt Snedeker</td><td>125/1</td><td>+12500</td></tr><tr><td>Andy Sullivan</td><td>150/1</td><td>+15000</td></tr><tr><td>Padraig Harrington</td><td>200/1</td><td>+20000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	