	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - European Tour - 72 Hole Match Betting">
			<caption>Golf - European Tour - 72 Hole Match Betting</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - European Tour - 72 Hole Match Betting  - Sep 19					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Alexander Bjork</td><td>10/11</td><td>-110</td></tr><tr><td>Marcus Kinhult</td><td>10/11</td><td>-110</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tony Finau</td><td>10/11</td><td>-110</td></tr><tr><td>Viktor Hovland</td><td>10/11</td><td>-110</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Matthew Fitzpatrick </td><td>4/5</td><td>-125</td></tr><tr><td>Francesco Molinari</td><td>21/20</td><td>+105</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tommy Fleetwood</td><td>5/6</td><td>-120</td></tr><tr><td>Henrik Stenson</td><td>EV</td><td>EV</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Branden Grace</td><td>10/11</td><td>-110</td></tr><tr><td>Ross Fisher</td><td>10/11</td><td>-110</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tyrell Hatton</td><td>10/11</td><td>-110</td></tr><tr><td>Rafa Cabrera Bello</td><td>10/11</td><td>-110</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Billy Horschel</td><td>10/11</td><td>-110</td></tr><tr><td>Ian Poulter</td><td>10/11</td><td>-110</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Shane Lowry</td><td>10/11</td><td>-110</td></tr><tr><td>Mat Wallace</td><td>10/11</td><td>-110</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o8t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Joost Luiten</td><td>10/11</td><td>-110</td></tr><tr><td>Haotong Li</td><td>10/11</td><td>-110</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o9t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Rory Mcllroy</td><td>2/3</td><td>-150</td></tr><tr><td>Jon Rahm</td><td>5/4</td><td>+125</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o10t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Thomas Pieters</td><td>5/6</td><td>-120</td></tr><tr><td>Danny Willet</td><td>EV</td><td>EV</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o11t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Bernd Wiesberger</td><td>10/11</td><td>-110</td></tr><tr><td>Erik Van Rooyen</td><td>10/11</td><td>-110</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');addSort('o6t');addSort('o7t');addSort('o8t');addSort('o9t');addSort('o10t');addSort('o11t');</script>
{/literal}	
	