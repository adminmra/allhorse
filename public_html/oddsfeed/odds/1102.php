	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Ent - Movies">
			<caption>Ent - Movies</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Ent - Movies  - Dec 31					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Be The Green Lantern In The Upcoming Movie</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tom Cruise</td><td>13/10</td><td>+130</td></tr><tr><td>Common</td><td>2/1</td><td>+200</td></tr><tr><td>Bradley Cooper</td><td>3/1</td><td>+300</td></tr><tr><td>Joel Mchale</td><td>4/1</td><td>+400</td></tr><tr><td>Idris Elba</td><td>5/1</td><td>+500</td></tr><tr><td>Alex O’loughlin</td><td>8/1</td><td>+800</td></tr><tr><td>Jake Gyllenhaal</td><td>20/1</td><td>+2000</td></tr><tr><td>Field</td><td>3/1</td><td>+300</td></tr><tr><td>Armie Hammer</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					Ent - Movies  - Dec 31					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Next Superman Actor In Any Dc Comics Movie</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Michael B. Jordan</td><td>3/2</td><td>+150</td></tr><tr><td>Tyler Hoechlin</td><td>5/4</td><td>+125</td></tr><tr><td>Armie Hammer</td><td>3/1</td><td>+300</td></tr><tr><td>Garrett Hedlund</td><td>4/1</td><td>+400</td></tr><tr><td>Idris Elba</td><td>13/2</td><td>+650</td></tr><tr><td>Wes Bentley</td><td>13/2</td><td>+650</td></tr><tr><td>Oscar Isaac</td><td>15/2</td><td>+750</td></tr><tr><td>Taron Edgerton</td><td>8/1</td><td>+800</td></tr><tr><td>Ryan Gosling</td><td>15/1</td><td>+1500</td></tr><tr><td>Benjamin Walker</td><td>22/1</td><td>+2200</td></tr><tr><td>Dwayne ‘ The Rock’ Johnson</td><td>25/1</td><td>+2500</td></tr><tr><td>Mark Wahlberg</td><td>30/1</td><td>+3000</td></tr><tr><td>Chris Evans</td><td>40/1</td><td>+4000</td></tr><tr><td>Nicholas Cage</td><td>50/1</td><td>+5000</td></tr><tr><td>Ben Affleck</td><td>50/1</td><td>+5000</td></tr><tr><td>Will Smith</td><td>100/1</td><td>+10000</td></tr><tr><td>Field</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	