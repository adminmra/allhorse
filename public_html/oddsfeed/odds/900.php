	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Cbb - Contest Game Props">
			<caption>Cbb - Contest Game Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 31, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Cbb - Contest Game Props  - Mar 31					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Auburn Vs Kentucky  - Race To 20 Points</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Auburn Sc 1st 20 Pts</td><td>29/20</td><td>+145</td></tr><tr><td>Kentucky  Sc 1st 20 Pts</td><td>4/7</td><td>-175</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Auburn Vs Kentucky  - Double Result</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Auburn 1h - Auburn Ft</td><td>14/5</td><td>+280</td></tr><tr><td>Tie - Auburn Ft</td><td>25/1</td><td>+2500</td></tr><tr><td>Kentucky  1h - Auburn Ft</td><td>13/2</td><td>+650</td></tr><tr><td>Auburn 1h - Kentucky  Ft</td><td>5/1</td><td>+500</td></tr><tr><td>Tie - Kentucky  Ft</td><td>20/1</td><td>+2000</td></tr><tr><td>Kentucky  1h - Kentucky  Ft</td><td>10/11</td><td>-110</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Auburn Vs Kentucky  - Winning Margin</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Auburn By 1-2 Pts</td><td>11/1</td><td>+1100</td></tr><tr><td>Auburn By 3-6 Pts</td><td>13/2</td><td>+650</td></tr><tr><td>Auburn By 7-9 Pts</td><td>12/1</td><td>+1200</td></tr><tr><td>Auburn By 10-13 Pts</td><td>14/1</td><td>+1400</td></tr><tr><td>Auburn By 14-16 Pts</td><td>25/1</td><td>+2500</td></tr><tr><td>Auburn By 17-20 Pts</td><td>25/1</td><td>+2500</td></tr><tr><td>Auburn By 21 Or More Pts</td><td>25/1</td><td>+2500</td></tr><tr><td>Kentucky  By 1-2 Pts</td><td>10/1</td><td>+1000</td></tr><tr><td>Kentucky  By 3-6 Pts</td><td>5/1</td><td>+500</td></tr><tr><td>Kentucky  By 7-9 Pts</td><td>7/1</td><td>+700</td></tr><tr><td>Kentucky  By 10-13 Pts</td><td>13/2</td><td>+650</td></tr><tr><td>Kentucky  By 14-16 Pts</td><td>11/1</td><td>+1100</td></tr><tr><td>Kentucky  By 17-20 Pts</td><td>11/1</td><td>+1100</td></tr><tr><td>Kentucky  By 21 Or More Pts</td><td>7/1</td><td>+700</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Michigan State Vs Duke  - Race To 20 Points</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Michigan State Sc 1st 20 Pts</td><td>EV</td><td>EV</td></tr><tr><td>Duke  Sc 1st 20 Pts</td><td>10/13</td><td>-130</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Michigan State Vs Duke  - Double Result</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Michigan State 1h - Michigan State Ft</td><td>2/1</td><td>+200</td></tr><tr><td>Tie - Michigan State Ft</td><td>22/1</td><td>+2200</td></tr><tr><td>Duke  1h - Michigan State Ft</td><td>11/2</td><td>+550</td></tr><tr><td>Michigan State 1h - Duke  Ft</td><td>11/2</td><td>+550</td></tr><tr><td>Tie - Duke  Ft</td><td>20/1</td><td>+2000</td></tr><tr><td>Duke  1h - Duke  Ft</td><td>13/10</td><td>+130</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Michigan State Vs Duke  - Winning Margin</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Michigan State By 1-2 Pts</td><td>10/1</td><td>+1000</td></tr><tr><td>Michigan State By 3-6 Pts</td><td>11/2</td><td>+550</td></tr><tr><td>Michigan State By 7-9 Pts</td><td>10/1</td><td>+1000</td></tr><tr><td>Michigan State By 10-13 Pts</td><td>10/1</td><td>+1000</td></tr><tr><td>Michigan State By 14-16 Pts</td><td>18/1</td><td>+1800</td></tr><tr><td>Michigan State By 17-20 Pts</td><td>20/1</td><td>+2000</td></tr><tr><td>Michigan State By 21 Or More Pts</td><td>14/1</td><td>+1400</td></tr><tr><td>Duke  By 1-2 Pts</td><td>10/1</td><td>+1000</td></tr><tr><td>Duke  By 3-6 Pts</td><td>5/1</td><td>+500</td></tr><tr><td>Duke  By 7-9 Pts</td><td>8/1</td><td>+800</td></tr><tr><td>Duke  By 10-13 Pts</td><td>15/2</td><td>+750</td></tr><tr><td>Duke  By 14-16 Pts</td><td>14/1</td><td>+1400</td></tr><tr><td>Duke  By 17-20 Pts</td><td>14/1</td><td>+1400</td></tr><tr><td>Duke  By 21 Or More Pts</td><td>10/1</td><td>+1000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');</script>
{/literal}	
	