	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - English Premier League - To Win">
			<caption>Soccer - English Premier League - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - English Premier League - To Win  - Sep 21					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-2020  England Premier League - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Man City</td><td>2/3</td><td>-150</td></tr><tr><td>Liverpool</td><td>3/2</td><td>+150</td></tr><tr><td>Tottenham</td><td>40/1</td><td>+4000</td></tr><tr><td>Man Utd</td><td>100/1</td><td>+10000</td></tr><tr><td>Arsenal</td><td>80/1</td><td>+8000</td></tr><tr><td>Chelsea</td><td>80/1</td><td>+8000</td></tr><tr><td>Everton</td><td>750/1</td><td>+75000</td></tr><tr><td>Wolverhampton</td><td>1000/1</td><td>+100000</td></tr><tr><td>Leicester</td><td>300/1</td><td>+30000</td></tr><tr><td>West Ham</td><td>1000/1</td><td>+100000</td></tr><tr><td>Crystal Palace</td><td>2000/1</td><td>+200000</td></tr><tr><td>Southampton</td><td>1500/1</td><td>+150000</td></tr><tr><td>Watford</td><td>2000/1</td><td>+200000</td></tr><tr><td>Newcastle</td><td>3000/1</td><td>+300000</td></tr><tr><td>Bournemouth</td><td>1500/1</td><td>+150000</td></tr><tr><td>Brighton</td><td>2000/1</td><td>+200000</td></tr><tr><td>Burnley</td><td>2000/1</td><td>+200000</td></tr><tr><td>Norwich</td><td>3000/1</td><td>+300000</td></tr><tr><td>Sheff Utd</td><td>4500/1</td><td>+450000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	