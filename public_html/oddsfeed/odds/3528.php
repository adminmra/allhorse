	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Churchill Distaff Turf Mile - To Win">
			<caption>Horses - Churchill Distaff Turf Mile - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Churchill Distaff Turf Mile - To Win  - May 04					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Churchill Distaff Turf Mile - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Raven's Lady</td><td>14/1</td><td>+1400</td></tr><tr><td>Take These Chains</td><td>16/1</td><td>+1600</td></tr><tr><td>Proctor's Ledge</td><td>11/2</td><td>+550</td></tr><tr><td>Beau Recall</td><td>10/1</td><td>+1000</td></tr><tr><td>Daddy Is A Legend</td><td>9/2</td><td>+450</td></tr><tr><td>Precieuse</td><td>11/4</td><td>+275</td></tr><tr><td>Capla Temptress</td><td>11/2</td><td>+550</td></tr><tr><td>Got Stormy</td><td>14/1</td><td>+1400</td></tr><tr><td>Environs</td><td>6/1</td><td>+600</td></tr><tr><td>Valedictorian</td><td>11/1</td><td>+1100</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	