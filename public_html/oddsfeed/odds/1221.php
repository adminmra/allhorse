	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Uefa Champions League Specials">
			<caption>Soccer - Uefa Champions League Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 31, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Uefa Champions League Specials  - Jun 01					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Uefa Champions League - Double Result</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tottenham - Tottenham</td><td>11/2</td><td>+550</td></tr><tr><td>Tottenham - Draw</td><td>14/1</td><td>+1400</td></tr><tr><td>Tottenham - Liverpool</td><td>22/1</td><td>+2200</td></tr><tr><td>Draw - Tottenham</td><td>15/2</td><td>+750</td></tr><tr><td>Draw - Draw</td><td>9/2</td><td>+450</td></tr><tr><td>Draw - Liverpool</td><td>15/4</td><td>+375</td></tr><tr><td>Liverpool - Tottenham</td><td>33/1</td><td>+3300</td></tr><tr><td>Liverpool - Draw</td><td>14/1</td><td>+1400</td></tr><tr><td>Liverpool - Liverpool</td><td>2/1</td><td>+200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Uefa Champions League - Method Of Victory</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tottenham In 90 Mins</td><td>13/5</td><td>+260</td></tr><tr><td>Tottenham In Extra Time</td><td>14/1</td><td>+1400</td></tr><tr><td>Tottenham In Penalties</td><td>12/1</td><td>+1200</td></tr><tr><td>Liverpool In 90 Mins</td><td>EV</td><td>EV</td></tr><tr><td>Liverpool In Extra Time</td><td>17/2</td><td>+850</td></tr><tr><td>Liverpool In Penalties</td><td>12/1</td><td>+1200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Uefa Champions League - Correct Score</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tottenham 1 - 0</td><td>11/1</td><td>+1100</td></tr><tr><td>Tottenham 2 - 0</td><td>20/1</td><td>+2000</td></tr><tr><td>Tottenham 2 - 1</td><td>12/1</td><td>+1200</td></tr><tr><td>Tottenham 3 - 0</td><td>40/1</td><td>+4000</td></tr><tr><td>Tottenham 3 - 1</td><td>28/1</td><td>+2800</td></tr><tr><td>Tottenham 3 - 2</td><td>33/1</td><td>+3300</td></tr><tr><td>Tottenham 4 - 0</td><td>100/1</td><td>+10000</td></tr><tr><td>Tottenham 4 - 1</td><td>66/1</td><td>+6600</td></tr><tr><td>Tottenham 4 - 2</td><td>80/1</td><td>+8000</td></tr><tr><td>Tottenham 4 - 3</td><td>125/1</td><td>+12500</td></tr><tr><td>Tottenham 5 - 0</td><td>350/1</td><td>+35000</td></tr><tr><td>Tottenham 5 - 1</td><td>200/1</td><td>+20000</td></tr><tr><td>Tottenham 5 - 2</td><td>250/1</td><td>+25000</td></tr><tr><td>Tottenham 5 - 3</td><td>500/1</td><td>+50000</td></tr><tr><td>Draw 0 - 0</td><td>10/1</td><td>+1000</td></tr><tr><td>Draw 1 - 1</td><td>11/2</td><td>+550</td></tr><tr><td>Draw 2 - 2</td><td>12/1</td><td>+1200</td></tr><tr><td>Draw 3 - 3</td><td>40/1</td><td>+4000</td></tr><tr><td>Draw 4 - 4</td><td>250/1</td><td>+25000</td></tr><tr><td>Liverpool 1 - 0</td><td>7/1</td><td>+700</td></tr><tr><td>Liverpool 2 - 0</td><td>17/2</td><td>+850</td></tr><tr><td>Liverpool 2 - 1</td><td>15/2</td><td>+750</td></tr><tr><td>Liverpool 3 - 0</td><td>16/1</td><td>+1600</td></tr><tr><td>Liverpool 3 - 1</td><td>14/1</td><td>+1400</td></tr><tr><td>Liverpool 3 - 2</td><td>22/1</td><td>+2200</td></tr><tr><td>Liverpool 4 - 0</td><td>33/1</td><td>+3300</td></tr><tr><td>Liverpool 4 - 1</td><td>33/1</td><td>+3300</td></tr><tr><td>Liverpool 4 - 2</td><td>40/1</td><td>+4000</td></tr><tr><td>Liverpool 4 - 3</td><td>80/1</td><td>+8000</td></tr><tr><td>Liverpool 5 - 0</td><td>66/1</td><td>+6600</td></tr><tr><td>Liverpool 5 - 1</td><td>66/1</td><td>+6600</td></tr><tr><td>Liverpool 5 - 2</td><td>100/1</td><td>+10000</td></tr><tr><td>Liverpool 5 - 3</td><td>200/1</td><td>+20000</td></tr><tr><td>Liverpool 6 - 0</td><td>200/1</td><td>+20000</td></tr><tr><td>Liverpool 6 - 1</td><td>150/1</td><td>+15000</td></tr><tr><td>Liverpool 6 - 2</td><td>300/1</td><td>+30000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Uefa Champions League - Winning Margin</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tottenham By 1 Goal</td><td>9/2</td><td>+450</td></tr><tr><td>Tottenham By 2 Goals</td><td>11/1</td><td>+1100</td></tr><tr><td>Tottenham By 3 Goals</td><td>33/1</td><td>+3300</td></tr><tr><td>Tottenham By 4 Or More Goals</td><td>66/1</td><td>+6600</td></tr><tr><td>Liverpool By 1 Goal</td><td>11/4</td><td>+275</td></tr><tr><td>Liverpool By 2 Goals</td><td>9/2</td><td>+450</td></tr><tr><td>Liverpool By 3 Goals</td><td>10/1</td><td>+1000</td></tr><tr><td>Liverpool By 4 Or More Goals</td><td>18/1</td><td>+1800</td></tr><tr><td>Score Draw</td><td>15/4</td><td>+375</td></tr><tr><td>No Goal</td><td>10/1</td><td>+1000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');</script>
{/literal}	
	