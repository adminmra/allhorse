	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--cricket Overs 0 To X - Team Run Range">
			<caption>Live--cricket Overs 0 To X - Team Run Range</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Live--cricket Overs 0 To X - Team Run Range  - Jul 04					</th>
			</tr>
            
	<tr><th colspan="3" class="center"></th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>0-54</td><td>28/1</td><td>+2800</td></tr><tr><td>55-59</td><td>39/10</td><td>+390</td></tr><tr><td>60-64</td><td>8/5</td><td>+160</td></tr><tr><td>65-69</td><td>7/4</td><td>+175</td></tr><tr><td>70-74</td><td>69/20</td><td>+345</td></tr><tr><td>75-79</td><td>9/1</td><td>+900</td></tr><tr><td>80-84</td><td>31/1</td><td>+3100</td></tr><tr><td>85-89</td><td>49/1</td><td>+4900</td></tr><tr><td>90+</td><td>49/1</td><td>+4900</td></tr></tbody></table></td></tr><tr><th colspan="3" class="center"></th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>0-59</td><td>49/1</td><td>+4900</td></tr><tr><td>60-69</td><td>39/1</td><td>+3900</td></tr><tr><td>70-79</td><td>309/100</td><td>+309</td></tr><tr><td>80-89</td><td>26/25</td><td>+104</td></tr><tr><td>90-99</td><td>8/5</td><td>+160</td></tr><tr><td>100-109</td><td>49/10</td><td>+490</td></tr><tr><td>110-119</td><td>26/1</td><td>+2600</td></tr><tr><td>120-129</td><td>49/1</td><td>+4900</td></tr><tr><td>130+</td><td>49/1</td><td>+4900</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	