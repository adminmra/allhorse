	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Eurocopa - To Win">
			<caption>Soccer - Eurocopa - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Eurocopa - To Win  - Jun 12					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Eurocopa - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>France</td><td>33/10</td><td>+330</td></tr><tr><td>Spain</td><td>7/1</td><td>+700</td></tr><tr><td>Belgium</td><td>7/1</td><td>+700</td></tr><tr><td>England</td><td>9/2</td><td>+450</td></tr><tr><td>Germany</td><td>7/1</td><td>+700</td></tr><tr><td>Portugal</td><td>16/1</td><td>+1600</td></tr><tr><td>Holland</td><td>9/1</td><td>+900</td></tr><tr><td>Italy</td><td>16/1</td><td>+1600</td></tr><tr><td>Croatia</td><td>25/1</td><td>+2500</td></tr><tr><td>Switzerland</td><td>80/1</td><td>+8000</td></tr><tr><td>Wales</td><td>150/1</td><td>+15000</td></tr><tr><td>Russia</td><td>80/1</td><td>+8000</td></tr><tr><td>Denmark</td><td>66/1</td><td>+6600</td></tr><tr><td>Serbia</td><td>80/1</td><td>+8000</td></tr><tr><td>Poland</td><td>80/1</td><td>+8000</td></tr><tr><td>Sweden</td><td>100/1</td><td>+10000</td></tr><tr><td>Ukraine</td><td>66/1</td><td>+6600</td></tr><tr><td>Austria</td><td>100/1</td><td>+10000</td></tr><tr><td>Turkey</td><td>100/1</td><td>+10000</td></tr><tr><td>Czech Republic</td><td>150/1</td><td>+15000</td></tr><tr><td>Bosnia-herzegovina</td><td>200/1</td><td>+20000</td></tr><tr><td>Iceland</td><td>250/1</td><td>+25000</td></tr><tr><td>Rep Of Ireland</td><td>200/1</td><td>+20000</td></tr><tr><td>Slovakia</td><td>150/1</td><td>+15000</td></tr><tr><td>Norway</td><td>100/1</td><td>+10000</td></tr><tr><td>Romania</td><td>150/1</td><td>+15000</td></tr><tr><td>Hungary</td><td>250/1</td><td>+25000</td></tr><tr><td>Greece</td><td>200/1</td><td>+20000</td></tr><tr><td>Slovenia</td><td>200/1</td><td>+20000</td></tr><tr><td>Northern Ireland</td><td>500/1</td><td>+50000</td></tr><tr><td>Israel</td><td>250/1</td><td>+25000</td></tr><tr><td>Scotland</td><td>500/1</td><td>+50000</td></tr><tr><td>Montenegro</td><td>2000/1</td><td>+200000</td></tr><tr><td>Finland</td><td>250/1</td><td>+25000</td></tr><tr><td>Bulgaria</td><td>1500/1</td><td>+150000</td></tr><tr><td>Albania</td><td>1000/1</td><td>+100000</td></tr><tr><td>Armenia</td><td>2000/1</td><td>+200000</td></tr><tr><td>Cyprus</td><td>1000/1</td><td>+100000</td></tr><tr><td>Belarus</td><td>1500/1</td><td>+150000</td></tr><tr><td>Azerbaijan</td><td>4500/1</td><td>+450000</td></tr><tr><td>Kosovo</td><td>1500/1</td><td>+150000</td></tr><tr><td>Latvia</td><td>4500/1</td><td>+450000</td></tr><tr><td>Moldova</td><td>4500/1</td><td>+450000</td></tr><tr><td>Kazakhstan</td><td>2500/1</td><td>+250000</td></tr><tr><td>Estonia</td><td>4500/1</td><td>+450000</td></tr><tr><td>Malta</td><td>4500/1</td><td>+450000</td></tr><tr><td>Faroe Islands</td><td>4500/1</td><td>+450000</td></tr><tr><td>Lithuania</td><td>4500/1</td><td>+450000</td></tr><tr><td>Georgia</td><td>4500/1</td><td>+450000</td></tr><tr><td>Luxembourg</td><td>2500/1</td><td>+250000</td></tr><tr><td>Liechtenstein</td><td>4500/1</td><td>+450000</td></tr><tr><td>Andorra</td><td>4500/1</td><td>+450000</td></tr><tr><td>Gibraltar</td><td>4500/1</td><td>+450000</td></tr><tr><td>San Marino</td><td>4500/1</td><td>+450000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	