	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--spain La Liga 2">
			<caption>Pre--spain La Liga 2</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--spain La Liga 2  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Albacete Balompie</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >470</div>
								 <div class="boxdata" >+1<br /> ( -133)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sd Huesca</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >-172</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Numancia Cd</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >442</div>
								 <div class="boxdata" >+&frac12;<br /> ( +117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rc Deportivo La Coruna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >-156</div>
								 <div class="boxdata" >-&frac12;<br /> ( -156 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sd Ponferradina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Lugo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >137</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--spain La Liga 2  - Sep 19</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sporting Gijon</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -135)</div>
								 <div class="boxdata" >193</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ud Las Palmas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( +102)</div>
								 <div class="boxdata" >147</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tenerife Cd</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >254</div>
								 <div class="boxdata" >+&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Elche Cf</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac12;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Extremadura Ud</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >365</div>
								 <div class="boxdata" >+&frac12;<br /> ( -101 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Oviedo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >-129</div>
								 <div class="boxdata" >-&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	