	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Racing - Motogp World Championship">
			<caption>Racing - Motogp World Championship</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Racing - Motogp World Championship  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Motogp World Championship - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Marc Marquez</td><td>1/500</td><td>-50000</td></tr><tr><td>Andrea Dovizioso</td><td>50/1</td><td>+5000</td></tr><tr><td>Valentino Rossi</td><td>4500/1</td><td>+450000</td></tr><tr><td>Maverick Vinales</td><td>4000/1</td><td>+400000</td></tr><tr><td>Alex Rins</td><td>3000/1</td><td>+300000</td></tr><tr><td>Danilo Petrucci</td><td>4000/1</td><td>+400000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	