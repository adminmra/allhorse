	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--poland 1 Liga">
			<caption>Live--poland 1 Liga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 29, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--poland 1 Liga  - Mar 29</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chrobry Glogow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -142 )</div>
								 <div class="boxdata" >2100</div>
								 <div class="boxdata" >+1<br /> ( +104)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mks Puszcza Niepolomice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-555</div>
								 <div class="boxdata" >-1<br /> ( -166)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Podbeskidzie Bielsko Biala</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+3<br /> ( -117)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lks Lodz Pss</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -153 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-3<br /> ( -133)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	