	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Australia A League">
			<caption>Australia A League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 3, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Australia A League  - Dec 07</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Central Coast Mariners<br /> - vs - <br /> Western Sydney Wanderers</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1<br /> (-140)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+388</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-145 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1<br /> (120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-165</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+125</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Australia A League  - Dec 08</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Wellington Phoenix<br /> - vs - <br /> Sydney</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1.5<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+691</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o3-110 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1.5<br /> (-105)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-310</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u3-110</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Adelaide United<br /> - vs - <br /> Melbourne Victory</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (100)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+285</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-135 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (-120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-121</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+115</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Melbourne City<br /> - vs - <br /> Perth Glory</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-140)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+204</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-120 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+121</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;EV</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Australia A League  - Dec 09</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Brisbane Roar<br /> - vs - <br /> Newcastle Jets</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+238</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-115 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (100)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+101</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-105</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	