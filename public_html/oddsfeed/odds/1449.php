	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - The Epsom">
			<caption>Horses - The Epsom</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated April 13, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - The Epsom  - Jun 01					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Epsom Derby - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Too Darn Hot</td><td>4/1</td><td>+400</td></tr><tr><td>Anthony Van Dyck</td><td>12/1</td><td>+1200</td></tr><tr><td>Quorto</td><td>16/1</td><td>+1600</td></tr><tr><td>Japan</td><td>10/1</td><td>+1000</td></tr><tr><td>Madhmoon</td><td>20/1</td><td>+2000</td></tr><tr><td>Magna Grecia</td><td>16/1</td><td>+1600</td></tr><tr><td>Line Of Duty</td><td>20/1</td><td>+2000</td></tr><tr><td>Turgenev</td><td>25/1</td><td>+2500</td></tr><tr><td>Sydney Opera House</td><td>33/1</td><td>+3300</td></tr><tr><td>Mount Everest</td><td>25/1</td><td>+2500</td></tr><tr><td>Norway</td><td>33/1</td><td>+3300</td></tr><tr><td>Rakan</td><td>33/1</td><td>+3300</td></tr><tr><td>Al Hilalee</td><td>33/1</td><td>+3300</td></tr><tr><td>Bangkok</td><td>50/1</td><td>+5000</td></tr><tr><td>Arthur Kitt</td><td>66/1</td><td>+6600</td></tr><tr><td>Old Glory</td><td>50/1</td><td>+5000</td></tr><tr><td>Beatboxer</td><td>50/1</td><td>+5000</td></tr><tr><td>Blenheim Palace</td><td>50/1</td><td>+5000</td></tr><tr><td>Waldstern</td><td>25/1</td><td>+2500</td></tr><tr><td>Eminence</td><td>40/1</td><td>+4000</td></tr><tr><td>Cape Of Good Hope</td><td>33/1</td><td>+3300</td></tr><tr><td>Calculation</td><td>66/1</td><td>+6600</td></tr><tr><td>Desert Island</td><td>33/1</td><td>+3300</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	