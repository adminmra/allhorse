	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Tennis - Specials">
			<caption>Tennis - Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 13, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Tennis - Specials  - May 14					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Grand Slams - Alexander Zverev Wins</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>None</td><td>1/5</td><td>-500</td></tr><tr><td>One</td><td>3/1</td><td>+300</td></tr><tr><td>Two</td><td>33/1</td><td>+3300</td></tr><tr><td>Three</td><td>150/1</td><td>+15000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Grand Slams - Novak Djokovic Wins</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>One</td><td>2/1</td><td>+200</td></tr><tr><td>Two</td><td>5/4</td><td>+125</td></tr><tr><td>Three</td><td>11/4</td><td>+275</td></tr><tr><td>Four</td><td>10/1</td><td>+1000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Grand Slams - Rafael Nadal Wins</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>None</td><td>8/5</td><td>+160</td></tr><tr><td>One</td><td>4/5</td><td>-125</td></tr><tr><td>Two</td><td>5/1</td><td>+500</td></tr><tr><td>Three</td><td>50/1</td><td>+5000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Grand Slams - Roger Federer Wins</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>None</td><td>2/7</td><td>-350</td></tr><tr><td>One</td><td>11/5</td><td>+220</td></tr><tr><td>Two</td><td>25/1</td><td>+2500</td></tr><tr><td>Three</td><td>150/1</td><td>+15000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');</script>
{/literal}	
	