	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Breeders Cup Filly And Mare Turf - To Win">
			<caption>Horses - Breeders Cup Filly And Mare Turf - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Breeders Cup Filly And Mare Turf - To Win  - Nov 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Breeders Cup Filly & Mare Turf</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Newspaperofrecord</td><td>20/1</td><td>+2000</td></tr><tr><td>Sistercharlie</td><td>7/4</td><td>+175</td></tr><tr><td>Magic Wand</td><td>10/1</td><td>+1000</td></tr><tr><td>Rymska</td><td>20/1</td><td>+2000</td></tr><tr><td>Qabala</td><td>33/1</td><td>+3300</td></tr><tr><td>Rushing Fall</td><td>10/1</td><td>+1000</td></tr><tr><td>Wild Illusion</td><td>16/1</td><td>+1600</td></tr><tr><td>Homerique</td><td>8/1</td><td>+800</td></tr><tr><td>Vasilika</td><td>10/1</td><td>+1000</td></tr><tr><td>Lys Gracieux</td><td>16/1</td><td>+1600</td></tr><tr><td>Pink Dogwood</td><td>16/1</td><td>+1600</td></tr><tr><td>Just Wonderful</td><td>20/1</td><td>+2000</td></tr><tr><td>Iridessa</td><td>10/1</td><td>+1000</td></tr><tr><td>Cambier Parc</td><td>16/1</td><td>+1600</td></tr><tr><td>Onthemoonagain</td><td>33/1</td><td>+3300</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	