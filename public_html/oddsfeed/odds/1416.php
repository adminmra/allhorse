	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Uae Agl">
			<caption>Uae Agl</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 3, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Uae Agl  - Dec 03</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Al Ittihad Kalba<br /> - vs - <br /> Dibba Al Fujairah</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+154</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"> </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+158</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Al Sharjah<br /> - vs - <br /> Al Wahda</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+184</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"> </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+114</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Al Ain<br /> - vs - <br /> Al Wasl</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-153</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"> </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+279</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	