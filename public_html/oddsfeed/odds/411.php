	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Sb - Home Team Props">
			<caption>Sb - Home Team Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Sb - Home Team Props  - Feb 03</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Touchdown</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-130</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Field Goal Or Safety</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc In All Qtrs Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc In All Qtrs No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Tot 1st Downs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o23&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Tot 1st Downs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u23&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Tot Sacks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Tot Sacks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc Rush Td Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-300</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc Rush Td No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >220</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Score First</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Punt First</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-140</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Touchdown</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-170</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Field Goal Or Safety</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc Td 1st Qt Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >105</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc Td 1st Qt No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc Td 2nd Qt Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-250</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc Td 2nd Qt No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc Td 3rd Qt Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-160</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc Td 3rd Qt No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >130</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc Td 4th Qt Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc Td 4th Qt No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >160</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Total 3rd Down Conv </div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Total 3rd Down Conv </div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams 4th Down Conv Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-145</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams 4th Down Conv No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >115</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Players With Pass Recp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Players With Pass Recp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-170</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc 1h Rushing Td - Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Sc 1h Rushing Td - No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-130</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Cross 50 Yd Line - Pass</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Cross 50 Yd Line - Run</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >155</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams 1st (3rd Down) Conv -pass</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams 1st (3rd Down) Conv - Run</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >155</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Tot Penalty Yds</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o45&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rams Tot Penalty Yds</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u45&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	