	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="International Ice Hockey - Futures">
			<caption>International Ice Hockey - Futures</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					International Ice Hockey - Futures  - May 08					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Iihf World Championship - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Canada</td><td>5/2</td><td>+250</td></tr><tr><td>Russia</td><td>5/2</td><td>+250</td></tr><tr><td>Sweden</td><td>4/1</td><td>+400</td></tr><tr><td>Usa</td><td>8/1</td><td>+800</td></tr><tr><td>Finland</td><td>8/1</td><td>+800</td></tr><tr><td>Czech Republic</td><td>12/1</td><td>+1200</td></tr><tr><td>Switzerland</td><td>14/1</td><td>+1400</td></tr><tr><td>Germany</td><td>33/1</td><td>+3300</td></tr><tr><td>Slovakia</td><td>100/1</td><td>+10000</td></tr><tr><td>Denmark</td><td>200/1</td><td>+20000</td></tr><tr><td>Latvia</td><td>200/1</td><td>+20000</td></tr><tr><td>Norway</td><td>200/1</td><td>+20000</td></tr><tr><td>Belarus</td><td>500/1</td><td>+50000</td></tr><tr><td>Kazakhstan</td><td>500/1</td><td>+50000</td></tr><tr><td>Italy</td><td>1000/1</td><td>+100000</td></tr><tr><td>Great Britain</td><td>1000/1</td><td>+100000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	