	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba -  Conference - Odds To Win">
			<caption>Nba -  Conference - Odds To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nba -  Conference - Odds To Win  - Oct 15					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Eastern Conference - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Toronto Raptors</td><td>19/1</td><td>+1900</td></tr><tr><td>Milwaukee Bucks</td><td>13/10</td><td>+130</td></tr><tr><td>Philadelphia 76ers</td><td>17/10</td><td>+170</td></tr><tr><td>New York Knicks</td><td>200/1</td><td>+20000</td></tr><tr><td>Boston Celtics</td><td>67/10</td><td>+670</td></tr><tr><td>Brooklyn Nets</td><td>14/1</td><td>+1400</td></tr><tr><td>Indiana Pacers</td><td>19/2</td><td>+950</td></tr><tr><td>Orlando Magic</td><td>45/1</td><td>+4500</td></tr><tr><td>Atlanta Hawks</td><td>90/1</td><td>+9000</td></tr><tr><td>Chicago Bulls</td><td>180/1</td><td>+18000</td></tr><tr><td>Washington Wizards</td><td>260/1</td><td>+26000</td></tr><tr><td>Detroit Pistons</td><td>90/1</td><td>+9000</td></tr><tr><td>Charlotte Hornets</td><td>350/1</td><td>+35000</td></tr><tr><td>Cleveland Cavaliers</td><td>285/1</td><td>+28500</td></tr><tr><td>Miami Heat</td><td>25/1</td><td>+2500</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Western Conference - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Golden State Warriors</td><td>5/1</td><td>+500</td></tr><tr><td>Los Angeles Lakers</td><td>23/10</td><td>+230</td></tr><tr><td>Houston Rockets</td><td>57/10</td><td>+570</td></tr><tr><td>Los Angeles Clippers</td><td>11/4</td><td>+275</td></tr><tr><td>Denver Nuggets</td><td>8/1</td><td>+800</td></tr><tr><td>Oklahoma City Thunder</td><td>220/1</td><td>+22000</td></tr><tr><td>Portland Trail Blazers</td><td>16/1</td><td>+1600</td></tr><tr><td>Utah Jazz</td><td>8/1</td><td>+800</td></tr><tr><td>Dallas Mavericks</td><td>55/1</td><td>+5500</td></tr><tr><td>San Antonio Spurs</td><td>50/1</td><td>+5000</td></tr><tr><td>New Orleans Pelicans</td><td>50/1</td><td>+5000</td></tr><tr><td>Memphis Grizzlies</td><td>300/1</td><td>+30000</td></tr><tr><td>Sacramento Kings</td><td>175/1</td><td>+17500</td></tr><tr><td>Phoenix Suns</td><td>340/1</td><td>+34000</td></tr><tr><td>Minnesota Timberwolves</td><td>245/1</td><td>+24500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	