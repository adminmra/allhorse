	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="South Korea K League Classic">
			<caption>South Korea K League Classic</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated November 30, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">South Korea K League Classic  - Dec 01</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Daegu<br /> - vs - <br /> Gangwon</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-130)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+210</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-145 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (110)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+110</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+125</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Jeonnam Dragons<br /> - vs - <br /> Incheon United</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1<br /> (-130)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+389</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o3+110 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1<br /> (110)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-168</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u3-130</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Seoul<br /> - vs - <br /> Sangju Sangmu</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-125)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+142</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-105 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (105)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+177</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-115</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">South Korea K League Classic  - Dec 02</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Ulsan<br /> - vs - <br /> Pohang Steelers</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+181</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"> </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+133</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	