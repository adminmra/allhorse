	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Georgia - Erovnuli Liga">
			<caption>Georgia - Erovnuli Liga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated November 30, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Georgia - Erovnuli Liga  - Nov 30</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Kolkheti Poti<br /> - vs - <br /> Lokomotivi Tbilisi</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1.5<br /> (-103)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+1085</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-155 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1.5<br /> (-127)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-531</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+125</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	