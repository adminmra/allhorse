	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--norway Toppserien, Women">
			<caption>Pre--norway Toppserien, Women</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--norway Toppserien, Women  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Avaldsnes Il</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -125 )</div>
								 <div class="boxdata" >-111</div>
								 <div class="boxdata" >-&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lyn</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -106 )</div>
								 <div class="boxdata" >259</div>
								 <div class="boxdata" >+&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kolbotn</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >-107</div>
								 <div class="boxdata" >-&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stabaek Fotball</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -109 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lillestrom Sk Kvinner</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -123)</div>
								 <div class="boxdata" >-270</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Trondheims-orn</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -107)</div>
								 <div class="boxdata" >600</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	