	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--luxembourg Division Nationale">
			<caption>Pre--luxembourg Division Nationale</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 12, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--luxembourg Division Nationale  - May 12</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Mondorf-les-bains</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >240</div>
								 <div class="boxdata" >+&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Rm Hamm Benfica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >-113</div>
								 <div class="boxdata" >-&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F91 Dudelange</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >-285</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -108 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Hostert</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >549</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	