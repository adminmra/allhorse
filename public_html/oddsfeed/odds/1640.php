	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--netherlands Eredivisie">
			<caption>Live--netherlands Eredivisie</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 12, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--netherlands Eredivisie  - May 12</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Emmen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >260</div>
								 <div class="boxdata" >+&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Willem Ii Tilburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Utrecht</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( -153 )</div>
								 <div class="boxdata" >450</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ajax Amsterdam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( +114 )</div>
								 <div class="boxdata" >-285</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >De Graafschap</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( +114 )</div>
								 <div class="boxdata" >425</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vitesse Arnhem</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -153 )</div>
								 <div class="boxdata" >-222</div>
								 <div class="boxdata" >-1&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nac Breda</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" >300</div>
								 <div class="boxdata" >+&frac34;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Heerenveen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -166 )</div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" >-&frac34;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fortuna Sittard</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -181 )</div>
								 <div class="boxdata" >400</div>
								 <div class="boxdata" >+1<br /> ( -111)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Groningen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +129 )</div>
								 <div class="boxdata" >-200</div>
								 <div class="boxdata" >-1<br /> ( -125)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sbv Excelsior</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Heracles Almelo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -166 )</div>
								 <div class="boxdata" >220</div>
								 <div class="boxdata" >+&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vvv Venlo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -142 )</div>
								 <div class="boxdata" >305</div>
								 <div class="boxdata" >+&frac34;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pec Zwolle</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +104 )</div>
								 <div class="boxdata" >-142</div>
								 <div class="boxdata" >-&frac34;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Psv Eindhoven</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -181 )</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Az Alkmaar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +129 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ado Den Haag</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( +114 )</div>
								 <div class="boxdata" >950</div>
								 <div class="boxdata" >+2&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Feyenoord Rotterdam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -166 )</div>
								 <div class="boxdata" >-714</div>
								 <div class="boxdata" >-2&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	