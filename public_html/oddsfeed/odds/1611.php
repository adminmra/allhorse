	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--bulgaria Parva Liga">
			<caption>Live--bulgaria Parva Liga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 21, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--bulgaria Parva Liga  - May 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cska Sofia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Levski Sofia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >235</div>
								 <div class="boxdata" >+&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ludogorets 1945 Razgrad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" >-181</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Botev Plovdiv</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -200 )</div>
								 <div class="boxdata" >500</div>
								 <div class="boxdata" >+1<br /> ( -133)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	