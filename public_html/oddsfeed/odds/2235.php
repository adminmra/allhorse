	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--italy Serie C">
			<caption>Pre--italy Serie C</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--italy Serie C  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Juventus U23</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >249</div>
								 <div class="boxdata" >+&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Arezzo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >-101</div>
								 <div class="boxdata" >-&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--italy Serie C  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Feralpi Salo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Modena Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >170</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Como 1907</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >174</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Pistoiese</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Sudtirol</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Arzignano Valchiampo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >188</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Gozzano</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >380</div>
								 <div class="boxdata" >+&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Pro Vercelli 1892</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" >-&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Imolese Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ravenna Fc 1913</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Piacenza Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cesena Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rimini Fc 1912</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >334</div>
								 <div class="boxdata" >+&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Carpi Fc 1909</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >-128</div>
								 <div class="boxdata" >-&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Teramo Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >146</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rende Calcio 1968</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >181</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--italy Serie C  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Triestina Calcio 1918</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Virtus Vecomp Verona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >212</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Reggio Audace Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >123</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aj Fano 1906</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >199</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aurora Pro Patria 1919</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >334</div>
								 <div class="boxdata" >+&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Robur Siena</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >-128</div>
								 <div class="boxdata" >-&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Pergolettese 1932</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >372</div>
								 <div class="boxdata" >+&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Novara Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >-142</div>
								 <div class="boxdata" >-&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Calcio Padova</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >118</div>
								 <div class="boxdata" >-&frac12;<br /> ( +116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ssd Vis Pesaro 1898</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >216</div>
								 <div class="boxdata" >+&frac12;<br /> ( -161 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Alessandria Calcio 1912</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Pianese</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Uc Albinoleffe</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >267</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Arezzo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >-103</div>
								 <div class="boxdata" >-&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lr Vicenza Virtus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >137</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gubbio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >192</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pontedera</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Juventus U23</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olbia Calcio 1905</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac12;<br /> ( -158 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Renate</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >116</div>
								 <div class="boxdata" >-&frac12;<br /> ( +114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sambenedettese</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >177</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fermana Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Catanzaro 1929</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Viterbo Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >187</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Bisceglie</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >270</div>
								 <div class="boxdata" >+&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Avellino 1912</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >-104</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Paganese Calcio 1926</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >227</div>
								 <div class="boxdata" >+&frac12;<br /> ( -158 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cavese 1919</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac12;<br /> ( +114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Calcio Catania</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Monopoli 1966</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >197</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Potenza Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >-102</div>
								 <div class="boxdata" >-&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Rieti</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Casertana Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >145</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Az Picerno</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >168</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ternana Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sicula Leonzio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >202</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ssc Bari</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac12;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Virtus Francavilla Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Vibonese Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >466</div>
								 <div class="boxdata" >+1<br /> ( -136)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Reggina 1914</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >-181</div>
								 <div class="boxdata" >-1<br /> ( -101)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Monza</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" >-&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Calcio Lecco</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >351</div>
								 <div class="boxdata" >+&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--italy Serie C  - Sep 23</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Carrarese Calcio 1908</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >134</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Giana Erminio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >197</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	