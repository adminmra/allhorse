	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mlb - American League Divisions - To Win">
			<caption>Mlb - American League Divisions - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Mlb - American League Divisions - To Win  - Jul 14					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 American League East Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>New York Yankees</td><td>1/25</td><td>-2500</td></tr><tr><td>Tampa Bay Rays</td><td>13/1</td><td>+1300</td></tr><tr><td>Boston Red Sox</td><td>15/1</td><td>+1500</td></tr><tr><td>Toronto Blue Jays</td><td>4000/1</td><td>+400000</td></tr><tr><td>Baltimore Orioles</td><td>5000/1</td><td>+500000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 American League Central Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Minnesota Twins</td><td>1/35</td><td>-3500</td></tr><tr><td>Cleveland Indians</td><td>8/1</td><td>+800</td></tr><tr><td>Chicago White Sox</td><td>150/1</td><td>+15000</td></tr><tr><td>Detroit Tigers</td><td>5000/1</td><td>+500000</td></tr><tr><td>Kansas City Royals</td><td>5000/1</td><td>+500000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 American League West Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Houston Astros</td><td>1/66</td><td>-6600</td></tr><tr><td>Oakland Athletics</td><td>15/1</td><td>+1500</td></tr><tr><td>Texas Rangers</td><td>18/1</td><td>+1800</td></tr><tr><td>Los Angeles Angels</td><td>50/1</td><td>+5000</td></tr><tr><td>Seattle Mariners</td><td>1500/1</td><td>+150000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');</script>
{/literal}	
	