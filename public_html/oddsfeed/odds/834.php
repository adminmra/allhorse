	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nhl - Conferences - Odds To Win">
			<caption>Nhl - Conferences - Odds To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nhl - Conferences - Odds To Win  - Oct 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Eastern Conference - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tampa Bay Lightning</td><td>13/4</td><td>+325</td></tr><tr><td>Toronto Maple Leafs</td><td>4/1</td><td>+400</td></tr><tr><td>Boston Bruins</td><td>5/1</td><td>+500</td></tr><tr><td>Washington Capitals</td><td>10/1</td><td>+1000</td></tr><tr><td>Florida Panthers</td><td>10/1</td><td>+1000</td></tr><tr><td>Pittsburgh Penguins</td><td>12/1</td><td>+1200</td></tr><tr><td>New York Islanders</td><td>14/1</td><td>+1400</td></tr><tr><td>Carolina Hurricanes</td><td>10/1</td><td>+1000</td></tr><tr><td>Philadelphia Flyers</td><td>16/1</td><td>+1600</td></tr><tr><td>Columbus Blue Jackets</td><td>22/1</td><td>+2200</td></tr><tr><td>Montreal Canadiens</td><td>18/1</td><td>+1800</td></tr><tr><td>New Jersey Devils</td><td>14/1</td><td>+1400</td></tr><tr><td>New York Rangers</td><td>22/1</td><td>+2200</td></tr><tr><td>Buffalo Sabres</td><td>35/1</td><td>+3500</td></tr><tr><td>Detroit Red Wings</td><td>45/1</td><td>+4500</td></tr><tr><td>Ottawa Senators</td><td>150/1</td><td>+15000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Western Conference - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Vegas Golden Knights</td><td>15/4</td><td>+375</td></tr><tr><td>Colorado Avalanche</td><td>19/4</td><td>+475</td></tr><tr><td>St Louis Blues</td><td>13/2</td><td>+650</td></tr><tr><td>Calgary Flames</td><td>9/1</td><td>+900</td></tr><tr><td>San Jose Sharks</td><td>9/1</td><td>+900</td></tr><tr><td>Winnipeg Jets</td><td>11/1</td><td>+1100</td></tr><tr><td>Nashville Predators</td><td>8/1</td><td>+800</td></tr><tr><td>Dallas Stars</td><td>8/1</td><td>+800</td></tr><tr><td>Edmonton Oilers</td><td>17/1</td><td>+1700</td></tr><tr><td>Arizona Coyotes</td><td>20/1</td><td>+2000</td></tr><tr><td>Chicago Blackhawks</td><td>17/1</td><td>+1700</td></tr><tr><td>Vancouver Canucks</td><td>20/1</td><td>+2000</td></tr><tr><td>Minnesota Wild</td><td>25/1</td><td>+2500</td></tr><tr><td>Los Angeles Kings</td><td>40/1</td><td>+4000</td></tr><tr><td>Anaheim Ducks</td><td>40/1</td><td>+4000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	