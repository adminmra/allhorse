	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nhl - 3 Way Line - Regulation Time">
			<caption>Nhl - 3 Way Line - Regulation Time</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nhl - 3 Way Line - Regulation Time  - Jun 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Bruins Vs Blues - 3 Way Line</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Bruins</td><td>7/4</td><td>+175</td></tr><tr><td>Blues</td><td>27/20</td><td>+135</td></tr><tr><td>Tie</td><td>53/20</td><td>+265</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	