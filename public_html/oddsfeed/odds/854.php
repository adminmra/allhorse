	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Uefa Champions League - To Win">
			<caption>Soccer - Uefa Champions League - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 17, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Uefa Champions League - To Win  - Sep 17					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Uefa Champions League - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Manchester City</td><td>33/10</td><td>+330</td></tr><tr><td>Barcelona</td><td>5/1</td><td>+500</td></tr><tr><td>Liverpool</td><td>13/2</td><td>+650</td></tr><tr><td>Real Madrid</td><td>9/1</td><td>+900</td></tr><tr><td>Psg</td><td>10/1</td><td>+1000</td></tr><tr><td>Juventus</td><td>10/1</td><td>+1000</td></tr><tr><td>Bayern Munich</td><td>12/1</td><td>+1200</td></tr><tr><td>Atletico Madrid</td><td>20/1</td><td>+2000</td></tr><tr><td>Tottenham</td><td>25/1</td><td>+2500</td></tr><tr><td>Borussia Dortmund</td><td>33/1</td><td>+3300</td></tr><tr><td>Chelsea</td><td>33/1</td><td>+3300</td></tr><tr><td>Napoli</td><td>33/1</td><td>+3300</td></tr><tr><td>Inter Milan</td><td>50/1</td><td>+5000</td></tr><tr><td>Ajax</td><td>66/1</td><td>+6600</td></tr><tr><td>Rb Leipzig</td><td>66/1</td><td>+6600</td></tr><tr><td>Valencia</td><td>100/1</td><td>+10000</td></tr><tr><td>Bayer Leverkusen</td><td>150/1</td><td>+15000</td></tr><tr><td>Lyon</td><td>80/1</td><td>+8000</td></tr><tr><td>Benfica</td><td>100/1</td><td>+10000</td></tr><tr><td>Atalanta</td><td>150/1</td><td>+15000</td></tr><tr><td>Zenit St Petersburg</td><td>150/1</td><td>+15000</td></tr><tr><td>Shakhtar Donetsk</td><td>200/1</td><td>+20000</td></tr><tr><td>Lille</td><td>250/1</td><td>+25000</td></tr><tr><td>Fc Salzburg</td><td>250/1</td><td>+25000</td></tr><tr><td>Galatasaray</td><td>250/1</td><td>+25000</td></tr><tr><td>Olympiakos</td><td>250/1</td><td>+25000</td></tr><tr><td>Red Star Belgrade</td><td>750/1</td><td>+75000</td></tr><tr><td>Dinamo Zagreb</td><td>350/1</td><td>+35000</td></tr><tr><td>Slavia Prague</td><td>500/1</td><td>+50000</td></tr><tr><td>Club Brugge</td><td>1000/1</td><td>+100000</td></tr><tr><td>Genk</td><td>1000/1</td><td>+100000</td></tr><tr><td>Lokomotiv Moscow</td><td>750/1</td><td>+75000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	