	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Ent - Razzies Awards - To Win">
			<caption>Ent - Razzies Awards - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Ent - Razzies Awards - To Win  - Feb 08					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">40th Razzie Awards - Worst Movie</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Serenity</td><td>4/1</td><td>+400</td></tr><tr><td>Replicas</td><td>5/1</td><td>+500</td></tr><tr><td>The Professor</td><td>7/1</td><td>+700</td></tr><tr><td>Hellboy</td><td>7/1</td><td>+700</td></tr><tr><td>The Hustle</td><td>8/1</td><td>+800</td></tr><tr><td>A Madea Family Funeral</td><td>9/1</td><td>+900</td></tr><tr><td>Glass</td><td>10/1</td><td>+1000</td></tr><tr><td>Poms</td><td>10/1</td><td>+1000</td></tr><tr><td>X-men: Dark Phoenix</td><td>12/1</td><td>+1200</td></tr><tr><td>Godzilla: King Of The Monsters</td><td>12/1</td><td>+1200</td></tr><tr><td>Aladdin</td><td>14/1</td><td>+1400</td></tr><tr><td>Rim Of The World</td><td>14/1</td><td>+1400</td></tr><tr><td>The Upside</td><td>14/1</td><td>+1400</td></tr><tr><td>Brightburn</td><td>16/1</td><td>+1600</td></tr><tr><td>The Intruder</td><td>16/1</td><td>+1600</td></tr><tr><td>Uglydolls</td><td>18/1</td><td>+1800</td></tr><tr><td>Men In Black: International</td><td>20/1</td><td>+2000</td></tr><tr><td>Wonder Park</td><td>20/1</td><td>+2000</td></tr><tr><td>After</td><td>33/1</td><td>+3300</td></tr><tr><td>What Men Want</td><td>33/1</td><td>+3300</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	