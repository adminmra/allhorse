	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Kentucky Derby - Top 2 Finish">
			<caption>Horses - Kentucky Derby - Top 2 Finish</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Horses - Kentucky Derby - Top 2 Finish  - May 04</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >War Of Will</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tax</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >By My Standards</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gray Magician</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Improbable</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vekoma</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Maximum Security</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tacitus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Plus Que Parfait</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cutting Humor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Code Of Honor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Win Win Win</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Master Fencer</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Game Winner</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Roadster</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Long Range Toddy</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Spinoff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Country House</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2019 Kentucky Derby Top 2 Finishers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bodexpress</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	