	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--finland Veikkausliiga">
			<caption>Pre--finland Veikkausliiga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--finland Veikkausliiga  - Jul 13</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hifk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >444</div>
								 <div class="boxdata" >+1<br /> ( -144)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Honka</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >-172</div>
								 <div class="boxdata" >-1<br /> ( +106)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vaasa Ps</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -116)</div>
								 <div class="boxdata" >353</div>
								 <div class="boxdata" >+&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Seinajoen Jk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -119)</div>
								 <div class="boxdata" >-123</div>
								 <div class="boxdata" >-&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--finland Veikkausliiga  - Jul 14</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tampereen Ilves</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Lahti</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >259</div>
								 <div class="boxdata" >+&frac12;<br /> ( -138 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kpv Kokkola</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >380</div>
								 <div class="boxdata" >+1<br /> ( -156)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ifk Mariehamn</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >-156</div>
								 <div class="boxdata" >-1<br /> ( +112)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	