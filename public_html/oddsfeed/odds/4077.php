	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Uefa Champions League Group Betting">
			<caption>Soccer - Uefa Champions League Group Betting</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 17, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Uefa Champions League Group Betting  - Sep 17					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Uefa Champions League - Group A</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Real Madrid</td><td>4/5</td><td>-125</td></tr><tr><td>Psg</td><td>EV</td><td>EV</td></tr><tr><td>Galatasaray</td><td>16/1</td><td>+1600</td></tr><tr><td>Club Brugge</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Uefa Champions League - Group B</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Bayern Munich</td><td>4/7</td><td>-175</td></tr><tr><td>Tottenham</td><td>3/2</td><td>+150</td></tr><tr><td>Olympiakos</td><td>20/1</td><td>+2000</td></tr><tr><td>Red Star Belgrade</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Uefa Champions League - Group C</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Manchester City</td><td>1/6</td><td>-600</td></tr><tr><td>Atalanta</td><td>13/2</td><td>+650</td></tr><tr><td>Shakhtar Donetsk</td><td>8/1</td><td>+800</td></tr><tr><td>Dinamo Zagreb</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Uefa Champions League - Group D</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Juventus</td><td>20/33</td><td>-165</td></tr><tr><td>Atletico Madrid</td><td>3/2</td><td>+150</td></tr><tr><td>Bayer Leverkusen</td><td>12/1</td><td>+1200</td></tr><tr><td>Lokomotiv Moscow</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Uefa Champions League - Group E</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Liverpool</td><td>1/2</td><td>-200</td></tr><tr><td>Napoli</td><td>7/4</td><td>+175</td></tr><tr><td>Fc Salzburg</td><td>14/1</td><td>+1400</td></tr><tr><td>Genk</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Uefa Champions League - Group F</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Barcelona</td><td>4/11</td><td>-275</td></tr><tr><td>Borussia Dortmund</td><td>7/2</td><td>+350</td></tr><tr><td>Inter Milan</td><td>11/2</td><td>+550</td></tr><tr><td>Slavia Prague</td><td>150/1</td><td>+15000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Uefa Champions League - Group G</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Rb Leipzig</td><td>37/20</td><td>+185</td></tr><tr><td>Lyon</td><td>9/4</td><td>+225</td></tr><tr><td>Benfica</td><td>5/2</td><td>+250</td></tr><tr><td>Zenit St Petersburg</td><td>5/1</td><td>+500</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Uefa Champions League - Group H</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Chelsea</td><td>5/7</td><td>-140</td></tr><tr><td>Ajax</td><td>7/2</td><td>+350</td></tr><tr><td>Valencia</td><td>7/2</td><td>+350</td></tr><tr><td>Lille</td><td>10/1</td><td>+1000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');addSort('o6t');addSort('o7t');</script>
{/literal}	
	