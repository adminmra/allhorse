	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Major League Baseball">
			<caption>Major League Baseball</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Major League Baseball  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Was Nationals</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o7&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-170</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stl Cardinals</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u7&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >145</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ny Mets</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o13<br /> ( -105)</div>
								 <div class="boxdata" >-150</div>
								 <div class="boxdata" >-1&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Col Rockies</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u13<br /> ( -115)</div>
								 <div class="boxdata" >130</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mia Marlins</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o9<br /> ( -105)</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -160 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ari Diamondbacks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u9<br /> ( -115)</div>
								 <div class="boxdata" >-160</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Phi Phillies</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o10<br /> ( -120)</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -160 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atl Braves</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-145</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sdg Padres</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o8<br /> ( -115)</div>
								 <div class="boxdata" >130</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -165 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mil Brewers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u8<br /> ( -105)</div>
								 <div class="boxdata" >-150</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cin Reds</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chi Cubs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-180</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Major League Baseball  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kan Royals</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o10<br /> ( -105)</div>
								 <div class="boxdata" >205</div>
								 <div class="boxdata" >+1&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Oak Athletics</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u10<br /> ( -115)</div>
								 <div class="boxdata" >-255</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -130 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >La Angels</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o10<br /> ( -105)</div>
								 <div class="boxdata" >235</div>
								 <div class="boxdata" >+1&frac12;<br /> ( +130 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ny Yankees</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u10<br /> ( -115)</div>
								 <div class="boxdata" >-290</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -150 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tor Blue Jays</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o9&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -190 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bal Orioles</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u9&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-120</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +160 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Det Tigers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o9&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+1&frac12;<br /> ( +130 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cle Indians</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u9&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >-310</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -150 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tex Rangers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o8&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >405</div>
								 <div class="boxdata" >+1&frac12;<br /> ( +185 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hou Astros</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u8&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-555</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -225 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Major League Baseball  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sea Mariners</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o9&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -190 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pit Pirates</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u9&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-120</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +160 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sfo Giants</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o10<br /> ( -105)</div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -145 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bos Red Sox</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u10<br /> ( -115)</div>
								 <div class="boxdata" >-165</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tam Rays</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o8&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -180 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >La Dodgers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u8&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >-130</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +150 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	