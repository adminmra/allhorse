	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Cricket - Indian Premier League">
			<caption>Cricket - Indian Premier League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Cricket - Indian Premier League  - Apr 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Indian Premier League - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Mumbai Indians</td><td>4/1</td><td>+400</td></tr><tr><td>Sunrisers Hyderabad</td><td>4/1</td><td>+400</td></tr><tr><td>Chennai Super Kings</td><td>5/1</td><td>+500</td></tr><tr><td>Delhi Capitals</td><td>7/1</td><td>+700</td></tr><tr><td>Rc Bangalore</td><td>7/1</td><td>+700</td></tr><tr><td>Kolkata Kr</td><td>7/1</td><td>+700</td></tr><tr><td>Kings Xi Punjab</td><td>9/1</td><td>+900</td></tr><tr><td>Rajasthan Royals</td><td>9/1</td><td>+900</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	