	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--mexico U20 League">
			<caption>Live--mexico U20 League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated November 29, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Live--mexico U20 League  - Nov 29</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">America Mexico<br /> - vs - <br /> Atlas</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-133)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+240</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-153 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (100)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+290</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+114</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	