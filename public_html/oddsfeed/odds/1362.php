	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Breeders Cup Dirt Mile - To Win">
			<caption>Horses - Breeders Cup Dirt Mile - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Breeders Cup Dirt Mile - To Win  - Nov 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Breeder`s Cup Dirt Mile - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Mitole</td><td>4/1</td><td>+400</td></tr><tr><td>Mckinzie</td><td>7/2</td><td>+350</td></tr><tr><td>Firenze Fire</td><td>14/1</td><td>+1400</td></tr><tr><td>Coal Front</td><td>22/1</td><td>+2200</td></tr><tr><td>Promises Fulfilled</td><td>20/1</td><td>+2000</td></tr><tr><td>Pavel</td><td>50/1</td><td>+5000</td></tr><tr><td>Mr Money</td><td>12/1</td><td>+1200</td></tr><tr><td>Shancelot</td><td>12/1</td><td>+1200</td></tr><tr><td>Catalina Cruiser</td><td>4/1</td><td>+400</td></tr><tr><td>Roadster</td><td>10/1</td><td>+1000</td></tr><tr><td>War Of Will</td><td>12/1</td><td>+1200</td></tr><tr><td>Code Of Honor</td><td>10/1</td><td>+1000</td></tr><tr><td>Improbable</td><td>8/1</td><td>+800</td></tr><tr><td>Owendale</td><td>16/1</td><td>+1600</td></tr><tr><td>Giant Expectations</td><td>33/1</td><td>+3300</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	