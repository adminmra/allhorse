	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Canadian Football - Grey Cup - To Win">
			<caption>Canadian Football - Grey Cup - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Canadian Football - Grey Cup - To Win  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Cfl Grey Cup - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Calgary Stampeders</td><td>14/5</td><td>+280</td></tr><tr><td>Ottawa Redblacks</td><td>200/1</td><td>+20000</td></tr><tr><td>Winnipeg Blue Bombers</td><td>11/4</td><td>+275</td></tr><tr><td>Hamilton Tiger-cats</td><td>18/5</td><td>+360</td></tr><tr><td>Saskatchewan Roughriders</td><td>19/5</td><td>+380</td></tr><tr><td>Edmonton Eskimos</td><td>9/1</td><td>+900</td></tr><tr><td>Bc Lions</td><td>1250/1</td><td>+125000</td></tr><tr><td>Toronto Argonauts</td><td>500/1</td><td>+50000</td></tr><tr><td>Montreal Alouettes</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	