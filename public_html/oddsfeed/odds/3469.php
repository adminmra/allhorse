	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - Pga Tour 3 Balls">
			<caption>Golf - Pga Tour 3 Balls</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - Pga Tour 3 Balls  - Jul 04					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">3m Open - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Rory Sabbatini</td><td>10/11</td><td>-110</td></tr><tr><td>Beau Hossler</td><td>9/4</td><td>+225</td></tr><tr><td>Hank Lebioda</td><td>11/4</td><td>+275</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">3m Open - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Matt Every</td><td>37/20</td><td>+185</td></tr><tr><td>Nick Taylor</td><td>37/20</td><td>+185</td></tr><tr><td>Talor Gooch</td><td>3/2</td><td>+150</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">3m Open - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Ryan Moore</td><td>3/2</td><td>+150</td></tr><tr><td>Charley Hoffman</td><td>37/20</td><td>+185</td></tr><tr><td>Nick Watney</td><td>37/20</td><td>+185</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">3m Open - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Kevin Na</td><td>33/10</td><td>+330</td></tr><tr><td>Brooks Koepka</td><td>20/21</td><td>-105</td></tr><tr><td>Patrick Reed</td><td>37/20</td><td>+185</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">3m Open - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Charles Howell</td><td>5/2</td><td>+250</td></tr><tr><td>Bryson Dechambeau</td><td>5/4</td><td>+125</td></tr><tr><td>Keegan Bradley</td><td>7/4</td><td>+175</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">3m Open - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Adam Long</td><td>7/5</td><td>+140</td></tr><tr><td>Patton Kizzire</td><td>11/5</td><td>+220</td></tr><tr><td>Ryan Armour</td><td>9/5</td><td>+180</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">3m Open - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Cameron Champ</td><td>8/5</td><td>+160</td></tr><tr><td>Jason Dufner</td><td>3/2</td><td>+150</td></tr><tr><td>Chesson Hadley</td><td>9/4</td><td>+225</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">3m Open - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Scott Piercy</td><td>4/5</td><td>-125</td></tr><tr><td>Andrew Landry</td><td>37/20</td><td>+185</td></tr><tr><td>Rod Pampling</td><td>9/2</td><td>+450</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');addSort('o6t');addSort('o7t');</script>
{/literal}	
	