	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Australian Open">
			<caption>Australian Open</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Australian Open  - Jan 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Womens Australian Open - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Serena Williams</td><td>11/2</td><td>+550</td></tr><tr><td>Naomi Osaka</td><td>10/1</td><td>+1000</td></tr><tr><td>Simona Halep</td><td>10/1</td><td>+1000</td></tr><tr><td>Angelique Kerber</td><td>22/1</td><td>+2200</td></tr><tr><td>Sloane Stephens</td><td>25/1</td><td>+2500</td></tr><tr><td>Petra Kvitova</td><td>25/1</td><td>+2500</td></tr><tr><td>Karolina Pliskova</td><td>12/1</td><td>+1200</td></tr><tr><td>Elina Svitolina</td><td>14/1</td><td>+1400</td></tr><tr><td>Bianca Andreescu</td><td>7/1</td><td>+700</td></tr><tr><td>Garbine Muguruza</td><td>33/1</td><td>+3300</td></tr><tr><td>Aryna Sabalenka</td><td>20/1</td><td>+2000</td></tr><tr><td>Ashleigh Barty</td><td>10/1</td><td>+1000</td></tr><tr><td>Madison Keys</td><td>16/1</td><td>+1600</td></tr><tr><td>Caroline Wozniacki</td><td>33/1</td><td>+3300</td></tr><tr><td>Maria Sharapova</td><td>50/1</td><td>+5000</td></tr><tr><td>Anastasija Sevastova</td><td>66/1</td><td>+6600</td></tr><tr><td>Kiki Bertens</td><td>33/1</td><td>+3300</td></tr><tr><td>Danielle Collins</td><td>66/1</td><td>+6600</td></tr><tr><td>Victoria Azarenka</td><td>33/1</td><td>+3300</td></tr><tr><td>Daria Kasatkina</td><td>40/1</td><td>+4000</td></tr><tr><td>Caroline Garcia</td><td>33/1</td><td>+3300</td></tr><tr><td>Belinda Bencic</td><td>16/1</td><td>+1600</td></tr><tr><td>Anett Kontaveit</td><td>66/1</td><td>+6600</td></tr><tr><td>Venus Williams</td><td>100/1</td><td>+10000</td></tr><tr><td>Jelena Ostapenko</td><td>50/1</td><td>+5000</td></tr><tr><td>Johanna Konta</td><td>28/1</td><td>+2800</td></tr><tr><td>Elise Mertens</td><td>25/1</td><td>+2500</td></tr><tr><td>Amanda Anisimova</td><td>25/1</td><td>+2500</td></tr><tr><td>Julia Goerges</td><td>100/1</td><td>+10000</td></tr><tr><td>A Pavlyuchenkova</td><td>100/1</td><td>+10000</td></tr><tr><td>Sofia Kenin</td><td>40/1</td><td>+4000</td></tr><tr><td>Coco Vandeweghe</td><td>100/1</td><td>+10000</td></tr><tr><td>Marketa Vondrousova</td><td>50/1</td><td>+5000</td></tr><tr><td>Dominika Cibulkova</td><td>250/1</td><td>+25000</td></tr><tr><td>Carla Suarez Navarro</td><td>150/1</td><td>+15000</td></tr><tr><td>Eugenie Bouchard</td><td>250/1</td><td>+25000</td></tr><tr><td>Ekaterina Makarova</td><td>250/1</td><td>+25000</td></tr><tr><td>Catherine Bellis</td><td>250/1</td><td>+25000</td></tr><tr><td>Dayana Yastremska</td><td>33/1</td><td>+3300</td></tr><tr><td>Iga Swiatek</td><td>40/1</td><td>+4000</td></tr><tr><td>Maria Sakkari</td><td>100/1</td><td>+10000</td></tr><tr><td>Caty Mcnally</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					Australian Open  - Feb 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Mens Australian Open - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Novak Djokovic</td><td>8/5</td><td>+160</td></tr><tr><td>Rafael Nadal</td><td>3/1</td><td>+300</td></tr><tr><td>Roger Federer</td><td>7/1</td><td>+700</td></tr><tr><td>Alexander Zverev</td><td>16/1</td><td>+1600</td></tr><tr><td>Stefanos Tsitsipas</td><td>16/1</td><td>+1600</td></tr><tr><td>Juan Martin Del Potro</td><td>25/1</td><td>+2500</td></tr><tr><td>Dominic Thiem</td><td>25/1</td><td>+2500</td></tr><tr><td>Marin Cilic</td><td>33/1</td><td>+3300</td></tr><tr><td>Milos Raonic</td><td>33/1</td><td>+3300</td></tr><tr><td>Kei Nishikori</td><td>33/1</td><td>+3300</td></tr><tr><td>Kevin Anderson</td><td>40/1</td><td>+4000</td></tr><tr><td>Nick Kyrgios</td><td>33/1</td><td>+3300</td></tr><tr><td>Karen Khachanov</td><td>40/1</td><td>+4000</td></tr><tr><td>Denis Shapovalov</td><td>50/1</td><td>+5000</td></tr><tr><td>Borna Coric</td><td>50/1</td><td>+5000</td></tr><tr><td>Daniil Medvedev</td><td>10/1</td><td>+1000</td></tr><tr><td>Alex De Minaur</td><td>66/1</td><td>+6600</td></tr><tr><td>Roberto Bautista-agut</td><td>66/1</td><td>+6600</td></tr><tr><td>Hyeon Chung</td><td>66/1</td><td>+6600</td></tr><tr><td>Kyle Edmund</td><td>66/1</td><td>+6600</td></tr><tr><td>Stan Wawrinka</td><td>50/1</td><td>+5000</td></tr><tr><td>Grigor Dimitrov</td><td>33/1</td><td>+3300</td></tr><tr><td>David Goffin</td><td>80/1</td><td>+8000</td></tr><tr><td>John Isner</td><td>80/1</td><td>+8000</td></tr><tr><td>Lucas Pouille</td><td>100/1</td><td>+10000</td></tr><tr><td>Tomas Berdych</td><td>100/1</td><td>+10000</td></tr><tr><td>Andrey Rublev</td><td>100/1</td><td>+10000</td></tr><tr><td>Andy Murray</td><td>20/1</td><td>+2000</td></tr><tr><td>Felix Auger Aliassime</td><td>50/1</td><td>+5000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	