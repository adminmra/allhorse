	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--greece Greek Cup">
			<caption>Live--greece Greek Cup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated January 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--greece Greek Cup  - Jan 08</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Panionios Athens</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -181 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pas Giannina Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +129 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	