	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Costa Rica Primera Division">
			<caption>Costa Rica Primera Division</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated November 28, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
            <tr class="header"><td>TEAM</td><td>SPREAD</td><td>MONEYLINE</td><td>TOTAL</td></tr>
	        <tr><th colspan="4">Costa Rica Primera Division  - Nov 28</th></tr>
         <tr><td colspan="4"><table class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0"><tbody>
         <tr><td class="teams"><br />San Carlos<br /> - vs - <br /> Alajuelense</td><td class="spmoto"><span class="box" >1<br /> (-110) </span><br /><hr /><br/><span class="box" > -1<br /> (-110)</span></td><td class="spmoto"><span class="box" >+511</span> <br /><hr /><br/> <span class="box" >-201</span></td><td class="spmoto"><span class="box" >o2&frac12;-105 </span><br /><hr /><br/> <span class="box" >u2&frac12;-115</span></td></tr><tr><td class="teams"><br />Herediano<br /> - vs - <br /> Saprissa</td><td class="spmoto"><span class="box" >0.5<br /> (135) </span><br /><hr /><br/><span class="box" > -0.5<br /> (-160)</span></td><td class="spmoto"><span class="box" >+379</span> <br /><hr /><br/> <span class="box" >-159</span></td><td class="spmoto"><span class="box" >o2&frac12;+105 </span><br /><hr /><br/> <span class="box" >u2&frac12;-125</span></td></tr>                  </tbody>
               </table>
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	