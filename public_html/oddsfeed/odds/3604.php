	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Cricket - Match Props">
			<caption>Cricket - Match Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 27, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Cricket - Match Props  - Jun 28					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">South Africa Vs Sri Lanka - To Win The Toss</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>South Africa</td><td>20/23</td><td>-115</td></tr><tr><td>Sri Lanka</td><td>20/23</td><td>-115</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">South Africa Vs Sri Lanka - Most Match Sixes</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>South Africa</td><td>1/2</td><td>-200</td></tr><tr><td>Sri Lanka</td><td>3/2</td><td>+150</td></tr><tr><td>Draw</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">South Africa Vs Sri Lanka - Most Match Fours</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>South Africa</td><td>1/2</td><td>-200</td></tr><tr><td>Sri Lanka</td><td>EV</td><td>EV</td></tr><tr><td>Draw</td><td>19/1</td><td>+1900</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">S Africa Vs Sri Lanka - Highest 1st 15 Overs Score</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>South Africa</td><td>1/2</td><td>-200</td></tr><tr><td>Sri Lanka</td><td>11/10</td><td>+110</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">South Africa Vs Sri Lanka -  A Hundred To Be Score</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes - A Hundred Is Scored In The Game</td><td>10/13</td><td>-130</td></tr><tr><td>No - A Hundred Not Scored In The Game</td><td>EV</td><td>EV</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">South Africa Vs Sri Lanka - A Fifty To Be Scored</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes - A Fifty Is Scored In The Game</td><td>1/65</td><td>-6500</td></tr><tr><td>No - A Fifty Not Scored In The Game</td><td>13/1</td><td>+1300</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">South Africa Vs Sri Lanka - Race To 10 Runs</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Fdm Karunaratne</td><td>EV</td><td>EV</td></tr><tr><td>Kusal Perera</td><td>5/7</td><td>-140</td></tr><tr><td>Neither</td><td>15/2</td><td>+750</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">South Africa Vs Sri Lanka - Race To 10 Runs</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Q De Kock</td><td>10/13</td><td>-130</td></tr><tr><td>Hm Amla</td><td>21/20</td><td>+105</td></tr><tr><td>Neither</td><td>10/1</td><td>+1000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o8t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">South Africa Vs Sri Lanka - 1st Wicket Method</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Caught</td><td>20/47</td><td>-235</td></tr><tr><td>Bowled</td><td>4/1</td><td>+400</td></tr><tr><td>Lbw</td><td>5/1</td><td>+500</td></tr><tr><td>Run Out</td><td>13/1</td><td>+1300</td></tr><tr><td>Stumped</td><td>24/1</td><td>+2400</td></tr><tr><td>Others</td><td>190/1</td><td>+19000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o9t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">S Africa Vs Sri Lanka -highest Opening Partnership</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>South Africa</td><td>4/7</td><td>-175</td></tr><tr><td>Sri Lanka</td><td>6/5</td><td>+120</td></tr><tr><td>Tie</td><td>32/1</td><td>+3200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o10t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">South Africa Vs Sri Lanka - Most Run Outs</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>South Africa</td><td>23/10</td><td>+230</td></tr><tr><td>Sri Lanka</td><td>14/5</td><td>+280</td></tr><tr><td>Tie</td><td>10/13</td><td>-130</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');addSort('o6t');addSort('o7t');addSort('o8t');addSort('o9t');addSort('o10t');</script>
{/literal}	
	