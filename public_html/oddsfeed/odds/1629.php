	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--poland Ekstraklasa">
			<caption>Live--poland Ekstraklasa</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 12, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--poland Ekstraklasa  - May 12</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jagiellonia Bialystok</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" >4900</div>
								 <div class="boxdata" >+1<br /> ( +129)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gks Piast Gliwice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -250 )</div>
								 <div class="boxdata" >-1249</div>
								 <div class="boxdata" >-1<br /> ( -200)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	