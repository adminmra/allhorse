	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Ukraine Premier League.">
			<caption>Ukraine Premier League.</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 3, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Ukraine Premier League.  - Dec 03</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Oleksandria<br /> - vs - <br /> Vorskla</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+178</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2-110 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+175</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2-120</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Chornomorets<br /> - vs - <br /> Dynamo Kiev</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+1188</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-145 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-610</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+115</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	