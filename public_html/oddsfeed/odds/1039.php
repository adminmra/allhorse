	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Cbb - Ncaa Conference Tournaments - To Win">
			<caption>Cbb - Ncaa Conference Tournaments - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 16, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Cbb - Ncaa Conference Tournaments - To Win  - Mar 16					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Ivy League - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yale</td><td>4/7</td><td>-175</td></tr><tr><td>Penn </td><td>7/2</td><td>+350</td></tr><tr><td>Harvard</td><td>7/2</td><td>+350</td></tr><tr><td>Princeton</td><td>12/1</td><td>+1200</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	