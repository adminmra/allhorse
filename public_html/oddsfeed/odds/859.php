	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Italy Serie A - To Win">
			<caption>Soccer - Italy Serie A - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Italy Serie A - To Win  - Sep 15					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Serie A - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Juventus</td><td>2/5</td><td>-250</td></tr><tr><td>Inter</td><td>6/1</td><td>+600</td></tr><tr><td>Napoli</td><td>5/1</td><td>+500</td></tr><tr><td>Ac Milan</td><td>66/1</td><td>+6600</td></tr><tr><td>Roma</td><td>50/1</td><td>+5000</td></tr><tr><td>Lazio</td><td>50/1</td><td>+5000</td></tr><tr><td>Atalanta</td><td>66/1</td><td>+6600</td></tr><tr><td>Fiorentina</td><td>250/1</td><td>+25000</td></tr><tr><td>Torino</td><td>100/1</td><td>+10000</td></tr><tr><td>Sampdoria</td><td>500/1</td><td>+50000</td></tr><tr><td>Bologna</td><td>500/1</td><td>+50000</td></tr><tr><td>Udinese</td><td>750/1</td><td>+75000</td></tr><tr><td>Sassuolo</td><td>1000/1</td><td>+100000</td></tr><tr><td>Spal</td><td>1500/1</td><td>+150000</td></tr><tr><td>Genoa</td><td>2500/1</td><td>+250000</td></tr><tr><td>Parma</td><td>1500/1</td><td>+150000</td></tr><tr><td>Cagliari</td><td>1000/1</td><td>+100000</td></tr><tr><td>Brescia</td><td>1500/1</td><td>+150000</td></tr><tr><td>Lecce</td><td>4500/1</td><td>+450000</td></tr><tr><td>Verona</td><td>4500/1</td><td>+450000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	