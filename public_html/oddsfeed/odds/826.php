	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba Championship - Odds To Win">
			<caption>Nba Championship - Odds To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nba Championship - Odds To Win  - Oct 15					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Nba Championship - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Los Angeles Lakers</td><td>17/5</td><td>+340</td></tr><tr><td>Toronto Raptors</td><td>55/1</td><td>+5500</td></tr><tr><td>Milwaukee Bucks</td><td>53/10</td><td>+530</td></tr><tr><td>Houston Rockets</td><td>97/10</td><td>+970</td></tr><tr><td>Golden State Warriors</td><td>9/1</td><td>+900</td></tr><tr><td>Los Angeles Clippers</td><td>39/10</td><td>+390</td></tr><tr><td>Philadelphia 76ers</td><td>15/2</td><td>+750</td></tr><tr><td>Denver Nuggets</td><td>15/1</td><td>+1500</td></tr><tr><td>Brooklyn Nets</td><td>50/1</td><td>+5000</td></tr><tr><td>Boston Celtics</td><td>25/1</td><td>+2500</td></tr><tr><td>New York Knicks</td><td>600/1</td><td>+60000</td></tr><tr><td>Utah Jazz</td><td>14/1</td><td>+1400</td></tr><tr><td>Oklahoma City Thunder</td><td>420/1</td><td>+42000</td></tr><tr><td>Portland Trail Blazers</td><td>26/1</td><td>+2600</td></tr><tr><td>Dallas Mavericks</td><td>90/1</td><td>+9000</td></tr><tr><td>New Orleans Pelicans</td><td>100/1</td><td>+10000</td></tr><tr><td>San Antonio Spurs</td><td>80/1</td><td>+8000</td></tr><tr><td>Indiana Pacers</td><td>33/1</td><td>+3300</td></tr><tr><td>Atlanta Hawks</td><td>275/1</td><td>+27500</td></tr><tr><td>Orlando Magic</td><td>185/1</td><td>+18500</td></tr><tr><td>Sacramento Kings</td><td>250/1</td><td>+25000</td></tr><tr><td>Washington Wizards</td><td>800/1</td><td>+80000</td></tr><tr><td>Chicago Bulls</td><td>500/1</td><td>+50000</td></tr><tr><td>Memphis Grizzlies</td><td>450/1</td><td>+45000</td></tr><tr><td>Minnesota Timberwolves</td><td>365/1</td><td>+36500</td></tr><tr><td>Detroit Pistons</td><td>300/1</td><td>+30000</td></tr><tr><td>Charlotte Hornets</td><td>1200/1</td><td>+120000</td></tr><tr><td>Miami Heat</td><td>90/1</td><td>+9000</td></tr><tr><td>Phoenix Suns</td><td>560/1</td><td>+56000</td></tr><tr><td>Cleveland Cavaliers</td><td>800/1</td><td>+80000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	