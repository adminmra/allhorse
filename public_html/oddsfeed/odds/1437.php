	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mlb - Specials">
			<caption>Mlb - Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 28, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Mlb - Specials  - Mar 28					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Outcome Of Bryce Harper's First Plate Appearance</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Single</td><td>3/1</td><td>+300</td></tr><tr><td>Double</td><td>10/1</td><td>+1000</td></tr><tr><td>Triple</td><td>90/1</td><td>+9000</td></tr><tr><td>Hr</td><td>10/1</td><td>+1000</td></tr><tr><td>Bb</td><td>3/1</td><td>+300</td></tr><tr><td>Ibb</td><td>30/1</td><td>+3000</td></tr><tr><td>So</td><td>2/1</td><td>+200</td></tr><tr><td>Out</td><td>7/4</td><td>+175</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	