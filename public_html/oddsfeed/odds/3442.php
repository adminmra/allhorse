	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba - At&t Slam Dunk">
			<caption>Nba - At&t Slam Dunk</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 16, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nba - At&t Slam Dunk  - Feb 16					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 All Star At&t Slam Dunk - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Dennis Smith Jr (ny)</td><td>3/2</td><td>+150</td></tr><tr><td>Hamidou Diallo (okc)</td><td>4/1</td><td>+400</td></tr><tr><td>John Collins (atl)</td><td>11/5</td><td>+220</td></tr><tr><td>Miles Bridges (cha)</td><td>5/2</td><td>+250</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	