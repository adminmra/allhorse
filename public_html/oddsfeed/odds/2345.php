	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--netherlands Knvb Beker">
			<caption>Pre--netherlands Knvb Beker</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 20, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Pre--netherlands Knvb Beker  - Dec 20</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Vitesse Arnhem<br /> - vs - <br /> Kozakken Boys</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-2<br /> (-129)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-526</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o3&frac34;-121 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">2<br /> (-102)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+1034</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u3&frac34;-108</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Fc Utrecht<br /> - vs - <br /> Feyenoord Rotterdam</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1<br /> (-116)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+407</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o3&frac14;-107 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1<br /> (-114)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-175</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u3&frac14;-123</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	