	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Pegasus World Cup">
			<caption>Horses - Pegasus World Cup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated January 25, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center">
					Horses - Pegasus World Cup  - Jan 26					</th>
			</tr>
            
	<tr><th colspan="3" class="center">Pegasus World Cup</th></tr><tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Accelerate</td><td>8/5</td><td>+160</td></tr><tr><td>Audible</td><td>9/1</td><td>+900</td></tr><tr><td>City Of Light</td><td>11/5</td><td>+220</td></tr><tr><td>Gunnevera</td><td>15/2</td><td>+750</td></tr><tr><td>Seeking The Soul</td><td>16/1</td><td>+1600</td></tr><tr><td>Bravazo</td><td>11/1</td><td>+1100</td></tr><tr><td>Patternrecognition</td><td>8/1</td><td>+800</td></tr><tr><td>Something Awesome</td><td>33/1</td><td>+3300</td></tr><tr><td>Tom's D'etat</td><td>66/1</td><td>+6600</td></tr><tr><td>True Timber</td><td>40/1</td><td>+4000</td></tr><tr><td>Kukulakan</td><td>100/1</td><td>+10000</td></tr><tr><td>Imperative</td><td>100/1</td><td>+10000</td></tr>			</tbody>
		</table>
	</div>

	