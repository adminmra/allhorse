	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nfl - Afc Divisions - To Win">
			<caption>Nfl - Afc Divisions - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nfl - Afc Divisions - To Win  - Sep 19					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Afc South Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Houston Texans</td><td>7/4</td><td>+175</td></tr><tr><td>Jacksonville Jaguars</td><td>7/1</td><td>+700</td></tr><tr><td>Indianapolis Colts</td><td>2/1</td><td>+200</td></tr><tr><td>Tennessee Titans</td><td>5/2</td><td>+250</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					Nfl - Afc Divisions - To Win  - Sep 22					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Afc West Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Kansas City Chiefs</td><td>1/4</td><td>-400</td></tr><tr><td>Los Angeles Chargers</td><td>3/1</td><td>+300</td></tr><tr><td>Denver Broncos</td><td>33/1</td><td>+3300</td></tr><tr><td>Oakland Raiders</td><td>20/1</td><td>+2000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	