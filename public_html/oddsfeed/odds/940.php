	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Racing - Nascar Truck Series">
			<caption>Racing - Nascar Truck Series</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 27, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Racing - Nascar Truck Series  - Jun 27					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Camping World 225 - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Brett Moffitt</td><td>5/1</td><td>+500</td></tr><tr><td>Johnny Sauter</td><td>6/1</td><td>+600</td></tr><tr><td>Matt Crafton</td><td>6/1</td><td>+600</td></tr><tr><td>Stewart Friesen</td><td>6/1</td><td>+600</td></tr><tr><td>Ben Rhodes</td><td>8/1</td><td>+800</td></tr><tr><td>Ross Chastain</td><td>8/1</td><td>+800</td></tr><tr><td>Brandon Jones</td><td>12/1</td><td>+1200</td></tr><tr><td>Grant Enfinger</td><td>12/1</td><td>+1200</td></tr><tr><td>Todd Gilliland</td><td>12/1</td><td>+1200</td></tr><tr><td>Harrison Burton</td><td>14/1</td><td>+1400</td></tr><tr><td>Sheldon Creed</td><td>22/1</td><td>+2200</td></tr><tr><td>Austin Hill</td><td>40/1</td><td>+4000</td></tr><tr><td>Tyler Ankrum</td><td>40/1</td><td>+4000</td></tr><tr><td>Anthony Alfredo</td><td>100/1</td><td>+10000</td></tr><tr><td>Dylan Lupton</td><td>100/1</td><td>+10000</td></tr><tr><td>Gus Dean</td><td>250/1</td><td>+25000</td></tr><tr><td>Spencer Boyd</td><td>250/1</td><td>+25000</td></tr><tr><td>Tyler Dippel</td><td>250/1</td><td>+25000</td></tr><tr><td>Natalie Decker</td><td>500/1</td><td>+50000</td></tr><tr><td>Field</td><td>22/1</td><td>+2200</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	