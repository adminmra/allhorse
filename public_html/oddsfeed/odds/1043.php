	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Boxing - Props">
			<caption>Boxing - Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Boxing - Props  - Dec 31					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">F Mayweather Vs M Pacquiao - Final Result</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Floyd Mayweather By Ko, Tko Or Dq</td><td>9/2</td><td>+450</td></tr><tr><td>Floyd Mayweather By Decision</td><td>20/27</td><td>-135</td></tr><tr><td>Manny Pacquiao By Ko, Tko Or Dq</td><td>19/4</td><td>+475</td></tr><tr><td>Manny Pacquiao By Decision</td><td>4/1</td><td>+400</td></tr><tr><td>Draw</td><td>9/1</td><td>+900</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Mayweather Vs Pacquao- Will Fight Go The Distance?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>5/13</td><td>-260</td></tr><tr><td>No</td><td>2/1</td><td>+200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">F Mayweather Vs M Pacquao - Round Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Floyd Mayweather Wins In Round 1</td><td>30/1</td><td>+3000</td></tr><tr><td>Floyd Mayweather Wins In Round 2</td><td>36/1</td><td>+3600</td></tr><tr><td>Floyd Mayweather Wins In Round 3</td><td>36/1</td><td>+3600</td></tr><tr><td>Floyd Mayweather Wins In Round 4</td><td>36/1</td><td>+3600</td></tr><tr><td>Floyd Mayweather Wins In Round 5</td><td>36/1</td><td>+3600</td></tr><tr><td>Floyd Mayweather Wins In Round 6</td><td>36/1</td><td>+3600</td></tr><tr><td>Floyd Mayweather Wins In Round 7</td><td>36/1</td><td>+3600</td></tr><tr><td>Floyd Mayweather Wins In Round 8</td><td>36/1</td><td>+3600</td></tr><tr><td>Floyd Mayweather Wins In Round 9</td><td>36/1</td><td>+3600</td></tr><tr><td>Floyd Mayweather Wins In Round 10</td><td>36/1</td><td>+3600</td></tr><tr><td>Floyd Mayweather Wins In Round 11</td><td>36/1</td><td>+3600</td></tr><tr><td>Floyd Mayweather Wins In Round 12</td><td>40/1</td><td>+4000</td></tr><tr><td>Floyd Mayweather By Decision</td><td>20/27</td><td>-135</td></tr><tr><td>Manny Pacquiao Wins In Round 1</td><td>33/1</td><td>+3300</td></tr><tr><td>Manny Pacquiao Wins In Round 2</td><td>40/1</td><td>+4000</td></tr><tr><td>Manny Pacquiao Wins In Round 3</td><td>40/1</td><td>+4000</td></tr><tr><td>Manny Pacquiao Wins In Round 4</td><td>40/1</td><td>+4000</td></tr><tr><td>Manny Pacquiao Wins In Round 5</td><td>40/1</td><td>+4000</td></tr><tr><td>Manny Pacquiao Wins In Round 6</td><td>40/1</td><td>+4000</td></tr><tr><td>Manny Pacquiao Wins In Round 7</td><td>40/1</td><td>+4000</td></tr><tr><td>Manny Pacquiao Wins In Round 8</td><td>40/1</td><td>+4000</td></tr><tr><td>Manny Pacquiao Wins In Round 9</td><td>40/1</td><td>+4000</td></tr><tr><td>Manny Pacquiao Wins In Round 10</td><td>40/1</td><td>+4000</td></tr><tr><td>Manny Pacquiao Wins In Round 11</td><td>40/1</td><td>+4000</td></tr><tr><td>Manny Pacquiao Wins In Round 12</td><td>40/1</td><td>+4000</td></tr><tr><td>Manny Pacquiao By Decision</td><td>4/1</td><td>+400</td></tr><tr><td>Draw</td><td>9/1</td><td>+900</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');</script>
{/literal}	
	