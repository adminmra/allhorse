	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--england League 2">
			<caption>Pre--england League 2</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--england League 2  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cheltenham Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -109 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Plymouth Argyle</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -126 )</div>
								 <div class="boxdata" >-111</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stevenage Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >266</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Forest Green Rovers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >-103</div>
								 <div class="boxdata" >-&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Exeter City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >191</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Newport County Afc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >137</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Leyton Orient</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >312</div>
								 <div class="boxdata" >+&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Colchester United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >-125</div>
								 <div class="boxdata" >-&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Swindon Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cambridge United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >218</div>
								 <div class="boxdata" >+&frac12;<br /> ( -153 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Crawley Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >235</div>
								 <div class="boxdata" >+&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Northampton Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac12;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Scunthorpe United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >223</div>
								 <div class="boxdata" >+&frac12;<br /> ( -153 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Walsall Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Carlisle United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >309</div>
								 <div class="boxdata" >+&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bradford City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >-133</div>
								 <div class="boxdata" >-&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Morecambe Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac12;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Oldham Athletic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >-108</div>
								 <div class="boxdata" >-&frac12;<br /> ( -108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Salford City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >243</div>
								 <div class="boxdata" >+&frac12;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Crewe Alexandra</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >-108</div>
								 <div class="boxdata" >-&frac12;<br /> ( -108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Macclesfield Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >269</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Grimsby Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >-109</div>
								 <div class="boxdata" >-&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mansfield Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >168</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Port Vale</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >148</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	