	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - Pga Championship">
			<caption>Golf - Pga Championship</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - Pga Championship  - May 14					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Pga Championship - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Brooks Koepka</td><td>7/1</td><td>+700</td></tr><tr><td>Dustin Johnson</td><td>9/1</td><td>+900</td></tr><tr><td>Rory Mcilroy</td><td>10/1</td><td>+1000</td></tr><tr><td>Alexander Noren</td><td>150/1</td><td>+15000</td></tr><tr><td>Tiger Woods</td><td>12/1</td><td>+1200</td></tr><tr><td>Justin Rose</td><td>16/1</td><td>+1600</td></tr><tr><td>Justin Thomas</td><td>16/1</td><td>+1600</td></tr><tr><td>Jordan Spieth</td><td>16/1</td><td>+1600</td></tr><tr><td>Jon Rahm</td><td>18/1</td><td>+1800</td></tr><tr><td>Rickie Fowler</td><td>22/1</td><td>+2200</td></tr><tr><td>Jason Day</td><td>25/1</td><td>+2500</td></tr><tr><td>Francesco Molinari</td><td>28/1</td><td>+2800</td></tr><tr><td>Hideki Matsuyama</td><td>33/1</td><td>+3300</td></tr><tr><td>Tony Finau</td><td>33/1</td><td>+3300</td></tr><tr><td>Bryson Dechambeau</td><td>33/1</td><td>+3300</td></tr><tr><td>Tommy Fleetwood</td><td>33/1</td><td>+3300</td></tr><tr><td>Xander Schauffele</td><td>33/1</td><td>+3300</td></tr><tr><td>Patrick Cantlay</td><td>33/1</td><td>+3300</td></tr><tr><td>Paul Casey</td><td>40/1</td><td>+4000</td></tr><tr><td>Adam Scott</td><td>50/1</td><td>+5000</td></tr><tr><td>Gary Woodland</td><td>50/1</td><td>+5000</td></tr><tr><td>Matt Kuchar</td><td>50/1</td><td>+5000</td></tr><tr><td>Marc Leishman</td><td>60/1</td><td>+6000</td></tr><tr><td>Patrick Reed</td><td>66/1</td><td>+6600</td></tr><tr><td>Henrik Stenson</td><td>66/1</td><td>+6600</td></tr><tr><td>Sergio Garcia</td><td>66/1</td><td>+6600</td></tr><tr><td>Phil Mickelson</td><td>66/1</td><td>+6600</td></tr><tr><td>Louis Oosthuizen</td><td>66/1</td><td>+6600</td></tr><tr><td>Bubba Watson</td><td>66/1</td><td>+6600</td></tr><tr><td>Matt Wallace</td><td>75/1</td><td>+7500</td></tr><tr><td>Ian Poulter</td><td>80/1</td><td>+8000</td></tr><tr><td>Tyrrell Hatton</td><td>80/1</td><td>+8000</td></tr><tr><td>Cameron Smith</td><td>100/1</td><td>+10000</td></tr><tr><td>Kevin Kisner</td><td>100/1</td><td>+10000</td></tr><tr><td>Haotong Li</td><td>100/1</td><td>+10000</td></tr><tr><td>Matthew Fitzpatrick</td><td>100/1</td><td>+10000</td></tr><tr><td>Jason Kokrak</td><td>110/1</td><td>+11000</td></tr><tr><td>Brandt Snedeker</td><td>125/1</td><td>+12500</td></tr><tr><td>Luke List</td><td>125/1</td><td>+12500</td></tr><tr><td>Jim Furyk</td><td>125/1</td><td>+12500</td></tr><tr><td>Shane Lowry</td><td>66/1</td><td>+6600</td></tr><tr><td>Rafael Cabrera Bello</td><td>125/1</td><td>+12500</td></tr><tr><td>Charles Howell Iii</td><td>125/1</td><td>+12500</td></tr><tr><td>Emiliano Grillo</td><td>125/1</td><td>+12500</td></tr><tr><td>Keegan Bradley</td><td>125/1</td><td>+12500</td></tr><tr><td>Aaron Wise</td><td>125/1</td><td>+12500</td></tr><tr><td>Sungjae Im</td><td>140/1</td><td>+14000</td></tr><tr><td>Billy Horschel</td><td>140/1</td><td>+14000</td></tr><tr><td>Eddie Pepperell</td><td>150/1</td><td>+15000</td></tr><tr><td>Russell Knox</td><td>150/1</td><td>+15000</td></tr><tr><td>Joost Luiten</td><td>150/1</td><td>+15000</td></tr><tr><td>Thorbjorn Olesen</td><td>150/1</td><td>+15000</td></tr><tr><td>Lucas Bjerregaard</td><td>150/1</td><td>+15000</td></tr><tr><td>Keith Mitchell</td><td>150/1</td><td>+15000</td></tr><tr><td>Byeong Hun An</td><td>150/1</td><td>+15000</td></tr><tr><td>Branden Grace</td><td>150/1</td><td>+15000</td></tr><tr><td>Charley Hoffman</td><td>150/1</td><td>+15000</td></tr><tr><td>Scott Piercy</td><td>150/1</td><td>+15000</td></tr><tr><td>Kiradech Aphibarnrat</td><td>175/1</td><td>+17500</td></tr><tr><td>Adam Hadwin</td><td>175/1</td><td>+17500</td></tr><tr><td>J B Holmes</td><td>125/1</td><td>+12500</td></tr><tr><td>Kyle Stanley</td><td>175/1</td><td>+17500</td></tr><tr><td>Kevin Na</td><td>175/1</td><td>+17500</td></tr><tr><td>Abraham Ancer</td><td>175/1</td><td>+17500</td></tr><tr><td>Andrew Putnam</td><td>150/1</td><td>+15000</td></tr><tr><td>Ryan Palmer</td><td>200/1</td><td>+20000</td></tr><tr><td>Justin Harding</td><td>200/1</td><td>+20000</td></tr><tr><td>Sung Kang</td><td>200/1</td><td>+20000</td></tr><tr><td>Jazz Janewattananond</td><td>200/1</td><td>+20000</td></tr><tr><td>C T Pan</td><td>200/1</td><td>+20000</td></tr><tr><td>Jorge Campillo</td><td>200/1</td><td>+20000</td></tr><tr><td>Chez Reavie</td><td>200/1</td><td>+20000</td></tr><tr><td>Tom Lewis</td><td>250/1</td><td>+25000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	