	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mlb - To Record The Most Wins">
			<caption>Mlb - To Record The Most Wins</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 26, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Mlb - To Record The Most Wins  - Mar 26					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Pitcher To Record The Most Wins</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Max Scherzer</td><td>8/1</td><td>+800</td></tr><tr><td>Corey Kluber</td><td>8/1</td><td>+800</td></tr><tr><td>Luis Severino</td><td>8/1</td><td>+800</td></tr><tr><td>Chris Sale</td><td>8/1</td><td>+800</td></tr><tr><td>Justin Verlander</td><td>16/1</td><td>+1600</td></tr><tr><td>Jacob Degrom</td><td>16/1</td><td>+1600</td></tr><tr><td>Blake Snell</td><td>25/1</td><td>+2500</td></tr><tr><td>Walker Buehler</td><td>25/1</td><td>+2500</td></tr><tr><td>Trevor Bauer</td><td>25/1</td><td>+2500</td></tr><tr><td>Clayton Kershaw</td><td>25/1</td><td>+2500</td></tr><tr><td>Patrick Corbin</td><td>30/1</td><td>+3000</td></tr><tr><td>Aaron Nola</td><td>30/1</td><td>+3000</td></tr><tr><td>Carlos Carrasco</td><td>30/1</td><td>+3000</td></tr><tr><td>Gerrit Cole</td><td>30/1</td><td>+3000</td></tr><tr><td>James Paxton</td><td>30/1</td><td>+3000</td></tr><tr><td>Kyle Freeland</td><td>30/1</td><td>+3000</td></tr><tr><td>Madison Bumgarner</td><td>30/1</td><td>+3000</td></tr><tr><td>Noah Syndergaard</td><td>30/1</td><td>+3000</td></tr><tr><td>David Price</td><td>40/1</td><td>+4000</td></tr><tr><td>Masahiro Tanaka</td><td>40/1</td><td>+4000</td></tr><tr><td>Dallas Keuchel</td><td>66/1</td><td>+6600</td></tr><tr><td>Zack Greinke</td><td>66/1</td><td>+6600</td></tr><tr><td>Chris Archer</td><td>66/1</td><td>+6600</td></tr><tr><td>Stephen Strasburg</td><td>66/1</td><td>+6600</td></tr><tr><td>Jon Lester</td><td>66/1</td><td>+6600</td></tr><tr><td>J.a. Happ</td><td>80/1</td><td>+8000</td></tr><tr><td>Marcus Stroman</td><td>80/1</td><td>+8000</td></tr><tr><td>Rick Porcello</td><td>80/1</td><td>+8000</td></tr><tr><td>Jake Arrieta</td><td>80/1</td><td>+8000</td></tr><tr><td>Miles Mikolas</td><td>100/1</td><td>+10000</td></tr><tr><td>Felix Hernandez</td><td>250/1</td><td>+25000</td></tr><tr><td>Cc Sabathia</td><td>250/1</td><td>+25000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	