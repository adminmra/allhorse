	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Tennis Props">
			<caption>Tennis Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Tennis Props  - Feb 10					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">M. Fucsovics Vs D. Medvedev - 1st Set Game Score</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Marton Fucsovics 6 - 0</td><td>250/1</td><td>+25000</td></tr><tr><td>Marton Fucsovics 6 - 1</td><td>80/1</td><td>+8000</td></tr><tr><td>Marton Fucsovics 6 - 2</td><td>40/1</td><td>+4000</td></tr><tr><td>Marton Fucsovics 6 - 3</td><td>18/1</td><td>+1800</td></tr><tr><td>Marton Fucsovics 6 - 4</td><td>10/1</td><td>+1000</td></tr><tr><td>Marton Fucsovics 7 - 5</td><td>33/1</td><td>+3300</td></tr><tr><td>Marton Fucsovics 7 - 6</td><td>8/1</td><td>+800</td></tr><tr><td>Danil Medvedev 6 - 0</td><td>40/1</td><td>+4000</td></tr><tr><td>Danil Medvedev 6 - 1</td><td>12/1</td><td>+1200</td></tr><tr><td>Danil Medvedev 6 - 2</td><td>8/1</td><td>+800</td></tr><tr><td>Danil Medvedev 6 - 3</td><td>3/1</td><td>+300</td></tr><tr><td>Danil Medvedev 6 - 4</td><td>37/10</td><td>+370</td></tr><tr><td>Danil Medvedev 7 - 5</td><td>11/1</td><td>+1100</td></tr><tr><td>Danil Medvedev 7 - 6</td><td>4/1</td><td>+400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">M. Fucsovics Vs D. Medvedev - Set Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Marton Fucsovics 2 - 0</td><td>7/1</td><td>+700</td></tr><tr><td>Marton Fucsovics 2 - 1</td><td>9/1</td><td>+900</td></tr><tr><td>Danil Medvedev 2 - 0</td><td>1/2</td><td>-200</td></tr><tr><td>Danil Medvedev 2 - 1</td><td>33/10</td><td>+330</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	