	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Switzerland Super League">
			<caption>Switzerland Super League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated November 30, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Switzerland Super League  - Dec 01</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">St. Gallen<br /> - vs - <br /> Thun</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+248</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o3-125 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (-105)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-105</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u3+105</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Luzern<br /> - vs - <br /> Fc Sion</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-140)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+201</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o3-105 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+119</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u3-115</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Switzerland Super League  - Dec 02</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Neuchatel Xamax<br /> - vs - <br /> Lugano</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-110)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+268</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o3+110 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (-110)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-111</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u3-130</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Young Boys<br /> - vs - <br /> Fc Basel</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-140)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+118</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o3&frac12;+115 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+181</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u3&frac12;-135</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Grasshopper<br /> - vs - <br /> Zurich</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (130)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+369</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-140 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (-150)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-156</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+120</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	