	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--morocco Botola">
			<caption>Pre--morocco Botola</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 11, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--morocco Botola  - Jun 11</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mouloudia Club Of Oujda</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" >+&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olympique Club Of Khouribga</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >136</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rapide Oued Zem</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >246</div>
								 <div class="boxdata" >+&frac14;<br /> ( -108 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rsb Berkane</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	