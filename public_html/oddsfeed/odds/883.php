	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba - Taco Bell Skills Challenge">
			<caption>Nba - Taco Bell Skills Challenge</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 16, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nba - Taco Bell Skills Challenge  - Feb 16					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 All Star Taco Bell Skills Challenge - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>De'aaron Fox (sac)</td><td>5/2</td><td>+250</td></tr><tr><td>Jayson Tatum (bos)</td><td>6/1</td><td>+600</td></tr><tr><td>Kyle Kuzma (lal)</td><td>7/1</td><td>+700</td></tr><tr><td>Luka Doncic (dal)</td><td>7/2</td><td>+350</td></tr><tr><td>Mike Conley (mem)</td><td>5/1</td><td>+500</td></tr><tr><td>Nikola Jokic (den)</td><td>5/1</td><td>+500</td></tr><tr><td>Nikola Vucevic (orl)</td><td>8/1</td><td>+800</td></tr><tr><td>Trae Young (atl)</td><td>37/10</td><td>+370</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	