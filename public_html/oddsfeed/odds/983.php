	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Preakness Stakes">
			<caption>Horses - Preakness Stakes</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Preakness Stakes  - May 18					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Preakness Stakes - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Improbable</td><td>19/10</td><td>+190</td></tr><tr><td>War Of Will</td><td>33/10</td><td>+330</td></tr><tr><td>Alwaysmining</td><td>7/1</td><td>+700</td></tr><tr><td>Bourbon War</td><td>17/2</td><td>+850</td></tr><tr><td>Anothertwistafate</td><td>9/1</td><td>+900</td></tr><tr><td>Owendale</td><td>14/1</td><td>+1400</td></tr><tr><td>Warrior's Charge</td><td>14/1</td><td>+1400</td></tr><tr><td>Win Win Win</td><td>15/1</td><td>+1500</td></tr><tr><td>Signalman</td><td>18/1</td><td>+1800</td></tr><tr><td>Bodexpress</td><td>18/1</td><td>+1800</td></tr><tr><td>Laughing Fox</td><td>28/1</td><td>+2800</td></tr><tr><td>Market King</td><td>33/1</td><td>+3300</td></tr><tr><td>Everfast</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	