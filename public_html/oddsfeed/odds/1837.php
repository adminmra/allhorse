	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba - Alternate Line">
			<caption>Nba - Alternate Line</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 13, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Nba - Alternate Line  - Jun 13</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Toronto Raptors</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o203&frac12;<br /> ( -220 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-5&frac12;<br /> ( +230 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Golden State Warriors</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u203&frac12;<br /> ( +180 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+5&frac12;<br /> ( -290 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Toronto Raptors</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o219&frac12;<br /> ( +180 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+10&frac12;<br /> ( -350 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Golden State Warriors</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u219&frac12;<br /> ( -220 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-10&frac12;<br /> ( +275 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	