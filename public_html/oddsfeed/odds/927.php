	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Breeders Cup Classic - To Win">
			<caption>Horses - Breeders Cup Classic - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Breeders Cup Classic - To Win  - Nov 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Breeeders Cup Classic - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Maximum Security</td><td>9/1</td><td>+900</td></tr><tr><td>Mckinzie</td><td>5/1</td><td>+500</td></tr><tr><td>Thunder Snow</td><td>10/1</td><td>+1000</td></tr><tr><td>Game Winner</td><td>10/1</td><td>+1000</td></tr><tr><td>Omaha Beach</td><td>10/1</td><td>+1000</td></tr><tr><td>Bravazo</td><td>16/1</td><td>+1600</td></tr><tr><td>Catholic Boy</td><td>20/1</td><td>+2000</td></tr><tr><td>Yoshida</td><td>25/1</td><td>+2500</td></tr><tr><td>Audible</td><td>25/1</td><td>+2500</td></tr><tr><td>Core Beliefs</td><td>33/1</td><td>+3300</td></tr><tr><td>Hofburg</td><td>25/1</td><td>+2500</td></tr><tr><td>Improbable</td><td>14/1</td><td>+1400</td></tr><tr><td>Catalina Cruiser</td><td>14/1</td><td>+1400</td></tr><tr><td>Wissahickon</td><td>25/1</td><td>+2500</td></tr><tr><td>Intrepid Heart</td><td>33/1</td><td>+3300</td></tr><tr><td>Hidden Scroll</td><td>33/1</td><td>+3300</td></tr><tr><td>Inti</td><td>33/1</td><td>+3300</td></tr><tr><td>Quorto</td><td>50/1</td><td>+5000</td></tr><tr><td>Comical Ghost</td><td>50/1</td><td>+5000</td></tr><tr><td>Vino Rosso</td><td>25/1</td><td>+2500</td></tr><tr><td>Tacitus</td><td>14/1</td><td>+1400</td></tr><tr><td>War Of Will</td><td>25/1</td><td>+2500</td></tr><tr><td>Code Of Honor</td><td>8/1</td><td>+800</td></tr><tr><td>Joevia</td><td>33/1</td><td>+3300</td></tr><tr><td>Sir Winston</td><td>20/1</td><td>+2000</td></tr><tr><td>Seeking The Soul</td><td>25/1</td><td>+2500</td></tr><tr><td>Preservationist</td><td>16/1</td><td>+1600</td></tr><tr><td>Tom's D'etat</td><td>100/1</td><td>+10000</td></tr><tr><td>Gunnevera</td><td>33/1</td><td>+3300</td></tr><tr><td>Higher Power</td><td>16/1</td><td>+1600</td></tr><tr><td>Elate</td><td>14/1</td><td>+1400</td></tr><tr><td>Tax</td><td>40/1</td><td>+4000</td></tr><tr><td>Shancelot</td><td>25/1</td><td>+2500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	