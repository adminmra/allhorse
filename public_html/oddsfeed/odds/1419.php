	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Argentina Torneo Super 20">
			<caption>Argentina Torneo Super 20</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 21, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Argentina Torneo Super 20  - Dec 21</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Comunicaciones Mercedes<br /> - vs - <br /> San Lorenzo</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">17<br /> (-105)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o163-110 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-17<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u163-110</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Quimsa<br /> - vs - <br /> Obras Sanitarias</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-3<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o167&frac12;-110 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">3<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u167&frac12;-110</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	