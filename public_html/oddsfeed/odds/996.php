	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Us Presidential Election">
			<caption>Us Presidential Election</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Us Presidential Election  - Sep 30					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Us Presidential Election 2020 - Democrat Candidate</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Joe Biden</td><td>5/2</td><td>+250</td></tr><tr><td>Bernie Sanders</td><td>11/2</td><td>+550</td></tr><tr><td>Beto O Rourke</td><td>16/1</td><td>+1600</td></tr><tr><td>Kamala Harris</td><td>12/1</td><td>+1200</td></tr><tr><td>Andrew Yang</td><td>11/1</td><td>+1100</td></tr><tr><td>Elizabeth Warren</td><td>3/2</td><td>+150</td></tr><tr><td>Amy Klobuchar</td><td>75/1</td><td>+7500</td></tr><tr><td>Cory Booker</td><td>42/1</td><td>+4200</td></tr><tr><td>Tulsi Gabbard</td><td>60/1</td><td>+6000</td></tr><tr><td>Julian Castro</td><td>165/1</td><td>+16500</td></tr><tr><td>Pete Buttigieg</td><td>20/1</td><td>+2000</td></tr><tr><td>Bill De Blasio</td><td>400/1</td><td>+40000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Us Presidential Election  - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Donald Trump</td><td>EV</td><td>EV</td></tr><tr><td>Bernie Sanders</td><td>12/1</td><td>+1200</td></tr><tr><td>Kamala Harris</td><td>21/1</td><td>+2100</td></tr><tr><td>Pete Buttigieg</td><td>37/1</td><td>+3700</td></tr><tr><td>Beto O`rourke</td><td>30/1</td><td>+3000</td></tr><tr><td>Joe Biden</td><td>24/5</td><td>+480</td></tr><tr><td>Andrew Yang</td><td>22/1</td><td>+2200</td></tr><tr><td>Elizabeth Warren</td><td>3/1</td><td>+300</td></tr><tr><td>Tulsi Gabbard</td><td>110/1</td><td>+11000</td></tr><tr><td>Amy Klobuchar</td><td>140/1</td><td>+14000</td></tr><tr><td>Cory Booker</td><td>100/1</td><td>+10000</td></tr><tr><td>Mike Pence</td><td>175/1</td><td>+17500</td></tr><tr><td>Julian Castro</td><td>300/1</td><td>+30000</td></tr><tr><td>John Kasich</td><td>800/1</td><td>+80000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	