	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Serbia - Super Liga">
			<caption>Serbia - Super Liga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated November 30, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Serbia - Super Liga  - Nov 30</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Napredak<br /> - vs - <br /> Backa Palanka</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+110</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+120 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+240</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-150</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	