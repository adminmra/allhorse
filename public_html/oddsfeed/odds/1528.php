	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf Props">
			<caption>Golf Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 12, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf Props  - Jun 13					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will There Be A Hole In One In The 2019 Us Open?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>2/3</td><td>-150</td></tr><tr><td>No</td><td>11/10</td><td>+110</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">What Will Be The Nationality Of The Us Open Winner</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Usa</td><td>2/3</td><td>-150</td></tr><tr><td>European</td><td>8/5</td><td>+160</td></tr><tr><td>Rest Of The World</td><td>15/2</td><td>+750</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will There Be A Playoff In The 2019 Us Open</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>3/1</td><td>+300</td></tr><tr><td>No</td><td>1/4</td><td>-400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Us Open Winning Margin</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Won In Playoff</td><td>3/1</td><td>+300</td></tr><tr><td>1 Stroke Exactly</td><td>5/2</td><td>+250</td></tr><tr><td>2 Strokes Exactly</td><td>7/2</td><td>+350</td></tr><tr><td>3 Strokes Exactly</td><td>5/1</td><td>+500</td></tr><tr><td>4 Strokes Exactly</td><td>7/1</td><td>+700</td></tr><tr><td>5 Strokes Exactly</td><td>12/1</td><td>+1200</td></tr><tr><td>6 Strokes Exactly</td><td>25/1</td><td>+2500</td></tr><tr><td>7 Strokes Exactly</td><td>35/1</td><td>+3500</td></tr><tr><td>8 Strokes Or More</td><td>22/1</td><td>+2200</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');</script>
{/literal}	
	