	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Death Matchups">
			<caption>Death Matchups</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Death Matchups  - Jan 01					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Die First?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Betty White</td><td>5/13</td><td>-260</td></tr><tr><td>Carol Burnett</td><td>2/1</td><td>+200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Die First?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Charlie Sheen</td><td>1/2</td><td>-200</td></tr><tr><td>Magic Johnson</td><td>8/5</td><td>+160</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Die First?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Jerry Lee Lewis</td><td>3/2</td><td>+150</td></tr><tr><td>Woody Allen</td><td>5/2</td><td>+250</td></tr><tr><td>Bill Cosby</td><td>EV</td><td>EV</td></tr><tr><td>Roman Polanski</td><td>5/2</td><td>+250</td></tr><tr><td>Jerry Sandusky</td><td>6/1</td><td>+600</td></tr><tr><td>Harvey Weinstein</td><td>8/1</td><td>+800</td></tr><tr><td>Subway Jared</td><td>10/1</td><td>+1000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Die First?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Regis Philbin</td><td>21/20</td><td>+105</td></tr><tr><td>Bob Barker</td><td>20/29</td><td>-145</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Die First?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Rosie O’donnell</td><td>2/3</td><td>-150</td></tr><tr><td>Rosanne Barr</td><td>11/10</td><td>+110</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Die First?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Demi Lovato</td><td>EV</td><td>EV</td></tr><tr><td>Artie Lange</td><td>5/7</td><td>-140</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Die First?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Jay Leno</td><td>6/5</td><td>+120</td></tr><tr><td>David Letterman</td><td>5/8</td><td>-160</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Die First?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Prince Phillip</td><td>5/7</td><td>-140</td></tr><tr><td>Queen Elizabeth</td><td>EV</td><td>EV</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o8t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Die First?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Donald Trump</td><td>5/6</td><td>-120</td></tr><tr><td>Vladimir Putin</td><td>5/6</td><td>-120</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o9t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Bill Cosby Dies In Prison?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Hey, Hey, Hey!</td><td>1/3</td><td>-300</td></tr><tr><td>No</td><td>19/10</td><td>+190</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o10t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Die First</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Rae Carruth</td><td>3/1</td><td>+300</td></tr><tr><td>Ray Lewis</td><td>20/1</td><td>+2000</td></tr><tr><td>Oj Simpson</td><td>1/10</td><td>-1000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');addSort('o6t');addSort('o7t');addSort('o8t');addSort('o9t');addSort('o10t');</script>
{/literal}	
	