	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Racing - Nascar - Monster Energy Championship">
			<caption>Racing - Nascar - Monster Energy Championship</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Racing - Nascar - Monster Energy Championship  - Sep 15					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Monster Energy Nascar Cup - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Kevin Harvick</td><td>9/2</td><td>+450</td></tr><tr><td>Kyle Busch</td><td>3/1</td><td>+300</td></tr><tr><td>Martin Truex Jr</td><td>11/2</td><td>+550</td></tr><tr><td>Chase Elliott</td><td>12/1</td><td>+1200</td></tr><tr><td>Kyle Larson</td><td>18/1</td><td>+1800</td></tr><tr><td>Brad Keselowski</td><td>15/2</td><td>+750</td></tr><tr><td>Joey Logano</td><td>13/2</td><td>+650</td></tr><tr><td>Ryan Blaney</td><td>28/1</td><td>+2800</td></tr><tr><td>Clint Bowyer</td><td>50/1</td><td>+5000</td></tr><tr><td>Erik Jones</td><td>16/1</td><td>+1600</td></tr><tr><td>Aric Almirola</td><td>50/1</td><td>+5000</td></tr><tr><td>Denny Hamlin</td><td>11/2</td><td>+550</td></tr><tr><td>Kurt Busch</td><td>18/1</td><td>+1800</td></tr><tr><td>Alex Bowman</td><td>25/1</td><td>+2500</td></tr><tr><td>William Byron</td><td>50/1</td><td>+5000</td></tr><tr><td>Ryan Newman</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	