	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - American Turf Stakes - To Win">
			<caption>Horses - American Turf Stakes - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - American Turf Stakes - To Win  - May 04					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">American Turf Stakes - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>A Thread Of Blue</td><td>4/1</td><td>+400</td></tr><tr><td>Social Paranoia</td><td>6/1</td><td>+600</td></tr><tr><td>Digital Age</td><td>4/1</td><td>+400</td></tr><tr><td>Marquee Prince</td><td>14/1</td><td>+1400</td></tr><tr><td>Seismic Wave</td><td>10/1</td><td>+1000</td></tr><tr><td>Forever Mo</td><td>20/1</td><td>+2000</td></tr><tr><td>Casa Creed</td><td>8/1</td><td>+800</td></tr><tr><td>Avie's Flatter</td><td>13/2</td><td>+650</td></tr><tr><td>War Film</td><td>25/1</td><td>+2500</td></tr><tr><td>Henley's Joy</td><td>13/2</td><td>+650</td></tr><tr><td>Weekly Call</td><td>33/1</td><td>+3300</td></tr><tr><td>The Black Album</td><td>9/1</td><td>+900</td></tr><tr><td>Louder Than Bombs</td><td>25/1</td><td>+2500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	