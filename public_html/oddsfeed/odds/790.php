	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nfl - Nfc Divisions - To Win">
			<caption>Nfl - Nfc Divisions - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nfl - Nfc Divisions - To Win  - Sep 22					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Nfc East Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Philadelphia Eagles</td><td>21/20</td><td>+105</td></tr><tr><td>Dallas Cowboys</td><td>4/5</td><td>-125</td></tr><tr><td>New York Giants</td><td>50/1</td><td>+5000</td></tr><tr><td>Washington Redskins</td><td>25/1</td><td>+2500</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Nfc North Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Chicago Bears</td><td>9/4</td><td>+225</td></tr><tr><td>Green Bay Packers</td><td>3/2</td><td>+150</td></tr><tr><td>Minnesota Vikings</td><td>11/4</td><td>+275</td></tr><tr><td>Detroit Lions</td><td>7/1</td><td>+700</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Nfc South Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>New Orleans Saints</td><td>4/5</td><td>-125</td></tr><tr><td>Atlanta Falcons</td><td>2/1</td><td>+200</td></tr><tr><td>Carolina Panthers</td><td>8/1</td><td>+800</td></tr><tr><td>Tampa Bay Buccaneers</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Nfc West Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Los Angles Rams</td><td>5/6</td><td>-120</td></tr><tr><td>Seattle Seahwaks</td><td>9/4</td><td>+225</td></tr><tr><td>San Fransisco 49ers</td><td>13/4</td><td>+325</td></tr><tr><td>Arizona Cardinals</td><td>50/1</td><td>+5000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');</script>
{/literal}	
	