	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - Wgc Match Betting">
			<caption>Golf - Wgc Match Betting</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 29, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - Wgc Match Betting  - Mar 29					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Wgc - Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Kevin Kisner</td><td>EV</td><td>EV</td></tr><tr><td>Keith Mitchell</td><td>6/5</td><td>+120</td></tr><tr><td>Draw</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Wgc - Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Brooks Koepka</td><td>20/21</td><td>-105</td></tr><tr><td>Alex Noren</td><td>13/10</td><td>+130</td></tr><tr><td>Draw</td><td>6/1</td><td>+600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Wgc - Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Hao Tong Li</td><td>5/7</td><td>-140</td></tr><tr><td>Tom Lewis</td><td>8/5</td><td>+160</td></tr><tr><td>Draw</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Wgc - Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Paul Casey</td><td>2/3</td><td>-150</td></tr><tr><td>Cameron Smith</td><td>9/5</td><td>+180</td></tr><tr><td>Draw</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Wgc - Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Charles Howell</td><td>5/6</td><td>-120</td></tr><tr><td>Abraham Ancer</td><td>7/5</td><td>+140</td></tr><tr><td>Draw</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Wgc - Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Francesco Molinari</td><td>2/3</td><td>-150</td></tr><tr><td>Webb Simpson</td><td>7/4</td><td>+175</td></tr><tr><td>Draw</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Wgc - Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Thorbjorn Olesen</td><td>5/7</td><td>-140</td></tr><tr><td>Satoshi Kodaira</td><td>17/10</td><td>+170</td></tr><tr><td>Draw</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Wgc - Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Jordan Spieth</td><td>5/6</td><td>-120</td></tr><tr><td>Bubba Watson</td><td>27/20</td><td>+135</td></tr><tr><td>Draw</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o8t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Wgc - Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Billy Horschel</td><td>10/11</td><td>-110</td></tr><tr><td>Kevin Na</td><td>13/10</td><td>+130</td></tr><tr><td>Draw</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o9t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Wgc - Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Justin Rose</td><td>5/6</td><td>-120</td></tr><tr><td>Gary Woodland</td><td>27/20</td><td>+135</td></tr><tr><td>Draw</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');addSort('o6t');addSort('o7t');addSort('o8t');addSort('o9t');</script>
{/literal}	
	