	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mlb - 1st 5 Full Innings">
			<caption>Mlb - 1st 5 Full Innings</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Mlb - 1st 5 Full Innings First Half Lines - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Was Nationals</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4<br /> ( -105)</div>
								 <div class="boxdata" >-180</div>
								 <div class="boxdata" >-&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Stl Cardinals</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4<br /> ( -115)</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" >+&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Ny Mets</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o7&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-150</div>
								 <div class="boxdata" >-&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Col Rockies</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u7&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >130</div>
								 <div class="boxdata" >+&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Mia Marlins</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5<br /> ( -105)</div>
								 <div class="boxdata" >130</div>
								 <div class="boxdata" >+&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Ari Diamondbacks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5<br /> ( -115)</div>
								 <div class="boxdata" >-150</div>
								 <div class="boxdata" >-&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Phi Phillies</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6<br /> ( -105)</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" >+&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Atl Braves</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6<br /> ( -115)</div>
								 <div class="boxdata" >-145</div>
								 <div class="boxdata" >-&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Sdg Padres</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >130</div>
								 <div class="boxdata" >+&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Mil Brewers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >-150</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - 1st 5 Full Innings First Half Lines - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Kan Royals</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >180</div>
								 <div class="boxdata" >+&frac12;<br /> ( +135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Oak Athletics</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-220</div>
								 <div class="boxdata" >-&frac12;<br /> ( -160 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h La Angels</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac12;<br /> ( +160 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Ny Yankees</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >-275</div>
								 <div class="boxdata" >-&frac12;<br /> ( -190 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Tor Blue Jays</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >+&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Bal Orioles</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-115</div>
								 <div class="boxdata" >-&frac12;<br /> ( +115 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Det Tigers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >230</div>
								 <div class="boxdata" >+&frac12;<br /> ( +160 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Cle Indians</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >-280</div>
								 <div class="boxdata" >-&frac12;<br /> ( -190 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Tex Rangers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >340</div>
								 <div class="boxdata" >+&frac12;<br /> ( +195 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Hou Astros</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5<br /> ( -120)</div>
								 <div class="boxdata" >-440</div>
								 <div class="boxdata" >-&frac12;<br /> ( -245 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - 1st 5 Full Innings First Half Lines - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Sea Mariners</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >+&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Pit Pirates</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >-120</div>
								 <div class="boxdata" >-&frac12;<br /> ( +115 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Sfo Giants</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" >+&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Bos Red Sox</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-160</div>
								 <div class="boxdata" >-&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Tam Rays</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >+&frac12;<br /> ( -130 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h La Dodgers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5<br /> ( -120)</div>
								 <div class="boxdata" >-130</div>
								 <div class="boxdata" >-&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	