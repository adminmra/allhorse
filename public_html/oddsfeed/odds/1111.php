	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Kentucky Derby Jockey">
			<caption>Horses - Kentucky Derby Jockey</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Kentucky Derby Jockey  - May 04					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Kentucky Derby Jockey - Odds To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tyler Gaffalione</td><td>16/1</td><td>+1600</td></tr><tr><td>Junior Alvarado</td><td>20/1</td><td>+2000</td></tr><tr><td>Gabriel Saez</td><td>16/1</td><td>+1600</td></tr><tr><td>Garden Van Dyke</td><td>50/1</td><td>+5000</td></tr><tr><td>Irad Ortiz Jr.</td><td>11/2</td><td>+550</td></tr><tr><td>Javier Castellano</td><td>20/1</td><td>+2000</td></tr><tr><td>Luis Saez</td><td>8/1</td><td>+800</td></tr><tr><td>Jose Ortiz</td><td>6/1</td><td>+600</td></tr><tr><td>Ricardo Santana Jr.</td><td>40/1</td><td>+4000</td></tr><tr><td>Corey Lanerie</td><td>30/1</td><td>+3000</td></tr><tr><td>Rajiv Maragh</td><td>40/1</td><td>+4000</td></tr><tr><td>John Velazquez</td><td>15/1</td><td>+1500</td></tr><tr><td>Julian Pimentel</td><td>20/1</td><td>+2000</td></tr><tr><td>Julien Leparoux</td><td>55/1</td><td>+5500</td></tr><tr><td>Joel Rosario</td><td>9/2</td><td>+450</td></tr><tr><td>Florent Geroux</td><td>11/2</td><td>+550</td></tr><tr><td>Jon Court</td><td>40/1</td><td>+4000</td></tr><tr><td>Manny Franco</td><td>22/1</td><td>+2200</td></tr><tr><td>Flavien Prat</td><td>45/1</td><td>+4500</td></tr><tr><td>Chris Landeros</td><td>33/1</td><td>+3300</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	