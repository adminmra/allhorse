	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - European Tour - 72 Hole Group Betting">
			<caption>Golf - European Tour - 72 Hole Group Betting</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - European Tour - 72 Hole Group Betting  - Sep 19					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Group Betting - Group B</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Henrik Stenson</td><td>31/10</td><td>+310</td></tr><tr><td>Matthew Fitzpatrick</td><td>16/5</td><td>+320</td></tr><tr><td>Shane Lowry</td><td>18/5</td><td>+360</td></tr><tr><td>Matt Wallace</td><td>18/5</td><td>+360</td></tr><tr><td>Francesco Molinari</td><td>4/1</td><td>+400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Group Betting - Group C</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tony Finau</td><td>14/5</td><td>+280</td></tr><tr><td>Viktor Hovland</td><td>14/5</td><td>+280</td></tr><tr><td>Patrick Reed</td><td>19/5</td><td>+380</td></tr><tr><td>Erik Van Rooyen</td><td>21/5</td><td>+420</td></tr><tr><td>Bernd Wiesberger</td><td>21/5</td><td>+420</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Group Betting - Group D</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Alex Noren</td><td>31/10</td><td>+310</td></tr><tr><td>Tyrell Hatton</td><td>17/5</td><td>+340</td></tr><tr><td>Thomas Pieters</td><td>7/2</td><td>+350</td></tr><tr><td>Danny Willet</td><td>19/5</td><td>+380</td></tr><tr><td>Rafa Cabrera Bello</td><td>19/5</td><td>+380</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Group Betting - Group E</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Ian Poulter</td><td>7/2</td><td>+350</td></tr><tr><td>Billy Horschel</td><td>7/2</td><td>+350</td></tr><tr><td>Robert Macintyre</td><td>7/2</td><td>+350</td></tr><tr><td>Joost Luiten</td><td>7/2</td><td>+350</td></tr><tr><td>Haotong Li</td><td>7/2</td><td>+350</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');</script>
{/literal}	
	