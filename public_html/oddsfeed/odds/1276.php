	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - Pga Tour - 72 Hole Group Betting">
			<caption>Golf - Pga Tour - 72 Hole Group Betting</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - Pga Tour - 72 Hole Group Betting  - Sep 19					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Group Betting - Group A</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Joaquin Niemann</td><td>11/4</td><td>+275</td></tr><tr><td>Sungjae Im</td><td>3/1</td><td>+300</td></tr><tr><td>Brandt Snedeker</td><td>19/5</td><td>+380</td></tr><tr><td>Byeong-hun An</td><td>4/1</td><td>+400</td></tr><tr><td>Scottie Scheffler</td><td>4/1</td><td>+400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Group Betting - Group B</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Lucas Glover</td><td>12/5</td><td>+240</td></tr><tr><td>Corey Conners</td><td>12/5</td><td>+240</td></tr><tr><td>Cameron Smith</td><td>13/5</td><td>+260</td></tr><tr><td>Brian Harman</td><td>31/10</td><td>+310</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Group Betting - Group C</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Brian Harman</td><td>11/4</td><td>+275</td></tr><tr><td>Aaron Wise</td><td>7/2</td><td>+350</td></tr><tr><td>Jt Poston</td><td>7/2</td><td>+350</td></tr><tr><td>Emiliano Grillo</td><td>19/5</td><td>+380</td></tr><tr><td>Russell Henley</td><td>19/5</td><td>+380</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Group Betting - Group D</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Vaughn Taylor</td><td>3/1</td><td>+300</td></tr><tr><td>Dylan Fritelli</td><td>17/5</td><td>+340</td></tr><tr><td>Si Woo Kim</td><td>17/5</td><td>+340</td></tr><tr><td>Wyndham Clark</td><td>19/5</td><td>+380</td></tr><tr><td>Austin Cook</td><td>4/1</td><td>+400</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');</script>
{/literal}	
	