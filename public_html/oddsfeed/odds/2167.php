	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--croatia 1. Hnl">
			<caption>Pre--croatia 1. Hnl</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--croatia 1. Hnl  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Osijek</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >133</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Lokomotiva Zagreb</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >195</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--croatia 1. Hnl  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hnk Gorica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Slaven</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >159</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gnk Dinamo Zagreb</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -102)</div>
								 <div class="boxdata" >-555</div>
								 <div class="boxdata" >-2<br /> ( -102)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vsnk Varazdin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -129)</div>
								 <div class="boxdata" >1315</div>
								 <div class="boxdata" >+2<br /> ( -129)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--croatia 1. Hnl  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hnk Rijeka</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >-172</div>
								 <div class="boxdata" >-1<br /> ( -101)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Istra 1961</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >442</div>
								 <div class="boxdata" >+1<br /> ( -131)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Inter Zapresic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -108)</div>
								 <div class="boxdata" >1221</div>
								 <div class="boxdata" >+2<br /> ( -131)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hnk Hajduk Split</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -121)</div>
								 <div class="boxdata" >-526</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	