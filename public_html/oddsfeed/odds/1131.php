	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Superbowl 53 - Special Props">
			<caption>Superbowl 53 - Special Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Superbowl 53 - Special Props  - Feb 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Times Tony Romo Says: "oh Jim, I Dont Know Jim"</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 2.5</td><td>5/6</td><td>-120</td></tr><tr><td>Under 2.5</td><td>5/6</td><td>-120</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">1st Couple Shown On A Commercial Be Inter-racial?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>5/7</td><td>-140</td></tr><tr><td>No</td><td>EV</td><td>EV</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Race Of The 1st Td Scorer</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>White</td><td>2/1</td><td>+200</td></tr><tr><td>Black</td><td>1/3</td><td>-300</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Who Will Show More Cleavage?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Veronika Khomyn</td><td>2/5</td><td>-250</td></tr><tr><td>Gisele Bundchen</td><td>7/4</td><td>+175</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Times Broadcast Says Tom Brady's Exact Age? (41)</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 2½</td><td>5/9</td><td>-180</td></tr><tr><td>Under 2½</td><td>7/5</td><td>+140</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Gronk Wears A Hat Between Game End And Locker Room</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>5/7</td><td>-140</td></tr><tr><td>No</td><td>6/5</td><td>+120</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Broadcast Shows A Picture Of Aaron Donald Topless?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>3/1</td><td>+300</td></tr><tr><td>No</td><td>1/5</td><td>-500</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">The Live Line On The Patriots Be +105 Or Better?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>1/2</td><td>-200</td></tr><tr><td>No</td><td>7/5</td><td>+140</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o8t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will A Fan Run Onto The Field During The Game?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>9/2</td><td>+450</td></tr><tr><td>No</td><td>1/9</td><td>-900</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o9t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center"># Of Flags On Rams Defense In The 4th Quarter</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 4½</td><td>3/2</td><td>+150</td></tr><tr><td>Under 4½</td><td>1/2</td><td>-200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o10t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Any Player Leave The Game With A Concussion?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>2/1</td><td>+200</td></tr><tr><td>No</td><td>1/3</td><td>-300</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o11t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will The Openning Kickoff Be Returned For A Td?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>9/1</td><td>+900</td></tr><tr><td>No</td><td>1/22</td><td>-2200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o12t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will T Brady Kiss His Kid On The Mouth After Game</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>8/5</td><td>+160</td></tr><tr><td>No</td><td>10/23</td><td>-230</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o13t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center"># Of Plyrs To Kiss Lombardi Trophy On Way To Stage</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 4.5</td><td>10/13</td><td>-130</td></tr><tr><td>Under 4.5</td><td>10/11</td><td>-110</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					Superbowl 53 - Special Props  - Feb 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o14t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Company To Have The Best Avg. Rating For Sb 53</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Anheuser-bush</td><td>57/10</td><td>+570</td></tr><tr><td>Bumble</td><td>9/1</td><td>+900</td></tr><tr><td>Doritos</td><td>16/5</td><td>+320</td></tr><tr><td>Expensify</td><td>10/1</td><td>+1000</td></tr><tr><td>M&m`s</td><td>11/2</td><td>+550</td></tr><tr><td>Pepsi</td><td>13/1</td><td>+1300</td></tr><tr><td>Pringles</td><td>15/2</td><td>+750</td></tr><tr><td>Field</td><td>5/4</td><td>+125</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o15t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Which Commercial Will Appear First?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Turskish Airlines</td><td>2/11</td><td>-550</td></tr><tr><td>Weather Tech</td><td>7/2</td><td>+350</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o16t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Which Commercial Will Appear First?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Expensify</td><td>1/2</td><td>-200</td></tr><tr><td>Turbo Tax (intuit)</td><td>3/2</td><td>+150</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o17t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Which Commercial Will Appear First?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Doritos</td><td>5/7</td><td>-140</td></tr><tr><td>Pringles</td><td>11/10</td><td>+110</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o18t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Times Ted Rath Is Mentioned During Cbs Broadcast</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 3.5</td><td>5/2</td><td>+250</td></tr><tr><td>Under 3.5</td><td>1/4</td><td>-400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o19t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Replay Of Ted Holding Sean Mcvoy Shown During Game</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 1.5</td><td>5/7</td><td>-140</td></tr><tr><td>Under 1.5</td><td>EV</td><td>EV</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o20t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Times Cbs Show Pictures Of Sean Mcvay In College</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 1.5</td><td>10/17</td><td>-170</td></tr><tr><td>Under 1.5</td><td>13/10</td><td>+130</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o21t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Times The Broadcast Mentions Sean Mcvays Age</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 1.5</td><td>10/27</td><td>-270</td></tr><tr><td>Under 1.5</td><td>9/5</td><td>+180</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o22t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Chik-fil-a In The Top 10 Usa Twitter Trends Feb 3</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>2/1</td><td>+200</td></tr><tr><td>No</td><td>1/3</td><td>-300</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o23t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Broadcast Mentions Chik-fil-a Is Closed On Sundays</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>1/2</td><td>-200</td></tr><tr><td>No</td><td>3/2</td><td>+150</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					Superbowl 53 - Special Props  - Feb 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o24t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Total Tds By Patriots Runnning Backs</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 1.5</td><td>5/8</td><td>-160</td></tr><tr><td>Under 1.5</td><td>6/5</td><td>+120</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o25t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will The Philly Special Be Mentioned?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>2/3</td><td>-150</td></tr><tr><td>No</td><td>11/10</td><td>+110</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					Superbowl 53 - Special Props  - Feb 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o26t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">How Many Times Will Travis Scott Say "yeah" ?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 5.5</td><td>5/6</td><td>-120</td></tr><tr><td>Under 5.5</td><td>5/6</td><td>-120</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o27t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Broadcast Replays Missed Rams/saints Pass Int?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>2/1</td><td>+200</td></tr><tr><td>No</td><td>1/3</td><td>-300</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o28t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Either Qb Have A Pass, Rush And Reception?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>5/2</td><td>+250</td></tr><tr><td>No</td><td>1/4</td><td>-400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o29t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Gronk Catch The Game Winning Td?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>3/1</td><td>+300</td></tr><tr><td>No</td><td>1/5</td><td>-500</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o30t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">First Td Celebration Will Feature:</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>One Player</td><td>3/1</td><td>+300</td></tr><tr><td>Multiple Players</td><td>1/5</td><td>-500</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o31t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Travis Scott Propose To Kylie Jenner At Halftime?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>12/1</td><td>+1200</td></tr><tr><td>No</td><td>1/36</td><td>-3600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o32t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Gronk Have Exactly 69 Receiving Yards?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>69/10</td><td>+690</td></tr><tr><td>No</td><td>10/169</td><td>-1690</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o33t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Sean Payton To Be Shown Or Mentioned On Broadcast</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>7/5</td><td>+140</td></tr><tr><td>No</td><td>1/2</td><td>-200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o34t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Matthew Slater Record A Tackle?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>7/5</td><td>+140</td></tr><tr><td>No</td><td>1/2</td><td>-200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o35t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Any Plyr Have Their Pants Fall Down In Game?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>7/1</td><td>+700</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o36t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will The Rams Be Called For Roughing The Passer?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>2/3</td><td>-150</td></tr><tr><td>No</td><td>11/10</td><td>+110</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o37t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Total Challenge Flags Thrown During Regulation?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 1½</td><td>7/5</td><td>+140</td></tr><tr><td>Under 1½</td><td>1/2</td><td>-200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o38t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will #forthebrand Be Mentioned On The Broadcast?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>5/1</td><td>+500</td></tr></tbody></table></td></tr>        <tr><th colspan="3"  class="datahead">Superbowl 53 - Special Props  - Feb 03</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" ></div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Any Plyr Caught With Their Hands Down Their Pants?</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" ></div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Will An Nba Trade Happen During The Broadcast?</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" ></div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Will Barstool Sports Be Mentioned On The Broadcast</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" ></div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aaron Donald Total Sacks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" ></div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wade Phillips Wear Bum's Cowboy Hat In  Broadcast?</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');addSort('o6t');addSort('o7t');addSort('o8t');addSort('o9t');addSort('o10t');addSort('o11t');addSort('o12t');addSort('o13t');addSort('o14t');addSort('o15t');addSort('o16t');addSort('o17t');addSort('o18t');addSort('o19t');addSort('o20t');addSort('o21t');addSort('o22t');addSort('o23t');addSort('o24t');addSort('o25t');addSort('o26t');addSort('o27t');addSort('o28t');addSort('o29t');addSort('o30t');addSort('o31t');addSort('o32t');addSort('o33t');addSort('o34t');addSort('o35t');addSort('o36t');addSort('o37t');addSort('o38t');</script>
{/literal}	
	