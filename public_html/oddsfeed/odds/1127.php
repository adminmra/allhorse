	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Superbowl 53 - Exact Outcome">
			<caption>Superbowl 53 - Exact Outcome</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated January 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center">
					Superbowl 53 - Exact Outcome  - Jan 20					</th>
			</tr>
            
	<tr><th colspan="3" class="center">Super Bowl Liii - Exact Result</th></tr><tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Saints Defeat Chiefs</td><td>16/5</td><td>+320</td></tr><tr><td>Saints Defeat Patriots</td><td>9/2</td><td>+450</td></tr><tr><td>Chiefs Defeat Saints</td><td>37/10</td><td>+370</td></tr><tr><td>Chiefs Defeat Rams</td><td>11/2</td><td>+550</td></tr><tr><td>Rams Defeat Chiefs</td><td>5/1</td><td>+500</td></tr><tr><td>Rams Defeat Patroits</td><td>15/2</td><td>+750</td></tr><tr><td>Patriots Defeat Saints</td><td>5/1</td><td>+500</td></tr><tr><td>Patriots Defeat Rams</td><td>15/2</td><td>+750</td></tr>			</tbody>
		</table>
	</div>

	