	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Racing - Formula E Drivers Championship - To Win">
			<caption>Racing - Formula E Drivers Championship - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Racing - Formula E Drivers Championship - To Win  - Jul 13					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Formula E Drivers Championship - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Jean-eric Vergne</td><td>1/10</td><td>-1000</td></tr><tr><td>Antonio Felix Da Costa</td><td>500/1</td><td>+50000</td></tr><tr><td>Andre Lotterer</td><td>50/1</td><td>+5000</td></tr><tr><td>Lucas Di Grassi</td><td>11/2</td><td>+550</td></tr><tr><td>Robin Frijns</td><td>500/1</td><td>+50000</td></tr><tr><td>Mitch Evans</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	