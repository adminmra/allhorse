	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Uefa Europa League Specials">
			<caption>Soccer - Uefa Europa League Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 23, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Uefa Europa League Specials  - May 29					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Uefa Europa League - Double Result</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Chelsea - Chelsea</td><td>11/4</td><td>+275</td></tr><tr><td>Chelsea - Draw</td><td>14/1</td><td>+1400</td></tr><tr><td>Chelsea - Arsenal</td><td>33/1</td><td>+3300</td></tr><tr><td>Draw - Chelsea</td><td>9/2</td><td>+450</td></tr><tr><td>Draw - Draw</td><td>15/4</td><td>+375</td></tr><tr><td>Draw - Arsenal</td><td>11/2</td><td>+550</td></tr><tr><td>Arsenal - Chelsea</td><td>28/1</td><td>+2800</td></tr><tr><td>Arsenal - Draw</td><td>14/1</td><td>+1400</td></tr><tr><td>Arsenal - Arsenal</td><td>4/1</td><td>+400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Uefa Europa League - Method Of Victory</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Chelsea In 90 Mins</td><td>13/10</td><td>+130</td></tr><tr><td>Chelsea In Extra Time</td><td>10/1</td><td>+1000</td></tr><tr><td>Chelsea In Penalties</td><td>10/1</td><td>+1000</td></tr><tr><td>Arsenal In 90 Mins</td><td>21/10</td><td>+210</td></tr><tr><td>Arsenal In Extra Time</td><td>12/1</td><td>+1200</td></tr><tr><td>Arsenal In Penalties</td><td>11/1</td><td>+1100</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Uefa Europa League - Winning Margin</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Chelsea By 1 Goal</td><td>11/4</td><td>+275</td></tr><tr><td>Chelsea By 2 Goals</td><td>11/2</td><td>+550</td></tr><tr><td>Chelsea By 3 Goals</td><td>14/1</td><td>+1400</td></tr><tr><td>Chelsea By 4 Or More Goals</td><td>25/1</td><td>+2500</td></tr><tr><td>Arsenal By 1 Goal</td><td>7/2</td><td>+350</td></tr><tr><td>Arsenal By 2 Goals</td><td>17/2</td><td>+850</td></tr><tr><td>Arsenal By 3 Goals</td><td>25/1</td><td>+2500</td></tr><tr><td>Arsenal By 4 Or More Goals</td><td>50/1</td><td>+5000</td></tr><tr><td>Score Draw</td><td>7/2</td><td>+350</td></tr><tr><td>No Goal</td><td>8/1</td><td>+800</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');</script>
{/literal}	
	