	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Poland Ekstraklasa">
			<caption>Poland Ekstraklasa</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 3, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Poland Ekstraklasa  - Dec 03</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Pogon Szczecin<br /> - vs - <br /> Piast Gliwice</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (105)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+182</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+105 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-125)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+146</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-125</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Poland Ekstraklasa  - Dec 07</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Piast Gliwice<br /> - vs - <br /> Zaglebie Lubin</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (110)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+184</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-110 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-130)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+136</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-110</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Slask Wroclaw<br /> - vs - <br /> Lech Poznan</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (110)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+314</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-115 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (-130)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-128</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-105</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Poland Ekstraklasa  - Dec 08</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Jagiellonia Bialystok<br /> - vs - <br /> Wisla Krakow</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-105)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+165</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-120 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+149</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;EV</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Zagebie Sosnowiec<br /> - vs - <br /> Pogon Szczecin</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1<br /> (-105)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+483</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-140 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-205</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+120</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Wisla Plock<br /> - vs - <br /> Korona Kielce</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-110)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+264</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-115 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (-110)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-108</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-105</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Poland Ekstraklasa  - Dec 09</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Legia Warszawa<br /> - vs - <br /> Lechia Gdansk</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+149</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;EV </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (100)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+174</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-120</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Miedz Legnica<br /> - vs - <br /> Gornik Zabrze</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+199</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-120 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+120</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;EV</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Poland Ekstraklasa  - Dec 10</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Cracovia Krakow<br /> - vs - <br /> Arka Gdynia</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+224</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+120 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+119</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-140</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	