	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nhl - Stanley Cup - Odds To Win">
			<caption>Nhl - Stanley Cup - Odds To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nhl - Stanley Cup - Odds To Win  - Oct 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Stanley Cup - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tampa Bay Lightning</td><td>27/4</td><td>+675</td></tr><tr><td>Boston Bruins</td><td>19/2</td><td>+950</td></tr><tr><td>Vegas Golden Knights</td><td>17/2</td><td>+850</td></tr><tr><td>Toronto Maple Leafs</td><td>47/5</td><td>+940</td></tr><tr><td>Colorado Avalanche</td><td>14/1</td><td>+1400</td></tr><tr><td>St Louis Blues</td><td>14/1</td><td>+1400</td></tr><tr><td>Calgary Flames</td><td>20/1</td><td>+2000</td></tr><tr><td>San Jose Sharks</td><td>18/1</td><td>+1800</td></tr><tr><td>Washington Capitals</td><td>28/1</td><td>+2800</td></tr><tr><td>Winnipeg Jets</td><td>22/1</td><td>+2200</td></tr><tr><td>Nashville Predators</td><td>14/1</td><td>+1400</td></tr><tr><td>Florida Panthers</td><td>20/1</td><td>+2000</td></tr><tr><td>Dallas Stars</td><td>16/1</td><td>+1600</td></tr><tr><td>Pittsburgh Penguins</td><td>26/1</td><td>+2600</td></tr><tr><td>Carolina Hurricanes</td><td>20/1</td><td>+2000</td></tr><tr><td>New York Islanders</td><td>26/1</td><td>+2600</td></tr><tr><td>Philadelphia Flyers</td><td>40/1</td><td>+4000</td></tr><tr><td>Edmonton Oilers</td><td>75/1</td><td>+7500</td></tr><tr><td>Columbus Blue Jackets</td><td>150/1</td><td>+15000</td></tr><tr><td>Chicago Blackhawks</td><td>35/1</td><td>+3500</td></tr><tr><td>Arizona Coyotes</td><td>40/1</td><td>+4000</td></tr><tr><td>Montreal Canadiens</td><td>60/1</td><td>+6000</td></tr><tr><td>Vancouver Canucks</td><td>30/1</td><td>+3000</td></tr><tr><td>Minnesota Wild</td><td>75/1</td><td>+7500</td></tr><tr><td>New Jersey Devils</td><td>30/1</td><td>+3000</td></tr><tr><td>New York Rangers</td><td>45/1</td><td>+4500</td></tr><tr><td>Buffalo Sabres</td><td>100/1</td><td>+10000</td></tr><tr><td>Anaheim Ducks</td><td>150/1</td><td>+15000</td></tr><tr><td>Los Angeles Kings</td><td>250/1</td><td>+25000</td></tr><tr><td>Detroit Red Wings</td><td>200/1</td><td>+20000</td></tr><tr><td>Ottawa Senators</td><td>300/1</td><td>+30000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	