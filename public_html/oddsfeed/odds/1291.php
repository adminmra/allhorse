	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Nfl - Fantasy Player Points">
			<caption>Nfl - Fantasy Player Points</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 24, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">If Decimal Lands On 0.5 Exactly<br />It Will Be Rounded Up To The Next Number</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">P Lindsay Fantasy Pts<br /> - vs - <br /> P Lindsay Fantasy Pts</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o13&frac12;-120 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u13&frac12;-120</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">D Carr Fantasy Pts<br /> - vs - <br /> D Carr Fantasy Pts</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o14&frac12;-120 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u14&frac12;-120</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	