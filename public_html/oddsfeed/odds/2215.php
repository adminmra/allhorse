	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--denmark 1st Division">
			<caption>Pre--denmark 1st Division</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--denmark 1st Division  - Sep 19</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Roskilde</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -126)</div>
								 <div class="boxdata" >473</div>
								 <div class="boxdata" >+1<br /> ( +102)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vejle Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -105)</div>
								 <div class="boxdata" >-212</div>
								 <div class="boxdata" >-1<br /> ( -136)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vendsyssel Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >275</div>
								 <div class="boxdata" >+&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Viborg Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-112</div>
								 <div class="boxdata" >-&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--denmark 1st Division  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hb Koege</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -112)</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hvidovre If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -117)</div>
								 <div class="boxdata" >144</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--denmark 1st Division  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Viborg Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >-121</div>
								 <div class="boxdata" >-&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Naestved Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >271</div>
								 <div class="boxdata" >+&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vendsyssel Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac12;<br /> ( +112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nykoebing Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >192</div>
								 <div class="boxdata" >+&frac12;<br /> ( -156 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bk Fremad Amager</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -106 )</div>
								 <div class="boxdata" >273</div>
								 <div class="boxdata" >+&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Fredericia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -129 )</div>
								 <div class="boxdata" >-119</div>
								 <div class="boxdata" >-&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Skive Ik</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >164</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kolding If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -126 )</div>
								 <div class="boxdata" >139</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	