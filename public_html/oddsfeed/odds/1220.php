	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="England Bbl">
			<caption>England Bbl</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 12, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">England Bbl  - May 12</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Plymouth Raiders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o162&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >255</div>
								 <div class="boxdata" >+7&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Leicester Riders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u162&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-315</div>
								 <div class="boxdata" >-7&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	