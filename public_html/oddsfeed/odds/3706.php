	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Campeonato Brasileiro - To Win">
			<caption>Soccer - Campeonato Brasileiro - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Campeonato Brasileiro - To Win  - Sep 21					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-2020 Campeonato Brasileiro - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Santos</td><td>7/1</td><td>+700</td></tr><tr><td>Palmeiras</td><td>2/1</td><td>+200</td></tr><tr><td>Flamengo</td><td>4/5</td><td>-125</td></tr><tr><td>Sao Paulo</td><td>40/1</td><td>+4000</td></tr><tr><td>Corinthians</td><td>40/1</td><td>+4000</td></tr><tr><td>Atletico Mineiro</td><td>450/1</td><td>+45000</td></tr><tr><td>International</td><td>33/1</td><td>+3300</td></tr><tr><td>Gremio</td><td>66/1</td><td>+6600</td></tr><tr><td>Athletico Paranaense</td><td>250/1</td><td>+25000</td></tr><tr><td>Bahia</td><td>100/1</td><td>+10000</td></tr><tr><td>Botafogo</td><td>500/1</td><td>+50000</td></tr><tr><td>Ceara</td><td>1000/1</td><td>+100000</td></tr><tr><td>Vasco Da Gama</td><td>1000/1</td><td>+100000</td></tr><tr><td>Fortaleza</td><td>1000/1</td><td>+100000</td></tr><tr><td>Cruzeiro</td><td>500/1</td><td>+50000</td></tr><tr><td>Fluminense</td><td>750/1</td><td>+75000</td></tr><tr><td>Goias</td><td>2500/1</td><td>+250000</td></tr><tr><td>Chapecoense</td><td>3500/1</td><td>+350000</td></tr><tr><td>Avai</td><td>4500/1</td><td>+450000</td></tr><tr><td>Csa</td><td>4500/1</td><td>+450000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	