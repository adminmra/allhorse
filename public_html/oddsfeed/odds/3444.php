	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Cbb - March Madness Props">
			<caption>Cbb - March Madness Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 28, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Cbb - March Madness Props  - Mar 28					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">How Many 1 Seeds Will Make The Final 4 ?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>0</td><td>19/2</td><td>+950</td></tr><tr><td>1</td><td>5/2</td><td>+250</td></tr><tr><td>2</td><td>31/20</td><td>+155</td></tr><tr><td>3</td><td>11/4</td><td>+275</td></tr><tr><td>4</td><td>5/1</td><td>+500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	