	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Aussie Rules - Afl Game Lines">
			<caption>Aussie Rules - Afl Game Lines</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 12, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Aussie Rules - Afl Game Lines  - Jun 13</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Richmond</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o155&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >210</div>
								 <div class="boxdata" >+18&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Adelaide</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u155&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >-280</div>
								 <div class="boxdata" >-18&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Aussie Rules - Afl Game Lines  - Jun 14</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hawthorn</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o162&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >+3&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Essendon</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u162&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >-130</div>
								 <div class="boxdata" >-3&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >St Kilda</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o161&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >-160</div>
								 <div class="boxdata" >-8&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gold Coast</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u161&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >130</div>
								 <div class="boxdata" >+8&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Aussie Rules - Afl Game Lines  - Jun 15</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Port Adelaide</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o162&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >115</div>
								 <div class="boxdata" >+5&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fremantle</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u162&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >-145</div>
								 <div class="boxdata" >-5&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Western Bulldogs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o168&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >-190</div>
								 <div class="boxdata" >-12&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Carlton</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u168&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >155</div>
								 <div class="boxdata" >+12&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Aussie Rules - Afl Game Lines  - Jun 16</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Greater Western Sydney</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o166&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >-190</div>
								 <div class="boxdata" >-11&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >North Melbourne</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u166&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >155</div>
								 <div class="boxdata" >+11&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	