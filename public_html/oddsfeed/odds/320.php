	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba - 2019-20 Regular Season Mvp - To Win">
			<caption>Nba - 2019-20 Regular Season Mvp - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nba - 2019-20 Regular Season Mvp - To Win  - Oct 22					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Regular Season Mvp - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Giannis Antetokounmpo</td><td>14/5</td><td>+280</td></tr><tr><td>Stephen Curry</td><td>11/2</td><td>+550</td></tr><tr><td>Kawhi Leonard</td><td>17/2</td><td>+850</td></tr><tr><td>James Harden</td><td>6/1</td><td>+600</td></tr><tr><td>Lebron James</td><td>8/1</td><td>+800</td></tr><tr><td>Anthony Davis</td><td>7/1</td><td>+700</td></tr><tr><td>Joel Embiid</td><td>16/1</td><td>+1600</td></tr><tr><td>Russell Westbrook</td><td>18/1</td><td>+1800</td></tr><tr><td>Paul George</td><td>20/1</td><td>+2000</td></tr><tr><td>Nikola Jokic</td><td>10/1</td><td>+1000</td></tr><tr><td>Karl-anthony Towns</td><td>33/1</td><td>+3300</td></tr><tr><td>Damian Lillard</td><td>22/1</td><td>+2200</td></tr><tr><td>Kyrie Irving</td><td>45/1</td><td>+4500</td></tr><tr><td>Luka Doncic</td><td>50/1</td><td>+5000</td></tr><tr><td>Kemba Walker</td><td>60/1</td><td>+6000</td></tr><tr><td>Rudy Gobert</td><td>150/1</td><td>+15000</td></tr><tr><td>Donovan Mitchell</td><td>60/1</td><td>+6000</td></tr><tr><td>Ben Simmons</td><td>100/1</td><td>+10000</td></tr><tr><td>Demar Derozan</td><td>100/1</td><td>+10000</td></tr><tr><td>Jimmy Butler</td><td>80/1</td><td>+8000</td></tr><tr><td>Blake Griffin</td><td>100/1</td><td>+10000</td></tr><tr><td>Trae Young</td><td>150/1</td><td>+15000</td></tr><tr><td>Zion Williamson</td><td>80/1</td><td>+8000</td></tr><tr><td>Nikola Vucevik</td><td>150/1</td><td>+15000</td></tr><tr><td>Jayson Tatum</td><td>125/1</td><td>+12500</td></tr><tr><td>Bradley Beal</td><td>150/1</td><td>+15000</td></tr><tr><td>Dangelo Russell</td><td>150/1</td><td>+15000</td></tr><tr><td>Devin Booker</td><td>150/1</td><td>+15000</td></tr><tr><td>Victor Oladipo</td><td>140/1</td><td>+14000</td></tr><tr><td>Mike Conley</td><td>150/1</td><td>+15000</td></tr><tr><td>Pascal Siakam</td><td>80/1</td><td>+8000</td></tr><tr><td>Jrue Holiday</td><td>200/1</td><td>+20000</td></tr><tr><td>Draymond Green</td><td>150/1</td><td>+15000</td></tr><tr><td>Kevin Love</td><td>150/1</td><td>+15000</td></tr><tr><td>Kristaps Porzingis</td><td>150/1</td><td>+15000</td></tr><tr><td>Gordon Hayward</td><td>150/1</td><td>+15000</td></tr><tr><td>Lonzo Ball</td><td>150/1</td><td>+15000</td></tr><tr><td>Lamarcus Aldridge</td><td>200/1</td><td>+20000</td></tr><tr><td>Aaron Gordon</td><td>200/1</td><td>+20000</td></tr><tr><td>Ja Morant</td><td>200/1</td><td>+20000</td></tr><tr><td>Andre Drummond</td><td>200/1</td><td>+20000</td></tr><tr><td>Jaylen Brown</td><td>200/1</td><td>+20000</td></tr><tr><td>Deandre Ayton</td><td>200/1</td><td>+20000</td></tr><tr><td>Zach Lavine</td><td>200/1</td><td>+20000</td></tr><tr><td>Klay Thompson</td><td>200/1</td><td>+20000</td></tr><tr><td>Brandon Ingram</td><td>200/1</td><td>+20000</td></tr><tr><td>Kyle Lowry</td><td>200/1</td><td>+20000</td></tr><tr><td>Fred Vanvleet</td><td>250/1</td><td>+25000</td></tr><tr><td>Lauri Markkanen</td><td>250/1</td><td>+25000</td></tr><tr><td>Rj Barrett</td><td>250/1</td><td>+25000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	