	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba - Exact Series Result">
			<caption>Nba - Exact Series Result</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 5, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nba - Exact Series Result  - Jun 05					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Warriors Vs Raptors - Exact Series Result</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Warriors Wins 4 - 1</td><td>13/5</td><td>+260</td></tr><tr><td>Warriors Wins 4 - 2</td><td>7/4</td><td>+175</td></tr><tr><td>Warriors Wins 4 - 3</td><td>9/2</td><td>+450</td></tr><tr><td>Raptors Wins 4 - 1</td><td>16/1</td><td>+1600</td></tr><tr><td>Raptors Wins 4 - 2</td><td>9/1</td><td>+900</td></tr><tr><td>Raptors Wins 4 - 3</td><td>9/2</td><td>+450</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	