	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--uruguay Segunda Division">
			<caption>Pre--uruguay Segunda Division</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 2, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--uruguay Segunda Division  - Jul 02</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Albion Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >355</div>
								 <div class="boxdata" >+&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Rentistas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >-128</div>
								 <div class="boxdata" >-&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Deportivo Maldonado</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-106</div>
								 <div class="boxdata" >-&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Cerrito</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >262</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Central Espanol Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >285</div>
								 <div class="boxdata" >+&frac12;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sud America</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >-103</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Atenas De San Carlos</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >177</div>
								 <div class="boxdata" >+&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Villa Teresa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >134</div>
								 <div class="boxdata" >-&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Bella Vista</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >403</div>
								 <div class="boxdata" >+&frac34;<br /> ( -108 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Villa Espanola</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >-156</div>
								 <div class="boxdata" >-&frac34;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	