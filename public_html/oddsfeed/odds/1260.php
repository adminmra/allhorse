	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mexico Lmb Baseball">
			<caption>Mexico Lmb Baseball</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 17, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Mexico Lmb Baseball  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Toros De Tijuana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o11<br /> ( -105)</div>
								 <div class="boxdata" >-115</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acereros Del Monclova</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u11<br /> ( -115)</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -165 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Diablos Rojos Del Mexico</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o8<br /> ( -105)</div>
								 <div class="boxdata" >155</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Leones De Yucatan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u8<br /> ( -115)</div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +120 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	