	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Breeders Cup Mile - To Win">
			<caption>Horses - Breeders Cup Mile - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Breeders Cup Mile - To Win  - Nov 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Breeders Cup Mile - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Laurens</td><td>14/1</td><td>+1400</td></tr><tr><td>Catapult</td><td>33/1</td><td>+3300</td></tr><tr><td>Without Parole</td><td>33/1</td><td>+3300</td></tr><tr><td>Ohio</td><td>25/1</td><td>+2500</td></tr><tr><td>River Boyne</td><td>25/1</td><td>+2500</td></tr><tr><td>Lord Glitters</td><td>14/1</td><td>+1400</td></tr><tr><td>Bolo</td><td>50/1</td><td>+5000</td></tr><tr><td>King Of Comedy</td><td>15/2</td><td>+750</td></tr><tr><td>Flying Scotsman</td><td>50/1</td><td>+5000</td></tr><tr><td>Bricks And Mortar</td><td>11/2</td><td>+550</td></tr><tr><td>Hermosa</td><td>12/1</td><td>+1200</td></tr><tr><td>Analyze It</td><td>16/1</td><td>+1600</td></tr><tr><td>Circus Maximus</td><td>5/1</td><td>+500</td></tr><tr><td>Got Stormy</td><td>10/1</td><td>+1000</td></tr><tr><td>Romanised</td><td>10/1</td><td>+1000</td></tr><tr><td>Rushing Fall</td><td>12/1</td><td>+1200</td></tr><tr><td>Valid Point</td><td>12/1</td><td>+1200</td></tr><tr><td>One Master</td><td>12/1</td><td>+1200</td></tr><tr><td>Mustashry</td><td>14/1</td><td>+1400</td></tr><tr><td>Uni</td><td>20/1</td><td>+2000</td></tr><tr><td>Billesdon Brook</td><td>40/1</td><td>+4000</td></tr><tr><td>El Tormenta</td><td>50/1</td><td>+5000</td></tr><tr><td>Raging Bull</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	