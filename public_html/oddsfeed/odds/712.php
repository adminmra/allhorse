	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nhl">
			<caption>Nhl</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 17, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Nhl  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ottawa Senators</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >210</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Toronto Maple Leafs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >-260</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Buffalo Sabres</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6<br /> ( -120)</div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -220 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Columbus Blue Jackets</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-140</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +180 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Carolina Hurricanes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6<br /> ( -110)</div>
								 <div class="boxdata" >155</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -180 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tampa Bay Lightning</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6<br /> ( -110)</div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +150 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Philadelphia Flyers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6<br /> ( -105)</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -210 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New York Islanders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6<br /> ( -115)</div>
								 <div class="boxdata" >-145</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +175 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chicago Blackhawks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6<br /> ( -115)</div>
								 <div class="boxdata" >-110</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +225 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Detroit Red Wings</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6<br /> ( -105)</div>
								 <div class="boxdata" >-110</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -275 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dallas Stars</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6<br /> ( -105)</div>
								 <div class="boxdata" >170</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -165 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Minnesota Wild</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6<br /> ( -115)</div>
								 <div class="boxdata" >-200</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vegas Golden Knights</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6<br /> ( -115)</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -260 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Colorado Avalanche</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6<br /> ( -105)</div>
								 <div class="boxdata" >-120</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +210 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Edmonton Oilers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6<br /> ( -105)</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -280 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vancouver Canucks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6<br /> ( -115)</div>
								 <div class="boxdata" >-120</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +230 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Anaheim Ducks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6<br /> ( -115)</div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -210 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >San Jose Sharks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6<br /> ( -105)</div>
								 <div class="boxdata" >-140</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +175 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	