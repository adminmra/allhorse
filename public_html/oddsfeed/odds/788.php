	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nfl - Afc Conference - To Win">
			<caption>Nfl - Afc Conference - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nfl - Afc Conference - To Win  - Sep 05					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Afc Conference - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Kansas City Chiefs</td><td>15/4</td><td>+375</td></tr><tr><td>New England Patriots</td><td>3/1</td><td>+300</td></tr><tr><td>Los Angeles Chargers</td><td>7/1</td><td>+700</td></tr><tr><td>Indianapolis Colts</td><td>8/1</td><td>+800</td></tr><tr><td>Pittsburgh Steelers</td><td>10/1</td><td>+1000</td></tr><tr><td>Cleveland Browns</td><td>7/1</td><td>+700</td></tr><tr><td>Baltimore Ravens</td><td>16/1</td><td>+1600</td></tr><tr><td>Houston Texans</td><td>16/1</td><td>+1600</td></tr><tr><td>Jacksonville Jaguars</td><td>16/1</td><td>+1600</td></tr><tr><td>Denver Broncos</td><td>33/1</td><td>+3300</td></tr><tr><td>Tennessee Titans</td><td>28/1</td><td>+2800</td></tr><tr><td>Buffalo Bills</td><td>50/1</td><td>+5000</td></tr><tr><td>New York Jets</td><td>40/1</td><td>+4000</td></tr><tr><td>Oakland Raiders</td><td>33/1</td><td>+3300</td></tr><tr><td>Cincinnati Bengals</td><td>66/1</td><td>+6600</td></tr><tr><td>Miami Dolphins</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	