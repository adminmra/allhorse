	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--france Coupe De France">
			<caption>Live--france Coupe De France</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 6, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--france Coupe De France  - Mar 06</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Nantes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -166 )</div>
								 <div class="boxdata" >-1666</div>
								 <div class="boxdata" >-2<br /> ( -117)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Vitre</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" >2700</div>
								 <div class="boxdata" >+2<br /> ( -117)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	