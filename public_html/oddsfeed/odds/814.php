	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nfl - Offensive Rookie Of The Year">
			<caption>Nfl - Offensive Rookie Of The Year</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nfl - Offensive Rookie Of The Year  - Sep 05					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Nfl Offensive Rookie Of The Year</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Kyler Murray</td><td>5/2</td><td>+250</td></tr><tr><td>Dwayne Haskins </td><td>7/1</td><td>+700</td></tr><tr><td>Josh Jacobs</td><td>8/1</td><td>+800</td></tr><tr><td>Marquise Brown</td><td>12/1</td><td>+1200</td></tr><tr><td>Tj Hockenson</td><td>22/1</td><td>+2200</td></tr><tr><td>Drew Lock</td><td>16/1</td><td>+1600</td></tr><tr><td>Mecole Hardman</td><td>14/1</td><td>+1400</td></tr><tr><td>Aj Brown</td><td>19/1</td><td>+1900</td></tr><tr><td>N Keal Harry</td><td>16/1</td><td>+1600</td></tr><tr><td>Parris Campbell</td><td>20/1</td><td>+2000</td></tr><tr><td>Daniel Jones</td><td>25/1</td><td>+2500</td></tr><tr><td>Jj Arcega Whiteside</td><td>30/1</td><td>+3000</td></tr><tr><td>Noah Fant</td><td>30/1</td><td>+3000</td></tr><tr><td>Dk Metcalf</td><td>12/1</td><td>+1200</td></tr><tr><td>Darrell Henderson</td><td>25/1</td><td>+2500</td></tr><tr><td>Hakeem Butler</td><td>55/2</td><td>+2750</td></tr><tr><td>Miles Sanders</td><td>20/1</td><td>+2000</td></tr><tr><td>Damien Harris</td><td>30/1</td><td>+3000</td></tr><tr><td>David Montgomery</td><td>17/1</td><td>+1700</td></tr><tr><td>Irv Smith Jr</td><td>40/1</td><td>+4000</td></tr><tr><td>Justice Hill</td><td>25/1</td><td>+2500</td></tr><tr><td>Will Grier</td><td>50/1</td><td>+5000</td></tr><tr><td>Bryce Love</td><td>25/1</td><td>+2500</td></tr><tr><td>Ryan Finley</td><td>25/1</td><td>+2500</td></tr><tr><td>Field</td><td>8/1</td><td>+800</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	