	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--usa Major League Soccer">
			<caption>Pre--usa Major League Soccer</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--usa Major League Soccer  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ny Red Bulls</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Portland Timbers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >-109</div>
								 <div class="boxdata" >-&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dallas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >284</div>
								 <div class="boxdata" >+&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Seattle Sounders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >-126</div>
								 <div class="boxdata" >-&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atlanta United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >-156</div>
								 <div class="boxdata" >-1<br /> ( -101)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Cincinnati</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >341</div>
								 <div class="boxdata" >+1<br /> ( -131)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--usa Major League Soccer  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Colorado Rapids</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >405</div>
								 <div class="boxdata" >+1<br /> ( -108)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sporting Kansas City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >-188</div>
								 <div class="boxdata" >-1<br /> ( -121)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Orlando City Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -111)</div>
								 <div class="boxdata" >233</div>
								 <div class="boxdata" >+&frac12;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Houston Dynamo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -119)</div>
								 <div class="boxdata" >-103</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >San Jose Earthquakes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >332</div>
								 <div class="boxdata" >+1<br /> ( -129)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atlanta United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >-158</div>
								 <div class="boxdata" >-1<br /> ( -102)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Salt Lake</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -126)</div>
								 <div class="boxdata" >301</div>
								 <div class="boxdata" >+&frac12;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New England Revolution</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -104)</div>
								 <div class="boxdata" >-136</div>
								 <div class="boxdata" >-&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Montreal Impact</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >337</div>
								 <div class="boxdata" >+1<br /> ( -131)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Los Angeles Galaxy</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >-158</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chicago Fire</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -125)</div>
								 <div class="boxdata" >-133</div>
								 <div class="boxdata" >-&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Cincinnati</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -106)</div>
								 <div class="boxdata" >297</div>
								 <div class="boxdata" >+&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Columbus Crew</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -123)</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vancouver Whitecaps Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -107)</div>
								 <div class="boxdata" >195</div>
								 <div class="boxdata" >+&frac12;<br /> ( -147 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Toronto Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac34;<br /> ( -113 )</div>
								 <div class="boxdata" >440</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Los Angeles Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac34;<br /> ( -116 )</div>
								 <div class="boxdata" >-222</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--usa Major League Soccer  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Seattle Sounders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -117)</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dc United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -112)</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Minnesota United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -129)</div>
								 <div class="boxdata" >245</div>
								 <div class="boxdata" >+&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Portland Timbers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -102)</div>
								 <div class="boxdata" >-111</div>
								 <div class="boxdata" >-&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Philadelphia Union</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >242</div>
								 <div class="boxdata" >+&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ny Red Bulls</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >-111</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New York City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dallas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	