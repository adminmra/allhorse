	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--bosnia & Herzegovina Premijer Liga">
			<caption>Pre--bosnia & Herzegovina Premijer Liga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--bosnia & Herzegovina Premijer Liga  - May 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Gosk Gabela</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >419</div>
								 <div class="boxdata" >+1<br /> ( -129)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Krupa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >-172</div>
								 <div class="boxdata" >-1<br /> ( -102)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Zvijezda 09</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -108)</div>
								 <div class="boxdata" >864</div>
								 <div class="boxdata" >+1&frac34;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Sarajevo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -120)</div>
								 <div class="boxdata" >-384</div>
								 <div class="boxdata" >-1&frac34;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Radnik Bijeljina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -125)</div>
								 <div class="boxdata" >647</div>
								 <div class="boxdata" >+1<br /> ( -107)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Siroki Brijeg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -106)</div>
								 <div class="boxdata" >-217</div>
								 <div class="boxdata" >-1<br /> ( -123)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Mladost Doboj Kakanj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >257</div>
								 <div class="boxdata" >+&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Sloboda Tuzla</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Tuzla City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >1150</div>
								 <div class="boxdata" >+1&frac34;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hsk Zrinjski Mostar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >-454</div>
								 <div class="boxdata" >-1&frac34;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Celik Zenica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -125)</div>
								 <div class="boxdata" >1157</div>
								 <div class="boxdata" >+2<br /> ( -128)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Zeljeznicar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -106)</div>
								 <div class="boxdata" >-526</div>
								 <div class="boxdata" >-2<br /> ( -103)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	