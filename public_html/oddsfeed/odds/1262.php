	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="South Korea Kbo">
			<caption>South Korea Kbo</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 25, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">South Korea Kbo  - Jun 26</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hanwha Eagles</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o8<br /> ( -115)</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -145 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nc Dinos</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u8<br /> ( -105)</div>
								 <div class="boxdata" >-145</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kt Wiz Suwon</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o9<br /> ( -115)</div>
								 <div class="boxdata" >115</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -160 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lotte Giants</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u9<br /> ( -105)</div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Wyverns</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o7<br /> ( +105)</div>
								 <div class="boxdata" >-140</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lg Twins</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u7<br /> ( -125)</div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -160 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Doosan Bears</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o8<br /> ( -120)</div>
								 <div class="boxdata" >-225</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Samsung Lions</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >185</div>
								 <div class="boxdata" >+1&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	