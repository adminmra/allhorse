	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nhl - Specials">
			<caption>Nhl - Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 31, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nhl - Specials  - Jun 01					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Conn Smythe Trophy Winner</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tuukka Rask</td><td>20/23</td><td>-115</td></tr><tr><td>Jordan Binnington</td><td>3/2</td><td>+150</td></tr><tr><td>Jaden Schwartz</td><td>4/1</td><td>+400</td></tr><tr><td>Brad Marchand</td><td>13/2</td><td>+650</td></tr><tr><td>Patrice Bergeron</td><td>16/1</td><td>+1600</td></tr><tr><td>Vladimir Tarasenko</td><td>8/1</td><td>+800</td></tr><tr><td>David Pastrnak</td><td>16/1</td><td>+1600</td></tr><tr><td>Ryan O'reilly</td><td>21/1</td><td>+2100</td></tr><tr><td>Alex Pietrangelo</td><td>30/1</td><td>+3000</td></tr><tr><td>David Krejci</td><td>35/1</td><td>+3500</td></tr><tr><td>David Perron</td><td>60/1</td><td>+6000</td></tr><tr><td>Torey Krug</td><td>50/1</td><td>+5000</td></tr><tr><td>Charlie Coyle</td><td>40/1</td><td>+4000</td></tr><tr><td>Brayden Schenn</td><td>55/1</td><td>+5500</td></tr><tr><td>Field</td><td>6/1</td><td>+600</td></tr><tr><td>Tyler Bozak</td><td>55/1</td><td>+5500</td></tr><tr><td>Oskar Sundqvist</td><td>55/1</td><td>+5500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	