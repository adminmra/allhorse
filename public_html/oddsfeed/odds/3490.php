	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Odds To Win The South Bracket">
			<caption>Odds To Win The South Bracket</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 28, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Odds To Win The South Bracket  - Mar 28					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Odds To Win South Region</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Virginia</td><td>10/13</td><td>-130</td></tr><tr><td>Tennessee</td><td>33/10</td><td>+330</td></tr><tr><td>Purdue</td><td>18/5</td><td>+360</td></tr><tr><td>Oregon</td><td>9/1</td><td>+900</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	