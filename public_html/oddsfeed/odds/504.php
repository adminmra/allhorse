	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Sb - Away Team Props">
			<caption>Sb - Away Team Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Sb - Away Team Props  - Feb 03</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Touchdown</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Field Goal Or Safety</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc In All Qtrs Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc In All Qtrs No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-125</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Tot Rush Yds</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o129&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Tot Rush Yds</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u129&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Tot 1st Downs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o23&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Tot 1st Downs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u23&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc Rush Td Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-250</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc Rush Td No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Score First</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-115</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Punt First</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-115</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Def Tot Sacks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -200 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Def Tot Sacks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Touchdown</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-180</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Field Goal Or Safety</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc Td 1st Qt Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-115</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc Td 1st Qt No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-115</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc Td 2nd Qt Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-290</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc Td 2nd Qt No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >230</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc Td 3rd Qt Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc Td 3rd Qt No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >145</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc Td 4th Qt Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-230</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc Td 4th Qt No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Total 3rd Down Conv </div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Total 3rd Down Conv </div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots 4th Down Conv Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-170</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots 4th Down Conv No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ne Tot Diff # Player Rush Att</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ne Tot Diff # Player Rush Att</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Players With Pass Recp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o7&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Players With Pass Recp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u7&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-140</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc 1h Rushing Td -yes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-115</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Sc 1h Rushing Td - No</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-115</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ne Cross 50 Yd Line -pass Play</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ne Cross 50 Yd Line - Run Play</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >155</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ne 1st (3rd Down) Conv - Pass</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-145</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ne 1st (3rd Down) Conv - Run</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >115</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Tot Penalty Yds</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o35&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Patriots Tot Penalty Yds</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u35&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	