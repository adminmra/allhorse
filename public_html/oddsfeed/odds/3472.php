	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - Champions Tour 3 Balls">
			<caption>Golf - Champions Tour 3 Balls</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 27, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - Champions Tour 3 Balls  - Jun 27					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Us Senior Open - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Steve Stricker</td><td>5/7</td><td>-140</td></tr><tr><td>Colin Montgomerie</td><td>2/1</td><td>+200</td></tr><tr><td>Rocco Mediate</td><td>9/2</td><td>+450</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Us Senior Open - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Scott Parel</td><td>7/4</td><td>+175</td></tr><tr><td>Kevin Sutherland</td><td>2/1</td><td>+200</td></tr><tr><td>Bernhard Langer</td><td>3/2</td><td>+150</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Us Senior Open - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Mark O`meara</td><td>33/10</td><td>+330</td></tr><tr><td>Darren Clarke</td><td>8/5</td><td>+160</td></tr><tr><td>Tom Lehman</td><td>11/10</td><td>+110</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Us Senior Open - 1st Round 3 Balls</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Joe Durant</td><td>3/1</td><td>+300</td></tr><tr><td>Paul Goydos</td><td>11/5</td><td>+220</td></tr><tr><td>Jerry Kelly</td><td>10/11</td><td>-110</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');</script>
{/literal}	
	