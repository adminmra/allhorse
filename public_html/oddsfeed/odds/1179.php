	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Croatia Hnl">
			<caption>Croatia Hnl</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated November 30, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Croatia Hnl  - Nov 30</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Inter Zapresic<br /> - vs - <br /> Istra 1961</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (130)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+213</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-110 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-150)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+116</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-110</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Slaven Belupo<br /> - vs - <br /> Lokomotiva Zagreb</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1<br /> (-125)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+487</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;EV </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1<br /> (105)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-182</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-120</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Croatia Hnl  - Dec 01</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Hnk Rijeka<br /> - vs - <br /> Osijek</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-105)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+168</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+110 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+154</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-130</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Hajduk Split<br /> - vs - <br /> Rudes</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1.5<br /> (-130)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-398</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-140 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1.5<br /> (110)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+934</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+120</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Croatia Hnl  - Dec 02</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Hnk Gorica<br /> - vs - <br /> Dinamo Zagreb</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1.5<br /> (100)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+956</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-135 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1.5<br /> (-120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-439</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+115</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	