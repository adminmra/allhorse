	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - England Championship - To Win">
			<caption>Soccer - England Championship - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - England Championship - To Win  - Sep 21					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 England Championship - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Leeds</td><td>11/10</td><td>+110</td></tr><tr><td>Fulham</td><td>5/1</td><td>+500</td></tr><tr><td>West Brom</td><td>9/1</td><td>+900</td></tr><tr><td>Cardiff</td><td>33/1</td><td>+3300</td></tr><tr><td>Stoke</td><td>100/1</td><td>+10000</td></tr><tr><td>Derby</td><td>66/1</td><td>+6600</td></tr><tr><td>Huddersfield</td><td>150/1</td><td>+15000</td></tr><tr><td>Middlesbrough</td><td>40/1</td><td>+4000</td></tr><tr><td>Brentford</td><td>33/1</td><td>+3300</td></tr><tr><td>Nottingham Forest</td><td>20/1</td><td>+2000</td></tr><tr><td>Bristol City</td><td>20/1</td><td>+2000</td></tr><tr><td>Sheffield Wednesday</td><td>25/1</td><td>+2500</td></tr><tr><td>Preston</td><td>33/1</td><td>+3300</td></tr><tr><td>Birmingham</td><td>40/1</td><td>+4000</td></tr><tr><td>Swansea</td><td>14/1</td><td>+1400</td></tr><tr><td>Blackburn</td><td>40/1</td><td>+4000</td></tr><tr><td>Hull</td><td>250/1</td><td>+25000</td></tr><tr><td>Qpr</td><td>40/1</td><td>+4000</td></tr><tr><td>Luton</td><td>250/1</td><td>+25000</td></tr><tr><td>Wigan</td><td>500/1</td><td>+50000</td></tr><tr><td>Millwall</td><td>80/1</td><td>+8000</td></tr><tr><td>Reading</td><td>80/1</td><td>+8000</td></tr><tr><td>Charlton</td><td>50/1</td><td>+5000</td></tr><tr><td>Barnsley</td><td>250/1</td><td>+25000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	