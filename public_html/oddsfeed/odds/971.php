	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="* Religion - Next Pope Specials">
			<caption>* Religion - Next Pope Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					* Religion - Next Pope Specials  - Dec 31					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Religion - Next Pope After Francis I Will Be?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Cardinal Luis Antonio Tagle (Philippines)</td><td>4/1</td><td>+400</td></tr><tr><td>Cardinal Marc Ouellet (Canada)</td><td>6/1</td><td>+600</td></tr><tr><td>Cardinal Peter Turkson (Ghana)</td><td>3/1</td><td>+300</td></tr><tr><td>Archbishop Angelo Scola (Italy)</td><td>15/2</td><td>+750</td></tr><tr><td>Cardinal Christoph Schonborn (Austria)</td><td>7/1</td><td>+700</td></tr><tr><td>Cardinal Timothy Dolan (United States)</td><td>14/1</td><td>+1400</td></tr><tr><td>Cardinal Peter Erdo (Hungary)</td><td>14/1</td><td>+1400</td></tr><tr><td>Cardinal Vincent Nichols (England)</td><td>14/1</td><td>+1400</td></tr><tr><td>Cardinal Odilo Scherer (Brazil)</td><td>17/1</td><td>+1700</td></tr><tr><td>Cardinal Leonardo Sandri (Argentina)</td><td>17/1</td><td>+1700</td></tr><tr><td>Cardinal Gianfranco Ravasi (Italy)</td><td>17/1</td><td>+1700</td></tr><tr><td>Cardinal Agostino Vallini (Italy)</td><td>17/1</td><td>+1700</td></tr><tr><td>Cardinal Joao Braz De Aviz (Brazil)</td><td>17/1</td><td>+1700</td></tr><tr><td>Cardinal Oscar Rodriguez Maradiaga (Honduras)</td><td>17/1</td><td>+1700</td></tr><tr><td>Cardinal Robert Sarah (French Guinea)</td><td>16/1</td><td>+1600</td></tr><tr><td>Cardinal Albert Malcolm Ranjith (Sri Lanka)</td><td>22/1</td><td>+2200</td></tr><tr><td>Cardinal Angelo Bagnasco (Italy)</td><td>22/1</td><td>+2200</td></tr><tr><td>Cardinal Antonio Canizares Llovera (Spain)</td><td>22/1</td><td>+2200</td></tr><tr><td>Cardinal Vinko Puljic (Bosnia)</td><td>22/1</td><td>+2200</td></tr><tr><td>Cardinal Pietro Parolin (Italy)</td><td>16/1</td><td>+1600</td></tr><tr><td>Cardinal Mauro Piacenza (Italy)</td><td>25/1</td><td>+2500</td></tr><tr><td>Patriarch Bechara Peter Rai (Lebanon)</td><td>25/1</td><td>+2500</td></tr><tr><td>Archbishop Piero Marini (Italy)</td><td>25/1</td><td>+2500</td></tr><tr><td>Cardinal Reinhard Marx (Germany)</td><td>33/1</td><td>+3300</td></tr><tr><td>Cardinal Wilfrid Napier (South Africa)</td><td>25/1</td><td>+2500</td></tr><tr><td>Cardinal Francisco Robles Ortega (Mexico)</td><td>33/1</td><td>+3300</td></tr><tr><td>Cardinal George Pell (Australia)</td><td>40/1</td><td>+4000</td></tr><tr><td>Cardinal Raymond Burke (United States)</td><td>40/1</td><td>+4000</td></tr><tr><td>Cardinal Thomas Collins (Canada)</td><td>40/1</td><td>+4000</td></tr><tr><td>Cardinal Philippe Barbarin (France)</td><td>50/1</td><td>+5000</td></tr><tr><td>Cardinal Norberto Rivera Carrera (Mexico)</td><td>66/1</td><td>+6600</td></tr><tr><td>Cardinal William Levada (United States)</td><td>100/1</td><td>+10000</td></tr><tr><td>Archbishop Diarmuid Martin (Ireland)</td><td>100/1</td><td>+10000</td></tr><tr><td>Cardinal Ivan Dias (India)</td><td>100/1</td><td>+10000</td></tr><tr><td>Richard Dawkins (Uk)</td><td>370/1</td><td>+37000</td></tr><tr><td>Father Dougal Maguire (Craggy Island)</td><td>500/1</td><td>+50000</td></tr><tr><td>Bono (Ireland)</td><td>500/1</td><td>+50000</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					* Religion - Next Pope Specials  - Dec 31					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Religion - Papal Name Of Next Pope Will Be?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Leo</td><td>33/10</td><td>+330</td></tr><tr><td>Francis</td><td>33/10</td><td>+330</td></tr><tr><td>Anastasius</td><td>25/1</td><td>+2500</td></tr><tr><td>John</td><td>4/1</td><td>+400</td></tr><tr><td>Pius</td><td>4/1</td><td>+400</td></tr><tr><td>Simon</td><td>50/1</td><td>+5000</td></tr><tr><td>Benedict</td><td>15/2</td><td>+750</td></tr><tr><td>Gregory</td><td>15/2</td><td>+750</td></tr><tr><td>Peter</td><td>4/1</td><td>+400</td></tr><tr><td>Paul</td><td>9/1</td><td>+900</td></tr><tr><td>John Paul</td><td>10/1</td><td>+1000</td></tr><tr><td>Clement</td><td>10/1</td><td>+1000</td></tr><tr><td>Boniface</td><td>14/1</td><td>+1400</td></tr><tr><td>Innocent/blessed Innocent</td><td>14/1</td><td>+1400</td></tr><tr><td>Stephen</td><td>14/1</td><td>+1400</td></tr><tr><td>Alexander</td><td>17/1</td><td>+1700</td></tr><tr><td>Joseph/josephius</td><td>17/1</td><td>+1700</td></tr><tr><td>Urban/blessed Urban</td><td>22/1</td><td>+2200</td></tr><tr><td>Celestine</td><td>22/1</td><td>+2200</td></tr><tr><td>Nicholas</td><td>25/1</td><td>+2500</td></tr><tr><td>Julius</td><td>33/1</td><td>+3300</td></tr><tr><td>Eugene</td><td>33/1</td><td>+3300</td></tr><tr><td>Paschal</td><td>40/1</td><td>+4000</td></tr><tr><td>Patrick</td><td>40/1</td><td>+4000</td></tr><tr><td>Adrian</td><td>40/1</td><td>+4000</td></tr><tr><td>Honorius</td><td>40/1</td><td>+4000</td></tr><tr><td>Theodore</td><td>50/1</td><td>+5000</td></tr><tr><td>James</td><td>50/1</td><td>+5000</td></tr><tr><td>Felix</td><td>50/1</td><td>+5000</td></tr><tr><td>Leonard</td><td>66/1</td><td>+6600</td></tr><tr><td>Valentine</td><td>66/1</td><td>+6600</td></tr><tr><td>Victor</td><td>66/1</td><td>+6600</td></tr><tr><td>Damian</td><td>66/1</td><td>+6600</td></tr><tr><td>Sylvester</td><td>66/1</td><td>+6600</td></tr><tr><td>Sixtus</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	