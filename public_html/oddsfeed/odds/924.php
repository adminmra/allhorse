	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Norway Eliteserien - To Win">
			<caption>Soccer - Norway Eliteserien - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Norway Eliteserien - To Win  - Sep 14					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-2020 Norway Eliteserien 2019 - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Molde</td><td>2/7</td><td>-350</td></tr><tr><td>Rosenborg</td><td>25/1</td><td>+2500</td></tr><tr><td>Bodo/glimt</td><td>4/1</td><td>+400</td></tr><tr><td>Odd Bk</td><td>10/1</td><td>+1000</td></tr><tr><td>Sk Brann</td><td>100/1</td><td>+10000</td></tr><tr><td>Valerenga</td><td>750/1</td><td>+75000</td></tr><tr><td>Kristiansund Bk</td><td>400/1</td><td>+40000</td></tr><tr><td>Viking Fk</td><td>1000/1</td><td>+100000</td></tr><tr><td>Haugesund</td><td>4500/1</td><td>+450000</td></tr><tr><td>Ranheim</td><td>4500/1</td><td>+450000</td></tr><tr><td>Lillestrom</td><td>4500/1</td><td>+450000</td></tr><tr><td>Mjondalen</td><td>4500/1</td><td>+450000</td></tr><tr><td>Stabaek</td><td>4500/1</td><td>+450000</td></tr><tr><td>Tromso</td><td>4500/1</td><td>+450000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	