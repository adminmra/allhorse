	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - Lpga Tour - To Win">
			<caption>Golf - Lpga Tour - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - Lpga Tour - To Win  - Sep 13					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Solheim Cup - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Europe</td><td>7/4</td><td>+175</td></tr><tr><td>Usa</td><td>4/7</td><td>-175</td></tr><tr><td>Tie</td><td>12/1</td><td>+1200</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	