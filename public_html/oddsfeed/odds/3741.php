	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--azerbaijan First Division">
			<caption>Pre--azerbaijan First Division</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--azerbaijan First Division  - Sep 19</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Kapaz</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >198</div>
								 <div class="boxdata" >+&frac12;<br /> ( -158 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Turan Tovuz</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >121</div>
								 <div class="boxdata" >-&frac12;<br /> ( +118 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pfk Neftci Baku-2</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >-333</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Agsu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >702</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Moik Baku</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Sumgayit-2</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -109 )</div>
								 <div class="boxdata" >214</div>
								 <div class="boxdata" >+&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	