	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba - Contest Game Props">
			<caption>Nba - Contest Game Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 13, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nba - Contest Game Props  - Jun 13					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Raptors Vs Warriors - Winning Margin</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Raptors Win By 1-2 Pts</td><td>11/1</td><td>+1100</td></tr><tr><td>Raptors Win By 3-6 Pts</td><td>6/1</td><td>+600</td></tr><tr><td>Raptors Win By 7-9 Pts</td><td>10/1</td><td>+1000</td></tr><tr><td>Raptors Win By 10-13 Pts</td><td>10/1</td><td>+1000</td></tr><tr><td>Raptors Win By 14-16 Pts</td><td>18/1</td><td>+1800</td></tr><tr><td>Raptors Win By 17-20 Pts</td><td>20/1</td><td>+2000</td></tr><tr><td>Raptors Win By 21 Or More </td><td>12/1</td><td>+1200</td></tr><tr><td>Warriors Win By 1-2 Pts</td><td>11/1</td><td>+1100</td></tr><tr><td>Warriors Win By 3-6 Pts</td><td>11/2</td><td>+550</td></tr><tr><td>Warriors Win By 7-9 Pts</td><td>8/1</td><td>+800</td></tr><tr><td>Warriors Win By 10-13 Pts</td><td>15/2</td><td>+750</td></tr><tr><td>Warriors Win By 14-16 Pts</td><td>12/1</td><td>+1200</td></tr><tr><td>Warriors Win By 17-20 Pts</td><td>14/1</td><td>+1400</td></tr><tr><td>Warriors Win By 21 Or More Pts</td><td>15/2</td><td>+750</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Raptors Vs Warriors - Double Result</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Raptors 1h/ Raptors Ft</td><td>11/5</td><td>+220</td></tr><tr><td>Raptors 1h/ Warriors Ft</td><td>11/2</td><td>+550</td></tr><tr><td>Tie 1h/ Raptors</td><td>25/1</td><td>+2500</td></tr><tr><td>Tie 1h/ Warriors</td><td>22/1</td><td>+2200</td></tr><tr><td>Warriors 1h/ Raptors Ft</td><td>11/2</td><td>+550</td></tr><tr><td>Warriors 1h/ Warriors Ft</td><td>6/5</td><td>+120</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	