	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--hockey - Sweden (allsvenskan)">
			<caption>Live--hockey - Sweden (allsvenskan)</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 6, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--hockey - Sweden (allsvenskan)  - Mar 06</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aik If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >-111</div>
								 <div class="boxdata" >+1<br /> ( -222)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sodertalje Sk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-133</div>
								 <div class="boxdata" >-1<br /> ( +140)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hc Vita Hasten</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5<br /> ( -142)</div>
								 <div class="boxdata" >305</div>
								 <div class="boxdata" >+2<br /> ( -142)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Leksands If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5<br /> ( -105)</div>
								 <div class="boxdata" >-555</div>
								 <div class="boxdata" >-2<br /> ( -105)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ik Pantern</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5<br /> ( -166)</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >+1<br /> ( -133)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >If Bjorkloven</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5<br /> ( +104)</div>
								 <div class="boxdata" >-200</div>
								 <div class="boxdata" >-1<br /> ( -111)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vik Vasteraas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-133</div>
								 <div class="boxdata" >-1<br /> ( +145)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bik Karlskoga</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -142 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >+1<br /> ( -222)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	