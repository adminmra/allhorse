	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Ascot Gold Cup">
			<caption>Horses - Ascot Gold Cup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 20, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Ascot Gold Cup  - Jun 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Ascto Gold Cup - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Called To The Bar</td><td>18/1</td><td>+1800</td></tr><tr><td>Capri</td><td>16/1</td><td>+1600</td></tr><tr><td>Magic Circle</td><td>20/1</td><td>+2000</td></tr><tr><td>Stradivarius</td><td>6/5</td><td>+120</td></tr><tr><td>Thomas Hobson</td><td>20/1</td><td>+2000</td></tr><tr><td>Cross Counter</td><td>4/1</td><td>+400</td></tr><tr><td>Cypress Creek</td><td>100/1</td><td>+10000</td></tr><tr><td>Dee Ex Bee</td><td>6/1</td><td>+600</td></tr><tr><td>Flag Of Honour</td><td>11/1</td><td>+1100</td></tr><tr><td>Master Of Reality</td><td>66/1</td><td>+6600</td></tr><tr><td>Raymond Tusk</td><td>40/1</td><td>+4000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	