	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mlb - Player Props">
			<caption>Mlb - Player Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 17, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Mlb - Player Props  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Harper H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Harper H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Freeman H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Freeman H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Hoskins H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -120)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Hoskins H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -110)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Acuna Jr H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Acuna Jr H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >V Velasquez Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >V Velasquez Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >V Velasquez Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >V Velasquez Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >V Velasquez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +280 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >V Velasquez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -410 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Keuchel Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Keuchel Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Keuchel Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o18&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Keuchel Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u18&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Keuchel Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +345 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Keuchel Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -445 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Hernandez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Hernandez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Segura Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Segura Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Harper Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +290 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Harper Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -450 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Harper Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Harper Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Dickerson Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Dickerson Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jt Realmuto Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +350 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jt Realmuto Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -555 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jt Realmuto Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jt Realmuto Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Hoskins Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +330 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Hoskins Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -545 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Hoskins Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Hoskins Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Kingery Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +390 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Kingery Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -635 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Kingery Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Kingery Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Mccann Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +345 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Mccann Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -560 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Mccann Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Mccann Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Markakis Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Markakis Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Joyce Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +375 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Joyce Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -620 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Joyce Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Joyce Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Donaldson Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +250 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Donaldson Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -355 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Donaldson Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Donaldson Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Freeman Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +245 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Freeman Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -350 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Freeman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Freeman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Swanson Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +385 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Swanson Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -630 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Swanson Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Swanson Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >O Albies Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +370 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >O Albies Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -610 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >O Albies Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >O Albies Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Acuna Jr Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +240 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Acuna Jr Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -340 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Acuna Jr Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Acuna Jr Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Rodriguez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Rodriguez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - Player Props  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >H Renfroe H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -105)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >H Renfroe H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -125)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Machado H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Machado H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Moustakas H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -125)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Moustakas H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -105)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Grandal H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -125)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Grandal H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -105)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Hosmer Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Hosmer Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Paddack Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Paddack Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Paddack Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Paddack Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Paddack Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o17&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Paddack Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u17&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Paddack Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +325 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Paddack Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -425 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Myers Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Myers Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Machado Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +360 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Machado Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -600 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Machado Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Machado Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Hedges Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Hedges Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Martini Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -200 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Martini Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Margot Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Margot Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Urias Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Urias Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T France Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T France Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Cain Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Cain Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Braun Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +355 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Braun Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -545 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Braun Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Braun Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Grandal Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +295 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Grandal Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -430 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Grandal Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Grandal Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Moustakas Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +270 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Moustakas Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -400 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Moustakas Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Moustakas Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Thames Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +295 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Thames Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -430 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Thames Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Thames Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Spangenberg Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Spangenberg Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >O Arcia Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >O Arcia Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Grisham Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +315 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Grisham Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -500 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Grisham Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Grisham Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - Player Props  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Rendon H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -125)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Rendon H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -105)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Soto H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -125)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Soto H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -105)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Dejong H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Dejong H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -130)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Goldschmidt H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Goldschmidt H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Cabrera Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Cabrera Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Zimmerman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -180 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Zimmerman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Gomes Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Gomes Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Rendon Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +340 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Rendon Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -530 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Rendon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Rendon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Corbin Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +255 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Corbin Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -315 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Corbin Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Corbin Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Corbin Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Corbin Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Corbin Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o18&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Corbin Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u18&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Eaton Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Eaton Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Turner Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Turner Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >V Robles Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >V Robles Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Soto Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +310 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Soto Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -445 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Soto Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Soto Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Molina Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Molina Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Fowler Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Fowler Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Goldschmidt Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +360 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Goldschmidt Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -600 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Goldschmidt Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Goldschmidt Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Ozuna Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +380 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Ozuna Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -625 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Ozuna Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Ozuna Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Wong Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -190 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Wong Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Mikolas Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +400 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Mikolas Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -600 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Mikolas Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Mikolas Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Mikolas Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Mikolas Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Dejong Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +380 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Dejong Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -625 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Dejong Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Dejong Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >H Bader Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >H Bader Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Edman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Edman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - Player Props  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Rizzo H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -130)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Rizzo H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Aquino H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Aquino H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -130)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Suarez H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( +110)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Suarez H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -140)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Castellanos H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Castellanos H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Darvish Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Darvish Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Darvish Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Darvish Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Darvish Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o17&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Darvish Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u17&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Darvish Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +400 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Darvish Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -600 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Gray Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Gray Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Gray Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Gray Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Gray Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o17&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Gray Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u17&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Gray Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +345 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Gray Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -445 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Zobrist Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -190 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Zobrist Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Heyward Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Heyward Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Rizzo Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Rizzo Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Contreras Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Contreras Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Bryant Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Bryant Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Castellanos Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Castellanos Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Schwarber Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +330 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Schwarber Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -515 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Schwarber Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Schwarber Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Hoerner Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Hoerner Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Votto Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -200 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Votto Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Galvis Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Galvis Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Suarez Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +275 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Suarez Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -405 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Suarez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Suarez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Barnhart Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Barnhart Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Iglesias Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Iglesias Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Aquino Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +340 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Aquino Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -530 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Aquino Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Aquino Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Vanmeter Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Vanmeter Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B O'grady Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B O'grady Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - Player Props  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Conforto H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Conforto H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Arenado H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Arenado H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Alonso H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Alonso H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Story H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Story H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >I Desmond Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >I Desmond Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Blackmon Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +300 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Blackmon Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -435 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Blackmon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Blackmon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Melville Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Melville Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Melville Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Melville Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Melville Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o15&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Melville Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u15&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Melville Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +300 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Melville Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -400 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Murphy Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +360 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Murphy Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -600 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Murphy Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Murphy Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Wolters Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Wolters Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Arenado Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +215 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Arenado Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -300 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Arenado Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Arenado Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Story Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +245 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Story Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -350 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Story Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Story Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Tapia Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Tapia Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Mcmahon Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +315 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Mcmahon Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -500 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Mcmahon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Mcmahon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Cano Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +260 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Cano Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -365 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Cano Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Cano Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Ramos Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +300 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Ramos Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -435 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Ramos Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Ramos Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Stroman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +240 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Stroman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -300 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Stroman Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Stroman Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Stroman Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Stroman Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jd Davis Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +255 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jd Davis Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -360 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jd Davis Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jd Davis Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Nimmo Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +290 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Nimmo Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -425 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Nimmo Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Nimmo Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Alonso Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Alonso Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -210 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Alonso Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Alonso Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Conforto Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +180 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Conforto Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -260 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Conforto Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Conforto Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Rosario Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Rosario Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Mcneil Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +225 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Mcneil Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -310 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Mcneil Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Mcneil Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - Player Props  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Escobar H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Escobar H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Alfaro H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Alfaro H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -130)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Marte H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -120)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Marte H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -110)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Castro H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -125)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Castro H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -105)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Young Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Young Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Young Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Young Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Young Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +410 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Young Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -610 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Jones Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +375 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Jones Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -615 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Jones Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Jones Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Escobar Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +245 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Escobar Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -350 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Escobar Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Escobar Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Flores Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +335 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Flores Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -525 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Flores Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Flores Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Walker Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +290 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Walker Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -420 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Walker Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Walker Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Ahmed Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +350 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Ahmed Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -540 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Ahmed Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Ahmed Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Marte Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +270 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Marte Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -400 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Marte Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Marte Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Kelly Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +335 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Kelly Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -520 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Kelly Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Kelly Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Locastro Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +385 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Locastro Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -630 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Locastro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Locastro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Walker Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Walker Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Rojas Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Rojas Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Castro Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +320 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Castro Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -505 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Castro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Castro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Smith Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +320 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Smith Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -420 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Smith Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Smith Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Smith Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Smith Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Smith Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o17&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Smith Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u17&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Alfaro Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +345 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Alfaro Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -535 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Alfaro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Alfaro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Brinson Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Brinson Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Sierra Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Sierra Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >G Cooper Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +330 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >G Cooper Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -515 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >G Cooper Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >G Cooper Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >I Diaz Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +330 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >I Diaz Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -515 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >I Diaz Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >I Diaz Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - Player Props  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Plutko Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Plutko Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Plutko Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Plutko Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Plutko Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o17&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Plutko Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u17&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Cabrera Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Cabrera Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >H Castro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >H Castro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Lugo Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -185 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Lugo Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Stewart Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +315 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Stewart Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -500 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Stewart Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Stewart Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >V Reyes Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >V Reyes Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Demeritte Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +360 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Demeritte Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -600 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Demeritte Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Demeritte Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Dixon Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +330 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Dixon Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -515 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Dixon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Dixon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Castro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Castro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Rogers Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +340 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Rogers Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -530 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Rogers Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Rogers Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Puig Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Puig Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Chang Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Chang Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Rodriguez Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +320 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Rodriguez Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -505 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Rodriguez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Rodriguez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Perez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Perez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >O Mercado Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >O Mercado Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Freeman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Freeman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Mercer Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -180 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Mercer Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Luplow Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +380 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Luplow Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -620 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Luplow Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Luplow Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >G Greiner Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >G Greiner Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Puig Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +360 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Puig Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -600 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Reyes Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +275 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Reyes Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -405 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Reyes Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Reyes Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Lindor Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +290 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Lindor Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -420 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Lindor Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F Lindor Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Santana Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +300 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Santana Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -435 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Santana Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Santana Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - Player Props  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Rosario H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Rosario H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Abreu H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Abreu H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Kepler H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Kepler H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Moncada H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Moncada H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Cruz Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +210 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Cruz Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -295 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Cruz Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Cruz Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Castro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Castro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cj Cron Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +295 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cj Cron Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -430 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cj Cron Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cj Cron Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Rosario Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +370 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Rosario Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -610 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Rosario Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Rosario Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Polanco Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +370 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Polanco Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -610 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Polanco Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Polanco Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Sano Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +235 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Sano Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -335 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Sano Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Sano Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Kepler Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +280 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Kepler Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -410 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Kepler Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Kepler Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Arraez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Arraez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Perez Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Perez Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Perez Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o16&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Perez Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u16&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Perez Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Perez Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Wade Jr Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Wade Jr Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Mccann Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Mccann Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Garcia Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Garcia Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Abreu Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +300 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Abreu Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -435 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Abreu Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Abreu Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Sanchez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -200 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Sanchez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Anderson Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Anderson Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Z Collins Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Z Collins Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Engel Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Engel Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Jimenez Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +315 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Jimenez Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -500 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Jimenez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Jimenez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Moncada Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Moncada Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Mccann Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +390 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Mccann Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -635 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - Player Props  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Bregman H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Bregman H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Santana H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Santana H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Altuve H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Altuve H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Odor H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Odor H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Brantley Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Brantley Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Gurriel Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +370 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Gurriel Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -610 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Gurriel Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Gurriel Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Reddick Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Reddick Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Altuve Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +350 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Altuve Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -540 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Altuve Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Altuve Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >G Springer Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +255 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >G Springer Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -360 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >G Springer Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >G Springer Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Bregman Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +335 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Bregman Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -520 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Bregman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Bregman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Toro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Toro Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Alvarez Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +270 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Alvarez Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -400 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Alvarez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Y Alvarez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Soo Choo Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Soo Choo Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Andrus Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -190 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Andrus Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Deshields Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Deshields Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Odor Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +330 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Odor Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -515 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Odor Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Odor Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Mazara Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Mazara Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Guzman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Guzman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Calhoun Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +330 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Calhoun Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -515 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Calhoun Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Calhoun Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Verlander Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Verlander Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Verlander Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Verlander Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Lynn Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Lynn Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Lynn Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Lynn Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Lynn Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o17&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Lynn Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u17&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Trevino Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Trevino Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Solak Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Solak Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Maldonado Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Maldonado Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - Player Props  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >H Dozier H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -105)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >H Dozier H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -125)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Soler H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Soler H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Chapman H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Chapman H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Olson H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Olson H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Gordon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Gordon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -190 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Merrifield Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Merrifield Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Cuthbert Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -180 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Cuthbert Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Mondesi Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Mondesi Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Soler Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +280 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Soler Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -410 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Soler Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Soler Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Lopez Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Lopez Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >H Dozier Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +350 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >H Dozier Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -540 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >H Dozier Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >H Dozier Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Mcbroom Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Mcbroom Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Dini Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Dini Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Lopez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Lopez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Davis Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +380 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Davis Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -625 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Davis Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Davis Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Grossman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Grossman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Anderson Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Anderson Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Anderson Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Anderson Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Anderson Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o17&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Anderson Pitching Outs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u17&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Semien Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +335 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Semien Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -525 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Semien Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Semien Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Phegley Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Phegley Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Canha Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +355 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Canha Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -545 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Canha Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Canha Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Profar Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Profar Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Olson Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +270 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Olson Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -400 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Olson Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Olson Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Chapman Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +270 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Chapman Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -400 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Chapman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Chapman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Brown Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +355 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Brown Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -545 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Brown Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Brown Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - Player Props  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Diaz Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Diaz Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Moran Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Moran Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Bell Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +335 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Bell Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -520 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Bell Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Bell Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Newman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Newman Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Frazier Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Frazier Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Reynolds Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Reynolds Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Nola Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Nola Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Gordon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Gordon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >O Narvaez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >O Narvaez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Seager Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +350 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Seager Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -540 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Seager Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Seager Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Lewis Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Lewis Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Long Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Long Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Vogelbach Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +340 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Vogelbach Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -530 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Vogelbach Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Vogelbach Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Walton Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Walton Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Osuna Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Osuna Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Reyes Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >P Reyes Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Gonzales Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Gonzales Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Gonzales Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Gonzales Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Keller Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Keller Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Keller Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Keller Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - Player Props  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jd Martinez Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +335 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jd Martinez Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -525 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jd Martinez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jd Martinez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Moreland Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Moreland Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Vazquez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Vazquez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >X Bogaerts Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +330 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >X Bogaerts Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -515 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >X Bogaerts Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >X Bogaerts Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Bradley Jr Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Bradley Jr Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Betts Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +350 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Betts Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -540 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Betts Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Betts Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Benintendi Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Benintendi Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Devers Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +335 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Devers Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -525 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Devers Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >R Devers Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Longoria Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Longoria Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Posey Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Posey Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Belt Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Belt Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Vogt Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >S Vogt Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Crawford Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B Crawford Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Yastrzemski Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +335 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Yastrzemski Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -520 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Yastrzemski Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Yastrzemski Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Pillar Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Pillar Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Dubon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -180 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Dubon Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Eovaldi Earned Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Eovaldi Earned Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -175 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Eovaldi Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Eovaldi Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Webb Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Webb Hits Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Webb Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >L Webb Er Allowed</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Davis Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Davis Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Hernandez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -190 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Hernandez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Mlb - Player Props  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Meadows H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Meadows H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Bellinger H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Bellinger H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Muncy H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >M Muncy H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Pham H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -105)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Pham H+r+rbi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -125)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Freese Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +340 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Freese Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -530 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Freese Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >D Freese Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aj Pollock Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +320 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aj Pollock Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -505 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aj Pollock Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aj Pollock Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Seager Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Seager Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Bellinger Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +290 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Bellinger Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -420 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Bellinger Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Bellinger Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Smith Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +280 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Smith Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -410 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Smith Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -155 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >W Smith Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Pham Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >T Pham Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -170 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Wendle Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Wendle Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -165 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Meadows Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +275 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Meadows Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -405 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Meadows Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >A Meadows Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Wong Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >K Wong Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Lowe Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +380 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Lowe Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -625 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Lowe Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -160 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >N Lowe Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Hernandez Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +390 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Hernandez Tot Hr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -635 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Hernandez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >E Hernandez Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Gyorko Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -150 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >J Gyorko Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Taylor Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >C Taylor Tot Bases</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	