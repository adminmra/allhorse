	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Queen Anne Stakes">
			<caption>Horses - Queen Anne Stakes</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 13, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Queen Anne Stakes  - Jun 18					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Queen Anne Stakes - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Le Brivido</td><td>4/1</td><td>+400</td></tr><tr><td>Mustashry</td><td>9/2</td><td>+450</td></tr><tr><td>Barney Roy</td><td>13/2</td><td>+650</td></tr><tr><td>Laurens</td><td>7/1</td><td>+700</td></tr><tr><td>Accidental Agent</td><td>12/1</td><td>+1200</td></tr><tr><td>Lord Glitters</td><td>12/1</td><td>+1200</td></tr><tr><td>Hazapour</td><td>14/1</td><td>+1400</td></tr><tr><td>Magical</td><td>14/1</td><td>+1400</td></tr><tr><td>Olmedo</td><td>16/1</td><td>+1600</td></tr><tr><td>I Can Fly</td><td>20/1</td><td>+2000</td></tr><tr><td>Dream Castle</td><td>20/1</td><td>+2000</td></tr><tr><td>One Master</td><td>20/1</td><td>+2000</td></tr><tr><td>Sharja Bridge</td><td>20/1</td><td>+2000</td></tr><tr><td>Beat The Bank</td><td>25/1</td><td>+2500</td></tr><tr><td>Matterhorn</td><td>25/1</td><td>+2500</td></tr><tr><td>Mythical Magic</td><td>25/1</td><td>+2500</td></tr><tr><td>Stormy Antarctic</td><td>33/1</td><td>+3300</td></tr><tr><td>Romanised</td><td>40/1</td><td>+4000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	