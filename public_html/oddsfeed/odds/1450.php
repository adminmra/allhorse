	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - 1000 Guineas">
			<caption>Horses - 1000 Guineas</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - 1000 Guineas  - May 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">1000 Guineas - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Daahyeh</td><td>20/1</td><td>+2000</td></tr><tr><td>Etoile</td><td>25/1</td><td>+2500</td></tr><tr><td>Final Song</td><td>33/1</td><td>+3300</td></tr><tr><td>Raffle Prize</td><td>14/1</td><td>+1400</td></tr><tr><td>Tango</td><td>50/1</td><td>+5000</td></tr><tr><td>Summer Romance</td><td>33/1</td><td>+3300</td></tr><tr><td>Divine Spirit</td><td>50/1</td><td>+5000</td></tr><tr><td>Heaven Of Heavens</td><td>40/1</td><td>+4000</td></tr><tr><td>Albigna</td><td>16/1</td><td>+1600</td></tr><tr><td>Peace Charter</td><td>50/1</td><td>+5000</td></tr><tr><td>Star Spirit</td><td>40/1</td><td>+4000</td></tr><tr><td>So Wonderful</td><td>50/1</td><td>+5000</td></tr><tr><td>Galadriel</td><td>50/1</td><td>+5000</td></tr><tr><td>Alabama Whitman</td><td>50/1</td><td>+5000</td></tr><tr><td>Silent Wave</td><td>33/1</td><td>+3300</td></tr><tr><td>Windracer</td><td>33/1</td><td>+3300</td></tr><tr><td>Ultra Violet</td><td>100/1</td><td>+10000</td></tr><tr><td>Salsa</td><td>33/1</td><td>+3300</td></tr><tr><td>Love</td><td>25/1</td><td>+2500</td></tr><tr><td>Alpine Star</td><td>33/1</td><td>+3300</td></tr><tr><td>Ennistymon</td><td>50/1</td><td>+5000</td></tr><tr><td>Baaqy</td><td>50/1</td><td>+5000</td></tr><tr><td>National Treasure</td><td>66/1</td><td>+6600</td></tr><tr><td>Miss Yoda</td><td>20/1</td><td>+2000</td></tr><tr><td>Cayenne Pepper</td><td>16/1</td><td>+1600</td></tr><tr><td>Petite Mustique</td><td>33/1</td><td>+3300</td></tr><tr><td>Theory Of Time</td><td>50/1</td><td>+5000</td></tr><tr><td>Under The Stars</td><td>66/1</td><td>+6600</td></tr><tr><td>Queen Daenerys</td><td>50/1</td><td>+5000</td></tr><tr><td>Soffika</td><td>50/1</td><td>+5000</td></tr><tr><td>Wejdan</td><td>80/1</td><td>+8000</td></tr><tr><td>Na Blianta Beo</td><td>100/1</td><td>+10000</td></tr><tr><td>West End Girl</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	