	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Kentucky Oaks">
			<caption>Horses - Kentucky Oaks</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Kentucky Oaks  - May 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Kentucky Oaks - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Bellafina</td><td>7/4</td><td>+175</td></tr><tr><td>Jaywalk</td><td>10/1</td><td>+1000</td></tr><tr><td>Jeltrin</td><td>12/1</td><td>+1200</td></tr><tr><td>Restless Rider</td><td>7/1</td><td>+700</td></tr><tr><td>Flor De La Mar</td><td>12/1</td><td>+1200</td></tr><tr><td>Dunbar Road</td><td>12/1</td><td>+1200</td></tr><tr><td>Champagne Anyone</td><td>8/1</td><td>+800</td></tr><tr><td>Serengeti Empress</td><td>10/1</td><td>+1000</td></tr><tr><td>Liora</td><td>18/1</td><td>+1800</td></tr><tr><td>Motion Emotion</td><td>16/1</td><td>+1600</td></tr><tr><td>Positive Spirit</td><td>20/1</td><td>+2000</td></tr><tr><td>Chocolate Kisses</td><td>20/1</td><td>+2000</td></tr><tr><td>Street Band</td><td>20/1</td><td>+2000</td></tr><tr><td>Out For A Spin</td><td>16/1</td><td>+1600</td></tr><tr><td>Lady Apple</td><td>16/1</td><td>+1600</td></tr><tr><td>Point Of Honor</td><td>40/1</td><td>+4000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	