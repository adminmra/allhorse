	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--czech Republic First League">
			<caption>Pre--czech Republic First League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--czech Republic First League  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Mlada Boleslav</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Teplice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -117 )</div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--czech Republic First League  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Fastav Zlin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >243</div>
								 <div class="boxdata" >+&frac12;<br /> ( -138 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bohemians Prague 1905</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac12;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--czech Republic First League  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Viktoria Plzen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" >-1<br /> ( +112)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Dynamo Ceske Budejovice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >384</div>
								 <div class="boxdata" >+1<br /> ( -149)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mfk Karvina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >241</div>
								 <div class="boxdata" >+&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1 Fk Pribram</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac12;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Teplice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >492</div>
								 <div class="boxdata" >+1<br /> ( -125)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1 Fc Slovacko</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" >-1<br /> ( -106)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Slovan Liberec</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac12;<br /> ( +112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sfc Opava</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >236</div>
								 <div class="boxdata" >+&frac12;<br /> ( -149 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--czech Republic First League  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Slavia Prague</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Sparta Prague</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >194</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Banik Ostrava</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >298</div>
								 <div class="boxdata" >+&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Mlada Boleslav</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -126 )</div>
								 <div class="boxdata" >-123</div>
								 <div class="boxdata" >-&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--czech Republic First League  - Sep 23</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Sigma Olomouc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >350</div>
								 <div class="boxdata" >+&frac12;<br /> ( +106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Jablonec</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >-138</div>
								 <div class="boxdata" >-&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	