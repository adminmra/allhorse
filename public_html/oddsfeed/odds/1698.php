	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--iceland Pepsideild">
			<caption>Live--iceland Pepsideild</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 22, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--iceland Pepsideild  - Jun 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ibv Vestmannaeyjar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( +140 )</div>
								 <div class="boxdata" >2700</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Breidablik Kopavogur</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -222 )</div>
								 <div class="boxdata" >-999</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	