	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Japan Npb">
			<caption>Japan Npb</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 17, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Japan Npb  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yomiuri Giants</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o7&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >-130</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +145 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chunichi Dragons</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u7&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -170 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yakult Swallows</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o7<br /> ( -115)</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -225 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hanshin Tigers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u7<br /> ( -105)</div>
								 <div class="boxdata" >-120</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +185 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rakuten Gold. Eagles</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o8<br /> ( -105)</div>
								 <div class="boxdata" >130</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -165 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fukuoka S. Hawks</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u8<br /> ( -115)</div>
								 <div class="boxdata" >-150</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Orix Buffaloes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o9&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >205</div>
								 <div class="boxdata" >+1&frac12;<br /> ( +115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Seibu Lions</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u9&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >-255</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hiroshima Carp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o7&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" >130</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -170 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yokohama Baystars</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u7&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-150</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +145 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	