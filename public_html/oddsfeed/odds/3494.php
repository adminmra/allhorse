	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Exact Finals Matchup">
			<caption>Exact Finals Matchup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 28, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Exact Finals Matchup  - Mar 28					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Ncaa Tournament - Exact Finals Matchup</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Duke Vs Virginia</td><td>59/10</td><td>+590</td></tr><tr><td>Duke Vs Tennessee</td><td>22/1</td><td>+2200</td></tr><tr><td>Duke Vs Purdue</td><td>21/1</td><td>+2100</td></tr><tr><td>Duke Vs Oregon</td><td>46/1</td><td>+4600</td></tr><tr><td>Duke Vs North Carolina</td><td>41/5</td><td>+820</td></tr><tr><td>Duke Vs Kentucky</td><td>19/1</td><td>+1900</td></tr><tr><td>Duke Vs Houston</td><td>36/1</td><td>+3600</td></tr><tr><td>Duke Vs Auburn</td><td>42/1</td><td>+4200</td></tr><tr><td>Michigan State Vs Virginia</td><td>23/1</td><td>+2300</td></tr><tr><td>Michigan State Vs Tennessee</td><td>82/1</td><td>+8200</td></tr><tr><td>Michigan State Vs Purdue</td><td>77/1</td><td>+7700</td></tr><tr><td>Michigan State Vs Oregon</td><td>165/1</td><td>+16500</td></tr><tr><td>Michigan State Vs North Carolina</td><td>31/1</td><td>+3100</td></tr><tr><td>Michigan State Vs Kentucky</td><td>72/1</td><td>+7200</td></tr><tr><td>Michigan State Vs Houston</td><td>131/1</td><td>+13100</td></tr><tr><td>Michigan State Vs Auburn</td><td>150/1</td><td>+15000</td></tr><tr><td>Lsu Vs Virginia</td><td>90/1</td><td>+9000</td></tr><tr><td>Lsu Vs Tennessee</td><td>309/1</td><td>+30900</td></tr><tr><td>Lsu Vs Purdue</td><td>291/1</td><td>+29100</td></tr><tr><td>Lsu Vs Oregon</td><td>620/1</td><td>+62000</td></tr><tr><td>Lsu Vs North Carolina</td><td>121/1</td><td>+12100</td></tr><tr><td>Lsu Vs Kentucky</td><td>273/1</td><td>+27300</td></tr><tr><td>Lsu Vs Houston</td><td>492/1</td><td>+49200</td></tr><tr><td>Lsu Vs Auburn</td><td>565/1</td><td>+56500</td></tr><tr><td>Virginia Tech Vs Oregon</td><td>298/1</td><td>+29800</td></tr><tr><td>Virginia Tech Vs North Carolina</td><td>58/1</td><td>+5800</td></tr><tr><td>Virginia Tech Vs Kentucky</td><td>131/1</td><td>+13100</td></tr><tr><td>Virginia Tech Vs Houston</td><td>237/1</td><td>+23700</td></tr><tr><td>Virginia Tech Vs Auburn</td><td>272/1</td><td>+27200</td></tr><tr><td>Gonzaga Vs Virginia</td><td>7/1</td><td>+700</td></tr><tr><td>Gonzaga Vs Tennessee</td><td>29/1</td><td>+2900</td></tr><tr><td>Gonzaga Vs Purdue</td><td>27/1</td><td>+2700</td></tr><tr><td>Gonzaga Vs Oregon</td><td>60/1</td><td>+6000</td></tr><tr><td>Gonzaga Vs North Carolina</td><td>11/1</td><td>+1100</td></tr><tr><td>Gonzaga Vs Kentucky</td><td>25/1</td><td>+2500</td></tr><tr><td>Gonzaga Vs Houston</td><td>47/1</td><td>+4700</td></tr><tr><td>Gonzaga Vs Auburn</td><td>54/1</td><td>+5400</td></tr><tr><td>Michigan Vs Virginia</td><td>28/1</td><td>+2800</td></tr><tr><td>Michigan Vs Tennessee</td><td>98/1</td><td>+9800</td></tr><tr><td>Michigan Vs Purdue</td><td>93/1</td><td>+9300</td></tr><tr><td>Michigan Vs Oregon</td><td>198/1</td><td>+19800</td></tr><tr><td>Michigan Vs North Carolina</td><td>38/1</td><td>+3800</td></tr><tr><td>Michigan Vs Kentucky</td><td>87/1</td><td>+8700</td></tr><tr><td>Michigan Vs Houston</td><td>157/1</td><td>+15700</td></tr><tr><td>Michigan Vs Auburn</td><td>181/1</td><td>+18100</td></tr><tr><td>Texas Tech Vs Virginia</td><td>31/1</td><td>+3100</td></tr><tr><td>Texas Tech Vs Tennessee</td><td>110/1</td><td>+11000</td></tr><tr><td>Texas Tech Vs Purdue</td><td>103/1</td><td>+10300</td></tr><tr><td>Texas Tech Vs Oregon</td><td>221/1</td><td>+22100</td></tr><tr><td>Texas Tech Vs North Carolina</td><td>42/1</td><td>+4200</td></tr><tr><td>Texas Tech Vs Kentucky</td><td>96/1</td><td>+9600</td></tr><tr><td>Texas Tech Vs Houston</td><td>175/1</td><td>+17500</td></tr><tr><td>Texas Tech Vs Auburn</td><td>201/1</td><td>+20100</td></tr><tr><td>Florida State Vs Virginia</td><td>43/1</td><td>+4300</td></tr><tr><td>Florida State Vs Tennessee</td><td>148/1</td><td>+14800</td></tr><tr><td>Florida State Vs Purdue</td><td>140/1</td><td>+14000</td></tr><tr><td>Florida State Vs Oregon</td><td>298/1</td><td>+29800</td></tr><tr><td>Florida State Vs North Carolina</td><td>58/1</td><td>+5800</td></tr><tr><td>Florida State Vs Kentucky</td><td>131/1</td><td>+13100</td></tr><tr><td>Florida State Vs Houston</td><td>237/1</td><td>+23700</td></tr><tr><td>Florida State Vs Auburn</td><td>272/1</td><td>+27200</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	