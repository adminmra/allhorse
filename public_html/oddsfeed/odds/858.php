	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Europa League - Outrights">
			<caption>Soccer - Europa League - Outrights</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Europa League - Outrights  - Sep 19					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Uefa Europa League - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Manchester United</td><td>11/2</td><td>+550</td></tr><tr><td>Arsenal</td><td>15/2</td><td>+750</td></tr><tr><td>Sevilla</td><td>14/1</td><td>+1400</td></tr><tr><td>Wolverhampton</td><td>16/1</td><td>+1600</td></tr><tr><td>Roma</td><td>20/1</td><td>+2000</td></tr><tr><td>Wolfsburg</td><td>33/1</td><td>+3300</td></tr><tr><td>Lazio</td><td>25/1</td><td>+2500</td></tr><tr><td>Borussia Monchengladbach</td><td>50/1</td><td>+5000</td></tr><tr><td>Fc Porto</td><td>50/1</td><td>+5000</td></tr><tr><td>Psv</td><td>50/1</td><td>+5000</td></tr><tr><td>Sporting</td><td>50/1</td><td>+5000</td></tr><tr><td>Eintracht Frankfurt</td><td>66/1</td><td>+6600</td></tr><tr><td>Dynamo Kiev</td><td>66/1</td><td>+6600</td></tr><tr><td>Espanyol</td><td>66/1</td><td>+6600</td></tr><tr><td>Cska Moscow</td><td>66/1</td><td>+6600</td></tr><tr><td>Getafe</td><td>80/1</td><td>+8000</td></tr><tr><td>St Etienne</td><td>80/1</td><td>+8000</td></tr><tr><td>Besiktas</td><td>80/1</td><td>+8000</td></tr><tr><td>Basel</td><td>80/1</td><td>+8000</td></tr><tr><td>Krasnodar</td><td>80/1</td><td>+8000</td></tr><tr><td>Celtic</td><td>100/1</td><td>+10000</td></tr><tr><td>Rennes</td><td>100/1</td><td>+10000</td></tr><tr><td>Braga</td><td>100/1</td><td>+10000</td></tr><tr><td>Young Boys</td><td>100/1</td><td>+10000</td></tr><tr><td>Feyenoord</td><td>100/1</td><td>+10000</td></tr><tr><td>Trabzonspor</td><td>150/1</td><td>+15000</td></tr><tr><td>Rangers</td><td>150/1</td><td>+15000</td></tr><tr><td>Az</td><td>150/1</td><td>+15000</td></tr><tr><td>Ludogorets Razgrad</td><td>150/1</td><td>+15000</td></tr><tr><td>Fc Copenhagen</td><td>150/1</td><td>+15000</td></tr><tr><td>Malmo Ff</td><td>200/1</td><td>+20000</td></tr><tr><td>Standard Liege</td><td>200/1</td><td>+20000</td></tr><tr><td>Apoel Nicosia</td><td>200/1</td><td>+20000</td></tr><tr><td>Gent</td><td>200/1</td><td>+20000</td></tr><tr><td>Partizan Belgrade</td><td>200/1</td><td>+20000</td></tr><tr><td>Cfr Cluj</td><td>200/1</td><td>+20000</td></tr><tr><td>Istanbul Basaksehir</td><td>250/1</td><td>+25000</td></tr><tr><td>Wolfsberger Ac</td><td>350/1</td><td>+35000</td></tr><tr><td>Pfc Oleksandria</td><td>500/1</td><td>+50000</td></tr><tr><td>Guimaraes</td><td>500/1</td><td>+50000</td></tr><tr><td>Fk Qarabag</td><td>500/1</td><td>+50000</td></tr><tr><td>Fc Astana</td><td>750/1</td><td>+75000</td></tr><tr><td>Rosenborg</td><td>750/1</td><td>+75000</td></tr><tr><td>Slovan Bratislava</td><td>750/1</td><td>+75000</td></tr><tr><td>Lugano</td><td>750/1</td><td>+75000</td></tr><tr><td>Lask Linz</td><td>750/1</td><td>+75000</td></tr><tr><td>Ferencvarosi Tc</td><td>1000/1</td><td>+100000</td></tr><tr><td>F91 Dudelange</td><td>1000/1</td><td>+100000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	