	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Cfb - Heisman Trophy - Odds To Win">
			<caption>Cfb - Heisman Trophy - Odds To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Cfb - Heisman Trophy - Odds To Win  - Sep 19					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Heisman Trophy - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tua Tagovailoa (Alabama)  </td><td>4/1</td><td>+400</td></tr><tr><td>Trevor Lawerence (Clemson)  </td><td>4/1</td><td>+400</td></tr><tr><td>Jalen Hurts (Oklahoma)  </td><td>15/4</td><td>+375</td></tr><tr><td>Justin Fields (Ohio St)  </td><td>11/1</td><td>+1100</td></tr><tr><td>Adrian Martinez (Nebraska)  </td><td>66/1</td><td>+6600</td></tr><tr><td>Jonathan Taylor (Wisconsin)  </td><td>12/1</td><td>+1200</td></tr><tr><td>Tee Higgins (Clemson)  </td><td>28/1</td><td>+2800</td></tr><tr><td>Jake Fromm (Georgia)  </td><td>11/1</td><td>+1100</td></tr><tr><td>Justin Herbert (Oregon)  </td><td>25/1</td><td>+2500</td></tr><tr><td>Dandre Swift (Georgia)  </td><td>25/1</td><td>+2500</td></tr><tr><td>Jk Dobbins (Ohio St)  </td><td>40/1</td><td>+4000</td></tr><tr><td>Sam Ehlinger (Texas)  </td><td>25/1</td><td>+2500</td></tr><tr><td>Shea Patterson (Michigan)  </td><td>50/1</td><td>+5000</td></tr><tr><td>Travis Etienne (Clemson)  </td><td>20/1</td><td>+2000</td></tr><tr><td>Jt Daniels (Usc)  </td><td>25/1</td><td>+2500</td></tr><tr><td>Aj Dillon (Boston College)  </td><td>66/1</td><td>+6600</td></tr><tr><td>Ian Book (Notre Dame)  </td><td>66/1</td><td>+6600</td></tr><tr><td>Jacob Eason (Washington)  </td><td>66/1</td><td>+6600</td></tr><tr><td>Kellen Mond (Texas A&m)  </td><td>66/1</td><td>+6600</td></tr><tr><td>Khalil Tate (Arizona)  </td><td>80/1</td><td>+8000</td></tr><tr><td>Najee Harris (Alabama)  </td><td>66/1</td><td>+6600</td></tr><tr><td>Tate Martell (Miami)  </td><td>66/1</td><td>+6600</td></tr><tr><td>Kj Costello (Stanford)  </td><td>80/1</td><td>+8000</td></tr><tr><td>Feleipe Franks (Florida)  </td><td>100/1</td><td>+10000</td></tr><tr><td>Deriq King (Houston)  </td><td>80/1</td><td>+8000</td></tr><tr><td>Joe Burrow (Lsu)  </td><td>14/1</td><td>+1400</td></tr><tr><td>Rondale Moore (Purdue)</td><td>80/1</td><td>+8000</td></tr><tr><td>Austin Kendall (Oklahoma)</td><td>28/1</td><td>+2800</td></tr><tr><td>Jerry Jeudy (Alabama)</td><td>50/1</td><td>+5000</td></tr><tr><td>Spencer Rattler (Oklahoma)</td><td>25/1</td><td>+2500</td></tr><tr><td>Chuba Hubbard (Oklahoma State)</td><td>40/1</td><td>+4000</td></tr><tr><td>Ceedee Lamb (Oklahoma)</td><td>25/1</td><td>+2500</td></tr><tr><td>Bryce Perkins (Virginia)</td><td>40/1</td><td>+4000</td></tr><tr><td>Eno Benjamin (Arizona State)</td><td>40/1</td><td>+4000</td></tr><tr><td>Sean Clifford (Penn State)</td><td>50/1</td><td>+5000</td></tr><tr><td>Kelly Bryant (Clemson)</td><td>50/1</td><td>+5000</td></tr><tr><td>Brandon Wimbush (Central Florida)</td><td>125/1</td><td>+12500</td></tr><tr><td>Mason Fine (North Texas)</td><td>66/1</td><td>+6600</td></tr><tr><td>Laviska Shenault Jr (Colorado)</td><td>100/1</td><td>+10000</td></tr><tr><td>A.j. Epenesa (Iowa)</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	