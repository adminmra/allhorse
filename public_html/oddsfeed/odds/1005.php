	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Dubai Cup">
			<caption>Horses - Dubai Cup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Dubai Cup  - Mar 28					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Dubai World Cup - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Thunder Snow</td><td>7/2</td><td>+350</td></tr><tr><td>North America</td><td>15/2</td><td>+750</td></tr><tr><td>Mckinzie</td><td>9/2</td><td>+450</td></tr><tr><td>Maximum Security</td><td>8/1</td><td>+800</td></tr><tr><td>Seeking The Soul</td><td>14/1</td><td>+1400</td></tr><tr><td>Game Winner</td><td>10/1</td><td>+1000</td></tr><tr><td>Yoshida</td><td>18/1</td><td>+1800</td></tr><tr><td>Tacitus</td><td>20/1</td><td>+2000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	