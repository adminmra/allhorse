	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--albania Kategoria Superiore">
			<caption>Pre--albania Kategoria Superiore</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--albania Kategoria Superiore  - May 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ks Kastrioti Kruje</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >268</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kf Tirana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >-107</div>
								 <div class="boxdata" >-&frac12;<br /> ( -108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Partizani Tirana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >362</div>
								 <div class="boxdata" >+&frac34;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ks Teuta Durres</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >-149</div>
								 <div class="boxdata" >-&frac34;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	