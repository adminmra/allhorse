	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--fifa Club World Cup">
			<caption>Pre--fifa Club World Cup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 21, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Pre--fifa Club World Cup  - Dec 22</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Al Ain Fc<br /> - vs - <br /> Real Madrid</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">2<br /> (-103)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+1083</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o3&frac34;-108 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-2<br /> (-128)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-526</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u3&frac34;-121</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Ca River Plate (arg)<br /> - vs - <br /> Kashima Antlers</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.75<br /> (-108)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-131</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o3-108 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.75<br /> (-121)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+323</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u3-121</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	