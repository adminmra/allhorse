	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--cricket Xth Dismissal Method">
			<caption>Live--cricket Xth Dismissal Method</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Live--cricket Xth Dismissal Method  - Jul 04					</th>
			</tr>
            
	<tr><th colspan="3" class="center"></th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Caught</td><td>20/57</td><td>-285</td></tr><tr><td>Bowled</td><td>9/2</td><td>+450</td></tr><tr><td>Lbw</td><td>47/10</td><td>+470</td></tr><tr><td>Run Out</td><td>9/1</td><td>+900</td></tr><tr><td>Stumped</td><td>19/1</td><td>+1900</td></tr><tr><td>Others</td><td>49/1</td><td>+4900</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	