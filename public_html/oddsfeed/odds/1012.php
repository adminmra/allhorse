	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Prix De L'arc De Triomphe">
			<caption>Horses - Prix De L'arc De Triomphe</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Prix De L'arc De Triomphe  - Oct 06					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Prix De L`arc De Triomphe - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Enable</td><td>5/7</td><td>-140</td></tr><tr><td>Magical</td><td>14/1</td><td>+1400</td></tr><tr><td>Cloth Of Stars</td><td>40/1</td><td>+4000</td></tr><tr><td>Lah Ti Dar</td><td>50/1</td><td>+5000</td></tr><tr><td>Stradivarius</td><td>20/1</td><td>+2000</td></tr><tr><td>Kew Gardens</td><td>40/1</td><td>+4000</td></tr><tr><td>Waldgeist</td><td>14/1</td><td>+1400</td></tr><tr><td>Quorto</td><td>40/1</td><td>+4000</td></tr><tr><td>Suave Richard</td><td>33/1</td><td>+3300</td></tr><tr><td>Anthony Van Dyck</td><td>33/1</td><td>+3300</td></tr><tr><td>Persian King</td><td>40/1</td><td>+4000</td></tr><tr><td>Ghaiyyath</td><td>8/1</td><td>+800</td></tr><tr><td>Young Rascal</td><td>66/1</td><td>+6600</td></tr><tr><td>Morgan Le Faye</td><td>50/1</td><td>+5000</td></tr><tr><td>Sottsass</td><td>7/1</td><td>+700</td></tr><tr><td>Japan</td><td>5/1</td><td>+500</td></tr><tr><td>Sir Dragonet</td><td>33/1</td><td>+3300</td></tr><tr><td>Pink Dogwood</td><td>40/1</td><td>+4000</td></tr><tr><td>Anapurna</td><td>20/1</td><td>+2000</td></tr><tr><td>Saturnalia</td><td>25/1</td><td>+2500</td></tr><tr><td>Kiseki</td><td>33/1</td><td>+3300</td></tr><tr><td>Fierement</td><td>40/1</td><td>+4000</td></tr><tr><td>Broome</td><td>33/1</td><td>+3300</td></tr><tr><td>Channel</td><td>33/1</td><td>+3300</td></tr><tr><td>Hermosa</td><td>33/1</td><td>+3300</td></tr><tr><td>Pelligrina</td><td>40/1</td><td>+4000</td></tr><tr><td>Nagano Gold</td><td>40/1</td><td>+4000</td></tr><tr><td>Wonderment</td><td>80/1</td><td>+8000</td></tr><tr><td>Roger Barows</td><td>50/1</td><td>+5000</td></tr><tr><td>Southern France</td><td>50/1</td><td>+5000</td></tr><tr><td>Telecaster</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	