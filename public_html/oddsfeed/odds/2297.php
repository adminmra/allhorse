	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--iran Pro League">
			<caption>Pre--iran Pro League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 16, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--iran Pro League  - May 16</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sepidrood Rasht Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >963</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Esteghlal Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-370</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Padideh Khorasan Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -109)</div>
								 <div class="boxdata" >-104</div>
								 <div class="boxdata" >-&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Machine Sazi Tabriz</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -120)</div>
								 <div class="boxdata" >312</div>
								 <div class="boxdata" >+&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Zob Ahan Isfahan Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -120)</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Naft Masjed Soleyman Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -109)</div>
								 <div class="boxdata" >165</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Persepolis Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -101)</div>
								 <div class="boxdata" >-131</div>
								 <div class="boxdata" >-&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pars Jonoubi Jam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -131)</div>
								 <div class="boxdata" >424</div>
								 <div class="boxdata" >+&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tractor Sazi Tabriz Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >117</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Foolad Khuzestan Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >236</div>
								 <div class="boxdata" >+&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Peykan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" >+&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Nassaji Mazandaran</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >142</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Esteghlal Khuzestan Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -113)</div>
								 <div class="boxdata" >1769</div>
								 <div class="boxdata" >+2&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Foolad Mobarakeh Sepahan Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -117)</div>
								 <div class="boxdata" >-769</div>
								 <div class="boxdata" >-2&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sanat Naft Abadan Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >202</div>
								 <div class="boxdata" >+&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Saipa Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" >-&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	