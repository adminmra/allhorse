	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba - Rookie Of The Year - To Win">
			<caption>Nba - Rookie Of The Year - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nba - Rookie Of The Year - To Win  - Sep 15					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-20 Nba Rookie Of The Year - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Zion Williamson</td><td>2/3</td><td>-150</td></tr><tr><td>Ja Morant</td><td>3/1</td><td>+300</td></tr><tr><td>Rj Barret</td><td>5/1</td><td>+500</td></tr><tr><td>Michael Porter Jr</td><td>17/1</td><td>+1700</td></tr><tr><td>Darius Garland</td><td>17/1</td><td>+1700</td></tr><tr><td>Jarret Culver</td><td>22/1</td><td>+2200</td></tr><tr><td>Coby White</td><td>22/1</td><td>+2200</td></tr><tr><td>De'andre Hunter</td><td>22/1</td><td>+2200</td></tr><tr><td>Rui Hachimura</td><td>28/1</td><td>+2800</td></tr><tr><td>Matt Thomas</td><td>33/1</td><td>+3300</td></tr><tr><td>Cameron Reddish</td><td>45/1</td><td>+4500</td></tr><tr><td>Brandon Clarke</td><td>50/1</td><td>+5000</td></tr><tr><td>Pj Washington</td><td>50/1</td><td>+5000</td></tr><tr><td>Jaxson Hayes</td><td>60/1</td><td>+6000</td></tr><tr><td>Nassir Little</td><td>60/1</td><td>+6000</td></tr><tr><td>Romeo Langford</td><td>60/1</td><td>+6000</td></tr><tr><td>Keldon Johnson</td><td>60/1</td><td>+6000</td></tr><tr><td>Nickeil Alexander-walker</td><td>60/1</td><td>+6000</td></tr><tr><td>Carsen Edwards</td><td>60/1</td><td>+6000</td></tr><tr><td>Tyler Herro</td><td>60/1</td><td>+6000</td></tr><tr><td>Mfiondu Kabengele</td><td>70/1</td><td>+7000</td></tr><tr><td>Sekou Doumbouya</td><td>70/1</td><td>+7000</td></tr><tr><td>Bruno Fernando</td><td>70/1</td><td>+7000</td></tr><tr><td>Goga Bitadze</td><td>80/1</td><td>+8000</td></tr><tr><td>Grant Williams</td><td>80/1</td><td>+8000</td></tr><tr><td>Ignas Brazdeikis</td><td>80/1</td><td>+8000</td></tr><tr><td>Ty Jerome</td><td>100/1</td><td>+10000</td></tr><tr><td>Kyle Guy</td><td>100/1</td><td>+10000</td></tr><tr><td>Bol Bol</td><td>100/1</td><td>+10000</td></tr><tr><td>Kevin Porter</td><td>150/1</td><td>+15000</td></tr><tr><td>Kz Okpala</td><td>150/1</td><td>+15000</td></tr><tr><td>Tacko Fall</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	