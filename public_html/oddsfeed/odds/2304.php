	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--north Macedonia 1 Mfl">
			<caption>Pre--north Macedonia 1 Mfl</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--north Macedonia 1 Mfl  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gjorce Petrov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >219</div>
								 <div class="boxdata" >+&frac12;<br /> ( -158 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Rabotnicki Skopje</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" >-&frac12;<br /> ( +119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Vardar Skopje</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Shkupi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >183</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Akademija Pandev</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Struga Trim Lum</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >261</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Shkendija</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >-166</div>
								 <div class="boxdata" >-1<br /> ( +104)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Renova</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >432</div>
								 <div class="boxdata" >+1<br /> ( -136)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Borec</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >470</div>
								 <div class="boxdata" >+1<br /> ( -138)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Sileks Kratovo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >-169</div>
								 <div class="boxdata" >-1<br /> ( +104)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	