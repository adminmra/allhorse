	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Greece Super League">
			<caption>Greece Super League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 3, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Greece Super League  - Dec 03</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Paok Fc<br /> - vs - <br /> Lamia</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-230</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2-115 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+766</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2-115</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Panaitolikos<br /> - vs - <br /> Olympiakos</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">2<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+1965</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o3-105 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-2<br /> (-115)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-809</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u3-125</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	