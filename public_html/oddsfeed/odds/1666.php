	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--uefa Champions League">
			<caption>Live--uefa Champions League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 17, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--uefa Champions League  - Sep 17</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Valencia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +104 )</div>
								 <div class="boxdata" >-333</div>
								 <div class="boxdata" >-&frac12;<br /> ( -333 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chelsea Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -166 )</div>
								 <div class="boxdata" >1400</div>
								 <div class="boxdata" >+&frac12;<br /> ( +195 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rb Leipzig</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sl Benfica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Liverpool Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( +104 )</div>
								 <div class="boxdata" >290</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ssc Napoli</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -166 )</div>
								 <div class="boxdata" >390</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Barcelona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >325</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Borussia Dortmund</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >285</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Losc Lille</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( +114 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+3&frac12;<br /> ( -454 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ajax Amsterdam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -181 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-3&frac12;<br /> ( +240 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Krc Genk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o8&frac12;<br /> ( -166 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+4&frac12;<br /> ( -222 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Salzburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u8&frac12;<br /> ( +104 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-4&frac12;<br /> ( +135 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	