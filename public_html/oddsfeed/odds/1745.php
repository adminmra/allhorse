	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--wales Premier League">
			<caption>Live--wales Premier League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated December 26, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Live--wales Premier League  - Dec 26</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Cardiff Metropolitan University Fc<br /> - vs - <br /> Barry Town United Fc</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"> </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"></button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Llandudno Fc<br /> - vs - <br /> Caernarfon Town Fc</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1.25<br /> (-133)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+500</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-142 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1.25<br /> (100)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-222</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;+104</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	