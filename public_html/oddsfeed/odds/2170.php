	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--argentina Superliga">
			<caption>Pre--argentina Superliga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--argentina Superliga  - Sep 14</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca River Plate (arg)</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >-147</div>
								 <div class="boxdata" >-&frac12;<br /> ( -149 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Huracan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >411</div>
								 <div class="boxdata" >+&frac12;<br /> ( +112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca San Lorenzo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -104)</div>
								 <div class="boxdata" >139</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Colon De Santa Fe</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -126)</div>
								 <div class="boxdata" >222</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Union De Santa Fe</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -111)</div>
								 <div class="boxdata" >214</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Arsenal De Sarandi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -119)</div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Argentinos Jrs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -103)</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Godoy Cruz A.t.</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -128)</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Defensa Y Justicia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( +102)</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Central Cordoba</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -136)</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--argentina Superliga  - Sep 15</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Estudiantes De La Plata</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >468</div>
								 <div class="boxdata" >+1<br /> ( -142)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Boca Juniors</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >-166</div>
								 <div class="boxdata" >-1<br /> ( +108)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Newell&apos;s Old Boys</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -111)</div>
								 <div class="boxdata" >196</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Rosario Central</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -119)</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Talleres De Cordoba</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -108)</div>
								 <div class="boxdata" >266</div>
								 <div class="boxdata" >+&frac12;<br /> ( -149 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Banfield</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -121)</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac12;<br /> ( +112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atletico Lanus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -128)</div>
								 <div class="boxdata" >301</div>
								 <div class="boxdata" >+&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Independiente</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -103)</div>
								 <div class="boxdata" >-104</div>
								 <div class="boxdata" >-&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Racing Club Avellaneda</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -121)</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac12;<br /> ( +116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gimnasia Y Esgrima</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -108)</div>
								 <div class="boxdata" >247</div>
								 <div class="boxdata" >+&frac12;<br /> ( -156 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--argentina Superliga  - Sep 16</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atletico Tucuman</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >421</div>
								 <div class="boxdata" >+1<br /> ( -151)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Velez Sarsfield</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >-156</div>
								 <div class="boxdata" >-1<br /> ( +114)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Patronato Parana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -121)</div>
								 <div class="boxdata" >221</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Aldosivi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -108)</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	