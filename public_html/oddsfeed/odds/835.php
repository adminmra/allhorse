	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nhl - Divisions - Odds To Win">
			<caption>Nhl - Divisions - Odds To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nhl - Divisions - Odds To Win  - Oct 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Atlantic Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tampa Bay Lightning</td><td>11/10</td><td>+110</td></tr><tr><td>Toronto Maple Leafs</td><td>11/4</td><td>+275</td></tr><tr><td>Boston Bruins</td><td>13/4</td><td>+325</td></tr><tr><td>Florida Panthers</td><td>13/2</td><td>+650</td></tr><tr><td>Montreal Canandiens</td><td>40/1</td><td>+4000</td></tr><tr><td>Buffalo Sabres</td><td>60/1</td><td>+6000</td></tr><tr><td>Detroit Red Wings</td><td>200/1</td><td>+20000</td></tr><tr><td>Ottawa Senators</td><td>500/1</td><td>+50000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Central Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Colorado Avalanche</td><td>5/2</td><td>+250</td></tr><tr><td>Saint Louis Blues</td><td>4/1</td><td>+400</td></tr><tr><td>Nashville Predators</td><td>17/4</td><td>+425</td></tr><tr><td>Dallas Stars</td><td>17/4</td><td>+425</td></tr><tr><td>Winnipeg Jets</td><td>5/1</td><td>+500</td></tr><tr><td>Chicago Blackhawks</td><td>9/1</td><td>+900</td></tr><tr><td>Minnesota Wild</td><td>50/1</td><td>+5000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Metropolitan Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Washington Capitals</td><td>7/2</td><td>+350</td></tr><tr><td>Pittsburgh Penguins</td><td>4/1</td><td>+400</td></tr><tr><td>New York Islanders</td><td>5/1</td><td>+500</td></tr><tr><td>Carolina Hurricanes</td><td>4/1</td><td>+400</td></tr><tr><td>New Jersey Devils</td><td>5/1</td><td>+500</td></tr><tr><td>Philadelphia Flyers</td><td>15/2</td><td>+750</td></tr><tr><td>New York Rangers</td><td>10/1</td><td>+1000</td></tr><tr><td>Columbus Blue Jackets</td><td>22/1</td><td>+2200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019/20 Pacific Division - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Vegas Golden Knights</td><td>10/11</td><td>-110</td></tr><tr><td>San Jose Sharks</td><td>21/5</td><td>+420</td></tr><tr><td>Calgary Flames</td><td>21/5</td><td>+420</td></tr><tr><td>Edmonton Oilers</td><td>11/1</td><td>+1100</td></tr><tr><td>Vancouver Canucks</td><td>12/1</td><td>+1200</td></tr><tr><td>Arizona Coyotes</td><td>12/1</td><td>+1200</td></tr><tr><td>Los Angeles Kings</td><td>66/1</td><td>+6600</td></tr><tr><td>Anaheim Ducks</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');</script>
{/literal}	
	