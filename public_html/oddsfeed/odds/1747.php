	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--algeria Ligue 1">
			<caption>Live--algeria Ligue 1</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 21, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--algeria Ligue 1  - May 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Paradou Ac</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +114 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac14;<br /> ( -153 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Ain Mlila</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -153 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" >-&frac14;<br /> ( +110 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	