	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Sb 53 - Announcers Props">
			<caption>Sb 53 - Announcers Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Sb 53 - Announcers Props  - Feb 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Color Of Tony Romo's Tie (predominant Color)</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Blue</td><td>5/6</td><td>-120</td></tr><tr><td>Grey</td><td>17/10</td><td>+170</td></tr><tr><td>Black</td><td>5/1</td><td>+500</td></tr><tr><td>Pink</td><td>7/1</td><td>+700</td></tr><tr><td>Red</td><td>8/1</td><td>+800</td></tr><tr><td>Purple</td><td>9/1</td><td>+900</td></tr><tr><td>Brown</td><td>12/1</td><td>+1200</td></tr><tr><td>Yellow</td><td>11/1</td><td>+1100</td></tr><tr><td>Any Other Color</td><td>7/1</td><td>+700</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Tony Romo's Tie Be One Solid Color?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>8/5</td><td>+160</td></tr><tr><td>No</td><td>1/2</td><td>-200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Tony Romo Be Clean Shaven?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>EV</td><td>EV</td></tr><tr><td>No</td><td>5/7</td><td>-140</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Color Of Jim Nantz's Tie (predominant Color)</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Red</td><td>5/2</td><td>+250</td></tr><tr><td>Pink</td><td>7/2</td><td>+350</td></tr><tr><td>Purple</td><td>EV</td><td>EV</td></tr><tr><td>Blue</td><td>5/1</td><td>+500</td></tr><tr><td>Grey</td><td>6/1</td><td>+600</td></tr><tr><td>Black</td><td>6/1</td><td>+600</td></tr><tr><td>Brown</td><td>7/1</td><td>+700</td></tr><tr><td>Yellow</td><td>7/1</td><td>+700</td></tr><tr><td>Any Other Color</td><td>6/1</td><td>+600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Jim Nantz's Tie Be One Solid Color?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>2/1</td><td>+200</td></tr><tr><td>No</td><td>1/3</td><td>-300</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Color Of Jay Feely's Tie</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Blue</td><td>5/2</td><td>+250</td></tr><tr><td>Pink</td><td>7/2</td><td>+350</td></tr><tr><td>Purple</td><td>5/1</td><td>+500</td></tr><tr><td>Grey</td><td>5/1</td><td>+500</td></tr><tr><td>Red</td><td>6/1</td><td>+600</td></tr><tr><td>Black</td><td>6/1</td><td>+600</td></tr><tr><td>Brown</td><td>7/1</td><td>+700</td></tr><tr><td>Yellow</td><td>7/1</td><td>+700</td></tr><tr><td>Any Other Color</td><td>6/1</td><td>+600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Jay Feely's Tie Be One Solid Color?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>8/5</td><td>+160</td></tr><tr><td>No</td><td>1/2</td><td>-200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">What Will Tracy Wolfson Be Wearing?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Skirt/dress</td><td>2/1</td><td>+200</td></tr><tr><td>Pants</td><td>1/3</td><td>-300</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o8t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Gene Steratore Get 1st Replay Call Correct?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>1/3</td><td>-300</td></tr><tr><td>No</td><td>2/1</td><td>+200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o9t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Quarter Of Tony Romo's 1st Correct Play Prediciton</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>1st</td><td>2/7</td><td>-350</td></tr><tr><td>2nd</td><td>7/2</td><td>+350</td></tr><tr><td>3rd</td><td>14/1</td><td>+1400</td></tr><tr><td>4th</td><td>13/1</td><td>+1300</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					Sb 53 - Announcers Props  - Feb 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o10t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Romo Or Nantz Mention The Spread?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>5/2</td><td>+250</td></tr><tr><td>No</td><td>1/4</td><td>-400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o11t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Color Tracy Wolfson's Top?-pred. Color - No Jacket</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>White</td><td>5/2</td><td>+250</td></tr><tr><td>Black</td><td>3/1</td><td>+300</td></tr><tr><td>Red</td><td>4/1</td><td>+400</td></tr><tr><td>Gold</td><td>5/1</td><td>+500</td></tr><tr><td>Yellow</td><td>5/1</td><td>+500</td></tr><tr><td>Green</td><td>6/1</td><td>+600</td></tr><tr><td>Blue</td><td>6/1</td><td>+600</td></tr><tr><td>Orange</td><td>10/1</td><td>+1000</td></tr><tr><td>Any Other Color</td><td>6/1</td><td>+600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');addSort('o6t');addSort('o7t');addSort('o8t');addSort('o9t');addSort('o10t');addSort('o11t');</script>
{/literal}	
	