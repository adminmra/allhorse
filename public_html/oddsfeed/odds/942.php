	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Racing - Indycar - To Win">
			<caption>Racing - Indycar - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 21, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Racing - Indycar - To Win  - Jun 21					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Rev Group Grand Prix - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Josef Newgarden</td><td>7/2</td><td>+350</td></tr><tr><td>Alexander Rossi</td><td>4/1</td><td>+400</td></tr><tr><td>Will Power</td><td>9/2</td><td>+450</td></tr><tr><td>Scott Dixon</td><td>9/2</td><td>+450</td></tr><tr><td>Simon Pagenaud</td><td>9/1</td><td>+900</td></tr><tr><td>Ryan Hunter-reay</td><td>12/1</td><td>+1200</td></tr><tr><td>Sebastien Bourdais</td><td>14/1</td><td>+1400</td></tr><tr><td>Felix Rosenqvist</td><td>16/1</td><td>+1600</td></tr><tr><td>Graham Rahal</td><td>16/1</td><td>+1600</td></tr><tr><td>Takuma Sato</td><td>20/1</td><td>+2000</td></tr><tr><td>Colton Herta</td><td>20/1</td><td>+2000</td></tr><tr><td>James Hinchcliffe</td><td>25/1</td><td>+2500</td></tr><tr><td>Marcus Ericsson</td><td>66/1</td><td>+6600</td></tr><tr><td>Zach Veach</td><td>66/1</td><td>+6600</td></tr><tr><td>Spencer Pigot</td><td>66/1</td><td>+6600</td></tr><tr><td>Marco Andretti</td><td>66/1</td><td>+6600</td></tr><tr><td>Ed Jones</td><td>80/1</td><td>+8000</td></tr><tr><td>Santino Ferrucci</td><td>100/1</td><td>+10000</td></tr><tr><td>Matheus Leist</td><td>125/1</td><td>+12500</td></tr><tr><td>Patricio Oward</td><td>125/1</td><td>+12500</td></tr><tr><td>Jack Harvey</td><td>150/1</td><td>+15000</td></tr><tr><td>Max Chilton</td><td>200/1</td><td>+20000</td></tr><tr><td>Tony Kanaan</td><td>200/1</td><td>+20000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	