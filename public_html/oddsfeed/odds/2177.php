	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--german Bundesliga 2">
			<caption>Pre--german Bundesliga 2</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--german Bundesliga 2  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Darmstadt 1898</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -108)</div>
								 <div class="boxdata" >292</div>
								 <div class="boxdata" >+&frac12;<br /> ( -101 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1. Fc Heidenheim 1846</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -121)</div>
								 <div class="boxdata" >-129</div>
								 <div class="boxdata" >-&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hannover 96</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -129)</div>
								 <div class="boxdata" >181</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Holstein Kiel</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -103)</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--german Bundesliga 2  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Greuther Furth</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -129)</div>
								 <div class="boxdata" >420</div>
								 <div class="boxdata" >+1<br /> ( -109)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vfb Stuttgart</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -102)</div>
								 <div class="boxdata" >-188</div>
								 <div class="boxdata" >-1<br /> ( -120)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vfl Bochum</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >246</div>
								 <div class="boxdata" >+&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Sandhausen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >-101</div>
								 <div class="boxdata" >-&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Karlsruher Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >257</div>
								 <div class="boxdata" >+&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1 Fc Nuremberg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >-102</div>
								 <div class="boxdata" >-&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dsc Arminia Bielefeld</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -121)</div>
								 <div class="boxdata" >-112</div>
								 <div class="boxdata" >-&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Wehen Wiesbaden</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -108)</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--german Bundesliga 2  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc St. Pauli</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -109 )</div>
								 <div class="boxdata" >218</div>
								 <div class="boxdata" >+&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vfl 1899 Osnabruck</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -125 )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ssv Jahn Regensburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -117)</div>
								 <div class="boxdata" >177</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sg Dynamo Dresden</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -117)</div>
								 <div class="boxdata" >121</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Erzgebirge Aue</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -129 )</div>
								 <div class="boxdata" >536</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -151 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hamburger Sv</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -106 )</div>
								 <div class="boxdata" >-250</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	