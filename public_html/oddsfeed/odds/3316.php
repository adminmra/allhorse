	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--bk - Brazil (nbb)">
			<caption>Live--bk - Brazil (nbb)</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated April 13, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--bk - Brazil (nbb)  - Apr 13</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Flamengo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o165&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-333</div>
								 <div class="boxdata" >-5&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Corinthians Paulista</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u165&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" >+5&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	