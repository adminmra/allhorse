	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba - Nba Finals Mvp - To Win">
			<caption>Nba - Nba Finals Mvp - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 13, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nba - Nba Finals Mvp - To Win  - Jun 13					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Nba Finals Mvp - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Steph Curry</td><td>11/4</td><td>+275</td></tr><tr><td>Kawhi Leonard</td><td>2/7</td><td>-350</td></tr><tr><td>Draymond Green</td><td>33/1</td><td>+3300</td></tr><tr><td>Kyle Lowry</td><td>66/1</td><td>+6600</td></tr><tr><td>Pascal Siakam</td><td>50/1</td><td>+5000</td></tr><tr><td>Klay Thompson</td><td>28/1</td><td>+2800</td></tr><tr><td>Andre Iguodala</td><td>250/1</td><td>+25000</td></tr><tr><td>Demarcus Cousins</td><td>100/1</td><td>+10000</td></tr><tr><td>Marc Gasol</td><td>200/1</td><td>+20000</td></tr><tr><td>Serge Ibaka</td><td>150/1</td><td>+15000</td></tr><tr><td>Fred Vanvleet</td><td>150/1</td><td>+15000</td></tr><tr><td>Danny Green</td><td>250/1</td><td>+25000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	