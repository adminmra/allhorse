	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nhl - When Will Series Finish">
			<caption>Nhl - When Will Series Finish</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nhl - When Will Series Finish  - Jun 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Blues Vs Bruins - When Will  Series Finish</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Game 5</td><td>11/5</td><td>+220</td></tr><tr><td>Game 6</td><td>17/10</td><td>+170</td></tr><tr><td>Game 7</td><td>3/2</td><td>+150</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	