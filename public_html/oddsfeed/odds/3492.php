	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Ncaa Basketball Cit 1h">
			<caption>Ncaa Basketball Cit 1h</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated April 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Ncaa Basketball Cit 1h First Half Lines - Apr 04</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Wisc Green Bay</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o78&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >155</div>
								 <div class="boxdata" >+3<br /> ( -110)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1h Marshall</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u78&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" >-3<br /> ( -110)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	