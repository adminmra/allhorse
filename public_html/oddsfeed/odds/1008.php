	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Wrestling - Wwe Specials">
			<caption>Wrestling - Wwe Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 20, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Wrestling - Wwe Specials  - Jun 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Next Wwe Superstar To Sign And Appear In Aew</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Dolph Ziggler</td><td>17/10</td><td>+170</td></tr><tr><td>Finn Balor</td><td>5/2</td><td>+250</td></tr><tr><td>Luke Harper</td><td>4/1</td><td>+400</td></tr><tr><td>Shinsuke Nakamura</td><td>9/2</td><td>+450</td></tr><tr><td>Zack Ryder</td><td>13/2</td><td>+650</td></tr><tr><td>The Revival</td><td>8/1</td><td>+800</td></tr><tr><td>Gallows And Anderson</td><td>9/1</td><td>+900</td></tr><tr><td>Rusev</td><td>10/1</td><td>+1000</td></tr><tr><td>Sasha Banks</td><td>10/1</td><td>+1000</td></tr><tr><td>Field</td><td>9/2</td><td>+450</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Cm Punk Appear At Aew "all Out" In Chicago</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes - August 31st, 2019</td><td>2/5</td><td>-250</td></tr><tr><td>No - August 31st, 2019</td><td>19/10</td><td>+190</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Cm Punk Wrestle At Aew "all Out" In Chicago</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>16/1</td><td>+1600</td></tr><tr><td>No</td><td>1/20</td><td>-2000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');</script>
{/literal}	
	