	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--int.club Friendly Games">
			<caption>Pre--int.club Friendly Games</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--int.club Friendly Games  - Jul 08</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Shakhtar Donetsk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >-212</div>
								 <div class="boxdata" >-1<br /> ( -129)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wolfsberger Ac</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >527</div>
								 <div class="boxdata" >+1<br /> ( -102)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Copenhagen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -116)</div>
								 <div class="boxdata" >-147</div>
								 <div class="boxdata" >-&frac12;<br /> ( -147 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Osijek</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -113)</div>
								 <div class="boxdata" >319</div>
								 <div class="boxdata" >+&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Inverness Caledonian Thistle</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -113)</div>
								 <div class="boxdata" >487</div>
								 <div class="boxdata" >+1<br /> ( +104)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Heart Of Midlothian</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -116)</div>
								 <div class="boxdata" >-217</div>
								 <div class="boxdata" >-1<br /> ( -138)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hereford Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >-232</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cinderford Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >444</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fortuna Dusseldorf</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -4999 )</div>
								 <div class="boxdata" >-9999</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sportfreunde Siegen 1899</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +1065 )</div>
								 <div class="boxdata" >4099</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Preston North End</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >-909</div>
								 <div class="boxdata" >-2&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cork City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >1823</div>
								 <div class="boxdata" >+2&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dinamo Kiev</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -109 )</div>
								 <div class="boxdata" >215</div>
								 <div class="boxdata" >+&frac12;<br /> ( -144 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olympiacos Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac12;<br /> ( +108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Accrington Stanley</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -112)</div>
								 <div class="boxdata" >-123</div>
								 <div class="boxdata" >-&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sligo Rovers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -117)</div>
								 <div class="boxdata" >276</div>
								 <div class="boxdata" >+&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rotor Volgograd Ii</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -116)</div>
								 <div class="boxdata" >276</div>
								 <div class="boxdata" >+&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Chernomorets Novorossiysk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -114)</div>
								 <div class="boxdata" >-119</div>
								 <div class="boxdata" >-&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Weymouth Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >-149</div>
								 <div class="boxdata" >-1<br /> ( +104)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wimborne Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >313</div>
								 <div class="boxdata" >+1<br /> ( -138)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Vojvodina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >-434</div>
								 <div class="boxdata" >-2<br /> ( -102)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Krsko</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >853</div>
								 <div class="boxdata" >+2<br /> ( -129)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--int.club Friendly Games  - Jul 09</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Paok Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >-101</div>
								 <div class="boxdata" >-&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pec Zwolle</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -106 )</div>
								 <div class="boxdata" >224</div>
								 <div class="boxdata" >+&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vitesse Arnhem</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -101)</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kks Lech Poznan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -131)</div>
								 <div class="boxdata" >183</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hibernian Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >-113</div>
								 <div class="boxdata" >-&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Carlisle United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -117 )</div>
								 <div class="boxdata" >265</div>
								 <div class="boxdata" >+&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Varzim Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -129)</div>
								 <div class="boxdata" >748</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sporting Braga</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -102)</div>
								 <div class="boxdata" >-357</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Morecambe Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >-178</div>
								 <div class="boxdata" >-1<br /> ( -113)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc United Of Manchester</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >354</div>
								 <div class="boxdata" >+1<br /> ( -116)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cove Rangers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >254</div>
								 <div class="boxdata" >+&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Forfar Athletic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Horn</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" >-1<br /> ( -120)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wiener Sportklub</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >363</div>
								 <div class="boxdata" >+1<br /> ( -109)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Top Oss</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >237</div>
								 <div class="boxdata" >+&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sbv Excelsior</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >-119</div>
								 <div class="boxdata" >-&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chelmsford City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -112)</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >-&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Haringey Borough</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -117)</div>
								 <div class="boxdata" >221</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >St Johnstone Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -112)</div>
								 <div class="boxdata" >-178</div>
								 <div class="boxdata" >-1<br /> ( -109)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Greenock Morton Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -117)</div>
								 <div class="boxdata" >398</div>
								 <div class="boxdata" >+1<br /> ( -120)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Exeter City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >-833</div>
								 <div class="boxdata" >-2&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tiverton Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >1388</div>
								 <div class="boxdata" >+2&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Viktoria 1889 Berlin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4<br /> ( -103)</div>
								 <div class="boxdata" >978</div>
								 <div class="boxdata" >+2<br /> ( +102)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Erzgebirge Aue</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4<br /> ( -129)</div>
								 <div class="boxdata" >-588</div>
								 <div class="boxdata" >-2<br /> ( -135)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Lausanne-sport</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >675</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Basel 1893</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >-344</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Walsall Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -112)</div>
								 <div class="boxdata" >-147</div>
								 <div class="boxdata" >-&frac12;<br /> ( -147 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Leamington Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -119)</div>
								 <div class="boxdata" >304</div>
								 <div class="boxdata" >+&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bradford City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >-666</div>
								 <div class="boxdata" >-2&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brighouse Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >1189</div>
								 <div class="boxdata" >+2&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sm Caen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -114)</div>
								 <div class="boxdata" >436</div>
								 <div class="boxdata" >+1<br /> ( +102)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vfl Wolfsburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -114)</div>
								 <div class="boxdata" >-217</div>
								 <div class="boxdata" >-1<br /> ( -135)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Curzon Ashton Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -116)</div>
								 <div class="boxdata" >-113</div>
								 <div class="boxdata" >-&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stalybridge Celtic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -114)</div>
								 <div class="boxdata" >241</div>
								 <div class="boxdata" >+&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wigan Athletic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-285</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Fylde</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >652</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Colchester United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >-333</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dulwich Hamlet</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >692</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Eastleigh Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -112)</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Slough Town Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -117)</div>
								 <div class="boxdata" >254</div>
								 <div class="boxdata" >+&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Oxford United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -129)</div>
								 <div class="boxdata" >-232</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Oxford City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -102)</div>
								 <div class="boxdata" >484</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -149 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Grimsby Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >-454</div>
								 <div class="boxdata" >-2<br /> ( -107)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Grantham Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >869</div>
								 <div class="boxdata" >+2<br /> ( -123)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Welling United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -113)</div>
								 <div class="boxdata" >233</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bromley Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -116)</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >-&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stevenage Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >-232</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wingate & Finchley Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >484</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Toulouse Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >-142</div>
								 <div class="boxdata" >-&frac12;<br /> ( -144 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Beziers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >361</div>
								 <div class="boxdata" >+&frac12;<br /> ( +108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bristol City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -126 )</div>
								 <div class="boxdata" >-114</div>
								 <div class="boxdata" >-&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Wimbledon</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >256</div>
								 <div class="boxdata" >+&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chorley Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >-212</div>
								 <div class="boxdata" >-1<br /> ( -142)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atherton Collieries</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >455</div>
								 <div class="boxdata" >+1<br /> ( +108)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Roda Jc Kerkrade</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -117)</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Alemannia Aachen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -112)</div>
								 <div class="boxdata" >210</div>
								 <div class="boxdata" >+&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Falkirk Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brechin City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >170</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wealdstone Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >-142</div>
								 <div class="boxdata" >-&frac12;<br /> ( -144 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hayes & Yeading United F.c.</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >317</div>
								 <div class="boxdata" >+&frac12;<br /> ( +108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1 Fc Kaiserslautern</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >-178</div>
								 <div class="boxdata" >-1<br /> ( -109)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc 08 Homburg-saar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >340</div>
								 <div class="boxdata" >+1<br /> ( -120)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Maidstone United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Folkestone Invicta</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vfl Bochum</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -116)</div>
								 <div class="boxdata" >147</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc St. Gallen 1879</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -113)</div>
								 <div class="boxdata" >147</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Southend United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >-163</div>
								 <div class="boxdata" >-1<br /> ( -104)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Billericay Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >355</div>
								 <div class="boxdata" >+1<br /> ( -128)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cowdenbeath Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >786</div>
								 <div class="boxdata" >+2<br /> ( -140)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Raith Rovers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >-416</div>
								 <div class="boxdata" >-2<br /> ( +106)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Plymouth Argyle</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-555</div>
								 <div class="boxdata" >-2<br /> ( -126)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Truro City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >1019</div>
								 <div class="boxdata" >+2<br /> ( -105)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Leyton Orient</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >353</div>
								 <div class="boxdata" >+1<br /> ( -144)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hull City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" >-1<br /> ( +108)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--int.club Friendly Games  - Jul 12</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Trencin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aek Athens Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	