	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--philippines Football League">
			<caption>Pre--philippines Football League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 17, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--philippines Football League  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ceres-negros Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac34;<br /> ( -117 )</div>
								 <div class="boxdata" >-285</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kaya Fc-iloilo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac34;<br /> ( -113 )</div>
								 <div class="boxdata" >458</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stallion Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >-400</div>
								 <div class="boxdata" >-2<br /> ( +108)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mendiola Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac34;<br /> ( -109 )</div>
								 <div class="boxdata" >627</div>
								 <div class="boxdata" >+2<br /> ( -142)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	