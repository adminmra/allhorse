	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Cbb - Ncaa Tournament Most Outstanding Player">
			<caption>Cbb - Ncaa Tournament Most Outstanding Player</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated April 6, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Cbb - Ncaa Tournament Most Outstanding Player  - Apr 06					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Ncaa Bk Tournament Most Outstanding Player</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Cassius Winston</td><td>5/2</td><td>+250</td></tr><tr><td>Kyle Guy</td><td>5/2</td><td>+250</td></tr><tr><td>Xavier Tillman</td><td>6/1</td><td>+600</td></tr><tr><td>Jarrett Culver</td><td>7/1</td><td>+700</td></tr><tr><td>Jared Harper</td><td>10/1</td><td>+1000</td></tr><tr><td>De'andre Hunter</td><td>12/1</td><td>+1200</td></tr><tr><td>Bryce Brown</td><td>12/1</td><td>+1200</td></tr><tr><td>Mamadi Diakite</td><td>12/1</td><td>+1200</td></tr><tr><td>Ty Jerome</td><td>20/1</td><td>+2000</td></tr><tr><td>David Moretti</td><td>20/1</td><td>+2000</td></tr><tr><td>Matt Mooney</td><td>20/1</td><td>+2000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	