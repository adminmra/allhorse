	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Challenges">
			<caption>Challenges</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 21, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Challenges  - Mar 21					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">How Many Ncaa 3's Will Pat Make In 5 Minutes</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 22.5</td><td>5/11</td><td>-220</td></tr><tr><td>Under 22.5</td><td>9/5</td><td>+180</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">How Many Ncaa 3's Will Pat Make In 5 Minutes</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 23.5</td><td>5/9</td><td>-180</td></tr><tr><td>Under 23.5</td><td>7/5</td><td>+140</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">How Many Ncaa 3's Will Pat Make In 5 Minutes</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 24.5</td><td>5/7</td><td>-140</td></tr><tr><td>Under 24.5</td><td>EV</td><td>EV</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">How Many Ncaa 3's Will Pat Make In 5 Minutes</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 25.5</td><td>20/27</td><td>-135</td></tr><tr><td>Under 25.5</td><td>20/21</td><td>-105</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Exact Number Of Ncaa 3's Pat Makes In 5 Minutes</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>1-10 Ncaa 3pt</td><td>16/1</td><td>+1600</td></tr><tr><td>11-16 Ncaa 3pt</td><td>12/1</td><td>+1200</td></tr><tr><td>17-21 Ncaa 3pt</td><td>5/1</td><td>+500</td></tr><tr><td>22-24 Ncaa 3pt</td><td>3/2</td><td>+150</td></tr><tr><td>28-29 Ncaa 3pt</td><td>2/1</td><td>+200</td></tr><tr><td>30+ Ncaa 3pt</td><td>3/2</td><td>+150</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Pat Hit 3 Straight Ncaa 3pt Shots</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes - Without A Miss</td><td>5/11</td><td>-220</td></tr><tr><td>No - Without A Miss</td><td>9/5</td><td>+180</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Pat Hit 4 Straight Ncaa 3pt Shots</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes - Without A Miss</td><td>5/6</td><td>-120</td></tr><tr><td>No - Without A Miss</td><td>5/6</td><td>-120</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Pat Hit 5 Straight Ncaa 3pt Shots</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes - Without A Miss</td><td>2/1</td><td>+200</td></tr><tr><td>No - Without A Miss</td><td>1/3</td><td>-300</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o8t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Pat Hit 6 Straight Ncaa 3pt Shots</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes - Without A Miss</td><td>7/2</td><td>+350</td></tr><tr><td>No - Without A Miss</td><td>1/5</td><td>-500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');addSort('o6t');addSort('o7t');addSort('o8t');</script>
{/literal}	
	