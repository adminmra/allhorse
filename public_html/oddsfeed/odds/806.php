	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mlb - Most Regular Season Home Runs">
			<caption>Mlb - Most Regular Season Home Runs</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 28, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Mlb - Most Regular Season Home Runs  - Apr 10					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Most Regular Season Home Runs</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Giancarlo Stanton</td><td>8/1</td><td>+800</td></tr><tr><td>Aaron Judge</td><td>8/1</td><td>+800</td></tr><tr><td>Khris Davis</td><td>12/1</td><td>+1200</td></tr><tr><td>Jd Martinez</td><td>12/1</td><td>+1200</td></tr><tr><td>Mike Trout</td><td>12/1</td><td>+1200</td></tr><tr><td>Bryce Harper</td><td>12/1</td><td>+1200</td></tr><tr><td>Matt Olson</td><td>20/1</td><td>+2000</td></tr><tr><td>Nolan Arenado</td><td>20/1</td><td>+2000</td></tr><tr><td>Rhys Hoskins</td><td>20/1</td><td>+2000</td></tr><tr><td>Joey Gallo</td><td>25/1</td><td>+2500</td></tr><tr><td>Nelson Cruz</td><td>25/1</td><td>+2500</td></tr><tr><td>Francisco Lindor</td><td>30/1</td><td>+3000</td></tr><tr><td>Manny Machado</td><td>30/1</td><td>+3000</td></tr><tr><td>Trevor Story</td><td>30/1</td><td>+3000</td></tr><tr><td>Jesus Aguilar</td><td>33/1</td><td>+3300</td></tr><tr><td>Alex Bregman</td><td>33/1</td><td>+3300</td></tr><tr><td>Christian Yelich</td><td>33/1</td><td>+3300</td></tr><tr><td>Anthony Rizzo</td><td>33/1</td><td>+3300</td></tr><tr><td>Jose Ramirez</td><td>33/1</td><td>+3300</td></tr><tr><td>Mookie Betts</td><td>40/1</td><td>+4000</td></tr><tr><td>Jose Abreu</td><td>40/1</td><td>+4000</td></tr><tr><td>Edwin Encarnacion</td><td>50/1</td><td>+5000</td></tr><tr><td>Javier Baez</td><td>50/1</td><td>+5000</td></tr><tr><td>George Springer</td><td>50/1</td><td>+5000</td></tr><tr><td>Gary Sanchez</td><td>50/1</td><td>+5000</td></tr><tr><td>Juan Soto</td><td>50/1</td><td>+5000</td></tr><tr><td>Kris Bryant</td><td>50/1</td><td>+5000</td></tr><tr><td>Paul Goldschmidt</td><td>50/1</td><td>+5000</td></tr><tr><td>Ronald Acuna</td><td>66/1</td><td>+6600</td></tr><tr><td>Vladimir Guerrero Jr</td><td>80/1</td><td>+8000</td></tr><tr><td>Cody Bellinger</td><td>80/1</td><td>+8000</td></tr><tr><td>Justin Upton</td><td>80/1</td><td>+8000</td></tr><tr><td>Mike Moustakas</td><td>80/1</td><td>+8000</td></tr><tr><td>Josh Donaldson</td><td>80/1</td><td>+8000</td></tr><tr><td>Yasiel Puig</td><td>80/1</td><td>+8000</td></tr><tr><td>Daniel Murphy</td><td>100/1</td><td>+10000</td></tr><tr><td>Eugenio Suarez</td><td>40/1</td><td>+4000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	