	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Ice Hockey World Championship">
			<caption>Ice Hockey World Championship</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 23, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Ice Hockey World Championship  - May 23</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Switzerland</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >255</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Canada</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >-315</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Usa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >295</div>
								 <div class="boxdata" >+1&frac12;<br /> ( +115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Russia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" >-370</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sweden</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5<br /> ( -110)</div>
								 <div class="boxdata" >-280</div>
								 <div class="boxdata" >-1&frac12;<br /> ( +105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Finland</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5<br /> ( -110)</div>
								 <div class="boxdata" >230</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Germany</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >385</div>
								 <div class="boxdata" >+2&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Czech Republic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >-520</div>
								 <div class="boxdata" >-2&frac12;<br /> ( +115 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	