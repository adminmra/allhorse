	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nfl - Nfc Conference - To Win">
			<caption>Nfl - Nfc Conference - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nfl - Nfc Conference - To Win  - Sep 22					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Nfc Conference - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>New Orleans Saints</td><td>10/1</td><td>+1000</td></tr><tr><td>Philadelphia Eagles</td><td>8/1</td><td>+800</td></tr><tr><td>Los Angeles Rams</td><td>4/1</td><td>+400</td></tr><tr><td>Chicago Bears</td><td>8/1</td><td>+800</td></tr><tr><td>Dallas Cowboys</td><td>6/1</td><td>+600</td></tr><tr><td>Green Bay Packers</td><td>6/1</td><td>+600</td></tr><tr><td>Minnesota Vikings</td><td>10/1</td><td>+1000</td></tr><tr><td>Seattle Seahawks</td><td>10/1</td><td>+1000</td></tr><tr><td>Carolina Panthers</td><td>40/1</td><td>+4000</td></tr><tr><td>Atlanta Falcons</td><td>16/1</td><td>+1600</td></tr><tr><td>San Francisco 49ers</td><td>10/1</td><td>+1000</td></tr><tr><td>Tampa Bay Buccaneers</td><td>33/1</td><td>+3300</td></tr><tr><td>Detroit Lions</td><td>33/1</td><td>+3300</td></tr><tr><td>New York Giants</td><td>80/1</td><td>+8000</td></tr><tr><td>Washington Redskins</td><td>150/1</td><td>+15000</td></tr><tr><td>Arizona Cardinals</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	