	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nba - Qtrs">
			<caption>Nba - Qtrs</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 13, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Nba - Qtrs First Quarter Lines - Jun 13</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1q Toronto Raptors</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o52&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >105</div>
								 <div class="boxdata" >+1<br /> ( -115)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1q Golden State Warriors</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u52&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" >-1<br /> ( -115)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Nba - Qtrs Second Quarter Lines - Jun 13</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2q Toronto Raptors</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o52<br /> ( -110)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >2q Golden State Warriors</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u52<br /> ( -120)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Nba - Qtrs Third Quarter Lines - Jun 13</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >3q Toronto Raptors</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o53&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >3q Golden State Warriors</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u53&frac12;<br /> ( -115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-&frac12;<br /> ( -115 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Nba - Qtrs Fourth Quarter Lines - Jun 13</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >4q Toronto Raptors</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o53<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >4q Golden State Warriors</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u53<br /> ( -115)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	