	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Breeders Cup Sprint - To Win">
			<caption>Horses - Breeders Cup Sprint - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Breeders Cup Sprint - To Win  - Nov 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Breeders Cup Sprint - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Roy H</td><td>7/2</td><td>+350</td></tr><tr><td>Mitole</td><td>6/1</td><td>+600</td></tr><tr><td>X Y Jet</td><td>8/1</td><td>+800</td></tr><tr><td>Mind Control</td><td>16/1</td><td>+1600</td></tr><tr><td>Much Better</td><td>25/1</td><td>+2500</td></tr><tr><td>Promises Fulfilled</td><td>16/1</td><td>+1600</td></tr><tr><td>Matera Sky</td><td>20/1</td><td>+2000</td></tr><tr><td>Whitmore</td><td>33/1</td><td>+3300</td></tr><tr><td>Copano Kicking</td><td>33/1</td><td>+3300</td></tr><tr><td>Conquest Tsunami</td><td>40/1</td><td>+4000</td></tr><tr><td>Imperial Hint</td><td>4/1</td><td>+400</td></tr><tr><td>Shancelot</td><td>7/2</td><td>+350</td></tr><tr><td>World Of Trouble</td><td>8/1</td><td>+800</td></tr><tr><td>Cistron</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	