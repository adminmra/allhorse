	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Nfl - Next Week Lines">
			<caption>Nfl - Next Week Lines</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center">
                        <em id='updateemp'>Updated November 27, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			
	        <tr><th>Nfl - Next Week Lines  - Nov 29</th></tr>
         <tr><td><table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"><tbody><tr class="header"><td>TEAM</td><td>SPREAD</td><td>MONEYLINE</td><td>Total</td></tr>
         <tr><td>New Orleans Saints<br /> vs<br /> Dallas Cowboys</td><td>-8<br /> (100) <br /><hr /><br/> 8<br /> (-120) 								</td><td>-380 <br /><hr /><br/> +303</td><td>o53-110 <br /><hr /><br/> u53-110</td></tr>                  </tbody>
               </table>
               </td>
               </tr>
        
                <tr><th>Nfl - Next Week Lines  - Dec 02</th></tr>
         <tr><td><table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"><tbody><tr class="header"><td>TEAM</td><td>SPREAD</td><td>MONEYLINE</td><td>Total</td></tr>
         <tr><td>Indianapolis Colts<br /> vs<br /> Jacksonville Jaguars</td><td>-4<br /> (-110) <br /><hr /><br/> 4<br /> (-110) 								</td><td>-220 <br /><hr /><br/> +184</td><td>o47&frac12;-110 <br /><hr /><br/> u47&frac12;-110</td></tr><tr><td>Los Angeles Chargers<br /> vs<br /> Pittsburgh Steelers</td><td>3.5<br /> (-115) <br /><hr /><br/> -3.5<br /> (-105) 								</td><td>+147 <br /><hr /><br/> -169</td><td>o51&frac12;-110 <br /><hr /><br/> u51&frac12;-110</td></tr><tr><td>Carolina Panthers<br /> vs<br /> Tampa Bay Buccaneers</td><td>-3.5<br /> (-110) <br /><hr /><br/> 3.5<br /> (-110) 								</td><td>-180 <br /><hr /><br/> +155</td><td>o56-110 <br /><hr /><br/> u56-110</td></tr><tr><td>Baltimore Ravens<br /> vs<br /> Atlanta Falcons</td><td>-1.5<br /> (-105) <br /><hr /><br/> 1.5<br /> (-115) 								</td><td>-120 <br /><hr /><br/> EV</td><td>o49-110 <br /><hr /><br/> u49-110</td></tr><tr><td>Buffalo Bills<br /> vs<br /> Miami Dolphins</td><td>5<br /> (-110) <br /><hr /><br/> -5<br /> (-110) 								</td><td>+176 <br /><hr /><br/> -210</td><td>o40-110 <br /><hr /><br/> u40-110</td></tr><tr><td>Denver Broncos<br /> vs<br /> Cincinnati Bengals</td><td>-4<br /> (-110) <br /><hr /><br/> 4<br /> (-110) 								</td><td>-207 <br /><hr /><br/> +173</td><td>o42&frac12;-110 <br /><hr /><br/> u42&frac12;-110</td></tr><tr><td>Los Angeles Rams<br /> vs<br /> Detroit Lions</td><td>-9.5<br /> (-110) <br /><hr /><br/> 9.5<br /> (-110) 								</td><td>-475 <br /><hr /><br/> +367</td><td>o54&frac12;-110 <br /><hr /><br/> u54&frac12;-110</td></tr><tr><td>Arizona Cardinals<br /> vs<br /> Green Bay Packers</td><td>14.5<br /> (-120) <br /><hr /><br/> -14.5<br /> (100) 								</td><td>+717 <br /><hr /><br/> -1112</td><td>o44&frac12;-110 <br /><hr /><br/> u44&frac12;-110</td></tr><tr><td>Kansas City Chiefs<br /> vs<br /> Oakland Raiders</td><td>-15<br /> (-110) <br /><hr /><br/> 15<br /> (-110) 								</td><td>-1500 <br /><hr /><br/> +876</td><td>o55&frac12;-110 <br /><hr /><br/> u55&frac12;-110</td></tr><tr><td>Minnesota Vikings<br /> vs<br /> New England Patriots</td><td>6<br /> (-110) <br /><hr /><br/> -6<br /> (-110) 								</td><td>+211 <br /><hr /><br/> -255</td><td>o48&frac12;-110 <br /><hr /><br/> u48&frac12;-110</td></tr><tr><td>San Francisco 49ers<br /> vs<br /> Seattle Seahawks</td><td>10<br /> (-115) <br /><hr /><br/> -10<br /> (-105) 								</td><td>+367 <br /><hr /><br/> -475</td><td>o46-110 <br /><hr /><br/> u46-110</td></tr>                  </tbody>
               </table>
               </td>
               </tr>
        
                <tr><th>Nfl - Next Week Lines  - Dec 03</th></tr>
         <tr><td><table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"><tbody><tr class="header"><td>TEAM</td><td>SPREAD</td><td>MONEYLINE</td><td>Total</td></tr>
         <tr><td>Washington Redskins<br /> vs<br /> Philadelphia Eagles</td><td>6.5<br /> (-110) <br /><hr /><br/> -6.5<br /> (-110) 								</td><td> <br /><hr /><br/> </td><td>o44-110 <br /><hr /><br/> u44-110</td></tr>                  </tbody>
               </table>
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	