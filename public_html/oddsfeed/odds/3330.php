	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--bk - Serbia (a1 League)">
			<caption>Live--bk - Serbia (a1 League)</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 9, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--bk - Serbia (a1 League)  - Mar 09</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kk Beovuk 72</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o163&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+23&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kk Metalac</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u163&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-23&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kk Sloboda Uzice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o179&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-142</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kk Vrsac</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u179&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	