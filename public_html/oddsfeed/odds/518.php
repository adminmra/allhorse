	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Mexico - Primera Division">
			<caption>Mexico - Primera Division</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated November 30, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Mexico - Primera Division  - Dec 01</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Queretaro<br /> - vs - <br /> Cruz Azul</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">1<br /> (-180)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+391</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+130 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-1<br /> (160)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-145</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-150</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Monterrey<br /> - vs - <br /> Santos Laguna</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+191</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+110 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-140)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+135</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-130</button></div></div></div></div>                  
               </td>
               </tr>
        
                <tr><th colspan="4">Mexico - Primera Division  - Dec 02</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Uanl Tigres<br /> - vs - <br /> Unam Pumas</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+121</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;+105 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (-140)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+215</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-125</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Toluca<br /> - vs - <br /> Club America</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0.5<br /> (110)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+320</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-115 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-0.5<br /> (-130)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">-128</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-105</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	