	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Germany Bundesliga I - To Win">
			<caption>Soccer - Germany Bundesliga I - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Germany Bundesliga I - To Win  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019-2020 Bundesliga - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Bayern Munich</td><td>1/3</td><td>-300</td></tr><tr><td>Borussia Dormund</td><td>3/1</td><td>+300</td></tr><tr><td>Rb Leipzig</td><td>12/1</td><td>+1200</td></tr><tr><td>Bayer Leverkusen</td><td>66/1</td><td>+6600</td></tr><tr><td>Tsg Hoffenheim</td><td>200/1</td><td>+20000</td></tr><tr><td>Borussia Monchengladbach</td><td>500/1</td><td>+50000</td></tr><tr><td>Wolfsburg</td><td>150/1</td><td>+15000</td></tr><tr><td>Eintracht Frankfurt</td><td>150/1</td><td>+15000</td></tr><tr><td>Werder Bremen</td><td>200/1</td><td>+20000</td></tr><tr><td>Schalke</td><td>100/1</td><td>+10000</td></tr><tr><td>Hertha Berlin</td><td>1000/1</td><td>+100000</td></tr><tr><td>Fortuna Dusseldorf</td><td>1000/1</td><td>+100000</td></tr><tr><td>Mainz</td><td>1000/1</td><td>+100000</td></tr><tr><td>Augsburg</td><td>1500/1</td><td>+150000</td></tr><tr><td>Sc Freiburg</td><td>500/1</td><td>+50000</td></tr><tr><td>Cologne</td><td>1500/1</td><td>+150000</td></tr><tr><td>Paderborn</td><td>2500/1</td><td>+250000</td></tr><tr><td>Union Berlin</td><td>1000/1</td><td>+100000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	