	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Racing - Nascar - Monster Energy Race Series">
			<caption>Racing - Nascar - Monster Energy Race Series</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Racing - Nascar - Monster Energy Race Series  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Federated Auto Parts 400 - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Kyle Busch</td><td>13/4</td><td>+325</td></tr><tr><td>Kevin Harvick</td><td>6/1</td><td>+600</td></tr><tr><td>Martin Truex Jr</td><td>6/1</td><td>+600</td></tr><tr><td>Denny Hamlin</td><td>6/1</td><td>+600</td></tr><tr><td>Joey Logano</td><td>7/1</td><td>+700</td></tr><tr><td>Brad Keselowski</td><td>8/1</td><td>+800</td></tr><tr><td>Chase Elliott</td><td>16/1</td><td>+1600</td></tr><tr><td>Erik Jones</td><td>16/1</td><td>+1600</td></tr><tr><td>Kyle Larson</td><td>22/1</td><td>+2200</td></tr><tr><td>Clint Bowyer</td><td>22/1</td><td>+2200</td></tr><tr><td>Kurt Busch</td><td>22/1</td><td>+2200</td></tr><tr><td>Ryan Blaney</td><td>40/1</td><td>+4000</td></tr><tr><td>Aric Almirola</td><td>40/1</td><td>+4000</td></tr><tr><td>Alex Bowman</td><td>40/1</td><td>+4000</td></tr><tr><td>William Byron</td><td>40/1</td><td>+4000</td></tr><tr><td>Jimmie Johnson</td><td>66/1</td><td>+6600</td></tr><tr><td>Daniel Suarez</td><td>80/1</td><td>+8000</td></tr><tr><td>Ryan Newman</td><td>80/1</td><td>+8000</td></tr><tr><td>Austin Dillon</td><td>80/1</td><td>+8000</td></tr><tr><td>Matt Dibenedetto</td><td>100/1</td><td>+10000</td></tr><tr><td>Ricky Stenhouse Jr</td><td>150/1</td><td>+15000</td></tr><tr><td>Chris Buescher</td><td>200/1</td><td>+20000</td></tr><tr><td>Paul Menard</td><td>200/1</td><td>+20000</td></tr><tr><td>Daniel Hemric</td><td>250/1</td><td>+25000</td></tr><tr><td>Ryan Preece</td><td>250/1</td><td>+25000</td></tr><tr><td>Ty Dillon</td><td>250/1</td><td>+25000</td></tr><tr><td>Darrell Wallace Jr</td><td>250/1</td><td>+25000</td></tr><tr><td>Michael Mcdowell</td><td>500/1</td><td>+50000</td></tr><tr><td>David Ragan</td><td>500/1</td><td>+50000</td></tr><tr><td>Matt Tifft</td><td>1000/1</td><td>+100000</td></tr><tr><td>Corey Lajoie</td><td>1000/1</td><td>+100000</td></tr><tr><td>Landon Cassill</td><td>1000/1</td><td>+100000</td></tr><tr><td>Reed Sorenson</td><td>1000/1</td><td>+100000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	