	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--norway Nm Cup">
			<caption>Pre--norway Nm Cup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 26, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--norway Nm Cup  - Jun 26</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Baerum Sk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -106)</div>
								 <div class="boxdata" >256</div>
								 <div class="boxdata" >+&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fram Larvik</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -125)</div>
								 <div class="boxdata" >-113</div>
								 <div class="boxdata" >-&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mjondalen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >136</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kongsvinger Il</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >171</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tromsdalen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >611</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kfum Oslo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >-285</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Strommen If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >664</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Haugesund Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >-263</div>
								 <div class="boxdata" >-1&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aalesunds Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >573</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rosenborg Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >-238</div>
								 <div class="boxdata" >-1&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Brann</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -101 )</div>
								 <div class="boxdata" >-113</div>
								 <div class="boxdata" >-&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ranheim Il</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >292</div>
								 <div class="boxdata" >+&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stabaek Fotball</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -101)</div>
								 <div class="boxdata" >397</div>
								 <div class="boxdata" >+1<br /> ( -126)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Viking Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -131)</div>
								 <div class="boxdata" >-172</div>
								 <div class="boxdata" >-1<br /> ( -104)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Odds Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kristiansund Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >183</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	