	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Breeders Cup Turf - To Win">
			<caption>Horses - Breeders Cup Turf - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Breeders Cup Turf - To Win  - Nov 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Breeders Cup Turf - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Enable</td><td>5/2</td><td>+250</td></tr><tr><td>Old Persian</td><td>25/1</td><td>+2500</td></tr><tr><td>Magical</td><td>3/1</td><td>+300</td></tr><tr><td>Bricks And Mortar</td><td>9/2</td><td>+450</td></tr><tr><td>Cross Counter</td><td>20/1</td><td>+2000</td></tr><tr><td>Channel Maker</td><td>50/1</td><td>+5000</td></tr><tr><td>Robert Bruce</td><td>33/1</td><td>+3300</td></tr><tr><td>Regal Reality</td><td>40/1</td><td>+4000</td></tr><tr><td>Lys Gracieux</td><td>16/1</td><td>+1600</td></tr><tr><td>Japan</td><td>9/2</td><td>+450</td></tr><tr><td>Sottsass</td><td>9/1</td><td>+900</td></tr><tr><td>Bandua</td><td>22/1</td><td>+2200</td></tr><tr><td>Sadler`s Joy</td><td>40/1</td><td>+4000</td></tr><tr><td>Waldgeist</td><td>11/1</td><td>+1100</td></tr><tr><td>Acclimate</td><td>25/1</td><td>+2500</td></tr><tr><td>Zulu Alpha</td><td>25/1</td><td>+2500</td></tr><tr><td>Annals Of Time</td><td>10/1</td><td>+1000</td></tr><tr><td>Channel Cat</td><td>33/1</td><td>+3300</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	