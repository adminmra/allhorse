	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Wimbledon">
			<caption>Wimbledon</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Wimbledon  - Jul 11					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Wimbledon Women - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Simona Halep</td><td>6/1</td><td>+600</td></tr><tr><td>Ashleigh Barty</td><td>9/1</td><td>+900</td></tr><tr><td>Serena Williams</td><td>6/1</td><td>+600</td></tr><tr><td>Karolina Pliskova</td><td>10/1</td><td>+1000</td></tr><tr><td>Angelique Kerber</td><td>22/1</td><td>+2200</td></tr><tr><td>Petra Kvitova</td><td>14/1</td><td>+1400</td></tr><tr><td>Naomi Osaka</td><td>14/1</td><td>+1400</td></tr><tr><td>Johanna Konta</td><td>16/1</td><td>+1600</td></tr><tr><td>Elina Svitolina</td><td>25/1</td><td>+2500</td></tr><tr><td>Garbine Muguruza</td><td>22/1</td><td>+2200</td></tr><tr><td>Madison Keys</td><td>22/1</td><td>+2200</td></tr><tr><td>Amanda Anisimova</td><td>22/1</td><td>+2200</td></tr><tr><td>Bianca Andreescu</td><td>10/1</td><td>+1000</td></tr><tr><td>Sloane Stephens</td><td>22/1</td><td>+2200</td></tr><tr><td>Kiki Bertens</td><td>25/1</td><td>+2500</td></tr><tr><td>Belinda Bencic</td><td>28/1</td><td>+2800</td></tr><tr><td>Cori Gauff</td><td>33/1</td><td>+3300</td></tr><tr><td>Marketa Vondrousova</td><td>33/1</td><td>+3300</td></tr><tr><td>Petra Martic</td><td>40/1</td><td>+4000</td></tr><tr><td>Donna Vekic</td><td>40/1</td><td>+4000</td></tr><tr><td>Caroline Wozniacki</td><td>50/1</td><td>+5000</td></tr><tr><td>Julia Goerges</td><td>50/1</td><td>+5000</td></tr><tr><td>Aryna Sabalenka</td><td>66/1</td><td>+6600</td></tr><tr><td>Caroline Garcia</td><td>66/1</td><td>+6600</td></tr><tr><td>Anett Kontaveit</td><td>80/1</td><td>+8000</td></tr><tr><td>Maria Sharapova</td><td>80/1</td><td>+8000</td></tr><tr><td>Barbora Strycova</td><td>80/1</td><td>+8000</td></tr><tr><td>Jelena Ostapenko</td><td>80/1</td><td>+8000</td></tr><tr><td>Maria Sakkari</td><td>80/1</td><td>+8000</td></tr><tr><td>Shuai Zhang</td><td>80/1</td><td>+8000</td></tr><tr><td>Kristina Mladenovic</td><td>100/1</td><td>+10000</td></tr><tr><td>Danielle Collins</td><td>100/1</td><td>+10000</td></tr><tr><td>Anastasija Sevastova</td><td>100/1</td><td>+10000</td></tr><tr><td>Elise Mertens</td><td>100/1</td><td>+10000</td></tr><tr><td>Daria Kasatkina</td><td>100/1</td><td>+10000</td></tr><tr><td>Su-wei Hsieh</td><td>100/1</td><td>+10000</td></tr><tr><td>Camila Giorgi</td><td>100/1</td><td>+10000</td></tr><tr><td>Coco Vandeweghe</td><td>100/1</td><td>+10000</td></tr><tr><td>Alize Cornet</td><td>150/1</td><td>+15000</td></tr><tr><td>Sofia Kenin</td><td>150/1</td><td>+15000</td></tr><tr><td>Venus Williams</td><td>150/1</td><td>+15000</td></tr><tr><td>Elena Vesnina</td><td>200/1</td><td>+20000</td></tr><tr><td>Svetlana Kuznetsova</td><td>200/1</td><td>+20000</td></tr><tr><td>Carla Suarez Navarro</td><td>250/1</td><td>+25000</td></tr><tr><td>Quiang Wang</td><td>250/1</td><td>+25000</td></tr><tr><td>Heather Watson</td><td>250/1</td><td>+25000</td></tr><tr><td>Daria Gavrilova</td><td>250/1</td><td>+25000</td></tr></tbody></table></td></tr>		<tr>
					<th colspan="3" class="center oddsheader">
					Wimbledon  - Jul 12					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Wimbledon Men - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Novak Djokovic</td><td>3/2</td><td>+150</td></tr><tr><td>Roger Federer</td><td>4/1</td><td>+400</td></tr><tr><td>Rafael Nadal</td><td>5/1</td><td>+500</td></tr><tr><td>Andy Murray</td><td>12/1</td><td>+1200</td></tr><tr><td>Stefanos Tsitsipas</td><td>14/1</td><td>+1400</td></tr><tr><td>Milos Raonic</td><td>25/1</td><td>+2500</td></tr><tr><td>Alexander Zverev</td><td>25/1</td><td>+2500</td></tr><tr><td>Marin Cilic</td><td>33/1</td><td>+3300</td></tr><tr><td>Dominic Thiem</td><td>33/1</td><td>+3300</td></tr><tr><td>Felix Auger Aliassime</td><td>33/1</td><td>+3300</td></tr><tr><td>Kevin Anderson</td><td>33/1</td><td>+3300</td></tr><tr><td>Nick Kyrgios</td><td>33/1</td><td>+3300</td></tr><tr><td>Kei Nishikori</td><td>40/1</td><td>+4000</td></tr><tr><td>Juan Martin Del Potro</td><td>40/1</td><td>+4000</td></tr><tr><td>Stan Wawrinka</td><td>66/1</td><td>+6600</td></tr><tr><td>David Goffin</td><td>66/1</td><td>+6600</td></tr><tr><td>Borna Coric</td><td>66/1</td><td>+6600</td></tr><tr><td>Karen Khachanov</td><td>66/1</td><td>+6600</td></tr><tr><td>Daniil Medvedev</td><td>22/1</td><td>+2200</td></tr><tr><td>John Isner</td><td>80/1</td><td>+8000</td></tr><tr><td>Roberto Bautista-agut</td><td>80/1</td><td>+8000</td></tr><tr><td>Grigor Dimitrov</td><td>80/1</td><td>+8000</td></tr><tr><td>Sam Querrey</td><td>100/1</td><td>+10000</td></tr><tr><td>Gael Monfils</td><td>100/1</td><td>+10000</td></tr><tr><td>Kyle Edmund</td><td>100/1</td><td>+10000</td></tr><tr><td>Matteo Berrettini</td><td>100/1</td><td>+10000</td></tr><tr><td>Jo-wilfred Tsonga</td><td>125/1</td><td>+12500</td></tr><tr><td>Guido Pella</td><td>150/1</td><td>+15000</td></tr><tr><td>Frances Tiafoe</td><td>150/1</td><td>+15000</td></tr><tr><td>Daniel Evans</td><td>200/1</td><td>+20000</td></tr><tr><td>Fabio Fognini</td><td>200/1</td><td>+20000</td></tr><tr><td>Cameron Norrie</td><td>250/1</td><td>+25000</td></tr><tr><td>Alex De Minaur</td><td>80/1</td><td>+8000</td></tr><tr><td>Denis Shapovalov</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	