	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--soccer 2nd Halves">
			<caption>Pre--soccer 2nd Halves</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ad San Carlos</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Universidad De Costa Rica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ofk Titograd</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >308</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ofk Grbalj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kolbotn</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac14;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stabaek Fotball</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lillestrom Sk Kvinner</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >-163</div>
								 <div class="boxdata" >-&frac34;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Trondheims-orn</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >494</div>
								 <div class="boxdata" >+&frac34;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Avaldsnes Il</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >116</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lyn</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >256</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Madrid</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >211</div>
								 <div class="boxdata" >+&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Paris Saint-germain</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Galatasaray</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >305</div>
								 <div class="boxdata" >+&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Club Brugge</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Red Star Belgrade</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -126)</div>
								 <div class="boxdata" >1771</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Bayern Munich</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -108)</div>
								 <div class="boxdata" >-555</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tottenham Hotspur</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olympiacos Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >277</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Manchester City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -113 )</div>
								 <div class="boxdata" >-166</div>
								 <div class="boxdata" >-&frac34;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Shakhtar Donetsk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >480</div>
								 <div class="boxdata" >+&frac34;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Juventus Turin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -133)</div>
								 <div class="boxdata" >254</div>
								 <div class="boxdata" >+&frac14;<br /> ( -161 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atletico Madrid</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -103)</div>
								 <div class="boxdata" >166</div>
								 <div class="boxdata" >-&frac14;<br /> ( +114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atalanta Bergamasca Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >137</div>
								 <div class="boxdata" >-&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gnk Dinamo Zagreb</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >242</div>
								 <div class="boxdata" >+&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Lokomotiv Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >583</div>
								 <div class="boxdata" >+&frac34;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bayer 04 Leverkusen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >-181</div>
								 <div class="boxdata" >-&frac34;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Legia Warsaw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >-101</div>
								 <div class="boxdata" >-&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wisla Plock Sa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >357</div>
								 <div class="boxdata" >+&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Supersport United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -138)</div>
								 <div class="boxdata" >268</div>
								 <div class="boxdata" >+&frac14;<br /> ( -149 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mamelodi Sundowns</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" >-&frac14;<br /> ( +108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dallas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >266</div>
								 <div class="boxdata" >+&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Seattle Sounders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -126 )</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atlanta United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >-114</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Cincinnati</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >303</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ny Red Bulls</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -109 )</div>
								 <div class="boxdata" >240</div>
								 <div class="boxdata" >+&frac14;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Portland Timbers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -125 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Senica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >437</div>
								 <div class="boxdata" >+&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Spartak Trnava</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-133</div>
								 <div class="boxdata" >-&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sd Ponferradina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >227</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Lugo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >168</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Albacete Balompie</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >436</div>
								 <div class="boxdata" >+&frac12;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sd Huesca</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >-112</div>
								 <div class="boxdata" >-&frac12;<br /> ( -108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Numancia Cd</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +104 )</div>
								 <div class="boxdata" >424</div>
								 <div class="boxdata" >+&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rc Deportivo La Coruna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -142 )</div>
								 <div class="boxdata" >-101</div>
								 <div class="boxdata" >-&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Mlada Boleslav</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Teplice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gks Tychy 71</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >290</div>
								 <div class="boxdata" >+&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fks Stal Mielec</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Termalica Bruk-bet Nieciecza</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olimpia Grudziadz</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >213</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aspn Miedz Legnica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac14;<br /> ( -147 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Podbeskidzie Bielsko Biala</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >146</div>
								 <div class="boxdata" >-&frac14;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Oks Odra Opole</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >229</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ks Warta Poznan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >164</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Banfield</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -125)</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Talleres De Cordoba</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -111)</div>
								 <div class="boxdata" >214</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Csd Independiente Del Valle</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -116)</div>
								 <div class="boxdata" >505</div>
								 <div class="boxdata" >+&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Corinthians Sp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -119)</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac12;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Paranaense Pr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >430</div>
								 <div class="boxdata" >+&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Internacional Rs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >-&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Juventus U23</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >256</div>
								 <div class="boxdata" >+&frac14;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Arezzo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" >-&frac14;<br /> ( -108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca River Plate (arg)</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >-256</div>
								 <div class="boxdata" >-1<br /> ( -123)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Godoy Cruz A.t.</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >1003</div>
								 <div class="boxdata" >+1<br /> ( -111)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jerv Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >222</div>
								 <div class="boxdata" >+&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ullensaker/kisa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >121</div>
								 <div class="boxdata" >-&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kongsvinger Il Fotball</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac14;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Notodden Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sogndal Il</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" >-&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Skeid Fotball</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >242</div>
								 <div class="boxdata" >+&frac14;<br /> ( -126 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stroemmen If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >361</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sandefjord Fotball</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" >-&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nest-sotra</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -101 )</div>
								 <div class="boxdata" >363</div>
								 <div class="boxdata" >+&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ik Start</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >-108</div>
								 <div class="boxdata" >-&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kfum Oslo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >316</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aalesunds Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >-114</div>
								 <div class="boxdata" >-&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Utah Royals Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >288</div>
								 <div class="boxdata" >+&frac14;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Reign Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" >-&frac14;<br /> ( -108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gyirmot Fc Gyor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >188</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Soroksar Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >193</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Budafoki Mte</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >185</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tiszakecske Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >181</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nyiregyhaza Spartacus Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >321</div>
								 <div class="boxdata" >+&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Eto Fc Gyor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vac Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >320</div>
								 <div class="boxdata" >+&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kazincbarcikai Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bfc Siofok</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dorogi Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >216</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Szombathelyi Haladas Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >166</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bekescsaba 1912 Elore</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Szolnoki Mav Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >422</div>
								 <div class="boxdata" >+&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aqvital Fc Csakvar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >-133</div>
								 <div class="boxdata" >-&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Szeged 2011</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >243</div>
								 <div class="boxdata" >+&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Budaorsi Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >139</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mtk Budapest Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -103 )</div>
								 <div class="boxdata" >192</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vasas Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -133 )</div>
								 <div class="boxdata" >144</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gjorce Petrov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >236</div>
								 <div class="boxdata" >+&frac14;<br /> ( -151 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Rabotnicki Skopje</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" >-&frac14;<br /> ( +108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Vardar Skopje</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >181</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Shkupi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >209</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Akademija Pandev</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >134</div>
								 <div class="boxdata" >-&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Struga Trim Lum</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >271</div>
								 <div class="boxdata" >+&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Shkendija</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >-109</div>
								 <div class="boxdata" >-&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Renova</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >404</div>
								 <div class="boxdata" >+&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Borec</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >443</div>
								 <div class="boxdata" >+&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Sileks Kratovo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >-109</div>
								 <div class="boxdata" >-&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ibv Vestmannaeyjar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -102)</div>
								 <div class="boxdata" >547</div>
								 <div class="boxdata" >+1<br /> ( -133)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fh Hafnarfjordur</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -135)</div>
								 <div class="boxdata" >-204</div>
								 <div class="boxdata" >-1<br /> ( -104)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vikingur Reykjavik</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >158</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fylkir Reykjavik</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >186</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Paide Linnameeskond</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >-434</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Maardu Linnameeskond</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >1160</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tartu Jk Tammeka</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >281</div>
								 <div class="boxdata" >+&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jk Narva Trans</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Levadia Tallinn</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >202</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Flora Tallinn</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ki Klaksvik</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >168</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nsi Runavik</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kfc Komarno</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >-588</div>
								 <div class="boxdata" >-1&frac34;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Novohrad Lucenec</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >1689</div>
								 <div class="boxdata" >+1&frac34;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ns Mura</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >-102</div>
								 <div class="boxdata" >-&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Triglav Kranj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >293</div>
								 <div class="boxdata" >+&frac12;<br /> ( -138 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Domzale</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-222</div>
								 <div class="boxdata" >-1<br /> ( -113)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Brda</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -136)</div>
								 <div class="boxdata" >603</div>
								 <div class="boxdata" >+1<br /> ( -121)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Maribor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >-149</div>
								 <div class="boxdata" >-&frac34;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Koper</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >484</div>
								 <div class="boxdata" >+&frac34;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Tabor Sezana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >351</div>
								 <div class="boxdata" >+&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Aluminij Kidricevo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Herediano</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" >-&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ad Municipal Grecia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >224</div>
								 <div class="boxdata" >+&frac14;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ararat Yerevan Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >326</div>
								 <div class="boxdata" >+&frac12;<br /> ( -138 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Ararat-armenia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >-102</div>
								 <div class="boxdata" >-&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tigres Uanl</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >159</div>
								 <div class="boxdata" >-&frac14;<br /> ( +112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cruz Azul</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >239</div>
								 <div class="boxdata" >+&frac14;<br /> ( -156 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Mladost Ljeskopolje</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >466</div>
								 <div class="boxdata" >+&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Zeta Golubovci</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >-123</div>
								 <div class="boxdata" >-&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Iskra Danilovgrad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >238</div>
								 <div class="boxdata" >+&frac14;<br /> ( -151 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ofk Petrovac</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" >-&frac14;<br /> ( +110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Rudar Pljevlja</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >361</div>
								 <div class="boxdata" >+&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Kom</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Radnik Bijeljina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >-125</div>
								 <div class="boxdata" >-&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Gosk Gabela</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >498</div>
								 <div class="boxdata" >+&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Siroki Brijeg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >-120</div>
								 <div class="boxdata" >-&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Buducnost Banovici</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >451</div>
								 <div class="boxdata" >+&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Zeljeznicar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -125 )</div>
								 <div class="boxdata" >-434</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -101 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Tekstilac Derventa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >1692</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Mladost Doboj Kakanj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Jedinstvo Bihac</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >280</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Progreso</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -101 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Juventud De Lp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" >-&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Defensor Sporting</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >216</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cerro Largo Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca River Plate (uru)</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >174</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Racing Club Mdeo.</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >174</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Penarol Uru</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >-107</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rampla Juniors Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >359</div>
								 <div class="boxdata" >+&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Independiente Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >403</div>
								 <div class="boxdata" >+&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Aguila</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >-107</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Once Municipal</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >259</div>
								 <div class="boxdata" >+&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jocoro Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >133</div>
								 <div class="boxdata" >-&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Municipal Limeno</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd El Vencedor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >195</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Tuzla City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -108 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Krupa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >270</div>
								 <div class="boxdata" >+&frac14;<br /> ( -126 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Slavija Sarajevo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >531</div>
								 <div class="boxdata" >+&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Rudar Prijedor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >-136</div>
								 <div class="boxdata" >-&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Zvijezda 09</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >209</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Alfa Modrica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >167</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Charleston Battery</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" >-&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atlanta United 2</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >243</div>
								 <div class="boxdata" >+&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tampa Bay Rowdies</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ottawa Fury Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Loudoun United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bethlehem Steel Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >146</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New York Red Bulls Ii</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Saint Louis Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rio Grande Valley Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >422</div>
								 <div class="boxdata" >+&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fresno Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" >-&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Borac Banja Luka</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >-256</div>
								 <div class="boxdata" >-1<br /> ( -117)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hnk Orasje</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >1425</div>
								 <div class="boxdata" >+1<br /> ( -117)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Sloboda Tuzla</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" >-&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Kozara Gradiska</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >260</div>
								 <div class="boxdata" >+&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Monarcas Morelia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >210</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Club Tijuana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >159</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Sutjeska</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +108 )</div>
								 <div class="boxdata" >229</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Buducnost</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -149 )</div>
								 <div class="boxdata" >173</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Palanga</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" >+&frac14;<br /> ( -147 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Banga Gargzdai</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >144</div>
								 <div class="boxdata" >-&frac14;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Melilla Ud</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >219</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Celta De Vigo B</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >164</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Adr Jicaral</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >274</div>
								 <div class="boxdata" >+&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Cartagines</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" >-&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Santos De Guapiles</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >245</div>
								 <div class="boxdata" >+&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Guadalupe Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >139</div>
								 <div class="boxdata" >-&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Ventspils</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >376</div>
								 <div class="boxdata" >+&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Riga Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >-112</div>
								 <div class="boxdata" >-&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hsk Zrinjski Mostar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >-212</div>
								 <div class="boxdata" >-1<br /> ( +102)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Zvijezda Gradacac</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >810</div>
								 <div class="boxdata" >+1<br /> ( -140)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Celik Zenica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >146</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Rudar Kakanj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >229</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Velez Mostar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >133</div>
								 <div class="boxdata" >-&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Bratstvo Gracanica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >246</div>
								 <div class="boxdata" >+&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Glasgow Rangers U21</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -112)</div>
								 <div class="boxdata" >304</div>
								 <div class="boxdata" >+&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ballymena United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -121)</div>
								 <div class="boxdata" >-125</div>
								 <div class="boxdata" >-&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ks Gornik Polkowice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >215</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lech Ii Poznan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Nove Mesto Nad Vahom</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -116 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Trencianske Stankovce</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >265</div>
								 <div class="boxdata" >+&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Sep 19</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bali United Pusam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >186</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Persija Jakarta</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >177</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sporting Cp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >285</div>
								 <div class="boxdata" >+&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Psv Eindhoven</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Astana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >756</div>
								 <div class="boxdata" >+1<br /> ( -128)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Manchester United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-222</div>
								 <div class="boxdata" >-1<br /> ( -107)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ferencvarosi Tc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >490</div>
								 <div class="boxdata" >+&frac34;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rcd Espanyol Barcelona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >-144</div>
								 <div class="boxdata" >-&frac34;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cska Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -144)</div>
								 <div class="boxdata" >208</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ludogorets 1945 Razgrad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +104)</div>
								 <div class="boxdata" >195</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Az Alkmaar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >187</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Partizan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Feyenoord Rotterdam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >224</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rangers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bsc Young Boys</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >463</div>
								 <div class="boxdata" >+&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Porto</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >-133</div>
								 <div class="boxdata" >-&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Celtic Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Stade Rennes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >F91 Dudelange</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >773</div>
								 <div class="boxdata" >+&frac34;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Apoel Nicosia Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >-181</div>
								 <div class="boxdata" >-&frac34;<br /> ( -126 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Sevilla</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >-119</div>
								 <div class="boxdata" >-&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Qarabag Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >426</div>
								 <div class="boxdata" >+&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Malmo Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >321</div>
								 <div class="boxdata" >+&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dynamo Kiev</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rosenborg Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >293</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lask</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Lazio Roma</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac14;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Cfr 1907 Cluj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >353</div>
								 <div class="boxdata" >+&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Basaksehir Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >535</div>
								 <div class="boxdata" >+&frac34;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Roma</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >-161</div>
								 <div class="boxdata" >-&frac34;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wolfsberger Ac</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >446</div>
								 <div class="boxdata" >+&frac34;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Borussia Monchengladbach</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -114 )</div>
								 <div class="boxdata" >-161</div>
								 <div class="boxdata" >-&frac34;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Saint-etienne</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >227</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kaa Gent</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >144</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Oleksandriya</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >780</div>
								 <div class="boxdata" >+1<br /> ( -116)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vfl Wolfsburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >-238</div>
								 <div class="boxdata" >-1<br /> ( -117)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Lugano</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >413</div>
								 <div class="boxdata" >+&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Copenhagen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >-120</div>
								 <div class="boxdata" >-&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Besiktas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" >-&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Slovan Bratislava</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >227</div>
								 <div class="boxdata" >+&frac14;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vitoria Guimaraes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >318</div>
								 <div class="boxdata" >+&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Standard Liege</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Krasnodar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >229</div>
								 <div class="boxdata" >+&frac14;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Basel 1893</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" >-&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Trabzonspor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >434</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Getafe</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sporting Braga</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >436</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wolverhampton Wanderers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >-114</div>
								 <div class="boxdata" >-&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Arsenal Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >148</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Eintracht Frankfurt</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sporting Gijon</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +104 )</div>
								 <div class="boxdata" >219</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ud Las Palmas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -142 )</div>
								 <div class="boxdata" >177</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tenerife Cd</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >266</div>
								 <div class="boxdata" >+&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Elche Cf</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" >-&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Extremadura Ud</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >359</div>
								 <div class="boxdata" >+&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Oviedo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Roskilde</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -103 )</div>
								 <div class="boxdata" >403</div>
								 <div class="boxdata" >+&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vejle Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -133 )</div>
								 <div class="boxdata" >-136</div>
								 <div class="boxdata" >-&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vendsyssel Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >272</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Viborg Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >118</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Rudar Velenje</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -109 )</div>
								 <div class="boxdata" >509</div>
								 <div class="boxdata" >+&frac34;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Olimpija Ljubljana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -125 )</div>
								 <div class="boxdata" >-169</div>
								 <div class="boxdata" >-&frac34;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atletico Mineiro Mg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -113)</div>
								 <div class="boxdata" >237</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Colon De Santa Fe</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -121)</div>
								 <div class="boxdata" >193</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Fenix</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >229</div>
								 <div class="boxdata" >+&frac14;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Liverpool Montevideo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" >-&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tira-persikabo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >296</div>
								 <div class="boxdata" >+&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Psm Makassar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-103</div>
								 <div class="boxdata" >-&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Persipura Jayapura</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +102 )</div>
								 <div class="boxdata" >268</div>
								 <div class="boxdata" >+&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pss Sleman</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" >-&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Persepar Kalteng Putra</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >359</div>
								 <div class="boxdata" >+&frac12;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Perseru Badak Lampung Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >-112</div>
								 <div class="boxdata" >-&frac12;<br /> ( -108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hapoel Ramat Gan Givatayim Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +108 )</div>
								 <div class="boxdata" >181</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Kfar Qasem</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -149 )</div>
								 <div class="boxdata" >215</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bnei Sakhnin Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hapoel Acre Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >215</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Maccabi Achi Nazareth Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >291</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hapoel Nir Ramat Hasharon Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hapoel Rishon Lezion Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hapoel Nof Hagalil Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >301</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Beitar Tel Aviv Bat Yam Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >334</div>
								 <div class="boxdata" >+&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Maccabi Petah Tikva Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ajman</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -125 )</div>
								 <div class="boxdata" >202</div>
								 <div class="boxdata" >+&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Baniyas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" >-&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Al Dhafra Scc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -109)</div>
								 <div class="boxdata" >294</div>
								 <div class="boxdata" >+&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Al Jazira (uae)</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -125)</div>
								 <div class="boxdata" >-119</div>
								 <div class="boxdata" >-&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Shabab Alahli</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -112)</div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" >-&frac34;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hatta Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -121)</div>
								 <div class="boxdata" >375</div>
								 <div class="boxdata" >+&frac34;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Al Khaleej Khor Fakkan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -108)</div>
								 <div class="boxdata" >423</div>
								 <div class="boxdata" >+&frac34;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sharjah</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -128)</div>
								 <div class="boxdata" >-166</div>
								 <div class="boxdata" >-&frac34;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Club Santos Laguna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >281</div>
								 <div class="boxdata" >+&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tiburones Rojos De Veracruz</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" >-&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Puebla Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +102 )</div>
								 <div class="boxdata" >-121</div>
								 <div class="boxdata" >-&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Club Necaxa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" >393</div>
								 <div class="boxdata" >+&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Deportivo Toluca Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >249</div>
								 <div class="boxdata" >+&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atlas Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >136</div>
								 <div class="boxdata" >-&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ha Noi Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >172</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Song Lam Nghe An</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >177</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Elva</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >181</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Parnu Jk Vaprus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Kapaz</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" >+&frac14;<br /> ( -147 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Turan Tovuz</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >145</div>
								 <div class="boxdata" >-&frac14;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pfk Neftci Baku-2</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >-192</div>
								 <div class="boxdata" >-&frac34;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Agsu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -114 )</div>
								 <div class="boxdata" >550</div>
								 <div class="boxdata" >+&frac34;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Cerro</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >188</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Boston River</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >173</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Plaza Colonia Cd</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >269</div>
								 <div class="boxdata" >+&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Danubio Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" >-&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Club Nacional De Football</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >-114</div>
								 <div class="boxdata" >-&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mdeo. Wanderers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >370</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Moik Baku</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" >-&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Sumgayit-2</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tps Turku</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >323</div>
								 <div class="boxdata" >+&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Haka Valkeakoski</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >-107</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kohtla-jarve Jk Jarve</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -128)</div>
								 <div class="boxdata" >416</div>
								 <div class="boxdata" >+&frac34;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tartu Jk Tammeka U21</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -107)</div>
								 <div class="boxdata" >-175</div>
								 <div class="boxdata" >-&frac34;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jk Tallinna Kalev U21</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -116)</div>
								 <div class="boxdata" >349</div>
								 <div class="boxdata" >+&frac34;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fci Levadia Tallinn U21</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -117)</div>
								 <div class="boxdata" >-142</div>
								 <div class="boxdata" >-&frac34;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tallinna Fc Flora U21</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -126)</div>
								 <div class="boxdata" >351</div>
								 <div class="boxdata" >+&frac34;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tallinna Jk Legion</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -108)</div>
								 <div class="boxdata" >-142</div>
								 <div class="boxdata" >-&frac34;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Spartak Moscow Reserve</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" >-&frac34;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ufa Youth</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >516</div>
								 <div class="boxdata" >+&frac34;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Sep 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Bournemouth</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >240</div>
								 <div class="boxdata" >+&frac14;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Southampton Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" >-&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1. Fsv Mainz 05</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >337</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Schalke 04</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-106</div>
								 <div class="boxdata" >-&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Spartak Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -147)</div>
								 <div class="boxdata" >163</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Ufa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +106)</div>
								 <div class="boxdata" >248</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Betis Balompie</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >221</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Osasuna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >161</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Djurgardens If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >-113</div>
								 <div class="boxdata" >-&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gif Sundsvall</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >395</div>
								 <div class="boxdata" >+&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Genoa Cfc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cagliari Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >158</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Nantes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +108 )</div>
								 <div class="boxdata" >239</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Racing Strasbourg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -149 )</div>
								 <div class="boxdata" >167</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Universitatea Craiova 1948 Cs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Gaz Metan Medias</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >184</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Darmstadt 1898</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >279</div>
								 <div class="boxdata" >+&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1. Fc Heidenheim 1846</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hannover 96</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >192</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Holstein Kiel</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >142</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yellow-red Kv Mechelen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kv Kortrijk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >159</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bohemians Dublin Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >277</div>
								 <div class="boxdata" >+&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Derry City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" >-&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dundalk Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >-140</div>
								 <div class="boxdata" >-&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Waterford Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >466</div>
								 <div class="boxdata" >+&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Finn Harps Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >355</div>
								 <div class="boxdata" >+&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cork City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Saint Patrick&apos;s Athletic Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >504</div>
								 <div class="boxdata" >+&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Shamrock Rovers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >-125</div>
								 <div class="boxdata" >-&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Agf Aarhus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >234</div>
								 <div class="boxdata" >+&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Odense Boldklub</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >134</div>
								 <div class="boxdata" >-&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Heracles Almelo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >199</div>
								 <div class="boxdata" >+&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Twente Enschede</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -131 )</div>
								 <div class="boxdata" >138</div>
								 <div class="boxdata" >-&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ks Rakow Czestochowa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >322</div>
								 <div class="boxdata" >+&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gks Piast Gliwice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jagiellonia Bialystok</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >236</div>
								 <div class="boxdata" >+&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kks Lech Poznan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >136</div>
								 <div class="boxdata" >-&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Venezia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >416</div>
								 <div class="boxdata" >+&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Frosinone Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >-&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Das Aves</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >296</div>
								 <div class="boxdata" >+&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Pacos Ferreira</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Meppen 1912</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >241</div>
								 <div class="boxdata" >+&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Wurzburger Kickers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >123</div>
								 <div class="boxdata" >-&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Osijek</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >158</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Lokomotiva Zagreb</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >210</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jiangsu Suning Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" >-&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chongqing Lifan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >235</div>
								 <div class="boxdata" >+&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Fastav Zlin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >248</div>
								 <div class="boxdata" >+&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bohemians Prague 1905</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >137</div>
								 <div class="boxdata" >-&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Banik Sokolov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >202</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mfk Vitkovice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Zbrojovka Brno</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Vysocina Jihlava</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >167</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Guadalajara Chivas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >209</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Monarcas Morelia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Deportivo Toluca Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >233</div>
								 <div class="boxdata" >+&frac14;<br /> ( -147 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atlas Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" >-&frac14;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Yenisey Krasnoyarsk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >330</div>
								 <div class="boxdata" >+&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Ska-khabarovsk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >116</div>
								 <div class="boxdata" >-&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd La Equidad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >458</div>
								 <div class="boxdata" >+&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Tolima</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >-108</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gais</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ik Frej</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >148</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ik Brage</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >254</div>
								 <div class="boxdata" >+&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Orgryte Is</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >137</div>
								 <div class="boxdata" >-&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tsarsko Selo Sofia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >680</div>
								 <div class="boxdata" >+&frac34;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cska Sofia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" >-&frac34;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Botev Plovdiv</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >278</div>
								 <div class="boxdata" >+&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cherno More Varna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" >-&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atiker Konyaspor 1922</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >234</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Goztepe Sk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >160</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hb Koege</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >173</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hvidovre If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aek Larnaca Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >-120</div>
								 <div class="boxdata" >-&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olympiakos Nicosia Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >405</div>
								 <div class="boxdata" >+&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pirata Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -114 )</div>
								 <div class="boxdata" >1029</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Escuela Municipal Deportivo Binacional</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >-303</div>
								 <div class="boxdata" >-1&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cc Deportivo Municipal</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Universidad Cesar Vallejo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >123</div>
								 <div class="boxdata" >-&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sbv Excelsior</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nac Breda</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" >-&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mvv Maastricht</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >186</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Den Bosch</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nec Nijmegen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -129 )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dordrecht</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -105 )</div>
								 <div class="boxdata" >214</div>
								 <div class="boxdata" >+&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Eindhoven</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >341</div>
								 <div class="boxdata" >+&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >De Graafschap</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jong Az Alkmaar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >305</div>
								 <div class="boxdata" >+&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Almere City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -133 )</div>
								 <div class="boxdata" >-108</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Volendam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >171</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Telstar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jong Fc Utrecht</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >325</div>
								 <div class="boxdata" >+&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Roda Jc Kerkrade</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >-113</div>
								 <div class="boxdata" >-&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jong Ajax Amsterdam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -120)</div>
								 <div class="boxdata" >163</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Go Ahead Eagles</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -114)</div>
								 <div class="boxdata" >145</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bw Linz</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Austria Lustenau</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >117</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Slavoj Trebisov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >730</div>
								 <div class="boxdata" >+1<br /> ( -144)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Poprad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >-208</div>
								 <div class="boxdata" >-1<br /> ( +104)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mfk Ruzomberok B</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -101 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Kosice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >137</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Partizan Bardejov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >297</div>
								 <div class="boxdata" >+&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Zeleziarne Podbrezova</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Petrzalka 1898</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >382</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mfk Skalica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Arema Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >247</div>
								 <div class="boxdata" >+&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Persela Lamongan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac14;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sku Amstetten</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kapfenberger Sv 1919</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >199</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >B 93 Copenhagen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -125 )</div>
								 <div class="boxdata" >646</div>
								 <div class="boxdata" >+1<br /> ( -123)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Helsingoer</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -109 )</div>
								 <div class="boxdata" >-222</div>
								 <div class="boxdata" >-1<br /> ( -111)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chamois Niort Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Valenciennes Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Clermont Foot</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -108)</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Paris Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -126)</div>
								 <div class="boxdata" >234</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ea Guingamp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -119)</div>
								 <div class="boxdata" >202</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sochaux</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -116)</div>
								 <div class="boxdata" >219</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Ajaccio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >194</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Le Mans Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >198</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Chambly Oise</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -131)</div>
								 <div class="boxdata" >245</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Nancy</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -105)</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Grenoble Foot</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +104 )</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lb Chateauroux</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -144 )</div>
								 <div class="boxdata" >192</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Havre</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >167</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Orleans 45</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >233</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Troyes Ac</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -142)</div>
								 <div class="boxdata" >264</div>
								 <div class="boxdata" >+&frac14;<br /> ( -151 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aj Auxerre</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +102)</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" >-&frac14;<br /> ( +108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Horn</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >241</div>
								 <div class="boxdata" >+&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Lafnitz</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >123</div>
								 <div class="boxdata" >-&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Austria Klagenfurt</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >161</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Grazer Ak</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >174</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Juniors Oo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >401</div>
								 <div class="boxdata" >+&frac34;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Ried</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -114 )</div>
								 <div class="boxdata" >-147</div>
								 <div class="boxdata" >-&frac34;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sheffield United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >383</div>
								 <div class="boxdata" >+&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Everton Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >-104</div>
								 <div class="boxdata" >-&frac12;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tottenham Hotspur</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Leicester City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Norwich City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Burnley Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >118</div>
								 <div class="boxdata" >-&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Watford Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -123)</div>
								 <div class="boxdata" >946</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Manchester City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -111)</div>
								 <div class="boxdata" >-344</div>
								 <div class="boxdata" >-1&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brighton & Hove Albion</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >217</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Newcastle United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >177</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1. Fc Cologne</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -133)</div>
								 <div class="boxdata" >986</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Bayern Munich</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -104)</div>
								 <div class="boxdata" >-370</div>
								 <div class="boxdata" >-1&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Augsburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >257</div>
								 <div class="boxdata" >+&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Freiburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >118</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1. Fc Union Berlin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -113 )</div>
								 <div class="boxdata" >521</div>
								 <div class="boxdata" >+&frac34;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bayer 04 Leverkusen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >-178</div>
								 <div class="boxdata" >-&frac34;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rb Leipzig</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -114 )</div>
								 <div class="boxdata" >-108</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Werder Bremen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >292</div>
								 <div class="boxdata" >+&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Paderborn 07</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >290</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hertha Bsc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >-&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Rubin Kazan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >677</div>
								 <div class="boxdata" >+&frac34;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Zenit St Petersburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >-166</div>
								 <div class="boxdata" >-&frac34;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Rostov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Tambov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >348</div>
								 <div class="boxdata" >+&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Krylia Sovetov Samara</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >268</div>
								 <div class="boxdata" >+&frac14;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Republican Fc Akhmat Grozny</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >145</div>
								 <div class="boxdata" >-&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sd Eibar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >212</div>
								 <div class="boxdata" >+&frac14;<br /> ( -147 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ud Levante</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >145</div>
								 <div class="boxdata" >-&frac14;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rc Celta De Vigo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >580</div>
								 <div class="boxdata" >+&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atletico Madrid</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >-140</div>
								 <div class="boxdata" >-&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Valladolid</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -101 )</div>
								 <div class="boxdata" >388</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Villarreal</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Barcelona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >-181</div>
								 <div class="boxdata" >-&frac34;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Granada Cf</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >583</div>
								 <div class="boxdata" >+&frac34;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bk Hacken</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kalmar Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >286</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ostersunds Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >236</div>
								 <div class="boxdata" >+&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Falkenbergs Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ik Sirius Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >343</div>
								 <div class="boxdata" >+&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ifk Goteborg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >-119</div>
								 <div class="boxdata" >-&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Orebro Sk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >416</div>
								 <div class="boxdata" >+&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ifk Norrkoping</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >-136</div>
								 <div class="boxdata" >-&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brescia Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >285</div>
								 <div class="boxdata" >+&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Udinese Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >116</div>
								 <div class="boxdata" >-&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Inter Milano</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >160</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Milan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >221</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hellas Verona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >1149</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Juventus Turin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >-294</div>
								 <div class="boxdata" >-1&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dijon Fco</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >371</div>
								 <div class="boxdata" >+&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ogc Nice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stade Brestois 29</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >361</div>
								 <div class="boxdata" >+&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Girondins Bordeaux</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Amiens</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -138)</div>
								 <div class="boxdata" >309</div>
								 <div class="boxdata" >+&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Metz</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >138</div>
								 <div class="boxdata" >-&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Montpellier Hsc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >303</div>
								 <div class="boxdata" >+&frac14;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olympique Marseille</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Toulouse</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >299</div>
								 <div class="boxdata" >+&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olympique Nimes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac14;<br /> ( -126 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Monaco</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >193</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stade Reims</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >193</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Servette Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc St. Gallen 1879</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >186</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Sion</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >148</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Neuchatel Xamax Fcs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >202</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Hermannstadt</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >240</div>
								 <div class="boxdata" >+&frac14;<br /> ( -153 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acs Sepsi Osk Sfantu Gheorghe</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" >-&frac14;<br /> ( +110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dinamo Bucuresti 1948</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -101 )</div>
								 <div class="boxdata" >243</div>
								 <div class="boxdata" >+&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Astra Giurgiu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" >-&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acs Fc Academica Clinceni</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >231</div>
								 <div class="boxdata" >+&frac14;<br /> ( -153 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Voluntari</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" >-&frac14;<br /> ( +110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Blackburn Rovers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >194</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Reading Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >166</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Barnsley</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >354</div>
								 <div class="boxdata" >+&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nottingham Forest</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >-104</div>
								 <div class="boxdata" >-&frac12;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Swansea City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bristol City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Middlesbrough Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >248</div>
								 <div class="boxdata" >+&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cardiff City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >133</div>
								 <div class="boxdata" >-&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Preston North End</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >183</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Birmingham City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Derby County</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >500</div>
								 <div class="boxdata" >+&frac34;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Leeds United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >-158</div>
								 <div class="boxdata" >-&frac34;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Queens Park Rangers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Millwall Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >173</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stoke City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >301</div>
								 <div class="boxdata" >+&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brentford Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hull City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Luton Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Charlton Athletic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >234</div>
								 <div class="boxdata" >+&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wigan Athletic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >137</div>
								 <div class="boxdata" >-&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fulham Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >166</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sheffield Wednesday</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >195</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Greuther Furth</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >365</div>
								 <div class="boxdata" >+&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vfb Stuttgart</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -131 )</div>
								 <div class="boxdata" >-126</div>
								 <div class="boxdata" >-&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vfl Bochum</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >248</div>
								 <div class="boxdata" >+&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Sandhausen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" >-&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Karlsruher Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >259</div>
								 <div class="boxdata" >+&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1 Fc Nuremberg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" >-&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dsc Arminia Bielefeld</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -102 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Wehen Wiesbaden</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -135 )</div>
								 <div class="boxdata" >246</div>
								 <div class="boxdata" >+&frac14;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Royal Excel Mouscron</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" >-&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Waasland-beveren</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >239</div>
								 <div class="boxdata" >+&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cercle Brugge</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -102 )</div>
								 <div class="boxdata" >482</div>
								 <div class="boxdata" >+&frac34;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Royal Antwerp Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -135 )</div>
								 <div class="boxdata" >-161</div>
								 <div class="boxdata" >-&frac34;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >St. Truidense Vv</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >355</div>
								 <div class="boxdata" >+&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Royal Charleroi Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kv Oostende</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -114 )</div>
								 <div class="boxdata" >500</div>
								 <div class="boxdata" >+&frac34;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Krc Genk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >-172</div>
								 <div class="boxdata" >-&frac34;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Haugesund Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >325</div>
								 <div class="boxdata" >+&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Brann</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Dunarea 2005 Calarasi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >216</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Mioveni</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Metaloglobus Bucuresti</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" >-&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Asc Daco-getica Bucuresti</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >231</div>
								 <div class="boxdata" >+&frac14;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acs Viitorul Pandurii Targu Jiu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >409</div>
								 <div class="boxdata" >+&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Turris-oltul Turnu Magurele</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >-113</div>
								 <div class="boxdata" >-&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Uta Arad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >-119</div>
								 <div class="boxdata" >-&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Pandurii Lignitul Targu Jiu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >390</div>
								 <div class="boxdata" >+&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Csikszereda Miercurea Ciuc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >-204</div>
								 <div class="boxdata" >-&frac34;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Sportul Snagov Sa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >707</div>
								 <div class="boxdata" >+&frac34;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acs Champions Fc Arges</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >171</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Concordia Chiajna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >194</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Csm Resita</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >208</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Asu Politehnica Timisoara</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >161</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Petrolul Ploiesti</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Fc Universitatea Cluj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >193</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aberdeen Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +106 )</div>
								 <div class="boxdata" >171</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Livingston Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -147 )</div>
								 <div class="boxdata" >231</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ross County Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( +102 )</div>
								 <div class="boxdata" >300</div>
								 <div class="boxdata" >+&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Motherwell Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hamilton Academical Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >374</div>
								 <div class="boxdata" >+&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >St Mirren Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac12;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fortuna Sittard</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -135 )</div>
								 <div class="boxdata" >590</div>
								 <div class="boxdata" >+1<br /> ( -126)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vitesse Arnhem</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -102 )</div>
								 <div class="boxdata" >-212</div>
								 <div class="boxdata" >-1<br /> ( -108)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pec Zwolle</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >254</div>
								 <div class="boxdata" >+&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Groningen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vvv Venlo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >301</div>
								 <div class="boxdata" >+&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Willem Ii Tilburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >-101</div>
								 <div class="boxdata" >-&frac14;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rkc Waalwijk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -117 )</div>
								 <div class="boxdata" >412</div>
								 <div class="boxdata" >+&frac34;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sparta Rotterdam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -117 )</div>
								 <div class="boxdata" >-149</div>
								 <div class="boxdata" >-&frac34;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dunfermline Athletic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Partick Thistle</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dundee</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Greenock Morton Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ayr United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -108 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Alloa Athletic Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >260</div>
								 <div class="boxdata" >+&frac14;<br /> ( -126 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Queen Of The South Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >317</div>
								 <div class="boxdata" >+&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Inverness Caledonian Thistle</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Arbroath Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >459</div>
								 <div class="boxdata" >+&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dundee United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >-136</div>
								 <div class="boxdata" >-&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kghm Zaglebie Lubin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >299</div>
								 <div class="boxdata" >+&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wks Slask Wroclaw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >118</div>
								 <div class="boxdata" >-&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Arka Gdynia 1929</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >241</div>
								 <div class="boxdata" >+&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lks Lodz Pss</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >145</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Korona Kielce Sa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >301</div>
								 <div class="boxdata" >+&frac14;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ks Lechia Gdansk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" >-&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dumbarton Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >236</div>
								 <div class="boxdata" >+&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >East Fife Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" >-&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Raith Rovers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >136</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Airdrieonians</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >218</div>
								 <div class="boxdata" >+&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Falkirk Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >-125</div>
								 <div class="boxdata" >-&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stranraer Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >392</div>
								 <div class="boxdata" >+&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Clyde Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Peterhead Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Montrose Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Forfar Athletic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >123</div>
								 <div class="boxdata" >-&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ipswich Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >142</div>
								 <div class="boxdata" >-&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gillingham Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >231</div>
								 <div class="boxdata" >+&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Burton Albion</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >138</div>
								 <div class="boxdata" >-&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tranmere Rovers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >247</div>
								 <div class="boxdata" >+&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Oxford United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac14;<br /> ( -147 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lincoln City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >147</div>
								 <div class="boxdata" >-&frac14;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rochdale Afc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >380</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fleetwood Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" >-&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Peterborough United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >189</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Doncaster Rovers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Blackpool</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Accrington Stanley</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Shrewsbury Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >288</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rotherham United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Southend United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >334</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Milton Keynes Dons</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-106</div>
								 <div class="boxdata" >-&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bristol Rovers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >183</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Wimbledon</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >198</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sunderland Afc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" >-&frac34;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bolton Wanderers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >470</div>
								 <div class="boxdata" >+&frac34;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Portsmouth Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >159</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wycombe Wanderers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Exeter City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >220</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Newport County Afc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >171</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Scunthorpe United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >242</div>
								 <div class="boxdata" >+&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Walsall Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >145</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Morecambe Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >262</div>
								 <div class="boxdata" >+&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Oldham Athletic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" >-&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mansfield Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >197</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Port Vale</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Swindon Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >146</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cambridge United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >239</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cheltenham Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >260</div>
								 <div class="boxdata" >+&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Plymouth Argyle</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" >-&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Crawley Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Northampton Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" >-&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stevenage Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >284</div>
								 <div class="boxdata" >+&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Forest Green Rovers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >133</div>
								 <div class="boxdata" >-&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Macclesfield Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >279</div>
								 <div class="boxdata" >+&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Grimsby Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" >-&frac14;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Leyton Orient</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >319</div>
								 <div class="boxdata" >+&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Colchester United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -126 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Carlisle United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >308</div>
								 <div class="boxdata" >+&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bradford City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Salford City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Crewe Alexandra</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >123</div>
								 <div class="boxdata" >-&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cove Rangers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -116 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Elgin City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >319</div>
								 <div class="boxdata" >+&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Edinburgh City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >117</div>
								 <div class="boxdata" >-&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Albion Rovers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >239</div>
								 <div class="boxdata" >+&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stirling Albion</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >297</div>
								 <div class="boxdata" >+&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Annan Athletic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cowdenbeath Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >279</div>
								 <div class="boxdata" >+&frac14;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Queens Park Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >117</div>
								 <div class="boxdata" >-&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brechin City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >270</div>
								 <div class="boxdata" >+&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stenhousemuir Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Barrow Afc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ebbsfleet United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >171</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wrexham Afc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >146</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aldershot Town Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >241</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dagenham & Redbridge Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >211</div>
								 <div class="boxdata" >+&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Torquay United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -126 )</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" >-&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Harrogate Town Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >193</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Maidenhead United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >173</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Woking Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >148</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chorley Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >212</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chesterfield Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >192</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sutton United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yeovil Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >287</div>
								 <div class="boxdata" >+&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Solihull Moors Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dover Athletic Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >210</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hartlepool United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Barnet Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >212</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Halifax Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >165</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stockport County Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >211</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Boreham Wood Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >163</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Notts County</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >233</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bromley Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >146</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Eastleigh Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >223</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Fylde</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Pisa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >285</div>
								 <div class="boxdata" >+&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Chievo Verona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >133</div>
								 <div class="boxdata" >-&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Perugia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >220</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Spezia Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >165</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Crotone</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+&frac14;<br /> ( -147 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Cremonese</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" >-&frac14;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cosenza Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >412</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Benevento Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >-106</div>
								 <div class="boxdata" >-&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pordenone Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >279</div>
								 <div class="boxdata" >+&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Livorno</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >138</div>
								 <div class="boxdata" >-&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ascoli Calcio 1898 Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >235</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Juve Stabia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >158</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Cittadella</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >376</div>
								 <div class="boxdata" >+&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Empoli</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >-104</div>
								 <div class="boxdata" >-&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sl Benfica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >-172</div>
								 <div class="boxdata" >-&frac34;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Moreirense Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >589</div>
								 <div class="boxdata" >+&frac34;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Virtus Entella</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pescara Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >173</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rio Ave Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Os Belenenses</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >217</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Spusu Skn St. Polten</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >198</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Admira Wacker Modling</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Rapid Wien</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-107</div>
								 <div class="boxdata" >-&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wsg Tirol</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >308</div>
								 <div class="boxdata" >+&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Sturm Graz</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Mattersburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >293</div>
								 <div class="boxdata" >+&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Carl Zeiss Jena</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >240</div>
								 <div class="boxdata" >+&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chemnitzer Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" >-&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Eintracht Braunschweig</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Viktoria Cologne</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >193</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sg Sonnenhof Grossaspach</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >321</div>
								 <div class="boxdata" >+&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Spvgg Unterhaching</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >-&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Preussen 06 Munster</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >458</div>
								 <div class="boxdata" >+&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hallescher Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" >-&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1. Fc Magdeburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >186</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1 Fc Kaiserslautern</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >170</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tsv 1860 Munich</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >303</div>
								 <div class="boxdata" >+&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Msv Duisburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >116</div>
								 <div class="boxdata" >-&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hnk Gorica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Slaven</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gnk Dinamo Zagreb</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >-250</div>
								 <div class="boxdata" >-1<br /> ( -119)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vsnk Varazdin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >959</div>
								 <div class="boxdata" >+1<br /> ( -114)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tianjin Teda</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >235</div>
								 <div class="boxdata" >+&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hebei China Fortune Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" >-&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Shanghai Shenhua</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >351</div>
								 <div class="boxdata" >+&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Shandong Luneng Taishan Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >-125</div>
								 <div class="boxdata" >-&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Shenzhen Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >400</div>
								 <div class="boxdata" >+&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dalian Yifang Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >-126</div>
								 <div class="boxdata" >-&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mfk Karvina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1 Fk Pribram</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" >-&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Slovan Liberec</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >146</div>
								 <div class="boxdata" >-&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sfc Opava</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Viktoria Plzen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >-&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Dynamo Ceske Budejovice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >359</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Teplice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >450</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1 Fc Slovacko</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mfk Ruzomberok</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >411</div>
								 <div class="boxdata" >+&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dac 1904 Dunajska Streda</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >-126</div>
								 <div class="boxdata" >-&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mfk Zemplin Michalovce</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Nitra</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Vion Zlate Moravce - Vrable</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Skf Sered</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >137</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Pohronie Ziar Nad Hronom Dolna Zdana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -107)</div>
								 <div class="boxdata" >752</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Msk Zilina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -128)</div>
								 <div class="boxdata" >-270</div>
								 <div class="boxdata" >-1&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Usti Nad Labem</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >418</div>
								 <div class="boxdata" >+&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Dukla Prague</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >-126</div>
								 <div class="boxdata" >-&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Viktoria Zizkov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >217</div>
								 <div class="boxdata" >+&frac14;<br /> ( -149 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Fotbal Trinec</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" >-&frac14;<br /> ( +108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Sellier & Bellot Vlasim</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >336</div>
								 <div class="boxdata" >+&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afk Chrudim</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1.sk Prostejov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -102 )</div>
								 <div class="boxdata" >242</div>
								 <div class="boxdata" >+&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Lisen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -135 )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac14;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pardubice</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -101 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Varnsdorf</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >315</div>
								 <div class="boxdata" >+&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Slavoj Vysehrad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >476</div>
								 <div class="boxdata" >+&frac34;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Hradec Kralove</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >-147</div>
								 <div class="boxdata" >-&frac34;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Club Santos Laguna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >San Luis</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >198</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Club Leon</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Necaxa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Xolos De Tijuana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >279</div>
								 <div class="boxdata" >+&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Pachuca</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Queretaro Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >305</div>
								 <div class="boxdata" >+&frac14;<br /> ( -108 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf America</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac14;<br /> ( -126 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Puebla Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >486</div>
								 <div class="boxdata" >+&frac34;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Monterrey</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >-151</div>
								 <div class="boxdata" >-&frac34;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ofi Crete Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >259</div>
								 <div class="boxdata" >+&frac14;<br /> ( -147 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Volos Nps</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -142 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" >-&frac14;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aek Athens Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +104 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac12;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Panaitolikos</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -144 )</div>
								 <div class="boxdata" >412</div>
								 <div class="boxdata" >+&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atromitos Athens</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +106 )</div>
								 <div class="boxdata" >270</div>
								 <div class="boxdata" >+&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Asteras Tripolis</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -147 )</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Avangard Kursk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +104 )</div>
								 <div class="boxdata" >334</div>
								 <div class="boxdata" >+&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Nizhny Novgorod</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -144 )</div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" >-&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Neftek. Nizhnekamsk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >261</div>
								 <div class="boxdata" >+&frac14;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Spartak-2 Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >144</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Krasnodar-2</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +106 )</div>
								 <div class="boxdata" >286</div>
								 <div class="boxdata" >+&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chayka Peschanokopskoye</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -147 )</div>
								 <div class="boxdata" >139</div>
								 <div class="boxdata" >-&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Tekstilshchik Ivanovo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >215</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Mordovia Saransk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >174</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Rotor Volgograd</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >173</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Baltika Kaliningrad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -142 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Shinnik Yaroslavl</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >241</div>
								 <div class="boxdata" >+&frac14;<br /> ( -151 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Chertanovo Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" >-&frac14;<br /> ( +110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Khimki</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Fakel Voronezh</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >329</div>
								 <div class="boxdata" >+&frac14;<br /> ( -108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Tom Tomsk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >345</div>
								 <div class="boxdata" >+&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Torpedo Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atletico Nacional Medellin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -151)</div>
								 <div class="boxdata" >196</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ad Pasto</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +108)</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Millonarios Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >267</div>
								 <div class="boxdata" >+&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Independiente Medellin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" >-&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Bucaramanga</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >378</div>
								 <div class="boxdata" >+&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >America De Cali</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Once Caldas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -136)</div>
								 <div class="boxdata" >242</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rionegro Aguilas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >170</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Atletico Huila</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -144)</div>
								 <div class="boxdata" >421</div>
								 <div class="boxdata" >+&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Boyaca Patriotas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +104)</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Orlando Pirates Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -147)</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bidvest Wits Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +106)</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Maritzburg United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +106 )</div>
								 <div class="boxdata" >415</div>
								 <div class="boxdata" >+&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mamelodi Sundowns</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -147 )</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bloemfontein Celtic Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -133)</div>
								 <div class="boxdata" >243</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chippa United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -103)</div>
								 <div class="boxdata" >174</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Black Leopards Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -129)</div>
								 <div class="boxdata" >294</div>
								 <div class="boxdata" >+&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stellenbosch Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -106)</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" >-&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Supersport United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -131)</div>
								 <div class="boxdata" >210</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Polokwane City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -104)</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Union Saint-gilloise</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kvc Westerlo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >173</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kfco Beerschot-wilrijk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >163</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Royal Excelsior Virton</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >210</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Norrby If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >447</div>
								 <div class="boxdata" >+&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jonkopings Sodra</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >-123</div>
								 <div class="boxdata" >-&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Osters If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dalkurd Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >168</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Feralpi Salo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Modena Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >202</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Como 1907</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Pistoiese</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >185</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Sudtirol</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Arzignano Valchiampo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >212</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Gozzano</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >373</div>
								 <div class="boxdata" >+&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Pro Vercelli 1892</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Imolese Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >234</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ravenna Fc 1913</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Piacenza Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cesena Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >181</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rimini Fc 1912</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >344</div>
								 <div class="boxdata" >+&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Carpi Fc 1909</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Teramo Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rende Calcio 1968</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >212</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dunav 2010</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >368</div>
								 <div class="boxdata" >+&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ofc Botev Vratsa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >-113</div>
								 <div class="boxdata" >-&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Levski Sofia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pfc Slavia Sofia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >320</div>
								 <div class="boxdata" >+&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Luch Vladivostok</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -144)</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac14;<br /> ( -153 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Torpedo Armavir</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +104)</div>
								 <div class="boxdata" >159</div>
								 <div class="boxdata" >-&frac14;<br /> ( +110 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Antalyaspor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >202</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kasimpasa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >146</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mke Ankaragucu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >628</div>
								 <div class="boxdata" >+1<br /> ( -140)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fenerbahce</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >-204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Denizlispor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kayserispor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >165</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Utc De Cajamarca</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >241</div>
								 <div class="boxdata" >+&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sport Boys Association</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" >-&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Universitario De Deportes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sport Huancayo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" >-&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Top Oss</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >187</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Helmond Sport</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >San Jose Earthquakes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -136 )</div>
								 <div class="boxdata" >295</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atlanta United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( EV )</div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Columbus Crew</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vancouver Whitecaps Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -133 )</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" >+&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Salt Lake</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >283</div>
								 <div class="boxdata" >+&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New England Revolution</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -131 )</div>
								 <div class="boxdata" >-101</div>
								 <div class="boxdata" >-&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chicago Fire</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Cincinnati</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -133 )</div>
								 <div class="boxdata" >279</div>
								 <div class="boxdata" >+&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Colorado Rapids</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >350</div>
								 <div class="boxdata" >+&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sporting Kansas City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -114 )</div>
								 <div class="boxdata" >-128</div>
								 <div class="boxdata" >-&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Montreal Impact</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >303</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Los Angeles Galaxy</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >-114</div>
								 <div class="boxdata" >-&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Toronto Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -117)</div>
								 <div class="boxdata" >363</div>
								 <div class="boxdata" >+&frac34;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Los Angeles Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -116)</div>
								 <div class="boxdata" >-149</div>
								 <div class="boxdata" >-&frac34;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Orlando City Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >234</div>
								 <div class="boxdata" >+&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Houston Dynamo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" >-&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ec Bahia Ba</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -117)</div>
								 <div class="boxdata" >297</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Corinthians Sp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -117)</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gremio Fb Porto Alegrense</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +106 )</div>
								 <div class="boxdata" >277</div>
								 <div class="boxdata" >+&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Santos Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -147 )</div>
								 <div class="boxdata" >144</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Flamengo Rj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cruzeiro Ec Mg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >288</div>
								 <div class="boxdata" >+&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wacker Innsbruck</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -106 )</div>
								 <div class="boxdata" >212</div>
								 <div class="boxdata" >+&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Liefering</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dornbirn 1913</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >248</div>
								 <div class="boxdata" >+&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Floridsdorfer Ac</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Doxa Katokopias Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >367</div>
								 <div class="boxdata" >+&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ael Limassol Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >-101</div>
								 <div class="boxdata" >-&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sao Paulo Sp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +108 )</div>
								 <div class="boxdata" >172</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Botafogo Rj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -149 )</div>
								 <div class="boxdata" >231</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Stk 1914 Samorin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >286</div>
								 <div class="boxdata" >+&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Puchov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Dubnica Nad Vahom</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >183</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mfk Tatran Liptovsky Mikulas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >165</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mfk Dukla Banska Bystrica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kfc Komarno</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >183</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fa 2000</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Skovshoved If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >166</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hik Hellerup</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hillerod Fodbold</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >174</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vanloese If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >218</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bk Frem</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brabrand If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >223</div>
								 <div class="boxdata" >+&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Middelfart Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >136</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aarhus Fremad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-126</div>
								 <div class="boxdata" >-&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vejgaard Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >378</div>
								 <div class="boxdata" >+&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Thisted Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Naesby Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >236</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Sydvest 05</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >173</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vejlby Skovbakken Aarhus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >166</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rc Lens</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -140)</div>
								 <div class="boxdata" >145</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sm Caen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +102)</div>
								 <div class="boxdata" >289</div>
								 <div class="boxdata" >+&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dalum If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >292</div>
								 <div class="boxdata" >+&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jammerbugt Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Montedio Yamagata</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >139</div>
								 <div class="boxdata" >-&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Ryukyu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >221</div>
								 <div class="boxdata" >+&frac14;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kyoto Sanga Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -101 )</div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >V-varen Nagasaki</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >197</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Niigata Albirex</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >235</div>
								 <div class="boxdata" >+&frac14;<br /> ( -151 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ventforet Kofu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" >-&frac14;<br /> ( +108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fagiano Okayama</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >188</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Avispa Fukuoka</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >195</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Renofa Yamaguchi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >248</div>
								 <div class="boxdata" >+&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Zweigen Kanazawa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >116</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Sep 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aston Villa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -116 )</div>
								 <div class="boxdata" >454</div>
								 <div class="boxdata" >+&frac34;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Arsenal Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >-161</div>
								 <div class="boxdata" >-&frac34;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Manchester United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" >-&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >West Ham United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >229</div>
								 <div class="boxdata" >+&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Liverpool Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >117</div>
								 <div class="boxdata" >-&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chelsea Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >243</div>
								 <div class="boxdata" >+&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fortuna Dusseldorf</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -102 )</div>
								 <div class="boxdata" >338</div>
								 <div class="boxdata" >+&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Borussia Monchengladbach</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -135 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Borussia Dortmund</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >-109</div>
								 <div class="boxdata" >-&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Eintracht Frankfurt</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -114 )</div>
								 <div class="boxdata" >293</div>
								 <div class="boxdata" >+&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wolverhampton Wanderers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +106 )</div>
								 <div class="boxdata" >192</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Crystal Palace</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -147 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pfc Sochi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >424</div>
								 <div class="boxdata" >+&frac12;<br /> ( -138 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dinamo Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >-103</div>
								 <div class="boxdata" >-&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Ural Yekaterinburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >258</div>
								 <div class="boxdata" >+&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Arsenal Tula</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" >-&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Lokomotiv Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >143</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Orenburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >264</div>
								 <div class="boxdata" >+&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Krasnodar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cska Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >166</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Madrid</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >144</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Sevilla</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -125 )</div>
								 <div class="boxdata" >187</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Sociedad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >174</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rcd Espanyol Barcelona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >218</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rcd Mallorca</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -125)</div>
								 <div class="boxdata" >398</div>
								 <div class="boxdata" >+&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Getafe</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -109)</div>
								 <div class="boxdata" >116</div>
								 <div class="boxdata" >-&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Deportivo Alaves Sad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -140)</div>
								 <div class="boxdata" >453</div>
								 <div class="boxdata" >+&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Athletic Bilbao</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +102)</div>
								 <div class="boxdata" >-101</div>
								 <div class="boxdata" >-&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Leganes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >438</div>
								 <div class="boxdata" >+&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Valencia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >-111</div>
								 <div class="boxdata" >-&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aik</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >222</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hammarby If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >159</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Malmo Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >If Elfsborg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >293</div>
								 <div class="boxdata" >+&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Eskilstuna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >401</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Helsingborgs If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >-114</div>
								 <div class="boxdata" >-&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Torino</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >189</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Uc Sampdoria</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >171</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ssc Napoli</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-163</div>
								 <div class="boxdata" >-&frac34;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Lecce</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >522</div>
								 <div class="boxdata" >+&frac34;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Spal 2013</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >259</div>
								 <div class="boxdata" >+&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Sassuolo Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" >-&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Roma</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bologna Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >187</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Parma Calcio 1913 S.r.l.</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >590</div>
								 <div class="boxdata" >+&frac34;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Lazio Roma</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >-178</div>
								 <div class="boxdata" >-&frac34;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acf Fiorentina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >258</div>
								 <div class="boxdata" >+&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atalanta Bergamasca Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Saint-etienne</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >260</div>
								 <div class="boxdata" >+&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sco Angers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >139</div>
								 <div class="boxdata" >-&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Paris Saint-germain</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -126 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac14;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olympique Lyonnais</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >243</div>
								 <div class="boxdata" >+&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lille Losc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >231</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Stade Rennes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >164</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Basel 1893</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >221</div>
								 <div class="boxdata" >+&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bsc Young Boys</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" >-&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Thun</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >233</div>
								 <div class="boxdata" >+&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Zurich</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Luzern</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >227</div>
								 <div class="boxdata" >+&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Lugano</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >134</div>
								 <div class="boxdata" >-&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Slavia Prague</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Sparta Prague</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >210</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Cfr 1907 Cluj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >159</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fotbal Club Fcsb</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >237</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chindia Targoviste</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >397</div>
								 <div class="boxdata" >+&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Botosani</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >-107</div>
								 <div class="boxdata" >-&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Huddersfield Town</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >363</div>
								 <div class="boxdata" >+&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >West Bromwich Albion</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >-108</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ssv Jahn Regensburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >193</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sg Dynamo Dresden</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >145</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc St. Pauli</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >231</div>
								 <div class="boxdata" >+&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vfl 1899 Osnabruck</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >134</div>
								 <div class="boxdata" >-&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Erzgebirge Aue</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >475</div>
								 <div class="boxdata" >+&frac34;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hamburger Sv</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >-147</div>
								 <div class="boxdata" >-&frac34;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kaa Gent</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -113 )</div>
								 <div class="boxdata" >121</div>
								 <div class="boxdata" >-&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Zulte Waregem</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >221</div>
								 <div class="boxdata" >+&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kas Eupen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >584</div>
								 <div class="boxdata" >+&frac34;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Standard Liege</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" >-&frac34;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rsc Anderlecht</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >363</div>
								 <div class="boxdata" >+&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Club Brugge</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -133 )</div>
								 <div class="boxdata" >-126</div>
								 <div class="boxdata" >-&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sarpsborg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >305</div>
								 <div class="boxdata" >+&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Odds Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kristiansund Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >209</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lillestrom Sk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rosenborg Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mjondalen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >279</div>
								 <div class="boxdata" >+&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stromsgodset If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tromsoe Il</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >146</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Viking Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >278</div>
								 <div class="boxdata" >+&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Valerenga If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bodo/glimt</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >135</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ranheim Il</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >211</div>
								 <div class="boxdata" >+&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Midtjylland</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Copenhagen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lyngby Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >191</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Silkeborg If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >147</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aalborg Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >196</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Nordsjaelland</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -131 )</div>
								 <div class="boxdata" >140</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sonderjyske</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >159</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hobro Ik</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brondby If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Esbjerg Fb</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >266</div>
								 <div class="boxdata" >+&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Scm Gloria Buzau</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >268</div>
								 <div class="boxdata" >+&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Rapid 1923</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" >-&frac14;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kilmarnock Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >655</div>
								 <div class="boxdata" >+&frac34;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Celtic Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-181</div>
								 <div class="boxdata" >-&frac34;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Heart Of Midlothian</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >193</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hibernian Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >173</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rangers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >-169</div>
								 <div class="boxdata" >-&frac34;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >St Johnstone Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >592</div>
								 <div class="boxdata" >+&frac34;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Az Alkmaar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -106 )</div>
								 <div class="boxdata" >-123</div>
								 <div class="boxdata" >-&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ado Den Haag</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -129 )</div>
								 <div class="boxdata" >348</div>
								 <div class="boxdata" >+&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Feyenoord Rotterdam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -105 )</div>
								 <div class="boxdata" >-149</div>
								 <div class="boxdata" >-&frac34;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Emmen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -129 )</div>
								 <div class="boxdata" >437</div>
								 <div class="boxdata" >+&frac34;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Utrecht</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >121</div>
								 <div class="boxdata" >-&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Heerenveen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >222</div>
								 <div class="boxdata" >+&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ajax Amsterdam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Psv Eindhoven</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -113 )</div>
								 <div class="boxdata" >185</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ks Gornik Zabrze</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >300</div>
								 <div class="boxdata" >+&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mks Pogon Szczecin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wisla Krakow Sa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >188</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wisla Plock Sa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >164</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Legia Warsaw</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >170</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ks Cracovia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >202</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sportiva Salernitana</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >196</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Trapani Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Portimonense Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >183</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vitoria Setubal</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >206</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Boavista Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -149)</div>
								 <div class="boxdata" >249</div>
								 <div class="boxdata" >+&frac14;<br /> ( -156 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gil Vicente Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +108)</div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" >-&frac14;<br /> ( +112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vitoria Guimaraes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" >-&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Tondela</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >239</div>
								 <div class="boxdata" >+&frac14;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Santa Clara</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >913</div>
								 <div class="boxdata" >+1<br /> ( -102)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Porto</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >-270</div>
								 <div class="boxdata" >-1<br /> ( -135)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wolfsberger Ac</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -103 )</div>
								 <div class="boxdata" >-101</div>
								 <div class="boxdata" >-&frac12;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tsv Hartberg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -135 )</div>
								 <div class="boxdata" >282</div>
								 <div class="boxdata" >+&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Salzburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >-131</div>
								 <div class="boxdata" >-&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lask</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -133 )</div>
								 <div class="boxdata" >380</div>
								 <div class="boxdata" >+&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Scr Altach</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >197</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Austria Wien</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fsv Zwickau</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >290</div>
								 <div class="boxdata" >+&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Hansa Rostock</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Ingolstadt 04</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Bayern Munich Ii</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Inter Zapresic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >897</div>
								 <div class="boxdata" >+1<br /> ( -117)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hnk Hajduk Split</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >-243</div>
								 <div class="boxdata" >-1<br /> ( -117)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hnk Rijeka</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >-112</div>
								 <div class="boxdata" >-&frac12;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nk Istra 1961</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >405</div>
								 <div class="boxdata" >+&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Beijing Guoan Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" >-&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tianjin Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >426</div>
								 <div class="boxdata" >+&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wuhan Zall</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -103 )</div>
								 <div class="boxdata" >580</div>
								 <div class="boxdata" >+&frac34;<br /> ( -103 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Guangzhou Evergrande</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -133 )</div>
								 <div class="boxdata" >-188</div>
								 <div class="boxdata" >-&frac34;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Henan Jianye</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >636</div>
								 <div class="boxdata" >+&frac34;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Shanghai Sipg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >-196</div>
								 <div class="boxdata" >-&frac34;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Banik Ostrava</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >290</div>
								 <div class="boxdata" >+&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Mlada Boleslav</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Slovan Bratislava</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -114 )</div>
								 <div class="boxdata" >118</div>
								 <div class="boxdata" >-&frac14;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Trencin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >227</div>
								 <div class="boxdata" >+&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cruz Azul</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >209</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pumas Unam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >172</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tiburones Rojos De Veracruz</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -101 )</div>
								 <div class="boxdata" >365</div>
								 <div class="boxdata" >+&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Juarez</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >-109</div>
								 <div class="boxdata" >-&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ao Xanthi Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -131)</div>
								 <div class="boxdata" >280</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ae Larissa Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -104)</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Panionios Athens</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -147)</div>
								 <div class="boxdata" >326</div>
								 <div class="boxdata" >+&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lamia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +106)</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" >-&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aris Thessaloniki Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +108 )</div>
								 <div class="boxdata" >556</div>
								 <div class="boxdata" >+&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Paok Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -149 )</div>
								 <div class="boxdata" >-123</div>
								 <div class="boxdata" >-&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olympiacos Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >-121</div>
								 <div class="boxdata" >-&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Panathinaikos Athens</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >497</div>
								 <div class="boxdata" >+&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ad Cali</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -142)</div>
								 <div class="boxdata" >240</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cucuta Deportivo Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +104)</div>
								 <div class="boxdata" >169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jaguares D. Cordoba</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >507</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Club Atletico Junior</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >U. M. Santa Marta</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -133)</div>
								 <div class="boxdata" >366</div>
								 <div class="boxdata" >+&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Alianza Petrolera</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -103)</div>
								 <div class="boxdata" >121</div>
								 <div class="boxdata" >-&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Envigado Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -133)</div>
								 <div class="boxdata" >393</div>
								 <div class="boxdata" >+&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Independiente Santa Fe</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -104)</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lamontville Golden Arrows Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -136)</div>
								 <div class="boxdata" >293</div>
								 <div class="boxdata" >+&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cape Town City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >144</div>
								 <div class="boxdata" >-&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Highlands Park</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -131)</div>
								 <div class="boxdata" >234</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Baroka Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -104)</div>
								 <div class="boxdata" >184</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lommel Sk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -101 )</div>
								 <div class="boxdata" >303</div>
								 <div class="boxdata" >+&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Oud-heverlee</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Trelleborgs Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >336</div>
								 <div class="boxdata" >+&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Halmstads Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Degerfors If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vasteraas Sk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >188</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >If Brommapojkarna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >430</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mjallby Aif</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Triestina Calcio 1918</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >160</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Virtus Vecomp Verona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >239</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aurora Pro Patria 1919</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >344</div>
								 <div class="boxdata" >+&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Robur Siena</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Reggio Audace Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aj Fano 1906</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >218</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Pergolettese 1932</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >376</div>
								 <div class="boxdata" >+&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Novara Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >106</div>
								 <div class="boxdata" >-&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Calcio Padova</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" >-&frac14;<br /> ( +108 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ssd Vis Pesaro 1898</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >242</div>
								 <div class="boxdata" >+&frac14;<br /> ( -151 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Alessandria Calcio 1912</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >163</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Pianese</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >231</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Uc Albinoleffe</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >288</div>
								 <div class="boxdata" >+&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Arezzo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >134</div>
								 <div class="boxdata" >-&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lr Vicenza Virtus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >171</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gubbio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >221</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pontedera</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >227</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Juventus U23</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olbia Calcio 1905</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >250</div>
								 <div class="boxdata" >+&frac14;<br /> ( -149 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Renate</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" >-&frac14;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sambenedettese</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >208</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fermana Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >183</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Bisceglie</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >290</div>
								 <div class="boxdata" >+&frac14;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Avellino 1912</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >133</div>
								 <div class="boxdata" >-&frac14;<br /> ( -108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Catanzaro 1929</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >173</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Viterbo Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >216</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Paganese Calcio 1926</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac14;<br /> ( -147 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cavese 1919</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" >-&frac14;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Calcio Catania</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >165</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Monopoli 1966</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >223</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Potenza Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Rieti</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >264</div>
								 <div class="boxdata" >+&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Casertana Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >174</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Az Picerno</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -106 )</div>
								 <div class="boxdata" >194</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ternana Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >164</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sicula Leonzio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >229</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ssc Bari</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" >-&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Virtus Francavilla Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >271</div>
								 <div class="boxdata" >+&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Vibonese Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >458</div>
								 <div class="boxdata" >+&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Reggina 1914</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >-111</div>
								 <div class="boxdata" >-&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Monza</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Calcio Lecco</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >361</div>
								 <div class="boxdata" >+&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Arda Kurdzhali</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >986</div>
								 <div class="boxdata" >+1<br /> ( -125)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ludogorets 1945 Razgrad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >-238</div>
								 <div class="boxdata" >-1<br /> ( -109)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Beroe Stara Zagora</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >293</div>
								 <div class="boxdata" >+&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lokomotiv Plovdiv</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ferencvarosi Tc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Zalaegerszeg Te</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gazisehir Gaziantep Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >262</div>
								 <div class="boxdata" >+&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Caykur Rizespor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >121</div>
								 <div class="boxdata" >-&frac14;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Alanyaspor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" >-&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Genclerbirligi Sk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >257</div>
								 <div class="boxdata" >+&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Galatasaray</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yeni Malatyaspor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >288</div>
								 <div class="boxdata" >+&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Viborg Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Naestved Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >277</div>
								 <div class="boxdata" >+&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vendsyssel Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >142</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nykoebing Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >208</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bk Fremad Amager</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >277</div>
								 <div class="boxdata" >+&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Fredericia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Skive Ik</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >187</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kolding If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >166</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Carlos Mannucci</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >682</div>
								 <div class="boxdata" >+&frac34;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fbc Melgar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" >-&frac34;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ayacucho Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >1895</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sporting Cristal</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >-344</div>
								 <div class="boxdata" >-1&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Garcilaso</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >480</div>
								 <div class="boxdata" >+&frac12;<br /> ( -101 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Alianza Lima</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-140</div>
								 <div class="boxdata" >-&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Usmp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >299</div>
								 <div class="boxdata" >+&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Alianza Universidad De Huanuco</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Academia Cantolao</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >270</div>
								 <div class="boxdata" >+&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Union Comercio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >124</div>
								 <div class="boxdata" >-&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Minnesota United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >240</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Portland Timbers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Seattle Sounders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >168</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dc United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -102 )</div>
								 <div class="boxdata" >166</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Philadelphia Union</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -114 )</div>
								 <div class="boxdata" >235</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ny Red Bulls</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nea Salamina Famagusta Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >161</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ethnikos Achnas Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >220</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pafos Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >300</div>
								 <div class="boxdata" >+&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Anorthosis Famagusta Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chapecoense Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +108 )</div>
								 <div class="boxdata" >590</div>
								 <div class="boxdata" >+&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Internacional Rs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -149 )</div>
								 <div class="boxdata" >-126</div>
								 <div class="boxdata" >-&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fluminense Fc Rj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >188</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Goias Ec Go</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >181</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ceara Ce</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -131)</div>
								 <div class="boxdata" >196</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Csa Al</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -105)</div>
								 <div class="boxdata" >217</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Paranaense Pr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >229</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vasco Da Gama Rj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >164</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >New York City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >189</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dallas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >142</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Se Palmeiras Sp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +106 )</div>
								 <div class="boxdata" >119</div>
								 <div class="boxdata" >-&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fortaleza Ec Ce</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -147 )</div>
								 <div class="boxdata" >341</div>
								 <div class="boxdata" >+&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Young Violets Fk Austria Wien</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >259</div>
								 <div class="boxdata" >+&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Vorwarts Steyr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -114 )</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Holbaek B&i</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >233</div>
								 <div class="boxdata" >+&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bk Avarta</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >139</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bronshoj Bk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Slagelse Bi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >326</div>
								 <div class="boxdata" >+&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ringkobing If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -103 )</div>
								 <div class="boxdata" >468</div>
								 <div class="boxdata" >+&frac34;<br /> ( -126 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ab Gladsaxe</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -135 )</div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" >-&frac34;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tokushima Vortis</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >133</div>
								 <div class="boxdata" >-&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jef United Ichihara Chiba</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >242</div>
								 <div class="boxdata" >+&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tokyo Verdy</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >285</div>
								 <div class="boxdata" >+&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Omiya Ardija</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >122</div>
								 <div class="boxdata" >-&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ehime Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >436</div>
								 <div class="boxdata" >+&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kashiwa Reysol</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >-126</div>
								 <div class="boxdata" >-&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yokohama Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -129 )</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" >-&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Machida Zelvia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >258</div>
								 <div class="boxdata" >+&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Gifu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >398</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mito Hollyhock</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kagoshima United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >181</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tochigi Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >197</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Sep 23</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tsg 1899 Hoffenheim</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >241</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vfl Wolfsburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -131 )</div>
								 <div class="boxdata" >114</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Molde Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stabaek Fotball</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >268</div>
								 <div class="boxdata" >+&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Horsens</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >309</div>
								 <div class="boxdata" >+&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Randers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >116</div>
								 <div class="boxdata" >-&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Maritimo Madeira</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -108 )</div>
								 <div class="boxdata" >564</div>
								 <div class="boxdata" >+&frac34;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sporting Braga</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >-161</div>
								 <div class="boxdata" >-&frac34;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Famalicao</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >354</div>
								 <div class="boxdata" >+&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sporting Cp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >-119</div>
								 <div class="boxdata" >-&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sv Waldhof Mannheim 07</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Krefelder Fc Uerdingen 05</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >175</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Sigma Olomouc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >334</div>
								 <div class="boxdata" >+&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Jablonec</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac14;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Carrarese Calcio 1908</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >168</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Giana Erminio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Vitosha Bistrica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >478</div>
								 <div class="boxdata" >+&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Etar 1924 Veliko Tarnovo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" >-&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Basaksehir Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Besiktas</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Trabzonspor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sivasspor</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Cambuur</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >167</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Jong Psv Eindhoven</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -133 )</div>
								 <div class="boxdata" >165</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Apoel Nicosia Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >-204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Enosis Neon Paralimni Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -128 )</div>
								 <div class="boxdata" >639</div>
								 <div class="boxdata" >+1<br /> ( -136)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atletico Mineiro Mg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >169</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Avai Fc Sc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >222</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rodez Aveyron</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -140)</div>
								 <div class="boxdata" >401</div>
								 <div class="boxdata" >+&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Lorient</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +102)</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Csm Politehnica Iasi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >374</div>
								 <div class="boxdata" >+&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Viitorul Constanta</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >-104</div>
								 <div class="boxdata" >-&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ripensia Timisoara</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -120 )</div>
								 <div class="boxdata" >224</div>
								 <div class="boxdata" >+&frac14;<br /> ( -149 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Farul Constanta 1920</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" >-&frac14;<br /> ( +108 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Sep 24</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ud Levante</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >257</div>
								 <div class="boxdata" >+&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Betis Balompie</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >118</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Villarreal</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -104)</div>
								 <div class="boxdata" >535</div>
								 <div class="boxdata" >+1<br /> ( -133)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Barcelona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -131)</div>
								 <div class="boxdata" >-200</div>
								 <div class="boxdata" >-1<br /> ( -103)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Granada Cf</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -103 )</div>
								 <div class="boxdata" >247</div>
								 <div class="boxdata" >+&frac14;<br /> ( -149 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Valladolid</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" >-&frac14;<br /> ( +108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kaizer Chiefs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -147)</div>
								 <div class="boxdata" >192</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Amazulu Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +106)</div>
								 <div class="boxdata" >213</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Falkenbergs Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >891</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Djurgardens If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >-270</div>
								 <div class="boxdata" >-1<br /> ( -136)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Gif Sundsvall</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >273</div>
								 <div class="boxdata" >+&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kalmar Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" >-&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Juventus Turin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >-161</div>
								 <div class="boxdata" >-&frac34;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brescia Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >532</div>
								 <div class="boxdata" >+&frac34;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Udinese Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >174</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hellas Verona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >187</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fk Csikszereda Miercurea Ciuc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Metalul Buzau</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >363</div>
								 <div class="boxdata" >+&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Astra Giurgiu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-263</div>
								 <div class="boxdata" >-1<br /> ( -126)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acs Flacara Horezu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >922</div>
								 <div class="boxdata" >+1<br /> ( -108)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dinamo Bucuresti 1948</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >-103</div>
								 <div class="boxdata" >-&frac12;<br /> ( -101 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Uta Arad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >366</div>
								 <div class="boxdata" >+&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Gaz Metan Medias</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" >-&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acs Foresta Suceava</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >484</div>
								 <div class="boxdata" >+&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Voluntari</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >136</div>
								 <div class="boxdata" >-&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Concordia Chiajna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >258</div>
								 <div class="boxdata" >+&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Sep 25</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Athletic Bilbao</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -126)</div>
								 <div class="boxdata" >191</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cd Leganes</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -109)</div>
								 <div class="boxdata" >227</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atletico Madrid</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -125)</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac12;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rcd Mallorca</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( -109)</div>
								 <div class="boxdata" >468</div>
								 <div class="boxdata" >+&frac12;<br /> ( -144 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Getafe</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >358</div>
								 <div class="boxdata" >+&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Valencia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Osasuna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -116 )</div>
								 <div class="boxdata" >732</div>
								 <div class="boxdata" >+1<br /> ( -116)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Madrid</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >-238</div>
								 <div class="boxdata" >-1<br /> ( -119)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ostersunds Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >301</div>
								 <div class="boxdata" >+&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Orebro Sk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ifk Goteborg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >376</div>
								 <div class="boxdata" >+&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aik</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >-104</div>
								 <div class="boxdata" >-&frac12;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >If Elfsborg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >401</div>
								 <div class="boxdata" >+&frac12;<br /> ( -108 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bk Hacken</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >-131</div>
								 <div class="boxdata" >-&frac12;<br /> ( -126 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hammarby If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -125)</div>
								 <div class="boxdata" >-123</div>
								 <div class="boxdata" >-&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ik Sirius Fk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -111)</div>
								 <div class="boxdata" >292</div>
								 <div class="boxdata" >+&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bologna Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >160</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Genoa Cfc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >184</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Uc Sampdoria</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >286</div>
								 <div class="boxdata" >+&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acf Fiorentina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac14;<br /> ( -128 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Lecce</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -101 )</div>
								 <div class="boxdata" >301</div>
								 <div class="boxdata" >+&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Spal 2013</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ss Lazio Roma</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -101 )</div>
								 <div class="boxdata" >236</div>
								 <div class="boxdata" >+&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Inter Milano</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -136 )</div>
								 <div class="boxdata" >136</div>
								 <div class="boxdata" >-&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Us Sassuolo Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >221</div>
								 <div class="boxdata" >+&frac14;<br /> ( -147 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Parma Calcio 1913 S.r.l.</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >145</div>
								 <div class="boxdata" >-&frac14;<br /> ( +106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atalanta Bergamasca Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -113 )</div>
								 <div class="boxdata" >224</div>
								 <div class="boxdata" >+&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >As Roma</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" >-&frac14;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cagliari Calcio</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -109 )</div>
								 <div class="boxdata" >623</div>
								 <div class="boxdata" >+1<br /> ( -138)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ssc Napoli</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -126 )</div>
								 <div class="boxdata" >-204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stade Reims</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -121)</div>
								 <div class="boxdata" >1068</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Paris Saint-germain</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -113)</div>
								 <div class="boxdata" >-370</div>
								 <div class="boxdata" >-1&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Republican Fc Akhmat Grozny</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >184</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Ska-khabarovsk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >218</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Tambov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >198</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Tom Tomsk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >195</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dinamo Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -147)</div>
								 <div class="boxdata" >147</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Luch Vladivostok</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +106)</div>
								 <div class="boxdata" >285</div>
								 <div class="boxdata" >+&frac14;<br /> ( -138 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Sep 26</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Sevilla</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >143</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sd Eibar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" >+&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Deportivo Alaves Sad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >363</div>
								 <div class="boxdata" >+&frac14;<br /> ( -102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Real Sociedad</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >108</div>
								 <div class="boxdata" >-&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rcd Espanyol Barcelona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >265</div>
								 <div class="boxdata" >+&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rc Celta De Vigo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >133</div>
								 <div class="boxdata" >-&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Eskilstuna</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >806</div>
								 <div class="boxdata" >+1<br /> ( -135)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ifk Norrkoping</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >-217</div>
								 <div class="boxdata" >-1<br /> ( -102)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Helsingborgs If</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >624</div>
								 <div class="boxdata" >+&frac34;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Malmo Ff</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -123 )</div>
								 <div class="boxdata" >-178</div>
								 <div class="boxdata" >-&frac34;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ac Milan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Torino</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >172</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Sep 27</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Eintracht Frankfurt</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >142</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1. Fc Union Berlin</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -133 )</div>
								 <div class="boxdata" >195</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Voluntari</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >366</div>
								 <div class="boxdata" >+&frac12;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dinamo Bucuresti 1948</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac12;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Sep 28</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Watford Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >267</div>
								 <div class="boxdata" >+&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wolverhampton Wanderers</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -114 )</div>
								 <div class="boxdata" >131</div>
								 <div class="boxdata" >-&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Manchester City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >-163</div>
								 <div class="boxdata" >-&frac34;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Everton Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -126 )</div>
								 <div class="boxdata" >480</div>
								 <div class="boxdata" >+&frac34;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brighton & Hove Albion</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >465</div>
								 <div class="boxdata" >+&frac34;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chelsea Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >-144</div>
								 <div class="boxdata" >-&frac34;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Southampton Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >487</div>
								 <div class="boxdata" >+&frac34;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tottenham Hotspur</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-158</div>
								 <div class="boxdata" >-&frac34;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Liverpool Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >-181</div>
								 <div class="boxdata" >-&frac34;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sheffield United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >578</div>
								 <div class="boxdata" >+&frac34;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Norwich City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >246</div>
								 <div class="boxdata" >+&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Crystal Palace</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" >-&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >West Ham United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -105 )</div>
								 <div class="boxdata" >177</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Bournemouth</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -131 )</div>
								 <div class="boxdata" >154</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Burnley Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >219</div>
								 <div class="boxdata" >+&frac14;<br /> ( -149 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Aston Villa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" >-&frac14;<br /> ( +108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Werder Bremen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2<br /> ( -101)</div>
								 <div class="boxdata" >482</div>
								 <div class="boxdata" >+&frac34;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Borussia Dortmund</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -136)</div>
								 <div class="boxdata" >-181</div>
								 <div class="boxdata" >-&frac34;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Schalke 04</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -106 )</div>
								 <div class="boxdata" >363</div>
								 <div class="boxdata" >+&frac12;<br /> ( -111 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rb Leipzig</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -129 )</div>
								 <div class="boxdata" >-128</div>
								 <div class="boxdata" >-&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Borussia Monchengladbach</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -106 )</div>
								 <div class="boxdata" >179</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tsg 1899 Hoffenheim</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -129 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Bayern Munich</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >-303</div>
								 <div class="boxdata" >-1&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Paderborn 07</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >752</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bayer 04 Leverkusen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-120</div>
								 <div class="boxdata" >-&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Augsburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2<br /> ( -138)</div>
								 <div class="boxdata" >305</div>
								 <div class="boxdata" >+&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vfl Wolfsburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -129 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1. Fsv Mainz 05</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >186</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Botosani</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >202</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Hermannstadt</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >186</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afc Astra Giurgiu</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >147</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chindia Targoviste</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Zenit St Petersburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >186</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Lokomotiv Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >196</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Dinamo Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >275</div>
								 <div class="boxdata" >+&frac14;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Rostov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >141</div>
								 <div class="boxdata" >-&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Tambov</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >297</div>
								 <div class="boxdata" >+&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Krylia Sovetov Samara</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >127</div>
								 <div class="boxdata" >-&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Puskas Akademia Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Debreceni Vsc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Budapest Honved Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -125 )</div>
								 <div class="boxdata" >216</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mezokovesd Zsory Se</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -109 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Paksi Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >273</div>
								 <div class="boxdata" >+&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Zalaegerszeg Te</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >116</div>
								 <div class="boxdata" >-&frac14;<br /> ( -120 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ujpest Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +104 )</div>
								 <div class="boxdata" >380</div>
								 <div class="boxdata" >+&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fehervar Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -144 )</div>
								 <div class="boxdata" >104</div>
								 <div class="boxdata" >-&frac14;<br /> ( -140 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Diosgyori Vtk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kaposvari Rakoczi Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Sep 29</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Newcastle United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >462</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Leicester City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Freiburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >191</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fortuna Dusseldorf</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >152</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hertha Bsc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >220</div>
								 <div class="boxdata" >+&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1. Fc Cologne</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fotbal Club Fcsb</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >-108</div>
								 <div class="boxdata" >-&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acs Fc Academica Clinceni</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >388</div>
								 <div class="boxdata" >+&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cs Gaz Metan Medias</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -107 )</div>
								 <div class="boxdata" >350</div>
								 <div class="boxdata" >+&frac14;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Cfr 1907 Cluj</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -128 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac14;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Arsenal Tula</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >482</div>
								 <div class="boxdata" >+&frac12;<br /> ( -106 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Krasnodar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >-135</div>
								 <div class="boxdata" >-&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Ufa</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +102 )</div>
								 <div class="boxdata" >344</div>
								 <div class="boxdata" >+&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Rubin Kazan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -142 )</div>
								 <div class="boxdata" >117</div>
								 <div class="boxdata" >-&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cska Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >121</div>
								 <div class="boxdata" >-&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Ural Yekaterinburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -111 )</div>
								 <div class="boxdata" >291</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kisvarda Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >746</div>
								 <div class="boxdata" >+1<br /> ( -123)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ferencvarosi Tc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -133 )</div>
								 <div class="boxdata" >-227</div>
								 <div class="boxdata" >-1<br /> ( -111)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Orenburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >409</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Spartak Moscow</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >-114</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Sep 30</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Arsenal Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -121 )</div>
								 <div class="boxdata" >217</div>
								 <div class="boxdata" >+&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Manchester United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -112 )</div>
								 <div class="boxdata" >134</div>
								 <div class="boxdata" >-&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Republican Fc Akhmat Grozny</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >222</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pfc Sochi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >165</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Acs Sepsi Osk Sfantu Gheorghe</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -149)</div>
								 <div class="boxdata" >265</div>
								 <div class="boxdata" >+&frac14;<br /> ( -144 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Csm Politehnica Iasi</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +108)</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" >-&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Viitorul Constanta</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >225</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Universitatea Craiova 1948 Cs</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >160</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Oct 02</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ajax Amsterdam</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cf Valencia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sl Benfica</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >221</div>
								 <div class="boxdata" >+&frac14;<br /> ( -149 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Zenit St Petersburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >149</div>
								 <div class="boxdata" >-&frac14;<br /> ( +108 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Salzburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -136 )</div>
								 <div class="boxdata" >493</div>
								 <div class="boxdata" >+&frac34;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Liverpool Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -101 )</div>
								 <div class="boxdata" >-185</div>
								 <div class="boxdata" >-&frac34;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chelsea Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >134</div>
								 <div class="boxdata" >-&frac14;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lille Losc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >245</div>
								 <div class="boxdata" >+&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Olympique Lyonnais</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" >299</div>
								 <div class="boxdata" >+&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rb Leipzig</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -103 )</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >-&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Borussia Dortmund</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sk Slavia Prague</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >277</div>
								 <div class="boxdata" >+&frac14;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Inter Milano</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >408</div>
								 <div class="boxdata" >+&frac34;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Barcelona</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -113 )</div>
								 <div class="boxdata" >-149</div>
								 <div class="boxdata" >-&frac34;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ssc Napoli</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" >-&frac34;<br /> ( -108 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Krc Genk</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >436</div>
								 <div class="boxdata" >+&frac34;<br /> ( -126 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Oct 10</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Scotland</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -147)</div>
								 <div class="boxdata" >440</div>
								 <div class="boxdata" >+&frac12;<br /> ( -140 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Russia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +106)</div>
								 <div class="boxdata" >-101</div>
								 <div class="boxdata" >-&frac12;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Northern Ireland</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >761</div>
								 <div class="boxdata" >+1<br /> ( -136)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Netherlands</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >-212</div>
								 <div class="boxdata" >-1<br /> ( -101)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cyprus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -105 )</div>
								 <div class="boxdata" >256</div>
								 <div class="boxdata" >+&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kazakhstan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -131 )</div>
								 <div class="boxdata" >147</div>
								 <div class="boxdata" >-&frac14;<br /> ( +102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hungary</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >596</div>
								 <div class="boxdata" >+&frac34;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Croatia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >-161</div>
								 <div class="boxdata" >-&frac34;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Slovenia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( +108 )</div>
								 <div class="boxdata" >172</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >North Macedonia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -149 )</div>
								 <div class="boxdata" >227</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Poland</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -126 )</div>
								 <div class="boxdata" >-400</div>
								 <div class="boxdata" >-1&frac14;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Latvia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >2039</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -113 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Estonia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >509</div>
								 <div class="boxdata" >+&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Belarus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >-123</div>
								 <div class="boxdata" >-&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Israel</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -133 )</div>
								 <div class="boxdata" >409</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Austria</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -104 )</div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wales</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -142)</div>
								 <div class="boxdata" >242</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Slovakia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +104)</div>
								 <div class="boxdata" >168</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >San Marino</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Belgium</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Oct 11</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >France</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -126 )</div>
								 <div class="boxdata" >-119</div>
								 <div class="boxdata" >-&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Iceland</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -108 )</div>
								 <div class="boxdata" >434</div>
								 <div class="boxdata" >+&frac12;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Albania</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >566</div>
								 <div class="boxdata" >+&frac12;<br /> ( -108 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Turkey</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >-131</div>
								 <div class="boxdata" >-&frac12;<br /> ( -126 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Luxembourg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Portugal</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >England</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -136 )</div>
								 <div class="boxdata" >-136</div>
								 <div class="boxdata" >-&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Czech Republic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -101 )</div>
								 <div class="boxdata" >498</div>
								 <div class="boxdata" >+&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Lithuania</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >1737</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -101 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ukraine</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -116 )</div>
								 <div class="boxdata" >-434</div>
								 <div class="boxdata" >-1&frac14;<br /> ( -136 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Moldova</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >-107</div>
								 <div class="boxdata" >-&frac12;<br /> ( -105 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Andorra</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -140 )</div>
								 <div class="boxdata" >446</div>
								 <div class="boxdata" >+&frac12;<br /> ( -131 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bulgaria</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >235</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Montenegro</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >162</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sydney Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -138 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Adelaide United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( EV )</div>
								 <div class="boxdata" >209</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Oct 12</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Armenia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >-294</div>
								 <div class="boxdata" >-1&frac14;<br /> ( +104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Liechtenstein</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >1160</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -142 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Finland</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >298</div>
								 <div class="boxdata" >+&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bosnia & Herzegovina</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -117 )</div>
								 <div class="boxdata" >120</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Greece</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >600</div>
								 <div class="boxdata" >+&frac34;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Italy</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-172</div>
								 <div class="boxdata" >-&frac34;<br /> ( -119 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Switzerland</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -112 )</div>
								 <div class="boxdata" >224</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Denmark</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -123 )</div>
								 <div class="boxdata" >158</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Romania</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( EV )</div>
								 <div class="boxdata" >-178</div>
								 <div class="boxdata" >-&frac34;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Faroe Islands</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -138 )</div>
								 <div class="boxdata" >683</div>
								 <div class="boxdata" >+&frac34;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Spain</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >-140</div>
								 <div class="boxdata" >-&frac12;<br /> ( -136 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Norway</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >475</div>
								 <div class="boxdata" >+&frac12;<br /> ( -101 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ireland</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1<br /> ( -142)</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" >-&frac14;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Georgia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1<br /> ( +102)</div>
								 <div class="boxdata" >337</div>
								 <div class="boxdata" >+&frac14;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Central Coast Mariners Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >301</div>
								 <div class="boxdata" >+&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Western Sydney Wanderers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >-105</div>
								 <div class="boxdata" >-&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Melbourne City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Melbourne Victory</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sweden</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-416</div>
								 <div class="boxdata" >-1&frac14;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Malta</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -104 )</div>
								 <div class="boxdata" >2158</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -106 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Oct 13</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Russia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -121 )</div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" >-&frac12;<br /> ( -113 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cyprus</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -113 )</div>
								 <div class="boxdata" >429</div>
								 <div class="boxdata" >+&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brisbane Roar Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac34;<br /> ( -104 )</div>
								 <div class="boxdata" >293</div>
								 <div class="boxdata" >+&frac12;<br /> ( -135 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Perth Glory Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac34;<br /> ( -131 )</div>
								 <div class="boxdata" >-104</div>
								 <div class="boxdata" >-&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Western United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >188</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Wellington Phoenix Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac12;<br /> ( -117 )</div>
								 <div class="boxdata" >158</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--soccer 2nd Halves  - Nov 16</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Belgium</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o1&frac14;<br /> ( -135 )</div>
								 <div class="boxdata" >112</div>
								 <div class="boxdata" >-&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Russia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u1&frac14;<br /> ( -102 )</div>
								 <div class="boxdata" >298</div>
								 <div class="boxdata" >+&frac14;<br /> ( -109 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	