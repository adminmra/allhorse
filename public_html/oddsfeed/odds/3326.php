	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--bk - Israel (super League)">
			<caption>Live--bk - Israel (super League)</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 20, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--bk - Israel (super League)  - May 20</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hapoel Unet Holon</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o208&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >300</div>
								 <div class="boxdata" >+9&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Maccabi Ashdod</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u208&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-714</div>
								 <div class="boxdata" >-9&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hapoel Beer Sheva</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o173&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-27&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bnei Herzelia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u173&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" >+27&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hapoel Jerusalem</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o188&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-133</div>
								 <div class="boxdata" >-&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hapoel Tel-aviv</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u188&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-133</div>
								 <div class="boxdata" >+&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hapoel Eilat</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o158&frac12;<br /> ( -133 )</div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Maccabi Rishon</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u158&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-111</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -125 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Maccabi Tel-aviv</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ironi Naharia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	