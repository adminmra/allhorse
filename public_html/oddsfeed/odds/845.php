	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Wnba - Championship - To Win">
			<caption>Wnba - Championship - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 17, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Wnba - Championship - To Win  - Sep 17					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Wnba Championship - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Washington Mystics</td><td>EV</td><td>EV</td></tr><tr><td>Connecticut Sun</td><td>33/10</td><td>+330</td></tr><tr><td>Las Vegas Aces</td><td>7/2</td><td>+350</td></tr><tr><td>Los Angeles Sparks</td><td>4/1</td><td>+400</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	