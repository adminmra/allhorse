	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Pat Day Mile - To Win">
			<caption>Horses - Pat Day Mile - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 4, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Pat Day Mile - To Win  - May 04					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Pat Day Mile - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Durkin's Call</td><td>22/1</td><td>+2200</td></tr><tr><td>Market King</td><td>16/1</td><td>+1600</td></tr><tr><td>Dream Maker</td><td>12/1</td><td>+1200</td></tr><tr><td>Mr Money Bags</td><td>14/1</td><td>+1400</td></tr><tr><td>Dunph</td><td>25/1</td><td>+2500</td></tr><tr><td>Tobacco Road</td><td>14/1</td><td>+1400</td></tr><tr><td>Mr. Money</td><td>25/1</td><td>+2500</td></tr><tr><td>Manny Wah</td><td>14/1</td><td>+1400</td></tr><tr><td>Captain Von Trapp</td><td>20/1</td><td>+2000</td></tr><tr><td>Instagrand</td><td>5/7</td><td>-140</td></tr><tr><td>Frolic More</td><td>25/1</td><td>+2500</td></tr><tr><td>Everfast</td><td>25/1</td><td>+2500</td></tr><tr><td>Hog Creek Hustle</td><td>14/1</td><td>+1400</td></tr><tr><td>Last Judgment</td><td>7/1</td><td>+700</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	