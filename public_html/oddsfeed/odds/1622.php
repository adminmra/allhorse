	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Live--german Bundesliga">
			<caption>Live--german Bundesliga</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 11, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Live--german Bundesliga  - May 11</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Werder Bremen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -166 )</div>
								 <div class="boxdata" >425</div>
								 <div class="boxdata" >+1<br /> ( -105)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1899 Hoffenheim</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( +125 )</div>
								 <div class="boxdata" >-222</div>
								 <div class="boxdata" >-1<br /> ( -133)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Schalke 04</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +104 )</div>
								 <div class="boxdata" >650</div>
								 <div class="boxdata" >+1&frac14;<br /> ( -125 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bayer Leverkusen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -142 )</div>
								 <div class="boxdata" >-285</div>
								 <div class="boxdata" >-1&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bayern Munich</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" >-&frac34;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rb Leipzig</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >325</div>
								 <div class="boxdata" >+&frac34;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vfl Wolfsburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +129 )</div>
								 <div class="boxdata" >129</div>
								 <div class="boxdata" >-&frac14;<br /> ( EV )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Vfb Stuttgart</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -181 )</div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" >+&frac14;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fortuna Dusseldorf</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Borussia Dortmund</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hertha Bsc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( +104 )</div>
								 <div class="boxdata" >475</div>
								 <div class="boxdata" >+1<br /> ( -125)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Fc Augsburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -153 )</div>
								 <div class="boxdata" >-200</div>
								 <div class="boxdata" >-1<br /> ( -111)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sc Freiburg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Hannover 96</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Monchengladbach</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( +120 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >1 Fc Nuremberg</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -166 )</div>
								 <div class="boxdata" >280</div>
								 <div class="boxdata" >+&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	