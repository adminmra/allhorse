	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Cfb - Ncaa Football Conference - To Win">
			<caption>Cfb - Ncaa Football Conference - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Cfb - Ncaa Football Conference - To Win  - Aug 24					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Cfb Atlantic Coast Conference - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Clemson</td><td>1/3</td><td>-300</td></tr><tr><td>Miami Fl</td><td>8/1</td><td>+800</td></tr><tr><td>Florida State</td><td>14/1</td><td>+1400</td></tr><tr><td>Syracuse</td><td>16/1</td><td>+1600</td></tr><tr><td>North Carolina State</td><td>20/1</td><td>+2000</td></tr><tr><td>Virginia Tech</td><td>20/1</td><td>+2000</td></tr><tr><td>Virginia</td><td>25/1</td><td>+2500</td></tr><tr><td>Boston College</td><td>33/1</td><td>+3300</td></tr><tr><td>Georgia Tech</td><td>33/1</td><td>+3300</td></tr><tr><td>Pittsburgh U</td><td>40/1</td><td>+4000</td></tr><tr><td>North Carolina</td><td>66/1</td><td>+6600</td></tr><tr><td>Duke</td><td>100/1</td><td>+10000</td></tr><tr><td>Louisville</td><td>100/1</td><td>+10000</td></tr><tr><td>Wake Forest</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Cfb Big 10 Conference - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Ohio State</td><td>11/10</td><td>+110</td></tr><tr><td>Michigan</td><td>5/2</td><td>+250</td></tr><tr><td>Penn State</td><td>7/1</td><td>+700</td></tr><tr><td>Michigan State</td><td>10/1</td><td>+1000</td></tr><tr><td>Nebraska</td><td>15/1</td><td>+1500</td></tr><tr><td>Wisconsin</td><td>16/1</td><td>+1600</td></tr><tr><td>Northwestern</td><td>20/1</td><td>+2000</td></tr><tr><td>Iowa</td><td>22/1</td><td>+2200</td></tr><tr><td>Purdue</td><td>25/1</td><td>+2500</td></tr><tr><td>Maryland</td><td>33/1</td><td>+3300</td></tr><tr><td>Minnesota</td><td>66/1</td><td>+6600</td></tr><tr><td>Indiana</td><td>80/1</td><td>+8000</td></tr><tr><td>Illinois</td><td>100/1</td><td>+10000</td></tr><tr><td>Rutgers</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Cfb Big 12 Conference - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Oklahoma</td><td>4/5</td><td>-125</td></tr><tr><td>Texas</td><td>5/2</td><td>+250</td></tr><tr><td>West Virginia</td><td>10/1</td><td>+1000</td></tr><tr><td>Iowa State</td><td>11/1</td><td>+1100</td></tr><tr><td>Oklahoma State</td><td>16/1</td><td>+1600</td></tr><tr><td>Tcu</td><td>18/1</td><td>+1800</td></tr><tr><td>Texas Tech</td><td>20/1</td><td>+2000</td></tr><tr><td>Baylor</td><td>25/1</td><td>+2500</td></tr><tr><td>Kansas State</td><td>40/1</td><td>+4000</td></tr><tr><td>Kansas</td><td>50/1</td><td>+5000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Cfb Pacific 12 Conference - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Washington</td><td>9/5</td><td>+180</td></tr><tr><td>Oregon</td><td>2/1</td><td>+200</td></tr><tr><td>Washington State</td><td>7/1</td><td>+700</td></tr><tr><td>Usc</td><td>8/1</td><td>+800</td></tr><tr><td>Utah</td><td>7/1</td><td>+700</td></tr><tr><td>Stanford</td><td>16/1</td><td>+1600</td></tr><tr><td>California</td><td>25/1</td><td>+2500</td></tr><tr><td>Arizona State</td><td>25/1</td><td>+2500</td></tr><tr><td>Arizona</td><td>28/1</td><td>+2800</td></tr><tr><td>Ucla</td><td>33/1</td><td>+3300</td></tr><tr><td>Colorado</td><td>66/1</td><td>+6600</td></tr><tr><td>Oregon State</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Cfb Southeastern Conference - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Alabama</td><td>2/3</td><td>-150</td></tr><tr><td>Georgia</td><td>27/10</td><td>+270</td></tr><tr><td>Florida</td><td>12/1</td><td>+1200</td></tr><tr><td>Lsu</td><td>12/1</td><td>+1200</td></tr><tr><td>Mississippi State</td><td>22/1</td><td>+2200</td></tr><tr><td>Auburn</td><td>25/1</td><td>+2500</td></tr><tr><td>Kentucky</td><td>25/1</td><td>+2500</td></tr><tr><td>Texas A&m</td><td>25/1</td><td>+2500</td></tr><tr><td>Missouri</td><td>33/1</td><td>+3300</td></tr><tr><td>South Carolina</td><td>66/1</td><td>+6600</td></tr><tr><td>Tennessee</td><td>66/1</td><td>+6600</td></tr><tr><td>Mississippi</td><td>100/1</td><td>+10000</td></tr><tr><td>Vanderbilt</td><td>125/1</td><td>+12500</td></tr><tr><td>Arkansas</td><td>250/1</td><td>+25000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');</script>
{/literal}	
	