	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Golf - Pga Tour - 72 Hole Match Betting">
			<caption>Golf - Pga Tour - 72 Hole Match Betting</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Golf - Pga Tour - 72 Hole Match Betting  - Sep 19					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Brunson Burgoon</td><td>5/6</td><td>-120</td></tr><tr><td>Grayson Murray</td><td>EV</td><td>EV</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Wyndham Clark</td><td>10/11</td><td>-110</td></tr><tr><td>Dylan Fritelli</td><td>10/11</td><td>-110</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Lucas Glover</td><td>EV</td><td>EV</td></tr><tr><td>Corey Conners</td><td>5/6</td><td>-120</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Si Woo Kim</td><td>EV</td><td>EV</td></tr><tr><td>Emiliano Grillo</td><td>5/6</td><td>-120</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Joaquin Niemann</td><td>5/6</td><td>-120</td></tr><tr><td>Sungjae Im</td><td>EV</td><td>EV</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Jt Poston</td><td>10/11</td><td>-110</td></tr><tr><td>Vaugh  Taylor</td><td>10/11</td><td>-110</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Cameron Smith</td><td>10/11</td><td>-110</td></tr><tr><td>Brian Harman</td><td>10/11</td><td>-110</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Brandt Snedeker</td><td>5/6</td><td>-120</td></tr><tr><td>Byeong-hun An</td><td>EV</td><td>EV</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o8t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">72 Hole Match Betting</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Aaron Wise</td><td>10/11</td><td>-110</td></tr><tr><td>Russell Henley</td><td>10/11</td><td>-110</td></tr><tr><td>Draw</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');addSort('o6t');addSort('o7t');addSort('o8t');</script>
{/literal}	
	