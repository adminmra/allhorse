	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Big3 Basketball Championship - To Win">
			<caption>Big3 Basketball Championship - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 27, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Big3 Basketball Championship - To Win  - Jun 29					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Big3 Championship - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Power</td><td>5/1</td><td>+500</td></tr><tr><td>Triplets</td><td>7/1</td><td>+700</td></tr><tr><td>Enemies</td><td>9/1</td><td>+900</td></tr><tr><td>3 Headed Monsters</td><td>6/1</td><td>+600</td></tr><tr><td>Aliens</td><td>10/1</td><td>+1000</td></tr><tr><td>Trilogy</td><td>11/1</td><td>+1100</td></tr><tr><td>3`s Company</td><td>12/1</td><td>+1200</td></tr><tr><td>Tri State</td><td>17/2</td><td>+850</td></tr><tr><td>Kiler 3s</td><td>9/1</td><td>+900</td></tr><tr><td>Bivouac</td><td>15/2</td><td>+750</td></tr><tr><td>Ghost Ballers</td><td>9/1</td><td>+900</td></tr><tr><td>Ball Hogs</td><td>14/1</td><td>+1400</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	