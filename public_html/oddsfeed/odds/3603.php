	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Cricket - Player Props">
			<caption>Cricket - Player Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 27, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Cricket - Player Props  - Jun 28					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Top Team Batsman - South Africa</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Q De Kock</td><td>5/2</td><td>+250</td></tr><tr><td>Hm Amla</td><td>3/1</td><td>+300</td></tr><tr><td>F Du Plessis</td><td>3/1</td><td>+300</td></tr><tr><td>Ak Markram</td><td>4/1</td><td>+400</td></tr><tr><td>He Van Der Dussen</td><td>5/1</td><td>+500</td></tr><tr><td>Da Miller</td><td>13/2</td><td>+650</td></tr><tr><td>Jp Duminy</td><td>9/1</td><td>+900</td></tr><tr><td>Al Phehlukwayo</td><td>15/1</td><td>+1500</td></tr><tr><td>D Petrorius</td><td>15/1</td><td>+1500</td></tr><tr><td>Ch Morris</td><td>21/1</td><td>+2100</td></tr><tr><td>K Rabada</td><td>75/1</td><td>+7500</td></tr><tr><td>Be Hendricks</td><td>140/1</td><td>+14000</td></tr><tr><td>I Tahir</td><td>190/1</td><td>+19000</td></tr><tr><td>L Ngidi</td><td>240/1</td><td>+24000</td></tr><tr><td>T Shamsi</td><td>290/1</td><td>+29000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Top Team Batsman - Sri Lanka</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Fdm Karunaratne</td><td>3/1</td><td>+300</td></tr><tr><td>Kusal Mendis</td><td>3/1</td><td>+300</td></tr><tr><td>Avishka Fernando</td><td>7/2</td><td>+350</td></tr><tr><td>Kusal Perera</td><td>7/2</td><td>+350</td></tr><tr><td>A Mathews</td><td>4/1</td><td>+400</td></tr><tr><td>Hdrl Thirimanne</td><td>9/2</td><td>+450</td></tr><tr><td>Thisara Perera</td><td>8/1</td><td>+800</td></tr><tr><td>Tam Siriwardana</td><td>11/1</td><td>+1100</td></tr><tr><td>Dm De Silva</td><td>13/1</td><td>+1300</td></tr><tr><td>Jeevan Mendis</td><td>21/1</td><td>+2100</td></tr><tr><td>I Udana</td><td>24/1</td><td>+2400</td></tr><tr><td>Jdf Vandersay</td><td>90/1</td><td>+9000</td></tr><tr><td>Ras Lakmal</td><td>90/1</td><td>+9000</td></tr><tr><td>Sl Malinga</td><td>140/1</td><td>+14000</td></tr><tr><td>Nuwan Pradeep</td><td>190/1</td><td>+19000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Top Team Bowler - South Africa</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>K Rabada</td><td>5/2</td><td>+250</td></tr><tr><td>I Tahir</td><td>14/5</td><td>+280</td></tr><tr><td>L Ngidi</td><td>3/1</td><td>+300</td></tr><tr><td>T Shamsi</td><td>7/2</td><td>+350</td></tr><tr><td>Ch Morris</td><td>7/2</td><td>+350</td></tr><tr><td>Be Hendricks</td><td>4/1</td><td>+400</td></tr><tr><td>Al Phehlukwayo</td><td>4/1</td><td>+400</td></tr><tr><td>D Petrorius</td><td>5/1</td><td>+500</td></tr><tr><td>Jp Duminy</td><td>7/1</td><td>+700</td></tr><tr><td>Ak Markram</td><td>11/1</td><td>+1100</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Top Team Bowler - Sri Lanka</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Dm De Silva</td><td>5/1</td><td>+500</td></tr><tr><td>Ras Lakmal</td><td>3/1</td><td>+300</td></tr><tr><td>Sl Malinga</td><td>5/2</td><td>+250</td></tr><tr><td>A Mathews</td><td>9/1</td><td>+900</td></tr><tr><td>Jeevan Mendis</td><td>4/1</td><td>+400</td></tr><tr><td>Thisara Perera</td><td>4/1</td><td>+400</td></tr><tr><td>Nuwan Pradeep</td><td>4/1</td><td>+400</td></tr><tr><td>Tam Siriwardana</td><td>5/1</td><td>+500</td></tr><tr><td>I Udana</td><td>4/1</td><td>+400</td></tr><tr><td>Jdf Vandersay</td><td>4/1</td><td>+400</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');</script>
{/literal}	
	