	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--thailand Premier League">
			<caption>Pre--thailand Premier League</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 21, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--thailand Premier League  - Jun 22</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Chiangmai Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >1607</div>
								 <div class="boxdata" >+2&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Buriram United Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >-909</div>
								 <div class="boxdata" >-2&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nakhon Ratchasima Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -111 )</div>
								 <div class="boxdata" >194</div>
								 <div class="boxdata" >+&frac14;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sukhothai Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -119 )</div>
								 <div class="boxdata" >123</div>
								 <div class="boxdata" >-&frac14;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Trat Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >110</div>
								 <div class="boxdata" >-&frac14;<br /> ( -119 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Muang Thong United</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -119 )</div>
								 <div class="boxdata" >200</div>
								 <div class="boxdata" >+&frac14;<br /> ( -111 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Prachuap Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -123 )</div>
								 <div class="boxdata" >261</div>
								 <div class="boxdata" >+&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ratchaburi Mitr Phol Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -107 )</div>
								 <div class="boxdata" >-116</div>
								 <div class="boxdata" >-&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	