	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Aussie Rules - Afl Grand Final Winner">
			<caption>Aussie Rules - Afl Grand Final Winner</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Aussie Rules - Afl Grand Final Winner  - Sep 20					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Afl Grand Final - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Collingwood</td><td>9/4</td><td>+225</td></tr><tr><td>Geelong</td><td>9/2</td><td>+450</td></tr><tr><td>Greater Western Sydney</td><td>6/1</td><td>+600</td></tr><tr><td>Richmond</td><td>5/4</td><td>+125</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	