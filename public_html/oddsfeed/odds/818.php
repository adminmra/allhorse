	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Usa Mls - To Win">
			<caption>Soccer - Usa Mls - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Usa Mls - To Win  - Sep 18					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2019 Usa Mls Cup  - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Los Angeles Fc</td><td>3/2</td><td>+150</td></tr><tr><td>La Galaxy</td><td>22/1</td><td>+2200</td></tr><tr><td>Atlanta United</td><td>12/1</td><td>+1200</td></tr><tr><td>New York City Fc</td><td>5/1</td><td>+500</td></tr><tr><td>Dc United</td><td>25/1</td><td>+2500</td></tr><tr><td>Philadelphia Union</td><td>12/1</td><td>+1200</td></tr><tr><td>New York Red Bulls</td><td>22/1</td><td>+2200</td></tr><tr><td>Portland Timbers</td><td>33/1</td><td>+3300</td></tr><tr><td>Fc Dallas</td><td>40/1</td><td>+4000</td></tr><tr><td>Seattle Sounders</td><td>14/1</td><td>+1400</td></tr><tr><td>Montreal Impact</td><td>200/1</td><td>+20000</td></tr><tr><td>Houston Dynamo</td><td>2500/1</td><td>+250000</td></tr><tr><td>Toronto Fc</td><td>20/1</td><td>+2000</td></tr><tr><td>Columbus Crew</td><td>1000/1</td><td>+100000</td></tr><tr><td>Minnesota United</td><td>22/1</td><td>+2200</td></tr><tr><td>Sporting Kansas City</td><td>250/1</td><td>+25000</td></tr><tr><td>Orlando City</td><td>250/1</td><td>+25000</td></tr><tr><td>San Jose Earthquakes</td><td>20/1</td><td>+2000</td></tr><tr><td>Chicago Fire</td><td>150/1</td><td>+15000</td></tr><tr><td>Real Salt Lake</td><td>33/1</td><td>+3300</td></tr><tr><td>New England Revolution</td><td>18/1</td><td>+1800</td></tr><tr><td>Colorado Rapids</td><td>1000/1</td><td>+100000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	