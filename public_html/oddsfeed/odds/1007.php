	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Nfl - Super Bowl Halftime">
			<caption>Nfl - Super Bowl Halftime</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 3, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Nfl - Super Bowl Halftime  - Feb 03					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">What Song Will Maroon 5 Open Halftime With</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>One More Night</td><td>9/1</td><td>+900</td></tr><tr><td>Makes Me Wonder</td><td>3/1</td><td>+300</td></tr><tr><td>Animals</td><td>10/1</td><td>+1000</td></tr><tr><td>Don’t Wanna Know</td><td>12/1</td><td>+1200</td></tr><tr><td>Girls Like You</td><td>10/1</td><td>+1000</td></tr><tr><td>Moves Like Jagger</td><td>10/1</td><td>+1000</td></tr><tr><td>Sugar</td><td>10/1</td><td>+1000</td></tr><tr><td>Payphone</td><td>20/1</td><td>+2000</td></tr><tr><td>Maps</td><td>22/1</td><td>+2200</td></tr><tr><td>Mic Jack</td><td>25/1</td><td>+2500</td></tr><tr><td>Any Other Song</td><td>1/7</td><td>-700</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Adam Levine Wear A Leather Jacket?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>5/7</td><td>-140</td></tr><tr><td>No</td><td>EV</td><td>EV</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Andre 3000 Join Big Boi On Stage?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>EV</td><td>EV</td></tr><tr><td>No</td><td>5/7</td><td>-140</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Big Boi Wear A Falcons Logo On Stage?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>7/5</td><td>+140</td></tr><tr><td>No</td><td>1/2</td><td>-200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Big Boi Be Wearing A Gold Chain?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>1/7</td><td>-700</td></tr><tr><td>No</td><td>4/1</td><td>+400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Kylie Jenner Appear On Stage?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>4/1</td><td>+400</td></tr><tr><td>No</td><td>1/10</td><td>-1000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Travis Scott Mention Kaepernick?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>5/1</td><td>+500</td></tr><tr><td>No</td><td>1/10</td><td>-1000</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Donald Trump Be Mentioned During Halftime?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>3/1</td><td>+300</td></tr><tr><td>No</td><td>1/5</td><td>-500</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o8t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Any Performer Fall Down?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>7/1</td><td>+700</td></tr><tr><td>No</td><td>1/20</td><td>-2000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');addSort('o2t');addSort('o3t');addSort('o4t');addSort('o5t');addSort('o6t');addSort('o7t');addSort('o8t');</script>
{/literal}	
	