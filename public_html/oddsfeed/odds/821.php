	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Mlb - Team Total Runs">
			<caption>Mlb - Team Total Runs</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Mlb - Team Total Runs  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nationals Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Nationals Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cardinals Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -120)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cardinals Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -110)</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mets Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o6&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mets Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u6&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rockies Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rockies Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Marlins Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Marlins Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dbacks Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dbacks Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Phillies Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Phillies Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Braves Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Braves Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Padres Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Padres Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brewers Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brewers Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Royals Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Royals Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Athletics Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Athletics Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Angels Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Angels Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yankees Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -140 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yankees Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( +110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Blue Jays Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Blue Jays Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Orioles Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Orioles Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tigers Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Tigers Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Indians Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Indians Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rangers Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rangers Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Astros Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Astros Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mariners Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mariners Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pirates Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Pirates Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Giants Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Giants Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Red Sox Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o5&frac12;<br /> ( +105 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Red Sox Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u5&frac12;<br /> ( -135 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rays Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac12;<br /> ( -145 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rays Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac12;<br /> ( +115 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dodgers Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o4&frac12;<br /> ( EV )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Dodgers Team Total</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u4&frac12;<br /> ( -130 )</div>
								 <div class="boxdata" ></div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	