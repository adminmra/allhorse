	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Racing - Rally - Race Series">
			<caption>Racing - Rally - Race Series</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 13, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Racing - Rally - Race Series  - Jun 13					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Rally Italia 2019 - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Ott Tanak</td><td>27/20</td><td>+135</td></tr><tr><td>Thierry Neuville</td><td>5/2</td><td>+250</td></tr><tr><td>Sebastien Ogier</td><td>13/2</td><td>+650</td></tr><tr><td>Kris Meeke</td><td>7/1</td><td>+700</td></tr><tr><td>Elfyn Evans</td><td>10/1</td><td>+1000</td></tr><tr><td>Dani Sordo</td><td>12/1</td><td>+1200</td></tr><tr><td>Jari-matti Latvala</td><td>14/1</td><td>+1400</td></tr><tr><td>Andreas Mikkelsen</td><td>18/1</td><td>+1800</td></tr><tr><td>Esapekka Lappi</td><td>20/1</td><td>+2000</td></tr><tr><td>Teemu Suninen</td><td>25/1</td><td>+2500</td></tr><tr><td>Juho Hanninen</td><td>200/1</td><td>+20000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	