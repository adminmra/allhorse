	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Breeders Cup Juvenile - To Win">
			<caption>Horses - Breeders Cup Juvenile - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Breeders Cup Juvenile - To Win  - Nov 02					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Breeders Cup Juvenile - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Dennis Moment</td><td>7/2</td><td>+350</td></tr><tr><td>Eight Rings</td><td>13/2</td><td>+650</td></tr><tr><td>Green Light Go</td><td>7/1</td><td>+700</td></tr><tr><td>Basin</td><td>8/1</td><td>+800</td></tr><tr><td>Gouverneur Morris</td><td>10/1</td><td>+1000</td></tr><tr><td>Nucky</td><td>14/1</td><td>+1400</td></tr><tr><td>By Your Side</td><td>16/1</td><td>+1600</td></tr><tr><td>Shoplifted</td><td>16/1</td><td>+1600</td></tr><tr><td>Storm The Court</td><td>20/1</td><td>+2000</td></tr><tr><td>Gozilla</td><td>25/1</td><td>+2500</td></tr><tr><td>Garth</td><td>33/1</td><td>+3300</td></tr><tr><td>Flap Jack</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	