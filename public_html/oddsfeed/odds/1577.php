	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Special Props - Current Events">
			<caption>Special Props - Current Events</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Special Props - Current Events  - Dec 31					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">First Organization To Send Humans To Mars?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Space X </td><td>1/3</td><td>-300</td></tr><tr><td>Blue Origin</td><td>4/1</td><td>+400</td></tr><tr><td>Boeing</td><td>5/1</td><td>+500</td></tr><tr><td>Space Force</td><td>20/1</td><td>+2000</td></tr><tr><td>Russia</td><td>45/1</td><td>+4500</td></tr><tr><td>Nasa</td><td>60/1</td><td>+6000</td></tr><tr><td>China</td><td>85/1</td><td>+8500</td></tr><tr><td>United Arab Emirates</td><td>200/1</td><td>+20000</td></tr><tr><td>Field</td><td>125/1</td><td>+12500</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	