	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Aaf">
			<caption>Aaf</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 31, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Aaf  - Mar 31</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Atlanta Legends</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o38&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >190</div>
								 <div class="boxdata" >+6<br /> ( -110)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Birmingham Iron</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u38&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-235</div>
								 <div class="boxdata" >-6<br /> ( -110)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Arizona Hotshots</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o43&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >105</div>
								 <div class="boxdata" >+1&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >San Antonio Commanders</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u43&frac12;<br /> ( -110 )</div>
								 <div class="boxdata" >-125</div>
								 <div class="boxdata" >-1&frac12;<br /> ( -110 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	