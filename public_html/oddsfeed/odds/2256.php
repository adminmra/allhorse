	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--scotland League Two">
			<caption>Pre--scotland League Two</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--scotland League Two  - Sep 21</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stirling Albion</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -111 )</div>
								 <div class="boxdata" >297</div>
								 <div class="boxdata" >+&frac12;<br /> ( -104 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Annan Athletic</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -125 )</div>
								 <div class="boxdata" >-133</div>
								 <div class="boxdata" >-&frac12;<br /> ( -133 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Brechin City Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -128 )</div>
								 <div class="boxdata" >262</div>
								 <div class="boxdata" >+&frac12;<br /> ( -120 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Stenhousemuir Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -107 )</div>
								 <div class="boxdata" >-114</div>
								 <div class="boxdata" >-&frac12;<br /> ( -114 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cove Rangers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac14;<br /> ( -116 )</div>
								 <div class="boxdata" >-172</div>
								 <div class="boxdata" >-1<br /> ( -107)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Elgin City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac14;<br /> ( -119 )</div>
								 <div class="boxdata" >346</div>
								 <div class="boxdata" >+1<br /> ( -128)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Edinburgh City</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -121)</div>
								 <div class="boxdata" >-111</div>
								 <div class="boxdata" >-&frac12;<br /> ( -112 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Albion Rovers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -113)</div>
								 <div class="boxdata" >235</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cowdenbeath Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >272</div>
								 <div class="boxdata" >+&frac12;<br /> ( -116 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Queens Park Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >-117</div>
								 <div class="boxdata" >-&frac12;<br /> ( -117 )</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	