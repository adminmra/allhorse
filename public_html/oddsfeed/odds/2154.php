	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--uruguay Primera Division">
			<caption>Pre--uruguay Primera Division</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--uruguay Primera Division  - Sep 18</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Progreso</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >252</div>
								 <div class="boxdata" >+&frac12;<br /> ( -128 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Juventud De Lp</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -105 )</div>
								 <div class="boxdata" >100</div>
								 <div class="boxdata" >-&frac12;<br /> ( -103 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Defensor Sporting</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >204</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Cerro Largo Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >125</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca River Plate (uru)</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Racing Club Mdeo.</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -117 )</div>
								 <div class="boxdata" >156</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Penarol Uru</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -125 )</div>
								 <div class="boxdata" >-153</div>
								 <div class="boxdata" >-1<br /> ( +108)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Rampla Juniors Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -106 )</div>
								 <div class="boxdata" >394</div>
								 <div class="boxdata" >+1<br /> ( -144)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
                <tr><th colspan="3"  class="datahead">Pre--uruguay Primera Division  - Sep 19</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Fenix</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -117)</div>
								 <div class="boxdata" >235</div>
								 <div class="boxdata" >+&frac12;<br /> ( -129 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Liverpool Montevideo</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -112)</div>
								 <div class="boxdata" >102</div>
								 <div class="boxdata" >-&frac12;<br /> ( -102 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Cerro</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -113 )</div>
								 <div class="boxdata" >168</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Ca Boston River</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -116 )</div>
								 <div class="boxdata" >150</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Plaza Colonia Cd</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >272</div>
								 <div class="boxdata" >+&frac12;<br /> ( -123 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Danubio Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -114 )</div>
								 <div class="boxdata" >-104</div>
								 <div class="boxdata" >-&frac12;<br /> ( -107 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Club Nacional De Football</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -108 )</div>
								 <div class="boxdata" >-166</div>
								 <div class="boxdata" >-1<br /> ( -101)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Mdeo. Wanderers Fc</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -121 )</div>
								 <div class="boxdata" >411</div>
								 <div class="boxdata" >+1<br /> ( -131)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	