	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Cfb - Ncaa Fb Specials">
			<caption>Cfb - Ncaa Fb Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated July 8, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Cfb - Ncaa Fb Specials  - Aug 24					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">When Will Nick Saban Retire?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 5.5 More Seasons</td><td>EV</td><td>EV</td></tr><tr><td>Under 5.5 More Seasons</td><td>10/13</td><td>-130</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">When Will Alabama Head Coach Nick Saban Retire?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>After The 2019 Season</td><td>7/1</td><td>+700</td></tr><tr><td>After 2020 Or 2021 Season</td><td>7/2</td><td>+350</td></tr><tr><td>After 2022 Or 2023 Season</td><td>3/1</td><td>+300</td></tr><tr><td>After 2024</td><td>3/1</td><td>+300</td></tr><tr><td>After 2025</td><td>7/4</td><td>+175</td></tr><tr><td>2026 Or Beyond</td><td>13/2</td><td>+650</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>
{/literal}	
	