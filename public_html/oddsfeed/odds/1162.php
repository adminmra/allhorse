	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Chile Primera Division">
			<caption>Chile Primera Division</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated November 30, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="4">Chile Primera Division  - Dec 02</th></tr>
       <tr  colspan="4"><td><div class="row headerrow"><div class="col-sm-6">Team</div><div class="col-sm-2">Spread</div><div class="col-sm-2">Moneyline</div><div class="col-sm-2">Total</div></div></td></tr>
         <tr><td colspan="4">
         <div class="row earchrow"><div class="col-sm-6 teams">Huachipato<br /> - vs - <br /> Palestino</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+238</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-120 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+105</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;EV</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Antofagasta<br /> - vs - <br /> Deportes Iquique</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (100)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+167</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-115 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">0<br /> (-120)</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+147</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-105</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">U Catolica<br /> - vs - <br /> Deportes Temuco</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+163</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;EV </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+153</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-120</button></div></div></div></div><div class="row earchrow"><div class="col-sm-6 teams">Univ. Concepcion<br /> - vs - <br /> Colo Colo</div><div class="col-sm-6"><div class="firstpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+198</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">o2&frac12;-105 </button></div></div><div class="clear hr"></div><div class="secondpart"><div class="col-sm-4"><button type="button" class="btn btn-default spmoto"><br /> ()</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">+131</button></div><div class="col-sm-4"><button type="button" class="btn btn-default spmoto">u2&frac12;-115</button></div></div></div></div>                  
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>

	