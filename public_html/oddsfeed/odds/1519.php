	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Soccer - Fifa World Cup To Win">
			<caption>Soccer - Fifa World Cup To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 18, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Soccer - Fifa World Cup To Win  - Nov 21					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2022 World Cup - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>France</td><td>7/1</td><td>+700</td></tr><tr><td>Brazil</td><td>6/1</td><td>+600</td></tr><tr><td>Germany</td><td>7/1</td><td>+700</td></tr><tr><td>Spain</td><td>7/1</td><td>+700</td></tr><tr><td>Belgium</td><td>12/1</td><td>+1200</td></tr><tr><td>Argentina</td><td>10/1</td><td>+1000</td></tr><tr><td>Italy</td><td>16/1</td><td>+1600</td></tr><tr><td>England</td><td>12/1</td><td>+1200</td></tr><tr><td>Netherlands</td><td>16/1</td><td>+1600</td></tr><tr><td>Croatia</td><td>40/1</td><td>+4000</td></tr><tr><td>Portugal</td><td>16/1</td><td>+1600</td></tr><tr><td>Uruguay</td><td>40/1</td><td>+4000</td></tr><tr><td>Chile</td><td>50/1</td><td>+5000</td></tr><tr><td>Colombia</td><td>40/1</td><td>+4000</td></tr><tr><td>Usa</td><td>100/1</td><td>+10000</td></tr><tr><td>Mexico </td><td>66/1</td><td>+6600</td></tr><tr><td>Russia </td><td>150/1</td><td>+15000</td></tr><tr><td>Turkey </td><td>150/1</td><td>+15000</td></tr><tr><td>Switzerland </td><td>150/1</td><td>+15000</td></tr><tr><td>Poland   </td><td>150/1</td><td>+15000</td></tr><tr><td>Sweden </td><td>100/1</td><td>+10000</td></tr><tr><td>Denmark </td><td>100/1</td><td>+10000</td></tr><tr><td>Ghana  </td><td>200/1</td><td>+20000</td></tr><tr><td>Ivory Coast </td><td>200/1</td><td>+20000</td></tr><tr><td>Egypt </td><td>250/1</td><td>+25000</td></tr><tr><td>Iceland  </td><td>250/1</td><td>+25000</td></tr><tr><td>Japan</td><td>250/1</td><td>+25000</td></tr><tr><td>Nigeria</td><td>200/1</td><td>+20000</td></tr><tr><td>Paraguay</td><td>100/1</td><td>+10000</td></tr><tr><td>Serbia</td><td>100/1</td><td>+10000</td></tr><tr><td>Wales </td><td>150/1</td><td>+15000</td></tr><tr><td>Peru</td><td>250/1</td><td>+25000</td></tr><tr><td>Qatar</td><td>150/1</td><td>+15000</td></tr><tr><td>Ireland</td><td>250/1</td><td>+25000</td></tr><tr><td>Senegal</td><td>200/1</td><td>+20000</td></tr><tr><td>Morocco</td><td>200/1</td><td>+20000</td></tr><tr><td>Canada</td><td>2000/1</td><td>+200000</td></tr><tr><td>Australia</td><td>250/1</td><td>+25000</td></tr><tr><td>Iran </td><td>500/1</td><td>+50000</td></tr><tr><td>N. Ireland</td><td>750/1</td><td>+75000</td></tr><tr><td>Scotland </td><td>500/1</td><td>+50000</td></tr><tr><td>Tunisia  </td><td>250/1</td><td>+25000</td></tr><tr><td>Costa Rica </td><td>500/1</td><td>+50000</td></tr><tr><td>Panama </td><td>1000/1</td><td>+100000</td></tr><tr><td>Saudi Arabia</td><td>1000/1</td><td>+100000</td></tr><tr><td>South Korea</td><td>250/1</td><td>+25000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	