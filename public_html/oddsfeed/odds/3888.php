	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Pre--wc Qualification, Afc">
			<caption>Pre--wc Qualification, Afc</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated September 10, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

	        <tr><th colspan="3"  class="datahead">Pre--wc Qualification, Afc  - Sep 10</th></tr>
       <tr><td colspan="3" class="teamrowHead"><div>
       
       <div class="row clearfix">
       <div class="team" >Team</div>
       <div class="wagerdata" >
         <div class="boxhead" >Total</div>
         <div class="boxhead" >Moneyline</div>
         <div class="boxhead" >Spread</div>
       </div>
       </div>
       
       </div></td></tr>
         <tr><td colspan="3">
         <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Australia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -101 )</div>
								 <div class="boxdata" >-222</div>
								 <div class="boxdata" >-1<br /> ( -133)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Kuwait</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -131 )</div>
								 <div class="boxdata" >593</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >China</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac34;<br /> ( -120 )</div>
								 <div class="boxdata" >-9999</div>
								 <div class="boxdata" >-3&frac12;<br /> ( -108 )</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Maldives</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac34;<br /> ( -109 )</div>
								 <div class="boxdata" >4628</div>
								 <div class="boxdata" >+3&frac12;<br /> ( -121 )</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Saudi Arabia</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac34;<br /> ( -112 )</div>
								 <div class="boxdata" >-526</div>
								 <div class="boxdata" >-2<br /> ( +106)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Yemen</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac34;<br /> ( -117 )</div>
								 <div class="boxdata" >1359</div>
								 <div class="boxdata" >+2<br /> ( -140)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Bangladesh</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o2&frac12;<br /> ( -109 )</div>
								 <div class="boxdata" >434</div>
								 <div class="boxdata" >+1<br /> ( -121)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Afghanistan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u2&frac12;<br /> ( -120 )</div>
								 <div class="boxdata" >-188</div>
								 <div class="boxdata" >-1<br /> ( -108)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >India</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3&frac34;<br /> ( -126 )</div>
								 <div class="boxdata" >2497</div>
								 <div class="boxdata" >+3<br /> ( -129)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Qatar</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3&frac34;<br /> ( -105 )</div>
								 <div class="boxdata" >-1666</div>
								 <div class="boxdata" >-3<br /> ( -102)</div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Korea Dpr</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >-2499</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Sri Lanka</div>
							   <div class="wagerdata" >
								 <div class="boxdata" ></div>
								 <div class="boxdata" >3522</div>
								 <div class="boxdata" ></div>
							   </div>
							   </div>
							   
							   </div> <div class="eachrow" >
							   
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Republic Of Korea</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >o3<br /> ( -119)</div>
								 <div class="boxdata" >-526</div>
								 <div class="boxdata" >-2<br /> ( -103)</div>
							   </div>
							   </div>
							   <div class="row clearfix">
							   <div class="team paddingtop" ><img  src="/img/vrs.png"></div>
							   <div class="wagerdata" ><hr /></div>
							   </div>
							   <div class="row teamrow clearfix">
							   <div class="team paddingtop" >Turkmenistan</div>
							   <div class="wagerdata" >
								 <div class="boxdata" >u3<br /> ( -111)</div>
								 <div class="boxdata" >1125</div>
								 <div class="boxdata" >+2<br /> ( -128)</div>
							   </div>
							   </div>
							   
							   </div>                   
               </td>
               </tr>
        
        			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	