<?php
ini_set('error_reporting', E_ALL);
ini_set("display_errors", 1); 		 	
set_time_limit(0);

if ( !defined( 'APP_PATH' ) ) {
    define( 'APP_PATH' , '/home/ah/scrappers/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

require_once APP_PATH . 'Cron.Scraper.php';




$printdetailsArray=array();
$IdLeagueToprint=array('998', '1006');

$printdetailsArray['998']['summaryTitle']='The latest odds for the Kentucky Derby available for wagering now at bet &#9733; usracing.';
$printdetailsArray['998']['OddstableType']='Old';
$printdetailsArray['998']['captionTitle']='Kentucky Derby Odds';
$printdetailsArray['998']['OUTPUTFILEPATH']='/home/ah/allhorse/public_html/kd/odds_kd_V1.php';
$printdetailsArray['998']['Schema']='{literal}	
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2020 Kentucky Derby Odds"
	  }
	</script>{/literal}';
	
$printdetailsArray['1006']['summaryTitle']='The latest odds for the Kentucky Oaks available for wagering now at  bet &#9733; usracing.';
$printdetailsArray['1006']['OddstableType']='Old';
$printdetailsArray['1006']['captionTitle']='Kentucky Oaks Odds';
$printdetailsArray['1006']['OUTPUTFILEPATH']='/home/ah/allhorse/public_html/kd/odds_kentucky_oaks_1006_v1.php';
$printdetailsArray['1006']['Schema']='{literal}
			<script type="application/ld+json">
			{
			  "@context": "http://schema.org",
			  "@type": "Table",
			  "about": "2020 Kentucky Oaks Odds"
			}
		</script> 
	{/literal}';
	
		
$printdetailsArray['1012']['summaryTitle']='';
$printdetailsArray['1012']['OddstableType']='Old';
$printdetailsArray['1012']['captionTitle']='';
$printdetailsArray['1012']['OUTPUTFILEPATH']='/home/ah/allhorse/public_html/misc/arc_de_triomphe_1012_v1.php';
$printdetailsArray['1012']['Schema']='	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Prix de l\'Arc de Triomphe Odds"
	  }
	</script>
	{/literal}';
	
$printdetailsArray['3447']['summaryTitle']='';
$printdetailsArray['3447']['OddstableType']='Old';
$printdetailsArray['3447']['captionTitle']='';
$printdetailsArray['3447']['OUTPUTFILEPATH']='/home/ah/allhorse/public_html/misc/commonwealth_cup_3447_v1.php';
$printdetailsArray['3447']['Schema']='	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Commonwealth Cup Odds"
	  }
	</script>
	{/literal}';	
	
$printdetailsArray['982']['summaryTitle']='';
$printdetailsArray['982']['OddstableType']='Old';
$printdetailsArray['982']['captionTitle']='';
$printdetailsArray['982']['OUTPUTFILEPATH']='/home/ah/allhorse/public_html/misc/melbourne_cup_982_v1.php';
$printdetailsArray['982']['Schema']='	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Melbourne Cup Odds"
	  }
	</script>
	{/literal}';
	

$printdetailsArray['993']['summaryTitle']='';
$printdetailsArray['993']['OddstableType']='New';
$printdetailsArray['993']['captionTitle']='';
$printdetailsArray['993']['OUTPUTFILEPATH']='/home/ah/allhorse/public_html/misc/triple_crown_special_v1.php';
$printdetailsArray['993']['Schema']='	{literal}
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Table",
  "about": "Triple Crown Odds"
}
</script> 
	{/literal}';
	
$printdetailsArray['1005']['summaryTitle']='The latest odds for the Dubai World Cup available for wagering now at  bet &#9733; usracing.';
$printdetailsArray['1005']['OddstableType']='Old';
$printdetailsArray['1005']['captionTitle']='2020 Dubai World Cup Odds';
$printdetailsArray['1005']['OUTPUTFILEPATH']='/home/ah/allhorse/public_html/dubai/odds_dubai_1005_xml_v1.php';
$printdetailsArray['1005']['Schema']='	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2020 Dubai World Cup Odds"
	  }
	</script>
	{/literal} ';		

$printdetailsArray['1279']['summaryTitle']='';
$printdetailsArray['1279']['OddstableType']='Old';
$printdetailsArray['1279']['captionTitle']='';
$printdetailsArray['1279']['OUTPUTFILEPATH']='/home/ah/allhorse/public_html/grand_national/odds_1279_v1.php';
$printdetailsArray['1279']['Schema']='	{literal}
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Table",
  "about": "Grand National Odds"
}
</script> 
	{/literal}';

$printdetailsArray['4275']['summaryTitle']='The latest odds for the Saudi Cup available for wagering now at bet &#9733; usracing.';
$printdetailsArray['4275']['OddstableType']='Old';
$printdetailsArray['4275']['captionTitle']='Saudi Cup Odds and Contenders';
$printdetailsArray['4275']['OUTPUTFILEPATH']='/home/ah/allhorse/public_html/saudi-cup/saudi_cup_odds_xml_v1.php';
$printdetailsArray['4275']['Schema']='	{literal}
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "about": "2020 Saudi Cup Odds",
        "keywords": "Saudi Cup 2020, 2020 Saudi Cup, Saudi Cup, Saudi Cup odds, Saudi Cup 2020 odds, Saudi Cup betting, Bet on the Saudi Cup, Riyadh, King Abdulaziz Racetrack, World\'s Richest Horse Race"
    }
    </script>
	{/literal}';

	
	
$FileXML= 'http://ww1.betusracing.ag/odds.xml';

 $xmlString = file_get_contents($FileXML);
 $SC = new SimpleGen;
$SC->xmlSource =$xmlString;
$xml = simplexml_load_string($xmlString);	

foreach($xml as $value){
	$AttributeArray= $value->attributes();
	$Type= explode("-",$AttributeArray['Description']);
	$Title=(string)$AttributeArray['Description'];
	$IdLeague=(string)$AttributeArray['IdLeague'];
	//if(in_array($IdLeague,$IdLeagueToprint)){
	if(array_key_exists($IdLeague, $printdetailsArray)){

	$SC->dataDESC=trim($AttributeArray['Description']);
	$SC->IdLeague = $AttributeArray['IdLeague'];
	//$SC->OUTPUTFILEPATH='oddsTables/'.$AttributeArray['IdLeague'].'.php';
	$SC->OddstableType=$printdetailsArray[$IdLeague]['OddstableType'];
	$SC->summaryTitle=$printdetailsArray[$IdLeague]['summaryTitle'];
	$SC->captionTitle=$printdetailsArray[$IdLeague]['captionTitle'];
	$SC->Schema=$printdetailsArray[$IdLeague]['Schema'];	
	$SC->OUTPUTFILEPATH=$printdetailsArray[$IdLeague]['OUTPUTFILEPATH'];
	$SC->run();

	}
	
	
	
}



?>
