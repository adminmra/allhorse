<style type="text/css">
.rsBlock { display:inline-block;}
.rsContent .infoBlock.center { margin:0 auto; position:relative; left:auto; text-align:center; }
.font20 { font-size:20px !important;}.font25 { font-size:25px !important;}.font30 { font-size:30px !important;}.font35 { font-size:35px !important; line-height:35px !important; }.font40 { font-size:40px !important;}.font45 { font-size:45px !important;}.font50{ font-size:50px !important;}.font55{ font-size:55px !important; line-height:55px !important;}
</style>
<script type="text/javascript">
$('.royalSlider').royalSlider({
        arrowsNav: true,
        loop: true,
        usePreloader: true,
        autoPlay: {
            enabled: true,
            delay: 6000,
            pauseOnHover: true
        },
        transitionSpeed: 500,
        keyboardNavEnabled: true,
        controlsInside: false,
        imageScaleMode: 'fill',
        arrowsNavAutoHide: true,
        autoScaleSlider: true,
        autoScaleSliderWidth: 824,
        autoScaleSliderHeight: 300,
        controlNavigation: 'bullets',
        thumbsFitInViewport: false,
        navigateByClick: false,
        numImagesToPreload: 1,
        startSlideId: 0,
        transitionType: 'fade',
        globalCaption: false,
        slidesSpacing: 0,
        deeplinking: {
            enabled: true,
            change: false
        },
        imgWidth: 824,
        imgHeight: 300
    });
$('<div class="rsNavLabel">Promos</div>').appendTo( $( ".royalSlider" ) );
$('.royalSlider .rsNavItem').each(function(i) {	
    num = i + 1;
	$(this).html("<span>" + num + "</span>");
});


$(function () {
/*
$("#road").dialog({
    autoOpen: false,
    dialogClass: "okDialog",
    modal: true,
	title: "Road to the Kentucky Derby",
    closeOnEscape: true,
    width: 500,
    closeText: "X",
	open: function(event) {
     $('.ui-dialog-buttonpane').find('button span:contains("Bet Now")').removeClass('btn-default').addClass('btn-red');
 	},
    buttons: {
        "Close ": function() {
            $(this).dialog('close');
        },
        "Bet Now" : function() {			
            window.location = '/bet';		
        }
    },
    show: {
        effect: "fade",
        duration: 200
    },
    hide: {
        effect: "fade",
        duration: 200
    }
});*/

$("#bonus").dialog({
    autoOpen: false,
    dialogClass: "okDialog",
    modal: true,
	title: "US Racing's $125 Free Cash Bonus",
    closeOnEscape: true,
    width: 500,
    closeText: "X",
    buttons: {
        "Close": function () {
            $(this).dialog("close");
        }
    },
    show: {
        effect: "fade",
        duration: 200
    },
    hide: {
        effect: "fade",
        duration: 200
    }
});
$("#carry").dialog({
    autoOpen: false,
    dialogClass: "okDialog",
    modal: true,
	title: "What are Carryover Pools?",
    closeOnEscape: true,
    width: 500,
    closeText: "X",
    buttons: {
        "Close": function () {
            $(this).dialog("close");
        }
    },
    show: {
        effect: "fade",
        duration: 200
    },
    hide: {
        effect: "fade",
        duration: 200
    }
});


$(".ui-dialog-titlebar-close").attr("title", "close");
$(".ui-dialog-buttonset button span").addClass("btn btn-default");


/* Modals Triggers */
$("#modalPop1").click(function () {
    $("#road").dialog("open");
});
$("#modalPop2").click(function () {
    $("#carry").dialog("open");
});
$("#modalPop3").click(function () {
    $("#bonus").dialog("open");
});




});


	
</script>