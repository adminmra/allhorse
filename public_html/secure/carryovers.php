<script type="text/javascript">
        var carryover_url = "https://secure.usracing.com/ajaxservice?do=getCarryovers";
        //var carryover_msg = "There is no carryover at the moment.";

        $.get(carryover_url)
            .done(function (r) {
                var arr = $.parseJSON(r);
                renderCarryoverList(arr);
            })
            .fail(function () {
                //$("#carryoverList .loader").html(carryover_msg);
				$("#carryoverList").hide();
				$("#carryoverMsg").show();
            });

        var renderCarryoverList = function (arr) {
            if (arr.length > 0) {
				$("#carryoverMsg").hide();
                $("#carryoverList .loader").remove();				
            }
            else {
				$("#carryoverList .loader").remove();
                //$("#carryoverList .loader").html(carryover_msg);
				$("#carryoverList").hide();
				$("#carryoverMsg").show();
				
            }

            for (var i = 0; i < arr.length; ++i) {
                var obj = arr[i];
                var trackName = obj.trackName;
                var wagerType = obj.wagerType;
                var amount = obj.amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

                var dom = $("<tr>")
                    .append("<td>" + trackName + "</td>")
                    .append("<td class=\"center\">" + wagerType + "</td>")
                    .append("<td class=\"right\"><small>$</small> " + amount + "</td>");

                $("#carryoverList").append(dom);
				dom.find('td:contains("Churchill Downs")').parent().remove();		
				dom.find('td:contains("Fair Grounds")').parent().remove();	
				dom.find('td:contains("Calder")').parent().remove();
				dom.find('td:contains("Hoosier")').parent().remove();	
				dom.find('td:contains("Indiana Downs")').parent().remove();	
				dom.find('td:contains("Miami Valley")').parent().remove();	
				dom.find('td:contains("Oaklawn Park")').parent().remove();
				dom.find('td:contains("The Meadows")').parent().remove();
				dom.find('td:contains("Arlington Park")').parent().remove();
				dom.find('td:contains("Finger Lakes")').parent().remove();
				dom.find('td:contains("Canterbury Park")').parent().remove();
				dom.find('td:contains("Preakness Stakes Advance")').parent().remove();
dom.find('td:contains("Pimlico")').parent().remove();
dom.find('td:contains("Pimlico Jockey Challenge")').parent().remove();
dom.find('td:contains("Gulfstream Park")').parent().remove();
dom.find('td:contains("Golden Gate Fields")').parent().remove();
dom.find('td:contains("Preakness Pick 3")').parent().remove();
dom.find('td:contains("Pim Spcl Preakness")').parent().remove();
dom.find('td:contains("Santa Anita")').parent().remove();
dom.find('td:contains("Meadowlands")').parent().remove();
dom.find('td:contains("Lone Star Park")').parent().remove();
dom.find('td:contains("Los Alamitos")').parent().remove();
dom.find('td:contains("Barretts")').parent().remove();
dom.find('td:contains("Del Mar")').parent().remove();
dom.find('td:contains("Ferndale")').parent().remove();
dom.find('td:contains("Fresno")').parent().remove();
dom.find('td:contains("Kentucky Downs")').parent().remove();
dom.find('td:contains("Laurel Park")').parent().remove();
dom.find('td:contains("Monmouth Park")').parent().remove();
dom.find('td:contains("Pleasanton")').parent().remove();
dom.find('td:contains("Portland Meadows")').parent().remove();
dom.find('td:contains("Sacramento ")').parent().remove();
dom.find('td:contains("Santa Rosa")').parent().remove();
dom.find('td:contains("Stockton")').parent().remove();
dom.find('td:contains("Tampa Bay Downs")').parent().remove();
dom.find('td:contains("Timonium")').parent().remove();
dom.find('td:contains("Turf Paradise")').parent().remove();
dom.find('td:contains("Latin American Racing")').parent().remove();
dom.find('td:contains("Sunshine Millions")').parent().remove();				
            };
        };
		
		
</script>

<div id="carryover" class="block">
	<h3>Today's Carryover Pools <i id="carryoversHelp" class="fa fa-question" title="<p><strong>What are Carryover Pools?</strong></p>
<p>A carryover is when no wins occur at the end of a race on a particular wager type - for example, a Pick 4, Pick 5 or Pick 6. All wagers in the carryover pool or 'pool' are then carried over to the next designated race. The carryover continues from race to race until a lucky winner or winners collect the prize pool, which can reach millions of dollars. As carryover pools increase in value, more betting interest is evidenced and consequently as horse bettors have a chance to win bigger money.</p>"></i></h3>
                       
  
    <table id="carryoverList" width="100%" cellspacing="0" cellpadding="0" border="0"> 
        <tr class="header">
            <th>Race</th>
            <th class="center">Type</th>
            <th class="right">Prize Pool</th>
        </tr>
        <tr>
            <td colspan="3" class="loader"><img src="images/ajax-loader.gif" /> Loading</td>
        </tr>
    </table>
    
    <div id="carryoverMsg" style="display:none;"> There are no carryovers at the moment.</div>
      
</div><!-- end/carryover -->
