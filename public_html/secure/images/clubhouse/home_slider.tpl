<!--=== Home Slider ===-->
<div class="sliderContainer fullWidth clearfix margin-bottom-30">
<div class="tp-banner-container">
<div class="tp-banner" >
<ul>
<!-- slide -->
<li data-transition="fade" data-slotamount="2" data-masterspeed="1000"  data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7">
						<!-- MAIN IMAGE -->
						<img src="/video/preakness-stakes-betting.jpg"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">			
						<div class="tp-dottedoverlay twoxtwo"></div>
						<!-- HTML5 Video -->
						<div class="tp-caption tp-fade fadeout fullscreenvideo hidden-xs"
							data-x="0"
							data-y="0"
							data-speed="1000"
							data-start="600"
							data-easing="Power4.easeOut"
							data-endspeed="1000"
							data-endeasing="Power4.easeIn"
							data-autoplay="true"
							data-autoplayonlyfirsttime="false"
							data-nextslideatend="true"
							data-forceCover="1"
							data-dottedoverlay="twoxtwo"
							data-forcerewind="on"
							style="z-index: 1">

						<video class="video-js vjs-default-skin" preload="none" width="100%" height="100%" poster='/video/preakness-stakes-betting.jpg' >
						<source src='/video/preakness-stakes-betting.mp4' type='video/mp4' />
						<source src='/video/preakness-stakes-betting.webm' type='video/webm' />
						<source src='/video/preakness-stakes-betting.ogv' type='video/ogg' />
						</video>

						</div>
                        
                    <!-- LARGE Text: DESKTOP -->
						<div class="tp-caption customin customout tp-resizeme txtLrg hidden-xs"
							data-x="center" data-hoffset="0"
							data-y="center" data-voffset="-80"
							data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="600"
							data-start="5800"
							data-easing="Power4.easeOut"
							data-endspeed="1000"
							data-endeasing="Power0.easeIn"
							style="z-index: 4; font-size:100px;">Online Horse Betting
						</div>
                        
                       <!-- LARGE Text: MOBILE --> 
                        <div class="tp-caption customin customout tp-resizeme txtLrg visible-xs"
							data-x="center" data-hoffset="0"
							data-y="center" data-voffset="-80"
							data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="600"
							data-start="1000"
							data-easing="Power4.easeOut"
							data-endspeed="1000"
							data-endeasing="Power0.easeIn"
							style="z-index: 4; font-size:100px;">Online Horse Betting
						</div>
                        
                        <!-- MED Text: DESKTOP -->
						<div class="tp-caption customin customout tp-resizeme txtMed hidden-xs"
							data-x="center" data-hoffset="0"
							data-y="center" data-voffset="0"
							data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="600"
							data-start="6100"
							data-easing="Power4.easeOut"
							data-endspeed="1000"
							data-endeasing="Power0.easeIn"
							style="z-index: 4;  font-size:40px;">The easiest site to bet online
						</div>
                        
                        <!-- MED Text: MOBILE -->
						<div class="tp-caption customin customout tp-resizeme txtMed visible-xs"
							data-x="center" data-hoffset="0"
							data-y="center" data-voffset="0"
							data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="600"
							data-start="1200"
							data-easing="Power4.easeOut"
							data-endspeed="1000"
							data-endeasing="Power0.easeIn"
							style="z-index: 4;  font-size:40px;">The easiest site to bet online
						</div>
                        
                        <!-- Button: DESKTOP -->
						<a href="https://secure.usracing.com/signup" rel="nofollow" class="tp-caption customin customout btn btn-red hidden-xs"
							data-x="center" data-hoffset="0"
							data-y="center" data-voffset="80"
							data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="600"
							data-start="6400"
							data-easing="Power4.easeOut"
							data-endspeed="1000"
							data-endeasing="Power0.easeIn"
							style="z-index: 4; font-size:30px;"><i class="fa fa-hand-o-right left"></i> Get your Account for Free!
						</a>
                        
                        <!-- Button: MOBILE -->
						<a href="https://secure.usracing.com/signup" rel="nofollow" class="tp-caption customin customout btn btn-red visible-xs"
							data-x="center" data-hoffset="0"
							data-y="center" data-voffset="80"
							data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="600"
							data-start="1400"
							data-easing="Power4.easeOut"
							data-endspeed="1000"
							data-endeasing="Power0.easeIn"
							style="z-index: 4; font-size:30px;"><i class="fa fa-hand-o-right left"></i> Get your Account for Free!
						</a>
</li><!-- end slide -->

<!-- slide -->
<li data-transition="fade" data-slotamount="7" data-masterspeed="1000" >
					<!-- IMAGE -->
					<img src="/img/belmont-stakes-winner.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                    
					<!-- Lrg Text -->
					<div class="tp-caption sfr fadeout txtLrg"
						data-x="right"
						data-hoffset="80"
						data-y="center"
                        data-voffset="-100"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:80px; line-height:80px;">Tonalist Trounces the Belmont!
					</div>
                    
                    <!-- Secondary Text -->
					<div class="tp-caption sfr fadeout txtMed"
						data-x="right"
						data-hoffset="80"
						data-y="center"
                        data-voffset="15"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;">Congratulations on winning Belmont!
					</div>
                    
                    <!-- Button -->
					<a href="https://secure.usracing.com/signup" rel="nofollow" class="tp-caption sfr fadeout btn btn-red"
						data-x="tight"
						data-hoffset="80"
						data-y="center"
                        data-voffset="90"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-star left"></i> Bet Now!
					</a>
</li><!-- end/slide -->



<!-- slide -->
<li data-transition="fade" data-slotamount="2" data-masterspeed="1000"  data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7">
						<!-- MAIN IMAGE -->
						<img src="/video/betonhorses4.jpg"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">					
						<div class="tp-dottedoverlay twoxtwo"></div>
						<!-- HTML5 Video -->
						<div class="tp-caption tp-fade fadeout fullscreenvideo"
							data-x="0"
							data-y="0"
							data-speed="1000"
							data-start="600"
							data-easing="Power4.easeOut"
							data-endspeed="1200"
							data-endeasing="Power4.easeIn"
							data-autoplay="true"
							data-autoplayonlyfirsttime="false"
							data-nextslideatend="true"
							data-forceCover="1"
							data-dottedoverlay="twoxtwo"
							data-forcerewind="on"
							style="z-index: 1">

						<video class="video-js vjs-default-skin" preload="none" width="100%" height="100%" poster='/video/betonhorses4.jpg' >
						<source src='/video/betonhorses4.mp4' type='video/mp4' />
						<source src='/video/betonhorses4.webm' type='video/webm' />
						<source src='/video/betonhorses4.ogv' type='video/ogg' />
						</video>

						</div>
                        
                    <!-- LARGE Text -->
						<div class="tp-caption customin customout tp-resizeme txtLrg"
							data-x="center" data-hoffset="0"
							data-y="center" data-voffset="-80"
							data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="600"
							data-start="1400"
							data-easing="Power4.easeOut"
							data-endspeed="600"
							data-endeasing="Power0.easeIn"
							style="z-index: 4; font-size:110px;">Bet on Horses!

						</div>
                        
                        <!-- MED Text -->
						<div class="tp-caption customin customout tp-resizeme txtMed"
							data-x="center" data-hoffset="0"
							data-y="center" data-voffset="0"
							data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="600"
							data-start="1700"
							data-easing="Power4.easeOut"
							data-endspeed="600"
							data-endeasing="Power0.easeIn"
							style="z-index: 4;  font-size:40px;">The Easiest Site to Bet Online!
						</div>
                        
                        <!-- Button -->
						<a href="https://secure.usracing.com/signup" rel="nofollow" class="tp-caption customin customout btn btn-red"
							data-x="center" data-hoffset="0"
							data-y="center" data-voffset="80"
							data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:5;scaleY:5;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
							data-speed="600"
							data-start="2100"
							data-easing="Power4.easeOut"
							data-endspeed="600"
							data-endeasing="Power0.easeIn"
							style="z-index: 4; font-size:30px;"><i class="fa fa-hand-o-right left"></i> Start Betting Now!
						</a>
</li><!-- end/slide -->

<!-- slide -->            
<li data-transition="fade" data-slotamount="7" data-masterspeed="1000" >
					<!-- IMAGE -->
					<img src="/img/online-horseracing-bonus.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                    
					<!-- Lrg Text -->
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;">Get <small>$</small>125 Free!
					</div>
                    
                    <!-- Secondary Text -->
					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-10"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;">Your bonus for trying US Racing.
					</div>
                    
                    <!-- Button -->
					<a href="https://secure.usracing.com/signup" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="70"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-star left"></i> Get Your Bonus!
					</a>
                    
                    <!-- Learn More Button -->
					<a href="#" rel="nofollow" class="tp-caption sfl fadeout btn btn-txt"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="130"
						data-speed="400"
						data-start="1700"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
                        data-toggle="modal" data-target="#bonus"
						style="z-index: 4; font-size:22px;">Learn More <i class="fa fa-question-circle"></i>
					</a>
</li><!-- end slide -->
 
 <!-- slide -->               
<li data-transition="fade" data-slotamount="6" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					<img src="img/bet-on-horses.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
					<!-- LAYERS -->
					
                    <!-- Lrg Text -->
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;">Live Horse Racing
					</div>
                    
                    <!-- Secondary Text -->
					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-10"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;">Watch from your desktop or mobile
					</div>
                    
                    <!-- Button -->
					<a href="https://secure.usracing.com/signup" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="70"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-thumbs-o-up left"></i> Try US Racing!
					</a>
 

				</li>
				
				<li data-transition="fade" data-slotamount="6" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					<img src="img/easy-withdraw.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
					<!-- LAYERS -->


					<!-- Lrg Text -->
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-70"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px; line-height:90px;">Hassle-free Deposits<br>& Quick Withdrawals
					</div>
                    
                                      
                    <!-- Button -->
					<a href="https://secure.usracing.com/signup" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="70"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-pencil left"></i> Sign Up Today!
					</a>
                    
                    <!-- Learn More Button -->
					<a href="#" rel="nofollow" class="tp-caption sfl fadeout btn btn-txt"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="130"
						data-speed="400"
						data-start="1700"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
                        data-toggle="modal" data-target="#cashier"
						style="z-index: 4; font-size:22px;">Learn More <i class="fa fa-question-circle"></i>
					</a>
</li><!-- end slide -->
</ul>

<div class="tp-bannertimer"></div>
</div><!-- end/tp-banner-container -->
</div><!-- end/tp-banner -->
</div><!-- end/SliderContainer -->


<!-- Modal1 -->
<div class="modal fade" id="bonus" tabindex="-1">
 <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"><button class="close" data-dismiss="modal" type="button">X</button>
 <h3 class="modal-title" id="bonusLabel">US Racing's {include file='/home/ah/allhorse/public_html/shared/bonus-amount.tpl'} Free Cash Bonus</h3>
 </div>
 <div class="modal-body">
 <p><strong>How our Bonus Works:</strong> &nbsp; <em>( Terms and Conditions )</em></p>
 
 {include file='/home/ah/allhorse/public_html/shared/bonus-terms.tpl'}
 
 </div>
 <div class="modal-footer">
 <button class="btn btn-lrg btn-default pull-left" data-dismiss="modal" type="button">I am not ready yet.</button>
 <a class=" btn btn-lrg btn-red" href="https://secure.usracing.com/signup" rel="nofollow" >Let's Start Wagering!</a>
 </div>
 </div>
 </div>
</div>

<!-- Modal2 -->
<div class="modal fade" id="cashier" tabindex="-1">
 <div class="modal-dialog"><div class="modal-content"><div class="modal-header"> <button class="close" data-dismiss="modal" type="button">X</button>
 <h3 class="modal-title" id="cashierLabel">US Racing Easy Deposits & Withdrawls</h3>
 </div>
 <div class="modal-body">
 <p><strong>How our Cashier Works:</strong></p>
 <ul>
 <li>Withdrawals are processed within 5 business days a the very latest.</li>
 <li>You can deposit with either your MasterCard or Visa credit cards.</li>
 <li>For your convenience, we also accept ACH (Instant BetCash), check, wire transfer or money order.</li>
 <li>If you prefer, you can transact via our toll free number: 1-844-US RACING (877-2246)</li>
 <li>All transactions are securely processed with 256-bit SSL Encryption.</li>
 <li>New customers can qualify for our free $125 bonus. Conditions Apply.</li>
 </ul>
 </div>
 <div class="modal-footer">
 <button class="btn btn-lrg btn-default pull-left" data-dismiss="modal" type="button">I am not ready yet.</button>
 <a class=" btn btn-lrg btn-red" href="https://secure.usracing.com/signup" rel="nofollow" >Let's Start Wagering!</a>
 </div>
 </div>
 </div>
</div>