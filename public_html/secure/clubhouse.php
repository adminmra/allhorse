<div id="clubhouse" style="width:100%;">

<!-- ==== BUTTONS === -->
<div class="buttons">
	<span class="col-md-4"><a class="btn btn-lrg btn-primary" href="/bet" ><i class="fa fa-ticket"></i> Bet Now</a></span>
	<span class="col-md-4"><a class="btn btn-lrg btn-green" href="/deposit"><i class="fa fa-credit-card"></i> Deposit</a></span>
	<span class="col-md-4"><a class="btn btn-lrg btn-default" href="/bet?tab=mybets"><i class="fa fa-clock-o"></i> Pending</a></span>
</div>


<!-- ==== SLIDER === -->
<div id="clubSlider" class="fullWidth clearfix">
	<div class="royalSlider rsDefault">
 
 

          
	<!--<a class="rsContent" target="_blank" href="https://touch.tvg.com/Login/Login" style="display:block;">
        <img class="rsImg" src="https://www.allhorse.com/secure/images/clubhouse/promo-notrack-link.jpg" />
        </a>  -->
            
    	
        <div class="rsContent">
        <img class="rsImg" src="https://www.allhorse.com/secure/images/clubhouse/promo-bg.jpg" />
        	<div class="infoBlock">
 			<span class="rsABlock headlineMed font45" data-move-effect="top" data-speed="200">Win Big from Carryover Pools!</span>
        	<span class="rsABlock headlineSml" data-move-effect="top" data-speed="200">It aint over til it's over!</span>
        	<button id="modalPop2" type="button" class="rsABlock btn btn-default" data-move-effect="bottom" data-speed="240"><i class="fa fa-hand-o-right"></i> Learn More</button>
           	</div>
   		</div>   
        
            
	</div>    
</div>


<!-- ==== CARRY OVERS === -->
<?php include('carryovers.php'); ?>




<!-- ==== MODAL1 === -->
<div id="road" style="display:none;">
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="table table-condensed table-striped table-bordered">
	
  <tr>
    <th>When</th>
    <th>Race</th>
    <th>Track</th>
  </tr>
  
  <tr>
    <td><strong>March 27th - 29th</strong></td>
    <td>Derby Future Pools</td>
    <td>Churchill Downs</td>
  </tr>
  <tr>
    <td><strong>May 3rd</strong></td>
    <td>Kentucky Derby</td>
    <td>Churchill Downs</td>
  </tr>
</table>

 </div>          
      

 <!-- ==== MODAL2 === -->
<div id="carry" style="display:none;">
<p>A carryover is when no wins occur at the end of a race on a particular wager type - for example, a Pick 4, Pick 5 or Pick 6. All wagers in the carryover pool or 'pool' are then carried over to the next designated race. The carryover continues from race to race until a lucky winner or winners collect the prize pool, which can reach millions of dollars. As carryover pools increase in value, more betting interest is evidenced and consequently as horse bettors have a chance to win bigger money.</p>
</div>

<!-- ==== MODAL3 === -->
<div id="bonus" style="display:none;">
 <p><strong>How our Bonus Works:</strong> &nbsp; <em>( Terms and Conditions )</em></p>
 <ul>
<li>This promotion is for new players only.</li>
<li>To qualify, you must deposit a minimum of $25 and wager at least $200 within 30 days of registering your account.</li>
<li>Once qualified, your bonus will be deposited into your account between 7-10 business days afterwards.</li>

<li>Current or former US Racing or TVG players are not eligible for this promotion.</li>
<li>Wagers must be made online.</li>
<li>Wagers must be made at the US Racing website.</li>
<li>This promotion may be suspended by the Management of US Racing without notice.</li>
<li>Bonus cash must be wagered at US Racing before it can be withdrawn from player's account.  In other words, you must make wagers of $125 after your receive your bonus.  Afterwards, you may withdraw these funds from your account.</li>
<li>If you have any questions about this promotion, please contact US Racing.</li>

<li>This promotion is void where prohibited. All terms and conditions will apply for qualification.</li>
</ul>
 </div>    
               
 </div><!-- end/clubhouse -->
 
 
 <?php include('clubhouse-slider-controls.php'); ?>