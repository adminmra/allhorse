<h2>Breeders' Cup Races</h2>

	
	<div class="table-responsive"><table class="data table table-condensed table-striped table-bordered" title="Breeders Cup Races" summary="A complete list of races for the {include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders' Cup." border="0" cellpadding="0" cellspacing="0" width="100%"><caption>Friday, November 2nd, 2018</caption>
	<tr><th width="25%">Race</th><th width="20%">Purse</th><th width="20%">Distance</th><th>Grade</th><th width="20%">Age</th></tr>
   <tr><td><a href="https://www.usracing.com/breeders-cup/juvenile-turf-sprint">Juvenile Turf Sprint</a></td><td>$1,000,000</td><td>5.5 Furlongs</td><td>TBD</td><td>2 Year Olds</td></tr>
   	<tr><td><a href="https://www.usracing.com/breeders-cup/juvenile-fillies-turf">Juvenile Fillies Turf</a></td><td>$1,000,000</td><td>1 Mile</td><td>1</td><td>2 Year Olds</td></tr>
	<tr><td><a href="https://www.usracing.com/breeders-cup/juvenile-fillies">Juvenile Fillies</a></td><td>$2,000,000</td><td>1 1/16 Miles</td><td>1</td><td>2 Year Olds</td></tr>
	
	<tr><td><a href="https://www.usracing.com/breeders-cup/juvenile-turf">Juvenile Turf</a></td><td>$1,000,000</td><td>1 Mile</td><td>1</td><td>2 Year Olds</td></tr>

    	


	<tr><td><a href="https://www.usracing.com/breeders-cup/juvenile">Juvenile</a></td><td>$2,000,000</td><td>1 1/16 Miles</td><td>1</td><td>2 Year Olds</td></tr>
</table></div>
<div class="table-responsive"><table class="data table table-condensed table-striped table-bordered" title="Breeders Cup Races" summary="A complete list of races for the {include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders' Cup." border="0" cellpadding="0" cellspacing="0" width="100%"><caption>Saturday, November 3rd, 2018</caption>
	<tr><th width="25%">Race</th><th width="20%">Purse</th><th width="20%">Distance</th><th>Grade</th><th width="20%">Age</th></tr>
<tr><td><a href="https://www.usracing.com/breeders-cup/filly-mare-sprint">Filly & Mare Sprint</a></td><td>$1,000,000</td><td>7 Furlongs</td><td>1</td><td>3+ Year Olds</td></tr>
	<tr><td><a href="https://www.usracing.com/breeders-cup/turf-sprint">Turf Sprint</a></td><td>$1,000,000</td>
	<td>6.5 Furlongs</td><td>1</td><td>3+ Year Olds</td></tr>
    <tr><td><a href="https://www.usracing.com/breeders-cup/dirt-mile">Dirt Mile</a></td><td>$1,000,000</td><td>1 Mile</td><td>1</td><td>3+ Year Olds</td></tr>

	<tr><td><a href="https://www.usracing.com/breeders-cup/filly-mare-turf">Filly & Mare Turf</a></td><td>$2,000,000</td>
	<td>1 1/14 Miles</td><td>1</td><td>3+ Year Olds</td></tr>
	<tr><td><a href="https://www.usracing.com/breeders-cup/sprint">Sprint</a></td><td>$1,500,000</td>
	<td>6 Furlongs</td><td>1</td><td>3+ Year Olds</td></tr>
	<tr><td><a href="https://www.usracing.com/breeders-cup/mile">Mile</a></td><td>$2,000,000</td><td>1 Mile</td><td>1</td><td>3+ Year Olds</td></tr>
    <tr><td><a href="https://www.usracing.com/breeders-cup/distaff">Distaff</a></td><td>$2,000,000</td>
	<td>1 1/8 Miles</td><td>1</td><td>3+ Year Olds</td></tr>
	
	<tr><td><a href="https://www.usracing.com/breeders-cup/turf">Turf</a></td>
	<td>$4,000,000</td><td>1 1/2 Miles</td><td>1</td><td>3+ Year Olds</td></tr>
	<tr><td><a href="https://www.usracing.com/breeders-cup/classic"><strong>Classic</strong></a></td>
	<td><strong>$6,000,000</strong></td><td><strong>1 1/4 miles</strong></td><td><strong>1</td><td><strong>3+ Year Olds</strong></td></tr>

</table></div>


