    <script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2020 US Presidential Election - Winning Party"
	  }
	</script> 
    <div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Us Presidential Election - Winning Party">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y H:00", time() - 7200); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Us Presidential Election - Winning Party  - Nov 02                    </th>
            </tr>
            -->
    <tr><td colspan="3" class="center"><table id="o9t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody> <br>   <tr><th colspan="3" class="center">2020 US Presidential Election - Winning Party</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Democratic Party</td><td>7/4</td><td>+175</td></tr><tr><td>Republican Party</td><td>2/5</td><td>-250</td></tr><tr><td>Any Other Party</td><td>55/1</td><td>+5500</td></tr></tbody></table></td></tr>            </tbody>
        </table>
    </div>
    
