	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenille Turf - To Win">
			<caption>Horses - Breeders Cup Juvenille Turf - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 3, 2018 17:09:10 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenille Turf - To Win  - Nov 03					</th>
			</tr>
        -->
			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr>
    <tr>
      <td>Line Of Duty</td>
      <td>10/1</td>
      <td>+1000</td>
    </tr>
    <tr>
      <td>Uncle Benny</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>Somelikeithotbrown</td>
      <td>12/1</td>
      <td>+1200</td>
    </tr>
    <tr>
      <td>Arthur Kitt</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>War of Will</td>
      <td>12/1</td>
      <td>+1200</td>
    </tr>
    <tr>
      <td>Forty Under </td>
      <td>5/1</td>
      <td>+500</td>
    </tr>
    <tr>
      <td>Current</td>
      <td>5/1</td>
      <td>+500</td>
    </tr>
    <tr>
      <td>The Black Album</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>
    <tr>
      <td>Anthony Van Dyck</td>
      <td>4/1</td>
      <td>+400</td>
    </tr>
    <tr>
      <td>Opry</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>Marie's Diamond</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>King Of Speed</td>
      <td>8/1</td>
      <td>+800</td>
    </tr>
    <tr>
      <td>Henley's Joy</td>
      <td>8/1</td>
      <td>+800</td>
    </tr>
    <tr>
      <td>Much Better </td>
      <td>10/1</td>
      <td>+1000</td>
    </tr> </tbody>		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	