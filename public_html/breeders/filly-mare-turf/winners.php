<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"title="Breeders' Cup Filly and Mare Turf Winners" summary="The past winners of the Breeders Cup Filly and Mare Turf. " ><tr >

<th width="12%" >Year</th><th width="25%" >Winner</th><th width="25%">Jockey</th><th width="25%">Trainer</th><th width="13%">Win Time</th></tr><tr>                                        
<td  >2017</td><td  >Wuheida (GB)</td><td >William Buick</td><td >Charles Appleby</td><td >1:47.91</td></tr><tr>
<td  >2016</td><td  >Queen's Trust</td><td >Frankie Dettori</td><td >Michael Stoute</td><td >1:57.75</td></tr><tr> 
<td  >2015</td><td  >Stephanie's Kitten</td><td >Irad Ortiz</td><td >Chad Brown</td><td >1:56.22</td></tr><tr> 
<td  >2014</td><td  >Dayatthespa</td><td >&nbsp;</td><td >Chad Brown</td><td >2:00.12</td></tr><tr> 
<td  >2013</td><td  >Dank (GB)</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr><tr>                                        
<td  >2012</td><td  >Zagora</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr>										 <tr>                                        
<td  >2011</td><td  >Perfect Shirl</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr>										  <tr>                                        
<td  >2010</td><td  >Shared Account</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr> <tr>                                        
<td  >2009</td><td  >Midday</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr> <tr>                                        
<td  >2008</td><td  >Forever Together</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr> <tr>                                        
<td  >2007</td><td  >Lahudood</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr> <tr>                                        
<td  >2006</td><td  >Ouija Board</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr> <tr>                                        
<td  >2005</td><td  >Intercontinental</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr> <tr>                                        
<td  >2004</td><td  >Ouija Board</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr> <tr>                                        
<td  >2003</td><td  >Islington</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr> <tr>                                        
<td  >2002</td><td  >Starine</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr> <tr>                                        
<td  >2001</td><td  >Banks Hill</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr> <tr>                                        
<td  >2000</td><td  >Perfect Sting</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td></tr> <tr>                                        
<td  >1999</td><td  >Soaring Sofltly</td><td >&nbsp;</td><td >&nbsp;</td><td >&nbsp;</td>

</tr> 										  </table></div>