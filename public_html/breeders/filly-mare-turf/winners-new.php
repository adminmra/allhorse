{literal}

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Breeders' Cup Filly &amp; Mare Turf Winners"
  }
</script>
{/literal}
<div id="no-more-tables">

  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed">

    <thead>
      <tr>
        <th>Year</th>
        <th>Winner</th>
        <th>Age</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th>Win Time</th>
      </tr>
    </thead>
    <tbody>
	  <tr>
        <td data-title="Year">2021</td>
        <td data-title="Winner">Loves Only You(JPN)</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Yuga Kawada</td>
        <td data-title="Trainer">Yoshito Yahagi</td>
        <td data-title="Owner">DMM Dream Club Co.</td>
        <td data-title="Win Time">2:13.87</td>
      </tr>
      <tr>
        <td data-title="Year">2020</td>
        <td data-title="Winner">Audarya (FR)</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Pierre-Charles Boudot</td>
        <td data-title="Trainer">James Fanshawe</td>
        <td data-title="Owner">Mrs. A M Swinburn</td>
        <td data-title="Win Time">1:52.72</td>
      </tr>
      <tr>
        <td data-title="Year">2019</td>
        <td data-title="Winner">Iridessa</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Wayne Lordan</td>
        <td data-title="Trainer">Joseph O’Brien</td>
        <td data-title="Owner">Mrs. C. C. Regalado-Gonzalez</td>
        <td data-title="Win Time">1:57.77</td>
      </tr>
      <tr>
        <td data-title="Year">2018</td>
        <td data-title="Winner">Sistercharlie</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">J. Velazquez</td>
        <td data-title="Trainer">C. Brown</td>
        <td data-title="Owner">Peter M. Brant</td>
        <td data-title="Win Time">2:20.96 </td>
      </tr>
      <tr>
        <td data-title="Year">2017 </td>
        <td data-title="Winner">Wuheida (GB) </td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">William Buick</td>
        <td data-title="Trainer">Charlie Appleby</td>
        <td data-title="Owner">Godolphin</td>
        <td data-title="Win Time">1:47.91 </td>
      </tr>
      <tr>
        <td data-title="Year">2016 </td>
        <td data-title="Winner">Queen's Trust (GB) </td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Frankie Dettori</td>
        <td data-title="Trainer">Sir Michael Stoute</td>
        <td data-title="Owner">Cheveley Park Stud</td>
        <td data-title="Win Time">1:57.75 </td>
      </tr>
      <tr>
        <td data-title="Year">2015 </td>
        <td data-title="Winner">Stephanie's Kitten</td>
        <td data-title="Age">6</td>
        <td data-title="Jockey">Irad Ortiz</td>
        <td data-title="Trainer">Chad Brown</td>
        <td data-title="Owner">Kenneth and Sarah Ramsey</td>
        <td data-title="Win Time">1:56.22 </td>
      </tr>
      <tr>
        <td data-title="Year">2014 </td>
        <td data-title="Winner">Dayatthespa</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Javier Castellano</td>
        <td data-title="Trainer">Chad Brown</td>
        <td data-title="Owner">Jerry Frankel et al. </td>
        <td data-title="Win Time">2:01.40 </td>
      </tr>
      <tr>
        <td data-title="Year">2013 </td>
        <td data-title="Winner">Dank</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Ryan Moore</td>
        <td data-title="Trainer">Sir Michael Stoute</td>
        <td data-title="Owner">James Wigan </td>
        <td data-title="Win Time">1:58.73 </td>
      </tr>
      <tr>
        <td data-title="Year">2012 </td>
        <td data-title="Winner">Zagora</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Javier Castellano</td>
        <td data-title="Trainer">Chad Brown</td>
        <td data-title="Owner">Martin S. Schwartz</td>
        <td data-title="Win Time">1:59.70 </td>
      </tr>
      <tr>
        <td data-title="Year">2011 </td>
        <td data-title="Winner">Perfect Shirl</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Roger Attfield</td>
        <td data-title="Owner">Charles E. Fipke</td>
        <td data-title="Win Time">2:18.62 </td>
      </tr>
      <tr>
        <td data-title="Year">2010 </td>
        <td data-title="Winner">Shared Account</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Edgar Prado</td>
        <td data-title="Trainer">Graham Motion</td>
        <td data-title="Owner">Sagamore Farm</td>
        <td data-title="Win Time">2:17.74 </td>
      </tr>
      <tr>
        <td data-title="Year">2009 </td>
        <td data-title="Winner">Midday</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Tom Queally</td>
        <td data-title="Trainer">Henry Cecil</td>
        <td data-title="Owner">Juddmonte Farms</td>
        <td data-title="Win Time">1:59.14 </td>
      </tr>
      <tr>
        <td data-title="Year">2008 </td>
        <td data-title="Winner">Forever Together</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Julien Leparoux</td>
        <td data-title="Trainer">Jonathan Sheppard</td>
        <td data-title="Owner">Augustin Stable</td>
        <td data-title="Win Time">2:01.58 </td>
      </tr>
      <tr>
        <td data-title="Year">2007 </td>
        <td data-title="Winner">Lahudood</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Alan Garcia</td>
        <td data-title="Trainer">Kiaran McLaughlin</td>
        <td data-title="Owner">Shadwell Racing</td>
        <td data-title="Win Time">2:22.75 </td>

      </tr>
      <tr>
        <td data-title="Year">2006 </td>
        <td data-title="Winner">Ouija Board</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Frankie Dettori</td>
        <td data-title="Trainer">Ed Dunlop</td>
        <td data-title="Owner">Lord Derby</td>
        <td data-title="Win Time">2:14.55 </td>
      </tr>
      <tr>
        <td data-title="Year">2005 </td>
        <td data-title="Winner">Intercontinental</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Rafael Bejarano</td>
        <td data-title="Trainer">Robert J. Frankel</td>
        <td data-title="Owner">Juddmonte Farms</td>
        <td data-title="Win Time">2:02.34 </td>
      </tr>
      <tr>
        <td data-title="Year">2004 </td>
        <td data-title="Winner">Ouija Board</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Kieren Fallon</td>
        <td data-title="Trainer">Ed Dunlop</td>
        <td data-title="Owner">Lord Derby</td>
        <td data-title="Win Time">2:18.25 </td>
      </tr>
      <tr>
        <td data-title="Year">2003 </td>
        <td data-title="Winner">Islington</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Kieren Fallon</td>
        <td data-title="Trainer">Sir Michael Stoute</td>
        <td data-title="Owner">Ballymacoll Stud</td>
        <td data-title="Win Time">1:59.13 </td>
      </tr>
      <tr>
        <td data-title="Year">2002 </td>
        <td data-title="Winner">Starine</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Robert J. Frankel</td>
        <td data-title="Owner">Robert J. Frankel</td>
        <td data-title="Win Time">2:03.57 </td>
      </tr>
      <tr>
        <td data-title="Year">2001 </td>
        <td data-title="Winner">Banks Hill</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Olivier Peslier</td>
        <td data-title="Trainer">Andre Fabre</td>
        <td data-title="Owner">Juddmonte Farms</td>
        <td data-title="Win Time">2:00.36 </td>
      </tr>
      <tr>
        <td data-title="Year">2000 </td>
        <td data-title="Winner">Perfect Sting</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">Joe Orseno</td>
        <td data-title="Owner">Stronach Stables</td>
        <td data-title="Win Time">2:13.07 </td>
      </tr>
      <tr>
        <td data-title="Year">1999 </td>
        <td data-title="Winner">Soaring Softly</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">James J. Toner</td>
        <td data-title="Owner">Phillips Racing Partnership</td>
        <td data-title="Win Time">2:13.89 </td>
      </tr>
    </tbody>
  </table>

</div>
{literal}
<style type="text/css">
  .table>tbody>tr th {
    text-align: center !important
  }
</style>
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>


{/literal}