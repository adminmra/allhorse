	{literal}
	<script type="application/ld+json">
	    {
	        "@context": "http://schema.org",
	        "@type": "Table",
	        "about": "Breeders' Cup Filly Mare Turf Odds"
	    }
	</script>
	{/literal}
	<div id="no-more-tables">
	    <table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
	        cellpadding="0" cellspacing="0" border="0" title="2020 Breeders' Cup Filly &amp; Mare Turf"
	        summary="The latest odds for the Breeders' Cup Filly &amp; Mare Turf available">

	        <caption class="hide-sm">2020 Breeders' Cup Filly &amp; Mare Turf Odds and Post Position</caption>


	        <tbody>
	            <tr class='head_title hide-sm'>
	                <th>PP</th>
	                <th>Horse</th>
	                <th>Odds</th>
	                <th>Trainer</th>
	                <th>Jockey</th>
	            </tr>
	            <tr>
	                <td data-title="PP">11</td>
	                <td data-title="Horse">Audarya (FR)</td>
	                <td data-title="Odds">12-1</td>
	                <td data-title="Trainer">James R. Fanshawe</td>
	                <td data-title="Jockey">Pierre-Charles Boudot</td>
	            </tr>
	            <tr>
	                <td data-title="PP">14</td>
	                <td data-title="Horse">Cayenne Pepper (IRE)</td>
	                <td data-title="Odds">8-1</td>
	                <td data-title="Trainer">Mrs. John Harrington</td>
	                <td data-title="Jockey">Shane Foley</td>
	            </tr>
	            <tr>
	                <td data-title="PP">10</td>
	                <td data-title="Horse">Civil Union</td>
	                <td data-title="Odds">12-1</td>
	                <td data-title="Trainer">Claude R. McGaughey III</td>
	                <td data-title="Jockey">Joel Rosario</td>
	            </tr>
	            <tr>
	                <td data-title="PP">9</td>
	                <td data-title="Horse">Harvey's Lil Goil</td>
	                <td data-title="Odds">20-1</td>
	                <td data-title="Trainer">William I. Mott</td>
	                <td data-title="Jockey">Junior Alvarado</td>
	            </tr>
	            <tr>
	                <td data-title="PP">12</td>
	                <td data-title="Horse">Lady Prancealot (IRE)</td>
	                <td data-title="Odds">30-1</td>
	                <td data-title="Trainer">Richard Baltas</td>
	                <td data-title="Jockey">Manuel Franco</td>
	            </tr>
	            <tr>
	                <td data-title="PP">4</td>
	                <td data-title="Horse">Mean Mary</td>
	                <td data-title="Odds">7-2</td>
	                <td data-title="Trainer">H. Graham Motion</td>
	                <td data-title="Jockey">Luis Saez</td>
	            </tr>
	            <tr>
	                <td data-title="PP">8</td>
	                <td data-title="Horse">Mucho Unusual</td>
	                <td data-title="Odds">30-1</td>
	                <td data-title="Trainer">Tim Yakteen</td>
	                <td data-title="Jockey">Flavien Prat</td>
	            </tr>
	            <tr>
	                <td data-title="PP">5</td>
	                <td data-title="Horse">My Sister Nat (FR)</td>
	                <td data-title="Odds">12-1</td>
	                <td data-title="Trainer">Chad C. Brown</td>
	                <td data-title="Jockey">Jose Ortiz</td>
	            </tr>
	            <tr>
	                <td data-title="PP">3</td>
	                <td data-title="Horse">Peaceful (IRE)</td>
	                <td data-title="Odds">12-1</td>
	                <td data-title="Trainer">Aidan P. O'Brien</td>
	                <td data-title="Jockey">Ryan Moore</td>
	            </tr>
	            <tr>
	                <td data-title="PP">6</td>
	                <td data-title="Horse">Rushing Fall</td>
	                <td data-title="Odds">5-2</td>
	                <td data-title="Trainer">Chad C. Brown</td>
	                <td data-title="Jockey">Javier Castellano</td>
	            </tr>
	            <tr>
	                <td data-title="PP">2</td>
	                <td data-title="Horse">Sistercharlie (IRE)</td>
	                <td data-title="Odds">6-1</td>
	                <td data-title="Trainer">Chad C. Brown</td>
	                <td data-title="Jockey">John Velazquez</td>
	            </tr>
	            <tr>
	                <td data-title="PP">1</td>
	                <td data-title="Horse">Starship Jubilee</td>
	                <td data-title="Odds">10-1</td>
	                <td data-title="Trainer">Kevin Attard</td>
	                <td data-title="Jockey">Florent Geroux</td>
	            </tr>
	            <tr>
	                <td data-title="PP">13</td>
	                <td data-title="Horse">Nay Lady Nay (IRE)</td>
	                <td data-title="Odds">20-1</td>
	                <td data-title="Trainer">Irad Ortiz Jr</td>
	                <td data-title="Jockey"></td>
	            </tr>
	            <tr>
	                <td data-title="PP">7</td>
	                <td data-title="Horse">Terebellum (IRE)</td>
	                <td data-title="Odds">20-1</td>
	                <td data-title="Trainer">John H.M. Gosden</td>
	                <td data-title="Jockey">Lanfranco Dettori</td>
	            </tr>
	        </tbody>
	    </table>
	</div>
	{literal}
	<style>
	    @media screen and (max-width: 640px) {
	        .fa-sort {
	            display: block !important;
	        }
	    }
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}