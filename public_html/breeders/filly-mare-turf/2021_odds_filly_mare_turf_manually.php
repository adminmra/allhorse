{literal}
	<script type="application/ld+json">
	    {
	        "@context": "http://schema.org",
	        "@type": "Table",
	        "about": "Breeders' Cup Filly Mare Turf Odds"
	    }
	</script>
	{/literal}
	<div id="no-more-tables">
	    <table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
	        cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Filly &amp; Mare Turf"
	        summary="The latest odds for the Breeders' Cup Filly &amp; Mare Turf available">

	        <caption class="hide-sm">2021 Breeders' Cup Filly &amp; Mare Turf Odds and Post Position</caption>


	        <tbody>
	            <tr class='head_title hide-sm'>
	                <th>PP</th>
	                <th>Horse</th>
	                <th>Odds</th>
	                <th>Trainer</th>
	                <th>Jockey</th>
	            </tr>
	            <tr>
	                <td data-title="PP">1</td>
	                <td data-title="Horse">Going to Vegas</td>
	                <td data-title="Odds">12-1</td>
	                <td data-title="Trainer">Richard Baltas</td>
	                <td data-title="Jockey">Umberto Rispoli</td>
	            </tr>
	            <tr>
	                <td data-title="PP">2</td>
	                <td data-title="Horse">Pocket Square(GB)</td>
	                <td data-title="Odds">15-1</td>
	                <td data-title="Trainer">Chad Brown</td>
	                <td data-title="Jockey">Irad Ortiz, Jr.</td>
	            </tr>
	            <tr>
	                <td data-title="PP">3</td>
	                <td data-title="Horse">Acanella(GB)</td>
	                <td data-title="Odds">12-1</td>
	                <td data-title="Trainer">Colin Keane</td>
	                <td data-title="Jockey">Ger Lyons</td>
	            </tr>
	            <tr>
	                <td data-title="PP">4</td>
	                <td data-title="Horse">Rougir(FR)</td>
	                <td data-title="Odds">6-1</td>
	                <td data-title="Trainer">Cedric Rossi</td>
	                <td data-title="Jockey">Maxine Guyon</td>
	            </tr>
	            <tr>
	                <td data-title="PP">5</td>
	                <td data-title="Horse">Queen Supreme</td>
	                <td data-title="Odds">20-1</td>
	                <td data-title="Trainer">Andrew Balding</td>
	                <td data-title="Jockey">James Doyle</td>
	            </tr>
	            <tr>
	                <td data-title="PP">6</td>
	                <td data-title="Horse">Love(IRE)</td>
	                <td data-title="Odds">7-2</td>
	                <td data-title="Trainer">Aidan O'Brien</td>
	                <td data-title="Jockey">Ryan Moore</td>
	            </tr>
	            <tr>
	                <td data-title="PP">7</td>
	                <td data-title="Horse">War Like Goddess</td>
	                <td data-title="Odds">7-2</td>
	                <td data-title="Trainer">William Mott</td>
	                <td data-title="Jockey">Julien Leparoux</td>
	            </tr>
	            <tr>
	                <td data-title="PP">8</td>
	                <td data-title="Horse">Loves Only You (JPN)</td>
	                <td data-title="Odds">4-1</td>
	                <td data-title="Trainer">Yoshito Yahagi</td>
	                <td data-title="Jockey">Yuga Kawada</td>
	            </tr>
	            <tr>
	                <td data-title="PP">9</td>
	                <td data-title="Horse">My Sister Nat(FR)</td>
	                <td data-title="Odds">15-1</td>
	                <td data-title="Trainer">Chad Brown</td>
	                <td data-title="Jockey">Jose Ortiz</td>
	            </tr>
	            <tr>
	                <td data-title="PP">10</td>
	                <td data-title="Horse">Ocean Road (IRE)</td>
	                <td data-title="Odds">20-1</td>
	                <td data-title="Trainer">Hugo Palmer</td>
	                <td data-title="Jockey">Oisin Murphy</td>
	            </tr>
	            <tr>
	                <td data-title="PP">11</td>
	                <td data-title="Horse">Dogtag</td>
	                <td data-title="Odds">30-1</td>
	                <td data-title="Trainer">Richard Mandella</td>
	                <td data-title="Jockey">Flavien Prat</td>
	            </tr>
	            <tr>
	                <td data-title="PP">12</td>
	                <td data-title="Horse">Audarya(FR)</td>
	                <td data-title="Odds">12-1</td>
	                <td data-title="Trainer">James R. Fanshawe</td>
	                <td data-title="Jockey">William Buick</td>
	            </tr>
	            <tr>
	                
	  
	        </tbody>
	    </table>
	</div>
	{literal}
	<style>
	    @media screen and (max-width: 640px) {
	        .fa-sort {
	            display: block !important;
	        }
	    }
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}