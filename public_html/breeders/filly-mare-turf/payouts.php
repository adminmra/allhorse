<h2>2021 Breeders' Cup Filly &amp; Mare Turf Winners Payouts
</h2>
<div>
    <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
        title="Breeders' Filly & Mare Turf Payouts"
        summary="Last year's Payouts of the Breeders' Cup Filly & Mare Turf. ">
        <tbody>
            <tr>
                <th width="5%">PP</th>
                <th width="35%">Horses</th>
                <th width="10%">Win</th>
                <th width="10%">Place</th>
                <th width="10%">Show</th>
            </tr>
            <tr>
                <td>8</td>
                <td class="name">Loves Only You (JPN)</td>
                <td>
                    <div class="box">$10.60</div>
                </td>
                <td>
                    <div class="box">$6.20</div>
                </td>
                <td class="payoff">
                    <div class="box">$4.00</div>
                </td>
            </tr>
            <tr>
                <td>9</td>
                <td class="name">My Sister Nat (FR)</td>
                <td></td>
                <td>
                    <div class="box">$17.60</div>
                </td>
                <td class="payoff">
                    <div class="box">$8.80</div>
                </td>
            </tr>
            <tr>
                <td>7</td>
                <td class="name">War Like Goddess</td>
                <td></td>
                <td></td>
                <td class="payoff">
                    <div class="box">$3.00</div>
                </td>
            </tr>

        </tbody>
    </table>
</div>

<p></p>

<div>
    <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
        title="Breeders' Filly & Mare Turf Payouts"
        summary="Last year's Payouts of the Breeders' Cup Filly & Mare Turf. ">
        <tbody>
            <tr>
                <th width="20%">Wager</th>
                <th width="20%">Horses</th>
                <th width="20%">Payout</th>
            </tr>
            <tr class="odd">
                <td>$1.00 Exacta</td>
                <td>8-9</td>
                <td class="payoff">$156.30</td>
            </tr>
            <tr class="even">
                <td>$0.50 Trifecta</td>
                <td>8-9-7</td>
                <td class="payoff">$233.30</td>
            </tr>
            <tr class="odd">
                <td>$0.10 Superfecta</td>
                <td>8-9-7-6</td>
                <td class="payoff">$273.20</td>
            </tr>
            <tr class="even">
                <td>$1.00 Double</td>
                <td>5/8</td>
                <td class="payoff">$8.60</td>
            </tr>
            <tr class="odd">
                <td>$0.50 Pick 3</td>
                <td>3/5/8</td>
                <td class="payoff">$26.75</td>
            </tr>



        </tbody>
    </table>
</div>