<h2>2021 Breeders' Cup Filly &amp; Mare Turf Winners
  Results
</h2>
<div id="no-more-tables">
  <table border="0" cellspacing="0" cellpadding="0"
    class="table table-condensed table-striped table-bordered ordenableResult"
    title="Breeders' Cup Filly and Mare Turf Results"
    summary="Last year's results of the Breeders' Cup Filly and Mare Turf. ">
    <thead>
      <tr>
        <th>Results</th>
        <th>PP</th>
        <th>Horse</th>
        <th>Trainer</th>
        <th>Jockey</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Result">1</td>
        <td data-title="PP">8</td>
        <td data-title="Horse">Loves Only You(JPN)</td>
        <td data-title="Trainer">Yoshito Yahagi</td>
        <td data-title="Jockey">Yuga Kawada</td>
      </tr>
      <tr>
        <td data-title="Result">2</td>
        <td data-title="PP">9</td>
        <td data-title="Horse">My Sister Nat (FR)</td>
        <td data-title="Trainer">Chad C. Brown</td>
        <td data-title="Jockey">Jose Ortiz</td>
      </tr>

      <tr>
        <td data-title="Result">3</td>
        <td data-title="PP">7</td>
        <td data-title="Horse">War Like Goddess</td>
        <td data-title="Trainer">William I. Mott</td>
        <td data-title="Jockey">Julien Leparoux</td>
      </tr>
    </tbody>
  </table>
</div>
<script src="/assets/js/sorttable_results.js"></script>