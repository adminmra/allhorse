	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Filly And Mare Turf - To Win">
			<caption>Horses - Breeders Cup Filly And Mare Turf - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 3, 2018 17:12:11 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Filly And Mare Turf - To Win  - Nov 02					</th>
			</tr>
        -->
			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr>
	    <tr>
      <td>Sistercharlie</td>
      <td>3/1</td>
      <td>+300</td>
    </tr>
    <tr>
      <td>Wild Illusion </td>
      <td>7/2</td>
      <td>+350</td>
    </tr>
    <tr>
      <td>A Raving Beauty</td>
      <td>10/1</td>
      <td>+1000</td>
    </tr>
    <tr>
      <td>Magic Wand</td>
      <td>5/1</td>
      <td>+500</td>
    </tr>
    <tr>
      <td>Athena</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>Eziyra</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>Fourstar Crook</td>
      <td>5/1</td>
      <td>+500</td>
    </tr>
    <tr>
      <td>Mom's On Strike</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Princess Yaiza</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>
    <tr>
      <td>Santa Monica</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>Fuhriously Kissed</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>
    <tr>
      <td>Paved </td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Thais</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Smart Choice</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr></tbody>
		</table>
	</div>
{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sort_table.js"></script>
{/literal}
	