{literal}
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "about": "Breeders' Cup Juvenile Fillies Odds"
    }
</script>
{/literal}
<div id="no-more-tables">
    <table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
        cellpadding="0" cellspacing="0" border="0" title="2020 Breeders' Cup Juvenile Fillies"
        summary="The latest odds for the Breeders' Cup Juvenile Fillies available">

        <caption class="hide-sm">2020 Breeders' Cup Juvenile Fillies Odds and Post Position</caption>


        <tbody>
            <tr class='head_title hide-sm'>
                <th>PP</th>
                <th>Horse</th>
                <th>Odds</th>
                <th>Trainer</th>
                <th>Jockey</th>
            </tr>
            <tr>
                <td data-title="PP">6</td>
                <td data-title="Horse">Crazy Beautiful</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">Kenneth G. McPeek</td>
                <td data-title="Jockey">Brian Hernandez Jr.</td>
            </tr>
            <tr>
                <td data-title="PP">3</td>
                <td data-title="Horse">Dayoutoftheoffice</td>
                <td data-title="Odds">5-2</td>
                <td data-title="Trainer">Timothy E. Hamm</td>
                <td data-title="Jockey">Junior Alvarado</td>
            </tr>
            <tr>
                <td data-title="PP">5</td>
                <td data-title="Horse">Girl Daddy</td>
                <td data-title="Odds">6-1</td>
                <td data-title="Trainer">Dale L. Romans</td>
                <td data-title="Jockey">Joseph Talamo</td>
            </tr>
            <tr>
                <td data-title="PP">7</td>
                <td data-title="Horse">Princess Noor</td>
                <td data-title="Odds">9-5</td>
                <td data-title="Trainer">Bob Baffert</td>
                <td data-title="Jockey">Victor Espinoza</td>
            </tr>
            <tr>
                <td data-title="PP">1</td>
                <td data-title="Horse">Simply Ravishing</td>
                <td data-title="Odds">5-2</td>
                <td data-title="Trainer">Kenneth G. McPeek</td>
                <td data-title="Jockey">Luis Saez</td>
            </tr>
            <tr>
                <td data-title="PP">4</td>
                <td data-title="Horse">Thoughtfully</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">Steven M. Asmussen</td>
                <td data-title="Jockey">Ricardo Santana Jr.</td>
            </tr>
            <tr>
                <td data-title="PP">2</td>
                <td data-title="Horse">Vequist</td>
                <td data-title="Odds">8-1</td>
                <td data-title="Trainer">Robert E. Reid Jr.</td>
                <td data-title="Jockey">Joel Rosario</td>
            </tr>
            <tr>
                <td data-title="PP"></td>
                <td data-title="Horse"></td>
                <td data-title="Odds"></td>
                <td data-title="Trainer"></td>
                <td data-title="Jockey"></td>
            </tr>
        </tbody>
    </table>
</div>
{literal}
<style>
    @media screen and (max-width: 640px) {
        .fa-sort {
            display: block !important;
        }
    }
</style>
<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sorttable_post.js"></script> {/literal}