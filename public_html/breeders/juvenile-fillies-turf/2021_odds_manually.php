{literal}
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "about": "Breeders' Cup Juvenile Fillies Turf Odds"
    }
</script>
{/literal}
<div id="no-more-tables">
    <table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
        cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Juvenile Fillies Turf"
        summary="The latest odds for the Breeders' Cup Juvenile Fillies Turf available">

        <caption class="hide-sm">2021 Breeders' Cup Juvenile Fillies Turf Odds and Post Position</caption>


        <tbody>
            <tr class='head_title hide-sm'>
                <th>PP</th>
                <th>Horse</th>
                <th>Odds</th>
                <th>Trainer</th>
                <th>Jockey</th>
            </tr>
            <tr>
                <td data-title="PP">1</td>
                <td data-title="Horse">Pizza Bianca</td>
                <td data-title="Odds">5-1</td>
                <td data-title="Trainer">Christophe Clement</td>
                <td data-title="Jockey">Jose L. Ortiz</td>
            </tr>
            <tr>
                <td data-title="PP">2</td>
                <td data-title="Horse">Cairo Memories</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">Robert Hess, Jr.</td>
                <td data-title="Jockey">Kent Desormeaux</td>
            </tr>
            <tr>
                <td data-title="PP">3</td>
                <td data-title="Horse">Cachet (IRE)</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">George Boughey</td>
                <td data-title="Jockey">Luis Saez</td>
            </tr>
            <tr>
                <td data-title="PP">4</td>
                <td data-title="Horse">Turnerloose</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">Brad Cox</td>
                <td data-title="Jockey">Florent Geroux</td>
            </tr>
            <tr>
                <td data-title="PP">5</td>
                <td data-title="Horse">Bubble Rock</td>
                <td data-title="Odds">8-1</td>
                <td data-title="Trainer">Brad Cox</td>
                <td data-title="Jockey">Irad Ortiz, Jr.</td>
            </tr>
            <tr>
                <td data-title="PP">6</td>
                <td data-title="Horse">Hello You(IRE)</td>
                <td data-title="Odds">10-1</td>
                <td data-title="Trainer"> D Loughnane</td>
                <td data-title="Jockey">John R. Velazquez</td>
            </tr>
            <tr>
                <td data-title="PP">7</td>
                <td data-title="Horse">Consumer Spending</td>
                <td data-title="Odds">8-1</td>
                <td data-title="Trainer">Chad Brown</td>
                <td data-title="Jockey">Flavien Prat</td>
            </tr>
            <tr>
                <td data-title="PP">8</td>
                <td data-title="Horse">Sail By</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">Leah Gyarmati</td>
                <td data-title="Jockey">Junior Alvarado</td>
            </tr>
            <tr>
                <td data-title="PP">9</td>
                <td data-title="Horse">Koala Princess</td>
                <td data-title="Odds">6-1</td>
                <td data-title="Trainer">Arnaud Delacour</td>
                <td data-title="Jockey">Joel Rosario</td>
            </tr>
            <tr>
                <td data-title="PP">10</td>
                <td data-title="Horse">Helens Well (IRE)</td>
                <td data-title="Odds">30-1</td>
                <td data-title="Trainer">Philip D'Amato</td>
                <td data-title="Jockey">Umberto Rispoli</td>
            </tr>
            <tr>
                <td data-title="PP">11</td>
                <td data-title="Horse">Haughty</td>
                <td data-title="Odds">30-1</td>
                <td data-title="Trainer">Tyler Gaffalione</td>
                <td data-title="Jockey">Chad Brown</td>
            </tr>
            <tr>
                <td data-title="PP">12</td>
                <td data-title="Horse">Malavath (IRE)</td>
                <td data-title="Odds">8-1</td>
                <td data-title="Trainer">Francis-Henri Graffard</td>
                <td data-title="Jockey">Olivier Peslier</td>
            </tr>
            <tr>
                <td data-title="PP">13</td>
                <td data-title="Horse">Mise En Scene (GB)</td>
                <td data-title="Odds">6-1</td>
                <td data-title="Trainer">James Ferguson</td>
                <td data-title="Jockey">Oisin Murphy</td>
            </tr>
            <tr>
                <td data-title="PP">14</td>
                <td data-title="Horse">California Angel</td>
                <td data-title="Odds">8-1</td>
                <td data-title="Trainer">George Leonard, III</td>
                <td data-title="Jockey">Rafael Bejarano</td>
            </tr>
        </tbody>
    </table>
</div>
{literal}
<style>
    @media screen and (max-width: 640px) {
        .fa-sort {
            display: block !important;
        }
    }
</style>
<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sorttable_post.js"></script> {/literal}