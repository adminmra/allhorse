<h2>2021 Breeders' Cup Juvenile Fillies Turf Payouts</h2>
<div id="no-more-tables">
        <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
                title="Breeders' Juvenile Fillies Turf Payouts"
                summary="Last year's Payouts of the Breeders' Cup Juvenile Fillies Turf. ">
                <thead>
                        <tr>
                                <th>PP</th>
                                <th>Horses</th>
                                <th>Win</th>
                                <th>Place</th>
                                <th>Show</th>
                        </tr>
                </thead>
                <tbody>
                        <tr>
                                <td data-title="PP">1</td>
                                <td data-title="Horse">Pizza Bianca</td>
                                <td data-title="Win">$21.80</td>
                                <td data-title="Place">$10.40</td>
                                <td data-title="Show">$6.60</td>
                        </tr>
                        <tr>
                                <td data-title="PP">12</td>
                                <td data-title="Horse">Malavath (IRE)</td>
                                <td data-title="Win"></td>
                                <td data-title="Place">$19.00</td>
                                <td data-title="Show">$12.00</td>
                        </tr>
                        <tr>
                                <td data-title="PP">11</td>
                                <td data-title="Horse">Haughty</td>
                                <td data-title="Win"></td>
                                <td data-title="Place"></td>
                                <td data-title="Show">$5.00</td>
                        </tr>
                </tbody>
        </table>
</div>
<p></p>

<div>
        <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
                title="Breeders' Juvenile Fillies Turf Payouts"
                summary="Last year's Payouts of the Breeders' Cup Juvenile Fillies Turf. ">
                <tbody>
                        <thead>
                                <tr>
                                        <th width="20%">Wager</th>
                                        <th width="20%">Horses</th>
                                        <th width="20%">Payout</th>
                                </tr>
                        </thead>
                        <tr>

                        <tr class="odd">
                                <td>$1.00 Exacta</td>
                                <td>1-12</td>
                                <td class="payoff">$197.00</td>
                        </tr>
                        <tr class="even">
                                <td>$0.50 Trifecta</td>
                                <td>1-12-11</td>
                                <td class="payoff">$878.10</td>
                        </tr>
                        <tr class="odd">
                                <td>$0.10 Superfecta</td>
                                <td>1-12-11-3</td>
                                <td class="payoff">$2092.96</td>
                        </tr>
                        <tr class="even">
                                <td>$1.00 Double</td>
                                <td>6/1</td>
                                <td class="payoff">$22.20</td>
                        </tr>
                        <tr class="odd">
                                <td>$0.50 Pick 3</td>
                                <td>6/6/1</td>
                                <td class="payoff">$54.30</td>
                        </tr>

                </tbody>
        </table>
</div>