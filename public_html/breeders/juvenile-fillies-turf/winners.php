<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"  title="Breeders' Cup Juvenile Fillies Turf Winners" summary="The past winners of the Breeders Cup Juvenile Fillies Turf. " >

  <tbody>
  				  <tr>
                    <th >Year</th>
                    <th>Winner</th>
                    <th>Jockey</th>
                    <th>Trainer</th>
                    <th>Owner</th>
                    <th>Win Time</th>
				  </tr>
<tr>                                        
<td >2018</td>
<td  >Newspaperofrecord</td>
<td >I. Ortiz, Jr.</td>
<td >Chad C. Brown</td>
<td>Klaravich Stables, Inc. </td>
<td >1:39.00 </td>
</tr>
<tr> 
  
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Rushing Fall </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Chad Brown</td>
      <td data-title="Owner">e Five Racing Thoroughbreds </td>
      <td data-title="Win Time">1:36.09 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">New Money Honey</td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Chad Brown</td>
      <td data-title="Owner">E Five Racing Thoroughbreds </td>
      <td data-title="Win Time">1:34.01 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Catch A Glimpse</td>
      <td data-title="Jockey">Florent Geroux</td>
      <td data-title="Trainer">Mark Casse</td>
      <td data-title="Owner">Gary Barber, Michael Ambler &amp; Windways Farm </td>
      <td data-title="Win Time">1:39.08 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Lady Eli</td>
      <td data-title="Jockey">Irad Ortiz Jr.</td>
      <td data-title="Trainer">Chad Brown</td>
      <td data-title="Owner">Sheep Pond Partners</td>
      <td data-title="Win Time">1:33.41 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Chriselliam (IRE) </td>
      <td data-title="Jockey">Richard Hughes</td>
      <td data-title="Trainer">Charles Hills</td>
      <td data-title="Owner">C Wright</td>
      <td data-title="Win Time">1:33.72 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Flotilla (FRA) </td>
      <td data-title="Jockey">Christophe Lemaire</td>
      <td data-title="Trainer">Mikel Delzangles</td>
      <td data-title="Owner">Sheikh Mohammed Al Thani </td>
      <td data-title="Win Time">1:34.64 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Stephanie's Kitten</td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Wayne Catalano</td>
      <td data-title="Owner">Ken &amp; Sarah Ramsey</td>
      <td data-title="Win Time">1:38.90 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">More Than Real</td>
      <td data-title="Jockey">Garrett K. Gomez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Bobby Flay</td>
      <td data-title="Win Time">1:36.61 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Tapitsfly</td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Dale Romans</td>
      <td data-title="Owner">Frank L. Jones, Jr. </td>
      <td data-title="Win Time">1:34.25 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Maram</td>
      <td data-title="Jockey">Jose Lezcano</td>
      <td data-title="Trainer">Chad Brown</td>
      <td data-title="Owner">Karen N. Woods &amp; Saud bin Khaled </td>
      <td data-title="Win Time">1:35.15 </td>
    </tr>
  </tbody>
										  
										 										  </table></div>
{literal}
<style type="text/css" >
.table> tbody > tr th {
    text-align: center!important
}
</style>
{/literal}
