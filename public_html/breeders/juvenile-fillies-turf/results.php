<h2>2021 Breeders' Cup Juvenile Fillies Turf Results</h2>
<div id="no-more-tables">
        <table cellspacing="0" cellpadding="0"
                class="table table-condensed table-striped table-bordered ordenableResult"
                title="Breeders' Cup Juvenile Fillies Turf Results"
                summary="Last year's results of the Breeders' Cup Juvenile Fillies Turf. ">
                <thead>

                        <tr>
                                <th>Results</th>
                                <th>PP</th>
                                <th>Horse</th>
                                <th>Trainer</th>
                                <th>Jockey</th>
                        </tr>
                </thead>
                <tbody>

                        <tr>
                                <td data-title="Result">1</td>
                                <td data-title="PP">1</td>
                                <td data-title="Horse">Pizza Bianca</td>
                                <td data-title="Trainer">Christophe Clement</td>
                                <td data-title="Jockey">Jose L. Ortiz</td>
                        </tr>
                        <tr>
                                <td data-title="Result">2</td>
                                <td data-title="PP">12</td>
                                <td data-title="Horse">Malavath (IRE)</td>
                                <td data-title="Trainer">Francis-Henri Graffard</td>
                                <td data-title="Jockey">Ryan Moore</td>
                        </tr>

                        <tr>
                                <td data-title="Result">3</td>
                                <td data-title="PP">11</td>
                                <td data-title="Horse">Haughty</td>
                                <td data-title="Trainer">Chad Brown</td>
                                <td data-title="Jockey">Tyler Gaffalione</td>
                        </tr>
        </table>
</div>
<script src="/assets/js/sorttable_results.js"></script>