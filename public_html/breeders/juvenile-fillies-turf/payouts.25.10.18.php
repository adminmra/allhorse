<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" title="Breeders' Juvenile Fillies Turf Payouts" summary="Last year's Payouts of the Breeders' Cup Juvenile Fillies Turf. "  >
<tbody><tr>
    <th width="5%"> </th><th width="35%"> </th><th width="10%">Win</th><th width="10%">Place</th><th width="10%">Show</th>
    </tr>
        <tr>
            <td>3</td>  
          <td class="name">New Money Honey</td>
          <td><div class="box">$15.00</div></td>
          <td><div class="box">$7.60</div></td>
          <td class="payoff"><div class="box">$5.20</div></td>
        </tr>
        <tr>
            <td>6</td>
          <td class="name">Coasted</td>
          <td></td>
          <td><div class="box">$17.40</div></td>
          <td class="payoff"><div class="box">$10.20</div></td>
        </tr>
        <tr>
            <td>9</td>
          <td class="name">Cavale Doree (FR)</td>
          <td></td>
          <td></td>
          <td class="payoff"><div class="box">$10.60.20</div></td>
        </tr>
    
    
  </tbody>
</table></div>
<p></p>
<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"  title="Breeders' Juvenile Fillies Turf Payouts" summary="Last year's Payouts of the Breeders' Cup Juvenile Fillies Turf. " >
<tbody><tr>
							 <th width="20%">Wager</th>
								 <th width="20%">Horses</th><th width="20%">Payout</th></tr>
 <tr>

      <tr class="odd">
      <td>Exacta</td>
      <td>3-6</td>
      <td class="payoff">$127.40</td>
    </tr>
      <tr class="even">
      <td>Trifecta</td>
      <td>3-6-9</td>
      <td class="payoff">$1,163.45</td>
    </tr>
      <tr class="odd">
      <td>Superfecta</td>
      <td>3-6-9-2</td>
      <td class="payoff">$27,971.40</td>
    </tr>
      <tr class="even">
      <td>Daily Double</td>
      <td>8-3</td>
      <td class="payoff">$209.00</td>
    </tr>
      <tr class="odd">
      <td>Pick 3</td>
      <td>13-8-3</td>
      <td class="payoff">$892.60</td>
    </tr>
 
</tbody>
</table> </div>