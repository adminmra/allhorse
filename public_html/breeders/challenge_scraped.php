		<table width="100%" class="table table-condensed table-striped table-bordered" id="infoEntries" >

			<tr >
				<th width="15%" >Date</th>
				<th>Race</th>
				<th>Track</th>
				<!--th >Distance</th>
				<th >Grade</th>
				<th >Age</th>
				-->
				<th >Qualifier</th>
				<th >Winner</th>
			</tr>
			<tr>
				<td  >Jan 7, 2017</td>
				<td >
					L'Ormarins Queen's Plate				</td>
				<td >
					KEN				</td>
				<td >
					Mile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Legal Eagle (SAF)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jan 7, 2017</td>
				<td >
					Maine Chance Farms Paddock S.				</td>
				<td >
					KEN				</td>
				<td >
					Filly & Mare Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Bela-Bela (SAF)</td>
			</tr>

			<tr>
				<td  >Feb 19, 2017</td>
				<td >
					February S.				</td>
				<td >
					TOK				</td>
				<td >
					Classic
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Gold Dream (JPN)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Apr 1, 2017</td>
				<td >
					Darley TJ Smith Stakes				</td>
				<td >
					RAN				</td>
				<td >
					Turf Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Chautauqua (AUS)</td>
			</tr>

			<tr>
				<td  >Apr 1, 2017</td>
				<td >
					The Star Doncaster Mile				</td>
				<td >
					RAN				</td>
				<td >
					Mile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Somewhat</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Apr 8, 2017</td>
				<td >
					Coolmore Legacy Queen of the Turf Stakes				</td>
				<td >
					RAN				</td>
				<td >
					Filly & Mare Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Foxplay (AUS)</td>
			</tr>

			<tr>
				<td  >May 1, 2017</td>
				<td >
					Gran Premio Criadores				</td>
				<td >
					PAL				</td>
				<td >
					Distaff
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Kiriaki (ARG)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >May 25, 2017</td>
				<td >
					Gran Premio 25 de Mayo				</td>
				<td >
					SIS				</td>
				<td >
					Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Ordak Dan (ARG)</td>
			</tr>

			<tr>
				<td  >May 28, 2017</td>
				<td >
					Gran Premio Club Hipico De Santiago Falabella				</td>
				<td >
					CHS				</td>
				<td >
					Mile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Top Casablanca (CHI)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jun 3, 2017</td>
				<td >
					Shoemaker Mile				</td>
				<td >
					SA				</td>
				<td >
					Mile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Bal a Bali (BRZ)</td>
			</tr>

			<tr>
				<td  >Jun 4, 2017</td>
				<td >
					Yasuda Kinen				</td>
				<td >
					TOK				</td>
				<td >
					Mile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Satono Aladdin (JPN)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jun 10, 2017</td>
				<td >
					Mohegan Sun Metropolitan H.				</td>
				<td >
					BEL				</td>
				<td >
					Dirt Mile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Mor Spirit</td>
			</tr>

			<tr>
				<td  >Jun 10, 2017</td>
				<td >
					Ogden Phipps				</td>
				<td >
					BEL				</td>
				<td >
					Distaff
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Songbird</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jun 11, 2017</td>
				<td >
					Grande Premio Brasil				</td>
				<td >
					GVA				</td>
				<td >
					Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Voador Magee (BRZ)</td>
			</tr>

			<tr>
				<td  >Jun 17, 2017</td>
				<td >
					Fleur de Lis H.				</td>
				<td >
					CD				</td>
				<td >
					Distaff
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Forever Unbridled</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jun 17, 2017</td>
				<td >
					Stephen Foster H.				</td>
				<td >
					CD				</td>
				<td >
					Classic
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Gun Runner</td>
			</tr>

			<tr>
				<td  >Jun 25, 2017</td>
				<td >
					Takarazuka Kinen				</td>
				<td >
					HSN				</td>
				<td >
					Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Satono Crown (JPN)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jun 25, 2017</td>
				<td >
					Gran Premio Pamplona				</td>
				<td >
					MON				</td>
				<td >
					Filly & Mare Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Birdie Gold</td>
			</tr>

			<tr>
				<td  >Jul 1, 2017</td>
				<td >
					Princess Rooney H.				</td>
				<td >
					GP				</td>
				<td >
					Filly & Mare Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Curlin's Approval</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jul 1, 2017</td>
				<td >
					Smile Sprint Stakes				</td>
				<td >
					GP				</td>
				<td >
					Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Imperial Hint</td>
			</tr>

			<tr>
				<td  >Jul 2, 2017</td>
				<td >
					Highlander Stakes				</td>
				<td >
					WO				</td>
				<td >
					Turf Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Green Mask</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jul 8, 2017</td>
				<td >
					Belmont Sprint Championship				</td>
				<td >
					BEL				</td>
				<td >
					Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Mind Your Biscuits</td>
			</tr>

			<tr>
				<td  >Jul 29, 2017</td>
				<td >
					King George VI & Queen Elizabeth Stakes sponsored by QIPCO				</td>
				<td >
					ASC				</td>
				<td >
					Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Enable (GB)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jul 29, 2017</td>
				<td >
					Bing Crosby S.				</td>
				<td >
					DMR				</td>
				<td >
					Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Ransom the Moon</td>
			</tr>

			<tr>
				<td  >Jul 30, 2017</td>
				<td >
					Clement L. Hirsch Stakes				</td>
				<td >
					DMR				</td>
				<td >
					Distaff
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Stellar Wind</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jul 30, 2017</td>
				<td >
					Betfair.com Haskell Invitational				</td>
				<td >
					MTH				</td>
				<td >
					Classic
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Girvin</td>
			</tr>

			<tr>
				<td  >Aug 2, 2017</td>
				<td >
					Qatar Sussex Stakes				</td>
				<td >
					GOO				</td>
				<td >
					Mile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Here Comes When (IRE)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 5, 2017</td>
				<td >
					The Whitney				</td>
				<td >
					SAR				</td>
				<td >
					Classic
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Gun Runner</td>
			</tr>

			<tr>
				<td  >Aug 12, 2017</td>
				<td >
					Arlington Million				</td>
				<td >
					AP				</td>
				<td >
					Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Beach Patrol</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 12, 2017</td>
				<td >
					Beverly D. Stakes				</td>
				<td >
					AP				</td>
				<td >
					Filly & Mare Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Dacita (CHI)</td>
			</tr>

			<tr>
				<td  >Aug 13, 2017</td>
				<td >
					Prix du Haras de Fresnay-Le Buffard - Jacques Le Marois				</td>
				<td >
					DEA				</td>
				<td >
					Mile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Al Wukair (IRE)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 19, 2017</td>
				<td >
					Del Mar H. Presented By The Japan Racing Association				</td>
				<td >
					DMR				</td>
				<td >
					Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Hunt (IRE)</td>
			</tr>

			<tr>
				<td  >Aug 19, 2017</td>
				<td >
					Pacific Classic				</td>
				<td >
					DMR				</td>
				<td >
					Classic
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Collected</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 23, 2017</td>
				<td >
					Juddmonte International S.				</td>
				<td >
					YOR				</td>
				<td >
					Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Ulysses (IRE)</td>
			</tr>

			<tr>
				<td  >Aug 24, 2017</td>
				<td >
					Darley Yorkshire Oaks				</td>
				<td >
					YOR				</td>
				<td >
					Filly & Mare Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Enable (GB)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 25, 2017</td>
				<td >
					Coolmore Nunthorpe Stakes				</td>
				<td >
					YOR				</td>
				<td >
					Turf Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Marsha (IRE)</td>
			</tr>

			<tr>
				<td  >Aug 26, 2017</td>
				<td >
					Pat O'Brien S.				</td>
				<td >
					DMR				</td>
				<td >
					Dirt Mile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Giant Expectations</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 26, 2017</td>
				<td >
					The Forego				</td>
				<td >
					SAR				</td>
				<td >
					Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Drefong</td>
			</tr>

			<tr>
				<td  >Aug 26, 2017</td>
				<td >
					The Ketel One Ballerina				</td>
				<td >
					SAR				</td>
				<td >
					Filly & Mare Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > By the Moon</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 26, 2017</td>
				<td >
					The Personal Ensign				</td>
				<td >
					SAR				</td>
				<td >
					Distaff
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Forever Unbridled</td>
			</tr>

			<tr>
				<td  >Aug 26, 2017</td>
				<td >
					The Sword Dancer				</td>
				<td >
					SAR				</td>
				<td >
					Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Sadler's Joy</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 2, 2017</td>
				<td >
					T. Von Zastrow Stutenpreis				</td>
				<td >
					BAD				</td>
				<td >
					Filly & Mare Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Ashiana (GER)</td>
			</tr>

			<tr>
				<td  >Sep 2, 2017</td>
				<td >
					The Spinaway				</td>
				<td >
					SAR				</td>
				<td >
					Juvenile Fillies
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Lady Ivanka</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 3, 2017</td>
				<td >
					Longines Grosser Preis Von Baden				</td>
				<td >
					BAD				</td>
				<td >
					Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Guignol (GER)</td>
			</tr>

			<tr>
				<td  >Sep 9, 2017</td>
				<td >
					Coolmore Fastnet Rock Matron S.				</td>
				<td >
					LEO				</td>
				<td >
					Filly & Mare Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Hydrangea (IRE)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 9, 2017</td>
				<td >
					QIPCO Irish Champion Stakes				</td>
				<td >
					LEO				</td>
				<td >
					Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Decorated Knight (GB)</td>
			</tr>

			<tr>
				<td  >Sep 9, 2017</td>
				<td >
					Willis Towers Watson Champions Juvenile Stakes				</td>
				<td >
					LEO				</td>
				<td >
					Juvenile Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Nelson (IRE)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 10, 2017</td>
				<td >
					Derrinstown Stud Flying Five Stakes				</td>
				<td >
					CUR				</td>
				<td >
					Turf Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Caravaggio</td>
			</tr>

			<tr>
				<td  >Sep 10, 2017</td>
				<td >
					Moyglare Stud Stakes				</td>
				<td >
					CUR				</td>
				<td >
					Juvenile Fillies Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Happily (IRE)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 16, 2017</td>
				<td >
					Iroquois S.				</td>
				<td >
					CD				</td>
				<td >
					Juvenile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > The Tabulator</td>
			</tr>

			<tr>
				<td  >Sep 16, 2017</td>
				<td >
					Pocahontas S.				</td>
				<td >
					CD				</td>
				<td >
					Juvenile Fillies
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Patrona Margarita</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 16, 2017</td>
				<td >
					Ricoh Woodbine Mile				</td>
				<td >
					WO				</td>
				<td >
					Mile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > World Approval</td>
			</tr>

			<tr>
				<td  >Sep 17, 2017</td>
				<td >
					Natalma Stakes				</td>
				<td >
					WO				</td>
				<td >
					Juvenile Fillies Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Capla Temptress (IRE)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 17, 2017</td>
				<td >
					Summer Stakes				</td>
				<td >
					WO				</td>
				<td >
					Juvenile Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Untamed Domain</td>
			</tr>

			<tr>
				<td  >Sep 29, 2017</td>
				<td >
					Shadwell Rockfel				</td>
				<td >
					NEW				</td>
				<td >
					Juvenile Fillies Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Juliet Capulet (IRE)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 30, 2017</td>
				<td >
					Joe Hirsch Turf Classic				</td>
				<td >
					BEL				</td>
				<td >
					Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Beach Patrol</td>
			</tr>

			<tr>
				<td  >Sep 30, 2017</td>
				<td >
					The Vosburgh				</td>
				<td >
					BEL				</td>
				<td >
					Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Takaful</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 30, 2017</td>
				<td >
					Juddmonte Royal Lodge Stakes				</td>
				<td >
					NEW				</td>
				<td >
					Juvenile Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Roaring Lion</td>
			</tr>

			<tr>
				<td  >Sep 30, 2017</td>
				<td >
					Awesome Again Stakes				</td>
				<td >
					SA				</td>
				<td >
					Classic
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Mubtaahij (IRE)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 30, 2017</td>
				<td >
					Chandelier Stakes				</td>
				<td >
					SA				</td>
				<td >
					Juvenile Fillies
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Moonshine Memories</td>
			</tr>

			<tr>
				<td  >Sep 30, 2017</td>
				<td >
					FrontRunner Stakes				</td>
				<td >
					SA				</td>
				<td >
					Juvenile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Bolt d'Oro</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 30, 2017</td>
				<td >
					Rodeo Drive Stakes				</td>
				<td >
					SA				</td>
				<td >
					Filly & Mare Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Avenge</td>
			</tr>

			<tr>
				<td  >Sep 30, 2017</td>
				<td >
					Zenyatta Stakes				</td>
				<td >
					SA				</td>
				<td >
					Distaff
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Paradise Woods</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 1, 2017</td>
				<td >
					Longines Prix de l'Opera				</td>
				<td >
					CHY				</td>
				<td >
					Filly & Mare Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Rhododendron (IRE)</td>
			</tr>

			<tr>
				<td  >Oct 1, 2017</td>
				<td >
					Qatar Prix Jean-Luc Lagardere				</td>
				<td >
					CHY				</td>
				<td >
					Juvenile Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Happily (IRE)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 1, 2017</td>
				<td >
					Total Prix Marcel Boussac				</td>
				<td >
					CHY				</td>
				<td >
					Juvenile Fillies Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Wild Illusion (GB)</td>
			</tr>

			<tr>
				<td  >Oct 1, 2017</td>
				<td >
					Sprinters S.				</td>
				<td >
					NKR				</td>
				<td >
					Turf Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Red Falx (JPN)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 6, 2017</td>
				<td >
					Darley Alcibiades Stakes				</td>
				<td >
					KEE				</td>
				<td >
					Juvenile Fillies
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Heavenly Love</td>
			</tr>

			<tr>
				<td  >Oct 6, 2017</td>
				<td >
					Stoll Keenon Ogden Phoenix Stakes				</td>
				<td >
					KEE				</td>
				<td >
					Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Whitmore</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 7, 2017</td>
				<td >
					Champagne Stakes				</td>
				<td >
					BEL				</td>
				<td >
					Juvenile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Firenze Fire</td>
			</tr>

			<tr>
				<td  >Oct 7, 2017</td>
				<td >
					Jockey Club Gold Cup Stakes				</td>
				<td >
					BEL				</td>
				<td >
					Classic
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Diversify</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 7, 2017</td>
				<td >
					Claiborne Breeders' Futurity				</td>
				<td >
					KEE				</td>
				<td >
					Juvenile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Free Drop Billy</td>
			</tr>

			<tr>
				<td  >Oct 7, 2017</td>
				<td >
					First Lady S.				</td>
				<td >
					KEE				</td>
				<td >
					Filly & Mare Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Zipessa</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 7, 2017</td>
				<td >
					Shadwell Turf Mile Stakes				</td>
				<td >
					KEE				</td>
				<td >
					Mile
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Suedois (FR)</td>
			</tr>

			<tr>
				<td  >Oct 7, 2017</td>
				<td >
					Thoroughbred Club of America Stakes				</td>
				<td >
					KEE				</td>
				<td >
					Filly & Mare Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Finley'sluckycharm</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 7, 2017</td>
				<td >
					Santa Anita Sprint Championship				</td>
				<td >
					SA				</td>
				<td >
					Sprint
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Roy H</td>
			</tr>

			<tr>
				<td  >Oct 8, 2017</td>
				<td >
					Flower Bowl Stakes				</td>
				<td >
					BEL				</td>
				<td >
					Filly & Mare Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > War Flag</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 8, 2017</td>
				<td >
					Frizette Stakes				</td>
				<td >
					BEL				</td>
				<td >
					Juvenile Fillies
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Separationofpowers</td>
			</tr>

			<tr>
				<td  >Oct 8, 2017</td>
				<td >
					Dixiana Bourbon S.				</td>
				<td >
					KEE				</td>
				<td >
					Juvenile Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Flameaway</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 8, 2017</td>
				<td >
					Juddmonte Spinster Stakes				</td>
				<td >
					KEE				</td>
				<td >
					Distaff
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Romantic Vision</td>
			</tr>

			<tr>
				<td  >Oct 11, 2017</td>
				<td >
					JPMorgan Chase Jessamine Stakes				</td>
				<td >
					KEE				</td>
				<td >
					Juvenile Fillies Turf
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Rushing Fall</td>
			</tr>

					<tr><td colspan="7" align="left">
				* - tentative date<br>
				** - purse includes Breeders' Cup Stakes funds
			</td></tr>

		</table>
		<!--</td>
		</tr>
		</table>-->

		