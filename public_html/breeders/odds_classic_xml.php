	<div class="table-responsive">
		<table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Classic - To Win">
			<caption>Horses - Breeders Cup Classic - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated October 13, 2017 09:59:10 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Classic - To Win  - Nov 04					</th>
			</tr>
        -->
			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Arrogate</td><td>5/6</td><td>-120</td></tr><tr><td>Gun Runner</td><td>13/10</td><td>+130</td></tr><tr><td>Irish War Cry</td><td>75/1</td><td>+7500</td></tr><tr><td>Hoppertunity</td><td>35/1</td><td>+3500</td></tr><tr><td>Epicharis</td><td>85/1</td><td>+8500</td></tr><tr><td>Keen Ice</td><td>12/1</td><td>+1200</td></tr><tr><td>Accelerate</td><td>30/1</td><td>+3000</td></tr><tr><td>Breaking Lucky</td><td>55/1</td><td>+5500</td></tr><tr><td>Cat Burglar</td><td>65/1</td><td>+6500</td></tr><tr><td>Collected</td><td>3/1</td><td>+300</td></tr><tr><td>Cupid</td><td>22/1</td><td>+2200</td></tr><tr><td>Gold Dream</td><td>120/1</td><td>+12000</td></tr><tr><td>Gunnevera</td><td>20/1</td><td>+2000</td></tr><tr><td>Hard Aces</td><td>80/1</td><td>+8000</td></tr><tr><td>Honorable Duty</td><td>60/1</td><td>+6000</td></tr><tr><td>Highland Reel</td><td>55/1</td><td>+5500</td></tr><tr><td>Midnight Storm</td><td>25/1</td><td>+2500</td></tr><tr><td>Mor Spirit</td><td>50/1</td><td>+5000</td></tr><tr><td>Timeline</td><td>85/1</td><td>+8500</td></tr><tr><td>War Story</td><td>60/1</td><td>+6000</td></tr><tr><td>West Coast</td><td>6/1</td><td>+600</td></tr><tr><td>Churchill</td><td>70/1</td><td>+7000</td></tr><tr><td>Neolithic</td><td>35/1</td><td>+3500</td></tr><tr><td>Mubtaahij</td><td>15/1</td><td>+1500</td></tr><tr><td>Colonelsdarktemper</td><td>75/1</td><td>+7500</td></tr><tr><td>Donworth</td><td>80/1</td><td>+8000</td></tr><tr><td>Vivlos</td><td>28/1</td><td>+2800</td></tr><tr><td>Good Samaritan</td><td>45/1</td><td>+4500</td></tr><tr><td>Cool Catomine</td><td>100/1</td><td>+10000</td></tr><tr><td>Game Over</td><td>75/1</td><td>+7500</td></tr><tr><td>Rally Cry</td><td>65/1</td><td>+6500</td></tr><tr><td>Pavel</td><td>25/1</td><td>+2500</td></tr><tr><td>Discreet Lover</td><td>100/1</td><td>+10000</td></tr><tr><td>Seeking The Soul</td><td>120/1</td><td>+12000</td></tr><tr><td>Win The Space</td><td>30/1</td><td>+3000</td></tr><tr><td>Destin</td><td>60/1</td><td>+6000</td></tr><tr><td>Curlin Rules</td><td>60/1</td><td>+6000</td></tr><tr><td>Term Of Art</td><td>120/1</td><td>+12000</td></tr><tr><td>War Decree</td><td>28/1</td><td>+2800</td></tr><tr><td>Diversify</td><td>25/1</td><td>+2500</td></tr>			</tbody>
		</table>
	</div>
{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sort_table.js"></script>
{/literal}
	