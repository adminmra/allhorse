{literal}
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "about": "Breeders' Cup Juvenile Turf Odds"
    }
</script>
{/literal}
<div id="no-more-tables">
    <table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
        cellpadding="0" cellspacing="0" border="0" title="2020 Breeders' Cup Juvenile Turf"
        summary="The latest odds for the Breeders' Cup Juvenile Turf available">

        <caption class="hide-sm">2020 Breeders' Cup Juvenile Turf Odds and Post Position</caption>


        <tbody>
            <tr class='head_title hide-sm'>
                <th>PP</th>
                <th>Horse</th>
                <th>Odds</th>
                <th>Trainer</th>
                <th>Jockey</th>
            </tr>
            <tr>
                <td data-title="PP">3</td>
                <td data-title="Horse">Abarta</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">Brad Cox</td>
                <td data-title="Jockey">Umberto Rispoli</td>
            </tr>
            <tr>
                <td data-title="PP">9</td>
                <td data-title="Horse">Battleground</td>
                <td data-title="Odds">6-1</td>
                <td data-title="Trainer">Aidan P. O'Brien</td>
                <td data-title="Jockey">Ryan Moore</td>
            </tr>
            <tr>
                <td data-title="PP">5</td>
                <td data-title="Horse">Cadillac (IRE)</td>
                <td data-title="Odds">6-1</td>
                <td data-title="Trainer">Mrs. John Harrington</td>
                <td data-title="Jockey">Shane Foley</td>
            </tr>
            <tr>
                <td data-title="PP">13</td>
                <td data-title="Horse">Devilwala (IRE)</td>
                <td data-title="Odds">30-1</td>
                <td data-title="Trainer">Ralph M. Beckett</td>
                <td data-title="Jockey">Rossa Ryan</td>
            </tr>
            <tr>
                <td data-title="PP">10</td>
                <td data-title="Horse">Ebeko (IRE)</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">Peter Miller</td>
                <td data-title="Jockey">Flavien Prat</td>
            </tr>
            <tr>
                <td data-title="PP">7</td>
                <td data-title="Horse">Fire At Will</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">Michael J. Maker</td>
                <td data-title="Jockey">Ricardo Santana Jr.</td>
            </tr>
            <tr>
                <td data-title="PP">12</td>
                <td data-title="Horse">Go Athletico (FR)</td>
                <td data-title="Odds">10-1</td>
                <td data-title="Trainer">Philippe Decouz</td>
                <td data-title="Jockey">Aurelien Lemaitre</td>
            </tr>
            <tr>
                <td data-title="PP">11</td>
                <td data-title="Horse">Gretzky the Great</td>
                <td data-title="Odds">8-1</td>
                <td data-title="Trainer">Mark E. Casse</td>
                <td data-title="Jockey">Tyler Gaffalione</td>
            </tr>
            <tr>
                <td data-title="PP">6</td>
                <td data-title="Horse">Mutasaabeq</td>
                <td data-title="Odds">5-1</td>
                <td data-title="Trainer">Todd A. Pletcher</td>
                <td data-title="Jockey">Luis Saez</td>
            </tr>
            <tr>
                <td data-title="PP">2</td>
                <td data-title="Horse">New Mandate (IRE)</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">Ralph M. Beckett</td>
                <td data-title="Jockey">Lanfranco Dettori</td>
            </tr>
            <tr>
                <td data-title="PP">8</td>
                <td data-title="Horse">Outadore</td>
                <td data-title="Odds">8-1</td>
                <td data-title="Trainer">Wesley A. Ward</td>
                <td data-title="Jockey">Jose Ortiz</td>
            </tr>
            <tr>
                <td data-title="PP">4</td>
                <td data-title="Horse">Public Sector (GB)</td>
                <td data-title="Odds">8-1</td>
                <td data-title="Trainer">Chad C. Brown</td>
                <td data-title="Jockey">Irad Ortiz Jr.</td>
            </tr>
            <tr>
                <td data-title="PP">1</td>
                <td data-title="Horse">Sealiway (FR)</td>
                <td data-title="Odds">8-1</td>
                <td data-title="Trainer">Frederic Rossi</td>
                <td data-title="Jockey">Mickael Barzalona</td>
            </tr>
            <tr>
                <td data-title="PP">14</td>
                <td data-title="Horse">The Lir Jet (IRE)</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">Michael Bell</td>
                <td data-title="Jockey">Oisin Murphy</td>
            </tr>
            <tr>
                <td data-title="PP">15</td>
                <td data-title="Horse">Barrister Tom (AE)</td>
                <td data-title="Odds">30-1</td>
                <td data-title="Trainer">Michael Ewing</td>
                <td data-title="Jockey">Florent Geroux</td>
            </tr>
            <tr>
                <td data-title="PP">16</td>
                <td data-title="Horse">Harlan Estate (AE)</td>
                <td data-title="Odds">30-1</td>
                <td data-title="Trainer">Peter Eurton</td>
                <td data-title="Jockey">Juan Hernandez</td>
            </tr>
        </tbody>
    </table>
</div>
{literal}
<style>
    @media screen and (max-width: 640px) {
        .fa-sort {
            display: block !important;
        }
    }
</style>
<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sorttable_post.js"></script> {/literal}