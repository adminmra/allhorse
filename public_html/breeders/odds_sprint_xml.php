    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="* Horses - Breeders Cup Turf - Odds">
            <caption>* Horses - Breeders Cup Turf - Odds</caption>
            <tbody>
                <tr>
                    <th colspan="3" class="center">
                    * Horses - Breeders Cup Turf - Odds  - Oct 31                    </th>
            </tr>
                <tr>
                    <th colspan="3" class="center">
                    Fixed Odds - All Horses Are Action, Run Or Not                    </th>
            </tr>
    <tr><th colspan="3" class="center">Turf - Odds To Win</th></tr><tr><th>Team</th><th>American Odds</th><th>Fractional Odds</th></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Golden Horn</td><td itemprop='Scratch'>-190</td><td itemprop='offers'>10/19</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Big Blue Kitten</td><td itemprop='Scratch'>+1000</td><td itemprop='offers'>10/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>The Pizza Man</td><td itemprop='Scratch'>+800</td><td itemprop='offers'>8/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Red Rifle</td><td itemprop='Scratch'>+3000</td><td itemprop='offers'>30/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Twilight Eclipse</td><td itemprop='Scratch'>+3000</td><td itemprop='offers'>30/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Big John B</td><td itemprop='Scratch'>+3500</td><td itemprop='offers'>35/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Ordak Dan</td><td itemprop='Scratch'>+4500</td><td itemprop='offers'>45/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Found</td><td itemprop='Scratch'>+300</td><td itemprop='offers'>3/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Shining Copper</td><td itemprop='Scratch'>+4500</td><td itemprop='offers'>45/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Cage Fighter</td><td itemprop='Scratch'>+6000</td><td itemprop='offers'>60/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Slumber</td><td itemprop='Scratch'>+3000</td><td itemprop='offers'>30/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Da Big Hoss</td><td itemprop='Scratch'>+4500</td><td itemprop='offers'>45/1</td></tr>                    <tr>
                        <td class="dateUpdated center" colspan="5">
                            <em id='updateemp'>  - Updated September 29, 2017 09:59:04 </em> BUSR - Official <a href="http://www.usracing.com/breeders-cup/sprint">Breeders' Cup Sprint Odds</a>. <br> <a href="http://www.usracing.com/breeders-cup/sprint">Breeders' Cup Sprint</a><!-- , all odds are fixed odds prices. -->

                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
