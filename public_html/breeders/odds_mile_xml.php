    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="* Horses - Breeders Cup Mile - Odds">
            <caption>* Horses - Breeders Cup Mile - Odds</caption>
            <tbody>
                <tr>
                    <th colspan="3" class="center">
                    * Horses - Breeders Cup Mile - Odds  - Oct 31                    </th>
            </tr>
                <tr>
                    <th colspan="3" class="center">
                    Fixed Odds - All Horses Are Action, Run Or Not                    </th>
            </tr>
    <tr><th colspan="3" class="center">Mile - Odds To Win</th></tr><tr><th>Team</th><th>American Odds</th><th>Fractional Odds</th></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Make Believe</td><td itemprop='Scratch'>+325</td><td itemprop='offers'>13/4</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Esoterique</td><td itemprop='Scratch'>+300</td><td itemprop='offers'>3/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Time Test</td><td itemprop='Scratch'>+400</td><td itemprop='offers'>4/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Karakontie</td><td itemprop='Scratch'>+1000</td><td itemprop='offers'>10/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Tepin</td><td itemprop='Scratch'>+900</td><td itemprop='offers'>9/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Mondialiste</td><td itemprop='Scratch'>+1200</td><td itemprop='offers'>12/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Grand Arch</td><td itemprop='Scratch'>+1800</td><td itemprop='offers'>18/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Obviously</td><td itemprop='Scratch'>+2200</td><td itemprop='offers'>22/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Tourist</td><td itemprop='Scratch'>+3000</td><td itemprop='offers'>30/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Impassable</td><td itemprop='Scratch'>+500</td><td itemprop='offers'>5/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Mshawish</td><td itemprop='Scratch'>+1800</td><td itemprop='offers'>18/1</td></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>Recepta</td><td itemprop='Scratch'>+3500</td><td itemprop='offers'>35/1</td></tr>                    <tr>
                        <td class="dateUpdated center" colspan="5">
                            <em id='updateemp'>  - Updated September 29, 2017 08:07:20 </em> BUSR - Official <a href="//www.usracing.com/breeders-cup/mile">Breeders' Cup Mile Odds</a>. <br> <a href="//www.usracing.com/breeders-cup/mile">Breeders' Cup Mile</a><!-- , all odds are fixed odds prices. -->
                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
