{literal}
<script>
    request();

    function request() {
        var req = new XMLHttpRequest();
        req.open('GET', '/dates_for_breeders.json', true);
        req.onreadystatechange = function (aEvt) {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    checkDate(req.responseText);
                    setInterval(function () {
                        checkDate(req.responseText);
                    }, 1000);
                }
            }
        };
        req.send(null);
    }

    function checkDate(response) {
        var newYorkTime = new Date().toLocaleString("en-US", {
            timeZone: "America/New_York"
        });
        var now = new Date(newYorkTime);
        var json = JSON.parse(response);
        var page = (window.location.href.split('/'))[4];
        var obj = json[0][page];
        var date = new Date(obj.year, obj.month, obj.day, obj.hour, obj.minutes, 59);
        if (now.getTime() === date.getTime())
            location.reload(true);
    }
</script>
{/literal}