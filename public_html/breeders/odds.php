<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"> 

<tbody><tr >
<th width="50%">Breeders' Cup Horses</th>
<th  >ODDS</th>

</tr>
<tr>
<td  >Alpha</td>
<td   >60/1</td>
</tr>
<tr >
<td >Bourbon Courage</td>
<td  >30/1</td>
</tr>
<tr>
<td >Camelot</td>
<td  >75/1</td>
</tr>
<tr >
<td >Capo Bastone</td>
<td  >80/1</td>
</tr>
<tr>
<td >Cigar Street</td>
<td  >6/1</td>
</tr>
<tr >
<td >Clubhouse Ride</td>
<td  >25/1</td>
</tr>
<tr>
<td >Crop Report</td>
<td  >30/1</td>
</tr>
<tr >
<td >Cross Traffic</td>
<td  >8/1</td>
</tr>
<tr>
<td >Cyber Secret</td>
<td  >50/1</td>
</tr>
<tr >
<td >Declaration Of War</td>
<td  >15/1</td>
</tr>
<tr>
<td >Departing</td>
<td  >60/1</td>
</tr>
<tr >
<td >Discreet Dancer</td>
<td  >30/1</td>
</tr>
<tr>
<td >El Padrino</td>
<td  >80/1</td>
</tr>
<tr >
<td >Flashback</td>
<td  >50/1</td>
</tr>
<tr>
<td >Flat Out</td>
<td  >10/1</td>
</tr>
<tr >
<td >Fort Larned</td>
<td  >6/1</td>
</tr>
<tr>
<td >Fortify</td>
<td  >100/1</td>
</tr>
<tr >
<td >Freedom Child</td>
<td  >100/1</td>
</tr>
<tr>
<td >Game On Dude</td>
<td  >5/2</td>
</tr>
<tr >
<td >Golden Soul</td>
<td  >100/1</td>
</tr>
<tr>
<td >Golden Ticket</td>
<td  >65/1</td>
</tr>
<tr >
<td >Goldencents</td>
<td  >20/1</td>
</tr>
<tr>
<td >Graydar</td>
<td  >10/1</td>
</tr>
<tr >
<td >Guilt Trip</td>
<td  >60/1</td>
</tr>
<tr>
<td >Hear The Ghost</td>
<td  >50/1</td>
</tr>
<tr >
<td >Hes Had Enough</td>
<td  >100/1</td>
</tr>
<tr>
<td >Hierro</td>
<td  >110/1</td>
</tr>
<tr >
<td >Ingognito</td>
<td  >85/1</td>
</tr>
<tr>
<td >Javas War</td>
<td  >50/1</td>
</tr>
<tr >
<td >Last Gunfighter</td>
<td  >20/1</td>
</tr>
<tr>
<td >Liaison</td>
<td  >50/1</td>
</tr>
<tr >
<td >Lines Of Battle</td>
<td  >110/1</td>
</tr>
<tr>
<td >Mark Valeski</td>
<td  >50/1</td>
</tr>
<tr >
<td >Mucho Macho Man</td>
<td  >30/1</td>
</tr>
<tr>
<td >Mylute</td>
<td  >75/1</td>
</tr>
<tr >
<td >Nates Mineshaft</td>
<td  >80/1</td>
</tr>
<tr>
<td >Normandy Invasion</td>
<td  >35/1</td>
</tr>
<tr >
<td >Orb</td>
<td  >6/1</td>
</tr>
<tr>
<td >Out Of Bounds</td>
<td  >80/1</td>
</tr>
<tr >
<td >Overanalyze</td>
<td  >30/1</td>
</tr>
<tr>
<td >Oxbow</td>
<td  >50/1</td>
</tr>
<tr >
<td >Palace Malice</td>
<td  >12/1</td>
</tr>
<tr>
<td >Paynter</td>
<td  >12/1</td>
</tr>
<tr >
<td >Pool Play</td>
<td  >85/1</td>
</tr>
<tr>
<td >Power Broker</td>
<td  >20/1</td>
</tr>
<tr >
<td >Revolutionary</td>
<td  >8/1</td>
</tr>
<tr>
<td >Richards Kid</td>
<td  >70/1</td>
</tr>
<tr >
<td >Ron the Greek</td>
<td  >25/1</td>
</tr>
<tr>
<td >Royal Delta</td>
<td  >14/1</td>
</tr>
<tr >
<td >Sea Island</td>
<td  >75/1</td>
</tr>
<tr>
<td >Shanghai Bobby</td>
<td  >30/1</td>
</tr>
<tr >
<td >Souper Speedy</td>
<td  >80/1</td>
</tr>
<tr>
<td >Super Ninety Nine</td>
<td  >80/1</td>
</tr>
<tr >
<td >Take Charge Indy</td>
<td  >10/1</td>
</tr>
<tr>
<td >Take Control</td>
<td  >110/1</td>
</tr>
<tr >
<td >Unbridled Command</td>
<td  >75/1</td>
</tr>
<tr>
<td >Uncaptured</td>
<td  >100/1</td>
</tr>
<tr >
<td >Verrazano</td>
<td  >8/1</td>
</tr>
<tr>
<td >Vyjack</td>
<td  >100/1</td>
</tr>
<tr >
<td >Will Take Charge</td>
<td  >15/1</td>
</tr>
<tr>
<td >Wise Dan</td>
<td  >8/1</td>
</tr>

</tbody></table></div>