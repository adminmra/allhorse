	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Breeders' Cup Mile Odds"
	  }
	</script>
	{/literal}
	<div>
		<table  class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"   summary="Horses - Breeders Cup Mile - To Win">
			<caption>Horses - Breeders Cup Mile - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:35 </em>
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Mile - To Win  - Nov 06					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Space Blues</td><td>9/4</td><td>+225</td></tr><tr><td>Mo Forza</td><td>9/2</td><td>+450</td></tr><tr><td>Mother Earth</td><td>9/1</td><td>+900</td></tr><tr><td>Blowout</td><td>14/1</td><td>+1400</td></tr><tr><td>In Love</td><td>16/1</td><td>+1600</td></tr><tr><td>Pearls Galore</td><td>16/1</td><td>+1600</td></tr><tr><td>Real Appeal</td><td>16/1</td><td>+1600</td></tr><tr><td>Got Stormy</td><td>18/1</td><td>+1800</td></tr><tr><td>Master Of The Seas</td><td>10/1</td><td>+1000</td></tr><tr><td>Casa Creed</td><td>40/1</td><td>+4000</td></tr><tr><td>Smooth Like Strait</td><td>16/1</td><td>+1600</td></tr><tr><td>Raging Bull</td><td>22/1</td><td>+2200</td></tr><tr><td>Ivar</td><td>20/1</td><td>+2000</td></tr><tr><td>Vin De Garde</td><td>33/1</td><td>+3300</td></tr>			</tbody>
		</table>
	</div>
{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
{/literal}
	