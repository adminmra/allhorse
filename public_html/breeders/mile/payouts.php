<h2>2021 Breeders' Cup Mile Payouts</h2>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Mile Payouts" summary="Last year's Payouts of the Breeders' Cup Mile. ">
    <tbody>
      <thead>
        <tr>
          <th width="5%">PP</th>
          <th width="35%">Horses</th>
          <th width="10%">Win</th>
          <th width="10%">Place</th>
          <th width="10%">Show</th>
        </tr>
      </thead>
      <tr>
        <td>3</td>
        <td class="name">Space Blues (IRE)</td>
        <td>
          <div class="box">$6.20</div>
        </td>
        <td>
          <div class="box">$4.60</div>
        </td>
        <td class="payoff">
          <div class="box">$3.40</div>
        </td>
      </tr>
      <tr>
        <td>2</td>
        <td class="name">Smooth Like Strait</td>
        <td></td>
        <td>
          <div class="box">$9.00</div>
        </td>
        <td class="payoff">
          <div class="box">$6.20</div>
        </td>
      </tr>
      <tr>
        <td>14</td>
        <td class="name">Ivar (BRZ)</td>
        <td></td>
        <td></td>
        <td class="payoff">
          <div class="box">$11.40</div>
        </td>
    </tbody>
  </table>
</div>
<p></p>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Mile Payouts" summary="Last year's Payouts of the Breeders' Cup Mile. ">
    <tbody>
      <thead>
        <tr>
          <th width="20%">Wager</th>
          <th width="20%">Horses</th>
          <th width="20%">Payout</th>
        </tr>
      </thead>

      <tr class="odd">
        <td>$1.00 Exacta</td>
        <td>3-2</td>
        <td class="payoff">$29.10</td>
      </tr>
      <tr class="even">
        <td>$0.50 Trifecta</td>
        <td>3-2-14</td>
        <td class="payoff">$259.10</td>
      </tr>
      <tr class="odd">
        <td>$0.10 Superfecta</td>
        <td>3-2-14-4</td>
        <td class="payoff">$370.81</td>
      </tr>
      <tr class="even">
        <td>$1.00 Double</td>
        <td>5/3</td>
        <td class="payoff">$40.50</td>
      </tr>
      <tr class="odd">
        <td>$0.50 Pick 3</td>
        <td>8/5/1</td>
        <td class="payoff">$132.75</td>
      </tr>
  </table>
</div>