<h2>2021 Breeders' Cup Mile Results</h2>
<div id="no-more-tables">
  <table border="0" cellspacing="0" cellpadding="0"
    class="table table-condensed table-striped table-bordered ordenableResult" title="Breeders' Cup Mile Results"
    summary="Last year's results of the Breeders' Cup Mile. ">
    <thead>
      <tr>
        <th>Results</th>
        <th>PP</th>
        <th>Horse</th>
        <th>Trainer</th>
        <th>Jockey</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Result">1</td>
        <td data-title="PP">3</td>
        <td data-title="Horse">Space Blues (IRE)</td>
        <td data-title="Trainer">Charlie Appleby</td>
        <td data-title="Jockey">James Doyle</td>
      </tr>
      <tr>
        <td data-title="Result">2</td>
        <td data-title="PP">2</td>
        <td data-title="Horse">Smooth Like Strait</td>
        <td data-title="Trainer">Michael McCarthy</td>
        <td data-title="Jockey">Umberto Rispoli</td>
      </tr>
      <tr>
        <td data-title="Result">3</td>
        <td data-title="PP">14</td>
        <td data-title="Horse">Ivar (BRZ)</td>
        <td data-title="Trainer">Paulo Lobo</td>
        <td data-title="Jockey">Joseph Talamo</td>
      </tr>
    </tbody>
  </table>

</div>
<script src="/assets/js/sorttable_results.js"></script>