<div class="table-responsive">
    <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" title="Breeders' Cup Mile Results" summary="Last year's results of the Breeders' Cup Mile. " >
        <tbody><tr>
                <th width="5%">Result</th>
                <th width="5%">Time</th> 
                <th width="5%">Post</th> 
                <th width="5%">Odds</th>  
                <th width="14%">Horse</th><th width="14%">Trainer</th><th width="14%">Jockey</th> <th width="20%">Owner</th>
            </tr>
            <tr><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>12.40 </td><td>Tourist</td><td ></td><td ></td> <td ></td></tr>
            <tr><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>3.70 </td><td>Tepin</td><td >&nbsp;</td><td >&nbsp;</td> <td ></td></tr>
            <tr><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>12.90 </td><td>Midnight Storm</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>3.80</td><td>Ironicus</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>22.20</td><td>Miss Temple City</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>3.40*</td><td>Limato (IRE)</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>47.00</td><td>Ring Weekend</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>82.10 </td><td>Cougar Mountain (IRE)</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>66.40 </td><td>Hit It a Bomb</td><td >&nbsp;</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
            <tr><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>4.20 </td><td>Alice Springs (IRE)</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>29.10 </td><td>Spectre (FR)</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>69.10</td><td>Dutch Connection (GB)</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>13</td><td>&nbsp;</td><td>&nbsp;</td><td>19.70</td><td>Photo Call (IRE)</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>

			<tr><td>14</td><td>&nbsp;</td><td>&nbsp;</td><td>47.20</td><td>What a View</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>

        </tbody>
    </table> 
</div>