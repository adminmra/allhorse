	<div>
		<table  class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"   summary="Horses - Breeders Cup Mile - To Win">
			<caption>Horses - Breeders Cup Mile - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated November 3, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Mile - To Win  - Nov 03					</th>
			</tr>
        -->
			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr>
	    <tr>
      <td>Expert Eye</td>
      <td>8/1</td>
      <td>+800</td>
    </tr>
    <tr>
      <td>Catapult</td>
      <td>6/1</td>
      <td>+600</td>
    </tr>
    <tr>
      <td>Analyze It</td>
      <td>6/1</td>
      <td>+600</td>
    </tr>
    <tr>
      <td>Divisidero</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>
    <tr>
      <td>One Master</td>
      <td>12/1</td>
      <td>+1200</td>
    </tr>
    <tr>
      <td>Clemmie</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>
    <tr>
      <td>Lightning Spear</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Happily</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>Gustav Klimt</td>
      <td>10/1</td>
      <td>+1000</td>
    </tr>
    <tr>
      <td>Almanaar</td>
      <td>12/1</td>
      <td>+1200</td>
    </tr>
    <tr>
      <td>Mustashry</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>I Can Fly</td>
      <td>8/1</td>
      <td>+800</td>
    </tr>
    <tr>
      <td>Next Shares</td>
      <td>10/1</td>
      <td>+1000</td>
    </tr></tbody>
		</table>
	</div>
{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sort_table.js"></script>
{/literal}
	