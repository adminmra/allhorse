	{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Mile Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2020 Breeders' Cup Mile"
			summary="The latest odds for the Breeders' Cup Mile available">

			<caption class="hide-sm">2020 Breeders' Cup Mile Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
				<tr>
					<td data-title="PP">3</td>
					<td data-title="Horse">Lope Y Fernandez (IRE)</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">Aidan O' Brian</td>
					<td data-title="Jockey">Lanfranco Dettori</td>
				</tr>
				<tr>
					<td data-title="PP">1</td>
					<td data-title="Horse">Circus Maximus (IRE)</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Aidan P. O'Brien</td>
					<td data-title="Jockey">Ryan Moore</td>
				</tr>
				<tr>
					<td data-title="PP">5</td>
					<td data-title="Horse">Digital Age (IRE)</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Chad C. Brown</td>
					<td data-title="Jockey">Javier Castellano</td>
				</tr>
				<tr>
					<td data-title="PP">13</td>
					<td data-title="Horse">Factor This</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Brad Cox</td>
					<td data-title="Jockey">Florent Geroux</td>
				</tr>
				<tr>
					<td data-title="PP">10</td>
					<td data-title="Horse">Halladay</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Todd A. Pletcher</td>
					<td data-title="Jockey">Luis Saez</td>
				</tr>
				<tr>
					<td data-title="PP">11</td>
					<td data-title="Horse">Ivar (BRZ)</td>
					<td data-title="Odds">4-1</td>
					<td data-title="Trainer">Paulo H. Lobo</td>
					<td data-title="Jockey">Joseph Talamo</td>
				</tr>
				<tr>
					<td data-title="PP">2</td>
					<td data-title="Horse">Kameko</td>
					<td data-title="Odds">6-1</td>
					<td data-title="Trainer">Andrew Balding</td>
					<td data-title="Jockey">Oisin Murphy</td>
				</tr>
				<tr>
					<td data-title="PP">8</td>
					<td data-title="Horse">March to the Arch</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">Mark E. Casse</td>
					<td data-title="Jockey">Tyler Gaffalione</td>
				</tr>
				<tr>
					<td data-title="PP">9</td>
					<td data-title="Horse">One Master (GB)</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">William John Haggas</td>
					<td data-title="Jockey">Pierre-Charles Boudot</td>
				</tr>
				<tr>
					<td data-title="PP">14</td>
					<td data-title="Horse">Raging Bull (FR)</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Chad C. Brown</td>
					<td data-title="Jockey">Jose Ortiz</td>
				</tr>
				<tr>
					<td data-title="PP">6</td>
					<td data-title="Horse">Safe Voyage (IRE)</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">John Quinn</td>
					<td data-title="Jockey">Jason Hart</td>
				</tr>
				<tr>
					<td data-title="PP">4</td>
					<td data-title="Horse">Siskin</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Ger Lyons</td>
					<td data-title="Jockey">Colin Keane</td>
				</tr>
				<tr>
					<td data-title="PP">7</td>
					<td data-title="Horse">Casa Creed</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">William Mott</td>
					<td data-title="Jockey">Junior Alvarado</td>
				</tr>
				<tr>
					<td data-title="PP">12</td>
					<td data-title="Horse">Uni (GB)</td>
					<td data-title="Odds">5-1</td>
					<td data-title="Trainer">Chad C. Brown</td>
					<td data-title="Jockey">Joel Rosario</td>
				</tr>
				<tr>
					<td data-title="PP">15</td>
					<td data-title="Horse">Order of Australia (IRE)</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">Aidan O'Brien</td>
					<td data-title="Jockey">Pierre Charles Boudot</td>
				</tr>
			</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}