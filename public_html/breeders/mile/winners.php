<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"  title="Breeders' Cup Mile Results" summary="The past winners of the Breeders Cup Mile." >
<tbody>
  <tr >
  	  <th>Year</th>
      <th>Winner</th>
      <th>Jockey</th>
      <th>Trainer</th>

      <th>Win Time</th>

      </tr>
            <tr>                                        
        <td  >2018</td>
      <td  >Expert Eye</td>
        <td >F. Dettori</td>
        <td >S. Stoute</td>

        <td >1:39.80 </td>

        </tr>  
      <tr>                                        
        <td  >2017</td>
      <td  >World Approval  </td>
        <td >John Velazquez</td>
        <td >Mark Casse</td>

        <td >1:34.55</td>

        </tr>  
      <tr>                                        
   	    <td  >2016</td>
	    <td  >Tourist</td>
        <td >Joel Rosario</td>
        <td >William I. Mott</td>

        <td >1:31.71</td>

        </tr>
       <tr>                                        
   	    <td  >2015</td>
	    <td  >Tepin</td>
        <td >Julien Leparoux</td>
        <td >Mark Casse</td>

        <td >1:36.69</td>

        </tr>
       <tr>                                        
   	    <td  >2014</td>
	    <td  >Karakontie (JPN)</td>
        <td >Stephane Pasquier</td>
        <td >Jonathan Pease</td>

        <td >1:32.88 </td>

        </tr>
      
      <tr>                                        
   	    <td  >2013</td>
	    <td  >Wise Dan</td>
        <td >Jose Lezcano</td>
        <td >Charles Lopresti</td>

        <td >1:32.47 </td>

        </tr> <tr>                                        
   	    <td  >2012</td>
	    <td  >Wise Dan</td>
        <td >John R. Velazquez</td>
        <td >Charles Lopresti</td>

        <td >1:31.78 </td>

        </tr>  <tr>                                        
       	  <td  >2011</td>
	      <td  >Court Vision</td>
          <td >Robby Albarado</td>
          <td >Dale Romans</td>

          <td >1:37:05 </td>

          </tr>  <tr>                                        
       	    <td  >2010</td>
		    <td  >Goldikova</td>
            <td >Olivier Peslier</td>
            <td >Freddy Head</td>

            <td >1:35.16</td>

            </tr>
										  
										 <tr>                                        
                                           	 <td  >2009</td>
										    <td  >Goldikova</td>
                                            <td >Olivier Peslier</td>
                                            <td >Freddy Head</td>

                                            <td >1:32.26</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  >Goldikova</td>
                                            <td >Oliver Peslier</td>
                                            <td >Freddie Head</td>

                                            <td >1:33.40</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2007</td>
										    <td  >Kip Deville</td>
                                            <td >C. Velasquez</td>
                                            <td >R. Dutrow</td>

                                            <td >1:39.78</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2006</td>
										    <td  >Miesque's Approval</td>
                                            <td >Eddie Castro</td>
                                            <td >Martin Wolfson</td>

                                            <td >1:34.75</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2005</td>
										    <td  >Artie Schiller</td>
                                            <td >Garret K. Gomez</td>
                                            <td >J. Jerkins</td>

                                            <td >1:36.10</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2004</td>
										    <td  >Singletary</td>
                                            <td >D. Flores</td>
                                            <td >D. Chatlos</td>

                                            <td >1:36.90</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2003</td>
										    <td  >Six Perfections (3/4)</td>
                                            <td >Jerry Bailey</td>
                                            <td >Pascal Bary</td>

                                            <td >1:334/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2002</td>
										    <td  >Domedriver (3/4)</td>
                                            <td >Thierry Thulliez</td>
                                            <td >Pascal Bary</td>

                                            <td >1:364/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2001</td>
										    <td  >Val Royal (1 3/4)</td>
                                            <td >Jose Valdivia</td>
                                            <td >Julio Canani</td>

                                            <td >1:32:00</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2000</td>
										    <td  >War Chant (nk)</td>
                                            <td >Gary Stevens</td>
                                            <td >Neil Drysdale</td>

                                            <td >1:343/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1999</td>
										    <td  >Silic (nk)</td>
                                            <td >Corey Nakatani</td>
                                            <td >Julio Canani</td>

                                            <td >1:341/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1998</td>
										    <td  >Da Hoss (hd)</td>
                                            <td >John Velazquez</td>
                                            <td >Michael Dickinson</td>

                                            <td >1:351/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1997</td>
										    <td  >Spinning World (2)</td>
                                            <td >Cash Asmussen</td>
                                            <td >Jonathan Pease</td>

                                            <td >1:323/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1996</td>
										    <td  >Da Hoss (1 1/2)</td>
                                            <td >Gary Stevens</td>
                                            <td >Michael Dickinson</td>

                                            <td >1:354/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1995</td>
										    <td  >Ridgewood Pearl (2)</td>
                                            <td >John Murtagh</td>
                                            <td >John Oxx</td>

                                            <td >1:433/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1994</td>
										    <td  >Barathea (hd)</td>
                                            <td >Frankie Dettori</td>
                                            <td >Luca Cumani</td>

                                            <td >1:342/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1993</td>
										    <td  >Lure (21/4)</td>
                                            <td >Mike E. Smith</td>
                                            <td >Shug McGaughey</td>

                                            <td >1:332/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1992</td>
										    <td  >Lure (3)</td>
                                            <td >Mike E. Smith</td>
                                            <td >Shug McGaughey</td>

                                            <td >1:324/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1991</td>
										    <td  >Opening Verse (2 1/4)</td>
                                            <td >Pat Valenzuela</td>
                                            <td >Dick Lundy</td>

                                            <td >1:372/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1990</td>
										    <td  >Royal Academy (nk)</td>
                                            <td >Lester Piggott</td>
                                            <td >M.V. O'Brien</td>

                                            <td >1:351/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1989</td>
										    <td  >Steinlen (3/4)</td>
                                            <td >Jose Santos</td>
                                            <td >D. Wayne Lukas</td>

                                            <td >1:371/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1988</td>
										    <td  >Miesque (4)</td>
                                            <td >Freddie Head</td>
                                            <td >Francois Boutin</td>

                                            <td >1:383/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1987</td>
										    <td  >Miesque (3 1/2)</td>
                                            <td >Freddie Head</td>
                                            <td >Francois Boutin</td>

                                            <td >1:324/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1986</td>
										    <td  >Last Tycoon (hd)</td>
                                            <td >Yves St.-Martin</td>
                                            <td >Robert Collet</td>

                                            <td >1:351/5</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1985</td>
										    <td  >Cozzene (2 1/4)</td>
                                            <td >Walter Guerra</td>
                                            <td >Jan Nerud</td>

                                            <td >1:35:00</td>

                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1984</td>
										    <td  >Royal Heroine (1 1/2)</td>
                                            <td >Fernando Toro</td>
                                            <td >John Gosden</td>

                                            <td >1:323/5</td>

                                            </tr>
										  
										 										  </table></div>
{literal} 
<style type="text/css" >
.table> tbody > tr th {
    text-align: center!important
}
</style>

{/literal}