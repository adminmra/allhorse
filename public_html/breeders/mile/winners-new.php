<div id="no-more-tables">

  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th width="69">Year</th>
        <th>Winner</th>
        <th>Age</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th>Win Time</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Year">2021</td>
        <td data-title="Winner">Space Blues (IRE)</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">James Doyle</td>
        <td data-title="Trainer">Charlie Appleby</td>
        <td data-title="Owner">Godolphin, LLC</td>
        <td data-title="Win Time">1:34.01</td>
      </tr>
      <tr>
        <td data-title="Year">2019</td>
        <td data-title="Winner">Uni</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Chad Brown</td>
        <td data-title="Owner">Michael Dubb, Head of Plains Partners LLC, Haras d'Etreham, Bethlehem Stables</td>
        <td data-title="Win Time">1:32.45</td>
      </tr>
      <tr>
        <td data-title="Year">2018</td>
        <td data-title="Winner">Expert Eye</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Frankie Dettori</td>
        <td data-title="Trainer">Michael Stoute</td>
        <td data-title="Owner">Khalid Abdullah</td>
        <td data-title="Win Time">1:39.80 </td>
      </tr>
      <tr>
        <td data-title="Year">2017 </td>
        <td data-title="Winner">World Approval</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">John R. Velazquez</td>
        <td data-title="Trainer">Mark E. Casse</td>
        <td data-title="Owner">Live Oak Plantation Racing</td>
        <td data-title="Win Time">1:34.55 </td>
      </tr>
      <tr>
        <td data-title="Year">2016 </td>
        <td data-title="Winner">Tourist</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Bill Mott</td>
        <td data-title="Owner">Winstar Farm</td>
        <td data-title="Win Time">1:31.71 </td>
      </tr>
      <tr>
        <td data-title="Year">2015 </td>
        <td data-title="Winner">Tepin† </td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Julien Leparoux</td>
        <td data-title="Trainer">Mark Casse</td>
        <td data-title="Owner">Robert E Masterson </td>
        <td data-title="Win Time">1:36.69 </td>
      </tr>
      <tr>
        <td data-title="Year">2014 </td>
        <td data-title="Winner">Karakontie</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Stephane Pasquier</td>
        <td data-title="Trainer">Jonathan Pease</td>
        <td data-title="Owner">Flaxman Holdings</td>
        <td data-title="Win Time">1:32.88 </td>
      </tr>
      <tr>
        <td data-title="Year">2013 </td>
        <td data-title="Winner">Wise Dan</td>
        <td data-title="Age">6 </td>
        <td data-title="Jockey">Jose Lezcano</td>
        <td data-title="Trainer">Charles Lopresti</td>
        <td data-title="Owner">Morton Fink </td>
        <td data-title="Win Time">1:32.47 </td>
      </tr>
      <tr>
        <td data-title="Year">2012 </td>
        <td data-title="Winner">Wise Dan</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Charles Lopresti</td>
        <td data-title="Owner">Morton Fink </td>
        <td data-title="Win Time">1:31.78 </td>
      </tr>
      <tr>
        <td data-title="Year">2011 </td>
        <td data-title="Winner">Court Vision</td>
        <td data-title="Age">6 </td>
        <td data-title="Jockey">Robby Albarado</td>
        <td data-title="Trainer">Dale Romans</td>
        <td data-title="Owner">Spendthrift Farm</td>
        <td data-title="Win Time">1:37:05 </td>
      </tr>
      <tr>
        <td data-title="Year">2010 </td>
        <td data-title="Winner">Goldikova† </td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Olivier Peslier</td>
        <td data-title="Trainer">Freddy Head</td>
        <td data-title="Owner">Wertheimer et Frere</td>
        <td data-title="Win Time">1:35.16 </td>
      </tr>
      <tr>
        <td data-title="Year">2009 </td>
        <td data-title="Winner">Goldikova† </td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Olivier Peslier</td>
        <td data-title="Trainer">Freddy Head</td>
        <td data-title="Owner">Wertheimer et Frere</td>
        <td data-title="Win Time">1:32.26 </td>
      </tr>
      <tr>
        <td data-title="Year">2008 </td>
        <td data-title="Winner">Goldikova† </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Olivier Peslier</td>
        <td data-title="Trainer">Freddy Head</td>
        <td data-title="Owner">Wertheimer et Frere</td>
        <td data-title="Win Time">1:33.40 </td>
      </tr>
      <tr>
        <td data-title="Year">2007 </td>
        <td data-title="Winner">Kip Deville</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Cornelio Velasquez</td>
        <td data-title="Trainer">Richard E. Dutrow, Jr.</td>
        <td data-title="Owner">IEAH Stables et al. </td>
        <td data-title="Win Time">1:39.78 </td>
      </tr>
      <tr>
        <td data-title="Year">2006 </td>
        <td data-title="Winner">Miesque's Approval</td>
        <td data-title="Age">7 </td>
        <td data-title="Jockey">Eddie Castro</td>
        <td data-title="Trainer">Martin D. Wolfson</td>
        <td data-title="Owner">Live Oak Plantation Racing</td>
        <td data-title="Win Time">1:34.75 </td>
      </tr>
      <tr>
        <td data-title="Year">2005 </td>
        <td data-title="Winner">Artie Schiller</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Garrett Gomez</td>
        <td data-title="Trainer">James A. Jerkens</td>
        <td data-title="Owner">Timber Bay Farm/Denise Walsh </td>
        <td data-title="Win Time">1:36.10 </td>
      </tr>
      <tr>
        <td data-title="Year">2004 </td>
        <td data-title="Winner">Singletary</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">David R. Flores</td>
        <td data-title="Trainer">Don Chatlos</td>
        <td data-title="Owner">Little Red Feather Stables </td>
        <td data-title="Win Time">1:36.90 </td>
      </tr>
      <tr>
        <td data-title="Year">2003 </td>
        <td data-title="Winner">Six Perfections† </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">Pascal Bary</td>
        <td data-title="Owner">Flaxman Holdings</td>
        <td data-title="Win Time">1:33.86 </td>
      </tr>
      <tr>
        <td data-title="Year">2002 </td>
        <td data-title="Winner">Domedriver</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Thierry Thulliez</td>
        <td data-title="Trainer">Pascal Bary</td>
        <td data-title="Owner">Flaxman Holdings</td>
        <td data-title="Win Time">1:36.88 </td>
      </tr>
      <tr>
        <td data-title="Year">2001 </td>
        <td data-title="Winner">Val Royal</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Jose Valdivia, Jr.</td>
        <td data-title="Trainer">Julio Canani</td>
        <td data-title="Owner">David S. Milch</td>
        <td data-title="Win Time">1:32.05 </td>
      </tr>
      <tr>
        <td data-title="Year">2000 </td>
        <td data-title="Winner">War Chant</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Gary Stevens</td>
        <td data-title="Trainer">Neil Drysdale</td>
        <td data-title="Owner">Irving &amp; Marjorie Cowan</td>
        <td data-title="Win Time">1:34.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1999 </td>
        <td data-title="Winner">Silic</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Corey Nakatani</td>
        <td data-title="Trainer">Julio Canani</td>
        <td data-title="Owner">J. Terrence Lanni </td>
        <td data-title="Win Time">1:34.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1998 </td>
        <td data-title="Winner">Da Hoss</td>
        <td data-title="Age">6 </td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Michael W. Dickinson</td>
        <td data-title="Owner">Prestonwood/Wallstreet Stables </td>
        <td data-title="Win Time">1:35.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1997 </td>
        <td data-title="Winner">Spinning World</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Cash Asmussen</td>
        <td data-title="Trainer">Jonathan E. Pease</td>
        <td data-title="Owner">Flaxman Holdings</td>
        <td data-title="Win Time">1:32.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1996 </td>
        <td data-title="Winner">Da Hoss</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Gary Stevens</td>
        <td data-title="Trainer">Michael W. Dickinson</td>
        <td data-title="Owner">Prestonwood/Wallstreet Stables </td>
        <td data-title="Win Time">1:35.80 </td>
      </tr>
      <tr>
        <td data-title="Year">1995 </td>
        <td data-title="Winner">Ridgewood Pearl† </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Johnny Murtagh</td>
        <td data-title="Trainer">John Oxx</td>
        <td data-title="Owner">Anne Coughlan </td>
        <td data-title="Win Time">1:43.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1994 </td>
        <td data-title="Winner">Barathea</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Frankie Dettori</td>
        <td data-title="Trainer">Luca Cumani</td>
        <td data-title="Owner">G. W. Leigh</td>
        <td data-title="Win Time">1:34.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1993 </td>
        <td data-title="Winner">Lure</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">C. R. McGaughey III</td>
        <td data-title="Owner">Claiborne Farm</td>
        <td data-title="Win Time">1:33.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1992 </td>
        <td data-title="Winner">Lure</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">C. R. McGaughey III</td>
        <td data-title="Owner">Claiborne Farm/The Gamely Corp. </td>
        <td data-title="Win Time">1:32.80 </td>
      </tr>
      <tr>
        <td data-title="Year">1991 </td>
        <td data-title="Winner">Opening Verse</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Pat Valenzuela</td>
        <td data-title="Trainer">Richard J. Lundy</td>
        <td data-title="Owner">Allen E. Paulson</td>
        <td data-title="Win Time">1:37.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1990 </td>
        <td data-title="Winner">Royal Academy</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Lester Piggott</td>
        <td data-title="Trainer">Vincent O'Brien</td>
        <td data-title="Owner">Classic Ire Stable </td>
        <td data-title="Win Time">1:35.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1989 </td>
        <td data-title="Winner">Steinlen</td>
        <td data-title="Age">6 </td>
        <td data-title="Jockey">José Santos</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Daniel Wildenstein</td>
        <td data-title="Win Time">1:37.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1988 </td>
        <td data-title="Winner">Miesque† </td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Freddy Head</td>
        <td data-title="Trainer">François Boutin</td>
        <td data-title="Owner">Stavros Niarchos</td>
        <td data-title="Win Time">1:38.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1987 </td>
        <td data-title="Winner">Miesque† </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Freddy Head</td>
        <td data-title="Trainer">François Boutin</td>
        <td data-title="Owner">Stavros Niarchos</td>
        <td data-title="Win Time">1:32.80 </td>
      </tr>
      <tr>
        <td data-title="Year">1986 </td>
        <td data-title="Winner">Last Tycoon</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Yves Saint-Martin</td>
        <td data-title="Trainer">Robert Collet</td>
        <td data-title="Owner">Richard C. Strauss</td>
        <td data-title="Win Time">1:35.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1985 </td>
        <td data-title="Winner">Cozzene</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Walter Guerra</td>
        <td data-title="Trainer">Jan H. Nerud</td>
        <td data-title="Owner">John A. Nerud</td>
        <td data-title="Win Time">1:35.00 </td>
      </tr>
      <tr>
        <td data-title="Year">1984 </td>
        <td data-title="Winner">Royal Heroine† </td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Fernando Toro</td>
        <td data-title="Trainer">John Gosden</td>
        <td data-title="Owner">Robert Sangster</td>
        <td data-title="Win Time">1:32.60 </td>
      </tr>
    </tbody>
  </table>
</div>
<div>† Indicates filly/mare </div>
{literal}

<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>

{/literal}