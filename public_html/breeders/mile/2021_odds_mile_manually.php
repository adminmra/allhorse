{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Mile Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Mile"
			summary="The latest odds for the Breeders' Cup Mile available">

			<caption class="hide-sm">2021 Breeders' Cup Mile Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
				<tr>
					<td data-title="PP">1</td>
					<td data-title="Horse">Master of The Seas (IRE)</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Charlie Appleby</td>
					<td data-title="Jockey">James Doyle</td>
				</tr>
				<tr>
					<td data-title="PP">2</td>
					<td data-title="Horse">Smooth Like Strait</td>
					<td data-title="Odds">10-1</td>
					<td data-title="Trainer">Michael McCarthy</td>
					<td data-title="Jockey">Umberto Rispoli</td>
				</tr>
				<tr>
					<td data-title="PP">3</td>
					<td data-title="Horse">Space Blues(IRE)</td>
					<td data-title="Odds">3-1</td>
					<td data-title="Trainer">Charlie Appleby</td>
					<td data-title="Jockey">William Buick</td>
				</tr>
				<tr>
					<td data-title="PP">4</td>
					<td data-title="Horse">Raging Bull(FR)</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">Chad Brown</td>
					<td data-title="Jockey">Irad Ortiz Jr</td>
				</tr>
				<tr>
					<td data-title="PP">5</td>
					<td data-title="Horse">Vin de Garde(JPN)</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">Hideaki Fujiwara</td>
					<td data-title="Jockey">Yuichi Fukunaga</td>
				</tr>
				<tr>
					<td data-title="PP">6</td>
					<td data-title="Horse">Mo Forza</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Aidan O'Brien</td>
					<td data-title="Jockey">Ryan Moore</td>
				</tr>
				<tr>
					<td data-title="PP">7</td>
					<td data-title="Horse">In Love(BRZ)</td>
					<td data-title="Odds">6-1</td>
					<td data-title="Trainer">Andrew Balding</td>
					<td data-title="Jockey">Oisin Murphy</td>
				</tr>
				<tr>
					<td data-title="PP">8</td>
					<td data-title="Horse">Hit the Road</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">Dan Blacker</td>
					<td data-title="Jockey">John R. Velazquez</td>
				</tr>
				<tr>
					<td data-title="PP">9</td>
					<td data-title="Horse">Mother Earth(IRE)</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Aidan O'Brien</td>
					<td data-title="Jockey">Ryan Moore</td>
				</tr>
				<tr>
					<td data-title="PP">10</td>
					<td data-title="Horse">Blowout(GB)</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Chad C. Brown</td>
					<td data-title="Jockey">Joel Rosario</td>
				</tr>
				<tr>
					<td data-title="PP">11</td>
					<td data-title="Horse">Got Stormy</td>
					<td data-title="Odds">10-1</td>
					<td data-title="Trainer">Mark Casse</td>
					<td data-title="Jockey">Tyler Gaffalione</td>
				</tr>
				<tr>
					<td data-title="PP">12</td>
					<td data-title="Horse">Pearls Galore(FR)</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">P. Twomey</td>
					<td data-title="Jockey">William Lee</td>
				</tr>
				<tr>
					<td data-title="PP">13</td>
					<td data-title="Horse">Casa Creed</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">William Mott</td>
					<td data-title="Jockey">Junior Alvarado</td>
				</tr>
				<tr>
					<td data-title="PP">14</td>
					<td data-title="Horse">Ivar (BRZ)</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Paulo Lobo</td>
					<td data-title="Jockey">Joseph Talamo</td>
				</tr>
				
			</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}