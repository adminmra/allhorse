<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"  title="Breeders' Cup Turf Results" summary="Last year's results of the Breeders' Cup Turf." >
<tbody><tr>
							 <th width="5%">Result</th>
								 <th width="5%">Time</th> 
								 <th width="5%">Post</th> 
								 <th width="5%">Odds</th>  
								 <th width="14%">Horse</th><th width="14%">Trainer</th><th width="14%">Jockey</th> <th width="20%">Owner</th>
</tr>
<tr><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>3.80 </td><td>Highland Reel (IRE)</td><td ></td><td ></td> <td ></td></tr>
<tr><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>1.80*</td><td>Flintshire (GB) </td><td >&nbsp;</td><td >&nbsp;</td> <td ></td></tr>
<tr><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>3.60 </td><td>Found (IRE) </td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>10.90</td><td>Ulysses (IRE)</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>18.40 </td><td>Ashleyluvssugar</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>20.30</td><td>Money Multiplier</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>67.30</td><td>Texas Ryano</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>9.80</td><td>Ectot (GB)</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>83.50</td><td>Ralis</td><td >&nbsp;</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
<tr><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>61.80</td><td>Twilight Eclipse</td><td >&nbsp;</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
<tr><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>19.40</td><td>Da Big Hoss</td><td >&nbsp;</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
<tr><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>21.40</td><td>Mondialiste (IRE)</td><td >&nbsp;</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
                            

										 										  </table> </div>