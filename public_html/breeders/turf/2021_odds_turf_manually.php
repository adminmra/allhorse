	{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Turf Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Turf"
			summary="The latest odds for the Breeders' Cup Turf available">

			<caption class="hide-sm">2021 Breeders' Cup Turf Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
				<tr>
					<td data-title="PP">1</td>
					<td data-title="Horse">Rockemperor (IRE)</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">Chad Brown</td>
					<td data-title="Jockey">Javier Castellano</td>
				</tr>
        <tr>
            <td data-title="PP">2</td>
            <td data-title="Horse"><strike>United</strike></td>
            <td data-title="Odds"><strike>20-1</strike></td>
            <td data-title="Trainer"><strike>Richard Mandella</strike></td>
            <td data-title="Jockey"><strike>John R. Velazquez</strike></td>
        </tr>
        <tr>
            <td data-title="PP">3</td>
            <td data-title="Horse"><strike>Domestic Spending</strike></td>
            <td data-title="Odds"><strike>4-1</strike></td>
            <td data-title="Trainer"><strike>Chad Brown</strike></td>
            <td data-title="Jockey"><strike>Flavien Prat</strike></td>
        </tr>
        <tr>
					<td data-title="PP">4</td>
					<td data-title="Horse">Astronaut</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">John Shirreffs</td>
					<td data-title="Jockey">Victor Espinoza</td>
				</tr>
				<tr>
					<td data-title="PP">5</td>
					<td data-title="Horse">Tribhuvan (FR)</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">Chad Brown</td>
					<td data-title="Jockey">Jose Ortiz</td>
				</tr>
				<tr>
					<td data-title="PP">6</td>
					<td data-title="Horse">Acclimate</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">Philip D'Amato</td>
					<td data-title="Jockey">Ricardo Gonzalez</td>
				</tr>
				<tr>
					<td data-title="PP">7</td>
					<td data-title="Horse">Walton Street (GB)</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Charles Appleby</td>
					<td data-title="Jockey">James Doyle</td>
				</tr>
				<tr>
					<td data-title="PP">8</td>
					<td data-title="Horse">Broome (IRE)</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">Aidan O'Brien</td>
					<td data-title="Jockey">Frankie Dettori</td>
				</tr>
				<tr>
					<td data-title="PP">9</td>
					<td data-title="Horse">Sisfahan (FR)</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Henk Grewe</td>
					<td data-title="Jockey">Cristian Demuro</td>
				</tr>
				<tr>
					<td data-title="PP">10</td>
					<td data-title="Horse">Yibir (GB)</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Charles Appleby</td>
					<td data-title="Jockey">Wiliam Buick</td>
				</tr>
				<tr>
					<td data-title="PP">11</td>
					<td data-title="Horse"> Gufo</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Christophe Clement</td>
					<td data-title="Jockey">Joel Rosario</td>
				</tr>
				<tr>
					<td data-title="PP">12</td>
					<td data-title="Horse"> Teona (IRE)</td>
					<td data-title="Odds">6-1</td>
					<td data-title="Trainer">R. Varian</td>
					<td data-title="Jockey">David Egan</td>
				</tr>
				<tr>
					<td data-title="PP">13</td>
					<td data-title="Horse"> Tarnawa(IRE)</td>
					<td data-title="Odds">9-5</td>
					<td data-title="Trainer">Dermot Weld</td>
					<td data-title="Jockey">Colin Keane</td>
				</tr>
				<tr>
					<td data-title="PP">14</td>
					<td data-title="Horse"> Japan(GB)</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">Aidan O'Brien</td>
					<td data-title="Jockey">Ryan Moore</td>
				</tr>
				<tr>
					<td data-title="PP">15</td>
					<td data-title="Horse"> Bolshoi Ballet (IRE)</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">Aidan O'Brien</td>
					<td data-title="Jockey">Ryan Moore</td>
				</tr>
				<tr>
					<td data-title="PP">16</td>
					<td data-title="Horse"> Channel Marker</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">William Mott</td>
					<td data-title="Jockey">Luis Saez</td>
				</tr>
			</tbody>

		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}