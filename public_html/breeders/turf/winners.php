<div class="table-responsive"><table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"   title="Breeders' Cup Turf Winners" summary="The past winners of the Breeders Cup Turf. " >	
			  <tbody>
				  <tr>
                    <th >Year</th>
                    <th>Winner</th>
                    <th>Age</th>
                    <th>Jockey</th>
                    <th>Trainer</th>
                    <th>Win Time</th>
				  </tr>
    <tr>
      <td data-title="Year">2018</td>
      <td data-title="Winner">Enable</td>
      <td data-title="Age">4</td>
      <td data-title="Jockey">J. Gosden</td>
      <td data-title="Trainer">Andr&eacute; Fabre</td>
      <td data-title="Win Time">2:32.65</td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Talismanic</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Mickael Barzalona</td>
      <td data-title="Trainer">Andr&eacute; Fabre</td>
      <td data-title="Win Time">2:26.19 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Highland Reel (IRE) </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Seamie Heffernan</td>
      <td data-title="Trainer">Aidan O'Brien</td>
      <td data-title="Win Time">2:23.00 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Found† </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Ryan Moore</td>
      <td data-title="Trainer">Aidan O'Brien</td>
      <td data-title="Win Time">2:32.06 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Main Sequence</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">John R. Velazquez</td>
      <td data-title="Trainer">Graham Motion</td>
      <td data-title="Win Time">2:24.91 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Magician</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Ryan Moore</td>
      <td data-title="Trainer">Aidan O'Brien</td>
      <td data-title="Win Time">2:23.23 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Little Mike</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Ramon Dominguez</td>
      <td data-title="Trainer">Dale Romans</td>
      <td data-title="Win Time">2:22.83 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">St Nicholas Abbey</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Joseph O'Brien</td>
      <td data-title="Trainer">Aidan O'Brien</td>
      <td data-title="Win Time">2:28:85 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Dangerous Midge</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Frankie Dettori</td>
      <td data-title="Trainer">Brian J. Meehan</td>
      <td data-title="Win Time">2:29.40 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Conduit</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Ryan Moore</td>
      <td data-title="Trainer">Sir Michael Stoute</td>
      <td data-title="Win Time">2:23.75 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Conduit</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Ryan Moore</td>
      <td data-title="Trainer">Sir Michael Stoute</td>
      <td data-title="Win Time">2:23.42 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">English Channel</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">John R. Velazquez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Win Time">2:36.96 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Red Rocks</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Frankie Dettori</td>
      <td data-title="Trainer">Brian J. Meehan</td>
      <td data-title="Win Time">2:27.32 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Shirocco</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Christophe Soumillon</td>
      <td data-title="Trainer">Andre Fabre</td>
      <td data-title="Win Time">2:29.20 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Better Talk Now</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Ramon Dominguez</td>
      <td data-title="Trainer">H. Graham Motion</td>
      <td data-title="Win Time">2:29.70 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">High Chaparral(DH) </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Michael Kinane</td>
      <td data-title="Trainer">Aidan O'Brien</td>
      <td data-title="Win Time">2:24.24 </td>
    </tr>
    <tr>
    <td class="year">2003</td>
      <td data-title="Winner">Johar (DH)</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Richard Mandella</td>
      <td data-title="Win Time">2:24.24 </td>
      </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">High Chaparral</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Michael Kinane</td>
      <td data-title="Trainer">Aidan O'Brien</td>
      <td data-title="Win Time">2:30.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Fantastic Light</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Frankie Dettori</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Win Time">2:24.20 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Kalanisi</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Johnny Murtagh</td>
      <td data-title="Trainer">Sir Michael Stoute</td>
      <td data-title="Win Time">2:26.96 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Daylami</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Frankie Dettori</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Win Time">2:24.73 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Buck's Boy</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">P. Noel Hickey</td>
      <td data-title="Win Time">2:28.74 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Chief Bearhart</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jose Santos</td>
      <td data-title="Trainer">Mark Frostad</td>
      <td data-title="Win Time">2:23.92 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Pilsudski</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Walter Swinburn</td>
      <td data-title="Trainer">Michael Stoute</td>
      <td data-title="Win Time">2:30.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Northern Spur</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Ron McAnally</td>
      <td data-title="Win Time">2:42.07 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Tikkanen</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Jonathan Pease</td>
      <td data-title="Win Time">2:26.50 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Kotashaan</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Richard Mandella</td>
      <td data-title="Win Time">2:25.16 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Fraise</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Win Time">2:24.08 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Miss Alleged† </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eric Legrix</td>
      <td data-title="Trainer">Pascal Bary</td>
      <td data-title="Win Time">2:30.95 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">In the Wings</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">André Fabre</td>
      <td data-title="Win Time">2:29.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Prized</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Ed Delahoussaye</td>
      <td data-title="Trainer">Neil Drysdale</td>
      <td data-title="Win Time">2:28.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Great Communicator</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Ray Sibille</td>
      <td data-title="Trainer">Thad Ackel </td>
      <td data-title="Win Time">2:35.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Theatrical</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Win Time">2:24.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Manila</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jose Santos</td>
      <td data-title="Trainer">LeRoy Jolley</td>
      <td data-title="Win Time">2:25.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Pebbles</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Pat Eddery</td>
      <td data-title="Trainer">Clive Brittain</td>
      <td data-title="Win Time">2:27.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Lashkari</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Yves Saint-Martin</td>
      <td data-title="Trainer">Alain de Royer-Dupre</td>
      <td data-title="Win Time">2:25.20 </td>
    </tr>
  </tbody>
</table>
</div>
{literal} 
<style type="text/css" >
.table> tbody > tr th {
    text-align: center!important
}
</style>

{/literal}