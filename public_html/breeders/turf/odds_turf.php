	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Breeders' Cup Turf Odds"
	  }
	</script>
	{/literal}
    <div>
        <table  class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Turf - To Win">
            <caption>Horses - Breeders Cup Turf - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:39 </em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Horses - Breeders Cup Turf - To Win  - Nov 06                    </th>
            </tr>
            -->
    <tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tarnawa</td><td>7/5</td><td>+140</td></tr><tr><td>Teona</td><td>6/1</td><td>+600</td></tr><tr><td>Yibir</td><td>15/2</td><td>+750</td></tr><tr><td>Gufo</td><td>16/1</td><td>+1600</td></tr><tr><td>Walton Street</td><td>7/1</td><td>+700</td></tr><tr><td>Broome</td><td>16/1</td><td>+1600</td></tr><tr><td>Rockemperor</td><td>18/1</td><td>+1800</td></tr><tr><td>Sisfahan</td><td>12/1</td><td>+1200</td></tr><tr><td>Bolshoi Ballet</td><td>25/1</td><td>+2500</td></tr><tr><td>Japan</td><td>16/1</td><td>+1600</td></tr><tr><td>Tribhuvan</td><td>33/1</td><td>+3300</td></tr><tr><td>Channel Maker</td><td>50/1</td><td>+5000</td></tr><tr><td>Acclimate</td><td>33/1</td><td>+3300</td></tr><tr><td>Astronaut</td><td>40/1</td><td>+4000</td></tr>            </tbody>
        </table>
    </div>
    {literal}
        <style>
            table.ordenable th{cursor: pointer}
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
            <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
    