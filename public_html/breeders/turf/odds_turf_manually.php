	{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Turf Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2020 Breeders' Cup Turf"
			summary="The latest odds for the Breeders' Cup Turf available">

			<caption class="hide-sm">2020 Breeders' Cup Turf Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
				<tr>
					<td data-title="PP">1</td>
					<td data-title="Horse">Arklow</td>
					<td data-title="Odds">5-1</td>
					<td data-title="Trainer">Brad Cox</td>
					<td data-title="Jockey">Florent Geroux</td>
				</tr>
				<tr>
					<td data-title="PP">9</td>
					<td data-title="Horse">Channel Maker</td>
					<td data-title="Odds">5-1</td>
					<td data-title="Trainer">William I. Mott</td>
					<td data-title="Jockey">Manuel Franco</td>
				</tr>
				<tr>
					<td data-title="PP">5</td>
					<td data-title="Horse">Donjah (GER)</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">Henk Grewe</td>
					<td data-title="Jockey">Clement Lecoeuvre</td>
				</tr>
				<tr>
					<td data-title="PP">6</td>
					<td data-title="Horse">Lord North (IRE)</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">John H.M. Gosden</td>
					<td data-title="Jockey">Lanfranco Dettori</td>
				</tr>
				<tr>
					<td data-title="PP">2</td>
					<td data-title="Horse">Magical (IRE)</td>
					<td data-title="Odds">5-2</td>
					<td data-title="Trainer">Aidan P. O'Brien</td>
					<td data-title="Jockey">Ryan Moore</td>
				</tr>
				<tr>
					<td data-title="PP">4</td>
					<td data-title="Horse">Mehdaayih (GB)</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">John H.M. Gosden</td>
					<td data-title="Jockey">Joel Rosario</td>
				</tr>
				<tr>
					<td data-title="PP">10</td>
					<td data-title="Horse">Mogul (GB)</td>
					<td data-title="Odds">4-1</td>
					<td data-title="Trainer">Aidan P. O'Brien</td>
					<td data-title="Jockey">Pierre-Charles Boudot</td>
				</tr>
				<tr>
					<td data-title="PP">8</td>
					<td data-title="Horse">Red King</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">Philip D'Amato</td>
					<td data-title="Jockey">Umberto Rispoli</td>
				</tr>
				<tr>
					<td data-title="PP">3</td>
					<td data-title="Horse">Tarnawa (IRE)</td>
					<td data-title="Odds">6-1</td>
					<td data-title="Trainer">Dermot K. Weld</td>
					<td data-title="Jockey">Colin Keane</td>
				</tr>
				<tr>
					<td data-title="PP">7</td>
					<td data-title="Horse">United</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Richard E. Mandella</td>
					<td data-title="Jockey">Flavien Prat</td>
				</tr>

			</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}