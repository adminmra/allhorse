<h2>2021 Breeders' Cup Turf Payouts</h2>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Cup Turf Payouts" summary="Last year's Payouts of the Breeders' Cup Turf. ">
    <tbody>
      <tr>
        <th width="5%">PP</th>
        <th width="35%">Horses</th>
        <th width="10%">Win</th>
        <th width="10%">Place</th>
        <th width="10%">Show</th>
      </tr>
      <tr>
        <td>10</td>
        <td class="name">Yibir (GB)</td>
        <td>
          <div class="box">$19.00</div>
        </td>
        <td>
          <div class="box">$9.80</div>
        </td>
        <td class="payoff">
          <div class="box">$7.20</div>
        </td>
      </tr>
      <tr>
        <td>8</td>
        <td class="name">Broome (IRE)</td>
        <td></td>
        <td>
          <div class="box">$10.00</div>
        </td>
        <td class="payoff">
          <div class="box">$7.00</div>
        </td>
      </tr>
      <tr>
        <td>12</td>
        <td class="name">Teona (IRE)</td>
        <td></td>
        <td></td>
        <td class="payoff">
          <div class="box">$7.40</div>
        </td>
      </tr>

    </tbody>
  </table>
</div>
<p></p>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Cup Turf Payouts" summary="Last year's Payouts of the Breeders' Cup Turf. ">
    <tbody>
      <tr>
        <th width="20%">Wager</th>
        <th width="20%">Horses</th>
        <th width="20%">Payout</th>
      </tr>
      <tr class="odd">
        <td>$1.00 Exacta</td>
        <td>10-8</td>
        <td class="payoff">$18.10</td>
      </tr>
      <tr class="even">
        <td>$0.50 Trifecta</td>
        <td>10-8-12</td>
        <td class="payoff">$583.05</td>
      </tr>
      <tr class="odd">
        <td>$0.10 Superfecta</td>
        <td>10-8-12-14</td>
        <td class="payoff">$1,681.08</td>
      </tr>
      <tr class="even">
        <td>$1.00 Double</td>
        <td>10/10</td>
        <td class="payoff">$972.50</td>
      </tr>
      <tr class="odd">
        <td>$0.50 Pick 3</td>
        <td>3/10/10</td>
        <td class="payoff">$947.95</td>
      </tr>

    </tbody>
  </table>
</div>