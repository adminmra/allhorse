<h2>2021 Breeders' Cup Turf Results</h2>
<div id="no-more-tables">
  <table border="0" cellspacing="0" cellpadding="0"
    class="table table-condensed table-striped table-bordered ordenableResult" title="Breeders' Cup Turf Results"
    summary="Last year's results of the Breeders' Cup Turf.">
    <thead>
      <tr>
        <th>Results</th>
        <th>PP</th>
        <th>Horse</th>
        <th>Trainer</th>
        <th>Jockey</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Result">1</td>
        <td data-title="PP">10</td>
        <td data-title="Horse">Yibir (GB)</td>
        <td data-title="Trainer">Charles Appleby</td>
        <td data-title="Jockey">William Buick</td>
      </tr>

      <tr>
        <td data-title="Result">2</td>
        <td data-title="PP">8</td>
        <td data-title="Horse">Broome (IRE)</td>
        <td data-title="Trainer">Aidan P. O'Brien</td>
        <td data-title="Jockey">Frankie Dettori</td>
      </tr>
      <tr>
        <td data-title="Result">3</td>
        <td data-title="PP">12</td>
        <td data-title="Horse">Teona (IRE)</td>
        <td data-title="Trainer">R. Varian</td>
        <td data-title="Jockey">David Egan</td>
      </tr>
    </tbody>
  </table>
</div>
<script src="/assets/js/sorttable_results.js"></script>