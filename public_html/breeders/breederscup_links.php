<h2 class="Heading">
Breeders' Cup   Links
</h2>
<p><ul>
	<li><a href="http://www.usracing.com/breeders-cup/odds">Breeders' Cup Odds</a></li>
	<li><a href="http://www.usracing.com/bet-on/breeders-cup">Bet on the Breeders' Cup </a></li>
	<!-- <li><a href="http://www.usracing.com/breeders-cup/challenge">Breeders' Cup Challenge Schedule</a> -->
	<li><a  href="http://www.breeders-cup.info/" target="_blank">Breeders' Cup Info</a> - Information on the Breeders' Cup</li>
	<li><a  href="http://www.breederscup.com/" target="_blank" rel="nofollow">Breeders' Cup.com</a> - Official Site</li>
	<li><a href="http://www.usracing.com/breeders-cup/betting">Breeders' Cup Betting</a></li>
	</ul>
</p>