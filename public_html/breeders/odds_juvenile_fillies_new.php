	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile Fillies - To Win">
			<caption>Horses - Breeders Cup Juvenile Fillies - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated Nov 02, 2019 19:00 EST .</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenile Fillies - To Win  - Nov 02					</th>
			</tr>
        -->
			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
	<tr><th>PP</th><th>Horse</th><th>Fractional</th><th>American</th></tr>
	    <tr>
      <td>1</td>
      <td>Donna Veloce</td>
      <td>3/1</td>
      <td>+300</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Two Sixty</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>3</td>
      <td>Perfect Alibi</td>
      <td>10/1</td>
      <td>+1000</td>
    </tr>
    <tr>
      <td>4</td>
      <td>British Idiom</td>
      <td>7/2</td>
      <td>+350</td>
    </tr>
    <tr>
    <td>5</td>
      <td>Lazy Daisy</td>
      <td>12/1</td>
      <td>+1200</td>
    </tr>
    <tr>
      <td>6</td>
      <td>Bast</td>
      <td>7/2</td>
      <td>+350</td>
    </tr>
    <tr>
      <td>7</td>
      <td>Wicked Whisper</td>
      <td>7/2</td>
      <td>+350</td>
    </tr>
    <tr>
      <td>8</td>
      <td>K P Dreamin</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>9</td>
      <td>Comical</td>
      <td>8/1</td>
      <td>+800</td>
    </tr>
    </tbody>
		</table>
	</div>
    {literal}
        <style>
		  @media screen and (max-width: 640px) {.fa-sort{ display: block !important;} }
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="/assets/js/sorttable_post.js"></script>    {/literal}
	