	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile Fillies - To Win">
			<caption>Horses - Breeders Cup Juvenile Fillies - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated October 3, 2018.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenile Fillies - To Win  - Nov 02					</th>
			</tr>
        -->
			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Serengeti Empress</td><td>5/2</td><td>+250</td></tr><tr><td>Bellafina</td><td>9/2</td><td>+450</td></tr><tr><td>Sippican Harbor</td><td>7/1</td><td>+700</td></tr><tr><td>Mother Mother</td><td>8/1</td><td>+800</td></tr><tr><td>Feedback</td><td>9/1</td><td>+900</td></tr><tr><td>Brill</td><td>10/1</td><td>+1000</td></tr><tr><td>Chasing Yesterday</td><td>10/1</td><td>+1000</td></tr><tr><td>Restless Rider</td><td>10/1</td><td>+1000</td></tr><tr><td>Sue’s Fortune</td><td>12/1</td><td>+1200</td></tr><tr><td>Tapwater</td><td>16/1</td><td>+1600</td></tr><tr><td>Standard Deviation</td><td>16/1</td><td>+1600</td></tr><tr><td>Virginia Eloise</td><td>20/1</td><td>+2000</td></tr><tr><td>Carnival Colors</td><td>33/1</td><td>+3300</td></tr>			</tbody>
		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	