{literal}
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "about": "Breeders' Cup Juvenile Fillies Turf Odds"
    }
</script>
{/literal}
<div id="no-more-tables">
    <table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
        cellpadding="0" cellspacing="0" border="0" title="2020 Breeders' Cup Juvenile Fillies Turf"
        summary="The latest odds for the Breeders' Cup Juvenile Fillies Turf available">

        <caption class="hide-sm">2020 Breeders' Cup Juvenile Fillies Turf Odds and Post Position</caption>


        <tbody>
            <tr class='head_title hide-sm'>
                <th>PP</th>
                <th>Horse</th>
                <th>Odds</th>
                <th>Trainer</th>
                <th>Jockey</th>
            </tr>
            <tr>
                <td data-title="PP">3</td>
                <td data-title="Horse">Alda</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">H. Graham Motion</td>
                <td data-title="Jockey">John Velazquez</td>
            </tr>
            <tr>
                <td data-title="PP">5</td>
                <td data-title="Horse">Aunt Pearl (IRE)</td>
                <td data-title="Odds">3-1</td>
                <td data-title="Trainer">Brad Cox</td>
                <td data-title="Jockey">Florent Geroux</td>
            </tr>
            <tr>
                <td data-title="PP">10</td>
                <td data-title="Horse">Campanelle (IRE)</td>
                <td data-title="Odds">4-1</td>
                <td data-title="Trainer">Wesley A. Ward</td>
                <td data-title="Jockey">Lanfranco Dettori</td>
            </tr>
            <tr>
                <td data-title="PP">14</td>
                <td data-title="Horse">Editor At Large (IRE)</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">Chad C. Brown</td>
                <td data-title="Jockey">Javier Castellano</td>
            </tr>
            <tr>
                <td data-title="PP">9</td>
                <td data-title="Horse">Madone</td>
                <td data-title="Odds">10-1</td>
                <td data-title="Trainer">Simon Callaghan</td>
                <td data-title="Jockey">Flavien Prat</td>
            </tr>
            <tr>
                <td data-title="PP">2</td>
                <td data-title="Horse">Miss Amulet (IRE)</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">Ken Condon</td>
                <td data-title="Jockey">Julien Leparoux</td>
            </tr>
            <tr>
                <td data-title="PP">8</td>
                <td data-title="Horse">Mother Earth (IRE)</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">Aidan P. O'Brien</td>
                <td data-title="Jockey">Ryan Moore</td>
            </tr>
            <tr>
                <td data-title="PP">6</td>
                <td data-title="Horse">Nazuna (IRE)</td>
                <td data-title="Odds">30-1</td>
                <td data-title="Trainer">Roger Varian</td>
                <td data-title="Jockey">Luis Saez</td>
            </tr>
            <tr>
                <td data-title="PP">1</td>
                <td data-title="Horse">Oodnadatta (IRE)</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">Mrs. John Harrington</td>
                <td data-title="Jockey">Shane Foley</td>
            </tr>
            <tr>
                <td data-title="PP">4</td>
                <td data-title="Horse">Plum Ali</td>
                <td data-title="Odds">4-1</td>
                <td data-title="Trainer">Christophe Clement</td>
                <td data-title="Jockey">Joel Rosario</td>
            </tr>
            <tr>
                <td data-title="PP">11</td>
                <td data-title="Horse">Royal Approval</td>
                <td data-title="Odds">10-1</td>
                <td data-title="Trainer">Wesley A. Ward</td>
                <td data-title="Jockey">Irad Ortiz Jr</td>
            </tr>
            <tr>
                <td data-title="PP">12</td>
                <td data-title="Horse">Spanish Loveaffair</td>
                <td data-title="Odds">15-1</td>
                <td data-title="Trainer">Mark E. Casse</td>
                <td data-title="Jockey">Tyler Gaffalione</td>
            </tr>
            <tr>
                <td data-title="PP">13</td>
                <td data-title="Horse">Union Gables</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">Todd Pletcher</td>
                <td data-title="Jockey">Jose Ortiz</td>
            </tr>
            <tr>
                <td data-title="PP">7</td>
                <td data-title="Horse">Tetragonal (IRE)</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">Richard Baltas</td>
                <td data-title="Jockey">Manuel Franco</td>
            </tr>
        </tbody>
    </table>
</div>
{literal}
<style>
    @media screen and (max-width: 640px) {
        .fa-sort {
            display: block !important;
        }
    }
</style>
<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sorttable_post.js"></script> {/literal}