	<div class="table-responsive">
		
	<table class="data table table-condensed table-striped table-bordered" summary="* Horses - Breeders Cup Classic - Odds" border="1" cellpadding="0" cellspacing="0" width="100%">
			<caption> Horses - Breeders' Cup Classic - Odds</caption>
			<tbody>
				<tr>
					<th colspan="5" class="center">
					Horses - Breeders Cup' Classic - Odds  to Win  - Oct 31					</th>
			</tr>
				<tr>
	  <th>Post</th>
	  <th>Horse</th>
	  <th>Jockey</th>
	  <th>Trainer</th>
	  <th> Odds</th></tr>
      <tr>
	          <td>1</td>
	          <td>Tonalist</td>
	          <td>John Velazquez</td>
	          <td>Christophe Clement</td>
	          <td>11/2</td>
      </tr>
               <tr>
                  <td>2</td>
                  <td>Keen Ice</td>
                  <td>Irad Ortiz, Jr.</td>
                  <td>Dale Romans</td>
                  <td>10/1</td>
        </tr>
                  <tr>
                  
                <td>3</td>
                <td>Frosted</td>
                <td>Joel Rosario</td>
                <td>Kiaran McLaughlin</td>
                <td>10/1</td>
         </tr>
           <tr>   
	    <td>4</td>
	    <td>American Pharoah</td>
	    <td>Victor Espinoza</td>
	    <td>Bob Baffert</td>
	    <td>4/5</td></tr>
         
                      <tr>
                        <td>5</td>
                        <td>Gleneagles</td>
                        <td>Ryan Moore</td>
                        <td>Aidan O’Brien</td>
                        <td>9/1</td>
              </tr>
                    <tr>
                      <td>6</td>
                      <td>Effinex</td>
                      <td>Mike Smith</td>
                      <td>James Jerkens</td>
                      <td>50/1</td>
              </tr>
                     
                        
              <tr>
                            <td>7</td>
                            <td>Smooth Roller</td>
                            <td>Tyler Baze</td>
                            <td>Victor Garcia</td>
                            <td>SCRATCHED</td>
             </tr>
             
              <tr>
                    <td>8</td>
                    <td>Hard Aces</td>
                    <td>Joe Talamo</td>
                    <td>John Sadler</td>
                    <td>50/1</td>
              </tr>
     <tr>
	        <td>9</td>
	        <td>Honor Code</td>
	        <td>Javier Castellano</td>
	        <td>Claude McGaughey III</td>
	        <td>6/1</td>
           </tr>
     <tr> <td>10</td>
                          <td>Beholder</td>
                          <td>Gary Stevens</td>
                          <td>Richard Mandella</td>
                          <td>SCRATCHED</td>
           </tr>
        
      
			</tbody>
</table>	</div>