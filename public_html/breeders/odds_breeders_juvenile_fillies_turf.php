	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile Filles Turf -to Win">
			<caption>Horses - Breeders Cup Juvenile Filles Turf -to Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:29 </em>
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenile Filles Turf -to Win  - Nov 05					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Pizza Bianca</td><td>9/1</td><td>+900</td></tr><tr><td>Koala Princess</td><td>9/1</td><td>+900</td></tr><tr><td>Mise En Scene</td><td>12/1</td><td>+1200</td></tr><tr><td>Cairo Memories</td><td>12/1</td><td>+1200</td></tr><tr><td>Hello You</td><td>9/1</td><td>+900</td></tr><tr><td>Consumer Spending</td><td>7/1</td><td>+700</td></tr><tr><td>Bubble Rock</td><td>8/1</td><td>+800</td></tr><tr><td>Haughty</td><td>8/1</td><td>+800</td></tr><tr><td>Sail By</td><td>22/1</td><td>+2200</td></tr><tr><td>Turnerloose</td><td>11/1</td><td>+1100</td></tr><tr><td>California Angel</td><td>33/1</td><td>+3300</td></tr><tr><td>Cachet</td><td>14/1</td><td>+1400</td></tr><tr><td>Malavath</td><td>14/1</td><td>+1400</td></tr><tr><td>Helens Well</td><td>50/1</td><td>+5000</td></tr>			</tbody>
		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	