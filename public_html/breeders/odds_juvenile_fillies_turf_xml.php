    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile - To Win">
            <caption>Horses - Breeders Cup Juvenile - To Win</caption>
            <tbody>
                <tr>
                    <th colspan="3" class="center">
                    Horses - Breeders Cup Juvenile - To Win  - Nov 04                    </th>
            </tr>
                <tr>
                    <th colspan="3" class="center">
                    All Bets Action Run Or Not                    </th>
            </tr>
    <tr><th colspan="3" class="center">2016 Breeders Cup Juvenile - Odds To Win</th></tr><tr><th>Team</th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Not This Time </td><td>+240</td><td>12/5</td></tr><tr><td>Klimt </td><td>+325</td><td>13/4</td></tr><tr><td>Big Gray Rocket </td><td>+650</td><td>13/2</td></tr><tr><td>Practical Joke </td><td>+650</td><td>13/2</td></tr><tr><td>Straight Fire </td><td>+750</td><td>15/2</td></tr><tr><td>Classic Empire </td><td>+1300</td><td>13/1</td></tr><tr><td>Bitumen </td><td>+1500</td><td>15/1</td></tr><tr><td>Syndergaard </td><td>+1500</td><td>15/1</td></tr><tr><td>Theory </td><td>+1500</td><td>15/1</td></tr><tr><td>Three Rules </td><td>+1500</td><td>15/1</td></tr><tr><td>Favorable Outcome </td><td>+1600</td><td>16/1</td></tr><tr><td>Royal Copy </td><td>+1700</td><td>17/1</td></tr><tr><td>Runaway Lute </td><td>+2000</td><td>20/1</td></tr>                    <tr>
                        <td class="dateUpdated center" colspan="5">
                            <em id='updateemp'>  - Updated September 29, 2017 08:07:28 </em> BUSR - Official <a href="//www.usracing.com/breeders-cup/juvenile-fillies-turf">Breeders' Cup Odds</a>. <br> <a href="//www.usracing.com/breeders-cup/juvenile-fillies-turf">Breeders' Cup Juvenile Fillies Turf</a><!-- , all odds are fixed odds prices. -->

                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
    