		{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Breeders' Cup Juvenile Turf Odds"
	  }
	</script>
	{/literal}
	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenille Turf - To Win">
			<caption>Horses - Breeders Cup Juvenille Turf - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:27 </em>
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenille Turf - To Win  - Nov 05					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Modern Games</td><td>9/4</td><td>+225</td></tr><tr><td>Dubawi Legend</td><td>8/1</td><td>+800</td></tr><tr><td>Albahr</td><td>9/1</td><td>+900</td></tr><tr><td>Glounthaune</td><td>12/1</td><td>+1200</td></tr><tr><td>Portfolio Company</td><td>10/1</td><td>+1000</td></tr><tr><td>Mackinnon</td><td>18/1</td><td>+1800</td></tr><tr><td>Dakota Gold</td><td>10/1</td><td>+1000</td></tr><tr><td>Tiz The Bomb</td><td>16/1</td><td>+1600</td></tr><tr><td>Slipstream</td><td>16/1</td><td>+1600</td></tr><tr><td>Grafton Street</td><td>40/1</td><td>+4000</td></tr><tr><td>Coinage</td><td>40/1</td><td>+4000</td></tr><tr><td>Stolen Base</td><td>40/1</td><td>+4000</td></tr><tr><td>Great Max</td><td>40/1</td><td>+4000</td></tr><tr><td>Credibility</td><td>80/1</td><td>+8000</td></tr>			</tbody>
		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	