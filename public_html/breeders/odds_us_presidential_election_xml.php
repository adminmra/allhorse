    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Us Presidential Election">
            <caption>US Presidential Election</caption>
            <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="3">
                  <em id='updateemp'>Updated December 16, 2019 .</em> <!-- BUSR - Official
                  <a href="//www.usracing.com/odds/us-presidential-election">US Presidential Election Odds</a>. --> <br />
                  <!-- <a href="//www.usracing.com/odds/us-presidential-election">US Presidential Odds</a>, -- All odds are fixed odds prices.-->
                </td>
              </tr>
            </tfoot>
            <tbody>
               <!-- <tr>
                    <th colspan="3" class="center">
                                        </th>
            </tr> -->
    <tr><th></th><th>Fractional</th><th>American</th></tr><tr><td>Donald Trump</td><td>2/5</td><td>-250</td></tr><tr><td>Bernie Sanders</td><td>10/1</td><td>+1000</td></tr><tr><td>Pete Buttigieg</td><td>10/1</td><td>+1000</td></tr><tr><td>Joe Biden</td><td>7/1</td><td>+700</td></tr><tr><td>Andrew Yang</td><td>40/1</td><td>+4000</td></tr><tr><td>Elizabeth Warren</td><td>19/5</td><td>+380</td></tr><tr><td>Tulsi Gabbard</td><td>120/1</td><td>+12000</td></tr><tr><td>Amy Klobuchar</td><td>115/1</td><td>+11500</td></tr><tr><td>Cory Booker</td><td>325/1</td><td>+32500</td></tr><tr><td>Mike Pence</td><td>60/1</td><td>+6000</td></tr><tr><td>Julian Castro</td><td>1200/1</td><td>+120000</td></tr><tr><td>John Kasich</td><td>350/1</td><td>+35000</td></tr><tr><td>Michael Bloomberg</td><td>17/1</td><td>+1700</td></tr><tr><td>Hillary Clinton</td><td>33/1</td><td>+3300</td></tr><tr><td>Nikki Haley</td><td>80/1</td><td>+8000</td></tr><tr><td>Marianne Williamson</td><td>1200/1</td><td>+120000</td></tr>           <!-- <tr>
                    <th colspan="3" class="center">
                                        </th>
            </tr> -->
                </tbody>
        </table>
    </div>
    {literal}
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
        {/literal}
    