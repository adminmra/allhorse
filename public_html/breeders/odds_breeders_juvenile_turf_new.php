	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenille Turf - To Win">
			<caption>Horses - Breeders Cup Juvenille Turf - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated Nov 02, 2019 19:00 EST</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenille Turf - To Win  - Nov 03					</th>
			</tr>
        -->
			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
	<tr><th>PP</th><th>Horse</th><th>Fractional</th><th>American</th></tr>
    <tr>
      <td>1</td>
      <td>Our Country</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Structor</td>
      <td>5/1</td>
      <td>+500</td>
    </tr>
    <tr>
      <td>3</td>
      <td>Peace Achieved</td>
      <td>10/1</td>
      <td>+1000</td>
    </tr>
    <tr>
      <td>4</td>
      <td>Decorated Invader</td>
      <td>4/1</td>
      <td>+400</td>
    </tr>
    <tr>
      <td style="text-decoration-line: line-through;">5</td>
      <td style="text-decoration-line: line-through;">Vitalogy (GB)</td>
      <td style="text-decoration-line: line-through;">10/1</td>
      <td style="text-decoration-line: line-through;">+1000</td>
    </tr>
    <tr>
      <td>6</td>
      <td>Graceful Kitten</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>7</td>
      <td>Andesite</td>
      <td>12/1</td>
      <td>+1200</td>
    </tr>
    <tr>
      <td>8</td>
      <td>Billy Batts</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>9</td>
      <td>Gear Jockey</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>10</td>
      <td>War Beast</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>11</td>
      <td>Proven Strategies</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>
    <tr>
      <td>12</td>
      <td>Arizona (IRE)</td>
      <td>5/2</td>
      <td>+250</td>
    </tr>
    <tr>
      <td>13</td>
      <td>Fort Myers</td>
      <td>12/1</td>
      <td>+1200</td>
    </tr>
    <tr>
      <td>14</td>
      <td>Hit the Road</td>
      <td>10/1</td>
      <td>+1000</td>
    </tr> </tbody>		</table>
	</div>
    {literal}
        <style>
		  @media screen and (max-width: 640px) {.fa-sort{ display: block !important;} }
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="/assets/js/sorttable_post.js"></script>    {/literal}
	