	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenille Turf - To Win">
			<caption>Horses - Breeders Cup Juvenille Turf Sprint - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 3, 2018 17:09:10 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenille Turf - To Win  - Nov 03					</th>
			</tr>
        -->
			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr>
                <tr>
                  <td>Bulletin</td>
                  <td>10/1</td>
                  <td>+1000</td>
                </tr>
                <tr>
                  <td>Chelsea Cloisters</td>
                  <td>8/1</td>
                  <td>+800</td>
                </tr>
                <tr>
                  <td>So Perfect</a></td>
                  <td>12/1</td>
                  <td>+1200</td>
                </tr>
                <tr>
                  <td>Queen Of Bermuda</a></td>
                  <td>20/1</td>
                  <td>+2000</td>
                </tr>
                <tr>
                  <td>Big Drink Of Water</a></td>
                  <td>30/1</td>
                  <td>+3000</td>
                </tr>
                <tr>
                  <td>Soldier's Call</a></td>
                  <td>9/2</td>
                  <td>+450</td>
                </tr>
                <tr>
                  <td>Strike Silver</a></td>
                  <td>4/1</td>
                  <td>+400</td>
                </tr>
                <tr>
                  <td>Sergei Prokofiev</a></td>
                  <td>6/1</td>
                  <td>+600</td>
                </tr>
                <tr>
                  <td>Pocket Dynamo</a></td>
                  <td>20/1</td>
                  <td>+2000</td>
                </tr>
                <tr>
                  <td>Stillwater Cove</a></td>
                  <td>20/1</td>
                  <td>+2000</td>
                </tr>
                <tr>
                  <td>Well Done Fox</a></td>
                  <td>12/1</td>
                  <td>+1200</td>
                </tr>
                <tr>
                  <td>Moonlight Romance</a></td>
                  <td>12/1</td>
                  <td>+1200</td>
                </tr> </tbody>		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	