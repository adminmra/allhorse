	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile - To Win">
			<caption>Horses - Breeders Cup Juvenile - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 2, 2018 17:07:09 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenile - To Win  - Nov 03					</th>
			</tr>
        -->
			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr>    <tr>
      <td>Game Winner </td>
      <td>8/5</td>
      <td>+160</td>
    </tr>
    <tr>
      <td>Knicks Go</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>
    <tr>
      <td>Signalman</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Mr. Money</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>
    <tr>
      <td>Gunmetal Gray</td>
      <td>10/1</td>
      <td>+1000</td>
    </tr>
    <tr>
      <td>Dueling</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Mind Control</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Topper T</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>
    <tr>
      <td>Tight Ten</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>
    <tr>
      <td>Complexity </td>
      <td>5/2</td>
      <td>+250</td>
    </tr>
    <tr>
      <td>Derby Date</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>
    <tr>
      <td>Well Defined</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Standard Deviation</td>
      <td>12/1</td>
      <td>+1200</td>
    </tr></tbody></table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	