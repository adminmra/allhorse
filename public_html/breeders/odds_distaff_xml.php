	<div class="table-responsive">
		<table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Distaff - To Win">
			<caption>Horses - Breeders Cup Distaff - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id='updateemp'>Updated October 13, 2017.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
				<!--
            <tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Distaff - To Win  - Nov 03					</th>
			</tr>
           -->
				<!--
            <tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
           -->
	<tr><th> Team </th><th>Fractional</th><th>American</th></tr><tr><td>Stellar Wind</td><td>8/5</td><td>+160</td></tr><tr><td>Paradise Woods</td><td>9/1</td><td>+900</td></tr><tr><td>Abel Tasman</td><td>3/1</td><td>+300</td></tr><tr><td>Forever Unbridled</td><td>9/5</td><td>+180</td></tr><tr><td>Holy Helena</td><td>35/1</td><td>+3500</td></tr><tr><td>Actress</td><td>60/1</td><td>+6000</td></tr><tr><td>Berned</td><td>100/1</td><td>+10000</td></tr><tr><td>Brooklynsway</td><td>150/1</td><td>+15000</td></tr><tr><td>Elate</td><td>4/1</td><td>+400</td></tr><tr><td>Ever So Clever</td><td>60/1</td><td>+6000</td></tr><tr><td>It Tiz Well</td><td>7/1</td><td>+700</td></tr><tr><td>Jamyson 'n Ginger</td><td>50/1</td><td>+5000</td></tr><tr><td>Jordan's Henny</td><td>100/1</td><td>+10000</td></tr><tr><td>Kiriaki</td><td>120/1</td><td>+12000</td></tr><tr><td>Lockdown</td><td>65/1</td><td>+6500</td></tr><tr><td>Moana</td><td>75/1</td><td>+7500</td></tr><tr><td>Babybluesbdancing</td><td>65/1</td><td>+6500</td></tr><tr><td>Channel's Legacy</td><td>120/1</td><td>+12000</td></tr><tr><td>Faypien</td><td>85/1</td><td>+8500</td></tr><tr><td>Martini Glass</td><td>65/1</td><td>+6500</td></tr><tr><td>Proud And Fearless</td><td>70/1</td><td>+7000</td></tr><tr><td>Overture</td><td>80/1</td><td>+8000</td></tr><tr><td>Mopotism</td><td>65/1</td><td>+6500</td></tr><tr><td>Faithfully</td><td>35/1</td><td>+3500</td></tr><tr><td>Stay Fond</td><td>90/1</td><td>+9000</td></tr><tr><td>Thirstforthecup</td><td>90/1</td><td>+9000</td></tr><tr><td>You Know Too</td><td>90/1</td><td>+9000</td></tr><tr><td>Blue Prize</td><td>65/1</td><td>+6500</td></tr><tr><td>Carina Mia</td><td>60/1</td><td>+6000</td></tr><tr><td>Eskenformoney</td><td>35/1</td><td>+3500</td></tr><tr><td>Sine Wave</td><td>85/1</td><td>+8500</td></tr><tr><td>Teresa Z</td><td>80/1</td><td>+8000</td></tr><tr><td>Tiger Moth</td><td>85/1</td><td>+8500</td></tr><tr><td>Shenandoah Queen</td><td>30/1</td><td>+3000</td></tr><tr><td>Midnight Toast</td><td>100/1</td><td>+10000</td></tr><tr><td>Money'soncharlotte</td><td>30/1</td><td>+3000</td></tr><tr><td>Motown Lady</td><td>60/1</td><td>+6000</td></tr><tr><td>Champagne Room</td><td>18/1</td><td>+1800</td></tr><tr><td>Romantic Vision</td><td>45/1</td><td>+4500</td></tr><tr><td>Run And Go</td><td>70/1</td><td>+7000</td></tr><tr><td>Bishop's Pond</td><td>75/1</td><td>+7500</td></tr><tr><td>Line Of Best Fit</td><td>70/1</td><td>+7000</td></tr><tr><td>Verve's Tale</td><td>45/1</td><td>+4500</td></tr>			</tbody>
		</table>
	</div>
{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sort_table.js"></script>
{/literal}
	