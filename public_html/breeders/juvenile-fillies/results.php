<h2>2021 Breeders' Cup Juvenile Fillies Results</h2>
<div id="no-more-tables">
  <table cellspacing="0" cellpadding="0" class="data table table-bordered table-striped table-condensed ordenableResult"
    title="Breeders' Cup Juvenile Fillies Results"
    summary="Last year's results of the Breeders' Cup Juvenile Fillies. ">
    <thead>
      <tr>
        <th>Results</th>
        <th>PP</th>
        <th>Horse</th>
        <th>Trainer</th>
        <th>Jockey</th>
      </tr>
    </thead>
    <tbody>

      <tr>
        <td data-title="Result">1</td>
        <td data-title="PP">6</td>
        <td data-title="Horse">Echo Zulu</td>
        <td data-title="Trainer">Steven Asmussen</td>
        <td data-title="Jockey">Joel Rosario</td>
      </tr>

      <tr>
        <td data-title="Result">2</td>
        <td data-title="PP">5</td>
        <td data-title="Horse">Juju's Map</td>
        <td data-title="Trainer">Brad Cox</td>
        <td data-title="Jockey">Florent Geroux</td>
      </tr>

      <tr>
        <td data-title="Result">3</td>
        <td data-title="PP">4</td>
        <td data-title="Horse">Tarabi</td>
        <td data-title="Trainer">Cherie DeVaux</td>
        <td data-title="Jockey">Javier Castellano</td>
      </tr>
    
    </tbody>
  </table>
</div>
<script src="/assets/js/sorttable_results.js"></script>