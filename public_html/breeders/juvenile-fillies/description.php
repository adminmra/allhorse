<p>
In Thoroughbred racing, a Juvenile race is one restricted to 2-year-old horses. The Juvenile Fillies is restricted to 2-year-old females. It is run at 1 1/16 miles, a little more than the circumference of the racetrack. In nearly all cases over the years, the winner of the Juvenile Fillies has been named the Champion of all 2-year-old females.
</p>