{literal}
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "about": "Breeders' Cup Juvenile Fillies Odds"
    }
</script>
{/literal}
<div id="no-more-tables">
    <table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
        cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Juvenile Fillies"
        summary="The latest odds for the Breeders' Cup Juvenile Fillies available">

        <caption class="hide-sm">2021 Breeders' Cup Juvenile Fillies Odds and Post Position</caption>


        <tbody>
            <tr class='head_title hide-sm'>
                <th>PP</th>
                <th>Horse</th>
                <th>Odds</th>
                <th>Trainer</th>
                <th>Jockey</th>
            </tr>
            <tr>
                <td data-title="PP">1</td>
                <td data-title="Horse">Desert Dawn</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">Philip D'Amato</td>
                <td data-title="Jockey">Ricardo Gonzalez</td>
            </tr>
            <tr>
                <td data-title="PP">2</td>
                <td data-title="Horse">Hidden Connection</td>
                <td data-title="Odds">5-2</td>
                <td data-title="Trainer">W.Calhoun</td>
                <td data-title="Jockey">Reylu Gutierrez</td>
            </tr>
            <tr>
                <td data-title="PP">3</td>
                <td data-title="Horse">Sequist</td>
                <td data-title="Odds">15-1</td>
                <td data-title="Trainer">Dallas Stewart</td>
                <td data-title="Jockey">Junior Alvarado</td>
            </tr>
            <tr>
                <td data-title="PP">4</td>
                <td data-title="Horse">Tarabi</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">Cherie Devaux</td>
                <td data-title="Jockey">Javier Castellano</td>
            </tr>
            <tr>
                <td data-title="PP">5</td>
                <td data-title="Horse">Juju's Map</td>
                <td data-title="Odds">5-2</td>
                <td data-title="Trainer">Brad Cox</td>
                <td data-title="Jockey">LFlorent Geroux</td>
            </tr>
            <tr>
                <td data-title="PP">6</td>
                <td data-title="Horse">TEcho Zulu</td>
                <td data-title="Odds">4-5</td>
                <td data-title="Trainer">Steven M. Asmussen</td>
                <td data-title="Jockey">Joel Rosario</td>
            </tr>
            <tr>
               
            
        </tbody>
    </table>
</div>
{literal}
<style>
    @media screen and (max-width: 640px) {
        .fa-sort {
            display: block !important;
        }
    }
</style>
<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sorttable_post.js"></script> {/literal}