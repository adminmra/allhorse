{literal}

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Breeders' Cup Juvenile Fillies Winners"
  }
</script>
{/literal}
<div id="no-more-tables">
  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th style="width:69px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th>Win Time</th>
      </tr>
    </thead>
    <tbody>
	<tr>
        <td data-title="Year">2021</td>
        <td data-title="Winner">Echo Zulu</td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Steven Asmussen</td>
        <td data-title="Owner">L and N Racing LLC and Winchell Thoroughbreds LLC</td>
        <td data-title="Win Time">1:42.24</td>
      </tr>
      <tr>
		    <td data-title="Year">2020</td>
        <td data-title="Winner">Vequist</td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Robert E. Reid Jr.	</td>
        <td data-title="Owner">Gary Barber, Wachtel Stable & Swilcan Stable, LLC</td>
        <td data-title="Win Time">1:42.30</td>
      </tr>
      <tr>
        <td data-title="Year">2019</td>
        <td data-title="Winner">British Idiom</td>
        <td data-title="Jockey">Javier Castellano</td>
        <td data-title="Trainer">Brad Cox</td>
        <td data-title="Owner">Dubb, Michael, The Elkstone Group, LLC, Madaket Stables LLC & Bethlehem Stables LLC</td>
        <td data-title="Win Time">1:47.07</td>
      </tr>
      <tr>
        <td data-title="Year">2018 </td>
        <td data-title="Winner">Jaywalk </td>
        <td data-title="Jockey"> J. Rosario</td>
        <td data-title="Trainer">J. Servis</td>
        <td data-title="Owner">Cash is King LLC and DJ Stable, LLC</td>
        <td data-title="Win Time">1:43.62 </td>
      </tr>
      <tr>
        <td data-title="Year">2017 </td>
        <td data-title="Winner">Caledonia Road </td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">Ralph E. Nicks </td>
        <td data-title="Owner">Zoom and Fish Stable, Charlie Spiring & Newtown Anner Stud</td>
        <td data-title="Win Time">1:45.05 </td>
      </tr>
      <tr>
        <td data-title="Year">2016 </td>
        <td data-title="Winner">Champagne Room</td>
        <td data-title="Jockey">Mario Gutierrez</td>
        <td data-title="Trainer">Peter Eurton</td>
        <td data-title="Owner">Ciaglia Racing</td>
        <td data-title="Win Time">1:45.12 </td>
      </tr>
      <tr>
        <td data-title="Year">2015 </td>
        <td data-title="Winner">Songbird</td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">Jerry Hollendorfer</td>
        <td data-title="Owner">Fox Hill Farms</td>
        <td data-title="Win Time">1.42.73 </td>
      </tr>
      <tr>
        <td data-title="Year">2014 </td>
        <td data-title="Winner">Take Charge Brandi</td>
        <td data-title="Jockey">Victor Espinoza</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Charming Syndicate</td>
        <td data-title="Win Time">1:41.95 </td>
      </tr>
      <tr>
        <td data-title="Year">2013 </td>
        <td data-title="Winner"># Ria Antonia</td>
        <td data-title="Jockey">Javier Castellano</td>
        <td data-title="Trainer">Jeremiah C. Englehart</td>
        <td data-title="Owner">Dunn/Loooch Racing Stable</td>
        <td data-title="Win Time">1:43.02 </td>
      </tr>
      <tr>
        <td data-title="Year">2012 </td>
        <td data-title="Winner">Beholder</td>
        <td data-title="Jockey">Garrett K. Gomez</td>
        <td data-title="Trainer">Richard E. Mandella</td>
        <td data-title="Owner">Spendthrift Farm</td>
        <td data-title="Win Time">1:43.61 </td>
      </tr>
      <tr>
        <td data-title="Year">2011 </td>
        <td data-title="Winner">My Miss Aurelia</td>
        <td data-title="Jockey">Corey Nakatani</td>
        <td data-title="Trainer">Steve Asmussen</td>
        <td data-title="Owner">Stonestreet Stables/Bolton</td>
        <td data-title="Win Time">1:46.00 </td>
      </tr>
      <tr>
        <td data-title="Year">2010 </td>
        <td data-title="Winner">Awesome Feather</td>
        <td data-title="Jockey">Jeffrey Sanchez</td>
        <td data-title="Trainer">Stanley I. Gold</td>
        <td data-title="Owner">Jacks or Better Farm, Inc.</td>
        <td data-title="Win Time">1:45.17 </td>
      </tr>
      <tr>
        <td data-title="Year">2009 </td>
        <td data-title="Winner">She Be Wild</td>
        <td data-title="Jockey">Julien Leparoux</td>
        <td data-title="Trainer">Wayne Catalano</td>
        <td data-title="Owner">Nancy Mazzoni</td>
        <td data-title="Win Time">1:43.80 </td>
      </tr>
      <tr>
        <td data-title="Year">2008 </td>
        <td data-title="Winner">Stardom Bound</td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">Christopher Paasch</td>
        <td data-title="Owner">Charles Cono LLC</td>
        <td data-title="Win Time">1:40.99 </td>
      </tr>
      <tr>
        <td data-title="Year">2007 </td>
        <td data-title="Winner">Indian Blessing</td>
        <td data-title="Jockey">Garrett Gomez</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Hal J. Earnhardt III</td>
        <td data-title="Win Time">1:44.73 </td>
      </tr>
      <tr>
        <td data-title="Year">2006 </td>
        <td data-title="Winner">Dreaming of Anna</td>
        <td data-title="Jockey">Rene Douglas</td>
        <td data-title="Trainer">Wayne Catalano</td>
        <td data-title="Owner">Frank C. Calabrese</td>
        <td data-title="Win Time">1:43.81 </td>
      </tr>
      <tr>
        <td data-title="Year">2005 </td>
        <td data-title="Winner">Folklore</td>
        <td data-title="Jockey">Edgar Prado</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Bob & Beverly Lewis</td>
        <td data-title="Win Time">1:43.85 </td>
      </tr>
      <tr>
        <td data-title="Year">2004 </td>
        <td data-title="Winner">Sweet Catomine</td>
        <td data-title="Jockey">Corey Nakatani</td>
        <td data-title="Trainer">Julio Canani</td>
        <td data-title="Owner">Pam & Martin Wygod</td>
        <td data-title="Win Time">1:41.65 </td>
      </tr>
      <tr>
        <td data-title="Year">2003 </td>
        <td data-title="Winner">Halfbridled</td>
        <td data-title="Jockey">Julie Krone</td>
        <td data-title="Trainer">Richard Mandella</td>
        <td data-title="Owner">Wertheimer Farm</td>
        <td data-title="Win Time">1:42.75 </td>
      </tr>
      <tr>
        <td data-title="Year">2002 </td>
        <td data-title="Winner">Storm Flag Flying</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">C. R. McGaughey III</td>
        <td data-title="Owner">Ogden Mills Phipps</td>
        <td data-title="Win Time">1:49.60 ‡ </td>
      </tr>
      <tr>
        <td data-title="Year">2001 </td>
        <td data-title="Winner">Tempera</td>
        <td data-title="Jockey">David Flores</td>
        <td data-title="Trainer">Eoin G. Harty</td>
        <td data-title="Owner">Godolphin Racing</td>
        <td data-title="Win Time">1:41.49 </td>
      </tr>
      <tr>
        <td data-title="Year">2000 </td>
        <td data-title="Winner">Caressing</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">David Vance</td>
        <td data-title="Owner">Carl F. Pollard</td>
        <td data-title="Win Time">1:42.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1999 </td>
        <td data-title="Winner">Cash Run</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Padua Stable</td>
        <td data-title="Win Time">1:43.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1998 </td>
        <td data-title="Winner">Silverbulletday</td>
        <td data-title="Jockey">Gary Stevens</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Michael E. Pegram</td>
        <td data-title="Win Time">1:43.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1997 </td>
        <td data-title="Winner">Countess Diana</td>
        <td data-title="Jockey">Shane Sellers</td>
        <td data-title="Trainer">Patrick B. Byrne</td>
        <td data-title="Owner">Propson, Kaster et al. </td>
        <td data-title="Win Time">1:42.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1996 </td>
        <td data-title="Winner">Storm Song</td>
        <td data-title="Jockey">Craig Perret</td>
        <td data-title="Trainer">Nick Zito</td>
        <td data-title="Owner">Dogwood Stable</td>
        <td data-title="Win Time">1:43.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1995 </td>
        <td data-title="Winner">My Flag</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">C. R. McGaughey III</td>
        <td data-title="Owner">Ogden Phipps</td>
        <td data-title="Win Time">1:42.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1994 </td>
        <td data-title="Winner">Flanders</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Overbrook Farm</td>
        <td data-title="Win Time">1:45.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1993 </td>
        <td data-title="Winner">Phone Chatter</td>
        <td data-title="Jockey">Laffit Pincay, Jr.</td>
        <td data-title="Trainer">Richard Mandella</td>
        <td data-title="Owner">Herman Sarkowsky</td>
        <td data-title="Win Time">1:43.00 </td>
      </tr>
      <tr>
        <td data-title="Year">1992 </td>
        <td data-title="Winner">Eliza</td>
        <td data-title="Jockey">Pat Valenzuela</td>
        <td data-title="Trainer">Alex L. Hassinger, Jr.</td>
        <td data-title="Owner">Allen E. Paulson</td>
        <td data-title="Win Time">1:42.80 </td>
      </tr>
      <tr>
        <td data-title="Year">1991 </td>
        <td data-title="Winner">Pleasant Stage</td>
        <td data-title="Jockey">Ed Delahoussaye</td>
        <td data-title="Trainer">Chris Speckert</td>
        <td data-title="Owner">Buckland Farm</td>
        <td data-title="Win Time">1:46.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1990 </td>
        <td data-title="Winner">Meadow Star</td>
        <td data-title="Jockey">Jose Santos</td>
        <td data-title="Trainer">LeRoy Jolley</td>
        <td data-title="Owner">Carl Icahn</td>
        <td data-title="Win Time">1:44.00 </td>
      </tr>
      <tr>
        <td data-title="Year">1989 </td>
        <td data-title="Winner">Go For Wand</td>
        <td data-title="Jockey">Randy Romero</td>
        <td data-title="Trainer">William Badgett, Jr.</td>
        <td data-title="Owner">Christiana Stables</td>
        <td data-title="Win Time">1:44.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1988 </td>
        <td data-title="Winner">Open Mind</td>
        <td data-title="Jockey">Angel Cordero, Jr.</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Eugene V. Klein</td>
        <td data-title="Win Time">1:46.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1987 </td>
        <td data-title="Winner">Epitome</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">Phil Hauswald</td>
        <td data-title="Owner">John A. Bell III</td>
        <td data-title="Win Time">1:36.40 † </td>
      </tr>
      <tr>
        <td data-title="Year">1986 </td>
        <td data-title="Winner">Brave Raj</td>
        <td data-title="Jockey">Pat Valenzuela</td>
        <td data-title="Trainer">Melvin F. Stute</td>
        <td data-title="Owner">Dolly Green</td>
        <td data-title="Win Time">1:43.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1985 </td>
        <td data-title="Winner">Twilight Ridge</td>
        <td data-title="Jockey">Jorge Velasquez</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Eugene V. Klein</td>
        <td data-title="Win Time">1:35.80 † </td>
      </tr>
      <tr>
        <td data-title="Year">1984 </td>
        <td data-title="Winner"># Outstandingly</td>
        <td data-title="Jockey">Walter Guerra</td>
        <td data-title="Trainer">Pancho Martin</td>
        <td data-title="Owner">Harbor View Farm</td>
        <td data-title="Win Time">1:37.80 † </td>
      </tr>
    </tbody>
  </table>

</div>
<div>
  † 1987, 1985, 1984 - raced at a distance of 1 mile <br />
  ‡ 2002 - raced at a distance of ​1 1⁄8 miles <br />
  # 1984, 2013 - won via DQ <br />
</div>
{literal}

<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>

{/literal}