
<div class="table-responsive">
    <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" title="Breeders' Cup Juvenile Fillies Results" summary="Last year's results of the Breeders' Cup Juvenile Fillies. " >
        <tbody><tr>
                <th width="5%">Result</th><th width="5%">Time</th> <th width="5%">Post</th> <th width="5%">Odds</th>  <th width="14%">Horse</th><th width="14%">Trainer</th><th width="14%">Jockey</th> <th width="20%">Owner</th>
            </tr>
            <tr><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>33.60</td><td>Champagne Room</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>6.90 </td><td>Valadorna</td><td >&nbsp;</td><td >&nbsp;</td> <td ></td></tr>
            <tr><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>4.40*</td><td>American Gal</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>10.70</td><td>Daddys Lil Darling</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>10.20</td><td>Jamyson 'n Ginger</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>9.70</td><td>Union Strike</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>6.80</td><td>Noted and Quoted</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>18.20 </td><td>Dancing Rags
</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>15.60 </td><td>With Honors</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>4.80</td><td>Yellow Agate</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>5.20</td><td>Sweet Loretta</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
            <tr><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>102.20</td><td>Colorful Charades</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>

    </table> 
</div>