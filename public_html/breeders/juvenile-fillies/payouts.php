<h2>2021 Breeders' Cup Juvenile Fillies Payouts</h2>
<div>
    <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
        title="Breeders' Juvenile Fillies Payouts"
        summary="Last year's Payouts of the Breeders' Cup Juvenile Fillies. ">
        <tbody>
            <thead>
                <tr>
                    <th width="5%">PP</th>
                    <th width="35%">Horses</th>
                    <th width="10%">Win</th>
                    <th width="10%">Place</th>
                    <th width="10%">Show</th>
                </tr>
            </thead>

            <tr>
                <td>6</td>
                <td class="name">Echo Zulu</td>
                <td>
                    <div class="box">$3.60</div>
                </td>
                <td>
                    <div class="box">$2.60</div>
                </td>
                <td class="payoff">
                    <div class="box">$2.10</div>
                </td>
            </tr>
            <tr>
                <td>5</td>
                <td class="name">Juju's Map</td>
                <td></td>
                <td>
                    <div class="box">$3.20</div>
                </td>
                <td class="payoff">
                    <div class="box">$2.40</div>
                </td>
            </tr>
            <tr>
                <td>4</td>
                <td class="name">Tarabi</td>
                <td></td>
                <td></td>
                <td class="payoff">
                    <div class="box">$3.40</div>
                </td>
            </tr>


        </tbody>
    </table>
</div>
<p></p>
<div>
    <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
        title="Breeders' Juvenile Fillies Payouts"
        summary="Last year's Payouts of the Breeders' Cup Juvenile Fillies. ">
        <tbody>
            <thead>
                <tr>
                    <th width="20%">Wager</th>
                    <th width="20%">Horses</th>
                    <th width="20%">Payout</th>
                </tr>

            </thead>
            <tr class="odd">
                <td>$1.00 Exacta</td>
                <td>6-5</td>
                <td class="payoff">$4.90</td>
            </tr>
            <tr class="even">
                <td>$0.50 Trifecta</td>
                <td>6-5-4</td>
                <td class="payoff">$8.20</td>
            </tr>
            <tr class="odd">
                <td>$0.10 Superfecta</td>
                <td>6-5-4-2</td>
                <td class="payoff">$3.53</td>
            </tr>
            <tr class="even">
                <td>$1.00 Daily Double</td>
                <td>6/6</td>
                <td class="payoff">$15.80</td>
            </tr>
            <tr class="odd">
                <td>$0.50 Pick 3</td>
                <td>12/6/6</td>
                <td class="payoff">$31.85</td>
            </tr>
			<tr class="odd">
                <td>$0.50 Pick 4</td>
                <td>2/12/6/6</td>
                <td class="payoff">$251.20</td>

        </tbody>

    </table>
</div>