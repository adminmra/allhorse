	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile Turf Sprint -to Win">
			<caption>Horses - Breeders Cup Juvenile Turf Sprint -to Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:30 </em>
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenile Turf Sprint -to Win  - Nov 05					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Averly Jane</td><td>11/4</td><td>+275</td></tr><tr><td>Twilight Gleaming</td><td>7/1</td><td>+700</td></tr><tr><td>One Timer</td><td>9/1</td><td>+900</td></tr><tr><td>Armor</td><td>6/1</td><td>+600</td></tr><tr><td>Twilight Jet</td><td>14/1</td><td>+1400</td></tr><tr><td>Kaufymaker</td><td>22/1</td><td>+2200</td></tr><tr><td>Derrynane</td><td>8/1</td><td>+800</td></tr><tr><td>Time To Party</td><td>33/1</td><td>+3300</td></tr><tr><td>Run Curtis Run</td><td>40/1</td><td>+4000</td></tr><tr><td>Hierarchy</td><td>16/1</td><td>+1600</td></tr><tr><td>Go Bears Go</td><td>10/1</td><td>+1000</td></tr><tr><td>Vertiginous</td><td>33/1</td><td>+3300</td></tr>			</tbody>
		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	