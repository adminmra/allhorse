{literal}
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "about": "Breeders' Cup Juvenile Turf Odds"
    }
</script>
{/literal}
<div id="no-more-tables">
    <table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
        cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Juvenile Turf"
        summary="The latest odds for the Breeders' Cup Juvenile Turf available">

        <caption class="hide-sm">2021 Breeders' Cup Juvenile Turf Odds and Post Position</caption>


        <tbody>
            <tr class='head_title hide-sm'>
                <th>PP</th>
                <th>Horse</th>
                <th>Odds</th>
                <th>Trainer</th>
                <th>Jockey</th>
            </tr>
            <tr>
                <td data-title="PP">1</td>
                <td data-title="Horse"> Modern Games(IRE)</td>
                <td data-title="Odds">5-1</td>
                <td data-title="Trainer">Charlie Appleby</td>
                <td data-title="Jockey">William Buick</td>
            </tr>
            <tr>
                <td data-title="PP">2</td>
                <td data-title="Horse">Albahr(GB)</td>
                <td data-title="Odds">6-1</td>
                <td data-title="Trainer">Charles Appleby</td>
                <td data-title="Jockey">Lanfranco Dettori</td>
            </tr>
            <tr>
                <td data-title="PP">3</td>
                <td data-title="Horse">Dakota Gold</td>
                <td data-title="Odds">8-1</td>
                <td data-title="Trainer">Danny Gargan</td>
                <td data-title="Jockey">Luis Saez</td>
            </tr>
            <tr>
                <td data-title="PP">4</td>
                <td data-title="Horse">Tiz the Bomb</td>
                <td data-title="Odds">8-1</td>
                <td data-title="Trainer">Kenneth McPeek</td>
                <td data-title="Jockey">Brian Hernandez, Jr.</td>
            </tr>
            <tr>
                <td data-title="PP">5</td>
                <td data-title="Horse">Slipstream</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">Christophe Clement</td>
                <td data-title="Jockey">Joel Rosario</td>
            </tr>
            <tr>
                <td data-title="PP">6</td>
                <td data-title="Horse">Mackinnon</td>
                <td data-title="Odds">8-1</td>
                <td data-title="Trainer">Doug O'Neill</td>
                <td data-title="Jockey">Juan Hernandez</td>
            </tr>
            <tr>
                <td data-title="PP">7</td>
                <td data-title="Horse">Great Max(IRE)</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">M. Bell</td>
                <td data-title="Jockey">John R. Velazquez</td>
            </tr>
            <tr>
                <td data-title="PP">8</td>
                <td data-title="Horse">Glounthaune(IRE)</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">Aidan O'Brien</td>
                <td data-title="Jockey">Ryan Moore</td>
            </tr>
            <tr>
                <td data-title="PP">9</td>
                <td data-title="Horse">Stolen Base</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">Chad Brown</td>
                <td data-title="Jockey">Flavien Prat</td>
            </tr>
            <tr>
                <td data-title="PP">10</td>
                <td data-title="Horse">Portfolio Company</td>
                <td data-title="Odds">6-1</td>
                <td data-title="Trainer">Chad Brown</td>
                <td data-title="Jockey">Flavien Prat</td>
            </tr>
            <tr>
                <td data-title="PP">11</td>
                <td data-title="Horse">Grafton Street</td>
                <td data-title="Odds">15-1</td>
                <td data-title="Trainer">Mark Casse</td>
                <td data-title="Jockey">Tyler Gaffalione</td>
            </tr>
            <tr>
                <td data-title="PP">12</td>
                <td data-title="Horse">Credibility</td>
                <td data-title="Odds">30-1</td>
                <td data-title="Trainer">Mark Casse</td>
                <td data-title="Jockey">Mike Smith</td>
            </tr>
            <tr>
                <td data-title="PP">13</td>
                <td data-title="Horse">Coinage</td>
                <td data-title="Odds">15-1</td>
                <td data-title="Trainer">Mark Casse</td>
                <td data-title="Jockey">Florent Geroux</td>
            </tr>
            <tr>
                <td data-title="PP">14</td>
                <td data-title="Horse">Dubawi Legend (IRE)</td>
                <td data-title="Odds">4-1</td>
                <td data-title="Trainer">H. Palmer</td>
                <td data-title="Jockey">J. Doyle</td>
            </tr>
           
        </tbody>
    </table>
</div>
{literal}
<style>
    @media screen and (max-width: 640px) {
        .fa-sort {
            display: block !important;
        }
    }
</style>
<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sorttable_post.js"></script> {/literal}