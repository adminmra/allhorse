<h2>2021 Breeders' Cup Juvenile Turf Payouts
</h2>
<div id="no-more-tables">
   <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
      title="Breeders' Juvenile Turf Payouts" summary="Last year's Payouts of the Breeders' Cup Juvenile Turf. ">
      <thead>
         <tr>
            <th>PP</th>
            <th>Horses</th>
            <th>Win</th>
            <th>Place</th>
            <th>Show</th>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td data-title="PP">4</td>
            <td data-title="Horse">Tiz The Bomb</td>
            <td data-title="Win">$17.60</td>
            <td data-title="Place">$7.40</td>
            <td data-title="Show">$5.20</td>
         </tr>
         <tr>
            <td data-title="PP">6</td>
            <td data-title="Horse">Mackinnon</td>
            <td data-title="Win"></td>
            <td data-title="Place">$6.60</td>
            <td data-title="Show">$5.40</td>
         </tr>
         <tr>
            <td data-title="PP">11</td>
            <td data-title="Horse">Grafton Street</td>
            <td data-title="Win"></td>
            <td data-title="Place"></td>
            <td data-title="Show">$9.40</td>
         </tr>
      </tbody>
   </table>
</div>
<p></p>
<div>

   <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
      title="Breeders' Juvenile Turf Payouts" summary="Last year's Payouts of the Breeders' Cup Juvenile Turf. ">
      <tbody>
         <thead>
            <tr>
               <th width="20%">Wager</th>
               <th width="20%">Horses</th>
               <th width="20%">Payout</th>
            </tr>
         </thead>


         <tr class="odd">
            <td>$1.00 Exacta</td>
            <td>4-6</td>
            <td class="payoff">$46.80</td>
         </tr>
         <tr class="even">
            <td>$0.50 Trifecta</td>
            <td>4-6-11</td>
            <td class="payoff">$479.35</td>
         </tr>
         <tr class="odd">
            <td>$0.10 Superfecta</td>
            <td>4-6-11-3</td>
            <td class="payoff">$501.49</td>
         </tr>
         <tr class="even">
            <td>$1.00 Double</td>
            <td>12/2</td>
            <td class="payoff">$2.70</td>
         </tr>
         <tr class="odd">
            <td>$0.50 Pick 3</td>
            <td>1/12/4</td>
            <td class="payoff">$170.80</td>
         </tr>
      </tbody>
   </table>
</div>