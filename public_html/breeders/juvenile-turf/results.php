<h2>2021 Breeders' Cup Juvenile Turf Results
</h2>
<div id="no-more-tables">
  <table border="0" cellspacing="0" cellpadding="0"
    class="table table-condensed table-striped table-bordered ordenableResult"
    title="Breeders' Cup Juvenile Turf Results" summary="Last year's results of the Breeders' Cup Juvenile Turf. ">
    <thead>
      <tr>
        <th>Results</th>
        <th>PP</th>
        <th>Horse</th>
        <th>Trainer</th>
        <th>Jockey</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Result">1</td>
        <td data-title="PP">4</td>
        <td data-title="Horse"> Tiz the Bomb</td>
        <td data-title="Trainer">Kenneth McPeek</td>
        <td data-title="Jockey">Brian Hernandez, Jr.</td>
      </tr>

      <tr>
        <td data-title="Result">2</td>
        <td data-title="PP">6</td>
        <td data-title="Horse">Mackinnon</td>
        <td data-title="Trainer">Doug O'Neill</td>
        <td data-title="Jockey">Juan Hernandez</td>
      </tr>

      <tr>
        <td data-title="Result">3</td>
        <td data-title="PP">11</td>
        <td data-title="Horse">Grafton Street</td>
        <td data-title="Trainer">Mark Casse</td>
        <td data-title="Jockey">Tyler Gaffalione</td>
      </tr>
    </tbody>
  </table>
</div>
<script src="/assets/js/sorttable_results.js"></script>