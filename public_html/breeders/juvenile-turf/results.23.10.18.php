<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" title="Breeders' Cup Juvenile Turf Results" summary="Last year's results of the Breeders' Cup Juvenile Turf. " >
<tbody><tr>
							 <th width="5%">Result</th>
								 <th width="5%">Time</th> 
								 <th width="5%">Post</th> 
								 <th width="5%">Odds</th>  
								 <th width="14%">Horse</th><th width="14%">Jockey</th><th width="14%">Trainer</th><th width="20%">Owner</th>
</tr>
<tr><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>6.60</td><td>Oscar Performance</td><td >Ortiz, Jose</td><td >Aidan O'Brien</td> <td >&nbsp;</td></tr>
<tr><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>8.40</td><td>Lancaster Bomber</td><td >Heffernan, Seamus</td><td >&nbsp;</td> <td ></td></tr>
<tr><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>2.70*</td><td>Good Samaritan</td><td >Rosario, Joel</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>23.00</td><td>Ticonderoga</td><td >Bejarano, Rafael</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>4.30</td><td>Big Score</td><td >Prat, Flavien</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>10.50</td><td>Made You Look</td><td >Castellano, Javier</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>15.00</td><td>Channel Maker</td><td >Velazquez, John</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>28.20</td><td>Keep Quiet (FR)</td><td >Geroux, Florent</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>7.10</td><td>Intelligence Cross</td><td >Moore, Ryan</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
<tr><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>30.40</td><td>Wellabled</td>  <td >Baird, E</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>59.10</td><td>Bowies Hero</td><td >Leparoux, Julien</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
<tr><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>19.30</td><td>Favorable Outcome </td><td >Ortiz, Jr., Irad</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>13</td><td>&nbsp;</td><td>&nbsp;</td><td>81.00</td><td>J. S. Choice</td><td >Desormeaux, Kent</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
<tr><td>14</td><td>&nbsp;</td><td>&nbsp;</td><td>27.90</td><td>Rodaini</td><td >Dettori, Lanfranco</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
                            

										 										  </table> </div>