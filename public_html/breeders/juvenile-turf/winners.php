<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" title="Breeders' Cup Juvenile Turf Results" summary="The past winners of the Breeders Cup Juvenile Turf." >
<tbody>
      <tr>
        <th >Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th>Win Time</th>
      </tr>
                                          
     <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Line Of Duty</td>
      <td data-title="Jockey">W. Buick</td>
      <td data-title="Trainer">C. Appleby</td>
      <td data-title="Owner">Godolphin</td>
      <td data-title="Win Time">1:40.60 </td>
    </tr>                                         
                                          
                                          
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Mendelssohn</td>
      <td data-title="Jockey">Ryan Moore</td>
      <td data-title="Trainer">Aidan O'Brien</td>
      <td data-title="Owner">Derrick Smith</td>
      <td data-title="Win Time">1:35.97 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Oscar Performance</td>
      <td data-title="Jockey">Jose Ortiz</td>
      <td data-title="Trainer">Brian Lynch</td>
      <td data-title="Owner">Amerman Racing Stables LLC </td>
      <td data-title="Win Time">1:33.28 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Hit It A Bomb</td>
      <td data-title="Jockey">Ryan Moore</td>
      <td data-title="Trainer">Aidan O'Brien</td>
      <td data-title="Owner">Evie Stockwell </td>
      <td data-title="Win Time">1:38.86 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Hootenanny</td>
      <td data-title="Jockey">Frankie Dettori</td>
      <td data-title="Trainer">Wesley A. Ward</td>
      <td data-title="Owner">Derrick Smith</td>
      <td data-title="Win Time">1:34.79 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Outstrip (GB) </td>
      <td data-title="Jockey">Mike Smith</td>
      <td data-title="Trainer">Charlie Appleby</td>
      <td data-title="Owner">Godolphin Racing</td>
      <td data-title="Win Time">1:33.20 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">George Vancouver</td>
      <td data-title="Jockey">Ryan Moore</td>
      <td data-title="Trainer">Aidan O'Brien</td>
      <td data-title="Owner">Michael Tabor/Derrick Smith </td>
      <td data-title="Win Time">1:33.78 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Wrote</td>
      <td data-title="Jockey">Ryan Moore</td>
      <td data-title="Trainer">Aidan O'Brien</td>
      <td data-title="Owner">Michael Tabor/Derrick Smith </td>
      <td data-title="Win Time">1:37.41 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Pluck</td>
      <td data-title="Jockey">Garrett K. Gomez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Team Valor</td>
      <td data-title="Win Time">1:36.98 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Pounced</td>
      <td data-title="Jockey">Frankie Dettori</td>
      <td data-title="Trainer">John Gosden</td>
      <td data-title="Owner">Lady Serena Rothschild</td>
      <td data-title="Win Time">1:35.47 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Donativum</td>
      <td data-title="Jockey">Frankie Dettori</td>
      <td data-title="Trainer">John Gosden</td>
      <td data-title="Owner">Darley</td>
      <td data-title="Win Time">1:34.68 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Nownownow</td>
      <td data-title="Jockey">Julien Leparoux</td>
      <td data-title="Trainer">Francois Parisel</td>
      <td data-title="Owner">Fab Oak Stable </td>
      <td data-title="Win Time">1:40.48 </td>
    </tr>

										  
										

										  </tbody>
										 										  </table></div>
                                                                                  
{literal}
<style type="text/css" >
.table> tbody > tr th {
    text-align: center!important
}
</style>
{/literal}
