{literal}

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Breeders' Cup Juvenile Turf Winners"
  }
</script>
{/literal}
<div id="no-more-tables">
  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th>Win Time</th>
      </tr>
    </thead>
    <tbody>
	<tr>
        <td data-title="Year">2021</td>
        <td data-title="Winner">Modern Games (IRE)</td>
        <td data-title="Jockey">William Buick</td>
        <td data-title="Trainer">Charlie Appleby</td>
        <td data-title="Owner">Godolphin, LLC</td>
        <td data-title="Win Time">1:34.72</td>
      </tr>

      <tr>
        <td data-title="Year">2020</td>
        <td data-title="Winner">Fire At Will</td>
        <td data-title="Jockey">Ricardo Santana Jr.</td>
        <td data-title="Trainer">Michael J. Maker</td>
        <td data-title="Owner">Three Diamonds Farm</td>
        <td data-title="Win Time">1:35.81</td>
      </tr>

      <tr>
        <td data-title="Year">2019</td>
        <td data-title="Winner">Structor</td>
        <td data-title="Jockey">Jose Ortiz</td>
        <td data-title="Trainer">Chad Brown</td>
        <td data-title="Owner">Jeff Drown, Don Rachel</td>
        <td data-title="Win Time">1:35.11</td>
      </tr>

      <tr>
        <td data-title="Year">2018 </td>
        <td data-title="Winner">Line Of Duty</td>
        <td data-title="Jockey">W. Buick</td>
        <td data-title="Trainer">C. Appleby</td>
        <td data-title="Owner">Godolphin</td>
        <td data-title="Win Time">1:40.60 </td>
      </tr>
      <tr>
        <td data-title="Year">2017 </td>
        <td data-title="Winner">Mendelssohn</td>
        <td data-title="Jockey">Ryan Moore</td>
        <td data-title="Trainer">Aidan O'Brien</td>
        <td data-title="Owner">Derrick Smith</td>
        <td data-title="Win Time">1:35.97 </td>
      </tr>
      <tr>
        <td data-title="Year">2016 </td>
        <td data-title="Winner">Oscar Performance</td>
        <td data-title="Jockey">Jose Ortiz</td>
        <td data-title="Trainer">Brian Lynch</td>
        <td data-title="Owner">Amerman Racing Stables LLC </td>
        <td data-title="Win Time">1:33.28 </td>
      </tr>
      <tr>
        <td data-title="Year">2015 </td>
        <td data-title="Winner">Hit It A Bomb</td>
        <td data-title="Jockey">Ryan Moore</td>
        <td data-title="Trainer">Aidan O'Brien</td>
        <td data-title="Owner">Evie Stockwell </td>
        <td data-title="Win Time">1:38.86 </td>
      </tr>
      <tr>
        <td data-title="Year">2014 </td>
        <td data-title="Winner">Hootenanny</td>
        <td data-title="Jockey">Frankie Dettori</td>
        <td data-title="Trainer">Wesley A. Ward</td>
        <td data-title="Owner">Derrick Smith</td>
        <td data-title="Win Time">1:34.79 </td>
      </tr>
      <tr>
        <td data-title="Year">2013 </td>
        <td data-title="Winner">Outstrip (GB) </td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">Charlie Appleby</td>
        <td data-title="Owner">Godolphin Racing</td>
        <td data-title="Win Time">1:33.20 </td>
      </tr>
      <tr>
        <td data-title="Year">2012 </td>
        <td data-title="Winner">George Vancouver</td>
        <td data-title="Jockey">Ryan Moore</td>
        <td data-title="Trainer">Aidan O'Brien</td>
        <td data-title="Owner">Michael Tabor/Derrick Smith </td>
        <td data-title="Win Time">1:33.78 </td>
      </tr>
      <tr>
        <td data-title="Year">2011 </td>
        <td data-title="Winner">Wrote</td>
        <td data-title="Jockey">Ryan Moore</td>
        <td data-title="Trainer">Aidan O'Brien</td>
        <td data-title="Owner">Michael Tabor/Derrick Smith </td>
        <td data-title="Win Time">1:37.41 </td>
      </tr>
      <tr>
        <td data-title="Year">2010 </td>
        <td data-title="Winner">Pluck</td>
        <td data-title="Jockey">Garrett K. Gomez</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Owner">Team Valor</td>
        <td data-title="Win Time">1:36.98 </td>
      </tr>
      <tr>
        <td data-title="Year">2009 </td>
        <td data-title="Winner">Pounced</td>
        <td data-title="Jockey">Frankie Dettori</td>
        <td data-title="Trainer">John Gosden</td>
        <td data-title="Owner">Lady Serena Rothschild</td>
        <td data-title="Win Time">1:35.47 </td>
      </tr>
      <tr>
        <td data-title="Year">2008 </td>
        <td data-title="Winner">Donativum</td>
        <td data-title="Jockey">Frankie Dettori</td>
        <td data-title="Trainer">John Gosden</td>
        <td data-title="Owner">Darley</td>
        <td data-title="Win Time">1:34.68 </td>
      </tr>
      <tr>
        <td data-title="Year">2007 </td>
        <td data-title="Winner">Nownownow</td>
        <td data-title="Jockey">Julien Leparoux</td>
        <td data-title="Trainer">Francois Parisel</td>
        <td data-title="Owner">Fab Oak Stable </td>
        <td data-title="Win Time">1:40.48 </td>
      </tr>
    </tbody>
  </table>
</div>
{literal}

<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>

{/literal}