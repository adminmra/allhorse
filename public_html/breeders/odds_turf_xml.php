    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Turf - To Win">
            <caption>Horses - Breeders Cup Turf - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id='updateemp'>  - Updated November 5, 2018 16:36:46 </em>
                        <!-- br> All odds are fixed odds prices. -->
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Horses - Breeders Cup Turf - To Win  - Nov 03                    </th>
            </tr>
            -->
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Run Or Not All Wagers Have Action                    </th>
            </tr>
            -->
    <tr><th>Team</th><th>Fractional</th><th>American</th></tr><tr><td>Ashleylovessugar</td><td>55/1</td><td>+5500</td></tr><tr><td>Daddys Lil Darling</td><td>28/1</td><td>+2800</td></tr><tr><td>Highland Reel</td><td>8/5</td><td>+160</td></tr><tr><td>Seventh Heaven</td><td>11/1</td><td>+1100</td></tr><tr><td>Time Test</td><td>13/1</td><td>+1300</td></tr><tr><td>Ulysses</td><td>13/2</td><td>+650</td></tr><tr><td>Cracksman</td><td>13/2</td><td>+650</td></tr><tr><td>Oscar Performance</td><td>3/1</td><td>+300</td></tr><tr><td>Called To The Bar</td><td>11/2</td><td>+550</td></tr><tr><td>Good Samaritan</td><td>8/1</td><td>+800</td></tr><tr><td>Frontiersman</td><td>11/1</td><td>+1100</td></tr><tr><td>Divisidero</td><td>16/1</td><td>+1600</td></tr><tr><td>Beach Patrol</td><td>13/1</td><td>+1300</td></tr><tr><td>Homesman</td><td>13/1</td><td>+1300</td></tr>            </tbody>
        </table>
    </div>
    {literal}
        <style>
            table.ordenable th{cursor: pointer}
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sort_table.js"></script>
    {/literal}
    