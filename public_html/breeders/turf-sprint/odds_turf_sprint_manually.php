	{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Turf Sprint Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2020 Breeders' Cup Turf Sprint"
			summary="The latest odds for the Breeders' Cup Turf Sprint available">

			<caption class="hide-sm">2020 Breeders' Cup Turf Sprint Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
				<tr>
					<td data-title="PP">1</td>
					<td data-title="Horse">Big Runnuer</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Victor Garcia</td>
					<td data-title="Jockey">Juan Hernandez</td>
				</tr>
				<tr>
					<td data-title="PP">10</td>
					<td data-title="Horse">Bombard</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">Richard E. Mandella</td>
					<td data-title="Jockey">Flavien Prat</td>
				</tr>
				<tr>
					<td data-title="PP">14</td>
					<td data-title="Horse">Extravagant Kid</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">Brendan P. Walsh</td>
					<td data-title="Jockey">Umberto Rispoli</td>
				</tr>
				<tr>
					<td data-title="PP">4</td>
					<td data-title="Horse">Front Run the Fed</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Chad C. Brown</td>
					<td data-title="Jockey">Javier Castellano</td>
				</tr>
				<tr>
					<td data-title="PP">6</td>
					<td data-title="Horse">Glass Slippers (GB)</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Kevin A. Ryan</td>
					<td data-title="Jockey">Tom Eaves</td>
				</tr>
				<tr>
					<td data-title="PP">12</td>
					<td data-title="Horse">Got Stormy</td>
					<td data-title="Odds">7-2</td>
					<td data-title="Trainer">Mark E. Casse</td>
					<td data-title="Jockey">Tyler Gaffalione</td>
				</tr>
				<tr>
					<td data-title="PP">3</td>
					<td data-title="Horse">Imprimis</td>
					<td data-title="Odds">4-1</td>
					<td data-title="Trainer">Joseph F. Orseno</td>
					<td data-title="Jockey">Irad Ortiz Jr</td>
				</tr>
				<tr>
					<td data-title="PP">2</td>
					<td data-title="Horse">Just Might</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">Michelle Lovell</td>
					<td data-title="Jockey">Colby Hernandez</td>
				</tr>
				<tr>
					<td data-title="PP">7</td>
					<td data-title="Horse">Leinster</td>
					<td data-title="Odds">4-1</td>
					<td data-title="Trainer">George R. Arnold II</td>
					<td data-title="Jockey">Luis Saez</td>
				</tr>
				<tr>
					<td data-title="PP">8</td>
					<td data-title="Horse">Oleksandra (AUS)</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Neil D. Drysdale</td>
					<td data-title="Jockey">Joel Rosario</td>
				</tr>
				<tr>
					<td data-title="PP">13</td>
					<td data-title="Horse">Texas Wedge</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">Peter Miller</td>
					<td data-title="Jockey">Florent Geroux</td>
				</tr>
				<tr>
					<td data-title="PP">5</td>
					<td data-title="Horse">Wet Your Whistle</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">Michael J. Trombetta</td>
					<td data-title="Jockey">Jose Ortiz</td>
				</tr>
				<tr>
					<td data-title="PP">11</td>
					<td data-title="Horse">Wildman Jack</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">Doug F. O'Neill</td>
					<td data-title="Jockey">Manuel Franco</td>
				</tr>
				<tr>
					<td data-title="PP">9</td>
					<td data-title="Horse">Into Mystic</td>
					<td data-title="Odds">9-1</td>
					<td data-title="Trainer">Brendan Walsh</td>
					<td data-title="Jockey">Joseph Talamo</td>
				</tr>
			</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}