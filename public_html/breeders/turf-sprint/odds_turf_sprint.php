	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Breeders' Cup Turf Sprint Odds"
	  }
	</script>
	{/literal}
	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Turf Sprint - To Win">
			<caption>Horses - Breeders Cup Turf Sprint - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:41 </em>
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Turf Sprint - To Win  - Nov 06					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Golden Pal</td><td>3/1</td><td>+300</td></tr><tr><td>Emaraaty Ana</td><td>4/1</td><td>+400</td></tr><tr><td>Glass Slippers</td><td>9/1</td><td>+900</td></tr><tr><td>Kimari</td><td>10/1</td><td>+1000</td></tr><tr><td>Gear Jockey</td><td>17/2</td><td>+850</td></tr><tr><td>A Case Of You</td><td>9/1</td><td>+900</td></tr><tr><td>Lieutenant Dan</td><td>5/1</td><td>+500</td></tr><tr><td>Extravagant Kid</td><td>20/1</td><td>+2000</td></tr><tr><td>Fast Boat</td><td>20/1</td><td>+2000</td></tr><tr><td>Chaos Theory</td><td>40/1</td><td>+4000</td></tr><tr><td>Caravel</td><td>40/1</td><td>+4000</td></tr><tr><td>Charmaines Mia</td><td>33/1</td><td>+3300</td></tr>			</tbody>
		</table>
	</div>
{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
{/literal}
	