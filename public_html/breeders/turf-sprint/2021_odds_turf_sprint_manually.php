	{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Turf Sprint Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Turf Sprint"
			summary="The latest odds for the Breeders' Cup Turf Sprint available">

			<caption class="hide-sm">2021 Breeders' Cup Turf Sprint Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
  <tr>
    <td data-title="PP">1 </td>
    <td data-title="Horse">Glass Slippers</td>
    <td data-title="Odds">6-1 </td>
    <td data-title="Trainer">K. Ryan</td>
<td data-title="Jockey">T. Eaves</td>
  </tr>
  <tr>
    <td data-title="PP">2 </td>
    <td data-title="Horse">Emaraaty Ana</td>
    <td data-title="Odds">5-1 </td>
    <td data-title="Trainer">K. Ryan</td>
<td data-title="Jockey">A. Atzeni</td>
  </tr>
  <tr>
    <td data-title="PP">3 </td>
    <td data-title="Horse">Golden Pal</td>
    <td data-title="Odds">7-2 </td>
    <td data-title="Trainer">W. Ward</td>
<td data-title="Jockey">I. Ortiz, Jr.</td>
  </tr>
  <tr>
    <td data-title="PP">4 </td>
    <td data-title="Horse">Lieutenant Dan</td>
    <td data-title="Odds">6-1 </td>
    <td data-title="Trainer">S. Miyadi</td>
<td data-title="Jockey">G. Franco</td>
  </tr>
  <tr>
    <td data-title="PP">5 </td>
    <td data-title="Horse">Arrest Me Red</td>
    <td data-title="Odds">12-1 </td>
    <td data-title="Trainer">W. Ward</td>
<td data-title="Jockey">J. Velazquez</td>
  </tr>
  <tr>
    <td data-title="PP">6 </td>
    <td data-title="Horse">A Case Of You</td>
    <td data-title="Odds">8-1 </td>
    <td data-title="Trainer">A. McGuinness</td>
<td data-title="Jockey">R. Whelan</td>
  </tr>
  <tr>
    <td data-title="PP">7 </td>
    <td data-title="Horse">Charmaine's Mia</td>
    <td data-title="Odds">30-1 </td>
    <td data-title="Trainer">P. D'Amato</td>
<td data-title="Jockey">F. Prat</td>
  </tr>
  <tr>
    <td data-title="PP">8 </td>
    <td data-title="Horse">Caravel</td>
    <td data-title="Odds">20-1 </td>
    <td data-title="Trainer">H. Motion</td>
<td data-title="Jockey">J. Ortiz</td>
  </tr>
  <tr>
    <td data-title="PP">9 </td>
    <td data-title="Horse">Kimari</td>
    <td data-title="Odds">6-1 </td>
    <td data-title="Trainer">W. Ward</td>
<td data-title="Jockey">J. Rosario</td>
  </tr>
  <tr>
    <td data-title="PP">10 </td>
    <td data-title="Horse">Gear Jockey</td>
    <td data-title="Odds">5-1 </td>
    <td data-title="Trainer">G. Arnold II</td>
<td data-title="Jockey">J. Lezcano</td>
  </tr>
  <tr>
    <td data-title="PP">11 </td>
    <td data-title="Horse">Fast Boat</td>
    <td data-title="Odds">12-1 </td>
    <td data-title="Trainer">J. Sharp</td>
<td data-title="Jockey">T. Gaffalione</td>
  </tr>
  <tr>
    <td data-title="PP">12 </td>
    <td data-title="Horse">Extravagant Kid</td>
    <td data-title="Odds">12-1 </td>
    <td data-title="Trainer">B. Walsh</td>
<td data-title="Jockey">R. Moore</td>
  </tr>
  <tr>
    <td data-title="PP">13 </td>
    <td data-title="Horse">Bombard</td>
    <td data-title="Odds">20-1 </td>
    <td data-title="Trainer">R. Mandella</td>
<td data-title="Jockey">F. Prat</td>
  </tr>
  <tr>
    <td data-title="PP">14 </td>
    <td data-title="Horse">The Critical Way</td>
    <td data-title="Odds">15-1 </td>
    <td data-title="Trainer">J. Delgado</td>
<td data-title="Jockey">L. Saez</td>
  </tr>
  <tr>
    <td data-title="PP">15 </td>
    <td data-title="Horse">Chaos Theory</td>
    <td data-title="Odds">30-1 </td>
    <td data-title="Trainer">R. Hess, Jr.</td>
<td data-title="Jockey">K. Desormeaux</td>
  </tr>
  <tr>
    <td data-title="PP">16 </td>
    <td data-title="Horse">Beer Can Man</td>
    <td data-title="Odds">20-1 </td>
    <td data-title="Trainer">M. Glatt</td>
<td data-title="Jockey">J. Hernandez</td>
  </tr>
  <tr>
    <td data-title="PP">17 </td>
    <td data-title="Horse">Commander</td>
    <td data-title="Odds">30-1 </td>
    <td data-title="Trainer">P. Miller</td>
<td data-title="Jockey">F. Geroux</td>
  </tr>
  <tr>
    <td data-title="PP">18 </td>
    <td data-title="Horse">Hollywood Talent</td>
    <td data-title="Odds">30-1 </td>
    <td data-title="Trainer">J. Vazquez</td>
<td data-title="Jockey">R. Santana, Jr.</td>
  </tr>		</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}