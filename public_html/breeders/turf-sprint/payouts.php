<h2>2021 Breeders' Cup Turf Sprint Payouts</h2>
<div>
    <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
        title="Breeders' Turf Sprint Payouts" summary="Last year's Payouts of the Breeders' Cup Turf Sprint. ">
        <tbody>
            <thead>
                <tr>
                    <th width="5%">PP</th>
                    <th width="35%">Horses</th>
                    <th width="10%">Win</th>
                    <th width="10%">Place</th>
                    <th width="10%">Show</th>
                </tr>
            </thead>
            <tr>
                <td>3</td>
                <td class="name">Golden Pal</td>
                <td>
                    <div class="box">$7.00</div>
                </td>
                <td>
                    <div class="box">$4.60</div>
                </td>
                <td class="payoff">
                    <div class="box">$3.60</div>
                </td>
            </tr>
            <tr>
                <td>4</td>
                <td class="name">Lieutenant Dan</td>
                <td></td>
                <td>
                    <div class="box">$5.40</div>
                </td>
                <td class="payoff">
                    <div class="box">$4.00</div>
                </td>
            </tr>
            <tr>
                <td>7</td>
                <td class="name">Charmaine's Mia</td>
                <td></td>
                <td></td>
                <td class="payoff">
                    <div class="box">$12.20</div>
                </td>
            </tr>

        </tbody>
    </table>
</div>


<p></p>


<div>
    <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
        title="Breeders' Turf Sprint Payouts" summary="Last year's Payouts of the Breeders' Cup Turf Sprint. ">
        <tbody>
            <thead>
                <tr>
                    <th width="20%">Wager</th>
                    <th width="20%">Horses</th>
                    <th width="20%">Payout</th>
                </tr>
            </thead>
            <tr class="odd">
                <td>$1.00 Exacta</td>
                <td>3-4</td>
                <td class="payoff">$16.00</td>
            </tr>
            <tr class="even">
                <td>$0.50 Trifecta</td>
                <td>3-4-7</td>
                <td class="payoff">$147.85</td>
            </tr>
            <tr class="odd">
                <td>$0.10 Superfecta</td>
                <td>3-4-7-2</td>
                <td class="payoff">$199.02</td>
            </tr>
            <tr class="odd">
                <td>$1.00 Double</td>
                <td>4/3</td>
                <td class="payoff">$37.00</td>
            </tr>
            <tr class="even">
                <td>$0.50 Pick 3</td>
                <td>7/4/3</td>
                <td class="payoff">$94.10</td>
            </tr>
        </tbody>
    </table>
</div>