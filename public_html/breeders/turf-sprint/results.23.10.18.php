<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"  title="Breeders' Cup Turf Sprint Results" summary="Last year's results of the Breeders' Cup Turf Sprint. " >
<tbody><tr>
							 <th width="5%">Result</th>
								 <th width="5%">Time</th> 
								 <th width="5%">Post</th> 
								 <th width="5%">Odds</th>  
								 <th width="14%">Horse</th><th width="14%">Trainer</th><th width="14%">Jockey</th> <th width="20%">Owner</th>
</tr>
<tr><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>3.80*</td><td>Obviously (IRE)</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>6.50</td><td>Om</td><td >&nbsp;</td><td >&nbsp;</td> <td ></td></tr>
<tr><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>12.50</td><td>Pure Sensation</td><td >&nbsp;</td><td >&nbsp;</td> <td ></td></tr>
<tr><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>41.10</td><td>Calgary Cat</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>27.20</td><td>Green Mask (AE)</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>22.50 </td><td>Holy Lute</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>6.10</td><td>Washington DC (IRE)</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>21.20</td><td>Undrafted</td><td >&nbsp;</td><td >&nbsp;</td> <td >&nbsp;</td></tr>
<tr><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>27.20 </td><td>Mongolian Saturday</td><td >&nbsp;</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
<tr><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>6.50 </td><td>A Lot</td><td >&nbsp;</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
<tr><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>16.50 </td><td>Home of the Brave (IRE)</td><td >&nbsp;</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
<tr><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>6.90</td><td>Celestine</td><td >&nbsp;</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
<tr><td>13</td><td>&nbsp;</td><td>&nbsp;</td><td>12.50</td><td>Ambitious Brew</td><td >&nbsp;</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>
<tr><td>14</td><td>&nbsp;</td><td>&nbsp;</td><td>15.50 </td><td>Karar (GB)</td><td >&nbsp;</td> <td >&nbsp;</td>   <td >&nbsp;</td></tr>

										 										  </table> </div>