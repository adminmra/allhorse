<h2>2021 Breeders' Cup Turf Sprint Results</h2>
<div id="no-more-tables">
  <table border="0" cellspacing="0" cellpadding="0"
    class="table table-condensed table-striped table-bordered ordenableResult" title="Breeders' Cup Turf Sprint Results"
    summary="Last year's results of the Breeders' Cup Turf Sprint. ">
    <thead>

      <tr>
        <th>Results</th>
        <th>PP</th>
        <th>Horse </th>
        <th>Trainer</th>
        <th>Jockey</th>
      </tr>
    </thead>
    <tbody>

      <tr>
        <td data-title="Result">1</td>
        <td data-title="PP">3</td>
        <td data-title="Horse">Golden Pal</td>
        <td data-title="Trainer">Wesley Ward</td>
        <td data-title="Jockey">Irad Ortiz Jr</td>
      </tr>

      <tr>
        <td data-title="Result">2</td>
        <td data-title="PP">4</td>
        <td data-title="Horse">Lieutenant Dan</td>
        <td data-title="Trainer">Steven Miyadi</td>
        <td data-title="Jockey">Geovanni Franco</td>
      </tr>

      <tr>
        <td data-title="Result">3</td>
        <td data-title="PP">7</td>
        <td data-title="Horse">Charmaine's Mia</td>
        <td data-title="Trainer">Philip D'Amato</td>
        <td data-title="Jockey">Flavien Prat</td>
      </tr>
    </tbody>
  </table>
</div>
<script src="/assets/js/sorttable_results.js"></script>