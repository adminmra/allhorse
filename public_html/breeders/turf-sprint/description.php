<p>

The Turf Sprint is a fast and furious charge across the  turf course and into the homestretch at {include file='/home/ah/allhorse/public_html/breeders/track.php'}. Run at approximately 1400 yards (6.5 furlongs) for horses 3-years of age and older, this race combines both speed and agility for the international competitors.
</p>