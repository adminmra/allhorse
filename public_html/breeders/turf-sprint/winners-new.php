{literal}

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Breeders' Cup Turf Sprint Winners"
  }
</script>
{/literal}

<div id="no-more-tables">

  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Year</th>
        <th>Winner</th>
        <th>Age</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th>Win Time</th>
      </tr>
    </thead>
    <tbody>
	<tr>
        <td data-title="Year">2021</td>
        <td data-title="Winner">Golden Pal</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Irad Ortiz Jr</td>
        <td data-title="Trainer">Wesley Ward</td>
        <td data-title="Owner">Mrs. John Magnier, Michael Tabor, Derrick Smith</td>
        <td data-title="Win Time">54:75</td>
      </tr>
      <tr>
        <td data-title="Year">2020</td>
        <td data-title="Winner">Glass Slippers (GB)	</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Tom Eaves</td>
        <td data-title="Trainer">Kevin A. Ryan</td>
        <td data-title="Owner">Bearstone Stud LTD</td>
        <td data-title="Win Time">1:01.53</td>
      </tr>
      <tr>
        <td data-title="Year">2019</td>
        <td data-title="Winner">Belvoir Bay</td>
        <td data-title="Age">6</td>
        <td data-title="Jockey">Javier Castellano</td>
        <td data-title="Trainer">Peter Miller</td>
        <td data-title="Owner">Gary Barber</td>
        <td data-title="Win Time">54.83</td>
      </tr>
      <tr>
        <td data-title="Year">2018</td>
        <td data-title="Winner">Stormy Liberal  </td>
        <td data-title="Age">6</td>
        <td data-title="Jockey">D. Van Dyke</td>
        <td data-title="Trainer">Peter Miller </td>
        <td data-title="Owner">Rockingham Ranch </td>
        <td data-title="Win Time">1:04.50 </td>
      </tr>
      <tr>
        <td data-title="Year">2017 </td>
        <td data-title="Winner">Stormy Liberal  </td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Peter Miller </td>
        <td data-title="Owner">Rockingham Ranch</td>
        <td data-title="Win Time">56.12 </td>
      </tr>
      <tr>
        <td data-title="Year">2016 </td>
        <td data-title="Winner">Obviously</td>
        <td data-title="Age">8 </td>
        <td data-title="Jockey">Flavien Prat</td>
        <td data-title="Trainer">Philip D'Amato</td>
        <td data-title="Owner">Anthony Fanticola & Joseph Scardino</td>
        <td data-title="Win Time">1:11.33 </td>
      </tr>
      <tr>
        <td data-title="Year">2015 </td>
        <td data-title="Winner">Mongolian Saturday </td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Florent Geroux</td>
        <td data-title="Trainer">Enebish Ganbat </td>
        <td data-title="Owner">Mongolian Stable</td>
        <td data-title="Win Time">1:03.19 </td>
      </tr>
      <tr>
        <td data-title="Year">2014 </td>
        <td data-title="Winner">Bobby's Kitten</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Chad Brown</td>
        <td data-title="Owner">Ken & Sarah Ramsey</td>
        <td data-title="Win Time">1:12.73 </td>
      </tr>
      <tr>
        <td data-title="Year">2013 </td>
        <td data-title="Winner">Mizdirection</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">Mike Puype</td>
        <td data-title="Owner">Jungle Racing/KMN Racing et al</td>
        <td data-title="Win Time">1:12.25 </td>
      </tr>
      <tr>
        <td data-title="Year">2012 </td>
        <td data-title="Winner">Mizdirection</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">Mike Puype</td>
        <td data-title="Owner">Jungle Racing/KMN Racing et al</td>
        <td data-title="Win Time">1:11.39 </td>
      </tr>
      <tr>
        <td data-title="Year">2011 </td>
        <td data-title="Winner">Regally Ready</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Corey Nakatani</td>
        <td data-title="Trainer">Steve Asmussen</td>
        <td data-title="Owner">Vinery Stable</td>
        <td data-title="Win Time">56.48 </td>
      </tr>
      <tr>
        <td data-title="Year">2010 </td>
        <td data-title="Winner">Chamberlain Bridge</td>
        <td data-title="Age">6 </td>
        <td data-title="Jockey">Jamie Theriot</td>
        <td data-title="Trainer">Bret Calhoun</td>
        <td data-title="Owner">Carl Moore Management</td>
        <td data-title="Win Time">56.53 </td>
      </tr>
      <tr>
        <td data-title="Year">2009 </td>
        <td data-title="Winner">California Flag</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Joe Talamo</td>
        <td data-title="Trainer">Brian Koriner</td>
        <td data-title="Owner">Hi Card Ranch</td>
        <td data-title="Win Time">1:11.28 </td>
      </tr>
      <tr>
        <td data-title="Year">2008 </td>
        <td data-title="Winner">Desert Code</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Richard Migliore</td>
        <td data-title="Trainer">David Hofmans</td>
        <td data-title="Owner">Tarabilla Farms</td>
        <td data-title="Win Time">1:11.60 </td>
      </tr>
    </tbody>
  </table>
</div>
{literal}

<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>

{/literal}