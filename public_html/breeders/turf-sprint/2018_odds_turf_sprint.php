	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Turf Sprint - To Win">
			<caption>Horses - Breeders Cup Turf Sprint - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 3, 2018 17:14:12 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Turf Sprint - To Win  - Nov 03					</th>
			</tr>
        -->
			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr>
	    <tr>
      <td>Stormy Liberal</td>
      <td>4/1</td>
      <td>+400</td>
    </tr>
    <tr>
      <td>World of Trouble</td>
      <td>6/1</td>
      <td>+600</td>
    </tr>
    <tr>
      <td>Disco Partner</td>
      <td>7/2</td>
      <td>+350</td>
    </tr>
    <tr>
      <td>Ruby Notion</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Will Call</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Chanteline</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>Lost Treasure</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Bucchero</td>
      <td>10/1</td>
      <td>+1000</td>
    </tr>
    <tr>
      <td>Rainbow Heir</td>
      <td>12/1</td>
      <td>+1200</td>
    </tr>
    <tr>
      <td>Hembree </td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Conquest Tsunami</td>
      <td>6/1</td>
      <td>+600</td>
    </tr>
    <tr>
      <td>Havana Grey</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Vision Perfect</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>Richard's Boy</td>
      <td>12/1</td>
      <td>+1200</td>
    </tr></tbody>
		</table>
	</div>
{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
     <script src="//www.usracing.com/assets/js/sorttable.js"></script>
{/literal}
	