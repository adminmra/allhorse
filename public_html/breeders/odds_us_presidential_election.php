    <div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Us Presidential Election">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:44 </em>
                        <br> <!--All odds are fixed odds prices.-->
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Us Presidential Election  - Nov 02                    </th>
            </tr>
            -->
    <tr><td colspan="3" class="center"><table id="o50t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>   <br> <tr><th colspan="3" class="center">2024 US Presidential Election
</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Joe Biden</td><td>7/2</td><td>+350</td></tr><tr><td>Kamala Harris</td><td>4/1</td><td>+400</td></tr><tr><td>Donald Trump</td><td>7/1</td><td>+700</td></tr><tr><td>Ron Desantis</td><td>9/1</td><td>+900</td></tr><tr><td>Nikki Haley</td><td>12/1</td><td>+1200</td></tr><tr><td>Mike Pence</td><td>16/1</td><td>+1600</td></tr><tr><td>Alexandria Ocasio-cortez</td><td>16/1</td><td>+1600</td></tr><tr><td>Tucker Carlson</td><td>25/1</td><td>+2500</td></tr><tr><td>Ivanka Trump</td><td>25/1</td><td>+2500</td></tr><tr><td>Ted Cruz</td><td>30/1</td><td>+3000</td></tr><tr><td>Dwayne Johnson</td><td>30/1</td><td>+3000</td></tr><tr><td>Michelle Obama</td><td>30/1</td><td>+3000</td></tr><tr><td>Mike Pompeo</td><td>40/1</td><td>+4000</td></tr><tr><td>Pete Buttigieg</td><td>40/1</td><td>+4000</td></tr><tr><td>Cory Booker</td><td>40/1</td><td>+4000</td></tr><tr><td>Kristi Noem</td><td>50/1</td><td>+5000</td></tr><tr><td>Elizabeth Warren</td><td>50/1</td><td>+5000</td></tr><tr><td>Candace Owens</td><td>50/1</td><td>+5000</td></tr><tr><td>Josh Hawley</td><td>50/1</td><td>+5000</td></tr><tr><td>Tom Cotton</td><td>50/1</td><td>+5000</td></tr><tr><td>Marco Rubio</td><td>50/1</td><td>+5000</td></tr><tr><td>Dan Crenshaw</td><td>50/1</td><td>+5000</td></tr><tr><td>Stacey Abrams</td><td>50/1</td><td>+5000</td></tr><tr><td>Rand Paul</td><td>50/1</td><td>+5000</td></tr><tr><td>Amy Klobuchar</td><td>50/1</td><td>+5000</td></tr><tr><td>Donald Trump Jr</td><td>50/1</td><td>+5000</td></tr><tr><td>Tulsi Gabbard</td><td>60/1</td><td>+6000</td></tr><tr><td>Andrew Yang</td><td>60/1</td><td>+6000</td></tr><tr><td>Paul Ryan</td><td>100/1</td><td>+10000</td></tr><tr><td>Bernie Sanders</td><td>100/1</td><td>+10000</td></tr><tr><td>Hillary Clinton</td><td>100/1</td><td>+10000</td></tr><tr><td>Gavin Newsom</td><td>100/1</td><td>+10000</td></tr><tr><td>Kanye West</td><td>100/1</td><td>+10000</td></tr><tr><td>Mitt Romney</td><td>100/1</td><td>+10000</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o51t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>   <br> <tr><th colspan="3" class="center">Will Donald Trump Be Elected US President In 2024
</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Yes </td><td>7/1</td><td>+700</td></tr><tr><td>No</td><td>1/13</td><td>-1300</td></tr></tbody></table></td></tr>            </tbody>
        </table>
    </div>
    