    <div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Donald Trump - Specials">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y"); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Donald Trump - Specials  - Dec 23                    </th>
            </tr>
            -->
    <tr><td colspan="3" class="center"><table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>   <br> <tr><th colspan="3" class="center">Donald Trump To Leave Office Via Impeachment</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Yes</td><td>6/1</td><td>+600</td></tr><tr><td>No</td><td>1/12</td><td>-1200</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>   <br> <tr><th colspan="3" class="center">Will Donald Trump Resign From Office?</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Yes</td><td>6/1</td><td>+600</td></tr><tr><td>No</td><td>1/12</td><td>-1200</td></tr></tbody></table></td></tr>            <!--
            <tr>
                    <th colspan="3" class="center">
                    Donald Trump - Specials  - Dec 31                    </th>
            </tr>
            -->
    <tr><td colspan="3" class="center"><table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>   <br> <tr><th colspan="3" class="center">Will Donald Trump run for re-election in 2020?</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Yes</td><td>1/7</td><td>-700</td></tr><tr><td>No</td><td>9/2</td><td>+450</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o8t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>   <br> <tr><th colspan="3" class="center">Will Donald Trump be elected to a 2nd term as POTUS?</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Yes</td><td>10/21</td><td>-210</td></tr><tr><td>No</td><td>29/20</td><td>+145</td></tr></tbody></table></td></tr>            </tbody>
        </table>
    </div>
    
