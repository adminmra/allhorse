<h2 class='DarkBlue'>Breeders' Cup Filly & Mare Sprint</h2>

<div >
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" title="Breeders' Cup Dirt Mile" >

<tbody>

<tr><td><strong>Purse:</strong></td><td >$1,000,000</td><td><strong>Grade: </strong></td><td>1</td></tr>

<tr><td><strong>Distance: </strong></td><td>7 Furlongs</td><td><strong>Age:  </strong></td><td>3+

</td></tr>

</tbody></table></div>
