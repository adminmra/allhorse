	{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Filly &amp; Mare Sprint Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2020 Breeders' Cup Filly &amp; Mare Sprint"
			summary="The latest odds for the Breeders' Cup Filly &amp; Mare Sprint available">

			<caption class="hide-sm">2020 Breeders' Cup Filly &amp; Mare Sprint Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
				<tr>
					<td data-title="PP">9</td>
					<td data-title="Horse">Bell's the One</td>
					<td data-title="Odds">6-1</td>
					<td data-title="Trainer">Neil L. Pessin</td>
					<td data-title="Jockey">Corey Lanerie</td>
				</tr>
				<tr>
					<td data-title="PP">3</td>
					<td data-title="Horse">Come Dancing</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Carlos Martin</td>
					<td data-title="Jockey">Irad Ortiz Jr.</td>
				</tr>
				<tr>
					<td data-title="PP">2</td>
					<td data-title="Horse">Gamine</td>
					<td data-title="Odds">7-5</td>
					<td data-title="Trainer">Bob Baffert</td>
					<td data-title="Jockey">John Velazquez</td>
				</tr>
				<tr>
					<td data-title="PP">4</td>
					<td data-title="Horse">Inthemidstofbiz</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">Cipriano Contreras</td>
					<td data-title="Jockey">Emmanuel Esquivel</td>
				</tr>
				<tr>
					<td data-title="PP">8</td>
					<td data-title="Horse">Sally's Curlin</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">Dale L. Romans</td>
					<td data-title="Jockey">Brian Hernandez Jr.</td>
				</tr>
				<tr>
					<td data-title="PP">5</td>
					<td data-title="Horse">Sconsin</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">Gregory D. Foley</td>
					<td data-title="Jockey">James Graham</td>
				</tr>
				<tr>
					<td data-title="PP">7</td>
					<td data-title="Horse">Serengeti Empress</td>
					<td data-title="Odds">3-1</td>
					<td data-title="Trainer">Thomas M. Amoss</td>
					<td data-title="Jockey">Luis Saez</td>
				</tr>
				<tr>
					<td data-title="PP">1</td>
					<td data-title="Horse">Speech</td>
					<td data-title="Odds">6-1</td>
					<td data-title="Trainer">Michael W. McCarthy</td>
					<td data-title="Jockey">Javier Castellano</td>
				</tr>
				<tr>
					<td data-title="PP">6</td>
					<td data-title="Horse">Venetian Harbor</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Richard Baltas</td>
					<td data-title="Jockey">Manuel Franco</td>
				</tr>
			</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}