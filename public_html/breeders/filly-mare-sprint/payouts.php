<h2>2021 Breeders' Cup Filly & Mare Sprint Payouts
</h2>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Filly & Mare Sprint Payouts"
    summary="Last year's Payouts of the Breeders' Cup Filly & Mare Sprint. ">
    <tbody>
      <thead>
        <tr>
          <th width="5%">PP</th>
          <th width="35%">Horses</th>
          <th width="10%">Win</th>
          <th width="10%">Place</th>
          <th width="10%">Show</th>
        </tr>
      </thead>

      <tr>
        <td>2</td>
        <td class="name">Ce Ce</td>
        <td>
          <div class="box">$14.40</div>
        </td>
        <td>
          <div class="box">$5.20</div>
        </td>
        <td class="payoff">
          <div class="box">$2.40</div>
        </td>
      </tr>
      <tr>
        <td>7</td>
        <td class="name">Edgeway</td>
        <td></td>
        <td>
          <div class="box">$9.60</div>
        </td>
        <td class="payoff">
          <div class="box">$3.20</div>
        </td>
      </tr>
      <tr>
        <td>9</td>
        <td class="name">Gamine</td>
        <td></td>
        <td></td>
        <td class="payoff">
          <div class="box">$2.10</div>
        </td>
      </tr>


    </tbody>
  </table>
</div>
<p></p>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Filly & Mare Sprint Payouts"
    summary="Last year's Payouts of the Breeders' Cup Filly & Mare Sprint. ">
    <tbody>
      <thead>
        <tr>
          <th width="20%">Wager</th>
          <th width="20%">Horses</th>
          <th width="20%">Payout</th>
        </tr>
      </thead>

      <tr class="odd">
        <td>$1.00 Exacta</td>
        <td>4-3</td>
        <td class="payoff">$55.50</td>
      </tr>
      <tr class="even">
        <td>$0.50 Trifecta</td>
        <td>4-3-5</td>
        <td class="payoff">$59.60</td>
      </tr>
      <tr class="odd">
        <td>$0.10 Superfecta</td>
        <td>2-4-3-5-6</td>
        <td class="payoff">$59.60</td>
      </tr>
      <tr class="even">
        <td>$1.00 Daily Double</td>
        <td>7/4</td>
        <td class="payoff">$36.40</td>
      </tr>
      <tr class="odd">
        <td>$0.50 Pick 3</td>
        <td>2/7/4</td>
        <td class="payoff">$41.20</td>
      </tr>

    </tbody>
  </table>
</div>