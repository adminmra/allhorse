<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" title="Breeders Cup Filly & Mare Sprint Results" summary="Results of the Breeders Cup Filly & Mare Sprint" >
<tbody><tr>
<th width="5%">Result</th><th width="5%">Time</th> <th width="5%">Post</th> <th width="5%">Odds</th>  <th width="14%">Horse</th><th width="14%">Trainer</th><th width="14%">Jockey</th> <th width="20%">Owner</th></tr>
<tr><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>8.70</td><td>Finest City</td><td >&nbsp;</td><td >Smith, Mike</td> <td >&nbsp;</td></tr>
<tr><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>13.90 </td><td>Wavell Avenue</td><td >&nbsp;</td><td >Rosario, Joel</td> <td ></td></tr>
<tr><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>15.20 </td><td>Paulassilverlining</td><td >&nbsp;</td><td >Talamo, Joseph</td> <td >&nbsp;</td></tr>
<tr><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>4.90 </td><td>Tara's Tango</td><td >&nbsp;</td><td >Bejarano, Rafel</td> <td >&nbsp;</td></tr>
<tr><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>7.20 </td><td>By the Moon</td><td >&nbsp;</td><td >Ortiz, Jose</td> <td >&nbsp;</td></tr>
<tr><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>89.80</td><td>Spelling Again</td><td >&nbsp;</td><td >Saez, Luis</td> <td >&nbsp;</td></tr>
<tr><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>3.80 </td><td>Haveyougoneaway</td><td >&nbsp;</td><td >Velazquez, John</td> <td >&nbsp;</td></tr>
<tr><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>10.80 </td><td>Irish Jasper</td><td >&nbsp;</td><td >Ortiz Jr, Irad</td> <td >&nbsp;</td></tr>
<tr><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>3.10*</td><td>Carina Mia</td><td >&nbsp;</td> <td >Leparoux, Julien</td>   <td >&nbsp;</td></tr>                             <tr><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>38.20</td><td>Wonder Gal</td><td >&nbsp;</td> <td >Carmouche, Kendrick</td>   <td >&nbsp;</td></tr>
<tr><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>18.60</td><td>Gloryzapper</td><td >&nbsp;</td> <td >Elliot, Stuart</td>   <td >&nbsp;</td></tr>                             <tr><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>44.70</td><td>Paola Queen</td><td >&nbsp;</td> <td >Castellano, Javier</td>   <td >&nbsp;</td></tr>
<tr><td>13	</td><td>&nbsp;</td><td>&nbsp;</td><td>45.60</td><td>Gomo</td><td >&nbsp;</td> <td >Gutierrez, Mario</td>   <td >&nbsp;</td></tr>                             