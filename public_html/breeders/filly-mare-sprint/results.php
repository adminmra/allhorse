<h2>2021 Breeders' Cup Filly & Mare Sprint Results</h2>
<div id="no-more-tables">
  <table border="0" cellspacing="0" cellpadding="0"
    class="data table table-condensed table-striped table-bordered ordenableResult"
    title="Breeders Cup Filly & Mare Sprint Results" summary="Results of the Breeders Cup Filly & Mare Sprint">
    <thead>
      <tr>
        <th>Results</th>
        <th>PP</th>
        <th>Horse</th>
        <th>Trainer</th>
        <th>Jockey</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Result">1</td>
        <td data-title="PP">4</td>
        <td data-title="Horse">Ce Ce</td>
        <td data-title="Trainer">Michael McCarthy</td>
        <td data-title="Jockey">Victor Espinoza</td>
      </tr>
      <tr>
        <td data-title="Result">2</td>
        <td data-title="PP">3</td>
        <td data-title="Horse">Edgeway</td>
        <td data-title="Trainer">John Sadler</td>
        <td data-title="Jockey">Joel Rosario</td>
      </tr>
      <tr>
        <td data-title="Result">3</td>
        <td data-title="PP">5</td>
        <td data-title="Horse">Gamine</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Jockey">John Velazquez</td>
      </tr>

    </tbody>

  </table>

</div>

<script src="/assets/js/sorttable_results.js"></script>