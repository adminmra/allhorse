	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Breeders' Cup Filly &amp; Mare Sprint Odds"
	  }
	</script>
	{/literal}
	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Filly And Mare Sprint">
			<caption>Horses - Breeders Cup Filly And Mare Sprint</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:32 </em>
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Filly And Mare Sprint  - Nov 06					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Gamine</td><td>5/8</td><td>-160</td></tr><tr><td>Bella Sofia</td><td>5/2</td><td>+250</td></tr><tr><td>Ce Ce</td><td>7/1</td><td>+700</td></tr><tr><td>Edgeway</td><td>22/1</td><td>+2200</td></tr><tr><td>Proud Emma</td><td>80/1</td><td>+8000</td></tr>			</tbody>
		</table>
	</div>
{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
{/literal}
	