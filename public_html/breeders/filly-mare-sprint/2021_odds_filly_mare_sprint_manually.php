{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Filly &amp; Mare Sprint Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Filly &amp; Mare Sprint"
			summary="The latest odds for the Breeders' Cup Filly &amp; Mare Sprint available">

			<caption class="hide-sm">2021 Breeders' Cup Filly &amp; Mare Sprint Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
				<tr>
					<td data-title="PP">1</td>
					<td data-title="Horse">Proud Emma</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">Peter Miller</td>
					<td data-title="Jockey">Flavien Prat</td>
				</tr>
				<tr>
					<td data-title="PP">3</td>
					<td data-title="Horse">Edgeway</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">John Sadler</td>
					<td data-title="Jockey">Joel Rosario</td>
				</tr>
				<tr>
					<td data-title="PP">4</td>
					<td data-title="Horse">Ce Ce</td>
					<td data-title="Odds">4-1</td>
					<td data-title="Trainer">Michael McCarthy</td>
					<td data-title="Jockey">Victor Espinoza</td>
				</tr>
				<tr>
					<td data-title="PP">5</td>
					<td data-title="Horse">Gamine</td>
					<td data-title="Odds">3-5</td>
					<td data-title="Trainer">Bob Baffert</td>
					<td data-title="Jockey">John Velazquez</td>
				</tr>
				<tr>
					<td data-title="PP">6</td>
					<td data-title="Horse">Bella Sofia</td>
					<td data-title="Odds">5-2</td>
					<td data-title="Trainer">Rudy Rodriguez</td>
					<td data-title="Jockey">Luis Saez</td>
				</tr>
		
			</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}