<div id="no-more-tables">
  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Year</th>
        <th>Winner</th>
        <th>Age</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th>Win Time</th>
      </tr>
    </thead>
    <tbody>
	<tr>
        <td data-title="Year">2021</td>
        <td data-title="Winner">Ce Ce</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Victor Espinoza</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Bo Hirsch, LLC</td>
        <td data-title="Win Time">1:21.00</td>
      </tr>
      <tr>
        <td data-title="Year">2020</td>
        <td data-title="Winner">Gamine</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Michael McCarthy</td>
        <td data-title="Owner">Michael Lund Petersen</td>
        <td data-title="Win Time">1:20.20</td>
      </tr>
      <tr>
        <td data-title="Year">2019</td>
        <td data-title="Winner">Covfefe</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Brad Cox</td>
        <td data-title="Owner">LNJ Foxwoods</td>
        <td data-title="Win Time">1:22.40 </td>
      </tr>
      <tr>
        <td data-title="Year">2018</td>
        <td data-title="Winner">Shamrock Rose</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">I. Ortiz, Jr.</td>
        <td data-title="Trainer">M. Casse</td>
        <td data-title="Owner">Conrad Farms</td>
        <td data-title="Win Time">1:23.13 </td>
      </tr>
      <tr>
        <td data-title="Year">2017 </td>
        <td data-title="Winner">Bar of Gold</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Irad Ortiz Jr.</td>
        <td data-title="Trainer">John C. Kimmel</td>
        <td data-title="Owner">Chester & Mary Broman</td>
        <td data-title="Win Time">1:22.63 </td>
      </tr>
      <tr>
        <td data-title="Year">2016 </td>
        <td data-title="Winner">Finest City</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">Ian Kruljac</td>
        <td data-title="Owner">Seltzer Thoroughbreds</td>
        <td data-title="Win Time">1:22.37 </td>
      </tr>
      <tr>
        <td data-title="Year">2015 </td>
        <td data-title="Winner">Wavell Avenue</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Chad Brown</td>
        <td data-title="Owner">Michael Dubb, David Simon & Bethlehem Stables</td>
        <td data-title="Win Time">1:22.39 </td>
      </tr>
      <tr>
        <td data-title="Year">2014 </td>
        <td data-title="Winner">Judy the Beauty</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">Wesley Ward</td>
        <td data-title="Owner">Wesley Ward</td>
        <td data-title="Win Time">1:21.92 </td>
      </tr>
      <tr>
        <td data-title="Year">2013 </td>
        <td data-title="Winner">Groupie Doll</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Rajiv Maragh</td>
        <td data-title="Trainer">William Bradley</td>
        <td data-title="Owner">Bradley F & W/Hurst/Brent</td>
        <td data-title="Win Time">1:20.75 </td>
      </tr>
      <tr>
        <td data-title="Year">2012 </td>
        <td data-title="Winner">Groupie Doll</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Rajiv Maragh</td>
        <td data-title="Trainer">William Bradley</td>
        <td data-title="Owner">Bradley F & W/Hurst/Brent</td>
        <td data-title="Win Time">1:20.72 </td>
      </tr>
      <tr>
        <td data-title="Year">2011 </td>
        <td data-title="Winner">Musical Romance</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Juan Leyva</td>
        <td data-title="Trainer">William A. Caplan</td>
        <td data-title="Owner">Pinnacle Racing Stable</td>
        <td data-title="Win Time">1:23.47 </td>
      </tr>
      <tr>
        <td data-title="Year">2010 </td>
        <td data-title="Winner">Dubai Majesty</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Jamie Theriot</td>
        <td data-title="Trainer">W. Bret Calhoun</td>
        <td data-title="Owner">Martin Racing Stable & Dan Morgan</td>
        <td data-title="Win Time">1:22.33 </td>
      </tr>
      <tr>
        <td data-title="Year">2009 </td>
        <td data-title="Winner">Informed Decision</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Julien Leparoux</td>
        <td data-title="Trainer">Jonathan Sheppard</td>
        <td data-title="Owner">Augustin Stable</td>
        <td data-title="Win Time">1:21.66 </td>
      </tr>
      <tr>
        <td data-title="Year">2008 </td>
        <td data-title="Winner">Ventura</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Garrett Gomez</td>
        <td data-title="Trainer">Robert J. Frankel</td>
        <td data-title="Owner">Juddmonte Farms</td>
        <td data-title="Win Time">1:19.90 </td>
      </tr>
      <tr>
        <td data-title="Year">2007 </td>
        <td data-title="Winner">Maryfield</td>
        <td data-title="Age">6 </td>
        <td data-title="Jockey">Elvis Trujillo</td>
        <td data-title="Trainer">Doug O'Neill</td>
        <td data-title="Owner">Gorman / Mestrandrea et al.</td>
        <td data-title="Win Time">1:09.85† </td>
      </tr>
    </tbody>
  </table>

</div>
{literal}

<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>

{/literal}