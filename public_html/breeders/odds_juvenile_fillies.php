	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Breeders' Cup Juvenile Fillies Odds"
	  }
	</script>
	{/literal}
	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile Fillies - To Win">
			<caption>Horses - Breeders Cup Juvenile Fillies - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:26 </em>
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenile Fillies - To Win  - Nov 05					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Echo Zulu</td><td>EV</td><td>EV</td></tr><tr><td>Hidden Connection</td><td>333/100</td><td>+333</td></tr><tr><td>Jujus Map</td><td>333/100</td><td>+333</td></tr><tr><td>Tarabi</td><td>14/1</td><td>+1400</td></tr><tr><td>Sequist</td><td>33/1</td><td>+3300</td></tr><tr><td>Desert Dawn</td><td>80/1</td><td>+8000</td></tr>			</tbody>
		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	