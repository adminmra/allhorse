<?php

$SwitchVariable['variableName'][]= 'BC_Switch_Early';       //One month out
$SwitchVariable['startTIME'][]= '2021-10-06 23:59';     //1 hero image, primary menu, pages metas
$SwitchVariable['endTIME'][]= '2021-10-30 23:59';       //TURN *OFF*   at BC_Switch_Main

$SwitchVariable['variableName'][]= 'BC_Switch_Main';        //One Week out.
$SwitchVariable['startTIME'][]= '2021-10-30 00:00';     //full BC hero, primary menu, metas, index, BC odds copy
$SwitchVariable['endTIME'][]= '2021-11-06 23:59';       //Ends with the last BC_Switch_Race_Day

$SwitchVariable['variableName'][]= 'BC_Switch_Race_Day';    //Starts friday
$SwitchVariable['startTIME'][]= '2021-11-05 00:00';
$SwitchVariable['endTIME'][]= '2021-11-06 17:18';       //Ends Saturday with the last race

$SwitchVariable['variableName'][]= 'BC_Switch_Results';        //Starts with the last race on second day 
$SwitchVariable['startTIME'][]= '2021-11-06 17:18';     //Keeps BC menu until the end of saturday
$SwitchVariable['endTIME'][]= '2021-11-06 23:59';       //Ends Saturday Midnight

/* BC Sub Races - Works on BC Menu */
/* Friday Races */
$SwitchVariable['variableName'][]= 'BC_JUVENILE_TURF_SPRINT_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-05 17:50';

$SwitchVariable['variableName'][]= 'BC_JUVENILE_TURF_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-05 20:30';

$SwitchVariable['variableName'][]= 'BC_JUVENILE_FILLIES_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-05 18:30';

$SwitchVariable['variableName'][]= 'BC_JUVENILE_FILLIES_TURF_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-05 19:10';

$SwitchVariable['variableName'][]= 'BC_JUVENILE_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-05 19:50'; 

/* Saturday Races */
$SwitchVariable['variableName'][]= 'BC_FILLY_MARE_SPRINT_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-06 15:05';

$SwitchVariable['variableName'][]= 'BC_TURF_SPRINT_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-06 15:40';

$SwitchVariable['variableName'][]= 'BC_DIRT_MILE_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-06 16:19';

$SwitchVariable['variableName'][]= 'BC_FILLY_MARE_TURF_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-06 16:59';

$SwitchVariable['variableName'][]= 'BC_SPRINT_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-06 17:38';

$SwitchVariable['variableName'][]= 'BC_MILE_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-06 18:20';

$SwitchVariable['variableName'][]= 'BC_DISTAFF_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-06 19:00';

$SwitchVariable['variableName'][]= 'BC_TURF_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-06 19:40';

$SwitchVariable['variableName'][]= 'BC_CLASSIC_Switch'; 
$SwitchVariable['startTIME'][]='2021-10-09 11:59'; 
$SwitchVariable['endTIME'][]='2021-11-06 20:40';

?>