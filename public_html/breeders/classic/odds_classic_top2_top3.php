	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Breeders' Cup Classic - Top 3 Finishers Odds"
	  }
	</script>
	{/literal}
	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Breeders Cup Classic - Wps">
			<caption>Breeders Cup Classic - Top 3 finishers</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated February 3, 2020 09:32:51 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Breeders Cup Classic - Wps  - Nov 02					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Mckinzie - Winner

</td><td>5/2</td><td>+250</td></tr><tr><td>Mckinzie - Top 2

</td><td>11/10</td><td>+110</td></tr><tr><td>Mckinzie - Top 3

</td><td>10/13</td><td>-130</td></tr><tr><td>Owendale - Winner</td><td>21/1</td><td>+2100</td></tr><tr><td>Owendale - Top 2</td><td>19/2</td><td>+950</td></tr><tr><td>Owendale - Top 3</td><td>127/20</td><td>+635</td></tr><tr><td>Yoshida - Winner

</td><td>6/1</td><td>+600</td></tr><tr><td>Yoshida - Top 2

</td><td>11/4</td><td>+275</td></tr><tr><td>Yoshida - Top 3

</td><td>9/5</td><td>+180</td></tr><tr><td>Vino Rosso - Winner

</td><td>5/1</td><td>+500</td></tr><tr><td>Vino Rosso - Top 2

</td><td>9/4</td><td>+225</td></tr><tr><td>Vino Rosso - Top 3

</td><td>3/2</td><td>+150</td></tr><tr><td>War Of Will - Winner

</td><td>14/1</td><td>+1400</td></tr><tr><td>War Of Will - Top 2

</td><td>127/20</td><td>+635</td></tr><tr><td>War Of Will - Top 3

</td><td>21/5</td><td>+420</td></tr><tr><td>Code Of Honor - Winner

</td><td>7/2</td><td>+350</td></tr><tr><td>Code Of Honor - Top 2

</td><td>31/20</td><td>+155</td></tr><tr><td>Code Of Honor - Top 3

</td><td>21/20</td><td>+105</td></tr><tr><td>Seeking The Soul - Winner</td><td>33/1</td><td>+3300</td></tr><tr><td>Seeking The Soul - Top 2</td><td>14/1</td><td>+1400</td></tr><tr><td>Seeking The Soul - Top 3</td><td>187/20</td><td>+935</td></tr><tr><td>Higher Power - Winner

</td><td>9/1</td><td>+900</td></tr><tr><td>Higher Power - Top 2

</td><td>81/20</td><td>+405</td></tr><tr><td>Higher Power - Top 3

</td><td>27/10</td><td>+270</td></tr><tr><td>Elate - Winner

</td><td>9/1</td><td>+900</td></tr><tr><td>Elate - Top 2

</td><td>81/20</td><td>+405</td></tr><tr><td>Elate - Top 3

</td><td>27/10</td><td>+270</td></tr><tr><td>Mongolian Groom - Winner

</td><td>15/1</td><td>+1500</td></tr><tr><td>Mongolian Groom - Top 2

</td><td>34/5</td><td>+680</td></tr><tr><td>Mongolian Groom - Top 3

</td><td>91/20</td><td>+455</td></tr><tr><td>Math Wizard - Winner

</td><td>30/1</td><td>+3000</td></tr><tr><td>Math Wizard - Top 2

</td><td>68/5</td><td>+1360</td></tr><tr><td>Math Wizard - Top 3

</td><td>9/1</td><td>+900</td></tr>			</tbody>
		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	