	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Breeders' Cup Classic Odds"
	  }
	</script>
	{/literal}
	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Classic - To Win">
			<caption>Horses - Breeders Cup Classic - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:18 </em>
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Classic - To Win  - Nov 06					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Knicks Go</td><td>7/2</td><td>+350</td></tr><tr><td>Essential Quality</td><td>2/1</td><td>+200</td></tr><tr><td>Hot Rod Charlie</td><td>5/2</td><td>+250</td></tr><tr><td>Medina Spirit</td><td>7/1</td><td>+700</td></tr><tr><td>Max Player</td><td>20/1</td><td>+2000</td></tr><tr><td>Art Collector</td><td>14/1</td><td>+1400</td></tr><tr><td>Tripoli</td><td>22/1</td><td>+2200</td></tr><tr><td>Stilleto Boy</td><td>80/1</td><td>+8000</td></tr>			</tbody>
		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	