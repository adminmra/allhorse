<h2>2021 Breeders' Cup Classic Results</h2>
<div class="results-nostyle results-bc-classic">
	<div id="no-more-tables">
		<table border="0" cellspacing="0" cellpadding="0"
			class="table table-condensed table-striped table-bordered ordenableResult" title="Breeders' Cup Results"
			summary="Last year's results of the Breeders' Cup ">
			<thead>
				<tr>
					<th>Results</th>
					<th>PP</th>
					<th>Horse</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td data-title="Result">1</td>
					<td data-title="PP">5</td>
					<td data-title="Horse">Knicks Go</td>
					<td data-title="Trainer">Brad Cox</td>
					<td data-title="Jockey">Joel Rosario</td>
				</tr>
				<tr>
					<td data-title="Result">2</td>
					<td data-title="PP">8</td>
					<td data-title="Horse">Medina Spirit</td>
					<td data-title="Trainer">Bob Baffert</td>
					<td data-title="Jockey">John Velazquez</td>
				</tr>
				<tr>
					<td data-title="Result">3</td>
					<td data-title="PP">4</td>
					<td data-title="Horse">Essential Quality</td>
					<td data-title="Trainer">Brad Cox</td>
					<td data-title="Jockey">Luis Saez</td>
				</tr>
			</tbody>
		</table>
	</div>
	<script src="/assets/js/sorttable_results.js"></script>
</div>