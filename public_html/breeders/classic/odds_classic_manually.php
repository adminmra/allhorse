	{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Classic Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2020 Breeders' Cup Classic"
			summary="The latest odds for the Breeders' Cup Classic available">

			<caption class="hide-sm">2020 Breeders' Cup Classic Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
				<tr>
					<td data-title="PP">9</td>
					<td data-title="Horse">Authentic</td>
					<td data-title="Odds">6-1</td>
					<td data-title="Trainer">Bob Baffert</td>
					<td data-title="Jockey">John Velazques</td>
				</tr>
				<tr>
					<td data-title="PP">3</td>
					<td data-title="Horse">By My Standards</td>
					<td data-title="Odds">10-1</td>
					<td data-title="Trainer">W. Bret Calhoun</td>
					<td data-title="Jockey">Gabriel Saez</td>
				</tr>
				<tr>
					<td data-title="PP">7</td>
					<td data-title="Horse">Global Campaign</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">Stanley M. Hough</td>
					<td data-title="Jockey">Javier Castellano</td>
				</tr>
				<tr>
					<td data-title="PP">6</td>
					<td data-title="Horse">Higher Power</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">John W. Sadler</td>
					<td data-title="Jockey">Flavien Prat</td>
				</tr>
				<tr>
					<td data-title="PP">8</td>
					<td data-title="Horse">Improbable</td>
					<td data-title="Odds">5-2</td>
					<td data-title="Trainer">Bob Baffert</td>
					<td data-title="Jockey">Irad Ortiz, Jr.</td>
				</tr>
				<tr>
					<td data-title="PP">10</td>
					<td data-title="Horse">Maximum Security</td>
					<td data-title="Odds">7-2</td>
					<td data-title="Trainer">Bob Baffert</td>
					<td data-title="Jockey">Luis Saez</td>
				</tr>
				<tr>
					<td data-title="PP">1</td>
					<td data-title="Horse">Tacitus</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">William I. Mott</td>
					<td data-title="Jockey">Jose Ortiz</td>
				</tr>
				<tr>
					<td data-title="PP">5</td>
					<td data-title="Horse">Title Ready</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">Dallas Stewart</td>
					<td data-title="Jockey">Corey Lanerie</td>
				</tr>
				<tr>
					<td data-title="PP">2</td>
					<td data-title="Horse">Tiz the Law</td>
					<td data-title="Odds">3-1</td>
					<td data-title="Trainer">Barclay Tagg</td>
					<td data-title="Jockey">Manuel Franco</td>
				</tr>
				<tr>
					<td data-title="PP">4</td>
					<td data-title="Horse">Tom's d'Etat</td>
					<td data-title="Odds">6-1</td>
					<td data-title="Trainer">Albert M. Stall Jr.</td>
					<td data-title="Jockey">Joel Rosario</td>
				</tr>
			</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}