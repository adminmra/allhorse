{literal}

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Breeders' Cup Classic Winners"
  }
</script>
{/literal}
<div id="no-more-tables">

  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Year</th>
        <th>Winner</th>
        <th>Age</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th>Win Time</th>
      </tr>
    </thead>
    <tbody>
	<tr>
        <td data-title="Year">2021</td>
        <td data-title="Winner">Knicks Go</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Brad Cox</td>
        <td data-title="Owner">Korea Racing Authority</td>
        <td data-title="Win Time">1:59.57</td>
      </tr>
      <tr>
        <td data-title="Year">2020</td>
        <td data-title="Winner">Authentic</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Spendthrift Farm LLC, MyRaceHorse.com, Madaket Stables LLC, Starlight Racing Stable</td>
        <td data-title="Win Time">1:59.19</td>
      </tr>
      <tr>
        <td data-title="Year">2019</td>
        <td data-title="Winner">Vino Rosso</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Todd Pletcher</td>
        <td data-title="Trainer">Irad Ortiz Jr.</td>
        <td data-title="Owner">Repole Stable, St. Elias Stable LLC</td>
        <td data-title="Win Time">2:02.80</td>
      </tr>
      <tr>
        <td data-title="Year">2018</td>
        <td data-title="Winner">Accelerate</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">J. Rosario</td>
        <td data-title="Trainer">J. Sadler</td>
        <td data-title="Owner">Hronis Racing LLC</td>
        <td data-title="Win Time">2:02.93</td>
      </tr>
      <tr>
        <td data-title="Year">2016</td>
        <td data-title="Winner">Arrogate</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Smith, Mike</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Winchell Thoroughbreds & Three Chimneys Farm</td>
        <td data-title="Win Time">2:00.11</td>
      </tr>
      <tr>
        <td data-title="Year">2015</td>
        <td data-title="Winner">American Pharoah</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Victor Espinoza</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Zayat Stables</td>
        <td data-title="Win Time">2:00.07</td>
      </tr>
      <tr>
        <td data-title="Year">2014</td>
        <td data-title="Winner">Bayern</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Martin Garcia</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Kaleem Shah</td>
        <td data-title="Win Time">1:59.88</td>
      </tr>
      <tr>
        <td data-title="Year">2013</td>
        <td data-title="Winner">Mucho Macho Man</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Gary Stevens</td>
        <td data-title="Trainer">Katherine Ritvo</td>
        <td data-title="Age">Reeves Thoroughbred Racing</td>
        <td data-title="Win Time">2:00.72</td>
      </tr>
      <tr>
        <td data-title="Year">2012</td>
        <td data-title="Winner">Fort Larned</td>
        <td data-title="Owner">4</td>
        <td data-title="Jockey">Brian Hernandez</td>
        <td data-title="Trainer">Ian Wilkes</td>
        <td data-title="Owner">Janis R. Whitham</td>
        <td data-title="Win Time">2:00.11</td>
      </tr>
      <tr>
        <td data-title="Year">2011</td>
        <td data-title="Winner">Drosselmeyer</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey"> Mike E. Smith</td>
        <td data-title="Trainer">William I. Mott</td>
        <td data-title="Owner">WinStar Farm LLC</td>
        <td data-title="Win Time">2:04.27</td>
      </tr>

      <tr>

        <td data-title="Year">2010</td>
        <td data-title="Winner">Blame</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Garrett K. Gomez</td>
        <td data-title="Trainer">Albert M. Stall</td>
        <td data-title="Owner">Claiborne Farm/Adele Dilschneider</td>
        <td data-title="Win Time">2:02.28</td>

      </tr>

      <tr>

        <td data-title="Year">2009</td>
        <td data-title="Winner">Zenyatta</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">John A Shirreffs</td>
        <td data-title="Owner">Jerry & Ann Moss</td>
        <td data-title="Win Time">2:00.62</td>

      </tr>



      <tr>

        <td data-title="Year">2008</td>
        <td data-title="Winner">Ravens Pass</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">L. Dettori</td>
        <td data-title="Trainer">H M John Gosden</td>
        <td data-title="Owner">Princess Haya/Darley Racing</td>
        <td data-title="Win Time">1:59.27</td>

      </tr>



      <tr>

        <td data-title="Year">2007</td>
        <td data-title="Winner">Curlin</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">R. Albarado</td>
        <td data-title="Trainer">S. Asmussen</td>
        <td data-title="Owner">Stonestreet Stables et al.</td>
        <td data-title="Win Time">2:00.59</td>

      </tr>



      <tr>

        <td data-title="Year">2006</td>
        <td data-title="Winner">Invasor (ARG)</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Fernando Jara</td>
        <td data-title="Trainer">Kiaran McLaughlin</td>
        <td data-title="Owner">Shadwell Racing</td>
        <td data-title="Win Time">2:02.18</td>

      </tr>



      <tr>

        <td data-title="Year">2005</td>
        <td data-title="Winner">Saint Liam</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">J. Bailey</td>
        <td data-title="Trainer">R. Dutrow Jr</td>
        <td data-title="Owner">M/M William K. Warren, Jr.</td>
        <td data-title="Win Time">2:01.49</td>

      </tr>



      <tr>

        <td data-title="Year">2004</td>
        <td data-title="Winner">Ghostzapper</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">J. Castellano</td>
        <td data-title="Trainer">R. Frankel</td>
        <td data-title="Owner">Stronach Stables</td>
        <td data-title="Win Time">1:59.02</td>

      </tr>



      <tr>

        <td data-title="Year">2003</td>
        <td data-title="Winner">Pleasantly Perfect</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Alex Solis</td>
        <td data-title="Trainer">Richard Mandella</td>
        <td data-title="Owner">Diamond A Racing Corp.</td>
        <td data-title="Win Time">1:59.88</td>

      </tr>



      <tr>

        <td data-title="Year">2002</td>
        <td data-title="Winner">Volponi</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Jose Santos</td>
        <td data-title="Trainer">Philip Johnson</td>
        <td data-title="Owner">Amherst & Spruce Pond St.</td>
        <td data-title="Win Time">2:01.39</td>

      </tr>



      <tr>

        <td data-title="Year">2001</td>
        <td data-title="Winner">Tiznow</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Chris McCarron</td>
        <td data-title="Trainer">Jay Robbins</td>
        <td data-title="Owner">Cee's Stables</td>
        <td data-title="Win Time">2:00.62</td>

      </tr>



      <tr>

        <td data-title="Year">2000</td>
        <td data-title="Winner">Tiznow</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Chris McCarron</td>
        <td data-title="Trainer">Jay Robbins</td>
        <td data-title="Owner">Cooper & Straub-Rubens</td>
        <td data-title="Win Time">2:00.75</td>

      </tr>



      <tr>

        <td data-title="Year">1999</td>
        <td data-title="Winner">Cat Thief</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Overbrook Farms</td>
        <td data-title="Win Time">1:59.52</td>

      </tr>



      <tr>

        <td data-title="Year">1998</td>
        <td data-title="Winner">Awesome Again</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">Patrick Byrne</td>
        <td data-title="Owner">Stronach Stables</td>
        <td data-title="Win Time">2:02.00</td>

      </tr>



      <tr>

        <td data-title="Year">1997</td>
        <td data-title="Winner">Skip Away</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">Hubert Hine</td>
        <td data-title="Owner">Carolyn Hine</td>
        <td data-title="Win Time">1:59.16</td>

      </tr>



      <tr>

        <td data-title="Year">1996</td>
        <td data-title="Winner">Alphabet Soup</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Chris McCarron</td>
        <td data-title="Trainer">David Hofmans</td>
        <td data-title="Owner">Ridder Thoroughbred Stable</td>
        <td data-title="Win Time">2:01.00</td>

      </tr>



      <tr>

        <td data-title="Year">1995</td>
        <td data-title="Winner">Cigar</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">Bill Mott</td>
        <td data-title="Owner">Allen E. Paulson</td>
        <td data-title="Win Time">1:59.58</td>

      </tr>



      <tr>

        <td data-title="Year">1994</td>
        <td data-title="Winner">Concern</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">Richard Small</td>
        <td data-title="Owner">Robert E. Meyerhoff</td>
        <td data-title="Win Time">2:02.41</td>

      </tr>



      <tr>

        <td data-title="Year">1993</td>
        <td data-title="Winner">Arcangues</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">Andre Fabre</td>
        <td data-title="Owner">Daniel Wildenstein</td>
        <td data-title="Win Time">2:00.83</td>

      </tr>



      <tr>

        <td data-title="Year">1992</td>
        <td data-title="Winner">A.P. Indy</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Eddie Delahoussaye</td>
        <td data-title="Trainer">Neil Drysdale</td>
        <td data-title="Owner">Tsurumaki/Farish/Kilroy</td>
        <td data-title="Win Time">2:00.20</td>

      </tr>



      <tr>

        <td data-title="Year">1991</td>
        <td data-title="Winner">Black Tie Affair</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">Ernie Poulos</td>
        <td data-title="Owner">Jeffrey Sullivan</td>
        <td data-title="Win Time">2:02.95</td>

      </tr>



      <tr>

        <td data-title="Year">1990</td>
        <td data-title="Winner">Unbridled</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">Carl Nafzger</td>
        <td data-title="Owner">Frances A. Genter</td>
        <td data-title="Win Time">2:02.20</td>

      </tr>



      <tr>

        <td data-title="Year">1989</td>
        <td data-title="Winner">Sunday Silence</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Chris McCarron</td>
        <td data-title="Trainer">C. Whittingham</td>
        <td data-title="Owner">H-G-W Partners</td>
        <td data-title="Win Time">2:00.20</td>

      </tr>



      <tr>

        <td data-title="Year">1988</td>
        <td data-title="Winner">Alysheba</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Chris McCarron</td>
        <td data-title="Trainer">Jack Van Berg</td>
        <td data-title="Owner">Dorothy & Pam Scharbauer</td>
        <td data-title="Win Time">2:04.80</td>

      </tr>



      <tr>

        <td data-title="Year">1987</td>
        <td data-title="Winner">Ferdinand</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Bill Shoemaker</td>
        <td data-title="Trainer">C. Whittingham</td>
        <td data-title="Owner">Elizabeth A. Keck</td>
        <td data-title="Win Time">2:01.40</td>

      </tr>



      <tr>

        <td data-title="Year">1986</td>
        <td data-title="Winner">Skywalker</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Laffit Pincay Jr.</td>
        <td data-title="Trainer">M. Whittingham</td>
        <td data-title="Owner">Oak Cliff Stables</td>
        <td data-title="Win Time">2:00.40</td>

      </tr>



      <tr>

        <td data-title="Year">1985</td>
        <td data-title="Winner">Proud Truth</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Jorge Velasquez</td>
        <td data-title="Trainer">John Veitch</td>
        <td data-title="Owner">Darby Dan Farm</td>
        <td data-title="Win Time">2:00.80</td>

      </tr>



      <tr>

        <td data-title="Year">1984</td>
        <td data-title="Winner">Wild Again</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">Vincent Timphony</td>
        <td data-title="Owner">Black Chip Stable</td>
        <td data-title="Win Time">2:03.40</td>

      </tr>
    </tbody>
  </table>
</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>

{/literal}