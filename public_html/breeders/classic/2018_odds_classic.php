	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Classic - To Win">
			<caption>Horses - Breeders Cup Classic - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 3, 2018 17:04:08 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Classic - To Win  - Nov 03					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Accelerate</td><td>5/2</td><td>+250</td></tr><tr><td>Axelroad</td><td>30/1</td><td>+3000</td></tr><tr><td>Catholic Boy</td><td>8/1</td><td>+800</td></tr><tr><td>Collected</td><td>30/1</td><td>+3000</td></tr><tr><td>Discreet Lover</td><td>20/1</td><td>+2000</td></tr><tr><td>Gunnevera</td><td>20/1</td><td>+2000</td></tr><tr><td>Lone Sailor</td><td>30/1</td><td>+3000</td></tr><tr><td>Mckinzie</td><td>6/1</td><td>+600</td></tr><tr><td>Mendelssohn</td><td>12/1</td><td>+1200</td></tr><tr><td>Mind Your Biscuits</td><td>6/1</td><td>+600</td></tr><tr><td>Pavel</td><td>20/1</td><td>+2000</td></tr><tr><td>Roaring Lion</td><td>20/1</td><td>+2000</td></tr><tr><td>Thunder Snow</td><td>12/1</td><td>+1200</td></tr><tr><td>Toast Of New York</td><td>20/1</td><td>+2000</td></tr><tr><td>West Coast</td><td>5/1</td><td>+500</td></tr><tr><td>Yoshida</td><td>10/1</td><td>+1000</td></tr>			</tbody>
		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	