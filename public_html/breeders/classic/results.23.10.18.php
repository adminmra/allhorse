

<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"title="Breeders' Cup Results" summary="Last year's results of the Breeders' Cup " >
<tbody><tr>
							 <th width="5%">Result</th>
								 <th width="5%">Time</th> 
								 <th width="5%">Post</th> 
								 <th width="5%">Odds</th>  
								 <th width="14%">Horse</th><th width="14%">Trainer</th><th width="14%">Jockey</th> <th width="20%">Owner</th>
</tr>
<tr><td>1</td><td>2:00.11</td><td>&nbsp;</td><td>1.70</td><td>Arrogate</td><td >&nbsp;</td><td >Smith, Mike</td> <td >&nbsp;</td></tr>
<tr><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>0.90*</td><td>California Chrome</td><td J&nbsp;</td><td >Espinoza, Victor</td> <td ></td></tr>
<tr><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>32.60</td><td>Keen Ice</td><td >&nbsp;</td><td >Castellano, Javier</td> <td >&nbsp;</td></tr>
<tr><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>18.10</td><td>Hoppertunity</td><td >&nbsp;</td><td >Velazquez, John</td> <td >&nbsp;</td></tr>
<tr><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>17.50</td><td>Melatonin</td><td >&nbsp;</td><td >Talamo, Joseph</td> <td >&nbsp;</td></tr>
<tr><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>8.40</td><td>Frosted</td><td >&nbsp;</td><td >Rosario, Joel</td> <td >&nbsp;</td></tr>
<tr><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>34.70</td><td>Effinex</td><td >&nbsp;</td><td >Prat, Flavien</td> <td >&nbsp;</td></tr>
<tr><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>104.70</td><td>War Story</td><td >&nbsp;</td><td >Spieth, Scott</td> <td >&nbsp;</td></tr>
<tr><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>74.30</td><td>Win the Space</td><td >&nbsp;</td><td >Stevens, Gary</td> <td >&nbsp;</td></tr>

</table> </div>
