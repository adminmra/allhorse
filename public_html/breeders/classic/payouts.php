<h2>2021 Breeders' Cup Classic Payouts</h2>
<div>
	<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
		title="Breeders' Classic Payouts" summary="Last year's Payouts of the Breeders' Cup Classic. ">
		<tbody>
			<tr>
				<th width="5%">PP</th>
				<th width="35%">Horses</th>
				<th width="10%">Win</th>
				<th width="10%">Place</th>
				<th width="10%">Show</th>
			</tr>
			<tr>
				<td>5</td>
				<td>Knicks Go</td>
				<td>$8.40</td>
				<td>$6.20</td>
				<td>$4.00</td>
			</tr>
			<tr>
				<td>8</td>
				<td>Medina Spirit</td>
				<td></td>
				<td>$6.60</td>
				<td>$4.00</td>
			</tr>
			<tr>
				<td>4</td>
				<td>Essential Quality</td>
				<td></td>
				<td></td>
				<td>$2.80</td>
			</tr>
		</tbody>
	</table>
</div>
<p></p>
<div>
	<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
		title="Breeders' Classic Payouts" summary="Last year's Payouts of the Breeders' Cup Classic. ">
		<tbody>
			<tr>
				<th width="20%">Wager</th>
				<th width="20%">Horses</th>
				<th width="20%">Payout</th>
			</tr>
			<tr>
				<td>$1.00 Exacta</td>
				<td>5-8</td>
				<td>$32.60</td>
			</tr>
			<tr>
				<td>$0.50 Trifecta</td>
				<td>5-8-4</td>
				<td>$41.95</td>
			</tr>
			<tr>
				<td>$0.10 Superfecta</td>
				<td>5-8-4-3</td>
				<td>$17.01</td>
			</tr>
			<tr>
				<td>$1.00 Double</td>
				<td>10/5</td>
				<td>$59.50</td>
			</tr>
			<tr>
				<td>$0.50 Pick 3</td>
				<td>10/10/5</td>
				<td>$917.50</td>
			</tr>
		</tbody>
	</table>
</div>