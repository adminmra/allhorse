{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Classic Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Classic"
			summary="The latest odds for the Breeders' Cup Classic available">

			<caption class="hide-sm">2021 Breeders' Cup Classic Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
				<tr>
					<td data-title="PP">1</td>
					<td data-title="Horse">Tripoli</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">John Sadler</td>
					<td data-title="Jockey">Irad Ortiz,Jr.</td>
				</tr>
				<tr>
					<td data-title="PP">2</td>
					<td data-title="Horse">Express Train</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">John Shirreffs</td>
					<td data-title="Jockey">Victor Espinoza</td>
				</tr>
				<tr>
					<td data-title="PP">3</td>
					<td data-title="Horse">Hot Rod Charlie</td>
					<td data-title="Odds">4-1</td>
					<td data-title="Trainer">Doug O'Neill</td>
					<td data-title="Jockey">Flavien Prat</td>
				</tr>
				<tr>
					<td data-title="PP">4</td>
					<td data-title="Horse">Essential Quality</td>
					<td data-title="Odds">3-1</td>
					<td data-title="Trainer">Brad Cox</td>
					<td data-title="Jockey">Luis Saez</td>
				</tr>
				<tr>
					<td data-title="PP">5</td>
					<td data-title="Horse">Knicks Go</td>
					<td data-title="Odds">5-2</td>
					<td data-title="Trainer">Brad Cox</td>
					<td data-title="Jockey">Joel Rosario</td>
				</tr>
				<tr>
					<td data-title="PP">6</td>
					<td data-title="Horse">Art Collector</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">William Mott</td>
					<td data-title="Jockey">Mike Smith</td>
				</tr>
				<tr>
					<td data-title="PP">7</td>
					<td data-title="Horse">Stiletto Boy</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">Ed Moger,Jr.</td>
					<td data-title="Jockey">Kent Desormeaux</td>
				</tr>
				<tr>
					<td data-title="PP">8</td>
					<td data-title="Horse">Medina Spirit</td>
					<td data-title="Odds">4-1</td>
					<td data-title="Trainer">Bob Baffert</td>
					<td data-title="Jockey">John Velazquez</td>
				</tr>
				<tr>
					<td data-title="PP">9</td>
					<td data-title="Horse">Max Player</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Steven Asmussen</td>
					<td data-title="Jockey">Ricardo Santana,Jr.</td>
				</tr>
			</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}