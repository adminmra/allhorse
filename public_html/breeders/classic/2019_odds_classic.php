	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Classic - To Win">
			<caption>Horses - Breeders Cup Classic - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated June 6, 2019 17:04:08 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Classic - To Win  - Nov 03					</th>
			</tr>
        -->
	<tr>
	    <th>
	        <!--Team-->
	    </th>
	    <th>Fractional</th>
	    <th>American</th>
	</tr>
	<tr>
	    <td>Maximum Security</td>
	    <td>9/1</td>
	    <td>+900</td>
	</tr>
	<tr>
	    <td>McKinzie</td>
	    <td>10/1</td>
	    <td>+1000</td>
	</tr>
	<tr>
	    <td>Thunder Snow </td>
	    <td>10/1</td>
	    <td>+1000</td>
	</tr>
	<tr>
	    <td>Game Winner</td>
	    <td>11/1</td>
	    <td>+1100</td>
	</tr>
	<tr>
	    <td>Omaha Beach</td>
	    <td>20/1</td>
	    <td>+2000</td>
	</tr>
	<tr>
	    <td>Bravazo</td>
	    <td>16/1</td>
	    <td>+1600</td>
	</tr>
	<tr>
	    <td>Catholic Boy </td>
	    <td>16/1</td>
	    <td>+1600</td>
	</tr>
	<tr>
	    <td>Yoshida</td>
	    <td>16/1</td>
	    <td>+1600</td>
	</tr>
	<tr>
	    <td>Audible</td>
	    <td>16/1</td>
	    <td>+1600</td>
	</tr>
	<tr>
	    <td>Core Beliefs</td>
	    <td>20/1</td>
	    <td>+2000</td>
	</tr>
	<tr>
	    <td>Hofburg</td>
	    <td>20/1</td>
	    <td>+2000</td>
	</tr>
	<tr>
	    <td>Improbable</td>
	    <td>20/1</td>
	    <td>+2000</td>
	</tr>
	<tr>
	    <td>Catalina Cruiser</td>
	    <td>20/1</td>
	    <td>+2000</td>
	</tr>
	<tr>
	    <td>Wissahickon </td>
	    <td>25/1</td>
	    <td>+2500</td>
	</tr>
	<tr>
	    <td>Intrepid Heart</td>
	    <td>25/1</td>
	    <td>+2500</td>
	</tr>
	<tr>
	    <td>Hidden Scroll</td>
	    <td>33/1</td>
	    <td>+3300</td>
    </tr>
    <tr>
	    <td>Inti</td>
	    <td>33/1</td>
	    <td>+3300</td>
    </tr>
    <tr>
	    <td>Too Darn Hot</td>
	    <td>33/1</td>
	    <td>+3300</td>
    </tr>
    <tr>
	    <td>Quorto</td>
	    <td>40/1</td>
	    <td>+4000</td>
    </tr>
    <tr>
	    <td>Bandua</td>
	    <td>200/1</td>
	    <td>+20000</td>
	</tr>
	</tbody>
		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	