<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="data table table-condensed table-striped table-bordered"  title="Breeders' Cup Winners" summary="The past winners of the Breeders Cup " >
			  <tbody>
				  <tr>
                    <th >Year</th>
                    <th>Winner</th>
                    <th>Jockey</th>
                    <th>Trainer</th>
                    <th>Win Time</th>
				  </tr>

<tr>
 <td data-title="Year">2018</td>
  <td data-title="Winner">Accelerate</td>
  <td data-title="Jockey">J. Rosario</td>
  <td data-title="Trainer">J. Sadler</td>
  <td data-title="Win Time">2:02.93</td>
</tr>                
<tr>
 <td data-title="Year">2017</td>
  <td data-title="Winner">Gun Runner</td>
  <td data-title="Jockey">Florent Geroux</td>
  <td data-title="Trainer">Steven Asmussen</td>
  <td data-title="Win Time">2:01.29</td>
</tr>
<tr>
 <td data-title="Year">2016</td>
  <td data-title="Winner">Arrogate</td>
  <td data-title="Jockey">Smith, Mike</td>
  <td data-title="Trainer">Bob Baffert</td>
  <td data-title="Win Time">2:00.11</td>
</tr>
<tr>
 <td data-title="Year">2015</td>
  <td data-title="Winner">American Pharoah</td>
  <td data-title="Jockey">Victor Espinoza</td>
  <td data-title="Trainer">Bob Baffert</td>
  <td data-title="Win Time">2:00.07</td>
</tr>
<tr>
 <td data-title="Year">2014</td>
  <td data-title="Winner">Bayern</td>
  <td data-title="Jockey">Martin Garcia</td>
  <td data-title="Trainer">Bob Baffert</td>
  <td data-title="Win Time">1:59.88</td>
</tr>
<tr>
 <td data-title="Year">2013</td>
  <td data-title="Winner">Mucho Macho Man</td>
  <td data-title="Jockey">Gary Stevens</td>
  <td data-title="Trainer">Katherine Ritvo</td>
  <td data-title="Win Time">2:00.72</td>
</tr><tr>
 <td data-title="Year">2012</td>
  <td data-title="Winner">Fort Larned</td>
  <td data-title="Jockey">Brian Hernandez</td>
  <td data-title="Trainer">Ian Wilkes</td>
  <td data-title="Win Time">2:00.11</td>
</tr>
<tr >
 <td data-title="Year">2011</td>
  <td data-title="Winner">Drosselmeyer</td>
  <td data-title="Jockey"> Mike E. Smith</td>
  <td data-title="Trainer">William I. Mott</td>
  <td data-title="Win Time">2:04.27</td>
</tr>

										  <tr>                                        

                                           	<td data-title="Year">2010</td>

										    <td data-title="Winner">Blame</td>

                                            <td data-title="Jockey">Garrett K. Gomez</td>

                                            <td data-title="Trainer">Albert M. Stall</td>

                                            <td data-title="Win Time">2:02:28</td>

                                            </tr>

										  <tr>                                        

                                           	<td data-title="Year">2009</td>

										    <td data-title="Winner">Zenyatta</td>

                                            <td data-title="Jockey">Mike Smith</td>

                                            <td data-title="Trainer">John A Shirreffs</td>

                                            <td data-title="Win Time">2:00:62</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">2008</td>

										    <td data-title="Winner">Ravens Pass</td>

                                            <td data-title="Jockey">L. Dettori</td>

                                            <td data-title="Trainer">H M John Gosden</td>

                                            <td data-title="Win Time">1:59.27</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">2007</td>

										    <td data-title="Winner">Curlin</td>

                                            <td data-title="Jockey">R. Albarado</td>

                                            <td data-title="Trainer">S. Asmussen</td>

                                            <td data-title="Win Time">2:00.59</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">2006</td>

										    <td data-title="Winner">Invasor (ARG)</td>

                                            <td data-title="Jockey">Fernando Jara</td>

                                            <td data-title="Trainer">Kiaran McLaughlin</td>

                                            <td data-title="Win Time">2:02.18</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">2005</td>

										    <td data-title="Winner">Saint Liam</td>

                                            <td data-title="Jockey">J. Bailey</td>

                                            <td data-title="Trainer">R. Dutrow Jr</td>

                                            <td data-title="Win Time">2:01.49</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">2004</td>

										    <td data-title="Winner">Ghostzapper</td>

                                            <td data-title="Jockey">J. Castellano</td>

                                            <td data-title="Trainer">R. Frankel</td>

                                            <td data-title="Win Time">1:59.02</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">2003</td>

										    <td data-title="Winner">Pleasantly Perfect</td>

                                            <td data-title="Jockey">Alex Solis</td>

                                            <td data-title="Trainer">Richard Mandella</td>

                                            <td data-title="Win Time">1:594/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">2002</td>

										    <td data-title="Winner">Volponi</td>

                                            <td data-title="Jockey">Jose Santos</td>

                                            <td data-title="Trainer">Philip Johnson</td>

                                            <td data-title="Win Time">2:012/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">2001</td>

										    <td data-title="Winner">Tiznow</td>

                                            <td data-title="Jockey">Chris McCarron</td>

                                            <td data-title="Trainer">Jay Robbins</td>

                                            <td data-title="Win Time">2:003/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">2000</td>

										    <td data-title="Winner">Tiznow</td>

                                            <td data-title="Jockey">Chris McCarron</td>

                                            <td data-title="Trainer">Jay Robbins</td>

                                            <td data-title="Win Time">2:003/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1999</td>

										    <td data-title="Winner">Cat Thief</td>

                                            <td data-title="Jockey">Pat Day</td>

                                            <td data-title="Trainer">D. Wayne Lukas</td>

                                            <td data-title="Win Time">1:592/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1998</td>

										    <td data-title="Winner">Awesome Again</td>

                                            <td data-title="Jockey">Pat Day</td>

                                            <td data-title="Trainer">Patrick Byrne</td>

                                            <td data-title="Win Time">2:02:00</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1997</td>

										    <td data-title="Winner">Skip Away</td>

                                            <td data-title="Jockey">Mike Smith</td>

                                            <td data-title="Trainer">Hubert Hine</td>

                                            <td data-title="Win Time">1:591/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1996</td>

										    <td data-title="Winner">Alphabet Soup</td>

                                            <td data-title="Jockey">Chris McCarron</td>

                                            <td data-title="Trainer">David Hofmans</td>

                                            <td data-title="Win Time">2:01:00</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1995</td>

										    <td data-title="Winner">Cigar</td>

                                            <td data-title="Jockey">Jerry Bailey</td>

                                            <td data-title="Trainer">Bill Mott</td>

                                            <td data-title="Win Time">1:592/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1994</td>

										    <td data-title="Winner">Concern</td>

                                            <td data-title="Jockey">Jerry Bailey</td>

                                            <td data-title="Trainer">Richard Small</td>

                                            <td data-title="Win Time">2:022/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1993</td>

										    <td data-title="Winner">Arcangues</td>

                                            <td data-title="Jockey">Jerry Bailey</td>

                                            <td data-title="Trainer">Andre Fabre</td>

                                            <td data-title="Win Time">2:004/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1992</td>

										    <td data-title="Winner">A.P. Indy</td>

                                            <td data-title="Jockey">Eddie Delahoussaye</td>

                                            <td data-title="Trainer">Neil Drysdale</td>

                                            <td data-title="Win Time">2:001/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1991</td>

										    <td data-title="Winner">Black Tie Affair</td>

                                            <td data-title="Jockey">Jerry Bailey</td>

                                            <td data-title="Trainer">Ernie Poulos</td>

                                            <td data-title="Win Time">2:024/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1990</td>


										    <td data-title="Winner">Unbridled</td>

                                            <td data-title="Jockey">Pat Day</td>

                                            <td data-title="Trainer">Carl Nafzger</td>

                                            <td data-title="Win Time">2:021/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1989</td>

										    <td data-title="Winner">Sunday Silence</td>

                                            <td data-title="Jockey">Chris McCarron</td>

                                            <td data-title="Trainer">C. Whittingham</td>

                                            <td data-title="Win Time">2:001/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1988</td>

										    <td data-title="Winner">Alysheba</td>

                                            <td data-title="Jockey">Chris McCarron</td>

                                            <td data-title="Trainer">Jack Van Berg</td>

                                            <td data-title="Win Time">2:044/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1987</td>

										    <td data-title="Winner">Ferdinand</td>

                                            <td data-title="Jockey">Bill Shoemaker</td>

                                            <td data-title="Trainer">C. Whittingham</td>

                                            <td data-title="Win Time">2:012/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1986</td>

										    <td data-title="Winner">Skywalker</td>

                                            <td data-title="Jockey">Laffit Pincay Jr.</td>

                                            <td data-title="Trainer">M. Whittingham</td>

                                            <td data-title="Win Time">2:002/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1985</td>

										    <td data-title="Winner">Proud Truth</td>

                                            <td data-title="Jockey">Jorge Velasquez</td>

                                            <td data-title="Trainer">John Veitch</td>

                                            <td data-title="Win Time">2:004/5</td>

                                            </tr>

										  

										 <tr>                                        

                                           	<td data-title="Year">1984</td>

										    <td data-title="Winner">Wild Again</td>

                                            <td data-title="Jockey">Pat Day</td>

                                            <td data-title="Trainer">Vincent Timphony</td>

                                            <td data-title="Win Time">2:032/5</td>

                                            </tr> 
                                            </tbody>
		  </table>
</div>
{literal} 
<style type="text/css" >
.table> tbody > tr th {
    text-align: center!important
}
</style>

{/literal}