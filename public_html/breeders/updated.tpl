<p><b>We are currently updating the results.</b></p>
{*<img class="horsegif" tabindex="1" src="/img/misc/horse_blue.gif" alt="Running Horse"> *}
{literal}
<script>
    request();

    function request() {
        var req = new XMLHttpRequest();
        req.open('GET', '/dates_for_breeders.json', true);
        req.onreadystatechange = function (aEvt) {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    checkDate(req.responseText);
                    setInterval(function () {
                        checkDate(req.responseText);
                    }, 30000);
                }
            }
        };
        req.send(null);
    }

    function checkDate(response) {
        var newYorkTime = new Date().toLocaleString("en-US", {
            timeZone: "America/New_York"
        });
        var now = new Date(newYorkTime);
        var json = JSON.parse(response);
        var page = (window.location.href.split('/'))[4];
        var obj = json[0][page];
        var date = new Date(obj.year, obj.month, obj.day, obj.hour, obj.minutes, 59);
        if (now.getTime() > date.getTime()) {
            checkReload(page);
        }
    }

    function checkReload(page) {
        var req = new XMLHttpRequest();
        var ver = Math.floor(Math.random() * (1000 - 0) + 100) / 10;
        req.open('GET', '/auto_reload.json?v=' + ver, true);
        req.onreadystatechange = function (aEvt) {
            if (req.readyState == 4) {
                if (req.status == 200) {
                    var json = JSON.parse(req.responseText);
                    console.log(json[0][page]);
                    if (json[0][page])
                        location.reload(true);
                }
            }
        };
        req.send(null);
    }
</script>
{/literal}