	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile Filles Turf -to Win">
			<caption>Horses - Breeders Cup Juvenile Filles Turf -to Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated Nov 02, 2019 19:00 EST .</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			  <!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenile Filles Turf -to Win  - Nov 02					</th>
			</tr>
        -->
			  <!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
			  <tr>
			  <th>
			    PP
			  </th>
			    <th>
				Horse
			    </th>
			    <th>Fractional</th>
			    <th>American</th>
			  </tr>
			  <tr><td>    1    </td><td>     Living In The Past (IRE)    </td><td>    15/1    </td><td>    +1500    </td></tr>
			  <tr><td>    2    </td><td>     Croughavouke (IRE)    </td><td>    20/1    </td><td>    +2000    </td></tr>
			  <tr><td>    3    </td><td>     Shadn (IRE)    </td><td>    10/1    </td><td>    +1000    </td></tr>
			  <tr><td>    4    </td><td>     Abscond    </td><td>    15/1    </td><td>    +1500    </td></tr>
			  <tr><td>    5    </td><td>     Daahyeh (GB)    </td><td>    5/1    </td><td>    +500    </td></tr>
			  <tr><td>    6    </td><td>     Unforgetable (IRE)    </td><td>    20/1    </td><td>    +2000    </td></tr>
			  <tr><td>    7    </td><td>     Crystalle    </td><td>    8/1    </td><td>    +800    </td></tr>
			  <tr><td>    8    </td><td>     Tango (IRE)    </td><td>    10/1    </td><td>    +1000    </td></tr>
			  <tr><td>    9    </td><td>     Albigna (IRE)    </td><td>    9/2    </td><td>    +450    </td></tr>
			  <tr><td>    10    </td><td>     Fair Maiden    </td><td>    12/1    </td><td>    +1200    </td></tr>
			  <tr><td>    11    </td><td>     Sharing    </td><td>    12/1    </td><td>    +1200    </td></tr>
			  <tr><td>    12    </td><td>     Sweet Melania    </td><td>    5/1    </td><td>    +500    </td></tr>
			  <tr><td>    13    </td><td>     Selflessly    </td><td>    8/1    </td><td>    +800    </td></tr>
			  <tr><td>    14    </td><td>     Etoile    </td><td>    12/1    </td><td>    +1200    </td></tr>
			</tbody>
		</table>
	</div>
    {literal}
        <style>
		  @media screen and (max-width: 640px) {.fa-sort{ display: block !important;} }
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="/assets/js/sorttable_post.js"></script>    {/literal}
	