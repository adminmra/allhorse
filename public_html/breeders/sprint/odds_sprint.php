	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Breeders' Cup Sprint Odds"
	  }
	</script>
	{/literal}
	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Sprint - To Win">
			<caption>Horses - Breeders Cup Sprint - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:37 </em>
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Sprint - To Win  - Nov 06					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Jackies Warrior</td><td>4/5</td><td>-125</td></tr><tr><td>Dr Schivel</td><td>9/2</td><td>+450</td></tr><tr><td>Special Reserve</td><td>11/1</td><td>+1100</td></tr><tr><td>Following Sea</td><td>14/1</td><td>+1400</td></tr><tr><td>Aloha West</td><td>11/1</td><td>+1100</td></tr><tr><td>Firenze Fire</td><td>18/1</td><td>+1800</td></tr><tr><td>Cz Rocket</td><td>18/1</td><td>+1800</td></tr><tr><td>Matera Sky</td><td>50/1</td><td>+5000</td></tr><tr><td>Lexitonian</td><td>33/1</td><td>+3300</td></tr>			</tbody>
		</table>
	</div>
{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
{/literal}
	