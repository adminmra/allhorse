	{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Sprint Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Sprint"
			summary="The latest odds for the Breeders' Cup Sprint available">

			<caption class="hide-sm">2021 Breeders' Cup Sprint Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
  <tr>
    <td data-title="PP">1</td>
    <td data-title="Horse">Following Sea</td>
    <td data-title="Odds">6-1</td>
    <td data-title="Trainer">T. Pletcher</td>
<td data-title="Jockey">J. Velazquez</td>
  </tr>
  <tr>
    <td data-title="PP">2</td>
    <td data-title="Horse">Jackie's Warrior</td>
    <td data-title="Odds">6-5</td>
    <td data-title="Trainer">S. Asmussen</td>
<td data-title="Jockey">J. Rosario</td>
  </tr>
  <tr>
    <td data-title="PP">3</td>
    <td data-title="Horse">C Z Rocket</td>
    <td data-title="Odds">12-1</td>
    <td data-title="Trainer">P. Miller</td>
<td data-title="Jockey">F. Geroux</td>
  </tr>
  <tr>
    <td data-title="PP">4</td>
    <td data-title="Horse">Matera Sky</td>
    <td data-title="Odds">20-1</td>
    <td data-title="Trainer">H. Mori</td>
<td data-title="Jockey">Y. Kawada</td>
  </tr>
  <tr>
    <td data-title="PP">5</td>
    <td data-title="Horse">Aloha West</td>
    <td data-title="Odds">8-1</td>
    <td data-title="Trainer">W. Catalano</td>
<td data-title="Jockey">J. Ortiz</td>
  </tr>
  <tr>
    <td data-title="PP">6</td>
    <td data-title="Horse">Firenze Fire</td>
    <td data-title="Odds">10-1</td>
    <td data-title="Trainer">K. Breen</td>
<td data-title="Jockey">T. Gaffalione</td>
  </tr>
  <tr>
    <td data-title="PP">7</td>
    <td data-title="Horse">Lexitonian</td>
    <td data-title="Odds">20-1</td>
    <td data-title="Trainer">J. Sisterson</td>
<td data-title="Jockey">J. Lezcano</td>
  </tr>
  <tr>
    <td data-title="PP">8</td>
    <td data-title="Horse">Special Reserve</td>
    <td data-title="Odds">6-1</td>
    <td data-title="Trainer">M. Maker</td>
<td data-title="Jockey">I. Ortiz, Jr.</td>
  </tr>
  <tr>
    <td data-title="PP">9</td>
    <td data-title="Horse">Dr. Schivel</td>
    <td data-title="Odds">4-1</td>
    <td data-title="Trainer">M. Glatt</td>
<td data-title="Jockey">F. Prat</td>
  </tr>	</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}