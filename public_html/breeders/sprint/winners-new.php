{literal}

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Breeders' Cup Sprint Winners"
  }
</script>
{/literal}
<div id="no-more-tables">

  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed">

    <thead>
      <tr>
        <th>Year</th>
        <th>Winner</th>
        <th>Age</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th>Win Time</th>
      </tr>
    </thead>
    <tbody>
	<tr>
        <td data-title="Year">2021</td>
        <td data-title="Winner">Aloha West</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Jose Ortiz</td>
        <td data-title="Trainer">Wayne Catalano</td>
        <td data-title="Owner">Eclipse Thoroughbred Partners</td>
        <td data-title="Win Time">1:08.49</td>
      </tr>
      <tr>
        <td data-title="Year">2020</td>
        <td data-title="Winner">Whitmore</td>
        <td data-title="Age">7</td>
        <td data-title="Jockey">Ron Moquett	</td>
        <td data-title="Trainer">Irad Ortiz, Jr.</td>
        <td data-title="Owner">Head of Plains Partners, LLC , Southern Springs Stables & Robert V. La Penta</td>
        <td data-title="Win Time">1:08.61</td>
      </tr>
      <tr>
        <td data-title="Year">2019</td>
        <td data-title="Winner">Mitole</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Ricardo Santana Jr.</td>
        <td data-title="Trainer">Steve Asmussen</td>
        <td data-title="Owner">William and Corinne Heiligbrodt</td>
        <td data-title="Win Time">1:09.00</td>
      </tr>
      <tr>
        <td data-title="Year">2018</td>
        <td data-title="Winner">Roy H</td>
        <td data-title="Age">6</td>
        <td data-title="Jockey">P. Lopez</td>
        <td data-title="Trainer">Peter Miller </td>
        <td data-title="Owner">Rockingham Ranch &amp; D. A. Bernsen </td>
        <td data-title="Win Time">1:08.24 </td>
      </tr>
      <tr>
        <td data-title="Year">2017 </td>
        <td data-title="Winner">Roy H</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Kent Desormeaux</td>
        <td data-title="Trainer">Peter Miller </td>
        <td data-title="Owner">Rockingham Ranch &amp; D. A. Bernsen </td>
        <td data-title="Win Time">1:08.61 </td>
      </tr>
      <tr>
        <td data-title="Year">2017 </td>
        <td data-title="Winner">Roy H</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Kent Desormeaux</td>
        <td data-title="Trainer">Peter Miller </td>
        <td data-title="Owner">Rockingham Ranch &amp; D. A. Bernsen </td>
        <td data-title="Win Time">1:08.61 </td>
      </tr>
      <tr>
        <td data-title="Year">2016 </td>
        <td data-title="Winner">Drefong</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Martin Garcia</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Baoma Corporation </td>
        <td data-title="Win Time">1:08.79 </td>
      </tr>
      <tr>
        <td data-title="Year">2015 </td>
        <td data-title="Winner">Runhappy</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Edgar Prado</td>
        <td data-title="Trainer">Maria Borell </td>
        <td data-title="Owner">James McIngvale</td>
        <td data-title="Win Time">1:08.58 </td>
      </tr>
      <tr>
        <td data-title="Year">2014 </td>
        <td data-title="Winner">Work All Week</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Florent Geroux</td>
        <td data-title="Trainer">Roger Brueggemann </td>
        <td data-title="Owner">Midwest Thoroughbreds Inc </td>
        <td data-title="Win Time">1:08.28 </td>
      </tr>
      <tr>
        <td data-title="Year">2013 </td>
        <td data-title="Winner">Secret Circle</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Martin Garcia</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Pegram/Watson &amp; Weitman </td>
        <td data-title="Win Time">1:08.73 </td>
      </tr>
      <tr>
        <td data-title="Year">2012 </td>
        <td data-title="Winner">Trinniberg</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Willie Martinez</td>
        <td data-title="Trainer">Shivananda Parbhoo</td>
        <td data-title="Owner">Sherry Parbhoo </td>
        <td data-title="Win Time">1:07.98 </td>
      </tr>
      <tr>
        <td data-title="Year">2011 </td>
        <td data-title="Winner">Amazombie</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">Bill Spawr</td>
        <td data-title="Owner">Spawr &amp; Sanford </td>
        <td data-title="Win Time">1:09.17 </td>
      </tr>
      <tr>
        <td data-title="Year">2010 </td>
        <td data-title="Winner">Big Drama</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Eibar Coa</td>
        <td data-title="Trainer">David Fawkes</td>
        <td data-title="Owner">Harold Queen</td>
        <td data-title="Win Time">1:09.05 </td>
      </tr>
      <tr>
        <td data-title="Year">2009 </td>
        <td data-title="Winner">Dancing in Silks</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Carla Gaines</td>
        <td data-title="Owner">Ken Kinakin </td>
        <td data-title="Win Time">1:08.14 </td>
      </tr>
      <tr>
        <td data-title="Year">2008 </td>
        <td data-title="Winner">Midnight Lute</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Garrett Gomez</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Pegram/Watson &amp; Weitman </td>
        <td data-title="Win Time">1:07.08 </td>
      </tr>
      <tr>
        <td data-title="Year">2007 </td>
        <td data-title="Winner">Midnight Lute</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Garrett Gomez</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Pegram/Watson &amp; Weitman </td>
        <td data-title="Win Time">1:09.18 </td>
      </tr>
      <tr>
        <td data-title="Year">2006 </td>
        <td data-title="Winner">Thor's Echo</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Corey Nakatani</td>
        <td data-title="Trainer">Doug O'Neill</td>
        <td data-title="Owner">R.S. Jaime/Suarez Racing </td>
        <td data-title="Win Time">1:08.80 </td>
      </tr>
      <tr>
        <td data-title="Year">2005 </td>
        <td data-title="Winner">Silver Train</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Edgar Prado</td>
        <td data-title="Trainer">Richard Dutrow</td>
        <td data-title="Owner">Buckram Oak Farm</td>
        <td data-title="Win Time">1:08.86 </td>
      </tr>
      <tr>
        <td data-title="Year">2004 </td>
        <td data-title="Winner">Speightstown</td>
        <td data-title="Age">6 </td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Todd A. Pletcher</td>
        <td data-title="Owner">Eugene Melnyk</td>
        <td data-title="Win Time">1:08.11 </td>
      </tr>
      <tr>
        <td data-title="Year">2003 </td>
        <td data-title="Winner">Cajun Beat</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Cornelio Velasquez</td>
        <td data-title="Trainer">Steve Margolis</td>
        <td data-title="Owner">Padua S./J. &amp; J. Iracane </td>
        <td data-title="Win Time">1:07.95 </td>
      </tr>
      <tr>
        <td data-title="Year">2002 </td>
        <td data-title="Winner">Orientate</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Bob &amp; Beverly Lewis</td>
        <td data-title="Win Time">1:08.89 </td>
      </tr>
      <tr>
        <td data-title="Year">2001 </td>
        <td data-title="Winner">Squirtle Squirt</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">Robert J. Frankel</td>
        <td data-title="Owner">David J. Lanzman </td>
        <td data-title="Win Time">1:08.41 </td>
      </tr>
      <tr>
        <td data-title="Year">2000 </td>
        <td data-title="Winner">Kona Gold</td>
        <td data-title="Age">6 </td>
        <td data-title="Jockey">Alex Solis</td>
        <td data-title="Trainer">Bruce Headley</td>
        <td data-title="Owner">Headley/Molasky/High Tech </td>
        <td data-title="Win Time">1:07.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1999 </td>
        <td data-title="Winner">Artax</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Jorge F. Chavez</td>
        <td data-title="Trainer">Louis Albertrani</td>
        <td data-title="Owner">Paraneck Stable</td>
        <td data-title="Win Time">1:07.80 </td>
      </tr>
      <tr>
        <td data-title="Year">1998 </td>
        <td data-title="Winner">Reraise</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Corey Nakatani</td>
        <td data-title="Trainer">Craig Dollase</td>
        <td data-title="Owner">B Fey/M Han et al. </td>
        <td data-title="Win Time">1:09.00 </td>
      </tr>
      <tr>
        <td data-title="Year">1997 </td>
        <td data-title="Winner">Elmhurst</td>
        <td data-title="Age">7 </td>
        <td data-title="Jockey">Corey Nakatani</td>
        <td data-title="Trainer">Jenine Sahadi</td>
        <td data-title="Owner">Sahadi</td>
        <td data-title="Win Time">1:08.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1996 </td>
        <td data-title="Winner">Lit de Justice</td>
        <td data-title="Age">6 </td>
        <td data-title="Jockey">Corey Nakatani</td>
        <td data-title="Trainer">Jenine Sahadi</td>
        <td data-title="Owner">Evergreen Farm</td>
        <td data-title="Win Time">1:08.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1995 </td>
        <td data-title="Winner">Desert Stormer</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Kent Desormeaux</td>
        <td data-title="Trainer">Frank Lyons</td>
        <td data-title="Owner">Joanne H. Nor </td>
        <td data-title="Win Time">1:09.00 </td>
      </tr>
      <tr>
        <td data-title="Year">1994 </td>
        <td data-title="Winner">Cherokee Run</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">Frank A. Alexander</td>
        <td data-title="Owner">Jill E. Robinson </td>
        <td data-title="Win Time">1:09.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1993 </td>
        <td data-title="Winner">Cardmania</td>
        <td data-title="Age">7 </td>
        <td data-title="Jockey">Ed Delahoussaye</td>
        <td data-title="Trainer">Derek Meredith</td>
        <td data-title="Owner">Jean Couvercelle </td>
        <td data-title="Win Time">1:08.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1992 </td>
        <td data-title="Winner">Thirty Slews</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Eddie Delahoussaye</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">DeGroot/Dutch Masters/Pegram</td>
        <td data-title="Win Time">1:08.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1991 </td>
        <td data-title="Winner">Sheikh Albadou</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Pat Eddery</td>
        <td data-title="Trainer">Alex Scott</td>
        <td data-title="Owner">Hilal Salem</td>
        <td data-title="Win Time">1:09.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1990 </td>
        <td data-title="Winner">Safely Kept</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Craig Perret</td>
        <td data-title="Trainer">Alan E. Goldberg</td>
        <td data-title="Owner">Jayeff B Stable</td>
        <td data-title="Win Time">1:09.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1989 </td>
        <td data-title="Winner">Dancing Spree</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Angel Cordero, Jr.</td>
        <td data-title="Trainer">C. R. McGaughey III</td>
        <td data-title="Owner">Ogden Phipps</td>
        <td data-title="Win Time">1:09.00 </td>
      </tr>
      <tr>
        <td data-title="Year">1988 </td>
        <td data-title="Winner">Gulch</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Angel Cordero, Jr.</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Peter M. Brant</td>
        <td data-title="Win Time">1:10.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1987 </td>
        <td data-title="Winner">Very Subtle</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Pat Valenzuela</td>
        <td data-title="Trainer">Melvin F. Stute</td>
        <td data-title="Owner">Ben Rochelle </td>
        <td data-title="Win Time">1:08.80 </td>
      </tr>
      <tr>
        <td data-title="Year">1986 </td>
        <td data-title="Winner">Smile</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Jacinto Vasquez</td>
        <td data-title="Trainer">Scotty Schulhofer</td>
        <td data-title="Owner">Frances A. Genter</td>
        <td data-title="Win Time">1:08.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1985 </td>
        <td data-title="Winner">Precisionist</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Chris McCarron</td>
        <td data-title="Trainer">Ross Fenstermaker</td>
        <td data-title="Owner">Fred W. Hooper</td>
        <td data-title="Win Time">1:08.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1984 </td>
        <td data-title="Winner">Eillo</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Craig Perret</td>
        <td data-title="Trainer">Budd Lepman</td>
        <td data-title="Owner">Crown Stable</td>
        <td data-title="Win Time">1:10.20 </td>
      </tr>
    </tbody>
  </table>
</div>
{literal}
<style type="text/css">
  .table>tbody>tr th {
    text-align: center !important
  }
</style>
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>

{/literal}