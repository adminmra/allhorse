<h2>2021 Breeders' Cup Sprint Payouts</h2>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Sprint Payouts" summary="Last year's Payouts of the Breeders' Cup Sprint. ">
    <tbody>
      <tr>
        <th width="5%">PP</th>
        <th width="35%">Horses</th>
        <th width="10%">Win</th>
        <th width="10%">Place</th>
        <th width="10%">Show</th>
      </tr>

      <tr>
        <td>5</td>
        <td class="name">Aloha West</td>
        <td>
          <div class="box">$24.60</div>
        </td>
        <td>
          <div class="box">$8.20</div>
        </td>
        <td class="payoff">
          <div class="box">$6.00</div>
        </td>
      </tr>
      <tr>
        <td>9</td>
        <td class="name">Dr. Schivel</td>
        <td></td>
        <td>
          <div class="box">$5.00</div>
        </td>
        <td class="payoff">
          <div class="box">$4.20</div>
        </td>
      </tr>
      <tr>
        <td>1</td>
        <td class="name">Following Sea</td>
        <td></td>
        <td></td>
        <td class="payoff">
          <div class="box">$9.60</div>
        </td>
      </tr>

    </tbody>
  </table>
</div>
<p></p>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Sprint Payouts" summary="Last year's Payouts of the Breeders' Cup Sprint. ">
    <tbody>
      <tr>
        <th width="20%">Wager</th>
        <th width="20%">Horses</th>
        <th width="20%">Payout</th>
      </tr>

      <tr class="odd">
        <td>$1.00 Exacta</td>
        <td>5-9</td>
        <td class="payoff">$49.60</td>
      </tr>
      <tr class="even">
        <td>$0.50 Trifecta</td>
        <td>5-9-1</td>
        <td class="payoff">$211.25</td>
      </tr>
      <tr class="odd">
        <td>$0.10 Superfecta</td>
        <td>5-9-1-8</td>
        <td class="payoff">$143.27</td>
      </tr>
      <tr class="even">
        <td>$1.00 Double</td>
        <td>8/5</td>
        <td class="payoff">$54.50</td>
      </tr>
      <tr class="odd">
        <td>$0.50 Pick 3</td>
        <td>5/8/5</td>
        <td class="payoff">$80.50</td>
      </tr>
    </tbody>
  </table>
</div>