<h2>2021 Breeders' Cup Sprint Results
</h2>
<div id="no-more-tables">
  <table border="0" cellspacing="0" cellpadding="0"
    class="table table-condensed table-striped table-bordered ordenableResult" title="Breeders' Cup Sprint Results"
    summary="Last year's results of the Breeders' Cup Sprint.">
    <thead>
      <tr>
        <th>Results</th>
        <th>PP</th>
        <th>Horse</th>
        <th>Trainer</th>
        <th>Jockey</th>
      </tr>
    </thead>

    <tbody>
      <tr>
        <td data-title="Result">1</td>
        <td data-title="PP">5</td>
        <td data-title="Horse">Aloha West</td>
        <td data-title="Trainer">Wayne Catalano</td>
        <td data-title="Jockey">Jose Ortiz</td>
      </tr>
      <tr>
        <td data-title="Result">2</td>
        <td data-title="PP">9</td>
        <td data-title="Horse">Dr. Schivel</td>
        <td data-title="Trainer">Mark Glatt</td>
        <td data-title="Jockey">Flavien Prat</td>
      </tr>
      <tr>
        <td data-title="Result">3</td>
        <td data-title="PP">1</td>
        <td data-title="Horse">Following Sea</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Jockey">John R. Velazquez</td>
      </tr>
    </tbody>
  </table>
</div>

<script src="/assets/js/sorttable_results.js"></script>