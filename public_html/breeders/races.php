<h2>Breeders' Cup Races</h2>

<h2><?php include('/home/ah/allhorse/public_html/breeders/date_friday.php');?></h2>
	<div id="no-more-tables"><table class="data table table-condensed table-striped table-bordered" title="Breeders Cup Races" summary="A complete list of races for the {include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders' Cup." border="0" cellpadding="0" cellspacing="0" width="100%">
	<thead><tr><th>Race</th><th>Purse</th><th>Distance</th><th>Grade</th><th>Age</th><th>Time (ET)</th></tr></thead>
    <tbody>
   <tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/juvenile-turf-sprint">Juvenile Turf Sprint</a></td><td data-title="Purse">$1,000,000</td><td data-title="Distance">5.5 Furlongs</td><td data-title="Grade">TBD</td><td data-title="Age">2 Year Olds</td><td data-title="Time (E.T)">3:21 PM</td></tr>
   	<tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/juvenile-fillies-turf">Juvenile Fillies Turf</a></td><td data-title="Purse">$1,000,000</td><td data-title="Distance">1 Mile</td><td data-title="Grade">1</td><td data-title="Age">2 Year Olds</td><td data-title="Time (E.T)">4:00 PM</td></tr>
	<tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/juvenile-fillies">Juvenile Fillies</a></td><td data-title="Purse">$2,000,000</td><td data-title="Distance">1 1/16 Miles</td><td data-title="Grade">1</td><td data-title="Age">2 Year Olds</td><td data-title="Time (E.T)">4:40 PM</td></tr>
	
	<tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/juvenile-turf">Juvenile Turf</a></td><td data-title="Purse">$1,000,000</td><td data-title="Distance">1 Mile</td><td data-title="Grade">1</td><td data-title="Age">2 Year Olds</td><td data-title="Time (E.T)">5:22 PM</td></tr>

    	


	<tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/juvenile">Juvenile</a></td><td data-title="Purse">$2,000,000</td><td data-title="Distance">1 1/16 Miles</td><td data-title="Grade">1</td><td data-title="Age">2 Year Olds</td><td data-title="Time (E.T)">6:05 PM</td></tr></tbody>
</table></div>
<h2><?php include('/home/ah/allhorse/public_html/breeders/date_saturday.php');?></h2>

<div id="no-more-tables"><table class="data table table-condensed table-striped table-bordered" title="Breeders Cup Races" summary="A complete list of races for the {include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders' Cup." border="0" cellpadding="0" cellspacing="0" width="100%">
	<thead><tr><th>Race</th><th>Purse</th><th>Distance</th><th>Grade</th><th>Age</th><th>Time (ET)</th></tr></thead>
    <tbody>
 <tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/filly-mare-sprint">Filly & Mare Sprint</a></td><td data-title="Purse">$1,000,000</td><td data-title="Distance">7 Furlongs</td><td data-title="Grade">1</td><td data-title="Age">3+ Year Olds</td><td data-title="Time (E.T)">12:00 PM</td></tr>
	<tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/turf-sprint">Turf Sprint</a></td><td data-title="Purse">$1,000,000</td>
  <td data-title="Distance">6.5 Furlongs</td><td data-title="Grade">1</td><td data-title="Age">3+ Year Olds</td><td data-title="Time (E.T)">12:38 PM</td></tr>
    <tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/dirt-mile">Dirt Mile</a></td><td data-title="Purse">$1,000,000</td><td data-title="Distance">1 Mile</td><td data-title="Grade">1</td><td data-title="Age">3+ Year Olds</td><td data-title="Time (E.T)">1:16 PM</td></tr>

	<tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/filly-mare-turf">Filly & Mare Turf</a></td><td data-title="Purse">$2,000,000</td>
  <td data-title="Distance">1 1/14 Miles</td><td data-title="Grade">1</td><td data-title="Age">3+ Year Olds</td><td data-title="Time (E.T)">2:04 PM</td></tr>
	<tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/sprint">Sprint</a></td><td data-title="Purse">$1,500,000</td>
  <td data-title="Distance">6 Furlongs</td><td data-title="Grade">1</td><td data-title="Age">3+ Year Olds</td><td data-title="Time (E.T)">2:46 PM</td></tr>
	<tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/mile">Mile</a></td><td data-title="Purse">$2,000,000</td><td data-title="Distance">1 Mile</td><td data-title="Grade">1</td><td data-title="Age">3+ Year Olds</td><td data-title="Time (E.T)">3:36 PM</td></tr>
    <tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/distaff">Distaff</a></td><td data-title="Purse">$2,000,000</td>
  <td data-title="Distance">1 1/8 Miles</td><td data-title="Grade">1</td><td data-title="Age">3+ Year Olds</td><td data-title="Time (E.T)">4:16 PM</td></tr>
	
	<tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/turf">Turf</a></td>
  <td data-title="Purse">$4,000,000</td><td data-title="Distance">1 1/2 Miles</td><td data-title="Grade">1</td><td data-title="Age">3+ Year Olds</td><td data-title="Time (E.T)">4:56 PM</td></tr>
	<tr><td data-title="Race"><a href="https://www.usracing.com/breeders-cup/classic"><strong>Classic</strong></a></td>
  <td data-title="Purse"><strong>$6,000,000</strong></td><td data-title="Distance"><strong>1 1/4 miles</strong></td><td data-title="Grade"><strong>1</strong></td><td data-title="Age"><strong>3+ Year Olds</strong></td><td data-title="Time (E.T)"><strong>5:44 PM</strong></td></tr>

</tbody>
</table></div>

<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css"> 

