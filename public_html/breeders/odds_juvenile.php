	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Breeders' Cup Juvenile Odds"
	  }
	</script>
	{/literal}
	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile - To Win">
			<caption>Horses - Breeders Cup Juvenile - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:24 </em>
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenile - To Win  - Nov 05					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Corniche</td><td>2/1</td><td>+200</td></tr><tr><td>Pinehurst</td><td>11/2</td><td>+550</td></tr><tr><td>Commandperformance</td><td>3/1</td><td>+300</td></tr><tr><td>Barossa</td><td>10/1</td><td>+1000</td></tr><tr><td>Double Thunder</td><td>22/1</td><td>+2200</td></tr><tr><td>Pappacap</td><td>22/1</td><td>+2200</td></tr><tr><td>Jasper Great</td><td>16/1</td><td>+1600</td></tr><tr><td>Oviatt Class</td><td>9/1</td><td>+900</td></tr><tr><td>American Sanctuary</td><td>50/1</td><td>+5000</td></tr><tr><td>Giant Game</td><td>25/1</td><td>+2500</td></tr><tr><td>Tough To Tame</td><td>50/1</td><td>+5000</td></tr>			</tbody>
		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	