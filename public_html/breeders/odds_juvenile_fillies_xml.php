    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile - To Win">
            <caption>Horses - Breeders Cup Juvenile - To Win</caption>
            <tbody>
                <tr>
                    <th colspan="3" class="center">
                    Horses - Breeders Cup Juvenile - To Win  - Nov 03                    </th>
            </tr>
                <tr>
                    <th colspan="3" class="center">
                    Run Or Not All Wagers Have Action                    </th>
            </tr>
    <tr><th colspan="3" class="center">Breeder’s Cup Filly And Mare Sprint - To Win</th></tr><tr><th>Team</th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Marley’s Freedom</td><td>+200</td><td>2/1</td></tr><tr><td>Dream Tree</td><td>+400</td><td>4/1</td></tr><tr><td>Finley’sluckycharm</td><td>+800</td><td>8/1</td></tr><tr><td>Lewis Bay</td><td>+800</td><td>8/1</td></tr><tr><td>Selcourt</td><td>+1000</td><td>10/1</td></tr><tr><td>Ami’s Mesa</td><td>+2000</td><td>20/1</td></tr><tr><td>Wow Cat</td><td>+800</td><td>8/1</td></tr><tr><td>Ivy Bell</td><td>+1600</td><td>16/1</td></tr><tr><td>Skye Diamonds</td><td>+1600</td><td>16/1</td></tr><tr><td>Mia Mischief</td><td>+2000</td><td>20/1</td></tr><tr><td>Stormy Embrace</td><td>+2000</td><td>20/1</td></tr><tr><td>Highway Star</td><td>+2500</td><td>25/1</td></tr>                    <tr>
                        <td class="dateUpdated center" colspan="5">
                            <em id='updateemp'>Updated October 31, 2018.</em> BUSR - Official <a href="//www.usracing.com/breeders-cup/juvenile-fillies">Breeders' Cup Juvenile Fillies Odds</a>. <br> <a href="//www.usracing.com/breeders-cup/juvenile-fillies">Breeders' Cup Juvenile Fillies</a>, all odds are fixed odds prices.

                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
    