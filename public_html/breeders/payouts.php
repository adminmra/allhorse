<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" title="Breeders' Juvenile Fillies Turf Payouts" summary="Last year's Payouts of the Breeders' Cup Juvenile Fillies Turf. "  >
<tbody><tr>
    <th width="5%">PP</th><th width="35%">Horses</th><th width="10%">Win</th><th width="10%">Place</th><th width="10%">Show</th>
    </tr>
        <tr>
            <td>6</td>  
          <td class="name">Newspaperofrecord</td>
          <td><div class="box">$3.20</div></td>
          <td><div class="box">$2.60</div></td>
          <td class="payoff"><div class="box">$2.40</div></td>
        </tr>
        <tr>
            <td>14</td>
          <td class="name">East</td>
          <td></td>
          <td><div class="box">$8.20</div></td>
          <td class="payoff"><div class="box">$6.00</div></td>
        </tr>
        <tr>
            <td>7</td>
          <td class="name">Stellar Agent</td>
          <td></td>
          <td></td>
          <td class="payoff"><div class="box">$21.00</div></td>
        </tr>
    
    
  </tbody>
</table></div>
<p></p>
<!--
<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"  title="Breeders' Juvenile Fillies Turf Payouts" summary="Last year's Payouts of the Breeders' Cup Juvenile Fillies Turf. " >
<tbody><tr>
							 <th width="20%">Wager</th>
								 <th width="20%">Horses</th><th width="20%">Payout</th></tr>
 <tr>

      <tr class="odd">
      <td>Exacta</td>
      <td>6-14</td>
      <td class="payoff">$3.20</td>
    </tr>
      <tr class="even">
      <td>Trifecta</td>
      <td>6-14-7</td>
      <td class="payoff">$1,163.45</td>
    </tr>
      <tr class="odd">
      <td>Superfecta</td>
      <td>6-14-7-4</td>
      <td class="payoff">$27,971.40</td>
    </tr>
      <tr class="even">
      <td>Daily Double</td>
      <td>8-3</td>
      <td class="payoff">$209.00</td>
    </tr>
      <tr class="odd">
      <td>Pick 3</td>
      <td>13-8-3</td>
      <td class="payoff">$892.60</td>
    </tr>
 
</tbody>
</table> </div> -->