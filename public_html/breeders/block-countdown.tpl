    <section class="bs-countdown bs-countdown--belmont usr-section">
      <div class="container">
        <div class="bs-countdown_content">
          <h3 class="bs-countdown_heading"><span class="bs-countdown_heading-text">Countdown <br>to Breeders' Cup Classic</span></h3>
          <div class="bs-countdown_items">
            <div class="bs-countdown_item bs-countdown_item--green clock_days">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_days"></canvas>
              </div>
              <div class="text"><span class="bs-countdown_num val">0</span><span class="bs-countdown_label time type_days">days</span></div>
            </div>
            <div class="bs-countdown_item bs-countdown_item--green clock_hours">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_hours"></canvas>
              </div>
              <div class="text"><span class="bs-countdown_num val">0</span><span class="bs-countdown_label time type_hours">hours</span></div>
            </div>
            <div class="bs-countdown_item bs-countdown_item--green clock_minutes">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_minutes"></canvas>
              </div>
              <div class="text"><span class="bs-countdown_num val">0</span><span class="bs-countdown_label time type_minutes">minutes</span></div>
            </div>
            <div class="bs-countdown_item bs-countdown_item--green clock_seconds">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_seconds"></canvas>
              </div>
              <div class="text"><span class="bs-countdown_num val">0</span><span class="bs-countdown_label time type_seconds">seconds</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    {php}
      $startDate = strtotime("now");
      $endDate  = strtotime("01 Nov 2018 20:35:00");
    {/php}

    {literal}
    <script type="text/javascript">
      $(document).ready(function(){
        JBCountDown({
          secondsColor : "#ffdc50",
          secondsGlow  : "none",
          minutesColor : "#9cdb7d",
          minutesGlow  : "none", 
          hoursColor   : "#378cff", 
          hoursGlow    : "none",  
          daysColor    : "#ff6565", 
          daysGlow     : "none", 
          startDate   : "{/literal}{php} echo strtotime('now'); {/php}{literal}", 
          endDate : "{/literal}{php} echo strtotime('01 Nov 2018 20:35:00');; {/php}{literal}", 
          now : "{/literal}{php} echo strtotime('now'); {/php}{literal}" 
        });
      });
    </script>
    {/literal}
