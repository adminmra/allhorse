<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"  title="Breeders' Cup Juvenile Fillies Turf Results" summary="Last year's results of the Breeders' Cup Juvenile Fillies Turf. " >
<tbody>

        <tr>
          <th>Results</th>
          <th>PP</th>
          <th>Horse</th><th>Sire</th>
          <th>Trainer</th><th>Jockey</th>
        </tr>
        
                <tr>
          <td>1</td>
          <td>6</td>
          <td>Newspaperofrecord</td>
<td>
            Lope De Vega</td>
          <td>C. Brown</td>
<td>
            I. Ortiz, Jr.</td>
        </tr>
              <tr>
          <td>2</td>
          <td>14</td>
          <td>East</td>
<td>
            Frankel</td>
          <td>K. Ryan</td>
<td>
            J. Spencer</td>
        </tr>
          
          <tr>
          <td>3</td>
          <td>7</td>
          <td>Stellar Agent</td>
<td>
            More Than Ready </td>
          <td>J. Abreu</td>
<td>
            M. Franco</td>
        </tr>  
        
                <tr>
          <td>4</td>
          <td>4</td>
          <td>Just Wonderful</td>
<td>
            Dansili</td>
          <td>A. O'Brien</td>
<td>
            R. Moore</td>
        </tr> 
                <tr>
          <td>5</td>
          <td>9</td>
          <td>Varenka </td>
<td>
            Ghostzapper</td>
          <td>H. Motion</td>
<td>
            J. Ortiz</td>
        </tr>   
                            

										 										  </table> </div>
										 										  