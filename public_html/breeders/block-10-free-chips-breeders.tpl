
    
    
    <section class="bc-card">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/index/icon-casino-rebate.png" alt="Breeders' Cup Casino Chips" class="bc-card_icon">
          <h2 class="bc-card_heading">Breeders' Cup Casino Chips</h2>
          <h3 class="bc-card_subheading">$10 FREE Casino Chips for Members</h3>
          <p>That's right, as a member of <i>bet  	&#9733; usracing</i> you get a $10 Casino Chips for the Breeders' Cup! Everybody's got a horse in the race - whether it's the Breeders' Cup or the United States Presidential Election.</p><a href="/signup?ref=Free-Breeders-Cup-Casino-Chips" class="bs-card_btn">Join Now</a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index/casino-rebate.jpg" alt="Free Casino Chips" class="bc-card_img"></div>
      </div>
    </section>