	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Dirt Mile - To Win">
			<caption>Horses - Breeders Cup Dirt Mile - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 3, 2018 17:05:08 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Dirt Mile - To Win  - Nov 03					</th>
			</tr>
        -->
			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr>
	                <tr>
                  <td>City of Light</td>
                  <td>5/2</td>
                  <td>+250</td>
                </tr>
                <tr>
                  <td>Seeking the Soul</td>
                  <td>5/1</td>
                  <td>+500</td>
                </tr>
                <tr>
                  <td>Bravazo </td>
                  <td>20/1</td>
                  <td>+2000</td>
                </tr>
                <tr>
                  <td>Firenze Fire</td>
                  <td>6/1</td>
                  <td>+600</td>
                </tr>
                <tr>
                  <td>Giant Expectations</td>
                  <td>15/1</td>
                  <td>+1500</td>
                </tr>
                <tr>
                  <td>Catalina Cruiser </td>
                  <td>8/5</td>
                  <td>+160</td>
                </tr>
                <tr>
                  <td>Trigger Warning</td>
                  <td>20/1</td>
                  <td>+2000</td>
                </tr>
                <tr>
                  <td>Seven Trumpets</td>
                  <td>15/1</td>
                  <td>+1500</td>
                </tr>
                <tr>
                  <td>Isotherm</td>
                  <td>20/1</td>
                  <td>+2000</td>
                </tr></tbody>
		</table>
	</div>
{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
{/literal}
	