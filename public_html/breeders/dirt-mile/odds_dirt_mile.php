	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Breeders' Cup Dirt Mile Odds"
	  }
	</script>
	{/literal}	
	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Dirt Mile - To Win">
			<caption>Horses - Breeders Cup Dirt Mile - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:20 </em>
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Dirt Mile - To Win  - Nov 06					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Life Is Good</td><td>5/7</td><td>-140</td></tr><tr><td>Silver State</td><td>4/1</td><td>+400</td></tr><tr><td>Ginobili</td><td>6/1</td><td>+600</td></tr><tr><td>Eight Rings</td><td>16/1</td><td>+1600</td></tr><tr><td>Pingxiang</td><td>25/1</td><td>+2500</td></tr><tr><td>Snapper Sinclair</td><td>25/1</td><td>+2500</td></tr><tr><td>Restrainedvengence</td><td>40/1</td><td>+4000</td></tr><tr><td>Jasper Prince</td><td>50/1</td><td>+5000</td></tr>			</tbody>
		</table>
	</div>
{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
{/literal}
	