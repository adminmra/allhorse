{literal}

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Breeders' Cup Dirt Mile Winners"
  }
</script>
{/literal}
<div id="no-more-tables">

  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Year</th>
        <th>Winner</th>
        <th>Age</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th>Win Time</th>
      </tr>
    </thead>
    <tbody>
	 <tr>
        <td data-title="Year">2021</td>
        <td data-title="Winner">Life is Good</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Irad Ortiz, Jr.</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Owner">WinStar Farm, LLC & CHC, Inc.</td>
        <td data-title="Win Time">1:34.12</td>
      </tr>
      <tr>
        <td data-title="Year">2020</td>
        <td data-title="Winner">Knicks Go</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Brad Cox</td>
        <td data-title="Owner">Korea Racing Authority</td>
        <td data-title="Win Time">1:33.85</td>
      </tr>
      <tr>
        <td data-title="Year">2019</td>
        <td data-title="Winner">Spun to Run</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey"> Irad Ortiz Jr.</td>
        <td data-title="Trainer">Juan Carlos Guerrero </td>
        <td data-title="Owner">Robert P. Donaldson</td>
        <td data-title="Win Time">1:36.58</td>
      </tr>
      <tr>
        <td data-title="Year">2018</td>
        <td data-title="Winner">City of Light</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">J. Castellano</td>
        <td data-title="Trainer">Michael McCarthy </td>
        <td data-title="Owner">Mr. & Mrs. William K Warren Jr.</td>
        <td data-title="Win Time">1:33.83 </td>
      </tr>
      <tr>
        <td data-title="Year">2017 </td>
        <td data-title="Winner">Battle Of Midway</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Flavien Prat </td>
        <td data-title="Trainer">Jerry Hollendorfer</td>
        <td data-title="Owner">Don Alberto Stable / WinStar Farm LLC</td>
        <td data-title="Win Time">1:35.20 </td>
      </tr>
      <tr>
        <td data-title="Year">2016 </td>
        <td data-title="Winner">Tamarkuz</td>
        <td data-title="Age">6 </td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">Kiaran McLaughlin</td>
        <td data-title="Owner">Shadwell Stable</td>
        <td data-title="Win Time">1:35.72 </td>
      </tr>
      <tr>
        <td data-title="Year">2015 </td>
        <td data-title="Winner">Liam's Map</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Javier Castellano</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Owner">Teresa Viola Racing Stables</td>
        <td data-title="Win Time">1:34.54 </td>
      </tr>
      <tr>
        <td data-title="Year">2014 </td>
        <td data-title="Winner">Goldencents</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Rafael Bejarano</td>
        <td data-title="Trainer">Leandro Mora</td>
        <td data-title="Owner">W. C. Racing</td>
        <td data-title="Win Time">1:35.19 </td>
      </tr>
      <tr>
        <td data-title="Year">2013 </td>
        <td data-title="Winner">Goldencents</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Rafael Bejarano</td>
        <td data-title="Trainer">Doug O'Neill</td>
        <td data-title="Owner">W. C. Racing et al</td>
        <td data-title="Win Time">1:35.12 </td>
      </tr>
      <tr>
        <td data-title="Year">2012 </td>
        <td data-title="Winner">Tapizar</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Corey Nakatani</td>
        <td data-title="Trainer">Steven M. Asmussen</td>
        <td data-title="Owner">Winchell Thoroughbreds</td>
        <td data-title="Win Time">1:35.34 </td>
      </tr>
      <tr>
        <td data-title="Year">2011 </td>
        <td data-title="Winner">Caleb's Posse</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Rajiv Maragh</td>
        <td data-title="Trainer">Donnie K. Von Hemel</td>
        <td data-title="Owner">McNeil Stables/Cheyenne Stables</td>
        <td data-title="Win Time">1:34.59 </td>
      </tr>
      <tr>
        <td data-title="Year">2010 </td>
        <td data-title="Winner">Dakota Phone</td>
        <td data-title="Age">5 </td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Jerry Hollendorfer</td>
        <td data-title="Owner">Todaro / Halo Farms / Carver / Hollendorfer</td>
        <td data-title="Win Time">1:35.29 </td>
      </tr>
      <tr>
        <td data-title="Year">2009 </td>
        <td data-title="Winner">Furthest Land</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Julien Leparoux</td>
        <td data-title="Trainer">Michael J. Maker</td>
        <td data-title="Owner">Ken & Sarah Ramsey</td>
        <td data-title="Win Time">1:35.50 </td>
      </tr>
      <tr>
        <td data-title="Year">2008 </td>
        <td data-title="Winner">Albertus Maximus</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Garrett Gomez</td>
        <td data-title="Trainer">Vladimir Cerin</td>
        <td data-title="Owner">Brandon & Marianne Chase</td>
        <td data-title="Win Time">1:33.41 </td>
      </tr>
      <tr>
        <td data-title="Year">2007 </td>
        <td data-title="Winner">Corinthian</td>
        <td data-title="Age">4 </td>
        <td data-title="Jockey">Kent Desormeaux</td>
        <td data-title="Trainer">James A. Jerkens</td>
        <td data-title="Owner">Centennial Farms</td>
        <td data-title="Win Time">1:39.06 </td>
      </tr>
    </tbody>
  </table>
</div>
{literal}

<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>

{/literal}