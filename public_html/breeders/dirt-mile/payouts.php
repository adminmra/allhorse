<h2>2021 Breeders' Cup Dirt Mile Payouts
</h2>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Dirt Mile Payouts" summary="Last year's Payouts of the Breeders' Cup Dirt Mile. ">
    <tbody>
      <thead>
        <tr>
          <th width="5%">PP</th>
          <th width="35%">Horses</th>
          <th width="10%">Win</th>
          <th width="10%">Place</th>
          <th width="10%">Show</th>
        </tr>
      </thead>
      <tr>
        <td>5</td>
        <td class="name">Life is Good</td>
        <td>
          <div class="box">$3.40</div>
        </td>
        <td>
          <div class="box">$2.60</div>
        </td>
        <td class="payoff">
          <div class="box">$2.20</div>
        </td>
      </tr>
      <tr>
        <td>3</td>
        <td class="name">Ginobili</td>
        <td></td>
        <td>
          <div class="box">$4.00</div>
        </td>
        <td class="payoff">
          <div class="box">$3.40</div>
        </td>
      </tr>
      <tr>
        <td>6</td>
        <td class="name">Restrainedvengence</td>
        <td></td>
        <td></td>
        <td class="payoff">
          <div class="box">$7.20</div>
        </td>
      </tr>


    </tbody>
  </table>
</div>
<p></p>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Dirt Mile Payouts" summary="Last year's Payouts of the Breeders' Cup Dirt Mile. ">
    <tbody>
      <thead>
        <tr>
          <th width="20%">Wager</th>
          <th width="20%">Horses</th>
          <th width="20%">Payout</th>
        </tr>
      </thead>

      <tr class="odd">
        <td>$1.00 Exacta</td>
        <td>5-11</td>
        <td class="payoff">$109.80</td>
      </tr>
      <tr class="even">
        <td>$0.50 Trifecta</td>
        <td>5-11-2</td>
        <td class="payoff">$616.15</td>
      </tr>
      <tr class="odd">
        <td>$0.10 Superfecta</td>
        <td>5-11-2-10</td>
        <td class="payoff">$729.80</td>
      </tr>
      <tr class="even">
        <td>$1.00 Double</td>
        <td>6/5</td>
        <td class="payoff">$38.80</td>
      </tr>
      <tr class="odd">
        <td>$0.50 Pick 3</td>
        <td>2/6/5</td>
        <td class="payoff">$60.15</td>
      </tr>


  </table>
</div>