{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Dirt Mile Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Dirt Mile"
			summary="The latest odds for the Breeders' Cup Dirt Mile available">

			<caption class="hide-sm">2021 Breeders' Cup Dirt Mile Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
				<tr>
					<td data-title="PP">1</td>
					<td data-title="Horse">Silver State</td>
					<td data-title="Odds">7-2</td>
					<td data-title="Trainer">Steven Asmussen</td>
					<td data-title="Jockey">Ricardo Santana,Jr.</td>
				</tr>
				<tr>
					<td data-title="PP">2</td>
					<td data-title="Horse">Pingxiang</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Hideyuki Mori</td>
					<td data-title="Jockey">Yuga Kawada</td>
				</tr>
				<tr>
					<td data-title="PP">3</td>
					<td data-title="Horse">KGinobili</td>
					<td data-title="Odds">7-2</td>
					<td data-title="Trainer">Richard Baltas</td>
					<td data-title="Jockey">Drayden Van Dyke</td>
				</tr>
				<tr>
					<td data-title="PP">4</td>
					<td data-title="Horse">Jasper Prince</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">Hideyuki Mori</td>
					<td data-title="Jockey">Yuichi Fukunaga</td>
				</tr>
				<tr>
					<td data-title="PP">5</td>
					<td data-title="Horse">Life Is Good</td>
					<td data-title="Odds">4-5</td>
					<td data-title="Trainer">Todd Pletcher</td>
					<td data-title="Jockey">Irad Ortiz, Jr.</td>
				</tr>
				<tr>
					<td data-title="PP">6</td>
					<td data-title="Horse"> Restrainedvengence</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">Val Brinkerhoff</td>
					<td data-title="Jockey">Edwin Maldonado</td>
				</tr>
				<tr>
					<td data-title="PP">7</td>
					<td data-title="Horse">Snapper Sinclair</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Steven Asmussen</td>
					<td data-title="Jockey"> Joel Rosario</td>
				</tr>
				<tr>
					<td data-title="PP">8</td>
					<td data-title="Horse">Eight Rings</td>
					<td data-title="Odds">10-1</td>
					<td data-title="Trainer">Bob Baffert</td>
					<td data-title="Jockey">Juan Hernandez</td>
				</tr>
				<tr>
			</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}