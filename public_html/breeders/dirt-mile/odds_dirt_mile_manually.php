	{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Dirt Mile Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2020 Breeders' Cup Dirt Mile"
			summary="The latest odds for the Breeders' Cup Dirt Mile available">

			<caption class="hide-sm">2020 Breeders' Cup Dirt Mile Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
				<tr>
					<td data-title="PP">1</td>
					<td data-title="Horse">Art Collector</td>
					<td data-title="Odds">6-1</td>
					<td data-title="Trainer">Thomas Drury Jr.</td>
					<td data-title="Jockey">Brian Hernandez Jr</td>
				</tr>
				<tr>
					<td data-title="PP">10</td>
					<td data-title="Horse">Complexity</td>
					<td data-title="Odds">2-1</td>
					<td data-title="Trainer">Chad C. Brown</td>
					<td data-title="Jockey">Jose Ortiz</td>
				</tr>
				<tr>
					<td data-title="PP">5</td>
					<td data-title="Horse">Knicks Go</td>
					<td data-title="Odds">7-2</td>
					<td data-title="Trainer">Brad Cox</td>
					<td data-title="Jockey">Joel Rosario</td>
				</tr>
				<tr>
					<td data-title="PP">12</td>
					<td data-title="Horse">Owendale</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Brad Cox</td>
					<td data-title="Jockey">Florent Geroux</td>
				</tr>
				<tr>
					<td data-title="PP">7</td>
					<td data-title="Horse">Rushie</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">Michael W. McCarthy</td>
					<td data-title="Jockey">Javier Castellano</td>
				</tr>
				<tr>
					<td data-title="PP">2</td>
					<td data-title="Horse">Sharp Samurai</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">Mark Glatt</td>
					<td data-title="Jockey">Irad Ortiz Jr.</td>
				</tr>
				<tr>
					<td data-title="PP">3</td>
					<td data-title="Horse">Silver Dust</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">W. Bret Calhoun</td>
					<td data-title="Jockey">Adam Beschizza</td>
				</tr>
				<tr>
					<td data-title="PP">4</td>
					<td data-title="Horse">War of Will</td>
					<td data-title="Odds">10-1</td>
					<td data-title="Trainer">Mark E. Casse</td>
					<td data-title="Jockey">Tyler Gaffalione</td>
				</tr>
				<tr>
					<td data-title="PP">6</td>
					<td data-title="Horse">Mr. Money</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">W. Calhoun</td>
					<td data-title="Jockey">Gabriel Saez</td>
				</tr>
				<tr>
					<td data-title="PP">8</td>
					<td data-title="Horse">Pirate's Punch</td>
					<td data-title="Odds">20-1</td>
					<td data-title="Trainer">Grant Forster</td>
					<td data-title="Jockey">Jorge Vargas Jr.</td>
				</tr>
				<tr>
					<td data-title="PP">11</td>
					<td data-title="Horse">Jesus' Team</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">Jose D'Angelo</td>
					<td data-title="Jockey">Luis Saez</td>
				</tr>
				<tr>
					<td data-title="PP">9</td>
					<td data-title="Horse">Mr Freeze</td>
					<td data-title="Odds">6-1</td>
					<td data-title="Trainer">Dale Romans</td>
					<td data-title="Jockey">Manuel Franco</td>
				</tr>
				<tr>
					<td data-title="PP">13</td>
					<td data-title="Horse">Pingxiang (AE)</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">Hideyuki Mori</td>
					<td data-title="Jockey">Flavien Prat</td>
				</tr>
			</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}