<h2>2021 Breeders' Cup Dirt Mile Results
</h2>
<div id="no-more-tables">

  <table border="0" cellspacing="0" cellpadding="0"
    class="table table-condensed table-striped table-bordered ordenableResult" title="Breeders' Cup Dirt Mile Results"
    summary="Last year's results of the Breeders' Cup Dirt Mile. ">
    <thead>
      <tr>
        <th>Results</th>
        <th>PP</th>
        <th>Horse</th>
        <th>Trainer</th>
        <th>Jockey</th>
      </tr>
    </thead>
    <tbody>
				<tr>
					<td data-title="Result">1</td>
					<td data-title="PP">5</td>
					<td data-title="Horse">Life is Good</td>
					<td data-title="Trainer">Todd Pletcher</td>
					<td data-title="Jockey">Irad Ortiz, Jr.</td>
				</tr>
				<tr>
					<td data-title="Result">2</td>
					<td data-title="PP">3</td>
					<td data-title="Horse">Ginobili</td>
					<td data-title="Trainer">Richard Baltas</td>
					<td data-title="Jockey">Drayden Van Dyke</td>
				</tr>
				<tr>
					<td data-title="Result">3</td>
					<td data-title="PP">6</td>
					<td data-title="Horse">Restrainedvengence</td>
					<td data-title="Trainer">Val Brinkerhoff</td>
					<td data-title="Jockey">Edwin Maldonado</td>
				</tr>
			</tbody>

  </table>
</div>

<script src="/assets/js/sorttable_results.js"></script>