{literal}

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Breeders' Cup Juvenile Turf Sprint Winners"
  }
</script>
{/literal}
<div id="no-more-tables">
  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed centerheadtable">
    <thead>
      <tr>
        <th>Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th>Win Time</th>
      </tr>
    </thead>
    <tbody>
	<tr>
        <td data-title="Year">2021</td>
        <td data-title="Winner">Twilight Gleaming</td>
        <td data-title="Jockey">Irad Ortiz Jr.</td>
        <td data-title="Trainer">Wesley A. Ward</td>
        <td data-title="Owner">Stonestreet Stables, LLC</td>
        <td data-title="Win Time">56:24</td>
      <tr>
        <td data-title="Year">2020</td>
        <td data-title="Winner">Golden Pal</td>
        <td data-title="Jockey">Irad Ortiz Jr.</td>
        <td data-title="Trainer">Wesley A. Ward</td>
        <td data-title="Owner">Ranlo Investments, LLC</td>
        <td data-title="Win Time">1:02.82</td>
      </tr>

      <tr>
        <td data-title="Year">2019</td>
        <td data-title="Winner">Four Wheel Drive</td>
        <td data-title="Jockey"> Irad Ortiz Jr</td>
        <td data-title="Trainer">Wesley Ward</td>
        <td data-title="Owner">Breeze Easy, LLC</td>
        <td data-title="Win Time">55.66</td>
      </tr>

      <tr>
        <td data-title="Year">2018</td>
        <td data-title="Winner">Bulletin</td>
        <td data-title="Jockey">J. Castellano</td>
        <td data-title="Trainer">T. Pletcher</td>
        <td data-title="Owner">WinStar Farm</td>
        <td data-title="Win Time">1:05.54 </td>
      </tr>

    </tbody>
  </table>
</div>
{literal}
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css">
  .table>tbody>tr th {
    text-align: center !important
  }
</style>
{/literal}