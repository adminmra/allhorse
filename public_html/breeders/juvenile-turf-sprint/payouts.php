<h2>2021 Breeders' Cup Juvenile Turf Sprint Payouts
</h2>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Juvenile Turf Sprint Payouts" summary="2021 Payouts of the Breeders' Cup Juvenile Turf Sprint. ">
    <tbody>
      <tr>
        <th width="5%">PP</th>
        <th width="35%">Horses</th>
        <th width="10%">Win</th>
        <th width="10%">Place</th>
        <th width="10%">Show</th>
      </tr>
      <tr>
        <td>6</td>
        <td class="name">Twilight Gleaming</td>
        <td>
          <div class="box">$12.40</div>
        </td>
        <td>
          <div class="box">$5.80</div>
        </td>
        <td class="payoff">
          <div class="box">$4.60</div>
        </td>
      </tr>
      <tr>
        <td>3</td>
        <td class="name">Go Bears Go</td>
        <td></td>
        <td>
          <div class="box">$10.60</div>
        </td>
        <td class="payoff">
          <div class="box">$7.20</div>
        </td>
      </tr>
      <tr>
        <td>2</td>
        <td class="name"> Kaufymaker</td>
        <td></td>
        <td></td>
        <td class="payoff">
          <div class="box">$11.20</div>
        </td>
      </tr>


    </tbody>
  </table>
</div>
<p></p>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Juvenile Turf Sprint Payouts"
    summary="Last year's Payouts of the Breeders' Cup Juvenile Turf Sprint. ">
    <tbody>
      <tr>
        <th width="20%">Wager</th>
        <th width="20%">Horses</th>
        <th width="20%">Payout</th>
      </tr>
      <tr>

      <tr class="odd">
        <td>$1.00 Exacta</td>
        <td>6-3</td>
        <td class="payoff">$67.50</td>
      </tr>
      <tr class="even">
        <td>$0.50 Trifecta</td>
        <td>6-3-2</td>
        <td class="payoff">$422.35</td>
      </tr>
      <tr class="odd">
        <td>$0.10 Superfecta</td>
        <td>6-3-2-11</td>
        <td class="payoff">$740.37</td>
      </tr>
      <tr class="even">
        <td>$1.00 Daily Double</td>
        <td>12/6</td>
        <td class="payoff">$30.50</td>
      </tr>
      <tr class="odd">
        <td>$0.50 Pick 3</td>
        <td>2/12/6</td>
        <td class="payoff">$120.20</td>
      </tr>
    </tbody>
  </table>
</div>