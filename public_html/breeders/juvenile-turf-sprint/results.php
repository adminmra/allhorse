<h2>2021 Breeders' Cup Juvenile Turf Sprint Results</h2>
<div id="no-more-tables">
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Cup Juvenile Turf Sprint Results"
    summary="2021 results of the Breeders' Cup Juvenile Turf Sprint. ">
    <thead>
      <tr>
        <th>Result</th>
        <th>PP</th>
        <th>Horse</th>
        <th>Trainer</th>
        <th>Jockey</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Result">1</td>
        <td data-title="PP">6</td>
        <td data-title="Horse">Twilight Gleaming</td>
        <td data-title="Trainer">Wesley Ward</td>
        <td data-title="Jockey">Irad Ortiz Jr.</td>
      </tr>
      <tr>
        <td data-title="Result">2</td>
        <td data-title="PP">3</td>
        <td data-title="Horse">Go Bears Go</td>
        <td data-title="Trainer">D Loughnane</td>
        <td data-title="Jockey">John R. Velazquez</td>
      </tr>
      <tr>
        <td data-title="Result">3</td>
        <td data-title="PP">2</td>
        <td data-title="Horse">Kaufymaker</td>
        <td data-title="Trainer">Wesley Ward</td>
        <td data-title="Jockey">Jose Ortiz</td>
      </tr>
    </tbody>
  </table>
</div>
<script src="/assets/js/sorttable_results.js"></script>