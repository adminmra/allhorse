{literal}
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "about": "Breeders' Cup Juvenile Turf Sprint Odds"
    }
</script>
{/literal}
<div id="no-more-tables">
    <table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
        cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Juvenile Turf Sprint "
        summary="The latest odds for the Breeders' Cup Juvenile Turf Sprint  available">

        <caption class="hide-sm">2021 Breeders' Cup Juvenile Turf Sprint Odds and Post Position</caption>


        <tbody>
            <tr class='head_title hide-sm'>
                <th>PP</th>
                <th>Horse</th>
                <th>Odds</th>
                <th>Trainer</th>
                <th>Jockey</th>
            </tr>
            <tr>
                <td data-title="PP">1</td>
                <td data-title="Horse">Twilight Jet(IRE) </td>
                <td data-title="Odds">15-1</td>
                <td data-title="Trainer">Michael O'Callaghan</td>
                <td data-title="Jockey">Leigh Roche</td>
            </tr>
            <tr>
                <td data-title="PP">2</td>
                <td data-title="Horse">Kaufymaker</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">Wesley Ward</td>
                <td data-title="Jockey">Jose Ortiz</td>
            </tr>
            <tr>
                <td data-title="PP">3</td>
                <td data-title="Horse">Go Bears Go(IRE)</td>
                <td data-title="Odds">15-1</td>
                <td data-title="Trainer">David Loughnane</td>
                <td data-title="Jockey">John Velazquez</td>
            </tr>
            <tr>
                <td data-title="PP">4</td>
                <td data-title="Horse">Vertiginous(IRE)</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">Brian Meehan</td>
                <td data-title="Jockey">Javier Castellano</td>
            </tr>
            <tr>
                <td data-title="PP">5</td>
                <td data-title="Horse">Hierarchy(IRE)</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">Hugo Palmer</td>
                <td data-title="Jockey">Oisin Murphy</td>
            </tr>
            <tr>
                <td data-title="PP">6</td>
                <td data-title="Horse">Twilight Gleaming(IRE)</td>
                <td data-title="Odds">4-1</td>
                <td data-title="Trainer">Wesley Ward</td>
                <td data-title="Jockey">Irad Ortiz, Jr.</td>
            </tr>
            <tr>
                <td data-title="PP">7</td>
                <td data-title="Horse">Armor</td>
                <td data-title="Odds">6-1</td>
                <td data-title="Trainer">Richard Hannon</td>
                <td data-title="Jockey">Ryan Moore</td>
            </tr>
            <tr>
                <td data-title="PP">8</td>
                <td data-title="Horse">Averly Jane</td>
                <td data-title="Odds">15-1</td>
                <td data-title="Trainer">Wesley Ward</td>
                <td data-title="Jockey">Tyler Gaffalione</td>
            </tr>
            <tr>
                <td data-title="PP">9</td>
                <td data-title="Horse">One Timer</td>
                <td data-title="Odds">4-1</td>
                <td data-title="Trainer">Larry Rivelli</td>
                <td data-title="Jockey">E.Baird</td>
            </tr>
            <tr>
                <td data-title="PP">10</td>
                <td data-title="Horse">Time to Party</td>
                <td data-title="Odds">15-1</td>
                <td data-title="Trainer">Peter Miller</td>
                <td data-title="Jockey">Flavien Prat</td>
            </tr>
            <tr>
                <td data-title="PP">11</td>
                <td data-title="Horse">Derrynane</td>
                <td data-title="Odds">12-1</td>
                <td data-title="Trainer">Christophe Clement</td>
                <td data-title="Jockey">Joel Rosario</td>
            </tr>
            <tr>
               
        </tbody>
    </table>
</div>
{literal}
<style>
    @media screen and (max-width: 640px) {
        .fa-sort {
            display: block !important;
        }
    }
</style>
<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sorttable_post.js"></script> {/literal}