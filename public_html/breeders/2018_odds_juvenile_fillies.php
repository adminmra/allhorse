	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile Fillies - To Win">
			<caption>Horses - Breeders Cup Juvenile Fillies - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 3, 2018 17:08:09 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenile Fillies - To Win  - Nov 02					</th>
			</tr>
        -->
			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr>
	    <tr>
      <td>Jaywalk</td>
      <td>7/2</td>
      <td>+350</td>
    </tr>
    <tr>
      <td>Restless Rider</td>
      <td>9/2</td>
      <td>+450</td>
    </tr>
    <tr>
      <td>Vibrance </td>
      <td>10/1</td>
      <td>+1000</td>
    </tr>
    <tr>
      <td>Bellafina</td>
      <td>2/1</td>
      <td>+200</td>
    </tr>
    <tr>
      <td>Cassies Dreamer</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>
    <tr>
      <td>Sippican Harbor</td>
      <td>12/1</td>
      <td>+1200</td>
    </tr>
    <tr>
      <td>Serengeti Empress</td>
      <td>7/2</td>
      <td>+350</td>
    </tr>
    <tr>
      <td>Splashy Kisses</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Reflect</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>Baby Nina</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr></tbody>
		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	