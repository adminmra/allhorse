	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Breeders' Cup Juvenile Odds"
	  }
	</script>
	{/literal}
	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile - To Win">
			<caption>Horses - Breeders Cup Juvenile - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="4">
                        <em id='updateemp'>Updated Nov 02, 2019 19:00 EST .</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
				<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenile - To Win  - Nov 01					</th>
			</tr>
        -->
				<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
				<tr>
					<th>
						PP
					</th>
					<th>
					Horse
					</th>
					<th>Fractional</th>
					<th>American</th>
				</tr>
				<tr>
					<td>1</td>
					<td>Dennis' Moment</td>
					<td>8/5</td>
					<td>+160</td>
				</tr>
				<tr>
					<td>2</td>
					<td>Wrecking Crew</td>
					<td>20/1</td>
					<td>+2000</td>
				</tr>
				<tr>
					<td>3</td>
					<td>Shoplifted</td>
					<td>20/1</td>
					<td>+2000</td>
				</tr>
				<tr>
					<td>4</td>
					<td>Storm the Court</td>
					<td>20/1</td>
					<td>+2000</td>
				</tr>
				<tr>
					<td>5</td>
					<td>Scabbard</td>
					<td>8/1</td>
					<td>+800</td>
				</tr>
				<tr>
					<td>6</td>
					<td>Eight Rings</td>
					<td>2/1</td>
					<td>+200</td>
				</tr>
				<tr>
					<td>7</td>
					<td>Anneau d'Or</td>
					<td>15/1</td>
					<td>+1500</td>
				</tr>
				<tr>
					<td>8</td>
					<td>Full Flat</td>
					<td>30/1</td>
					<td>+3000</td>
				</tr>
				<tr style="text-decoration-line: line-through;"> 
					<td style="text-decoration-line: line-through;">9</td>
					<td style="text-decoration-line: line-through;">Maxfield</td>
					<td style="text-decoration-line: line-through;">3/1</td>
					<td style="text-decoration-line: line-through;">+300</td>
				</tr>
			</tbody>
			</table>
	</div>
    {literal}
        <style>
		  @media screen and (max-width: 640px) {.fa-sort{ display: block !important;} }
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="/assets/js/sorttable_post.js"></script>    {/literal}
	