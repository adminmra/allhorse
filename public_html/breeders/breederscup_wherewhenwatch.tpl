{literal}
<script type="text/javascript" src="/assets/js/atemay.js"></script>
<script type="text/javascript">
addthisevent.settings({
	license   	: "ammgus1bqzg0il7ntm75",
	mouse		: false,
	css			: false,
	outlook		: {show:true, text:"Outlook <i class='fa fa-plus-square'></i>"},
	google		: {show:true, text:"Google <i class='fa fa-plus-square'></i>"},
	yahoo		: {show:true, text:"Yahoo <i class='fa fa-plus-square'></i>"},
	ical		: {show:true, text:"iCal <i class='fa fa-plus-square'></i>"},
	hotmail		: {show:true, text:"Hotmail <i class='fa fa-plus-square'></i>"},
	facebook	: {show:true, text:"Facebook <i class='fa fa-plus-square'></i>"}
});
</script>
{/literal}


<div class="row infoBlocks" itemscope itemtype="http://schema.org/Event">

<div class="col-md-4">
<div class="section">
	<i class="fa fa-calendar"></i>
	<div class="info">
	<p>The <a itemprop="url" title="Bet on Breeders' Cup" href="/breeders-cup"> <meta itemprop="name" content="Bet on the {include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders' Cup - Odds are live!" />
  		     <meta itemprop="description" content="The {include file='/home/ah/allhorse/public_html/breeders/running.php'} Annual Breeders' Cup, where the best of the best race." /> Breeders' Cup</a> is on {include file='/home/ah/allhorse/public_html/breeders/racedate.php'}
    </p>
	</div>
    <a href="#" title="Add to Calendar" class="addthisevent" rel="nofollow">Add to calendar <i class="fa fa-plus"></i>
	<span class="_start">11-03-2018 15:00:00</span>
	<span class="_end">11-03-2018 16:00:00</span>
	<span class="_zonecode">15</span> <!-- EST US -->
	<span class="_summary">{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders Cup</span>
	<span class="_description">www.usracing.com <br/>Place your bets on the Breeders' Cup at Betusracing.ag.<br/>Races times subject to change.</span>
	<span class="_location">Churchill Downs, Louisville, Kentucky</span>
	<span class="_organizer">USRacing.com</span>
	<span class="_organizer_email">comments@usracing.com</span>
	<span class="_all_day_event">true</span>
	<span class="_date_format">03/11/2018</span>
	</a>
</div>
</div>

<div class="col-md-4">
<div class="section">
	<i class="fa fa-map-marker"></i>
	<div class="info">
	<p itemprop="location" itemscope itemtype="http://schema.org/Place">Who Will Win the Breeders' Cup? <a href="/breeders-cup/odds">Odds are Live!</a></p>
    </div>
    <a href="/signup?ref=Breeders-Cup" class="btn btn-primary btn-sm btn-red" rel="nofollow">Bet Now!<i class="fa fa-chevron-right"></i></a>
</div>
</div>
{*
<div class="col-md-4">
<div class="section">
	<i class="fa fa-map-marker"></i>
	<div class="info">
	<p itemprop="location" itemscope itemtype="http://schema.org/Place">Located at {include file='/home/ah/allhorse/public_html/breeders/location.php'}</p>
    </div>
    <a href="/keeneland" class="btn btn-primary btn-sm" rel="nofollow">Go to map<i class="fa fa-chevron-right"></i></a>
</div>
</div>
*}

<div class="col-md-4">
<div class="section">
	<i class="glyphicon glyphicon-eye-open"></i>
    <div class="info">
	<p>Watch on NBC Sports on Friday at 3pm EST & Saturday at 1pm EST</p>
    </div>
</div>
</div>
</div>
