	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile Filles Turf -to Win">
			<caption>Horses - Breeders Cup Juvenile Filles Turf -to Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 3, 2018 17:10:10 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Juvenile Filles Turf -to Win  - Nov 02					</th>
			</tr>
        -->
			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr>
    <tr>
      <td>Newspaperofrecord</td>
      <td>2/1</td>
      <td>+200</td>
    </tr>
    <tr>
      <td>East</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Stellar Agent</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>
    <tr>
      <td>Just Wonderful</td>
      <td>6/1</td>
      <td>+600</td>
    </tr>
    <tr>
      <td>Varenka </td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>The Mackem Bullet</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>Belle Laura</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>Concrete Rose</td>
      <td>6/1</td>
      <td>+600</td>
    </tr>
    <tr>
      <td>My Gal Betty</td>
      <td>20/1</td>
      <td>+2000</td>
    </tr>
    <tr>
      <td>La Pelosa</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>Pakhet</td>
      <td>12/1</td>
      <td>+1200</td>
    </tr>
    <tr>
      <td>Lily's Candle</td>
      <td>8/1</td>
      <td>+800</td>
    </tr>
    <tr>
      <td>Summering</td>
      <td>15/1</td>
      <td>+1500</td>
    </tr>
    <tr>
      <td>Lady Prancealot</td>
      <td>30/1</td>
      <td>+3000</td>
    </tr>	</tbody>
		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	