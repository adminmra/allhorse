	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2020 US Presidential Election - To Win?"
	  }
	</script>
    <script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Gender of next US President?"
	  }
	</script>
    <script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "US Presidential Election 2020 - Democrat Candidate"
	  }
	</script>
    <script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Gender of next Vice President?"
	  }
	</script> 
    <div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Us Presidential Election">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y H:00", time() - 7200); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Us Presidential Election  - Jan 10                    </th>
            </tr>
            -->
    <tr><td colspan="3" class="center"><table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>    <tr><th colspan="3" class="center">2020 US Presidential Election  - To Win</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Donald Trump</td><td>2/5</td><td>-250</td></tr><tr><td>Bernie Sanders</td><td>10/1</td><td>+1000</td></tr><tr><td>Pete Buttigieg</td><td>10/1</td><td>+1000</td></tr><tr><td>Joe Biden</td><td>7/1</td><td>+700</td></tr><tr><td>Andrew Yang</td><td>40/1</td><td>+4000</td></tr><tr><td>Elizabeth Warren</td><td>19/5</td><td>+380</td></tr><tr><td>Tulsi Gabbard</td><td>120/1</td><td>+12000</td></tr><tr><td>Amy Klobuchar</td><td>115/1</td><td>+11500</td></tr><tr><td>Cory Booker</td><td>325/1</td><td>+32500</td></tr><tr><td>Mike Pence</td><td>60/1</td><td>+6000</td></tr><tr><td>Julian Castro</td><td>1200/1</td><td>+120000</td></tr><tr><td>John Kasich</td><td>350/1</td><td>+35000</td></tr><tr><td>Michael Bloomberg</td><td>17/1</td><td>+1700</td></tr><tr><td>Hillary Clinton</td><td>33/1</td><td>+3300</td></tr><tr><td>Nikki Haley</td><td>80/1</td><td>+8000</td></tr><tr><td>Marianne Williamson</td><td>1200/1</td><td>+120000</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>  <br>  <tr><th colspan="3" class="center">Gender of next US President?</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Man</td><td>1/5</td><td>-500</td></tr><tr><td>Woman</td><td>7/2</td><td>+350</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody> <br>   <tr><th colspan="3" class="center">US Presidential Election 2020 - Democrat Candidate</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Joe Biden</td><td>14/5</td><td>+280</td></tr><tr><td>Bernie Sanders</td><td>9/2</td><td>+450</td></tr><tr><td>Andrew Yang</td><td>14/1</td><td>+1400</td></tr><tr><td>Elizabeth Warren</td><td>7/2</td><td>+350</td></tr><tr><td>Amy Klobuchar</td><td>40/1</td><td>+4000</td></tr><tr><td>Cory Booker</td><td>100/1</td><td>+10000</td></tr><tr><td>Tulsi Gabbard</td><td>45/1</td><td>+4500</td></tr><tr><td>Julian Castro</td><td>125/1</td><td>+12500</td></tr><tr><td>Pete Buttigieg</td><td>3/1</td><td>+300</td></tr><tr><td>Marianne Williamson</td><td>150/1</td><td>+15000</td></tr><tr><td>Hillary Clinton</td><td>12/1</td><td>+1200</td></tr><tr><td>Michael Bloomberg</td><td>9/1</td><td>+900</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody> <br>   <tr><th colspan="3" class="center">Gender of next Vice President?</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Man</td><td>5/13</td><td>-260</td></tr><tr><td>Woman</td><td>2/1</td><td>+200</td></tr></tbody></table></td></tr>            </tbody>
        </table>
    </div>
    
