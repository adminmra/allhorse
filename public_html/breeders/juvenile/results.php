<h2>2021 Breeders' Cup Juvenile Results
</h2>
<div id="no-more-tables">
    <table border="0" cellspacing="0" cellpadding="0"
        class="data table table-condensed table-striped table-bordered ordenableResult"
        title="Breeders' Cup Juvenile Results" summary="Last year's results of the Breeders' Cup Juvenile">
        <thead>
            <tr>
                <th>Results</th>
                <th>PP</th>
                <th>Horse</th>
                <th>Trainer</th>
                <th>Jockey</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Result">1</td>
                <td data-title="PP">12</td>
                <td data-title="Horse">Corniche</td>
                <td data-title="Trainer">Bob Baffert</td>
                <td data-title="Jockey">Mike Smith</td>
            </tr>
            <tr>
                <td data-title="Result">2</td>
                <td data-title="PP">4</td>
                <td data-title="Horse">Pappacap</td>
                <td data-title="Trainer">Mark Casse</td>
                <td data-title="Jockey">Joe Bravo</td>
            </tr>
            <tr>
                <td data-title="Result">3</td>
                <td data-title="PP">7</td>
                <td data-title="Horse">Giant Game</td>
                <td data-title="Trainer">Dale Romans</td>
                <td data-title="Jockey">Joseph Talamo</td>
            </tr>

            </tr>

        </tbody>

    </table>
</div>

<script src="/assets/js/sorttable_results.js"></script>