<h2> 2021 Breeders' Cup Juvenile Payouts
</h2>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Juvenile Payouts" summary="Last year's Payouts of the Breeders' Cup Juvenile. ">
    <tbody>
    <thead>
    <tr>
        <th width="5%">PP</th>
        <th width="35%">Horses</th>
        <th width="10%">Win</th>
        <th width="10%">Place</th>
        <th width="10%">Show</th>
      </tr></thead>
      
      <tr>
        <td>12</td>
        <td>Corniche</td>
        <td>$4.80</td>
        <td>$4.00</td>
        <td>$3.40</td>
      </tr>
      <tr>
        <td>4</td>
        <td>Pappacap</td>
        <td></td>
        <td>$9.00</td>
        <td>$6.20</td>
      </tr>
      <tr>
        <td>7</td>
        <td>Giant Game</td>
        <td></td>
        <td></td>
        <td>$7.60</td>
      </tr>
    </tbody>
  </table>
</div>
<p></p>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Juvenile Payouts" summary="Last year's Payouts of the Breeders' Cup Juvenile. ">
    <tbody>
    <thead>
    <tr>
        <th width="20%">Wager</th>
        <th width="20%">Horses</th>
        <th width="20%">Payout</th>
      </tr></thead>
     
      <tr>
        <td>$1.00 Exacta</td>
        <td>12-4</td>
        <td>$25.10</td>
      </tr>
      <tr>
        <td>$0.50 Trifecta</td>
        <td>12-4-7</td>
        <td>$269.35</td>
      </tr>
      <tr>
        <td>$0.10 Superfecta</td>
        <td>12-4-7-10</td>
        <td>$262.21</td>
      </tr>
      <tr>
        <td>$1.00 Daily Double</td>
        <td>1/12</td>
        <td>$32.20</td>
      </tr>
      <tr>
        <td>$0.50 Pick 3</td>
        <td>6/1/12</td>
        <td>$24.50</td>
      </tr>

  </table>
</div>