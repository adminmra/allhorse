<h2 class='DarkBlue'>Breeders' Cup Juvenile</h2>
<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"  title="Breeders' Cup Juvenile" >
<tbody>
<tr><td><strong>Purse:</strong></td><td >$2,000,000</td><td><strong>Grade: </strong></td><td>1</td></tr>
<tr><td><strong>Distance: </strong></td><td>1 1/16 Miles</td><td><strong>Age:  </strong></td><td>2</td></tr>

</tbody></table></div>
