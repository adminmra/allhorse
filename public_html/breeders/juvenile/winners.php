<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"  title="Breeders' Cup Sprint Winners" summary="tde past winners of tde Breeders Cup Sprint " >
<tbody>
				  <tr>
                    <th >Year</th>
                    <th>Winner</th>
                    <th>Jockey</th>
                    <th>Trainer</th>
                    <th>Win Time</th>
				  </tr>
<tr> 
 <td data-title="Year">2018</td>  <td data-title="Winner">Game Winner</td>  <td data-title="Jockey">J. Rosario</td>  <td data-title="Trainer">B. Baffert</td>  <td data-title="Win Time">1:43.67 </td></tr>                
<tr> 
 <td data-title="Year">2017</td>  <td data-title="Winner">Good Magic</td>  <td data-title="Jockey">Jose Ortiz</td>  <td data-title="Trainer">Chad Brown</td>  <td data-title="Win Time">1:43.34</td></tr>  <tr>  
 <td data-title="Year">2016</td>  <td data-title="Winner">Classic Empire</td>  <td data-title="Jockey">Julien Leparoux</td>  <td data-title="Trainer">Mark Casse</td>  <td data-title="Win Time">1:42.60</td></tr>  <tr>  
 <td data-title="Year">2015</td>  <td data-title="Winner">Nyquist</td>  <td data-title="Jockey">Mario Gutierrez</td>  <td data-title="Trainer">Doug O'Neill</td>  <td data-title="Win Time">1:43.79</td></tr>  <tr>                                       
 <td data-title="Year">2014</td>  <td data-title="Winner">Texas Red </td>  <td data-title="Jockey">Kent Desormeaux</td>  <td data-title="Trainer">Keith Desormeaux</td>  <td data-title="Win Time">1:41.91</td></tr>  <tr>                                       
 <td data-title="Year">2013</td>  <td data-title="Winner">New Year's Day</td>  <td data-title="Jockey">Martin Garcia</td>  <td data-title="Trainer">Bob Baffert</td>  <td data-title="Win Time">1:43.52</td></tr>  <tr>                                        
 <td data-title="Year">2012</td>  <td data-title="Winner">Shanghai Bobby</td>  <td data-title="Jockey">Rosie Napravnik</td>  <td data-title="Trainer">Todd Pletcher</td>  <td data-title="Win Time">1:44.58</td></tr><tr>                                        
 <td data-title="Year">2011</td>  <td data-title="Winner">Hansen</td>  <td data-title="Jockey">Ramon Dominguez</td>  <td data-title="Trainer">Michael Maker</td>  <td data-title="Win Time">1:44.44</td></tr> <tr>                                        
 <td data-title="Year">2010</td>  <td data-title="Winner">Uncle Mo</td>  <td data-title="Jockey">John R. Velazquez</td>  <td data-title="Trainer">Todd A. Pletcher</td>  <td data-title="Win Time">1:42.60</td></tr><tr>                                        
 <td data-title="Year">2009</td>  <td data-title="Winner">Vale of York</td>  <td data-title="Jockey">Ahmed Ajtebi</td>  <td data-title="Trainer">Saeed bin Suroor</td>  <td data-title="Win Time">1:43.48</td></tr><tr>                                        
 <td data-title="Year">2008</td>  <td data-title="Winner">Midshipman</td>  <td data-title="Jockey">Garrett K. Gomez</td>  <td data-title="Trainer">Bob Baffert</td>  <td data-title="Win Time">1:40.94</td></tr><tr>                                        
 <td data-title="Year">2007</td>  <td data-title="Winner">War Pass</td>  <td data-title="Jockey">C. Velasquez</td>  <td data-title="Trainer">Nicholas P. Zito</td>  <td data-title="Win Time">1:42.76</td></tr><tr>                                        
 <td data-title="Year">2006</td>  <td data-title="Winner">Street Sense</td>  <td data-title="Jockey">Calvin Borel</td>  <td data-title="Trainer">C. Nafzger</td>  <td data-title="Win Time">1:42.59</td></tr><tr>                                        
 <td data-title="Year">2005</td>  <td data-title="Winner">Stevie Wonderboy</td>  <td data-title="Jockey">Garrett K. Gomez</td>  <td data-title="Trainer">Doug F. ONeill</td>  <td data-title="Win Time">1:41.64</td></tr><tr>                                        
 <td data-title="Year">2004</td>  <td data-title="Winner">Wilko</td>  <td data-title="Jockey">F. Dettori</td>  <td data-title="Trainer">J. Noseda</td>  <td data-title="Win Time">1:42.09</td></tr><tr>                                        
 <td data-title="Year">2003</td>  <td data-title="Winner">Action tdis Day</td>  <td data-title="Jockey">David Flores</td>  <td data-title="Trainer">Richard Mandella</td>  <td data-title="Win Time">1:433/5</td></tr><tr>                                        
 <td data-title="Year">2002</td>  <td data-title="Winner">Vindication</td>  <td data-title="Jockey">Mike E. Smitd</td>  <td data-title="Trainer">Bob Baffert</td>  <td data-title="Win Time">1:493/5</td></tr><tr>                                        
 <td data-title="Year">2001</td>  <td data-title="Winner">Array</td>  <td data-title="Jockey">Michael Kinane</td>  <td data-title="Trainer">Aidan O'Brien</td>  <td data-title="Win Time">1:421/5</td></tr><tr>                                        
 <td data-title="Year">2000</td>  <td data-title="Winner">Array</td>  <td data-title="Jockey">Jerry Bailey</td>  <td data-title="Trainer">Joe Orseno</td>  <td data-title="Win Time">1:42:00</td></tr><tr>                                        
 <td data-title="Year">1999</td>  <td data-title="Winner">Anees</td>  <td data-title="Jockey">Gary Stevens</td>  <td data-title="Trainer">Alex Hassinger Jr.</td>  <td data-title="Win Time">1:421/5</td></tr><tr>                                        
 <td data-title="Year">1998</td>  <td data-title="Winner">Answer Lively</td>  <td data-title="Jockey">Jerry Bailey</td>  <td data-title="Trainer">Bobby Barnett</td>  <td data-title="Win Time">1:44:00</td></tr><tr>                                        
 <td data-title="Year">1997</td>  <td data-title="Winner">Favorite Trick</td>  <td data-title="Jockey">Pat Day</td>  <td data-title="Trainer">Patrick Byrne</td>  <td data-title="Win Time">1:412/5</td></tr><tr>                                        
 <td data-title="Year">1996</td>  <td data-title="Winner">Boston Harbor</td>  <td data-title="Jockey">Jerry Bailey</td>  <td data-title="Trainer">D. Wayne Lukas</td>  <td data-title="Win Time">1:432/5</td></tr><tr>                                        
 <td data-title="Year">1995</td>  <td data-title="Winner">Unbridleds Song</td>  <td data-title="Jockey">Mike Smitd</td>  <td data-title="Trainer">James Ryerson</td>  <td data-title="Win Time">1:413/5</td></tr><tr>                                        
 <td data-title="Year">1994</td>  <td data-title="Winner">Timber Country</td>  <td data-title="Jockey">Pat Day</td>  <td data-title="Trainer">D. Wayne Lukas</td>  <td data-title="Win Time">1:442/5</td></tr><tr>                                        
 <td data-title="Year">1993</td>  <td data-title="Winner">Brocco</td>  <td data-title="Jockey">Gary Stevens</td>  <td data-title="Trainer">Randy Winick</td>  <td data-title="Win Time">1:424/5</td></tr><tr>                                        
 <td data-title="Year">1992</td>  <td data-title="Winner">Gilded Time</td>  <td data-title="Jockey">Chris McCarron</td>  <td data-title="Trainer">Darrell Vienna</td>  <td data-title="Win Time">1:432/5</td></tr><tr>                                        
 <td data-title="Year">1991</td>  <td data-title="Winner">Arazi</td>  <td data-title="Jockey">Pat Valenzuela</td>  <td data-title="Trainer">Francois Boutin</td>  <td data-title="Win Time">1:443/5</td></tr><tr>                                        
 <td data-title="Year">1990</td>  <td data-title="Winner">Fly So Free</td>  <td data-title="Jockey">Jose Santos</td>  <td data-title="Trainer">Scotty Schulhofer</td>  <td data-title="Win Time">1:432/5</td></tr><tr>                                        
 <td data-title="Year">1989</td>  <td data-title="Winner">Rhytdm</td>  <td data-title="Jockey">Craig Perret</td>  <td data-title="Trainer">Shug McGaughey</td>  <td data-title="Win Time">1:433/5</td></tr><tr>                                        
 <td data-title="Year">1988</td>  <td data-title="Winner">Is It True</td>  <td data-title="Jockey">Laffit Pincay Jr.</td>  <td data-title="Trainer">D. Wayne Lukas</td>  <td data-title="Win Time">1:463/5</td></tr><tr>                                        
 <td data-title="Year">1987</td>  <td data-title="Winner">Success Express</td>  <td data-title="Jockey">Jose Santos</td>  <td data-title="Trainer">D. Wayne Lukas</td>  <td data-title="Win Time">1:351/5</td></tr><tr>                                        
 <td data-title="Year">1986</td>  <td data-title="Winner">Capote</td>  <td data-title="Jockey">Laffit Pincay Jr.</td>  <td data-title="Trainer">D. Wayne Lukas</td>  <td data-title="Win Time">1:434/5</td></tr><tr>                                        
 <td data-title="Year">1985</td>  <td data-title="Winner">Tasso</td>  <td data-title="Jockey">Laffit Pincay Jr.</td>  <td data-title="Trainer">Neil Drysdale</td>  <td data-title="Win Time">1:361/5</td></tr><tr>                                        
 <td data-title="Year">1984</td>  <td data-title="Winner">Chiefs Crown</td>  <td data-title="Jockey">Don MacBetd</td>  <td data-title="Trainer">Roger Laurin</td>  <td data-title="Win Time">1:361/5</td></tr></tbody>	





									  </table> </div>
{literal}
<style type="text/css" >
.table> tbody > tr th {
    text-align: center!important
}
</style>
{/literal}
