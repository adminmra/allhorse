    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Juvenile - To Win">
            <caption>Horses - Breeders Cup Juvenile - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id='updateemp'>- Updated September 29, 2017.</em>
                        <!-- br> All odds are fixed odds prices. -->
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <!--<tr>
                    <th colspan="3" class="center">Horses - Breeders Cup Juvenile - To Win  - Nov 04</th>
                </tr>
                <tr>
                    <th colspan="3" class="center">All Bets Action Run Or Not</th>
                </tr>
                <tr>
                    <th colspan="3" class="center">2016 Breeders Cup Juvenile - Odds To Win</th>
                </tr>-->
                <tr>
                    <th>Team</th>
                    <th>Fractional</th>
                    <th>American</th>
                </tr>
                <tr>
                    <td>Not This Time</td>
                    <td>19/5</td>
                    <td>+380</td>
                </tr>
                <tr>
                    <td>Klimt</td>
                    <td>29/5</td>
                    <td>+580</td>
                </tr>
                <tr>
                    <td>Practical Joke</td>
                    <td>4/1</td>
                    <td>+400</td>
                </tr>
                <tr>
                    <td>Classic Empire</td>
                    <td>9/2</td>
                    <td>+450</td>
                </tr>
                <tr>
                    <td>Syndergaard</td>
                    <td>5/1</td>
                    <td>+500</td>
                </tr>
                <tr>
                    <td>Theory</td>
                    <td>9/1</td>
                    <td>+900</td>
                </tr>
                <tr>
                    <td>Three Rules</td>
                    <td>13/1</td>
                    <td>+1300</td>
                </tr>
                <tr>
                    <td>Good Samaritan</td>
                    <td>8/1</td>
                    <td>+800</td>
                </tr>
                <tr>
                    <td>Lookin At Lee</td>
                    <td>16/1</td>
                    <td>+1600</td>
                </tr>
                <tr>
                    <td>Star Empire</td>
                    <td>28/1</td>
                    <td>+2800</td>
                </tr>
                <tr>
                    <td>Term Of Art</td>
                    <td>28/1</td>
                    <td>+2800</td>
                </tr>
                <tr>
                    <td>Gormley</td>
                    <td>33/10</td>
                    <td>+330</td>
                </tr>
            </tbody>
        </table>
    </div>
    {literal}
        <style>
            table.ordenable th{cursor: pointer}
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sort_table.js"></script>
    {/literal}
    
