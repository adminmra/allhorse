{literal}
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "about": "Breeders' Cup Juvenile Odds"
    }
</script>
{/literal}
<div id="no-more-tables">
    <table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
        cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Juvenile"
        summary="The latest odds for the Breeders' Cup Juvenile available">

        <caption class="hide-sm">2021 Breeders' Cup Juvenile Odds and Post Position</caption>


        <tbody>
            <tr class='head_title hide-sm'>
                <th>PP</th>
                <th>Horse</th>
                <th>Odds</th>
                <th>Trainer</th>
                <th>Jockey</th>
            </tr>
            <tr>
                <td data-title="PP">1</td>
                <td data-title="Horse">Jack Christopher</td>
                <td data-title="Odds">9-5</td>
                <td data-title="Trainer">Chad Brown</td>
                <td data-title="Jockey">Jose Ortiz</td>
            </tr>
            <tr>
                <td data-title="PP">2</td>
                <td data-title="Horse">Jasper Great</td>
                <td data-title="Odds">15-1</td>
                <td data-title="Trainer">Hideyuki Mori</td>
                <td data-title="Jockey">Yuichi Fukunaga</td>
            </tr>
            <tr>
                <td data-title="PP">3</td>
                <td data-title="Horse">Oviatt Class</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">J. Desormeaux</td>
                <td data-title="Jockey">Kent Desormeaux</td>
            </tr>

            <tr>
                <td data-title="PP">4</td>
                <td data-title="Horse">Pappacap</td>
                <td data-title="Odds">15-1</td>
                <td data-title="Trainer">Mark Casse</td>
                <td data-title="Jockey">Joe Bravo</td>
            </tr>
            <tr>
                <td data-title="PP">5</td>
                <td data-title="Horse">Double Thunder</td>
                <td data-title="Odds">20-1</td>
                <td data-title="Trainer">Todd Pletcher</td>
                <td data-title="Jockey">Flavien Prat</td>
            </tr>
            <tr>
                <td data-title="PP">6</td>
                <td data-title="Horse">American Sanctuary</td>
                <td data-title="Odds">30-1</td>
                <td data-title="Trainer">Christopher Davis</td>
                <td data-title="Jockey">Florent Geroux</td>
            </tr>
            <tr>
                <td data-title="PP">7</td>
                <td data-title="Horse">Giant Game</td>
                <td data-title="Odds">30-1</td>
                <td data-title="Trainer">Dale Romans</td>
                <td data-title="Jockey">Joseph Talamo</td>
            </tr>
            <tr>
                <td data-title="PP">8</td>
                <td data-title="Horse">Barossa</td>
                <td data-title="Odds">10-1</td>
                <td data-title="Trainer">Bob Baffert</td>
                <td data-title="Jockey">Juan Hernandez</td>
            </tr>
            <tr>
                <td data-title="PP">9</td>
                <td data-title="Horse">Pinehurst</td>
                <td data-title="Odds">8-1</td>
                <td data-title="Trainer">Bob Baffert</td>
                <td data-title="Jockey">John Velazquez</td>
            </tr>
            <tr>
                <td data-title="PP">10</td>
                <td data-title="Horse">Commandperformance</td>
                <td data-title="Odds">5-1</td>
                <td data-title="Trainer">Todd Pletcher</td>
                <td data-title="Jockey">Irad Ortiz Jr</td>
            </tr>
            <tr>
                <td data-title="PP">11</td>
                <td data-title="Horse">Tough to Tame</td>
                <td data-title="Odds">30-1</td>
                <td data-title="Trainer">Christopher Davis</td>
                <td data-title="Jockey">Sophie Doyle</td>
            </tr>
            <tr>
                <td data-title="PP">12</td>
                <td data-title="Horse">Corniche</td>
                <td data-title="Odds">5-2</td>
                <td data-title="Trainer">Bob Baffert</td>
                <td data-title="Jockey">Mike Smith</td>
            </tr>
        
           </tr>
        </tbody>
    </table>
</div>
{literal}
<style>
    @media screen and (max-width: 640px) {
        .fa-sort {
            display: block !important;
        }
    }
</style>
<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sorttable_post.js"></script> {/literal}