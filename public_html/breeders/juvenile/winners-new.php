{literal}

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Breeders' Cup Juvenile Winners"
  }
</script>
{/literal}

<div id="no-more-tables">
  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th>Win Time</th>
      </tr>
    </thead>
    <tbody>
	   <tr>
        <td data-title="Year"> 2021 </td>
        <td data-title="Winner">Corniche</td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Speedway Stables LLC</td>
        <td data-title="Win Time">1:42.50</td>
      </tr>
      <tr>
        <td data-title="Year"> 2020 </td>
        <td data-title="Winner">Essential Quality</td>
        <td data-title="Jockey">Luis Saez</td>
        <td data-title="Trainer">Brad Cox</td>
        <td data-title="Owner">Godolphin, LLC</td>
        <td data-title="Win Time">1:42.09</td>
      </tr>

      <tr>
        <td data-title="Year">2019 </td>
        <td data-title="Winner">Storm the Court</td>
        <td data-title="Jockey">Flavien Prat</td>
        <td data-title="Trainer">Peter Eurton</td>
        <td data-title="Owner">Exline-Border Racing LLC, David A. Bernsen LLC & Susanna Wilson</td>
        <td data-title="Win Time">1:44.93</td>
      </tr>
      <tr>
        <td data-title="Year">2018 </td>
        <td data-title="Winner">Game Winner</td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Gary and Mary West</td>
        <td data-title="Win Time">1:43.67 </td>
      </tr>
      <tr>
        <td data-title="Year">2017 </td>
        <td data-title="Winner">Good Magic</td>
        <td data-title="Jockey">Jose L. Ortiz</td>
        <td data-title="Trainer">Chad C. Brown</td>
        <td data-title="Owner">e Five Racing / Stonestreet Stables </td>
        <td data-title="Win Time">1:43.34 </td>
      </tr>
      <tr>
        <td data-title="Year">2016 </td>
        <td data-title="Winner">Classic Empire</td>
        <td data-title="Jockey">Julien Leparoux</td>
        <td data-title="Trainer">Mark Casse</td>
        <td data-title="Owner">John C. Oxley</td>
        <td data-title="Win Time">1:42.60 </td>
      </tr>
      <tr>
        <td data-title="Year">2015 </td>
        <td data-title="Winner">Nyquist</td>
        <td data-title="Jockey">Mario Gutierrez</td>
        <td data-title="Trainer">Doug O'Neill</td>
        <td data-title="Owner">Reddam Racing</td>
        <td data-title="Win Time">1:43.79 </td>
      </tr>
      <tr>
        <td data-title="Year">2014 </td>
        <td data-title="Winner">Texas Red</td>
        <td data-title="Jockey">Kent Desormeaux</td>
        <td data-title="Trainer">Keith Desormeaux</td>
        <td data-title="Owner">Erich Brehm, Wayne Detmar et al. </td>
        <td data-title="Win Time">1:41.91 </td>
      </tr>
      <tr>
        <td data-title="Year">2013 </td>
        <td data-title="Winner">New Year's Day</td>
        <td data-title="Jockey">Martin Garcia</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Gary and Mary West</td>
        <td data-title="Win Time">1:43.52 </td>
      </tr>
      <tr>
        <td data-title="Year">2012 </td>
        <td data-title="Winner">Shanghai Bobby</td>
        <td data-title="Jockey">Rosie Napravnik</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Owner">Starlight Racing/Tabor/Smith </td>
        <td data-title="Win Time">1:44.58 </td>
      </tr>
      <tr>
        <td data-title="Year">2011 </td>
        <td data-title="Winner">Hansen</td>
        <td data-title="Jockey">Ramon Dominguez</td>
        <td data-title="Trainer">Michael Maker</td>
        <td data-title="Owner">Kendall Hansen, MD/Sky Chai Racing </td>
        <td data-title="Win Time">1:44.44 </td>
      </tr>
      <tr>
        <td data-title="Year">2010 </td>
        <td data-title="Winner">Uncle Mo</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Owner">Repole Stable</td>
        <td data-title="Win Time">1:42.60 </td>
      </tr>
      <tr>
        <td data-title="Year">2009 </td>
        <td data-title="Winner">Vale of York</td>
        <td data-title="Jockey">Ahmed Ajtebi</td>
        <td data-title="Trainer">Saeed bin Suroor</td>
        <td data-title="Owner">Godolphin</td>
        <td data-title="Win Time">1:43.48 </td>
      </tr>
      <tr>
        <td data-title="Year">2008 </td>
        <td data-title="Winner">Midshipman</td>
        <td data-title="Jockey">Garrett Gomez</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Darley Stable</td>
        <td data-title="Win Time">1:40.94 </td>
      </tr>
      <tr>
        <td data-title="Year">2007 </td>
        <td data-title="Winner">War Pass</td>
        <td data-title="Jockey">Cornelio Velasquez</td>
        <td data-title="Trainer">Nick Zito</td>
        <td data-title="Owner">Robert V. LaPenta</td>
        <td data-title="Win Time">1:42.76 </td>
      </tr>
      <tr>
        <td data-title="Year">2006 </td>
        <td data-title="Winner">Street Sense</td>
        <td data-title="Jockey">Calvin Borel</td>
        <td data-title="Trainer">Carl Nafzger</td>
        <td data-title="Owner">James B. Tafel</td>
        <td data-title="Win Time">1:42.59 </td>
      </tr>
      <tr>
        <td data-title="Year">2005 </td>
        <td data-title="Winner">Stevie Wonderboy</td>
        <td data-title="Jockey">Garrett Gomez</td>
        <td data-title="Trainer">Doug O'Neill</td>
        <td data-title="Owner">Merv Griffin Ranch Company</td>
        <td data-title="Win Time">1:41.64 </td>
      </tr>
      <tr>
        <td data-title="Year">2004 </td>
        <td data-title="Winner">Wilko</td>
        <td data-title="Jockey">Frankie Dettori</td>
        <td data-title="Trainer">Jeremy Noseda</td>
        <td data-title="Owner">J. Paul Reddam</td>
        <td data-title="Win Time">1:42.09 </td>
      </tr>
      <tr>
        <td data-title="Year">2003 </td>
        <td data-title="Winner">Action This Day</td>
        <td data-title="Jockey">David R. Flores</td>
        <td data-title="Trainer">Richard Mandella</td>
        <td data-title="Owner">B. Wayne Hughes</td>
        <td data-title="Win Time">1:43.62 </td>
      </tr>
      <tr>
        <td data-title="Year">2002 </td>
        <td data-title="Winner">Vindication</td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Padua Stables</td>
        <td data-title="Win Time">1:49.61 </td>
      </tr>
      <tr>
        <td data-title="Year">2001 </td>
        <td data-title="Winner">Johannesburg</td>
        <td data-title="Jockey">Michael Kinane</td>
        <td data-title="Trainer">Aidan O'Brien</td>
        <td data-title="Owner">Susan Magnier</td>
        <td data-title="Win Time">1:42.27 </td>
      </tr>
      <tr>
        <td data-title="Year">2000 </td>
        <td data-title="Winner">Macho Uno</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">Joseph Orseno</td>
        <td data-title="Owner">Stronach Stables</td>
        <td data-title="Win Time">1:42.05 </td>
      </tr>
      <tr>
        <td data-title="Year">1999 </td>
        <td data-title="Winner">Anees</td>
        <td data-title="Jockey">Gary Stevens</td>
        <td data-title="Trainer">Alex Hassinger, Jr.</td>
        <td data-title="Owner">The Thoroughbred Corp.</td>
        <td data-title="Win Time">1:42.29 </td>
      </tr>
      <tr>
        <td data-title="Year">1998 </td>
        <td data-title="Winner">Answer Lively</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">Bobby C. Barnett</td>
        <td data-title="Owner">John A. Franks</td>
        <td data-title="Win Time">1:44.00 </td>
      </tr>
      <tr>
        <td data-title="Year">1997 </td>
        <td data-title="Winner">Favorite Trick</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">Patrick B. Byrne</td>
        <td data-title="Owner">Joseph LaCombe Stable</td>
        <td data-title="Win Time">1:41.47 </td>
      </tr>
      <tr>
        <td data-title="Year">1996 </td>
        <td data-title="Winner">Boston Harbor</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Overbrook Farm</td>
        <td data-title="Win Time">1:43.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1995 </td>
        <td data-title="Winner">Unbridled's Song</td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">James T. Ryerson</td>
        <td data-title="Owner">Paraneck Stable</td>
        <td data-title="Win Time">1:41.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1994 </td>
        <td data-title="Winner">Timber Country</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Lewis</td>
        <td data-title="Win Time">1:44.55 </td>
      </tr>
      <tr>
        <td data-title="Year">1993 </td>
        <td data-title="Winner">Brocco</td>
        <td data-title="Jockey">Gary Stevens</td>
        <td data-title="Trainer">Randy Winick</td>
        <td data-title="Owner">Albert R. Broccoli</td>
        <td data-title="Win Time">1:42.99 </td>
      </tr>
      <tr>
        <td data-title="Year">1992 </td>
        <td data-title="Winner">Gilded Time</td>
        <td data-title="Jockey">Chris McCarron</td>
        <td data-title="Trainer">Darrell Vienna</td>
        <td data-title="Owner">Milch, Silverman &amp; Silverman </td>
        <td data-title="Win Time">1:43.43 </td>
      </tr>
      <tr>
        <td data-title="Year">1991 </td>
        <td data-title="Winner">Arazi</td>
        <td data-title="Jockey">Pat Valenzuela</td>
        <td data-title="Trainer">Francois Boutin</td>
        <td data-title="Owner">Allen E. Paulson</td>
        <td data-title="Win Time">1:44.78 </td>
      </tr>
      <tr>
        <td data-title="Year">1990 </td>
        <td data-title="Winner">Fly So Free</td>
        <td data-title="Jockey">Jose Santos</td>
        <td data-title="Trainer">Scotty Schulhofer</td>
        <td data-title="Owner">Tommy Valando</td>
        <td data-title="Win Time">1:43.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1989 </td>
        <td data-title="Winner">Rhythm</td>
        <td data-title="Jockey">Craig Perret</td>
        <td data-title="Trainer">C. R. McGaughey III</td>
        <td data-title="Owner">Ogden Mills Phipps</td>
        <td data-title="Win Time">1:43.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1988 </td>
        <td data-title="Winner">Is It True</td>
        <td data-title="Jockey">Laffit Pincay, Jr.</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Eugene V. Klein</td>
        <td data-title="Win Time">1:46.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1987 </td>
        <td data-title="Winner">Success Express</td>
        <td data-title="Jockey">José Santos</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Eugene V. Klein</td>
        <td data-title="Win Time">1:35.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1986 </td>
        <td data-title="Winner">Capote</td>
        <td data-title="Jockey">Laffit Pincay, Jr.</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Beal, French Jr. &amp; E. V. Klein</td>
        <td data-title="Win Time">1:43.80 </td>
      </tr>
      <tr>
        <td data-title="Year">1985 </td>
        <td data-title="Winner">Tasso</td>
        <td data-title="Jockey">Laffit Pincay, Jr.</td>
        <td data-title="Trainer">Neil Drysdale</td>
        <td data-title="Owner">Gerald Robins </td>
        <td data-title="Win Time">1:36.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1984 </td>
        <td data-title="Winner">Chief's Crown</td>
        <td data-title="Jockey">Don MacBeth</td>
        <td data-title="Trainer">Roger Laurin</td>
        <td data-title="Owner">Star Crown Stable </td>
        <td data-title="Win Time">1:36.20 </td>
      </tr>
    </tbody>
  </table>

</div>
{literal}

<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>

{/literal}