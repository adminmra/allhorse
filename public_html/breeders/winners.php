<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"  title="Breeders' Cup Juvenile Fillies Turf Winners" summary="The past winners of the Breeders Cup Juvenile Fillies Turf. " >

										   <tr >

										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>

                                          </tr>
                                                                                    <tr>                                        
                                            <td >2018</td>
                        <td  >Newspaperofrecord</td>
                                            <td >I. Ortiz, Jr.</td>
                                            <td >Chad C. Brown</td>
                                            <td >-</td>
                                            </tr>
                                          <tr>                                        
                                            <td >2017</td>
                        <td  >Rushing Fall</td>
                                            <td >Javier Castellano</td>
                                            <td >Chad C. Brown</td>
                                            <td >1:36.09</td>
                                            </tr>

                                          <tr>                                        
                                           	<td >2016</td>
										    <td  >New Money Honey</td>
                                            <td >Javier Castellano</td>
                                            <td >Chad C. Brown</td>
                                            <td >1:34.01</td>
                                            </tr>

                                        <tr>                                        
                                           	 <td  >2015</td>
										    <td  >Catch A Glimpse</td>
                                            <td >Florent Geroux</td>
                                            <td >Mark Casse</td>
                                            <td >1:39.08</td>

                                            </tr>
		  		  <tr>                                        
                                           	 <td  >2014</td>
										    <td  >Lady Eli</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >1:33.41</td>

                                            </tr>
                                            <tr>                                        
                                           	 <td  >2013</td>
										    <td  >Chriselliam (IRE)</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>

                                            </tr>

										  <tr>                                        
                                           	 <td  >2012</td>
										    <td  >Flotilla</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>

                                            </tr>

										  
										 <tr>                                        
                                           	 <td  >2011</td>
										    <td  >Stephanie's Kitten</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>

                                            </tr>

										  <tr>                                        
                                           	 <td  >2010</td>
										    <td  >More than Real</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>

                                            </tr>

										  
										 <tr>                                        
                                           	 <td  >2009</td>
										    <td  >Tapitsfly</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>

                                            </tr>

										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  >Maram</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>
                                            <td >&nbsp;</td>

                                            </tr>

										  
										 										  </table></div>