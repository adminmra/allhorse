<h2>2021 Breeders' Cup  Distaff Results</h2>
<div id="no-more-tables">
<table border="0" cellspacing="0" cellpadding="0"
  class="table table-condensed table-striped table-bordered ordenableResult" title="Breeders' Distaff Results"
  summary="Last year's results of the Breeders' Cup Distaff. ">
  <thead>
    <tr>
      <th>Results</th>
      <th>PP</th>
      <th>Horse</th>
      <th>Trainer</th>
      <th>Jockey</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td data-title="Result">1</td>
      <td data-title="PP">10</td>
      <td data-title="Horse">Marche Lorraine (JPN)</td>
      <td data-title="Trainer">Yoshito Yahagi</td>
      <td data-title="Jockey">Oisin Murphy</td>
    </tr>

    <tr>
      <td data-title="Result">2</td>
      <td data-title="PP">11</td>
      <td data-title="Horse">Dunbar Road</td>
      <td data-title="Trainer">Chad Brown</td>
      <td data-title="Jockey">Jose L. Ortiz</td>
    </tr>
    <tr>
      <td data-title="Result">3</td>
      <td data-title="PP">3</td>
      <td data-title="Horse">Malathaat</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Jockey">John Velazquez</td>
    </tr>
  </tbody>
</table>

 </div>
  <script src="/assets/js/sorttable_results.js"></script>
