{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Distaff Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2021 Breeders' Cup Distaff"
			summary="The latest odds for the Breeders' Cup Distaff available">

			<caption class="hide-sm">2021 Breeders' Cup Distaff Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
				<tr>
					<td data-title="PP">1</td>
					<td data-title="Horse">Private Mission</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Bob Baffert</td>
					<td data-title="Jockey">Flavien Prat</td>
				</tr>
				<tr>
					<td data-title="PP">2</td>
					<td data-title="Horse">Royal Flag</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">Chad C. Brown</td>
					<td data-title="Jockey">Joel Rosario</td>
				</tr>
				<tr>
					<td data-title="PP">3</td>
					<td data-title="Horse">Malathaat</td>
					<td data-title="Odds">4-1</td>
					<td data-title="Trainer">Todd Pletcher</td>
					<td data-title="Jockey">John Velazquez</td>
				</tr>
				<tr>
					<td data-title="PP">4</td>
					<td data-title="Horse">Blue Stripe(ARG)</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">Marcelo Polanco</td>
					<td data-title="Jockey">Lafranco Dettori</td>
				</tr>
				<tr>
					<td data-title="PP">5</td>
					<td data-title="Horse">Clairiere</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Steven Asmussen</td>
					<td data-title="Jockey">Ricardo Santana,Jr.</td>
				</tr>
				<tr>
					<td data-title="PP">6</td>
					<td data-title="Horse">Letruska</td>
					<td data-title="Odds">8-5</td>
					<td data-title="Trainer">Fausto Gutierrez</td>
					<td data-title="Jockey">Irad Ortiz,Jr.</td>
				</tr>
				<tr>
					<td data-title="PP">7</td>
					<td data-title="Horse">Horologist</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">William Mott</td>
					<td data-title="Jockey">Junior Alvarado</td>
				</tr>
				<tr>
					<td data-title="PP">8</td>
					<td data-title="Horse">Shedaresthedevil</td>
					<td data-title="Odds">4-1</td>
					<td data-title="Trainer">Brad Cox</td>
					<td data-title="Jockey">Florent Geroux</td>
				</tr>
				<tr>
					<td data-title="PP">9</td>
					<td data-title="Horse">As Time Goes By</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">Bob Baffert</td>
					<td data-title="Jockey">Luis Saez</td>
				</tr>
				<tr>
					<td data-title="PP">10</td>
					<td data-title="Horse">Marche Lorraibne(JPN)</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer"> Yoshito Yahagi</td>
					<td data-title="Jockey">Oisin Murphy</td>
				</tr>
				<tr>
					<td data-title="PP">11</td>
					<td data-title="Horse">Dunbar Road</td>
					<td data-title="Odds">5-1</td>
					<td data-title="Trainer"> Chad Brown</td>
					<td data-title="Jockey">Jose Ortiz</td>
			</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}