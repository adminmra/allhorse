<h2>2021 Breeders' Cup Distaff Payouts</h2>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Distaff Payouts" summary="Last year's Payouts of the Breeders' Cup Distaff. ">
    <tbody>
      <thead>
        <tr>
          <th width="5%">PP</th>
          <th width="35%">Horses</th>
          <th width="10%">Win</th>
          <th width="10%">Place</th>
          <th width="10%">Show</th>
        </tr>
      </thead>
      <tr>
        <td>10</td>
        <td class="name">Marche Lorraine (JPN)</td>
        <td>
          <div class="box">$101.80</div>
        </td>
        <td>
          <div class="box">$41.00</div>
        </td>
        <td class="payoff">
          <div class="box">$18.80</div>
        </td>
      </tr>
      <tr>
        <td>11</td>
        <td class="name">Dunbar Road</td>
        <td></td>
        <td>
          <div class="box">$11.60</div>
        </td>
        <td class="payoff">
          <div class="box">$6.80</div>
        </td>
      </tr>
      <tr>
        <td>3</td>
        <td class="name">Malathaat</td>
        <td></td>
        <td></td>
        <td class="payoff">
          <div class="box">$3.80</div>
        </td>
      </tr>

    </tbody>
  </table>
</div>
<p></p>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Distaff Payouts" summary="Last year's Payouts of the Breeders' Cup Distaff. ">
    <tbody>
      <thead>
        <tr>
          <th width="30%">Wager</th>
          <th width="30%">Horses</th>
          <th width="30%">Payout</th>
        </tr>
      </thead>

      <tr class="odd">
        <td>$1.00 Exacta</td>
        <td>10-11</td>
        <td class="payoff">$513.30</td>
      </tr>
      <tr class="even">
        <td>$0.50 Trifecta</td>
        <td>10-11-3</td>
        <td class="payoff">$2,005.00</td>
      </tr>
      <tr class="odd">
        <td>$0.10 Superfecta</td>
        <td>10-11-3-5</td>
        <td class="payoff">$2,245.52</td>
      </tr>
      <tr class="even">
        <td>$1.00 Double</td>
        <td>3/10</td>
        <td class="payoff">$437.90</td>
      </tr>
      <tr class="odd">
        <td>$0.50 Pick 3</td>
        <td>5/3/10</td>
        <td class="payoff">$403.25</td>
      </tr>
    </tbody>
  </table>
</div>