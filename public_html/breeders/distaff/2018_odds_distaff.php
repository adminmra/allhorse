	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Distaff - To Win">
			<caption>Horses - Breeders Cup Distaff - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 3, 2018 17:06:08 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Distaff - To Win  - Nov 03					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Abel Tasman</td><td>7/2</td><td>+350</td></tr><tr><td>Blue Prize</td><td>6/1</td><td>+600</td></tr><tr><td>Champagne Problems</td><td>12/1</td><td>+1200</td></tr><tr><td>Midnight Bisou</td><td>6/1</td><td>+600</td></tr><tr><td>Monomoy Girl</td><td>2/1</td><td>+200</td></tr><tr><td>Mopotism</td><td>30/1</td><td>+3000</td></tr><tr><td>Vale Dori</td><td>12/1</td><td>+1200</td></tr><tr><td>Verve's Tale</td><td>30/1</td><td>+3000</td></tr><tr><td>Wonder Gadot</td><td>15/1</td><td>+1500</td></tr><tr><td>Wow Cat</td><td>8/1</td><td>+800</td></tr><tr><td>La Force</td><td>20/1</td><td>+2000</td></tr>			</tbody>
		</table>
	</div>
{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
{/literal}
	