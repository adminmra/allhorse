<p>

The Distaff  brings together the best female dirt horses for a championship showdown at 1 1/8 miles on the main track. This race was won by the great Zenyatta at <a href="/santa-anita-park">Santa Anita</a> in 2008.
</p>