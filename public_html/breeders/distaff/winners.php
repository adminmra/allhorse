<div class="table-responsive">
<table cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"  title="Breeders' Cup Distaff Winners" summary="The past winners of the Breeders Cup Distaff. " ><tbody>

				  <tr>
                    <th >Year</th>
                    <th>Winner</th>
                    <th>Jockey</th>
                    <th>Trainer</th>
                    <th>Win Time</th>
				  </tr>

    <tr>
      <td data-title="Year">2018</td>
      <td data-title="Winner">Monomoy Girl</td>
      <td data-title="Jockey">F. Geroux</td>
      <td data-title="Trainer">B. Cox</td>
     <td data-title="Win Time">1:49.79 </td>
    </tr>    <tr>
      <td data-title="Year">2017</td>
      <td data-title="Winner">Forever Unbridled</td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Dallas Stewart</td>
     <td data-title="Win Time">1:50.25</td>
    </tr>
    <tr>
      <td data-title="Year">2016</td>
      <td data-title="Winner">Beholder</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Richard Mandella</td>
     <td data-title="Win Time">1:49.22</td>
    </tr>
    <tr>
      <td data-title="Year">2015</td>
      <td data-title="Winner">Stopchargingmaria</td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
     <td data-title="Win Time">1:48.98</td>
    </tr>
    <tr>
      <td data-title="Year">2014</td>
      <td data-title="Winner">Untapable</td>
      <td data-title="Jockey">Rosie Napravnik</td>
      <td data-title="Trainer">Steve Asmussen</td>
     <td data-title="Win Time">1:48.68</td>
    </tr>
    <tr>
      <td data-title="Year">2013</td>
      <td data-title="Winner">Beholder</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Richard Mandella</td>
     <td data-title="Win Time">1:47.77</td>
    </tr>
    <tr>
      <td data-title="Year">2012</td>
      <td data-title="Winner">Royal Delta</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">William I. Mott</td>
     <td data-title="Win Time">1:48.80</td>
    </tr>
    <tr>
      <td data-title="Year">2011</td>
      <td data-title="Winner">Royal Delta</td>
      <td data-title="Jockey">Jose Lezcano</td>
      <td data-title="Trainer">William I. Mott</td>
     <td data-title="Win Time">1:50.78</td>
    </tr>
    <tr>
      <td data-title="Year">2010</td>
      <td data-title="Winner">Unrivaled Belle</td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">William I. Mott</td>
     <td data-title="Win Time">1:50.04</td>
    </tr>
    <tr>
      <td data-title="Year">2009</td>
      <td data-title="Winner">Life Is Sweet</td>
      <td data-title="Jockey">Garrett K. Gomez</td>
      <td data-title="Trainer">John Shirreffs</td>
     <td data-title="Win Time">1:48.58</td>
    </tr>
    <tr>
      <td data-title="Year">2008</td>
      <td data-title="Winner">Zenyatta</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">John Shirreffs</td>
     <td data-title="Win Time">1:46.85</td>
    </tr>
    <tr>
      <td data-title="Year">2007</td>
      <td data-title="Winner">Ginger Punch</td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Robert J. Frankel</td>
     <td data-title="Win Time">1:50.11</td>
    </tr>
    <tr>
      <td data-title="Year">2006</td>
      <td data-title="Winner">Round Pond</td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Michael R. Matz</td>
     <td data-title="Win Time">1:50.50</td>
    </tr>
    <tr>
      <td data-title="Year">2005</td>
      <td data-title="Winner">Pleasant Home</td>
      <td data-title="Jockey">Cornelio Velasquez</td>
      <td data-title="Trainer">C. R. McGaughey III</td>
     <td data-title="Win Time">1:48.34</td>
    </tr>
    <tr>
      <td data-title="Year">2004</td>
      <td data-title="Winner">Ashado</td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd Pletcher</td>
     <td data-title="Win Time">1:48.26</td>
    </tr>
    <tr>
      <td data-title="Year">2003</td>
      <td data-title="Winner">Adoration</td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">David Hofmans</td>
     <td data-title="Win Time">1:49.17</td>
    </tr>
    <tr>
      <td data-title="Year">2002</td>
      <td data-title="Winner">Azeri</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Laura de Seroux</td>
     <td data-title="Win Time">1:48.64</td>
    </tr>
    <tr>
      <td data-title="Year">2001</td>
      <td data-title="Winner">Unbridled Elaine</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Dallas Stewart</td>
     <td data-title="Win Time">1:49.21</td>
    </tr>
    <tr>
      <td data-title="Year">2000</td>
      <td data-title="Winner">Spain</td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
     <td data-title="Win Time">1:47.66</td>
    </tr>
    <tr>
      <td data-title="Year">1999</td>
      <td data-title="Winner">Beautiful Pleasure</td>
      <td data-title="Jockey">Jorge F. Chavez</td>
      <td data-title="Trainer">John T. Ward, Jr.</td>
     <td data-title="Win Time">1:47.56</td>
    </tr>
    <tr>
      <td data-title="Year">1998</td>
      <td data-title="Winner">Escena</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">William I. Mott</td>
     <td data-title="Win Time">1:49.89</td>
    </tr>
    <tr>
      <td data-title="Year">1997</td>
      <td data-title="Winner">Ajina</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">William I. Mott</td>
     <td data-title="Win Time">1:47.20</td>
    </tr>
    <tr>
      <td data-title="Year">1996</td>
      <td data-title="Winner">Jewel Princess</td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Wallace Dollase</td>
     <td data-title="Win Time">1:48.40</td>
    </tr>
    <tr>
      <td data-title="Year">1995</td>
      <td data-title="Winner">Inside Information</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">C. R. McGaughey III</td>
     <td data-title="Win Time">1:46.15</td>
    </tr>
    <tr>
      <td data-title="Year">1994</td>
      <td data-title="Winner">One Dreamer</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Thomas F. Proctor</td>
     <td data-title="Win Time">1:50.79</td>
    </tr>
    <tr>
      <td data-title="Year">1993</td>
      <td data-title="Winner">Hollywood Wildcat</td>
      <td data-title="Jockey">Ed Delahoussaye</td>
      <td data-title="Trainer">Neil Drysdale</td>
     <td data-title="Win Time">1:48.35</td>
    </tr>
    <tr>
      <td data-title="Year">1992</td>
      <td data-title="Winner">Paseana (ARG) </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Ron McAnally</td>
     <td data-title="Win Time">1:48.17</td>
    </tr>
    <tr>
      <td data-title="Year">1991</td>
      <td data-title="Winner">Dance Smartly</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">James E. Day</td>
     <td data-title="Win Time">1:50.95</td>
    </tr>
    <tr>
      <td data-title="Year">1990</td>
      <td data-title="Winner">Bayakoa (ARG) </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Ron McAnally</td>
     <td data-title="Win Time">1:49.20</td>
    </tr>
    <tr>
      <td data-title="Year">1989</td>
      <td data-title="Winner">Bayakoa (ARG) </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Ron McAnally</td>
     <td data-title="Win Time">1:47.40</td>
    </tr>
    <tr>
      <td data-title="Year">1988</td>
      <td data-title="Winner">Personal Ensign</td>
      <td data-title="Jockey">Randy Romero</td>
      <td data-title="Trainer">C. R. McGaughey III</td>
     <td data-title="Win Time">1:52.00</td>
    </tr>
    <tr>
      <td data-title="Year">1987</td>
      <td data-title="Winner">Sacahuista</td>
      <td data-title="Jockey">Randy Romero</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
     <td data-title="Win Time">2:02.80</td>
    </tr>
    <tr>
      <td data-title="Year">1986</td>
      <td data-title="Winner">Lady's Secret</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
     <td data-title="Win Time">2:01.20</td>
    </tr>
    <tr>
      <td data-title="Year">1985</td>
      <td data-title="Winner">Life's Magic</td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
     <td data-title="Win Time">2:02.00</td>
    </tr>
    <tr>
      <td data-title="Year">1984</td>
      <td data-title="Winner">Princess Rooney</td>
      <td data-title="Jockey">Ed Delahoussaye</td>
      <td data-title="Trainer">Neil Drysdale</td>
     <td data-title="Win Time">2:02.40</td>
    </tr>
       </tbody>
		  </table>
</div>
{literal} 
<style type="text/css" >
.table> tbody > tr th {
    text-align: center!important
}
</style>

{/literal}