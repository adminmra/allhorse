	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Breeders' Cup Distaff Odds"
	  }
	</script>
	{/literal}
	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Breeders Cup Distaff - To Win">
			<caption>Horses - Breeders Cup Distaff - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:23 </em>
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Breeders Cup Distaff - To Win  - Nov 06					</th>
			</tr>
        -->
	<tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Letruska</td><td>81/50</td><td>+162</td></tr><tr><td>Malathaat</td><td>9/2</td><td>+450</td></tr><tr><td>Shedaresthedevil</td><td>7/1</td><td>+700</td></tr><tr><td>Royal Flag</td><td>6/1</td><td>+600</td></tr><tr><td>Private Mission</td><td>8/1</td><td>+800</td></tr><tr><td>Clairiere</td><td>11/1</td><td>+1100</td></tr><tr><td>Dunbar Road</td><td>20/1</td><td>+2000</td></tr><tr><td>As Time Goes By</td><td>25/1</td><td>+2500</td></tr><tr><td>Blue Stripe</td><td>25/1</td><td>+2500</td></tr><tr><td>Horologist</td><td>50/1</td><td>+5000</td></tr><tr><td>Marche Lorraine</td><td>50/1</td><td>+5000</td></tr>			</tbody>
		</table>
	</div>
{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
{/literal}
	