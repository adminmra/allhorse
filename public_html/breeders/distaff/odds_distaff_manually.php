	{literal}
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Table",
			"about": "Breeders' Cup Distaff Odds"
		}
	</script>
	{/literal}
	<div id="no-more-tables">
		<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center"
			cellpadding="0" cellspacing="0" border="0" title="2020 Breeders' Cup Distaff"
			summary="The latest odds for the Breeders' Cup Distaff available">

			<caption class="hide-sm">2020 Breeders' Cup Distaff Odds and Post Position</caption>


			<tbody>
				<tr class='head_title hide-sm'>
					<th>PP</th>
					<th>Horse</th>
					<th>Odds</th>
					<th>Trainer</th>
					<th>Jockey</th>
				</tr>
				<tr>
					<td data-title="PP">1</td>
					<td data-title="Horse">Ce Ce</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Michael W. McCarthy</td>
					<td data-title="Jockey">John Velazques</td>
				</tr>
				<tr>
					<td data-title="PP">3</td>
					<td data-title="Horse">Dunbar Road</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Chad C. Brown</td>
					<td data-title="Jockey">Jose Ortiz</td>
				</tr>
				<tr>
					<td data-title="PP">2</td>
					<td data-title="Horse">Harvest Moon</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Simon Callaghan</td>
					<td data-title="Jockey">Flavien Prat</td>
				</tr>
				<tr>
					<td data-title="PP">4</td>
					<td data-title="Horse">Horologist</td>
					<td data-title="Odds">8-1</td>
					<td data-title="Trainer">William I. Mott</td>
					<td data-title="Jockey">Junior Alvarado</td>
				</tr>
				<tr>
					<td data-title="PP">6</td>
					<td data-title="Horse">Lady Kate</td>
					<td data-title="Odds">15-1</td>
					<td data-title="Trainer">Eddie Kenneally</td>
					<td data-title="Jockey">Tyler Gaffalione</td>
				</tr>
				<tr>
					<td data-title="PP">10</td>
					<td data-title="Horse">Monomoy Girl</td>
					<td data-title="Odds">8-5</td>
					<td data-title="Trainer">Brad Cox</td>
					<td data-title="Jockey">Florent Geroux</td>
				</tr>
				<tr>
					<td data-title="PP">9</td>
					<td data-title="Horse">Ollie's Candy</td>
					<td data-title="Odds">10-1</td>
					<td data-title="Trainer">John W. Sadler</td>
					<td data-title="Jockey">Joel Rosario</td>
				</tr>
				<tr>
					<td data-title="PP">7</td>
					<td data-title="Horse">Point of Honor</td>
					<td data-title="Odds">30-1</td>
					<td data-title="Trainer">George Weaver</td>
					<td data-title="Jockey">Javier Castellano</td>
				</tr>
				<tr>
					<td data-title="PP">5</td>
					<td data-title="Horse">Swiss Skydiver</td>
					<td data-title="Odds">2-1</td>
					<td data-title="Trainer">Kenneth G. McPeek</td>
					<td data-title="Jockey">Robby Alvarado</td>
				</tr>
				<tr>
					<td data-title="PP">8</td>
					<td data-title="Horse">Valiance</td>
					<td data-title="Odds">12-1</td>
					<td data-title="Trainer">Todd A. Pletcher</td>
					<td data-title="Jockey">Luis Saez</td>
				</tr>
			</tbody>
		</table>
	</div>
	{literal}
	<style>
		@media screen and (max-width: 640px) {
			.fa-sort {
				display: block !important;
			}
		}
	</style>
	<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
	<script src="/assets/js/sorttable_post.js"></script> {/literal}