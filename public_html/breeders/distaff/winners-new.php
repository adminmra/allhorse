{literal}

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Breeders' Cup Distaff Winners"
  }
</script>
{/literal}
<div id="no-more-tables">

  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed">
    <thead>
      <tr>
        <th>Year</th>
        <th>Winner</th>
        <th>Age</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th>Win Time</th>
      </tr>
    </thead>
    <tbody>
	   <tr>
        <td data-title="Year">2021</td>
        <td data-title="Winner">Marche Lorraine (JPN)</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Oisin Murphy</td>
        <td data-title="Trainer">Yoshito Yahagi</td>
        <td data-title="Owner">U. Carrot Farm</td>
        <td data-title="Win Time">1:47.67</td>
      </tr>
      <tr>
        <td data-title="Year">2020</td>
        <td data-title="Winner">Monomoy Girl</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Florent Geroux</td>
        <td data-title="Trainer">Brad Cox</td>
        <td data-title="Owner">Stuart Grant, Michael Dubb, Monomoy Stables, LLC</td>
        <td data-title="Win Time">1:47.84</td>
      </tr>
      <tr>
        <td data-title="Year">2019</td>
        <td data-title="Winner">Blue Prize</td>
        <td data-title="Age">6</td>
        <td data-title="Jockey">Joe Bravo</td>
        <td data-title="Trainer">Ignacio Correas</td>
        <td data-title="Owner">Merriebelle Stable, LLC</td>
        <td data-title="Win Time">1:50.50 </td>
      </tr>
      <tr>
        <td data-title="Year">2018</td>
        <td data-title="Winner">Monomoy Girl</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Florent Geroux</td>
        <td data-title="Trainer">Brad Cox </td>
        <td data-title="Owner">Michael Dubb, Monomoy Stables et al</td>
        <td data-title="Win Time">1:49.79 </td>
      </tr>
      <tr>
        <td data-title="Year">2017</td>
        <td data-title="Winner">Forever Unbridled</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Dallas Stewart</td>
        <td data-title="Owner">Charles E. Fipke</td>
        <td data-title="Win Time">1:50.25</td>
      </tr>
      <tr>
        <td data-title="Year">2016</td>
        <td data-title="Winner">Beholder</td>
        <td data-title="Age">6</td>
        <td data-title="Jockey">Gary Stevens</td>
        <td data-title="Trainer">Richard Mandella</td>
        <td data-title="Owner">Spendthrift Farm</td>
        <td data-title="Win Time">1:49.22</td>
      </tr>
      <tr>
        <td data-title="Year">2015</td>
        <td data-title="Winner">Stopchargingmaria</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Javier Castellano</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Owner">Town And Country Farms</td>
        <td data-title="Win Time">1:48.98</td>
      </tr>
      <tr>
        <td data-title="Year">2014</td>
        <td data-title="Winner">Untapable</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Rosie Napravnik</td>
        <td data-title="Trainer">Steve Asmussen</td>
        <td data-title="Owner">Winchell Thoroughbreds</td>
        <td data-title="Win Time">1:48.68</td>
      </tr>
      <tr>
        <td data-title="Year">2013</td>
        <td data-title="Winner">Beholder</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Gary Stevens</td>
        <td data-title="Trainer">Richard Mandella</td>
        <td data-title="Owner">Spendthrift Farm</td>
        <td data-title="Win Time">1:47.77</td>
      </tr>
      <tr>
        <td data-title="Year">2012</td>
        <td data-title="Winner">Royal Delta</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">William I. Mott</td>
        <td data-title="Owner">Besilu Stables</td>
        <td data-title="Win Time">1:48.80</td>
      </tr>
      <tr>
        <td data-title="Year">2011</td>
        <td data-title="Winner">Royal Delta</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Jose Lezcano</td>
        <td data-title="Trainer">William I. Mott</td>
        <td data-title="Owner">Palides Investments</td>
        <td data-title="Win Time">1:50.78</td>
      </tr>
      <tr>
        <td data-title="Year">2010</td>
        <td data-title="Winner">Unrivaled Belle</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Kent Desormeaux</td>
        <td data-title="Trainer">William I. Mott</td>
        <td data-title="Owner">Gary Seidler & Pete Vegso</td>
        <td data-title="Win Time">1:50.04</td>
      </tr>
      <tr>
        <td data-title="Year">2009</td>
        <td data-title="Winner">Life Is Sweet</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Garrett K. Gomez</td>
        <td data-title="Trainer">John Shirreffs</td>
        <td data-title="Owner">Pam & Martin Wygod</td>
        <td data-title="Win Time">1:48.58</td>
      </tr>
      <tr>
        <td data-title="Year">2008</td>
        <td data-title="Winner">Zenyatta</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">John Shirreffs</td>
        <td data-title="Owner">Ann & Jerry Moss</td>
        <td data-title="Win Time">1:46.85</td>
      </tr>
      <tr>
        <td data-title="Year">2007</td>
        <td data-title="Winner">Ginger Punch</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Rafael Bejarano</td>
        <td data-title="Trainer">Robert J. Frankel</td>
        <td data-title="Owner">Stronach Stables</td>
        <td data-title="Win Time">1:50.11</td>
      </tr>
      <tr>
        <td data-title="Year">2006</td>
        <td data-title="Winner">Round Pond</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Edgar Prado</td>
        <td data-title="Trainer">Michael R. Matz</td>
        <td data-title="Owner">Fox Hill Farm</td>
        <td data-title="Win Time">1:50.50</td>
      </tr>
      <tr>
        <td data-title="Year">2005</td>
        <td data-title="Winner">Pleasant Home</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Cornelio Velasquez</td>
        <td data-title="Trainer">C. R. McGaughey III</td>
        <td data-title="Owner">Ogden Mills Phipps</td>
        <td data-title="Win Time">1:48.34</td>
      </tr>
      <tr>
        <td data-title="Year">2004</td>
        <td data-title="Winner">Ashado</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Owner">Starlight Stables et al</td>
        <td data-title="Win Time">1:48.26</td>
      </tr>
      <tr>
        <td data-title="Year">2003</td>
        <td data-title="Winner">Adoration</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Pat Valenzuela</td>
        <td data-title="Trainer">David Hofmans</td>
        <td data-title="Owner">Amerman Racing</td>
        <td data-title="Win Time">1:49.17</td>
      </tr>
      <tr>
        <td data-title="Year">2002</td>
        <td data-title="Winner">Azeri</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">Laura de Seroux</td>
        <td data-title="Owner">Allen E. Paulson</td>
        <td data-title="Win Time">1:48.64</td>
      </tr>
      <tr>
        <td data-title="Year">2001</td>
        <td data-title="Winner">Unbridled Elaine</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">Dallas Stewart</td>
        <td data-title="Owner">Roger J. Devenport</td>
        <td data-title="Win Time">1:49.21</td>
      </tr>
      <tr>
        <td data-title="Year">2000</td>
        <td data-title="Winner">Spain</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Victor Espinoza</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">The Thoroughbred Corp.</td>
        <td data-title="Win Time">1:47.66</td>
      </tr>
      <tr>
        <td data-title="Year">1999</td>
        <td data-title="Winner">Beautiful Pleasure</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Jorge F. Chavez</td>
        <td data-title="Trainer">John T. Ward, Jr.</td>
        <td data-title="Owner">John C. Oxley</td>
        <td data-title="Win Time">1:47.56</td>
      </tr>
      <tr>
        <td data-title="Year">1998</td>
        <td data-title="Winner">Escena</td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Gary Stevens</td>
        <td data-title="Trainer">William I. Mott</td>
        <td data-title="Owner">Allen E. Paulson</td>
        <td data-title="Win Time">1:49.89</td>
      </tr>
      <tr>
        <td data-title="Year">1997</td>
        <td data-title="Winner">Ajina</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">William I. Mott</td>
        <td data-title="Owner">Allen E. Paulson</td>
        <td data-title="Win Time">1:47.20</td>
      </tr>
      <tr>
        <td data-title="Year">1996</td>
        <td data-title="Winner">Jewel Princess</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Corey Nakatani</td>
        <td data-title="Trainer">Wallace Dollase</td>
        <td data-title="Owner">M & R Stephen/TTC</td>
        <td data-title="Win Time">1:48.40</td>
      </tr>
      <tr>
        <td data-title="Year">1995</td>
        <td data-title="Winner">Inside Information</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">C. R. McGaughey III</td>
        <td data-title="Owner">Ogden Mills Phipps</td>
        <td data-title="Win Time">1:46.15</td>
      </tr>
      <tr>
        <td data-title="Year">1994</td>
        <td data-title="Winner">One Dreamer</td>
        <td data-title="Age">6</td>
        <td data-title="Jockey">Gary Stevens</td>
        <td data-title="Trainer">Thomas F. Proctor</td>
        <td data-title="Owner">Glen Hill Farm</td>
        <td data-title="Win Time">1:50.79</td>
      </tr>
      <tr>
        <td data-title="Year">1993</td>
        <td data-title="Winner">Hollywood Wildcat</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Ed Delahoussaye</td>
        <td data-title="Trainer">Neil Drysdale</td>
        <td data-title="Owner">Irving & Marjorie Cowan</td>
        <td data-title="Win Time">1:48.35</td>
      </tr>
      <tr>
        <td data-title="Year">1992</td>
        <td data-title="Winner">Paseana (ARG) </td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Chris McCarron</td>
        <td data-title="Trainer">Ron McAnally</td>
        <td data-title="Owner">Sidney H. Craig</td>
        <td data-title="Win Time">1:48.17</td>
      </tr>
      <tr>
        <td data-title="Year">1991</td>
        <td data-title="Winner">Dance Smartly</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">James E. Day</td>
        <td data-title="Owner">Sam-Son Farm</td>
        <td data-title="Win Time">1:50.95</td>
      </tr>
      <tr>
        <td data-title="Year">1990</td>
        <td data-title="Winner"><strong>Bayakoa</strong> (ARG) </td>
        <td data-title="Age">6</td>
        <td data-title="Jockey">Laffit Pincay, Jr.</td>
        <td data-title="Trainer">Ron McAnally</td>
        <td data-title="Owner">Frank & Janis Whitham</td>
        <td data-title="Win Time">1:49.20</td>
      </tr>
      <tr>
        <td data-title="Year">1989</td>
        <td data-title="Winner"><strong>Bayakoa</strong> (ARG) </td>
        <td data-title="Age">5</td>
        <td data-title="Jockey">Laffit Pincay, Jr.</td>
        <td data-title="Trainer">Ron McAnally</td>
        <td data-title="Owner">Frank & Janis Whitham</td>
        <td data-title="Win Time">1:47.40</td>
      </tr>
      <tr>
        <td data-title="Year">1988</td>
        <td data-title="Winner">Personal Ensign</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Randy Romero</td>
        <td data-title="Trainer">C. R. McGaughey III</td>
        <td data-title="Owner">Ogden Phipps</td>
        <td data-title="Win Time">1:52.00</td>
      </tr>
      <tr>
        <td data-title="Year">1987</td>
        <td data-title="Winner">Sacahuista</td>
        <td data-title="Age">3</td>
        <td data-title="Jockey">Randy Romero</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">B. A. Beal & L. R. French</td>
        <td data-title="Win Time">2:02.80†</td>
      </tr>
      <tr>
        <td data-title="Year">1986</td>
        <td data-title="Winner">Lady's Secret</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Gene Klein</td>
        <td data-title="Win Time">2:01.20†</td>
      </tr>
      <tr>
        <td data-title="Year">1985</td>
        <td data-title="Winner">Life's Magic</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Angel Cordero, Jr.</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Mel Hatley/Gene Klein</td>
        <td data-title="Win Time">2:02.00†</td>
      </tr>
      <tr>
        <td data-title="Year">1984</td>
        <td data-title="Winner">Princess Rooney</td>
        <td data-title="Age">4</td>
        <td data-title="Jockey">Ed Delahoussaye</td>
        <td data-title="Trainer">Neil Drysdale</td>
        <td data-title="Owner">Paula J. Tucker</td>
        <td data-title="Win Time">2:02.40†</td>
      </tr>
    </tbody>
  </table>
</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>

{/literal}