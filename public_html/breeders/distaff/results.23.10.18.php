<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"  title="Breeders' Distaff Results" summary="Last year's results of the Breeders' Cup Distaff. " >
<tbody><tr>
							 <th width="5%">Result</th>
								 <th width="5%">Time</th> 
								 <th width="5%">Post</th> 
								 <th width="5%">Odds</th>  
								 <th width="24%">Horse</th>
                                  <th width="14%">Trainer</th>
                                  <th width="14%">Jockey</th> 
                                  <th width="20%">Owner</th>
                                </tr>
                                <tr>
                                  <td>1</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td></td>
                                  <td>Forever Unbridled</td>
                                  <td >Dallas Stewart</td>
                                  <td >John R. Velazquez</td> 
                                  <td >Charles E. Fipke</td>
                           		</tr>
                                <tr>
                                  <td>2</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td></td>
                                  <td>Abel Tasman</td>
                                  <td >Bob Baffert</td>
                                  <td ></td> 
                                  <td >China Horse Club International Ltd. and Clearsky Farms</td>
                           		</tr>
                                <tr>
                                  <td>3</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td></td>
                                  <td>Paradise Woods</td>
                                  <td >Richard Mandella</td>
                                  <td >&nbsp;</td> 
                                  <td >Sarkowsky, Steven, Wygod, Martin J. and Wygod, Pam</td>
                           		</tr>
                                <tr>
                                  <td>4</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td></td>
                                  <td>Elate</td>
                                  <td >William Mott</td>
                                  <td >&nbsp;</td> 
                                  <td >Claiborne Farm and Dilschneider, Adele B.</td>
                           		</tr>
                                <tr>
                                  <td>5</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td></td>
                                  <td>Mopotism</td>
                                  <td >Doug O'Neill</td>
                                  <td >&nbsp;</td> 
                                  <td >Reddam Racing LLC</td>
                           		</tr>
                                <tr>
                                  <td>6</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td></td>
                                  <td>Champagne Room</td>
                                  <td >Peter Eurton</td>
                                  <td >&nbsp;</td> 
                                  <td >Alesia, Christensen, Ciaglia RacingLLC, Exline-Border RacingLLC, Gulliver RacingLL</td>
                           		</tr>
                                <tr>
                                  <td>7</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td></td>
                                  <td>Romantic Vision</td>
                                  <td >George Arnold II</td>
                                  <td >&nbsp;</td> 
                                  <td >G. Watts Humphrey, Jr.</td>
                           		</tr>
                                        
                                <tr>
                                  <td>8</td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                  <td></td>
                                  <td>Stellar Wind</td>
                                  <td >John Sadler</td>
                                  <td >&nbsp;</td> 
                                  <td >Hronis Racing LLC</td>
                           		</tr>
                                                                      

										 										
</table> </div>