<!-- BREEDERS -->


{php}
$startDate = strtotime("17 March 2014 12:00:00");
$endDate  = strtotime("04 November 2018 17:35:00");
{/php}

<div class="countdown margin-bottom-30">
<div class="headline"><h2>The Breeders' Cup</h2></div>

<div class="panel">
	<div class="race"><span>Race: Nov 2nd & 3rd </span>- It's and open field to run at this year's  <a href="/breeders-cup/betting">Breeders' Cup</a> at <a href="/churchill-downs">Churchill Downs</a>.</div>
	<div class="info"> <p>{* <span>You can bet now:</span>Fixed <a href="/breeders-cup/odds">odds</a> for the <a href="/breeders-cup/classic">Classic</a>, <a href="/breeders-cup/distaff">Distaff</a>, <a href="/breeders-cup/mile">Mile</a>, <a href="/breeders-cup/turf">Turf</a>, <a href="/breeders-cup/juvenile">Juvenile</a> and <a href="/breeders-cup/juvenile-fillies">Juvenile Fillies</a> are  live!</p> *}</div>
</div>

<div class="clock">
  <div class="item clock_days">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>      
     <div class="topLayer"><canvas id="canvas_days" width="188" height="188"> </canvas></div>      
      <div class="text">
        <p class="val">0</p>
        <p class="time type_days">Days</p>
      </div>
  </div>

  <div class="item clock_hours">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>
      <div class="topLayer"> <canvas id="canvas_hours" width="188" height="188"> </canvas></div>
      <div class="text">
      	<p class="val">0</p>
        <p class="time type_hours">Hours</p>
      </div>
  </div>  

  <div class="item clock_minutes">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>
      <div class="topLayer"><canvas id="canvas_minutes" width="188" height="188"> </canvas></div>
      <div class="text">
        <p class="val">0</p>
        <p class="time type_minutes">Minutes</p>
      </div>
  </div>
  
  <div class="item clock_seconds">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>
      <div class="topLayer"><canvas id="canvas_seconds" width="188" height="188"> </canvas></div>     
      <div class="text">
        <p class="val">0</p>
        <p class="time type_seconds">Seconds</p>
      </div>
  </div>
</div>

<br /><br /><br /><br /><br /><!-- delete these if you use/uncomment the button below -->
<!--<div class="blockfooter"><a class="btn btn-primary" href="/travers-stakes" target="_blank" rel="nofollow">Learn More <i class="fa fa-angle-right"></i></a></div>
</div><!-- end/countdown -->


<!-- Initialize the countdown -->
{literal}
<script type="text/javascript">
$(document).ready(function(){	
JBCountDown({ secondsColor : "#ffdc50", secondsGlow  : "none", minutesColor : "#9cdb7d", minutesGlow  : "none", hoursColor : "#378cff", hoursGlow    : "none",  daysColor    : "#ff6565", daysGlow     : "none", startDate   : "{/literal}{php} echo $startDate; {/php}{literal}", endDate : "{/literal}{php} echo $endDate; {/php}{literal}", now : "{/literal}{php} echo strtotime('now'); {/php}{literal}" });
 });
</script>
{/literal}