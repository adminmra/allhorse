	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Melbourne Cup Odds"
	  }
	</script>
	{/literal}     <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" summary="Horses - Melbourne Cup - To Win">
            <caption>Horses - Melbourne Cup - To Win</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 27, 2020.</em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
    <tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td data-title=' '>Surprise Baby</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Vow And Declare</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Shared Ambition</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Il Paradiso</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Cross Counter</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Finche</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Warning</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Master Of Reality</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Castelvecchio</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Schabau</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Degraves</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Southern Moon</td><td data-title='Fractional'>50/1</td><td data-title='American'>+5000</td></tr><tr><td data-title=' '>Southern France</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Constantinople</td><td data-title='Fractional'>50/1</td><td data-title='American'>+5000</td></tr><tr><td data-title=' '>Prnice Of Arran</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Miami Bound</td><td data-title='Fractional'>50/1</td><td data-title='American'>+5000</td></tr><tr><td data-title=' '>Skyward</td><td data-title='Fractional'>66/1</td><td data-title='American'>+6600</td></tr><tr><td data-title=' '>Young Rascal</td><td data-title='Fractional'>66/1</td><td data-title='American'>+6600</td></tr><tr><td data-title=' '>True Self</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Downdraft</td><td data-title='Fractional'>66/1</td><td data-title='American'>+6600</td></tr><tr><td data-title=' '>Carif</td><td data-title='Fractional'>100/1</td><td data-title='American'>+10000</td></tr><tr><td data-title=' '>Master Of Wine</td><td data-title='Fractional'>66/1</td><td data-title='American'>+6600</td></tr><tr><td data-title=' '>Shadow Hero</td><td data-title='Fractional'>100/1</td><td data-title='American'>+10000</td></tr><tr><td data-title=' '>Mirage Dancer</td><td data-title='Fractional'>100/1</td><td data-title='American'>+10000</td></tr><tr><td data-title=' '>Latrobe</td><td data-title='Fractional'>100/1</td><td data-title='American'>+10000</td></tr><tr><td data-title=' '>Hasta La War</td><td data-title='Fractional'>100/1</td><td data-title='American'>+10000</td></tr><tr><td data-title=' '>Gamay</td><td data-title='Fractional'>100/1</td><td data-title='American'>+10000</td></tr><tr><td data-title=' '>Vegas Jewel</td><td data-title='Fractional'>100/1</td><td data-title='American'>+10000</td></tr><tr><td data-title=' '>Mosh Music</td><td data-title='Fractional'>100/1</td><td data-title='American'>+10000</td></tr><tr><td data-title=' '>Russian Camelot</td><td data-title='Fractional'>100/1</td><td data-title='American'>+10000</td></tr><tr><td data-title=' '>Django Freeman</td><td data-title='Fractional'>100/1</td><td data-title='American'>+10000</td></tr><tr><td data-title=' '>Pancho</td><td data-title='Fractional'>100/1</td><td data-title='American'>+10000</td></tr><tr><td data-title=' '>Spirit Ridge</td><td data-title='Fractional'>100/1</td><td data-title='American'>+10000</td></tr><tr><td data-title=' '>Celestial Falls</td><td data-title='Fractional'>100/1</td><td data-title='American'>+10000</td></tr><tr><td data-title=' '>Never Listen</td><td data-title='Fractional'>200/1</td><td data-title='American'>+20000</td></tr><tr><td data-title=' '>Yonkers</td><td data-title='Fractional'>200/1</td><td data-title='American'>+20000</td></tr>            </tbody>
        </table>
    </div>
	{literal}
        <style>
            table.ordenable th{cursor: pointer}
			table.ordenable td{ word-wrap: break-word !important; word-break: break-all !important;}
			table.ordenable{ max-width: 100% !important; }
        </style>
        <script src="/assets/js/jquery.sortElements.js"></script>
        <script src="/assets/js/sorttable.js"></script>
	{/literal}
    
	 