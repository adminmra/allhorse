	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Triple Crown Specials">
			<caption>Horses - Triple Crown Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 28, 2020.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			
			<!--<tr>
					<th colspan="3" class="center">
					Horses - Triple Crown Specials  - May 16					</th>
			</tr> -->
	<thead><tr><th colspan="3" class="center">To Win Us Triple Crown In 2020</th></tr></thead><tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tbody><tr><td>Independence Hall</td><td>50/1</td><td>+5000</td></tr><tr><td>Tiz The Law</td><td>50/1</td><td>+5000</td></tr><tr><td>Dennis` Moment</td><td>60/1</td><td>+6000</td></tr><tr><td>Maxfield</td><td>66/1</td><td>+6600</td></tr><tr><td>Thousand Words</td><td>66/1</td><td>+6600</td></tr><tr><td>Honor A P</td><td>80/1</td><td>+8000</td></tr><tr><td>Storm The Court</td><td>80/1</td><td>+8000</td></tr><tr><td>By Your Side</td><td>90/1</td><td>+9000</td></tr><tr><td>Cezzane</td><td>90/1</td><td>+9000</td></tr><tr><td>Green Light Go</td><td>90/1</td><td>+9000</td></tr><tr><td>Anneau D`or</td><td>100/1</td><td>+10000</td></tr><tr><td>Eight Rings</td><td>100/1</td><td>+10000</td></tr><tr><td>Three Technique</td><td>100/1</td><td>+10000</td></tr><tr><td>Basin</td><td>125/1</td><td>+12500</td></tr><tr><td>Garth</td><td>125/1</td><td>+12500</td></tr><tr><td>Gouverneur Morris</td><td>125/1</td><td>+12500</td></tr><tr><td>Mo Hawk</td><td>125/1</td><td>+12500</td></tr><tr><td>Phantom Boss</td><td>125/1</td><td>+12500</td></tr><tr><td>Scabbard</td><td>125/1</td><td>+12500</td></tr><tr><td>Tap It To Win</td><td>125/1</td><td>+12500</td></tr><tbody><thead><tr><th colspan="3" class="center">Will There Be A Triple Crown Winner In 2020?</th></tr></thead><tbody><tr><td>Yes</td><td>8/1</td><td>+800</td></tr><tr><td>No</td><td>1/25</td><td>-2500</td></tr><tbody>		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="/assets/js/sorttbody_tc.js"></script>
    {/literal}
	