    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Ascot Gold Cup">
            <caption>Horses - Ascot Gold Cup</caption>
            <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="3">
                  <em id='updateemp'>  - Updated July 2, 2020 02:30:14 </em> <!-- Bet US Racing - Official
                  <a href="//www.usracing.com/odds/us-presidential-election">US Presidential Election Odds</a>. -->
                  <!-- <a href="//www.usracing.com/odds/us-presidential-election">US Presidential Odds</a>, -->
                </td>
              </tr>
            </tfoot>
            <tbody>
               <!-- <tr>
                    <th colspan="3" class="center">
                                        </th>
            </tr> -->
    <tr><th></th><th>Fractional</th><th>American</th></tr><tr><td>Stradivarius</td><td>EV</td><td>EV</td></tr><tr><td>Kew Gardens</td><td>7/2</td><td>+350</td></tr><tr><td>Cross Counter</td><td>14/1</td><td>+1400</td></tr><tr><td>Dee Ex Bee</td><td>12/1</td><td>+1200</td></tr><tr><td>Withold</td><td>20/1</td><td>+2000</td></tr><tr><td>Search For A Song</td><td>20/1</td><td>+2000</td></tr><tr><td>Royal Line</td><td>20/1</td><td>+2000</td></tr><tr><td>Sir Dragonet</td><td>33/1</td><td>+3300</td></tr><tr><td>Sir Ron Priestley</td><td>33/1</td><td>+3300</td></tr><tr><td>Il Parodiso</td><td>50/1</td><td>+5000</td></tr><tr><td>Nayef Road</td><td>66/1</td><td>+6600</td></tr><tr><td>Mekong</td><td>66/1</td><td>+6600</td></tr>            </tbody>
        </table>
    </div>
    {literal}
    <!--
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    -->    
        {/literal}
    