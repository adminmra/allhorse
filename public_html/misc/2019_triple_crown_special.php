	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Triple Crown Specials">
   <caption>Horses - Triple Crown Specials</caption>
   <tfoot>
      <tr>
         <td class="dateUpdated center" colspan="3">
            <em id='updateemp'>Updated {php} $tdate = date("M d, Y H:00", time() - 7200); echo $tdate . " EST"; {/php} .</em>
            <!-- br>All odds are fixed odds prices. -->
         </td>
      </tr>
   </tfoot>
   <!--<tr>
      <th colspan="3" class="center">
      Horses - Triple Crown Specials  - May 04					</th>
      </tr> -->
   <thead>
      <tr>
         <th colspan="3" class="center">Country House Wins The Triple Crown?</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <th>
            <!--Team-->
         </th>
         <th>Fractional</th>
         <th>American</th>
      </tr>
      <tr>
         <td>Yes</td>
         <td>20/1</td>
         <td>+2000</td>
      </tr>
      <tr>
         <td>No</td>
         <td>1/100</td>
         <td>-10000</td>
      </tr>
   
   </tbody>
</table>
	</div>
    {*{literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}*}
	