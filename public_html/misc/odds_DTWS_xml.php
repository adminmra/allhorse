    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="* Tv Shows - Dancing With The Stars">
            <caption>* Tv Shows - Dancing With The Stars</caption>
            <tbody>
                <tr>
                    <th colspan="3" class="center">
                    * Tv Shows - Dancing With The Stars  - Sep 14                    </th>
            </tr>
                <tr>
                    <th colspan="3" class="center">
                    All Bets Have Action                    </th>
            </tr>
    <tr><th colspan="3" class="center">Tv Shows - Dancing With The Stars - Odds To Win</th></tr><tr><th>Team</th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Alek Skarlatos</td><td>+400</td><td>4/1</td></tr><tr><td>Alexa Penavega</td><td>+850</td><td>17/2</td></tr><tr><td>Andy Grammer</td><td>+1000</td><td>10/1</td></tr><tr><td>Bindi Irwin</td><td>+225</td><td>9/4</td></tr><tr><td>Carlos Penavega</td><td>+1000</td><td>10/1</td></tr><tr><td>Chaka Khan</td><td>+1500</td><td>15/1</td></tr><tr><td>Gary Busey</td><td>+1800</td><td>18/1</td></tr><tr><td>Hayes Grier</td><td>+600</td><td>6/1</td></tr><tr><td>Kim Zolciak</td><td>+450</td><td>9/2</td></tr><tr><td>Nick Carter</td><td>+275</td><td>11/4</td></tr><tr><td>Paula Deen</td><td>+2200</td><td>22/1</td></tr><tr><td>Tamar Braxton</td><td>+350</td><td>7/2</td></tr><tr><td>Victor Espinoza</td><td>+1600</td><td>16/1</td></tr>                    <tr>
                        <td class="dateUpdated center" colspan="5">
                            <em id='updateemp'>Updated September 8, 2015 07:30:20.</em>   BUSR . <!-- br>All odds are fixed odds prices. -->
                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
    