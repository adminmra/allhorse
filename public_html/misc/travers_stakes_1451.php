	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Travers Stakes Odds"
	  }
	</script>
	{/literal} 
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Travers Stakes">
            <caption>Horses - Travers Stakes</caption>
            <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="3">
                  <em id='updateemp'>  - Updated July 2, 2020 02:30:39 </em> <!-- Bet US Racing - Official
                  <a href="//www.usracing.com/odds/us-presidential-election">US Presidential Election Odds</a>. --> <br />
                  <!-- <a href="//www.usracing.com/odds/us-presidential-election">US Presidential Odds</a>, --> All odds are fixed odds prices.
                </td>
              </tr>
            </tfoot>
            <tbody>
               <!-- <tr>
                    <th colspan="3" class="center">
                                        </th>
            </tr> -->
    <tr><th></th><th>Fractional</th><th>American</th></tr><tr><td>Tacitus</td><td>3/1</td><td>+300</td></tr><tr><td>Code Of Honor</td><td>11/4</td><td>+275</td></tr><tr><td>Mucho Gusto</td><td>5/1</td><td>+500</td></tr><tr><td>Highest Honors</td><td>8/1</td><td>+800</td></tr><tr><td>Tax</td><td>13/2</td><td>+650</td></tr><tr><td>Endorsed</td><td>16/1</td><td>+1600</td></tr><tr><td>Global Campaign</td><td>12/1</td><td>+1200</td></tr><tr><td>Owendale</td><td>7/1</td><td>+700</td></tr><tr><td>Looking At Bikinis</td><td>16/1</td><td>+1600</td></tr><tr><td>Chess Chief</td><td>50/1</td><td>+5000</td></tr><tr><td>Everfast</td><td>25/1</td><td>+2500</td></tr><tr><td>Rowayton</td><td>25/1</td><td>+2500</td></tr><tr><td>Spinoff</td><td>20/1</td><td>+2000</td></tr><tr><td>Laughing Fox</td><td>33/1</td><td>+3300</td></tr><tr><td>Scars Are Cool</td><td>40/1</td><td>+4000</td></tr>            </tbody>
        </table>
    </div>
    {literal}
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
        {/literal}
    