	<div class="table-responsive">
		<table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="* American Pharoah - Specials">
			<caption>* American Pharoah - Specials</caption>
			<tbody>
				<tr>
					<th colspan="3" class="center">
					* American Pharoah - Specials  - Oct 30					</th>
			</tr>
				<tr>
					<th colspan="3" class="center">
					Will The Horse Win The Grand Slam By Winning<br />The Breeders Cup Classic? (must Start For Action)					</th>
			</tr>
	<tr><th colspan="3" class="center">Yes</th></tr><tr><th>Team</th><th>American Odds</th><th>Fractional Odds</th></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>American Pharoah Wins Yes</td><td itemprop='Scratch'>EV</td><td itemprop='offers'>EV</td></tr><tr><th colspan="3" class="center">No</th></tr><tr itemscope='' itemtype='http://schema.org/Event'><td itemprop='competitor'>American Pharoah Wins No</td><td itemprop='Scratch'>-140</td><td itemprop='offers'>5/7</td></tr>					<tr>
						<td class="dateUpdated center" colspan="5">
							<em id='updateemp'>Updated October 30, 2015 05:00:16.</em>   BUSR - Official <a href="http://www.usracing.com/bet-on/american-pharoah">American Pharoah Odds</a>. <!-- br>All odds are fixed odds prices. -->
						</td>
					</tr>
			</tbody>
		</table>
	</div>
	