    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Futures">
           <!-- <caption></caption> -->
            <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="3">
                  <em id='updateemp'>  - Updated June 22, 2018 09:16:42 </em> <!-- br /> All odds are fixed odds prices. -->
                </td>
              </tr>
            </tfoot>
            <tbody>
    			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Futures  - Jun 21					</th>
			</tr> -->
    <caption>2018 Ascot Gold Cup - Odds To Win</caption><tr><th></th><th>Fractional</th><th>American</th></tr><tr><td>Order Of St George</td><td>7/5</td><td>+140</td></tr><tr><td>Stradivarius</td><td>8/5</td><td>+160</td></tr><tr><td>Vazirabad</td><td>19/5</td><td>+380</td></tr><tr><td>Torcedor</td><td>7/1</td><td>+700</td></tr><tr><td>Desert Skyline</td><td>9/1</td><td>+900</td></tr><tr><td>Max Dynamite</td><td>20/1</td><td>+2000</td></tr><tr><td>Sheikhzayedroad</td><td>18/1</td><td>+1800</td></tr><tr><td>Mount Moriah</td><td>33/1</td><td>+3300</td></tr><tr><td>Scotland</td><td>55/1</td><td>+5500</td></tr>			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Futures  - Jun 23					</th>
			</tr> -->
    			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Futures  - Oct 07					</th>
			</tr> -->
    			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr> -->
    			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Futures  - Apr 13					</th>
			</tr> -->
                </tbody>
        </table>
    </div>
    {*literal}
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
        {/literal*}
    