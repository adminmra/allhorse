{literal}
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Travers Stakes Odds"
}
</script>
{/literal}
<h2>2021 Travers Stakes Odds</h2>
<div id="no-more-tables">
    <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" summary="Horses - Travers Stakes">
        <caption class="hide-sm">Horses - Travers Stakes</caption>
        <tbody>
    <tr class="head_title hide-sm"> <th>Post</th> 
  <th>Horse</th> <th>Trainer</th> <th>Jockey</th> 
  <th>Odds</th> 
    </tr>
	      <tr>
              <td data-title="Post">1</td>
              <td data-title="Horse">Midnight Bourbon</td>
              <td data-title="Trainer">Steve Asmussen</td>
              <td data-title="Jockey">Ricardo Santana Jr</td>
              <td data-title="Odds">9-2</td>
          </tr>
          <tr>
              <td data-title="Post">2</td>
              <td data-title="Horse">Essential Quality</td>
              <td data-title="Trainer">Brad Cox</td>
              <td data-title="Jockey">Luis Saez</td>
              <td data-title="Odds">4-5</td>
          </tr>
          <tr>
              <td data-title="Post">3</td>
              <td data-title="Horse">Keepmeinmind</td>
              <td data-title="Trainer">Robertino Diodoro</td>
              <td data-title="Jockey">Joel Rosario</td>
              <td data-title="Odds">6-1</td>
          </tr>

          <tr>
              <td data-title="Post">4</td>
              <td data-title="Horse">Dynamic One</td>
              <td data-title="Trainer">Todd Pletcher</td>
              <td data-title="Jockey">Irad Ortiz Jr</td>
              <td data-title="Odds">6-1</td>
          </tr>
          <tr>
              <td data-title="Post">5</td>
              <td data-title="Horse">Miles D</td>
              <td data-title="Trainer">Chad Brown</td>
              <td data-title="Jockey">Flavien Prat</td>
              <td data-title="Odds">12-1</td>
          </tr> 		  
          <tr>
              <td data-title="Post">6</td>
              <td data-title="Horse">Masqueparade</td>
              <td data-title="Trainer">Albert M.Stall Jr</td>
              <td data-title="Jockey">Miguel Mena </td>
              <td data-title="Odds">8-1</td>
          </tr>
          <tr>
              <td data-title="Post">7</td>
              <td data-title="Horse">King Fury</td>
              <td data-title="Trainer">Kenneth McPeek</td>
              <td data-title="Jockey">Jose Ortiz</td>
              <td data-title="Odds">15-1</td>
          </tr>
     </tbody>
      <tfoot class="hide-sm">
          <tr>
              <td class="dateUpdated center" colspan="5">
                  <em id='updateemp'>Updated {php} $tdate = date("M d, Y H:00", time() - 7200); echo $tdate . " EST"; {/php} .</em>
              </td>
          </tr>
      </tfoot>
    </table>
</div>
{literal}
<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
<script src="//www.usracing.com/assets/js/sorttable_post.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
{/literal}