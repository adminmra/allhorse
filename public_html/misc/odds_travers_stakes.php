    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Travers Stakes">
           <!-- <caption></caption> -->
            <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="3">
                  <em id='updateemp'>  - Updated August 28, 2018 08:17:13 </em> <!-- br /> All odds are fixed odds prices. -->
                </td>
              </tr>
            </tfoot>
            <tbody>
    			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr> -->
    <caption>Travers Stakes - To Win</caption><tr><th></th><th>Fractional</th><th>American</th></tr><tr><td>Good Magic</td><td>3/2</td><td>+150</td></tr><tr><td>Hofburg</td><td>9/5</td><td>+180</td></tr><tr><td>Wonder Gadot</td><td>9/2</td><td>+450</td></tr><tr><td>Tenfold</td><td>5/1</td><td>+500</td></tr><tr><td>Gronkowski</td><td>5/1</td><td>+500</td></tr><tr><td>Firenze Fire</td><td>11/2</td><td>+550</td></tr><tr><td>Catholic Boy</td><td>13/2</td><td>+650</td></tr><tr><td>My Boy Jack</td><td>10/1</td><td>+1000</td></tr><tr><td>Vino Rosso</td><td>10/1</td><td>+1000</td></tr><tr><td>Bravazo</td><td>11/1</td><td>+1100</td></tr><tr><td>Mckinzie</td><td>11/1</td><td>+1100</td></tr><tr><td>Lone Sailor</td><td>20/1</td><td>+2000</td></tr>            </tbody>
        </table>
    </div>
    {literal}
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
        {/literal}
    