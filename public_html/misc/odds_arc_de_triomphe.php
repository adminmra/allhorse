    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Futures">
           <!-- <caption></caption> -->
            <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="3">
                  <em id='updateemp'>  - Updated September 27, 2018 08:38:07 </em> <!-- br /> All odds are fixed odds prices. -->
                </td>
              </tr>
            </tfoot>
            <tbody>
    			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not Or Wagers Have Action					</th>
			</tr> -->
    <caption>Prix De L`arc De Triomphe 2018 - Odds To Win</caption><tr><th></th><th>Fractional</th><th>American</th></tr><tr><td>Enable</td><td>3/1</td><td>+300</td></tr><tr><td>Cracksman</td><td>5/1</td><td>+500</td></tr><tr><td>Masar</td><td>8/1</td><td>+800</td></tr><tr><td>Study Of Man</td><td>10/1</td><td>+1000</td></tr><tr><td>Poets Word</td><td>10/1</td><td>+1000</td></tr><tr><td>Crystal Ocean</td><td>10/1</td><td>+1000</td></tr><tr><td>Saxon Warrior</td><td>11/1</td><td>+1100</td></tr><tr><td>Forever Together</td><td>13/1</td><td>+1300</td></tr><tr><td>Laurens</td><td>16/1</td><td>+1600</td></tr><tr><td>Lah Ti Dar</td><td>20/1</td><td>+2000</td></tr><tr><td>Defoe</td><td>20/1</td><td>+2000</td></tr><tr><td>Hydrangea</td><td>20/1</td><td>+2000</td></tr><tr><td>Soul Stirring</td><td>20/1</td><td>+2000</td></tr><tr><td>Brametot</td><td>23/1</td><td>+2300</td></tr><tr><td>Shahnaza</td><td>28/1</td><td>+2800</td></tr><tr><td>Rhododendron</td><td>28/1</td><td>+2800</td></tr><tr><td>Order Of St George</td><td>28/1</td><td>+2800</td></tr><tr><td>Happily</td><td>28/1</td><td>+2800</td></tr><tr><td>Elarqam</td><td>28/1</td><td>+2800</td></tr><tr><td>Kiseki</td><td>28/1</td><td>+2800</td></tr><tr><td>Cloth Of Stars</td><td>28/1</td><td>+2800</td></tr><tr><td>Amedeo Modigliani</td><td>33/1</td><td>+3300</td></tr><tr><td>Kew Gardens</td><td>33/1</td><td>+3300</td></tr><tr><td>The Pentagon</td><td>40/1</td><td>+4000</td></tr><tr><td>Tocco Damore</td><td>40/1</td><td>+4000</td></tr><tr><td>Zarkamiya</td><td>40/1</td><td>+4000</td></tr><tr><td>Nelson</td><td>55/1</td><td>+5500</td></tr>			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Futures  - Apr 13					</th>
			</tr> -->
    			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr> -->
                </tbody>
        </table>
    </div>
    {literal}
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
        {/literal}
    