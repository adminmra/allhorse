    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - 2017 Prix De L'arc De Triomphe">
            <caption>Horses - 2017 Prix De L'arc De Triomphe</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated October 1, 2017 00:00:41.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr>
            </tfoot>
            <tbody>
            <!--
    <tr>
                    <th colspan="3" class="center">
                    Horses - 2017 Prix De L'arc De Triomphe  - Sep 30                    </th>
            </tr>
-->
            <!--
    <tr>
                    <th colspan="3" class="center">
                    Run Or Not All Wagers Have Action                    </th>
            </tr>
-->
    <tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Enable</td><td>2/3</td><td>-150</td></tr><tr><td>Ulysses</td><td>11/2</td><td>+550</td></tr><tr><td>Highland Reel</td><td>8/1</td><td>+800</td></tr><tr><td>Order Of St George</td><td>8/1</td><td>+800</td></tr><tr><td>Winter</td><td>8/1</td><td>+800</td></tr><tr><td>Brametot</td><td>10/1</td><td>+1000</td></tr><tr><td>Dschingis Secret</td><td>11/1</td><td>+1100</td></tr><tr><td>Zarak</td><td>11/1</td><td>+1100</td></tr><tr><td>Capri</td><td>11/1</td><td>+1100</td></tr><tr><td>Satono Diamond</td><td>13/1</td><td>+1300</td></tr><tr><td>Cloth Of Stars</td><td>20/1</td><td>+2000</td></tr><tr><td>Cliffs Of Moher</td><td>23/1</td><td>+2300</td></tr><tr><td>Idaho</td><td>28/1</td><td>+2800</td></tr><tr><td>Seventh Heaven</td><td>28/1</td><td>+2800</td></tr><tr><td>Plumatic</td><td>33/1</td><td>+3300</td></tr><tr><td>Silverwave</td><td>33/1</td><td>+3300</td></tr><tr><td>Doha Dream</td><td>33/1</td><td>+3300</td></tr><tr><td>One Foot In Heaven</td><td>40/1</td><td>+4000</td></tr><tr><td>Iquitos</td><td>40/1</td><td>+4000</td></tr><tr><td>Left Hand</td><td>40/1</td><td>+4000</td></tr><tr><td>Satono Noblesse</td><td>80/1</td><td>+8000</td></tr>            </tbody>
        </table>
    </div>
    {literal}
        <style>
            table.ordenable th{cursor: pointer}
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sort_table.js"></script>
    {/literal}
    