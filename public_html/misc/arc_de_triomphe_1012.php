	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Prix de l'Arc de Triomphe Odds"
	  }
	</script>
	{/literal} 
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Prix De L'arc De Triomphe">
            <caption>Horses - Prix De L'arc De Triomphe</caption>
            <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="3">
                  <em id='updateemp'>  - Updated November 16, 2021 19:00:10 </em> <!-- Bet US Racing - Official
                  <a href="//www.usracing.com/odds/us-presidential-election">US Presidential Election Odds</a>. -->
                  <!-- <a href="//www.usracing.com/odds/us-presidential-election">US Presidential Odds</a>, -->
                </td>
              </tr>
            </tfoot>
            <tbody>
               <!-- <tr>
                    <th colspan="3" class="center">
                                        </th>
            </tr> -->
    <tr><th></th><th>Fractional</th><th>American</th></tr><tr><td>Snowfall</td><td>12/5</td><td>+240</td></tr><tr><td>Adayar</td><td>4/1</td><td>+400</td></tr><tr><td>Tarnawa</td><td>6/1</td><td>+600</td></tr><tr><td>St Marks Basilica</td><td>6/1</td><td>+600</td></tr><tr><td>Hurricane Lane</td><td>7/1</td><td>+700</td></tr><tr><td>Mishriff</td><td>10/1</td><td>+1000</td></tr><tr><td>Love</td><td>10/1</td><td>+1000</td></tr><tr><td>Wonderful Tonight</td><td>12/1</td><td>+1200</td></tr><tr><td>Lei Papale</td><td>12/1</td><td>+1200</td></tr><tr><td>Chrono Genesis</td><td>14/1</td><td>+1400</td></tr><tr><td>In Swoop</td><td>16/1</td><td>+1600</td></tr><tr><td>Raabihah</td><td>33/1</td><td>+3300</td></tr><tr><td>High Definition</td><td>33/1</td><td>+3300</td></tr><tr><td>Santa Barbara</td><td>33/1</td><td>+3300</td></tr><tr><td>Mogul</td><td>33/1</td><td>+3300</td></tr><tr><td>Japan</td><td>40/1</td><td>+4000</td></tr><tr><td>Daring Tact</td><td>40/1</td><td>+4000</td></tr><tr><td>Serpentine</td><td>40/1</td><td>+4000</td></tr><tr><td>Gold Trip</td><td>40/1</td><td>+4000</td></tr><tr><td>Verry Elleegant</td><td>50/1</td><td>+5000</td></tr>            </tbody>
        </table>
    </div>
    {literal}
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
        {/literal}
    