	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Melbourne Cup Odds"
	  }
	</script>
	{/literal} 
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Melbourne Cup - To Win">
            <caption>Horses - Melbourne Cup - To Win</caption>
                                <tfoot>
                    <tr>
                        <td class="dateUpdated center" colspan="5">
                            <em id='updateemp'>  - Updated January 27, 2021 16:03:03 </em> <!--BUSR - Official <a href="https://www.usracing.com/melbourne-cup">Melbourne Cup Odds</a>. -->  <br><!--All odds are fixed odds prices.-->
                        </td>
                    </tr>
                    </tfoot>
            <tbody>
                   <tr><td>Tiger Moth</td><td>+500</td><td>5/1</td></tr><tr><td>Sir Dragonet</td><td>+650</td><td>13/2</td></tr><tr><td>Surprise Baby</td><td>+900</td><td>9/1</td></tr><tr><td>Anthony Van Dyck</td><td>+1000</td><td>10/1</td></tr><tr><td>Verry Elleegant</td><td>+1000</td><td>10/1</td></tr><tr><td>Russian Camelot</td><td>+1200</td><td>12/1</td></tr><tr><td>Prince Of Arran</td><td>+1200</td><td>12/1</td></tr><tr><td>Finche</td><td>+1600</td><td>16/1</td></tr><tr><td>Master Of Reality</td><td>+2000</td><td>20/1</td></tr><tr><td>Warning</td><td>+2500</td><td>25/1</td></tr><tr><td>Ashrun</td><td>+3300</td><td>33/1</td></tr><tr><td>Persan</td><td>+3300</td><td>33/1</td></tr><tr><td>Twilight Payment</td><td>+3300</td><td>33/1</td></tr><tr><td>Steel Prince</td><td>+3300</td><td>33/1</td></tr><tr><td>Avilius</td><td>+3300</td><td>33/1</td></tr><tr><td>Miami Bound</td><td>+3300</td><td>33/1</td></tr><tr><td>True Self</td><td>+3300</td><td>33/1</td></tr><tr><td>Stratum Albion</td><td>+3300</td><td>33/1</td></tr><tr><td>King Of Leogrance</td><td>+5000</td><td>50/1</td></tr><tr><td>The Chosen One</td><td>+5000</td><td>50/1</td></tr><tr><td>Oceanex</td><td>+5000</td><td>50/1</td></tr><tr><td>Vow And Declare</td><td>+5000</td><td>50/1</td></tr><tr><td>Nickajack Cave</td><td>+5000</td><td>50/1</td></tr><tr><td>Le Don De Vie</td><td>+6600</td><td>66/1</td></tr><tr><td>Mustajeer</td><td>+6600</td><td>66/1</td></tr><tr><td>Chapada</td><td>+6600</td><td>66/1</td></tr><tr><td>Admire Robson</td><td>+6600</td><td>66/1</td></tr><tr><td>San Huberto</td><td>+10000</td><td>100/1</td></tr><tr><td>Pondus</td><td>+10000</td><td>100/1</td></tr><tr><td>Etah James</td><td>+10000</td><td>100/1</td></tr><tr><td>Future Score</td><td>+10000</td><td>100/1</td></tr><tr><td>Schabau</td><td>+10000</td><td>100/1</td></tr><tr><td>Skyward</td><td>+10000</td><td>100/1</td></tr><tr><td>Dashing Willoughby</td><td>+10000</td><td>100/1</td></tr><tr><td>Sound</td><td>+10000</td><td>100/1</td></tr><tr><td>Lord Belvedere</td><td>+10000</td><td>100/1</td></tr><tr><td>Zebrowski</td><td>+10000</td><td>100/1</td></tr><tr><td>Saracen Knight</td><td>+20000</td><td>200/1</td></tr><tr><td>Platinum Invador</td><td>+20000</td><td>200/1</td></tr><tr><td>Carif</td><td>+20000</td><td>200/1</td></tr><tr><td>Attorney</td><td>+30000</td><td>300/1</td></tr><tr><td>Azuro</td><td>+30000</td><td>300/1</td></tr><tr><td>Gallic Chieftain</td><td>+30000</td><td>300/1</td></tr><tr><td>Hush Writer</td><td>+30000</td><td>300/1</td></tr><tr><td>Haky</td><td>+30000</td><td>300/1</td></tr>            </tbody>
        </table>
    </div>

{literal}
    <style>
       table.ordenable th{cursor: pointer}
    </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
{/literal}


    