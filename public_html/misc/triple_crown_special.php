	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0"  summary="Horses - Triple Crown Specials">
			<caption>Horses - Triple Crown Specials</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated September 21, 2020 08:00:12 </em>
                    </td>
                </tr> 
            </tfoot>
			
			<!--<tr>
					<th colspan="3" class="center">
					Horses - Triple Crown Specials  - Sep 01					</th>
			</tr> -->
	<thead><tr><th colspan="3" class="center">Will There Be A Triple Crown Winner In 2020?</th></tr></thead><tbody><tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>11/4</td><td>+275</td></tr><tr><td>No</td><td>2/7</td><td>-350</td></tr>			</tbody>
		</table>
	</div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
	