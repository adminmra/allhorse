	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Commonwealth Cup Odds"
	  }
	</script>
	{/literal} 
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Commonwealth Cup">
            <caption>Horses - Commonwealth Cup</caption>
            <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="3">
                  <em id='updateemp'>  - Updated July 2, 2020 </em>
                </td>
              </tr>
            </tfoot>
            <tbody>
    
    <tr><th></th><th>Fractional</th><th>American</th></tr><tr><td>Earthlight</td><td>8/1</td><td>+800</td></tr><tr><td>Pinatubo</td><td>10/1</td><td>+1000</td></tr><tr><td>Pierre Lapin</td><td>10/1</td><td>+1000</td></tr><tr><td>A'ali</td><td>12/1</td><td>+1200</td></tr><tr><td>Siskin</td><td>12/1</td><td>+1200</td></tr><tr><td>Millisle</td><td>14/1</td><td>+1400</td></tr><tr><td>Daahyeh</td><td>16/1</td><td>+1600</td></tr><tr><td>Raffle Prize</td><td>16/1</td><td>+1600</td></tr><tr><td>Royal Lytham</td><td>16/1</td><td>+1600</td></tr><tr><td>Sunday Sovereign</td><td>16/1</td><td>+1600</td></tr><tr><td>Threat</td><td>16/1</td><td>+1600</td></tr><tr><td>Mums Tipple</td><td>16/1</td><td>+1600</td></tr><tr><td>Golden Horde</td><td>16/1</td><td>+1600</td></tr><tr><td>Arizona</td><td>20/1</td><td>+2000</td></tr><tr><td>Ventura Rebel</td><td>20/1</td><td>+2000</td></tr><tr><td>Wichita</td><td>20/1</td><td>+2000</td></tr><tr><td>Kimari</td><td>25/1</td><td>+2500</td></tr><tr><td>Monarch Of Egypt</td><td>25/1</td><td>+2500</td></tr><tr><td>Alligator Alley</td><td>33/1</td><td>+3300</td></tr><tr><td>Lampang</td><td>33/1</td><td>+3300</td></tr><tr><td>Art Power</td><td>33/1</td><td>+3300</td></tr>            </tbody>
        </table>
    </div>
    {literal}
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
        {/literal}
    