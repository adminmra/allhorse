	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Prix de l'Arc de Triomphe Odds"
	  }
	</script>
	{/literal}     <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" summary="Horses - Prix De L'arc De Triomphe">
            <caption>Horses - Prix De L'arc De Triomphe</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 27, 2020.</em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
    <tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td data-title=' '>Enable</td><td data-title='Fractional'>5/1</td><td data-title='American'>+500</td></tr><tr><td data-title=' '>Almond Eye</td><td data-title='Fractional'>8/1</td><td data-title='American'>+800</td></tr><tr><td data-title=' '>Sottsass</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Logician</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Japan</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Pinatubo</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Search For A Song</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Quadrilateral</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Stradivarius</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr>            </tbody>
        </table>
    </div>
	{literal}
        <style>
            table.ordenable th{cursor: pointer}
			table.ordenable td{ word-wrap: break-word !important; word-break: break-all !important;}
			table.ordenable{ max-width: 100% !important; }
        </style>
        <script src="/assets/js/jquery.sortElements.js"></script>
        <script src="/assets/js/sorttable.js"></script>
	{/literal}
    
	 