    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Triple Crown Specials">
            <caption>Horses - Triple Crown Specials</caption>
            <tbody>
                <tr>
                    <th colspan="3" class="center">
                    Horses - Triple Crown Specials  - May 05                    </th>
            </tr>
    <tr><th colspan="3" class="center">2018 To Win Triple Crown</th></tr><tr><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Justify</td><td>+700</td><td>7/1</td></tr><tr><td>Mendelssohn</td><td>+1800</td><td>18/1</td></tr><tr><td>Good Magic</td><td>+1800</td><td>18/1</td></tr><tr><td>Audible</td><td>+2300</td><td>23/1</td></tr><tr><td>Magnum Moon</td><td>+2500</td><td>25/1</td></tr><tr><td>Bolt Doro</td><td>+2500</td><td>25/1</td></tr><tr><td>My Boy Jack</td><td>+4000</td><td>40/1</td></tr><tr><td>Vino Rosso</td><td>+5500</td><td>55/1</td></tr><tr><td>Enticed</td><td>+8000</td><td>80/1</td></tr><tr><td>Noble Indy</td><td>+12000</td><td>120/1</td></tr><tr><td>Solomini</td><td>+12000</td><td>120/1</td></tr><tr><td>Blended Citizen</td><td>+12000</td><td>120/1</td></tr><tr><td>Promises Fulfilled</td><td>+12000</td><td>120/1</td></tr><tr><td>Flameaway</td><td>+12000</td><td>120/1</td></tr><tr><td>Hofburg</td><td>+14000</td><td>140/1</td></tr><tr><td>Free Drop Billy</td><td>+14000</td><td>140/1</td></tr><tr><td>Instilled Regard</td><td>+14000</td><td>140/1</td></tr><tr><td>Combatant</td><td>+18000</td><td>180/1</td></tr><tr><td>Lone Sailor</td><td>+20000</td><td>200/1</td></tr><tr><td>Bravazo</td><td>+30000</td><td>300/1</td></tr><tr><td>Firenze Fire</td><td>+35000</td><td>350/1</td></tr><tr><td>No Triple Crown Winner</td><td>-1300</td><td>1/13</td></tr>                    <tr>
                        <td class="dateUpdated center" colspan="5">
                            <em id='updateemp'>  - Updated May 10, 2018 12:34:59 </em>   BUSR - Official <a href="https://www.usracing.com/triple-crown">Triple Crown Betting</a>. <!-- br>All odds are fixed odds prices. -->
                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
    