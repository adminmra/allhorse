	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Commonwealth Cup Odds"
	  }
	</script>
	{/literal}     <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" summary="Horses - Commonwealth Cup">
            <caption>Horses - Commonwealth Cup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 27, 2020.</em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
    <tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td data-title=' '>Siskin</td><td data-title='Fractional'>12/1</td><td data-title='American'>+1200</td></tr><tr><td data-title=' '>A`ali</td><td data-title='Fractional'>12/1</td><td data-title='American'>+1200</td></tr><tr><td data-title=' '>Raffle Prize</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Sunday Sovereign</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Threat</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Arizona</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>Daahyeh</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Kimari</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Monarch Of Egypt</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Royal Lytham</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Ventura Rebel</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>Pinatubo</td><td data-title='Fractional'>10/1</td><td data-title='American'>+1000</td></tr>            </tbody>
        </table>
    </div>
	{literal}
        <style>
            table.ordenable th{cursor: pointer}
			table.ordenable td{ word-wrap: break-word !important; word-break: break-all !important;}
			table.ordenable{ max-width: 100% !important; }
        </style>
        <script src="/assets/js/jquery.sortElements.js"></script>
        <script src="/assets/js/sorttable.js"></script>
	{/literal}
    
	 