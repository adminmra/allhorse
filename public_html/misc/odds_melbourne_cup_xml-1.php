    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Melbourne Cup - To Win">
            <caption>Horses - Melbourne Cup - To Win</caption>
                                <tfoot>
                    <tr>
                        <td class="dateUpdated center" colspan="5">
                            <em id='updateemp'>  - Updated May 10, 2018 11:30:01 </em>   BUSR - Official <a href="https://www.usracing.com/melbourne-cup">Melbourne Cup Odds</a>. <!-- br>All odds are fixed odds prices. -->
                        </td>
                    </tr>
                    </tfoot>
            <tbody>
                <tr>
                    <th colspan="3" class="center">
                    Horses - Melbourne Cup - To Win  - Nov 06                    </th>
            </tr>
                <tr>
                    <th colspan="3" class="center">
                    Run Or Not All Wagers Have Action                    </th>
            </tr>
    <tr><th colspan="3" class="center">2017 Melbourne Cup - To Win</th></tr><tr><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Almandin</td><td>+450</td><td>9/2</td></tr><tr><td>Marmelo</td><td>+580</td><td>29/5</td></tr><tr><td>Humidor</td><td>+650</td><td>13/2</td></tr><tr><td>Johannes Vermeer</td><td>+750</td><td>15/2</td></tr><tr><td>Wall Of Fire</td><td>+800</td><td>8/1</td></tr><tr><td>Red Cardinal</td><td>+1100</td><td>11/1</td></tr><tr><td>Rekindling</td><td>+800</td><td>8/1</td></tr><tr><td>Thomas Hobson</td><td>+1000</td><td>10/1</td></tr><tr><td>Max Dynamite</td><td>+1000</td><td>10/1</td></tr><tr><td>Nakeeta</td><td>+1600</td><td>16/1</td></tr><tr><td>Big Duke</td><td>+1300</td><td>13/1</td></tr><tr><td>Tiberian</td><td>+1600</td><td>16/1</td></tr><tr><td>Amelies Star</td><td>+1300</td><td>13/1</td></tr><tr><td>Hartnell</td><td>+2000</td><td>20/1</td></tr><tr><td>Ventura Storm</td><td>+2800</td><td>28/1</td></tr><tr><td>Boom Time</td><td>+2000</td><td>20/1</td></tr><tr><td>Single Gaze</td><td>+2800</td><td>28/1</td></tr><tr><td>Us Army Ranger</td><td>+3300</td><td>33/1</td></tr><tr><td>Wicklow Brave</td><td>+2800</td><td>28/1</td></tr><tr><td>Libran</td><td>+2800</td><td>28/1</td></tr><tr><td>Bondi Beach</td><td>+5500</td><td>55/1</td></tr><tr><td>Gallante</td><td>+5500</td><td>55/1</td></tr><tr><td>Cismontane</td><td>+4000</td><td>40/1</td></tr>            </tbody>
        </table>
    </div>

{literal}
    <style>
       table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sort_table.js"></script>
{/literal}


    