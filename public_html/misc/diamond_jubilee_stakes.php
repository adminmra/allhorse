    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Futures">
           <!-- <caption></caption> -->
            <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="3">
                  <em id='updateemp'>  - Updated June 27, 2018 04:42:56 </em> <!-- br /> All odds are fixed odds prices. -->
                </td>
              </tr>
            </tfoot>
            <tbody>
    			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Futures  - Jun 23					</th>
			</tr> -->
    <caption>Diamond Jubilee Stakes - Odds To Win</caption><tr><th></th><th>Fractional</th><th>American</th></tr><tr><td>Harry Angel</td><td>9/4</td><td>+225</td></tr><tr><td>Merchant Navy</td><td>7/2</td><td>+350</td></tr><tr><td>Redkirk Warrior</td><td>7/2</td><td>+350</td></tr><tr><td>The Tin Man</td><td>13/2</td><td>+650</td></tr><tr><td>Bound For Nowhere</td><td>8/1</td><td>+800</td></tr><tr><td>Librisa Breeze</td><td>8/1</td><td>+800</td></tr><tr><td>City Light</td><td>10/1</td><td>+1000</td></tr><tr><td>D’bai</td><td>23/1</td><td>+2300</td></tr><tr><td>Projection</td><td>28/1</td><td>+2800</td></tr><tr><td>Sir Dancealot</td><td>28/1</td><td>+2800</td></tr><tr><td>Spirit Of Valor</td><td>28/1</td><td>+2800</td></tr><tr><td>Intelligence Cross</td><td>80/1</td><td>+8000</td></tr>			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Futures  - Oct 07					</th>
			</tr> -->
    			<!--	<tr>
					<th colspan="3" class="center">
					Run Or Not All Wagers Have Action					</th>
			</tr> -->
    			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Futures  - Apr 13					</th>
			</tr> -->
                </tbody>
        </table>
    </div>
    {*literal}
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
        {/literal*}
    