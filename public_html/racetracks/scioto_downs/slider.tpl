<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/racetracks/scioto-downs/scioto-downs.jpg" alt="Scitoto downs" />
    <figure class="rsCaption" data-move-effect="left" data-speed="200">The changing sign at Scioto Downs</figure>
   
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/scioto-downs/scioto-downs-off-track-betting.jpg" alt="Scioto Downs Off Track Betting"  />
    <figure class="rsCaption">The view from the start car</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/scioto-downs/scioto-downs-betting.jpg" alt="Scioto Downs Betting" />
    <figure class="rsCaption">Hard at work in the stables</figure>
   </div>
   
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/scioto-downs/scioto-downs-live-racing.jpg" alt="Scioto Downs Live Racing" />
    <figure class="rsCaption"></figure>
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/scioto-downs/scioto-downs-horse-betting.jpg" alt="Scioto Downd Horse Betting" />
    <figure class="rsCaption">Harness Racing at it's best!</figure>
   </div>

      
   
   
   
   
   
   
  </div>
 </div>
