<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/racetracks/mountaineer-park/mountaineer-park-off-track-betting.jpg" alt="Mountaineer Park  Off Track Betting - OTB" />
    <figure class="rsCaption" data-move-effect="left" data-speed="200"> </figure>
   
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/mountaineer-park/mountaineer-park-horse-betting.jpg" alt="Mountaineer Park  Horse Betting" />
    <figure class="rsCaption"> </figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/mountaineer-park/bet-on-mountaineer-park.jpg" alt="Bet on Mountaineer Park"  />
    <figure class="rsCaption"> </figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/mountaineer-park/mountaineer-park-horse-racing.jpg" alt="Mountaineer Park Horse Racing" />
    <figure class="rsCaption"> </figure>
   </div>

   
  </div>
 </div>
