<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/racetracks/saratoga/saratoga-offtrack-betting.jpg" alt="Saratoga Off Track Betting - OTB" />
    <figure class="rsCaption" data-move-effect="left" data-speed="200">A sunset workout at Saratoga</figure>
   
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/saratoga/saratoga-travers-stakes.jpg" alt="Saratoga Travers Stakes" />
    <figure class="rsCaption">Enjoying the Travers Stakes</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/saratoga/welcome-to-saratoga.jpg" alt="Welcome to Saratoga Race Course"  />
    <figure class="rsCaption">Welcome to Saratoga Race Course</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/saratoga/saratoga-online-horse-betting.jpg" alt="Saratoga Online Horse Betting" />
    <figure class="rsCaption">A misty morning workout session</figure>
   </div>
   
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/saratoga/saratoga-paddock.jpg" alt="Saratoga Racetrack" />
    <figure class="rsCaption">Don't ride and text</figure>
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/saratoga/saratoga-gate-horses.jpg" alt="Saratoga Starting Gate" />
    <figure class="rsCaption">And they are off!</figure>
   </div>

     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/saratoga/saratoga-paddock-fall.jpg" alt="Saratoga Race Course" />
    <figure class="rsCaption">A beautiful fall afternoon</figure>
   </div>
 
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/saratoga/saratoga-race-course-front.jpg" alt="Saratoga Race Course" />
    <figure class="rsCaption">Today is a good day to race...</figure>
   </div>
       <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/saratoga/saratoga-paddock-winter.jpg" alt="The paddock at Saratoga Race Course" />
    <figure class="rsCaption">Brr..</figure>
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/saratoga/saratoga-race-course.jpg" alt="Saratoga Race Course" />
    <figure class="rsCaption">Saratoga Race Course</figure>
   </div>

   

   
   
   
   
   
   
   
  </div>
 </div>
