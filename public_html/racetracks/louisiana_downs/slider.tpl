<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/racetracks/louisiana-downs/louisiana-downs-off-track-betting.jpg" alt="Louisiana Downs  Off Track Betting - OTB" />
    <figure class="rsCaption" data-move-effect="left" data-speed="200" </figure>
   
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/louisiana-downs/louisiana-downs-horse-betting.jpg" alt="Louisiana Downs  Horse Betting" />
    <figure class="rsCaption"> </figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/louisiana-downs/bet-on-louisiana-downs.jpg" alt="Bet on Louisiana Downs "  />
    <figure class="rsCaption"> </figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/louisiana-downs/louisiana-downs-horse-racing.jpg" alt="Louisiana Downs Horse Racing" />
    <figure class="rsCaption"> </figure>
   </div>
   
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/louisiana-downs/louisiana-downs-otb.jpg" alt="Louisiana Downs OTB" />
    <figure class="rsCaption"> </figure>
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/louisiana-downs/louisiana-downs-schedule.jpg" alt="Louisiana Downs Schedule " />
    <figure class="rsCaption"> </figure>
   </div>


   
  </div>
 </div>
