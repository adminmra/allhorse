<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/racetracks/belmont-park/american-pharoah.jpg" alt="American Pharoah" />
    <figure class="rsCaption" data-move-effect="left" data-speed="200">American Pharoah - 2015 Belmont Stakes Winner</figure>
   
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/belmont-park/belmont-park1.jpg" alt="The paddock at Belmont Park"  />
    <figure class="rsCaption">The paddock at Belmont Park</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/belmont-park/belmont-park3.jpg" alt="Belmont Stakes" />
    <figure class="rsCaption">The Belmont Stakes</figure>
   </div>
   
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/belmont-park/belmont-park4.jpg" alt="Belmont Stakes" />
    <figure class="rsCaption">And they're off!</figure>
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/belmont-park/tonalist.jpg" alt="Tonalist" />
    <figure class="rsCaption">2015 Belmont Stakes Winner Tonalist</figure>
   </div>

     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/belmont-park/belmont-park7.jpg" alt="Belmont Park" />
    <figure class="rsCaption"></figure>
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/belmont-park/belmont-park8.jpg" alt="The paddock at Belmont Park" />
    <figure class="rsCaption">The paddock at Belmont Park</figure>
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/belmont-park/belmont-park9.jpg" alt="Belmont Park" />
    <figure class="rsCaption">Today is a good day to race...</figure>
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/belmont-park/belmont-park10.jpg" alt="Belmont Park Arial View" />
    <figure class="rsCaption">Majestic arial view of Belmont Park</figure>
   </div>
  <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/belmont-park/american-pharoah2.jpg" alt="American Pharoah" />
    <figure class="rsCaption">Victor Espinoza, winning the 147th running of the Belmont Stakes, June 6, 2015.</figure>
   </div>
   
    <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/belmont-park/belmont-stakes-trophy.jpg" alt="Belmont Stakes Trophy" />
    <figure class="rsCaption"> The Belmont Stakes Trophy</figure>
   </div>
   
   
   
   
   
   
   
  </div>
 </div>
