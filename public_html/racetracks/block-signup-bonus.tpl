<section class="card">
    <div class="card_wrap">
        <div class="card_half card_content">
            <a href="/promos/cash-bonus-20">
	        {* <img src="/img/index-kd/icon-kentucky-derby-betting.png" alt="Bet on Kentucky Derby" class="card_icon"> *}
          <img class="icon-kentucky-derby-betting card_icon" alt="Bet on Kentucky Derby" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABVAQMAAADUnuB8AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABFJREFUeNpjYBgFo2AUDHIAAAOnAAHnt6kCAAAAAElFTkSuQmCC">
	        </a>
            <h2 class="card_heading">GET UP TO A $500 NEW MEMBER BONUS!</h2>
            <h3 class="card_subheading">
            <p style="margin-bottom: 10px">Get the most out of your initial deposit at <a href="/signup?ref={$ref}" rel= "no follow">BUSR</a> with a new member cash bonus up to $500!</p>
                          			<p>Start winning at <a href="/signup?ref={$ref}" rel="no follow">BUSR</a> now!</p>
                        	<br>
                            <p><a href="/cash-bonus-20"
            class="btn-xlrg fixed_cta">Learn More</a></p>
        </div>
        <div class="card_half card_hide-mobile">
            <a href="/signup?ref={$ref}" rel="no follow">{*change the path of the link*}
                <img src="/img/states/cash-bonus-20.jpg" class="card_img" alt="Horse Racing 2020 Signup Offer">{*change the path the img in src*}
            </a>
        </div>
    </div>
</section>