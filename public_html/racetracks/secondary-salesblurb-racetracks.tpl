<p>Get your Gulfstream Park betting action with <a href="www.betusracing.ag">BUSR</a> directly from your smartphone, tablet or desktop PC with complete ease!<br>
	
Sign up now, and get an 8% <a href="https://www.usracing.com/rebates">rebate</a> on all of your horse wagering activity. Win or lose, you’ll see the cash deposited into your player account the very next day. No exceptions!
<a href="https://www.usracing.com/promos/cash-bonus-10">Activate your new USRacing</a> account today, and get ready to shine!<br>
	
Experience it for yourself, <a href="https://www.betusracing.ag/signup?ref=Gulfstream-Park">apply now</a>!
</p>