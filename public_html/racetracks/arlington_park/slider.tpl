<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/racetracks/arlington-park/arlington-park-off-track-betting.jpg" alt="Arlington Park  Off Track Betting - OTB" />
    <figure class="rsCaption" data-move-effect="left" data-speed="200"> </figure>
   
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/arlington-park/arlington-park-horse-betting.jpg" alt="Arlington Park  Horse Betting" />
    <figure class="rsCaption"> </figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/arlington-park/bet-on-arlington-park.jpg" alt="Bet on Arlington Park"  />
    <figure class="rsCaption"> </figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/arlington-park/arlington-park-horse-racing.jpg" alt="Arlington Park Horse Racing" />
    <figure class="rsCaption"> </figure>
   </div>
   
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/arlington-park/arlington-park-otb.jpg" alt="Arlington Park OTB" />
    <figure class="rsCaption"> </figure>
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/arlington-park/arlington-park-schedule.jpg" alt="Arlington Park Schedule " />
    <figure class="rsCaption"> </figure>
   </div>


   
  </div>
 </div>
