<section class="card">
    <div class="card_wrap">
        <div class="card_half card_content">
           <a href="/rebates">
                <img class="icon-bet-kentucky-derby-jockeys card_icon" alt="Horse Racing 2020 Rebates Offer"
                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkAQMAAABKLAcXAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRQE8AAAV4AAHYKaK4AAAAAElFTkSuQmCC">
              </a>
            <h2 class="card_heading">EARN UP TO AN 8% REBATE ON HORSE RACING WAGERS!</h2>
            <h3 class="kd_subheading">Paid Daily</h3>
            <p style="margin-bottom: 10px">Log into your account and you will find a rebate of up to 8% from the previous days horse wagers. Wagering on Win, Place, Show, or Exotics will earn you a rebate.</p>
            <p>Win now with <a href="/signup?ref={$ref}" rel="nofollow">BUSR!</a></p>
            <br>
            <p><a href="/rebates"
            class="btn-xlrg fixed_cta">Learn More</a></p>
            </div>
        <div class="card_half card_hide-mobile">
            <a href="/signup?ref={$ref}" rel="nofollow">{*change the path of the link*}
                <img src="/img/states/rebates.jpg" class="card_img" alt="Horse Racing 2020 Rebates Offer">{*change the path the img in src*}
            </a>
        </div>
    </div>
</section>