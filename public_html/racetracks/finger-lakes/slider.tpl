<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/racetracks/{$racetrackpath}/{$racetrackpath}-off-track-betting.jpg" alt="{$racetrackname}  Off Track Betting - OTB" />
    <figure class="rsCaption" data-move-effect="left" data-speed="200"> </figure>
   
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/{$racetrackpath}/{$racetrackpath}-horse-betting.jpg" alt="{$racetrackname}  Horse Betting" />
    <figure class="rsCaption"> </figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/{$racetrackpath}/bet-on-{$racetrackpath}.jpg" alt="Bet on {$racetrackname}"  />
    <figure class="rsCaption"> </figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/{$racetrackpath}/{$racetrackpath}-horse-racing.jpg" alt="{$racetrackname} Horse Racing" />
    <figure class="rsCaption"> </figure>
   </div>
   


   
  </div>
 </div>
