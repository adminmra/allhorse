	<div>
		<table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Preakness Stakes Match Races"  summary="">
			<caption>Horses - Belmont Stakes Matchups</caption>
			<tbody>
			
<tr>
					<th colspan="3" class="center">
					Horses - Belmont Stakes Matchups  - Jun 08					</th>
			</tr>

			
<tr>
					<th colspan="3" class="center">
					2019belmont Stakes<br />@ Belmont Park - Belmont, Ny					</th>
			</tr>

	<tr class='head_title'><th>Team</th><th colspan='2' class='center'>ML</th></tr><tr><td style='background:white'>Tacitus</td><td style='background:white'>-216</td></tr><tr><td style='background:white'>War Of Will</td><td style='background:white'>+170</td></tr><tr><td style='background:#ddd;'>Master Fencer</td><td style='background:#ddd;'>+124</td></tr><tr><td style='background:#ddd;'>Intrepid Heart</td><td style='background:#ddd;'>-158</td></tr><tr><td style='background:white'>Everfast</td><td style='background:white'>+142</td></tr><tr><td style='background:white'>Bourbon War</td><td style='background:white'>-181</td></tr><tr><td style='background:#ddd;'>Sir Winston</td><td style='background:#ddd;'>-170</td></tr><tr><td style='background:#ddd;'>Everfast</td><td style='background:#ddd;'>+140</td></tr><tr><td style='background:white'>Tax</td><td style='background:white'>+134</td></tr><tr><td style='background:white'>Spinoff</td><td style='background:white'>-168</td></tr>					<tr>
						<td class="dateUpdated center" colspan="5">
							<em id='updateemp'>  - Updated February 3, 2020 09:30:59 </em>
							 

<!-- br />
							All odds are fixed odds prices. -->
						</td>
					</tr>
			</tbody>
		</table>
	</div>
	