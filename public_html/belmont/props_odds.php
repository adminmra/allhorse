		{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Belmont Stakes Betting Odds:  Props"
	  }
	</script>
		{/literal}
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Belmont Stakes Betting Odds:  Props" summary="Preakness Stakes Betting Odds:  Props">
            <caption>Belmont Stakes Betting Odds:  Props</caption>
            <tbody>
               <!--
 <tr>
                    <th colspan="3" class="center">
                    Horses - Belmont Stakes Props  - Jun 19                    </th>
            </tr>
-->
    <tr class='head_title'><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Man Un/tott Total Goals Scored</td><td>-115</td><td>20/23</td></tr><tr><td>Tiz The Law Finishing Position - Belmont Stakes</td><td>-115</td><td>20/23</td></tr>           <!--
 <tr>
                    <th colspan="3" class="center">
                    Horses - Belmont Stakes Props  - Jun 20                    </th>
            </tr>
-->
    <tr><td>Tiz The Law / Tap It To Win Wins</td><td>-215</td><td>20/43</td></tr><tr><td>Field Wins Belmont Stakes</td><td>+170</td><td>17/10</td></tr><tr><td>Tiz The Law / Dr Post Wins</td><td>-215</td><td>20/43</td></tr><tr><td>Field Wins Belmont Stakes</td><td>+170</td><td>17/10</td></tr><tr><td>Tiz The Law / Sole Volante Wins</td><td>-200</td><td>1/2</td></tr><tr><td>Field Wins Belmont Stakes</td><td>+165</td><td>33/20</td></tr><tr><td>Tap It To Win / Dr Post Wins</td><td>+310</td><td>31/10</td></tr><tr><td>Field Wins Belmont Stakes</td><td>-420</td><td>5/21</td></tr><tr><td>Tap It To Win / Sole Volante Wins</td><td>+330</td><td>33/10</td></tr><tr><td>Field Wins Belmont Stakes</td><td>-450</td><td>2/9</td></tr><tr><td>Dr Post / Sole Volante Wins</td><td>+330</td><td>33/10</td></tr><tr><td>Field Wins Belmont Stakes</td><td>-450</td><td>2/9</td></tr><tr><td>Tiz The Law/tap It To Win/dr Post Wins</td><td>-350</td><td>2/7</td></tr><tr><td>Field Wins Belmont Stakes</td><td>+270</td><td>27/10</td></tr><tr><td>Tiz The Law/tap It To Win/sole Volante</td><td>-335</td><td>20/67</td></tr><tr><td>Field Wins Belmont Stakes</td><td>+265</td><td>53/20</td></tr><tr><td>Tiz The Law/dr Post/sole Volante Wins</td><td>-335</td><td>20/67</td></tr><tr><td>Field Wins Belmont Stakes</td><td>+265</td><td>53/20</td></tr><tr><td>Tap It To Win/dr Post/sole Volante Wins</td><td>+200</td><td>2/1</td></tr><tr><td>Field Wins Belmont Stakes</td><td>-255</td><td>20/51</td></tr><tr><td>Top 4 Horses Win Belmont Stakes</td><td>-700</td><td>1/7</td></tr><tr><td>Field Wins Belmont Stakes</td><td>+500</td><td>5/1</td></tr><tr><td>Saddlecloth Of Winning Horse Odd</td><td>+225</td><td>9/4</td></tr><tr><td>Saddlecloth Of Winning Horse Even</td><td>-275</td><td>4/11</td></tr><tr><td>Saddlecloth Of Winning Horse Is 1-5</td><td>+230</td><td>23/10</td></tr><tr><td>Saddlecloth Of Winning Horse Is 6-10</td><td>-285</td><td>20/57</td></tr><tr><td>Winning Horse's Name Starts With A-m</td><td>+300</td><td>3/1</td></tr><tr><td>Winning Horse's Name Starts With N-z</td><td>-420</td><td>5/21</td></tr><tr><td>Exactly 1 Word In Winning Horse's Name</td><td>+1000</td><td>10/1</td></tr><tr><td>Not Exactly 1 Word In Winning Horse Name</td><td>-2000</td><td>1/20</td></tr><tr><td>Exactly 2 Word In Winning Horse's Name</td><td>+220</td><td>11/5</td></tr><tr><td>Not Exactly 2 Word In Winning Horse Name</td><td>-275</td><td>4/11</td></tr><tr><td> 3+ Words In Winning Horse's Name</td><td>-225</td><td>4/9</td></tr><tr><td>2 Or Less Words In Winning Horse's Name</td><td>+170</td><td>17/10</td></tr><tr><td>Wolver/west Ham Total Goals In Game 6-20</td><td>-115</td><td>20/23</td></tr><tr><td>Tiz The Law Finishing Position - Belmont Stakes</td><td>-115</td><td>20/23</td></tr><tr><td>Official Belmont Stakes Winning Time</td><td>-105</td><td>20/21</td></tr><tr><td>Quickest Finish Mma Fight Night On 6-20</td><td>-125</td><td>4/5</td></tr><tr><td>Total Kos/submissions Combined - Fight Nigth 6-20</td><td>-150</td><td>2/3</td></tr><tr><td>Finishing Position Of Dr Post In Belmont Stakes</td><td>+120</td><td>6/5</td></tr><tr><td>Post Race Interview -yes Wears Mask Covering Mouth</td><td>-115</td><td>20/23</td></tr><tr><td>Post Race Interview - No Mask/not Covering Mouth</td><td>-115</td><td>20/23</td></tr>            </tbody>
			<tfoot>
		 <tr>
                        <td class="dateUpdated center"  colspan="3" >
                            <em id='updateemp'>Updated June 17, 2020.</em><br />
                        </td>
                    </tr>
            </tfoot>			
        </table>
		    {literal}
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
 

        {/literal}
    </div>
	