{literal}
<script type="text/javascript" src="/assets/js/atemay.js"></script>
<script type="text/javascript">
addthisevent.settings({
	license   	: "ammgus1bqzg0il7ntm75",
	mouse		: false,
	css			: false,
	outlook		: {show:true, text:"Outlook <i class='fa fa-plus-square'></i>"},
	google		: {show:true, text:"Google <i class='fa fa-plus-square'></i>"},
	yahoo		: {show:true, text:"Yahoo <i class='fa fa-plus-square'></i>"},
	ical		: {show:true, text:"iCal <i class='fa fa-plus-square'></i>"},
	hotmail		: {show:true, text:"Hotmail <i class='fa fa-plus-square'></i>"},
	facebook	: {show:true, text:"Facebook <i class='fa fa-plus-square'></i>"}
});
</script>
{/literal}

<div class="row infoBlocks">

<div class="col-md-4">
<div class="section">
	<i class="fa fa-calendar"></i>
	<div class="info"> <p><strong>When is the Belmont Stakes?</strong></p>   
	<p>The Belmont Stakes is on  {include file='/home/ah/allhorse/public_html/belmont/racedate.php'}</p>
	</div>
    {*<a href="#" title="Add to Calendar" class="addthisevent" rel="nofollow">Add to calendar <i class="fa fa-plus"></i>
	<span class="_start">06-08-2019 17:00:00</span>
	<span class="_end">06-08-2019 19:00:00</span>
	<span class="_zonecode">15</span> <!-- EST US -->
	<span class="_summary">2019 Belmont Stakes</span>
	<span class="_description">www.usracing.com <br/>Place My Belmont Stakes Bet at https://www.betusracing.ag</span>
	<span class="_location">Belmont Park, Elmont, New York</span>
	<span class="_organizer">usracing.com</span>
	<span class="_organizer_email">comments@usracing.com</span>
	<span class="_all_day_event">false</span>
	<span class="_date_format">06/08/2019</span>
	</a> *}
</div>
</div>

<div class="col-md-4">
<div class="section">
	<i class="fa fa-map-marker"></i>
	<div class="info">
	<p><strong>Where Can I Bet on the Belmont Stakes? </strong></p><p>At BUSR: <br><a itemprop="url" href="/belmont-stakes/odds?ref={$ref}">Odds are Live!</a></p>
    </div>
 <p><a href="/signup?ref={$ref}" class="btn btn-primary btn-sm btn-red" rel="nofollow">Bet Now!<i class="fa fa-chevron-right"></i></a>   </p></div>
</div>

<div class="col-md-4">
<div class="section">
	<i class="glyphicon glyphicon-eye-open"></i>
    <div class="info">
	    <p><strong>What Channel is the Belmont Stakes on?</strong></p>
	<p>Watch the Belmont Stakes live on TV with NBC at 5:00 p.m. EST</p>
    </div>
</div>
</div>
</div>
