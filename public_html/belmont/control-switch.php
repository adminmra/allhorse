
<?php //BELMONT STAKES Control Switch
$SwitchVariable['variableName'][]= 'BS_Switch_Early';         //Three Weeks Out
$SwitchVariable['startTIME'][]= '2021-05-15 18:54';         // 1 hero image, primary menu, pages metas
$SwitchVariable['endTIME'][]= '2021-05-22 00:00';           // TURN *OFF*   at KD_Switch_Main

$SwitchVariable['variableName'][]= 'BS_Switch_Main';        // Two Weeks out.
$SwitchVariable['startTIME'][]= '2021-05-24 00:00'; 	// full BS hero, primary menu, metas, index, kd & ko odds copy
$SwitchVariable['endTIME'][]= '2021-06-05 8:00';	//Ends day before BS Day

	$SwitchVariable['variableName'][]= 'BS_Switch_Full_Site';        //********** Runs Concurrently! ***********  One Week out.
	$SwitchVariable['startTIME'][]= '2021-05-28 9:00'; 	// BS Switch Main + states & Misc page copy
	$SwitchVariable['endTIME'][]= '2021-06-05 18:00';


$SwitchVariable['variableName'][]= 'BS_Switch_Race_Day';        // 
$SwitchVariable['startTIME'][]= '2021-06-05 08:00'; 	// BS Switch Full site + day of meta changes 
$SwitchVariable['endTIME'][]= '2021-06-05 18:00';


$SwitchVariable['variableName'][]= 'BS_Results';        // Day of Belmont to End of Sunday
$SwitchVariable['startTIME'][]= '2021-06-05 18:00';         // starts at end of the race
$SwitchVariable['endTIME'][]= '2021-06-07 23:59';           // 


/*
	$SwitchVariable['variableName'][]= 'BS_Switch'; 		// End of Preakness Race to End of Belmont Race
$SwitchVariable['startTIME'][]='2020-06-08 00:00'; 		// TURN *ON*    AT END OF PREAKNESS ***************************** PREAKNESS
$SwitchVariable['endTIME'][]='2020-06-12 13:00';	 	// TURN *OFF*    UPDATE AT END OF BELMONT ***************************** BELMONT

$SwitchVariable['variableName'][]= 'BS_Switch_Phase_1'; // One Week Out to End of Belmont Race
$SwitchVariable['startTIME'][]='2020-06-12 13:00'; 		// One Week Out to End of Belmont Race
$SwitchVariable['endTIME'][]='2020-06-20 17:42';	 	// TURN *OFF*    UPDATE AT END OF BELMONT ***************************** BELMONT


$SwitchVariable['variableName'][]= 'BS_Switch_Menu'; 
$SwitchVariable['startTIME'][]='2020-06-12 13:00'; 
$SwitchVariable['endTIME'][]='2020-06-21 23:59';

$SwitchVariable['variableName'][]= 'BS_Switch_Results'; // End of Preakness Race to End of Belmont Race
$SwitchVariable['startTIME'][]='2019-06-08 00:00'; 		// Starts Day of Belmont 
$SwitchVariable['endTIME'][]='2019-06-15 23:59';	 	// End 1 week after Belmont

$SwitchVariable['variableName'][]= 'BS_Switch_Old_Odds'; // Day of Kentucky Derby for 5 days
$SwitchVariable['startTIME'][]='2019-06-08 18:42'; 		// TURN *ON*   AT END OF BELMONT ***************************** BELMONT 
$SwitchVariable['endTIME'][]='2019-06-15 23:59';		// End 5 days after BS

$SwitchVariable['variableName'][]= 'BS_Switch_Race_Day';
$SwitchVariable['startTIME'][]='2020-06-19 23:59';
$SwitchVariable['endTIME'][]='2020-06-20 19:00';

$SwitchVariable['variableName'][]= 'BS_Switch_Sunday';
$SwitchVariable['startTIME'][]='2020-06-20 17:42';
$SwitchVariable['endTIME'][]='2020-06-21 23:59';

$SwitchVariable['variableName'][]= 'BS_Switch_Clubhouse';
$SwitchVariable['startTIME'][]='2020-06-08 00:00';
$SwitchVariable['endTIME'][]='2020-06-20 17:42';

$SwitchVariable['variableName'][]= 'BS_Switch_Clubhouse_Friday';
$SwitchVariable['startTIME'][]='2020-06-08 00:00';
$SwitchVariable['endTIME'][]='2020-06-19 23:59';

$SwitchVariable['variableName'][]= 'BS_Switch_Clubhouse_Sunday';
$SwitchVariable['startTIME'][]='2020-06-08 00:00';
$SwitchVariable['endTIME'][]='2020-06-21 23:59';

$SwitchVariable['variableName'][]= 'BS_Switch_Blackjack';
$SwitchVariable['startTIME'][]='2020-06-21 23:59';
$SwitchVariable['endTIME'][]='2020-06-23 16:59';*/

?>