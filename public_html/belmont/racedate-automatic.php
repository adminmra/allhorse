<?php
$cdate = date("Y-m-d H:i:s");
if ($cdate > "2019-06-08 23:59:00" && $cdate < "2020-06-20 23:59:00") {
    echo "Saturday, June 20th, 2020";
} else if ($cdate > "2020-06-20 23:59:00" && $cdate < "2021-06-05 23:59:00") {
    echo "Saturday, June 5th, 2021";
} else if ($cdate > "2021-06-05 23:59:00" && $cdate < "2022-06-04 23:59:00") {
    echo "Saturday, Jun 11th, 2022";
} else if ($cdate > "2022-06-04 23:59:00" && $cdate < "2023-06-03 23:59:00") { 
    echo "Saturday, 10th Jun, 2023";
} else if ($cdate > "2023-06-03 23:59:00" && $cdate < "2024-06-08 23:59:00") {
    echo "Saturday, June 8th, 2024";
} else if ($cdate > "2024-06-08 23:59:00" && $cdate < "2025-06-07 23:59:00") {
    echo "Saturday, June 7th, 2025";
}
?>