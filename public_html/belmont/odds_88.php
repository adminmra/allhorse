
<style>
	@media (max-width: 800px) {
		.hide-sm {
			display: none;
		}
	}
	@media (min-width: 801px) {
		.hide-lg {
			display: none;
		}
	}
</style>

<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2020 Belmont Stakes Odds "
	}
</script>

<!--<h2 class="hide-lg">2020 Belmont Stakes Odds</h2>-->


<div id="no-more-tables">
	<table id="infoEntries" class="table table-condensed table-striped table-bordered ordenable data" cellpadding="0"
		cellspacing="0" border="0" title="Belmont Stakes Odds and Post Positions"
		summary="The Latest odds for Belmont Stakes at Belmont Park">
	    <tbody>
	    	<caption class="hide-sm">Belmont Stakes Odds</caption>
			<tr class='head_title'>
				<th style="width: 8%;">PP</th>
	            <th style="width: 8%;">Horse</th>
	            <th style="width: 8%;">Odds</th>
	            <th style="width: 8%;">Jockey</th>
	            <th style="width: 13%;">Trainer</th>
			</tr>
	        <tr>
	            <td data-title="PP">1</td>
	            <td data-title="Horse"><strong>Bourbonic</strong></td>
	            <td data-title="Odds">15-1</td>
	            <td data-title="Jockey">Kendrick Carmouche</td>
	            <td data-title="Trainer">Todd Pletcher</td>
	        </tr>
	        <tr>
	            <td data-title="PP">2</td>
	            <td data-title="Horse"><strong>Essential Quality</strong></td>
	            <td data-title="Odds">2-1</td>
	            <td data-title="Jockey">Luis Saez</td>
	            <td data-title="Trainer">Brad Cox</td>
	        </tr>
	        <tr>
	            <td data-title="PP">3</td>
	            <td data-title="Horse"><strong>Rombauer</strong></td>
	            <td data-title="Odds">3-1</td>
	            <td data-title="Jockey">John Velazquez</td>
	            <td data-title="Trainer">Michael McCarthy</td>
	        </tr>
	        <tr>
	            <td data-title="PP">4</td>
	            <td data-title="Horse"><strong>Hot Rod Charlie</strong></td>
	            <td data-title="Odds">7-2</td>
	            <td data-title="Jockey">Flavien Pratt</td>
	            <td data-title="Trainer">WDoug O'Neill</td>
	        </tr>
	        <tr>
	            <td data-title="PP">5</td>
	            <td data-title="Horse"><strong>France Go de Ina</strong></td>
	            <td data-title="Odds">30-1</td>
	            <td data-title="Jockey">Ricardo Santana Jr.</td>
	            <td data-title="Trainer">Hideyuki Mori</td>
	        </tr>
	        <tr>
	            <td data-title="PP">6</td>
	            <td data-title="Horse"><strong>Known Agenda</strong></td>
	            <td data-title="Odds">6-1</td>
	            <td data-title="Jockey">Jose Ortiz</td>
	            <td data-title="Trainer">Todd Pletcher</td>
	        </tr>
	        <tr>
	            <td data-title="PP">7</td>
	            <td data-title="Horse"><strong>Rock Your World</strong></td>
	            <td data-title="Odds">9-2</td>
	            <td data-title="Jockey">Joel Rosario</td>
	            <td data-title="Trainer">John W. Sadler</td>
	        </tr>
	        <tr>
	            <td data-title="PP">8</td>
	            <td data-title="Horse"><strong>Overtook</strong></td>
	            <td data-title="Odds">20-1</td>
	            <td data-title="Jockey">Manny Franco</td>
	            <td data-title="Trainer">Todd Pletcher</td>
	        </tr>
	    </tbody>
	</table>


</div> 

<p class="dateUpdated center"><em id="updateemp">Updated <?php  $tdate = date("M d, Y H:00", time() - 7200); echo $tdate
		. " EST"; ?> .</em></p>


	

<style>
	table.ordenable tbody th {
		cursor: pointer;
	}
</style>

<script src="/assets/js/jquery.sortElements.js"></script>

<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">

<script src="/assets/js/sorttable_post.js"></script>

<script src="//www.usracing.com/assets/js/aligncolumn.js"></script>

<script>
	$(document).ready(function () {

		alignColumn([2], 'left');

	});
</script>

