{literal}
  <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "name": "Bet the 2022 Belmont Stakes",
        "startDate": "2022-06-11",
        "endDate": "2022-06-11",
        "sport": "Horse Racing",
        "location": {
            "@type": "Place",
            "name" : "Belmont Park",
            "address": "2150 Hempstead Turnpike, Elmont, NY 11003"
			},
        "url": "https://www.usracing.com/belmont-stakes/betting"
    }
        {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "name": "Bet the 2022 Kentucky Derby",
        "startDate": "2022-05-07",
        "endDate": "2022-05-07",
        "sport": "Horse Racing",
        "location": {
            "@type": "Place",
            "name" : "Churchill Downs",
            "address": "700 Central Avenue Louisville, KY 40208"
			},
        "url": "https://www.usracing.com/kentucky-derby/betting"
    }
        {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "name": "Bet the 2020 Preakness Stakes",
        "startDate": "2022-05-21",
        "endDate": "2022-05-21",
        "sport": "Horse Racing",
        "location": {
            "@type": "Place",
            "name" : "Pimlico Race Course",
            "address": "5201 Park Heights Avenue Baltimore, MD 21215"
			},
        "url": "https://www.usracing.com/preakness-stakes/betting"
    }
</script>
{/literal}