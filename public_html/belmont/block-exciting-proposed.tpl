    <section class="card card--red" style="margin-bottom:-40px; background:#1672bd;">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red" style="background:#1672bd; color:#fff;">
            <h2 class="card_heading card_heading--no-cap">First Deposit Bonus, Up To $500 Cash!<br>at BUSR</h2>
            <br>
            <a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Sign Up Now</a>
          </div>
        </div>
      </div>
    </section>
