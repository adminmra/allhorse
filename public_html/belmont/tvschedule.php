<h2> Belmont Stakes TV Schedule</h2>
<div class="table-responsive">
<table  class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"summary="A TV schedule for betting on the Belmont Stakes">


  <tbody><tr>
    <th width="73">Network</th>
    <th width="115">Date</th>
    <th width="270">Races</th>
    <th width="100">Time (ET)</th>
  </tr>
  <tr>
	<td>NBC Sports Network</td>
    <td width="115">Friday, June 7th </td>
    <td width="287">Belmont Stakes Classics </td>
    <td width="94">4 - 5pm</td>
  </tr>

  <tr class="odd">
    <td>NBC Sports Network</td>
    <td>Friday, June 7th </td>
    <td>Belmont Access: (Jaipur and Brooklyn) </td>
    <td>5 - 6pm</td>
  </tr>

  <tr>
    <td>NBC Sports Network</td>
    <td>Saturday, June 8th </td>
    <td>Belmont Stakes Prep: (Just A Game, True North, Woody Stephens) </td>
    <td>3 - 5pm</td>
  </tr>

  <tr class="odd">
    <td>NBC</td>
    <td>Saturday, June 8th </td>
    <td>151st Belmont Stakes: (Woodford Reserve Manhattan and Belmont Stakes) </td>
    <td>5 - 7pm</td>
  </tr>

  <tr>
    <td>NBC Sports Network</td>
    <td>Saturday, June 8th </td>
    <td>Belmont Stakes Post Race </td>
    <td>7 - 7:30pm</td>
  </tr>

</tbody></table>