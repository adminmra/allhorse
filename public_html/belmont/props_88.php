{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2019
 Belmont Stakes Odds"
	  }
	</script>
	{/literal}
	<h2>2020 Belmont Stakes Props</h2>
	<h3>Wagers cut off: 2020 17th June 3:00 PM</h3>
	<div  id="no-more-tables">
		
		<table id="infoEntries" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Belmont Stakes Props"  summary="The latest props for the Belmont Stakes available for wagering now at BUSR.">
			<thead>
				<tr><th>Prop</th><th>Odds</th></tr>
		  </thead>
			<tbody>
<tr><td data-title="Prop">Winning Horse's Name Starts With a Letter / A-i</td><td data-title="Odds">-115</td></tr>
<tr><td data-title="Prop">Winning Horse's Name Starts With a Letter / J-z</td><td data-title="Odds">-115</td></tr>
<tr><td data-title="Prop">Will The Belmont stakes Winner Lead Wire To Wire? YES</td><td data-title="Odds">+400</td></tr>
<tr><td data-title="Prop">Will The Belmont stakes Winner Lead Wire To Wire? NO</td><td data-title="Odds">-750</td></tr>
<tr><td data-title="Prop">Number of Words in The Name of The Belmont Winner - 1 WORD</td><td data-title="Odds">+105</td></tr>
<tr><td data-title="Prop">Number of Words in The Name of The Belmont Winner - 2 WORDS</td><td data-title="Odds">+175</td></tr>
<tr><td data-title="Prop">Number of Words in The Name of The Belmont Winner - 3 WORDS</td><td data-title="Odds">+300</td></tr> 
<tr><td data-title="Prop">Last Place Saddlecloth Number Is - ODD</td><td data-title="Odds">-170</td></tr>
<tr><td data-title="Prop">Last Place Saddlecloth Number Is - EVEN</td><td data-title="Odds">+130</td></tr> 
<tr><td data-title="Prop">Winning Trainer Have Won An Eclipse Award? - YES</td><td data-title="Odds">-200</td></tr>
<tr><td data-title="Prop">Winning Trainer Have Won An Eclipse Award? - NO</td><td data-title="Odds">+160</td></tr>
<tr><td data-title="Prop">Will The Belmont Winner Be Wearing Blinkers? - YES</td><td data-title="Odds">+550</td></tr>
<tr><td data-title="Prop">Will The Belmont Winner Be Wearing Blinkers? - NO</td><td data-title="Odds">-950</td></tr>
<tr><td data-title="Prop">Belmont Stakes Winning Exacta Payout - Over $48.75</td><td data-title="Odds">+130</td></tr>
<tr><td data-title="Prop">Belmont Stakes Winning Exacta Payout - Under $991.79</td><td data-title="Odds">-180</td></tr>
    </tbody>
		</table>
		</div>
	</div>
	<p class="dateUpdated center"><em id="updateemp">Updated {php} $tdate = date("M d, Y H:00", time() - 7200); echo $tdate . " EST"; {/php} .<!-- br />All odds are fixed odds prices. --></em>
                </td></p>
{literal}
    <style>
        table.ordenable tbody th{cursor: pointer;}
    </style>
    <script src="/assets/js/jquery.sortElements.js"></script>
	<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
	<script src="/assets/js/sorttable_results.js"></script>
	<script src="//www.usracing.com/assets/js/aligncolumn.js"></script>
    <script>
        $(document).ready(function(){
            alignColumn([2], 'left');
        });
   </script>
{/literal}



	

