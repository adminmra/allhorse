<h2>Belmont Stakes Purse Structure</h2>
<div class="table-responsive">
<table  class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0" summary="  Belmont Stakes Purse" border="0" cellpadding="0" cellspacing="0" width="500">

  <tbody><tr>
    <th width="250">Result</th>
    <th width="224">Purse of $1,500,000</th>
  </tr>
  <tr>
    <td>Winner</td>
    <td>$800,000 (62%) </td>
  </tr>
  <tr class="odd">
    <td>Second</td>
    <td>$300,000 (20%) </td>
  </tr>
  <tr>
    <td>Third</td>
    <td>$150,000 (10%) </td>
  </tr>
  <tr class="odd">
    <td>Forth</td>
    <td>$75,000 (5%) </td>
  </tr>
  <tr>
    <td>Fifth</td>
    <td>$45,000 (3%)</td>
  </tr>
</tbody></table>