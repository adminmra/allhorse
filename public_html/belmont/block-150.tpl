    <section class="card">
      <div class="card_wrap">
          <div class="card_half card_hide-mobile">
            <a href="/signup?ref={$ref13}">{*change the path of the link*}
                <img src="/img/index-kd/150-cash-bonus.jpg" class="card_img" alt="Horse Racing 2020 $150 Cash Bonus Offer">{*change the path the img in src*}
            </a>
        </div>
       <div class="card_half card_content">
            <a href="ttps://www.usracing.com/promos/cash-bonus-150">{*change the path of the link*}
                <img class="card_icon"
                    src="/img/states/cash-bonus-150.png">{*change the path the img in src*}
            </a>
            <h2 class="card_heading">$150 CASH BONUS!</h2>
            <h3 class="card_subheading">
            <p style="margin-bottom: 50px">If you play at least $500 in cumulative wagering at <a href="/signup?ref={$ref14}">BUSR</a> within 30 days of registering your account, we will automatically deposit an extra $150 into your account!</p>
            <a href="https://www.usracing.com/promos/cash-bonus-150" class="card_btn">Learn More</a>
        </div>
    </section>
