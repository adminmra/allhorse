	<div class="table-responsive">
		<table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Belmont Stakes Odds">
			<caption> 2021
 Belmont Stakes Odds</caption>
			<tbody>
						<tr>
				<th colspan="3" class="center">
				Horses - Belmont Stakes - To Win  - Jun 05				</th>
			</tr>
			<tr><th colspan="3" class="center">Belmont Stakes - To Win</th></tr><tr class='head_title'><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Essential Quality </td><td>+175</td><td>7/4</td></tr><tr><td>Rombauer </td><td>+450</td><td>9/2</td></tr><tr><td>Hot Rod Charlie </td><td>+500</td><td>5/1</td></tr><tr><td>Known Agenda </td><td>+600</td><td>6/1</td></tr><tr><td>Rock Your World </td><td>+375</td><td>15/4</td></tr><tr><td>Bourbonic </td><td>+1600</td><td>16/1</td></tr><tr><td>Overtook </td><td>+2500</td><td>25/1</td></tr><tr><td>France Go De Ina </td><td>+4000</td><td>40/1</td></tr>
				<tr>
					<td class="dateUpdated center" colspan="5">
						<em id='updateemp'>Updated June 5, 2021 22:00:08.</em>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	