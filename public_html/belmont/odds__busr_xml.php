  
    <div class="table-responsive">
        <table  class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Belmont Stakes Odds">
            <caption>* 2015 BELMONT STAKES - ODDS</caption>     
            <tbody>
                <tr>   
                    <th>Contenders</th>            
                    <th>American Odds</th>            
                    <th>Fractional Odds</th>            
                </tr>
                                <tr>
                    <td>
                        AMERICAN PHAROAH                    </td>
                    <td>                    
                        <table class="fer" style="width:100%">
                                                        <tr>
                                <td>-150</td>                                
                            </tr>
                                                </table>
                </td>
                <td>                    
                    <table class="fer" style="width:100%">
                                                <tr>                                
                            <td>2/3</td>
                        </tr>
                                        </table>
            </td>
        </tr>
                        <tr>
                    <td>
                        FROSTED                    </td>
                    <td>                    
                        <table class="fer" style="width:100%">
                                                        <tr>
                                <td>+500</td>                                
                            </tr>
                                                </table>
                </td>
                <td>                    
                    <table class="fer" style="width:100%">
                                                <tr>                                
                            <td>5/1</td>
                        </tr>
                                        </table>
            </td>
        </tr>
                        <tr>
                    <td>
                        MATERIALITY                    </td>
                    <td>                    
                        <table class="fer" style="width:100%">
                                                        <tr>
                                <td>+550</td>                                
                            </tr>
                                                </table>
                </td>
                <td>                    
                    <table class="fer" style="width:100%">
                                                <tr>                                
                            <td>11/2</td>
                        </tr>
                                        </table>
            </td>
        </tr>
                        <tr>
                    <td>
                        MUBTAAHIJ                    </td>
                    <td>                    
                        <table class="fer" style="width:100%">
                                                        <tr>
                                <td>+1200</td>                                
                            </tr>
                                                </table>
                </td>
                <td>                    
                    <table class="fer" style="width:100%">
                                                <tr>                                
                            <td>12/1</td>
                        </tr>
                                        </table>
            </td>
        </tr>
                        <tr>
                    <td>
                        MADEFROMLUCKY                    </td>
                    <td>                    
                        <table class="fer" style="width:100%">
                                                        <tr>
                                <td>+1400</td>                                
                            </tr>
                                                </table>
                </td>
                <td>                    
                    <table class="fer" style="width:100%">
                                                <tr>                                
                            <td>14/1</td>
                        </tr>
                                        </table>
            </td>
        </tr>
                        <tr>
                    <td>
                        KEEN ICE                    </td>
                    <td>                    
                        <table class="fer" style="width:100%">
                                                        <tr>
                                <td>+2000</td>                                
                            </tr>
                                                </table>
                </td>
                <td>                    
                    <table class="fer" style="width:100%">
                                                <tr>                                
                            <td>20/1</td>
                        </tr>
                                        </table>
            </td>
        </tr>
                        <tr>
                    <td>
                        TALE OF VERVE                    </td>
                    <td>                    
                        <table class="fer" style="width:100%">
                                                        <tr>
                                <td>+1800</td>                                
                            </tr>
                                                </table>
                </td>
                <td>                    
                    <table class="fer" style="width:100%">
                                                <tr>                                
                            <td>18/1</td>
                        </tr>
                                        </table>
            </td>
        </tr>
                        <tr>
                    <td>
                        FRAMMENTO                    </td>
                    <td>                    
                        <table class="fer" style="width:100%">
                                                        <tr>
                                <td>+3500</td>                                
                            </tr>
                                                </table>
                </td>
                <td>                    
                    <table class="fer" style="width:100%">
                                                <tr>                                
                            <td>35/1</td>
                        </tr>
                                        </table>
            </td>
        </tr>
              
    <tr>      
        <td class="dateUpdated center" colspan="5"><em>Updated June 5, 2015. BUSR - Official <a href="http://www.usracing.com/belmont-stakes/odds">Belmont Stakes Odds</a>. <br>Bet now to lock in your Belmont Stakes future odds prices. </em></td>
    </tr>
</tbody>   
</table>    
</div>

