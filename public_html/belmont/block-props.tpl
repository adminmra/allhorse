{literal}
    <style>
		@media (min-width: 768px) and (max-width: 797px) {
			.fixed_cta {
                font-size: 17px;
            }
		}
        @media (min-width: 889px) and (max-width: 933px) {
			.fixed_cta {
                font-size: 22px;
            }
		}
        @media (min-width: 1024px) and (max-width: 1033px) {
            .fixed_cta {
                font-size: 23px;
            }
        }
	</style>
{/literal}

<section class="card">
      <div class="card_wrap">
          <<div class="card_half card_hide-mobile">
            <a href="/signup?ref={$ref}" rel="no follow">
            <img src="/img/index-kd/bet-kentucky-derby-jockeys-2.jpg" class="card_img" alt="Belmont Stakes Props">{*change the path the img in src*}
            </a></div>
       <div class="card_half card_content">
            <a href="https://www.usracing.com/belmont-stakes/props">{*change the path of the link*}
                <img class="icon-kentucky-derby-betting card_icon"
                    src="/img/index-kd/icon-bet-kentucky-derby-jockeys.png">{*change the path the img in src*}
            </a>
            <h2 class="card_heading">Belmont Stakes Props</h2>
            <p style="margin-bottom: 10px">You'll  get great fixed odds with amazing payouts on the leading Belmont Stakes horses.</p>
            <p> Bet on the prop wagers such as margin of victory and more.</p>
            <p><a href="/belmont-stakes/props" class="btn-xlrg fixed_cta">Learn More</a></p>
        </div>
    </section> 