

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Belmont Stakes Results"
  }
</script>
<h2>2021 Belmont Stakes Results</h2>
<!--fix this to the new table-->
<div id="no-more-tables">
  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed ordenableResult">
    <thead>
      <tr>
        <th>Result</th>
        <th>PP</th>
        <th>Horse</th>
        <th>Jockey</th>
        <th>Trainer</th>
      </tr>
    </thead>
    <tbody>

      <tr>
        <td data-title="Result">1</td>
        <td data-title="PP">2</td>
        <td data-title="Horse">Essential Quality</td>
        <td data-title="Jockey">Luis Saez</td>
        <td data-title="Trainer">Brad H. Cox</td>
      </tr>
      <tr>
        <td data-title="Result">2</td>
        <td data-title="PP">4</td>
        <td data-title="Horse">Hot Rod Charlie</td>
        <td data-title="Jockey">Flavien Prat</td>
        <td data-title="Trainer">Doug F. O'Neill</td>
      </tr>
      <tr>
        <td data-title="Result">3</td>
        <td data-title="PP">3</td>
        <td data-title="Horse">Rombauer</td>
        <td data-title="Jockey">Ricardo Santana Jr.</td>
        <td data-title="Trainer">Hideyuki Mori</td>
      </tr>
      <tr>
      <td data-title="Result">4</td>
        <td data-title="PP">6</td>
        <td data-title="Horse">Known Agenda</td>
        <td data-title="Jockey">Manny Franco</td>
        <td data-title="Trainer">Todd A. Pletcher</td>
      </tr>
    </tbody>
  </table>
</div>



<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_results.js"></script>


<h2>2021 Belmont Stakes Payouts</h2>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="data table table-condensed table-striped table-bordered"
    title="Belmont Stakes" summary="Belmont Stakes Payouts. ">
    <tbody>
      <tr>
        <th>PP</th>
        <th>Horses</th>
        <th>Win</th>
        <th>Place</th>
        <th>Show</th>
      </tr>
      <tr>
        <td>2</td>
        <td>Essential Quality</td>
        <td>$4.60</td>
        <td>$3.00</td>
        <td>$2.60</td>
      </tr>
      <tr>
        <td>4</td>
        <td>Hot Rod Charlie</td>
        <td></td>
        <td>$4.10</td>
        <td>$2.90</td>
      </tr>
      <tr>
        <td>3</td>
        <td>Rombauer</td>
        <td></td>
        <td></td>
        <td>$3.50</td>
      </tr>
    </tbody>
  </table>
</div>
<p></p>

<div id="no-more-tables">
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Classic Payouts" summary="Last year's Payouts of the Breeders' Cup Classic. ">
    <thead>
      <tr>
        <th>Wager</th>
        <th>Horses</th>
        <th>Denomination</th>
        <th>Payout</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Wager">Exacta</td>
        <td data-title="Horses">2-4</td>
        <td data-title="Denomination">$1.00</td>
        <td data-title="Payout">$7.50</td>
      </tr>
      <tr>
        <td data-title="Wager">Trifecta</td>
        <td data-title="Horses">2-4-3</td>
        <td data-title="Denomination">$0.50</td>
        <td data-title="Payout">$10.85</td>
      </tr>
      <tr>
        <td data-title="Wager">Superfecta</td>
        <td data-title="Horses">2-4-3-6</td>
        <td data-title="Denomination">$0.10</td>
        <td data-title="Payout">$6.07</td>
      </tr>
      <tr>
        <td data-title="Wager">Double</td>
        <td data-title="Horses">4-2</td>
        <td data-title="Denomination">$1.00</td>
        <td data-title="Payout">$5.00</td>
      </tr>
      <tr>
        <td data-title="Wager">Pick 3</td>
        <td data-title="Horses">3-4-2</td>
        <td data-title="Denomination">$1.00</td>
        <td data-title="Payout">$59.00</td>
      </tr>
    </tbody>
  </table>
</div>