<section class="card">
    <div class="card_wrap">
        <div class="card_half card_content">
            <a href="https://www.usracing.com/rebates">{*change the path of the link*}
                <img class="card_icon"
                    src="/img/states/rebates.png">{*change the path the img in src*}
            </a>
            <h2 class="card_heading">UP TO 8% HORSE BETTING REBATE!</h2>
            <p style="margin-bottom: 10px">Feel more in control thanks to an outstanding rebate bonus!</p>
            <p>Make your straight bets with <a href="/signup?ref={$ref15}">BUSR</a>, and the next day, you'll find a 3% rebate in your account! You can also bet on anything except for Win, Place and Show, and you'll find up to 8% rebate!</p>
            <a href="https://www.usracing.com/rebates" class="card_btn">Learn More</a>
        </div>
        <div class="card_half card_hide-mobile">
            <a href="/signup?ref=horse-racing">{*change the path of the link*}
                <img src="/img/index-kd/rebate-bonus-2.jpg" class="card_img" alt="Horse Racing 2020 Signup Offer">{*change the path the img in src*}
            </a>
        </div>
    </div>
</section>