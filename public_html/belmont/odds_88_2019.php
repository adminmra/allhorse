	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2019
 Belmont Stakes Odds"
	  }
	</script>
	{/literal}
	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable"  cellpadding="0" cellspacing="0" border="0"  title="Belmont Stakes Odds"  summary="The latest odds for the Belmont Stakes available for wagering now at BUSR.">
			<caption>2019
 Belmont Stakes Odds</caption>
			<tbody>
	<!--		
<tr>
					<th colspan="3" class="center">
					Horses - Belmont Stakes - To Win  - Jun 08					</th>
			</tr> -->

	<!--		
<tr>
					<th colspan="3" class="center">
					No Runner No Bet					</th>
			</tr> -->

	<tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Joevia</td><td>30/1</td><td>+3000</td></tr><tr><td>Everfast</td><td>20/1</td><td>+2000</td></tr><tr><td>Master Fencer</td><td>11/1</td><td>+1100</td></tr><tr><td>Tax</td><td>15/1</td><td>+1500</td></tr><tr><td>Bourbon War</td><td>14/1</td><td>+1400</td></tr><tr><td>Spinoff</td><td>12/1</td><td>+1200</td></tr><tr><td>Sir Winston</td><td>13/1</td><td>+1300</td></tr><tr><td>Intrepid Heart</td><td>15/2</td><td>+750</td></tr><tr><td>War Of Will</td><td>3/1</td><td>+300</td></tr><tr><td>Tacitus</td><td>7/4</td><td>+175</td></tr></tbody>
		<tfoot>
						<tr>
						<td class="dateUpdated center" colspan="3">
							<em id='updateemp'>  - Updated February 3, 2020 09:30:49 </em>

<!-- br />
							All odds are fixed odds prices. -->
						</td>
					</tr>
			</tfoot>
		</table>
	</div>
{literal}
        <style>
            table.ordenable tbody th{cursor: pointer;}
			.table-bordered > thead > tr th { background:#1571BA !important; border: 1px solid #ccc; border-right:0; border-top:0; border-bottom:0;color:#fff; }
			.table-bordered > tbody > tr td, .table-bordered > tbody > tr th { text-align: center !important; }
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
        {/literal}

	