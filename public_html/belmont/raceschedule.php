<h2><?php include('/home/ah/allhorse/public_html/belmont/year.php'); ?>   Belmont Stakes DAY Race Schedule</h2>


<table id="horse-betting" class="horse-betting" title="Belmont Stakes Betting - Stakes Race Schedule" summary=" Belmont Stakes Race Schedule" border="0" cellpadding="0" cellspacing="0" width="95%">
 
   <tbody><tr>
   <th width="10%">Post Time</th>

    <th width="26%">Stakes</th>
    <th width="14%">Division</th>
    <th width="5%">Grade</th>
    <th width="12%">Purse </th>
    <th width="15%"> Distance</th>
    <th width="18%">Age</th>

   </tr>

   <tr class="odd">
  <td>3:15pm</td>
    <td>True North Stakes</td>
    <td>Dirt </td>

    <td> II </td>
    <td>$400,000  </td>
    <td> 6 f </td>
    <td>3 Yrs  &amp; Up </td>
  </tr>

    <tr>
  <td>3:59pm</td>
    <td>Woody Stephens Stakes</td>
    <td>Dirt</td>
    <td>II</td>
    <td>$400,000 </td>

    <td> 7F </td>
    <td> 3 Yrs </td>
  </tr>
  <tr class="odd">
  <td>4:43pm</td>
    <td> Just a Game Stakes</td>

    <td>fillies &amp; mares- turf </td>
    <td> I </td>
    <td>$500,000</td>
    <td> 1 mile </td>
    <td>3 yo's &amp; up </td>

  </tr> 
    <tr>
	<td>5:39pm</td>
    <td>Woodford Reserve Manhattan Stakes</td>
    <td>Turf </td>
    <td> I </td>
    <td> $500,000</td>

    <td>1 1/4 miles </td>
    <td>3 yo's &amp; up </td>
  </tr>  
   <tr class="odd">
   <td>6:35pm</td>
    <td>Belmont Stakes</td>

    <td>Dirt </td>
    <td>I </td>
    <td>$1,000,000</td>
    <td>1 1/2 miles</td>
    <td>3 Yo</td>
  </tr>  


</tbody></table>
<br>
Gates open at 8:30 a.m.<br>
First race starts at 11:45 a.m.<br>
Belmont Stakes runs at approximately 6:32 p.m.<br>
General Admission: Grandstand $10, Clubhouse $20.<br>