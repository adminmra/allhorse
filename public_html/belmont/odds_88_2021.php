	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2021
 Belmont Stakes Odds"
	  }
	</script>

	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable"  cellpadding="0" cellspacing="0" border="0"  title="Belmont Stakes Odds"  summary="The latest odds for the Belmont Stakes available for wagering now at Bet US Racing.">
			<caption>2021
 Belmont Stakes Odds</caption>
			<tbody>
	<!--		
<tr>
					<th colspan="3" class="center">
					Horses - Belmont Stakes - To Win  - Jun 05					</th>
			</tr> -->

	<tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Essential Quality </td><td>7/4</td><td>+175</td></tr><tr><td>Rombauer </td><td>9/2</td><td>+450</td></tr><tr><td>Hot Rod Charlie </td><td>5/1</td><td>+500</td></tr><tr><td>Known Agenda </td><td>6/1</td><td>+600</td></tr><tr><td>Rock Your World </td><td>15/4</td><td>+375</td></tr><tr><td>Bourbonic </td><td>16/1</td><td>+1600</td></tr><tr><td>Overtook </td><td>25/1</td><td>+2500</td></tr><tr><td>France Go De Ina </td><td>40/1</td><td>+4000</td></tr></tbody>
		<tfoot>
						<tr>
						<td class="dateUpdated center" colspan="3">
							<em id='updateemp'>  - Updated November 16, 2021 19:00:08 </em>
						</td>
					</tr>
			</tfoot>
		</table>
	</div>

        <style>
            table.ordenable tbody th{cursor: pointer;}
			.table-bordered > thead > tr th { background:#1571BA !important; border: 1px solid #ccc; border-right:0; border-top:0; border-bottom:0;color:#fff; }
			.table-bordered > tbody > tr td, .table-bordered > tbody > tr th { text-align: center !important; }
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>


	