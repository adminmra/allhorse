	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2020
 Belmont Stakes Odds"
	  }
	</script>
	{/literal}
	<div>
		<table class="data table table-condensed table-striped table-bordered ordenable"  cellpadding="0" cellspacing="0" border="0"  title="Belmont Stakes Odds"  summary="The latest odds for the Belmont Stakes available for wagering now at Bet US Racing.">
			<caption>2020
 Belmont Stakes Odds</caption>
			<tbody>
	<!--		
<tr>
					<th colspan="3" class="center">
					Horses - Belmont Stakes - To Win  - Jun 20					</th>
			</tr> -->

	<tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Tap It To Win</td><td>4/1</td><td>+400</td></tr><tr><td>Sole Volante</td><td>12/1</td><td>+1200</td></tr><tr><td>Max Player</td><td>10/1</td><td>+1000</td></tr><tr><td>Modernist</td><td>20/1</td><td>+2000</td></tr><tr><td>Farmington Road</td><td>16/1</td><td>+1600</td></tr><tr><td>Fore Left</td><td>13/1</td><td>+1300</td></tr><tr><td>Jungle Runner</td><td>15/1</td><td>+1500</td></tr><tr><td>Tiz The Law</td><td>6/5</td><td>+120</td></tr><tr><td>Dr Post</td><td>8/1</td><td>+800</td></tr><tr><td>Pneumatic</td><td>20/1</td><td>+2000</td></tr></tbody>
		<tfoot>
						<tr>
						<td class="dateUpdated center" colspan="3">
							<em id='updateemp'>  - Updated July 2, 2020 02:30:05 </em>
						</td>
					</tr>
			</tfoot>
		</table>
	</div>
{literal}
        <style>
            table.ordenable tbody th{cursor: pointer;}
			.table-bordered > thead > tr th { background:#1571BA !important; border: 1px solid #ccc; border-right:0; border-top:0; border-bottom:0;color:#fff; }
			.table-bordered > tbody > tr td, .table-bordered > tbody > tr th { text-align: center !important; }
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
        {/literal}

	