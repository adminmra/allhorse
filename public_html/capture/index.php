<?php	 		 	
//Bet Online Splash
if(isset($_POST['action']) && $_POST['action'] == 'betonline'){
	$list = array (
		array($_POST['first_name'], $_POST['last_name'], $_POST['email'], '', '', 'Kentucky Derby', 'betonline.com')
	);

	$fp = fopen('email_capture.csv', 'a');

	foreach ($list as $fields) {
		fputcsv($fp, $fields);
	}

	fclose($fp);

	header("Location: http://www.betonline.ag/join?btag=a_1890b_844c_&affid=1691");
	exit;
}
//Everybody Plays
else if(isset($_POST['action']) && $_POST['action'] == 'everybodyplays')
{
	$list = array (
		array( $_POST['name'],$_POST['email'], $_POST['state'],  'everybodyplays.com', date('Y-m-d H:i:s'))
	);

	$fp = fopen('everybodyplays.csv', 'a');

	foreach ($list as $fields) {
		fputcsv($fp, $fields);
	}

	fclose($fp);

	header("Location: http://www.everybodyplays.com?thanks");
	exit;
}
//US Racing
else if(isset($_POST['action']) && $_POST['action'] == 'usracing')
{
	$list = array (
		array( $_POST['name'],$_POST['email'], $_POST['state'],  'usracing.com', date('Y-m-d H:i:s'))
	);

	$fp = fopen('usracing.csv', 'a');

	foreach ($list as $fields) {
		fputcsv($fp, $fields);
	}

	fclose($fp);

	header("Location: http://www.usracing.com/join?thanks");
	exit;
}
//US Racing 2
else if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'usracing.com2')
{
	$list = array (
		array( $_REQUEST['email'], $_REQUEST['state'],  'usracing.com', date('Y-m-d H:i:s'))
	);

	$fp = fopen('usracing.csv', 'a');

	foreach ($list as $fields) {
		fputcsv($fp, $fields);
	}

	fclose($fp);

	echo $_REQUEST['callback'].'('.json_encode(array('action'=>'success')).')';
	exit;
}
//Capture  ****************************************
else if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'capture')
{
	$list = array (
		array( $_REQUEST['name'],$_REQUEST['email'], $_REQUEST['zipcode'],  $_REQUEST['source'], date('Y-m-d'))
	);

	$fp = fopen('usracing.csv', 'a');

	foreach ($list as $fields) {
		fputcsv($fp, $fields);
	}

	fclose($fp);
	echo $_REQUEST['callback'].'('.json_encode(array('action'=>'success')).')';
ob_start();

	// code added for the crm post
	$fields = array(
		"firstname"=>$_REQUEST['name'], 
		"lastname"=>'NA',
		"state"=>$_REQUEST['zipcode'],
		"email"=>$_REQUEST['email'],
		"website"=>$_REQUEST['source'],
		"publicid"=>"2745f379ef27bcffc615c951f2357f01",
		"name"=>"Info Site Leads"
	);
	$urlcrm = "https://usracing.od1.vtiger.com/modules/Webforms/capture.php";
	$fields_string = "";
	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	rtrim($fields_string,'&');
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL,$urlcrm);
	curl_setopt($ch,CURLOPT_POST,count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
	$result = curl_exec($ch);
	//file_put_contents("/home/ah/allhorse/public_html/capture/fer.txt", print_r($result, true) . print_r($fields, true));
	ob_end_clean();

	if ($_REQUEST['source'] == 'mybelmontbets.com') {
		ob_start();
		// CC -> Constant Contact
		$fields = array(
			"ca"=>'408bbb5e-36c1-4bcf-8be6-bf18c202a066', 
			"list"=>'1630770379',
			"source"=>'EFD',
			"required"=>'list,email',
			"first_name"=>$_REQUEST['name'],
			"email"=>$_REQUEST['email']
		);
		$urlCC = "https://visitor2.constantcontact.com/api/signup";
		$fields_string = "";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string,'&');
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$urlCC);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		$result = curl_exec($ch);
		ob_end_clean();
	} elseif ($_REQUEST['source'] == 'mypreaknessbets.com') {
		ob_start();
		// CC -> Constant Contact
		$fields = array(
			"ca"=>'130701e7-43a5-4242-a1f9-953aada171b9', 
			"list"=>'1442011241',
			"source"=>'EFD',
			"required"=>'list,email',
			"first_name"=>$_REQUEST['name'],
			"email"=>$_REQUEST['email']
		);
		$urlCC = "https://visitor2.constantcontact.com/api/signup";
		$fields_string = "";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string,'&');
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$urlCC);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		$result = curl_exec($ch);
		ob_end_clean();
	}

	exit;
}
//Belmont-stakes.info
else if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'belmont-stakes')
{
	$list = array (
		array( $_REQUEST['name'],$_REQUEST['email'], $_REQUEST['zipcode'],  'belmont-stakes.info', date('Y-m-d H:i:s'))
	);

	$fp = fopen('usracing.csv', 'a');

	foreach ($list as $fields) {
		fputcsv($fp, $fields);
	}

	fclose($fp);

	echo $_REQUEST['callback'].'('.json_encode(array('action'=>'success')).')';
	exit;
}
//Bovada Splash
else if(isset($_POST['action']) && $_POST['action'] == 'bovada')
{
	$list = array (array($_POST['first_name'], $_POST['last_name'], $_POST['email'], '', $_POST['location'])
	);

	$fp = fopen('email_capture.csv', 'a');

	foreach ($list as $fields) {
		fputcsv($fp, $fields);
	}

	fclose($fp);

	if($_REQUEST['location'] == 'US')
	{
		header("Location: http://record.bettingpartners.com/_AXhnt5_BYxyPdh4jW9iXJmNd7ZgqdRLk/0/");
		exit;
	}
	else if($_REQUEST['location'] == 'NonUS')
	{
		header("Location: http://record.bettingpartners.com/_AXhnt5_BYxxuA8hS55GHbmNd7ZgqdRLk/1/");
		exit;
	}
} // below 28 abril added 
