 {*<!--------- NEW HERO  ---------------------------------------->*}
 
{literal}
  <style>
    @media (min-width: 1024px) {
      .usrHero {
        background-image: url(/img/states/horse-racing.jpg);
      }
    }

    @media (min-width: 481px) and (max-width: 1023px) {
      .usrHero {
        background-image: url(/img/states/horse-racing-sm.jpg);
      }
    }

    @media (min-width: 320px) and (max-width: 480px) {
      .usrHero {
        /*background-image: url({/literal} {$hero_sm} {literal});*/
        display: none;
      }
    }

    @media (max-width: 479px) {
      .break-line {
        display: none;
      }
    }

    @media (min-width: 320px) and (max-width: 357px) {
      .fixed_cta {
        font-size: 13px !important;
      }
    }
  </style>
{/literal}



<div class="usrHero" alt="{$state} Horse Betting">
    <div class="text text-xl">
        <span>{$h1}</span>
    </div>
    <div class="text text-md copy-md">
        <span>Get up to a {$BONUS_WELCOME} New Member Bonus</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">Sign Up Now</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h1}</span>
        <span>Get up to a {$BONUS_WELCOME} New Member Bonus</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">Sign Up Now</a>
    </div>
</div>

{* <!--------- NEW HERO END ---------------------------------------->  *}   
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>{*This h1 is change in the xml whit the tag h1*}
        
        
		   {if $KD_Switch_Full_Site} <h3 class="kd_subheading">Bet on the Kentucky Derby!</h3>
		   {elseif $PS_Switch_Full_Site}<h3 class="kd_subheading">Bet on the Preakness Stakes!</h3>
		   {elseif $BS_Switch_Full_Site}<h3 class="kd_subheading">Bet on the Belmont Stakes!</h3>
		   {else} <h3 class="kd_subheading">Get up to a {$BONUS_WELCOME} New Member Bonus</h3>
		   {/if}
		  
		   
        <p><a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> is the best site to bet on horses. Why?  Because, all new members will  get up to a <a href="/promos/cash-bonus-20">{$BONUS_WELCOME} bonus</a> for joining and you can also qualify for an extra  <a href="/promos/cash-bonus-150">{$BONUS_RETENTION} bonus!</a></p>
        
        
{if $KD_Switch_Full_Site}<p>When you bet on the Kentucky Derby you'll  also get a Free Bet!</p>
{elseif $PS_Switch_Full_Site}<p>When you bet on the Preakness Stakes you'll  also get a Free Bet!</p>
{elseif $BS_Switch_Race_Day}<p>When you bet on the Belmont Stakes you'll  also get a Free Bet!</p>
{else}<p>Enjoy off track horse betting with <a href="/promos/rebates">rebates</a> up to 8% on all your  horse racing wagers paid into your account the very next day. </p>
{/if}



<p>Start your {$state} horse betting  with <a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> today! </p>
      
<p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>
<hr>
<h2>{$state} Horse Racing </h2>
 