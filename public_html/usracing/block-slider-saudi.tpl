{literal}
<style>
    @media (min-width:481px) {
         .swiper-slide.saudi {
              background-image: url(/img/saudi-cup/lg-saudi-cup.jpg)
        }
        .swiper-slide.first {
            background-image: url(/img/heros-index/online-horse-betting.jpg)
        }

        .swiper-slide.second {
            background-image: url(/img/heros-index/md_hero_02.jpg)
        }

        .swiper-slide.third {
            background-image: url(/img/heros-index/horse-racing-bonus2.jpg)
        }

    }

    @media (max-width:480px) {
        .swiper-slide.saudi {
              background-image: url(/img/saudi-cup/saudi-cup-sm.jpg)
        }
        .swiper-slide.first {
            background-image: url(/img/heros-index/online-horse-betting-mobile.jpg)
        }

        .swiper-slide.second {
            background-image: url(/img/heros-index/sm_hero_02.jpg)
        }

        .swiper-slide.third {
            background-image: url(/img/heros-index/sm_hero_01.jpg)
        }
    }

    /*.sliderBtn.tp-caption.btn:active {
        position: static !important;
        margin-top: 25px !important;
    }*/
</style>
{/literal}

<div class="slider" style="opacity: 0">
    <div class="swiper-container">
        <div class="swiper-wrapper">
                    <div class="swiper-slide saudi">
                <div class="slide first">
                    <span class="large-text tp-caption txtLrg">Saudi Cup</span>
                    <span class="secondary-text txtMed tp-caption desk">New Members Get Up to $500 Cash!</span>
                    <div class="secondary-text txtMed tp-caption mobile" style="display:flex; justify-content:center">
                        <span>New Members Get Up </span>
                        <span>to $500 Cash!</span>
                    </div>
                    <a class="sliderBtn tp-caption btn btn-red" href="/signup/?ref=saudi-cup-index"><i
                            class="fa fa-thumbs-o-up left"></i>Sign Up Now</i></a>
                </div>
            </div>
            <div class="swiper-slide first">
                <div class="slide first">
                    <span class="large-text tp-caption txtLrg">Online Horse Betting</span>
                    <span class="secondary-text txtMed tp-caption desk">Bet on Over 200 Tracks!</span>
                    <div class="secondary-text txtMed tp-caption mobile" style="display:flex; justify-content:center">
                        <span>Bet on&nbsp;</span>
                        <span>Over 200 Tracks!</span>
                    </div>
                    <a class="sliderBtn tp-caption btn btn-red" href="/signup/?ref={$ref}-200-tracks"><i
                            class="fa fa-thumbs-o-up left"></i>Sign Up Now</i></a>
                </div>
            </div>
            <div class="swiper-slide second">
                <div class="slide second">
                    <span class="large-text tp-caption txtLrg">Rebates Paid Daily</span>
                    <span class="secondary-text txtMed tp-caption desk">Up to 8%! The Most Anywhere!</span>
                    <div class="secondary-text txtMed tp-caption mobile" style="display:flex; justify-content:center">
                        <span>Up to 8%!&nbsp;</span>
                        <span>The Most Anywhere!</span>
                    </div>
                    <a class="sliderBtn tp-caption btn btn-red" href="/signup?ref={$ref}-rebates"><i
                            class="fa fa-thumbs-o-up left"></i>Sign Up
                        Now</i></a>
                </div>
            </div>
            <div class="swiper-slide third">
                <div class="slide third">
                    <span class="large-text tp-caption txtLrg desk">20% Instant Cash Bonus</span>
                    <div class="large-text tp-caption txtLrg mobile" style="display:flex; justify-content:center">
                        <span>20% Instant&nbsp;</span>
                        <span>Cash Bonus</span>
                    </div>
                    <span class="secondary-text txtMed tp-caption">and Qualify for Another $150!</span>
                    <a class="sliderBtn tp-caption btn btn-red" href="/signup?ref={$ref}-instant-cash"><i
                            class="fa fa-star left"></i>Sign Up Now</i></a>
                </div>
            </div>
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next hide-arrow"></div>
        <div class="swiper-button-prev hide-arrow"></div>
    </div>
</div>
{literal}
<script>
    $(window).load(function () {
        var swiper = new Swiper('.swiper-container', {
            effect: 'fade',
            slidesPerView: 1,
            loop: true,
            lazy: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            }
        });

        setInterval(function () {
            var $sample = $(".slider");
            if ($sample.is(":hover")) {
                $(".swiper-button-next").removeClass("hide-arrow");
                $(".swiper-button-prev").removeClass("hide-arrow");
            } else {
                $(".swiper-button-next").addClass("hide-arrow");
                $(".swiper-button-prev").addClass("hide-arrow");
            }
        }, 200);

        $(".slider").animate({
            opacity: 1
        }, 1200, function () {});
    });
</script>
{/literal}