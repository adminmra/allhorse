    <section class="bc-card undefined">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/index/icon-online-sports-betting.png" alt="" class="bc-card_icon">
          
          {if $PegasusBetonFootSwitch} 
			<h2 class="bc-card_heading">Bet on Football</h2>
			<h3 class="bc-card_subheading">Anytime and Anywhere!</h3>
			<p>Not only can you bet on horses but  also sports with:
			<br><br>Live In-Game Betting
			<br>Best Player and Team Propositions
			<br> Complete Baseball Betting Odds
			<br>Fights, Politics and Entertainment Bets
			<br>Bet Anywhere With Your Mobile Phone
		{else}
			<h2 class="bc-card_heading">Bet on Football</h2>
			<h3 class="bc-card_subheading">Anytime and Anywhere!</h3>
			<p>Not only can you bet on horses but  also sports with:
			<br><br>Live In-Game Betting
			<br>Best Player and Team Propositions
			<br> Complete Baseball Betting Odds
			<br>Fights, Politics and Entertainment Bets
			<br>Bet Anywhere With Your Mobile Phone
		{/if}


</p><a href="/signup?ref={$ref}" class="bc-card_btn-red">Sign Me Up!</a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img data-src="/img/index/online-sports-betting.jpg" data-src-img="/img/index/online-sports-betting.jpg" alt="Online Sports Betting" class="bc-card_img lazyload"></div>
      </div>
    </section>