 {*<!--------- NEW HERO  ---------------------------------------->*}
 
{literal}
  <style>
    @media (min-width: 1024px) {
      .usrHero {
        background-image: url(/img/states/horse-racing.jpg);
      }
    }

    @media (min-width: 481px) and (max-width: 1023px) {
      .usrHero {
        background-image: url(/img/states/horse-racing-sm.jpg);
      }
    }

    @media (min-width: 320px) and (max-width: 480px) {
      .usrHero {
        /*background-image: url({/literal} {$hero_sm} {literal});*/
        display: none;
      }
    }

    @media (max-width: 479px) {
      .break-line {
        display: none;
      }
    }

    @media (min-width: 320px) and (max-width: 357px) {
      .fixed_cta {
        font-size: 13px !important;
      }
    }
  </style>
{/literal}



<div class="usrHero" alt="{$h1}">
    <div class="text text-xl">
        <span>{$h1}</span>
    </div>
    <div class="text text-md copy-md">
        <span>Get up to {$BONUS_WELCOME} welcome bonus!</span>
        
        <a class="btn btn-red" href="/signup?ref={$ref}">Bet Now</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h1}</span>
        <span>Get up to {$BONUS_WELCOME} welcome bonus</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">Sign Up Now</a>
    </div>
</div>

{* <!--------- NEW HERO END ---------------------------------------->  *}   

 <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
       
		   {if $KD_Switch_Full_Site} 
		   <span class="kd_heading">Bet on the Kentucky Derby!</span>
		   		
		   {elseif $PS_Switch_Full_Site}
		   <span class="kd_heading">Bet on the Preakness Stakes!</span>
		   		
		   {elseif $BS_Switch_Full_Site}
		   <span class="kd_heading">Bet on the Belmont Stakes!</span>
		   	
		   {else} <span class="kd_heading">Bet on Horses</span>
		   		
		   {/if}
		    <h3 class="kd_subheading">Get up to a {$BONUS_WELCOME} New Member Bonus</h3>
        <p><a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> is the best site to bet on horses. Why?  Because, all new members will  get up to a <a href="/promos/cash-bonus-20">{$BONUS_WELCOME} bonus</a> for joining and you can also qualify for an extra  <a href="/promos/cash-bonus-150">{$BONUS_RETENTION} bonus!</a> </p>
        
{if $KD_Switch_Full_Site}<p>When you bet on the Kentucky Derby you'll  also get a Free Bet!</p>
{elseif $PS_Switch_Full_Site}<p>When you bet on the Preakness Stakes you'll  also get a Free Bet!</p>
{elseif $BS_Switch_Race_Day}<p>When you bet on the Belmont Stakes you'll  also get a Free Bet!</p>
{else}{* <p>Enjoy off track horse betting with <a href="/promos/rebates">rebates</a> up to 8% on all your  horse racing wagers paid into your account the very next day. </p> *}
{/if}

<p>Start your  horse betting  with <a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> today! </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>        
<hr>
 