 <section class=" bc-countdown  bc-countdown-belmont usr-section">
      <div class="container">
          <h3 class="bc-countdown_heading"><span class="bs-countdown_heading-text">&nbsp;Countdown <br>to Breeders' Cup&nbsp; Classic</span></h3>
          <div class="bc-countdown_items">
            <div class="bc-countdown_item bc-countdown_item--green clock_days">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_days"></canvas>
              </div>
              <div class="text"><span class="bc-countdown_num val">0</span><span class="bc-countdown_label time type_days">days</span></div>
            </div>
            <div class="bc-countdown_item bc-countdown_item--green clock_hours">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_hours"></canvas>
              </div>
              <div class="text"><span class="bc-countdown_num val">0</span><span class="bc-countdown_label time type_hours">hours</span></div>
            </div>
            <div class="bc-countdown_item bc-countdown_item--green clock_minutes">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_minutes"></canvas>
              </div>
              <div class="text"><span class="bc-countdown_num val">0</span><span class="bc-countdown_label time type_minutes">minutes</span></div>
            </div>
            <div class="bc-countdown_item bc-countdown_item--green clock_seconds">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_seconds"></canvas>
              </div>
              <div class="text"><span class="bc-countdown_num val">0</span><span class="bc-countdown_label time type_seconds">seconds</span></div>
            </div>
          </div>
      </div>
    </section>
      {php}
      date_default_timezone_set('America/Kentucky/Louisville');
      $startDate = strtotime("now");
      $endDate  = strtotime("3 Nov 2018 17:44:00");
    {/php}

    {literal}
    <script type="text/javascript">
      $(document).ready(function(){
        JBCountDown({
          secondsColor : "#ffdc50",
          secondsGlow  : "none",
          minutesColor : "#9cdb7d",
          minutesGlow  : "none", 
          hoursColor   : "#378cff", 
          hoursGlow    : "none",  
          daysColor    : "#ff6565", 
          daysGlow     : "none", 
          startDate   : "{/literal}{php} echo $startDate; {/php}{literal}", 
          endDate : "{/literal}{php} echo $endDate; {/php}{literal}", 
          now : "{/literal}{php} echo strtotime('now'); {/php}{literal}" 
        });
      });
    </script>
    {/literal}
