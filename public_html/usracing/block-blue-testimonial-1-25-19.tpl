    <section class="bs-testimonials--blue usr-section">
      <div class="container">
        <blockquote class="bs-testimonial-blue">
          <figure class="bs-testimonial-blue_figure">{* <img src="/img/index/laura-pugh.jpg" alt="Laura Pugh, Feature US Racing Writer" class="bs-testimonial-blue_img"> *}</figure>
          <div class="bs-testimonial-blue_content">
            <p class="bs-testimonial-blue_p">When it comes to  horse betting, BUSR has you covered. They provide many unique betting options, more than anybody else.</p>{*
<span class="bs-testimonial-blue_name"> Laura Pugh, <br>
              <cite class="bs-testimonial-blue_profile">Feature Writer for US Racing, Lady and the Track & Horse Racing Nation.</cite></span>
*}
          </div>
        </blockquote>
      </div>
</section>