<div class="container-fluid as-seen-on">
    <div class="row">
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="espn card_4" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="sportsillustrated card_13" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="usatoday card_15" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="bloombergmarkets card_1" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="bloombergtv card_2" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="forbes card_5" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
    </div>
    <div class="row">
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="marketwatch card_9" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-3">
            <a>
                <img class="bleacherreport card_" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="laweekly card_8" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-6 col-sm-3">
            <a>
                <img class="sporttechie card_14" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-6  col-sm-2">
            <a>
                <img class="seekingalpha card_16" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAAA5AQMAAAAlXqRkAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABdJREFUeNpjYBgFo2AUjIJRMApGAdUAAAdZAAFwryL9AAAAAElFTkSuQmCC">
            </a>
        </div>
    </div>
</div>