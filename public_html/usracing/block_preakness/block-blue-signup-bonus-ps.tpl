<section class="bs-bonus"><div class="container"><div class="row"><div class="col-xs-12 col-sm-8 col-md-10 col-lg-8">
<h2 class="bs-bonus_heading">{$BONUS_WELCOME} Preakness Stakes Bonus</h2>
<h3 class="bs-bonus_subheading">Exceptional New Member Reward for the Preakness Stakes</h3>
<p>{* Use the promocode <strong>KENTUCKY</strong>  and receive*}Get up to {$BONUS_WELCOME} free! How does it work? You'll receive  a {$BONUS_WELCOME_PERCENTAGE} cash bonus  on your first deposit of $100 or more. The more you deposit, the bigger the bonus! {*  It doesn't get any better with BUSR. *}  Join today!</p><a href="/signup?ref={$ref}" rel="nofollow" class="bs-bonus_btn">Sign Me up!</a>
</div></div></div></section>

{*
    <section class="bs-bonus">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8 col-md-10 col-lg-8">
            <h2 class="bs-bonus_heading">Preakness Stakes Sign Up Bonus</h2>
            <h3 class="bs-bonus_subheading">You get a 10% bonus on your first deposit added automatically. Exceptional New Member Bonuses</h3>
            
           <p>For your first deposit with BUSR, you'll get an additional 10% cash bonus added to your account, absolutely free. No Limits!</p>

<p>Deposit a minimum of $100 and you can qualify to earn an additional $150!  It doesn't get any better. Join today. </p><a href="/signup?ref={$ref}" rel="nofollow" class="bs-bonus_btn"> CLAIM YOUR BONUS</a>
          </div>
        </div>
      </div>
    </section>
*}
{*
    <section class="bs-bonus">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8 col-md-10 col-lg-8">
            <h2 class="bs-bonus_heading">Preakness Stakes Sign Up Bonus</h2>
            <h3 class="bs-bonus_subheading">Get a bonus up to {include file="/home/ah/allhorse/public_html/common/bonus-welcome.php"} on your first deposit with BUSR!</h3>
         </p><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Get your Cash Today</a>
          </div>
        </div>
      </div>
    </section>
*}