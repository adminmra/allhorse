
      {literal}
    <style>
    .background{
        background-image: url("/img/index-ps/bg-pimlico.jpg");
        background-size: contain;
        background-repeat: no-repeat;
        background-position: 0% 110%;
    }

    /* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  .background{
    background-position: 0% 110%;
  }
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  .background{
    background-position: 0% 100%;
  }
}

/* 
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) {
  .background{
    background-position: 0% 100%;
  }
}
/*
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 767px) {
  .background{
    background-position: 0% 90%;
  }
}

/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  .background{
    background-position: 0% 90%;
  }
}
    
    </style>
    {/literal}
    
        <section class="ps-kd ps-kd--default usr-section background">
      <div class="container">
        <div class="ps-kd_content">
          <img src="/img/index-ps/preakness-stakes-icon.png" alt="Online Horse Betting" class="ps-kd_logo img-responsive">
          <h1 class="kd_heading">Horse Betting</h1>
           <h3 class="bs-kd_subheading">{* With the Most Preakness Stakes Bet Options Anywhere *}Bet Now and up to {$BONUS_WELCOME} Signup Bonus and a Free Bet</h3>
         <p><a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> is the best site to place your bet on the   <a href="/preakness-stakes/betting"> {$PREAKNESS_YEAR} Preakness Stakes</a>.  Why?  Because, all new members can get up to a <a href="/cash-bonus-20">{$BONUS_WELCOME} welcome bonus</a> and a <strong>free bet</strong>!</p>
         <p>You'll also get great <a href="/preakness-stakes/props">Preakness Stakes Props</a> found nowhere else online! Such as  head-to-head match races and will a horse lead wire-to-wire. </p>
         <p>With  more benefits and  bet choices than any other place online, what are you waiting for?</p>
{if $PS_Switch_Race_Day}
         <p>{include_php file='/home/ah/allhorse/public_html/ps/odds.php'}</p>
<a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg">Bet Now</a> </p> 

{else}
         <p><a href="/preakness-stakes/betting"  class="btn-xlrg">See the Latest Preakness Odds</a>  
{/if}         
        </div>
      </div>
      </div>
    </section>
