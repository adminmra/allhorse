    <section class="card">
      <div class="card_wrap">
        <div class="card_half card_content">
	        <a href="/promos/free-bet">
	        <img src="/img/index-kd/icon-kentucky-derby-betting.png" alt="Bet on Preaknes stakes" class="card_icon ">
	        </a>
          <h2 class="card_heading">Get a Free Bet</h2>
          <h3 class="card_subheading">New Members get a free $10 Bet </h3>
          <p>As a member, you'll can qualify for Free $10 Bet on the Preakness. When you sign up and make make your first deposit at BUSR, you will receive a Free $10 Bet to place on the Preakness Stakes.</p>
          <p>You can only win with <i>BUSR</i>!</p><a href="/promos/free-bet" class="card_btn">Learn More</a>
        </div>
        <div class="card_half card_hide-mobile"><a href="/promos/free-bet"><img data-src="/img/index-kd/kentucky-derby-betting.jpg" data-src-img="/img/index-kd/kentucky-derby-betting.jpg" alt="Preakness Stakes betting" class="card_img lazyload"></a></div>
      </div>
    </section>