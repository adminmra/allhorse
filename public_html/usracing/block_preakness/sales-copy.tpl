<p><a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> is the best site to place your bet on the   <a href="/preakness-stakes/betting"> {$PREAKNESS_YEAR} Preakness Stakes</a>. </p><p> Why?  Because, all new members can get up to a <a href="/cash-bonus-20">{$BONUS_WELCOME} welcome bonus!</a>  You'll also get a <strong>free bet</strong>{*Change to a link when page is ready*} for the big race and great <a href="/preakness-stakes/props">Preakness Stakes Props</a> found nowhere else online!</p>

{if $PS_Switch_Early || $PS_Switch_Main || $PS_Switch_Race_Date}
<p>So what are you waiting for? <a href="/preakness-stakes/odds">Preakness Stakes Odds are live.</a></p>
{/if}

{*
{if $smarty.server.HTTP_HOST == "www.betusracing.ag" or $smarty.server.HTTP_HOST == "dev.betusracing.ag"} 
{assign var="triplecrown" value='/horse-betting/triple-crown'} 
{else}
{assign var="triplecrown" value='/bet-on/triple-crown'} 
{/if}
*}

{* <p><strong><i>BUSR</i></strong> is the best place to  bet on the second leg of the Triple Crown, the <a href="/preakness-stakes/betting">Preakness Stakes</a>. The latest <a href="/preakness-stakes/odds">Preakness Stakes Odds</a> are live. *} {*Will Mike Smith and Justify go on to win the Preakness Stakes and then the Preakness Stakes?*}<br>
{*
  <br>
You'll also get great fixed odds with amazing payouts on the leading Preakness Stakes horses.  Bet on the prop wagers such as  margin of victory, head-to-head matchups and more.</p>
*}
{* <p> Place your bets on our <a href="{$triplecrown}">Triple Crown Odds</a>, watch and win! *}