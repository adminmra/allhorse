   <section class="ps-countdown ps-countdown--ps usr-section">
      <div class="container">
        <div class="ps-countdown_content">
          <h3 class="ps-countdown_heading"><span class="ps-countdown_heading-text">Countdown <br>to the Preakness Stakes</span></h3>
          <div class="ps-countdown_items">
            <div class="ps-countdown_item ps-countdown_item--yellow clock_days">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_days"></canvas>
              </div>
              <div class="text"><span class="ps-countdown_num val">0</span><span class="ps-countdown_label time type_days">days</span></div>
            </div>
            <div class="ps-countdown_item ps-countdown_item--yellow clock_hours">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_hours"></canvas>
              </div>
              <div class="text"><span class="ps-countdown_num val">0</span><span class="ps-countdown_label time type_hours">hours</span></div>
            </div>
            <div class="ps-countdown_item ps-countdown_item--yellow clock_minutes">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_minutes"></canvas>
              </div>
              <div class="text"><span class="ps-countdown_num val">0</span><span class="ps-countdown_label time type_minutes">minutes</span></div>
            </div>
            <div class="ps-countdown_item ps-countdown_item--yellow clock_seconds">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_seconds"></canvas>
              </div>
              <div class="text"><span class="ps-countdown_num val">0</span><span class="ps-countdown_label time type_seconds">seconds</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    {php}
      $startDate = strtotime("now");
      $endDate  = strtotime("16 May 2020 18:48:00");
    {/php}

    {literal}
    <script type="text/javascript">
      $(document).ready(function(){
        JBCountDown({
          secondsColor : "#ffdc50",
          secondsGlow  : "none",
          minutesColor : "#9cdb7d",
          minutesGlow  : "none", 
          hoursColor   : "#378cff", 
          hoursGlow    : "none",  
          daysColor    : "#ff6565", 
          daysGlow     : "none", 
          startDate   : "{/literal}{php} echo strtotime('now'); {/php}{literal}", 
          endDate : "{/literal}{php} echo strtotime('16 May 2020 18:48:00'); {/php}{literal}", 
          now : "{/literal}{php} echo strtotime('now'); {/php}{literal}" 
        });
      });
    </script>
    {/literal}
    
    