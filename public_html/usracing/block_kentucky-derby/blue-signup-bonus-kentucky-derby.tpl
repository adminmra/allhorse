<section class="bs-bonus"><div class="container"><div class="row"><div class="col-xs-12 col-sm-8 col-md-10 col-lg-8">
<h2 class="bs-bonus_heading">{$BONUS_WELCOME} Kentucky Derby Bonus</h2>
<h3 class="bs-bonus_subheading">Exceptional New Member Reward for the Kentucky Derby</h3>
<p>{* Use the promocode <strong>KENTUCKY</strong>  and receive*}Get up to {$BONUS_WELCOME} free! How does it work? You'll receive  a {$BONUS_WELCOME_PERCENTAGE} cash bonus  on your first deposit of $100 or more. The more you deposit, the bigger the bonus! {*  It doesn't get any better with BUSR. *}  Join today!</p><a href="/signup?ref={$ref}" rel="nofollow" class="bs-bonus_btn" data-ref-val="block-signup-bonus-kd-CTA">Join Now!</a>
</div></div></div></section>

