{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}
{include file="/home/ah/allhorse/public_html/kd/schema.tpl"}
{include file='inc/left-nav-btn.tpl'}<div id="left-nav">{include file='menus/kentuckyderby.tpl'}</div>
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-hero.tpl"}    
{* {include file="/home/ah/allhorse/public_html/usracing/as_seen_on.tpl"} *}
{*OLD Hero*} {*
  {literal}
    <style>
      @media (min-width: 992px) {
        .newHero {
          background-image: url({/literal}{$hero}{literal});
        }
      }
    </style>
  {/literal}
  <div class="newHero hidden-sm hidden-xs" alt="{$hero_alt}">
  <div class="text text-xl">{$h1}</div>
  <div class="text text-xl" style="margin-top:0px;"></div>
  <div class="text text-md">{if $KD_Switch_Phase_2-5}Signup Today and Get a Free Bet!{else}{$h2}{/if <br><a href="/signup?ref={$ref}">
    <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i> {$signup_cta}</div> </a> </div>
</div>*}{*end hero*}
{include file="/home/ah/allhorse/public_html/kd/block-countdown-new.tpl"}

    <section class="kd usr-section">
      <div class="container">
        <div class="kd_content">
           <a href="/signup?ref={$ref}" rel="nofollow" data-ref-val="kd-logo"><img src="/img/index-kd/kd.png" alt="Kentucky Derby Betting" class="kd_logo img-responsive"></a>
          <h1 class="kd_heading">{$h1}</h1>
          <h3 class="kd_subheading">
	          {if $KD_Switch_Early}{$h2_early}
	          {elseif $KD_Switch_Main}{$h2_main}
	          {elseif $KD_Switch_Race_Day}{$h2_race_day}
	          {else}{$h2}{/if}
	          </h3>
          
{if $salescopy=="oaks"}{*Determined by the page, eg. KO/odds vs KD/odds -tf*}
<p>{include file='/home/ah/allhorse/public_html/usracing/block_kentucky-derby/sales-copy-oaks.tpl'} </p>
{else}
<p>{include file='/home/ah/allhorse/public_html/usracing/block_kentucky-derby/sales-copy.tpl'} </p>
{/if}
<p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on Kentucky Derby</a></p>