        <section class="card">
          <div class="card_wrap">

            <div class="card_half card_content">
              <a href="/kentucky-derby/jockey-betting">
                {* <img src="/img/index-kd/icon-bet-kentucky-derby-jockeys.png" alt="Kentucky Derby Odds:  Jockeys"
                  class="card_icon"> *}
                <img class="icon-bet-kentucky-derby-jockeys card_icon" alt="Kentucky Derby Odds:  Jockeys"
                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkAQMAAABKLAcXAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRQE8AAAV4AAHYKaK4AAAAAElFTkSuQmCC">
              </a>
              <h2 class="card_heading">Bet on JOCKEYS</h2>
              <h3 class="card_subheading">During the {include_php file='/home/ah/allhorse/public_html/kd/running.php'}
                Kentucky Derby</h3>
              <p>Sure, anyone can offer Kentucky Derby Odds but what about making a bet on a jockey?
                <p> BUSR offers more wagers on the Kentucky Derby than any other horse betting
                  site.</p><a href="/kentucky-derby/jockey-betting" class="btn-xlrg fixed_cta">See the Jockey Odds</a>
            </div>

            <div class="card_half card_hide-mobile">
              <a href="/kentucky-derby/jockey-betting">
                <img data-src="/img/index-kd/bet-kentucky-derby-jockeys.jpg" data-src-img="/img/index-kd/bet-kentucky-derby-jockeys.jpg"
                  alt="Kentucky Derby Odds:  Jockeys" class="card_img lazyload"></a></div>
          </div>
        </section>