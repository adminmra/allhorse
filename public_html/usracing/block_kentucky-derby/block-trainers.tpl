    <section class="card">
      <div class="card_wrap">
        <div class="card_half card_hide-mobile">
	        <a href="/kentucky-derby/trainer-betting">
		        <img data-src="/img/index-kd/bet-kentucky-derby.jpg" data-src-img="/img/index-kd/bet-kentucky-derby.jpg" alt="Kentucky Derby Odds:  Trainers"  class="card_img lazyload"></a></div>
        <div class="card_half card_content">
	        <a href="/kentucky-derby/trainer-betting">
	        {* <img src="/img/index-kd/icon-bet-kentucky-derby.png"alt="Kentucky Derby Odds:  Trainers"  class="card_icon"> *}
          <img class="icon-bet-kentucky-derby card_icon" alt="Kentucky Derby Odds:  Trainers" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFIAAABdAQMAAAAMtKMNAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABJJREFUeNpjYBgFo2AUjALqAwAEXAABxG45GAAAAABJRU5ErkJggg==">
	        </a>
          <h2 class="card_heading">Bet on the Trainers</h2>
          <h3 class="card_subheading">And Other Exclusive Kentucky Derby Props</h3>
          <p>Anybody can place a bet who will win the Kentucky Derby, but what about placing a bet on what Trainer will win the Derby? How about  Bob Baffert or Todd Pletcher?</p>
          <p>Additional prop bets are available like the margin of victory, the winning time vs Secretariat, a Triple Crown contention and more. Kentucky Derby Odds are live, bet now!</p><a href="/kentucky-derby/trainer-betting" class="btn-xlrg fixed_cta">See the Trainer Odds</a>{* class="card_btn" *}
        </div>
      </div>
    </section>
    
 
