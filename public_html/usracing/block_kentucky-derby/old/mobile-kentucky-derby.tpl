    <section class="bc-card bc-card-mobile">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index/mobile-horse-betting.png" alt="Mobile Horse Betting" class="bc-card_img">
        </div>
        <div class="bc-card_half bc-card_content"><img src="/img/index/icon-mobile-horse-betting.png" alt="Mobile Horsed Betting Icon" class="bc-card_icon">
          <h2 class="bc-card_heading">Bet the Kentucky Derby on Your Phone!{* the go! *}</h2>
          <p>Take  the races with you! Bet on your phone or tablet  at the track, at home, everywhere.</p> <p>Are you out at the bar with your friends? Bet on the game anytime, anywhere.  Or, while waiting for your friends to arrive,  how about playing a few hands of blackjack or roulette! <p> BUSR - super easy and super fun.</p><a href="/mobile-horse-betting" class="bc-card_btn-red">See Your Mobile Betting Options</a>
        </div>
      </div>
    </section>