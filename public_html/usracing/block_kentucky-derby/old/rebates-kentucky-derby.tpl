  <section class="bs-card">

      <div class="bs-card_wrap">

        <div class="bs-card_half bs-card_content"><img src="/img/index/icon-horse-betting-rebates.png" alt="Horse Betting Rebates Icon" class="bs-card_icon">

          <h2 class="bs-card_heading">Up to 8% Horse Racing Rebates</h2>

          <h3 class="bs-card_subheading">Paid Daily Into Your Account</h3>

          <p>Every day when you log into your account you will find up to 8%  on your exotic wagers and a 3% rebate on Win, Place and Show in your account from yesterday's bets on select tracks.</p>

          <p>You can only win with BUSR!</p><a href="/rebates?ref={$ref}" class="bs-card_btn">Learn More About Rebates</a>

        </div>

        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index/horse-betting-rebates.jpg" alt="Horse Betting Rebates" class="bs-card_img"></div>

      </div>

    </section>