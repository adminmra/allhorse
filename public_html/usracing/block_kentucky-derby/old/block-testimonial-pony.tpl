  <section class="testimonials--blue usr-section">
      <div class="container">
        <blockquote class="testimonial-blue">
          <figure class="testimonial-blue_figure"><img src="/img/index-kd/horseracingguy.jpg" alt="Horse Racing Dudes" class="testimonial-blue_img"></figure>
          <div class="testimonial-blue_content">
            <p class="testimonial-blue_p">Bet on your Favorite Trainer to win the Kentucky Derby at BUSR!  Sure, anybody can bet the Kentucky Derby Future Wagers but what about making a bet on a horse trainer like Bob Baffert or Todd Pletcher? <br>Why Not!?</p><span class="testimonial-blue_name">
               
               - The Racing Dudes<br>
              </span>
          </div>
        </blockquote>
      </div>
    </section>