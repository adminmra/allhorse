    <section class="bs-testimonials block-center usr-section">
      <div class="block-center_content">
        <div class="container">
          <blockquote class="bs-testimonial">
            <figure class="bs-testimonial_figure"><img src="/img/index/derek-simon.jpg" alt="Derek Simon, Senior Editor, US Racing" class="bs-testimonial_img"></figure>
            <div class="bs-testimonial_content">
              <p class="bs-testimonial_p">At BUSR, in addition to all the traditional Kentucky Derby horse bets, you can bet on individual Derby horse match-ups, sports and awesome casino games right from your your phone!</p><span class="bs-testimonial_name">Derek Simon</span><span class="bs-testimonial_profile">Senior Editor and  Handicapper  at US Racing.</span>
            </div>
          </blockquote>
        </div>
      </div>
    </section>
