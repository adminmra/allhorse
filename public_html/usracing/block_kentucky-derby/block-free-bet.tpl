    <section class="card">
      <div class="card_wrap">
        <div class="card_half card_content">
	        <a href="/kentucky-derby/free-bet">
	        {* <img src="/img/index-kd/icon-kentucky-derby-betting.png" alt="Bet on Kentucky Derby" class="card_icon"> *}
          <img class="icon-kentucky-derby-betting card_icon" alt="Bet on Kentucky Derby" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABVAQMAAADUnuB8AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABFJREFUeNpjYBgFo2AUDHIAAAOnAAHnt6kCAAAAAElFTkSuQmCC">
	        </a>
          <h2 class="card_heading">Get a Free Bet</h2>
          <h3 class="card_subheading">All Members get a free $10   Bet </h3>
          <p>As a member, you'll can qualify for a guaranteed no-lose bet.  If you win your bet, great! If you lose the wager, your $10 will be returned to your account.</p>
          <p>You can only win with BUSR!</p><a href="/kentucky-derby/free-bet" class="btn-xlrg fixed_cta">Learn More</a>
        </div>
        <div class="card_half card_hide-mobile"><a href="/kentucky-derby/free-bet"><img  data-src="/img/index-kd/kentucky-derby-betting.jpg" data-src-img="/img/index-kd/kentucky-derby-betting.jpg" alt="Kentucky Derby Betting" class="card_img lazyload"></a></div>
      </div>
    </section>
