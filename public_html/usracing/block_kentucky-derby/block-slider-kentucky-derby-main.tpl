{literal}
<style>
    @media (min-width:481px) {
        .swiper-slide.first {
            background-image: url(/img/heros-index/bet-on-the-kentucky-derby.jpg)
        }

        .swiper-slide.second {
            background-image: url(/img/heros-index/md_hero_02.jpg)
        }

        .swiper-slide.third {
            background-image: url(/img/heros-index/kentucky-derby-odds.jpg)
        }
    }

    @media (max-width:480px) {
        .swiper-slide.first {
            background-image: url(/img/heros-index/bet-on-the-kentucky-derby-mobile.jpg)
        }

        .swiper-slide.second {
            background-image: url(/img/heros-index/sm_hero_02.jpg)
        }

        .swiper-slide.third {
            background-image: url(/img/heros-index/kentucky-derby-odds-mobile.jpg)
        }
    }

    /*.sliderBtn.tp-caption.btn:active {
        position: static !important;
        margin-top: 25px !important;
    }*/
</style>
{/literal}

<div class="slider" style="opacity: 0">
    <div class="swiper-container">
        <div class="swiper-wrapper">


{if $KO_Switch_KO_Race_Day}     {*KO Day - tf************************************}      
<div class="swiper-slide first">
				<div class="slide first">
					<span class="large-text tp-caption txtLrg desk">Online Horse Betting</span>
					<div class="large-text tp-caption txtLrg mobile" style="display:flex; justify-content:center">
                        <span>Online Horse Betting</span>
					</div>
					<span class="secondary-text txtMed tp-caption desk">Bet the Kentucky Oaks & Derby</span>
					<div class="secondary-text txtMed tp-caption mobile" style="display:flex; justify-content:center">
						<span>Bet the</span>
                        <span>Kentucky Oaks & Derby</span>
					</div>
					<a class="sliderBtn tp-caption btn btn-red" href="/signup?ref={$ref}"><i
							class="fa fa-thumbs-o-up left"></i>Bet Now!</i></a>
				</div>
			</div>			           
           
{else}          {*Defaults to odds - tf********************************}
           
            <div class="swiper-slide first">
                <div class="slide first">
                    <span class="large-text tp-caption txtLrg desk">Online Horse Betting</span>
                    <div class="large-text tp-caption txtLrg mobile column" style="display:flex; justify-content:center">
       
                        <span>Online Horse Betting</span>
                    </div>
                    <span class="secondary-text txtMed tp-caption desk">Bet the Kentucky Derby Today!</span>
                    <div class="secondary-text txtMed tp-caption mobile column" style="display:flex; justify-content:center">
                        <span>Bet the</span>
                        <span>Kentucky Derby Today!</span>
                    </div>
 
	{if $KD_Switch_Full_Site} {*Hard sell one week out - tf*}{*nested*}
                     <a class="sliderBtn tp-caption btn btn-red" href="/signup?ref={$ref}-exclusive-odds"><i
                            class="fa fa-thumbs-o-up left"></i>Bet Now!</i></a>
    {else}
  					<a class="sliderBtn tp-caption btn btn-red" href="/kentucky-derby/odds"><i
                            class="fa fa-thumbs-o-up left"></i>See the Odds</i></a>
	{/if}                                
                </div>
            </div>
 
 {/if} 
            <div class="swiper-slide third">
                <div class="slide third">
                    <span class="large-text tp-caption txtLrg desk">Get up to a {$BONUS_WELCOME}  Bonus</span>
                    <div class="large-text tp-caption txtLrg mobile column" style="display:flex; justify-content:center; flex-direction: column;">
					    <span>Get up to a &nbsp;</span>
                        <span>{$BONUS_WELCOME} Welcome Bonus</span>
                    </div>
                    <span class="secondary-text txtMed tp-caption">and Qualify for Another $150!</span>
                    <a class="sliderBtn tp-caption btn btn-red" href="/signup?ref={$ref}-instant-cash"><i
                            class="fa fa-star left"></i>Sign Up Now</i></a>
                </div>
            </div>            
            <div class="swiper-slide second">
                <div class="slide second">
                    <span class="large-text tp-caption txtLrg">Get a Free Bet!</span>
                    <span class="secondary-text txtMed tp-caption desk">When you Bet on the Kentucky Derby</span>
                    <div class="secondary-text txtMed tp-caption mobile column" style="display:flex; justify-content:center">
                        <span>When you Bet on&nbsp;</span>
                        <span>the Kentucky Derby</span>
                    </div>
                    <a class="sliderBtn tp-caption btn btn-red" href="/signup/?ref={$ref}-free-bet"><i
                            class="fa fa-thumbs-o-up left"></i> Sign Up
                        Now</i></a>
                </div>
            </div>

        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next hide-arrow"></div>
        <div class="swiper-button-prev hide-arrow"></div>
    </div>
</div>
{literal}
<script>
    $(window).load(function () {
        var swiper = new Swiper('.swiper-container', {
            effect: 'fade',
            slidesPerView: 1,
            loop: true,
            lazy: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            }
        });

        setInterval(function () {
            var $sample = $(".slider");
            if ($sample.is(":hover")) {
                $(".swiper-button-next").removeClass("hide-arrow");
                $(".swiper-button-prev").removeClass("hide-arrow");
            } else {
                $(".swiper-button-next").addClass("hide-arrow");
                $(".swiper-button-prev").addClass("hide-arrow");
            }
        }, 200);

        $(".slider").animate({
            opacity: 1
        }, 1200, function () {});
    });
</script>
{/literal}