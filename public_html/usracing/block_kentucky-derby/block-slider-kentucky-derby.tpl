{include file='/home/ah/allhorse/public_html/usracing/inc-common-slider-css.tpl'} 
{*<!--=== Home Slider ===-->*}
<div class="sliderContainer fullWidth clearfix margin-bottom-30">
<div class="tp-banner-container">
<div class="tp-banner" >
<ul>
{*

<!-- slide KENTUCKY DERBY --> 

					<li data-transition="fade" data-slotamount="6" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					<img src="img/racetracks/churchill-downs/churchill-downs-online-betting.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Bet on the Kentucky Derby">
					<!-- LAYERS -->
                    <!-- Lrg Text -->
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;"><!--Kentucky Derby Futures-->Bet the Kentucky Derby
					</div>

                    <!-- Secondary Text -->

					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="0"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;">Exclusive Odds Found Nowhere Else!

					</div>

                   <!-- Button-->
					<a href="/signup" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="80"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-thumbs-o-up left"></i>Sign Up Now

					</a></li>*}

{*<!-- end slide -->*}

{*<!-- slide Preakness STAKES --> 

        			<li data-transition="fade" data-slotamount="6" data-masterspeed="1000" >

					<!-- MAIN IMAGE -->

					<img src="/img/preakness-stakes-slider.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Preakness Stakes Betting">

					<!-- LAYERS -->

                    <!-- Lrg Text -->

					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;">Bet the Preakness Stakes
					</div>

                    <!-- Secondary Text -->

					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="0"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;">Exclusive Odds Found Nowhere Else!
					</div>
					
                  <!-- Button-->
					<a href="/signup?ref=slider-preakness-stakes" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="80"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-thumbs-o-up left"></i>Sign Up Now

					</a></i>
*}

{*<!-- end slide -->*}
{*				
	<!-- slide BELMONT STAKES --> 
        			<li data-transition="fade" data-slotamount="6" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					<img src="/img/belmont-stakes-slider.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Live Horse Racing">
					<!-- LAYERS -->

                 <!-- Lrg Text -->

					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;">Bet the Belmont Stakes
					</div>

                   <!-- Secondary Text -->

					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="0"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;">Exclusive Odds Found Nowhere Else!
					</div>

                    <!-- Button-->

					<a href="/signup?ref=slider-bet-the-belmont" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="80"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-thumbs-o-up left"></i>Sign Up Now
					</a></li>                    *}

{*<!-- end slide -->*}

{*<!-- slide FIRST SLIDE HAPPY YOUNG LADIES --> *}

 					<li data-transition="fade" data-slotamount="9" data-masterspeed="1000" >

					{*<!-- MAIN IMAGE -->*}

					{* <img src="../img/new_hero/index-kentucky-derby-betting.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Horse Betting"> *}

					<img id="kd-betting-img" src="img/new_hero/index-kentucky-derby-betting.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Kentucky Derby Betting" height="600px" width="1920px">

					{*<!-- LAYERS -->*}

                    {*<!-- Lrg Text -->*}

					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;"> Online Horse Betting {* Get a Free Bet! *}
					</div>

                    {*<!-- Secondary Text -->*}

					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="0"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;"> {* No-Risk for All Members* *}Bet on  Over 200 Tracks!
					</div>

                    {*<!-- Button--> *}

					<a href="/signup/?ref=slide-rebates" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="80"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-thumbs-o-up left"></i> Sign Up Now
					</a></li>

{*<!-- end slide -->	*}

{*<!-- slide HAPPY MAN PARTY IMAGE -->   *}

					<li data-transition="fade" data-slotamount="7" data-masterspeed="1000" >

					{*<!-- IMAGE -->*}

					{*<!-- <img src="/img/online-horseracing-bonus.jpg" alt = "online horse racing" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Horse Racing Bonus">--><!-- slide HAPPY MAN --> *}

                    <img src="/img/new_hero/horse-racing-bonus2.jpg" alt = "online horse racing" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Horse Racing Bonus" height="600px" width="1920px"  />

					{*<!-- Lrg Text -->*}

					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;">{*<!--$150-->*}10% Instant Cash Bonus
					</div>

                    {*<!-- Secondary Text -->*}

					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-10"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;">and Qualify for Another $150! {*<!--Cash Back and Bonuses from US Racing-->*}
					</div>

                    {*<!-- Button -->*}
                    
					<a href="/signup?ref=slide-instant-cash" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="70"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-star left"></i> Sign Up Now
					</a>                    

                    {*<!-- Learn More Button 
						<a href="#" rel="nofollow" class="tp-caption sfl fadeout btn btn-txt"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="130"
						data-speed="400"
						data-start="1700"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
                        data-toggle="modal" data-target="#bonus"
						style="z-index: 4; font-size:2 2px;">Learn More <i class="fa fa-question-circle"></i>
					</a>-->*}
					</li>

{*<!-- end slide -->*}

{*<!-- slide FIRST SLIDE HAPPY YOUNG LADIES --> *}

 					<li data-transition="fade" data-slotamount="9" data-masterspeed="1000" >
					{*<!-- MAIN IMAGE -->*}

					{* <img src="../img/new_hero/index-kentucky-derby-betting.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Horse Betting"> *}

					{* <img src="img/new_hero/2019_kd_hero01.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Kentucky Derby Betting" height="600px" width="1920px"> *}
<img src="img/new_hero/2019_kd_hero03.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Kentucky Derby Betting" height="600px" width="1920px">
					{*<!-- LAYERS -->*}
					{*<!-- Lrg Text -->*}
                    
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;"> Bet the Kentucky Derby{* Get a Free Bet! *}
					</div>

                    {*<!-- Secondary Text -->*}

					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="0"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;"> {* No-Risk for All Members* *}Future Odds are Live for Betting!
					</div>

                    {*<!-- Button--> *}

					<a href="/kentucky-derby/odds" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="80"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-thumbs-o-up left"></i> See the Odds
					</a></li>
{*<!-- end slide -->	*}
</ul>
{*<!--<div class="tp-bannertimer"></div>-->*}
</div>{*<!-- end/tp-banner-container -->*}
</div>{*<!-- end/tp-banner -->*}
</div>{*<!-- end/SliderContainer -->*}