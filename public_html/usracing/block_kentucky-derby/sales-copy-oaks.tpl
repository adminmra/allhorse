{if $KD_Switch_Early} 

{*
<p><strong>BUSR</strong> is the best place you can <a href="/kentucky-derby/betting">bet on the Kentucky Derby</a>. Not only are over 256 different types of bets you can make, you'll get a <a href="/kentucky-derby/free-bet">FREE Bet</a>, and a <a href="/promos/cash-bonus-10">welcome bonus</a> up to $250!</p>

<p>You'll also get great fixed odds with amazing payouts on the leading <a href="/kentucky-oaks/odds">Kentucky Oaks</a> and <a href="/kentucky-derby/contenders">Derby horses</a> and even the leading <a href="/kentucky-derby/jockey-betting">Jockeys</a> and <a href="/kentucky-derby/trainer-betting">Trainers</a>.  Bet on the <a href="/kentucky-derby/margin-of-victory">margin of victory</a>, head-to-head <a href="/kentucky-derby/match-races">matchups</a>,  the <a href="/kentucky-derby/props">winning time vs Secretariat's</a> world record, a <a href="/kentucky-derby/props"> Triple Crown contention</a> and more. </p>

<p>The <a href="/kentucky-derby/odds">{include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Derby Odds</a> are live. Time to place your <a href="/signup?ref={$ref}" rel="nofollow">bet now!</a></p> 
*}

<p><strong>BUSR</strong> is the best place you can <a href="/kentucky-oaks/betting">bet on the Kentucky Oaks</a>. <p>Why? Because, not only are 256 different types of bets you can make, you'll get a <a href="/kentucky-derby/free-bet">FREE Bet</a>, and a <a href="/promos/cash-bonus-10">new member bonus up to {$BONUS_WELCOME}!</a></p>

<p>You'll also get great futures odds with the highest payouts on the <a href="/kentucky-oaks/odds">Kentucky Oaks</a>, the leading <a href="/kentucky-derby/contenders">Kentucky Derby horses</a>,  <a href="/kentucky-derby/jockey-betting">Jockeys</a> and <a href="/kentucky-derby/trainer-betting">Trainers</a>.  {* Bet on the <a href="/kentucky-derby/margin-of-victory">margin of victory</a>, head-to-head <a href="/kentucky-derby/match-races">matchups</a>,  the <a href="/kentucky-derby/props">winning time vs Secretariat's</a> world record, a <a href="/kentucky-derby/props"> Triple Crown contention</a> and more.  *}</p>

<p>The <a href="/kentucky-oaks/odds">Kentucky Oaks Odds</a> are live. {*So what are you waiting for?*} <a href="/signup?ref={$ref}" rel="nofollow">Bet now!</a></p> 

{elseif $KD_Switch_Main}

<p><strong>BUSR</strong> is the best place you can <a href="/kentucky-oaks/betting">bet on the Kentucky Oaks</a>. <p>Why? Because, not only are 256 different types of bets you can make, you'll get a <a href="/kentucky-derby/free-bet">FREE Bet</a>, and a <a href="/promos/cash-bonus-10">new member bonus up to {$BONUS_WELCOME}!</a></p>

<p>Place your bets on the  <a href="/kentucky-oaks/odds">Kentucky Oaks</a>, the leading <a href="/kentucky-derby/contenders">Kentucky Derby horses</a>,  <a href="/kentucky-derby/jockey-betting">Jockeys</a> and <a href="/kentucky-derby/trainer-betting">Trainers</a>.   You can also bet on the <a href="/kentucky-derby/margin-of-victory">margin of victory</a>, head-to-head <a href="/kentucky-derby/match-races">matchups</a> {* ,  the <a href="/kentucky-derby/props">winning time vs Secretariat's</a> world record, a <a href="/kentucky-derby/props"> Triple Crown contention</a>  *} and more. You'll find exclusive <a href="/kentucky-derby/props">Kentucky Derby Props</a> found no where else online!</p>

<p>So what are you waiting for? <a href="/signup?ref={$ref}" rel="nofollow">Bet now!</a></p> 


{elseif $KO_Switch_KO_Race_Day}
<p><strong>BUSR</strong> is the best place you can <a href="/kentucky-oaks/betting">bet on the Kentucky Oaks</a>. <p>Why? Because, not only are 256 different types of bets you can make, you'll get a <a href="/kentucky-derby/free-bet">FREE Bet</a>, and a <a href="/promos/cash-bonus-10">new member bonus up to {$BONUS_WELCOME}!</a></p>

<p>Place your bets on the  leading <a href="/kentucky-derby/contenders">Kentucky Derby horses</a>,  <a href="/kentucky-derby/jockey-betting">Jockeys</a> and <a href="/kentucky-derby/trainer-betting">Trainers</a>.   You can also bet on the <a href="/kentucky-derby/margin-of-victory">margin of victory</a>, head-to-head <a href="/kentucky-derby/match-races">matchups</a> {* ,  the <a href="/kentucky-derby/props">winning time vs Secretariat's</a> world record, a <a href="/kentucky-derby/props"> Triple Crown contention</a>  *} and more. You'll find exclusive <a href="/kentucky-derby/props">Kentucky Derby Props</a> found no where else online!</p>

<p>So what are you waiting for? <a href="/signup?ref={$ref}" rel="nofollow">Bet now!</a></p> 





{else }{*offseason*}
{*{if $dateNow <= '2020-03-30 23:59:59'}
<p>The 146th <a href="/kentucky-derby/betting"> Kentucky Derby</a> has been rescheduled to run on September 5, 2020. Due to the global pandemic of COVID-19, Churchill Downs has moved the race to ensure the fans will be able to attend and the race can run in all it's glory. </p>{/if}*}
{*  There are over 256 different types of bets you can place from the comfort of your computer or mobile device.   *}
{* Bet on the <a href="/kentucky-derby/margin-of-victory">margin of victory</a>, head-to-head <a href="/kentucky-derby/match-races">matchups</a>,  the <a href="/kentucky-derby/props">winning time vs Secretariat's</a> world record, a <a href="/kentucky-derby/props"> Triple Crown contention</a> and more.  *}

<p>You can now <a href="/kentucky-oaks/betting">bet on the Kentucky Oaks</a>!</p>

<p>Get great future odds with amazing payouts on the leading <a href="/kentucky-oaks/odds">Kentucky Oaks</a> and <a href="/kentucky-derby/contenders">Derby horses</a> and even the leading <a href="/kentucky-derby/jockey-betting">Jockeys</a> and <a href="/kentucky-derby/trainer-betting">Trainers</a>.  Bet  the "Most Exciting Two Minutes in Sports" running on {include_php file='/home/ah/allhorse/public_html/kd/date.php'} at Churchill Downs.</p>

<p>The <a href="/kentucky-oaks/odds">{include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Oaks Future Odds</a> are live. <a href="/signup?ref={$ref}" rel="nofollow">Bet now!</a></p> 

{/if}
{*
<strong> BUSR</strong> is the best place you can <a href="/kentucky-oaks/betting">bet on the Kentucky Oaks</a>. Place odds to win, match ups and prop bets all from your mobile device or  computer.  Take advantage of our great future odds with fast payouts on the leading <a href="/kentucky-oaks/odds">Kentucky Oaks</a> contenders. <br>
<br>  

Want to place a wager on the Kentucky Derby? You can do that too with BUSR <a href="/kentucky-derby/betting">Bet on the Kentucky Derby</a>, or the top <a href="/kentucky-derby/jockey-betting">Jockeys</a> and <a href="/kentucky-derby/trainer-betting">Trainers</a>.  How about the <a href="/kentucky-derby/margin-of-victory">margin of victory</a>, head-to-head <a href="/kentucky-derby/match-races">matchups</a>,  the <a href="/kentucky-derby/props">winning time</a>, a <a href="/kentucky-derby/props"> Triple Crown contention</a> and more. <br><br>



<p align="center"><a href="/kentucky-oaks/odds">{include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Oaks Odds</a> are live. Time to place your bet now!</p> 
*}
