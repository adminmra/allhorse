    <section class="card card--red" style="margin-bottom:-40px; background:#1672bd;">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red" style="background:#1672bd; color:#fff;">
            <h2 class="card_heading card_heading--no-cap">Bet the Kentucky Derby Today!</h2>
            <a href="/signup?ref={$ref}" rel="nofollow" class="card_btn card_btn--default" data-ref-val="block-exciting-CTA">Join Now</a>
          </div>
        </div>
      </div>
    </section>
