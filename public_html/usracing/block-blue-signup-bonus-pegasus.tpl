    <section class="bs-bonus">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8 col-md-10 col-lg-8">
            <h2 class="bs-bonus_heading">Pegasus World  Cup Sign Up Bonus</h2>
            <h3 class="bs-bonus_subheading">
            Up to $250 Cash Bonus <br> + Double Rebates
            {* Get up to $625 in cash bonus when you become a bet &#9733; usracing member *}
            {*You get a 10% bonus on your first deposit added automatically. Exceptional New Member Bonuses *}
            </h3>
            <p>All your bets at Gulfstream Park qualify for double rebates. That's right! Get up to 16% back on your bets, win or lose.</p>
            <p><a href="/pegasus-world-cup/signup-bonus" rel="nofollow" class="btn-xlrg fixed_cta">Learn More</a></p>

           {*<p>For your first deposit with <em>bet  	&#9733; usracing</em>, you'll get an additional 10% cash bonus added to your account, absolutely free. No Limits!</p>*}
          {* <p>Use the promocode <strong>PEGASUS</strong> when you make your deposit and receive 25% cash on your first deposit. It doesn't get any better. Join today!  *}
          {* Deposit a minimum of $100 and you can qualify to earn an additional $150!  It doesn't get any better. Join today.*} 
          {* </p><a href="/signup?ref={$ref}" rel="nofollow" class="bs-bonus_btn"> GET YOUR CASH TODAY  *}
          {* CLAIM YOUR BONUS*}
          {* </a> *}
          </div>
        </div>
      </div>
    </section>