    <section class="bs-payouts usr-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
            <h2 class="bs-payouts_heading">
               
              fast payouts
            </h2>
            <p>Why wait weeks or even months to get paid?</p>
            <p>You can get paid out in days with BUSR.</p>
          </div>
          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
            <figure class="bs-payouts_figure">
              <div class="block-center">
                <div class="block-center_content"><img src="/img/index/visa.png" alt="Horse Racing Payouts" class="img-responsive"></div>
              </div>
            </figure>
          </div>
        </div>
      </div>
    </section>