{literal}
<style>
	@media (min-width: 1024px) {
		.usrHero {
			background-image: url(/img/saudi-cup/saudi-cup-lg.jpg);
		}
	}

	@media (min-width: 481px) and (max-width: 1023px) {
		.usrHero {
			background-image: url(/img/saudi-cup/saudi-cup-md.jpg);
		}
	}

	@media (min-width: 320px) and (max-width: 480px) {
		.usrHero {
			background-image: url(/img/saudi-cup/saudi-cup-sm.jpg);
		}
	}
</style>
{/literal}
<div class="usrHero">
	<div class="text text-xl">
		<span>Saudi Cup</span>
	</div>
	<div class="text text-md copy-md">
		<span>New Members Get Up to $500 Cash!</span>
		<span></span>
		<a class="btn btn-red" href="/signup?ref={$ref}">Sign Up Now</a>
	</div>
	<div class="text text-md copy-sm">
		<span>New Members</span>
		<span>Get Up to $500 Cash!</span>
		<a class="btn btn-red" href="/signup?ref={$ref}">Sign Up Now</a>
	</div>
</div>