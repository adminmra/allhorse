
    
    
    <section class="bc-card">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/index-bs/icon-belmont-stakes-betting.png" alt="Bet on Breeders' Cup" class="bc-card_icon">
          <h2 class="bc-card_heading">Breeders' Cup Bet</h2>
          <h3 class="bc-card_subheading">$10 FREE Breeders' Cup Bet for Members</h3>
          <p>That's right, as a member of BUSR you get a $10 FREE Bet for the Breeders' Cup! Everybody's got a horse in the race - whether it's the Breeders' Cup or the United States Presidential Election.</p><a href="/signup?ref=Free-Breeders-Cup-Bet" class="bs-card_btn">Join Now</a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index-bc/breeders-cup-betting.jpg" alt="Breeders Cup Betting" class="bc-card_img"></div>
      </div>
    </section>