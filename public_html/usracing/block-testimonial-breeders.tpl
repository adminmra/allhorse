    <section class="bc-testimonials block-center usr-section">
      <div class="block-center_content">
        <div class="container">
          <blockquote class="bc-testimonial">
            <figure class="bc-testimonial_figure"><img src="/img/index-bs/derek-simon.jpg" alt="Derek Simon, Senior Editor, BUSR" class="bc-testimonial_img"></figure>
            <div class="bc-testimonial_content">
              <p class="bc-testimonial_p">At BUSR, in addition to all the traditional Breeders' Cup wagers, you can bet on individual horse match-ups, and whether there will be a Triple Crown winner in {include file='/home/ah/allhorse/public_html/kd/year.php'}!</p><span class="bc-testimonial_name">Derek Simon</span><span class="bc-testimonial_profile">Senior Editor & Handicapper at US Racing.</span>
            </div>
          </blockquote>
        </div>
      </div>
    </section>