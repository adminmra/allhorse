{literal}
<script type="application/ld+json">
  {
  "@context": "http://schema.org",
  "@type": "Event",
  "name": "Preakness Stakes",
  "startDate": "2019-05-18T15:30",
  "endDate": "2019-05-18T20:30",
  "location": {
    "@type": "EventVenue",
    "name": "Pimlico Race Course",
	"address": "5201 Park Heights Ave, Baltimore, MD 21215",
    "geo": {
    "@type": "GeoCoordinates",
    "latitude": "39.35",
    "longitude": "76.67"
    }
  	}, 
    "image": "https://www.usracing.com/img/preaknessstakes/preakness-stakes-small-hero.jpg",
    "description": "Bet on the Preakness Stakes, Preakness Stakes bets are online right now! The most Preakness Stakes Bets Anywere."
  }
</script>


{/literal}