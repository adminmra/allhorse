{literal}
<script type="application/ld+json">
  {
  "@context": "http://schema.org",
  "@type": "Event",
  "name": "Belmont Stakes",
  "startDate": "2020-10-03T15:30",
  "endDate": "2020-10-03T20:30",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing | Online Horse Betting"
  },
  "offers": [
    {
      "@type": "Offer",
      "url": "https://www.usracing.com/promos/cash-bonus-150",
      "validFrom": "2015-10-01T00:01",
      "validThrough": "2026-01-31T23:59",
     "price": "150.00",
	  "availability": "https://schema.org/InStock",
      "priceCurrency": "USD"
    } ],  
  "location": {
    "@type": "EventVenue",
    "name": "Belmont Park",
	"address": "2150 Hempstead Turnpike, Elmont, NY 11003",
    "geo": {
    "@type": "GeoCoordinates",
    "latitude": "32.7706",
    "longitude": "117.2521"
    }
  	}, 
    "image": "https://www.usracing.com/img/belmontstakes/belmont-stakes-belmont-park.jpg",
    "description": "The Belmont Stakes is the Third Jewel and is widely considered the hardest leg of the Triple Crown.  The Belmont Stakes is run at Belmont Park in Elmont, New York."
  }
</script>
{/literal}