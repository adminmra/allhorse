{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "{/literal}{$schema_name}{literal}",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "{/literal}{$schema_locality}{literal}",
					"addressRegion": "{/literal}{$schema_region}{literal}"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your {/literal}{$schema_racetrack}{literal} Horse Betting action today! Receive a welcome bonus up to {/literal}{%BONUS-WELCOME%}{literal} cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/keeneland/keeneland-off-track-betting.jpg"
    }
	</script>
{/literal}