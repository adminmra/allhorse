{literal}
<script type="application/ld+json">
  {
  "@context": "http://schema.org",
  "@type": "Event",
  "name": "Kentucky Oaks",
  "startDate": "2019-05-03T15:30",
  "endDate": "2019-05-03T18:30",
  "location": {
    "@type": "EventVenue",
    "name": "Churchill Downs",
    "telephone": "(502) 636-4400",
	"address": "700 Central Ave, Louisville, KY 40208",
    "geo": {
    "@type": "GeoCoordinates",
    "latitude": "38.19",
    "longitude": "85.45"
    }
  	}, 
    "image": "https://www.usracing.com//img/dubai-world-cup/2017-dubai-world-cup-betting-usracing.jpg",
    "description": "Kentucky Oaks - Betting the Kentucky Oaks Fillies at Churchill Downs with US Racing"
  }
</script>
{/literal}