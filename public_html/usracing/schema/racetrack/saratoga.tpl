{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Saratoga Race Course",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "267 Union Ave",
					"addressRegion": "Saratoga Springs, NY 12866"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your Saratoga Race Coursearatog Horse Betting action today! Receive a welcome bonus up to $500 cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/saratoga/saratoga-off-track-betting.jpg"
    }
	</script>
{/literal}