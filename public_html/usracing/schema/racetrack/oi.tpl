{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Oi Racecourse",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "Shinagawa City",
					"addressRegion": "Tokyo 140-0012"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your Oi Horse Betting action today! Receive a welcome bonus up to $500 cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/oi-off-track-betting.jpg"
    }
	</script>
{/literal}