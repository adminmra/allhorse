{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Oaklawn Racing Casino Resort",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "2705 Central Ave",
					"addressRegion": "Hot Springs, AR 71901"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your Oaklawn Park Horse Betting action today! Receive a welcome bonus up to $500 cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/oaklawn-park/oaklawn-racing-online.jpg"
    }
	</script>
{/literal}