{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Hanshin Racecourse",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "Takarazuka",
					"addressRegion": "Hyogo 665-0053"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your Hanshin Horse Betting action today! Receive a welcome bonus up to $500 cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/hashin-off-track-betting.jpg"
    }
	</script>
{/literal}