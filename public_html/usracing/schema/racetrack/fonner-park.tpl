{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Fonner Park",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "700 E Stolley Park Rd",
					"addressRegion": "Grand Island, NE 68801"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Bet the latest Fonner Park Horse Betting Odds. Get an exclusive bonus up to $500 and 8% daily racebook rebates!",
      "image": "https://www.usracing.com/img/racetracks/fonner-park/fonner-park-live-odds.jpg"
    }
	</script>
{/literal}