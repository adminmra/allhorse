{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Fukushima Racecourse",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "Fukushima",
					"addressRegion": "960-8114"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your Fukushima Horse Betting action today! Receive a welcome bonus up to $500 cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/fukushima-off-track-betting.jpg"
    }
	</script>
{/literal}