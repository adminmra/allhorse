{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Gulfstream Park Racing and Casino",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "901 Federal Hwy",
					"addressRegion": "Hallandale Beach, FL 33009"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your Gulfstream Park Horse Betting action today! Receive a welcome bonus up to $500 cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/gulfstream-park/gulfstream-park-off-track-betting.jpg"
    }
	</script>
{/literal}