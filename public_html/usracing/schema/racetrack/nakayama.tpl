{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Nakayama Racecource",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "Funabashi",
					"addressRegion": "Chiba 273-0037"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your Nakayama Horse Betting action today! Receive a welcome bonus up to $500 cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/nakayama-off-track-betting.jpg"
    }
	</script>
{/literal}