{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Churchill Downs",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "700 Central Av",
					"addressRegion": "Louisville, KY 40208"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your Churchill Downs Betting action. Online horse racing, up to 8% daily betting rebates, up to $500 welcome bonus, handicapping picks and much more at BUSR.",
      "image": "https://www.usracing.com/img/racetracks/churchill-downs/churchill-live-odds.jpg"
    }
	</script>
{/literal}