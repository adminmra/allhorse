{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Will Rogers Downs",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "2900 South 4200 Road",
					"addressRegion": "Claremore, OK 74019"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your Will Rogers Downs Horse Betting action today! Receive a welcome bonus up to $500 cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/will-rogers-downs/will-rogers-race-track.jpg"
    }
	</script>
{/literal}