{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Parx Racing",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "2999 Street Rd",
					"addressRegion": "Bensalem, PA 19020"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your Parx Racing Horse Betting action today! Receive a welcome bonus up to $500 cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/parx-racing/parx-racing-off-track-betting.jpg"
    }
	</script>
{/literal}