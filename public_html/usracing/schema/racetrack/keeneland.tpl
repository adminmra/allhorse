{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Keeneland Association Inc",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "4201 Versailles Rd",
					"addressRegion": "Lexington, KY 40510"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your Keeneland Horse Betting action today! Receive a welcome bonus up to $500 cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/keeneland/keeneland-off-track-betting.jpg"
    }
	</script>
{/literal}