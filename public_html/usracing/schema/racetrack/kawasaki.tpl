{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Kawasaki Racecourse",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "Kawasaki",
					"addressRegion": "Kanagawa 210-0011"
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your Kawasaki Horse Betting action today! Receive a welcome bonus up to $500 cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/kawasaki-off-track-betting.jpg"
    }
	</script>
{/literal}