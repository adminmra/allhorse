{* literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Aqueduct Racetrack",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "110-00 Rockaway Blvd",
					"addressRegion": "Queens, NY 11420
				}
			},
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20"
      },
      "description": "Get your Aqueduct Racetrack Horse Betting action today! Receive a welcome bonus up to $500 cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/aqueduct/live-racing-from-aqueduct.jpg"
    }
	</script>
{/literal *}
{literal}
<script type="application/ld+json">
	  {
      "@context": "http://schema.org",
				"@type": "Place",
				"name": "Aqueduct Racetrack",
				"address":{
					"@type": "PostalAddress",
					"addressLocality": "110-00 Rockaway Blvd",
					"addressRegion": "Queens, NY 11420"
			},
      "description": "Get your Aqueduct Racetrack Horse Betting action today! Receive a welcome bonus up to $500 cash, and earn a percentage of your daily action back!",
      "image": "https://www.usracing.com/img/racetracks/aqueduct/live-racing-from-aqueduct.jpg"
    }
	</script>
{/literal}