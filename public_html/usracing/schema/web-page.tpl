{literal}
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebPage",
        "name": "{/literal}{$h1|escape}{literal}",
        "description": "{/literal}{$description}{literal}",
        "publisher": {
            "@type": "Organization",
            "name": "US Racing"
        }
    }
    </script>

 
{/literal}