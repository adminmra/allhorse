{literal}
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Event",
    "name": "{/literal}{$eventname}{literal}",
    "description": "{/literal}{if $BC_Switch_Early}{$description_early}{elseif $BC_Switch_Race_Day}{$description_race_day}{elseif $BC_Switch_Main}{$description_main}{else}{$description}{/if}{literal}"
    "startDate": "2021-11-05T00:00",
    "endDate": "2021-11-07T00:00",
    "eventStatus": "https://schema.org/EventScheduled",
    "eventAttendanceMode": "https://schema.org/OfflineEventAttendanceMode",
    "location": {		
      "@type": "Place",
      "name": "Del Mar Racetrack",
      "address": {
        "@type": "PostalAddress",
        "streetAddress": "2260 Jimmy Durante Blvd",
        "addressLocality": "California",
        "addressRegion": "CA",
        "postalCode": "CA 92014",
        "addressCountry": "US"
      }
    }
  }
</script>
{/literal}