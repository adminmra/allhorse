{literal}
<script type="application/ld+json">
  {
  "@context": "http://schema.org",
  "@type": "Event",
  "name": "{/literal}{$eventname}{literal}",
  "startDate": "2020-11-07T13:30",
  "endDate": "2020-11-07T18:30",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing | Online Horse Betting"
  },
  "offers": [
    {
      "@type": "Offer",
      "url": "https://www.usracing.com/promos/cash-bonus-150",
      "validFrom": "2015-10-01T00:01",
      "validThrough": "2026-01-31T23:59",
      "price": "150.00",
	  "availability": "https://schema.org/InStock",
      "priceCurrency": "USD"
    } ],
  "location": {
    "@type": "EventVenue",
    "name": "Keeneland Racecourse",
    "telephone": "(800) 456-3412",
	"address": "4201 Versailles Rd, Lexington, KY 40510",
    "geo": {
    "@type": "GeoCoordinates",
    "latitude": "38.0461301",
    "longitude": "-84.6080081"
    }
  	},  
    "image": "{/literal}{$image}{literal}",
    "description": "{/literal}{$description}{literal}"
  }
</script>
{/literal}