{literal}
 <script type="application/ld+json">
{
    "@context": "https://schema.org",
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20",
        "price": "500",
        "priceCurrency": "USD",
        "availability": "https://schema.org/InStock",
        "validFrom": "2020-01-01T12:00",
	"image": "https://www.usracing.com/img/states/new-york/ny-off-track-betting.jpg",
    	"description": "New York Horse Betting."
}
</script>
{/literal}