

{literal}
  <style>
    @media (min-width: 1024px) {
      .usrHero {
        background-image: url({/literal} {$hero_lg} {literal});
      }
    }

    @media (min-width: 481px) and (max-width: 1023px) {
      .usrHero {
        background-image:  url({/literal} {$hero_md} {literal});
      }
    }

    @media (min-width: 320px) and (max-width: 480px) {
      .usrHero {
        /*background-image: url({/literal} {$hero_sm} {literal});*/
        display: none;
      }
    }

    @media (max-width: 479px) {
      .break-line {
        display: none;
      }
    }

    @media (min-width: 320px) and (max-width: 357px) {
      .fixed_cta {
        font-size: 13px !important;
      }
    }
  </style>
{/literal}



<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
 {*
   <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}
*}</a>
    </div>
</div>