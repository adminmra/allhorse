{literal}
    <style>
		@media (min-width: 768px) and (max-width: 797px) {
			.fixed_cta {
                font-size: 17px;
            }
		}
        @media (min-width: 889px) and (max-width: 933px) {
			.fixed_cta {
                font-size: 22px;
            }
		}
        @media (min-width: 1024px) and (max-width: 1033px) {
            .fixed_cta {
                font-size: 23px;
            }
        }
	</style>
{/literal}

<section class="card">
  <div class="card_wrap">
    <div class="card_half card_hide-mobile">
      <a href="/belmont-stakes/props">
        <img src="" data-src-img="/img/index-kd/bet-kentucky-derby-jockeys.jpg" alt="Kentucky Derby Props"
          class="card_img"></a></div>
    <div class="card_half card_content"><a href="/kentucky-derby/props">
        <img src="/img/index-kd/icon-bet-kentucky-derby-jockeys.png" alt="Haskell Props" class="card_icon"></a>
      <h2 class="card_heading">Kentucky Derby Props</h2>
      <p>You'll get great fixed odds with amazing payouts on the leading Kentucky Derby horses.</p>
      <p> Bet on the prop wagers such as margin of victory, head-to-head matchups and more.</p><a
        href="/kentucky-derby/props" class="btn-xlrg fixed_cta">See the Kentucky Derby Props</a>
    </div>
  </div>
</section>