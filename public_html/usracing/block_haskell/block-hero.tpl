
 {*<!--------- NEW HERO  ---------------------------------------->
 styles for the new hero dont touch*}
 
{literal}
  <style>
    @media (min-width: 1024px) {
      .usrHero {
        background-image: url({/literal} {$hero_lg} {literal});
      }
    }

    @media (min-width: 481px) and (max-width: 1023px) {
      .usrHero {
        background-image: url({/literal} {$hero_md} {literal});
      }
    }

    @media (min-width: 320px) and (max-width: 480px) {
      .usrHero {
        /*background-image: url({/literal} {$hero_sm} {literal});*/
        display: none;
      }
    }

    @media (max-width: 479px) {
      .break-line {
        display: none;
      }
    }

    @media (min-width: 320px) and (max-width: 357px) {
      .fixed_cta {
        font-size: 13px !important;
      }
    }
  </style>
{/literal}
{*End*}
 {*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2}</span>
        <a class="btn btn-red" href="https://busr.ag?ref={$ref}&signup-modal=open">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h1}</span>
        <span>{$h2}</span>
        <a class="btn btn-red" href="https://busr.ag?ref={$ref}&signup-modal=open">{$signup_cta}</a>
    </div>
</div>

{* <!--------- NEW HERO END ---------------------------------------->  *}   