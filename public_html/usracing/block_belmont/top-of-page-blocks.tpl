{* old schema *}
{* {include file="/home/ah/allhorse/public_html/usracing/schema/belmont-stakes.tpl"} *}{*Is this content up-to-date? -TF*}
{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}
{include file="/home/ah/allhorse/public_html/belmont/schema.tpl"} {*Is this content up-to-date? -TF*}
{include file='inc/left-nav-btn.tpl'}<div id="left-nav">{include file='menus/belmontstakes.tpl'}</div>
{*
{literal}
  <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "location": {
            "@type": "Place",
            "address": {
                "@type": "PostalAddress"
            }
        },
        "name": "Belmont Stakes",
        "startDate": "2020-06-22",
        "endDate": "2020-06-22",
        "image": "/img/belmontstakes/bet-belmont-stakes-odds.jpg",
        "description": "Bet Belmont Stakes",
        "offers": {
            "@type": "Offer",
            "name": "NEW MEMBERS GET UP TO $500 CASH!",
            "price": "500.00",
            "priceCurrency": "USD",
            "url": "https://www.usracing.com/promos/cash-bonus-20",
            "availability": "http://schema.org/InStock"
        },
        "sport": "Horse Racing",
        "url": "https://www.usracing.com/belmont-stakes/betting"
    }
</script>
{/literal}
*}{* Old Hero Style - May 27, 20 - TF
	
	  <!--------- NEW HERO - MAY 2017 ---------------------------------------->
	
{literal}
    <style>
        .background{
        background-image: url("/img/index-bs/bg-belmont.jpg");
        background-size: contain;
        background-repeat: no-repeat;
        background-position: 0% 110%;
    }

    /* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  .background{
    background-position: 0% 110%;
  }
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  .background{
    background-position: 0% 104%;
  }
}

/* 
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) {
  .background{
    background-position: 0% 98%;
  }
}

@media (min-width: 1024px) and (max-width: 1366px) {
  .background{
    background-position: 0% 100% !important;
  }
}
/*
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 767px) {
  .background{
    background-position: 0% 89%;
  }
}

/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  .background{
    background-position: 0% 89%;
  }
}
    </style>
  {/literal}
  

<div class="newHero hidden-sm hidden-xs" style="background-image: url({$hero});">

 <div class="text text-xl centerText">{$h1}</div>

 <div class="text text-xl centerText" style="margin-top:0px;">{$h1b}</div>

 	<div class="text text-md centerText">{if $BS_Switch}Signup Today and Get Free Bets!{else}{$h2}{/if}

	 	<br>

	 		{* <a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a> *}
		{* <a href="/signup?ref={$ref}" class="btn-xlrg ">{$signup_cta}</a>

	</div>

 </div> *}
{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-hero.tpl"}    
{include file="/home/ah/allhorse/public_html/belmont/block-countdown-new.tpl"}
   <section class="bs-kd bs-kd--default usr-section bs">
      <div class="container">
        <div class="bs-kd_content">
          <img src="/img/index-bs/bs.png" alt="Online Horse Betting" class="bs-kd_logo img-responsive">
          <h1 class="kd_heading">{$h1}</h1>
          <h3 class="kd_subheading">
	          {if $BS_Switch_Early}{$h2_early} 
	          {elseif $BS_Switch_Main}{$h2_main} 
	          {elseif $BS_Switch_Race_Day}{$h2_race_day}
	          {else}{$h2}{/if}
	      </h3>

<p>{include file='/home/ah/allhorse/public_html/usracing/block_belmont/sales-copy.tpl'} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>
