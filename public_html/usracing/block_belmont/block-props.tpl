{literal}
    <style>
		@media (min-width: 768px) and (max-width: 797px) {
			.fixed_cta {
                font-size: 17px;
            }
		}
        @media (min-width: 889px) and (max-width: 933px) {
			.fixed_cta {
                font-size: 22px;
            }
		}
        @media (min-width: 1024px) and (max-width: 1033px) {
            .fixed_cta {
                font-size: 23px;
            }
        }
	</style>
{/literal}

<section class="card">
  <div class="card_wrap">
    <div class="card_half card_hide-mobile">
      <a href="/belmont-stakes/props">
        <img data-src="/img/index-kd/bet-kentucky-derby-jockeys.jpg" data-src-img="/img/index-kd/bet-kentucky-derby-jockeys.jpg" alt="Belmont Stakes Props"
          class="card_img lazyload"></a></div>
    <div class="card_half card_content"><a href="/belmont-stakes/props">
        <img src="/img/index-kd/icon-bet-kentucky-derby-jockeys.png" alt="Belmont  Props" class="card_icon"></a>
      <h2 class="card_heading">Belmont Stakes Props</h2>
      <p>You'll get great fixed odds with amazing payouts on the leading Belmont Stakes horses.</p>
      <p> Bet on the prop wagers such as margin of victory, head-to-head matchups and more.</p>
      <a href="/belmont-stakes/props" class="btn-xlrg fixed_cta">See the Belmont Stakes Props</a>
    </div>
  </div>
</section>