  <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_content"><img src="/img/index-bs/icon-belmont-stakes-betting.png" alt="Bet on Belmont Stakes" class="bs-card_icon">
          <h2 class="bs-card_heading">Belmont Stakes Bet</h2>
          <h3 class="bs-card_subheading">$10 FREE Belmont Stakes Bets for Members</h3>
          <p>Place a wager of $25 or more on the Belmont Stakes and you will get $10 in casino chips! </p>
          <p>You can only win with BUSR</i>!</p><a href="/signup?ref={*Free-*}Belmont-Stakes {*Bet*}" class="bs-card_btn">Join Now</a>
        </div>
        <div class="bs-card_half bs-card_hide-mobile">
          <a href="/signup?ref=Belmont-Stakes">
            <img data-src="/img/index-bs/belmont-stakes-betting.jpg" data-src-img="/img/index-bs/belmont-stakes-betting.jpg" alt="Belmont Stakes Betting" class="bs-card_img lazyload">
          </a>
        </div>
      </div>
    </section>
