{*
    <section class="bc-testimonials block-center usr-section">
      <div class="block-center_content">
        <div class="container">
          <blockquote class="bc-testimonial">
            <figure class="bc-testimonial_figure"><img src="/img/index-bs/derek-simon.jpg" alt="Derek Simon, Senior Editor, BUSR" class="bc-testimonial_img"></figure>
            <div class="bc-testimonial_content">
              <p class="bc-testimonial_p">At BUSR, in addition to all the traditional Belmont Stakes wagers, you can bet on the Belmont Stakes Prob Bets and Match Races!  </p><span class="bc-testimonial_name">Derek Simon</span><span class="bc-testimonial_profile">Senior Editor & Handicapper at US Racing.</span>
            </div>
          </blockquote>
        </div>
      </div>
    </section>
*}