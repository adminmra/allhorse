{*created by TF, May 28 2020*}
<p><a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> is the best site to place your bet on the   <a href="/belmont-stakes/betting"> {$BELMONT_YEAR} Belmont Stakes</a>. </p><p> Why?  Because, all new members can get up to a <a href="/cash-bonus-20">{$BONUS_WELCOME} welcome bonus!</a>  You'll also get a <strong>free bet</strong>{*Change to a link when page is ready*} for the big race and great <a href="/belmont-stakes/props">Belmont Stakes Props</a> found nowhere else online!</p>

{if $BS_Switch_Early || $BS_Switch_Main || $BS_Switch_Race_Date}
<p>So what are you waiting for? <a href="/belmont-stakes/odds">Belmont Stakes Odds are live.</a></p>
{/if}
{* <p><a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> is the best place you can place your bet on the final leg of the Triple Crown, the <a href="/belmont-stakes/betting">Belmont Stakes</a>.  *}
{* The latest <a href="/belmont-stakes/odds">Belmont Stakes Odds</a> are live. *}
 {*Will Mike Smith and Justify win the  Belmont Stakes? Place your bets on our <a href="/bet-on/triple-crown">Triple Crown Odds</a> and watch! *}