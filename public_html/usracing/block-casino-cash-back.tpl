    <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_content"><img src="/img/index/icon-casino-rebate.png" alt=" Casino Rebate" class="bs-card_icon">
          <h2 class="bs-card_heading"> 50% Casino Cash Back </h2>
           <h3 class="bs-card_subheading">Every Thursday!</h3> 
          <p>Every Thursday at the Casino is extra special at BUSR because you get 50% Cash returned to your account of any losses! </p>
          <p>That's right, you will get 50% Cash Back to your account every Thursday. </p><a href="/signup?ref={$ref}" class="bs-card_btn">Join Now</a>
        </div>
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index/casino-rebate.jpg" alt="Free Casino Chips" class="bs-card_img">
        </div>
      </div>
    </section>