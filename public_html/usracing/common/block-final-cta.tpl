  <section class="card card--red">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red">
            <h2 class="card_heading card_heading--no-cap">Get in on the Exciting Horse Betting Action!</h2>
            <a href="/signup?ref={$ref}" rel="nofollow" class="card_btn card_btn--default">Join Now</a>
          </div>
        </div>
      </div>
    </section>