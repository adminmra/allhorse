    <section class="bc-card bc-card-mobile">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_hide-mobile"><img data-src="/img/index/mobile-horse-betting.png?v=6" data-src-img="/img/index/mobile-horse-betting.png?v=6"
            alt="Mobile Horse Betting" class="bc-card_img lazyload">
        </div>
        <div class="bc-card_half bc-card_content">
          {* <img src="/img/index/icon-mobile-horse-betting.png" alt="Mobile Horsed Betting Icon" class="bc-card_icon"> *}
          <img class="icon-mobile-horse-betting bc-card_icon" alt="Mobile Horsed Betting Icon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABQAQMAAACEU3HPAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABFJREFUeNpjYBgFo2AU0AsAAANwAAFHM3wcAAAAAElFTkSuQmCC">
          <h2 class="bc-card_heading">Bet on Your Phone!{* the go! *}</h2>
          <p>Take all the races with you! All tracks are available on phone or tablet at the track, at home, everywhere.
          </p>
          <p>Are you out at the bar with your friends? Bet on the game any time, anywhere. Or, while waiting for your
            friends to arrive, how about playing a few hands of blackjack or roulette! <p> BUSR -
              super easy and super fun.</p><a href="/mobile-horse-betting" class="btn-xlrg fixed_cta">See Your Mobile
              Betting Options</a>
        </div>
      </div>
    </section>