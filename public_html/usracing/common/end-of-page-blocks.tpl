</div></div></section>
{* Common blocks adjusted for Major Races - tf See also race blocks*}

{if $KD_Switch_Full_Site} 
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/blue-signup-bonus-kentucky-derby.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-free-bet.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-trainers.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-final-cta.tpl"}   

{elseif $PS_Switch_Full_Site}

	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-blue-signup-bonus-ps.tpl"} 		
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-free-bet.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-props.tpl"}
	{* 	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-triple-crown.tpl"} *}
	{* {include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-testimonial-other.tpl"} *}
	{* {include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-countdown.tpl"} *}
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-final-cta.tpl"}
			
		
{elseif $BS_Switch_Full_Site}

	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-blue-signup-bonus-belmont-stakes.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-free-bet.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-props.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-final-cta.tpl"}

 {else} 
  
	{include file="/home/ah/allhorse/public_html/usracing/common/block-testimonial-pace-advantage.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-signup-bonus.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-150.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-rebates.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-final-cta-bonus.tpl"}   
{/if}