<div class="breadcrumb">
	<ul>
		{if !isset($crumb0)}<li><a href="/" >US Racing</a></li>{/if}
		{if isset($crumb0)}<li><a href="{$crumb0link}">{$crumb0}</a></li>{/if}
		{if isset($crumb1)}<li><a href="{$crumb1link}">{$crumb1}</a></li>{/if}
		{if isset($crumb2)}<li><a href="{$crumb2link}">{$crumb2}</a></li>{/if}
		{if isset($crumb3)}<li><a href="{$crumb3link}">{$crumb3}</a></li>{/if}
		{if isset($crumb4)}<li><a href="{$crumb4link}">{$crumb4}</a></li>{/if}
		<li><a href="{$crumbpagelink}">{$crumbpage}</a></li>
	</ul>
</div>
{literal}
<style type="text/css" >
.breadcrumb {background:transparent; padding:0; text-align: left;}
.breadcrumb ul { margin: 0; padding: 0 }
.breadcrumb ul li { list-style:none; display:inline-block; margin: 0px; padding: 0px; font-size:16px; padding-right: 20px; padding-left: 8px; position: relative;  }
.breadcrumb ul li:first-child { padding-left: 0 }
.breadcrumb ul li:last-child { padding-right: 0; }
.breadcrumb ul li::after { content: '>'; position: absolute; right: 0; font-weight: bold;}
.breadcrumb ul li:last-child::after { content: '' }
</style>
{/literal}
