      <section class="bc-racetracks">
        <div class="container">
          <h2 class="bc-racetracks_title">

            Choose from <strong>{$RACETRACKS}</strong> Racetracks Worldwide
          </h2>
          <ul class="bc-racetracks_list">
            <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
                <div class="bc-racetrack">
                  {* <img src="/img/index/icon-thoroughbred-racing.png" alt="Thoroughbred Racetracks" class="bc-racetrack_icon"> *}
                  <img class="icon-thoroughbred-racing bc-racetrack_icon" alt="Thoroughbred Racetracks" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAABmAQMAAAADEXYhAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRMFgAAAWUAAF2+LisAAAAAElFTkSuQmCC">
                  <h4 class="bc-racetrack_title">Thoroughbred</h4>
                </div>
              </a></li>
            <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
                <div class="bc-racetrack">
                  {* <img src="/img/index/icon-harness-racing.png" alt="Harness Racing Racetracks" class="bc-racetrack_icon"> *}
                  <img class="icon-harness-racing bc-racetrack_icon" alt="Harness Racing Racetracks" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAABmAQMAAAADEXYhAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRMFgAAAWUAAF2+LisAAAAAElFTkSuQmCC">
                  <h4 class="bc-racetrack_title">Harness</h4>
                </div>
              </a></li>
            <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
                <div class="bc-racetrack">
                  {* <img src="/img/index/icon-quarter-horse-racing.png" alt="Quarter Horse Racetracks" class="bc-racetrack_icon"> *}
                  <img class="icon-quarter-horse-racing bc-racetrack_icon" alt="Quarter Horse Racetracks" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAABmAQMAAAADEXYhAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRMFgAAAWUAAF2+LisAAAAAElFTkSuQmCC">
                  <h4 class="bc-racetrack_title">Quarter Horses</h4>
                </div>
              </a></li>
            <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
                <div class="bc-racetrack">
                  {* <img src="/img/index/icon-international-horse-racing.png" alt="International  Racetracks" class="bc-racetrack_icon"> *}
                  <img class="icon-international-horse-racing bc-racetrack_icon" alt="International  Racetracks" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAABmAQMAAAADEXYhAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRMFgAAAWUAAF2+LisAAAAAElFTkSuQmCC">
                  <h4 class="bc-racetrack_title">International</h4>
                </div>
              </a></li>
            <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
                <div class="bc-racetrack">
                  {* <img src="/img/index/icon-greyhound-racing.png" alt="Greyhound  Racetracks" class="bc-racetrack_icon"> *}
                  <img class="icon-greyhound-racing bc-racetrack_icon" alt="Greyhound  Racetracks" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAABmAQMAAAADEXYhAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRMFgAAAWUAAF2+LisAAAAAElFTkSuQmCC">
                  <h4 class="bc-racetrack_title">Greyhound</h4>
                </div>
              </a></li>
          </ul>
        </div>
      </section>