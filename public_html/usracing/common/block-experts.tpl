    <section class="bc-card bc-card--hre">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content">
          {* <img src="/img/index/icon-horse-racing-experts.png" alt="Horse Racing Experts" class="bc-card_icon"> *}
          <img class="icon-horse-racing-experts bc-card_icon" alt="Horse Racing Experts" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABFAQMAAADw5mMPAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABFJREFUeNpjYBgFo2AUDBgAAAOBAAH0sYD7AAAAAElFTkSuQmCC">
          <h2 class="bc-card_heading">Horse Racing Experts</h2>
          <p>US Racing's Authors and Handicappers will make you a better player. Get the latest news, odds, race reports
            and betting advice from our experienced group of horse racing writers.</p><a href="/news"
            class="bc-card_btn-red">Read the Latest </a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index/horse-racing-experts.png"  data-src="/img/index/horse-racing-experts.png"
            alt="Expert Handicappers" class="bc-card_img lazyload"></div>
      </div>
    </section>