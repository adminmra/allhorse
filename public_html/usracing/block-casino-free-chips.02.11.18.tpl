    <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_content"><img src="/img/index/icon-casino-rebate.png" alt="US Racing Casino Rebate" class="bs-card_icon">
          <h2 class="bs-card_heading">Get $25 in Chips for Blackjack{* 50% Casino Cash Back *}</h2>
         {*  <h3 class="bs-card_subheading">Every Thursday!</h3> *}
          <p>Place $25 or more on any Exacta or Trifecta wagers on the Breeders' Cup Classic race on Saturday, November 3rd and you will receive a free $25 casino chip to play Blackjack. {* Every Thursday at the Casino is extra special at BUSR because you get 50% Cash returned to your account of any losses! *}</p>
          <p>Now is your chance to win twice on the Breeders' Cup Classic! {* That's right, you will get 50% Cash Back to your account every Thursday. *}</p><a href="/signup?ref={$ref}" class="bs-card_btn">Join Now</a>
        </div>
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index/casino-rebate.jpg" alt="Free Casino Chips" class="bs-card_img">
        </div>
      </div>
    </section>