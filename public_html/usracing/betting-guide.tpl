<div class="block-betting-guide justify-left">
	<p>
		How to Bet on Horses:<br>
		New to horse betting? Check out our horse betting guides to learn more about the types of bets and how to bet on horses.
	</p>
	<ul class="spUL">
		<li><a href="/horse-betting/exacta">What is an Exacta Bet?</a></li>
		<li><a href="/horse-betting/trifecta">What is a Trifecta Bet?</a></li>
		<li><a href="/horse-betting/box-bet">What is a Box Bet?</a></li>
		<li><a href="/horse-betting/key-a-horse">How to Key a Horse</a></li>
		<li><a href="/horse-betting/across-the-board">What is Across the Board?</a></li>
		<li><a href="/horse-betting/key-a-horse">How to Key a Horse</a></li>
		<li><a href="/horse-betting/claiming-race">Claiming Races</a></li>
		<li><a href="/horse-betting/thoroughbred-tips">Thoroughbred Tips</a></li>
		<li><a href="/horse-betting/place">How to Make a Place Bet</a></li>
		<li><a href="/horse-betting/beginner-tips">Beginner's Tips for Betting on Horses</a></li>
	</ul>
</div>