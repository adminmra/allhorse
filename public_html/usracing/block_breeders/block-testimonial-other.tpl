    <section class="ps-testimonials--blue usr-section">
      <div class="container">
        <blockquote class="ps-testimonial-blue">
          <figure class="ps-testimonial-blue_figure"><img src="/img/index-kd/horseracingguy.jpg" alt="Racing Dudes" class="ps-testimonial-blue_img"></figure>
          <div class="ps-testimonial-blue_content">
            <p class="ps-testimonial-blue_p">When it comes to the Preakness Stakes, BUSR has you covered. They provide many unique betting options; more than any body else.</p><span class="ps-testimonial-blue_name">
               
              - The Racing Dudes
             {*  <cite class="ps-testimonial-blue_profile">Feature Writer for BUSR, Lady and the Track & Horse Racing Nation.</cite> *}</span>
          </div>
        </blockquote>
      </div>
    </section>
    