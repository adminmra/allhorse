        <section class="card">
      <div class="card_wrap">
	   <div class="card_half card_hide-mobile">
	           <a href="/preakness-stakes/props">
	        <img src="" data-src-img="/img/index-kd/bet-kentucky-derby-jockeys.jpg" alt="Preakness Stakes Props" class="card_img"></a></div>   
        <div class="card_half card_content"><a href="/preakness-stakes/props">
	        <img src="/img/index-kd/icon-bet-kentucky-derby-jockeys.png" alt="Preakness  Props" class="card_icon"></a>
	        
	        
          <h2 class="card_heading">Preakness Stakes Props</h2>
          <h3 class="card_subheading"></h3>
          <p>You'll  get great fixed odds with amazing payouts on the leading Preakness Stakes horses.</p><p> Bet on the prop wagers such as margin of victory, head-to-head matchups and more.</p><a href="/preakness-stakes/props" class="card_btn">See the Preakness Props</a>
        </div>
        
           
      </div>
    </section>
    
    
    
