
      {literal}
    <style>
    .background{
        background-image: url("/img/index-ps/bg-pimlico.jpg");
        background-size: contain;
        background-repeat: no-repeat;
        background-position: 0% 110%;
    }

    /* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  .background{
    background-position: 0% 110%;
  }
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  .background{
    background-position: 0% 100%;
  }
}

/* 
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) {
  .background{
    background-position: 0% 100%;
  }
}
/*
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 767px) {
  .background{
    background-position: 0% 90%;
  }
}

/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  .background{
    background-position: 0% 90%;
  }
}
    
    </style>
    {/literal}
        <section class="ps-kd ps-kd--default usr-section background">
      <div class="container">
        <div class="ps-kd_content">
          <img src="/img/index-ps/preakness-stakes-icon.png" alt="Online Horse Betting" class="ps-kd_logo img-responsive">
          <h1 class="kd_heading">Preakness Stakes Betting</h1>
          <h3 class="kd_subheading">With the Most Preakness Stakes Bet Options Anywhere</h3>
          <p>{* Only at Bet <i>bet  	&#9733; usracing</i>, you get great future odds with amazing payouts on the leading Kentucky Oaks and Preakness horses and even the leading jockeys and trainers. *}Only at <i>bet  	&#9733; usracing</i>, you get great future odds with amazing payouts on the leading Preakness horses.</p>
          <p>{* Bet on the margin of victory, the winning time vs Secretariat, a Triple Crown contention and more.  Preakness Stakes Odds are live, bet now. *} {* Will the winner of the {'Y'|date}  Kentucky Derby win the Preakness Stakes?  Will a horse lead wire to wire? *}{* Will Secretariat's 1973 Record be broken? *} 
          <p> <i>bet  	&#9733; usracing</i> offers more wagering choices than any other site.</p>{* <a href="/preakness-stakes/odds?ref={$ref}" class="kd_btn">SEE THE <br>LIVE ODDS</a> *}
          
           <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="bs-kd_btn ">Bet Now</a></p>  
        </div>
      </div>
      </div>
    </section>
