<p><a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> is the best site to place your bet on the   <a href="/breeders-cup/betting"> {$BC_YEAR} Breeders' Cup</a>. </p><p> Why?  Because, all new members can get up to a <a href="/cash-bonus-20">{$BONUS_WELCOME} welcome bonus!</a>  By betting on the races you can also qualify for another <a href="/promos/cash-bonus-150">{$BONUS_RETENTION} free</a>! </p>

{if $BC_Switch_Early || $BC_Switch_Main || $BC_Switch_Race_Day}
<p>So what are you waiting for? <a href="/breeders-cup/odds">Breeders' Cup Odds are live.</a></p>
{/if}
