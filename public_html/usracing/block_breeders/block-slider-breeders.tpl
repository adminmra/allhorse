{literal}
<style>

    @media (min-width:481px) {
        .swiper-slide.first {
            /*background-image: url(/img/heros-index/breeders-cup-santa-anita.jpg)*/
            background-image: url(/img/heros-index/breeders-cup-keeneland.jpg)
        }

        .swiper-slide.second {
            background-image: url(/img/new_hero/horse-racing-bonus2.jpg)
        }
    }

    @media (max-width:480px) {
        .swiper-slide.first {
            /*background-image: url(/img/heros-index/breeders-cup-santa-anita-mobile.jpg)*/
            background-image: url(/img/heros-index/breeders-cup-keeneland-mobile.jpg)

        }

        .swiper-slide.second {
            background-image: url(/img/heros-index/sm_hero_01.jpg)
        }

        .tp-caption.txtLrg {
            font-size: 33px !important;
        }

        .tp-caption.txtMed {
            font-size: 22px !important;
        }
    }

    .sliderBtn.tp-caption.btn:active {
        position: static !important;
        margin-top: 25px !important;
    }
</style>
{/literal}

<div class="slider" style="opacity: 0">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide first">
                <div class="slide first">
                    <span class="large-text tp-caption txtLrg desk">Bet the Breeders' Cup</span>
                    <div class="large-text tp-caption txtLrg mobile column"
                        style="display:flex; justify-content:center">
                        <span>Bet the&nbsp;</span>
                        <span>Breeders' Cup</span>
                    </div>
                    <span class="secondary-text txtMed tp-caption desk"> Get Up to $500 in Cash</span>
                    <div class="secondary-text txtMed tp-caption mobile column"
                        style="display:flex; justify-content:center">
                        <span>Get Up to</span>
                        <span>$500 in Cash</span>
                    </div>
                    <a class="sliderBtn tp-caption btn btn-red" href="/signup?ref={$ref}">
                        <i class="fa fa-thumbs-o-up left"></i>Sign Up Now!
                    </a>
                </div>
            </div>
            <div class="swiper-slide second">
                <div class="slide third">
                    <span class="large-text tp-caption txtLrg desk">{$BONUS_WELCOME} Instant Cash Bonus</span>
                    <div class="large-text tp-caption txtLrg mobile" style="display:flex; justify-content:center">
                        <span>{$BONUS_WELCOME} Instant&nbsp;</span>
                        <span>Cash Bonus</span>
                    </div>
                    <span class="secondary-text txtMed tp-caption">and Qualify for Another {$BONUS_RETENTION}!</span>
                    <a class="sliderBtn tp-caption btn btn-red" href="/signup?ref={$ref}"><i
                            class="fa fa-star left"></i>Sign Up Now</i></a>
                </div>
            </div>
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next hide-arrow"></div>
        <div class="swiper-button-prev hide-arrow"></div>
    </div>
</div>
{literal}
<script>
    $(window).load(function () {
        var swiper = new Swiper('.swiper-container', {
            effect: 'fade',
            slidesPerView: 1,
            loop: true,
            lazy: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            }
        });

        setInterval(function () {
            var $sample = $(".slider");
            if ($sample.is(":hover")) {
                $(".swiper-button-next").removeClass("hide-arrow");
                $(".swiper-button-prev").removeClass("hide-arrow");
            } else {
                $(".swiper-button-next").addClass("hide-arrow");
                $(".swiper-button-prev").addClass("hide-arrow");
            }
        }, 200);

        $(".slider").animate({
            opacity: 1
        }, 1200, function () {});
    });
</script>
{/literal}