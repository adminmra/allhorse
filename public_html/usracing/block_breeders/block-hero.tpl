
 {*<!--------- NEW HERO  ---------------------------------------->
 styles for the new hero dont touch*}
 
{literal}
  <style>
    @media (min-width: 1024px) {
      .usrHero {
        background-image: url('/img/index-bc/breeders-cup-odds-hero-min.jpg');
      }
    }

    @media (min-width: 481px) and (max-width: 1023px) {
      .usrHero {
         background-image: url('/img/index-bc/breeders-cup-odds-hero-min.jpg');
      }
    }

    @media (min-width: 320px) and (max-width: 480px) {
      .usrHero {
        /*background-image: url({/literal} {$hero_sm} {literal});*/
        display: none;
      }
    }

    @media (max-width: 479px) {
      .break-line {
        display: none;
      }
    }

    @media (min-width: 320px) and (max-width: 357px) {
      .fixed_cta {
        font-size: 13px !important;
      }
    }
  </style>
{/literal}
{*End*}
 {*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1} <br />{$h1b}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{if $BC_Switch_Early}{$h2_early} 
	          {elseif $BC_Switch_Main}{$h2_main} 
	          {elseif $BC_Switch_Race_Day}{$h2_race_day}
	          {else}{$h2}{/if}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h1}</span>
        <span>{if $BC_Switch_Early}{$h2_early} 
	          {elseif $BC_Switch_Main}{$h2_main} 
	          {elseif $BC_Switch_Race_Day}{$h2_race_day}
	          {else}{$h2}{/if}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
</div>

{* <!--------- NEW HERO END ---------------------------------------->  *}   