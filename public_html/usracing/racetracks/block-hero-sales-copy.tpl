 {*<!--------- NEW HERO  ---------------------------------------->*}
 
{* Hero logic 
 if $tracktype = harness, hero is harness image.
 			= dog, hero is dog image
 			= empty, hero (default) is /img/states/horse-racing.jpg
 
 
 *}
 
 {assign var="hero" value="/img/states/horse-racing.jpg"}
 {assign var="hero_sm" value="/img/states/horse-racing-sm.jpg"}
 
{literal}
  <style>
    @media (min-width: 1024px) {
      .usrHero {
        background-image: url({/literal} {$hero} {literal});
      }
    }

    @media (min-width: 481px) and (max-width: 1023px) {
      .usrHero {
        background-image: url({/literal} {$hero_sm} {literal});
      }
    }

    @media (min-width: 320px) and (max-width: 480px) {
      .usrHero {
        /*background-image: url({/literal} {$hero_sm} {literal});*/
        display: none;
      }
    }

    @media (max-width: 479px) {
      .break-line {
        display: none;
      }
    }

    @media (min-width: 320px) and (max-width: 357px) {
      .fixed_cta {
        font-size: 13px !important;
      }
    }
  </style>
{/literal}



<div class="usrHero" alt="{$racetrackname} Horse Betting">
    <div class="text text-xl">
        <span>{$h1}</span>
    </div>
    <div class="text text-md copy-md">
        <span>Get up to a {$BONUS_WELCOME} New Member Bonus</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">Sign Up Now</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h1}</span>
        <span>Get up to a {$BONUS_WELCOME} New Member Bonus</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">Sign Up Now</a>
    </div>
</div>

{* <!--------- NEW HERO END ---------------------------------------->  *}   
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>{*This h1 is change in the xml whit the tag h1*}
        <h3 class="kd_subheading">Get up to a {$BONUS_WELCOME} New Member Bonus</h3>
		
{*Sales copy*}		   
        <p><a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> is the best site to bet on horses. Why?  Because, all new members will  get up to a <a href="/promos/cash-bonus-20">{$BONUS_WELCOME} bonus</a> for joining and you can also qualify for an extra  <a href="/promos/cash-bonus-150">{$BONUS_RETENTION} bonus!</a> 

		<p>Enjoy off track horse betting with <a href="/promos/rebates">rebates</a> up to 8% on all your  horse racing wagers paid into your account the very next day. </p>

{*    <p>{include file='/home/ah/allhorse/public_html/racetracks/salesblurb-racetracks-primary.tpl'} </p>   *}

{*End Sales copy*}	

      <p>Get your {$racetrackname} Betting action with <a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> now! </p>
      <p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>
{*
        <p><a href="/signup?ref={$ref}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
*}


<hr>
<h2>{$racetrackname}  Racing </h2>
 