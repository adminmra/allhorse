{if $smarty.server.HTTP_HOST == "www.betusracing.ag"} 
{assign var="cashbonus" value='/promos/150-cash-bonus'} 
{assign var="cashbonus10" value='/promos/10-cash-bonus'}
{assign var="cashbonus20" value='/promos/cash-bonus-20'}
{assign var="rebates" value='/promos/8percent-rebate'} 

{else}
   {assign var="cashbonus" value='/promos/cash-bonus-150'}   
   {assign var="cashbonus10" value='/promos/cash-bonus-10'}
   {assign var="cashbonus20" value='/promos/cash-bonus-20'}
   {assign var="rebates" value='/rebates'} 
 
{/if}
<h2>Bet {$racetrackname} Online!</h2>
<p>Betting {$racetrackname} online has never been easier. Whether you are on your smartphone, tablet, computer you can bet  on races at {$racetrackname} with <a href="https://www.betusracing.ag?ref={$ref}" rel="nofollow" >BUSR</a> anytime or anywhere.   Feel like a VIP with an 8% <a href="{$rebates}">horse betting rebate</a> on horse bets, win or lose, paid to your account the very next day.  For brand new members, you can get a <a href="{$cashbonus20}">20% bonus</a> on your first deposit  and qualify for a <a href="{$cashbonus}">$150 cash bonus</a>!  <a href="/signup?ref={$ref}" rel="nofollow">Join Today! </a> </p>

	{include file='/home/ah/allhorse/public_html/usracing/racetracks/cta_button.tpl'} 
