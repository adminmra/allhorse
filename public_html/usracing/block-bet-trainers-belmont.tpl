   <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index/bet-belmont-stakes.jpg" alt="Bet on the Belmont Stakes" class="bs-card_img"></div>
        <div class="bs-card_half bs-card_content"><img src="/img/index/icon-bet-belmont-stakes.png" alt="Bet on Belmont Stakes" class="bs-card_icon">
          <h2 class="bs-card_heading">Bet on the Trainers</h2>
          <h3 class="bs-card_subheading">And Other Exclusive Derby Props</h3>
          <p>Bob Baffert or Todd Pletcher?  Can Steve Asmussen add a Derby Victory in 2016?  US Racing is the only place to bet on Trainers and Jockeys for the Belmont Stakes.</p>
          <p>Future Wagers for the Breeders' Cup Classic, Triple Crown races and many other events are live, bet now.</p><a href="/belmont-stakes/trainer-betting" class="bs-card_btn">Live Odds</a>
        </div>
      </div>
    </section>