<!--fix this to the new table-->
<h2> {include file='/home/ah/allhorse/public_html/saudi-cup/yearlast.php'} Saudi Cup Results</h2>
 
 <br><br>
 <div id="no-more-tables">
 
 	<table  class="data table table-condensed table-striped table-bordered ordenableResult" width="100%" cellpadding="0" cellspacing="0" border="0" title="Saudi Cup Results" summary="  Saudi Cup Results">	
	
	<thead>	
	<tr >	
	<th>Result</th>	
	<th>PP</th>	
	<th>Horse</th>	
	<th >Jockey</th>	
	<th>Trainer</th>	
	</tr>	
	</thead>	
	<tbody>	
		
	<tr>	
<td data-title="Result">	1	</td>
<td data-title="PP">	12	</td>
<td data-title="Horse">	Mishriff	</td>
<td data-title="Jockey">	David Eagan	</td>
<td data-title="Trainer">	John Gosden	</td>
	</tr>	
	<tr>	
<td data-title="Result">	2	</td>
<td data-title="PP">	9	</td>
<td data-title="Horse">	Charlatan	</td>
<td data-title="Jockey">	Mike Smith	</td>
<td data-title="Trainer">	Bob. Baffert	</td>
	</tr>	
	<tr>	
<td data-title="Result">	3	</td>
<td data-title="PP">	3	</td>
<td data-title="Horse">	Great Scot	</td>
<td data-title="Jockey">	Adel Alfouraidi	</td>
<td data-title="Trainer">	Tom Dascombe	</td>
	</tr>	
		
	</tbody>	
	</table>	

                          </div>


<h2>{include file='/home/ah/allhorse/public_html/saudi-cup/yearlast.php'} Saudi Cup Payouts</h2>                              
<br><br>
<div>
		<table border="0" cellspacing="0" cellpadding="0" class="data table table-condensed table-striped table-bordered" title="Saudi Cup" summary="Saudi Cup Payouts. "  >	
	<tbody><tr>	
	<th>PP</th>	
	<th>Horses</th>	
	<th>Win</th>	
	<th>Place</th>	
	<th>Show</th>	
	</tr>	
	<tr>	
<td>	12	</td>
<td>	Mishriff	</td>
<td>	$41.60	</td>
<td>	$12.80	</td>
<td>	$6.40	</td>
	</tr>	
	<tr>	
		
<td>	9	</td>
<td>	Charlatan	</td>
<td>		</td>
<td>	$3.00	</td>
<td>	$2.40	</td>
	</tr>	
	<tr>	
		
<td>	3	</td>
<td>	Great Scot	</td>
<td>		</td>
<td>		</td>
<td>	$27.40	</td>
	</tr>	
	</tbody>	
	</table>		
</div>
<p></p>
                              
<div id="no-more-tables">
	<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" title="Saudi Cup Payouts" summary="Last year's Payouts of the Saudi Cup. ">		
<thead>		
	<tr>	
	<th>Wager</th>	
	<th>Horses</th>	
	<th>Denomination</th>	
	<th>Payout</th>	
	</tr>	
	</thead>	
	<tbody>	
	<tr>	
<td data-title="Wager">	Exacta	</td>
<td data-title="Horses">	11-3	</td>
<td data-title="Denomination">	$1.00	</td>
<td data-title="Payout">	$69.60	</td>
	</tr>	
	<tr>	
<td data-title="Wager">	Trifecta	</td>
<td data-title="Horses">	11-3-7	</td>
<td data-title="Denomination">	$0.50	</td>
<td data-title="Payout">	$1,219.15	</td>
	</tr>	
	<tr>	
<td data-title="Wager">	Superfecta	</td>
<td data-title="Horses">	11-3-7-8	</td>
<td data-title="Denomination">	$0.10	</td>
<td data-title="Payout">	$835.99	</td>
	</tr>	
	<tr>	
<td data-title="Wager">	Double	</td>
<td data-title="Horses">	2-11	</td>
<td data-title="Denomination">	$1.00	</td>
<td data-title="Payout">	$344.10	</td>
	</tr>	
	<tr>	
<td data-title="Wager">	Pick 3	</td>
<td data-title="Horses">	7-2-11	</td>
<td data-title="Denomination">	$0.50	</td>
<td data-title="Payout">	$5,228.55	</td>
	</tr>	
</tbody>		
</table>				
</div>    
                              
                              
                
{literal}

<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_results.js"></script>
{/literal}