<section class="card">
    <div class="card_wrap">
        <div class="card_half card_content">
            <a href="https://www.betusracing.ag/promos/10-cash-bonus">
                {* <img src="/img/index-kd/icon-kentucky-derby-betting.png" alt="Bet on Kentucky Derby"
                    class="card_icon"> *}
                <img class="card_icon" alt="Saudi Cup 2020 Signup Offer "
                    src="/img/saudi-cup/saudi-cup-icon.png">
            </a>
            <h2 class="card_heading">Get a Sign Up Bonus up to $500 Cash</h2>
            <p>At <a href="/signup?ref={$ref}">BUSR</a>, you are entitled to exceptional new member bonuses. <br /> <br />
            With <a href="/signup?ref={$ref}">BUSR</a>, you'll get a New Member Cash Bonus up to $500 on top of your first deposit. No Limits! Can't get enough? A minimum deposit of $100 qualifies you to receive an additional $150!<br /> <br />
            Let the winning begin at <a href="/signup?ref={$ref}">BUSR</a>! <br />  <br />
			<a href="https://www.betusracing.ag/promos/20-cash-bonus" class="card_btn">Learn More</a>
			</p>
            
        </div>
        <div class="card_half card_hide-mobile"><a href="/promos/20-cash-bonus"><img
                    src="/img/saudi-cup/saudi-cup-betting.jpg"
                    data-src-img="/img/saudi-cup/saudi-cup-betting.jpg" alt="Saudi Cup Odds, Sign up and get a welcome bonus up to $500"
                    class="card_img"></a></div>
    </div>
</section>