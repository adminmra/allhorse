	{literal}
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "about": "2020 Saudi Cup Odds",
        "keywords": "Saudi Cup 2020, 2020 Saudi Cup, Saudi Cup, Saudi Cup odds, Saudi Cup 2020 odds, Saudi Cup betting, Bet on the Saudi Cup, Riyadh, King Abdulaziz Racetrack, World's Richest Horse Race"
    }
    </script>
	{/literal}     <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" summary="The latest odds for the Saudi Cup available for wagering now at BUSR.">
            <caption>Saudi Cup Odds and Contenders</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 27, 2020.</em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
    <tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td data-title=' '>Maximum Security</td><td data-title='Fractional'>2/1</td><td data-title='American'>+200</td></tr><tr><td data-title=' '>Mckinzie</td><td data-title='Fractional'>5/2</td><td data-title='American'>+250</td></tr><tr><td data-title=' '>Mucho Gusto</td><td data-title='Fractional'>11/2</td><td data-title='American'>+550</td></tr><tr><td data-title=' '>Midnight Bisou</td><td data-title='Fractional'>15/2</td><td data-title='American'>+750</td></tr><tr><td data-title=' '>Benbatl</td><td data-title='Fractional'>11/2</td><td data-title='American'>+550</td></tr><tr><td data-title=' '>Gronkowski</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>Tacitus</td><td data-title='Fractional'>14/1</td><td data-title='American'>+1400</td></tr><tr><td data-title=' '>Chrysoberyl</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Magic Wand</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Gold Dream</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Capezzano</td><td data-title='Fractional'>12/1</td><td data-title='American'>+1200</td></tr><tr><td data-title=' '>North America</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Great Scot</td><td data-title='Fractional'>40/1</td><td data-title='American'>+4000</td></tr><tr><td data-title=' '>Mjjack</td><td data-title='Fractional'>50/1</td><td data-title='American'>+5000</td></tr>            </tbody>
        </table>
    </div>
	{literal}
        <style>
            table.ordenable th{cursor: pointer}
			table.ordenable td{ word-wrap: break-word !important; word-break: break-all !important;}
			table.ordenable{ max-width: 100% !important; }
        </style>
        <script src="/assets/js/jquery.sortElements.js"></script>
        <script src="/assets/js/sorttable.js"></script>
	{/literal}
    
	 