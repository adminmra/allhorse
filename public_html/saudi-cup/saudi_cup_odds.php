    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "about": "2021 Saudi Cup Odds",
        "keywords": "Saudi Cup 2021, 2021 Saudi Cup, Saudi Cup, Saudi Cup odds, Saudi Cup 2021 odds, Saudi Cup betting, Bet on the Saudi Cup, Riyadh, King Abdulaziz Racetrack, World's Richest Horse Race"
    }
    </script>
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Saudi Cup Odds"  summary="The latest odds for the Saudi Cup available for wagering now at BUSR.">
            <caption> Saudi Cup Odds and Contenders</caption>
			<tfoot>
                    <tr>
                        <td class="dateUpdated center" colspan="3">
                            <em id='updateemp'>  - Updated May 3, 2021 14:30:22 </em>
                        </td>
                    </tr>			
			</tfoot>
            <tbody>
               <!--
 <tr>
                    <th colspan="3" class="center">
                    Horses - Saudi Cup  - Feb 20                    </th>
            </tr>
-->
    <tr class='head_title'><th>Horse</th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Charlatan </td><td>+150</td><td>3/2</td></tr><tr><td>Knicks Go </td><td>+200</td><td>2/1</td></tr><tr><td>Mishriff </td><td>+700</td><td>7/1</td></tr><tr><td>Tacitus </td><td>+1400</td><td>14/1</td></tr><tr><td>Military Law </td><td>+1200</td><td>12/1</td></tr><tr><td>Chuwa Wizard </td><td>+1200</td><td>12/1</td></tr><tr><td>Max Player </td><td>+1700</td><td>17/1</td></tr><tr><td>Sleepy Eyes Todd </td><td>+2000</td><td>20/1</td></tr><tr><td>Bangkok </td><td>+2500</td><td>25/1</td></tr><tr><td>Extra Elusive </td><td>+3500</td><td>35/1</td></tr><tr><td>Global Giant </td><td>+2500</td><td>25/1</td></tr><tr><td>Great Scot </td><td>+5000</td><td>50/1</td></tr><tr><td>Simsir </td><td>+3300</td><td>33/1</td></tr><tr><td>Derevo </td><td>+2500</td><td>25/1</td></tr>
            </tbody>

        </table>
    </div>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>

    