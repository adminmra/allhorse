<p>What many have pointed out as "The Most Valuable Horse Race", the <a href="/signup?ref={$ref}"}>Saudi Cup</a> is built around enthusiasm and preparations ensuring even the smallest details are taken care of.</p>


<p>The edition at this venue promises a unique display of talent and effort. Expectations are high and so is the mind-blowing $20 Million prize purse. The <a href="/signup?ref={$ref}"}>Saudi Cup</a> attracts thousands of horse racing fans meeting to enjoy an unforgettable horse racing experience in all its splendor.</p>

<p>Mark your calendar for this high profile event on {include_php file='/home/ah/allhorse/public_html/saudi-cup/day.php'}. The <a href="/signup?ref={$ref}"}>Saudi Cup</a> is a world class sports celebration at its finest. Held this year at the elegant King Abdulaziz Racetrack, located Riyadh, Saudi Arabia.</p>
