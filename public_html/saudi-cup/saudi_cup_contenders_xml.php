<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "name": "Saudi World Cup 2020 Contenders",
        "keywords": "2020 Saudi Cup contenders, Saudi cup 2020, Saudi cup odds, Saudi cup 2020 contenders, Saudi cup contenders, King Abdulaziz Racetrack, horse racing, bet Saudi cup, Saudi 2020"
    }
</script>
<style>
    @media (max-width: 800px) {
        caption {
            display: none;
        }

    }
</style>
        <div id="no-more-tables">               
                    <table id="infoEntries" class="table table-condensed table-striped table-bordered ordenableResult data" cellpadding="0"     
            cellspacing="0" border="0" title="2021 Saudi Cup Odds and Contenders"   
            summary="The latest odds for The 2021 Saudi Cup.">  
            <caption  class="hide-sm">2021 Saudi Cup Odds and Contenders</caption>  
            <thead> 
                <tr>
    <th class="first-th">PP</th>            
    <th>Horse</th>          
    <th>Jockey</th>         
    <th>Trainer</th>            
    <th>Owner</th>          
    <th>Odds</th>           
                </tr>
                
            </thead>    
        <tbody>     
    <tr>            
<td data-title="PP">    1   </td>       
<td data-title="Horse"> Chuwa Wizard    </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   Ryuji Okubo </td>       
<td data-title="Owner"> Shinobu Nakanishi   </td>       
<td data-title="Odds">  10/1    </td>       
    <!--     <td data-title="American Odds">+2500 </td>         
    <td data-title="Fractional Odds"> 25/1 </td>            
    -->         
    </tr>           
    <tr>            
<td data-title="PP">    2   </td>       
<td data-title="Horse"> Bangkok </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   Andrew M. Balding   </td>       
<td data-title="Owner"> King Power Racing Co., Ltd. </td>       
<td data-title="Odds">  25/1    </td>       
    <!--     <td data-title="American Odds">+1400 </td>         
    <td data-title="Fractional Odds"> 14/1 </td>            
    -->         
    </tr>           
    <tr>            
<td data-title="PP">    3   </td>       
<td data-title="Horse"> Great Scot  </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   Tom Dascombe    </td>       
<td data-title="Owner"> Empire State Racing Partnership </td>       
<td data-title="Odds">  50/1    </td>       
    <!--     <td data-title="American Odds">+550 </td>          
    <td data-title="Fractional Odds"> 11/2 </td>            
    -->         
    </tr>           
    <tr>            
<td data-title="PP">    4   </td>       
<td data-title="Horse"> Max Player  </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   Steven M. Asmussen  </td>       
<td data-title="Owner"> George E. Hall  </td>       
<td data-title="Odds">  16/1    </td>       
    <!--     <td data-title="American Odds">+2500 </td>         
    <td data-title="Fractional Odds"> 25/1 </td>            
    -->         
    </tr>           
                
    <tr>            
<td data-title="PP">    5   </td>       
<td data-title="Horse"> Knicks Go   </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   Brad H. Cox </td>       
<td data-title="Owner"> KRA Stud Farm   </td>       
<td data-title="Odds">  5/2 </td>       
    <!--     <td data-title="American Odds">+2000 </td>         
    <td data-title="Fractional Odds"> 20/1 </td>            
    -->         
    </tr>           
    <tr>            
<td data-title="PP">    6   </td>       
<td data-title="Horse"> Global Giant    </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   John Gosden </td>       
<td data-title="Owner"> Al Adiyat Racing    </td>       
<td data-title="Odds">  40/1    </td>       
    <!--     <td data-title="American Odds">+750 </td>          
    <td data-title="Fractional Odds"> 15/2 </td>            
    -->         
    </tr>           
    <tr>            
<td data-title="PP">    7   </td>       
<td data-title="Horse"> Tacitus </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   William I. Mott </td>       
<td data-title="Owner"> Juddmonte Farms Inc.    </td>       
<td data-title="Odds">  14/1    </td>       
    <!--     <td data-title="American Odds">+200 </td>          
    <td data-title="Fractional Odds"> 2/1 </td>         
                
    -->            <tr>         
<td data-title="PP">    8   </td>       
<td data-title="Horse"> Sleepy Eyes Todd    </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   Miguel Angel Silva  </td>       
<td data-title="Owner"> David Cobb  </td>       
<td data-title="Odds">  20/1    </td>       
    <!--     <td data-title="American Odds">+550 </td>          
    <td data-title="Fractional Odds"> 11/2 </td>            
    -->         
    </tr>           
    </tr>           
    <tr>            
<td data-title="PP">    9   </td>       
<td data-title="Horse"> Charlatan   </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   Bob. Baffert    </td>       
<td data-title="Owner"> Starlight Racing - SF Racing LLC - Stonestreet Stables LLC - Madaket Stables LLC    </td>       
<td data-title="Odds">  7/5 </td>       
    <!--     <td data-title="American Odds">+250 </td>          
    <td data-title="Fractional Odds"> 5/2 </td>         
                
    -->            </tr>            
    </tr>           
    <tr>            
<td data-title="PP">    10  </td>       
<td data-title="Horse"> Military Law    </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   Musabbeh Al Mheiri  </td>       
<td data-title="Owner"> Nasir Askar </td>       
<td data-title="Odds">  12/1    </td>       
    <!--     <td data-title="American Odds">+1600 </td>         
    <td data-title="Fractional Odds"> 16/1 </td>            
    -->         
    </tr>           
    <tr>            
<td data-title="PP">    11  </td>       
<td data-title="Horse"> Simsir  </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   Fawzi Abdulla Nass  </td>       
<td data-title="Owner"> Victorious  </td>       
<td data-title="Odds">  33/1    </td>       
    <!--     <td data-title="American Odds">+4000 </td>         
    <td data-title="Fractional Odds"> 40/1 </td>            
    -->         
    </tr>           
<td data-title="PP">    12  </td>       
<td data-title="Horse"> Mishriff    </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   John Gosden </td>       
<td data-title="Owner"> Prince A A Faisal   </td>       
<td data-title="Odds">  7/1 </td>       
    <!-- <td data-title="American Odds">+2500 </td>         
    <td data-title="Fractional Odds"> 25/1 </td>            
                
    -->         </tr>           
    <tr>            
<td data-title="PP">    13  </td>       
<td data-title="Horse"> Derevo  </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   Sir Michael Stoute  </td>       
<td data-title="Owner"> K Abdullah  </td>       
<td data-title="Odds">  40/1 </td>       
    <!--     <td data-title="American Odds">+1200 </td>         
    <td data-title="Fractional Odds"> 12/1 </td>            
    -->         
    </tr>           
    <tr>            
<td data-title="PP">    14  </td>       
<td data-title="Horse"> Extra Elusive   </td>       
<td data-title="Jockey">    TBA </td>       
<td data-title="Trainer">   Roger Charlton  </td>       
<td data-title="Owner"> Imad Al Sagar   </td>       
<td data-title="Odds">  33/1    </td>       
    <!--     <td data-title="American Odds">+5000 </td>         
    <td data-title="Fractional Odds"> 50/1 </td>            
    -->         
    </tr>           
        </tbody>        
        </table>        
                       
                        
                        </div>
<style type="text/css">
    table.ordenableResults th {
        cursor: pointer
    }
</style>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_pwc_turf_contenders.js"></script>