{* <a href="https://www.betusracing.ag/signup?ref={$ref}">BUSR</a> is the best place for you to <a href="https://www.betusracing.ag/signup?ref={$ref}">bet on the Saudi Cup</a>. Soon to become the most valuable horse race in history, with a generous $20 million purse. The prize pool is to be distributed among ten competitors. There are several types of bets you can place easily from your computer or mobile device. Find the best picks and bets on the Saudi Cup thanks to the great range of available odds, props, and futures.
<br>
<br>
The <a href="https://www.betusracing.ag/signup?ref={$ref}">Saudi Cup</a> Odds are live. Place your <a href="https://www.betusracing.ag/signup?ref={$ref}">bets now!</a> *}

<p><a href="/signup?ref={$ref}">BUSR</a> is the best place you can bet on the <a href="/signup?ref={$ref}">Saudi Cup</a>. This February, horse racing fans will be heading to the Kingdom of Saudi Arabia to embrace a new addition to the horse racing calendar.

The very first edition of the <a href="/signup?ref={$ref}">Saudi Cup</a> is set for Saturday, Feb 29th at the stunning King Abdulaziz Racetrack that will host 11,000 horse racing fans.</p> 
<p>The organizers have also gone above and beyond with a purse share of 20 million dollars, which makes it the world’s richest horse race.</p> 