 <div id="no-more-tables">
  <table id="sortTable" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" summary="Winners of the Saudi Cup.">  
  <thead> 
  <tr>  
  <th width="70px">Year</th>  
  <th>Winner </th>  
  <th width="70px">Age</th> 
  <th>Jockey </th>  
  <th>Trainer </th> 
  <th>Owner </th> 
  <th width="77px">Time </th> 
  </tr> 
  </thead>  
  <tbody> 
  <tr>  
<td data-title="Year">  2020  </td>
<td data-title="Winner">  Maximum Security  </td>
<td data-title="Age"> 4 </td>
<td data-title="Jockey">  Luis Saez </td>
<td data-title="Trainer"> J. Servis </td>
<td data-title="Owner"> Gary & Mary West  </td>
<td data-title="Time">  1:50.59 </td>
  </tr> 
  <tr>  
<td data-title="Year">  2021  </td>
<td data-title="Winner">  Mishriff  </td>
<td data-title="Age"> 4 </td>
<td data-title="Jockey">  David Eagan  </td>
<td data-title="Trainer"> John Gosden </td>
<td data-title="Owner"> Prince A A Faisal </td>
<td data-title="Time">  1:49.60 </td>
  </tr> 
  </tbody>  
  </table>  
</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>
{/literal}