{literal}
    <style>
        @media (max-width: 1157px) {
            .breakline {
                display: none;
            }
        }

        @media (min-width: 1200px) and (max-width: 1495px) {
            .breakline {
                display: none;
            }
        }
    </style>
{/literal}
<section class="card">
    <div class="card_wrap">
        <div class="card_half card_hide-mobile">
            <a href="https://www.betusracing.ag/login?ref={$ref}&to=sports?leagueId=4272">
                <img src="/img/saudi-cup/specials.jpg" data-src-img="/img/saudi-cup/specials.jpg"
                    alt="2020 Saudi Cup Specials" class="card_img"></a></div>
        <div class="card_half card_content">
            <a href="href="/signup?ref={$ref}">
                {* <img src="/img/index-kd/icon-bet-kentucky-derby.png" alt="Kentucky Derby Odds:  Trainers"
                    class="card_icon"> *}
                <img class="icon-kentucky-derby-betting card_icon"
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABVAQMAAADUnuB8AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABFJREFUeNpjYBgFo2AUDHIAAAOnAAHnt6kCAAAAAElFTkSuQmCC">
            </a>
            <h2 class="card_heading">Bet on the <br class="breakline"> Saudi Cup Specials</h2>
            <p style="margin-bottom: 10px">Anybody can place a bet on which horse will win the Saudi Cup but what about placing a bet on place and show?</p>
            <p><a href="/saudi-cup/odds">Saudi Cup Odds</a>, Props and Futures are live, Bet Now!</p>
            <a href="/signup?ref={$ref}" class="card_btn">Bet on the Saudi Cup Specials</a>
        </div>
    </div>
</section>