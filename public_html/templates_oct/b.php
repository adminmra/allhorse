<link rel="stylesheet" href="//www.betusracing.ag/public/assets/css/signup.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<form class="form" id="signupForm" method="post" action="">
	<section class="signup">
		<div class="container">
			<div class="signup_header"><a href="/" class="signup_logo"><img src="/public/assets/images/busr_logo_images/busr_new_logo/busr.png" alt="BUSR Setting the Pace"></a>
				<h1 class="signup_title">Getting Started</h1>
				<p class="signup_copy">Join the tens of thousands of players who get a great welcome bonus and rebates paid to their accounts daily.</p>
				<a href="/login" class="signup_btn-login"><span class="mobile">I already have an account   ></span><span class="desktop">Sign In</span></a>
			</div>
			<div class="clearfix"></div>
			<div class="signup_form">
				<input type="hidden" value="" name="lid" id="lid">
				<input type="hidden" value="" name="signup_referer" >
				<div class="form_row">
					<div class="form_col-50">
						<div class="form_field">
							<input class="autopost" id="first_name"  maxlength="30" name="first_name" title="Enter your First Name here" type="text" placeholder="First name" value="">
						</div>
						<span id="first_name_error" class="validTD msg-hidden">Please provide first name</span>
					</div>
					<div class="form_col-50">
						<div class="form_field">
							<input class="autopost" id="last_name"  maxlength="30" name="last_name" title="Enter your Last Name here" type="text" placeholder="Last name" value="">
						</div>
						<span id="last_name_error" class="validTD msg-hidden">Please provide last name</span>
					</div>
				</div>
				<div class="form_row">
					<div class="form_col-50">
						<div class="form_field">
							<input class="autopost" placeholder="Password (6 - 10 chars)" maxlength="10" name="password" value="" title="The password must contain 6 or more characters" type="password">
						</div>
						<span id="password_error" class="validTD msg-hidden">Password must have between 6 and 10 characters.</span>
					</div>
					<div class="form_col-50">
						<div class="form_field">
							<input class="autopost" placeholder="Phone" maxlength="25" name="phone" type="text" value="">
						</div>
						<span id="phone_error" class="validTD msg-hidden">Please provide a valid phone number</span>
					</div>
				</div>
				<div class="form_row">
					<div class="form_col-100">
						<div class="form_field">
							<input class="autopost" placeholder="Email Address"  maxlength="50" name="email"  type="text" value="">
						</div>
						<span id="email_error" class="validTD msg-hidden">Please provide a valid email</span>
					</div>
				</div>
				<div class="form_row">
					<div class="form_col-100">
						<div class="form_field">
							<input class="autopost" placeholder="Retype Email Address"  maxlength="50" name="email_confirm"  type="text" value="">
						</div>
						<span id="email_confirm_error" class="validTD msg-hidden">The email addresses you entered don't match</span>
					</div>
				</div>
				<div class="form_row">
					<label class="form_label">Date of Birth:</label>
					<div class="form_col-100">
						<div id="div_birth_date" class="form_row form_row-birth">
							<div class="form_col-33-3">
								<div class="form_field">
									<div class="form_select">
										<select name="dob_month" class="autopost">
											<option value="">Month</option>
											<?php
											for ($x=1; $x<=12; $x++){
												$zx = sprintf("%02d", $x);
												$attrs = "";
												echo "<option value='{$zx}' {$attrs}>{$x}</option>";
											} ?>
										</select>
									</div>
								</div>
							</div>
							<div class="form_col-33-3">
								<div class="form_field">
									<div class="form_select">
										<select name="dob_day" class="autopost">
											<option value="" >Day</option>
											<?php
											for ($x=1; $x<=31; $x++) {
												$zx = sprintf("%02d", $x);
												$attrs = "";
												echo "<option value='{$zx}' {$attrs}>{$x}</option>";
											} ?>
										</select>
									</div>
								</div>
							</div>
							<div class="form_col-33-3">
								<div class="form_field">
									<div class="form_select">
										<select name="dob_year" class="autopost">
											<option value="">Year</option>
											<?php
											for ($x=date("Y")-18; $x>=1900; $x--) {
												$attrs = "";
												echo "<option value='{$x}' {$attrs}>{$x}</option>";
											} ?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<center>
							<span id="birth_date_error" class="validTD msg-hidden">Please provide a valid birth date</span>
						</center>
					</div>
				</div>
				<div class="form_row">
					<div class="form_col-66">
						<div class="form_field">
							<div class="form_select">
								<select class="autopost" name="address_country" title="Select your country of residence here."  onChange="printStateMenu(this.value,'');">
									<?php
									require 'us_states.php';
									foreach ($us_states as $state_code => $state_name) {
										$attrs = "";
										echo "<option value='{$state_code}' {$attrs}>{$state_name}</option>";
									}
									?>
								</select>
								</div>
							</div>
							<span id="address_country_error" class="validTD msg-hidden">Please provide a country</span>
						</div>
						<div class="form_col-33">
							<div class="form_field">
								<input class="autopost" maxlength="15" name="address_postalcode" title="Enter Zip/Postal Code here." placeholder="Zip or postal code" type="text" value="">
							</div>
							<span id="address_postalcode_error" class="validTD msg-hidden">Please provide a zip code</span>
						</div>
					</div>
					<div class="form_row">
						<div class="form_col-100">
							<div class="form_field">
								<input class="autopost" maxlength="180" name="address_street"  value="" placeholder="Street address" title="Please provide your address of residence here." type="text" >
							</div>
							<span id="address_street_error" class="validTD msg-hidden">Please provide an address</span>
						</div>
					</div>
					<div class="form_row">
						<div class="form_col-50">
							<div class="form_field">
								<div class="form_select">
									<select class="autopost" name="address_state" title="Select your State here."></select>
									<script>printStateMenu('US','');</script>
								</div>
							</div>
							<span id="address_state_error" class="validTD msg-hidden">Please provide a state</span>
						</div>
						<div class="form_col-50">
							<div class="form_field">
								<input class="autopost" id="address_city"  maxlength="100" name="address_city"  title="Please provide your city of residence here." type="text" placeholder="City" value="">
							</div>
							<span id="address_city_error" class="validTD msg-hidden">Please provide a city</span>
						</div>
					</div>
					<div class="form_row">
						<label class="form_label">Security Question:</label>
						<div class="form_col-50">
							<div class="form_select">
								<select class="autopost" name="security_question" title="Choose your Security Question here">
									<option value="Mothers maiden name">Mothers maiden name</option>
									<option value="Favorite hobby">Favorite hobby</option>
									<option value="Favorite club">Favorite club</option>
									<option value="Favorite book">Favorite book</option>
									<option value="Childhood hero">Childhood hero</option>
									<option value="Best friends name">Best friends name</option>
									<option value="My pet">My pet</option>
									<option value="My nickname">My nickname</option>
									<option value="My first car">My first car</option>
									<option value="My secret code">My secret code</option>
								</select>
							</div>
							<span id="security_question_error" class="validTD msg-hidden">Please provide a security question</span>
						</div>
						<div class="form_col-50">
							<div class="form_field">
								<input class="autopost" maxlength="50" name="security_answer" title="Enter your Security Asnwer here" type="text" placeholder="Security answer" value="">
							</div>
							<span id="security_answer_error" class="validTD msg-hidden">Please provide a security answer</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="infos">
	<div class="signup_form">
		<div class="form_row">
			<div class="form_col-100">
				<div class="form_btns">
					<!--  <input type="submit" id="button-submit-normal" class="click_disable" value="Create My Account">  -->
					<button type="submit" class="form_submit">Create My Account</button>
					<!-- <input type="submit" id="button-submit-normal form_submit" class="click_disable" value="Create My Account"> -->
				</div>
			</div>
		</div>
		<div class="form_row">
			<div class="form_col-100">
				<p class="form_note">* To receive your winnings we will need your verifiable phone number, email address and billing address. </p>
			</div>
		</div>
		<span id="promo_code_error" class="validTD msg-hidden"></span>
		<input type="hidden" name="postdata" value="1" >
		<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
	</fb:login-button>
</div>
<div class="container-fluid">
	<div class="info"><span class="info_icon plus18"></span>
		<p>BUSR only accepts players over 18 years old.</p>
	</div>
	<div class="info"><span class="info_icon protected"></span>
		<p>Your personal information is safe and every transaction is protected with 256-bit encryption to protect your information.</p>
	</div>
	<div class="info"><span class="info_icon agree"></span>
		<p>By creating an account you agree to our Terms & Conditions.</p>
	</div>
</div>
<!-- <div class="container-fluid"><img src="/public/assets/images/friends.png" alt="" class="friends_img"></div> -->
</section>
</form>

<script>
	<?php require_once "footer_scripts.php"; ?>
</script>