    </main>
    <?php //ini_set("display_errors", "on")?>
    <div id="login-box-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-md" style="top:-70px;" >
        <div class="modal-content" style="max-width:320px; margin:0 auto;">
          <!--<div class="header-logo"></div>-->
          <a href="javascript:void()" onclick="hide_login_form()" style="
              display: block;
              right: 10px;
              padding: 15px;
              float:right;
              top: 10px;
              color: white;
              text-decoration:none;
          ">X</a>
          <div class="modal-header nobottomborder">
              <img src="/public/assets/images/busr_logo_images/busr_new_logo/busr.png" style="max-width:100%; padding:0px 20px 0 20px;" alt="">
              <div class="modal-spacer" style="padding:20px 20px 10px 20px;">
                  <p>
                      Hi there.  Our records indicate that you already have an account with us. Account <?php echo isset($login_account) ? $login_account : ""; ?> is registered to the email address you just entered. <br><br>  Please Login below or select  Forgot Password to reset your password.
                  </p>
              </div>
          </div>
          <hr class="modalbox2" />
          <div id="login-box" class="modal-body">
              <form class="navbar-form"  style="padding:0 15px; margin-left:0; margin-right:0;" action="/login" method="get" id="loginform" name="loginform" role="form" onsubmit="this.ctl00$MainContent$Password.value = this.ctl00$MainContent$Password.value.toUpperCase();" >
                  <input type="hidden" style="width:100%;" name="ctl00$MainContent$Account" value="<?php echo isset($login_account) ? $login_account : ""; ?>">
                  <input type="submit" style="width:100%;" class="btn btn-login" id="ctl00_MainContent_ctlLogin_BtnSubmit" value="Login" name="ctl00$MainContent$ctlLogin$BtnSubmit">
                  <!--<input type="button" class="btn btn-default" name="Close" value="Close" onclick="hide_login_form();">-->
              </form>
              <div style="padding:0px 15px;">
              <a class="btn btn-forgot" style="width:100%;" href="/forgot-password">Forgot password</a></div>
              <a class='btn-tel' href="tel:1-844-BET-HORSES" style="text-align:center;">Or, you can call 1-844-BET-HORSES to speak with representative.</a>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script>
      document.documentElement.className = 'js';
      function is_int(value){
          if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
              return true;
          }else{
              return false;
          }
      }
      $(function() {
          // Handle Zip Code
        $("#primary_address_postalcode").blur(function() {
          console.log('blur');
          var zipcode = $(this).val();
          var country = $("#primary_address_country").val();
          if ((zipcode.length == 5) && (is_int(zipcode)) && country == "US")  {
            $.get( "signup.php?getzip=" + zipcode, function(data) {
              data = $.parseJSON(data);
              printStateMenu('US',data.Table.STATE);
              $("#primary_address_city").val(data.Table.CITY);
              $("#primary_address_state").val(data.Table.STATE);
            })
          };
        });
      });
    </script>
    <script>
        function leocapitalize(str) {
            strVal = '';
            str = str.split(' ');
            for (var chr = 0; chr < str.length; chr++) {
                strVal += str[chr].substring(0, 1).toUpperCase() + str[chr].substring(1, str[chr].length).toLowerCase() + ' '
            }
            return strVal;
        }

        $( "input#first_name" ).bind( "change", function( evt ){
                $(this).val( leocapitalize($(this).val()) ) ;
        } );

        $( "input#last_name" ).bind( "change", function( evt ){
                $(this).val( leocapitalize($(this).val()) ) ;
        } );
    </script>
    <script>
        function show_login_form(){
            //$("#login-box").fadeIn();
            $('#login-box-modal').modal('show');
        }
        function hide_login_form(){
            //$("#login-box").fadeOut();
            $('#login-box-modal').modal('hide');
        }
        <?php if(array_key_exists("email_exists", $registration->errors)) { ?>
        $(document).ready(function(){
            show_login_form();
        });
        <?php } ?>

    </script>

    <script>
        //window.errors = <?php $registration->errors; ?>;

        <?php

            if ($registration->errors) {
              echo "$(\".form_field\").addClass(\"success\");";
            }
            $focus_first = true;
            foreach($registration->errors as $error){
                if($focus_first){
                    echo "$(\"[name='".$error."']\").focus();";
                    $focus_first=false;
                }

                if ( $error == "birth_date" ) {
                  echo "$(\"[id=div_birth_date]\").find(\".form_field\").addClass(\"error\");";
                }
                else{
                  echo "$(\"[name='".$error."']\").parents( \".form_field\" ).addClass(\"error\");";
                }

                echo 'console.log("errrorr!!!!!");';
                echo "$(\"#".$error."_error\").removeClass(\"msg-hidden\");";
                echo "$(\"#".$error."_error\").addClass(\"msg-error\");";
                /* For v3 */
                echo "$(\"[data='"."{$error}_error"."']\").removeClass('msg-hidden');";
                echo "$(\"[data='"."{$error}_error"."']\").addClass('msg-error');";
            }
        ?>
        function FillPromoCode( promoCode ){
          $("#promo_code").val(promoCode);
        }
    </script>
    <?php include ROOT . '/includes/analytics.php'?>
    <script type="text/javascript"> new Image().src = '//clickserv.pixel.ad/conv/d2e70b3a55daa742'; </script>
</body>
</html>