<?php if ( ! isset( $_SESSION ) ) session_start();?>
<?php
  $assets = "/public/assets/";
 ?><!DOCTYPE html>
<!--if lt IE 7html.no-js.lt-ie9.lt-ie8.lt-ie7
-->
<!--if IE 7html.no-js.lt-ie9.lt-ie8
-->
<!--if IE 8html.no-js.lt-ie9
-->
<!--[if gt IE 8]><!-->
<html class="no-js">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta name="language" content="en">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>Sign Up at bet &#9733; usracing</title>
    <meta property="og:title" content="Sign Up at bet &#9733; usracing" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.betusracing.ag/signup" />
    <meta property="og:description"  content="Get a cash bonus, daily rebates, free bets, exclusive racing odds found nowhere else so and much more!" />
    <meta property="og:image" content="https://www.betusracing.ag/public/assets/images/fb-bh.jpg" />
    <link href="https://fonts.googleapis.com/css?family=Lato:400,300,700" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo $assets; ?>css/signup.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="<?php echo $assets; ?>js/countries-states.js"></script>
    <!-- <link rel="stylesheet" type="text/css" href="//www.betusracing.ag/wp-content/themes/betusracing/css/bootstrap.min.css" /> -->
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <?php include ROOT . '/includes/chat.php';?>
  </head>
  <body>
    <main>
