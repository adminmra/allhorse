<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$config['db']['host']   = "allhorse.cphi3kk40pqq.us-west-1.rds.amazonaws.com";
$config['db']['user']   = "ah_allhorse";
//$config['db']['pass']   = "PAe2Sg~SQbqHx&i";
$config['db']['pass']   = "6q8aK7bvStRgQAhf~";
$config['db']['dbname'] = "webhook";

$hook = new \Slim\App(["settings" => $config]);

$container = $hook->getContainer();
$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'],
        $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};


$hook->get('/', function(Request $request, Response $response) {
	echo "";
});
$hook->post('/', function(Request $request, Response $response) {
	$req = $request->getParams();

    $firstname = $req['lead']['firstName'];
    $firstname = ucwords($firstname);
    
    $email = $req['lead']['email'];
    $campaign = $req['campaign']['title'];
    $website = $req['lead']['referrer'];

    $client = new \GuzzleHttp\Client();

    $url_mib_vtiger = 'http://css.school-metrics.com/modules/Webforms/capture.php';
    //$url_aws_vtiger = 'http://54.183.196.206/vtigercrm/modules/Webforms/capture.php';
    $url_old_vtiger = 'https://usracing.od1.vtiger.com/modules/Webforms/capture.php';

    $response_mib_vtiger = $client->request(
        'POST',
        $url_mib_vtiger,
        [
            'form_params' => [
                'email' => $email,
                'publicid'=>'4979fb44206e04a776152634d322776a',
                'lastname'=>' ',
                'firstname' => $firstname,
                'website' => $website,
                'label:Campaign' => $campaign
            ]
        ]
    );

    /*$response_aws_vtiger = $client->request(
        'POST',
        $url_aws_vtiger,
        [
            'form_params' => [
                'email' => $email,
                'publicid'=>'87d94d13fe6182a5d01cf15f40504252',
                'lastname'=>' ',
                'firstname' => $firstname,
                'website' => $website,
                'label:Campaign' => $campaign
            ]
        ]
    );*/

    $response_old_vtiger = $client->request(
        'POST',
        $url_old_vtiger,
        [
            'form_params' => [
                'email' => $email,
                'publicid'=>'0d52f01037196d0f0397f48689a6739a',
                'lastname'=>' ',
                'firstname' => $firstname,
                'website' => $website,
                'cf_1185' => $campaign
            ]
        ]
    );

    $mib = $response_mib_vtiger->getBody();
    //$aws = $response_aws_vtiger->getBody();
    $old = $response_old_vtiger->getBody();
    //$for_db = "MIB:{$mib}-AWS:{$aws}-OLD:{$old}";
    $for_db = "MIB:{$mib}-OLD:{$old}";

	$stmt = $this->db->prepare('INSERT INTO history (response) VALUES (:res)');
    $stmt->execute(array(':res'=>$for_db));
    return $response;
    //echo json_encode($stmt);
});

$hook->run();
