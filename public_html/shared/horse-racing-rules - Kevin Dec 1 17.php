<div class="headline">
    <h1>Horse Racing Rules</h1>
</div>
<div class="content">
<p class="lead">BUSR offers daily racing from tracks within the USA (and internationally) via the racing interface.</p>
<p class="lead">
    We accept wagers up until post time. We cannot assume liability for wagers that are unsuccessfully entered before post time. Before logging-out, please check your Active Wagers on the Reports menu to make sure your bets were accepted.  Click on the Racebook and then HISTORY on the Bet Slip.
</p>
<p>Once you submit a bet online, your wager is considered final.</p>
<p>
    Bets shall not be accepted after post-time. In the event that the race begins before the advertised post-time and for that reason (or any other reason) you are able to place a wager after the race has started,
that wager will be void.
</p>
<p>
    Payouts are based on the actual track payout. The morning lines provided when wagering on the Horse Racing Interface are for informational purposes only and not used to calculate the payout.
</p>
<p>There are no house odds. If there are no track payoffs for a certain type of wager, all wagers on that type will be refunded.</p>
<p>In the event a new track is not listed in one of the categories, its default payout limit will be Category E.</p>
<p>
    In the event that there is evidence of pool manipulation, all affected wagers will be deemed void and all monies wagered on the affected wagers will be refunded.
</p>
<p>
    Management reserves the right to refuse or limit any wager and to restrict wagering on any event at any time without any advance notice.
</p>
<h2>Scratches</h2>
<p>If a horse is scratched, all Win/Place/Show wagers placed via the racing interface will be refunded.</p>
<p>
    <strong>
        <i>
            This does not apply to "Odds to Win" bets placed via the sports betting interface. On those bets a scratch counts as a loss.  For example, if you place a Kentucky Derby Future wager on a horse that later does not compete in the Kentucky Derby for any reason, it is a loss.  Future Wagers in the Sportsbook are considered "All Action."
        </i>
</strong>
</p>
<p>In case a horse is considered a non-starter by the track, that horse will be considered a scratch and refunded accordingly.</p>
<p>
    In exotic wagers (Exacta, Trifecta, Superfecta, Quinella) the portion containing the scratched horse will be refunded. We do not honor "all" payouts in any exotic wager even though the track may do so. In the case that there is an "all" payout on a particular race, we will replace the all payout with the rightful winner of that race and/or position.
</p>
<p>
    On Daily Doubles, Pick 3 and Pick 4 wagers, a scratch will result in an automatic refund of the combination including the scratched horse. There will be no consolation payouts. No special track payouts will be recognized:
</p>
<h2>Pick 3 and Pick 4 Grading and Payout Rules</h2>
<p>
    All Pick 3 and Pick 4 tickets are paid out based on the official race results published by the track at which the race was run. If there is a scratched horse in any Pick 3 or Pick 4, only combinations including the scratched horse will be refunded.
</p>
<p>
    There will be no consolation payouts. No special track payouts (such as payouts on scratched horses, 2 out of 3 special Pick 3 payouts and/or 3 out of 4 special Pick 4 payouts) will be recognized.
</p>
<h2>Official Source of Results</h2>
<p>
    For thoroughbred racing we use <a href="www.equibase.com">www.equibase.com</a> as our official source of results. For harness racing we use <a href="www.ustrotting.com">www.ustrotting.com</a> or the tracks.
</p>
<p>In the event of a discrepancy, we will contact the track for the official result.</p>
<h2>Daily Rebate</h2>
<p>BUSR offers a generous rebate program.</p>
<div class="row">
    <div class="col-sm-11 col-md-11 col-lg-11">
        <table border="1" class="data table table-condensed table-striped table-bordered" width="50%" cellpadding="0" cellspacing="0" summary="">
            <thead>
                <tr>
                    <td></td>
                    <td><strong>Straights</strong></td>
                    <td><strong>Exotics</strong></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Category A</td>
                    <td>3%</td>
                    <td>8%</td>
                </tr>
                <tr>
                    <td>Category B</td>
                    <td>3%</td>
                    <td>5%</td>
                </tr>
                <tr>
                    <td>Category C</td>
                    <td>3%</td>
                    <td>3%</td>
                </tr>
                <tr>
                    <td>Category D</td>
                    <td>No rebate</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Category E</td>
                    <td>No rebate</td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<p>
    This rebate only applies to wagers placed through the horse-wagering interface. It does not apply to any horse matchups or other future wagers made through the sports betting login. No rebate will be given on cancelled wagers or wagers refunded due to a scratch. No rebate will be paid on Win, Place and Show tickets that pay $2.20 to $2 or less. The rebate bonus will be paid on a daily basis. There is no rollover requirement associated with the rebate.
</p>
<h2>Matchups</h2>
<ul>
    <li>The following rules apply to horse matchup bets made via the sports betting interface.</li>
    <li>In matchups between two or more named horses, all horses must go for wagers to have action.</li>
    <li>Matchups are determined by horse name. Any track coupling is irrelevant towards determining the winner of a matchup.</li>
    <li>
        In matchups between one named horse and the Field, wagers will have action if and only if the named horse starts. Scratches of horses in the Field will not affect the standing of the wager.
    </li>
    <li>Matchups are not eligible for rebates.</li>
    <li>
        The winner of a matchup will be determined by the highest placed finisher involved in the matchup. If no horse finishes, the matchup will be graded no action.
    </li>
    <li>Matchup wagers which indicate Turf Only will be cancelled if the surface is changed.</li>
    <li>In the case of a dead heat, the matchup will be graded no action.</li>
</ul>
<h2>PROMOTIONS</h2>
<p>
    If, as part of a promotion offered by BUSR, you receive betting cash, you can only use such amount as wagers for playing in the games offered by the company. You are not entitled to withdraw such amounts and we reserve the right to reclaim those amounts if you do not use them to play the games during the period specified in the promotion.
</p>
<p>Only members of BUSR can redeem promotional prizes and winnings from promotions.</p>
<p>
    If you have more than one betting account, all promotional credits and/or winnings will be null and void and they will be deducted from your account.
</p>
<p>
    If a member wagers both sides of a game using a promotional bet (to bet at the same time on the favorite and the underdog) or using bonus money, Management considers that to be fraudulent behavior. This includes making bets of this nature in two or more of our affiliated websites. Any money won playing both sides of the game will be deducted, together with any other promotional money deposited in the member" account.
</p>
<p>Management reserves the right to modify any promotion rules or cancel promotions at its own discretion.</p>
</div>