
<div class="table-responsive">
                  <table class="table table-condensed table-striped table-bordered"  border="0" cellpadding="0" cellspacing="0" title="Racetrack Rebate Categories" subject="The category in which the track lies in the rebate schedule.">
                  <caption>BUSR Track Categories</caption>
                   <tbody>
                     <tr >
                      <td><strong>Category A</strong></td>
                      <td><strong>Category B</strong></td>
                      <td><strong>Category C</strong></td>
                      <td><strong>Category D</strong></td>
                      <td><strong>Category E</strong></td>
                    </tr>
                    <tr>
                      <td>Aqueduct</td>
                      <td>Arlington Park</td>
                      <td>Aintree</td>
                      <td>Australia A</td>
                      <td>Bluffs</td>
                    </tr>
                    <tr>
                      <td>Belmont Park</td>
                      <td>Canterbury Park</td>
                      <td>Albuquerque</td>
                      <td>Australia B</td>
                      <td>Daytona Beach Evening</td>
                    </tr>
                    <tr>
                      <td>Churchill Downs</td>
                      <td>Charles Town</td>
                      <td>Arapahoe Park</td>
                      <td>Australia C</td>
                      <td>Daytona Beach Matinee</td>
                    </tr>
                    <tr>
                      <td>Del Mar</td>
                      <td>Delaware Park</td>
                      <td>Ascot Racecourse</td>
                      <td>Balmoral Park</td>
                      <td>Derby Lane Evening</td>
                    </tr>
                    <tr>
                      <td>Gulfstream Park</td>
                      <td>Delta Downs</td>
                      <td>Atlantic City</td>
                      <td>Buffalo Raceway</td>
                      <td>Derby Lane Matinee</td>
                    </tr>
                    <tr>
                      <td>Gulfstream Park West</td>
                      <td>Emerald Downs</td>
                      <td>Ayr</td>
                      <td>Dover Downs</td>
                      <td>Hazel Raceway</td>
                    </tr>
                    <tr>
                      <td>Keenland</td>
                      <td>Fair Grounds</td>
                      <td>Ballinrobe</td>
                      <td>Evangeline Downs</td>
                      <td>Horsemen Park</td>
                    </tr>
                    <tr>
                      <td>Meydan</td>
                      <td>Finger Lakes</td>
                      <td>Bangor-on-Dee</td>
                      <td>Ferndale</td>
                      <td>Jacksonville EVE</td>
                    </tr>
                    <tr>
                      <td>Monmouth Park</td>
                      <td>Golden Gate Fields</td>
                      <td>Bath</td>
                      <td>Folkestone</td>
                      <td>Mardi Gras Evening</td>
                    </tr>
                    <tr>
                      <td>Parx Racing</td>
                      <td>Hoosier Park</td>
                      <td>Bellewstown</td>
                      <td>Freehold Raceway</td>
                      <td>Mardi Gras Matinee</td>
                    </tr>
                    <tr>
                      <td>Santa Anita Park</td>
                      <td>Laurel Park</td>
                      <td>Belterra</td>
                      <td>Fresno</td>
                      <td>Orange Park Evening</td>
                    </tr>
                    <tr>
                      <td>Saratoga</td>
                      <td>Lone Star Park</td>
                      <td>Beverly</td>
                      <td>Happy Valley</td>
                      <td>Orange Park Matinee</td>
                    </tr>
                    <tr>
                      <td>Tampa Bay Downs</td>
                      <td>Los Alamitos</td>
                      <td>Brighton</td>
                      <td>Harrah's Philadelphia</td>
                      <td>Palm Beach Evening</td>
                    </tr>
                    <tr>
                      <td>Tucson</td>
                      <td>Los Alamitos Racecourse</td>
                      <td>Cal Expo</td>
                      <td>Harrington Raceway</td>
                      <td>Palm Beach Matinee</td>
                    </tr>
                    <tr>
                      <td>Woodbine</td>
                      <td>Louisiana Downs</td>
                      <td>Carlisle</td>
                      <td>Hoosier Park Harness</td>
                      <td>Retama Park</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Oakland Park</td>
                      <td>Cartmel</td>
                      <td>Indiana Downs</td>
                      <td>Saratoga Harness Early</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Penn National</td>
                      <td>Catterick</td>
                      <td>Maywood Park</td>
                      <td>Solano</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Pimlico</td>
                      <td>Chantilly Racecourse</td>
                      <td>Meadows Harness</td>
                      <td>Southland Evening</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Prairie Meadowns</td>
                      <td>Chelmsford City</td>
                      <td>Mohawk</td>
                      <td>Southland Matinee</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Presque Isle Downs</td>
                      <td>Cheltenham</td>
                      <td>Monticello Raceway</td>
                      <td>Tri-State Evening</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Sam Houston Race Park</td>
                      <td>Chepstown</td>
                      <td>Northfield Park</td>
                      <td>Tri-State Matinee</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Sunland Park</td>
                      <td>Chester</td>
                      <td>Pleasanton</td>
                      <td>Wheeling Downs Evening</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Turfway Park</td>
                      <td>Clonmel</td>
                      <td>Plumpton</td>
                      <td>Wheeling Downs Matinee</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Colonial Downs</td>
                      <td>Pocono Downs</td>
                      <td>Zia Park</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Columbus Park</td>
                      <td>Pocono Downs Early</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Cork Racecourse</td>
                      <td>Pompano Park</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Curragh</td>
                      <td>Sacramento</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Deauville-La Touques</td>
                      <td>Santa Rosa</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Doncaster</td>
                      <td>Saratoga Harness</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Down Royal</td>
                      <td>Sha Tin</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Downpatrick</td>
                      <td>Stockton</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Dundalk</td>
                      <td>Timonium</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Durbanville</td>
                      <td>Tipperary</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Ellis Park</td>
                      <td>Western Fair Raceway</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Epsom Downs</td>
                      <td>&nbsp;</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Exeter</td>
                      <td>Will Rogers Downs</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Fairmount Park</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Fairview</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Fairyhouse Racecourse</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Fakenham</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Ffos Las</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Flamingo</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Fontwell</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Fort Erie</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Galway</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Goodwood</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Gowran</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Greyville</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Hamilton</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Hastings Park</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Hawthorne</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Haydock</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Hereford</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Hexham</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Hialeah Park</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Huntingdon</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Kelso</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Kempton</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Kenilworth</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Kentucky Downs</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Kilbeggan</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Killarney</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Laytown</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Leicester</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Leopardstown</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Limerick</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Lingfield</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Listowel</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Longchamp</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Ludlow</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Mahoning Valley</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Market Rasen</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Meadowlands</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Meadowlands Harness</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Mountaineer Park</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Musselburgh</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Naas Racecourse</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Navan</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Newbury Racecourse</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Newcastle</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Newmarket</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Newton Abbot</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Nottingham</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Perth</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Pontefract</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Portland Meadows</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Punchestown</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Redcar</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Remington Park</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Ripon</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Roscommon</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Ruidoso Downs</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Saint-Cloud</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Salisbury</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Sandown</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Scottsville</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Sedgefield</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Sligo</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Southwell</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Stratford</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Suffolk Downs</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>SunRay Park</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Taunton</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Thirsk</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Thistledown</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Thurles</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Towcester</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Tramore</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Turf Paradise</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Turffontein</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Uttoxeter</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Vaal</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Warwick</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Wetherby</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Wexford</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Wincanton</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Windsor</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Wolverhampton</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Woodbine Harness</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Worcester</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Yarmouth</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Yavapai Downs</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>Yonkers Raceway Harness</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td>York</td>
                      <td></td>
                      <td></td>
                    </tr>
                  


                  </tbody>
                </table>
              </div><!-- table-responsive -->