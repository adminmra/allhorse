<link rel="stylesheet" type="text/css" href="/css/no-more-tables.css">		
<style>
    .overflow{
        overflow: scroll;
        height: 500px;
        overflow-x: hidden;
    }
</style>	
<div id="no-more-tables">
                    <table class="table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                            <tr>
                                <th>Bet Type</th>
                                <th>Class A</th>
                                <th>Class B</th>
                                <th>Class C</th>
                                <th>Class D</th>
                                <th>Class E</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-title="Bet Type"><strong>Win</strong></td>
                                <td data-title="Class A">Track Odds</td>
                                <td data-title="Class B">Track Odds</td>
                                <td data-title="Class C">Track Odds</td>
                                <td data-title="Class D">Track Odds</td>
                                <td data-title="Class E">Track Odds</td>
                            </tr>
                            <tr>
                                <td data-title="Bet Type"><strong>Place</strong></td>
                                <td data-title="Class A">Track Odds</td>
                                <td data-title="Class B">Track Odds</td>
                                <td data-title="Class C">Track Odds</td>
                                <td data-title="Class D">Track Odds</td>
                                <td data-title="Class E">Track Odds</td>
                            </tr>
                            <tr>
                                <td data-title="Bet Type"><strong>Show</strong></td>
                                <td data-title="Class A">Track Odds</td>
                                <td data-title="Class B">Track Odds</td>
                                <td data-title="Class C">Track Odds</td>
                                <td data-title="Class D">Track Odds</td>
                                <td data-title="Class E">Track Odds</td>
                            </tr>
                            <tr>
                                <td data-title="Bet Type"><strong>Exacta</strong></td>
                                <td data-title="Class A">500-1</td>
                                <td data-title="Class B">400-1</td>
                                <td data-title="Class C">300-1</td>
                                <td data-title="Class D">300-1</td>
                                <td data-title="Class E">300-1</td>
                            </tr>
                            <tr>
                                <td data-title="Bet Type"><strong>Quinella</strong></td>
                                <td data-title="Class A">400-1</td>
                                <td data-title="Class B">300-1</td>
                                <td data-title="Class C">300-1</td>
                                <td data-title="Class D">300-1</td>
                                <td data-title="Class E">300-1</td>
                            </tr>
							<tr>
                                <td data-title="Bet Type"><strong>Trifecta</strong></td>
                                <td data-title="Class A">1000-1</td>
                                <td data-title="Class B">750-1</td>
                                <td data-title="Class C">300-1</td>
                                <td data-title="Class D">300-1</td>
                                <td data-title="Class E">300-1</td>
                            </tr>
                            <tr>
                                <td data-title="Bet Type"><strong>Superfecta</strong></td>
                                <td data-title="Class A">1500-1</td>
                                <td data-title="Class B">1000-1</td>
                                <td data-title="Class C">300-1</td>
                                <td data-title="Class D">300-1</td>
                                <td data-title="Class E">300-1</td>
                            </tr>
                            <tr>
                                <td data-title="Bet Type"><strong>Pick 3</strong></td>
                                <td data-title="Class A">1500-1</td>
                                <td data-title="Class B">1000-1</td>
                                <td data-title="Class C">300-1</td>
                                <td data-title="Class D">300-1</td>
                                <td data-title="Class E">300-1</td>
                            </tr>
                            <tr>
                                <td data-title="Bet Type"><strong>Pick 4</strong></td>
                                <td data-title="Class A">5000-1</td>
                                <td data-title="Class B">2000-1</td>
                                <td data-title="Class C">300-1</td>
                                <td data-title="Class D">300-1</td>
                                <td data-title="Class E">300-1</td>
                            </tr>
                            <tr>
                                <td data-title="Bet Type"><strong>Daily Double</strong></td>
                                <td data-title="Class A">750-1</td>
                                <td data-title="Class B">400-1</td>
                                <td data-title="Class C">300-1</td>
                                <td data-title="Class D">300-1</td>
                                <td data-title="Class E">300-1</td>
                            </tr>
                            <tr>
                                <td data-title="Bet Type"><strong>Maximum Payout</strong></td>
                                <td data-title="Class A">$20,000.00</td>
                                <td data-title="Class B">$15,000.00</td>
                                <td data-title="Class C">$10,000.00</td>
                                <td data-title="Class D">$3,000.00</td>
                                <td data-title="Class E">$1,000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
	