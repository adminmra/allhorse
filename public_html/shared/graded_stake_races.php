






          
<div class="headline"><h1>Graded Stake Races</h1>
</div>



<div id="left-col" class="col-md-9">
  <div class="content">
    <p>BUSR presents the the complete graded stakes races schedule for {'Y'|date}.  At BUSR can bet from your home computer, mobile or tablet for any of these following races.  You will get up to an 8% <a href="/rebates">horse betting rebate</a> and there is a <a href="/cash-bonus">$150 Bonus</a> for new members. <a href="/signup/">Join </a> Today!</p>
    {*include file='includes/ahr_block_graded_stakes_schedule.tpl'*}
    <h2>Upcoming Stakes Races</h2>
    <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>
    {include file='includes/ahr_block_graded-schedule-new.tpl'}
    <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>
    <h2>Stakes Results</h2>
    {include file='includes/ahr_block_graded-schedule-old.tpl'}
    <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>
    <!-- ------------------------ content ends -------------------------- -->
  </div>
  <!-- end/ content -->
</div>
<p>