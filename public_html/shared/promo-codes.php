<!-- <p><img class="img-responsive" src="https://www.usracing.com/img/promos/promo-codes.jpg" alt="Horse Betting Promo Codes"></p> -->
          
<div class="headline"><h1>Promo Codes</h1></div>


<p><strong>I have a promo code, what do I do next?</strong></p>

<p>After you login to your account, please click on the DEPOSIT link to get to the cashier.  At the top of the Cashier page you will see an option to enter in your promo code and claim your bonus.</p>
<p>Please note that some promotions are seasonal. Likewise, promo codes can only be used once per account and household.</p>



<p>The bonus will be added to your account immediately after making the deposit.</p>
<p>If you make multiple deposits on the same day, you will only receive a a bonus based on your first deposit.</p>
<p>All promos are on Eastern Standard time (EST)</p>
<p>Reload bonuses and other cash rewards are subject to the following standard rollover requirements:
<ul><li>Sports betting activity equal to 30 times the amount of the bonus.</li>
<li>Casino activity equal to 100 times the amount of the bonus. Craps and Roulette (all variations) and certain casino games, as specified in the Casino Rules, do not apply towards rollover requirements.</li>
<li>Blackjack activity equal to 200 times the amount of the bonus.</li>
<li>Rollover requirements must be met within 15 days of your deposit.</li></ul>
<p>You cannot make a deposit, receive the bonus funds and then withdraw your deposit and bet with the bonus funds.</p>
<p>You cannot open multiple accounts to abuse promotions or bonuses.</p>
<p>Management reserves the right to modify or cancel this promotion at any time. General house rules/terms and conditions apply.</p>
<p>Thank you for playing with BUSR and we hope you enjoy our promotions.</p>
<p>If you have any questions at all, please contact Customer Service via live chat, email or call us at 1-844-444-BUSR.</p>
