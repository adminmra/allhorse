<h2>100% Reload Bonus up to $200</h2>

<ul>
<li>This promotion is for existing members.
</li><li>This promotion cannot be combined with any other signup bonus for existing players
</li><li>To qualify, you must wager at least 5 times your bonus ("five time roll over") if taken as FREE PLAY and 10 times your bonus ("ten time roll over") if taken as cash. Roll over must be completed within 30 days of your deposit for this promotion.</li>

<blockquote><strong>Example:</strong>  You deposit $100.  Your account will automatically be credited another $100, for a total of $200.   You must make cumulative wagers of $1,000 ($200 x 5 = five time roll over) in order to be able to withdraw your bonus cash for FREE PLAY and cumulative wagers of $2,000 ($200 x 10 = ten time roll over) for cash.</blockquote>

<li>Once you qualify, your cash bonus will be automatically deposited into your racing account.
</li><li>Wagers must be made online.
</li><li>Wagers must be made at the US Racing website.
</li><li>Wagers must be made in the sportsbook only.  Wagers made in the racebook or casino do not count towards roll over requirements for this promotion.
</li><li>This promotion may be suspended by the Management without notice.
</li><li>If you have any questions about this promotion, please contact Customer Support.
</li><li>This promotion is void where prohibited. All terms and conditions will apply for qualification.</li>
</ul>