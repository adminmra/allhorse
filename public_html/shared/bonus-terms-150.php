<h2>Get your $150 Cash Bonus today!</h2>

<p><strong>Here's how our $150 Bonus Works:</strong> &nbsp;</p>
<ul>
<li>This promotion is for new players only</li>
<li>To qualify, you must deposit a minimum of $100 and wager at least $500 ("cumulative wager") within 30 days of registering your account.</li>
<li>Once you qualify, contact us directly and we will credit your account.</li>
<li>TVG, TwinSpires, Xpressbet and BetAmerica players are eligible for this promotion!</li>
<li>Wagers must be made online.</li>
<li>Wagers must be made at the BUSR website.</li>
<li>Wagers must be made in the racebook only.</li>
<!--<li>Wagers placed on the Kentucky Derby, Preakness Stakes, Belmont Stakes and Breeders' Cup Classic do NOT count towards your cumulative wager.</li>-->
<li>This promotion may be suspended by the Management without notice.</li>
<li>Bonus cash must be wagered once before it can be withdrawn from player's account. In other words, after you receive your $150 Cash Bonus, you must make wagers of $150 ("one time roll over") in the racebook.  Afterwards, you may withdraw those funds from your racing account.</li>

<li>If you have any questions about this promotion, please contact Customer Support.</li>
<li>This promotion is void where prohibited. All terms and conditions will apply for qualification.</li>
<p></p>

</ul>