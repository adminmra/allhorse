<div class="headline">
	<h1>Triple Crown Reward</h1>
</div>



<p>The most waited horse racing competition of the year is here! BUSR is giving you an excellent reason to bet with us during the Triple Crown. Place any wager of $25 or more on the Kentucky Derby, Preakness Stakes and Belmont Stakes and receive:<br>
  <br>
  <strong>• $25 CASH BONUS <br>
  • $25 in BLACKJACK <br>
  • 100% RELOAD BONUS </strong><br>
<br></p>
<hr>
<p><strong>Terms and Conditions</strong></p>
<ul>
  <li>This promotion is for active and new players.</li>
  <li>To qualify, you must wager on our racebook $25 or more at the Kentucky Derby, Preakness Stakes and Belmont Stakes, you can bet on different races, but the total wagering amount must be over $25 at the end of each event.</li>
  <li>Once you qualify, your cash bonus and blackjack bonus will be automatically deposited into your racing account during the evening of June 11th, 2019. In addition, we will notify via e-mail and SMS your unique and one time promo code for your 100% Reload Bonus.</li>
  <li>The $25 Cash Bonus comes with 5x rollover and the $25 Blackjack Bonus comes with a 20x rollover before any withdraw.</li>
  <li>Your unique and one time promo code for the 100% Reload Bonus comes with a 20x rollover, a deposit of $100 or more must be made to apply it into your account. </li>
  <li>The promo code for the 100% Reload Bonus is valid since the moment we notify you until June 25th, 2019. </li>
</ul>
<hr>