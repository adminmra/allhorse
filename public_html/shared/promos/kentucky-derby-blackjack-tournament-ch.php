<p style="font-weight: 400;">
    <strong>
        <u>Free $50k&nbsp;Kentucky Derby Blackjack Tournament<br /></u>
    </strong>
</p>
<p>
    - This tournament will run&nbsp;from Monday, April, 26th, at 12:00 am ET to Sunday, May, 2nd, at 11:59 pm ET.<br />
    - This is an open free tournament.<br />
    - Players will receive $1,000 in tournament casino chips (TOR).<br />
    - The tournament casino chips have no cash value.<br />
    - Rebuys: &nbsp;$5 unlimited rebuys.<br />
    - The top 3,000 players will be awarded as per their final ranking.<br />
</p>
<table class="table table-condensed table-striped table-bordered">
  <caption>Tournament Pay Table</caption>
    <tbody>
        <tr>
            <th>Rank</th>
            <th>Prizes</th>
        </tr>
        <tr>
            <td>1</td>
            <td>2,000.00</td>
        </tr>
        <tr>
            <td>2</td>
            <td>1,500.00</td>
        </tr>
        <tr>
            <td>3</td>
            <td>1,000.00</td>
        </tr>
        <tr>
            <td>4</td>
            <td>600.00</td>
        </tr>
        <tr>
            <td>5</td>
            <td>500.00</td>
        </tr>
        <tr>
            <td>6</td>
            <td>450.00</td>
        </tr>
        <tr>
            <td>7</td>
            <td>350.00</td>
        </tr>
        <tr>
            <td>8</td>
            <td>300.00</td>
        </tr>
        <tr>
            <td>9</td>
            <td>200.00</td>
        </tr>
        <tr>
            <td>10</td>
            <td>100.00</td>
        </tr>
        <tr>
            <td>11 to 100</td>
            <td>50.00</td>
        </tr>
        <tr>
            <td>101 to 250</td>
            <td>35.00</td>
        </tr>
        <tr>
            <td>251 to 500</td>
            <td>25.00</td>
        </tr>
        <tr>
            <td>501 to 1000</td>
            <td>10.00</td>
        </tr>
        <tr>
            <td>1001 to 1750</td>
            <td>10.00</td>
        </tr>
        <tr>
            <td>1751 to 3000</td>
            <td>5.00</td>
        </tr>
    </tbody>
</table>
<p>
    - The prizes will be credited as casino bonus cash with a 10x rollover.<br />
    - In order to receive a prize, the player must have a remaining tournament balance (TOR).<br />
    - In a tie situation, if players have the same score, the player that posted the score last is higher up on the leaderboard.
</p>