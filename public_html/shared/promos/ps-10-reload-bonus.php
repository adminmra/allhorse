
          
<div class="headline"><h1>Preakness Stakes Reload Deposit Offer</h1></div>



<p>Are you ready for the Preakness Stakes? </p>

<p>
Are you ready for the Preakness Stakes? Make a deposit between Monday, May 15th and Thursday, May 18th and you will receive a 20% bonus. Enter promo code <strong style="color:#1360a0;">PREAKNESS</strong> prior to making your deposit and the 20% bonus will credited instantly. No max cap on the bonus amount issued and you will receive the bonus on each deposit you make that day. Make your deposit and starting betting right away!
</p>


<hr>
<p><strong>Terms and Conditions</strong></p>

<ul>

<li>The Preakness Stakes Reload Deposit Bonus promotion is available to all  BUSR  clients.</li>
<li>The bonus will be added to your account immediately after making each deposit.</li>
<li>You can make multiple deposits on this day and receive a bonus for each deposit.</li>
<li>Only one promo code can be used for each individual deposit.</li>
<li>The Preakness Stakes Reload Deposit Bonus is only valid from 12:00am on Tuesday, May 15th till 11:59pm EST on Friday, May 18th.</li>
<li>Reload bonuses and other cash rewards are subject to the following standard rollover requirements:
  <ul>
    <li> Sports betting activity equal to 30 times the amount of the bonus.</li>
    <li> Casino activity equal to 100 times the amount of the bonus. Craps and Roulette (all variations) and certain casino games, as specified in the Casino Rules, do not apply towards rollover requirements.</li>
    <li> Blackjack activity equal to 200 times the amount of the bonus.    </li>
  </ul>
</li>
<li>You cannot make a deposit, receive the bonus funds and then withdraw your deposit and bet with the bonus funds.</li>
<li>You cannot open multiple accounts to abuse promotions or bonuses.</li>
<li>Management reserves the right to modify or cancel this promotion at any time. General house rules/terms and conditions apply.</li>

</ul>


