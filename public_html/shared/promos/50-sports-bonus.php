 <?php
$restictedURL=$_SERVER['HTTP_HOST'];
$noURL=0;
if($restictedURL == 'www.busr.ag' or $restictedURL == 'dev.busr.ag' or $restictedURL == 'busr.ag'){ $noURL=1; }
?>             
<div class="headline"><h1>50% New Member Sports Bonus</h1></div>



<p>At <?php if($noURL==0){ ?><a href="/signup?ref=50-New-Member-Sports-Text-01">BUSR</a> <?php }else{ echo 'BUSR'; } ?>, you are entitled to exceptional new member bonuses.</p>

<p>For your first deposit with <?php if($noURL==0){ ?> <a href="/signup?ref=50-New-Member-Sports-Text-02">BUSR</a> <?php }else{ echo 'BUSR'; } ?>, you can get an industry leading welcome bonus to add more sports action. Bet on sports with an exclusive bonus up to $1,250 on top of your first deposit with any Cryptocurrency method.</p>
<p>Deposit a minimum of $100 and you could qualify to earn an additional $150!</p>
<hr>
<p><strong>Terms and Conditions</strong></p>
<ul>
<li>The 50% Sign Up Sports Bonus is for new members and it's applied to their initial deposit with any Cryptocurrency method.</li>
<li>You can use one of the three welcome bonus.</li>
<li>You cannot open multiple accounts to abuse promotions or bonuses.</li>
<li>The 50% Sports Bonus is added to your account when selecting the bonus (WELCOME50) at the cashier and deposit using any Cryptocurrency method</li>
<li>If you make multiple deposits on the same day that you make your initial deposit, you will only receive a 50% sports bonus based on your FIRST deposit. But there are multiple reload bonuses for each deposit you make.</li>
<li>You must wager your initial deposit and sign up bonus 5 (five) times before it can be included in a withdrawal request from your account. This is a 5 time rollover of your initial deposit and 50% signup bonus.</li>
<li>You cannot make a deposit, receive your 50% sports bonus and then withdraw your initial deposit and bet with the bonus funds.  That's a no-no.  Play fair and we will, too!</li>
<li>Management reserves the right to modify or cancel this promotion at any time.  General house rules/terms and conditions apply.
</li>

</ul>
<hr>


<p><strong>Special Note from Management</strong></p>
<P>The $150 Signup Cash Bonus is a separate bonus from the 50% Sports Bonus - but guess what?  You are still eligible for both! Just follow the terms of the $150 Signup Bonus to qualify for the cash.</P>

