          
<div class="headline"><h1>10% New Member Bonus</h1></div>



<p>
	At BUSR, you are entitled to exceptional new member bonuses.</p>

<p>For your first deposit with BUSR, you'll get an additional 10% bonus to your deposit absolutely free.  No Limits!  Deposit a minimum of $100 and you could qualify to earn an additional $150!   </p>

<hr>
<p><strong>Terms and Conditions</strong></p>

<ul>

<li>The 10% Sign Up Bonus is for new members and is applied to their initial deposit.</li>
<li>You cannot open multiple accounts to abuse promotions or bonuses.</li>
<li>The 10% bonus is added to your account within 10 minutes of making your deposit.</li>
<li>If you make multiple deposits on the same day that you make your initial deposit, you will only receive a 10% bonus based on your FIRST deposit.</li>
<li>You must wager your initial deposit and sign up bonus 1 (one) time before it can be included in a withdraw from your account. This is a 1 time roll over of your initial deposit and 10% signup bonus. </li>
<li>You cannot make a deposit, receive your 10% bonus and then withdraw your initial deposit and bet with the bonus funds.  That's a no-no.  Play fair and we will, too!</li>
<li>Management reserves the right to modify or cancel this promotion at any time.  General house rules/terms and conditions apply.
</li>

</ul>
<hr>


<p><strong>Special Note from Management</strong></p>
<p>Remember, there are MANY sites that offer $300 or $500 bonuses -  read the fine print.  You'd have better luck solving a Rubik's Cube while blindfolded after drinking 2 Makers Mark whiskeys.</p>

<p>But seriously, this is a great bonus because it is instant and you only need to wager the cash one time.</p>




<P>Oh, the $150 Signup Bonus is a separate bonus from the 10% Cash Bonus - but guess what?  You are still eligible for both! Just follow the terms of the $150 Signup Bonus to qualify for the cash.</P>

