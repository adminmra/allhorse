<div class="headline">
<h1>Super Bowl Reload Deposit Bonus</h1></div>

<p>Get your deposits in early  for the Super Bowl this Sunday, February 4th with the&nbsp;Super Bowl Reload Deposit  Bonus Promotion. </p>
<ul>
  <li>February 1st - Enter code &quot;<strong>SB20</strong>&quot; and you get a 20% reload  bonus.</li>
  <li>February 2nd - Enter  code &quot;<strong>SB15</strong>&quot; and you get a 15% reload bonus. </li>
  <li>February 3rd - Enter code &quot;<strong>SB10</strong>&quot; you get a 10% reload bonus. </li>
</ul>
<p>Once you've placed your bets you can sit back on  Sunday, sip on a ice cold beer and enjoy the game.</p>
<hr>
<p><strong>Terms and Conditions</strong></p>

<ul>

<li>The Super Bowl Reload Deposit Bonus promotion is available to all BUSR clients. </li>
<li>The bonus will be added to your account immediately after making the deposit. </li>
<li>If you make multiple deposits on the same day, you will only receive a a bonus based on your first deposit.</li>
<li>The Super Bowl promo codes are valid during the following times:
	<ul>
	<li style="font-size:14px;">SB20 promo code is valid from 12:00am -  11:59pm EST on Thursday, February 1st.</li>
	<li style="font-size:14px;">SB15 promo code is valid from 12:00am - 11:59pm EST on Friday, February 2nd.</li>
	<li style="font-size:14px;">SB10 promo code is valid from 12:00am - 11:59pm EST on Saturday, February 3rd.</li>
	</ul>
</li>
<li>Reload bonuses and other cash rewards are subject to the following standard rollover requirements:
	<ul>
		<li style="font-size:14px;">Sports betting activity equal to 30 times the amount of the bonus.</li>
		<li style="font-size:14px;">Casino activity equal to 100 times the amount of the bonus. Craps and Roulette (all variations) and certain casino games, as specified in the Casino Rules, do not apply towards rollover requirements.</li>
		<li style="font-size:14px;">Blackjack activity equal to 200 times the amount of the bonus</li>
	</ul>
</li>
<li>You can make a deposit and receive a bonus for each day.</li>
<li>You cannot make a deposit, receive the bonus funds and then withdraw your deposit and bet with the bonus funds.</li>
<li>You cannot open multiple accounts to abuse promotions or bonuses.</li>
<li>Management reserves the right to modify or cancel this promotion at any time. General house rules/terms and conditions apply.</li>
</ul>


