<!-- <div class="tag-box tag-box-v4"> -->
<!-- <h2> Who deserves $10  in Casino Chips?  You do!</h2> -->
<!--<P>Place a wager of $25 or more on the Belmont Stakes and you will get $10 in casino chips. Take advantage of this great offer at BUSR.</P>
<p>Do you want to get $10 in Free Casino Chips? Here is how it works:</p>
<ol>
	<li>Place a wager of $25 or more on the Belmont Stakes.</li>
	<li>After the Belmont Stakes is over, log into your account and go to the Casino.</li>
	<li>You will have $10.00 available to have fun in the Casino!</li>
</ol> -->
<!-- <p><strong>The Free Bet is available in the Club House. Login to your account to place the bet.</strong></p> -->
<!-- <p>You can only win with BUSR!</p>
<hr>
<p><strong>Terms and Conditions of the Free Casino Chip
	</strong>
	<ul>
		<li>The Free Casino Chip is available to all BUSR clients</li>
		<li>The $10.00 Free Casino Chips will be found in the CASINO section under Bonuses.</li>
		<li>The Casino Chips will be credited to your racing account within 48 hours after the race.</li>
		<li>Management reserves the right to modify or cancel this promotion at any time. </li>
		<li>General house rules/terms and conditions apply.</li>
	</ul> -->

<p>Simply place a wager of $25 or more on the Kentucky Oaks or Kentucky Derby from Tuesday Sept 1st through Friday Sept 4th and you'll get $10 to bet the Derby. Take advantage of this great offer at BUSR.</p>	
<p>Do you want to get a $10 Bet for the Kentucky Derby? Here is how it works:</p>
<ol>
	<li>Place a wager of $25 or more on the Kentucky Oaks or Kentucky Derby before Friday Sept 4th.</li>
	<li>Your Free Bet will be credited to your account on the morning of the Kentucky Derby.</li>
	<li>Log into your account and use your free bet to place a straight wager on any horse. </li>
</ol>
<p>You can only win with BUSR!</p>
<hr>
<p><strong>	Terms and Conditions of the Free Bet </strong></p>

<ul>
	<li>The Free Bet offer is available to all BUSR clients.</li>
	<li>To qualify you must wager a total of $25 or more on the Kentucky Oaks or Kentucky Derby between September 1st and September 4th.</li>
	<li>The $10.00 Free Bet will be credited to qualifying accounts on September 5th before the first race.</li>
	<li>Bet not available for use on advanced wagering.</li>
	<li>Management reserves the right to modify or cancel this promotion at any time.</li>
	<li>General house rules/terms and conditions apply.</li>
</ul>