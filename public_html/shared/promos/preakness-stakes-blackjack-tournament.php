<p>Enter the free Preakness Stakes Blackjack Tournament at BUSR with $5,000 in prizes.</p>
<p>Receive a free entry and you start out with $2,500 in chips and it's time to build your bankroll. Don't worry if you bust out. Each re-buy is only $5 so you can try as many times as you want. Prizes will be paid automatically at the end of the tournament. Open the tournament at any time to check your position and keep playing to achieve the top spot.</p>

<div class="table-responsive">
  <table class="table table-condensed table-striped table-bordered" title="Blackjack Tournament"
    summary="Play any Blackjack variation (except live dealer) and get ready to compete for real cash." border="0"
    cellpadding="0" cellspacing="0">
    <caption>Winners Pay table</caption>
    <tbody>
      <tr>
        <th><strong>Position</strong></th>
        <th><strong>Prize</strong></th>
      </tr>
      <tr>
        <td><strong>1</strong></td>
        <td>$1,000.00</td>
      </tr>
      <tr>
        <td><strong>2</strong></td>
        <td>$750.00</td>
      </tr>
      <tr>
        <td><strong>3</strong></td>
        <td>$500.00</td>
      </tr>
      <tr>
        <td><strong>4 - 6</strong></td>
        <td>$250.00</td>
      </tr>
      <tr>
        <td><strong>7 - 13</strong></td>
        <td>$100.00</td>
      </tr>
      <tr>
        <td><strong>14 - 25</strong></td>
        <td>$75.00</td>
      </tr>
      <tr>
        <td><strong>26 - 33</strong></td>
        <td>$50.00</td>
      </tr>
    </tbody>
  </table>
</div>
<?php
 if($_SERVER['HTTP_HOST'] != 'www.usracing.com'){ ?>

<p style="text-align: center;"><a target="_parent" href="https://www.betusracing.ag/login?to=cashier"
    class="AdditionaldepositNow btn-xlrg" target="_self">Deposit Now</a></p>
<?php } ?>
<hr>
<strong>Terms and Conditions</strong>
<ul>
<?php // if($_SERVER['HTTP_HOST'] != 'www.usracing.com'){ ?>
  <li>Promotion is open to all BUSR clients.</li>
  <?php // } ?>
  <li>This is an open free tournament.</li>
  <li>Players will receive $2,500 in tournament casino chips (TOR).</li>
  <li>The tournament casino chips have no cash value.</li>
  <li>Rebuys:  $5 unlimited rebuys are available for all clients.</li>
  <li>To be eligible you must play Tournament Blackjack and select the Preakness Stakes $5,000 Blackjack Tournament.</li>
  <li>Tournament prizes will be credited within 24 hours of the end of the tournament.</li>
  <li>Prizes will be credited as casino bonus cash with a 10x rollover.</li>
  <li>In order to receive a prize, the player must have a remaining tournament balance (TOR).</li>
  <li>In a tie situation, if players have the same score, the player that posted the score last is higher up in the leaderboard.</li>
  <li>We reserve the right to modify or remove any promotion or cash prize bonuses at any time without prior notice.</li>
  <li>See the Casino terms & conditions for more information.</li>
  <li>General house rules/terms and conditions apply.</li>
    <!--

					<h2>$2,000 Breeders' Cup Blackjack Tournament</h2>
<p>We invite you to win cash in our $2,000 Breeders' Cup Blackjack Tournament. Play any Blackjack variation from October 30th 7PM ET time and November 2nd and 11:59P ET time and get ready to compete for some serious cash. Prizes are paid to the top 20 players with the highest payout percentage. Please read the Terms & Conditions below.</p>
 <center><div class="table-responsive">
                  <table class="table table-condensed table-striped table-bordered"  border="0" cellpadding="0" cellspacing="0">
                    <caption>Winners Pay table</caption>



<tr><th>Position</th><th>	Prize</th></tr>
<tr><td>1st	</td><td>$500.00</td></tr>
<tr><td>2nd	</td><td>$300.00</td></tr>
<tr><td>3rd	</td><td>$200.00</td></tr>
<tr><td>4th - 10th</td><td>$100.00</td></tr>
<tr><td>11th - 20th	</td><td>$30.00</td></tr>
                  </table>

</center>

<h3>Terms and Conditions</h3>
<ul><li>Promotion is open to all active members and registration and participation is not required. 
</li><li>To be eligible you must play Blackjack between October 30th 7PM ET time and November 2nd  at 11:59PM ET time.
</li><li>Tournament prizes will be credited within 48 hours after the end of the Tournament.
</li><li>To qualify for prizes, player must play a minimum of 100 Rounds, wagering a minimum of $500 playing for real cash in the FOR REAL mode.
</li><li>Tournament prizes are awarded based on highest payout percentage and will only be available for use in the Casino.
</li><li>Standard 50 time casino rollover requirements will apply.
</li><li>Games which will not count towards wagering rollover requirements are Craps, all variations of Roulette, Baccarat & PaiGow.
</li><li>Only one bonus per account/household or environment where computers are shared.
</li><li>BUSR reserves the right to modify or cancel this promotion at any time.
</li><li>General house rules/terms and conditions apply.</li>
</ul>
-->
  </li>
</ul>