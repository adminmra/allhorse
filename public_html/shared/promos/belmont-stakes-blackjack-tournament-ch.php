<p>
  Find below all the details of the Free $20k Belmont Stakes Blackjack Tournament that we will run from May 31st to June 6th<br/>
  Graphics are in progress, will send socials to Blair and Brian for promotion.<br/>
  Including Jamie and Milena from BUSR to have full knowledge about this.
</p>

<p style="font-weight: 400;">
    <strong>
        <u>Free $20k Belmont Stakes Blackjack Tournament<br></u>
    </strong>
</p>

<p>
- This tournament will run from Monday, May, 31st, at 12:00 am ET to Sunday, June, 6th, at 11:59 pm ET.<br/>
- This is an open free tournament.<br/>
- Players will receive $1,000 in tournament casino chips (TOR).<br/>
- The tournament casino chips have no cash value.<br/>
- Rebuys:  $5 unlimited rebuys.<br/>
- The top 2,000 players will be awarded as per their final ranking.
</p>

	
<table class="table table-condensed table-striped table-bordered">
  <caption>Tournament Pay Table</caption>
    <tbody>
        <tr>
            <th>Rank</th>
            <th>Prizes</th>
        </tr>
        <tr>
            <td>1</td>	
            <td>1000</td>
        </tr>
        <tr>
            <td>2</td>	
            <td>750</td>
        </tr>
        <tr>
            <td>3</td>	
            <td>500</td>
        </tr>
        <tr>
            <td>4</td>	
            <td>400</td>
        </tr>
        <tr>
            <td>5</td>	
            <td>300</td>
        </tr>
        <tr>
            <td>6</td>	
            <td>200</td>
        </tr>
        <tr>
            <td>7</td>	
            <td>100</td>
        </tr>
        <tr>
            <td>8</td>	
            <td>75</td>
        </tr>
        <tr>
            <td>9</td>	
            <td>50</td>
        </tr>
        <tr>
            <td>10</td>	
            <td>40</td>
        </tr>
        <tr>
            <td>11 to 50</td>	
            <td>30</td>
        </tr>
        <tr>
            <td>51 to 150</td>	
            <td>20</td>
        </tr>
        <tr>
            <td>151 to 977</td>	
            <td>10</td>
        </tr>
        <tr>
            <td>978 to 2000</td>	
            <td>5</td>
        </tr>
    </tbody>
</table>

<p>
- The prizes will be credited as casino bonus cash with a 10x rollover.<br/>
- In order to receive a prize, the player must have a remaining tournament balance (TOR).<br/>
- In a tie situation, if players have the same score, the player that posted the score last is higher up on the leaderboard.
</p>
