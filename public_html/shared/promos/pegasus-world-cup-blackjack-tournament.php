<p>BUSR  invites you to win an exclusive Casino Bonus in our Pegasus World Cup Blackjack Tournament. 
Play any Blackjack variation between Saturday, January 25th and Sunday, January 26th and  get ready to compete for big prizes. Casino Bonus prizes are paid to the top 10 players with the highest winning percentage!</p>



	


<div class="table-responsive">
                  <table class="table table-condensed table-striped table-bordered" title="Blackjack Tournament" summary="Play any Blackjack variation (except live dealer) and get ready to compete for real cash." border="0" cellpadding="0" cellspacing="0">
                    <caption>Winners Pay table</caption>
                    <tbody>
                      <tr>
                        <th><strong>Position</strong></th>			<th><strong>Prize </strong></th>
                      </tr>
                      <tr>
                        <td><strong>1st</strong></td>		<td>$1,000.00</td> 	
                      </tr>
                      <tr>
                        <td><strong>2nd</strong></td>		<td> $500.00</td> 	
                      </tr>
                      <tr>
                        <td><strong>3rd - 5th</strong></td>		<td>$250.00</td> 	
                      </tr>
                      <tr>
                        <td><strong>6th - 10th</strong></td>	<td>$100.00</td>			
                      </tr>
                    </tbody>
                  </table>
                </div> 
                <?php
 if($_SERVER['HTTP_HOST'] != 'www.usracing.com'){ ?>

<p style="text-align: center;"><a target="_parent" href="https://engine.betusracing.ag/casino/casino_lobby_game/332" target="_blank"" class="AdditionaldepositNow btn-xlrg"	target="_self">Play Now</a></p>
<?php } ?>
<hr style="margin-top: 96px;">
<strong>Terms and Conditions</strong>
	<ul>
	  <li>Promotion is open to all BUSR clients and registration is not required.</li>
	  <li>To be eligible you must play Blackjack (non live-dealer games) between Saturday, January 25th, 2020 (12:00AM ET)
	    until Sunday, January 26th, 2020 (11:59PM ET)</li>
	  <li>To qualify for prizes, player must play a minimum of 50 Rounds, wagering a minimum of $200 playing for real cash
	    in the FOR REAL mode.</li>
	  <li>Tournament prizes will be credited within 48 hours.</li>
	  <li>Tournament prizes are awarded based on highest payout percentage and will only be available for use in the
	    Casino.</li>
	  <li>40 times casino rollover requirements apply to any winning prize funds.</li>
	  <li>Games which will not count towards wagering rollover requirements are Live Casino Blackjack, Craps, all
	    variations of Roulette, Baccarat & PaiGow.</li>
	  <li>Only one bonus per account/household or environment where computers are shared.</li>
	  <li>BUSR reserves the right to modify or cancel this promotion at any time.</li>
	  <li>General house rules/terms and conditions apply.</li>
	</ul>

