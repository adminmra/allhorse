<div class="headline">
  <h1>Breeders' Cup Free Casino Chips</h1></div>
<!--
<p>Are you betting on the Breeders' Cup Classic? Place $25 or more on any Exacta or Trifecta wagers on the Breeders' Cup Classic race on Saturday, November 3rd and you will receive a free $25 casino chip to play Blackjack. Now is your chance to win twice on the Breeders' Cup Classic!
</p>
-->
<p>
	Are you betting on the Breeders' Cup Classic? Place $25 or more on any Exacta or Trifecta wagers on the Breeders' Cup Classic race on Saturday, November 3rd and you will receive $25 in free casino chips to play Blackjack. Now is your chance to win twice on the Breeders' Cup Classic!
</p>
<hr>
<p><strong>Terms and Conditions</strong></p>
<!--
<ul>
<li>You must place a total of $25 in Exacta or Trifecta wagers on the Breeders' Cup Classic race on Saturday, November 3rd - Race #12 only.</li>
<li>The $25 casino chip will be credited to your racing account by end of day on Monday, Novemeber 5th.</li>
<li>Once the $25 casino chip has been added to your account, we will send you a SMS message.</li>
<li>The $25 casino chip will be available to be used to play Blackjack only.</li>
<li>The $25 casino chip has a 20 time rollover. This means you will have to place a total of $500 in Blackjack wagers to be able to cash out any of the funds.</li>
<li>The max cash out on the casino chip will be $50.</li>
<li>You must redeem the $25 casino chip by Monday, November 12th, 2018.</li>
<li>General house rules/terms and conditions apply.</li>
</ul>
-->
<ul>
<li>You must place a total of $25 in Exacta or Trifecta wagers on the Breeders' Cup Classic race on Saturday, November 3rd - Race #11 only.</li>
<li>The $25 in casino chips will be credited to your racing account by end of day on Monday, November 5th.</li>
<li>Once the free casino chips have been added to your account, we will send you a SMS message.</li>
<li>You can only use the free chips to play Blackjack.</li>
<li>The $25 in casino chips have a 20 time rollover. This means you will have to place a total of $500 in Blackjack wagers to be able to cash out any of the funds.</li>
<li>The max you can cash out will be $50.</li>
<li>You must redeem the free chips by Monday, November 12th, 2018.</li>
<li>General house rules/terms and conditions apply.</li>
</ul>