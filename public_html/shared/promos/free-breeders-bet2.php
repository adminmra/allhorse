
<div class="headline"><h1>Breeders' Cup $10 Free Bet</h1></div>


<!-- <div class="tag-box tag-box-v4"> -->
<p> Where else can you get a free bet for the Breeders' Cup?  Nowhere but BUSR.  </p>

<p>That's right, as a member of BUSR you get a $10 FREE Bet for the Breeders' Cup!  Everybody's got a horse in the race - whether it's the Breeders' Cup or the United States Presidential Election.</p>

<h2>BUSR Introduces the  Presidential Classic!</h2>




<p>Pick the horse you think will win the Breeders' Cup Classic and the candidate who you think will win the Presidential Election. California Chrome and Clinton?  California Chrome and Trump?  Place your $10 risk free bet - all you can do is win at BUSR!</p>


<p><strong>Here's how it works (Terms and Conditions):</strong></p>
<ol>
<li> Open to all members, you need to have $10 available in your account to place the wager.</li>

<li>Place a $10  bet in Sportsbook for the Breeders' Cup $10 Free Bet (aka: The Presidential Classic).</li>
<li>Bet on 1 (and only 1) of 4 possible outcomes. More than one bet, means disqualification from Presidential Classic Free Bet.</li>
<li>Within 24 hours of the official election results, you will find your original $10 back  in your account (win or lose) as well as your $10 in winnings (if you chose wisely!).  </li>
<li>That's it! Good luck!</li>
</ol>

<!-- </div> -->
