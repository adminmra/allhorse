          
<div class="headline">
<h1>10% Bitcoin Bonus</h1></div>



<p>
	At BUSR you can receive even more bonuses when you deposit via Bitcoin.</p>

<p>Deposit with Bitcoin <i>and </i> you'll get an  10% bonus with your deposit absolutely free.No limit and you can do this as many times as you like.</p>

<hr>
<p><strong>Terms and Conditions</strong></p>

<ul>

<li>The 10% Sign Up Bonus is for new members and is applied to their initial deposit.</li>
<li>You cannot open multiple accounts to abuse promotions or bonuses.</li>
<li>The 10% bonus is added to your account within 10 minutes of making your deposit.</li>
<li>If you make multiple deposits on the same day that you make your initial deposit, you will only receive a 10% bonus based on your FIRST deposit.</li>
<li>You must wager your initial deposit and sign up bonus 1 (one) time before it can be included in a withdraw from your account. This is a 1 time roll over of your initial deposit and 10% signup bonus. </li>
<li>You cannot make a deposit, receive your 10% bonus and then withdraw your initial deposit and bet with the bonus funds.  That's a no-no.  Play fair and we will, too!</li>
<li>Management reserves the right to modify or cancel this promotion at any time.  General house rules/terms and conditions apply.
</li>

</ul>
<hr>
