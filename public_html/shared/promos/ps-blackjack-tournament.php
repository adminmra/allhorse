



<p>BUSR invites you to win cash in our Preakness Stakes  Blackjack Tournament. Play any Blackjack variation from Saturday, May 5th (12:00AM EST) till Friday, May 18th (11:59PM EDT) and get ready to compete for real cash. Cash prizes are paid to the top 10 players with the highest winning percentage!</p>

	


<div class="table-responsive">
                  <table class="table table-condensed table-striped table-bordered" title="Blackjack Tournament" summary="Play any Blackjack variation (except live dealer) and get ready to compete for real cash." border="0" cellpadding="0" cellspacing="0">
                    <caption>Winners Pay table</caption>
                    <tbody>
                      <tr>
                        <th><strong>Position</strong></th>			<th><strong>Prize </strong></th>
                      </tr>
                      <tr>
                        <td><strong>1st</strong></td>		<td>$1,000.00</td> 	
                      </tr>
                      <tr>
                        <td><strong>2nd</strong></td>		<td> $500.00</td> 	
                      </tr>
                      <tr>
                        <td><strong>3rd - 5th</strong></td>		<td>$250.00</td> 	
                      </tr>
                      <tr>
                        <td><strong>6th - 10th</strong></td>	<td>$100.00</td>			
                      </tr>
                    </tbody>
                  </table>
                </div> 
<p><a target="_parent" href="https://engine.betusracing.ag/cashier" class="AdditionaldepositNow"	target="_self">Deposit Now</a></p>
<hr>
<strong>Terms and Conditions</strong>
	<ul>
		<li>Promotion is open to all available to all BUSR clients and registration is not required.</li>
		<li>To be eligible you must play Blackjack (non live-dealer games) between Saturday, May 5th (12:00AM EST) till Friday, May 18th (11:59PM EDT).</li>
	<li>Tournament prizes will be credited within 48 hours.</li>
	<li>To qualify for prizes, player must play a minimum of 100 Rounds, wagering a minimum of $500 playing for real cash in the FOR REAL mode.</li>
		<li>Tournament prizes are awarded based on highest payout percentage and will only be available for use in the Casino.</li>
		<li>1 time casino rollover requirements apply to any winning prize funds.</li>
		<li>Games which will not count towards wagering rollover requirements are Live Casino Blackjack, Craps, all variations of Roulette, Baccarat & PaiGow.</li>
		<li>Only one bonus per account/household or environment where computers are shared.</li>
		<li>BUSR reserves the right to modify or cancel this promotion at any time.</li>
		<li>General house rules/terms and conditions apply.<!--

					<h2>$2,000 Breeders' Cup Blackjack Tournament</h2>
<p>We invite you to win cash in our $2,000 Breeders' Cup Blackjack Tournament. Play any Blackjack variation from October 30th 7PM ET time and November 2nd and 11:59P ET time and get ready to compete for some serious cash. Prizes are paid to the top 20 players with the highest payout percentage. Please read the Terms & Conditions below.</p>
 <center><div class="table-responsive">
                  <table class="table table-condensed table-striped table-bordered"  border="0" cellpadding="0" cellspacing="0">
                    <caption>Winners Pay table</caption>



<tr><th>Position</th><th>	Prize</th></tr>
<tr><td>1st	</td><td>$500.00</td></tr>
<tr><td>2nd	</td><td>$300.00</td></tr>
<tr><td>3rd	</td><td>$200.00</td></tr>
<tr><td>4th - 10th</td><td>$100.00</td></tr>
<tr><td>11th - 20th	</td><td>$30.00</td></tr>
                  </table>

</center>

<h3>Terms and Conditions</h3>
<ul><li>Promotion is open to all active members and registration and participation is not required. 
</li><li>To be eligible you must play Blackjack between October 30th 7PM ET time and November 2nd  at 11:59PM ET time.
</li><li>Tournament prizes will be credited within 48 hours after the end of the Tournament.
</li><li>To qualify for prizes, player must play a minimum of 100 Rounds, wagering a minimum of $500 playing for real cash in the FOR REAL mode.
</li><li>Tournament prizes are awarded based on highest payout percentage and will only be available for use in the Casino.
</li><li>Standard 50 time casino rollover requirements will apply.
</li><li>Games which will not count towards wagering rollover requirements are Craps, all variations of Roulette, Baccarat & PaiGow.
</li><li>Only one bonus per account/household or environment where computers are shared.
</li><li>BUSR reserves the right to modify or cancel this promotion at any time.
</li><li>General house rules/terms and conditions apply.</li>
</ul>
--></li>
	</ul>
