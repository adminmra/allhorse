<?php
$restictedURL=$_SERVER['HTTP_HOST'];
$noURL=0;
if($restictedURL == 'www.busr.ag' or $restictedURL == 'dev.busr.ag' or $restictedURL == 'busr.ag'){ $noURL=1; }
?>           
<div class="headline"><h1>50% Casino Cash Back Special</h1></div>



<p>Every Thursday at the Casino is special at <?php if($noURL==0){ ?> <a href="/signup?ref=50-Cash-Back-Text-01">BUSR</a> <?php }else{ echo 'BUSR'; } ?> because you get 50% Cash returned to your account for any losses*!  That's right, you will get 50% Cash Back to your account for any losses on Thursday in the Casino.  Play Thursday and get Casino Cash back the next day!</p>

<hr>
<p><strong>Terms and Conditions</strong></p>

<ul>

<li>50% Casino Cash Back is for LOSSES in the CASINO, only.</li>
<li>Live Dealer Casino action is NOT counted for 50% Casino Cash Back promotion.</li>
<li>Casino losses are counted for THURSDAY between the hours of 00:00AM  to 23:59PM Eastern Time.</li>
<li>Maximum of $100 is returned to member's account.</li>
<li>Example: If you lost a total of $150 in the casino on Thursday, you will get $75 (i.e., 50% of $150) added to your account the following day.</li>
<li>Member's account will be credited on FRIDAY by 11AM Eastern Time.  If you do not see your credit by this time, please email or call us.</li>
<li>There is a 10x rollover required on Casino Cash Back before withdraw.  That is, you must wager your cash back bonus funds ten times within the casino. </li>
<li>Games which will not count towards wagering rollover requirements are Craps, all variations of Roulette, Baccarat & Pai Gow.</li>

<li>Management reserves the right to modify or cancel this promotion at any time.  General house rules/terms and conditions apply.</li>
</ul>