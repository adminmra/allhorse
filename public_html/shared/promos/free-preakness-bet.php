<h2>Who deserves $10 Free Bet? You do!</h2>

<p>Simply place a wager of $25 or more on the Preakness Stakes between Monday Sept 28th through Friday Oct 2nd and
	you'll get $10 to bet the Preakness Stakes. Take advantage of this great offer at BUSR.</p>

<p>Do you want to get a $10 Bet for the Preakness? Here is how it works:</p>
<ul class="num-list">
	<li>Place a wager of $25 or more on the Preakness Stakes before Friday Oct 2nd.</li>
	<li>Your Free $10 Bet will be credited to your balance on the morning of Oct 3rd.</li>
	<li>Log into your account and use your free bet to place a bet on the race.</li>
</ul>
<p>You can only win with BUSR!</p>
<p><strong>Terms and Conditions of the Free Bet</strong></p>
<ul>
	<li>The Free Bet offer is available to all BUSR clients.</li>
	<li>To qualify you must wager a total of $25 or more on the Preakness Stakes between September 28th and October 2nd.
	</li>
	<li>The Free $10.00 Bet will be credited to qualifying accounts on October 3rd before the first race.</li>
	<li>Bet only available for use on advance wagering.</li>
	<li>Management reserves the right to modify or cancel this promotion at any time.</li>
	<li>General house rules/terms and conditions apply.
	</li>
</ul>

<style>
	.num-list > li {
		list-style-type: decimal !important;
	}
</style>