<p style="font-weight: 400;">
    <strong>
        <u>Free $20,000 Crazy Payouts - Preakness Stakes<br></u>
    </strong>
</p>
<ul>
  <li>This tournament will run from Monday, May, 10th, at 12:00 am ET to Sunday, May, 16th, at 11:59 pm ET.</li>
  <li>The top 1,300 players will be awarded as per their final ranking according to the following pay-table</li>
</ul>
<br/>

<div class="table-responsive">
  <table class="table table-condensed table-striped table-bordered" title="Blackjack Tournament" summary="" border="0" cellpadding="0" cellspacing="0">
    <caption>
        <strong>Tournament Pay Table</strong>
    </caption>
    <tbody>
        <tr>
            <th width="50%">Rank</th>
            <th>Prizes</th>
        </tr>
        <tr>
            <td>1</td>
            <td>$1,500</td>
        </tr>
        <tr>
            <td>2</td>
            <td>$900</td>
        </tr>
        <tr>
            <td>3</td>
            <td>$740</td>
        </tr>
        <tr>
            <td>4 to 99</td>
            <td>$25</td>
        </tr>
        <tr>
            <td>100</td>
            <td>$25</td>
        </tr>
        <tr>
            <td>101 to 399</td>
            <td>$10</td>
        </tr>
        <tr>
            <td>400</td>
            <td>$500</td>
        </tr>
        <tr>
            <td>401 to 699</td>
            <td>$10</td>
        </tr>
        <tr>
            <td>700</td>
            <td>$500</td>
        </tr>
        <tr>
            <td>701 to 999</td>
            <td>$10</td>
        </tr>
        <tr>
            <td>1000</td>
            <td>$500</td>
        </tr>
        <tr>
            <td>1001 to 1299</td>
            <td>$10</td>
        </tr>
        <tr>
            <td>$10</td>
            <td>$500</td>
        </tr>
    </tbody>
</table>

</div>
<br><br><br>

<div><a target="_self" class="AdditionaldepositNow"
    href="http://engine.busr.ag/casino/launch/logged/919?name=Blackjack+Tournament">Play Now</a></div>
<br><br><br><br><br><br><br>

<?php /*
 if($_SERVER['HTTP_HOST'] != 'www.usracing.com'){ ?>

<?php
$bjt = file_get_contents('https://www.betusracing.ag/tournament/KDBlackjackTournament');
echo $bjt;
?>

<p style="text-align: center;"><a target="_parent" href="https://engine.betusracing.ag/cashier" class="card_btn-red"
    target="_self">Deposit Now</a></p>
<?php } */ ?>
<hr>
<p><strong>The terms and conditions are the following:</strong></p>
<ul> 
  <li>This is an open free tournament.</li>
  <li>Players will receive $1,000 in tournament casino chips (TOR).</li>
  <li>The tournament casino chips have no cash value.</li>
  <li>Re-buys: $5 unlimited re-buys.</li>
  <li>The prizes will be credited as casino bonus cash with a 10x rollover.</li>
  <li>In order to receive a prize, the player must have a remaining tournament balance (TOR).</li>
  <li>In a tie situation, if players have the same score, the player that posted the score last is higher up on the leader board.</li>
</ul>
<hr />