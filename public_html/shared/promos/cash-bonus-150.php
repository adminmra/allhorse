 <?php
$restictedURL=$_SERVER['HTTP_HOST'];
$noURL=0;
if($restictedURL == 'www.busr.ag' or $restictedURL == 'dev.busr.ag' or $restictedURL == 'busr.ag'){ $noURL=1; }
?> 
<div class="headline"><h1>$150 Cash Bonus!</h1></div>



<p>Did you know <?php if($noURL==0){ ?>  <a href="/signup?ref=150-Cash-Bonus-Text-01" rel="nofollow" >BUSR</a> <?php }else{ echo 'BUSR'; } ?>offers a $150 CASH BONUS for your new online betting account? Just deposit and play to earn $150 in CASH!</p>

<p>We are sure you will like our easy to use interface and hassle-free horse betting experience, so it's our pleasure to give you a cash bonus just to try <?php if($noURL==0){ ?> <a href="/signup?ref=150-Cash-Bonus-Text-02" rel="nofollow" >BUSR</a><?php }else{ echo 'BUSR'; } ?> !</p>


<!-- <h2>Get your $150 Cash Bonus today!</h2> -->
<hr>
<p><strong>Here's how our $150 Bonus Works:</strong> &nbsp;</p>
<ul>
<li>This promotion is for new players only</li>
<li>To qualify, you must deposit a minimum of $100 and wager at least $500 ("cumulative wager") within 30 days of registering your account.</li>
<li>Once you qualify, your cash bonus will be automatically deposited into your racing account.</li>
<li>TVG, TwinSpires, Xpressbet and BetAmerica players are eligible for this promotion!</li>
<li>Wagers must be made online.</li>
<li>Wagers must be made at the BUSR website.</li>
<li>Wagers must be made in the racebook only.</li>
<li>This promotion may be suspended by the Management without notice.</li>
<li>Bonus cash must be wagered once before it can be withdrawn from player's account. In other words, after you receive your $150 Cash Bonus, you must make wagers of $150 ("one time roll over") in the racebook.  Afterwards, you may withdraw those funds from your racing account.</li>

<li>If you have any questions about this promotion, please contact Customer Support.</li>
<li>This promotion is void where prohibited. All terms and conditions will apply for qualification.</li></ul>

