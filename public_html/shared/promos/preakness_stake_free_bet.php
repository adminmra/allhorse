<div class="headline">
	<h1>Preakness Stakes Free Bet</h1>
</div>
<p>Place  a wager of $25 or more on the Preakness Stakes and you will get a FREE BET for  the Belmont Stakes! The Preakness Stakes experience is right now at BUSR!</p>
<p>Do  you want a Belmont Stakes Free Bet? This is how it works:</p>
<ol>
  <li>Place a wager of $25 or more on the Preakness Stakes.</li>
  <li>After the Preakness  Stakes is over, a &ldquo;Belmont Stakes - Free Bet&rdquo; will be available in the Sports  section. Log into your account to place the bet. </li>
  <li>You need at least $10.00 available in your account to place  the wager. Have fun!</li>
</ol>
<p>If  you win your bet, enjoy your money! If you lose the wager, your $10.00 will go  back to your account within 24 hours.</p>
<hr>
<p><strong>Terms and Conditions</strong></p>

<ul>

  <li>The Free Bet is available to all BUSR clients.</li>
  <li>The $10.00 Free Bet is found in the Sportsbook section of  the website (chose Sports or Sports Betting from the Club House).</li>
  <li>The free bet is called &quot;Belmont Stakes - Free Bet&quot;</li>
  <li>You need to have $10.00 in your account in order to  participate in the $10.00 Free Bet.</li>
  <li>If you lose the bet, your $10.00 will be credit back to your racing account within 24 hours after the race.</li>
  <li>Management reserves the right to modify or cancel this  promotion at any time. General house rules/terms and conditions apply.</li>

</ul>
<hr>