          
<div class="headline"><h1>Get up to $500 Sign up Cash Bonus</h1></div>

<p>How do you get your $500 bonus?</p>
<p>For your first deposit with <a href="/signup?ref=500-Cash-Bonus">BUSR</a>, you'll get an additional 20% bonus to your deposit absolutely free. So, if you deposit $2500, you'll get $500 free! </p>

<p>At <a href="/signup?ref=500-Cash-Bonus">BUSR</a>, you are entitled to exceptional new member bonuses.</p>

<p>If you wish to deposit more, please contact contact <a href="https://busr.ag/horse-betting-support" rel="nofollow">Customer Service</a> and ask about your VIP Program options.  You will be guided into a program tailored for just for you.</p>

<hr>
<p><strong>Terms and Conditions</strong></p>

<ul>

<li>The 20% Sign Up Bonus is for new members and is applied to their initial deposit.</li>
<li>You cannot open multiple accounts to abuse promotions or bonuses.</li>
<li>The 20% bonus is added to your account within 10 minutes of making your deposit.</li>
<li>If you make multiple deposits on the same day that you make your initial deposit, you will only receive a 20% bonus based on your FIRST deposit.</li>
<li>You must wager your initial deposit and sign up bonus 5 (five) times before it can be included in a withdraw from your account. This is a 5 time rollover of your initial deposit and 20% signup bonus. </li>
<li>You cannot make a deposit, receive your 20% bonus and then withdraw your initial deposit and bet with the bonus funds.  <!-- That's a no-no.  Play fair and we will, too! --></li>
<li>Management reserves the right to modify or cancel this promotion at any time.  General house rules/terms and conditions apply.
</li>

</ul>
<p><strong>Special Note from Management</strong></p>
<P>The $150 Signup Bonus is a separate bonus from the 20% Cash Bonus - but guess what?  You are still eligible for both! Just follow the terms of the $150 Signup Bonus to qualify for the cash.</P>

