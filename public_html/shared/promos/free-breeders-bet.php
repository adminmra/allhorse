<img class="img-responsive" src="https://www.usracing.com/img/belmontstakes/belmont-stakes-free-bet.jpg" alt="Belmont Stakes Free Bet">
<div class="headline"><h1>Breeders' Cup $10 Free Bet</h1></div>

<!-- <div class="tag-box tag-box-v4"> -->
<p> Where else can you get a free bet for the Breeders' Cup?  Nowhere but BUSR.  </p>

<p>That's right, as a member of BUSR you get a $10 FREE Bet for the Breeders' Cup!</p>

<h2>Introducing the BUSR's Presidential Classic!</h2>


<p>Everybody’s got a horse in the race— whether it’s the Breeders’ Cup or the United States Presidential Election.</p>

<p>Pick the horse you think will win the Breeders’ Cup Classic and the candidate who you think will win the Presidential Election.</p>

<p>Chrome and Clinton?  Chrome and Trump?  </p>

<p>Place your $10 risk free bet— all you can do is win at BUSR!</p>
<p>Here's how it works:</p>
<ol>
<li>You need to have $10 available in your account to place the wager.</li>

<li>The Free Bet is the 2016 Belmont Stakes Winning Time.</li>
<li>Place your bet on whether you think the winning time will be OVER 2:29.00 or UNDER 2:29.00.</li>
<li><strong>The Free Bet is available to all members</strong></li>
<li>That's it! Good luck!</li>
</ol>

<p><strong>The Free Bet is available in the Club House. Login to your account to place the bet.</strong></p>


<p>If you win your bet, great! </p>

<p>If you lose the wager, your $10 will be returned to your account within 24 hours.</p>

<p>You can only win with US Racing!</p>

<hr>
<p><strong>Terms and Conditions of the Free Bet

</strong><br><I>Please pay careful attention: </I>
  <ul>
  <li>The $10 Free bet is found in the Sportsbook (chose Sports or Sports Betting from the Clubhouse), it is NOT for any bets placed in the Racebook.</li>
  <li>The free bet is is for the WINNING TIME of the Belmont Stakes not whether Donald Trump will be the next President-- but that bet is available! </li>
  <li>The free bet is called "2016 <!-- Kentucky Derby --> Belmont Stakes - $10 Free Bet"</li>
  <li>You need to have $10 in your account in order to participate in the $10 Free Bet.  That means we don't lend you $10 bucks.  Come on. It's not called "free money" it's a free bet! For those of you who are on your 3rd scotch, let's repeat: you need $10 to place the free bet.  If you lose the bet, your $10 is returned to your account.  If you win the bet, awesome-- drink another scotch.</li>
  </ul></p><br>
<!-- </div> -->