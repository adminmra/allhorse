<p><img class="img-responsive" src="https://www.usracing.com/img/promos/super-saturday-free-chips.jpg" alt="Super Saturday Free Chips"></p>
          
<div class="headline"><h1>Super Saturday WIN Bet Gets You Free Casino Chips</h1></div>



<p>
	Are you ready for Super Saturday? </p>

<p>Place a WIN bet of $25.00 or more on any of the three Super Saturday Prep races on Saturday, April 8th and if your horse wins you will get your wager amount up to $50.00 issued in casino chips. The three races available are: </p>
<strong><p>Wood Memorial at Aqueduct - <span style="color:#c71f24;">Race #: 10</span><br>
	Blue Grass at Keeneland - <span style="color:#c71f24;">Race #: 10</span><br> 
	Santa Anita Derby at Santa Anita Park - <span style="color:#c71f24;">Race #: 8</span>
	</p></strong>
	
<hr>

<p><strong>Terms and Conditions</strong></p>

<ul>

<li>The Super Saturday Win Bet promotion is available to all BUSR clients.</li>
<li>he casino cash bonus will be credited to your account if you place a WIN bet on any of the following tracks/races on April 8th and your horse wins. <br><br>
	<ol>
		<li style="font-size:14px;"> Wood Memorial at Aqueduct - Race #: 10</li>
		<li style="font-size:14px;"> Blue Grass at Keeneland - Race #: 10</li>
		<li style="font-size:14px;"> Santa Anita Derby at Santa Anita Park - Race #: 8</li>
	</ol>

</li><br>
	
<li>The casino cash bonus will be added to your account within 48 hours of the end of the race with a maximum credit amount of $50.00 issued.</li>
<li>The casino cash bonus is only available to be redeemed in the casino and is subject to the following standard rollover requirements:<br><br>
<ol>
		<li style="font-size:14px;"> Casino activity equal to 100 times the amount of the bonus. Craps and Roulette (all variations) and certain casino games, as specified in the Casino Rules, do not apply towards rollover requirements.</li>
		<li style="font-size:14px;"> Blackjack activity equal to 200 times the amount of the bonus.</li>
	</ol><br>
	</li>
<li>You cannot open multiple accounts to abuse promotions or bonuses.</li>
<li>Management reserves the right to modify or cancel this promotion at any time. General house rules/terms and conditions apply.</li>

</ul><br><br>


<!--<p><strong>Special Note from Management</strong></p>
<p>Remember, there are MANY sites that offer $300 or $500 bonuses -  read the fine print.  You'd have better luck solving a Rubik's Cube while blindfolded after drinking 2 Makers Mark whiskeys.</p>

<p>But seriously, this is a great bonus because it is instant and you only need to wager the cash one time.</p>











<P>Oh, the $150 Signup Bonus is a separate bonus from the 10% Cash Bonus - but guess what?  You are still eligible for both! Just follow the terms of the $150 Signup Bonus to qualify for the cash.</P>-->

