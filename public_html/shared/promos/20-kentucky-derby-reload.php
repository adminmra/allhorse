 <?php
$restictedURL=$_SERVER['HTTP_HOST'];
$noURL=0;
if($restictedURL == 'engine.busr.ag' ){ $noURL=1; }
$noURL=1;
?>         
<div class="headline"><h1>KD2021: 20% Kentucky Derby Reload Cash Bonus</h1></div>

<p>How do you get your $500 bonus?</p>
<p>You'll get an additional 20% bonus to your deposit absolutely free. So, if you deposit $2500, you'll get $500 free!</p>
<p>At <?php if($noURL==0){ ?> <a href="/signup?ref=500-Cash-Bonus">BUSR</a> <?php }else{ echo 'BUSR'; } ?>, you are entitled to exceptional new member bonuses.</p>
<hr>
<p><strong>Terms and Conditions</strong></p>

<ul>
  <li>The 20% KD Reload Bonus is redeemable in the Cashier upon making your deposit by using Promocode <strong>KD2021</strong>. </li>
  <li>You cannot open multiple accounts to abuse promotions or bonuses. </li>
  <li>The 20% bonus is added to your account within 10 minutes of making your deposit. </li>
  <li>If you make multiple deposits on the same day that you make your initial deposit, you can receive a 20% bonus on every deposit. </li>
  <li>You must wager your initial deposit and reload bonus 5 (five) times before it can be included in a withdraw from your account. This is a 5- time rollover of your initial deposit and 20% reload bonus. </li>
  <li>You cannot make a deposit, receive your 20% bonus and then withdraw your initial deposit and bet with the bonus funds. </li>
  <li>Management reserves the right to modify or cancel this promotion at any time. General house rules/terms and conditions apply. </li>
</ul>
<?php /* <p><strong>Special Note from Management</strong></p>
<P>The $150 Signup Bonus is a separate bonus from the 20% Cash Bonus - but guess what?  You are still eligible for both! Just follow the terms of the $150 Signup Bonus to qualify for the cash.</P>
*/ ?>

