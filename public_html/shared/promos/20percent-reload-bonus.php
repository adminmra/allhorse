<div class="headline">
	<h1>20% Reload Bonus for the Breeders' Cup</h1>
</div>



<p>
	BUSR is your partner for this years Breeders’ Cup!</p>

<p>Make any reload deposit of $100.00 or more on Friday, November 2nd or Saturday, November 3rd and you'll receive a <b>20% cash bonus</b>. Enter promo code <strong> <font color="#1360a0">BREEDERSCUP</font></strong> prior to making your reload deposit and the 20% bonus will be credited instantly. Make your deposit  then place your wager on the Breeders' Cup.</p>

<hr>

<p><strong>Terms and Conditions</strong>
</p>

<ul>

<li>The  20% Breeders' Cup Reload Bonus is available to all BUSR clients.</li>
<li>The promo code <strong> <font color="#1360a0">BREEDERSCUP</font></strong> must be entered prior to the deposit to receive the cash bonus.</li>
<li>Minimum deposit amount to receive the cash bonus is $100.00.</li>
<li>You can make multiple deposits and receive a bonus for each  as long as you use the promo code.</li>
<li>The 20% Breeders' Cup Reload Bonus is valid from 12:00am EST on Friday, November 2nd until 11:59pm ET on Saturday, November 3rd.</li>
<li>No maximum bonus amount. The only limit will be based on the deposit method used.</li>
<li>You must wager the reload deposit and bonus over three times before it can be included in a withdraw from your account.</li>
<li>General house rules/terms and conditions apply.</li>

</ul>