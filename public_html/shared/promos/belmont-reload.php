          
<div class="headline"><h1>Belmont Reload Deposit Bonus Offer	</h1></div>


<div>
<p>Are you ready for the Belmont Stakes? </p>

<p>Make a reload deposit between Tuesday, June 5th and Friday, June 8th and you will receive a 20% bonus. Enter promo code <strong style="color:#1360A0;">BELMONT</strong> prior to making your reload deposit and the 20% bonus will credited instantly. No max cap on the bonus amount issued and you will receive the bonus on each reload deposit you make during these days. Make your deposit and so you can place your bet on the Belmont right away!</p>
<br>
<p><a target="_parent" href="https://engine.betusracing.ag/cashier" class="AdditionaldepositNow"	target="_self">Deposit Now</a></p>

<br><br><br>
<hr>
<p><strong>Terms and Conditions</strong></p>

<ul>

<li>The Belmont Reload Deposit Bonus promotion is available to all BUSR clients.</li>
<li>The bonus will be added to your account immediately after making each deposit.</li>
<li>You can make multiple deposits  and receive a bonus for each deposit as long as you use the promo code.</li>
<li>Only one promo code can be used for each individual deposit.</li>
<li>The Belmont Reload Deposit Bonus is only valid from 12:00am EDT on Tuesday, June 5th until 11:59pm EDT on Friday, June 8th.</li>
<li>Reload bonuses and other cash rewards are subject to the following standard rollover requirements:
  <ul>
    <li>Sports/Racebook: 3 times the deposit + bonus amount.</li>
    <li>Casino: 10 times the deposit + bonus amount.  Craps and Roulette (all variations) and certain casino games, as specified in the Casino Rules, do not apply towards rollover requirements.</li>
    <li> Blackjack:  20 times the deposit + bonus amount</li>
  </ul>
</li>
<li>You cannot open multiple accounts to abuse promotions or bonuses.</li>
<li>Management reserves the right to modify or cancel this promotion at any time. General house rules/terms and conditions apply.</li>

</ul>

</div>
