
<div id="A-D">
<dl class="justify-left">
	<dt>Abandoned</dt>
	<dd>A race meeting which has been cancelled because a club did not receive sufficient nominations to be able to stage it, or because of bad weather which made racing on the track unsafe. All bets placed on abandoned races are fully refunded.</dd>
	<dt>Acceptor</dt> <dd>A runner officially listed to start in a race.
 </dd>



	<dt>Accumulator</dt>
	<dd>(Also, Parlay) A multiple bet. A kind of 'let-it-ride' bet. Making simultaneous selections on two or more races with the intent of pressing the winnings of the first win on the bet of the following race selected, and so on. All the selections made must win for you to win the accumulator.</dd>


	<dt>Across The Board</dt>
	<dd>(See 'Place') A bet on a horse to win, place or show. Three wagers combined in one. If the horse wins, the player wins all three wagers, if second, two, and if third, one.</dd>
	<dt>Age</dt>
	<dd>All thoroughbreds count January 1 as their birth date.</dd>
	<dt>Ajax</dt>
	<dd>UK slang term for 'Betting Tax'.</dd>
	<dt>All-age Race</dt>
	<dd>A race for two-year-olds and up.</dd>
	<dt>All Out</dt>
	<dd>A horse who is trying to the best of his ability.</dd>
	<dt>Allowances</dt>
	<dd>Reductions in weights to be carried allowed because of certain conditions such as; an apprentice jockey is on a horse, a female horse racing against males, or three-year-olds racing against older horses.</dd>
	<dt>All Weather Racing</dt>
	<dd>Racing that takes place on an artificial surface.</dd>
	<dt>Also Ran</dt>
	<dd>Any selection not finishing 1st, 2nd, 3rd or 4th in a race or event.</dd>
	<dt>Ante Post</dt>
	<dd>(Also, Futures) Bets placed in advance predicting the outcome of a future event. Ante-post prices are those on major sporting events, usually prior to the day of the event itself. In return for the chance of better odds, punters risk the fact that stakes are not returned if their selection pulls out or is cancelled.</dd>
	<dt>Apprentice</dt>
	<dd>A trainee jockey. An apprentice will usually ride only flat races.</dd>
	<dt>Approximates</dt>
	<dd>The approximate price a horse is quoted at before a race begins. Bookmakers use these approximates as a guide to set their boards.</dd>
	<dt>Arbitrage</dt>
	<dd>Where a variation in odds available allows a punter to back both sides and guarantee a win.</dd>
	<dt>ART</dt>
	<dd>Artificial Turf.</dd>
	<dt>ATS</dt>
	<dd>Against The Spread.</dd>
	<dt>AWT</dt>
	<dd>All weather track.</dd>
	<dt>Baby Race</dt>
	<dd>A race for two-year-olds.</dd>
	<dt>Back</dt>
	<dd>To bet or wager.</dd>
	<dt>Backed</dt>
	<dd>A 'backed' horse is one on which lots of bets have been placed.</dd>
	<dt>Backed-In</dt>
	<dd>A horse which is backed-in means that bettors have outlaid a lot of money on that horse, with the result being a decrease in the odds offered.</dd>
	<dt>Back Marker</dt>
	<dd>In a standing start event, which is handicapped, the horse who is given the biggest handicap is known as the backmarker.</dd>
	<dt>Backstretch</dt>
	<dd>The straight way on the far side of the track.</dd>
	<dt>Back Straight</dt>
	<dd>The straight length of the track farthest away from the spectators and the winning post.</dd>
	<dt>Backward</dt>
	<dd>A horse that is either too young or not fully fit.</dd>
	<dt>Banker</dt>
	<dd>(Also, Key) Highly expected to win. The strongest in a multiple selection in a parlay or accumulator. In permutation bets the banker is a selection that must win to guarantee any returns.</dd>
	<dt>Bar Price</dt>
	<dd>Refers to the odds of those runners in a race not quoted with a price during early betting shows. The bar price is the minimum odds for any of those selections not quoted.</dd>
	<dt>Barrier</dt>
	<dd>(Also, Tape) A starting device used in steeple chasing consisting of an elastic band stretched across the racetrack which retracts when released.</dd>
	<dt>Barrier Draw</dt>
	<dd>The ballot held by the race club to decide which starting stall each runner will occupy.</dd>
	<dt>Bat</dt>
	<dd>(Also, Stick) A jockey's whip.</dd>
	<dt>Beard</dt>
	<dd> (US) - A friend or acquaintance or other contact who is used to placing bets so that the bookmakers will not know the identity of the actual bettor. Many top handicappers and persons occupying sensitive positions use this method of wagering.</dd>
	<dt>Bearing In</dt>
	<dd> (Out) - Failing to maintain a straight course, veering to the left or right. Can be caused by injury, fatigue, outside distraction, or poor riding.</dd>
	<dt>Beeswax</dt>
	<dd>UK slang term for betting tax. Also known as 'Bees' or 'Ajax'.</dd>
	<dt>Bell Lap</dt>
	<dd>In harness racing, the last lap of a race, signified by the ringing of the bell.</dd>
	<dt>Bet</dt>
	<dd>A transaction in which monies are deposited or guaranteed.</dd>
	<dt>Betting Board</dt>
	<dd>A board used by the bookmaker to display the odds of the horses engaged in a race.</dd>
	<dt>Betting Ring</dt>
	<dd>The main area at a racecourse where the bookmakers operate.</dd>
	<dt>Betting Tax</dt>
	<dd>Tax on a Bookmaker's turnover. In the UK this is a 'Duty' levied on every Pound wagered. Common methods of recouping this by the punter are to deduct tax from returns (winnings) or to pay tax with the stake/wager. In the latter case, no tax is deducted from the punter's winnings.</dd>
	<dt>Bettor</dt>
	<dd> (US) - Someone who places or has a bet. A 'Punter' in the UK.</dd>
	<dt>Beyer Number</dt>
	<dd>A handicapping tool, popularized by author Andrew Beyer, assigning a numerical value to each race run by a horse based on final time and track condition. This enables different horses running at different racetracks to be objectively compared.</dd>
	<dt>Bismarck</dt>
	<dd>A favourite which the bookmakers do not expect to win.</dd>
	<dt>Blanket Finish</dt>
	<dd>When the horses finish so close to the winning line you could theoretically put a single blanket across them.</dd>
	<dt>Blind Bet</dt>
	<dd>A bet made by a racetrack bookmaker on another horse to divert other bookmakers' attention away from his sizeable betting on his/her main horse thus to avoid a shortening of the odds on the main horse.</dd>
	<dt>Blinkers</dt>
	<dd>A cup-shaped device applied over the sides of the horse's head near his eyes to limit his vision. This helps to prevent him from swerving away from distracting objects or other horses on either side of him. Blinker cups come in a variety of sizes and shapes to allow as little or as much vision as the trainer feels is appropriate.</dd>
	<dt>Board</dt>
	<dd>Short for 'Tote Board' on which odds, betting pools and other race information are displayed.</dd>
	<dt>Bomb</dt>
	<dd>(er) - A winning horse sent off at very high odds.</dd>
	<dt>Book</dt>
	<dd>A bookmaker's tally of amounts bet on each competitor, and odds necessary to assure him of profit.</dd>
	<dt>Bookie</dt>
	<dd>(U.K.) Short for bookmaker. The person or shop who accepts bets.</dd>
	<dt>Bookmaker</dt>
	<dd>Person who is licensed to accept bets on the result of an event based on their provision of odds to the customer. (Sportsbook US).</dd>
	<dt>Bottle</dt>
	<dd>UK slang, odds of 2 to 1.</dd>
	<dt>Box</dt>
	<dd>A wagering term denoting a combination bet whereby all possible numeric combinations are covered.</dd>
	<dt>Boxed</dt>
	<dd> (in) - To be trapped between other horses.</dd>
	<dt>Bobble</dt>
	<dd>A bad step away from the starting gate, sometimes caused by the ground breaking away from under a horse and causing him to duck his head or go to his knees.</dd>
	<dt>Bolt</dt>
	<dd>Sudden veering from a straight course.</dd>
	<dt>Book</dt>
	<dd>A collection of all the bets taken on fixed odds betting events.</dd>
	<dt>Bookmaker</dt>
	<dd> (Bookie) - A person registered and licensed to bet with the public.</dd>
	<dt>Breakage</dt>
	<dd>Those pennies that are left over in pari-mutuel payoffs which are rounded out to a nickel or dime.</dd>
	<dt>Breeders' Cup</dt>
	<dd>Thoroughbred racing's year-end championship. Known as Breeders' Cup Day, it consists of eight races conducted on one day at a different racetrack each year with purses and awards totalling $13 million. First run in 1984.</dd>
	<dt>Bridge-Jumper</dt>
	<dd> (US) - Bettor who specializes in large show bets on odd-on favourites.</dd>
	<dt>Buck</dt>
	<dd> (US) - A bet of US$ 100 (also known as a 'dollar bet').</dd>
	<dt>Bug Boy</dt>
	<dd>An apprentice rider.</dd>
	<dt>Bull Ring</dt>
	<dd>Small racetrack less than one mile around.</dd>
	<dt>Burkington Bertie</dt>
	<dd>100/30.</dd>
	<dt>Buy Price</dt>
	<dd>In Spread or Index betting, the higher figure quoted by an Index bookmaker.</dd>
	<dt>Buy the Rack</dt>
	<dd> (US) - Purchase every possible daily-double or other combination ticket.</dd>
	<dt>Canadian</dt>
	<dd>Also known as a Super Yankee. A Canadian is a combination bet consisting of 26 bets with 5 selections in different events. The combination bet is made up of 10 doubles, 10 trebles, five 4-folds and one 5-fold.</dd>
	<dt>Card</dt>
	<dd>Another term for fixture or race meeting.</dd>
	<dt>Carpet</dt>
	<dd>UK slang for Odds of 3 to 1 (also known as 'Tres' or 'Gimmel').</dd>
	<dt>Caulk</dt>
	<dd>Projection on the bottom of a shoe to give the horse better traction, especially on a wet track.</dd>
	<dt>Century</dt>
	<dd>GBP£ 100 (also known as a 'Ton').</dd>
	<dt>Chalk</dt>
	<dd>Wagering favorite in a race. Dates from the days when on-track bookmakers would write current odds on a chalkboard.</dd>
	<dt>Chalk Player</dt>
	<dd>Bettor who wagers on favorites.</dd>
	<dt>Chase</dt>
	<dd>See 'Steeplechase'.</dd>
	<dt>Checked</dt>
	<dd>A horse pulled up by his jockey for an instant because he is cut off or in tight quarters.</dd>
	<dt>Chute</dt>
	<dd>Extension of the backstretch or homestretch to allow a longer straight run.</dd>
	<dt>Client</dt>
	<dd> (US) - Purchaser of betting information from horseman or other tipster.</dd>
	<dt>Close</dt>
	<dd> (US) - Final odds on a horse (e.g. 'closed at 5 to 1'). Confusingly equates to 'Starting Price' in the UK.</dd>
	<dt>Closer</dt>
	<dd>A horse that runs best in the latter part of the race (closing race), coming from off the pace.</dd>
	<dt>Co-Favorites</dt>
	<dd>Where three or more competitors share the status as favorite.</dd>
	<dt>Colors</dt>
	<dd> (Colours) - Racing silks, the jacket and cap worn by jockeys. Silks can be generic and provided by the track or specific to one owner.</dd>
	<dt>Colt</dt>
	<dd>An ungelded (entire) male horse four-years-old or younger.</dd>
	<dt>Combination Bet</dt>
	<dd>Selecting any number of teams/horses to finish first and second in either order.</dd>
	<dt>Conditional Jockey</dt>
	<dd>Same as 'Apprentice' but also allowed to jump.</dd>
	<dt>Correct Weight</dt>
	<dd>Horses are allocated a weight to carry that is checked before and, for at least the placegetters, after a race. Correct weight must be signaled before bets can be paid out.</dd>
	<dt>Daily Double</dt>
	<dd>Type of wager calling for the selection of winners of two consecutive races, usually the first and second. See 'Late Double'.</dd>
	<dt>Daily Racing Form</dt>
	<dd>A daily newspaper containing racing information including news, past performance data and handicapping.</dd>
	<dt>Daily Triple</dt>
	<dd>A wager where the bettor must select the winner of three consecutive races.</dd>
	<dt>Dead Heat</dt>
	<dd>A tie. Two or more horses finishing equal in a race.</dd>
	<dt>Dead Track</dt>
	<dd>Racing surface lacking resiliency.</dd>
	<dt>Declaration Of Weights</dt>
	<dd>The publication of weights allocated to each horse nominated for a race by the handicapper.</dd>
	<dt>Declared</dt>
	<dd>In the United States, a horse withdrawn from a stakes race in advance of scratch time. In Europe, a horse confirmed to start in a race.</dd>
	<dt>Deductions</dt>
	<dd>When a horse is scratched from a race after betting on that race has already started, deductions are taken out of the win and place bets at a rate in proportion to the odds of the scratched horse.</dd>
	<dt>Derby</dt>
	<dd>A stakes event for three-year-olds.</dd>
	<dt>Dime</dt>
	<dd> (US) - A bet of USD$ 1,000 (also known as a 'dime bet').</dd>
	<dt>Distanced</dt>
	<dd>Well beaten, finishing a long distance behind the winner.</dd>
	<dt>Dividend</dt>
	<dd>The amount that a winning or placed horse returns for every $1 bet by the bettor.</dd>
	<dt>Dog</dt>
	<dd> (US) - The underdog in any betting proposition.</dd>
	<dt>Dog Player</dt>
	<dd> (US) - A bettor who mainly wagers on the underdog.</dd>
	<dt>Double</dt>
	<dd>Selecting the winners in two specific races.</dd>
	<dt>Double Carpet</dt>
	<dd>UK slang for Odds of 33 to 1, based on 'Carpet'.</dd>
	<dt>Draw</dt>
	<dd>Refers to a horse's placing in the starting stalls. For flat racing only. Stall numbers are drawn at random.</dd>
	<dt>Drift</dt>
	<dd>(Also, Ease) Odds that 'Lengthen', are said to have drifted, or be 'On The Drift'.</dd>
	<dt>Driving</dt>
	<dd>Strong urging by rider.</dd>
	<dt>Dual Forecast</dt>
	<dd>A tote bet operating in races of 3 or more declared runners in which the punter has to pick the first two to finish in either order.<br /><br />

</dl>
</div>
				
<div id="E-G">
<dl>
	<dt>Each Way</dt>
	<dd>UK term for betting on a horse to win and/or 'Place'. An each way bet is when you have the same amount on the horse for a win and for a place. Bookmakers will give you one quarter of the win odds for a place in fields of eight or more and one third of the win odds in fields of six or seven horses.</dd>
	<dt>Each Way Double</dt>
	<dd>Two separate bets of a win double and a place double.</dd>
	<dt>Each Way Single</dt>
	<dd>Two bets. The first is for the selection to win; the second for it to be placed (each way).</dd>
	<dt>Eclipse Award</dt>
	<dd>Thoroughbred racing's year-end awards, honoring the top horses in 11 separate categories.</dd>
	<dt>Enclosure</dt>
	<dd>The area where the Runners gather for viewing before and after the race.</dd>
	<dt>Equibase</dt>
	<dd> (Company) - A partnership between The Jockey Club and the Thoroughbred Racing Associations to establish and maintain an industry-owned, central database of racing records. Equibase past-performance information is used in track programs across North America.</dd>
	<dt>Equivalent Odds</dt>
	<dd>Mutuel price horses would pay for each $1 bet.</dd>
	<dt>Evenly</dt>
	<dd>Neither gaining nor losing position or distance during a race.</dd>
	<dt>Even Money Bet</dt>
	<dd> (or Evens) - A 1:1 bet. A $10 wager wins $10.</dd>
	<dt>Exacta</dt>
	<dd>(Also, Perfecta) A wager that picks the first two finishers in a race in the exact order of finish. (Straight Forecast in the UK.)</dd>
	<dt>Exacta Box</dt>
	<dd>A wager in which all possible combinations using a given number of horses are covered.</dd>
	<dt>Exotic</dt>
	<dd> (wager) - Any wager other than win, place or show.</dd>
	<dt>Exposure</dt>
	<dd>The amount of money one actually stands to lose on a game or race.</dd>
	<dt>Extended</dt>
	<dd>Forced to run at top speed.</dd>
	<dt>False Favorite</dt>
	<dd>A horse that is a race favorite despite being outclassed by others.</dd>
	<dt>Faltered</dt>
	<dd>A horse that was in contention early in the race but drops back in the late stages.</dd>
	<dt>Fast</dt>
	<dd> (track) - Optimum condition for a dirt track that is dry, even, resilient and fast.</dd>
	<dt>Favorite</dt>
	<dd>The most popular horse in a race, which is quoted at the lowest odds because it is deemed to have the best chance of winning the race.</dd>
	<dt>Feature Races</dt>
	<dd>Top races.</dd>
	<dt>Fence</dt>
	<dd>The inside fence is the inside running rail around the race track, while the outside fence is the outside running rail.</dd>
	<dt>Field</dt>
	<dd>1) All the runners in a race. 2) Some sportsbooks or bookmakers may well group all the outsiders in a competition under the banner headline of 'Field' and put it head to head with the favorite. This is known as favorite vs the field betting and is common in horse and golf betting.</dd>
	<dt>Field Horse</dt>
	<dd>Two or more starters running as a single betting unit, when there are more entrants than positions on the totalisator board can accommodate.</dd>
	<dt>Filly</dt>
	<dd>Female horse four-years-old or younger.</dd>
	<dt>Firm</dt>
	<dd> (track) - A condition of a turf course corresponding to fast on a dirt track. A firm, resilient surface.</dd>
	<dt>First Up</dt>
	<dd>The first run a horse has in a new campaign or preparation.</dd>
	<dt>Fixed Odds</dt>
	<dd>Your dividend is fixed at the odds when you placed your bet.</dd>
	<dt>Fixture</dt>
	<dd>See 'Meeting'.</dd>
	<dt>Flag</dt>
	<dd>A bet consisting of 23 bets (a 'Yankee' plus 6 'Single Stakes About' bets in pairs) on 4 selections in different event.</dd>
	<dt>Flash</dt>
	<dd> (US) - Change of odds information on tote board.</dd>
	<dt>Flat race</dt>
	<dd>Contested on level ground as opposed to a steeplechase.</dd>
	<dt>Flatten Out</dt>
	<dd>When a horse drops his head almost in a straight line with his body, generally from exhaustion.</dd>
	<dt>Foal</dt>
	<dd>A baby horse, usually refers to either a male or female horse from birth to January 1st of the following year.</dd>
	<dt>Fold</dt>
	<dd>When preceded by a number, a fold indicates the number of selections in an accumulator (e.g. 5-Fold = 5 selections).</dd>
	<dt>Forecast</dt>
	<dd>A wager that involves correctly predicting the 1st and 2nd for a particular event. This bet can be straight, reversed or permed. (USA, Perfecta or Exacta).</dd>
	<dt>Form</dt>
	<dd>Statistics of previous performance and comment as to the expected current performance of a runner, useful in deciding which runner to bet on.</dd>
	<dt>Form Player</dt>
	<dd>A bettor who makes selections from past-performance records.</dd>
	<dt>Front-runner</dt>
	<dd>A horse whose running style is to attempt to get on or near the lead at the start of the race and stay there as long as possible.</dd>
	<dt>Frozen</dt>
	<dd> (track) - A condition of a racetrack where any moisture present is frozen.</dd>
	<dt>Full Cover</dt>
	<dd>All the doubles, trebles and accumulators involved in a given number of selections.</dd>
	<dt>Furlong</dt>
	<dd>One-eighth of a mile or 220 yards or 660 feet (approx. 200 meters).</dd>
	<dt>Futures</dt>
	<dd>(Also, Ante Post) Bets placed in advance predicting the outcome of a future event.</dd>
	<dt>Gait</dt>
	<dd>Harness horses are divided into two distinct groups, pacers or trotters, depending on their gait when racing. The gait is the manner in that a horse moves its legs when running. The pacer is a horse with a lateral gait, whereas a trotter or square-gaiter has a diagonal gait.</dd>
	<dt>Gate</dt>
	<dd>Another term for barrier, or position a horse will start from.</dd>
	<dt>Gelding</dt>
	<dd>A male horse that has been castrated.</dd>
	<dt>Gentleman Jockey</dt>
	<dd>Amateur rider, generally in steeplechases.</dd>
	<dt>Going</dt>
	<dd>The condition of the racecourse (firm, heavy, soft, etc.).</dd>
	<dt>Good</dt>
	<dd> (track) - Condition between fast and slow, generally a bit wet. A dirt track that is almost fast or a turf course slightly softer than firm.</dd>
	<dt>Graded Race</dt>
	<dd>Established in 1973 to classify select stakes races in North America, at the request of European racing authorities, who had set up group races two years earlier. Always denoted with Roman numerals I, II, or III. Capitalized when used in race title (the Grade I Kentucky Derby). See 'Group Race' below.</dd>
	<dt>Graduate</dt>
	<dd>Winning for the first time.</dd>
	<dt>Grand</dt>
	<dd>GBP£ 1,000 (also known as a Big'un).</dd>
	<dt>Green</dt>
	<dd>An inexperienced horse.</dd>
	<dt>Group Race</dt>
	<dd>An elite group of races. Established in 1971 by racing organizations in Britain, France, Germany and Italy to classify select stakes races outside North America. Collectively called 'Pattern Races'. Equivalent to North American graded races. Always denoted with Arabic numerals 1, 2, or 3. Capitalized when used in race title (the Group 1 Epsom Derby). See 'Graded Race' above.<br /><br />

</dl>
</div>
				
<div id="H-M">
<dl>
	<dt>Hand</dt>
	<dd>Four inches. A horse's height is measured in hands and inches from the top of the shoulder (withers) to the ground, e.g., 15.2 hands is 15 hands, 2 inches. Thoroughbreds typically range from 15 to 17 hands.</dd>
	<dt>Handicap</dt>
	<dd>1) Race for which the track handicapper assigns the weights to be carried. Each horse is allocated a different weight to carry, the theory being all horses then run on a fair and equal basis.. 2) To make selections on the basis of past performances.</dd>
	<dt>Handicapper</dt>
	<dd>The official who decides the weights to be carried in handicap events, and the grading of horses and greyhounds.</dd>
	<dt>Hand Ride</dt>
	<dd>The jockey urges a horse with the hands and arms without using the whip.</dd>
	<dt>Hard</dt>
	<dd> (track) - A condition of a turf course where there is no resiliency to the surface.</dd>
	<dt>Head</dt>
	<dd>A margin between horses. One horse leading another by the length of its head.</dd>
	<dt>Head Of The Stretch</dt>
	<dd>Beginning of the straight run to the finish line.</dd>
	<dt>Heavy</dt>
	<dd> (track) - Wettest possible condition of a turf course, similar to muddy but slower; not usually found in North America.</dd>
	<dt>Hedge</dt>
	<dd>The covering of a bet with a second bet.</dd>
	<dt>Hedging</dt>
	<dd>A bet made by a cautious bookie on a horse on which he has accepted large bets - in order to cut his losses if the horse wins (also known as a 'lay-off bet').</dd>
	<dt>Heinz</dt>
	<dd>A Heinz is a multiple bet consisting of 57 bets involving 6 selections in different events. The multiple bet breakdown is 15 doubles, 20 trebles, 15x4-folds, 6x5-folds and one 6-fold.</dd>
	<dt>High Weight</dt>
	<dd>Highest weight assigned or carried in a race.</dd>
	<dt>Home Turn</dt>
	<dd>The final turn a horse must travel around before entering the home straight in the run to the finish line.</dd>
	<dt>Horse</dt>
	<dd>When reference is made to sex, a 'horse' is an ungelded male five-years-old or older.</dd>
	<dt>Hung</dt>
	<dd>A horse holding the same position, unable to make up distance on the winner.</dd>
	<dt>Impost</dt>
	<dd>Weight carried or assigned.</dd>
	<dt>In Hand</dt>
	<dd>Running under moderate control, at less than best pace.</dd>
	<dt>Inquiry</dt>
	<dd>Reviewing the race to check into a possible infraction of the rules. Also, a sign flashed by officials on the tote board on such occasions. If lodged by a jockey, it is called an objection.</dd>
	<dt>In The Money</dt>
	<dd>Describes the horses in a race that finish 1st, 2nd and 3rd (and sometimes 4th) or the horses on which money will be paid to bettors, depending on the place terms.</dd>
	<dt>In The Red</dt>
	<dd>Are odds shown in red on the betting boards because they are Odds-On bets.</dd>
	<dt>Investor</dt>
	<dd>A bettor. A person at a licensed race meeting who bets with a bookmaker or the totalisator, or a person not present at the meeting, but places bets on the horses engaged at that meeting with the off-course totalisator.</dd>
	<dt>Joint Favourites</dt>
	<dd>When a sportsbook or bookmaker cannot separate two horses or teams for favouritism, they are made joint favourites.</dd>
	<dt>Judge</dt>
	<dd>The person who declares the official placing for each race.</dd>
	<dt>Juice</dt>
	<dd>The bookmaker's commission, also known as vigorish or vig.</dd>
	<dt>Jumper</dt>
	<dd>Steeplechase or hurdle horse.</dd>
	<dt>Jolly</dt>
	<dd>The favourite in a race.</dd>
	<dt>Judge</dt>
	<dd>The official who determines the finishing order of a race.</dd>
	<dt>Juvenile</dt>
	<dd>Two-year-old horse.</dd>
	<dt>Key Horse</dt>
	<dd>The main expected winning horse used in multiple combinations in an exotic wager.</dd>
	<dt>Kite</dt>
	<dd>UK slang for a cheque ('Check' in the US).</dd>
	<dt>Late Double</dt>
	<dd>A second daily double offered during the latter part of the program. See 'Daily Double' above.</dd>
	<dt>Lay Off, Layoff</dt>
	<dd>Bets made by one bookmaker with another bookmaker, in an effort to reduce his liability in respect of bets already laid by him with investors.</dd>
	<dt>LBO</dt>
	<dd>Acronym for 'Licensed Betting Office' in the UK.</dd>
	<dt>Leg In</dt>
	<dd>To nominate one runner to win with a selection of other runners. This is possible on Forecast, Quinella, Trifecta, Quartet and Superfecta (eg. Quinella bet with selection 4 to win, from runners 5, 7, 8 and 9 to come second, in any order).</dd>
	<dt>Length</dt>
	<dd>A measurement approximating the length of a horse from nose to tail, about 8 feet, used to denote distance between horses in a race. For example, "Secretariat won the Belmont by 31 lengths".</dd>
	<dt>Lengthen</dt>
	<dd>The opposite of 'Shorten'. Referred to odds getting longer, that is, more attractive to the bettor.</dd>
	<dt>Listed Race</dt>
	<dd>A stakes race just below a group race or graded race in quality.</dd>
	<dt>Lock</dt>
	<dd>(As in 'Banker') US term for an almost certain winner. Easy winner.</dd>
	<dt>Long Odds</dt>
	<dd>More than 10:1.</dd>
	<dt>Long Shot</dt>
	<dd>(Also, Outsider) An runner is often referred to as being a long shot, because of the fact it is returning high odds and is therefore deemed to have little chance of winning the race.</dd>
	<dt>Lug In</dt>
	<dd> (Out) - Action of a tiring horse, bearing in or out, failing to keep a straight course.</dd>
	<dt>Maiden</dt>
	<dd>1) A horse or rider that has not won a race. 2) A female that has never been bred.</dd>
	<dt>Maiden Race</dt>
	<dd>A race for non-winners.</dd>
	<dt>Mare</dt>
	<dd>Female horse five-years-old or older.</dd>
	<dt>Market</dt>
	<dd>The list of all horses engaged in a race and their respective odds.</dd>
	<dt>Meeting</dt>
	<dd>A collection of races conducted by a club on the same day or night forms a race meeting.</dd>
	<dt>Middle Distance</dt>
	<dd>Broadly, from one mile to 1-1/8 miles.</dd>
	<dt>Mile Rate</dt>
	<dd>In harness racing it is the approximate time a horse would have run per mile (1609 meters).</dd>
	<dt>Minus Pool</dt>
	<dd>A mutuel pool caused when a horse is so heavily played that, after deductions of state tax and commission, there is not enough money left to pay the legally prescribed minimum on each winning bet. The racing association usually makes up the difference.</dd>
	<dt>Money Rider</dt>
	<dd>A rider who excels in rich races.</dd>
	<dt>Monkey</dt>
	<dd>GBP£ 500.</dd>
	<dt>Morning Glory</dt>
	<dd>Horse who performs well in morning workouts but fails to fire in actual races.</dd>
	<dt>Morning Line</dt>
	<dd>Approximate odds quoted before wagering begins.</dd>
	<dt>Mudder</dt>
	<dd>A horse that races well on muddy tracks. Also known as a 'Mudlark'.</dd>
	<dt>Muddy</dt>
	<dd> (track) - A condition of a racetrack which is wet but has no standing water.</dd>
	<dt>Mutuel Pool</dt>
	<dd>Short for 'Parimutuel Pool'. Sum of the wagers on a race or event, such as the win pool, daily double pool, exacta pool, etc.<br /><br />

</dl>
</div>
				
<div id="N-P">
<dl>
	<dt>Nap</dt>
	<dd>The selection that racing correspondents and tipsters nominate as their strongest selection of the day or meeting. Reputed to stand for 'Napoleon'.</dd>
	<dt>National Thoroughbred Racing Association</dt>
	<dd> (NTRA) - A non-profit, membership organization created in 1997 to improve economic conditions and public interest in Thoroughbred racing.</dd>
	<dt>Neck</dt>
	<dd>Unit of measurement about the length of a horse's neck.</dd>
	<dt>Nickel</dt>
	<dd>A $500 wager.</dd>
	<dt>Nod</dt>
	<dd>Lowering of head. To win by a nod, a horse extends its head with its nose touching the finish line ahead of a close competitor.</dd>
	<dt>Nominations</dt>
	<dd>The complete list of runners entered by owners and trainers for a race.</dd>
	<dt>Nose</dt>
	<dd>Smallest advantage a horse can win by. Called a short head in Britain.</dd>
	<dt>Nursery</dt>
	<dd>A handicap for two-year-old horses.</dd>
	<dt>Oaks</dt>
	<dd>A stakes event for three-year-old fillies (females).</dd>
	<dt>Objection</dt>
	<dd>Claim of foul lodged by rider, patrol judge or other official after the running of a race. If lodged by official, it is called an inquiry.</dd>
	<dt>Odds</dt>
	<dd>The sportsbook's or bookmaker's view of the chance of a competitor winning (adjusted to include a profit). The figure or fraction by which a bookmaker or totalisator offers to multiply a bettor's stake, which the bettor is entitled to receive (plus his or her own stake) if their selection wins.</dd>
	<dt>Odds-against</dt>
	<dd>Where the odds are greater than evens (e.g. 5 to 2). When the bookmaker's or totalisator's stake is greater than the bettor's stake. For example, a horse that is quoted at 4:1 would be odds against, because if it wins a race, the bookmaker or totalisator returns $4 for every dollar a bettor places on that horse, plus his or her original outlay.</dd>
	<dt>Odds Compiler</dt>
	<dd>Same as 'Oddsmaker' below.</dd>
	<dt>Oddsmaker</dt>
	<dd>A person who sets the betting odds. (Sportsbooks or Bookies don't set the odds. Most major sportsbooks use odds set by Las Vegas oddsmakers.)</dd>
	<dt>Odds Man</dt>
	<dd> (US) - At tracks where computers are not in use, an employee who calculates changing odds as betting progresses.</dd>
	<dt>Odds-On</dt>
	<dd>Odds of less than even money. This a bet where you have to outlay more than you win. For example if a horse is two to one Odds-On, you have to outlay two dollars to win one dollar and your total collect if the horse wins is three dollars. That is made up of your two dollars and the one dollar you win.</dd>
	<dt>Official</dt>
	<dd>Sign displayed when result is confirmed. Also racing official.</dd>
	<dt>Off the Board</dt>
	<dd> (US) - A horse so lightly bet that its pari-mutuel odds exceed 99 to 1. Also, a game or event on which the bookie will not accept action.</dd>
	<dt>Off-Track Betting</dt>
	<dd> (OTB) - Wagering at legalized betting outlets.</dd>
	<dt>On The Board</dt>
	<dd>Finishing among the first three.</dd>
	<dt>On The Nose</dt>
	<dd>Betting a horse to win only.</dd>
	<dt>Open Ditch</dt>
	<dd>Steeplechase jump with a ditch on the side facing the jockey.</dd>
	<dt>Outlay</dt>
	<dd>The money a bettor wagers is called his or her outlay.</dd>
	<dt>Out Of The Money</dt>
	<dd>A horse that finishes worse than third.</dd>
	<dt>Outsider</dt>
	<dd>A horse that is not expected to win. An outsider is usually quoted at the highest odds.</dd>
	<dt>Overbroke</dt>
	<dd>Where the book results in a loss for the bookmaker.</dd>
	<dt>Overlay</dt>
	<dd>A horse going off at higher odds than it appears to warrant based on its past performances.</dd>
	<dt>Overnight Race</dt>
	<dd>A race in which entries close a specific number of hours before running (such as 48 hours), as opposed to a stakes race for which nominations close weeks and sometimes months in advance.</dd>
	<dt>Over The Top</dt>
	<dd>When a horse is considered to have reached its peak for that season.</dd>
	<dt>Overweight</dt>
	<dd>Surplus weight carried by a horse when the rider cannot make the assigned weight.</dd>
	<dt>Pacesetter</dt>
	<dd>The horse that is running in front (on the lead).</dd>
	<dt>Paddock</dt>
	<dd>Area where horses are saddled and kept before post time.</dd>
	<dt>Panel</dt>
	<dd>A slang term for a furlong.</dd>
	<dt>Parimutuel</dt>
	<dd>(s) - A form of wagering originated in 1865 by Frenchman Pierre Oller in which all money bet is divided up among those who have winning tickets, after taxes, takeout and other deductions are made. Oller called his system 'Parier Mutuel' meaning 'Mutual Stake' or 'betting among ourselves'. As this wagering method was adopted in England it became known as 'Paris Mutuals', and soon after 'Parimutuels'.</dd>
	<dt>Parlay</dt>
	<dd>(Also, Accumulator) A multiple bet. A kind of 'let-it-ride' bet. Making simultaneous selections on two or more races with the intent of pressing the winnings of the first win on the bet of the following race selected, and so on. All the selections made must win for you to win the parlay.</dd>
	<dt>Part Wheel</dt>
	<dd>Using a key horse or horses in different, but not all possible, exotic wagering combinations.</dd>
	<dt>Pasteboard Track</dt>
	<dd>A lightning fast racing surface.</dd>
	<dt>Patent</dt>
	<dd>A multiple bet consisting of 7 bets involving 3 selections in different events. A single on each selection, plus 3 doubles and 1 treble.</dd>
	<dt>Penalty</dt>
	<dd>A weight added to the handicap weight of a horse.</dd>
	<dt>Permutations</dt>
	<dd>It is possible to Perm bets or selections (e.g. on 4 selections all the possible doubles could be Permed making 6 bets).</dd>
	<dt>Phone Betting</dt>
	<dd>A service enabling punters to bet on horses with bookmakers by using telephones.</dd>
	<dt>Phone TAB</dt>
	<dd>Another phone betting service, provided by a totalisator which allows people with special betting accounts to place bets via the telephone. Much the same as a bank account, you must have a credit balance to be able to place a bet. The cost of the investment is debited to your account, and winning dividends and refunds are automatically credited to your account.</dd>
	<dt>Photo Finish</dt>
	<dd>A photo is automatically taken as the horses pass the winning line and when the race is too close to be judged the photo is used to determine the order of finish.</dd>
	<dt>Picks</dt>
	<dd>Betting selections, usually by an expert.</dd>
	<dt>Pick Six</dt>
	<dd> (or more) - A type of wager in which the winners of all the included races must be selected.</dd>
	<dt>Pitch</dt>
	<dd>The position where a bookmaker conducts his business on a racecourse.</dd>
	<dt>Place</dt>
	<dd>Finish in the top two, top three, top four and sometimes also top five in a competition or event. A Place bet will win if the selection you bet on is among those placed. Usually, a horse runs a place if it finishes in the first three in fields of eight or more horses. If there are only six or seven runners the horse must finish first or second to place. Different sportsbooks have different Place terms and you should check their rules before placing a bet. In US, 2nd place finish. (See 'Each Way' UK)</dd>
	<dt>Point Spread</dt>
	<dd>(Also, Line or Handicap) The points allocated to the 'underdog' to level the odds with the 'favorite/favourite'.</dd>
	<dt>Pole</dt>
	<dd>(s) - Markers at measured distances around the track designating the distance from the finish. The quarter pole, for instance, is a quarter of a mile from the finish, not from the start.</dd>
	<dt>Pony</dt>
	<dd>GBP£ 25.</dd>
	<dt>Pool</dt>
	<dd>Mutuel pool, the total sum bet on a race or a particular bet.</dd>
	<dt>Post</dt>
	<dd>1) Starting point for a race. 2) An abbreviated version of post position. For example, "He drew post four". 3) As a verb, to record a win. For example, "He's posted 10 wins in 14 starts".</dd>
	<dt>Post Position</dt>
	<dd>Position of stall in starting gate from which a horse starts.</dd>
	<dt>Post Time</dt>
	<dd>Designated time for a race to start.</dd>
	<dt>Price</dt>
	<dd>The odds.</dd>
	<dt>Protest</dt>
	<dd>When a jockey, owner, trainer or steward alleges interference by one party against another during a race that may have affected the outcome of a race. If a protest is upheld by officials, the runner that caused the interference is placed directly after the horse interfered with. If a protest is dismissed by officials, the original result of the race stands.</dd>
	<dt>Punt</dt>
	<dd>Another term for bet or wager.</dd>
	<dt>Punter</dt>
	<dd>Bettor or investor.</dd>
	<dt>Pull Up</dt>
	<dd>To stop or slow a horse during or after a race or workout.<br /><br />

</dl>
</div>
				
<div id="Q-S">
<dl>
	<dt>Quadrella</dt>
	<dd>Selecting the winner of four specifically nominated races.</dd>
	<dt>Quiniela</dt>
	<dd> (Quinella) - Wager in which the first two finishers must be picked in either order. Payoff is made no matter which of the two wins and which runs second. ('Reverse Forecast' in the UK. See Wagers for Quiniela variants.)</dd>
	<dt>Race Caller</dt>
	<dd>The person who describes the race at a racecourse.</dd>
	<dt>Racecard</dt>
	<dd>A programme for the day's racing.</dd>
	<dt>Rail Runner</dt>
	<dd>Horse that prefers to run next to the inside rail.</dd>
	<dt>Ratings</dt>
	<dd>Tipsters may determine a set of ratings which reflect, in their opinion, each runner's chance of winning a particular race taking a number of factors into account when preparing them.</dd>
	<dt>Restricted Races</dt>
	<dd>Races which only certain horses are eligible.</dd>
	<dt>Return</dt>
	<dd>The dividend you receive on a particular bet.</dd>
	<dt>Reverse Forecast</dt>
	<dd> (UK) - See 'Quinella' above.</dd>
	<dt>Ringer</dt>
	<dd>A horse (or greyhound) entered in a race under another's name - usually a good runner replacing a poorer one.</dd>
	<dt>Roughie</dt>
	<dd>A horse which is considered to have a 'rough' chance of winning a race.</dd>
	<dt>Roundabout</dt>
	<dd>A bet consisting of 3 bets involving three selections in different events (i.e. 1 single any to come and double stake double on remaining two selections, 3 times).</dd>
	<dt>Rounder</dt>
	<dd>A bet consisting of 3 bets involving three selections in different events (i.e. 1 single any to come a single stake double on remaining two selections, 3 times).</dd>
	<dt>Round Robin</dt>
	<dd>A bet consisting of 10 bets (3 pairs of 'Single Stakes About' bets plus 3 doubles and 1 treble) involving three selections in different events. (US, A series of three or more teams into two-team wagers).</dd>
	<dt>Route</dt>
	<dd>Broadly, a race distance of longer than 1-1/8 miles.</dd>
	<dt>Router</dt>
	<dd>Horse that performs well at longer distances.</dd>
	<dt>Run Free</dt>
	<dd>A horse going too fast.</dd>
	<dt>Runner</dt>
	<dd>A participant in a race. In US, a sportsbook's employee who gathers information on the progress of betting elsewhere on the course. Also, a messenger 'running' to and from pari-mutuel windows for occupants of clubhouse boxes.</dd>
	<dt>Scale Of Weights</dt>
	<dd>Fixed weights to be carried by horses in a race according to age, distance, sex, and time of year.</dd>
	<dt>Scalper</dt>
	<dd>One who attempts to profit from the differences in odds from book to book by betting both sides of the same game at different prices.</dd>
	<dt>Schooled</dt>
	<dd>A horse trained for jumping.</dd>
	<dt>Scope</dt>
	<dd>The potential in a horse.</dd>
	<dt>Score</dt>
	<dd>GBP£ 20. In US, to win a race or a bet. Also, a victory.</dd>
	<dt>Scratch</dt>
	<dd>To be taken out of a race before it starts. Trainers usually scratch horses due to adverse track conditions or a horse's adverse health. A veterinarian can scratch a horse at any time.</dd>
	<dt>Scratch Sheet</dt>
	<dd>Daily publication that includes graded handicaps, tips and scratches.</dd>
	<dt>Second Call</dt>
	<dd>A secondary mount of a jockey in a race in the event his primary mount is scratched.</dd>
	<dt>Selections</dt>
	<dd>The horses selected by a knowledgeable person (Tipster) to have the most likely chance of finishing in first, second and third place. This may also refer to a person's own selections - the horses they have chosen to back.</dd>
	<dt>Selling Race</dt>
	<dd>A race where the winner is sold by auction immediately afterwards.</dd>
	<dt>Settler</dt>
	<dd>A bookmaker's expert who calculates payouts.</dd>
	<dt>Shadow Roll</dt>
	<dd>Usually a lamb's wool roll half way up the horse's face to keep him from seeing his own shadow.</dd>
	<dt>Shorten</dt>
	<dd>, Shortening the Odds - When the odds of a horse decrease, usually because a lot of money has been wagered on that horse.</dd>
	<dt>Short Runner</dt>
	<dd>A horse who barely stays, or doesn't stay, the full distance of a race.</dd>
	<dt>Short Price</dt>
	<dd>Low odds, meaning a punter will get little return for their initial outlay.</dd>
	<dt>Show</dt>
	<dd>Third position at the finish.</dd>
	<dt>Show Bet</dt>
	<dd>Wager on a horse to finish in the money; third or better.</dd>
	<dt>Shut Out</dt>
	<dd> (US) - What happens to a bettor who gets on the betting line to late and is still waiting in line when the window closes. Also, in sports betting, when the losing team do not score.</dd>
	<dt>Silks</dt>
	<dd>See 'Colors'.</dd>
	<dt>Simulcast</dt>
	<dd>A simultaneous live television transmission of a race to other tracks, off-track betting offices or other outlets for the purpose of wagering.</dd>
	<dt>Single</dt>
	<dd>A Straight bet on one selection to win one race or event, also known as a straight-up bet.</dd>
	<dt>Single Stakes About</dt>
	<dd> (or SSA) - A bet consisting of 2 bets on two selections (1 single on each selection any to come 1 single on the other selection reversed).</dd>
	<dt>Sire</dt>
	<dd>Father of a horse.</dd>
	<dt>Six-Dollar Combine</dt>
	<dd> (US) - An across-the-board bet in racing.</dd>
	<dt>Sloppy</dt>
	<dd> (track) - A track that is wet on surface, with standing water visible, with firm bottom.</dd>
	<dt>Slow</dt>
	<dd> (track) - A racing strip that is wet on both the surface and base. Between good and heavy.</dd>
	<dt>Smart Money</dt>
	<dd>Insiders' bets or the insiders themselves.</dd>
	<dt>Soft</dt>
	<dd> (track) - Condition of a turf course with a large amount of moisture. Horses sink very deeply into it.</dd>
	<dt>Spell</dt>
	<dd>The resting period between preparations or racing.</dd>
	<dt>Sportsbook</dt>
	<dd>The person, shop or website who accepts bets.</dd>
	<dt>Spot Play</dt>
	<dd> (US) - Type of play in which bettor risks money only on types of races and horses which seem relatively worthwhile risks.</dd>
	<dt>Sprint</dt>
	<dd>Short race, less than one mile.</dd>
	<dt>Stake</dt>
	<dd>The prize money for the winning horses paid to the owner (eg. trophy or prize money).</dd>
	<dt>Stakes</dt>
	<dd>The sums of money deposited or guaranteed by the parties to a bet.</dd>
	<dt>Stakes-Placed</dt>
	<dd>Finished second or third in a stakes race.</dd>
	<dt>Stakes Horse</dt>
	<dd>A horse whose level of competition includes mostly stakes races.</dd>
	<dt>Stallion</dt>
	<dd>A male horse used for breeding.</dd>
	<dt>Standing Start</dt>
	<dd>In harness racing, starters start from a standing position, once the barrier across the track is released.</dd>
	<dt>Starter</dt>
	<dd>The person responsible for starting a race.</dd>
	<dt>Starting Gate</dt>
	<dd>Partitioned mechanical device having stalls in which the horses are confined until the starter releases the doors in front to begin the race.</dd>
	<dt>Starting Price</dt>
	<dd> (or SP) - An estimation of odds available when the race starts.</dd>
	<dt>Starting Stalls</dt>
	<dd>Mechanical gates that ensure all horses start in unison.</dd>
	<dt>Stayer</dt>
	<dd> (Also, Slayer) - A horse that can race long distances.</dd>
	<dt>Steam</dt>
	<dd>When a betting selection starts to move quite rapidly, usually caused by many bettors betting on it.</dd>
	<dt>Steeplechase</dt>
	<dd>A race in which horses are required to jump over a series of obstacles on the course. Also known as a 'Chase'.</dd>
	<dt>Stewards</dt>
	<dd>The group of people who control the day's racing by ensuring that every runner competes on its merits and imposing penalties for any breach of the rules of racing.</dd>
	<dt>Stewards Enquiry</dt>
	<dd>An enquiry by the stewards into a race.</dd>
	<dt>Stick</dt>
	<dd>(Also, Bat) A jockey's whip.</dd>
	<dt>Stickers</dt>
	<dd>Calks on shoes which give a horse better traction in mud or on soft tracks.</dd>
	<dt>Stipes</dt>
	<dd>Another term for the Stewards. (Or Stipendiary Stewards)</dd>
	<dt>Stooper</dt>
	<dd> (US) - Those who make a living picking up discarded mutuel tickets at racetracks and cashing those that have been thrown away by mistake.</dd>
	<dt>Store</dt>
	<dd> (US) - A sportsbook or a bookie.</dd>
	<dt>Straight</dt>
	<dd>Betting to win only.</dd>
	<dt>Straight Forecast</dt>
	<dd> (UK) - A tote bet operating in races of 3 or more declared runners in which the punter has to pick the first and second to finish in the correct order. See 'Exacta'.</dd>
	<dt>Straight Six</dt>
	<dd>A wager to correctly select the winner of each of six consecutive nominated races.</dd>
	<dt>Strapper</dt>
	<dd>Also known as an attendant. A person who assists the trainer, cares for the horse or helps to put on its equipment.</dd>
	<dt>Stretch</dt>
	<dd> (home-Stretch) - Final straight portion of the racetrack to the finish.</dd>
	<dt>Stretch Runner</dt>
	<dd>Horse that runs its fastest nearing the finish of a race.</dd>
	<dt>Stretch Turn</dt>
	<dd>Bend of track into homestretch.</dd>
	<dt>Stud</dt>
	<dd>1) Male horse used for breeding. 2) A breeding farm.</dd>
	<dt>Superfecta</dt>
	<dd>A bet placed on four horses to cross the finish line in exact chosen order.</dd>
	<dt>Super Yankee</dt>
	<dd>Alternative name for a multiple bet known as Canadian, a Super Yankee is a Yankee type bet with five selections instead of four.</dd>
	<dt>Sure Thing</dt>
	<dd>A horse which a punter or tipster believes is unbeatable in a race.</dd>
	<dt>Sweepstakes</dt>
	<dd>Type of betting whereby each horse in a race is drawn out of a hat by a particular person (who pays a set amount of money for the privilege of buying a horse). The people which chose the winner and placegetters will receive a percentage of the total money pool.</dd>
	<dt>System</dt>
	<dd>A method of betting, usually mathematically based, used by a punter or bettor to try to get an advantage.<br /><br />

</dl>
</div>
				
<div id="T-Z">
	<dl>
	<dt>TAB</dt>
	<dd>Totalisator Agency Board. The body appointed to regulate off-course betting (bets made by people who are not present at the race track).</dd>
	<dt>Take</dt>
	<dd> (Takeout) - Commission deducted from mutuel pools which is shared by the track, horsemen (in the form of purses) and local and state governing bodies in the form of tax.</dd>
	<dt>Taken Up</dt>
	<dd>A horse pulled up sharply by his rider because of being in close quarters.</dd>
	<dt>The Jockey Club</dt>
	<dd>An organization dedicated to the improvement of Thoroughbred breeding and racing. Incorporated Feb. 10, 1894 in New York City, The Jockey Club serves as North America's Thoroughbred registry, responsible for the maintenance of 'The American Stud Book', a register of all Thoroughbreds foaled in the United States, Puerto Rico and Canada; and of all Thoroughbreds imported into those countries from jurisdictions that have a registry recognized by The Jockey Club and the International Stud Book Committee.</dd>
	<dt>Thick'un</dt>
	<dd>A big bet.</dd>
	<dt>Thoroughbred</dt>
	<dd>A Thoroughbred is a horse whose parentage traces back to any of the three 'Founding Sires' the Darley Arabian, Byerly Turk and Godolphin Barb, and who has satisfied the rules and requirements of The Jockey Club and is registered in 'The American Stud Book' or in a foreign stud book recognized by The Jockey Club and the International Stud Book Committee. Any other horse, no matter what its parentage, is not considered a Thoroughbred for racing and/or breeding purposes.</dd>
	<dt>Thoroughbred Racing Associations</dt>
	<dd> (TRA) - An industry group comprised of many of the racetracks in North America.</dd>
	<dt>Ticket</dt>
	<dd>The betting slip or ticket which is received by the bettor from the bookmaker or totalisator, as proof of his or her wager. The ticket is necessary to collect the dividends.</dd>
	<dt>Ticketer</dt>
	<dd> (US) - A forger of bookmakers' tickets.</dd>
	<dt>Tic-Tac</dt>
	<dd>The secret and complex sign language used by bookmakers at racecourses to indicate movements in the price of a horse. See BBC's Tic-Tac guide.</dd>
	<dt>Tierce</dt>
	<dd>A French combination bet in which the bettor predicts the horses that will finish 1st, 2nd and 3rd.</dd>
	<dt>Tips</dt>
	<dd>The selections chosen by an expert to bet on (also known as Picks). See 'Selections'.</dd>
	<dt>Tipster</dt>
	<dd>A person who makes selections for a race, providing tips on which horses they believe will win the first three places.</dd>
	<dt>Top Weight</dt>
	<dd>See 'High Weight'.</dd>
	<dt>Totalizator</dt>
	<dd> (Totalisator) - The system of betting on races (an automated system that dispenses and records betting tickets, calculates and displays odds and payoffs and provides the mechanism for cashing winning tickets) in which the winning bettors share the total amount bet, minus a percentage for the operators of the system, taxes etc. Synonyms: Tote, Parimutuel.</dd>
	<dt>Tote</dt>
	<dd>Totalizator. The organisation appointed to receive bets and supply dividends in proportion to the amount of the investment. A body in the UK set up to operate pool-betting on all racecourses.</dd>
	<dt>Tote Board</dt>
	<dd>The (usually) electronic totalizator display in the infield which reflects up-to-the-minute odds. It may also show the amounts wagered in each mutuel pool as well as information such as jockey and equipment changes, etc. Also known as the 'Board'.</dd>
	<dt>Tote Returns</dt>
	<dd>Returns from a tote pool (also known as a Dividend), calculated by taking the total stake in each pool (after the take out) and dividing it by the number of winning tickets. A dividend is declared to a fixed stake, for various win, place and forecast pools.</dd>
	<dt>Tout</dt>
	<dd>Person who professes to have, and sells, advance information on a race. Also used as a verb meaning to sell or advertise.</dd>
	<dt>Track Condition</dt>
	<dd>Condition of the racetrack surface. Slow; Fast; good; muddy; sloppy; frozen; hard; firm; soft; yielding; heavy.</dd>
	<dt>Track Record</dt>
	<dd>Fastest time for a distance at a particular track.</dd>
	<dt>Trail</dt>
	<dd>Racing immediately behind another horse. A trail is also known as a sit.</dd>
	<dt>Trainer</dt>
	<dd>The person responsible for looking after a horse and preparing it to race. A trainer must hold a license or permit to be entitled to train.</dd>
	<dt>Treble</dt>
	<dd>A bet consisting of 3 selections, all of which must win for the wager to be successful.</dd>
	<dt>Tricast</dt>
	<dd> (UK) - See 'Trifecta' below.</dd>
	<dt>Trifecta</dt>
	<dd>A wager picking the first three finishers in exact order. Called a 'Triactor' in Canada and a 'Triple' in some parts of the U.S. ('Tricast' in the UK.)<br />
Trifecta Box - A trifecta wager in which all possible combinations using a given number of horses are bet upon. The total number of combinations can be calculated according to the formula (x3)-(3x2)+(2x), where x equals the amount of horses in the box. The sum of the formula is then multiplied by the amount wagered on each combination.</dd>
	<dt>Triple</dt>
	<dd>(Also 'Treble') See 'Trifecta' above.</dd>
	<dt>Triple Crown</dt>
	<dd>Used generically to denote a series of three important races, but is always capitalized when referring to historical races for three-year-olds. In the United States, the Kentucky Derby, Preakness Stakes and Belmont Stakes. In England the 2,000 Guineas, Epsom Derby and St. Leger Stakes. In Canada, the Queen's Plate, Prince of Wales Stakes and Breeders' Stakes.</dd>
	<dt>Trixie</dt>
	<dd>A Trixie consists of 4 bets involving 3 selections in different events, i.e. 3 doubles plus 1 treble.</dd>
	<dt>Trotting</dt>
	<dd>A term for harness racing in general. It also describes the specific gait of a trotter.</dd>
	<dt>Turf Accountant</dt>
	<dd>The UK euphemism for a bookmaker.</dd>
	<dt>Turf Course</dt>
	<dd>Grass course.</dd>
	<dt>Unbackable</dt>
	<dd>A horse which is quoted at short odds that punters decide is too short to return any reasonable amount for the money they outlay.</dd>
	<dt>Underlay</dt>
	<dd>A horse racing at shorter odds than seems warranted by its past performances.</dd>
	<dt>Under Starters Orders</dt>
	<dd> (or Under Orders) - The starting of a race.</dd>
	<dt>Under Wraps</dt>
	<dd>Horse under stout restraint in a race or workout.</dd>
	<dt>Union Jack</dt>
	<dd>A bet consisting of 8 trebles on 9 selections A to I: ABC, DEF, GHI, ADG, BEH, CFI, AEI, and CEG.</dd>
	<dt>Value</dt>
	<dd>Getting the best odds on a wager.</dd>
	<dt>Wager</dt>
	<dd>Another term for bet.</dd>
	<dt>Walkover</dt>
	<dd>A race in which only one horse competes.</dd>
	<dt>Warming Up</dt>
	<dd>Galloping horse on way to post.</dd>
	<dt>Weigh In</dt>
	<dd> (Out) - The certification, by the clerk of scales, of a rider's weight before (after) a race. A jockey weighs in fully dressed with all equipment except for his/her helmet, whip and (in many jurisdictions) flak jacket.</dd>
	<dt>Welsh/Welch</dt>
	<dd>To fail to pay a gambling bet.</dd>
	<dt>Wheel</dt>
	<dd>Betting all possible combinations in an exotic wager using at least one horse as the key. See 'Part Wheel'.</dd>
	<dt>Wheeling</dt>
	<dd>A racing system devised for the daily double bet in which the bettor backs one horse in the first race and every horse in the second (also known as Baseball or Locking).</dd>
	<dt>Weight-For-Age</dt>
	<dd>The purpose of weight-for-age is to allow horses of different age and sex to compete on equal terms. The weight a horse carried is allocated on a set scale according to its sex and age.</dd>
	<dt>Whip</dt>
	<dd>Instrument or a stick, usually of leather, with which rider strikes horse to increase his speed.</dd>
	<dt>Win</dt>
	<dd>The term used to describe a 1st place finish.</dd>
	<dt>Win Bet</dt>
	<dd>Wager on a horse to finish first.</dd>
	<dt>Winning Post</dt>
	<dd>The finishing line of a race. (Also, The Post.)</dd>
	<dt>Wire</dt>
	<dd>The finish line of a race.</dd>
	<dt>Wise Guy</dt>
	<dd>A knowledgeable handicapper or bettor.</dd>
	<dt>With the Field</dt>
	<dd>Having one horse linked with all the other horses in an event. It can apply to forecasts or in doubles.</dd>
	<dt>Yankee</dt>
	<dd>A multiple bet consisting of 11 bets (6 doubles, 4 trebles and 1 4-fold) on 4 selections in different events.</dd>
	<dt>Yap</dt>
	<dd>Yankee Patent. The same 11 bets as a Yankee, but with singles on each of the 4 selections as well, making 15 bets in all (also known as a 'Lucky 15').</dd>
	<dt>Yearling</dt>
	<dd>A horse in its second calendar year of life, beginning January 1 of the year following its birth.</dd>
	<dt>Yielding</dt>
	<dd>Condition of a turf course with a great deal of moisture. Horses sink into it noticeably.<br /><br />

<dl>
</div>   