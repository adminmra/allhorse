<div class="lnkDrop" ><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq4');"> What information do I need to open an account?</a></div>
<div class="faqs lnkDropItem" id="faq4">
You will need to provide your basic personal and mailing address to open an account. For your convenience to be able to play right away, you can make your deposit immediately.<br><br> However, if you deposit via credit card,  a signed credit card authorization form and a photo ID will be needed.  It is best to have them submitted before your payout request so you can receive your winnings without delay. You also should confirm with your bank or credit card company that your card is approved for foreign transactions.
</div>

<a name="promocode"></a>

<div class="lnkDrop" ><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq5');">Why do you need my ID?</a></div>
<div class="faqs lnkDropItem" id="faq5">
Your financial security is absolutely paramount. With this in formation we can validate that:
<ul><li>You are at least 18 years of age.</li>
	<li>You are the true owner of the credit card (KYC, Know Your Customer).</li>
	<li>Your winnings go to the correct person: <strong>you.</strong></li>
	
</ul>
This is standard industry practice for everybody's safety.  Find out more about why at <a href="http://www.sportsbookreview.com/sbr-news/online-sportsbooks-kyc-policies-2015-edition-59739/" target="_blank" rel="nofollow">SB Review</a>.
</div>
<div class="lnkDrop" ><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq6');"> What if I don't have a credit card.</a></div>
<div class="faqs lnkDropItem" id="faq6">
Several alternate payment methods available including  Bitcoin, MoneyGram and eCheck!
Please  call  1-844-BET-HORSES to speak to one of our representatives to see which one is best for you. </div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq14');">I have a Promo Code.  Where do enter it to get my bonus?</a></div>
<div class="faqs lnkDropItem" id="faq14" >
The area to put the Promo Code in is in the deposit section of the cashier.  Promo codes do expire quickly so please use it as soon as possible.  If you have any questions, please contact Customer Service. </div>


<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq8');"> I know a lot of places can take weeks, even months to pay out. How long does it take BUSR?</a></div>
<div class="faqs lnkDropItem" id="faq8">
If you have your KYC documents in place, payouts are processed in <strong>just 2 business days!</strong> We are happy to pay the fastest in the industry!  Once processed, standard transfer times will apply depending on payout method (more information can be found in the Payouts section of the website).  
</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq9');"> Is there a fee for my payout?</a></div>
<div class="faqs lnkDropItem" id="faq9">
Yes, there is a nominal charge that will vary depending on payout of choice.  This fee ensures the integrity and speed of our payouts so you can get your winnings as fast as possible and far faster than industry average.
</div>



<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq11');"> Will my winnings be taxed?</a></div>
<div class="faqs lnkDropItem" id="faq11">
It is up to the members to represent to their tax authorities their own winnings/losses.  BUSR will not provide the account activity of its members to any tax authority unless required by recognized law. </div>
<div class="lnkDrop" id="remove"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq12');"> Does BUSR offer rebates or bonuses?</a></div>
<div class="faqs lnkDropItem" id="faq12">
Yes, there is up to an 8% daily rebate on horse racing depending on the specific type of wager and the racetrack.  First deposits at BUSR are eligible for various cash bonuses and promotions. Please check our are weekly cash specials and <a href="/cash-bonus">horse racing bonuses</a> offered to BUSR members. </div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq13');"> Does BUSR have live video?</a></div>
<div class="faqs lnkDropItem" id="faq13">
Yes, we are excited to announce that we have live video. Make a deposit, place your bet and you can watch your bet live from either your computer or mobile device.
</div>

<!-----------------------------------  WAGERING ANSWERS ------------------------------->
<br />
<h3>WAGERING ANSWERS</h3>


<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq101');"> How do I place a bet? </a></div>
<div class="faqs lnkDropItem" id="faq101">
BUSR account holders can wager online at using a computer, mobile device or tablet.   Telephone wagering is also available. </div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq102');"> What are the minimum and maximum limitations on wagering amounts? </a></div>
<div class="faqs lnkDropItem" id="faq102">
BUSR's wagering system accepts wager amounts typically ranging from $1.00 to $1,000.00 per wager, subject to host track restrictions. In the case of multiple wagers, such as a Daily Double or Exacta box, the base wager is multiplied by the number of combinations to determine the amount of the total wager.  If a customer wishes to place more than $1,000 on a specific wager, then multiple bets can be made for the same horse at the same racetrack.  Please see the horse betting rules section in the Clubhouse for details. </div>


<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq103');"> When can I bet on the races? </a></div>
<div class="faqs lnkDropItem" id="faq103">
You can place wagers 24x7 for all of the day's races. Besides popular tracks like Churchill Downs, Saratoga and Santa Anita, BUSR offers the most international racetracks of any racebook.  Bet on international races in Hong Kong, Australia, UK, France, Ireland and Japan!  Wagers can be placed up to post time of each race.  For special events like the Kentucky Derby, BUSR will provide advance wagering as well as wagering on the day of the racing event. </div>


<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq104');"> What wagering choices do I have?</a></div>
<div class="faqs lnkDropItem" id="faq104">
The full menu of wagering options are typically you would have available at the racetrack.  BUSR currently offers more choices and types of wagers on horses than any other online ADW. </div>


<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq105');"> When do I get confirmation that my wager has been accepted?</a></div>
<div class="faqs lnkDropItem" id="faq105">
Normally, within 5 seconds after the wager has been accepted. Your betting ticket and confirmation number serves as evidence of your wager being successfully accepted. Your receipt of the confirmation may be slowed due to site volume, internet traffic, or connection speed for your PC.
</div>



<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq107');"> How are the wagers processed?</a></div>
<div class="faqs lnkDropItem" id="faq107">
All wagers are processed at our gaming provider's licensed hub system for each particular race.
</div>


<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq109');"> How can I access my account information?</a></div>
<div class="faqs lnkDropItem" id="faq109">
Access your account history using BUSR's "My Account" area by by selecting the "My Account" page.  Your Account Balance is always in the top right hand column of your screen. 
</div>



<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq112');"> Are winners paid full-track odds?</a></div>
<div class="faqs lnkDropItem" id="faq112">
Full track odds are paid on most popular wagers although there are maximum payouts for certain types of wagers.  Please see the Rules section for more details.
</div>


<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq113');"> How do I collect my winnings?</a></div>
<div class="faqs lnkDropItem" id="faq113">
Your winnings are automatically credited to your account as soon as the race is declared "official" and results are posted.  This may take a few minutes on higher volume races such as the Kentucky Derby. Payouts cans be requested through your account online or by calling 1-844-BET-HORSES.
</div>


<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq115');"> How do I make a withdrawal from my account?</a></div>
<div class="faqs lnkDropItem" id="faq115">
<p><strong>To make a withdrawal from your account, login and follow these steps:</strong></p>

<p><strong>1.</strong> Select the "Depost" tab followed by &quot;Payout&quot;.<br />
  <strong>2.</strong> Enter the $ amount you want to request followed by selecting the payout method.<br />
  <strong>3.</strong> Additional infomation may be required prior to be processed.</p>

<p>All payouts will be subject to a 2 business day review timeframe. Once processed, the payout timeframe can range from 1 - 7 business days depending on the method you choose. </p>

</div>