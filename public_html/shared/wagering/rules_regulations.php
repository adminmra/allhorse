<?php

if ( strpos($_SERVER["HTTP_HOST"], 'allhorseracing') !== false ) {
    $host = "All Horse Racing";
}elseif ( strpos($_SERVER["HTTP_HOST"], 'gohorsebetting') !== false ) {
    $host = "Go Horse Betting";
}

?>
<style type="text/css">
    p{padding-bottom: 20px;}
    h3{font-weight: 800;
        padding-bottom: 20px;}
    h1 a{
        font-size: 35px !important;
    }    
    h1{
        text-align: center;
        line-height: 60px;
    }

    .terms-conditions ol{
        list-style-type: decimal;
        list-style: decimal;
        list-style-type: decimal;
        margin-top: 1em;
        margin-bottom: 1em;
        margin-left: 0;
        margin-right: 0;
        padding-left: 20px;
    }
    
    .terms-conditions ul{
        margin-top: 1em;
        margin-bottom: 1em;
        margin-left: 0;
        margin-right: 0;
        padding-left: 20px;
    }

    .terms-conditions ol li{
        list-style: decimal;
        list-style-type: decimal;
        padding-bottom: 10px;
        color: #000;
        font-family: 'Open Sans', Arial, sans-serif;
    }

    .terms-conditions ul li{
        list-style: disc;
        padding-bottom: 10px;
        color: #000;
        font-family: 'Open Sans', Arial, sans-serif;
    }
    .skiptext{
        padding-bottom: 0px;
    }
     h3#kw{
        padding-bottom: 0px
    }

</style>

<h3>General House Rules</h3>
<ol >
<li>All rules and regulations are subject to revision by <?php echo $host; ?> without prior written notice to <?php echo $host; ?> members.</li>

<li>The management of <?php echo $host; ?> reserves the right to refuse or limit any bet and to restrict betting on any event at any time without any advance notice.</li>

<li>Any service that <?php echo $host; ?> offers, is intended for recreational players only. We reserve the exclusive right to refuse, limit or close any client account if we find that it is being used otherwise.</li>

<li><?php echo $host; ?> permits only one (1) account per person, household, IP address and shared computer environment such as public library.</li>

<li>You must be not less than 18 years of age and of legal age in your jurisdiction to use our services.</li>

<li>You must have funds in your account to place a wager. <?php echo $host; ?> does not offer credit at anytime under any circumstance. Proof of payment must be made before <?php echo $host; ?> credits your account balance. Wagers will not be accepted if you do not have funds in your account balance.</li>

<li>If you have a claim on a bet, you must contact us no later than (1) one week from the day the disputed bet was placed. Claims will not be honored after this period.</li>

<li>Claims on bets will normally be solved within 24 hours of having received the complaint.</li>

<li>All wagers must be placed and accepted before the start of the race. Any bet placed after the event has started will be void unless approved by a manager.</li>

<li><?php echo $host; ?> reserves the right to close any account or group of accounts if they are suspected of fraud and or abuse of our bonuses program. In the event an account or group of accounts are found within the <?php echo $host; ?> system, where the customers have conspired to fraudulently or maliciously manipulate our race offerings to guarantee themselves profits regardless of the outcome of the events, <?php echo $host; ?> reserves the right to cancel any or all wagers and profits derived from such fraudulent activity as well as terminate the accounts in question permanently.</li>

<li><?php echo $host; ?>'s management reserves the right to refuse or limit the maximum amount allowed on any wager prior to acceptance of such wager. Members are not permitted to open multiple accounts either in their name or someone else's in order to circumvent the limits imposed by our management. If multiple accounts are used all bets will be void and profits will be reversed.</li>

<li>All horse wagers must be placed two (2) minutes prior to the start of the race.</li>

<li><?php echo $host; ?> reserves the right to suspend any of its products at any time. When a product is suspended, any wagers entered will be rejected. <?php echo $host; ?> also reserves the right to cease wagering on any of its products indefinitely at any time without prior notice.</li>

<li>All minimums, maximums, and betting payout prices are subject to change without prior written notice.</li>

<li>If funds are credited to your account in error, it is incumbent upon you to notify <?php echo $host; ?> of this mistake without delay. For accounts with negative balances, <?php echo $host; ?> reserves the right to cancel any pending wagers, whether placed with funds resulting from the error or not.</li>

<li>All winnings will be credited to the customer's account. Any winnings credited to an account in error are not available for use, and <?php echo $host; ?> reserves the right to void any transactions involving such funds, either at the time or retrospectively.</li>

<li><?php echo $host; ?> reserves the right to suspend any account based on abusive behavior from the customer towards any staff member of <?php echo $host; ?>. The account may be re-opened after a warning has been issued to the client, however if such abusive behavior continues management reserves the right to terminate the account permanently.</li>

<li>House Rules will supersede at all times any verbal or written offering of any staff member of <?php echo $host; ?>.</li>
</ol>
<br/>

<h3>Bets Placed Over the Phone</h3>
<ol>
<li>When calling <?php echo $host; ?> the customer must give their personal account number and their password before placing a wager or gaining access to their account information.</li>

<li>All conversations will be recorded in case they are needed in the future to settle any discrepancies during a phone call. The recording will be listened to to settle any disputes and after reviewing the recording the wagers will be adjusted accordingly.</li>

<li>When receiving the final read back on a phone call from the customer service operator, it is up to the customer to listen carefully and verify that the wager(s) was read back correctly.</li>

<li>Once the customer has confirmed the read back all wagers will be confirmed.</li>

<li>If a dollar amount has been placed on a wager and the phone call is disconnected or lost, it is up to the customer to call back. The customer will have to call back before post time to assure that the wager was accepted and placed correctly.</li>

<li>All actions will be carried out and completed if the correct username and password is provided.</li>
</ol>
<br/>

<h3>Bets placed over the Internet</h3>
<ol>

<li>The Customer must log in using their Personal Account Number or email and password to wager or access their account information.</li>

<li>It is the Customer's responsibility to keep their password secure. All wagers placed in the account will be binding. Any unauthorized use of an account is the sole responsibility of the account holder and <?php echo $host; ?> cannot be held liable.</li>

<li>It is the Customer's responsibility to review the wagers before submitting them, and before logging out to make sure all bets were placed correctly. All wagers placed over the internet are final, and cannot be changed or deleted.</li>

<li>Minimum wager on horse racing events is $1 depending on the track.</li>
</ol>
<br/>

<h3>Horse Wagering</h3>

<p><?php echo $host; ?> offers daily horse racing from various tracks around the world, you may call our wagering line to place your bets or if you prefer you may access our online racebook.</p>

<p class="skiptext">All horse wagers will follow the rules detailed here:</p>
<ol>
<li>A time stamp will be placed on all horse wagers and wagers must be placed 2 minutes prior to the start of the race.</li>

<li>Bets will not be accepted after post time. In the event the race starts before the advertised post time and for that reason the customers are able to place wagers after the race has started all wagers are void.</li>

<li><?php echo $host; ?> will not be liable for wagers that were unsuccessfully entered before post time. We recommend that our clients check their pending wagers before login out of their accounts to ensure the wagers were placed correctly.</li>

<li>All wagers are final once they have been confirmed and submitted.</li>

<li>We reserve the right, at our discretion, to change, modify, add, or remove customers from our rebate programs at any time.</li>
<li><?php echo $host; ?> is a non-pari-mutuel horse betting website. We do not participate directly in the track's pari-mutuel or tote. Straight wagers are paid full track odds and exotic wagers are paid full track odds up to a fixed-odds max amount.</li>
</ol>
<p>Win, Place, Show wagers pay full track odds. All Exotic wagers pay full track odds up to the maximum pay-outs as shown in the table below:</p>
<h2><em><?php echo $host; ?></em> Track Payouts</h2>
<div class="table-responsive mt-5">
    <table class="table table-condensed table-striped table-bordered" width="100%" cellpadding="0"
        cellspacing="0" border="0">
        <thead>
            <tr>
                <th>Bet Type</th>
                <th>Class A</th>
                <th>Class B</th>
                <th>Class C</th>
                <th>Class D</th>
                <th>Class E</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Bet Type"><strong>Win</strong></td>
                <td data-title="Class A">Track Odds</td>
                <td data-title="Class B">Track Odds</td>
                <td data-title="Class C">Track Odds</td>
                <td data-title="Class D">Track Odds</td>
                <td data-title="Class E">Track Odds</td>
            </tr>
            <tr>
                <td data-title="Bet Type"><strong>Place</strong></td>
                <td data-title="Class A">Track Odds</td>
                <td data-title="Class B">Track Odds</td>
                <td data-title="Class C">Track Odds</td>
                <td data-title="Class D">Track Odds</td>
                <td data-title="Class E">Track Odds</td>
            </tr>
            <tr>
                <td data-title="Bet Type"><strong>Show</strong></td>
                <td data-title="Class A">Track Odds</td>
                <td data-title="Class B">Track Odds</td>
                <td data-title="Class C">Track Odds</td>
                <td data-title="Class D">Track Odds</td>
                <td data-title="Class E">Track Odds</td>
            </tr>
            <tr>
                <td data-title="Bet Type"><strong>Exacta</strong></td>
                <td data-title="Class A">500-1</td>
                <td data-title="Class B">400-1</td>
                <td data-title="Class C">300-1</td>
                <td data-title="Class D">300-1</td>
                <td data-title="Class E">300-1</td>
            </tr>
            <tr>
                <td data-title="Bet Type"><strong>Quinella</strong></td>
                <td data-title="Class A">400-1</td>
                <td data-title="Class B">300-1</td>
                <td data-title="Class C">300-1</td>
                <td data-title="Class D">300-1</td>
                <td data-title="Class E">300-1</td>
            </tr>
            <tr>
                <td data-title="Bet Type"><strong>Trifecta</strong></td>
                <td data-title="Class A">1000-1</td>
                <td data-title="Class B">750-1</td>
                <td data-title="Class C">300-1</td>
                <td data-title="Class D">300-1</td>
                <td data-title="Class E">300-1</td>
            </tr>
            <tr>
                <td data-title="Bet Type"><strong>Superfecta</strong></td>
                <td data-title="Class A">1500-1</td>
                <td data-title="Class B">1000-1</td>
                <td data-title="Class C">300-1</td>
                <td data-title="Class D">300-1</td>
                <td data-title="Class E">300-1</td>
            </tr>
            <tr>
                <td data-title="Bet Type"><strong>Pick 3</strong></td>
                <td data-title="Class A">1500-1</td>
                <td data-title="Class B">1000-1</td>
                <td data-title="Class C">300-1</td>
                <td data-title="Class D">300-1</td>
                <td data-title="Class E">300-1</td>
            </tr>
            <tr>
                <td data-title="Bet Type"><strong>Pick 4</strong></td>
                <td data-title="Class A">5000-1</td>
                <td data-title="Class B">2000-1</td>
                <td data-title="Class C">300-1</td>
                <td data-title="Class D">300-1</td>
                <td data-title="Class E">300-1</td>
            </tr>
            <tr>
                <td data-title="Bet Type"><strong>Daily Double</strong></td>
                <td data-title="Class A">750-1</td>
                <td data-title="Class B">400-1</td>
                <td data-title="Class C">300-1</td>
                <td data-title="Class D">300-1</td>
                <td data-title="Class E">300-1</td>
            </tr>
            <tr>
                <td data-title="Bet Type"><strong>Maximum Payout</strong></td>
                <td data-title="Class A">$20,000.00</td>
                <td data-title="Class B">$15,000.00</td>
                <td data-title="Class C">$10,000.00</td>
                <td data-title="Class D">$3,000.00</td>
                <td data-title="Class E">$1,000</td>
            </tr>
        </tbody>
    </table>
</div>
<h2>Track Categories</h2>
<?php include_once("/home/ah/allhorse/public_html/shared/rebates-track-categories.php") ?>
<br/>

<h3>Rules regarding criminal activity, fraud, or collusion.</h3>
<ol>
<li>If reasonable cause to believe that you are involved in any type of criminal activity, fraud, or collusion, we reserve the right to suspend your account pending review. We also reserve the right to freeze your account balance and/or close your account permanently. We reserve the right to deduct from your account the amount of any payouts, bonuses or winnings due to activities which include:</li>
<li>Being linked to any form of collusion, cheating, unfair or deceitful practices, or any other criminal activity.</li>
<li>On bets placed that you have maliciously gained an unfair advantage on bets using collusive or deceitful practices.</li>
<li>That you have been found to engage in deceitful or collusive behaviors at other online gaming businesses including charge backs, or any criminal activities that we deem harmful to our establishment.</li>
<li>We become aware that you have “charged back” or denied any purchases or deposits that you make to your account.</li>
<li>Fraudulent practice is defined as any activity using stolen, cloned or otherwise illegal credit or debit cards to fund an account</li>
<li>Criminal activity is defined as money laundering and/or any other illegal activity</li>
<li>Unfair advantage is defined as the abuse of a fault, loophole, or error in our software. This also includes the use of computers known as ‘bots' (Robots, or Spiders).</li>
<li>Collusion is defined as the use of multiple accounts to gain an unfair advantage as defined in these rules and limits per account. This includes the use of beards and movers as a way to place wagers.</li>
<li>We will conduct a thorough review of an account in which we believe collusion, cheating, fraud, or illegal activities have taken place.</li>
<li>We reserve the right to inform our business units, processing companies, electronic payment providers or other financial institutions of any unlawful, fraudulent or inappropriate activity.</li>
<li>Privileged information that arises from any direct or indirect relationship with a recognized racing steward, jockey, trainer, breeder or owner is considered an unfair advantage as this is not public knowledge.  Any winnings which are the result of such privileged information shall be voided and, in Management's decision, the customer's account may be closed.</li>
</ol>
<br/>

<h3>Rules Regarding Bonuses</h3>
<ol>
<li>Bonuses at <?php echo $host; ?> are all subject to the following rules and regulations. Promotions and bonuses are restricted to one per deposit, per person, account, household, email, telephone number; same payment account and shared computer such as school, public library or workplace. No exceptions are made to this rule.</li>
<li>All promotions are for recreational players only, <?php echo $host; ?> reserves the right to refute or reverse any bonus given to any customer at any time if we determined that the customer is not eligible to take part in such promotion or was found to have abused our bonus and promotion program. Management reserves the right to refute or retract promotions at any time to any customer regardless of the situation.</li>
<li>If a member has numerous accounts, all promotional bonuses or credits achieved and winnings produced from the account will be void and subtracted from the account.</li>
<li>Any customer found to be part of a syndicate or gambling organization as well as those who misuse our bonus and promotions program will have all promotions cancelled and all winnings from them will be forfeited and cancelled.</li>
<li>Bonuses are only offered on new money deposited to an account, if a client has recently withdrawn funds from his account, the client will be required to make up the total amount of the payout in deposits before being eligible for a bonus.</li>
<li>Bonuses may not be credited automatically; you must contact Customer Service upon having made a deposit in order to receive any bonuses or promotions.</li>
<li>Once a bonus has been received it cannot be forfeited by the customer and all terms and conditions regarding bonuses must be followed before any payouts are issued.</li>
<li>For rollover determinations the lesser amount between the risk and the win on each eligible wager is the one that is measured.</li>
<li>Management reserves the right to refute or retract any bonus at any time to any customer it believes is not eligible to receive such promotions.
<li>Rollover means you must wager the quantified amount at least that many times before requesting a payout. To calculate rollover for example, if you deposit $100 and receive a 10% ($10) Re-Up Bonus with a 3 times rollover prerequisite, you would only need an amount of $330 ($110 of the deposit plus the bonus amount given x 3) in action prior to requesting for a payout.</li>
<li>Once you have received a bonus, you cannot request a payout before the rollover obligation is met.</li>
<li>Rollover calculations start from the moment the first wager is placed after a deposit has been made unless otherwise specified in the bonus terms and conditions.</li>
<li>Rollover calculations do not include any pending wagers, only wagers that are settled and have a status of win or loss are tallied in the rollover report.</li>
<li>Wagers that result in a termination due to postponed events or the wager resulting in a push or no action do not count towards the rollover.</li>
<li>In the event that a customer funds his account while still having a balance from a previous deposit where a bonus was issued, the client must then finish all rollover and holding periods that are required by each of these deposits before a withdrawal can be made.</li>
<li>Any customer who is given a promotion including no deposit promotions, contest winnings from affiliates or sponsor sites must comply with all terms and regulations of such promotion before any withdrawal can be made.</li>
<li>Promotions cannot be used with other bonuses or promotions.</li>
<li>All promotional offerings and policies are solely at the discretion of <?php echo $host; ?> and are subject to annulment or alteration without warning or prior notice.</li>
<li><?php echo $host; ?> reserves the right to remove the availability of any and all offers to any customer at any time.</li>
<li>These Bonus and Promotions rules will supplant any verbal or written offering of any employee of <?php echo $host; ?>.</li>
</ol>
<br/>

<h3>Cash Bonus Rules</h3>

<p>We offer cash bonuses on select promotions from time to time, these promotions have a limited time offering to qualified clients.</p>
<p>All rollover requirements for cash bonuses must be met by wagering on horse racing or greyhound unless expressly stated in the bonus rules.  As with all bonuses and promotions offered by <?php echo $host; ?> all rollover and play through periods must be accomplished before any withdrawal can be made.</p>
<br/>

<h3>Rollover Calculation:</h3>
<p class="skiptext">The formula for the rollover calculation for free play, match play or cash bonuses at <?php echo $host; ?> is as follows:</p>
<ul>
<li>Formula: The original deposit plus the transfer fees (when applicable), plus bonus amount given, multiplied by the definite rollover requirement of the bonus.</li>

<li>Significant Notice: Maximum winnings permissible from any wager made with a bonus are five thousand US dollars and any amount won above this figure will be overturned. All rollover and hold periods must be accomplished before a payout can be made.</li>
</ul>
<br/>

<h3>Referral Bonus Rules</h3>

<p>Please read the following rules as they pertain to all referral bonuses given by <?php echo $host; ?></p>

<ol>
<li>Referral bonus requires a 3X rollover requirement before receiving a payout.</li>

<li>Both accounts must have all documents on file before a referral is awarded.</li>

<li>Referrer must have made deposit within last 30 days to receive the referral bonus.</li>

<li>Referrer must request bonus within 72 hours of time of referees initial deposit.</li>

<li>Each account must reside at a different address.</li>

<li>Referrer and referee has no negative transaction history.</li>

<li>Referrer and referee must not engage in syndicate wagering or bonus abuse by playing both sides of same game or line.</li>

<li>Referrer has not received a payout in the past 180 days.</li>

<li>Referrer and referee accounts will be subject to review prior to payout approval.</li>
</ol>
<br/>

<h3>Rules Regarding Specific Offers</h3>

<p>Please login to the Club House to see the terms and conditions for all promotions and bonuses.</p>

<h3>Deposits Terms</h3>

<ol>
<li>All deposits are subject to management approval, <?php echo $host; ?> reserves the right to accept, deny or reverse any deposit.</li>
<li>The use of credit cards and other electronic payment methods is restricted to one account, meaning the same credit card number, bank account cannot be used by more than one client.</li>
<li>Credit cards must be in the name registered in the account with <?php echo $host; ?>. In the event that the customer wishes to use a card that is not in their name, the deposit must first be approved by management and proper documentation of card must be on file prior to the deposit being made.</li>
<li>Deposit limits vary depending on the method used, any limit increase must be approved by management and <?php echo $host; ?> reserves the right to request sufficient documentation from any customer to validate the identity of the client before any limits are increased. Providing such documentation will not guarantee any increase in the deposit limits.</li>
<li>Deposit options may vary depending on your location, for a complete list of deposit options please contact our Customer Service team at 1-844-GHB-BETS (1-844-442-2387) or via email at support@myracingaccount.com.</li>
<li>Any customer found to be committing fraud of any kind with credit cards or any other electronic payment method will be subject to criminal and/or civil prosecution.</li>
<li>All winnings resulting from the illicit use of credit cards or any other electronic payment method will be automatically forfeited.</li>
<li>The descriptor on your credit card billing statement may vary from one transaction to the next as we use different processing companies. If you are unsure about a charge reflected on your statement please contact our security department to inquire about the charge cashier@myracingaccount.com.</li>
<li>Credit card deposits are subject to a 30 business day hold before any payout can be processed and proper documentation will be required.</li>
<li>Currently the default currency in our cashier is USD, if your account is in a different currency you must contact our customer service department prior to any deposit to receive the proper instructions.</li>
<li>Deposits that are not processed through our cashier will not be credited automatically; it is the responsibility of the customer to contact our customer service department in order to inform them of the deposit to ensure it is credited in a timely manner.</li>
<li><?php echo $host; ?> is not responsible for deposits not notified to its customer service department.</li>
<li>In the event funds are credited to a customer's account in error, it is incumbent upon the customer to notify <?php echo $host; ?> of aforesaid error without delay. For accounts with negative balances, <?php echo $host; ?> reserves the right to cancel any pending plays, whether placed with funds resulting from the error or not.</li>
<li>In the event of delays due to transaction errors or third party processing delays <?php echo $host; ?> cannot not be held liable for any wagers missed due to the delay.</li>
<li>Fees will be covered on person to person transfers of $300 or more.</li>
<li><?php echo $host; ?> will not be liable for any additional fees charged by the members banking institution such as but no limited to; cash advance fees, international processing or currency fees, over draft fees or administrative banking fees.</li>
<li><?php echo $host; ?> relies on the service of third party processors and cannot be held liable for any failure on their part.</li>
<li>The availability of deposit methods are subject to change without prior notice.</li>
<li>The house deposit rules will supersede any verbal or written offering of any employee of <?php echo $host; ?>.</li>
<li>Online horse betting may be restricted or not permitted in some jurisdictions. It is the sole responsibility of each customer to inform themselves of their local laws before funding their accounts.</li>
</ol>
<br/>

<h3>How To Fund Your <?php echo $host; ?> Account</h3>

<ol>
<li>There are several easy deposit methods to fund your account. Please feel free to contact us anytime at 1-844-GHB-BETS (1-844-442-2387) should you need any help with your deposits.</li>
<li>If you are sending a Person-to-Person money transfer, you will be charged a fee to send funds. We'll cover the transfer fees on applicable deposits.</li>
<li>If you are using your credit or debit card, please read the deposit policies carefully, as there are certain requirements that you must complete prior to requesting payout from your account, including a copy of a valid photo id, copies of the front and back of each card used, and a signed verification form.</li>
<li>From time to time, some credit card companies will charge a foreign transaction fee when you make a deposit to your <?php echo $host; ?> account.  If you are charged a foreign transition fee, simply make a screen shot and email it to support@myracingaccount.com and the foreign transaction charges will be credited to your racing account.</li>
<li>Gambling may be restricted in some jurisdictions and may not be permitted where you live. It is your responsibility to know and comply with any local laws before funding your account.</li>
</ol>
<br/>

<h3>Person to Person Money Transfers</h3>

<p>You can deposit to your account by sending a person-to-person cash transfer at a local outlet near you.</p>

<p class="skiptext">Here are the easy steps to send a money transfer:</p>

<ol>
<li>Contact the Customer Service Team to get the transfer details.</li>
<li>Go to a local money transfer outlet to pay for and send your transfer.</li>
<li>Call us back to give your confirmation number and the funds will be posted to your account in as little as 15 minutes</li>
</ol>

<p class="skiptext">Please note:</p>
<ul>
<li>The minimum deposit is $100 and the maximum is $5000, per transaction (see limits below).</li>
<li>When depositing via this method, you will be charged a fee for the money transfer; however, we will reimburse the transfer fees for deposits above $300 USD</li>
<li>It may take up to 15 minutes for the funds to reflect in your account balance after you provided us with the confirmation number.</li>
<li>Fees covered on deposits of $300 or more.</li>
<li>Min: $100 USD</li>
<li>Max: $800 USD</li>
<li>For details on sending a larger person to person money transfer amount please contact customer service.<br/>
</li>
</ul>
<h3>Deposits Made With Your Credit Card</h3>

<p>You can deposit money into your account using any Visa or MasterCard branded credit card or check/debit card. Please ensure the card is eligible for international purchases by calling your credit card company to request that you can make foreign purchases with your credit/debit card.</p>

<p class="skiptext">When you request a payout, you may need to complete one Credit Card Verification form for each credit card you use to deposit funds along with the following documents:</p>
<ul>
<li>A photocopy of your government issued photo ID (e.g., your driver's license).</li>
<li>Photocopies of the front and back of your credit card.</li>
</ul>

<p>Please Note:</p>
<ul>
<li>When you complete a credit card deposit, the descriptor for this transaction will appear on your credit card statement. Please take a moment to take note of the descriptor for your records.</li>
<li>It may take up to 10 business days for the transaction to show up as a completed debit on your credit card statement. It may show as a pending “Pre-Authorization” until the transaction has been debited by us. A pre-authorization is a temporary “hold” on your card's available balance for the deposit amount you have made. This is done for any deposit attempts that are started but have yet to be completed and can often apply to failed deposit attempts. This is very common for electronic (online) transactions. Once payment is completed, the “hold” will be replaced by an actual charge to your card. If the transaction failed initially, the pre-authorized funds will become available in your card's balance.</li>
<li>Many credit and debit cards have restrictions on online purchases or for use on horse betting websites. If your card issuer restricts use on our site, we would suggest viewing alternate deposit options that may be convenient to you.</li>
<li>Deposits you make within a few days of one another may be deducted from your balance on the same day. While these may appear as ‘duplicated' deposits, we ask that you carefully review your statement or contact us for clarification as this is most likely not the case.</li>
<li>If you should require help with reconciling your credit card statement, do not recognize a transaction that is appearing or feel that you may have been over or under charged, please contact us at 1-844-GHB-BETS (1-844-442-2387). We are happy to help you and want to make sure everything is in order.</li>
<li>Remember that before you receive a payout, you are may need to provide a completed and signed Credit Card Verification Form. If you want to avoid unnecessary delays, we encourage you to send in any documents promptly.</li>
<li>If you are depositing using a Visa credit card, you might be prompted to enter the Verified By Visa password associated with your card.</li>
<li>If you need further assistance, please feel free to call us toll free 1-844-GHB-BETS (1-844-442-2387).</li>
</ul>
<br/>

<h3>Withdrawal Rules</h3>

<p>Payouts are processed daily on a first come, first served basis. As long as all pre-authorized or pending deposits to your account have settled we can process your payout in as little as 24 hours. Make sure that you have a sufficient balance in your account to cover your payout amount until the funds are removed from your balance, to avoid the payout from being declined. After a payout has been processed and the funds have been debited from your <?php echo $host; ?> account, the transfer times and fees can be found below.</p>

<ol>
<li>Payout requests are accepted Monday through Sunday between the hours of 8:00 AM and 2:00 PM EST. Payouts will be sent Monday through Friday during normal banking hours.</li>
<li>Withdrawals will only be processed in the name of the account holder, in the event that a client cannot receive the funds in his/her name a written signed authorization must be received with the name of the person to whom the payout is endorsed, in addition to this a valid photo id of the account holder must accompany the authorization form.</li>
<li>The minimum withdrawal amount is $100 USD.  Minimum withdraw amount may be less than $100 USD if by bitcoin.  Please email or call for details.</li>
<li>All withdrawal requests must be placed directly with a customer service representative by phone. Email withdrawal requests will not be honored.</li>
<li>Customers must provide account number and password when requesting a payout.</li>
<li>All terms and conditions including rollovers, play through periods and required documentation must be completed and received before a payout can be processed.</li>
<li>All payout requests have a processing period of a minimum of 48 hrs. The submission of a request by a representative on behalf of the customer does not guarantee approval.</li>
<li>All payouts are subject to the funds being available in the account at the time the debit is made; if funds are not available, the payout request will be cancelled.</li>
<li><?php echo $host; ?> reserves the right to request proper documentation to verify the account holder's identity, including a valid photo id and copies of credit cards before processing a payout. Failure to provide this documentation will result in the cancellation of the request.</li>
<li>Customers are responsible for ensuring that all personal information including physical mailing address, phone number and email address are updated and correct, in the event that a redirect or re-send of a payment due to an incorrect address is necessary, there is a administrative fee of $50 USD</li>
<li>Processing of person to person transfer requests may take up to 48 to 72 hours depending on processor availability.</li>
<li>Although we strive to process payouts as quickly as possible, some methods may take longer to process due to transfer procedures and any payout may take up to 10 to 14 business days to be processed.</li>
<li>Depending on the deposit method and/or bonuses received, some payout requests may be subject to additional holding periods of up to 30 days.</li>
<li>Payouts may be sent via another method than the one originally requested on certain occasions, we will notify you of any changes should they be necessary.</li>
<li>Payout requests may be cancelled or modified prior processing, once processed however changes are not allowed, in the event that a payout is cancelled at the customer's request, any applicable fees will not be reimbursed.</li>
<li>Withdrawal methods, limits and fees are subject to change without prior notification.</li>
<li>Non cashable bonuses will be reversed at the time of a withdrawal.</li>
<li><?php echo $host; ?> reserves the right to deny, reverse or hold any withdrawal request if fraud is suspected or if terms and conditions are violated in any way.</li>
<li>Withdrawal rules will have precedence over any verbal or written offering by any employee of the company.</li>
<li><?php echo $host; ?> reserves the right, in our sole discretion, to terminate or modify these rules at any time without notice.</li>
</ol>

<p class="skiptext">Additionally:</p>
<ol>
<li>If you have pending deposits to your account, we cannot process your payout until we receive the funds. This may take up to 5 business days after you make a deposit.</li>
<li>We can only process one payout request at a time. If you want to request another payout, please cancel the one that is pending.</li>
<li>Make sure that your address and account information is correct before making a payout request.</li>
<li>You may be required to submit additional information for account verification.</li>
<li>All rollover requirements must be met in order to process a payout, fees are subject to change without prior notice however we will take all precautions to ensure you are informed of any change in the fee structure.</li>
</ol>

<div class="table-responsive mt-5">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Method</th>
            <th scope="col">Amount Available</th>
            <th scope="col">Delivery Time</th>
            <th scope="col">Fee</th>
          </tr>
        </thead>
        <tbody>
<tr>
<td>Bitcoin</td>
<td>$25 – $5000</td>
<td>2 business days</td>
<td>FREE</td>
</tr>
<tr>
<td>E-Check</td>
<td>$50-$200</td>
<td>7 to 10 business days</td>
<td>$5</td>
</tr>
<tr>
<td></td>
<td>$201-$500</td>
<td>7 to 10 business days</td>
<td>$15</td>
</tr>
<tr>
<td>Bank Wire</td>
<td>$500-$5000</td>
<td>5 to 7 business days</td>
<td>$35</td>
</tr>	
        </tbody>
      </table>
    </div>