<?php

if ( strpos($_SERVER["HTTP_HOST"], 'allhorseracing') !== false ) {
    $host = "All Horse Racing";
	$phno='1-844-777-BETS (1-844-777-2387)';
}elseif ( strpos($_SERVER["HTTP_HOST"], 'gohorsebetting') !== false ) {
    $host = "Go Horse Betting";
	$phno='1-844-GHB-BETS (1-844-442-2387)';
}

?>
<style type="text/css">
    p{padding-bottom: 20px;}
    h3{font-weight: 800;
        padding-bottom: 20px;}
    .skiptext{
        padding-bottom: 0px;
    }
     h3#kw{
        padding-bottom: 0px
    }
    .ghb-help ul li{
        list-style: disc;
        padding-bottom: 10px;
        color: #000;
        font-family: 'Open Sans', Arial, sans-serif;
    }

    .ghb-help ul{
        margin-top: 1em;
        margin-bottom: 1em;
        margin-left: 0;
        margin-right: 0;
        padding-left: 20px;
    }

    .ghb-help h2{
        font-weight: 800;
        padding-bottom: 20px;
    }
</style>

<h3 class="hr-rls-low">Help Centre</h3>
<ul>
<li><a href="#general">General Queries</a></li>
<li><a href="#deposits">Deposits</a></li>
<li><a href="#bonuses">Bonuses</a></li>
<li><a href="#payouts">Payouts</a></li>
<li><a href="#horswagering">Horse Wagering</a></li>

</ul>
<br/>

<a name="general"></a>
<h2>General Queries</h2>

<p><strong>Q: Who is <?php echo $host; ?>?</strong>  <?php echo $host; ?> is an international gaming company which provides online horse betting and greyhound betting. </p>

<p><strong>Q: Where do I find my Account Number and Password to log in?</strong> You will receive your Account Number and password as soon as you join and we will also send it in an email. If you don't see this in your inbox please be sure to check your junk folder.  Your account number will begin with the letters: GHB followed by a numeral.  You can also login with the EMAIL address that you used to open your account.</p>

<p><strong>Q: I've lost my password; how do I retrieve it?</strong>  If you've forgotten or lost your password you can retrieve it by contacting our Customer Service Team at <?php echo $phno; ?> where you will be asked the security question that you used when you created your account.  You can also reset your password on the website.</p>

<p><strong>Q: Where can I see my available balance?</strong>  After you log into your account you will be able to see you available balance on the navigation bar.</p>
<p><strong>Q: What racetracks can I bet on?</strong> </p>
 <p>We are excited to offer the following tracks available for wagering:</p>
 <?php include '/home/ah/allhorse/public_html/shared/rebates-track-categories-ghb.php'; ?>
<br/>

<a name="deposits"></a>
<h2>Deposits</h2>

<p><strong>Q: What are the costs to join?</strong>  Membership is free at <?php echo $host; ?>.</p>
<p><strong>Q: What credit cards do you accept?</strong>  We accept Visa and MasterCard as well as several other online options to fund your account.  Credit card deposits are instantaneous.  When making a withdraw or if you need to increase your daily or monthly deposit limits, you may need to fill out an Authorization Agreement.  This is required by <?php echo $host; ?> as part of KYC ("Know Your Customer") rule that banks and financial services demand.</p>
<p><strong>Q: My credit card was declined, why?</strong>  Your card could be declined for many reasons. Often your card may be blocked for international transactions. Please contact your credit card company and simply instruct them to allow your card to be used for international transactions. You can alternatively try any of our other simple and quick deposit methods in the cashier.</p>
<p class="skiptext"><strong>Q: I used my credit card to fund my account, what documents do you need for me to request a payout?</strong>  When you request a payout, you may need to complete one Credit Card Verification form for each credit card you use to deposit funds along with the following documents:</p>
<ul>
<li>A photocopy of your government issued photo ID (e.g., your driver's license).</li>
<li>Photocopies of the front and back of your credit card.</li>
</ul>

<p><strong>Q: In addition to credit cards, what other methods can I use to deposit money?</strong></p>

<p class="skiptext">Login to your account and click on "Cashier" to learn how to easily and quickly fund your account online. You can also contact our Customer Service Department to receive instructions on how to deposit funds.</p>
<br/>

<a name="bonuses"></a>
<h2>Bonuses</h2>

<p><strong>Q: How do I claim my bonus?</strong>  Most deposit bonuses are only given on a request basis. Please enter the "Promo Code" for the bonus you want when you are creating your account. You can also enter the Promo Code by clicking on the "Cashier" or "Deposit" link on the navigation bar when you log in to your account. If your account is eligible to receive a bonus, it will be added promptly.  For your initial deposit, you will automatically get 10% cash added to your account.</p>

<p><strong>Q: What bonuses do you offer?</strong>  We always offer several promotions to our players. Visit our promos page for available promotions.</p>
<p><strong>Q: How do I claim my bonus?</strong>  Most deposit bonuses are only given on a request basis. Please enter the "Promo Code" for the bonus you want when you are making your deposit. If your account is eligible to receive a bonus, it will be added to promptly.</p>
<p><strong>Q: Are there roll over requirements for bonuses?</strong>  Depending on the type of bonus you receive, there will be terms and conditions.  Please see our bonus and promotion terms and conditions displayed in the Club House.</p>
<br/>

<a name="payouts"></a>
<h2>Payouts</h2>

<p><strong>Q: How do I collect when I win?</strong></p>
<p>You can request a payout any time via the online cashier. Log into your <?php echo $host; ?> account, click "Cashier", select the "payout" option and follow the easy instructions. You can also request a payout by contacting our Customer Service Department toll free 1-844-GHB-BETS (1-844-442-2387). We pay out Monday through Friday and unlike the industry standard of a week, you can usually get your money within 48 hrs.</p>

<p>– Please allow 24 hours for your payout request to be reviewed.</p>
<p>– Payouts are processed Monday through Friday, between 8:00 a.m.ET to 2:00 p.m. ET, excluding holidays. Important notice: Identity verification and other documentation are often required. Payout requests will not be completed until all requested documentation has been submitted, and received by <?php echo $host; ?>.</p>
<p><strong>Q: What are the track payouts?</strong></p>
<p>GHB is a non-parimutuel horse racing website. Win, Place, Show wagers pay full track odds. All Exotic wagers pay full track odds up to the maximum pay-outs as shown in the table below:</p>
<?php include '/home/ah/allhorse/public_html/shared/track-payout.php'; ?>
<br/>

<a name="horswagering"></a>
<h2>Horse Wagering</h2>

<p><strong>Q: What bet types does <?php echo $host; ?> offer?</strong></p>
<p>We accept all bet types for horse and greyhound racing.  If there is a racetrack or bet type out there that we don't currently offer, just ask, we'll see about getting it added as an option for our members.</p>
<p><strong>Q: What is <?php echo $host; ?> wagering limits?</strong></p>

<p>The minimum wager over the Internet is typically $1 within the racebook with a maximum of $2,000 per event.  In order to wager more on a single race, you will need to get your account authorized by Management or you can simply make multiple tickets for a particular race. If you have the money in your account, you can make the bet.  Under no circumstances will credit be provided to members.</p>

<!-- <div class="table-responsive mt-5">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Method</th>
                <th scope="col">Amount Available</th>
                <th scope="col">Delivery Time</th>
                <th scope="col">Fee</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Bitcoin</th>
                <td>$25 to $5,000</td>
                <td>24 Hours</td>
                <td>FREE</td>
            </tr>
            <tr>
                <th>E-Check</th>
                <td>$50 to $200</td>
                <td>5 to 7 Business Days</td>
                <td>$5</td>
            </tr>
            <tr>
                <th>E-Check</th>
                <td>$201 - $750</td>
                <td>5 to 7 Business Days</td>
                <td>$15</td>
            </tr>
            <tr>
                <th>E-Check</th>
                <td>$751 - $2,000</td>
                <td>5 to 7 Business Days</td>
                <td>$40</td>
            </tr>
            <tr>
                <th>E-Check</th>
                <td>$2,001 - $3,000</td>
                <td>5 to 7 Business Days</td>
                <td>$100</td>
            </tr>
            <tr>
                <th>Bank Wire</th>
                <td>$500 - $1,999</td>
                <td>5 to 7 Business Days</td>
                <td>$75</td>
            </tr>
            <tr>
                <th>Bank Wire</th>
                <td>$2,000 - $4,000</td>
                <td>5 to 7 Business Days</td>
                <td>$125</td>
            </tr>
            <tr>
                <th>Bank Wire</th>
                <td>$4,001 - $5,000</td>
                <td>5 to 7 Business Days</td>
                <td>$160</td>
            </tr>
        </tbody>
    </table>
</div> -->