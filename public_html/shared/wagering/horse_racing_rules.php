<?php

if ( strpos($_SERVER["HTTP_HOST"], 'allhorseracing') !== false ) {
    $host = "All Horse Racing";
}elseif ( strpos($_SERVER["HTTP_HOST"], 'gohorsebetting') !== false ) {
    $host = "Go Horse Betting";
}

?>
<style type="text/css">
    p{padding-bottom: 20px;}
    h3{font-weight: 800;
        padding-bottom: 20px;}
    h1 a{
        font-size: 35px !important;
    }    
    h1{
        text-align: center;
        line-height: 60px;
    }

    .hors-racingr ol{
        list-style-type: decimal;
        list-style: decimal;
        list-style-type: decimal;
        margin-top: 1em;
        margin-bottom: 1em;
        margin-left: 0;
        margin-right: 0;
        padding-left: 20px;
    }
    
    .hors-racingr ul{
        margin-top: 1em;
        margin-bottom: 1em;
        margin-left: 0;
        margin-right: 0;
        padding-left: 20px;
    }

    .hors-racingr ol li{
        list-style: decimal;
        list-style-type: decimal;
        padding-bottom: 10px;
        color: #000;
        font-family: 'Open Sans', Arial, sans-serif;
    }

    .hors-racingr ul li{
        list-style: disc;
        padding-bottom: 10px;
        color: #000;
        font-family: 'Open Sans', Arial, sans-serif;
    }
    .skiptext{
        padding-bottom: 0px;
    }
     h3#kw{
        padding-bottom: 0px
    }

    tr:nth-child(odd) {background: #EBEBEB}

</style>


<p><?php echo $host; ?> offers daily racing from tracks within the United States and internationally.</p>

<p>We accept wagers up until post time. We cannot assume liability for wagers that are unsuccessfully entered before post time. Before logging-out, please check your Active Wagers on the Reports menu to make sure your bets were accepted. Click on the Racebook and then HISTORY on the Bet Slip.</p> 

<p>Once you submit a bet online, your wager is considered final.</p>

<p>Bets shall not be accepted after post-time. In the event that the race begins before the advertised post-time and for that reason (or any other reason) you are able to place a wager after the race has started, that wager will be void.</p>

<p>Payouts are based on the actual track payout. The morning lines (“M/L”) provided when wagering on the racebook are for informational purposes only and not used to calculate the payout.</p>

<p>There are no house odds. If there are no track payoffs for a certain type of wager, all wagers on that type will be refunded.</p>

<p>In the event a new track is not listed in one of the categories, its default payout limit will be Category E.</p>

<p>In the event that there is evidence of pool manipulation, all affected wagers will be deemed void and all monies wagered on the affected wagers will be refunded.</p>

<p>Management reserves the right to refuse or limit any wager and to restrict wagering on any event at any time without any advance notice.</p> 
<br/>

<h3>Scratches</h3>

<p>If a horse is scratched, all Win/Place/Show wagers placed via the racing interface will be refunded.</p>

<p>In case a horse is considered a non-starter by the track, that horse will be considered a scratch and refunded accordingly.</p>

<p>In exotic wagers (Exacta, Trifecta, Superfecta, Quinella) the portion containing the scratched horse will be refunded. We do not honor "all" payouts in any exotic wager even though the track may do so. In the case that there is an "all" payout on a particular race, we will replace the all payout with the rightful winner of that race and/or position. </p>

<p>For Daily Doubles,  Pick 3&rsquo;s, Pick 4&rsquo;s and Pick 6&rsquo;s, if a horse is scratched before any leg, a  consolation prize will be paid on that combo, as specified by the track. If the  track doesn&rsquo;t pay a consolation, wagers with these combinations will be  refunded accordingly.</p>
<br/>

<h3>Pick 3 and Pick 4 Grading and Payout Rules</h3>

<p>All Pick 3 and Pick 4 tickets are paid out based on the official race results published by the track at which the race was run. If there is a scratched horse in any Pick 3 or Pick 4, only combinations including the scratched horse will be refunded.
<br/>
</p>
<h3>Official Source of Results</h3>

<p>For thoroughbred racing we use <a href="http://www.equibase.com/" target="_blank">www.equibase.com</a> as our official source of results. For harness racing we use <a href="http://www.ustrotting.com/" target="_blank">www.ustrotting.com</a> or the tracks. </p>

<p>In the event of a discrepancy, we will contact the track for the official result.</p>
<br/>

<h3>Daily Rebate</h3>

<p class="skiptext"><?php echo $host; ?> offers a generous rebate program.</p>

<table class="data">
<tbody>
  <tr>
    <th></th>
    <th>Straights</th>
    <th>Exotics</th>
  </tr>
  <tr>
    <td>Category A</td>
    <td>3%</td>
    <td>8%</td>
  </tr>
  <tr>
    <td>Category B</td>
    <td>3%</td>
    <td>5%</td>
  </tr>
  <tr>
    <td>Category C</td>
    <td>3%</td>
    <td>3%</td>
  </tr>
  <tr>
    <td>Category D</td>
    <td>No rebate</td>
    <td></td>
  </tr>
  <tr>
    <td>Category E</td>
    <td>No rebate</td>
    <td></td>
  </tr>
</tbody>
</table>
<br/>

<p>This rebate only applies to wagers placed through the horse-wagering interface. No rebate will be given on cancelled wagers or wagers refunded due to a scratch. No rebate will be paid on Win, Place and Show tickets that pay $2.20 to $2 or less. The rebate bonus will be paid on a daily basis. There is no rollover requirement associated with the rebate. Rebates may be suspended by the Management without notice.</p>
<br/>

<h3>PROMOTIONS</h3>

<p>If, as part of a promotion offered by <?php echo $host; ?>, you receive betting cash, you can only use such amount as wagers for playing at the tracks offered by the company. You are not entitled to withdraw such amounts and we reserve the right to reclaim those amounts if you do not use them to play at the tracks during the period specified in the promotion. </p>

<p>Only members of <?php echo $host; ?> can redeem promotional prizes and winnings from promotions.</p>

<p>If you have more than one betting account, all promotional credits and/or winnings will be null and void and they will be deducted from your account. </p>

<p>Management reserves the right to modify any promotion rules or cancel promotions at its own discretion.</p>