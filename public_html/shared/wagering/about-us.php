<?php

if ( strpos($_SERVER["HTTP_HOST"], 'allhorseracing') !== false ) {
    $host = "All Horse Racing";
}elseif ( strpos($_SERVER["HTTP_HOST"], 'gohorsebetting') !== false ) {
    $host = "Go Horse Betting";
}

?>
<style type="text/css">
    p{padding-bottom: 20px;}
    h3{font-weight: 800;
        padding-bottom: 20px;}
    h1 a{
        font-size: 35px !important;
    }    
    .about-us-content h1{
        padding-bottom: 20px
    }

    .all-sport-menu ul li a {
        color: #ffffff;
    }

    .all-sport-menu ul li a:hover{
        color: #47af58 !important;
    }
    h3#kw{
        padding-bottom: 0px
    }

</style>

<h1>Betting on Excitement - Since 1997</h1>

<p><?php echo $host; ?> started in 1997 as a racebook review site. Over the years, it became an online racebook and was later acquired by Sportingbet Plc, a public company traded in the United Kingdom.  In 2017, <?php echo $host; ?> was purchased by its current management who are avid horse players.</p>

<p><?php echo $host; ?> has returned to its roots of providing horse betting information, news and online horse betting on the top races in the world.</p>

<p><?php echo $host; ?> is focused on the thoroughbred and harness horse racing market but also provides betting on greyhound races.  Members of <?php echo $host; ?> have the ability to place wagers online on over 150 racetracks including Churchill Downs, Del Mar, Philadelphia Park, Pimlico Race Course and Belmont Park. Real time audio and video feeds are available for certain tracks for free.</p>

<p>Our goal is to provide you with the absolute best online horse betting platform along with friendly customer service.</p> 

<p>There are NO TAXES on any of the winnings to a member's wagering account and <?php echo $host; ?> gives a 8% REBATE to all customers on their bets—win or lose!</p>
