<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<style>
    .showPass{
        position: absolute;
        font-size: 23px;
        right: 8%;
        top: 27%;
        width: 5px;
        height: 5px;
        color: #1572bc;
    }

    .input[type=checkbox], input[type=radio]{
        margin:0px!important;
        height: 12px;
    }

    .login_checkbox-label {
        vertical-align: top !important;
    }

    
    .off{
        display:none;
    }
    .on{
        cursor: pointer;
        display:inline;
    }
</style>
 <div class="showPass">
        <i class="far fa-eye-slash off eye-slash"></i>
        <i class="far fa-eye on eye"></i> 
 </div>

<script>
$( ".eye" ).click(function() {
$(".eye").addClass("off");
$(".eye").removeClass("on");
$(".eye-slash").addClass("on");
$(".eye-slash").removeClass("off");
$("#password").prop('type', 'text');
$("#fp_passwordconfirm").prop('type', 'text');
});

$( ".eye-slash" ).click(function() {
$(".eye-slash").addClass("off");
$(".eye-slash").removeClass("on");
$(".eye").addClass("on");
$(".eye").removeClass("off");
$("#password").prop('type', 'password');
$("#fp_passwordconfirm").prop('type', 'password');
});

</script>