<style type="text/css">
    p{padding-bottom: 20px;}
    h3{font-weight: 800;
        padding-bottom: 20px;}
    h1 a{
        font-size: 35px !important;
    }    
    h1{
        text-align: center;
        line-height: 60px;
    }

    .hors-rules h2{
        font-weight: 800;
        padding-bottom: 20px;
    }

    .hors-rules ol{
        list-style-type: decimal;
        list-style: decimal;
        list-style-type: decimal;
        margin-top: 1em;
        margin-bottom: 1em;
        margin-left: 0;
        margin-right: 0;
        padding-left: 20px;
    }
    
    .hors-rules ul{
        margin-top: 1em;
        margin-bottom: 1em;
        margin-left: 0;
        margin-right: 0;
        padding-left: 20px;
    }

    .hors-rules ol li{
        list-style: decimal;
        list-style-type: decimal;
        padding-bottom: 10px;
        color: #000;
        font-family: 'Open Sans', Arial, sans-serif;
    }

    .hors-rules ul li{
        list-style: disc;
        padding-bottom: 10px;
        color: #000;
        font-family: 'Open Sans', Arial, sans-serif;
    }
    .skiptext{
        padding-bottom: 0px;
    }
     h3#kw{
        padding-bottom: 0px
    }

    tr:nth-child(odd) {background: #EBEBEB}

    .ihrules{
        font-style: italic;
    }
    
    .hors-rules ol ol li{
        list-style: circle;
        list-style-type: circle;
    }

    .hors-rules table.data caption{
        text-align: center;
    }

    h2.hr-rls-low, h3.hr-rls-low{
        text-transform: capitalize;
    }

</style>

<h2>WELCOME!</h2>

<p>This agreement describes the terms and conditions applicable to the use of the account wagering services provided at Go Horse Betting. The use of our services and products provided by Go Horse Betting will be taken as your acceptance of all of these terms, conditions, and disclosures contained herein. </p>
<br/>

<h2 class="hr-rls-low">House Rules</h2>

<p>Our rules are governed on the principle of fair play. Horse wagering is a game of skill. The challenge is to gather and assess the information available, use your own knowledge and then compare your opinion to the results at the racetrack after a race has been completed. </p>

<p>If you choose correctly you win.</p>

<p>All rules, regulations, and payoffs contained herein are subject to change and revision by the Management without prior notice.</p>
<br/>

<h2>AGE RESTRICTIONS</h2>

<p>No one under the age of 18 years is permitted to wager on or use any of the services and products on this website.</p>
<br/>

<h2>DISCREPANCIES/DISPUTES</h2>

<p>All members should verify their balances on each account log-in and prior to wagering. When you verify and accept your account balance you agree that all previous transactions are correct and you do not have any claims. Claims or disputes must be settled prior to making any further wagers. All internet transactions will be logged and the log file will also be saved on a back-up system. Any correspondence via telephone, email or live chat will be recorded and stored on file. </p>

<p>Notwithstanding anything in this agreement, in the event of any dispute regarding a wager or winnings, the decision of Management will be final and binding in all matters. </p>
<br/>

<h2>TAXES</h2>

<p>Management will not disclose details of a member's net winnings or losses except as required under applicable law. If you reside in a jurisdiction where your winnings are taxable, it is your responsibility to comply with any appropriate laws. </p>
<br/>

<h2>LIMITATION OF PLAYS and/or TERMINATION OF AGREEMENT</h2>

<p>The agreement between Go Horse Betting and the member may be terminated at any time upon the request of the member as long as there is no balance and no pending wagers. </p>

<p>Or, if deemed necessary, Management reserves the right to limit a member's plays or revoke the agreement between the member and Go Horse Betting. Reasons for limitation of plays or revocation of agreement include, but are not limited to: </p>

<p class="skiptext"><strong>Betting Syndicates:</strong> Management reserves the right to limit or exclude any player who attempts to defraud the house either on his/her own or in collusion with other players or other racebooks. Other sanctions could include: </p>
<ul>
<li>Administration fees</li>
<li>Refusal of payouts</li>
<li>Reversal of wagering transactions</li>
</ul>

<p class="skiptext"><strong>Abuse of System Vulnerability:</strong> If Management determines that a member is taking advantage of a vulnerability in the system (software error, malfunction or bug), we reserve the right to take some or all of the following actions: </p>

<ul>
<li>Limit wagers</li>
<li>Close the account</li>
<li>Confiscate monies in the account, including deposits and winnings.</li>
</ul>

<p><strong>Criminal Activity:</strong> If there is reason to believe that criminal or other suspicious activities are occurring through one or more accounts (including attempted money-laundering or fraud), Management reserves the right to close those accounts and/or report such activity to the Gaming Commission and/or other regulatory bodies or services. All account balances (including both deposits and any winnings) shall be forfeited. </p>
<br/>

<h2>PASSWORD</h2>

<p>Members are solely responsible for the security of their passwords.</p>

<p>Members are responsible for any unauthorized use of their accounts. In the event that a third party places a bet, or is thought to have placed a bet, the said bet shall be valid whether or not the alleged third party had the prior consent of the member. Under no circumstances will any bet be set to no action for that reason. If you suspect that a third party may have access to your password or username, you should contact Go Horse Betting immediately.</p>

<p class="skiptext">When choosing a password, Management recommends you follow these guidelines:</p>

<ul>
<li>Do not use all lower case letters</li>
<li>Do use numbers and or symbols</li>
<li>Do use a mix of upper and lower case letters</li>
</ul>
<br/>

<h2>BONUSES</h2>
<ol>
<li>Members of Go Horse Betting are eligible to receive bonuses and/or free bets from time to time, including a bonus when signing-up and for each referral. </li>
<li>Bonuses and/or free bets shall be non-transferable and non-refundable.</li> 
<li>We only allow one bonus/free bet per account/household or environments where computers are shared. </li>
<li>A bonus/free bet is only valid if the rollover requirement associated with the said bonus has been fulfilled. </li>
<li>These requirements/terms may vary depending on the bonus/free bet given and you must always refer to the terms and conditions of the particular bonus for clarification. </li>
<li>Please note that bonuses/free bets are not cumulative, for example, we offer a standard $150 signup bonus; this cannot be combined with other promotions or specials to become a larger bonus.  </li>
<li> 10% Sign-up bonuses, reload bonuses and other cash rewards are subject to the following standard rollover requirements: </li>
    <ol>
        <li>Rollover requirements must be met within 15 days of your deposit.</li>
        <li>Bonus policies can be viewed online</li>
    </ol>    
<li>The recycling of funds is not permitted. Requesting a withdrawal to make a deposit with little or no activity between the withdrawal and deposit does not earn a bonus.</li>
<li>When you withdraw, some (or all) of your bonus funds may be removed from your account if their rollover requirements have not been met. The amount removed is proportional to the amount you are withdrawing: for example, if you are withdrawing 25% of your available balance, 25% of your bonus is removed. If you are withdrawing 100% of your available balance, 100% of your bonus is removed. This does not apply to free bets. </li>
<li>Members must play their own money in order to earn a bonus. If you deposit, receive a bonus and then withdraw the amount of the deposit without risking your own funds, we will deduct both the bonus and the winnings. </li>
<li>Management reserves the right to revoke a bonus and winnings derived from the revoked bonus at any time.</li>
<li>Please read the terms and conditions for bonuses and promotions found in the Club House. </li>
<li><strong>Refer a Friend Promotion</strong> The following terms and conditions apply:</li>
    <ol>
     <li>To be eligible for this offer the Referrer must have deposited before the Referred Friend.</li>
     <li>Referred Friend Minimum initial deposit should be $100.</li>
     <li>Bonus will be paid seven days after the Referred Friend has made their first deposit.</li>
     <li>In order to be eligible as a valid referral, the Referred Friend must have used at least 50% of his initial deposit and must not have requested a withdrawal before completing the wagering requirement on his own Sign-Up Bonus. </li>
     <li>If, after seven days of the initial deposit, 50% of the amount has not been wagered, your referral bonus will be credited once it is wagered. </li>
     <li>The Referred Friend still receives their 10% Free Cash Sign-up Bonus offer even if they have not met the wagering requirements for a referral bonus. </li>
     <li>The Referred Friend 10% signup bonus offer supersedes all other signup bonus offers.</li>
     <li>There is no limit to the number of times you're credited a referral bonus, but limited to one bonus per Referred Friend.</li>
     <li>Opening multiple accounts is strictly against the house rules. If the referee already has an account with us (Inactive or otherwise) you will not be entitled to receive any referral bonus funds. </li>
     <li>All referral claims for credit must be made within 60 days of your friend/acquaintance (the referee) signing up.</li>
    </ol>
</ol>
<br/>

<h3 class="hr-rls-low">Free money or Cash Code Bonuses are subject to the following restrictions:</h3>

<p>If a member attempts to withdraw winnings derived entirely from a free cash bonus prior to meeting the requirements specified in the promotion, both the winnings and promotional bonus shall be forfeited. </p>
<br/>

<h3 class="hr-rls-low">Abuse of Bonus Programs:</h3>

<p class="skiptext">Our bonus program is intended for recreational bettors only. Go Horse Betting reserves the right to revoke bonuses for and further sanction any member considered to be abusing the bonus system. Bonus abuse includes: </p>
<ul>
<li>Cashing out for the purpose of re-depositing</li>
<li>Referring new accounts for your own use</li>
</ul>

<p class="skiptext">The sanctions may take the form of:</p>
<ul>
<li>Increased rollover requirement</li>
<li>Loss of bonus privileges for the offending account and any linked accounts</li>
<li>Geographical restrictions on bonus eligibility</li>
</ul>
<br/>

<h2>WITHDRAWING FUNDS</h2>

<p>We accept withdrawal requests 24 hours a day, 7 days a week and the minimum withdrawal is $100. There is a maximum of 1 withdrawal request per day.  </p>

<p><strong>Please note:</strong> On occasions we may have to send your withdrawal via a method other than the one you have chosen. In the event that the method is changed, we will notify you. </p>

<p>Before you request a withdrawal, please make sure your address is correct and up-to-date.</p>

<p>If a stop payment is requested by the member, it is the member's responsibility to assume the stop-payment fees of a minimum of $50.</p>

<p>A <strong>pending</strong> withdrawal may be cancelled if desired; you must access the Withdrawal Confirm Cancel page within the Cashier by selecting Cancel from either: </p>

<ul>
<li>Withdrawal Success Page</li>
<li>Withdrawal/Transaction History page</li>
</ul>

<p>Once the withdrawal cancellation is submitted the money will be credited back to your wagering account immediately.</p>
<br/>

<h2>REVERSED DEPOSITS</h2>

<p>Whenever a deposit is stopped/reversed Management reserves the right to void all activity in the account and the account holder will forfeit any accounts winnings deriving from the stopped/reversed deposit. </p>
<br/>

<h2>DORMANT AND ABANDONED ACCOUNTS</h2>

<p>If no real money transaction has been recorded on an account for more than twelve (12) months it shall be determined to be dormant and we shall lock the account for the member protection. If requested by the member, the account will be unlocked, but the member will have to supply the correct answers to all security questions. The unlocking of an account must also be approved by the Management. </p>

<p class="ihrules">Management has the right to charge a $100 monthly account management fee on all accounts where there has been no activity in the preceding 12 months. Players who wish to return to play with us are eligible to have any account management fee previously charged re-credited back to their account with Management's approval. Please note: for accounts located within a restricted region the monthly account management fee will be charged within 30 days of inactivity or the date of account. Additional Management fees may apply.</p>


<p>If no real money transaction has been recorded on an account for more than five (5) years it shall be determined to be abandoned.</p>
<br/>

<h2>CONFIDENTIALITY</h2>

<p>Go Horse Betting shall undertake to maintain your anonymity unless you agree to your identity being used for future publicity or other purpose </p>
<br/>

<h2>LIABILITY OF GO HORSE BETTING</h2>
<ol>
<li>The decisions of the Management will be final and binding in all matters between Go Horse Betting and members.</li>
<li>Laws regarding gaming vary throughout the world and online horse betting and online gambling, in general, may be unlawful in some jurisdictions; it is the responsibility of members to ensure that they understand and comply fully with any laws or regulations relevant to themselves in their own country/state/locale. </li>
<li>Management does not undertake to notify members that they have outstanding balances to collect.</li>
<li>Any wager made by the member will be member's liability in respect to any claim or loss.</li>
<li>Management shall reserve the right to suspend or withdraw any track at its absolute discretion.</li>
<li>All personal details of all members will be held in confidence unless members agree to their identities and details being used for future publicity purposes. </li>
<li>Management reserves the right to at any time, request documentation from a member before releasing a withdrawal or for general auditing or identification purposes. We also reserve the right to request additional documentation or photographic proof of yourself with the documentation. </li>
<li>Withdrawals based on account closures are subject to a $100 processing fee.</li>
<li>In the event of the cancellation of any race for any reason, Management will not be liable.</li>
<li>Management will not be liable in the following scenarios:
In the event of force majeure the failure of the Go Horse Betting or its software provider's central computer system or any part thereof for delays, losses, errors, or omissions resulting from failure of any telecommunications or any other data transmission system for any loss as a result of any act of God for an outbreak of hostilities, riot, civil disturbance, acts of terrorism for the acts of Government or authority (including refusal or revocation of any license or consent) for fire, explosion, flood, theft, malicious damage, strike, lockout, or industrial action of any kind.</li> 
<li>Management and the member shall not commit or purport to commit the other to honor any obligation other than is specifically provided for by these rules. </li>
<li>Go Horse Betting accepts no liability for any damages, which may be caused to the member by the interception or misuse of any information transmitted over the internet. </li>
<li>These rules constitute the entire agreement and understanding between Go Horse Betting and the member.</li>
<li>The maximum dollar amount that may be won by an individual member on a weekly basis is US$100,000.00. Please note, our weeks run Tuesday thru Monday. </li>
<li>Management reserves the right to refuse or limit any wager.</li>
<li>Warning: Gambling involves risk. By gambling on this website, you run the risk that you may lose money or suffer psychological injuries. You gamble at your own risk.  </li>
</ol>
<br/>

<h2>OUR SOCIAL RESPONSIBILITY</h2>

<p>For most people gambling is entertainment - an enjoyable activity that has no harmful effect. But for some it becomes a serious problem. Compulsive gambling is not easily detected and individuals with gambling problems will often go to great lengths to disguise the addiction and the problems that it causes. </p>

<p>We are concerned about problem gambling. We will immediately inactivate any wagering account belonging to a member for whom gambling has become a problem and that account will remain permanently closed. Furthermore, any new accounts opened by that member will be inactivated. </p>

<p>Members must be at least 18 years of age.</p>
<br/>

<h2>HORSE RACING</h2>

<p>Go Horse Betting offers daily racing from tracks within the United States and internationally. </p>

<p>We accept wagers up until post time. We cannot assume liability for wagers that are unsuccessfully entered before post time. Before logging-out, please check your Active Wagers on the Reports menu to make sure your bets were accepted. </p>

<p>Once you submit a bet online, your wager is considered final.</p>

<p>Bets shall not be accepted after post-time. In the event that the race begins before the advertised post-time and for that reason (or any other reason) you are able to place a wager after the race has started, that wager will be void. </p>

<h3 class="hr-rls-low">Track Categories</h3>

<p>Click here for an up-to-date list of tracks offered and their categories.</p>

<h3 class="hr-rls-low">Odds and Limits</h3>

<p>Each event has a bet limit (typically $2,000.00).</p>

<p>Win, Place, Show wagers pay full track odds. All Exotic wagers pay full track odds up to the maximum pay-outs as shown in the tables below. </p>

<p>Payouts are based on the actual track payout. The morning lines provided when wagering on the horse betting interface are for informational purposes only and not used to calculate the payout. </p>

<p>There are no house odds. If there are no track payoffs for a certain type of wager, all wagers on that type will be refunded. </p>

<table class="data">
<caption><strong>Class A Tracks </strong></caption>
  <tr>
    <th>Bet Type</th>
    <th>Max. Payoff</th>
  </tr>
  <tr>
    <td>Win</td>
    <td>Track odds</td>
  </tr>
  <tr>
    <td>Place</td>
    <td>Track odds</td>
  </tr>
  <tr>
    <td>Show</td>
    <td>Track odds</td>
  </tr>
  <tr>
    <td>Exacta</td>
    <td>500-1</td>
  </tr>
  <tr>
    <td>Quinella</td>
    <td>400-1</td>
  </tr>
  <tr>
    <td>Trifecta</td>
    <td>1000-1</td>
  </tr>
  <tr>
    <td>Superfecta</td>
    <td>1500-1</td>
  </tr>
  <tr>
    <td>Pick 3</td>
    <td>1500-1</td>
  </tr>
  <tr>
    <td>Pick 4</td>
    <td>5000-1</td>
  </tr>
  <tr>
    <td>Daily Double</td>
    <td>750-1</td>
  </tr>
</table>

<table class="data">
<caption><strong>Class B Tracks</strong></caption>
  <tr>
    <th>Bet Type</th>
    <th>Max. Payoff</th>
  </tr>
  <tr>
    <td>Win</td>
    <td>Track odds</td>
  </tr>
  <tr>
    <td>Place</td>
    <td>Track odds</td>
  </tr>
  <tr>
    <td>Show</td>
    <td>Track odds</td>
  </tr>
  <tr>
    <td>Exacta</td>
    <td>400-1</td>
  </tr>
  <tr>
    <td>Quinella</td>
    <td>300-1</td>
  </tr>
  <tr>
    <td>Trifecta</td>
    <td>750-1</td>
  </tr>
  <tr>
    <td>Superfecta</td>
    <td>1000-1</td>
  </tr>
  <tr>
    <td>Pick 3</td>
    <td>1000-1</td>
  </tr>
  <tr>
    <td>Pick 4</td>
    <td>2000-1</td>
  </tr>
  <tr>
    <td>Daily Double</td>
    <td>400-1</td>
  </tr>
</table>

<table class="data">
<caption><strong>Class C Tracks </strong></caption>
  <tr>
    <th>Bet Type</th>
    <th>Max. Payoff</th>
  </tr>
  <tr>
    <td>Win</td>
    <td>Track odds</td>
  </tr>
  <tr>
    <td>Place</td>
    <td>Track odds</td>
  </tr>
  <tr>
    <td>Show</td>
    <td>Track odds</td>
  </tr>
  <tr>
    <td COLSPAN="2">All Exotics 300-1</td>
  </tr>
</table>

<table class="data">
<caption><strong>Class D Tracks (Max Bet = $250)</strong></caption>
  <tr>
    <th>Bet Type</th>
    <th>Max. Payoff</th>
  </tr>
  <tr>
    <td>Win</td>
    <td>Track odds</td>
  </tr>
  <tr>
    <td>Place</td>
    <td>Track odds</td>
  </tr>
  <tr>
    <td>Show</td>
    <td>Track odds</td>
  </tr>
  <tr>
    <td COLSPAN="2">All Exotics 300-1</td>
  </tr>
</table>
<br/>

<p class="skiptext">Maximum pay-outs per event are as follows:</p>
<ul>
<li>Class A tracks: $20,000.00</li>
<li>Class B tracks: $15,000.00</li>
<li>Class C tracks: $10,000.00</li>
<li>Class D tracks: $3,000.00</li>
<li>Class E tracks: $1,000.00</li>
</ul>

<p>In the event a new track is not listed in one of the categories, its default payout limit will be Category E.</p>

<p>In the event that there is evidence of pool manipulation, all affected wagers will be deemed void and all monies wagered on the affected wagers will be refunded. </p>

<p>Management reserves the right to refuse or limit any wager and to restrict wagering on any event at any time without any advance notice. </p>

<p>Management reserves the right to increase the maximum wager for any event without any advance notice.</p>

<h3 class="hr-rls-low">Scratches</h3>

<p>If a horse is scratched, all Win/Place/Show wagers placed via the racing interface will be refunded.  In case a horse is considered a non-starter by the track, that horse will be considered a scratch and refunded accordingly. </p>

<p>In exotic wagers (Exacta, Trifecta, Superfecta, Quinella) the portion containing the scratched horse will be refunded. We do not honor all payouts in any exotic wager even though the track may do so. In the case that there is an all payout on a particular race, we will replace the all payout with the rightful winner of that race and/or position. </p>

<p>On Daily Doubles, Pick 3 and Pick 4 wagers, a scratch will result in an automatic refund of the combination including the scratched horse. There will be no consolation payouts. No special track payouts will be recognized: </p>

<h3 class="hr-rls-low">Pick 3 and Pick 4 Grading and Payout Rules</h3>

<p>All Pick 3 and Pick 4 tickets are paid out based on the official race results published by the track at which the race was run. If there is a scratched horse in any Pick 3 or Pick 4, only combinations including the scratched horse will be refunded. </p>

<p>There will be no consolation payouts. No special track payouts (such as payouts on scratched horses, 2 out of 3 special Pick 3 payouts and/or 3 out of 4 special Pick 4 payouts) will be recognized. </li>

<h3 class="hr-rls-low">Official Source of Results</h3>

<p>For thoroughbred racing we use <a href="www.equibase.com">www.equibase.com</a> as our official source of results. For harness racing we use <a href="www.ustrotting.com">www.ustrotting.com</a> or the track's. </p>

<p>In the event of a discrepancy, we will contact the track for the official result.</p>

<h3 class="hr-rls-low">Daily Rebate</h3>

<p class="skiptext">Go Horse Betting offers a generous rebate program:</p>


<table class="data">
  <tr>
    <th></th>
    <th>Straights</th>
    <th>Exotics</th>
  </tr>
  <tr>
    <td>Category A</td>
    <td>3%</td>
    <td>8%</td>
  </tr>
  <tr>
    <td>Category B</td>
    <td>3%</td>
    <td>5%</td>
  </tr>
  <tr>
    <td>Category C</td>
    <td>3%</td>
    <td></td>
  </tr>
  <tr>
    <td>Category D</td>
    <td>No rebate</td>
    <td></td>
  </tr>
  <tr>
    <td>Category E</td>
    <td>No rebate</td>
    <td></td>
  </tr>
</table>
<br/>

<p>This rebate only applies to wagers placed through the horse betting interface. No rebate will be given on cancelled wagers or wagers refunded due to a scratch. No rebate will be paid on Win, Place and Show tickets that pay $2.20 to $2 or less. The rebate bonus will be paid on a daily basis. There is no rollover requirement associated with the rebate. </p>
<br/>

<h2>PROMOTIONS</h2>

<p>If, as part of a promotion offered by Go Horse Betting, you receive betting cash, you can only use such amount as wagers for playing in the games offered by the company. You are not entitled to withdraw such amounts and we reserve the right to reclaim those amounts if you do not use them to play the games during the period specified in the promotion. </p>

<p>Only members of Go Horse Betting can redeem promotional prizes and winnings from promotions.</p>

<p>If you have more than one betting account, all promotional credits and/or winnings will be null and void and they will be deducted from your account. </p>

<p>Management reserves the right to modify any promotion rules or cancel promotions at its own discretion.</p>
<br/>

<!--<h2>PROMOTIONS</h2>

<p>If, as part of a promotion offered by Go Horse Betting, you receive betting cash, you can only use such amount as wagers for playing in the games offered by the company. You are not entitled to withdraw such amounts and we reserve the right to reclaim those amounts if you do not use them to play the games during the period specified in the promotion. </p>
<p>Only members of Go Horse Betting can redeem promotional prizes and winnings from promotions.</p>
<p>If you have more than one betting account, all promotional credits and/or winnings will be null and void and they will be deducted from your account. </p>
<p>Management reserves the right to modify any promotion rules or cancel promotions at its own discretion.</p>
<br/>-->

<h2>MULTIPLE ACCOUNTS</h2>

<p>Members are not permitted to open multiple accounts (either within Go Horse Betting itself or affiliated websites) in order to circumvent the limits imposed by our wagering system. Performing such an action may trigger an audit of the member’s accounts. If multiple accounts have been used then all bets may be voided, accounts may be inactivated and winnings may be forfeited. </p>
<br/>

<h2>ACCOUNT CLOSURES</h2>

<p>If an account is closed per Management's decision, all pending wagers will be graded as losses.</p>
<br/>

<h2>MEMBER ADDRESS</h2>

<p>It is your responsibility to ensure that we have your current address on file at all times. You may only have one address on file at a time, and this address MUST match the billing address of ALL cards that have been used in the account. Any failure on your part to do this may result in an audit on your account. </p>

<p class="Straights">Before making a deposit and to avoid unnecessary delays in processing your payouts, please make sure your address is correct and that it matches bank records. In the event that we are not able to verify your account information you may be required to submit additional documentation for this purpose. This may include the following: </p>
<ul>
<li>A photocopy of your picture ID (e.g. Driver's License, passport)</li>
<li>Photocopies (front and back) of your credit cards</li>
<li>A credit card statement or utility bill</li>
<li>A credit card verification form</li>
<li>Imprint of your credit card</li>
</ul>

<p>When requesting a withdrawal, please ensure that your current address is correct and up-to-date, also ensure we have received and verified all the required Know Your Customer documents at least 72 hours prior to making your request; we will need you to provide us with a copy of a valid form of ID and a copy of a utility bill showing your name and address. Depending on your method of withdrawal, we may need to provide these documents to our payment processor prior to processing your withdrawal. If a stop-payment request is necessary due to an out-of-date address, it will be your responsibility to assume the stop payment fees of a minimum of $50. </p>
<br/>

<h2>MULTIPLE CREDIT CARDS</h2>

<p>The maximum number of credit cards allowed for use in one account is five. All cards used on an account must be in the name of the Go Horse Betting member. </p>
<br/>

<h2>LEGALITY</h2>

<p>All bets made on products provided by Go Horse Betting website are placed over the internet which reaches virtually every country in the world. Some jurisdictions have not addressed the legality of horse betting, some have specifically legalised horse betting, while others may take the position that horse wagering or gaming is illegal. In practical terms, it is impossible for Go Horse Betting to determine the state of the law in every country, state and province around the world on an ongoing basis. Therefore, by clicking the "agree" button, you are acknowledging that it is responsibility of each individual to determine the law that applies in the jurisdiction in which he or she is present and that accordingly (a) you have determined what the laws are in your jurisdiction; and (b) it is legal for you to place a bet via the internet and for Go Horse Betting to receive your bet via the internet. </p>

<p>Go Horse Betting has offices in Hong Kong and Central America. Any matter to be adjudicated shall be determined utilizing the laws of Costa Rica.</p>

<p>Should there be any discrepancy in the legality of any transactions between you and Management, or any of its affiliates, the matter shall be determined by a court of competent jurisdiction in the country of Costa Rica. </p>

<p>This website does not constitute an offer, solicitation or invitation by Go Horse Betting for the use of or subscription to betting or other services in any jurisdiction in which such activities are prohibited by law. All contractual issues between you and Go Horse Betting or any of its affiliates, that are disputed, shall be resolved by a court of competent jurisdiction in the country of Costa Rica. All contracts shall be interpreted in accordance with the laws of Costa Rica. </p>

<p>Go Horse Betting does not guarantee the accuracy of verbal or written communications. House Rules have precedence over any verbal or written communications and, in the event of any dispute, the policies and terms and conditions posted on our site are final. Furthermore, Management does not guarantee the privacy of live chat conversations. </p>
<br/>

<h2 class="hr-rls-low">Restrictions on Use of Materials</h2>

<p>Materials in this website are Copyrighted and all rights are reserved. Text, graphics, databases, HTML code, and other intellectual property are protected by International Copyright Laws, and may not be copied, reprinted, published, reengineered, translated, hosted, or otherwise distributed by any means without explicit permission. All of the trademarks on this site are trademarks of Go Horse Betting or of other owners used with their permission. </p>
<br/>

<h2 class="hr-rls-low">Database Ownership, License, and Use</h2>

<p>Go Horse Betting warrants, and you accept, that we own of the copyright of the Databases of Links to articles and resources available from time to time through Go Horse Betting and its contributors reserve all rights and no intellectual property rights are conferred by this agreement. </p>

<p>Go Horse Betting grants you a non-exclusive, non-transferable license to use database(s) accessible to you subject to these Terms and Conditions. The database(s) may be used only for viewing information or for extracting information to the extent described below. </p>

<p>You agree to use information obtained from Go Horse Betting databases only for your own private use or the internal purposes of your home or business, provided that is not the selling or broking of information, and in no event cause or permit to be published, printed, downloaded, transmitted, distributed, reengineered, or reproduced in any form any part of the databases (whether directly or in condensed, selective or tabulated form) whether for resale, republishing, redistribution, viewing, or otherwise.</p>

<p>Nevertheless, you may on an occasional limited basis download or print out individual pages of information that have been individually selected, to meet a specific, identifiable need for information which is for your personal use only, or is for use in your business only internally, on a confidential basis. You may make such limited number of duplicates of any output, both in machine-readable or hard copy form, as may be reasonable for these purposes only. Nothing herein shall authorize you to create any database, directory or hard copy publication of or from the databases, whether for internal or external distribution or use. </p>
<br/>

<h2 class="hr-rls-low">Liability</h2>

<p>The materials in this site are provided as is and without warranties of any kind either express or implied. Go Horse Betting disclaims all warranties, express or implied, including, but not limited to, implied warranties of merchantability and fitness for a particular purpose. Go Horse Betting does not warrant that the functions contained in the materials will be uninterrupted or error-free, that defects will be corrected, or that this site or the server that makes it available are free of viruses or other harmful components. Go Horse Betting does not warrant or make any representations regarding the use or the results of the use of the materials in this site in terms of their correctness, accuracy, reliability, or otherwise. You (and not Go Horse Betting) assume the entire cost of all necessary servicing, repair or correction. Applicable law may not allow the exclusion of implied warranties, so the above exclusion may not apply to you. </p>

<p>Under no circumstances, including, but not limited to, negligence, shall Go Horse Betting be liable for any special or consequential damages that result from the use of, or the inability to use, the materials in this site, even if Go Horse Betting or a Go Horse Betting authorized representative has been advised of the possibility of such damages. Applicable law may not allow the limitation or exclusion of liability or incidental or consequential damages, so the above limitation or exclusion may not apply to you. In no event shall Go Horse Betting total liability to you for all damages, losses, and causes of action (whether in contract, tort, including but not limited to, negligence or otherwise) exceed the amount paid by you, if any, for accessing this site. </p>

<p>Facts and information at this website are believed to be accurate at the time they were placed on the website. Changes may be made at any time without prior notice. All data provided on this website is to be used for information purposes only. The information contained on this website and pages within, is not intended to provide specific legal, financial or tax advice, or any other advice, whatsoever, for any individual or company and should not be relied upon in that regard. The services described on this website are only offered in jurisdictions where they may be legally offered. Information provided in our website is not all-inclusive, and is limited to information that is made available to Go Horse Betting and such information should not be relied upon as all-inclusive or accurate. </p>
<br/>

<h2 class="hr-rls-low">Links and Marks</h2>

<p>The owner of this site is not necessarily affiliated with sites that may be linked to this site and is not responsible for their content. The linked sites are for your convenience only and you access them at your own risk. Links to other websites or references to products, services or publications other than those of Go Horse Betting and its subsidiaries and affiliates at this website, do not imply the endorsement or approval of such websites, products, services or publications by Go Horse Betting or its subsidiaries and affiliates. </p>

<p>Certain names, graphics, logos, icons, designs, words, titles or phrases at this website may constitute trade names, trademarks or service marks of Go Horse Betting or of other entities. The display of trademarks on this website does not imply that a license of any kind has been granted. Photo credits have been provided when available. If your image appears here without proper credit please contact us and we will remove the photo or give you credit for your work. Any unauthorized downloading, re-transmission, or other copying of modification of trademarks and/or the contents herein may be a violation of federal common law trademark and/or copyright laws and could subject the copier to legal action. </p>
<br/>

<h2 class="hr-rls-low">Confidentiality of Codes, Passwords and Information</h2>

<p>You agree to treat as strictly private and confidential any account number or password which you may have received from Go Horse Betting, and all information to which you have access through password-protected areas of Go Horse Betting websites and will not cause or permit any such information to be communicated, copied or otherwise divulged to any other person whatsoever. </p>
<br/>

<h2 class="hr-rls-low">Domains</h2>

<p>These Terms of Use will apply to every access to Go Horse Betting (gohorsebetting.com) and any variant of that top level domain name. We reserve the right to issue revisions to these Terms of Use by publishing a revised version of this document on this site: that version will then apply to all use by you following the date of publication. Each access of information from Go Horse Betting will be a separate, discrete transaction based on the then prevailing terms. </p>

<p>This Terms of Use and the license granted may not be assigned or sublet by you without Go Horse Betting written consent in advance.</p>

<p>These Terms of Use shall be governed by, construed and enforced in accordance with the laws of Costa Rica.  </p>

<p class="Straights">Any other disputes will be resolved as follows:</p>

<p>If a dispute arises under this agreement, we agree to first try to resolve it with the help of a mutually agreed-upon mediator in the following choice by Go Horse Betting among the following countries:  Costa Rica and Hong Kong. Any costs and fees other than attorney fees associated with the mediation will be shared equally by each of us. </p>

<p>If it proves impossible to arrive at a mutually satisfactory solution through mediation, we agree to submit the dispute to binding arbitration at the following locations under its respective rules of arbitration: Costa Rica. Judgment upon the award rendered by the arbitration may be entered in any court with jurisdiction to do so. </p>

<p>If any provision of this agreement is void or unenforceable in whole or in part, the remaining provisions of this Agreement shall not be affected thereby. </p>
<br/>

<h2 class="hr-rls-low">Termination</h2>

<p>These Terms of Use agreement are effective until terminated by either party. You may terminate this agreement at any time by destroying all materials obtained from any and all Go Horse Betting and all related documentation and all copies and installations thereof, whether made under the terms of this agreement or otherwise. This agreement will terminate immediately without notice at Go Horse Betting sole discretion, should you fail to comply with any term or provision of this agreement. Upon termination, you must destroy all materials obtained from this site and any and all other Go Horse Betting site(s) and all copies thereof, whether made under the terms of this agreement or otherwise. </p>


