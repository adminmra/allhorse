<?php
require 'signup_form_misc/us_states.php';

if ( strpos($_SERVER["HTTP_HOST"], 'allhorseracing') !== false ) {
    $host = "All Horse Racing";
}elseif ( strpos($_SERVER["HTTP_HOST"], 'gohorsebetting') !== false ) {
    $host = "Go Horse Betting";
}
?>

<style type="text/css">
    #accordion h3{
        cursor: pointer;
        text-decoration: underline;
    }

    #accordion .panel-collapse{
        padding: 15px
    }

    h3#kw{
        display: none;
    }
</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<h2>Frequently Asked Questions</h2>

<p>Here are some frequently asked questions for new members. If you have any questions at all please call us, toll free, at 1-844-GHB-BETS (1-844-442-2387). At <?php echo $host; ?> you can bet from your home computer, mobile or tablet. Get up to an 8% horse betting rebate and there is a $150 Bonus for new members. Join Today!</p>


    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 data-parent="#accordion" data-toggle="collapse" href="#qa01" >Can anyone join <?php echo $host; ?>?</h3> 
             </div> 

             <div id="qa01" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>To open a <?php echo $host; ?> account, you must be at least 18 years old.</p>
                </div>
             </div>
        </div>           

        <div class="panel panel-default">
            <div class="panel-heading">        
                <h3 data-parent="#accordion" data-toggle="collapse" href="#qa1">Can you take customers from my state?</h3>
            </div>

            <div id="qa1" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Everybody is welcome to signup at <?php echo $host; ?>.  Laws for online horses betting vary from country to country and state to state.  It is up to the individual member to determine whether they are eligible to play at <?php echo $host; ?>.</p>
                </div>
            </div>
        </div>    
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 data-parent="#accordion" data-toggle="collapse" href="#qa2">How can I open an account?</h3>
            </div>
                
        <div class="panel-collapse collapse" id="qa2">
            <div class="panel-body">
                <p>Sign up for <?php echo $host; ?> by clicking Sign Up</p>
            </div>    
        </div></div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 data-parent="#accordion" data-toggle="collapse" href="#qa3">What information do I need to open an account?</h3></div>

    <div class="panel-collapse collapse" id="qa3">
        <p>You will need to provide your basic personal and mailing address to open an account. For your convenience to be able to play right away, you can make your deposit immediately.</p>
    
        <p>However, if you deposit via credit card, a signed credit card authorization form and a photo ID may be needed. It is best to have them submitted before your payout request so you can receive your winnings without delay. You also should confirm with your credit card company that your card is approved for foreign transactions. </p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa4">Why do you need my ID?</h3></div>

    <div class="panel-collapse collapse" id="qa4">
        <p class="skiptext">Your financial security is absolutely paramount. With this in formation we can validate that:</p>
            <ul>
                <li>You are at least 18 years of age.</li>
                <li>You are the true owner of the credit card (KYC, Know Your Customer).</li>
                <li>Your winnings go to the correct person: you.</li>
            </ul>
        <p>This is standard industry practice for everybody's safety.</p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa5">What if I don't have a credit card.</h3></div>
    <div class="panel-collapse collapse" id="qa5">
        <p>Several alternate deposit methods available including MoneyGram, Bank Wire, ACH, Peer-to-Peer and even Bitcoin! Please call 1-844-GHB-BETS (1-844-442-2387) to speak to one of our representatives to see which one is best for you.  Or, click on the cashier after you have logged in to your racing account.</p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa6">I have a Promo Code. Where do enter it to get my bonus?</h3></div>    
    <div class="panel-collapse collapse" id="qa6">
        <p>The area to put the Promo Code in is in the deposit section of the cashier. Promo codes do expire quickly so please use it as soon as possible. If you have any questions, please contact Customer Service. </p>
    </div>
    </div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa7">I know a lot of places can take weeks, even months to pay out. How long does it take <?php echo $host; ?>?</h3></div>
    <div class="panel-collapse collapse" id="qa7">
        <p>If you have your KYC documents in place, payouts process very quickly. Once processed, standard transfer times will apply depending on payout method (found in the cashier). </p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa8">Is there a fee for my payout?</h3></div>
    <div class="panel-collapse collapse" id="qa8">
        <p>Yes, there is a nominal charge that will vary depending on payout of choice. This fee ensures the integrity and speed of our payouts so you can get your winnings as fast as possible and far faster than industry average. </p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa9">Will my winnings be taxed?</h3></div>
    <div class="panel-collapse collapse" id="qa9">
        <p>It is up to the members to represent to their tax authorities their own winnings/losses. <?php echo $host; ?> will not provide the account activity of its members to any tax authority unless required by recognized law. </p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa10">Does <?php echo $host; ?> offer rebates or bonuses?</h3></div>
    <div class="panel-collapse collapse" id="qa10">
        <p>Yes, there is up to an 8% daily rebate on horse racing depending on the specific type of wager and the racetrack. First deposits at <?php echo $host; ?> are eligible for various cash bonuses and promotions. Please check out our weekly cash specials and horse racing bonuses offered to <?php echo $host; ?> members. </p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa11">Does <?php echo $host; ?> have live video?</h3></div>
    <div class="panel-collapse collapse" id="qa11">
        <p>Yes, live horse racing video is available on desktop, tablet and mobile devices.  Members must place a wager in order to have access to live video feeds. </p>
    </div>
</div>

<h2>WAGERING ANSWERS</h2>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa12">How do I place a bet? </h3></div>
    <div class="panel-collapse collapse" id="qa12">
        <p><?php echo $host; ?> account holders can wager online at using a computer, mobile device or tablet. Telephone wagering is also available.  Tutorials on how to place your wager are available in the Club House.</p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa13">What are the minimum and maximum limitations on wagering amounts? </h3></div>
    <div class="panel-collapse collapse" id="qa13">
        <p><?php echo $host; ?>'s wagering system accepts wager amounts typically ranging from $1.00 to $2,000.00 per wager, subject to host track restrictions. In the case of multiple wagers, such as a Daily Double or Exacta box, the base wager is multiplied by the number of combinations to determine the amount of the total wager. If a customer wishes to place more than $2,000, please contact <?php echo $host; ?> to increase your betting limits. Please see the horse betting rules section in the clubhouse for details. </p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa14">When can I bet on the races? </h3></div>
    <div class="panel-collapse collapse" id="qa14">
        <p>You can place wagers 24x 7 for all of the day's races. Besides popular tracks like Churchill Downs, Saratoga and Santa Anita, <?php echo $host; ?> offers the most international racetracks of any racebook. Bet on international races in Hong Kong, Australia, UK, France, Ireland and Japan! Wagers can be placed up to post time of each race. For special events like the Kentucky Derby, <?php echo $host; ?> will provide advance wagering as well as wagering on the day of the racing event. </p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa15">What wagering choices do I have?</h3></div>
    <div class="panel-collapse collapse" id="qa15">
        <p>The full menu of wagering options are typically you would have available at the racetrack. <?php echo $host; ?> currently offers more choices and types of wagers on horses than any other online ADW or racebook.</p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa16">When do I get confirmation that my wager has been accepted?</h3></div>
    <div class="panel-collapse collapse" id="qa16">
        <p>Normally, within 5 seconds after the wager has been accepted. Your betting ticket and confirmation number serves as evidence of your wager being successfully accepted. Your receipt of the confirmation may be slowed due to site volume, internet traffic, or connection speed for your computer or mobile device. </p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa17">How are the wagers processed?</h3></div>
    <div class="panel-collapse collapse" id="qa17">
        <p>All wagers are processed at our racebook provider's hub system for each particular race. Races are graded as soon as the official results from the track have been announced.  On heavy volume days like the Kentucky Derby, please be patient for your account to be updated.</p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa18">How can I access my account information?</h3></div>
    <div class="panel-collapse collapse" id="qa18">
        <p>Access your account history using <?php echo $host; ?>'s "My Account" area by selecting the "My Account" link or Club House link. Your Account Balance is always in the top right hand column of your screen. </p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa19">Are winners paid full-track odds?</h3></div>
    <div class="panel-collapse collapse" id="qa19">
        <p>Full track odds are paid on most popular wagers although there are maximum payouts for certain types of wagers. Please see the Rules section in the Club House for more details. </p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa20">How do I collect my winnings?</h3></div>
    <div class="panel-collapse collapse" id="qa20">
        <p>Your winnings are automatically credited to your account as soon as the race is declared "official" and results are posted. This may take a few minutes on higher volume races such as the Kentucky Derby. Payouts are requested through the online cashier or calling 1-844-GHB-BETS (1-844-442-2387).</p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa21">How do I make a withdrawal from my account?</h3></div>
    <div class="panel-collapse collapse" id="qa21">
        <p><strong>To make a withdrawal from your account, login and follow these steps:</strong></p>
            <ol>
                <li>Select the Cashier or Deposit link_</li>
                <li>Click the “Payout” or “withdraw” tab or link_</li>
                <li>Complete the withdrawal request. Be sure to include the dollar amount of your withdrawal_</li>
            </ol>

        <p>Your withdrawal request will be processed and your money will be on your way to you in approximately 2-3 business days.</p>
    </div></div>

<h2>More about <?php echo $host; ?></h2>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa22">Who is <?php echo $host; ?>?</h3></div>
    <div class="panel-collapse collapse" id="qa22">
        <p>At <?php echo $host; ?> (gohorsebetting.com), you can wager on horses with your computer or mobile device. </p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa23">What distinguishes you from other account-wagering providers?</h3></div>
    <div class="panel-collapse collapse" id="qa23">
        <p><?php echo $host; ?> is committed to providing the best and most secure experience to its members. Your satisfaction is our number one priority.</p>

        <p><?php echo $host; ?> features content from premier racetracks along with great bonuses.  Signup bonuses, Triple Crown bonuses and reload (deposit) bonuses are all part of what makes <?php echo $host; ?> one of the best online racebooks in the industry.</p>
    </div></div>

<div class="panel panel-default">
            <div class="panel-heading">
<h3 data-parent="#accordion" data-toggle="collapse" href="#qa24">Is there a responsible wagering program?</h3></div>
    <div class="panel-collapse collapse" id="qa24">
        <p><?php echo $host; ?> supports responsible wagering by its members. </p>
    </div></div>

</div>

