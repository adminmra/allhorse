<p>If you have questions, please send us an email about your racing account to</p>
<p><a href="mailto:support@myracingaccount.com?to=&subject=My%20Account%20at%20Go%20Horse%20Betting">support@myracingaccount.com</a></p>
<p>and we'll get back to you promptly.  If you need immediate service, please call us at:</p>
<p><a href="tel:18444422387">1-844-GHB-BETS  (1-844-442-2387)</a>.
</p>