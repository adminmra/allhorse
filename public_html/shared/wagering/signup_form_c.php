<?php require 'signup_form_misc/us_states.php';?>
<script src="//code.$.com/$-1.10.2.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<?php require_once "/home/ah/allhorse/public_html/shared/wagering/signup_form_misc/js_scripts_c.php";?>
<?php
$tel = "";
$call = "";
$logoimg = "";
$originalUrl ="";
$referringpage ="";

if(isset($_SERVER['HTTP_REFERER'])) {  
	$referringpage = $_SERVER['HTTP_REFERER'];
	$referringpage = parse_url($referringpage, PHP_URL_HOST); 
}else{
	$originalUrl = "default";
}

$ref = null;
if (isset($_GET['ref'])) {
    $ref = $_GET['ref'];
}
$raf = null;
if (isset($_GET['raf'])) {
    $raf = $_GET['raf'];
}
$promo_code = null;
if (isset($_GET['promo_code'])) {
    $promo_code = $_GET['promo_code'];
}


if($referringpage == "www.betusracing.ag"){
	$originalUrl = "BUSR_".$ref;
}

if($referringpage == "m.betusracing.ag"){
	$originalUrl = "M_BUSR_".$ref;
}

if($referringpage == "www.usracing.com"){
	$originalUrl = "USR_".$ref;
}

if($referringpage == "ww.allhorseracing.ag"){
	$originalUrl = "AHR_".$ref;
}

if($referringpage == "www.gohorsebetting.com"){
	$originalUrl = "GHB_".$ref;
}

if ($_SERVER['HTTP_HOST'] == 'www.betusracing.ag') {
    $sitename = 'bet &#9733; usracing';
    $tel = '1-844-BET-HORSES (1-844-238-4677)';
    $logoimg = '/public/assets/images/busr_logo_images/busr_new_logo/busr.png';
    $call = "1-844-238-4677";
} elseif ($_SERVER['HTTP_HOST'] == 'www.allhorseracing.ag') {
    $sitename = 'All Horse Racing';
    $tel = '1-844-777-BETS (1-844-777-2387)';
    $logoimg = '/themes/images/ahr-logo.png';
    $call = "1-844-777-BETS";
} else {
    $sitename = 'Go Horse Betting';
    $tel = '1-844-GHB-BETS (1-844-442-2387)';
    $logoimg = '/SportNewsTemplate/img/logo.png';
    $call = "1-844-442-2387";
}?>
<form class="form" id="signupForm" method="post" action="">
	<section class="signup">
		<div class="container">
			<!-- Modal -->
			<div id="existing_account_modal" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div class="modal-body">
							<p><img src="<?php echo $logoimg; ?>" ></p>
							<p> Hi there.  Our records indicate that you already have an account with us. Account <span id="alias_ghb"></span> is registered to the email address you just entered. <br /><br />  Please Login below or select  Forgot Password to reset your password.</p>

							<!--<form class="navbar-form" style="margin-left:0; margin-right:0;" action="/login" method="post" id="loginform" name="loginform" role="form">
								<input type="hidden" style="width:100%;" name="" value="">
								<input type="submit" style="width:100%;" class="btn btn-login" value="Login" name="">
							</form>-->
							<div class="navbar-form">
								<a class="btn btn-login" style="width:100%;" href="/login">Login</a>
							</div>
							<div>
								<a class="btn btn-forgot" style="width:100%;" href="/contact-us">Forgot password</a>
							</div>
							<a class="btn-tel" href="<?php echo $call; ?>" style="text-align:center;">Or, you can call <br/><div id="call_representative"> <?php echo $tel; ?> to speak with representative. </div></a>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="signup_form">
				<input type="hidden" value="" name="lid" id="lid">
				<input type="hidden" value="" name="signup_referer" >
				<input type="hidden" value="<?php echo $ref; ?>" name="ref">
				<input type="hidden" value="<?php echo $raf; ?>" name="raf">
				<!--
				<input type="hidden" value="<?php echo $promo_code; ?>" name="promo_code" id="promo_code">
				:v
			-->
				<input type="hidden" value="<?php if (empty($promo_code)){$promo_code = $originalUrl;}echo $promo_code;?>" name="promo_code" id="promo_code">


				<div class="form_row">
					<div class="form_col-50">
						<div class="row_box">
							<div class="form_field">
								<label for="first_name" class="label">First Name</label>
								<input class="autopost" id="first_name"  maxlength="30" name="first_name" title="Enter your First Name here" type="text" placeholder="First name" value="">
							</div>
							<div id="name_result" class="result"></div>
						</div>
						<span id="first_name_error" class="validTD msg-hidden">Please provide first name</span>
					</div>
					<div class="sep_1">&nbsp;</div>
					<div class="form_col-50">
						<div class="row_box">
							<div class="form_field">
								<label for="last_name" class="label">Last Name</label>
								<input class="autopost" id="last_name"  maxlength="30" name="last_name" title="Enter your Last Name here" type="text" placeholder="Last name" value="">
							</div>
							<div id="last_name_result" class="result"></div>
						</div>
						<span id="last_name_error" class="validTD msg-hidden">Please provide last name</span>
					</div>
				</div>
				<div class="form_row">
					<div class="form_col-50">
						<div class="row_box">
							<div class="form_field">
								<label for="password" class="label">Password (6 - 10 chars)</label>
								<?php include '/home/ah/allhorse/public_html/shared/wagering/password_eye.php';?>
								<input class="autopost" placeholder="Password (6 - 10 chars)" maxlength="11" name="password"
									value="" id="password" title="The password must contain 6 or more characters" type="password">
							</div>
							<div id="password_result" class="result"></div>
						</div>
						<span id="password_error" class="validTD msg-hidden">Password must have between 6 and 10 characters</span>
						<span id="password_invalidchar" class="validTD msg-hidden">Special characters are not allowed to be used on the password field</span>
					</div>
					<div class="sep_1">&nbsp;</div>
					<div class="form_col-50">
						<div class="row_box">
							<div class="form_field">
								<label for="primary_phone" class="label">Mobile Phone Only</label>
								<input class="autopost" placeholder="Mobile Phone Only" minlength="7" maxlength="13" name="primary_phone" id="primary_phone" type="tel" value="">
							</div>
							<div id="phone_result" class="result"></div>
						</div>
						<span id="primary_phone_error" class="validTD msg-hidden">Please provide a valid phone number</span>
					</div>
				</div>
				<div class="form_row">
					<div class="form_col-50">
						<div class="row_box">
							<div class="form_field">
								<label for="primary_email" class="label">Email Address</label>
								<input class="autopost" placeholder="Email Address"  maxlength="50" name="primary_email" id="primary_email" type="text" value="">
							</div>
							<div id="email_result" class="result"></div>
						</div>
						<span id="primary_email_error" class="validTD msg-hidden">Please provide a valid email</span>
						<!-- <span id="primary_email_error_white_space" class="validTD msg-hidden">Please check your email address again</span> -->
					</div>
					<div class="form_col-50">
						<div class="row_box">
							<div class="form_field">
								<label for="address_postal_code" class="label">Zip or postal code (Optional)</label>
								<input class="autopost" maxlength="15" name="address_postal_code" id="address_postal_code" title="Enter Zip/Postal Code here." placeholder="Zip or postal code (Optional)" type="text" value="">
							</div>
							<div id="zip_result" class="result"></div>
						</div>
						<span id="address_postal_code_error" class="validTD msg-hidden">Please provide a zip code</span>
						<span id="address_zip_code_error" class="validTD msg-hidden">Invalid zip code</span>
					</div>
				</div>
                <!--
				<div class="form_row">
					<div class="form_col-100">
						<div class="form_field">
							<input class="autopost" placeholder="Retype Email Address"  maxlength="50" name="email_confirm" id="email_confirm_su"  type="text" value="">
						</div>
						<span id="email_confirm_su_error" class="validTD msg-hidden">The email addresses you entered don't match</span>
					</div>
				</div>
                -->
				<div class="form_row hided_form">
					<label class="form_label">Date of Birth:</label>
					<div class="form_col-100">
						<div id="div_birth_date" class="form_row form_row-birth">
							<div class="form_col-33-3">
								<div class="form_field">
									<div class="form_select">
										<select name="dob_month" id="dob_month" class="autopost">
											<option value="">Month</option>
											<?php
for ($x = 1; $x <= 12; $x++) {
    $zx = sprintf("%02d", $x);
    $attrs = "";
    echo "<option value='{$zx}' {$attrs}>{$x}</option>";
}?>
										</select>
									</div>
								</div>
							</div>
							<div class="sep_1">&nbsp;</div>
							<div class="form_col-33-3">
								<div class="form_field">
									<div class="form_select">
										<select name="dob_day" id="dob_day" class="autopost">
											<option value="" >Day</option>
											<?php
for ($x = 1; $x <= 31; $x++) {
    $zx = sprintf("%02d", $x);
    $attrs = "";
    echo "<option value='{$zx}' {$attrs}>{$x}</option>";
}?>
										</select>
									</div>
								</div>
							</div>
							<div class="sep_1">&nbsp;</div>
							<div class="form_col-33-3">
								<div class="form_field">
									<div class="form_select">
										<select name="dob_year" id="dob_year" class="autopost">
											<option value="">Year</option>
											<?php
for ($x = date("Y") - 18; $x >= 1900; $x--) {
    $attrs = "";
    echo "<option value='{$x}' {$attrs}>{$x}</option>";
}?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<center>
						<span id="birth_date_error" class="validTD msg-hidden">Please provide a valid birth date</span>
						</center>
					</div>
				</div>
				<div class="form_row hided_form">
					<div class="form_col-66">
						<div class="form_field">
							<div class="form_select">
								<select class="autopost" name="address_country" id="address_country" title="Select your country of residence here."  onChange="printStateMenu(this.value,'');">
									<?php
foreach ($us_states as $state_code => $state_name) {
    $attrs = "";
    echo "<option value='{$state_code}' {$attrs}>{$state_name}</option>";
}
?>
								</select>
							</div>
						</div>
						<span id="address_country_error" class="validTD msg-hidden">Please provide a country</span>
					</div>
					<div class="sep_1">&nbsp;</div>
					<!-- <div class="form_col-33">
						<div class="form_field">
							<label for="address_postal_code" class="label">Zip or postal code</label>
							<input class="autopost" maxlength="15" name="address_postal_code" id="address_postal_code" title="Enter Zip/Postal Code here." placeholder="Zip or postal code" type="text" value="">
						</div>
						<span id="address_postal_code_error" class="validTD msg-hidden">Please provide a zip code</span>
						<span id="address_zip_code_error" class="validTD msg-hidden">Invalid zip code</span>
					</div> -->
				</div>
				<!-- <div class="form_row">
					<div class="form_col-100">
						<div class="form_field">
							<input class="autopost" maxlength="180" name="address_street" id="address_street"  value="" placeholder="Street address" title="Please provide your address of residence here." type="text" >
						</div>
						<span id="address_street_error" class="validTD msg-hidden">Please provide an address</span>
					</div>
				</div> -->
				<div class="form_row hided_form">
					<div class="form_col-50">
						<div class="form_field">
							<div class="form_select">
							<select class="autopost" name="address_state" id="address_state" title="Select your State here."></select>
							<script>printStateMenu('US','');</script>
						</div>
					</div>
					<span id="address_state_error" class="validTD msg-hidden">Please provide a state</span>
				</div>
				<div class="sep_1">&nbsp;</div>
				<div class="form_col-50">
					<div class="form_field">
						<input class="autopost" id="address_city"  maxlength="100" name="address_city"  title="Please provide your city of residence here." type="text" placeholder="City" value="">
					</div>
					<span id="address_city_error" class="validTD msg-hidden">Please provide a city</span>
				</div>
			</div>
			<!-- <div class="form_row">
				<label class="form_label">Security Question:</label>
				<div class="form_col-50">
					<div class="form_select">
						<select class="autopost" name="security_question" id="security_question" title="Choose your Security Question here">
							<option value="Mothers maiden name">Mothers maiden name</option>
							<option value="Favorite hobby">Favorite hobby</option>
							<option value="Favorite club">Favorite club</option>
							<option value="Favorite book">Favorite book</option>
							<option value="Childhood hero">Childhood hero</option>
							<option value="Best friends name">Best friends name</option>
							<option value="My pet">My pet</option>
							<option value="My nickname">My nickname</option>
							<option value="My first car">My first car</option>
							<option value="My secret code">My secret code</option>
						</select>
					</div>
					<span id="security_question_error" class="validTD msg-hidden">Please provide a security question</span>
				</div>
				<div class="sep_1">&nbsp;</div>
				<div class="form_col-50">
					<div class="form_field">
						<input class="autopost" maxlength="50" name="security_answer" id="security_answer" title="Enter your Security Asnwer here" type="text" placeholder="Security answer" value="">
					</div>
					<span id="security_answer_error" class="validTD msg-hidden">Please provide a security answer</span>
				</div>
			</div> -->
		</div>
	</div>
</section>
<section class="infos">
	<div class="signup_form">
		<div class="form_row">
			<div class="form_col-50">
				<div class="form_field">
					<p class="form_note"><input id="subscribe" name="subscribe_email" value="1" checked="checked" type="checkbox"> Send me <?php echo $sitename; ?> promotional offers <!-- Send me promotional offers and Newsletters from <?php //echo $sitename; ?>--></p>
					<label id="subscribe_label" for="subscribe"></label>
				</div>
			</div>
			<div class="sep_1">&nbsp;</div>
			<div class="form_col-50">
				<div class="form_field">
					<p class="form_note"><input id="subscribe_sms" name="subscribe_sms" value="1" checked="checked" type="checkbox"> Send me horse racing/handicapping information<!-- Send me exclusive offers and account notifications by SMS. --></p>
					<label id="subscribe_sms_label" for="subscribe_sms"></label>
				</div>
			</div>
		</div>
		<div class="form_row">
			<div class="form_col-100">

				<div class="form_btns">
					
					<center>
						<?php // require_once('/home/ah/allhorse/public_html/shared/wagering/captcha.php'); ?>
					</center>

					<center id="loading_ajax" style="display: none">
					<img src="https://documentation.devexpress.com/HelpResource.ashx?help=WPF&document=img118763.jpg" />
					</center>
					<br />
					<button type="submit" class="form_submit" id="send_signup_form_fer" onclick="return signupformbet('#signupForm')" >Create My Account</button>
				</div>
			</div>
		</div>
		<div class="form_row">
			<div class="form_col-100">
				<!-- <p class="form_note">* To receive your winnings we will need your verifiable phone number, email address and billing address. </p> -->
				<p class="form_note">* To receive your winnings we will need to verify your cell phone number, email and billing address.</p>

			</div>
		</div>
		<span id="promo_code_error" class="validTD msg-hidden"></span>
		<input type="hidden" name="postdata" value="1" >
		<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
		</fb:login-button>
	</div>
	<div class="container-fluid">
		<div class="info"><span class="info_icon plus18"></span>
		<p><?php echo $sitename; ?> only accepts players over 18 years old.</p>
	</div>
	<div class="info"><span class="info_icon protected"></span>
	<p>Your personal information is safe and every transaction is protected with 256-bit encryption to protect your information.</p>
</div>
<div class="info"><span class="info_icon agree"></span>
<p>By creating an account you agree to our Terms & Conditions.</p>
</div>
</div>
<!-- <div class="container-fluid"><img src="/public/assets/images/friends.png" alt="" class="friends_img"></div> -->
</section>
</form>

<style>
	.label {
		color: #fff;
		font-family: "Lato", Arial, "Helvetica Neue", Helvetica, sans-serif;
		font-weight: 400;
		line-height: 1;
		position: absolute;
		bottom: 5rem;
		opacity: 0;
	}

	.hided_form {
		display: none;
	}

	.form input[type="tel"] {
		width: 100%;
		color: #414141;
		border-radius: 3px;
		border: solid #AAAAAA 1px;
		text-indent: 5px;
		font-size: 14px;
		padding: 15px 17px;
		height: auto;
	}

	.success {
		height: auto;
	}

	.form input[type="tel"] {
		min-height: 35px;
	}

	.row_box {
		display: flex;
		justify-content: space-between;
		align-items: center;
	}

	@media (min-width: 1418px) {
		.form input[type="tel"] {
			padding: 21px 17px;
			height: 60px;
		}
		.label {
			bottom: 6rem;
		}
	}

	.showPass {
		left: 88%;
		top: 32%;
		color: #333832;
	}
</style>
<script>
	$(document).ready(() => {
		$("#first_name").on("input", (e) => {
			manageLabel(e.currentTarget);
			validateFirstname();
		});
		$("#last_name").on("input", (e) => {
			manageLabel(e.currentTarget);
			validateLastname();
		});
		$("#password").on("input", (e) => {
			manageLabel(e.currentTarget);
			validatePassword();
		});
		$("#primary_phone").on("input", (e) => {
			manageLabel(e.currentTarget);
			validatePhone();
		});
		$("#primary_email").change((e) => {
			manageLabel(e.currentTarget);
			checkEmail();
		});
		$("#address_postal_code").on("input", (e) => {
			manageLabel(e.currentTarget);
			validateZipcode();
		});
	});

	function validateFirstname() {
		if ($("#first_name").val() == "") {
			$("#first_name").parent().addClass("error");
			$("#first_name").parent().removeClass("success");
			$("#name_result").addClass("error");
			$("#name_result").removeClass("success");
			$("#first_name_error").addClass('msg-error');
			$("#first_name_error").removeClass('msg-hidden');
		} else {
			$("#first_name").parent().addClass("success");
			$("#first_name").parent().removeClass("error");
			$("#name_result").addClass("success");
			$("#name_result").removeClass("error");
			$("#first_name_error").addClass('msg-hidden');
			$("#first_name_error").removeClass('msg-error');
		}
	}

	function validateLastname() {

		if ($("#last_name").val() == "") {
			$("#last_name").parent().addClass("error");
			$("#last_name").parent().removeClass("success");
			$("#last_name_result").addClass("error");
			$("#last_name_result").removeClass("success");
			$("#last_name_error").addClass('msg-error');
			$("#last_name_error").removeClass('msg-hidden');
			error_flag = true;
		} else {
			$("#last_name").parent().addClass("success");
			$("#last_name").parent().removeClass("error");
			$("#last_name_result").addClass("success");
			$("#last_name_result").removeClass("error");
			$("#last_name_error").addClass('msg-hidden');
			$("#last_name_error").removeClass('msg-error');
		}
	}

	function manageLabel(element) {
		var label = $("label[for='" + element.id + "']");
		if (element.value.length > 0)
			label.animate({
				opacity: 1
			}, 100)
		else
			label.animate({
				opacity: 0
			}, 100)
	}

	function validatePhone() {
		var primary_phone = $("#primary_phone").val();
		    primary_phone = primary_phone.replace(/[^0-9]/g, '');
		    $('#primary_phone').val(primary_phone);

            if( primary_phone == "" || !(primary_phone.length >= 7 && primary_phone.length <= 13)){
                $("#primary_phone").parent().addClass("error");
                $("#primary_phone").parent().removeClass("success");
				$("#phone_result").addClass("error");
				$("#phone_result").removeClass("success");
                $("#primary_phone_error").addClass('msg-error');
                $("#primary_phone_error").removeClass('msg-hidden');
            } else {
                $("#primary_phone").parent().addClass("success");
                $("#primary_phone").parent().removeClass("error");
				$("#phone_result").addClass("success");
				$("#phone_result").removeClass("error");
                $("#primary_phone_error").addClass('msg-hidden');
                $("#primary_phone_error").removeClass('msg-error');
            }
	}

	function validatePassword() {
		var password = $("#password").val();
		var regex = /[.*+?^${}#"-+*#@!/,-/ /([´'=\°~Ö}._?${:;¨¬^Ó)|[\]\\]/g;
		if(password != "") {
			if (password.match(regex)) {
				$("#password").parent().addClass("error");
                $("#password").parent().removeClass("success");
				$("#password_result").addClass("error");
				$("#password_result").removeClass("success");
				$("#password_invalidchar").addClass('msg-error');
				$("#password_invalidchar").removeClass('msg-hidden');
				$("#password_error").addClass('msg-hidden');
				$("#password_error").removeClass('msg-error');
			} else if(!(password.length >= 6 && password.length <= 10)) {
				$("#password").parent().addClass("error");
                $("#password").parent().removeClass("success");
				$("#password_result").addClass("error");
                $("#password_result").removeClass("success");
                $("#password_error").addClass('msg-error');
                $("#password_error").removeClass('msg-hidden');
                $("#password_invalidchar").addClass('msg-hidden');
                $("#password_invalidchar").removeClass('msg-error');
			} else {
				$("#password").parent().addClass("success");
                $("#password").parent().removeClass("error");
				$("#password_result").addClass("success");
				$("#password_result").removeClass("error");
				$("#password_error").addClass('msg-hidden');
				$("#password_error").removeClass('msg-error');
				$("#password_invalidchar").addClass('msg-hidden');
				$("#password_invalidchar").removeClass('msg-error');
			}
		} else {
			$("#password").parent().addClass("success");
            $("#password").parent().removeClass("error");
			$("#password_result").addClass("success");
			$("#password_result").removeClass("error");
			$("#password_error").addClass('msg-hidden');
			$("#password_error").removeClass('msg-error');
			$("#password_invalidchar").addClass('msg-hidden');
			$("#password_invalidchar").removeClass('msg-error');
		}	
	}

	function checkEmail() {
		var email = $("#primary_email").val();
		if (email != "") {
			if (validateEmail(email)) {
				$("#primary_email").parent().addClass("success");
				$("#primary_email").parent().removeClass("error");
				$("#email_result").addClass("success");
				$("#email_result").removeClass("error");
				$("#primary_email_error").addClass('msg-hidden');
				$("#primary_email_error").removeClass('msg-error');
			} else {
				$("#primary_email").parent().addClass("error");
				$("#primary_email").parent().removeClass("success");
				$("#email_result").addClass("error");
				$("#email_result").removeClass("success");
				$("#primary_email_error").addClass('msg-error');
				$("#primary_email_error").removeClass('msg-hidden');
			}
		} else {
			$("#primary_email").parent().removeClass("success");
			$("#primary_email").parent().removeClass("error");
			$("#email_result").removeClass("success");
			$("#email_result").removeClass("error");
			$("#primary_email_error").addClass('msg-hidden');
			$("#primary_email_error").removeClass('msg-error');
		}
	}

	function validateZipcode() {
		if ($("#address_postal_code").val() == "") {
            $("#address_postal_code").parent().addClass("success");
			$("#address_postal_code").parent().removeClass("error");
			$("#zip_result").addClass("success");
			$("#zip_result").removeClass("error");
			$("#address_postal_code_error").addClass('msg-hidden');
			$("#address_postal_code_error").removeClass('msg-error');
			$("#address_zip_code_error").addClass('msg-hidden');
			$("#address_zip_code_error").removeClass('msg-error');
        } else if(!validatePostalCode($("#address_postal_code").val(), $("#address_country").val())) {
            $("#address_postal_code").parent().addClass("error");
            $("#address_postal_code").parent().removeClass("success");
            $("#address_postal_code_error").addClass('msg-hidden');
			$("#zip_result").addClass("error");
			$("#zip_result").removeClass("success");
            $("#address_postal_code_error").removeClass('msg-error');
            $("#address_zip_code_error").addClass('msg-error');
            $("#address_zip_code_error").removeClass('msg-hidden');
        } else {
            $("#address_postal_code").parent().addClass("success");
            $("#address_postal_code").parent().removeClass("error");
			$("#zip_result").addClass("success");
			$("#zip_result").removeClass("error");
            $("#address_postal_code_error").addClass('msg-hidden');
            $("#address_postal_code_error").removeClass('msg-error');
            $("#address_zip_code_error").addClass('msg-hidden');
            $("#address_zip_code_error").removeClass('msg-error');
        }
	}
</script>