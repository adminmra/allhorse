<?php require_once "/home/ah/allhorse/public_html/shared/wagering/signup_form_misc/js_scripts.php"; ?>
<div class="login">
<form action="" method="post" id="login-form" name="loginform" role="form" novalidate="novalidate">
        <div class="login_top">
           <img src="/public/assets/images/busr_logo_images/busr_new_logo/busr.png" alt="" class="login_logo" style="display: none;">
          <h1 class="login_title">Enter your login and password</h1>
          <div id="login_error_msg"></div>
          <div class="login_form">
            <input value="Submit" name="ctl00$MainContent$btnLogin" type="hidden">
            <input value="93" name="ctl00$MainContent$IdBook" type="hidden">
            <div class="login_field">
              <input type="text" placeholder="Email Address or Account Number" name="username" id="login_name" required="" tabindex="99" aria-required="true" value="">
              <input id="account_name" type="hidden" name="ctl00$MainContent$Account" value="">
            </div>
            <div class="login_field">
              <input type="password" placeholder="Password" name="password" id="password" required="" tabindex="100" aria-required="true">
              <input type="hidden" placeholder="Password" maxlength="10" name="ctl00$MainContent$Password" class="form-control-login sign-in-password">
            </div>
            <div class="login_info">
              <label for="remember_account_checkbox" class="login_save">
                <input type="checkbox" name="remember_account" id="remember_account_checkbox" value="DoRemember" class="login_checkbox-hide">
                <!--<span class="login_checkbox"></span>-->
                <span class="login_checkbox-label">Save my information</span>
              </label>
              <a href="/forgot-password" onclick="" class="login_forgot">Forgot password?</a>
            </div>
          </div>
        </div>
        <div class="login_bottom">
                    <center>
            <?php //require_once('/home/ah/allhorse/public_html/shared/wagering/captcha.php'); ?>
          </center>
          <button type="submit" class="login_btn login_btn-blue" onclick="">Login</button>
          <span class="login_or">
            <span class="text">OR </span>
          </span>
          <span class="login_text">Don't have an account?</span><a href="/signup" onclick="_redirect_signup()" class="login_btn login_btn-red">Join Now</a>
        </div>
      </form>
</div>

