<?php

if ( strpos($_SERVER["HTTP_HOST"], 'allhorseracing') !== false ) {
    $host = "<i>All Horse Racing</i>";
}elseif ( strpos($_SERVER["HTTP_HOST"], 'gohorsebetting') !== false ) {
    $host = "<i>Go Horse Betting</i>";
}
elseif ( strpos($_SERVER["HTTP_HOST"], 'betusracing') !== false ) {
    $host = "<strong>BUSR</strong>";
}

?>

<!-- <h1>Terms and Conditions</h1> -->

<h3>WEBSITE TERMS OF USE</h3>

<div>Use of this website by yourself constitutes your agreement to the Terms of Use.</div>
<div>By using this site, you signify your Assent and Agreement to these Terms of Use. If you do not agree to these Terms of Use, do not use the website, services and products.</div>
<div>Please make sure you read and understand the following Terms of Use before using your account on this website. We wish you the best of luck and thank you for choosing <?php echo $host; ?>!</div>
<br/>
<div>Residents from the following countries CANNOT open an account:<br>
  <br>
  Afghanistan, Albania, Algeria, Anguilla, Antigua and Barbuda, Aruba, Azerbaijan, Bahrain, Bangladesh, Belarus, Belgium, Bhutan, Bonaire, Sint Eustatius and Saba, Bouvet Island, Brunei Darussalam, Bulgaria, Burkina Faso, Burundi, Cambodia, Cayman Islands, Central African Republic, China, Comoros, Congo, Costa Rica, Cuba, Curacao, Cyprus, Denmark, Djibouti, Dominican Republic, Eritrea, Estonia, Ethiopia, Fiji, Finland, France, Gambia, Gibraltar, Guadeloupe, Guernsey, Guinea-Bissau, holy see (Vatican City State), Hungary, India, Indonesia, Iran, Iraq, Ireland, Isle of Man, Israel, Jersey, Kazakhstan, Korea, democratic people&rsquo;s republic of Korea, republic of, Kyrgyzstan, Lebanon, Liberia, Libya, Maldives, Mali, Malta, Martinique, Mauritania, Mauritius, Mayotte, Montenegro, Montserrat, Morocco, Nauru, Nepal, Netherlands, New Caledonia, Niger, Oman, Pakistan, Palestine, state of, Papua New Guinea, Pitcairn, Poland, Réunion, Russian federation, Rwanda, Saint Helena, Ascension and Tristan da Cunha, St martin (French  part), Saint Pierre and Miquelon, Sao Tome and Principe, Saudi Arabia, Serbia, Seychelles, Sierra Leone, Somalia, South Sudan, Spain, Sudan, Svalbard and Jan Mayen, Syrian Arab Republic, Tajikistan, turkey, Turkmenistan, Tuvalu, Uganda, United Arab Emirates, Uzbekistan, Vanuatu, Vietnam, Wallis and Futuna, Yemen.<br>
  <br>
  <?php echo $host; ?> holds the right to update or modify the list of restricted countries, at any moment and without any previous notification.</div>
<div>Some countries not appearing on the list may be subject to additional restrictions, such Deposit and Payout options and limits.<br>
</div>
<h3>Restrictions on Use of Materials</h3>
<div>Materials in this website are Copyrighted and all rights are reserved. Text, graphics, databases, HTML code, and other intellectual property are protected by International Copyright Laws, and may not be copied, reprinted, published, reengineered, translated, hosted, or otherwise distributed by any means without explicit permission. All of the trademarks on this site are trademarks of <?php echo $host; ?> or of other owners used with their permission.</div>
<br/>

<h3>Database Ownership, License, and Use</h3>
<div><?php echo $host; ?> warrants, and you accept, that we own of the copyright of the Databases of Links to articles and resources available from time to time through <?php echo $host; ?> and its contributors reserve all rights and no intellectual property rights are conferred by this agreement.</div>
<div><?php echo $host; ?> grants you a non-exclusive, non-transferable license to use database(s) accessible to you subject to these Terms and Conditions. The database(s) may be used only for viewing information or for extracting information to the extent described below.</div>
<div>You agree to use information obtained from <?php echo $host; ?> databases only for your own private use or the internal purposes of your home or business, provided that is not the selling or broking of information, and in no event cause or permit to be published, printed, downloaded, transmitted, distributed, reengineered, or reproduced in any form any part of the databases (whether directly or in condensed, selective or tabulated form) whether for resale, republishing, redistribution, viewing, or otherwise.</div>
<div>Nevertheless, you may on an occasional limited basis download or print out individual pages of information that have been individually selected, to meet a specific, identifiable need for information which is for your personal use only, or is for use in your business only internally, on a confidential basis. You may make such limited number of duplicates of any output, both in machine-readable or hard copy form, as may be reasonable for these purposes only. Nothing herein shall authorize you to create any database, directory or hard copy publication of or from the databases, whether for internal or external distribution or use.</div>
<br/>

<h3>Liability</h3>
<div>The materials in this site are provided “as is” and without warranties of any kind either express or implied. <?php echo $host; ?> disclaims all warranties, express or implied, including, but not limited to, implied warranties of merchantability and fitness for a particular purpose. <?php echo $host; ?> does not warrant that the functions contained in the materials will be uninterrupted or error-free, that defects will be corrected, or that this site or the server that makes it available are free of viruses or other harmful components. <?php echo $host; ?> does not warrant or make any representations regarding the use or the results of the use of the materials in this site in terms of their correctness, accuracy, reliability, or otherwise. You (and not <?php echo $host; ?>) assume the entire cost of all necessary servicing, repair or correction. Applicable law may not allow the exclusion of implied warranties, so the above exclusion may not apply to you.</div>
<div>Under no circumstances, including, but not limited to, negligence, shall <?php echo $host; ?> be liable for any special or consequential damages that result from the use of, or the inability to use, the materials in this site, even if <?php echo $host; ?> or a <?php echo $host; ?> authorized representative has been advised of the possibility of such damages. Applicable law may not allow the limitation or exclusion of liability or incidental or consequential damages, so the above limitation or exclusion may not apply to you. In no event shall <?php echo $host; ?> total liability to you for all damages, losses, and causes of action (whether in contract, tort, including but not limited to, negligence or otherwise) exceed the amount paid by you, if any, for accessing this site.</div>
<div>Facts and information at this website are believed to be accurate at the time they were placed on the website. Changes may be made at any time without prior notice. All data provided on this website is to be used for information purposes only. The information contained on this website and pages within, is not intended to provide specific legal, financial or tax advice, or any other advice, whatsoever, for any individual or company and should not be relied upon in that regard. The services described on this website are only offered in jurisdictions where they may be legally offered. Information provided in our website is not all-inclusive, and is limited to information that is made available to <?php echo $host; ?> and such information should not be relied upon as all-inclusive or accurate.</div>
<br/>

<h3>Links and Marks</h3>
<div>The owner of this site is not necessarily affiliated with sites that may be linked to this site and is not responsible for their content. The linked sites are for your convenience only and you access them at your own risk. Links to other websites or references to products, services or publications other than those of <?php echo $host; ?> and its subsidiaries and affiliates at this website, do not imply the endorsement or approval of such websites, products, services or publications by <?php echo $host; ?> or its subsidiaries and affiliates.</div>
<div>Certain names, graphics, logos, icons, designs, words, titles or phrases at this website may constitute trade names, trademarks or service marks of <?php echo $host; ?> or of other entities. The display of trademarks on this website does not imply that a license of any kind has been granted. Any unauthorized downloading, re-transmission, or other copying of modification of trademarks and/or the contents herein may be a violation of federal common law trademark and/or copyright laws and could subject the copier to legal action.  If you wish to contact us about a trademark or copyright issue, please contact us.</div>
<br/>

<h3>Confidentiality of Codes, Passwords and Information</h3>
<div>You agree to treat as strictly private and confidential any Subscriber Code, username, user ID, or password which you may have received from <?php echo $host; ?>, and all information to which you have access through password-protected areas of <?php echo $host; ?> websites and will not cause or permit any such information to be communicated, copied or otherwise divulged to any other person whatsoever.</div>
<br/>

<h3>Domains</h3>
<div>These Terms of Use will apply to every access to <?php echo $host; ?> (gohorsebetting.com) and any variant of that top level domain name. We reserve the right to issue revisions to these Terms of Use by publishing a revised version of this document on this site: that version will then apply to all use by you following the date of publication. Each access of information from <?php echo $host; ?> will be a separate, discrete transaction based on the then prevailing terms.</div>
<div>This Terms of Use and the license granted may not be assigned or sublet by You without <?php echo $host; ?> written consent in advance.</div>
<div>These Terms of Use shall be governed by, construed and enforced in accordance with the laws of the country that <?php echo $host; ?> shall assign, including but not limited to those of Costa Rica as it is applied to agreements entered into and to be performed entirely within such jurisdictions.</div>
<div>To the extent you have in any manner violated or threatened to violate <?php echo $host; ?> and/or its affiliates’ intellectual property rights, <?php echo $host; ?> and/or its affiliates may seek injunctive or other appropriate relief in any court in the Countries of Costa Rica, and you consent to exclusive jurisdiction and venue in such courts as <?php echo $host; ?> stipulates.</div>

<div>Any other disputes will be resolved as follows:</div>
<div>If a dispute arises under this agreement, we agree to first try to resolve it with the help of a mutually agreed-upon mediator in the following choice by <?php echo $host; ?> among the following countries: Costa Rica. Any costs and fees other than attorney fees associated with the mediation will be shared equally by each of us.</div>
<div>If it proves impossible to arrive at a mutually satisfactory solution through mediation, we agree to submit the dispute to binding arbitration at one of the following locations under their respective rules of arbitration: Costa Rica.  Judgment upon the award rendered by the arbitration may be entered in any court with jurisdiction to do so.</div>
<div>If any provision of this agreement is void or unenforceable in whole or in part, the remaining provisions of this Agreement shall not be affected thereby.</div>
<br/>

<h3>Termination</h3>
<div>These Terms of Use agreement are effective until terminated by either party. You may terminate this agreement at any time by destroying all materials obtained from any and all <?php echo $host; ?> and all related documentation and all copies and installations thereof, whether made under the terms of this agreement or otherwise. This agreement will terminate immediately without notice at <?php echo $host; ?> sole discretion, should you fail to comply with any term or provision of this agreement. Upon termination, you must destroy all materials obtained from this site and any and all other <?php echo $host; ?> site(s) and all copies thereof, whether made under the terms of this agreement or otherwise.</div>
