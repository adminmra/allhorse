<?php
date_default_timezone_set('America/New_York');
$cdate = date("Y-m-d H:i:s");
$pwcDateStart = "2020-01-20 00:00:00";
$pwcDateEnd = "2020-01-25 17:40:00";
$saudiDateStart = "2020-02-24 00:00:00";
$saudiDateEnd = "2020-02-29 12:40:00";

$refs = array(
	'racetrack'=>['ajax-downs','aqueduct','Arapahoe-Park','Arlington-Park','Assiniboia-Downs','Atlantic-City','Balmoral-Park','Batavia-Downs','Belmont-Park','Hollywood-Park','Buffalo-Raceway','Cal-Expo','Calder-Race-Course','Calder-Race-Course','Canterbury-Park','Charles-Town','Churchill-Downs','colonial-downs','Del-Mar','Delaware-Park','Delta-Downs','Dover-Downs','Ellis-Park','Emerald-Downs','Evangeline-Downs','Fair-Grounds','Fair-Grounds','Fair-Meadows','Fairmount-Park','Fairplex-Park','Ferndale','Finger-Lakes','Fonner-Park','Fort-Erie','Fraiser-Downs','Freehold-Raceway','Fresno','Golden-Gate-Fields','Golden-Gate-Fields','Gulfstream-Park','Harrahs-Philadelphia','Harrington-Park','Hastings-Park','Hastings-Park','Hawthorne-Park','Hawthorne-Race-Course','Hialeah-Park','Hollywood-Park','Hollywood-Slots-Raceway','Hoosier-Park','Indiana-Downs','Keeneland','Kentucky-Downs','Laurel-Park','Lone-Star-Park','Los-Alamitos','Louisiana-Downs','Maywood-Park','Meadowlands','meydan-racecourse','meydan-racecourse','Mohawk-Raceway','Monmouth-Park','Mountaineer-Park','Mountaineer-Park','Northfield-Park','Northlands-Park','Oaklawn-Park','Parks-Racing','Penn-National','Pimlico','Pleasanton','Pocono-Downs','Pompano-Park','Portland-Meadows','Prairie-Meadows','Presque-Isle-Downs','Raceway-Park','Remington-Park','Retama-Park','Rosecroft-Raceway','Ruidoso-Downs','Ruidoso-Quarter-Horse','Running-Aces','Sacramento','Sam-Houston-Race-Park','Santa-Anita','Santa-Rosa','Saratoga','Scioto-Downs','Stockton','Suffolk-Downs','Sunland-Park','Tampa Bay Downs','The-Meadows-Racetrack','The-Meadows-Racetrack','Tioga-Downs','Turf-Paradise','Turfway-Park','Vernon-Downs','Western-Fair-Raceway','Will-Rogers-Downs','Woodbine','Woodbine-Harness','Yonkers-Raceway','zia-park','racetracks','ajax-downs','aqueduct','arapahoe-park','arlington-park','assiniboia-downs','atlantic-city-race-course','balmoral-park','batavia-downs','belmont-park','buffalo-raceway','calder-race-course','cal-expo','canterbury-park','charles-town','churchill-downs','colonial-downs','delaware-park','del-mar','delta-downs','dover-downs','ellis-park','emerald-downs','evangeline-downs','fair-grounds-race-course','fair-meadows','fairmount-park','fairplex-park','ferndale','finger-lakes','fonner-park','fort-erie','fraser-downs','freehold-raceway','fresno','golden-gate-fields','gulfstream-park','harrahs-philadelphia','harrington','hastings-park','hawthorne-race-course','hialeah-park','hollywood-park','hollywood-slots-raceway','hoosier-park','indiana-downs','keeneland','kentucky-downs','laurel-park','lone-star-park','los-alamitos','louisiana-downs','maywood-park','meadowlands','mohawk-raceway','monmouth-park','mountaineer-park','northfield-park','northlands-park','oaklawn-park','parx-racing','penn-national','pimlico','pleasanton','pocono-downs','pompano-park','portland-meadows','prairie-meadows','presque-isle-downs','raceway-park','remington-park','retama-park','rosecroft-raceway','ruidoso-downs','ruidoso-quarter-horse','running-aces','sacramento','sam-houston-race-park','santa-anita','santa-rosa','saratoga','scioto-downs','stockton','suffolk-downs','sunland-park','tampa-bay-downs','the-meadows-racetrack','tioga-downs','turf-paradise','turfway-park','vernon-downs','western-fair-raceway','will-rogers-downs','woodbine','woodbine-harness','yonkers-raceway','zia-park','parx-racing','ag-racetracks'],
	'stake'=>['ascot-gold-cup','Diamond Jubilee Stakes','diamond-jubilee-stakes','grand-national','prix-de-larc-de-triomphe','queen-anne-stakes','Royal Ascot','arkansas-derby','arlington-million','ascot-gold-cup','autumn-miss-stakes','autumn-stakes','bluegrass-stakes','bold-ruler-handicap','cashcall-futurity','cigar-mile-handicap','commonwealth-cup','delta-downs-jackpot-stakes','delta-downs-princess-stakes','diamond-jubilee-stakes','discovery-handicap','ep-taylor-stakes','fall-highweight-handicap','florida-derby','fort-lauderdale-stakes','forward-gal-stakes','goldikova-stakes','gotham-stakes','grand-national','hagyard-fayette-stakes','haskell-stakes','hawthorne-gold-cup-handicap','holy-bull-stakes','hong-kong-cup','illinois-derby','jerome-stakes','kentucky-jockey-club-stakes','la-canada-stakes','lecomte-stakes','lexington-stakes','long-island-handicap','louisiana-derby','maple-leaf-stakes','monrovia-stakes','nashua-stakes','nearctic-stakes','ontario-fashion-stakes','pacific-classic','pattison-canadian-international','pennsylvania-derby','pharatoga-betting','prix-de-larc-de-triomphe','queen-anne-stakes','rachel-alexandra-stakes','raven-run-stakes','rebel-stakes','red-smith-handicap','remsen-stakes','ricoh-woodbine-mile','risen-star-stakes','royal-ascot','san-fernando-stakes','san-gabriel-stakes','san-pasqual-stakes','santa-anita-derby','santa-monica-stakes','santa-ynez-stakes','senator-ken-maddy-stakes','sham-stakes','spiral-stakes','strub-stakes','sword-dancer','tampa-bay-derby','tempted-stakes','travers-stakes','turnback-the-alarm-handicap','twilight-derby','uae-derby','whitney','wood-memorial'],
	'state'=>['Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New-Jersey','New Mexico','New York','North-Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode-Island','South-Carolina','South-Dakota','Tennessee','texas','Utah','Vermont','Virginia','Washington','West Virginia','Wyoming','Conneticut','New Jersey','North Carolina','Rhode Island','South Carolina','South Dakota'],
	'KD'=>['kentucky-derby-betting','kentucky-derby-contender-rankings','kentucky-derby-free-bet','kentucky-derby-future-wager','kentucky-derby-odds-jockeys','kentucky-derby','kentucky-derby-margin-of-victory','kentucky-derby-match-races','kentucky-derby-odds','kentucky-derby-prep-races','kentucky-derby-props','kentucky-derby-racing-schedule','kentucky-derby-results','kentucky-derby-odds-trainers','kentucky-derby-video','kentucky-derby-winners','ag-kentucky-derby-betting','kentucky-derby-contender','ag-kentucky-derby-contest','ag-kentucky-derby-free-bet','ag-kentucky-derby-future-wager','ag-kentucky-derby-odds-jockeys','ag-kentucky-derby-margin-of-victory','ag-kentucky-derby-match-races','ag-kentucky-derby-prep-races','ag-kentucky-derby-props','ag-kentucky-derby-results','ag-kentucky-derby-trainer-betting','ag-kentucky-derby-winners'],
	'KO'=>['kentucky-oaks-betting','kentucky-oaks','kentucky-oaks-odds','ag-kentucky-oaks-betting','ag-kentucky-oaks','ag-kentucky-oaks-odds'],
	'BC'=>['classic-top3-finishing-odds','ag-classic-top3-finishing-odds','breeders-cup-betting','breeders-cup','breeders-cup-challenge','breeders-cup-classic','breeders-cup-classic-results','breeders-cup-contenders','breeders-cup-dirt-mile','breeders-cup-dirt-mile-results','breeders-cup-distaff','breeders-cup-distaff-results','breeders-cup-filly-mare-sprint','breeders-cup-filly-mare-sprint-results','breeders-cup-filly-mare-turf','breeders-cup-filly-mare-turf-results','breeders-cup-free-bet','breeders-cup-juvenile','breeders-cup-juvenile-results','breeders-cup-juvenile-fillies','breeders-cup-juvenile-fillies-results','breeders-cup-juvenile-fillies-turf','breeders-cup-juvenile-fillies-turf-results','breeders-cup-juvenile-turf','breeders-cup-juvenile-turf-results','breeders-cup-juvenile-turf-sprint','breeders-cup-juvenile-turf-sprint-results','breeders-cup-mile','breeders-cup-mile-results','breeders-cup-odds','breeders-cup-races','breeders-cup-sprint-results','breeders-cup-sprint-odds','breeders-cup-turf-results','breeders-cup-turf-odds','breeders-cup-turf-sprint-results','breeders-cup-turf-sprint-odds','breeders-cup-winners','ag-breeders-cup-challenge','ag-breeders-cup-classic','ag-breeders-cup-classic-results','ag-breeders-cup-dirt-mile','ag-breeders-cup-dirt-mile-results','ag-breeders-cup-distaff','ag-breeders-cup-distaff-results','ag-breeders-cup-filly-mare-sprint','ag-breeders-cup-filly-mare-sprint-results','ag-breeders-cup-filly-mare-turf','ag-breeders-cup-filly-mare-turf-results','ag-breeders-cup-juvenile','ag-breeders-cup-juvenile-results','ag-breeders-cup-juvenile-fillies','ag-breeders-cup-juvenile-fillies-results','ag-breeders-cup-juvenile-fillies-turf','ag-breeders-cup-juvenile-fillies-turf-results','ag-breeders-cup-juvenile-sprint','ag-breeders-cup-juvenile-turf','ag-breeders-cup-juvenile-turf-results','ag-breeders-cup-juvenile-turf-sprint','ag-breeders-cup-juvenile-turf-sprint-results','ag-breeders-cup-mile','ag-breeders-cup-mile-results','ag-breeders-cup-odds','ag-breeders-cup-sprint-results','ag-breeders-cup-sprint','ag-breeders-cup-turf-results','ag-breeders-cup-turf','ag-breeders-cup-turf-sprint-results','ag-breeders-cup-turf-sprint','ag-breeders-cup-winners'],
	'BS'=>['belmont-stakes','belmont-stakes-betting','belmont-stakes-guide','belmont-stakes-contenders','belmont-stakes-free-bet','belmont-stakes-match-races','belmont-stakes-odds','belmont-stakes-prop-bets','belmont-stakes-results','belmont-stakes-video','belmont-stakes-winners','bet-on-belmont-stakes','ag-belmont-stakes-free-bet','ag-belmont-stakes-odds','ag-belmont-stakes-results','ag-belmont-stakes-winners'],
	'DWC'=>['dubai-world-cup-betting','dubai-world-cup','dubai-world-cup-odds','dubai-world-cup-results','dubai-world-cup-winners','ag-dubai-world-cup-betting','ag-dubai-world-cup-odds','ag-dubai-world-cup-results','ag-dubai-world-cup-winners','ag-dubai-world-cup-betting'],
	'pega'=>['pegasus-world-cup','racing-schedule','pegasus-world-cup-odds', 'pegasus-world-cup-betting', 'pegasus-world-cup-results', 'pegasus-world-cup-winners', 'pegasus-world-cup-signup-bonus', 'pegasus-world-cup-contenders'],
	'PS'=>['preakness-stakes-betting','preakness-stakes-contenders','preakness-stakes-winners','preakness-stakes-free-bet','preakness-stakes-match-races','preakness-stakes-odds','preakness-stakes','preakness-stakes-prop-bets','preakness-stakes-results','ag-preakness-stakes-betting','ag-preakness-stakes-contenders','ag-preakness-stakes-contest','ag-preakness-stakes-free-bet','ag-preakness-stakes-match-races','ag-preakness-stakes-odds','ag-preakness-stakes-props','ag-preakness-stakes-results','ag-preakness-stakes-winners'],
	'country'=>['Australia','Dubai','France','Hong Kong','Ireland','Japan','Northern Ireland','South Africa','United Kingdom'],
	'SC'=>['saudi-cup-odds', 'saudi-cup-betting', 'saudi-cup-odds', 'saudi-cup', 'saudi-cup-winners', 'saudi-cup-results', 'saudi-cup-contenders'],
	'greyhound'=>['derby-lane-evening','derby-lane-matinee','orange-park-evening','orange-park-matinee','palm-beach-evening','palm-beach-matinee','southland-evening','southland-matinee','wheeling-downs-evening','wheeling-downs-matinee'],
	'promo'=>['promo-blackjack-tournament','promo-casino-rebate','promo-free-bet-Belmont','race-of-the-week','simon-says','promo-belmont-stakes-blackjack-tournament','ag-belmont-stakes-free-bet','promo-kentucky-derby-blackjack-tournament','ag-kentucky-derby-free-bet','promo-preakness-stakes-blackjack-tournament','ag-preakness-stakes-free-bet','ag-triple-crown-reward'],
	'leader'=>['leaders-horses','leaders-jockeys','leaders-trainers','top-horses','top-jockeys','top-trainers'],
	'international'=>['harness-racing-schedule','ag-triple-crown','horse-racing-schedule','indiana-derby','aintree-racecourse','ascot-racecourse','australia-A','australia-B','australia-C','auteuil','ballinrobe','bangor-on-dee-racecourse','bath-racecourse','bellewstown','beverley-racecourse','brighton-racecourse','carlisle-racecourse','cartmel-racecourse','catterick-racecourse','chantilly','cheltenham','chepstow-racecourse','chester-racecourse','clonmel-racecourse','cork-racecourse','curragh-racecourse','deauville','doncaster-racecourse','downpatrick','down-royal','dundalk-racecourse','durbanville','epsom-downs','exeter-racecourse','fairyhouse-racecourse','fakenham-racecourse','ffos-las-racecourse','flamingo','fontwell-park','galway','goodwood-racecourse','gowran-park','greyville','hamilton-park','happy-valley-racecourse','haydock','hereford','hexham-racecourse','huntingdon-racecourse','kelso-racecourse','kempton-park','kenilworth','kilbeggan','killarney','laytown','leicester-racecourse','leopardstown-racecourse','limerick','lingfield-park','listowel','longchamp','ludlow-racecourse','maisons-laffitte','market-rasen-racecourse','meydan-racecourse','musselburgh-racecourse','naas-racecourse','navan-racecourse','newbury-racecourse','newcastle-racecourse','newmarket-racecourse','newtonabbot-racecourse','nottingham-racecourse','perth-racecourse','plumpton-racecourse','pontefract-racecourse','punchestown-racecourse','redcar-racecourse','ripon-racecourse','roscommon','saint-cloud','salisbury-racecourse','sandown-park','scottsville','sedgefield-racecourse','sha-tin-racecourse ','sligo','southwell-racecourse','stratford-racecourse','taunton-racecourse','thirsk-racecourse','thurles-racecourse','tipperary','towcester-racecourse','tramore-racecourse','turffontein','uttoxeter-racecourse','vaal','warwick-racecourse','wetherby-racecourse','wexford','wincanton-racecourse','windsor-racecourse','wolverhampton','worcester-racecourse','yarmouth-racecourse','york-racecourse'],
	'other'=>['about-us','best-horse-racing-site','bet-on-belmont-stakes','bet-on-breeders-cup','bet-on-kentucky-derby','bet-on-preakness-stakes','bet-on-triple-crown','bet-on-horses','disclaimer','getting-started','graded-stakes-races','harness-racing','harness-racing-schedule','hong-kong-racing','racing-schedule','how-to-bet-on-horses','index','login','results','mint-julep-recipe','mobile-horse-betting','odds','emmys-odds','masters-odds','oscar-odds','us-presidential-election','off-track-betting','online-horse-wagering','privacy','promo-rebates-8','promo-refer-a-friend','responsible-gaming','road-to-the-roses','states-accepted','us-racing-support','terms','twin-spires','usracing-reviews','alamito-derby','ag-triple-crown','horse-racing-schedule','indiana-derby','melbourne-cup','racetracks']
);


$data=array(
	'racetrack'=>array(
					'ref'=>'Bet on ',					
					'copy'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.',
					'ref_mobile' =>'Bet on ',
					'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
		),
	'stake'=>array(
				'ref'=>'Bet on the ',
                'copy'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.',
				'ref_mobile'=>'Bet on the ',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
		),
	'state'=>array(
                'ref'=>' Loves to Bet on Horses',
                'copy'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.',
				'ref_mobile'=>'Loves to Bet on Horses',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
                ),
	'KD'=>array(
                'ref'=>'Sign Up',
                'copy'=>'Get Lucky in Kentucky with the best bonuses, daily rebates and free bets for the Run for the Roses.',
				'ref_mobile'=>'Sign Up',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
                ),
	'KO'=>array(
                'ref'=>'Sign Up',
                'copy'=>'The best bonuses, daily rebates and free bets for the Lillies for the Fillies.',
				'ref_mobile'=>'Sign Up',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
                ),
    'BC'=>array(
                'ref'=>'Bet on the Breeders’ Cup',
                'copy'=>'The best bonuses and daily rebates for the World Championships.',
				'ref_mobile'=>'Bet on the Breeders’ Cup',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
                ),
    'BS'=>array(
                'ref'=>'Sign Up',
                'copy'=>'Get ready for the third jewel of the Triple Crown. Daily rebates, bonuses and free bets.',
				'ref_mobile'=>'Sign Up',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
                ),
    'DWC'=>array(
                'ref'=>'Sign Up',
                'copy'=>'The richest race in the world with the best daily rebates and bonuses online.',
				'ref_mobile'=>'Sign Up',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
                ),
    'pega'=>array(
                'ref'=>'Sign Up',
                'copy'=>'Get exclusive odds and bonuses  for the  richest horse race in America.',
				'ref_mobile'=>'Sign Up',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
                ),
    'PS'=>array(
                'ref'=>'Sign Up',
                'copy'=>'Get ready for the second jewel of the Triple Crown. Daily rebates, bonuses and free bets.',
				'ref_mobile'=>'Sign Up',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
				),
	'SC'=>array(
				'ref'=>'Bet on the Saudi Cup',
				'copy'=>'The richest race in the world with the best daily rebates and bonuses online.',
				'ref_mobile'=>'Bet on the Saudi Cup',
				'copy_mobile'=>'The richest race in the world with the best daily rebates and bonuses online.'
				),
	'country'=>array(
				'ref'=>'Sign Up',
                'copy'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.',
				'ref_mobile'=>'Sign Up',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
		),
	'greyhound'=>array(
				'ref'=>'Sign Up',
                'copy'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.',
				'ref_mobile'=>'Sign Up',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
		),
	'promo'=>array(
				'ref'=>'Sign Up',
                'copy'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.',
				'ref_mobile'=>'Sign Up',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
		),
	'leader'=>array(
				'ref'=>'Sign Up',
                'copy'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.',
				'ref_mobile'=>'Sign Up',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
		),	
	'international'=>array(
				'ref'=>'Sign Up',
                'copy'=>'The best bonuses, daily rebates and free bets for the Lillies for the Fillies.',
				'ref_mobile'=>'Sign Up',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
		),
	'other'=>array(
				'ref'=>'Sign Up',
                'copy'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.',
				'ref_mobile'=>'Sign Up',
                'copy_mobile'=>'Bet on Horses, Sports and Casino games with daily rebates, the best bonuses and free bets.'
		),	
	'default'=>array(
				'ref'=>'Sign Up',
                'copy'=>'Bet on Presidential Elections, Pick Your Favorite Candidate. Best Bonuses and Free Bets. ',
				'ref_mobile'=>'Sign Up',
                'copy_mobile'=>'Bet on Presidential Elections, Pick Your Favorite Candidate. Best Bonuses and Free Bets. '
		)
);
if ($cdate > $saudiDateStart && $cdate < $saudiDateEnd) {
	$data['default'] = $data['SC'];
	$data['other'] = $data['SC'];
}
$out=array();
if (isset($_GET['ref']) ){
	$ref = $_GET['ref'];
	$str='';
	foreach ( $refs as $k=>$v ) {
		if ( in_array($ref,$v) ) {
			$str=$k;
			break;
		}
	}
	if ( isset( $data[$str])) {
		$out= $data[$str];
		if ( $str=='racetrack') {
			$out['ref'] = $out['ref']. ' ' . ucwords(str_replace("-", " ",$ref) . " Today");
			$out['ref_mobile'] = $out['ref_mobile']. ' ' . ucwords(str_replace("-", " ",$ref) . " with the latest odds");
		} else if ($str=='stake') {
			$out['ref'] = $out['ref']. ' ' . ucwords(str_replace("-", " ",$ref)) ;
			$out['ref_mobile'] = $out['ref_mobile']. ' ' . ucwords(str_replace("-", " ",$ref)) ;
		} else if ($str=='state') {
			$out['ref'] = ucwords(str_replace("-", " ",$ref)) . $out['ref'];
			$out['ref_mobile'] = ucwords(str_replace("-", " ",$ref)) . $out['ref_mobile'];
		}
	}else{
		$out = $data['default'];
	}
 	echo json_encode($out,1);
} else {
	$out = $data['default'];
	echo json_encode($out,1);
}
?>
