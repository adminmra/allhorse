<script>
    function printStateMenu(a,b){var c="",d="";"US"==a?(c='<select name="address_state">',c+=""!=d?d:'<option value="">Please Select a State</option>',c+='<option value="AK">AK-Alaska</option><option value="AL">AL-Alabama</option><option value="AR">AR-Arkansas</option><option value="AZ">AZ-Arizona</option><option value="CA">CA-California</option><option value="CO">CO-Colorado</option><option value="CT">CT-Connecticut</option><option value="DC">DC-District of Columbia</option><option value="DE">DE-Delaware</option><option value="FL">FL-Florida</option><option value="GA">GA-Georgia</option><option value="HI">HI-Hawaii</option><option value="IA">IA-Iowa</option><option value="ID">ID-Idaho</option><option value="IL">IL-Illinois</option><option value="IN">IN-Indiana</option><option value="KS">KS-Kansas</option><option value="KY">KY-Kentucky</option><option value="LA">LA-Louisiana</option><option value="MA">MA-Massachusetts</option><option value="MD">MD-Maryland</option><option value="ME">ME-Maine</option><option value="MI">MI-Michigan</option><option value="MN">MN-Minnesota</option><option value="MO">MO-Missouri</option><option value="MS">MS-Mississippi</option><option value="MT">MT-Montana</option><option value="NC">NC-North Carolina</option><option value="ND">ND-North Dakota</option><option value="NE">NE-Nebraska</option><option value="NH">NH-New Hampshire</option><option value="NJ">NJ-New Jersey</option><option value="NM">NM-New Mexico</option><option value="NV">NV-Nevada</option><option value="NY">NY-New York</option><option value="OH">OH-Ohio</option><option value="OK">OK-Oklahoma</option><option value="OR">OR-Oregon</option><option value="PA">PA-Pennsylvania</option><option value="RI">RI-Rhode Island</option><option value="SC">SC-South Carolina</option><option value="SD">SD-South Dakota</option><option value="TN">TN-Tennessee</option><option value="TX">TX-Texas</option><option value="UT">UT-Utah</option><option value="VA">VA-Virginia</option><option value="VT">VT-Vermont</option><option value="WA">WA-Washington</option><option value="WI">WI-Wisconsin</option><option value="WV">WV-West Virginia</option><option value="WY">WY-Wyoming</option></select>'):"CA"==a?(c='<select name="address_state" id="address_state">',c+=""!=d?d:'<option value="">Please Select One</option>',c+='<option value="AB">AB-Alberta</option><option value="BC">BC-British Columbia</option><option value="MB">MB-Manitoba</option><option value="NB">NB-New Brunswick</option><option value="NL">NL-Newfoundland and Labrador</option><option value="NT">NT-Northwest Territories</option><option value="NS">NS-Nova Scotia</option><option value="NU">NU-Nunavut</option><option value="ON">ON-Ontario</option><option value="PE">PE-Prince Edward Island</option><option value="QC">QC-Quebec</option><option value="SK">SK-Saskatchewan</option><option value="YT">YT-Yukon</option></select>'):c='<input name="address_state" id="address_state"><option value="Other" selected="selected">Other</option></select>',$("select[name=address_state]").html(c),""!=b&&$("select[name=address_state]").val(b),$("select[name=address_state]").trigger('change')}

    jQuery(document).ready(function(){
        printStateMenu('US', '');

        /*jQuery("#send_signup_form_fer").bind("click", function(e){
            e.preventDefault();
            check_form_before_submission();
        });*/

        jQuery( "input#first_name" ).bind("change", function( evt ){
            jQuery(this).val( capitalizar($(this).val()) ) ;
        });

        jQuery( "input#last_name" ).bind("change", function( evt ){
            jQuery(this).val( capitalizar($(this).val()) ) ;
        });
    });


    document.documentElement.className = 'js';
    function is_int(value){
        if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
            return true;
        }else{
            return false;
        }
    }
    $(function() {
        // Handle Zip Code
        $("#primary_address_postalcode").blur(function() {
            console.log('blur');
            var zipcode = $(this).val();
            var country = $("#primary_address_country").val();
            if ((zipcode.length == 5) && (is_int(zipcode)) && country == "US")  {
                $.get( "signup.php?getzip=" + zipcode, function(data) {
                    data = $.parseJSON(data);
                    printStateMenu('US',data.Table.STATE);
                    $("#primary_address_city").val(data.Table.CITY);
                    $("#primary_address_state").val(data.Table.STATE);
                })
            };
        });
    });

       $(function() {
        // Handle Zip Code
        $('#address_postal_code').blur(function(){
          var zip = trim($(this).val());
          var city = '';
          var state = '';
          var count = '';
        var country = $("#address_country").val();
        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDjzhboH7uZ89SxXUvo_wwKYQBNtrUCkGI&address='+zip).success(function(response){
            try {
                //find the city and state
                var address_components = response.results[0].address_components;
                $.each(address_components, function (index, component) {
                    var types = component.types;
                    $.each(types, function (index, type) {
                        if (type == 'locality') {
                            city = component.long_name;
                        }
                        if (type == 'country') {
                            count = component.short_name;
                        }
                        if (type == 'administrative_area_level_1') {
                            state = component.short_name;
                        }
                    });
                });
                //alert(city);
                //pre-fill the city and state
                printStateMenu(count, '');
                if (count == "US" || count == "CA") {
                    $('#address_state').val(state);
                }
                $('#address_city').val(city);
                $('#address_country').val(count);
            } catch(err) {
                $('#address_city').val("");
                $('#address_country').val("");
                $('#address_state').val("");
            }

          }).error(function(response) {
              $('#address_city').val("");
              $('#address_country').val("");
              $('#address_state').val("");
          });
        });
    });

    function trim(pstrString)
    {
        var intLoop=0;
        for(intLoop=0; intLoop<pstrString.length; )
        {
                if(pstrString.charAt(intLoop)==" ")
                        {
                        pstrString=pstrString.substring(intLoop+1, pstrString.length);
                        }
                else

                        break;
        }

        for(intLoop=pstrString.length-1; intLoop>=0; intLoop=pstrString.length-1)
        {
                if(pstrString.charAt(intLoop)==" ")
                        pstrString=pstrString.substring(0,intLoop);
                else
                        break;
        }
        return pstrString;
    }







    function capitalizar(str) {
        strVal = '';
        str = str.split(' ');
        for (var chr = 0; chr < str.length; chr++) {
            strVal += str[chr].substring(0, 1).toUpperCase() + str[chr].substring(1, str[chr].length).toLowerCase() + ' '
        }
        return jQuery.trim(strVal);
    }

    function show_login_form(){
        //$("#login-box").fadeIn();
        $('#login-box-modal').modal('show');
    }
    function hide_login_form(){
        //$("#login-box").fadeOut();
        $('#login-box-modal').modal('hide');
    }

    function sendemailreset(form){

        var a = check_email_resetpassword();
        if(a == 'true'){
            return false;
        }

        $("#loading_ajax").show();
        var formData = $(form).serialize();
        $.post('/includes/betus-send-email-pass.php', formData)
        .done(function(data){
            $("#loading_ajax").hide();
            var obj = JSON.parse(data);
            if ( obj == 'ok' ) {
                $('#trouble-title').text("Resetting Your Password");
                $('.account-group').css("display","none");
                $('#forgot_email_field').html($('#forgot_email').val())
                $('#forgot_error').css("display","none");
                $('#forgot_error_post').css("display","block");
                $('#link_help').html("I know my password, let me Log In!");
                $('input.btn-red-form').attr('value','Resend Email');
                $('#pn_forgot_pass form input[type=submit]')[0].disabled = false;
            }else{
                $('#pn_forgot_pass #forgot_error').html("<h1>" + data.MSG + "</h1>");
                $('#pn_forgot_pass form input[type=submit]')[0].disabled = false;
            }
        })
        .fail( function( data ){
            $('#pn_forgot_pass form input[type=submit]')[0].disabled = false;
        });

        return false;
    }

    function sendnewspass(form){
        var a = check_fp_password();
        if(a=='true'){
            return false;
        }
        $("#loading").show();
        var password        = $('form#reset-pass input[name=password]').val();
        // var passwordconfirm = $('form#reset-pass input[name=passwordconfirm]').val();

        if(password == "") {
            $("#loading").hide();
        } else{
            var formData = {
                'token' : $('form#reset-pass input[name=token]').val(),
                'password' : $('form#reset-pass input[name=password]').val(),
                // 'passwordconfirm' : $('form#reset-pass input[name=passwordconfirm]').val()
            };
        }
        // else if(password !== passwordconfirm ){
        //     // $( "#reset-pass-log" ).text( "Your passwords do not match" );
        // } 
        
        console.log(formData);
        $.post('/includes/betus-send-new-pass.php', formData)
        .done(function(data){
            var myObj = JSON.parse(data);
            console.log(myObj);
            var objJSON = eval("(function(){return " + myObj + ";})()");
            console.log(objJSON);
            if(!objJSON || ( objJSON.error != null && objJSON.error == "invalid_token")){
                //  $('#reset-pass-log').html("<span style='color:red'>" + data.MSG + "</span>");
                    $('form#reset-pass input[type=submit]')[0].disabled = false;
                    // $('#reset-pass-log').html("Your request link has been expired.");
            }else if(objJSON.id != null){
                $('form#reset-pass').hide();
                    $('div#password_reset-helps').hide();
                    $('#reset-pass-log').html("Thank you, your password has been updated successfully.");
                    window.location.assign("/login");
            }
        })
        .fail( function( data ){
                $('form#reset-pass input[type=submit]')[0].disabled = false;
        });

        return false;
    }

    function signupformbet(form) {
        var phone = $('#primary_phone').val();
		phone = phone.replace(/[^a-zA-Z0-9]/g, '');
        $('#primary_phone').val(phone);

        if(!check_form_before_submission()){
            $('#send_signup_form_fer').attr("disabled", false);        
            return false;
        }

        $('#send_signup_form_fer').attr("disabled", true);
        var __domain = window.location.host;
        var __post_target = '';
        if ( __domain.indexOf('gohorsebetting') > -1){
             __post_target = 'allhorse_gw.php';
        } else if ( __domain.indexOf('allhorseracing') > -1){
             __post_target = 'allhorse_gw.php';
        } else if (  __domain.indexOf('betusracing') > -1 ) {
             __post_target = 'allhorse_busr.php';
        } else {
            return false;
        }

        $("#loading_ajax").show();
        var formData = $(form).serialize();
        formData += '&affname='+getCookie("affname");
        formData += '&aff_token=' + getCookie("aff_token");
        $.post(__post_target, formData)
        .done(function(data){
            $("#loading_ajax").hide();
            var myObj = JSON.parse(data);
                    var objJSON = eval("(function(){return " + myObj + ";})()");
                    if(objJSON.exception != null && objJSON.exception == "Account with email exists"){
                        $('#alias_ghb').html(objJSON.data.alias);
                        $('#existing_account_modal').modal('show');
                        $('#send_signup_form_fer').attr("disabled", false);
                    } else if(objJSON.alias != null){
                        var alias = objJSON.alias;
                        var pass = document.getElementById("password").value;
                        var url = window.location;
                        if(url.host.indexOf('gohorsebetting') > -1){
                            var urlw = 'https://engine.gohorsebetting.com/login?username='+alias+'&password='+pass+'&to=cashier&welcome=display';
                        }else if(url.host.indexOf('allhorseracing') > -1){
                            var urlw = 'https://engine.allhorseracing.ag/login?username='+alias+'&password='+pass+'&to=cashier&welcome=display';
                        } else if ( url.host.indexOf('betusracing')  > -1){
                            var urlw = 'https://engine.betusracing.ag/login?username='+alias+'&password='+pass+'&to=clubhouse&welcome=display'+'&user=' + objJSON.first_name;
                        }
                        window.location.replace(urlw);
                    }
                })
        .fail(function(){
            $("#loading_ajax").hide();
            $('#send_signup_form_fer').attr("disabled", false);
        });
            return false;
        }
        $(function(){
            var phone_str = "1-844-GHB-BETS";
            var phone_number = "1-844-442-2387";
            var url = window.location;
            if ( url.host.indexOf('betusracing')  > -1){
                phone_str = "1-844-BET-HORSES";
                phone_number = "1-844-238-4677";
            }
            $(".btn-tel").attr("href","tel:"+phone_str);
            $("#call_representative").text(phone_str + " ( " + phone_number + " ) to speak with representative." );
        });
        $(function(){
            var url = window.location;
            if ( url.host.indexOf('betusracing')  > -1 && url.href.indexOf('signup')  > -1 ){
                var arr = getQueryStringData();
                if ( document.getElementById("first_name") && arr["first_name"]) document.getElementById("first_name").value=arr["first_name"];
                if ( document.getElementById("last_name") && arr["last_name"]) document.getElementById("last_name").value=arr["last_name"];
                if ( document.getElementById("primary_email") && arr["email"] ) document.getElementById("primary_email").value=arr["email"].trim();
                //if ( document.getElementById("email_confirm_su")  && arr["email"] ) document.getElementById("email_confirm_su").value=arr["email"];
                var str = 'signup_c?';
                if (  arr["v"] ) str += 'v=' + arr["v"];
                if (  arr["ref"] && arr["v"] ) str += '&';
                if (  arr["ref"] ) str += 'ref=' + arr["ref"];
                if (  arr["raf"] && arr["ref"] ) str += '&';
                if (  arr["raf"] ) str += 'raf=' + arr["raf"];
                
                //google analitycs
                if (  arr["utm_campaign"] && arr["ref"]) str += '&';
                if (  arr["utm_source"] && arr["utm_medium"] && arr["utm_campaign"]){
                    str += 'utm_source=' +  arr["utm_source"]  +'&'+ 'utm_medium=' +  arr["utm_medium"] +'&'+ 'utm_campaign=' +  arr["utm_campaign"];
                } else if ( arr["utm_campaign"] ) {
                    str += 'utm_campaign=' + arr["utm_campaign"];
                }

                history.pushState({}, '',str);
            }
        });
        function getQueryStringData(url){
            if ( !url ) url = window.location.href;
            var __str = url.split("?");
            var op = {};
            if ( __str.length > 1 ) {
                var query_str = __str.shift();
                var _str = __str.join("?");
                var _q_data = _str.split("&");
                if ( _q_data.length > 0) {
                    for ( var i in _q_data ) {
                        var a = _q_data[i].split("=");
                        op[a[0]] = decodeURIComponent(a[1]).trim().replace(/\s+/g," ");

                    }
                }

            }
            return op;
        }
        function loginformbet() {

            var username=document.getElementById("login_name").value;
            var pass=document.getElementById("password").value;
            var url = window.location;
            if(url.host.indexOf('gohorsebetting') > -1){
                window.location.href = "https://engine.gohorsebetting.com/login?username="+username+"&password="+pass;
            }else if(url.host.indexOf('allhorseracing') > -1){
                window.location.href = "https://engine.allhorseracing.ag/login?username="+username+"&password="+pass;
            }

        }


function removeCookie(Name){
    var ThreeDays = 3 * 24 * 60 * 60 * 1000;
    var ExpDate = new Date();

    ExpDate.setTime(ExpDate.getTime() - ThreeDays);
    document.cookie = Name + "=ImOutOfHere; expires=" + ExpDate.toGMTString();
}

function setUsername(cname){""!=getCookie(cname)&&(document.getElementsByName("username")[0].value=getCookie(cname))}
function setPassword(cname){""!=getCookie(cname)&&(document.getElementsByName("password")[0].value=getCookie(cname))}
function setSelector(cname){""!=getCookie(cname)&&(document.getElementsByName("remember_account")[0].checked=1)}



        $(function(){
            $('#login-form'). on ( 'submit',function (e){

            var url = window.location;

             if(url.host.indexOf('gohorsebetting') > -1){
                if(document.getElementsByName("remember_account")[0].checked){
                setCookie('pwdghb',document.getElementsByName("password")[0].value);
                setCookie('usrghb',document.getElementsByName("username")[0].value);
                }else{
                    if(getCookie("usrghb")!=""){removeCookie('usrghb'); }
                    if(getCookie("pwdghb")!=""){removeCookie('pwdghb'); }
                }
             }else if ( url.host.indexOf('betusracing') > -1){
                if(document.getElementsByName("remember_account")[0].checked){
                setCookie('pwdbsur',document.getElementsByName("password")[0].value);
                setCookie('usrbsur',document.getElementsByName("username")[0].value);
                }else{
                    if(getCookie("usrbsur")!=""){removeCookie('usrbsur'); }
                    if(getCookie("pwdbsur")!=""){removeCookie('pwdbsur'); }
                }
             }

            e.preventDefault();
            var link = '';
            var username=document.getElementById("login_name").value;
            var pass=document.getElementById("password").value;
            var queryStr = getQueryStringData();
            setCookie("__username",username);
            setCookie("__pass",pass);
            if(url.host.indexOf('gohorsebetting') > -1){
                link = "https://engine.gohorsebetting.com/login?username="+username+"&password="+pass;
            }else if(url.host.indexOf('allhorseracing') > -1){
                link = "https://engine.allhorseracing.ag/login?username="+username+"&password="+pass;
            }else if ( url.host.indexOf('betusracing') > -1){
                link = "https://engine.betusracing.ag/login?username="+username+"&password="+pass;
            }
                //define get vars
                if ( queryStr['to'] )  link += '&to='+queryStr['to'];
                if ( queryStr['promo_code'] )  link += '&promo_code_redirect_cashier='+queryStr['promo_code'];
                if ( queryStr['meetingRace'] )  link += '&meetingRace='+queryStr['meetingRace'];

            window.location.href = link;
            });
            var isError = getParameterByName("login");
            if ( isError ) {
                   $("#login_name").val(getCookie("__username"));
                   $("#password").val(getCookie("__pass"));
                   removeCookie('pwdbsur');
                   removeCookie('usrbsur');
                   removeCookie('pwdghb');
                   removeCookie('usrghb');
                   $("#login_error_msg").html("We didn’t recognize the username or password you entered. Please check for CAPS and try again. Or you can reset your password below.");
             } else $("#login_error_msg").html("");
        });
        $(function(){
            var url = window.location;
            if ( url.host.indexOf('betusracing') > -1){
                $(".login_logo").show();
            } else {
                $(".login_logo").hide();
            }
        });

        $(function(){
             var url = window.location;
        if(url.host.indexOf('gohorsebetting') > -1){
            setUsername('usrghb');
            setPassword('pwdghb');
            setSelector('usrghb');
        }else if ( url.host.indexOf('betusracing') > -1){
            setUsername('usrbsur');
            setPassword('pwdbsur');
            setSelector('usrbsur');
        }
        });

        function setCookie(cname, cvalue) {
            var d = new Date();
            d.setTime(d.getTime() + (365*24*60*60*1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = cname + "=" + btoa(cvalue) + "; " + expires;
        }
        /*
        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) == 0) {
                return atob(c.replace(name,'')); // need to decode the string, who encode the string really, why he comment out the decode 
                //return c.replace(name,'');
            //return atob(c.substring(name.length,c.length));
            }
    }
            return "";
        } */


        //this change was made by Francisco Pacheco
        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) == 0) {
                    var cookieString = c.replace(name, '');
                    var regex = new RegExp("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$");
                    if (regex.test(cookieString)) {
                        return atob(c.replace(name, ''));
                    } else {
                        return c.replace(name, '');
                    }
                    //return atob(c.replace(name, '')); // need to decode the string, who encode the string really, why he comment out the decode
                    //return c.replace(name,'');
                    //return atob(c.substring(name.length,c.length));
                }
            }
            return "";
        }

        function getParameterByName(name, url) {
                    if (!url) url = window.location.href;
                    name = name.replace(/[\[\]]/g, "\\$&");
                    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                    results = regex.exec(url);
                    if (!results) return null;
                    if (!results[2]) return '';
                    return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
        /*
        function validateEmail(email){
            var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            var valid = emailReg.test(email);

            if(!valid) {
                return false;
            } else {
                return true;
            }
        }
        */

        function validateEmail(email){
            var emailReg = new RegExp( /^\s*(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])\s*$/i);
            var valid = emailReg.test(email);

            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "GET", "includes/forms/email_validate.php?email="+ email, false );
            xmlHttp.send( null );
            valid = xmlHttp.responseText;
            valid = JSON.parse(valid);

            if(!valid) {
                return false;
            } else {
                return true;
            }
        }

        function validateEmailForgotPass(email){
            var emailReg = new RegExp( /^\s*(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])\s*$/i);
            var valid = emailReg.test(email);

            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "GET", "includes/forms/email_validate.php?email="+ email, false );
            xmlHttp.send( null );
            valid = xmlHttp.responseText;
            valid = JSON.parse(valid);

            if(!valid) {
                return false;
            } else {
                return true;
            }
        }

        /*
        function validateEmailWhiteSpace(email){
            var emailReg = new RegExp(/\s/i);
            var valid = emailReg.test(email);

            if(!valid) {
                return false;
            } else {
                return true;
            }
        }
        */
        function validatePostalCode(postcode, countryCode){
            var zippostcode= trim(postcode);
            var zip = new RegExp(/^(\d{5}(-\d{4})?|[A-Z]\d[A-Z] ?\d[A-Z]\d)$/);
            var valid = zip.test(zippostcode);

            if(!valid) {
                return false;
            } else {
                return true;
            }
        }

        function check_email_resetpassword() {
            var error_flag = "";
            if (jQuery("#forgot_email").val() == "") {
                jQuery("#forgot_email").addClass("error");
                jQuery("#forgot_email").removeClass("success");
                jQuery("#email_error").addClass('msg-error');
                jQuery("#email_error").removeClass('msg-hidden');
                error_flag = "true";
            } else {
                jQuery("#forgot_email").addClass("success");
                jQuery("#forgot_email").removeClass("error");
                jQuery("#email_error").addClass('msg-hidden');
                jQuery("#email_error").removeClass('msg-error');
            }
            if (validateEmailForgotPass(jQuery("#forgot_email").val())) {
                $("#forgot_email").addClass("success");
                $("#forgot_email").removeClass("error");
                jQuery("#email_error").addClass('msg-hidden');
                jQuery("#email_error").removeClass('msg-error');
            } else {
                jQuery("#forgot_email").addClass("error");
                jQuery("#forgot_email").removeClass("success");
                jQuery("#email_error").addClass('msg-error');
                jQuery("#email_error").removeClass('msg-hidden');
                error_flag = "true";
            }
            return error_flag;
        }

        function check_fp_password() {
            var error_flag = "";
            // if( jQuery("#fp_password").val() == "" ){
            //     jQuery("#fp_password").parent().addClass("error");
            //     jQuery("#fp_password").parent().removeClass("success");
            //     error_flag = "true";
            // } else {
            //     jQuery("#fp_password").parent().addClass("success");
            //     jQuery("#fp_password").parent().removeClass("error");
            // }
            // return error_flag;
            var password = $("#password").val();
            var regex = /[.*+?^${}#"-+*#@!/,-/ /([´'=\°~Ö}._?${:;¨¬^Ó)|[\]\\]/g;
            if (password == "") {
                $("#password").addClass("error");
                $("#password").removeClass("success");
                $("#password_error").addClass('msg-error');
                $("#password_error").removeClass('msg-hidden');
                $("#password_invalidchar").addClass('msg-hidden');
                $("#password_invalidchar").removeClass('msg-error');
                error_flag = "true";
            } else if (password.match(regex)) {
                $("#password").addClass("error");
                $("#password").removeClass("success");
                $("#password_invalidchar").addClass('msg-error');
                $("#password_invalidchar").removeClass('msg-hidden');
                $("#password_error").addClass('msg-hidden');
                $("#password_error").removeClass('msg-error');
                error_flag = "true";
            } else if (!(password.length >= 6 && password.length <= 10)) {
                $("#password").addClass("error");
                $("#password").removeClass("success");
                $("#password_error").addClass('msg-error');
                $("#password_error").removeClass('msg-hidden');
                $("#password_invalidchar").addClass('msg-hidden');
                $("#password_invalidchar").removeClass('msg-error');
                error_flag = "true";
            } else {
                $("#password").addClass("success");
                $("#password").removeClass("error");
                $("#password_error").addClass('msg-hidden');
                $("#password_error").removeClass('msg-error');
                $("#password_invalidchar").addClass('msg-hidden');
                $("#password_invalidchar").removeClass('msg-error');
            }
            return error_flag;
        }

        function check_form_before_submission(){
            var error_flag = false;

            if( jQuery("#first_name").val() == "" ){
                jQuery("#first_name").parent().addClass("error");
                jQuery("#first_name").parent().removeClass("success");
                jQuery("#name_result").addClass("error");
                jQuery("#name_result").removeClass("success");
                jQuery("#first_name_error").addClass('msg-error');
                jQuery("#first_name_error").removeClass('msg-hidden');
                error_flag = true;
            } else {
                jQuery("#first_name").parent().addClass("success");
                jQuery("#first_name").parent().removeClass("error");
                jQuery("#name_result").addClass("success");
                jQuery("#name_result").removeClass("error");
                jQuery("#first_name_error").addClass('msg-hidden');
                jQuery("#first_name_error").removeClass('msg-error');
            }

            if( jQuery("#last_name").val() == "" ){
                jQuery("#last_name").parent().addClass("error");
                jQuery("#last_name").parent().removeClass("success");
                jQuery("#last_name_result").addClass("error");
                jQuery("#last_name_result").removeClass("success");
                jQuery("#last_name_error").addClass('msg-error');
                jQuery("#last_name_error").removeClass('msg-hidden');
                error_flag = true;
            } else {
                jQuery("#last_name").parent().addClass("success");
                jQuery("#last_name").parent().removeClass("error");
                jQuery("#last_name_result").addClass("success");
                jQuery("#last_name_result").removeClass("error");
                jQuery("#last_name_error").addClass('msg-hidden');
                jQuery("#last_name_error").removeClass('msg-error');
            }

            //validate regular expression - special chars
            var regex               = /[.*+?^${}#"-+*#@!/,-/ /([´'=\°~Ö}._?${:;¨¬^Ó)|[\]\\]/g;
            var passwordVar         = jQuery("#password").val();
            // var passwordconfirmVar  = jQuery("#passwordconfirm").val();
            var passwordFound       = passwordVar.match(regex);

            // if(passwordconfirmVar){
            //     var passwordconfirmFound = passwordconfirmVar.match(regex);
            // }else{
            //     var passwordconfirmFound = null;
            // }


            // if( passwordconfirmFound ) {
            //     jQuery("#passwordconfirm").parent().addClass("error");
            //     jQuery("#passwordconfirm").parent().removeClass("success");
            //     jQuery("#passwordconfirm_invalidchar").addClass('msg-error');
            //     jQuery("#passwordconfirm_invalidchar").removeClass('msg-hidden');
            //     jQuery("#passwordconfirm_error").addClass('msg-hidden');
            //     jQuery("#passwordconfirm_error").removeClass('msg-error');
            //     jQuery("#passwords_not_match").addClass('msg-hidden');
            //     jQuery("#passwords_not_match").removeClass('msg-error');
            //     error_flag = true;
            // } else if( jQuery("#passwordconfirm").val() == "" ){
            //     jQuery("#passwordconfirm").parent().addClass("error");
            //     jQuery("#passwordconfirm").parent().removeClass("success");
            //     jQuery("#passwordconfirm_invalidchar").addClass('msg-hidden');
            //     jQuery("#passwordconfirm_invalidchar").removeClass('msg-error');
            //     jQuery("#passwordconfirm_error").addClass('msg-error');
            //     jQuery("#passwordconfirm_error").removeClass('msg-hidden');
            //     jQuery("#passwords_not_match").addClass('msg-hidden');
            //     jQuery("#passwords_not_match").removeClass('msg-error');
            //     error_flag = true;
            // } else if(jQuery('form#reset-pass input[name=password]').val() !== jQuery('form#reset-pass input[name=passwordconfirm]').val()) {
            //     jQuery("#passwordconfirm").addClass("error");
            //     jQuery("#passwordconfirm").removeClass("success");
            //     jQuery("#passwordconfirm_error").addClass('msg-hidden');
            //     jQuery("#passwordconfirm_error").removeClass('msg-error');
            //     jQuery("#passwordconfirm_invalidchar").addClass('msg-hidden');
            //     jQuery("#passwordconfirm_invalidchar").removeClass('msg-error');
            //     jQuery("#passwords_not_match").addClass('msg-error');
            //     jQuery("#passwords_not_match").removeClass('msg-hidden');
            //     error_flag = true;
            // } else {
            //     jQuery("#passwordconfirm").parent().addClass("success");
            //     jQuery("#passwordconfirm").parent().removeClass("error");
            //     jQuery("#passwordconfirm_invalidchar").addClass('msg-hidden');
            //     jQuery("#passwordconfirm_invalidchar").removeClass('msg-error');
            //     jQuery("#passwordconfirm_error").addClass('msg-hidden');
            //     jQuery("#passwordconfirm_error").removeClass('msg-error');
            //     jQuery("#passwords_not_match").addClass('msg-hidden');
            //     jQuery("#passwords_not_match").removeClass('msg-error');
            // }
            

            var password = $("#password").val();
            var regex = /[.*+?^${}#"-+*#@!/,-/ /([´'=\°~Ö}._?${:;¨¬^Ó)|[\]\\]/g;
            if (password == "") {
                $("#password").parent().addClass("error");
                $("#password").parent().removeClass("success");
				$("#password_result").addClass("error");
                $("#password_result").removeClass("success");
                $("#password_error").addClass('msg-error');
                $("#password_error").removeClass('msg-hidden');
                $("#password_invalidchar").addClass('msg-hidden');
                $("#password_invalidchar").removeClass('msg-error');
                error_flag = "true";
            } else if (password.match(regex)) {
                $("#password").parent().addClass("error");
                $("#password").parent().removeClass("success");
				$("#password_result").addClass("error");
				$("#password_result").removeClass("success");
				$("#password_invalidchar").addClass('msg-error');
				$("#password_invalidchar").removeClass('msg-hidden');
				$("#password_error").addClass('msg-hidden');
				$("#password_error").removeClass('msg-error');
                error_flag = "true";
            } else if (!(password.length >= 6 && password.length <= 10)) {
                $("#password").parent().addClass("error");
                $("#password").parent().removeClass("success");
				$("#password_result").addClass("error");
                $("#password_result").removeClass("success");
                $("#password_error").addClass('msg-error');
                $("#password_error").removeClass('msg-hidden');
                $("#password_invalidchar").addClass('msg-hidden');
                $("#password_invalidchar").removeClass('msg-error');
                error_flag = "true";
            } else {
                $("#password").parent().addClass("success");
                $("#password").parent().removeClass("error");
                $("#password_result").addClass("success");
                $("#password_result").removeClass("error");
                $("#password_error").addClass('msg-hidden');
                $("#password_error").removeClass('msg-error');
                $("#password_invalidchar").addClass('msg-hidden');
                $("#password_invalidchar").removeClass('msg-error');
            }

            var primary_phone = $("#primary_phone").val();
		    primary_phone = primary_phone.replace(/[^a-zA-Z0-9]/g, '');
            if( primary_phone == "" || !(primary_phone.length >= 7 && primary_phone.length <= 13)){
                $("#primary_phone").parent().addClass("error");
                $("#primary_phone").parent().removeClass("success");
				$("#phone_result").addClass("error");
				$("#phone_result").removeClass("success");
                $("#primary_phone_error").addClass('msg-error');
                $("#primary_phone_error").removeClass('msg-hidden');
                error_flag = true;
            } else {
                $("#primary_phone").parent().addClass("success");
                $("#primary_phone").parent().removeClass("error");
				$("#phone_result").addClass("success");
				$("#phone_result").removeClass("error");
                $("#primary_phone_error").addClass('msg-hidden');
                $("#primary_phone_error").removeClass('msg-error');
            }

            if( jQuery("#primary_email").val() == "" ){
                $("#primary_email").parent().addClass("error");
				$("#primary_email").parent().removeClass("success");
				$("#email_result").addClass("success");
				$("#email_result").removeClass("error");
				$("#primary_email_error").addClass('msg-error');
				$("#primary_email_error").removeClass('msg-hidden');
                error_flag = true;
            } else {
                $("#primary_email").parent().addClass("success");
				$("#primary_email").parent().removeClass("error");
				$("#email_result").addClass("success");
				$("#email_result").removeClass("error");
				$("#primary_email_error").addClass('msg-hidden');
				$("#primary_email_error").removeClass('msg-error');
            }

            if(validateEmail(jQuery("#primary_email").val())){
                $("#primary_email").parent().addClass("success");
				$("#primary_email").parent().removeClass("error");
				$("#email_result").addClass("success");
				$("#email_result").removeClass("error");
				$("#primary_email_error").addClass('msg-hidden');
				$("#primary_email_error").removeClass('msg-error');
            } else {
                $("#primary_email").parent().addClass("error");
				$("#primary_email").parent().removeClass("success");
				$("#email_result").addClass("success");
				$("#email_result").removeClass("error");
				$("#primary_email_error").addClass('msg-error');
				$("#primary_email_error").removeClass('msg-hidden');
                error_flag = true;
            }
/*
        if( jQuery("#email_confirm_su").val() == "" ){
            jQuery("#email_confirm_su").parent().addClass("error");
            jQuery("#email_confirm_su").parent().removeClass("success");
            jQuery("#email_confirm_su_error").addClass('msg-error');
            jQuery("#email_confirm_su_error").removeClass('msg-hidden');
            error_flag = true;
        } else {
            if( jQuery("#primary_email").val() ==  jQuery("#email_confirm_su").val() ){
            jQuery("#email_confirm_su").parent().addClass("success");
            jQuery("#email_confirm_su").parent().removeClass("error");
            jQuery("#email_confirm_su_error").addClass('msg-hidden');
            jQuery("#email_confirm_su_error").removeClass('msg-error');
          }
          else{
            jQuery("#email_confirm_su").parent().addClass("error");
            jQuery("#email_confirm_su").parent().removeClass("success");
            jQuery("#email_confirm_su_error").addClass('msg-error');
            jQuery("#email_confirm_su_error").removeClass('msg-hidden');
            error_flag = true;
          }
        }

        if( (jQuery("#primary_email").val() != "")  && (jQuery("#email_confirm_su").val() != "") ){
            if ( jQuery("#primary_email").val() ==  jQuery("#email_confirm_su").val() ) {
            jQuery("#email_confirm_su").parent().addClass("success");
            jQuery("#email_confirm_su").parent().removeClass("error");
            }
        }
*/
        // var flag_dob_verification = false;

        // if( jQuery("#dob_month").val() == "" ){
        //     flag_dob_verification = true;
        // }

        // if( jQuery("#dob_day").val() == "" ){
        //     flag_dob_verification = true;
        // }

        // if( jQuery("#dob_year").val() == "" ){
        //     flag_dob_verification = true;
        // }

        // if(flag_dob_verification){
        //     jQuery("#birth_date_error").addClass('msg-error');
        //     jQuery("#birth_date_error").removeClass('msg-hidden');
        //     jQuery("#div_birth_date").addClass('error_bloque');
        //     error_flag = true;
        // } else {
        //     jQuery("#birth_date_error").addClass('msg-hidden');
        //     jQuery("#birth_date_error").removeClass('msg-error');
        //     jQuery("#div_birth_date").removeClass('error_bloque');
        // }

        // if( jQuery("#address_country").val() == "" ){
        //     jQuery("#address_country").parent().addClass("error");
        //     jQuery("#address_country").parent().removeClass("success");
        //     jQuery("#address_country_error").addClass('msg-error');
        //     jQuery("#address_country_error").removeClass('msg-hidden');
        //     error_flag = true;
        // } else {
        //     jQuery("#address_country").parent().addClass("success");
        //     jQuery("#address_country").parent().removeClass("error");
        //     jQuery("#address_country_error").addClass('msg-hidden');
        //     jQuery("#address_country_error").removeClass('msg-error');
        // }

        /*if( jQuery("#address_country").val() == "" ){
            jQuery("#address_country").parents().addClass("error");
            jQuery("#address_country").parents().removeClass("success");
            jQuery("#address_country_error").addClass('msg-error');
            jQuery("#address_country_error").removeClass('msg-hidden');
        } else {
            jQuery("#address_country").parents().addClass("success");
            jQuery("#address_country").parents().removeClass("error");
            jQuery("#address_country_error").addClass('msg-hidden');
            jQuery("#address_country_error").removeClass('msg-error');
        }*/

        if( jQuery("#address_postal_code").val() == "" ){
            jQuery("#address_postal_code").parent().addClass("success");
            jQuery("#address_postal_code").parent().removeClass("error");
            jQuery("#zip_result").addClass("success");
			jQuery("#zip_result").removeClass("error");
            jQuery("#address_postal_code_error").addClass('msg-hidden');
            jQuery("#address_postal_code_error").removeClass('msg-error');
        } else {
            // jQuery("#address_postal_code").parent().addClass("success");
            // jQuery("#address_postal_code").parent().removeClass("error");
            // jQuery("#zip_result").addClass("success");
			// jQuery("#zip_result").removeClass("error");
            // jQuery("#address_postal_code_error").addClass('msg-hidden');
            // jQuery("#address_postal_code_error").removeClass('msg-error');

            if(validatePostalCode(jQuery("#address_postal_code").val(),jQuery("#address_country").val())){
                jQuery("#address_postal_code").parent().addClass("success");
                jQuery("#address_postal_code").parent().removeClass("error");
                jQuery("#zip_result").addClass("success");
			    jQuery("#zip_result").removeClass("error");
                jQuery("#address_zip_code_error").addClass('msg-hidden');
                jQuery("#address_zip_code_error").removeClass('msg-error');
            }
            else{
                jQuery("#address_postal_code").parent().addClass("error");
                jQuery("#address_postal_code").parent().removeClass("success");
                jQuery("#zip_result").addClass("error");
			    jQuery("#zip_result").removeClass("success");
                jQuery("#address_zip_code_error").addClass('msg-error');
                jQuery("#address_zip_code_error").removeClass('msg-hidden');
                error_flag = true;
            }

        }



        // if( jQuery("#address_street").val() == "" ){
        //     jQuery("#address_street").parent().addClass("error");
        //     jQuery("#address_street").parent().removeClass("success");
        //     jQuery("#address_street_error").addClass('msg-error');
        //     jQuery("#address_street_error").removeClass('msg-hidden');
        //     error_flag = true;
        // } else {
        //     jQuery("#address_street").parent().addClass("success");
        //     jQuery("#address_street").parent().removeClass("error");
        //     jQuery("#address_street_error").addClass('msg-hidden');
        //     jQuery("#address_street_error").removeClass('msg-error');
        // }

        /*if( jQuery("#address_state").val() == "" ){
            jQuery("#address_state").parents().addClass("error");
            jQuery("#address_state").parents().removeClass("success");
            jQuery("#address_state_error").addClass('msg-error');
            jQuery("#address_state_error").removeClass('msg-hidden');
        } else {
            jQuery("#address_state").parents().addClass("success");
            jQuery("#address_state").parents().removeClass("error");
            jQuery("#address_state_error").addClass('msg-hidden');
            jQuery("#address_state_error").removeClass('msg-error');
        }*/

        // if( jQuery("#address_city").val() == "" ){
        //     jQuery("#address_city").parent().addClass("error");
        //     jQuery("#address_city").parent().removeClass("success");
        //     jQuery("#address_city_error").addClass('msg-error');
        //     jQuery("#address_city_error").removeClass('msg-hidden');
        //     error_flag = true;
        // } else {
        //     jQuery("#address_city").parent().addClass("success");
        //     jQuery("#address_city").parent().removeClass("error");
        //     jQuery("#address_city_error").addClass('msg-hidden');
        //     jQuery("#address_city_error").removeClass('msg-error');
        // }


        /*if( jQuery("#security_question").val() == "" ){
            jQuery("#security_question").parents().addClass("error");
            jQuery("#security_question").parents().removeClass("success");
            jQuery("#security_question_error").addClass('msg-error');
            jQuery("#security_question_error").removeClass('msg-hidden');
        } else {
            jQuery("#security_question").parents().addClass("success");
            jQuery("#security_question").parents().removeClass("error");
            jQuery("#security_question_error").addClass('msg-hidden');
            jQuery("#security_question_error").removeClass('msg-error');
        }*/

        // if( jQuery("#security_answer").val() == "" ){
        //     jQuery("#security_answer").parent().addClass("error");
        //     jQuery("#security_answer").parent().removeClass("success");
        //     jQuery("#security_answer_error").addClass('msg-error');
        //     jQuery("#security_answer_error").removeClass('msg-hidden');
        //     error_flag = true;
        // } else {
        //     jQuery("#security_answer").parent().addClass("success");
        //     jQuery("#security_answer").parent().removeClass("error");
        //     jQuery("#security_answer_error").addClass('msg-hidden');
        //     jQuery("#security_answer_error").removeClass('msg-error');
        // }




        /*$(".form_field").addClass("success");
        $("[name='password']").focus();
        $("[name='password']").parents( ".form_field" ).addClass("error");
        console.log("errrorr!!!!!");


        $("#address_street_error").removeClass("msg-hidden");
        $("#address_street_error").addClass("msg-error");
        $("[data='address_street_error']").removeClass('msg-hidden');
        $("[data='address_street_error']").addClass('msg-error');
        $("[name='address_postalcode']").parents( ".form_field" ).addClass("error");
        console.log("errrorr!!!!!");

        $("#address_postalcode_error").removeClass("msg-hidden");
        $("#address_postalcode_error").addClass("msg-error");
        $("[data='address_postalcode_error']").removeClass('msg-hidden');
        $("[data='address_postalcode_error']").addClass('msg-error');
        $("[name='address_city']").parents( ".form_field" ).addClass("error");
        console.log("errrorr!!!!!");

        $("#address_city_error").removeClass("msg-hidden");
        $("#address_city_error").addClass("msg-error");
        $("[data='address_city_error']").removeClass('msg-hidden');
        $("[data='address_city_error']").addClass('msg-error');
        $("[name='address_state']").parents( ".form_field" ).addClass("error");
        console.log("errrorr!!!!!");

        $("#address_state_error").removeClass("msg-hidden");
        $("#address_state_error").addClass("msg-error");
        $("[data='address_state_error']").removeClass('msg-hidden');
        $("[data='address_state_error']").addClass('msg-error');
        $("[name='security_answer']").parents( ".form_field" ).addClass("error");
        console.log("errrorr!!!!!");

        $("#security_answer_error").removeClass("msg-hidden");
        $("#security_answer_error").addClass("msg-error");
        $("[data='security_answer_error']").removeClass('msg-hidden');
        $("[data='security_answer_error']").addClass('msg-error');
        $("[name='email_confirm']").parents( ".form_field" ).addClass("error");
        console.log("errrorr!!!!!");

        $("#email_confirm_error").removeClass("msg-hidden");
        $("#email_confirm_error").addClass("msg-error");
        $("[data='email_confirm_error']").removeClass('msg-hidden');
        $("[data='email_confirm_error']").addClass('msg-error');*/

        return !error_flag;
    }

    function FillPromoCode( promoCode ){
        $("#promo_code").val(promoCode);

    }
</script>
