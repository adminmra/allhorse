<style>
    #forgot_email.error {
        box-shadow: 0px 0px 0px 3px #fc4a4a;
        border-radius: 3px;
    }

    #forgot_email.success {
        box-shadow: 0px 0px 0px 3px #59cb8f;
        border-radius: 3px;
    }

    .forgot-pwd-t .col-lg-5 {
        float: none;
        margin: 0 auto;
    }
</style>
<?php
require 'signup_form_misc/us_states.php';
?>
<style type="text/css">
.signup.forgotpass{
    background: none;
}
.validTD{
    display: none;
}
</style>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<?php
function __extractName($url){
    $domain = parse_url($url , PHP_URL_HOST);
    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $list)) {
      return substr($list['domain'], 0,strpos($list['domain'], "."));
    }
    return false;
  }
?>

<?php require_once "/home/ah/allhorse/public_html/shared/wagering/signup_form_misc/js_scripts.php"; ?>
    <div class="col-md-7 col-lg-5 calltoaction" id="pn_forgot_pass">
            <div id="forgot_error">
                <p class="sub-title" style="padding: 0px; margin: 0px"><strong id="title">Reset Your Password</strong></p>
                <p class="desc" >Enter your email, and we'll send you a link to reset your password.</p>
            </div>

            <!-- start post submit -->
            <div id="forgot_error_post" style="display:none;">
                <p style="font-size:20px;margin-bottom:5px;font-weight:bold;">
                    <strong>Please check your email.</strong>
                </p>
                <p>Look for an email from us with a link to reset your password.</p>
                <p id="login-checked">We've sent instructions for resetting your password to 
                <strong id="forgot_email_field"></strong>
                    as long as that's the email you signed up with.If you don't receive an email (check your spam folder), please 
                <a href='/login'>try a different email address</a> that you might have registered with instead.</p><p>If you still are having troubles or can not find the email we sent, please call us at:<br> <strong>1-844-BET-HORSES</strong> or email us at <strong>support@myracingaccount.com</strong>.</p>
            </div>
            <!-- end post submit -->

            <form class="form-signin" action="" method="POST">
            <div class="signup forgotpass" style="padding: 0px">
                <div class="form-group account-group">
                <label for="forgot_email">Your Email</label>
                <input class="form-control form-control-me" type="email" name="email" id='forgot_email' placeholder='Email address' required="" autofocus>
                <span id="email_error" class="validTD msg-hidden">Please provide a valid email</span>
                </div>
                <div class="form-group login-group" style="margin-top: 26px">
                    <p id="login-checked" style="font-size:12px;"></p>
                    <input class="btn btn-sm btn-red btn-red-form" type="submit" onclick="return sendemailreset(this.form);" value="Submit" />
                    <!--<i class="fa fa-refresh fa-spin" style="display:none;" id="ajax-loader"></i>-->
                    <br/>
                    <a id="link_help" href="//www.betusracing.ag/login">Log in if you know your password</a>
                    <center id="loading_ajax" style="display: none">
                        <?php include '/home/ah/allhorse/public_html/Loading/loading.php';?>
                    </center>
                </div>
                </div>
            </form>
    </div>
<script>
    $(document).ready(() => {
        $("#forgot_email").change((e) => {
            checkEmail();
        });
    });

    function checkEmail() {
        var email = $("#forgot_email").val();
        if (email != "") {
            if (validateEmailForgotPass(email)) {
                $("#forgot_email").addClass("success");
                $("#forgot_email").removeClass("error");
                $("#forgot_email").parent().addClass("success");
                $("#forgot_email").parent().removeClass("error");
                $("#email_error").addClass('msg-hidden');
                $("#email_error").removeClass('msg-error');
            } else {
                $("#forgot_email").addClass("error");
                $("#forgot_email").removeClass("success");
                $("#forgot_email").parent().addClass("error");
                $("#forgot_email").parent().removeClass("success");
                $("#email_error").addClass('msg-error');
                $("#email_error").removeClass('msg-hidden');
            }
        } else {
            $("#forgot_email").removeClass("success");
            $("#forgot_email").removeClass("error");
            $("#forgot_email").parent().removeClass("success");
            $("#forgot_email").parent().removeClass("error");
            $("#email_error").addClass('msg-hidden');
            $("#email_error").removeClass('msg-error');
        }
    }
</script>