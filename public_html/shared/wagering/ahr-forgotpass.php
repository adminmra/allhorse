<style>
    #forgot_email.error {
        box-shadow: 0px 0px 0px 3px #fc4a4a;
        border-radius: 3px;
    }

    #forgot_email.success {
        box-shadow: 0px 0px 0px 3px #59cb8f;
        border-radius: 3px;
    }

    .forgot-pwd-t .col-lg-5 {
        float: none;
        margin: 0 auto;
    }

    .msg-hidden {
        display: none;
    }

    .msg-error {
        color: red;
        display: block;
        font-size: 12px;
        margin: 5px 0;
    }
</style>
<?php
require 'signup_form_misc/us_states.php';
?>

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<?php require_once "/home/ah/allhorse/public_html/shared/wagering/signup_form_misc/js_scripts.php"; ?>

    <div class="col-md-5 calltoaction" id="pn_forgot_pass">
            <div id="forgot_error">
                <p class="sub-title" ><strong id="title">Reset Your Password</strong></p>
                <p class="desc" >Give us your email, and we'll send you a link to reset your password.</p>
            </div>

            <!-- start post submit -->
            <div id="forgot_error_post" style="display:none;">
                <p style="font-size:20px;margin-bottom:5px;font-weight:bold;">
                    <strong>Please check your email.</strong>
                </p>
                <p>Look for an email from us with a link to reset your password.</p>
                <p id="login-checked">We've sent instructions for resetting your password to 
                <strong id="forgot_email_field"></strong>
                    as long as that's the email you signed up with. If you don't receive an email (check your spam folder), please 
                <a href='/login'>try a different email address</a> that you might have registered with instead.</p><p>If you still are having troubles or can not find the email we sent, please call us at:<br> <strong>1-844-777-BETS &nbsp; (1-844-777-2387)</strong> or email us at <strong>support@myracingaccount.com</strong>.</p>
            </div>
            <!-- end post submit -->

            <form class="form-signin" action="" method="POST">
                <div class="form-group account-group">
                <label for="forgot_email">Your Email</label>
                <input class="form-control form-control-me" type="email" name="email" id='forgot_email' placeholder='Email address' required="" autofocus>
                <span id="email_error" class="validTD msg-hidden">Please provide a valid email</span>
                </div>
                <div class="form-group login-group">
                    <p id="login-checked" style="font-size:12px;"></p>
                    <input class="btn btn-sm btn-red btn-red-form" type="submit" onclick="return sendemailreset(this.form);" value="Send Email" />
                    <!--<i class="fa fa-refresh fa-spin" style="display:none;" id="ajax-loader"></i>-->
                    <br/>
                    <a id="link_help" href="/login">Log In if you know your password</a>
                    <center id="loading_ajax" style="display: none">
                        <img src="https://documentation.devexpress.com/HelpResource.ashx?help=WPF&document=img118763.jpg" />
                    </center>
                </div>
            </form>
    </div>
    
    <script>
        $(document).ready(() => {
            $("#forgot_email").change((e) => {
                checkEmail();
            });
        });

        function checkEmail() {
            var email = $("#forgot_email").val();
            if (email != "") {
                if (validateEmailForgotPass(email)) {
                    $("#forgot_email").addClass("success");
                    $("#forgot_email").removeClass("error");
                    $("#forgot_email").parent().addClass("success");
                    $("#forgot_email").parent().removeClass("error");
                    $("#email_error").addClass('msg-hidden');
                    $("#email_error").removeClass('msg-error');
                } else {
                    $("#forgot_email").addClass("error");
                    $("#forgot_email").removeClass("success");
                    $("#forgot_email").parent().addClass("error");
                    $("#forgot_email").parent().removeClass("success");
                    $("#email_error").addClass('msg-error');
                    $("#email_error").removeClass('msg-hidden');
                }
            } else {
                $("#forgot_email").removeClass("success");
                $("#forgot_email").removeClass("error");
                $("#forgot_email").parent().removeClass("success");
                $("#forgot_email").parent().removeClass("error");
                $("#email_error").addClass('msg-hidden');
                $("#email_error").removeClass('msg-error');
            }
        }
    </script>


