<style>
    .form-control.error {
        box-shadow: 0px 0px 0px 3px #fc4a4a;
        border-radius: 3px;
    }

    .form-control.success {
        box-shadow: 0px 0px 0px 3px #59cb8f;
        border-radius: 3px;
    }

    .showPass {
        top: 30% !important;
        right: 10% !important;
    }

    .forgot-pwd-t .col-lg-5 {
        float: none;
        margin: 0 auto;
    }
</style>
<?php require 'signup_form_misc/us_states.php';?>
<style type="text/css">
    .signup.forgotpass {
        background: none;
    }

    .validTD {
        display: none;
    }

    .msg-error {
        color: red;
        display: block;
        font-size: 12px;
        margin: 5px 0;
    }
</style>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<?php require_once "/home/ah/allhorse/public_html/shared/wagering/signup_form_misc/js_scripts.php";?>
<div class="col-md-7 col-lg-5 calltoaction">
    <form id="reset-pass" method="post" action="">
        <div class="signup forgotpass" style="padding: 0px">
            <div id="password_reset-helps">
                <p ><strong>Choose Your Password</strong></p>
                <p >Pick a new password
                    <?php
                    if(isset($_GET["email"]) && $_GET["email"]!=""){
                        echo "for <strong>{$_GET['email']}</strong>";http://www.betusracing.ag/login?ctl00%24MainContent%24Account=USR10661&ctl00%24MainContent%24ctlLogin%24BtnSubmit=Login
                    }
                    ?>
                </p>
            </div>
            <input class="" placeholder="token" maxlength="50" name="token" value="<?php echo $_GET['t']; ?>" type="hidden">
            <div class="form-group account-group" style="position: relative; margin-top: 18px">
                <!--<input class="form-control form-control-me form-custom-me" placeholder="password" maxlength="50" name="password" id="fp_password" type="password" >-->
                <?php include '/home/ah/allhorse/public_html/shared/wagering/password_eye.php';?>
                <input class="form-control form-control-me form-custom-me" placeholder="Password (6 - 10 chars)" maxlength="11" name="password" id="password" type="password">
            </div>
            <span id="password_error" class="validTD msg-hidden">Password must have between 6 and 10 characters.</span>
            <span id="password_invalidchar" class="validTD msg-hidden">Special characters like #@$!%^&*-_ cannot be used for your password</span>

            <!-- <div class="form-group account-group" style="position: relative; margin-top: 27px">
                <input class="form-control form-control-me form-custom-me" placeholder="Password Confirm (6 - 10 chars)" maxlength="10" name="passwordconfirm" id="passwordconfirm" type="password">
            </div>
            <span id="passwordconfirm_error" class="validTD msg-hidden">Password Confirm must have between 6 and 10 characters.</span>
            <span id="passwordconfirm_invalidchar" class="validTD msg-hidden">Special characters are not allowed to be used on the password confirm field</span>
            <span id="passwords_not_match" class="validTD msg-hidden">Your passwords do not match</span> -->
            <div class="form-group account-group" style="margin-top: 26px">
                <!--<input class="btn btn-sm btn-red btn-red-form" type="submit" onclick="return signupformbet(this.form)" value="Update Password and Login" />-->
                <input class="btn btn-sm btn-red btn-red-form" type="submit" onclick="return sendnewspass(this.form)" value="Update Password and Login" />
                <center id="loading" style="display: none">
				    <?php include '/home/ah/allhorse/public_html/Loading/loading.php';?>
		        </center>
                <br />
                <a class="login-buus" href="/login">Log in if you know your password</a>
            </div>
        </div>
    </form>
    <div style="color:#999;font-size:16px;" id="reset-pass-log"></div>
</div>

<script>
    $(document).ready(() => {
        // $("#password").change((e) => {
        //     validatePassword();
        // });
        $("#password").on("input", validatePassword);
        // $("#passwordconfirm").change((e) => {
        //     validateConfirmPassword();
        // });
    });

    function validatePassword() {
        var password = $("#password").val();
        var regex = /[.*+?^${}#"-+*#@!/,-/ /([´'=\°~Ö}._?${:;¨¬^Ó)|[\]\\]/g;
        if(password != "") {
            if (password.match(regex)) {
                $("#password").addClass("error");
                $("#password").removeClass("success");
                $("#password_invalidchar").addClass('msg-error');
                $("#password_invalidchar").removeClass('msg-hidden');
                $("#password_error").addClass('msg-hidden');
                $("#password_error").removeClass('msg-error');
            } else if(!(password.length >= 6 && password.length <= 10)) {
                $("#password").addClass("error");
                $("#password").removeClass("success");
                $("#password_error").addClass('msg-error');
                $("#password_error").removeClass('msg-hidden');
                $("#password_invalidchar").addClass('msg-hidden');
                $("#password_invalidchar").removeClass('msg-error');
            } else {
                $("#password").addClass("success");
                $("#password").removeClass("error");
                $("#password_error").addClass('msg-hidden');
                $("#password_error").removeClass('msg-error');
                $("#password_invalidchar").addClass('msg-hidden');
                $("#password_invalidchar").removeClass('msg-error');
            }
        } else {
            $("#password").removeClass("success");
            $("#password").removeClass("error");
            $("#password_error").addClass('msg-hidden');
            $("#password_error").removeClass('msg-error');
            $("#password_invalidchar").addClass('msg-hidden');
            $("#password_invalidchar").removeClass('msg-error');
        }
    }

    // function validateConfirmPassword() {
    //     var password = $("#passwordconfirm").val();
    //     var regex = /[.*+?^${}#"-+*#@!/,-/ /([´'=\°~Ö}._?${:;¨¬^Ó)|[\]\\]/g;
    //     if(password != "") {
    //         if (password.match(regex)) {
    //             $("#passwordconfirm").addClass("error");
    //             $("#passwordconfirm").removeClass("success");
    //             $("#passwordconfirm_invalidchar").addClass('msg-error');
    //             $("#passwordconfirm_invalidchar").removeClass('msg-hidden');
    //             $("#passwordconfirm_error").addClass('msg-hidden');
    //             $("#passwordconfirm_error").removeClass('msg-error');
    //             $("#passwords_not_match").addClass('msg-hidden');
    //             $("#passwords_not_match").removeClass('msg-error');
    //         } else if(!(password.length >= 6 && password.length <= 10)) {
    //             $("#passwordconfirm").addClass("error");
    //             $("#passwordconfirm").removeClass("success");
    //             $("#passwordconfirm_error").addClass('msg-error');
    //             $("#passwordconfirm_error").removeClass('msg-hidden');
    //             $("#passwordconfirm_invalidchar").addClass('msg-hidden');
    //             $("#passwordconfirm_invalidchar").removeClass('msg-error');
    //             $("#passwords_not_match").addClass('msg-hidden');
    //             $("#passwords_not_match").removeClass('msg-error');
    //         } else if($('form#reset-pass input[name=password]').val() !== $('form#reset-pass input[name=passwordconfirm]').val()) {
    //             $("#passwordconfirm").addClass("error");
    //             $("#passwordconfirm").removeClass("success");
    //             $("#passwordconfirm_error").addClass('msg-hidden');
    //             $("#passwordconfirm_error").removeClass('msg-error');
    //             $("#passwordconfirm_invalidchar").addClass('msg-hidden');
    //             $("#passwordconfirm_invalidchar").removeClass('msg-error');
    //             $("#passwords_not_match").addClass('msg-error');
    //             $("#passwords_not_match").removeClass('msg-hidden');
    //         } else {
    //             $("#passwordconfirm").addClass("success");
    //             $("#passwordconfirm").removeClass("error");
    //             $("#passwordconfirm_error").addClass('msg-hidden');
    //             $("#passwordconfirm_error").removeClass('msg-error');
    //             $("#passwordconfirm_invalidchar").addClass('msg-hidden');
    //             $("#passwordconfirm_invalidchar").removeClass('msg-error');
    //             $("#passwords_not_match").addClass('msg-hidden');
    //             $("#passwords_not_match").removeClass('msg-error');
    //         }
    //     } else {
    //         $("#password").removeClass("success");
    //         $("#password").removeClass("error");
    //         $("#passwordconfirm_error").addClass('msg-hidden');
    //         $("#passwordconfirm_error").removeClass('msg-error');
    //         $("#passwordconfirm_invalidchar").addClass('msg-hidden');
    //         $("#passwordconfirm_invalidchar").removeClass('msg-error');
    //         $("#passwords_not_match").addClass('msg-hidden');
    //         $("#passwords_not_match").removeClass('msg-error');
    //     }
    // }
</script>