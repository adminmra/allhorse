<?php require 'signup_form_misc/us_states.php';?>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<?php require_once "/home/ah/allhorse/public_html/shared/wagering/signup_form_misc/js_scripts.php";?>
<?php
$tel = "";
$call = "";
$logoimg = "";

if ($_SERVER['HTTP_HOST'] == 'www.betusracing.ag') {
    $sitename = 'bet &#9733; usracing';
    $tel = '1-844-BET-HORSES (1-844-238-4677)';
    $logoimg = '/public/assets/images/busr_logo_images/busr_new_logo/busr.png';
    $call = "1-844-238-4677";
} elseif ($_SERVER['HTTP_HOST'] == 'www.allhorseracing.ag') {
    $sitename = 'All Horse Racing';
    $tel = '1-844-777-BETS (1-844-777-2387)';
    $logoimg = '/themes/images/ahr-logo.png';
    $call = "1-844-777-BETS";
} else {
    $sitename = 'Go Horse Betting';
    $tel = '1-844-GHB-BETS (1-844-442-2387)';
    $logoimg = '/SportNewsTemplate/img/logo.png';
    $call = "1-844-442-2387";
}?>
<form class="form" id="signupForm" method="post" action="">
	<section class="signup">
		<div class="container">
			<!-- Modal -->
			<div id="existing_account_modal" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div class="modal-body">
							<p><img src="<?php echo $logoimg; ?>" ></p>
							<p> Hi there.  Our records indicate that you already have an account with us. Account <span id="alias_ghb"></span> is registered to the email address you just entered. <br /><br />  Please Login below or select  Forgot Password to reset your password.</p>

							<!--<form class="navbar-form" style="margin-left:0; margin-right:0;" action="/login" method="post" id="loginform" name="loginform" role="form">
								<input type="hidden" style="width:100%;" name="" value="">
								<input type="submit" style="width:100%;" class="btn btn-login" value="Login" name="">
							</form>-->
							<div class="navbar-form">
								<a class="btn btn-login" style="width:100%;" href="/login">Login</a>
							</div>
							<div>
								<a class="btn btn-forgot" style="width:100%;" href="/contact-us">Forgot password</a>
							</div>
							<a class="btn-tel" href="<?php echo $call; ?>" style="text-align:center;">Or, you can call <br/><div id="call_representative"> <?php echo $tel; ?> to speak with representative. </div></a>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="signup_form">
				<input type="hidden" value="" name="lid" id="lid">
				<div class="form_row">
					<div class="form_col-50">
						<div class="form_field">
							<input class="autopost" id="first_name"  maxlength="30" name="first_name" title="Enter your First Name here" type="text" placeholder="First name" value="">
						</div>
						<span id="first_name_error" class="validTD msg-hidden">Please provide first name</span>
					</div>
					<div class="sep_1">&nbsp;</div>
					<div class="form_col-50">
						<div class="form_field">
							<input class="autopost" id="last_name"  maxlength="30" name="last_name" title="Enter your Last Name here" type="text" placeholder="Last name" value="">
						</div>
						<span id="last_name_error" class="validTD msg-hidden">Please provide last name</span>
					</div>
				</div>
				<div class="form_row">
					<div class="form_col-50">
						<div class="form_field">
							<input class="autopost" placeholder="Password (6 - 10 chars)" maxlength="10" name="password" value="" id="password" title="The password must contain 6 or more characters" type="password">
						</div>
						<span id="password_error" class="validTD msg-hidden">Password must have between 6 and 10 characters.</span>
					</div>
					<div class="sep_1">&nbsp;</div>
					<div class="form_col-50">
						<div class="form_field">
							<input class="autopost" placeholder="Phone" maxlength="25" name="primary_phone" id="primary_phone" type="text" value="">
						</div>
						<span id="primary_phone_error" class="validTD msg-hidden">Please provide a valid phone number</span>
					</div>
				</div>
				<div class="form_row">
					<div class="form_col-100">
						<div class="form_field">
							<input class="autopost" placeholder="Email Address"  maxlength="50" name="primary_email" id="primary_email" type="text" value="">
						</div>
						<span id="primary_email_error" class="validTD msg-hidden">Please provide a valid email</span>
					</div>
				</div>
				<div class="form_row">
					<div class="form_col-100">
						<div class="form_field">
							<input class="autopost" placeholder="Retype Email Address"  maxlength="50" name="email_confirm" id="email_confirm_su"  type="text" value="">
						</div>
						<span id="email_confirm_su_error" class="validTD msg-hidden">The email addresses you entered don't match</span>
					</div>
				</div>
				<div class="form_row">
					<label class="form_label">Date of Birth:</label>
					<div class="form_col-100">
						<div id="div_birth_date" class="form_row form_row-birth">
							<div class="form_col-33-3">
								<div class="form_field">
									<div class="form_select">
										<select name="dob_month" id="dob_month" class="autopost">
											<option value="">Month</option>
											<?php
for ($x = 1; $x <= 12; $x++) {
    $zx = sprintf("%02d", $x);
    $attrs = "";
    echo "<option value='{$zx}' {$attrs}>{$x}</option>";
}?>
										</select>
									</div>
								</div>
							</div>
							<div class="sep_1">&nbsp;</div>
							<div class="form_col-33-3">
								<div class="form_field">
									<div class="form_select">
										<select name="dob_day" id="dob_day" class="autopost">
											<option value="" >Day</option>
											<?php
for ($x = 1; $x <= 31; $x++) {
    $zx = sprintf("%02d", $x);
    $attrs = "";
    echo "<option value='{$zx}' {$attrs}>{$x}</option>";
}?>
										</select>
									</div>
								</div>
							</div>
							<div class="sep_1">&nbsp;</div>
							<div class="form_col-33-3">
								<div class="form_field">
									<div class="form_select">
										<select name="dob_year" id="dob_year" class="autopost">
											<option value="">Year</option>
											<?php
for ($x = date("Y") - 18; $x >= 1900; $x--) {
    $attrs = "";
    echo "<option value='{$x}' {$attrs}>{$x}</option>";
}?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<center>
						<span id="birth_date_error" class="validTD msg-hidden">Please provide a valid birth date</span>
						</center>
					</div>
				</div>
				<div class="form_row">
					<div class="form_col-66">
						<div class="form_field">
							<div class="form_select">
								<select class="autopost" name="address_country" id="address_country" title="Select your country of residence here."  onChange="printStateMenu(this.value,'');">
									<?php
foreach ($us_states as $state_code => $state_name) {
    $attrs = "";
    echo "<option value='{$state_code}' {$attrs}>{$state_name}</option>";
}
?>
								</select>
							</div>
						</div>
						<span id="address_country_error" class="validTD msg-hidden">Please provide a country</span>
					</div>
					<div class="sep_1">&nbsp;</div>
					<div class="form_col-33">
						<div class="form_field">
							<input class="autopost" maxlength="15" name="address_postal_code" id="address_postal_code" title="Enter Zip/Postal Code here." placeholder="Zip or postal code" type="text" value="">
						</div>
						<span id="address_postal_code_error" class="validTD msg-hidden">Please provide a zip code</span>
						<span id="address_zip_code_error" class="validTD msg-hidden">Invalid zip code</span>
					</div>
				</div>
				<div class="form_row">
					<div class="form_col-100">
						<div class="form_field">
							<input class="autopost" maxlength="180" name="address_street" id="address_street"  value="" placeholder="Street address" title="Please provide your address of residence here." type="text" >
						</div>
						<span id="address_street_error" class="validTD msg-hidden">Please provide an address</span>
					</div>
				</div>
				<div class="form_row">
					<div class="form_col-50">
						<div class="form_field">
							<div class="form_select">
							<select class="autopost" name="address_state" id="address_state" title="Select your State here."></select>
							<script>printStateMenu('US','');</script>
						</div>
					</div>
					<span id="address_state_error" class="validTD msg-hidden">Please provide a state</span>
				</div>
				<div class="sep_1">&nbsp;</div>
				<div class="form_col-50">
					<div class="form_field">
						<input class="autopost" id="address_city"  maxlength="100" name="address_city"  title="Please provide your city of residence here." type="text" placeholder="City" value="">
					</div>
					<span id="address_city_error" class="validTD msg-hidden">Please provide a city</span>
				</div>
			</div>
			<div class="form_row">
				<label class="form_label">Security Question:</label>
				<div class="form_col-50">
					<div class="form_select">
						<select class="autopost" name="security_question" id="security_question" title="Choose your Security Question here">
							<option value="Mothers maiden name">Mothers maiden name</option>
							<option value="Favorite hobby">Favorite hobby</option>
							<option value="Favorite club">Favorite club</option>
							<option value="Favorite book">Favorite book</option>
							<option value="Childhood hero">Childhood hero</option>
							<option value="Best friends name">Best friends name</option>
							<option value="My pet">My pet</option>
							<option value="My nickname">My nickname</option>
							<option value="My first car">My first car</option>
							<option value="My secret code">My secret code</option>
						</select>
					</div>
					<span id="security_question_error" class="validTD msg-hidden">Please provide a security question</span>
				</div>
				<div class="sep_1">&nbsp;</div>
				<div class="form_col-50">
					<div class="form_field">
						<input class="autopost" maxlength="50" name="security_answer" id="security_answer" title="Enter your Security Asnwer here" type="text" placeholder="Security answer" value="">
					</div>
					<span id="security_answer_error" class="validTD msg-hidden">Please provide a security answer</span>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="infos">
	<div class="signup_form">
		<div class="form_row">
			<div class="form_col-100">
				<div class="form_btns">
					<center id="loading_ajax" style="display: none">
					<img src="https://documentation.devexpress.com/HelpResource.ashx?help=WPF&document=img118763.jpg" />
					</center>
					<br />
					<button type="submit" class="form_submit" id="send_signup_form_fer" onclick="return signupformbet('#signupForm')" >Create My Account</button>
				</div>
			</div>
		</div>
		<div class="form_row">
			<div class="form_col-100">
				<p class="form_note">* To receive your winnings we will need your verifiable phone number, email address and billing address. </p>
			</div>
		</div>
		<span id="promo_code_error" class="validTD msg-hidden"></span>
		<input type="hidden" name="postdata" value="1" >
		<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
		</fb:login-button>
	</div>
	<div class="container-fluid">
		<div class="info"><span class="info_icon plus18"></span>
		<p><?php echo $sitename; ?> only accepts players over 18 years old.</p>
	</div>
	<div class="info"><span class="info_icon protected"></span>
	<p>Your personal information is safe and every transaction is protected with 256-bit encryption to protect your information.</p>
</div>
<div class="info"><span class="info_icon agree"></span>
<p>By creating an account you agree to our Terms & Conditions.</p>
</div>
</div>
<!-- <div class="container-fluid"><img src="/public/assets/images/friends.png" alt="" class="friends_img"></div> -->
</section>
</form>