






          
<div class="headline"><h1>Referring Friends to BUSR</h1></div>



<p>
	
	Be a friend and share the love of horse racing and we'll give you love back -  the kind that's green! <p>For every friend you refer to BUSR, you will get a 10% Cash bonus added to your account (based on your friend's initial deposit). No crazy requirements or confusion. You share the love- we share it back. There is no other race book that offers better bonuses than BUSR.  </p>


<hr>
<p><strong>Terms and Conditions</strong></p>

<ul>

<li>Member's account must be in good standing to participate in the Refer a Friend promotion.</li>
<li>You will not be credited any bonuses for referring a friend who already has an account at BUSR.</li>
<li>10% Refer a Friend bonus cash will be added to your account after the friend you have referred has opened a new account and made an initial deposit. The funds will show up a few minutes after your friends deposit.  If 24 hours has passed and you don't see credit, please contact customer service.</li>
<li>Members cannot open multiple accounts in order to abuse promotions and bonuses.</li>
<li>If your friend disputes his deposit to BUSR and cancels his account without playing, then you will not be entitled to any bonus for the referral.</li>
<li>You have to wager the Refer a Friend bonus cash 1 time before withdrawing the funds.</li>
<li>Management reserves the right to modify or cancel this promotion at any time.  General house rules/terms and conditions apply.
</li>

</ul>