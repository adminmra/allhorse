<div class="headline">
    <h1>House Rules</h1>
</div>
<h2 style="margin:0px">WELCOME!</h2>
<p>
    This agreement describes the terms and conditions applicable to the use of the account wagering services provided at bet usracing. US Racing is an online horse racing website that provides information about horse racing and betting. US Racing does not conduct any gambling activities. All accounts created via links or banners on the US Racing website are bet usracing wagering accounts and, as such, are subject to and protected by bet usracing's Terms and Conditions and Privacy Policy. The use of our services and products provided by bet usracing will be taken as your acceptance of all of these terms, conditions, and disclosures contained herein.
</p>
<h2>House Rules</h2>
<p>
    Our rules are governed on the principle of fair play. Horse wagering is a game of skill. The challenge is to gather and assess the information available, use your own knowledge and then compare your opinion to that of our linesmaker and/or the results at the racetrack after a race has been completed.
</p>
<p>If you choose correctly you win.</p>
<p>All rules, regulations, and payoffs contained herein are subject to change and revision by the Management without prior notice.</p>
<h2>AGE RESTRICTIONS</h2>
<p>No one under the age of 18 years is permitted to wager on and products or platforms provided by our gaming provider.</p>
<h2>DISCREPANCIES/DISPUTES</h2>
<p>
    All members should verify their balances on each account log-in and prior to wagering. When you verify and accept your account balance you agree that all previous transactions are correct and you do not have any claims.  Claims or disputes must be settled prior to making any further wagers.  All internet transactions will be logged and the log file will also be saved on a back-up system.  Any correspondence via telephone, email or live chat will be recorded and stored on file.
</p>
<p>
    Notwithstanding anything in this agreement, in the event of any dispute regarding a wager or winnings, the decision of Management will be final and binding in all matters.
</p>
<h2>TAXES</h2>
<p>
    Management will not disclose details of a member's net winnings or losses except as required under applicable law. If you reside in a jurisdiction where your winnings are taxable, it is your responsibility to comply with any appropriate laws.
</p>
<h2>LIMITATION OF PLAYS and/or TERMINATION OF AGREEMENT</h2>
<p>
    The agreement between bet usracing and the member may be terminated at any time upon the request of the member as long as there is no balance and no pending wagers.
</p>
<p>
    Or, if deemed necessary, gaming provider reserves the right to limit a member's plays or revoke the agreement between the member and US Racing. Reasons for limitation of plays or revocation of agreement include, but are not limited to:
</p>
<p>
    <strong>Betting Syndicates:</strong> Management reserves the right to limit or exclude any player who attempts to defraud the house either on his/her own or in collusion with other players or other racebooks or sportsbooks. Other sanctions could include:
</p>
<ul>
    <li>Administration fees</li>
    <li>Refusal of payouts</li>
    <li>Reversal of wagering transactions</li>
</ul>
<p>
    <strong>Abuse of System Vulnerability:</strong> If gaming provider determines that a member is taking advantage of a vulnerability in the system (software error, malfunction or bug), we reserve the right to take some or all of the following actions:
</p>
<ul>
    <li>Limit wagers</li>
    <li>Close the account</li>
    <li>Confiscate monies in the account, including deposits and winnings.</li>
</ul>

<p>
    <strong>Criminal Activity:</strong> If there is reason to believe that criminal or other suspicious activities are occurring through one or more accounts (including attempted money-laundering or fraud), our Gaming Provider reserves the right to close those accounts and/or report such activity to the Gaming Commission and/or other regulatory bodies or services. All account balances (including both deposits and any winnings) shall be forfeited.
</p>
<h2>PASSWORD</h2>
<p>
    Members are solely responsible for the security of their passwords. Should you inadvertently let someone else know your password you must immediately change it via the secure My Account area of the website.
</p>
<p>
    Members are responsible for any unauthorized use of their accounts. In the event that a third party places a bet, or is thought to have placed a bet, the said bet shall be valid whether or not the alleged third party had the prior consent of the member. Under no circumstances will any bet be set to no action for that reason. If you suspect that a third party may have access to your password or username, you should proceed to the secure "My Account area and change your password.
</p>
<p>When choosing a password, the Gaming provider recommends you follow these guidelines:</p>
<ul>
    <li>Do not use all lower case letters</li>
    <li>Do use numbers and or symbols</li>
    <li>Do use a mix of upper and lower case letters</li>
</ul>

<h2>BONUSES</h2>
<ol>
    <li>
        Members of bet usracing are eligible to receive bonuses and/or free bets from time to time, including a bonus when signing-up and for each referral.
    </li>
    <li>
        Bonuses and/or free bets shall be non-transferable and non-refundable.
    </li>
    <li>
        We only allow one bonus/free bet per account/household or environments where computers are shared.
    </li>
    <li>
        A bonus/free bet is only valid if the rollover requirement associated with the said bonus has been fulfilled.
    </li>
    <li>
        These requirements/terms may vary depending on the bonus/free bet given and you must always refer to the terms and conditions of the particular bonus for clarification.
    </li>
    <li>
        Please note that bonuses/free bets are not cumulative, for example, we offer a standard $150 signup bonus; this cannot be combined with a 25% promotion or special to become a larger bonus.
    </li>
    <li>
        Poker bonuses fall under an entirely separate agreement and are awarded and released within the poker application.
    </li>
    <li>
        Match-play bonuses awarded in the Casino require a real money deposit to be made before the member is eligible to receive these bonuses. Match-play bonuses are only paid out on ante bets, i.e., the initial amount wagered. Any further play bets are not included in the Match Bet offer.
    </li>
    <li>
        <p>
            9.1 Sign-up bonuses, reload bonuses and other cash rewards are subject to the following standard rollover requirements:
        </p>
        <ul>
            <li>Sports-betting activity equal to 30 times the amount of the bonus.</li>
            <li>
                Casino activity equal to 100 times the amount of the bonus. Craps and Roulette (all variations) and Certain Casino games, as specified in the Casino Rules, do not apply towards rollover requirements.
            </li>
            <li>Blackjack activity equal to 200 times the amount of the bonus.</li>
        </ul>
    </li>
    <li>Bonus policies can be viewed online</li>
    <li>
        The recycling of funds is not permitted. Requesting a withdrawal to make a deposit with little or no activity between the withdrawal and deposit does not earn a bonus.
    </li>
    <li>
        When you withdraw, some (or all) of your bonus funds may be removed from your account if their rollover requirements have not been met. The amount removed is proportional to the amount you are withdrawing: for example, if you are withdrawing 25% of your available balance, 25% of your bonus is removed. If you are withdrawing 100% of your available balance, 100% of your bonus is removed. This does not apply to free bets.
    </li>
    <li>
        Members must play their own money in order to earn a bonus. If you deposit, receive a bonus and then withdraw the amount of the deposit without risking your own funds, we will deduct both the bonus and the winnings.
    </li>
    <li>Management reserves the right to revoke a bonus and winnings derived from the revoked bonus at any time.</li>
    <li>
        Real money play in the Poker Room, Live Casino and/or Racebook does not count towards the rollover requirement of bonuses awarded in sports betting and flash/download casino areas of the site.
    </li>
    <li>
        <strong>Refer a Friend Promotion</strong>
        <p>The following terms and conditions apply:</p>
        <ul>
            <li>To be eligible for this offer the Referrer must have deposited before the Referred Friend.</li>
            <li>Referred Friend Minimum initial deposit should be $100.</li>
            <li>Maximum referral bonus is $200: $100 as free cash bonus and $100 as free bet.</li>
            <li>Free bet expires 15 days from the date of issue.</li>
            <li>Bonus will be paid seven days after the Referred Friend has made their first deposit.</li>
            <li>
                In order to be eligible as a valid referral, the Referred Friend must have used at least 50% of his initial deposit and must not have requested a withdrawal before completing the wagering requirement on his own Sign-Up Bonus.
            </li>
            <li>
                If, after seven days of the initial deposit, 50% of the amount has not been wagered, your referral bonus will be credited once it is wagered.
            </li>
            <li>
                The Referred Friend still receives their 25% Free Cash Sign-up Bonus offer even if they have not met the wagering requirements for a referral bonus.
            </li>
            <li>The Referred Friend 25% signup bonus offer supersedes all other signup bonus offers.</li>
            <li>There is no limit to the number of times you're credited a referral bonus, but limited to one bonus per Referred Friend.</li>
            <li>
                Opening multiple accounts is strictly against the house rules. If the referee already has an account with us (Inactive or otherwise) you will not be entitled to receive any referral bonus funds.
            </li>
            <li>All referral claims for credit must be made within 60 days of your friend/acquaintance (the referee) signing up.</li>
        </ul>
    </li>
</ol>
<h3>Free money or Cash Code Bonuses are subject to the following restrictions:</h3>
<p>
    If a member attempts to withdraw winnings derived entirely from a free cash bonus prior to meeting the requirements specified in the promotion, both the winnings and promotional bonus shall be forfeited.
</p>

<h3>Abuse of Bonus Programs:</h3>
<p>
    Our bonus program is intended for recreational bettors only. Our Gaming provider reserves the right to revoke bonuses for and further sanction any member considered to be abusing the bonus system. Bonus abuse includes:
</p>
<ul>
    <li>Cashing out for the purpose of redepositing</li>
    <li>Betting both sides of a single event for the purpose of falsely achieving bonus rollover requirements</li>
    <li>Referring new accounts for your own use</li>
</ul>
<span>The sanctions may take the form of:</span>
<ul>
    <li>Increased rollover requirement</li>
    <li>Loss of bonus privileges for the offending account and any linked accounts</li>
    <li>Geographical restrictions on bonus eligibility</li>
</ul>
<h2>WITHDRAWING FUNDS</h2>
<p>
    We accept withdrawal requests 24 hours a day, 7 days a week and the minimum withdrawal is $100. There is a maximum of 1 withdrawal request per day.
</p>
<p>
    Unless otherwise stated, all members will be entitled to one free withdrawal per calendar year. Additional withdrawals sent within the same calendar year will incur a fee.
</p>
<p>
    <strong>Please note:</strong> On occasions we may have to send your withdrawal via a method other than the one you have chosen. In the event that the method is changed, we will notify you.
</p>
<p>Before you request a withdrawal, please make sure your address is correct and up-to-date.</p>
<p>If a stop payment is requested by the member, it is the member's responsibility to assume the stop-payment fees of a minimum of $50.</p>
<p>
    A <strong>pending</strong> withdrawal may be cancelled if desired; you must access the Withdrawal Confirm Cancel page within the Cashier by selecting Cancel from either:
</p>
<ul>
    <li>Withdrawal Success Page</li>
    <li>Withdrawal/Transaction History page</li>
</ul>
<p>Once the withdrawal cancellation is submitted the money will be credited back to your wagering account immediately.</p>
<p>
    If Fedex are unable to deliver your package they will hold it at the local depot/office for a further period (usually about 7 days). You can call them quoting the tracking number and arrange a convenient time for delivery or arrange to collect the package yourself from the local depot/office. If the given time elapses and you have not contacted Fedex they will return the package to us, and once received we will credit the funds back into your wagering account.
</p>
<p>
    If your check has been delivered to a previous residence please try and retrieve the check from that residence if at all possible. This is more likely to occur when delivering to apartments where the concierge or receptionist will sign for the package.
</p>
<p>
    If all these avenues have been exhausted and you still do not have the check in your hands, please contact us and we will place a stop on the check. Once we ask the issuing bank to stop payment, we have to wait out the clearing process. This process may take from 2 - 8 weeks, or longer, to complete and may cost you up to $250. Once the stop is confirmed, the funds are placed back in your wagering account (less the stop check fee which could be as much as $250) so the withdrawal process can be started again.
</p>
<h2>REVERSED DEPOSITS</h2>
<p>
    Whenever a deposit is stopped/reversed Management reserves the right to void all activity in the account and the account holder will forfeit any accounts winnings deriving from the stopped/reversed deposit.
</p>
<h2>DORMANT AND ABANDONED ACCOUNTS</h2>
<p>If, after your first deposit, there is no activity on your account within 7 days, the deposit will be refunded to your card.</p>
<p>
    If no real money transaction has been recorded on an account for more than twelve (12) months it shall be determined to be  dormant and we shall lock the account for the member protection. If requested by the member, the account will be unlocked, but the member will have to supply the correct answers to all security questions. The unlocking of an account must also be approved by the Management.
</p>
<i>
    Management has the right to charge a $100 monthly account management fee on all accounts where there has been no activity in the preceding 12 months. Players who wish to return to play with us are eligible to have any account management fee previously charged re-credited back to their account with Management's approval. Please note: for accounts located within a restricted region the monthly account management fee will be charged within 30 days of inactivity or the date of account. Additional Management fees may apply.
</i>
<p>
    If no real money transaction has been recorded on an account for more than five (5) years it shall be determined to be abandoned and Management is obliged to inform the member of any balance remaining in the account. If the member cannot be contacted then the balance will be remitted to the Antigua and Barbuda Financial Services Regulatory Commission where it will be held securely for the member.
</p>
<h2>CONFIDENTIALITY</h2>
<p>
    US Racing shall undertake to maintain your anonymity unless you agree to your identity being used for future publicity or other purpose
</p>
<h2>LIABILITY OF US RACING's GAMING PROVIDER</h2>
<ol>
    <li>The decisions of the Management will be final and binding in all matters between the Gaming Provider and members.</li>
    <li>
        Laws regarding gaming vary throughout the world and internet casino gambling may be unlawful in some jurisdictions; it is the responsibility of members to ensure that they understand and comply fully with any laws or regulations relevant to themselves in their own country/state/locale.
    </li>
    <li>Management does not undertake to notify members that they have outstanding balances to collect.</li>
    <li>Any wager made by the member will be member's liability in respect to any claim or loss.</li>
    <li>Management shall reserve the right to suspend or withdraw any game at its absolute discretion.</li>
    <li>
        All personal details of all members will be held in confidence unless members agree to their identities and details being used for future publicity purposes.
    </li>
    <li>
        Management reserves the right to at any time, request documentation from a member before releasing a withdrawal or for general auditing or identification purposes. We also reserve the right to request additional documentation or photographic proof of yourself with the documentation.
    </li>
    <li>Withdrawals based on account closures are subject to a $100 processing fee.</li>
    <li>In the event of the cancellation of any race or game for any reason, Management will not be liable.</li>
    <li>
        <p>Management will not be liable in the following scenarios:</p>
        <p>
            In the event of force majeure the failure of the US Racing or its Gaming Provider's central computer system or any part thereof for delays, losses, errors, or omissions resulting from failure of any telecommunications or any other data transmission system for any loss as a result of any act of God for an outbreak of hostilities, riot, civil disturbance, acts of terrorism for the acts of Government or authority (including refusal or revocation of any license or consent) for fire, explosion, flood, theft, malicious damage, strike, lockout, or industrial action of any kind.
        </p>
    </li>
    <li>
        Management and the member shall not commit or purport to commit the other to honor any obligation other than is specifically provided for by these rules.
    </li>
    <li>
        US Racing and its Gaming Provider accept no liability for any damages, which may be caused to the member by the interception or misuse of any information transmitted over the internet.
    </li>
    <li>These rules constitute the entire agreement and understanding between US Racing, the Gaming Provider and the member.</li>
    <li>
        The maximum dollar amount that may be won by an individual member on a weekly basis is US$100,000.00. Please note, our weeks run Tuesday thru Monday. See below for large parlays and progressive jackpots.
    </li>
    <li>
        In the event that $50,000 or more of the weekly winnings is the result of a winning a 10, 11 or 12 team parlay or the $100,000.00 Pick 13? Parlay Challenge, then the weekly maximum winnings is raised to $200,000.
    </li>
    <li>
        In the event of a member winning in excess of $50,000 on a progressive jackpot in any of the casino games where it is offered, the member agrees to be photographed while receiving a check from one of our representatives and gives US Racing and its Gaming Provider the exclusive right to use the photographic material at its own discretion. The weekly limit on winnings of $100,000 does not apply to progressive jackpot winnings.
    </li>
    <li>Management reserves the right to refuse or limit any wager.</li>
    <li>
        Warning: Gambling involves risk. By gambling on this website, you run the risk that you may lose money or suffer psychological injuries. You gamble at your own risk.
    </li>
</ol>
<p>
    All gaming which is marketed by US Racing is provided by the Gaming Provider. US Racing is an online horse racing information website; it does not conduct any real money or play for fun gaming in any form. US Racing licenses its trademark and provides marketing and consulting services to companies involved in the horse racing industry.
</p>
<h2>OUR SOCIAL RESPONSIBILITY</h2>
<p>As an internationally respected online gaming company we are committed to promoting a responsible attitude to gambling.</p>
<p>
    For most people gambling is entertainment - an enjoyable activity that has no harmful effect. But for some it becomes a serious problem. Compulsive gambling is not easily detected and individuals with gambling problems will often go to great lengths to disguise the addiction and the problems that it causes.
</p>
<p>
    We are concerned about problem gambling. We will immediately inactivate any wagering account belonging to a member for whom gambling has become a problem and that account will remain permanently closed. Furthermore, any new accounts opened by that member will be inactivated.
</p>
<p>Neither do we condone underage gambling. Members must be at least 18 years of age.</p>
<h2>HORSE RACING</h2>
<p>
    US Racing offers daily racing from tracks within the USA (and internationally) via the racing interface which is provided by its Gaming Provider and which is accessible once you have logged-in to your wagering account.
</p>
<p>
    We accept wagers up until post time. We cannot assume liability for wagers that are unsuccessfully entered before post time. Before logging-out, please check your Active Wagers on the Reports menu to make sure your bets were accepted.
</p>
<p>Once you submit a bet online, your wager is considered final.</p>
<p>
    Bets shall not be accepted after post-time. In the event that the race begins before the advertised post-time and for that reason (or any other reason) you are able to place a wager after the race has started, that wager will be void.
</p>
<h3>Track Categories</h3>
<p>Click here for an up-to-date list of tracks offered and their categories.</p>
<h3>Odds and Limits</h3>
<p>Each event has a bet limit (typically $1,000.00).</p>
<p>
    Win, Place, Show wagers pay full track odds. All Exotic wagers pay full track odds up to the maximum pay-outs as shown in the tables below.
</p>
<p>
    Payouts are based on the actual track payout. The morning lines provided when wagering on the Horse Racing Interface are for informational purposes only and not used to calculate the payout.
</p>
<p>
    There are no house odds. If there are no track payoffs for a certain type of wager, all wagers on that type will be refunded.
</p>

<div class="row">
    <div class="col-sm-4 col-md-4 col-lg-4">
    <table border="1" class="data table table-condensed table-striped table-bordered" width="50%" cellpadding="0" cellspacing="0" summary="MVR  -  Mahoning Valley Racecourse">
        <caption> Class A Tracks </caption>
        <thead>
            <tr>
                <td ><strong>Bet Type</strong></td>
                <td ><strong>Max. Payoff</strong></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Win</td>
                <td>Track odds</td>
            </tr>
            <tr>
                <td>Place</td>
                <td>Track odds</td>
            </tr>
            <tr>
                <td>Show</td>
                <td>Track odds</td>
            </tr>
            <tr>
                <td>Exacta</td>
                <td>500-1</td>
            </tr>
            <tr>
                <td>Quinella</td>
                <td>400-1</td>
            </tr>
            <tr>
                <td>Trifecta</td>
                <td>1000-1</td>
            </tr>
            <tr>
                <td>Superfecta</td>
                <td>1500-1</td>
            </tr>
            <tr>
                <td>Pick 3</td>
                <td>1500-1</td>
            </tr>
            <tr>
                <td>Pick 4</td>
                <td>5000-1</td>
            </tr>
            <tr>
                <td>Daily Double</td>
                <td>750-1</td>
            </tr>
        </tbody>
    </table>
    </div>

    <div class="col-sm-4 col-md-4 col-lg-4">
    <table border="1" class="data table table-condensed table-striped table-bordered" width="50%" cellpadding="0" cellspacing="0" summary="MVR  -  Mahoning Valley Racecourse">
        <caption> Class B Tracks </caption>
        <thead>
            <tr>
                <td><strong>Bet Type</strong></td>
                <td><strong>Max. Payoff</strong></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Win</td>
                <td>Track odds</td>
            </tr>
            <tr>
                <td>Place</td>
                <td>Track odds</td>
            </tr>
            <tr>
                <td>Show</td>
                <td>Track odds</td>
            </tr>
            <tr>
                <td>Exacta</td>
                <td>400-1</td>
            </tr>
            <tr>
                <td>Quinella</td>
                <td>300-1</td>
            </tr>
            <tr>
                <td>Trifecta</td>
                <td>750-1</td>
            </tr>
            <tr>
                <td>Superfecta</td>
                <td>1000-1</td>
            </tr>
            <tr>
                <td>Pick 3</td>
                <td>1000-1</td>
            </tr>
            <tr>
                <td>Pick 4</td>
                <td>2000-1</td>
            </tr>
            <tr>
                <td>Daily Double</td>
                <td>400-1</td>
            </tr>
        </tbody>
    </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-4 col-md-4 col-lg-4">
    <table border="1" class="data table table-condensed table-striped table-bordered" width="50%" cellpadding="0" cellspacing="0" summary="MVR  -  Mahoning Valley Racecourse">
        <caption> Class C Tracks </caption>
        <thead>
            <tr>
                <td><strong>Bet Type</strong></td>
                <td><strong>Max. Payoff</strong></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Win</td>
                <td>Track odds</td>
            </tr>
            <tr>
                <td>Place</td>
                <td>Track odds</td>
            </tr>
            <tr>
                <td>Show</td>
                <td>Track odds</td>
            </tr>
            <tr>
                <td>All Exotics 300-1</td>
            </tr>
        </tbody>
    </table>
    </div>

    <div class="col-sm-4 col-md-4 col-lg-4">
    <table border="1" class="data table table-condensed table-striped table-bordered" width="50%" cellpadding="0" cellspacing="0" summary="MVR  -  Mahoning Valley Racecourse">
        <caption> Class D Tracks (Max Bet = $250) </caption>
        <thead>
            <tr>
                <td><strong>Bet Type</strong></td>
                <td><strong>Max. Payoff</strong></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Win</td>
                <td>Track odds</td>
            </tr>
            <tr>
                <td>Place</td>
                <td>Track odds</td>
            </tr>
            <tr>
                <td>Show</td>
                <td>Track odds</td>
            </tr>
            <tr>
                <td>All Exotics 300-1</td>
            </tr>
        </tbody>
    </table>
    </div>
</div>
<p>Maximum pay-outs per event are as follows:</p>
<ul>
    <li>Class A tracks: $20,000.00</li>
    <li>Class B tracks: $15,000.00</li>
    <li>Class C tracks: $10,000.00</li>
    <li>Class D tracks: $3,000.00</li>
    <li>Class E tracks: $1,000.00</li>
</ul>

<p>In the event a new track is not listed in one of the categories, its default payout limit will be Category E.</p>
<p>
    In the event that there is evidence of pool manipulation, all affected wagers will be deemed void and all monies wagered on the affected wagers will be refunded.
</p>
<p>
    Management reserves the right to refuse or limit any wager and to restrict wagering on any event at any time without any advance notice.
</p>
<h3>Scratches</h3>
<p>
    If a horse is scratched, all Win/Place/Show wagers placed via the racing interface will be refunded. <i>(This does not apply to Odds to Win bets placed via the sports betting interface. On those bets a scratch counts as a loss).</i> In case a horse is considered a non-starter by the track, that horse will be considered a scratch and refunded accordingly.
</p>
<p>
    In exotic wagers (Exacta, Trifecta, Superfecta, Quinella) the portion containing the scratched horse will be refunded. We do not honor all payouts in any exotic wager even though the track may do so. In the case that there is an all payout on a particular race, we will replace the all payout with the rightful winner of that race and/or position.
</p>
<p>
    On Daily Doubles, Pick 3 and Pick 4 wagers, a scratch will result in an automatic refund of the combination including the scratched horse. There will be no consolation payouts. No special track payouts will be recognized:
</p>
<h3>Pick 3 and Pick 4 Grading and Payout Rules</h3>
<p>
    All Pick 3 and Pick 4 tickets are paid out based on the official race results published by the track at which the race was run. If there is a scratched horse in any Pick 3 or Pick 4, only combinations including the scratched horse will be refunded.
</p>
<p>
    There will be no consolation payouts. No special track payouts (such as payouts on scratched horses, 2 out of 3 special Pick 3 payouts and/or 3 out of 4 special Pick 4 payouts) will be recognized.
</p>
<h3>Official Source of Results</h3>
<p>
    For thoroughbred racing we use <a href="www.equibase.com">www.equibase.com</a> as our official source of results. For harness racing we use <a href="www.ustrotting.com">www.ustrotting.com</a> or the tracks's.
</p>
<p>In the event of a discrepancy, we will contact the track for the official result.</p>
<h3>Daily Rebate</h3>
<p>US Racing offers a generous rebate program:</p>


<table border="1" class="data table table-condensed table-striped table-bordered" width="50%" cellpadding="0" cellspacing="0" summary="">
    <thead>
        <tr>
            <td></td>
            <td>Straights</td>
            <td>Exotics</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Category A</td>
            <td>3%</td>
            <td>8%</td>
        </tr>
        <tr>
            <td>Category B</td>
            <td>3%</td>
            <td>5%</td>
        </tr>
        <tr>
            <td>Category C</td>
            <td>3%</td>
            <td>3%</td>
        </tr>
        <tr>
            <td>Category D</td>
            <td>No rebate</td>
            <td>No rebate</td>
        </tr>
        <tr>
            <td>Category E</td>
            <td>No rebate</td>
            <td>No rebate</td>
        </tr>
    </tbody>
</table>
<p>
    This rebate only applies to wagers placed through the horse-wagering interface. It does not apply to any horse matchups or other future wagers made through the sports betting login. No rebate will be given on cancelled wagers or wagers refunded due to a scratch. No rebate will be paid on Win, Place and Show tickets that pay $2.20 to $2 or less. The rebate bonus will be paid on a daily basis. There is no rollover requirement associated with the rebate.
</p>
<h3>Matchups</h3>
<p>The following rules apply to horse matchup bets made via the sports betting interface.</p>
<ul>
    <li>In matchups between two or more named horses, all horses must go for wagers to have action.</li>
    <li>Matchups are determined by horse name. Any track coupling is irrelevant towards determining the winner of a matchup.</li>
    <li>
        In matchups between one named horse and the Field, wagers will have action if and only if the named horse starts. Scratches of horses in the Field will not affect the standing of the wager.
    </li>
    <li>Matchups are not eligible for rebates.</li>
    <li>
        The winner of a matchup will be determined by the highest placed finisher involved in the matchup. If no horse finishes, the matchup will be graded no action.
    </li>
    <li>Matchup wagers which indicate Turf Only will be cancelled if the surface is changed.</li>
    <li>In the case of a dead heat, the matchup will be graded no action.</li>
</ul>

<h2>PROMOTIONS</h2>
<p>
    If, as part of a promotion offered by US Racing, you receive betting cash, you can only use such amount as wagers for playing in the games offered by the company. You are not entitled to withdraw such amounts and we reserve the right to reclaim those amounts if you do not use them to play the games during the period specified in the promotion.
</p>
<p>Only members of US Racing or its Gaming Provider can redeem promotional prizes and winnings from promotions.</p>
<p>
    If you have more than one betting account, all promotional credits and/or winnings will be null and void and they will be deducted from your account.
</p>
<p>
    If a member wagers both sides of a game using a promotional bet (to bet at the same time on the favorite and the underdog) or using bonus money, Management considers that to be fraudulent behavior. This includes making bets of this nature in two or more of our affiliated websites. Any money won playing both sides of the game will be deducted, together with any other promotional money deposited in the member account.
</p>
<p>Management reserves the right to modify any promotion rules or cancel promotions at its own discretion.</p>

<h2>SPORTING EVENTS</h2>
<p>Las Vegas rules, regulations, payoffs and wager types apply where not covered herein.</p>
<p>
    All sporting events with the exception of the Super Bowl* must be played on the date and site scheduled unless otherwise specified. Sporting events postponed by more than 12 hours and or rescheduled, will constitute no-action and all money will be credited accordingly.
</p>
<p>
    All bets must be made and accepted before the start of the game/event wagered upon. Any bet placed or received after the start of the game/event wagered upon will be void.
</p>
<p>
    Management reserves the right to limit the maximum amount wagered prior to acceptance of any bet. Members are not permitted to open multiple accounts (either within US Racing itself or affiliated websites on the same platform) in order to circumvent the limits imposed by our wagering system. If multiple accounts are used then all bets will be void.
</p>
<p>
    In the event that monies are credited to a member's account in error, it is incumbent upon the member to notify Management of the error without delay. Any winnings realized after the error, whether directly or indirectly subsequent to the error but prior to notification of Management, shall be void.
</p>
<p>
    Management does not recognize suspended games, protests, scoring amendments, result reversals and overturned decisions for wagering purposes.
</p>
<p>
    In order for a bet to be valid, the member must receive a bet identification number from the Gaming Provider confirming that the bet has been accepted. This number appears via a confirmation message that will appear on your betting ticket.
</p>
<p>
    In general, accepted bets cannot be cancelled or amended in any way, either by the member or by Management, except in the following circumstances:
</p>
<ul>
    <li>
        In the unusual case that a bet is duplicated by the system (the time-stamp on the two bets will be identical) one of the bets may be voided as long as the member contacts us before the event has taken place.
    </li>
    <li>
        If the member has placed a bet that is stated as forbidden in our House Rules then Management reserves the right to void that bet regardless of whether the game has started or not.
    </li>
    <li>If the bet is placed after the result is public knowledge (Exotics, Television, Politics, etc).</li>
</ul>
<p>
    Every effort will be made to ensure that all information available on the internet site is accurate. However, if an error is made in the prices or conditions published on-line, we reserve the right to either correct any mistakes or to void the affected bets or to settle any bets already laid at the correct price.
</p>
<h3>Winners and losers are official after:</h3>
<ul>
    <li>Football - NCAA & NFL - 55 minutes of play</li>
    <li>Basketball - NBA - 43 minutes of play</li>
    <li>Basketball - NCAA - 35 minutes of play</li>
    <li>Hockey - NHL - 55 minutes of play</li>
    <li>All other sporting events - 55 minutes of play</li>
    <li>
        Overtime periods, quarters or extra innings are counted in the final score when wagering on totals, money line and spread betting.
    </li>
    <li>On half time wagers, overtime periods are included as part of the 2nd half.</li>
    <li>
        Other sports: All other contests that involve a scheduled length of play or time limit must play to their conclusion or have five minutes or less of the scheduled playing time remaining when the contest concludes to be considered official for wagering purposes.
    </li>
</ul>

<p>
    <strong>All Bets Action:</strong> On certain future (long term) bets there may be a clause stating: All Bets Action This means that the wager will have action whether or not the contestant on whom you have bet takes part in the tournament/event.
</p>
<p>
    <strong>Live Scores disclaimer:</strong> We are not responsible for any inaccuracies in scores posted in our live scores.
</p>
<p>*All wagers on the Super Bowl stand even if the date, time or site has been changed.</p>
<p>Special Football Season Win Rules</p>
<p>
    "NFL Season Win Totals" only include regular season games and all 16 regular season games must be played for action. Play-off games and pre-season games do not count for this bet offering.
</p>
<p>
    "NCAA Season Win Totals" only include regular season games. Each team must play their scheduled 12 regular season games. Championship games and Bowl games do not count for this bet offering. Teams must play all scheduled opponents.
</p>
<p>Special Major League Baseball Rules</p>
<p>
    Major League Baseball games are official after 5 innings of play, or 4? innings if the home team is leading. If a game is subsequently called or suspended, the winner is determined by the score after the last full inning unless the home team ties the score or takes the lead in the bottom half of the inning in which the game was suspended. In that case the winner will be determined by the score at the time the game is called. (This rule holds for betting purposes even if the game is suspended and /or completed on a different day than it began).
</p>
<p>
    When wagering on total runs or run lines, the game must go the regulation 9 innings, or 8? innings if the home team is leading, otherwise it will constitute a no-action wager, and all money will be credited accordingly.
</p>
<p>
    Baseball wagers are accepted as Listed. In the event of a pitching change prior to the game, all wagers on the game with said listed pitchers are considered no-action . All wagers will be refunded to members accounts as soon as official confirmation is received allowing you to make the bet again with the new pitcher and the new price if you so wish. Starting pitchers must throw at least one pitch for wager to be deemed Action. Each team starting pitcher is defined for betting purposes as the pitcher who throws the first pitch.
</p>
<p>
    Any and all baseball series wagers must have all 3 games played by the end of a specified date for the wager to have action. If all 3 games are not played by that specified date then all wagers have  no-action , regardless if one team has 2 wins. All 3 games must be official by Las Vegas Gaming Standards (see our rules regarding baseball wagers and what constitutes an official result).
</p>
<p>
    <strong>Note:</strong> Each team's starting pitcher is defined for betting purposes as the pitcher that throws the first pitch.
</p>
<p>
    <strong>Note:</strong> If we provide a baseball series price for a series that has 4 games, for grading purposes, the wager is based on the initial 3 games.
</p>
<p>
    All baseball props must go the full 9 innings (8? if the home team is winning) unless otherwise stated. They also must start with the listed pitchers. If a game is suspended and/or completed on a different day than it began, props will be graded no-action unless otherwise stated.
</p>
<p>
    In baseball, all games that go at least the full 9 innings and finish tied as a result of a suspension of play, shall be graded as a push on all money line bets. However, all run line and total bets will have action. Note that this is an extremely rare occurrence. In the event of a change in Las Vegas rules, Management will adhere to the decision set forth in Las Vegas.
</p>
<p>
    "First Five Innings Bets" are accepted as listed pitchers and the game must go a full 5 innings to have action regardless if the game is called or postponed after the 5th inning.
</p>
<p>
    "Team to Score First" props are accepted as listed pitchers and will be graded as soon as the first team has scored and the home team has had a turn at bat. If the home team scores first, that bet will be graded at that time and bets are action regardless of game suspension or postponement. If the visiting team scores first, the bet will be graded once the home team has completed its turn at bat.
</p>
<p>
    "Will there be a Score in the First Inning? props are accepted as listed pitchers and will be graded as soon as the first inning ends. All bets will be action regardless of game suspension or postponement, as long as the first inning is completed.
</p>
<p>
    "Total Runs + Hits + Errors props are accepted as listed pitchers and the game must go the full 9 innings (8? if the home team is winning) for bets to have action.
</p>
<p>For "In-Game Betting" games must go a full 8? innings for bets to have action.</p>
<p>
    Baseball Player and Pitcher Props are accepted as "listed pitchers". The game must go a full 9 innings (8? "if the home team is winning) for bets to have action. If there is a pitching change, all player props for that game will be closed immediately. When the game starts and it is confirmed that a pitcher other than the pitcher listed on the bet threw out the first pitch, those props will have "no-action". (If the game is suspended and/or completed on a different day than it began, Player/Pitcher Props will have "no-action ).
</p>

<p>
    Baseball Grand Salami: The Baseball Grand Salami will be decided by the total runs scored in all major-league games scheduled for that day. All scheduled games must go at least 9 innings (8.5 if the home team is winning). If any scheduled game is canceled or stopped before the completion of 8.5 innings, all wagers on the Baseball Grand Salami will be canceled. No pitchers will be listed for the Grand Salami; all wagers will have action regardless of the starting pitchers.
</p>
<p>Other Baseball Tournaments or Leagues</p>
<p>
    For NCAA Baseball games, or any other non-MLB league, pitchers are generally not listed or required so pitching changes will not affect the bet. If pitchers are listed, games will be treated in exactly the same way as MLB games.
</p>
<p>
    College World Series Moneylines, Runlines and Totals are all action regardless of when the game is completed. Unlike MLB rules, if a game is postponed your bet will not be graded until the completion of that game, at which point all bets will be graded and paid.
</p>
<h4>Boxing</h4>
<p>When the bell sounds for the first round, the bout will be considered official, regardless of the scheduled length or title.</p>
<p>
    The official stopping of a round before the sounding of the bell does not constitute a full round. A full round is only considered for wagering purposes when the bell sounds signifying the end of said round.
</p>

<p><strong>Special Boxing Rule:</strong> Fights must be run within two months of the specified date for your bet to have action.</p>
<h3>Boxing Proposition Bets</h3>
<p>For any proposition bets the following apply:</p>
<ul>
    <li>The rules stated on the card at the time of placing the wager will govern.</li>
    <li>When the bell sounds for the first round, the bout will be considered official.</li>
</ul>
<span>
    When wagering on Pick the Round propositions, in which you must pick the round in which the bout is won, the following will apply:
</span>
<ul>
    <li>If the fight ends in a points verdict or a draw, the bettor loses.</li>
    <li>If either fighter fails to answer the bell for a round, the fight is adjudged to have finished in the previous round.</li>
</ul>

<p>
    However, this does not apply to specific fight props which must come-out as stated, such as a draw, either fighter by knockout or decision, or a specific round knockout etc.
</p>
<h4>Golf</h4>
<p>
    For tournament match-up betting, both players listed in the match-up must tee-off for your bet to be deemed Action. The player with the most completed holes wins. If the players complete the same number of holes, then the player with the lowest score wins. If the players are still tied then the wager shall be deemed no-action and all monies will be refunded. If both golfers in a match-up are in a playoff, the winner of play-off wins the match-up.
</p>
<p>
    Should a tournament be shortened, or otherwise affected, due to weather conditions the official result will be used when settling, regardless of the number or rounds played. However, should there be no further play after a bet is struck that bet will be void.
</p>

<p>
    Single day match-ups are wagers on the particular day 18-holes. Single Day match-ups do not include holes played as part of a completion from the previous day round or playoff holes considered part of the overall tournament score. Should a day round be shortened, or otherwise affected, due to weather conditions and the round is continued the next day, the full 18 holes shall be considered in determining the outcome of the bet even if they are played over two days. Both golfers must tee off for action. If both players end the 18 holes in a tie, the money line wagers shall be refunded and stroke line wagers will be deemed Action .
</p>
<p>For single round-single player propositions, all 18 holes must be completed.</p>
<p>
    For single round betting on match play tournaments, the players/team must tee off for action. 18 holes do not necessarily need to be played.
</p>
<p>
    Golf Odds to Win: Wagers on a golfer who does not play in the tournament are graded as no-action and all monies refunded (unless otherwise specified). A golfer is deemed to have played once he or she has teed off. In the event of a player withdrawing after having teed off, wagers on that player will be lost.
</p>
<p>Las Vegas Rules apply for any rules not mentioned here.</p>
<p>
    Stroke Line Betting: In golf the lower score wins. If you bet (-1/2) stroke, your golfer must shoot 1 or more shots lower than the opponent. If you bet (+1/2) your golfer must shoot the same score or a lower score than the opponent. Example: should you bet Singh (-1/2) vs. Mickelson and Singh shoots 69, Mickelson 70, then Singh (-1/2) is the winner. If Singh shoots 69 and Mickelson shoots 69, then Mickelson (+1/2) is the winner.
</p>
<p>
    Rules for Finishing Top 3: In the case of a tie or multiple players finishing in the top 3 position, wagers will be paid using our Dead Heat Rule.
</p>
<h4>General Soccer Rules</h4>
<p>
    All soccer bets (except those on Under 17s matches) will be settled on 90 minutes of play. This includes any time added by the referee in respect of injuries and other stoppages. In major knock-out tournaments, for example the latter stages of the World Cup, where a winner is required in order to progress to the next leg, bets are still settled on 90 minutes of play. Extra Time, Golden Goals and Penalty Shoot-Outs do not count.
</p>
<p>
    Bets on Under 17s Soccer matches will be settled on 80 minutes of play plus any time added by the referee in respect of injuries and other stoppages.
</p>
<p>
    Match details, such as dates and kick-off times, displayed on the website are for guidance only and may be amended or taken off the board at any time. Soccer events officially postponed by more than 24 hours and/or rescheduled, will constitute no-action and all money will be credited accordingly. The exceptions to this rule are official international and club tournament games (e.g. World Cup or Champions League) where a match must be played regardless, in which case all bets will be actioned on the rearranged game.
</p>
<p>
    If a match is abandoned and/or suspended all bets shall be void unless the relevant wagering option has already been decided. For example, a bet on the First Goal Scorer will stand if a goal has already been scored.
</p>
<p>
    Where a venue is changed, bets will stand unless the game is to be played at the original away team ground in which case all bets will be voided.
</p>
<p>For soccer wagering, in order to place your bet correctly you must predict the result of at least one game choosing either:</p>
<ul>
    <li>the Home team,</li>
    <li>the Away team or</li>
    <li>a draw.</li>
</ul>
<p>Should you parlay multiple games together, you must win all games in the parlay in order to collect any winnings.</p>
<p>Bets taken on<strong>"Half Time Results"</strong>will be settled on 45 minutes of the first half plus any added injury time or stoppage time.</p>
<p>
    Bets taken on<strong>"2nd Half Lines"</strong>will be settled on the 45 minutes of the second half plus any added injury or stoppage time. Extra Time, Golden Goals and Penalty Shoot-Outs do not count. Goals scored in the first half do not count toward the second half wager.
</p>
<p>
    Bets taken on the <strong>"Soccer Grand Salami"</strong> must predict the total (over/under) goals scored in a particular soccer league per round or league fixture (including games played on both weekdays and weekends as long as they comprise part of the same round). If any scheduled game is canceled/postponed/rearranged all wagers on the Soccer Grand Salami will be canceled/no action.
</p>
<p>
    Bets taken on <strong>"Draw No Bet"</strong> must predict which team wins the match in regular time (90 minutes plus any time added by the referee in respect of injuries and other stoppages). In the event of the match ending in a draw (tie), bets will have "no action".
</p>
<p>
    Future bets placed on<strong>"Odds to Win"</strong> must predict the team winning the division, league or any other tournament and will be paid as soon as the said division, league or tournament has finished.
</p>
<p>
    Bets taken on <strong>"Correct Score"</strong> must predict the match score at the end of regular time (90 minutes plus any time added by the referee in respect of injuries and other stoppages).
</p>
<p>Bets taken on <strong>"Double Chance"</strong> must predict the outcome of the match based on three selections, either:</p>
<ul>
    <li>the Home or Away team wins,</li>
    <li>the Home team wins or draws or</li>
    <li>the Away team wins or draws.</li>
</ul>
<p>
    Bets taken on <strong>"Half with Most Goals"</strong> must predict which half has most goals scored by either team in regular time (90 minutes plus any time added by the referee in respect of injuries and other stoppages).
</p>
<p>
    When betting on Double Result(half time - full time) you must predict the result at half time and at the end of normal time (90 minutes).
</p>
<p>
    When betting on <strong>"Goals: odds or even"</strong> you must predict if the total score by both teams ends up as an odd or an even number. A 0-0 score counts as an even number. Bets will be settled on 90 minutes of play plus any time added by the referee in respect of injuries and other stoppages.
</p>
<p>
    Bets taken on <strong>"Kick Off Bet"</strong> must predict which team will kick-off the match.  Odds will be taken off the board 15 minutes before the match starting time.
</p>
<p>
    For the <strong>"Goal/No Goal"</strong> bet you can choose to wager on either "Goal" for both teams to score, or "No Goal" for either or both teams not to score.
</p>
<p>
    When betting on <strong>"Soccer Special Props"</strong> or <strong>"Match Specials"</strong> you are required to predict the team or teams to win the game, event, tournament, competitions, match-up or any other situation within the game.  Any cancelled or postponed game will cause the entire event to have "no action" .  Soccer Specials will be settled on 90 minutes of play.
</p>
<p>Soccer Goal-scorer Markets</p>
<ol>
    <li>
        <strong>To score the first goal:</strong>
        <ul>
            <li>
                Bets on players not taking part in the match will be no-actioned. Bets on player to score the first goal where the selection comes on after the first goal is scored will be no-actioned, unless the first goal is an own goal, in which case bets will be actioned.
            </li>
            <li>
                In the event of a dispute over the award of a goal, for betting purposes, settlement will be based on the goal-scorer listed by the official sources immediately after the match has finished. Any subsequent amendments to official sources records will not count.
            </li>
            <li>
                Own goals do not count in pre-match first goal-scorer betting. In the event of an own goal first goal-scorer bets are not no-actioned but carry forward to the next goal.
            </li>
            <li>Extra time does not count.</li>
        </ul>
    </li>
    <li>
        <strong>To score the last goal:</strong>
        <ul>
            <li>
                Predict the player who will score the last goal in the match. All players that take part will be considered runners for last goal-scorer purposes. Every effort will be made to quote last goal-scorer odds for all participants. However, other players will always be quoted on request and will count as winners should they score the last goal.
            </li>
            <li>
                Bets on last goal-scorers that are substituted/sent off before the last goal is scored will be treated as losing selections.
            </li>
            <li>
                In the event of a dispute over the award of a goal, for betting purposes, settlement will be based on the goal-scorer listed by the official sources immediately after the match has finished. Any subsequent amendments to official sources records will not count.
            </li>
            <li>Own goals do not count. If the last goal is an own goal bets default to the previous goal.</li>
            <li>Extra time does not count.</li>
        </ul>
    </li>
    <li>
        <strong>Anytime Scorer, to Score 2 or more goals, to Score a hat-trick:</strong>
        <ul>
            <li>Select a player to score at any time during the match. Unless otherwise stated, extra time does not count.</li>
            <li>
                Players must start the match for bets to be valid. Stakes will be refunded on anytime goal-scorers who do not start, whether or not they score.
            </li>
            <li>Any selection taken from a match that is not completed will be treated as a non-participant.</li>
            <li>Own goals do not count.</li>
        </ul>
    </li>
    <li>
        <strong>First goal-scorer and correct score (scorecast):</strong>
        <ul>
            <li>
                Predict the first goal-scorer and the correct score in a double (parlay). Bets will be settled at the combined odds displayed.
            </li>
            <li>
                If a player comes on after a goal has been scored or does not take part in the game, a bet involving that player will be settled as a straight wager on the selected correct score at the appropriate odds.
            </li>
            <li>
                For first goal-scorer purposes, own goals do not count. If the only goals in the match are own goals, bets will be settled as straight wagers on the selected correct score at the appropriate odds.
            </li>
            <li>
                If the match is abandoned after a goal has been scored, bets will be settled as straight wagers on the selected first goal-scorer at the appropriate odds.
            </li>
        </ul>
    </li>
    <li>
        <strong>Score First and Win:</strong>
        <ul>
            <li>
                A Score First and Win bet requires you to select a player to score first and his team to win in a double (parlay). Bets will be settled at the combined odds displayed.
            </li>
            <li>Unless otherwise stated, extra time does not count.</li>
            <li>If the player does not start, the bet will be no-actioned.</li>
            <li>
                If the only goals in the match are own goals, bets will be settled as a straight wager on the team to win at the appropriate odds.
            </li>
            <li>
                If the match is abandoned after a goal is scored, bets will be settled on the selected first goal-scorer at the appropriate odds.
            </li>
        </ul>
    </li>
    <li>
        <strong>Score Anytime and Win:</strong>
        <ul>
            <li>
                A Score Anytime and Win bet requires you to select a player to score anytime and his team to win in a double (parlay). Bets will be settled at the combined odds displayed.
            </li>
            <li>Unless otherwise stated, extra time does not count.</li>
            <li>If the player does not start the match or if a match is abandoned, bets are void.</li>
            <li>
                If the only goals in the match are own goals, a Score Anytime and Win bet will be settled as a straight wager on the team to win at the appropriate odds.
            </li>
        </ul>
    </li>
    <li>
        <strong>Player vs Player match bets:</strong>
        <ul>
            <li>This bet requires you to select a player to score more goals than his opponent.</li>
            <li>Extra time does not count.</li>
            <li>
                The two selections do not necessarily have to play each other in a win/lose situation; the grading is determined by one selection performance relative to the other.
            </li>
            <li>Both players must start their match(es) for bets to stand.</li>
            <li>Unless odds are quoted for a tie, in the event of a tie bets will be void.</li>
        </ul>
    </li>
    <li>
        <strong>Anytime Goal-scorer Double (parlay):</strong>
        <ul>
            <li>Predict a pair of players who will each score a goal at any time in the match in a double (parlay).</li>
            <li>If either or both players do not start the match the bet is void.</li>
            <li>Extra time does not count.</li>
        </ul>
    </li>
</ol>
<p>
    When betting on <strong>"Asian Handicap"</strong> you are essentially betting on a point (or goal) spread.  Asian Handicap offers only two betting options rather than the three possible outcomes normally offered on a soccer game (namely home, away or draw).
</p>
<table border="1" class="data table table-condensed table-striped table-bordered" width="50%" cellpadding="0" cellspacing="0" summary="">
    <tbody>
        <tr>
            <td>A</td>
            <td>+0.5</td>
            <td>(1.95)</td>
        </tr>
        <tr>
            <td>B</td>
            <td>-0.5</td>
            <td>(1.95)</td>
        </tr>
    </tbody>
</table>

<p>
    In this example, if you bet on team A they must win or draw the match for you to win your bet.  If you bet on team B they must win by at least one goal for you to win your bet.
</p>
<p>
    Betting on one of just two outcomes makes things simpler and it also allows you to get a better price when betting on the favorite.  Where offered, the Asian handicap will always include a half-ball, i.e. the spread will be +/- 0.5, +/- 1.5 or higher.  All Asian Handicap bets are based on regular time (90 minutes plus any time added by the referee in respect of injuries and other stoppages).
</p>
<p>
    Betting on <strong>"Extra Time"</strong> : In knock-out tournaments where a winner has not been decided after 90 minutes of play, extra time is added.  The Extra Time consists of 30 minutes of play (two halves of 15 minutes each) plus any time added to this by the referee in respect of injuries and other stoppages.  Where bets on Extra Time are offered, they are settled only on these additional 30 minutes.  Results achieved during regular time and/or penalty shoot-outs do not count.
</p>
<p>
    In a <strong>"Fantasy Soccer Match-up"</strong> two teams compete virtually.  The winner of this match-up is the team that scores the most goals on the actual match-day against a real opponent.  Only goals are counted, victory or loss is irrelevant.
</p>
<p><i>For example:</i> On a particular day Manchester United plays Manchester City and Chelsea plays Bolton.</p>
<p>You choose to bet on a Fantasy Match-up, Manchester United vs. Chelsea.</p>
<p>
    Manchester United win their match 1-0 against Manchester City, while Chelsea loses 2-4 to Bolton.  However, Chelsea would win the Fantasy Match-up with a 2 -1 score.
</p>
<p>
    <strong>In-Running Soccer</strong> betting is offered on selected games.  Once the game starts, in-running odds will be posted on the site under In-Running Soccer and will be updated constantly during the game, unless the game becomes extremely one-sided, in which case in-running betting will be suspended.  In-Running Soccer wagers are based on the final result of the entire game (90 minutes plus any added injury time or stoppage time).
</p>
<p>
    <strong>Beach Soccer:</strong> Beach Soccer is based on 36 minutes of play (3 periods of 12 minutes) followed by extra periods and penalty shots, if necessary, to decide who wins the match.  Bets are graded on normal regulation time (36 minutes plus any time added by the referee in respect of injuries and other stoppages).  Extra Time and Penalty Shoot-Outs do not count for wagering purposes.
</p>
<p>
    <strong>Futsal:</strong> All bets on Futsal matches are based on the score at the end of 40 minutes play unless otherwise stated or if the relevant wagering option has already been decided.  Extra Time and Penalty Shootouts do not count.  If a match is abandoned and/or suspended before the completion of 40 minutes play, all bets shall be void unless the relevant wagering option has already been decided.  All bets on matches that are postponed, rearranged or moved to a different venue shall be void.
</p>
<h3>Cricket</h3>
<p>For Cricket games, if the match is shortened by the weather, bets will be governed by the official competition rules.</p>
<h3>Hockey</h3>
<p>
    For wagering purposes, winners and losers are determined by the final score, provided that the game has gone the minimum time as specified above.
</p>
<p>
    Overtime: For wagering on games in the National Hockey League, the final score includes overtime. Regardless of the number of goals scored during the shootout portion of overtime, the final score recorded for the game will give the winning team one more goal than the score at the end of regulation time.
</p>
<p>
    Personal Player Stats and other Stats achieved during an NHL Shootout are not taken into account for certain proposition bets, for example: Player Total Shot on Goals, Player Total Points, Player Total Goals, Team to Score First, Team to Score Last.
</p>
<p>Las Vegas rules, regulations, payoffs and wager types apply where not covered herein.</p>
<h3>Rugby</h3>
<p>
    For Rugby Games, if any match is abandoned or postponed all bets will be void, except bets on the first try scorer if a try has been scored prior to abandonment.
</p>
<p>
    Where a venue is changed, bets will stand unless the game is to be played at the original away team ground in which case all bets will be void.
</p>
<h3>Tennis</h3>
<p>
    For tennis match betting, two full sets must be completed for wagers to stand.  If less than two sets are completed then all wagers will be considered "no-action" .  In the event of any of the following circumstances taking place, all bets will stand:
</p>
<ul>
    <li>a change of playing surface</li>
    <li>a change of venue</li>
    <li>a change from indoor court to outdoor court or vice versa</li>
    <li>The match is delayed or postponed due to inclement weather/bad light and is completed on a later date.</li>
</ul>
<h3>Motorsports</h3>
<p>
    When placing a wager on "Odds to Win" a race, wagers shall be deemed "no-action" should the driver not start the race.  For a motor sports match-up to be deemed Action both drivers must start the race.  The start of any motor race is defined as the signal to start the warm-up lap.  The official winner at the conclusion of the race will be the winner for betting purposes.  In League Championship wagering, drivers must race in at least one race during the season to be deemed "Action" .
</p>
<p>Special Motorsport Rule: Races must be run within eight (8) days of the scheduled date for your bet to have action.</p>
<p>
    The NASCAR.com website is our official source for the grading of all NASCAR wagers.  The race must complete the scheduled number of laps and/or distance for specific proposition wagers (pertaining to number of laps and/or distance) to be deemed "Action" .  Match-ups, finishing position props and future wagers will still have action regardless.
</p>
<p>
    Rules for Finishing Top 3:  In the case of a tie or multiple players finishing in the top 3 position, wagers will be paid using our Dead Heat Rule.
</p>
<p>Formula 1: At least 15 drivers must start the race for action.</p>
<h3>NCAA Basketball</h3>
<p>
    In the event of a tie in NCAA conference wagering, the team entering the conference tournament "seeded" higher shall be deemed the winner.
</p>
<h3>Parlay or Multiple Betting</h3>
<p>
    No parlay wagers can be accepted where individual wagers are "connected", "dependent" or "correlated". Using a baseball game as an example, let say you want to take the Boston Red Sox to win on the money-line and the Boston Red Sox on the run-line. If the Boston Red Sox win the game, it is also likely that they will win by at least two runs therefore the selections are said to be correlated and cannot be parlayed together. For the same reason, we do not allow the favorite/underdog on the runline and the over/under of the same game to be placed in the same parlay.
</p>
<p>
    In the same way, you cannot parlay the line for the first half of a football game with the line for the whole game or the total for the first half of a football game with the total for the whole game, as the two are "dependent" . Less obviously, but nonetheless correlated, are the favorite/underdog on the spread and the over/under of the same game placed together in a parlay or teaser bet when the spread is greater than or equal to 30% of the total.
</p>
<h3>Dead Heat Rule</h3>
<p>
    In the event of a dead heat (a tie), stakes will be divided by the number of selections that have tied, with the divided stake settled at full odds.  Remaining stakes are lost.  If the tie was a betting option, then the dead heat rule does not apply.  The dead heat rule only applies to "future" wagers.  For example, if there is a 3 way tie for the top scorer in a basketball game, then your winnings are calculated by taking your stake, dividing it by 3, and multiplying that amount by the odds on your betting ticket.
</p>
<h3>Buying Points</h3>
<p>Buying points allows you to change the point-spread or total of a football or basketball game.</p>
<p>
    You can move the point-spread so you get more points when betting the underdog or you give away fewer points when betting the favorite.  For each half point that you change the point spread you need to pay an extra 10c.
</p>
<p>
    So, for example, if you are betting the Lakers -5 -110 (lay $110 to win $100), you can buy the Lakers a half point to -4.5 -120 (lay $120 to win $100). You can buy the Lakers a full point to -4 -130 (lay $130 to win $100).
</p>
<p>The same theory is true for the underdog.  If the Bulls are +4 -110, you buy the game to +4.5 -120 or +5 -130.</p>
<p>You can only buy a maximum of 3 points in Football and Basketball, both College and Professional.</p>
<p>There are two special rules:</p>
<ol>
    <li>
        When buying on or off the number 3 in the Pro Football, there is a premium cost of 25c, not 10c.  So, for example, to move the Chargers from -3 -110 to -2.5, the new line will be -2.5 -135 (lay $135 to win $100).  The same is true for the underdog, to buy Bears from +3 -110 to +3.5 will be -135 (lay $135 to win $110).
    </li>
    <li>
        On half, reduced or no juice games or half, reduced or no juice days, there are additional premium costs for buying points:
        <ol>
            <li>In college football there is a 5c additional charge per half point purchased.</li>
            <li>For buying on or off the number 7 in college football, the premium is 25c.</li>
            <li>In professional football there is a 5c additional charge per half point purchased.</li>
            <li>For buying on or off the numbers 3 or 7 in professional football, the premium is 25c.</li>
        </ol>

    </li>
</ol>
<p>Please note: these premium costs for buying points apply from 12:01am to 11:59pm EST on the specified half, reduced or no juice day.</p>
<h3>Matchplay Matchups</h3>
<p>If a match does not start (e.g. player injured or disqualified before the start of a match) then all bets on that match will be void.</p>
<p>
    Bets on markets that can be settled by using the official tournament and match results (including final match correct score and individual match betting) will be settled using those results. This includes where a match finishes early either by agreement of the players or through injury.
</p>
<p>
    All other markets where a match finishes before completion of 18 holes (e.g. by agreement), such as match score, will be settled as if the remaining uncompleted holes are ties. For example, a player 2 up at the 13th hole when the match finishes will be deemed to have won 2 and 1 (at the 17th hole). Uncompleted single hole bets will be void.
</p>
<h3>Free Bets</h3>
<p>The following terms and conditions apply to all free bets:</p>
<ul>
    <li>One wager per member.</li>
    <li>You must have funds in your wagering account to cover the cost of the free bet (usually $10) and this will be refunded.</li>
    <li>All sides must play in order for the bet to have action.</li>
    <li>In case of tie, all bets will have "no-action."</li>
    <li>
        Wagering on both sides of this free bet or opening multiple accounts to take advantage of this offer will result in non-payment of all free wagers in the related accounts and a $5 penalty charge per free wager and may result in the disabling of the accounts and forfeiture of winnings.
    </li>
    <li>
        The free bets are open to all active members who make a minimum of 5 sports wagers per week or total action of $50 in poker, casino or horses per week.  Making only free bets may result in the disabling of the accounts and forfeiture of winnings.
    </li>
</ul>
<h2>CASINO</h2>
<p>
    In the event of a member winning in excess of $50,000 on a Progressive Jackpot in any of the casino games where a jackpot is offered, the member agrees to be photographed while receiving a check from one of our representatives and gives US Racing and its Gaming Provider the exclusive right to use the photographic material at its own discretion.   The weekly limit on winnings of $100,000 does not apply to progressive jackpot winnings.
</p>
<p>Management reserves the right to change the casino settings and payouts without any forewarning to the members at any given time.</p>
<p>
    Wagering in any of the casino games listed below does not count towards any bonus rollover requirements and Management shall make all decisions with regard to the application and retention of bonuses:
</p>
<ul>
    <li>BlackJack Switch</li>
    <li>Blackjack 52</li>
    <li>American Roulette</li>
    <li>European Roulette</li>
    <li>Mini Roulette</li>
    <li>Baccarat</li>
    <li>Pai Gow Poker</li>
    <li>Craps</li>
    <li>Touchdown Frenzy</li>
    <li>Football Frenzy</li>
    <li>Red Dog</li>
    <li>Sic Bo</li>
    <li>Battle Royale</li>
</ul>
<p>
    Note:  For general casino disputes we provide a record that contains the bet amount, running balance, game type, Round ID and time for each single casino round only.   Every casino round has a unique Round ID and members should provide it in their casino claim.   Due to technology constraints we can only provide the member with the card/number/dice details and outcomes for a limited number of casino rounds only (25 rounds maximum).  If a unique Round ID or exact time for a particular casino round is not provided by the member we cannot guarantee the funds will be reimbursed.
</p>
<p>All of the casino games provided by US Racing's gaming provider are TST certified (see TST site logo displayed on our pages).</p>
<h2>POKER</h2>
<p>
    Providing a safe and secure environment is essential to players enjoying their online poker experience.  Our Gaming Provider is able to achieve this by ensuring cards are dealt randomly and taking the necessary steps to eliminate cheating.
</p>
<h3>Integrity</h3>
<p>
    The solution chosen for Random Number Generator/Shuffling is critical to ensuring a fair game for all players in the Poker room.  The Random Number Generator ensures the even distribution of cards whereby no sequence or relationship can be discovered in any way.  The RNG solution used in the Poker room is the cryptographically-secure RNG system found in Microsoft's CryptoAPI.
</p>
<h3>Etiquette</h3>
<p>
    Any player using racist, derogatory or abusive language on the chat within the Poker room will have his/her chat privileges removed and may be banned from the Poker room.  Only the English language is permitted on the chat.  Neither shall the practices of begging, solicitation or chat-flooding be tolerated.
</p>
<h3>Bonuses</h3>
<p>
    Bonuses for Poker play shall be awarded and earned out in $10 increments within the Poker room.  Once a portion of a bonus has been earned it may be removed from the Poker room.
</p>
<h3>Disputes</h3>
<p>
    The random number generator ("RNG" will determine the outcome of the games or other promotions that utilize the "RNG"  Furthermore, in the event of a discrepancy between the result showing on the software and the gaming server, the result showing on the gaming server shall be the official and governing result of the game.
</p>
<h3>Disclaimer</h3>
<ul>
    <li>Any player found to be tampering with the poker software will be banned permanently from the Poker room.</li>
    <li>The use of Artificial Intelligence or "bots" is strictly forbidden.</li>
    <li>Any players found to be colluding or participating in unethical play will be banned permanently from the Poker room.</li>
    <li>Any funds gained by cheating will be confiscated.</li>
</ul>
<h2>PROMOTIONS</h2>
<p>
    If, as part of a promotion offered by US Racing, you receive betting cash, you can only use such amount as wagers for playing in the games offered by the company.  You are not entitled to withdraw such amounts and we reserve the right to reclaim those amounts if you do not use them to play the games during the period specified in the promotion.
</p>
<p>Only members of US Racing or its Gaming Provider can redeem promotional prizes and winnings from promotions.</p>
<p>
    If you have more than one betting account, all promotional credits and/or winnings will be null and void and they will be deducted from your account.
</p>
<p>
    If a member wagers both sides of a game using a promotional bet (to bet at the same time on the favorite and the underdog) or using bonus money, Management considers that to be fraudulent behavior.  This includes making bets of this nature in two or more of our affiliated websites.  Any money won playing both sides of the game will be deducted, together with any other promotional money deposited in the member" account.
</p>
<p>Management reserves the right to modify any promotion rules or cancel promotions at its own discretion.</p>
<h2>MULTIPLE ACCOUNTS</h2>
<p>
    Members are not permitted to open multiple accounts (either within US Racing itself or affiliated websites on the same platform) in order to circumvent the limits imposed by our wagering system.  Performing such an action may trigger an audit of the member" accounts.  If multiple accounts have been used then all bets may be voided, accounts may be inactivated and winnings may be forfeited.
</p>
<h2>ACCOUNT CLOSURES</h2>
<p>If an account is closed per Management's decision, all pending wagers will be graded as No Action.</p>
<h2>MEMBER ADDRESS</h2>
<p>
    It is your responsibility to ensure that we have your current address on file at all times.  You may only have one address on file at a time, and this address MUST match the billing address of ALL cards that have been used in the account.  Any failure on your part to do this may result in an audit on your account.
</p>
<p>
    Before making a deposit and to avoid unnecessary delays in processing your payouts, please make sure your address is correct and that it matches bank records.  In the event that we are not able to verify your account information you may be required to submit additional documentation for this purpose.  This may include the following:
</p>
<ul>
    <li>A photocopy of your picture ID (e.g. Driver's License, passport)</li>
    <li>Photocopies (front and back) of your credit cards</li>
    <li>A credit card statement or utility bill</li>
    <li>A credit card verification form</li>
    <li>Imprint of your credit card</li>
</ul>
<p>
    When requesting a withdrawal, please ensure that your current address is correct and up-to-date, also ensure we have received and verified all the required Know Your Customer documents at least 72 hours prior to making your request; we will need you to provide us with a copy of a valid form of ID and a copy of a utility bill showing your name and address. Depending on your method of withdrawal, we may need to provide these documents to our payment processor prior to processing your withdrawal.  If a stop-payment request is necessary due to an out-of-date address, it will be your responsibility to assume the stop payment fees of a minimum of $50.
</p>
<h2>MULTIPLE CREDIT CARDS</h2>
<p>
    The maximum number of credit cards allowed for use in one account is five. All cards used on an account must be in the name of the US Racing/Gaming Provider member.
</p>
<h2>LEGALITY</h2>
<p>
    All bets made on products provided by BUSR website, in the Gaming Provider's Casino or the Gaming Provider's Sportsbook, are placed over the internet which reaches virtually every country in the world. Some jurisdictions have not addressed the legality of internet gaming, some have specifically legalised internet gaming, while others may take the position that internet wagering or gaming is illegal. In practical terms, it is impossible for BUSR to determine the state of the law in every country, state and province around the world on an ongoing basis. Therefore, by clicking the "agree" button, you are acknowledging that it is responsibility of each individual to determine the law that applies in the jurisdiction in which he or she is present and that accordingly (a) you have determined what the laws are in your jurisdiction; and (b) it is legal for you to place a bet via the internet and for the Gaming Provider to receive your bet via the internet.
</p>
<p>
    The Gaming Provider has offices in South America, North America, Hong Kong, China and Central America. The Gaming Provider holds a license to conduct on-line wagering and gaming and is legally authorized to conduct online gaming operations. All bets with US Racing's Gaming Provider, BUSR, are considered to be placed and received in Antigua and Barbuda. Any matter to be adjudicated shall be determined utilizing the laws of Antigua and Barbuda.
</p>
<p>
    Should there be any discrepancy in the legality of any transactions between you and Management, or any of its affiliates, the matter shall be determined by a court of competent jurisdiction in the country of Antigua and Barbuda.
</p>
<p>
    This website does not constitute an offer, solicitation or invitation by US Racing or its Gaming Provider for the use of or subscription to betting or other services in any jurisdiction in which such activities are prohibited by law. All contractual issues between you and US Racing, US Racing's Gaming Provider, or any of its affiliates, that are disputed, shall be resolved by a court of competent jurisdiction in the country of Antigua and Barbuda. All contracts shall be interpreted in accordance with the laws of Antigua and Barbuda.
</p>
<p>
    US Racing and its Gaming Provider, BUSR, do not guarantee the accuracy of verbal or written communications. House Rules have precedence over any verbal or written communications and, in the event of any dispute, the policies and terms and conditions posted on our site are final. Furthermore, Management does not guarantee the privacy of live chat conversations.
</p>
<p>
    US Racing and the website usracing.com do not provide any gambling services or financial instruments which allow for the transactions of wagers over the internet.
</p>
<h2>Restrictions on Use of Materials</h2>
<p>
    Materials in this website are Copyrighted and all rights are reserved. Text, graphics, databases, HTML code, and other intellectual property are protected by International Copyright Laws, and may not be copied, reprinted, published, reengineered, translated, hosted, or otherwise distributed by any means without explicit permission. All of the trademarks on this site are trademarks of BUSR or of other owners used with their permission.
</p>
<h2>Database Ownership, License, and Use</h2>
<p>
    BUSR warrants, and you accept, that we own of the copyright of the Databases of Links to articles and resources available from time to time through BUSR and its contributors reserve all rights and no intellectual property rights are conferred by this agreement.
</p>
<p>
    BUSR grants you a non-exclusive, non-transferable license to use database(s) accessible to you subject to these Terms and Conditions. The database(s) may be used only for viewing information or for extracting information to the extent described below.
</p>
<p>
    You agree to use information obtained from BUSR databases only for your own private use or the internal purposes of your home or business, provided that is not the selling or broking of information, and in no event cause or permit to be published, printed, downloaded, transmitted, distributed, reengineered, or reproduced in any form any part of the databases (whether directly or in condensed, selective or tabulated form) whether for resale, republishing, redistribution, viewing, or otherwise.
</p>
<p>
    Nevertheless, you may on an occasional limited basis download or print out individual pages of information that have been individually selected, to meet a specific, identifiable need for information which is for your personal use only, or is for use in your business only internally, on a confidential basis. You may make such limited number of duplicates of any output, both in machine-readable or hard copy form, as may be reasonable for these purposes only. Nothing herein shall authorize you to create any database, directory or hard copy publication of or from the databases, whether for internal or external distribution or use.
</p>
<h2>Liability</h2>
<p>
    The materials in this site are provided as is and without warranties of any kind either express or implied. BUSR disclaims all warranties, express or implied, including, but not limited to, implied warranties of merchantability and fitness for a particular purpose. BUSR does not warrant that the functions contained in the materials will be uninterrupted or error-free, that defects will be corrected, or that this site or the server that makes it available are free of viruses or other harmful components. BUSR does not warrant or make any representations regarding the use or the results of the use of the materials in this site in terms of their correctness, accuracy, reliability, or otherwise. You (and not BUSR) assume the entire cost of all necessary servicing, repair or correction. Applicable law may not allow the exclusion of implied warranties, so the above exclusion may not apply to you.
</p>
<p>
    Under no circumstances, including, but not limited to, negligence, shall BUSR be liable for any special or consequential damages that result from the use of, or the inability to use, the materials in this site, even if BUSR or a BUSR authorized representative has been advised of the possibility of such damages. Applicable law may not allow the limitation or exclusion of liability or incidental or consequential damages, so the above limitation or exclusion may not apply to you. In no event shall BUSR total liability to you for all damages, losses, and causes of action (whether in contract, tort, including but not limited to, negligence or otherwise) exceed the amount paid by you, if any, for accessing this site.
</p>
<p>
    Facts and information at this website are believed to be accurate at the time they were placed on the website. Changes may be made at any time without prior notice. All data provided on this website is to be used for information purposes only. The information contained on this website and pages within, is not intended to provide specific legal, financial or tax advice, or any other advice, whatsoever, for any individual or company and should not be relied upon in that regard. The services described on this website are only offered in jurisdictions where they may be legally offered. Information provided in our website is not all-inclusive, and is limited to information that is made available to BUSR and such information should not be relied upon as all-inclusive or accurate.
</p>
<h2>Links and Marks</h2>
<p>
    The owner of this site is not necessarily affiliated with sites that may be linked to this site and is not responsible for their content. The linked sites are for your convenience only and you access them at your own risk. Links to other websites or references to products, services or publications other than those of BUSR and its subsidiaries and affiliates at this website, do not imply the endorsement or approval of such websites, products, services or publications by BUSR or its subsidiaries and affiliates.
</p>
<p>
    Certain names, graphics, logos, icons, designs, words, titles or phrases at this website may constitute trade names, trademarks or service marks of BUSR or of other entities. The display of trademarks on this website does not imply that a license of any kind has been granted. Photo credits have been provided when available.  If your image appears here without proper credit please contact us and we will remove the photo or give you credit for your work. Any unauthorized downloading, re-transmission, or other copying of modification of trademarks and/or the contents herein may be a violation of federal common law trademark and/or copyright laws and could subject the copier to legal action.
</p>
<h2>Confidentiality of Codes, Passwords and Information</h2>
<p>
    You agree to treat as strictly private and confidential any Subscriber Code, username, user ID, or password which you may have received from BUSR, and all information to which you have access through password-protected areas of BUSR websites and will not cause or permit any such information to be communicated, copied or otherwise divulged to any other person whatsoever.
</p>
<h2>Domains</h2>
<p>
    These Terms of Use will apply to every access to BUSR (betusracing.com) and any variant of that top level domain name. We reserve the right to issue revisions to these Terms of Use by publishing a revised version of this document on this site: that version will then apply to all use by you following the date of publication. Each access of information from BUSR will be a separate, discrete transaction based on the then prevailing terms.
</p>
<p>This Terms of Use and the license granted may not be assigned or sublet by You without BUSR written consent in advance.</p>
<p>
    These Terms of Use shall be governed by, construed and enforced in accordance with the laws of the country that BUSR shall assign, including but not limited to those of Canada, Costa Rica, Argentina, Hong Kong and Isle of Man as it is applied to agreements entered into and to be performed entirely within such jurisdictions.
</p>
<p>
    To the extent you have in any manner violated or threatened to violate BUSR and/or its affiliates' intellectual property rights, BUSR and/or its affiliates may seek injunctive or other appropriate relief in any court in the Countries of Canada, Argentina, Costa Rica, Hong Kong or Isle of Man, and you consent to exclusive jurisdiction and venue in such courts as BUSR stipulates.
</p>
<p>Any other disputes will be resolved as follows:</p>
<p>
    If a dispute arises under this agreement, we agree to first try to resolve it with the help of a mutually agreed-upon mediator in the following choice by BUSR among the following countries: Canada, Argentina, Costa Rica, Hong Kong and Isle of Man. Any costs and fees other than attorney fees associated with the mediation will be shared equally by each of us.
</p>
<p>
    If it proves impossible to arrive at a mutually satisfactory solution through mediation, we agree to submit the dispute to binding arbitration at one of the following locations under their respective rules of arbitration: Canada, Argentina, Costa Rica, Hong Kong and Isle of Man.  Judgment upon the award rendered by the arbitration may be entered in any court with jurisdiction to do so.
</p>
<p>
    If any provision of this agreement is void or unenforceable in whole or in part, the remaining provisions of this Agreement shall not be affected thereby.
</p>
<h2>Termination</h2>
<p>
    These Terms of Use agreement are effective until terminated by either party. You may terminate this agreement at any time by destroying all materials obtained from any and all BUSR and all related documentation and all copies and installations thereof, whether made under the terms of this agreement or otherwise. This agreement will terminate immediately without notice at BUSR sole discretion, should you fail to comply with any term or provision of this agreement. Upon termination, you must destroy all materials obtained from this site and any and all other BUSR site(s) and all copies thereof, whether made under the terms of this agreement or otherwise.
</p>