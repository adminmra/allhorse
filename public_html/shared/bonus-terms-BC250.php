<h2>20% Bonus for New Member Deposits and Reloads.   </h2>


<p><strong>Here's how it works: 20% Bonus added to first deposit (new players) OR 20% added to deposit of existing players.  Maximum bonus of $250.</strong></p>
<ul>
<li>This promotion is for existing members and new signups.
</li><li>This promotion cannot be combined with any other signup bonus for new players
</li><li>to qualify, you must wager at least 5 times your deposit ("five times roll over") within 30 days of your deposit for this promotion
</li>
<blockquote><strong>Example:</strong>  You deposit $300.  You must make cumulative wagers of $1,800 ("five time roll over" = $360 x 5) within 30 days of your deposit and you will then be eligible to withdraw the 20% Cash added to your account (Example, $300 x 20% = $60 Bonus Cash).</blockquote>

<li>Once you qualify, your cash bonus will be automatically deposited into your racing account.
</li><li>TVG, TwinSpires, Xpressbet and BetAmerica players are eligible for this promotion!
</li><li>Wagers must be made online.
</li><li>Wagers must be made at the BUSR website.
</li><li>Wagers must be made in the racebook only.  Wagers made in sportsbook or casino do not count towards roll over requirements.
</li><li>This promotion may be suspended by the Management without notice.
</li><li>Bonus cash must be wagered five times before it can be withdrawn from player's account. In other words, after you receive your Cash Bonus, you must make wagers equivalent to your deposit and Cash Bonus ("five time roll over") in the racebook. Afterwards, you may withdraw those funds from your racing account.

</li><li>If you have any questions about this promotion, please contact Customer Support.
</li><li>This promotion is void where prohibited. All terms and conditions will apply for qualification.
</li></ul>
