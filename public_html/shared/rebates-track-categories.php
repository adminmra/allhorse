<?php
//Add your local on the ifs to test
//Remember to add new track lists on the Accordion down below
$pageURL  = $_SERVER['SERVER_NAME'];
if ($pageURL == "betusracing.ag" || $pageURL == "www.betusracing.ag" || $pageURL == "dev.betusracing.ag"|| $pageURL == "falcon.betusracing.ag") {
    $sitename = 'BUSR';
    $accordion = true;
} elseif ($pageURL == "allhorseracing.ag" || $pageURL == "www.allhorseracing.ag" || $pageURL == "dev.allhorseracing.ag"|| $pageURL == "falcon.allhorseracing.ag") {
    $sitename = 'All Horse Racing';
    $accordion = false;
} elseif ($pageURL == "gohorsebetting.com" || $pageURL == "www.gohorsebetting.com" || $pageURL == "dev.gohorsebetting.com"|| $pageURL == "falcon.gohorsebetting.com") {
    $sitename = 'Go Horse Betting';
    $accordion = false;
} else {
    $sitename = '';
    $accordion = false;
}
?>
<style>
    #raceTrackAccordion {
        display: none;
    }
    <?php if ($accordion && !preg_match('~MSIE|Internet Explorer~i', $_SERVER['HTTP_USER_AGENT']) && !(strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false)) { ?>
    @media (max-width: 506px) {
        #racetrackTable {
            display: none;
        }

        #raceTrackAccordion {
            display: initial;
        }
    }
    <?php } ?>

    .card-body {
        padding-left: 50px;
    }

    .btn-link {
        width: 100%;
        text-align: left;
    }
</style>
<div id="racetrackTable" class="table-responsive">
<table class="table table-condensed table-striped table-bordered" border="0" cellpadding="0" cellspacing="0"
        title="Racetrack Rebate Categories" subject="The category in which the track lies in the rebate schedule.">
        <caption><?php echo $sitename ?> Track Categories</caption>
        <tbody>
            <tr>
                <td>Category A</td>
                <td>Category B</td>
                <td>Category C</td>
                <td>Category D</td>
                <td>Category E</td>
            </tr>

  <tr>
    <td>Aintree </td>
    <td>Canterbury Park </td>
    <td>Albuquerque </td>
    <td>Arawa Park </td>
    <td>Aby </td>
  </tr>
  <tr>
    <td>Aqueduct (T) </td>
    <td>Charles Town </td>
    <td>Arapahoe Park </td>
    <td>Australia B </td>
    <td>Amal </td>
  </tr>
  <tr>
    <td>Arlington Park </td>
    <td>Delaware Park </td>
    <td>Belterra </td>
    <td>Australia C </td>
    <td>Arjang </td>
  </tr>
  <tr>
    <td>Ascot Racecourse </td>
    <td>Delta Downs </td>
    <td>Colonial Downs </td>
    <td>Australia D </td>
    <td>Arvika </td>
  </tr>
  <tr>
    <td>Australia A </td>
    <td>Emerald Downs </td>
    <td>Columbus Park </td>
    <td>Australia Harness 1 </td>
    <td>Assiniboia </td>
  </tr>
  <tr>
    <td>Auteuil Racecourse </td>
    <td>Fair Grounds </td>
    <td>Dax </td>
    <td>Australia Harness 2 </td>
    <td>Axevalla </td>
  </tr>
  <tr>
    <td>Ayr </td>
    <td>Finger Lakes </td>
    <td>Ellis Park </td>
    <td>Balmoral Park </td>
    <td>Bergsaker </td>
  </tr>
  <tr>
    <td>Ballinrobe </td>
    <td>Golden Gate Fields </td>
    <td>FanDuel Racing </td>
    <td>Cal Expo </td>
    <td>Bluffs </td>
  </tr>
  <tr>
    <td>Bangor-on-Dee </td>
    <td>Hoosier Park </td>
    <td>Fontainebleau </td>
    <td>Dover Downs </td>
    <td>Boden </td>
  </tr>
  <tr>
    <td>Bath </td>
    <td>Indiana Grand </td>
    <td>Fukushima </td>
    <td>Evangeline Downs </td>
    <td>Bollnas </td>
  </tr>
  <tr>
    <td>BC Juvenile /Classic DD </td>
    <td>Laurel Park </td>
    <td>Funabashi </td>
    <td>Ferndale </td>
    <td>Bro Park </td>
  </tr>
  <tr>
    <td>BC Juvenile Fillies/Distaff DD </td>
    <td>Lone Star Park </td>
    <td>Hanshin </td>
    <td>Flamboro Downs </td>
    <td>Buffalo Raceway </td>
  </tr>
  <tr>
    <td>BC Juvenile Turf/Turf DD </td>
    <td>Los Alamitos </td>
    <td>Hastings Park </td>
    <td>Fort Erie </td>
    <td>Compiegne </td>
  </tr>
  <tr>
    <td>Bellewstown </td>
    <td>Los Alamitos Race Course </td>
    <td>Hawthorne </td>
    <td>Freehold Raceway </td>
    <td>Dannero </td>
  </tr>
  <tr>
    <td>Belmont Gold/Stakes Double </td>
    <td>Louisiana Downs </td>
    <td>Hialeah Park </td>
    <td>Fresno </td>
    <td>Daytona Beach Evening </td>
  </tr>
  <tr>
    <td>Belmont Park </td>
    <td>Oaklawn Park </td>
    <td>Kentucky Downs </td>
    <td>Harrah's Philadelphia </td>
    <td>Daytona Beach Matinee </td>
  </tr>
  <tr>
    <td>Beverley </td>
    <td>Penn National </td>
    <td>Kokura </td>
    <td>Harrington Raceway </td>
    <td>Derby Lane Evening </td>
  </tr>
  <tr>
    <td>Brighton </td>
    <td>Prairie Meadows </td>
    <td>Kyoto </td>
    <td>Hawera </td>
    <td>Derby Lane Matinee </td>
  </tr>
  <tr>
    <td>Carlisle </td>
    <td>Presque Isle Downs </td>
    <td>Mahoning Valley </td>
    <td>Matamata </td>
    <td>Eskilstuna </td>
  </tr>
  <tr>
    <td>Cartmel </td>
    <td>Sam Houston Race Park </td>
    <td>Meadowlands </td>
    <td>Maywood Park </td>
    <td>Farjestad </td>
  </tr>
  <tr>
    <td>Catterick </td>
    <td>Sunland Park </td>
    <td>Mont de Marsan </td>
    <td>Otaki </td>
    <td>Fonner Park </td>
  </tr>
  <tr>
    <td>Chantilly Racecourse </td>
    <td>Turfway Park </td>
    <td>Mountaineer Park </td>
    <td>Phar Lap </td>
    <td>Gavle </td>
  </tr>
  <tr>
    <td>Chelmsford City </td>
    <td></td>
    <td>Nakayama </td>
    <td>Plainridge </td>
    <td>Hagmyren </td>
  </tr>
  <tr>
    <td>Cheltenham </td>
    <td></td>
    <td>Niigata </td>
    <td>Pleasanton </td>
    <td>Halmstad </td>
  </tr>
  <tr>
    <td>Chepstow </td>
    <td></td>
    <td>Oi </td>
    <td>Pocono Downs </td>
    <td>Hawthorne Harness </td>
  </tr>
  <tr>
    <td>Chester </td>
    <td></td>
    <td>Remington Park </td>
    <td>Pocono Downs Early </td>
    <td>Hazel Raceway </td>
  </tr>
  <tr>
    <td>Churchill Downs </td>
    <td></td>
    <td>Sapporo </td>
    <td>Pompano Park </td>
    <td>Hoosier Park Harness </td>
  </tr>
  <tr>
    <td>Clonmel </td>
    <td></td>
    <td>Scottsville </td>
    <td>Portland Meadows </td>
    <td>Jacksonville EVE </td>
  </tr>
  <tr>
    <td>Cork Racecourse </td>
    <td></td>
    <td>SunRay Park </td>
    <td>Pukekohe </td>
    <td>Jagersro </td>
  </tr>
  <tr>
    <td>Curragh </td>
    <td></td>
    <td>Turf Paradise </td>
    <td>Retama Park </td>
    <td>Jagersro Galopp </td>
  </tr>
  <tr>
    <td>Deauville-La Touques </td>
    <td></td>
    <td></td>
    <td>Riccarton </td>
    <td>Kalmar </td>
  </tr>
  <tr>
    <td>Del Mar </td>
    <td></td>
    <td></td>
    <td>Sacramento </td>
    <td>Karlshamn </td>
  </tr>
  <tr>
    <td>Distaff Classic Daily Double </td>
    <td></td>
    <td></td>
    <td>Santa Rosa </td>
    <td>La Teste </td>
  </tr>
  <tr>
    <td>Doncaster Racecourse </td>
    <td></td>
    <td></td>
    <td>Sha Tin </td>
    <td>Lindesberg </td>
  </tr>
  <tr>
    <td>Down Royal </td>
    <td></td>
    <td></td>
    <td>Solano </td>
    <td>Lycksele </td>
  </tr>
  <tr>
    <td>Downpatrick </td>
    <td></td>
    <td></td>
    <td>Stockton </td>
    <td>Mantorp </td>
  </tr>
  <tr>
    <td>Dundalk </td>
    <td></td>
    <td></td>
    <td>Suffolk Downs </td>
    <td>Meadowlands Harness </td>
  </tr>
  <tr>
    <td>Durbanville </td>
    <td></td>
    <td></td>
    <td>Tauranga </td>
    <td>Meadows Harness </td>
  </tr>
  <tr>
    <td>Epsom Downs </td>
    <td></td>
    <td></td>
    <td>Te Aroha </td>
    <td>Monticello Raceway </td>
  </tr>
  <tr>
    <td>Exeter </td>
    <td></td>
    <td></td>
    <td>Te Rapa </td>
    <td>Naples Fort Myers Evening </td>
  </tr>
  <tr>
    <td>Fairview </td>
    <td></td>
    <td></td>
    <td>The Red Mile </td>
    <td>Naples Fort Myers Matinee </td>
  </tr>
  <tr>
    <td>Fairyhouse Racecourse </td>
    <td></td>
    <td></td>
    <td>Thistledown </td>
    <td>Northfield Park </td>
  </tr>
  <tr>
    <td>Fakenham </td>
    <td></td>
    <td></td>
    <td>Timonium </td>
    <td>Orange Park Evening </td>
  </tr>
  <tr>
    <td>Ffos Las </td>
    <td></td>
    <td></td>
    <td>Wanganui </td>
    <td>Orange Park Matinee </td>
  </tr>
  <tr>
    <td>Flamingo </td>
    <td></td>
    <td></td>
    <td>Western Fair Raceway </td>
    <td>Orebro </td>
  </tr>
  <tr>
    <td>Folkestone </td>
    <td></td>
    <td></td>
    <td>Will Rogers Downs </td>
    <td>Ostersund </td>
  </tr>
  <tr>
    <td>Fontwell </td>
    <td></td>
    <td></td>
    <td>Woodbine Mohawk Park </td>
    <td>Oviken </td>
  </tr>
  <tr>
    <td>Galway </td>
    <td></td>
    <td></td>
    <td>Woodville </td>
    <td>Palm Beach Evening </td>
  </tr>
  <tr>
    <td>Goodwood </td>
    <td></td>
    <td></td>
    <td>Yavapai Downs </td>
    <td>Palm Beach Matinee </td>
  </tr>
  <tr>
    <td>Gowran </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Rattvik </td>
  </tr>
  <tr>
    <td>Greyville </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Romme </td>
  </tr>
  <tr>
    <td>Gulfstream Park </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Ruidoso Downs </td>
  </tr>
  <tr>
    <td>Gulfstream Park West </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Saratoga Harness </td>
  </tr>
  <tr>
    <td>Hamilton </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Saratoga Harness Early </td>
  </tr>
  <tr>
    <td>Happy Valley Racecourse </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Scioto Downs </td>
  </tr>
  <tr>
    <td>Haydock </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Skelleftea </td>
  </tr>
  <tr>
    <td>Hereford </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Solanget </td>
  </tr>
  <tr>
    <td>Hexham </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Solvalla </td>
  </tr>
  <tr>
    <td>Huntingdon </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Southland Evening </td>
  </tr>
  <tr>
    <td>Kawasaki </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Southland Matinee </td>
  </tr>
  <tr>
    <td>Keeneland </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Southland Twilight </td>
  </tr>
  <tr>
    <td>Kelso </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Tarbes </td>
  </tr>
  <tr>
    <td>Kempton </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Tingsryd </td>
  </tr>
  <tr>
    <td>Kenilworth </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Tri-State Evening </td>
  </tr>
  <tr>
    <td>Kilbeggan </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Tri-State Matinee </td>
  </tr>
  <tr>
    <td>Killarney </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Umaker </td>
  </tr>
  <tr>
    <td>King Abdulaziz Racetrack </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Vaggeryd </td>
  </tr>
  <tr>
    <td>Laytown </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Visby </td>
  </tr>
  <tr>
    <td>Leicester </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Wheeling Downs Evening </td>
  </tr>
  <tr>
    <td>Leopardstown </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Wheeling Downs Matinee </td>
  </tr>
  <tr>
    <td>Limerick </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Woodbine Harness </td>
  </tr>
  <tr>
    <td>Lingfield </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Yonkers Raceway Harness </td>
  </tr>
  <tr>
    <td>Listowel </td>
    <td></td>
    <td></td>
    <td></td>
    <td>Zia Park </td>
  </tr>
  <tr>
    <td>Longchamp </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Ludlow </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Maisons-Laffitte Racecourse </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Market Rasen </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Marseille-Vivaux </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Meydan </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Monmouth Park </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Musselburgh </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Naas Racecourse </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Navan </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>New York/Metropolitan Double </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Newbury Racecourse </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Newcastle </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Newmarket </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Newton Abbot </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Nottingham </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Oak Tree at Pleasanton </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Oaks/Derby Daily Double </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Oaks/Old Forester/Derby Pick 3 </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Parx Racing </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Perth </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Pimlico </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Plumpton </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Pontefract </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Punchestown </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Redcar </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Ripon </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Roscommon </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Saint-Cloud </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Salisbury </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Sandown </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Santa Anita Park </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Saratoga (T) </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Sedgefield </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Sligo </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Southwell </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Stratford </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Tampa Bay Downs </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Taunton </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Thirsk </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Thurles </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Tipperary </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Tokyo </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Towcester </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Tramore </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Tucson </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Turffontein </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Uttoxeter </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Vaal </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Warwick </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Wetherby </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Wexford </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Wincanton </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Windsor </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Wolverhampton </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Woodbine </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Worcester </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Yarmouth </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>York </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>        </tbody>
    </table>

</div>
<!-- table-responsive -->

<div class="accordion" id="raceTrackAccordion">
    <div class="card">
        <div class="card-header" id="categoryA">
            <h2 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseA" aria-expanded="true"
                    aria-controls="collapseA">Category A</button>
            </h2>
        </div>

        <div id="collapseA" class="collapse" aria-labelledby="categoryA" data-parent="#raceTrackAccordion">
            <div class="card-body">
                <ul>
<li>Aintree</li>
<li>Aqueduct (T)</li>
<li>Arlington Park</li>
<li>Ascot Racecourse</li>
<li>Australia A</li>
<li>Auteuil Racecourse</li>
<li>Ayr</li>
<li>Ballinrobe</li>
<li>Bangor-on-Dee</li>
<li>Bath</li>
<li>BC Juvenile /Classic DD</li>
<li>BC Juvenile Fillies/Distaff DD</li>
<li>BC Juvenile Turf/Turf DD</li>
<li>Bellewstown</li>
<li>Belmont Gold/Stakes Double</li>
<li>Belmont Park</li>
<li>Beverley</li>
<li>Brighton</li>
<li>Carlisle</li>
<li>Cartmel</li>
<li>Catterick</li>
<li>Chantilly Racecourse</li>
<li>Chelmsford City</li>
<li>Cheltenham</li>
<li>Chepstow</li>
<li>Chester</li>
<li>Churchill Downs</li>
<li>Clonmel</li>
<li>Cork Racecourse</li>
<li>Curragh</li>
<li>Deauville-La Touques</li>
<li>Del Mar</li>
<li>Distaff Classic Daily Double</li>
<li>Doncaster Racecourse</li>
<li>Down Royal</li>
<li>Downpatrick</li>
<li>Dundalk</li>
<li>Durbanville</li>
<li>Epsom Downs</li>
<li>Exeter</li>
<li>Fairview</li>
<li>Fairyhouse Racecourse</li>
<li>Fakenham</li>
<li>Ffos Las</li>
<li>Flamingo</li>
<li>Folkestone</li>
<li>Fontwell</li>
<li>Galway</li>
<li>Goodwood</li>
<li>Gowran</li>
<li>Greyville</li>
<li>Gulfstream Park</li>
<li>Gulfstream Park West</li>
<li>Hamilton</li>
<li>Happy Valley Racecourse</li>
<li>Haydock</li>
<li>Hereford</li>
<li>Hexham</li>
<li>Huntingdon</li>
<li>Kawasaki</li>
<li>Keeneland</li>
<li>Kelso</li>
<li>Kempton</li>
<li>Kenilworth</li>
<li>Kilbeggan</li>
<li>Killarney</li>
<li>King Abdulaziz Racetrack</li>
<li>Laytown</li>
<li>Leicester</li>
<li>Leopardstown</li>
<li>Limerick</li>
<li>Lingfield</li>
<li>Listowel</li>
<li>Longchamp</li>
<li>Ludlow</li>
<li>Maisons-Laffitte Racecourse</li>
<li>Market Rasen</li>
<li>Marseille-Vivaux</li>
<li>Meydan</li>
<li>Monmouth Park</li>
<li>Musselburgh</li>
<li>Naas Racecourse</li>
<li>Navan</li>
<li>New York/Metropolitan Double</li>
<li>Newbury Racecourse</li>
<li>Newcastle</li>
<li>Newmarket</li>
<li>Newton Abbot</li>
<li>Nottingham</li>
<li>Oak Tree at Pleasanton</li>
<li>Oaks/Derby Daily Double</li>
<li>Oaks/Old Forester/Derby Pick 3</li>
<li>Parx Racing</li>
<li>Perth</li>
<li>Pimlico</li>
<li>Plumpton</li>
<li>Pontefract</li>
<li>Punchestown</li>
<li>Redcar</li>
<li>Ripon</li>
<li>Roscommon</li>
<li>Saint-Cloud</li>
<li>Salisbury</li>
<li>Sandown</li>
<li>Santa Anita Park</li>
<li>Saratoga (T)</li>
<li>Sedgefield</li>
<li>Sligo</li>
<li>Southwell</li>
<li>Stratford</li>
<li>Tampa Bay Downs</li>
<li>Taunton</li>
<li>Thirsk</li>
<li>Thurles</li>
<li>Tipperary</li>
<li>Tokyo</li>
<li>Towcester</li>
<li>Tramore</li>
<li>Tucson</li>
<li>Turffontein</li>
<li>Uttoxeter</li>
<li>Vaal</li>
<li>Warwick</li>
<li>Wetherby</li>
<li>Wexford</li>
<li>Wincanton</li>
<li>Windsor</li>
<li>Wolverhampton</li>
<li>Woodbine</li>
<li>Worcester</li>
<li>Yarmouth</li>
<li>York</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="categoryB">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseB"
                    aria-expanded="false" aria-controls="collapseB">
                    Category B
                </button>
            </h2>
        </div>
        <div id="collapseB" class="collapse" aria-labelledby="categoryB" data-parent="#raceTrackAccordion">
            <div class="card-body">
                <ul>
<li>Canterbury Park</li>
<li>Charles Town</li>
<li>Delaware Park</li>
<li>Delta Downs</li>
<li>Emerald Downs</li>
<li>Fair Grounds</li>
<li>Finger Lakes</li>
<li>Golden Gate Fields</li>
<li>Hoosier Park</li>
<li>Indiana Grand</li>
<li>Laurel Park</li>
<li>Lone Star Park</li>
<li>Los Alamitos</li>
<li>Los Alamitos Race Course</li>
<li>Louisiana Downs</li>
<li>Oaklawn Park</li>
<li>Penn National</li>
<li>Prairie Meadows</li>
<li>Presque Isle Downs</li>
<li>Sam Houston Race Park</li>
<li>Sunland Park</li>
<li>Turfway Park</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="categoryC">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseC"
                    aria-expanded="false" aria-controls="collapseC">
                    Category C
                </button>
            </h2>
        </div>
        <div id="collapseC" class="collapse" aria-labelledby="categoryC" data-parent="#raceTrackAccordion">
            <div class="card-body">
                <ul>
<li>Albuquerque</li>
<li>Arapahoe Park</li>
<li>Belterra</li>
<li>Colonial Downs</li>
<li>Columbus Park</li>
<li>Dax</li>
<li>Ellis Park</li>
<li>FanDuel Racing</li>
<li>Fontainebleau</li>
<li>Fukushima</li>
<li>Funabashi</li>
<li>Hanshin</li>
<li>Hastings Park</li>
<li>Hawthorne</li>
<li>Hialeah Park</li>
<li>Kentucky Downs</li>
<li>Kokura</li>
<li>Kyoto</li>
<li>Mahoning Valley</li>
<li>Meadowlands</li>
<li>Mont de Marsan</li>
<li>Mountaineer Park</li>
<li>Nakayama</li>
<li>Niigata</li>
<li>Oi</li>
<li>Remington Park</li>
<li>Sapporo</li>
<li>Scottsville</li>
<li>SunRay Park</li>
<li>Turf Paradise</li>

                </ul>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="categoryD">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseD"
                    aria-expanded="false" aria-controls="collapseD">
                    Category D
                </button>
            </h2>
        </div>
        <div id="collapseD" class="collapse" aria-labelledby="categoryD" data-parent="#raceTrackAccordion">
            <div class="card-body">
                <ul>
<li>Arawa Park</li><li>Australia B</li><li>Australia C</li><li>Australia D</li><li>Australia Harness 1</li><li>Australia Harness 2</li><li>Balmoral Park</li><li>Cal Expo</li><li>Dover Downs</li><li>Evangeline Downs</li><li>Ferndale</li><li>Flamboro Downs</li><li>Fort Erie</li><li>Freehold Raceway</li><li>Fresno</li><li>Harrah's Philadelphia</li><li>Harrington Raceway</li><li>Hawera</li><li>Matamata</li><li>Maywood Park</li><li>Otaki</li><li>Phar Lap</li><li>Plainridge</li><li>Pleasanton</li><li>Pocono Downs</li><li>Pocono Downs Early</li><li>Pompano Park</li><li>Portland Meadows</li><li>Pukekohe</li><li>Retama Park</li><li>Riccarton</li><li>Sacramento</li><li>Santa Rosa</li><li>Sha Tin</li><li>Solano</li><li>Stockton</li><li>Suffolk Downs</li><li>Tauranga</li><li>Te Aroha</li><li>Te Rapa</li><li>The Red Mile</li><li>Thistledown</li><li>Timonium</li><li>Wanganui</li><li>Western Fair Raceway</li><li>Will Rogers Downs</li><li>Woodbine Mohawk Park</li><li>Woodville</li><li>Yavapai Downs</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="categoryE">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseE"
                    aria-expanded="false" aria-controls="collapseE">
                    Category E
                </button>
            </h2>
        </div>
        <div id="collapseE" class="collapse" aria-labelledby="categoryE" data-parent="#raceTrackAccordion">
            <div class="card-body">
                <ul>
<li>Aby</li>
<li>Amal</li>
<li>Arjang</li>
<li>Arvika</li>
<li>Assiniboia</li>
<li>Axevalla</li>
<li>Bergsaker</li>
<li>Bluffs</li>
<li>Boden</li>
<li>Bollnas</li>
<li>Bro Park</li>
<li>Buffalo Raceway</li>
<li>Compiegne</li>
<li>Dannero</li>
<li>Daytona Beach Evening</li>
<li>Daytona Beach Matinee</li>
<li>Derby Lane Evening</li>
<li>Derby Lane Matinee</li>
<li>Eskilstuna</li>
<li>Farjestad</li>
<li>Fonner Park</li>
<li>Gavle</li>
<li>Hagmyren</li>
<li>Halmstad</li>
<li>Hawthorne Harness</li>
<li>Hazel Raceway</li>
<li>Hoosier Park Harness</li>
<li>Jacksonville EVE</li>
<li>Jagersro</li>
<li>Jagersro Galopp</li>
<li>Kalmar</li>
<li>Karlshamn</li>
<li>La Teste</li>
<li>Lindesberg</li>
<li>Lycksele</li>
<li>Mantorp</li>
<li>Meadowlands Harness</li>
<li>Meadows Harness</li>
<li>Monticello Raceway</li>
<li>Naples Fort Myers Evening</li>
<li>Naples Fort Myers Matinee</li>
<li>Northfield Park</li>
<li>Orange Park Evening</li>
<li>Orange Park Matinee</li>
<li>Orebro</li>
<li>Ostersund</li>
<li>Oviken</li>
<li>Palm Beach Evening</li>
<li>Palm Beach Matinee</li>
<li>Rattvik</li>
<li>Romme</li>
<li>Ruidoso Downs</li>
<li>Saratoga Harness</li>
<li>Saratoga Harness Early</li>
<li>Scioto Downs</li>
<li>Skelleftea</li>
<li>Solanget</li>
<li>Solvalla</li>
<li>Southland Evening</li>
<li>Southland Matinee</li>
<li>Southland Twilight</li>
<li>Tarbes</li>
<li>Tingsryd</li>
<li>Tri-State Evening</li>
<li>Tri-State Matinee</li>
<li>Umaker</li>
<li>Vaggeryd</li>
<li>Visby</li>
<li>Wheeling Downs Evening</li>
<li>Wheeling Downs Matinee</li>
<li>Woodbine Harness</li>
<li>Yonkers Raceway Harness</li>
<li>Zia Park</li>                </ul>
            </div>
        </div>
    </div>
</div>