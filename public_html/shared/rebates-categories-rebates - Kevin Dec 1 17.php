<div class="table-responsive">
                  <table class="table table-condensed table-striped table-bordered"  border="0" cellpadding="0" cellspacing="0" title="Daily Horse Racing Rebates" summary="The rebate payouts per track that will be paid to you daily.">
                    <caption>BUSR Daily Rebates</caption>
                    <tbody>
                      <tr>
                        <th><strong>Track Type</strong></th>			<th><strong>Straight Wagers </strong></th><th><strong>Exotic Wagers</strong></th>
                      </tr>
                      <tr>
                        <td><strong>Category A</strong></td>		<td>3%</td> 	<td>8%</td>
                      </tr>
                      <tr>
                        <td><strong>Category B</strong></td>		<td>3%</td> 	<td>5%</td>
                      </tr>
                      <tr>
                        <td><strong>Category C</strong></td>		<td>3%</td> 	<td></td>
                      </tr>
                      <tr>
                        <td><strong>Category D</strong></td>	<td></td>			<td></td>
                      </tr>
                      <tr>
                        <td><strong>Category E</strong></td> 	<td></td>			<td></td>
                      </tr>
                    </tbody>
                  </table>
                </div>