<?php
$pageURL  = $_SERVER['SERVER_NAME'];
if ($pageURL == "betusracing.ag" || $pageURL == "www.betusracing.ag" || $pageURL == "dev.betusracing.ag"|| $pageURL == "falcon.betusracing.ag") {
    $sitename = 'BUSR';
} elseif ($pageURL == "allhorseracing.ag" || $pageURL == "www.allhorseracing.ag" || $pageURL == "dev.allhorseracing.ag"|| $pageURL == "falcon.allhorseracing.ag") {
    $sitename = 'All Horse Racing';
} elseif ($pageURL == "gohorsebetting.com" || $pageURL == "www.gohorsebetting.com" || $pageURL == "dev.gohorsebetting.com"|| $pageURL == "falcon.gohorsebetting.com") {
    $sitename = 'Go Horse Betting';
} else {
    $sitename = '';
}
?>
<div id="racetrackTable" class="table-responsive">
<table class="table table-condensed table-striped table-bordered" border="0" cellpadding="0" cellspacing="0"
        title="Racetrack Rebate Categories" subject="The category in which the track lies in the rebate schedule.">
        <caption><?php echo $sitename ?> Track Categories</caption>
        <tbody>
            <tr>
                <td>Category A</td>
                <td>Category B</td>
                <td>Category C</td>
                <td>Category D</td>
                <td>Category E</td>
            </tr>

            <tr>
    <td>Aintree</td>
    <td>Canterbury Park</td>
    <td>Albuquerque</td>
    <td>Arawa Park</td>
    <td>Aby</td>
</tr>
<tr>
    <td>Aqueduct (T)</td>
    <td>Charles Town</td>
    <td>Arapahoe Park</td>
    <td>Australia B</td>
    <td>Amal</td>
</tr>
<tr>
    <td>Arlington Park</td>
    <td>Delaware Park</td>
    <td>Belterra</td>
    <td>Australia C</td>
    <td>Arjang</td>
</tr>
<tr>
    <td>Ascot Racecourse</td>
    <td>Delta Downs</td>
    <td>Colonial Downs</td>
    <td>Australia D</td>
    <td>Arvika</td>
</tr>
<tr>
    <td>Australia A</td>
    <td>Emerald Downs</td>
    <td>Columbus Park</td>
    <td>Australia Harness 1</td>
    <td>Assiniboia</td>
</tr>
<tr>
    <td>Auteuil Racecourse</td>
    <td>Fair Grounds</td>
    <td>Dax</td>
    <td>Australia Harness 2</td>
    <td>Axevalla</td>
</tr>
<tr>
    <td>Ayr</td>
    <td>Finger Lakes</td>
    <td>Ellis Park</td>
    <td>Cal Expo</td>
    <td>Bergsaker</td>
</tr>
<tr>
    <td>Ballinrobe</td>
    <td>Golden Gate Fields</td>
    <td>FanDuel Racing</td>
    <td>Dover Downs</td>
    <td>Boden</td>
</tr>
<tr>
    <td>Bangor-on-Dee</td>
    <td>Hoosier Park</td>
    <td>Fontainebleau</td>
    <td>Evangeline Downs</td>
    <td>Bollnas</td>
</tr>
<tr>
    <td>Bath</td>
    <td>Indiana Grand</td>
    <td>Fukushima</td>
    <td>Ferndale</td>
    <td>Bro Park</td>
</tr>
<tr>
    <td>BC Juvenile /Classic DD</td>
    <td>Laurel Park</td>
    <td>Funabashi</td>
    <td>Flamboro Downs</td>
    <td>Buffalo Raceway</td>
</tr>
<tr>
    <td>BC Juvenile Fillies/Distaff DD</td>
    <td>Lone Star Park</td>
    <td>Hanshin</td>
    <td>Fort Erie</td>
    <td>Compiegne</td>
</tr>
<tr>
    <td>BC Juvenile Turf/Turf DD</td>
    <td>Los Alamitos</td>
    <td>Hastings Park</td>
    <td>Freehold Raceway</td>
    <td>Dannero</td>
</tr>
<tr>
    <td>Bellewstown</td>
    <td>Los Alamitos Race Course</td>
    <td>Hawthorne</td>
    <td>Fresno</td>
    <td>Daytona Beach Evening</td>
</tr>
<tr>
    <td>Belmont Gold/Stakes Double</td>
    <td>Louisiana Downs</td>
    <td>Kentucky Downs</td>
    <td>Harrah's Philadelphia</td>
    <td>Daytona Beach Matinee</td>
</tr>
<tr>
    <td>Belmont Park</td>
    <td>Oaklawn Park</td>
    <td>Kokura</td>
    <td>Harrington Raceway</td>
    <td>Eskilstuna</td>
</tr>
<tr>
    <td>Beverley</td>
    <td>Penn National</td>
    <td>Kyoto</td>
    <td>Hawera</td>
    <td>Farjestad</td>
</tr>
<tr>
    <td>Brighton</td>
    <td>Prairie Meadows</td>
    <td>Mahoning Valley</td>
    <td>Matamata</td>
    <td>Fonner Park</td>
</tr>
<tr>
    <td>Carlisle</td>
    <td>Presque Isle Downs</td>
    <td>Meadowlands</td>
    <td>Otaki</td>
    <td>Gavle</td>
</tr>
<tr>
    <td>Cartmel</td>
    <td>Sam Houston Race Park</td>
    <td>Mont de Marsan</td>
    <td>Phar Lap</td>
    <td>Hagmyren</td>
</tr>
<tr>
    <td>Catterick</td>
    <td>Sunland Park</td>
    <td>Mountaineer Park</td>
    <td>Plainridge</td>
    <td>Halmstad</td>
</tr>
<tr>
    <td>Chantilly Racecourse</td>
    <td>Turfway Park</td>
    <td>Nakayama</td>
    <td>Pleasanton</td>
    <td>Hawthorne Harness</td>
</tr>
<tr>
    <td>Chelmsford City</td>
    <td></td>
    <td>Niigata</td>
    <td>Pocono Downs</td>
    <td>Hoosier Park Harness</td>
</tr>
<tr>
    <td>Cheltenham</td>
    <td></td>
    <td>Oi</td>
    <td>Pompano Park</td>
    <td>Jagersro</td>
</tr>
<tr>
    <td>Chepstow</td>
    <td></td>
    <td>Remington Park</td>
    <td>Pukekohe</td>
    <td>Jagersro Galopp</td>
</tr>
<tr>
    <td>Chester</td>
    <td></td>
    <td>Sapporo</td>
    <td>Retama Park</td>
    <td>Kalmar</td>
</tr>
<tr>
    <td>Churchill Downs</td>
    <td></td>
    <td>Scottsville</td>
    <td>Riccarton</td>
    <td>Karlshamn</td>
</tr>
<tr>
    <td>Clonmel</td>
    <td></td>
    <td>SunRay Park</td>
    <td>Sacramento</td>
    <td>La Teste</td>
</tr>
<tr>
    <td>Cork Racecourse</td>
    <td></td>
    <td>Turf Paradise</td>
    <td>Santa Rosa</td>
    <td>Lindesberg</td>
</tr>
<tr>
    <td>Curragh</td>
    <td></td>
    <td></td>
    <td>Sha Tin</td>
    <td>Lycksele</td>
</tr>
<tr>
    <td>Deauville-La Touques</td>
    <td></td>
    <td></td>
    <td>Tauranga</td>
    <td>Mantorp</td>
</tr>
<tr>
    <td>Del Mar</td>
    <td></td>
    <td></td>
    <td>Te Aroha</td>
    <td>Meadowlands Harness</td>
</tr>
<tr>
    <td>Distaff Classic Daily Double</td>
    <td></td>
    <td></td>
    <td>Te Rapa</td>
    <td>Meadows Harness</td>
</tr>
<tr>
    <td>Doncaster Racecourse</td>
    <td></td>
    <td></td>
    <td>The Red Mile</td>
    <td>Monticello Raceway</td>
</tr>
<tr>
    <td>Down Royal</td>
    <td></td>
    <td></td>
    <td>Thistledown</td>
    <td>Northfield Park</td>
</tr>
<tr>
    <td>Downpatrick</td>
    <td></td>
    <td></td>
    <td>Timonium</td>
    <td>Orebro</td>
</tr>
<tr>
    <td>Dundalk</td>
    <td></td>
    <td></td>
    <td>Wanganui</td>
    <td>Ostersund</td>
</tr>
<tr>
    <td>Durbanville</td>
    <td></td>
    <td></td>
    <td>Western Fair Raceway</td>
    <td>Oviken</td>
</tr>
<tr>
    <td>Epsom Downs</td>
    <td></td>
    <td></td>
    <td>Will Rogers Downs</td>
    <td>Rattvik</td>
</tr>
<tr>
    <td>Exeter</td>
    <td></td>
    <td></td>
    <td>Woodbine Mohawk Park</td>
    <td>Romme</td>
</tr>
<tr>
    <td>Fairview</td>
    <td></td>
    <td></td>
    <td>Woodville</td>
    <td>Ruidoso Downs</td>
</tr>
<tr>
    <td>Fairyhouse Racecourse</td>
    <td></td>
    <td></td>
    <td>Arizona Downs</td>
    <td>Saratoga Harness</td>
</tr>
<tr>
    <td>Fakenham</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Saratoga Harness Early</td>
</tr>
<tr>
    <td>Ffos Las</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Scioto Downs</td>
</tr>
<tr>
    <td>Fontwell</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Skelleftea</td>
</tr>
<tr>
    <td>Galway</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Solanget</td>
</tr>
<tr>
    <td>Goodwood</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Solvalla</td>
</tr>
<tr>
    <td>Gowran</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Southland Evening</td>
</tr>
<tr>
    <td>Greyville</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Southland Matinee</td>
</tr>
<tr>
    <td>Gulfstream Park</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Southland Twilight</td>
</tr>
<tr>
    <td>Gulfstream Park West</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Tarbes</td>
</tr>
<tr>
    <td>Hamilton</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Tingsryd</td>
</tr>
<tr>
    <td>Happy Valley Racecourse</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Tri-State Evening</td>
</tr>
<tr>
    <td>Haydock</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Tri-State Matinee</td>
</tr>
<tr>
    <td>Hereford</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Umaker</td>
</tr>
<tr>
    <td>Hexham</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Vaggeryd</td>
</tr>
<tr>
    <td>Huntingdon</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Visby</td>
</tr>
<tr>
    <td>Kawasaki</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Wheeling Downs Evening</td>
</tr>
<tr>
    <td>Keeneland</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Wheeling Downs Matinee</td>
</tr>
<tr>
    <td>Kelso</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Yonkers Raceway Harness</td>
</tr>
<tr>
    <td>Kempton</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Zia Park</td>
</tr>
<tr>
    <td>Kenilworth</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Kilbeggan</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Killarney</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>King Abdulaziz Racetrack</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Laytown</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Leicester</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Leopardstown</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Limerick</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Lingfield</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Listowel</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Longchamp</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Ludlow</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Market Rasen</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Meydan</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Monmouth Park</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Musselburgh</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Naas Racecourse</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Navan</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>New York/Metropolitan Double</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Newbury Racecourse</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Newcastle</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Newmarket</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Newton Abbot</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Nottingham</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Oak Tree at Pleasanton</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Oaks/Derby Daily Double</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Oaks/Old Forester/Derby Pick 3</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Parx Racing</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Perth</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Pimlico</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Plumpton</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Pontefract</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Punchestown</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Redcar</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Ripon</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Roscommon</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Saint-Cloud</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Salisbury</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Sandown</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Santa Anita Park</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Sedgefield</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Sligo</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Southwell</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Stratford</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Tampa Bay Downs</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Taunton</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Thirsk</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Thurles</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Tipperary</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Tokyo</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Tramore</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Turffontein</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Uttoxeter</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Vaal</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Warwick</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Wetherby</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Wexford</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Wincanton</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Windsor</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Wolverhampton</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Woodbine</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Worcester</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>Yarmouth</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>York</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>

        </tbody>
    </table>
</div>
<!-- table-responsive -->