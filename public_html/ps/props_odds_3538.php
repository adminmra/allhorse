	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Preakness Stakes Betting Odds:  Props"
	  }
	</script>
		{/literal}
	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Preakness Stakes Betting: Props">
			<caption>Horses - Preakness Stakes Top Finishers</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 11, 2019.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Preakness Stakes Top Finishers  - May 18					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Tacitus - To Win</td><td>4/1</td><td>+400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Tacitus - Top 2</td><td>8/5</td><td>+160</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Tacitus - Top 3</td><td>5/6</td><td>-120</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Game Winner - To Win</td><td>5/1</td><td>+500</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Game Winner - Top 2</td><td>2/1</td><td>+200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Game Winner - Top 3</td><td>EV</td><td>EV</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes War Of Will - To Win</td><td>4/1</td><td>+400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o7t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes War Of Will - Top 2</td><td>8/5</td><td>+160</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o8t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes War Of Will - Top 3</td><td>5/6</td><td>-120</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o9t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Improbable - To Win</td><td>15/2</td><td>+750</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o10t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Improbable - Top 2</td><td>3/1</td><td>+300</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o11t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Improbable - Top 3</td><td>8/5</td><td>+160</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o12t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Laughing Fox - To Win</td><td>12/1</td><td>+1200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o13t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Laughing Fox - Top 2</td><td>24/5</td><td>+480</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o14t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Laughing Fox - Top 3</td><td>13/5</td><td>+260</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o15t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Owendale - To Win</td><td>14/1</td><td>+1400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o16t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Owendale - Top 2</td><td>28/5</td><td>+560</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o17t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Owendale - Top 3</td><td>31/10</td><td>+310</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o18t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Anothertwistafate - To Win</td><td>16/1</td><td>+1600</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o19t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Anothertwistafate - Top 2</td><td>32/5</td><td>+640</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o20t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Anothertwistafate - Top 3</td><td>7/2</td><td>+350</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o21t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Alwaysmining - To Win</td><td>12/1</td><td>+1200</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o22t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Alwaysmining - Top 2</td><td>24/5</td><td>+480</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o23t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>2019 Preakness Stakes Alwaysmining - Top 3</td><td>13/5</td><td>+260</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
{/literal}    
	