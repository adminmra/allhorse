<h2>Preakness Stakes Purse Structure</h2>

<table id="horse-betting" class="horse-betting" title="Preakness Stakes Purse" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody><tr>
                              <th width="250">Result</th>
                              <th>Purse of $1,000,000</th>
                            </tr>
                            <tr>
                              <td>Winner</td>
                              <td>$620,000 (62%) </td>
                            </tr>
                            <tr>
                              <td>Second</td>
                              <td>$200,000 (20%) </td>
                            </tr>
                            <tr>
                              <td>Third</td>
                              <td>$100,000 (10%) </td>
                            </tr>
                            <tr >
                              <td>Forth</td>
                              <td>$50,000 (5%) </td>
                            </tr>
                            <tr>
                              <td>Fifth</td>
                              <td>$30,000 (3%)</td>
                            </tr>
                            </tbody></table><a href="http://www.usracing.com" style="color:white;">Online Horse Betting</a>