<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Preakness Stakes Results"
  }
</script>
<h2>2021 Preakness Stakes Results</h2>
<!--fix this to the new table-->
<div id="no-more-tables">
<table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed ordenableResult">
    <thead>
      <tr>
        <th>Result</th>
        <th>Time</th>
        <th>Post</th>
        <th>Horse</th>
        <th>Jockey</th>
        <th>Trainer</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Result">1</td>
        <td data-title="Time">1:53.62</td>
        <td data-title="PP">6</td>
        <td data-title="Winner">Rombauer</td>
        <td data-title="Jockey">Flavien Prat</td>
        <td data-title="Trainer">Michael McCarthy</td>
      </tr>
      <tr>
        <td data-title="Result">2</td>
        <td data-title="Time">&nbsp;</td>
        <td data-title="PP">5</td>
        <td data-title="Horse">Midnight Bourbon</td>
        <td data-title="Jockey">Irad Ortiz</td>
        <td data-title="Trainer">Steve Asmussen</td>
      </tr>
      <tr>
        <td data-title="Result">3</td>
        <td data-title="Time">&nbsp;</td>
        <td data-title="PP">3</td>
        <td data-title="Horse">Medina Spirit</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Bob Baffert</td>
      </tr>
    </tbody>
  </table>
</div>

<h2>2021 Preakness Stakes Payouts</h2>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="data table table-condensed table-striped table-bordered"
    title="Preakness Stakes" summary="Preakness Stakes Payouts. ">
    <tbody>
      <tr>
        <th>PP</th>
        <th>Horses</th>
        <th>Win</th>
        <th>Place</th>
        <th>Show</th>
      </tr>
      <tr>
        <td>6</td>
        <td>Rombauer</td>
        <td>$25.60</td>
        <td>$10.00</td>
        <td>$5.20</td>
      </tr>
      <tr>
        <td>5</td>
        <td>Midnight Bourbon</td>
        <td>&nbsp;</td>
        <td>$4.60</td>
        <td>$3.00</td>
      </tr>
      <tr>
        <td>3</td>
        <td>Medina Spirit</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>$2.28</td>
      </tr>
    </tbody>
  </table>
</div>

<div id="no-more-tables">
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Classic Payouts" summary="Last year's Payouts of the Breeders' Cup Classic. ">
    <thead>
      <tr>
        <th>Wager</th>
        <th>Horses</th>
        <th>Denomination</th>
        <th>Payout</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Wager">Exacta</td>
        <td data-title="Horses">6-5</td>
        <td data-title="Denomination">$2</td>
        <td data-title="Payout">$98.60</td>
      </tr>
      <tr>
        <td data-title="Wager">Trifecta</td>
        <td data-title="Horses">6-5-3</td>
        <td data-title="Denomination">$1</td>
        <td data-title="Payout">$162.70</td>
      </tr>
      <tr>
        <td data-title="Wager">Superfecta</td>
        <td data-title="Horses">6-5-3-2</td>
        <td data-title="Denomination">$1</td>
        <td data-title="Payout">$1,025.50</td>
      </tr>
      <tr>
        <td data-title="Wager">Double</td>
        <td data-title="Horses">TBA</td>
        <td data-title="Denomination">$2</td>
        <td data-title="Payout">$301.20</td>
      </tr>
      <tr>
        <td data-title="Wager">Pick 3</td>
        <td data-title="Horses">TBA</td>
        <td data-title="Denomination">$0.50</td>
        <td data-title="Payout">$369.20</td>
      </tr>
    </tbody>
  </table>
</div>

<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_results.js"></script>