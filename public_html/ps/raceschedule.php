<h2 >  Preakness Stakes Day Race Schedule</h2>

			  <table id="horse-betting" class="horse-betting" title="Preakness Stakes Race Schedule" border="0" cellpadding="0" cellspacing="0" width="100%">                           
                            <tbody><tr>
                              <th>Stakes</th>
                              <th>Condition</th>
                              <th>Distance</th>
                              <th>Purse</th>
							  <th>Post Time</th>
                            </tr>
                            <tr>
                              <td>Maiden</td>
                              <td>Maisdens, 3YO&amp;Up</td>
                              <td>1-1/16 Miles</td>
                              <td>$50,000</td>
                              <td>10:45AM</td>
                            </tr>
                            <tr class="odd">
                              <td>Deputed Testamony Starter Handicap</td>
                              <td>3YO&amp;Up</td>
                              <td>1-1/16 Miles</td>
                              <td>$40,000</td>
                              <td>11:18AM</td>
                            </tr>
							  <tr>
                              <td>  Purse </td>
                              <td>	3YO&amp;Up </td>
                              <td>1 1/16 mile (T)</td>
                              <td>$55,000</td>
                              <td>11:53AM</td>
                            </tr>
							 <tr>
                              <td>Good Neighbors Race</td>
                              <td>3YO&amp;Up</td>
                              <td>	5 furlongs (T)</td>
                              <td>$55,000</td>
                              <td>12:30PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Maryland Sprint (G3) </td>
                              <td>3YO&amp;Up </td>
                              <td>6 furlongs </td>
                              <td>$150,000</td>
                              <td>1:09PM</td>
                            </tr>
                            <tr>
                              <td>The Very One Satkes</td>
                              <td>F&amp;M 3YO&amp;Up</td>
                              <td>5 furlongs (T)</td>
                              <td>$100,000</td>
                              <td>1:50PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Chick Lang Stakes</td>
                              <td>3YO</td>
                              <td>6 furlongs (T)</td>
                              <td>$100,000</td>
                              <td>2:33PM</td>
                            </tr>
						    <tr>
                              <td>James Murphy Stakes</td>
                              <td>3YO</td>
                              <td>1 Mile (T)</td>
                              <td>$100,000</td>
                              <td>3:14PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Gallorette Handicap (G3) </td>
                              <td>F&amp;M 3YO&amp;Up</td>
                              <td>1-1/16 Miles - Dirt </td>
                              <td>$150,000</td>
                              <td>4:04PM</td>
                            </tr>
							<tr>
                              <td>Sir Barton Stakes</td>
                              <td>3YO</td>
                              <td>1-1/16 Miles (T)</td>
                              <td>$100,000</td>
                              <td>4:45PM</td>
                            </tr>
							
                            <tr class="odd">
                              <td>Dixie Stakes (G2)</td>
                              <td>3YO&amp;Up</td>
                              <td>1-1/16 Miles (T)</td>
                              <td>$400,000</td>
                              <td>5:25PM</td>
                            </tr>
                           
							 <tr>
							   <td class="odd">Preakness Stakes (G1)</td>
							   <td class="odd">3YO</td>
							   <td class="odd">1-3/16 Miles </td>
							   <td class="odd">$1,500,000</td>
							   <td>6:18PM</td>
						      </tr>
							 <tr>
                              <td>Maiden</td>
                              <td>Maidens, 3YO&amp;Up</td>
                              <td>6 furlongs</td>
                              <td>$50,000</td>
                              <td>7:08PM</td>
                            </tr> 
                          </tbody></table>
</div><em><a href="http://www.usracing.com">US Racing</a></em>