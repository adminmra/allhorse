	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Preakness Stakes Odds:  Props"
	  }
	</script>
		{/literal}
	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Preakness Stakes Betting: Props">
			<caption>Horses - Preakness Stakes - Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated February 3, 2020 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Preakness Stakes - Props  - May 18					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Will Country House Win The 2019 Preakness Stakes?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>4/1</td><td>+400</td></tr><tr><td>No</td><td>1/7</td><td>-700</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Country House Wins The Triple Crown?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>20/1</td><td>+2000</td></tr><tr><td>No</td><td>1/100</td><td>-10000</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');addSort('o1t');</script>

{/literal}    
	