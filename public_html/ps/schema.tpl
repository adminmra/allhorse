{literal}
  <script type="application/ld+json">

        {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "name": "Bet the 2021 Preakness Stakes - Get Up To $500 Cash!",
        "startDate": "2021-05-15 00:00",
        "endDate": "2021-05-15 17:42",
        "sport": "Horse Racing",
        "location": {
            "@type": "Place",
            "name" : "Pimlico Race Course",
            "address": "5201 Park Heights Avenue Baltimore, MD 21215"
			},
        "url": "https://www.usracing.com/preakness-stakes/betting"
    }
        {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "name": "Bet the 2021 Belmont Stakes - Free Bets, Bonuses and More!",
        "startDate": "2021-06-06",
        "endDate": "2021-06-06",
        "sport": "Horse Racing",
        "location": {
            "@type": "Place",
            "name" : "Belmont Park",
            "address": "2150 Hempstead Turnpike, Elmont, NY 11003"
			},
        "url": "https://www.usracing.com/belmont-stakes/betting"
    }
        {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "name": "Bet the 2021 Kentucky Derby",
        "startDate": "2021-05-01",
        "endDate": "2021-05-01",
        "sport": "Horse Racing",
        "location": {
            "@type": "Place",
            "name" : "Churchill Downs",
            "address": "700 Central Avenue Louisville, KY 40208"
			},
        "url": "https://www.usracing.com/kentucky-derby/betting"
    }
</script>
{/literal}