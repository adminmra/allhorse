	<div class="table-responsive">
		<table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Preakness Stakes Match Races"  summary="">
			<caption>2018 Preakness Matchups</caption>
			<tbody>
			
<tr>
					<th colspan="3" class="center">
					2018 Preakness Matchups  - May 19					</th>
			</tr>

	<tr class='head_title'><th>Team</th><th colspan='2' class='center'>ML</th></tr><tr><td style='background:white'>Sporting Chance</td><td style='background:white'>-115</td></tr><tr><td style='background:white'>Tenfold</td><td style='background:white'>-115</td></tr><tr><td style='background:#ddd;'>Good Magic</td><td style='background:#ddd;'>+250</td></tr><tr><td style='background:#ddd;'>Justify</td><td style='background:#ddd;'>-330</td></tr><tr><td style='background:white'>Quip</td><td style='background:white'>-130</td></tr><tr><td style='background:white'>Diamond King</td><td style='background:white'>EV</td></tr><tr><td style='background:#ddd;'>Lone Sailor</td><td style='background:#ddd;'>+110</td></tr><tr><td style='background:#ddd;'>Bravazo</td><td style='background:#ddd;'>-140</td></tr>					<tr>
						<td class="dateUpdated center" colspan="5">
							<em id='updateemp'>Updated May 17, 2018 17:03:47.</em>
							BUSR - Official <a href="https://www.usracing.com/preakness-stakes/match-races">Preakness Stakes Match</a>. 

<!-- br />
							All odds are fixed odds prices. -->
						</td>
					</tr>
			</tbody>
		</table>
	</div>
	