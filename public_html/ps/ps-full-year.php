<?php
$cdate = date("Y-m-d H:i:s");
if ($cdate > "2019-05-18 23:59:00" && $cdate < "2020-05-16 23:59:00") {
    echo "Saturday, May 16th, 2020";
} else if ($cdate > "2020-05-16 23:59:00" && $cdate < "2021-05-15 23:59:00") {
    echo "Saturday, May 15th, 2021";
} else if ($cdate > "2021-05-15 23:59:00" && $cdate < "2022-05-21 23:59:00") {
    echo "Saturday, May 21st, 2022";
} else if ($cdate > "2022-05-21 23:59:00" && $cdate < "2023-05-20 23:59:00") { 
    echo "Saturday, May 20th, 2023";
} else if ($cdate > "2023-05-20 23:59:00" && $cdate < "2024-05-18 23:59:00") {
    echo "Saturday, May 24th, 2024";
} else if ($cdate > "2024-05-18 23:59:00" && $cdate < "2025-05-17 23:59:00") {
    echo "Saturday, May 17th, 2025";
}
?>