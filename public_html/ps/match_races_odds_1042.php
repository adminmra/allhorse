	<div>
		<table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Preakness Stakes Match Races"  summary="">
			<caption>Horses - Preakness Matchups</caption>
			<tbody>
			
<tr>
					<th colspan="3" class="center">
					Horses - Preakness Matchups  - May 18					</th>
			</tr>

			
<tr>
					<th colspan="3" class="center">
					2019 Preakness Matchups<br />@ Pimlico Race Track - Baltimore, Md					</th>
			</tr>

	<tr class='head_title'><th>Team</th><th colspan='2' class='center'>ML</th></tr><tr><td style='background:white'>War Of Will</td><td style='background:white'>+127</td></tr><tr><td style='background:white'>Improbable</td><td style='background:white'>-157</td></tr><tr><td style='background:#ddd;'>War Of Will</td><td style='background:#ddd;'>-194</td></tr><tr><td style='background:#ddd;'>Anothertwistafate</td><td style='background:#ddd;'>+159</td></tr><tr><td style='background:white'>Improbable</td><td style='background:white'>-209</td></tr><tr><td style='background:white'>Anothertwistafate</td><td style='background:white'>+169</td></tr><tr><td style='background:#ddd;'>Alwaysmining</td><td style='background:#ddd;'>-134</td></tr><tr><td style='background:#ddd;'>Anothertwistafate</td><td style='background:#ddd;'>+104</td></tr><tr><td style='background:white'>Bourbon War</td><td style='background:white'>-116</td></tr><tr><td style='background:white'>Owendale</td><td style='background:white'>-114</td></tr><tr><td style='background:#ddd;'>Warriors Charge</td><td style='background:#ddd;'>-130</td></tr><tr><td style='background:#ddd;'>Win Win Win</td><td style='background:#ddd;'>EV</td></tr><tr><td style='background:white'>Warriors Charge</td><td style='background:white'>+161</td></tr><tr><td style='background:white'>Owendale</td><td style='background:white'>-201</td></tr><tr><td style='background:#ddd;'>Bourbon War</td><td style='background:#ddd;'>-199</td></tr><tr><td style='background:#ddd;'>Win Win Win</td><td style='background:#ddd;'>+164</td></tr><tr><td style='background:white'>Market King</td><td style='background:white'>+198</td></tr><tr><td style='background:white'>Signalman</td><td style='background:white'>-248</td></tr><tr><td style='background:#ddd;'>Bodexpress</td><td style='background:#ddd;'>-133</td></tr><tr><td style='background:#ddd;'>Laughing Fox</td><td style='background:#ddd;'>+103</td></tr>					<tr>
						<td class="dateUpdated center" colspan="5">
							<em id='updateemp'>  - Updated February 3, 2020 09:31:12 </em>
							 

<!-- br />
							All odds are fixed odds prices. -->
						</td>
					</tr>
			</tbody>
		</table>
	</div>
	