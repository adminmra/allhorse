<?php /*
<div class="table-responsive"><table width="100%" cellspacing="0" cellpadding="0" summary="2016 Preakness Stakes Odds" title="Preakness Stakes Odds" class="table table-condensed table-striped table-bordered" id="infoEntries">
<tr>
<th colspan="2">2015 Preakness Stakes Contenders</th>
<th class="right">Odds to Win</th>
</tr>
  <tr>
    <td>1</td>
    <td>American Pharoah</td>
    <td class="right">5/4</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Dortmund</td>
    <td class="right">9/2</td>
  </tr>
  <tr>
    <td>3</td>
    <td>Mr. Z</td>
    <td class="right">25/1</td>
  </tr>
  <tr>
    <td>4</td>
    <td>Danzig Moon</td>
    <td class="right">20/1</td>
  </tr>
  <tr>
    <td>5</td>
    <td>Tail of Verve</td>
    <td class="right">50/1</td>
  </tr>
  <tr>
    <td>6</td>
    <td>Bodhisattva</td>
    <td class="right">30/1</td>
  </tr>
 <!-- <tr>
    <td>Carpe Diem</td>
    <td class="right">10/1</td>
  </tr>
  <tr>
    <td>Competitive Edge</td>
    <td class="right">15/1</td>
  </tr>-->
  <tr>
    <td>7</td>
    <td>Divining Rod</td>
    <td class="right">20/1</td>
  </tr>
  <tr>
    <td>8</td>
    <td>Firing Line</td>
    <td class="right">4/1</td>
  </tr>
  <!--<tr>
    <td>International Star</td>
    <td class="right">18/1</td>
  </tr>-->   <!--<tr>
    <td>Materiality</td>
    <td class="right">12/1</td>
  </tr>-->  <!--<tr>
    <td>Stanford</td>
    <td class="right">30/1</td>
  </tr>-->
 
    <tr>
      <td colspan="3"class="dateUpdated center" ><em>Updated May 13th, 2015 - US Racing - <a href="http://www.usracing.com/preakness-stakes/odds">Preakness Stakes Odds</a></td></tr>
</table></div>
*/ ?>
<?php 

include('/home/ah/allhorse/public_html/ps/odds_983.php');

/*
$content=file_get_contents("/home/ah/allhorse/public_html/ps/2019_odds_983.php");
$content=str_replace('{literal}','',$content);
$content=str_replace('{/literal}','',$content);
echo $content; */
?>
