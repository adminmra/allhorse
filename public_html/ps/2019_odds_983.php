
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2019
 Preakness Stakes Odds"
	  }
	</script>

    <div>
        <table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Preakness Stakes Odds"  summary="The latest odds for the Preakness Stakes available for wagering now at BUSR.">
            <caption>2019
 Preakness Stakes Odds</caption>
            <tbody>
            <!--
    <tr>
                    <th colspan="3" class="center">
                    Horses - Preakness Stakes  - May 18                    </th>
            </tr>
-->
    <tr class='head_title'>
						</th>
						<th>
        <!--Team--> Horse</th>
    <th>Fractional</th>
    <th>American</th>
</tr>
<tr>
    <td>Improbable </td>
    <td>5/2</td>
    <td>+250</td>
</tr>
<tr>
    <td>War of Will</td>
    <td>4/1</td>
    <td>+400</td>
</tr>
<tr>
    <td>Alwaysmining</td>
    <td>8/1</td>
    <td>+800</td>
</tr>
<tr>
    <td>Win Win Win</td>
    <td>15/1</td>
    <td>+1500</td>
</tr>
<tr>
    <td>Anothertwistafate</td>
    <td>6/1</td>
    <td>+600</td>
</tr>
<tr>
    <td>Owendale</td>
    <td>10/1</td>
    <td>+1000</td>
</tr>
<tr>
    <td>Bourbon War</td>
    <td>12/1</td>
    <td>+1200</td>
</tr>
<tr>
    <td>Signalman</td>
    <td>30/1</td>
    <td>+3000</td>
</tr>
<tr>
    <td>Bodexpress</td>
    <td>20/1</td>
    <td>+2000</td>
</tr>
<tr>
    <td>Laughing Fox</td>
    <td>20/1</td>
    <td>+2000</td>
</tr>
<tr>
    <td>Warrior&apos;s Charge</td>
    <td>12/1</td>
    <td>+1200</td>
</tr>
<tr>
    <td>Market King</td>
    <td>30/1</td>
    <td>+3000</td>
</tr>
<tr>
    <td>Everfast</td>
    <td>50/1</td>
    <td>+5000</td>
</tr>
						</tbody>
        </table>
        <div class="dateUpdated center">
            <em id='updateemp'>Updated <?php $tdate = date("M d, Y H:00", time() - 7200); echo $tdate . " EST"; ?> .</em><!-- br />
            All odds are fixed odds prices. -->
        </div>
    </div>
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sort_table.js"></script>
    
