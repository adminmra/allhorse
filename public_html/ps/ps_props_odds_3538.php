		{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Preakness Stakes Betting Odds:  Props"
	  }
	</script>
		{/literal}
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Preakness Stakes Betting Odds:  Props" summary="Preakness Stakes Betting Odds:  Props">
            <caption>Preakness Stakes Betting Odds:  Props</caption>
            <tbody>
               <!--
 <tr>
                    <th colspan="3" class="center">
                    Horses - Preakness Stakes Top Finishers  - May 18                    </th>
            </tr>
-->
               <!--
 <tr>
                    <th colspan="3" class="center">
                    No Runner No Bet                    </th>
            </tr>
-->
    <tr class='head_title'><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>2019 Preakness Stakes Improbable - To Win</td><td>+190</td><td>19/10</td></tr><tr><td>2019 Preakness Stakes Improbable - Top 2</td><td>-130</td><td>10/13</td></tr><tr><td>2019 Preakness Stakes Improbable - Top 3</td><td>-240</td><td>5/12</td></tr><tr><td>2019 Preakness Stakes War Of Will - To Win</td><td>+330</td><td>33/10</td></tr><tr><td>2019 Preakness Stakes War Of Will - Top 2</td><td>+150</td><td>3/2</td></tr><tr><td>2019 Preakness Stakes War Of Will - Top 3</td><td>EV</td><td>EV</td></tr><tr><td>2019 Preakness Stakes Alwaysmining - To Win</td><td>+700</td><td>7/1</td></tr><tr><td>2019 Preakness Stakes Alwaysmining - Top 2</td><td>+320</td><td>16/5</td></tr><tr><td>2019 Preakness Stakes Alwaysmining - Top 3</td><td>+210</td><td>21/10</td></tr><tr><td>2019 Preakness Stakes Bourbon War - To Win</td><td>+850</td><td>17/2</td></tr><tr><td>2019 Preakness Stakes Bourbon War - Top 2</td><td>+385</td><td>77/20</td></tr><tr><td>2019 Preakness Stakes Bourbon War - Top 3</td><td>+255</td><td>51/20</td></tr><tr><td>2019 Preakness Stakes Anothertwistafate - To Win</td><td>+900</td><td>9/1</td></tr><tr><td>2019 Preakness Stakes Anothertwistafate - Top 2</td><td>+360</td><td>18/5</td></tr><tr><td>2019 Preakness Stakes Anothertwistafate - Top 3</td><td>+200</td><td>2/1</td></tr><tr><td>2019 Preakness Stakes Owendale - To Win</td><td>+1400</td><td>14/1</td></tr><tr><td>2019 Preakness Stakes Owendale - Top 2</td><td>+560</td><td>28/5</td></tr><tr><td>2019 Preakness Stakes Owendale - Top 3</td><td>+310</td><td>31/10</td></tr><tr><td>2019 Preakness Stakes Warrior's Charge - To Win</td><td>+1400</td><td>14/1</td></tr><tr><td>2019 Preakness Stakes Warrior's Charge - Top 2</td><td>+635</td><td>127/20</td></tr><tr><td>2019 Preakness Stakes Warrior's Charge - Top 3</td><td>+420</td><td>21/5</td></tr><tr><td>2019 Preakness Stakes Win Win Win - To Win</td><td>+1500</td><td>15/1</td></tr><tr><td>2019 Preakness Stakes Win Win Win - Top 2</td><td>+600</td><td>6/1</td></tr><tr><td>2019 Preakness Stakes Win Win Win - Top 3</td><td>+330</td><td>33/10</td></tr><tr><td>2019 Preakness Stakes Signalman - To Win</td><td>+1800</td><td>18/1</td></tr><tr><td>2019 Preakness Stakes Signalman - Top 2</td><td>+720</td><td>36/5</td></tr><tr><td>2019 Preakness Stakes Signalman - Top 3</td><td>+400</td><td>4/1</td></tr><tr><td>2019 Preakness Stakes Bodexpress - To Win</td><td>+1800</td><td>18/1</td></tr><tr><td>2019 Preakness Stakes Bodexpress - Top 2</td><td>+815</td><td>163/20</td></tr><tr><td>2019 Preakness Stakes Bodexpress - Top 3</td><td>+540</td><td>27/5</td></tr><tr><td>2019 Preakness Stakes Laughing Fox - To Win</td><td>+2800</td><td>28/1</td></tr><tr><td>2019 Preakness Stakes Laughing Fox - Top 2</td><td>+1270</td><td>127/10</td></tr><tr><td>2019 Preakness Stakes Laughing Fox - Top 3</td><td>+845</td><td>169/20</td></tr><tr><td>2019 Preakness Stakes Market King - To Win</td><td>+3300</td><td>33/1</td></tr><tr><td>2019 Preakness Stakes Market King - Top 2</td><td>+1500</td><td>15/1</td></tr><tr><td>2019 Preakness Stakes Market King - Top 3</td><td>+1000</td><td>10/1</td></tr><tr><td>2019 Preakness Stakes Everfast - To Win</td><td>+10000</td><td>100/1</td></tr><tr><td>2019 Preakness Stakes Everfast - Top 2</td><td>+4500</td><td>45/1</td></tr><tr><td>2019 Preakness Stakes Everfast - Top 3</td><td>+3000</td><td>30/1</td></tr>            </tbody>
			<tfoot>
		 <tr>
                        <td class="dateUpdated center"  colspan="3" >
                            <em id='updateemp'>Updated May 18, 2019.</em><!-- br />
                            All odds are fixed odds prices. -->
                        </td>
                    </tr>
            </tfoot>			
        </table>
		    {literal}
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
 

        {/literal}
    </div>
    