
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2021
 Preakness Stakes Odds"
	  }
	</script>

    <div>
        <table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Preakness Stakes Odds"  summary="The latest odds for the Preakness Stakes available for wagering now at Bet US Racing.">
            <caption>2021
 Preakness Stakes Odds</caption>
            <tbody>
            <!--
    <tr>
                    <th colspan="3" class="center">
                    Horses - Preakness Stakes  - May 15                    </th>
            </tr>
-->
    <tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Medina Spirit</td><td>23/10</td><td>+230</td></tr><tr><td>Concert Tour</td><td>11/4</td><td>+275</td></tr><tr><td>Midnight Bourbon</td><td>3/1</td><td>+300</td></tr><tr><td>Crowded Trade</td><td>7/1</td><td>+700</td></tr><tr><td>Risk Taking</td><td>16/1</td><td>+1600</td></tr><tr><td>Robmauer</td><td>16/1</td><td>+1600</td></tr><tr><td>Keepmeinmind </td><td>18/1</td><td>+1800</td></tr><tr><td>Unbridled Honor</td><td>28/1</td><td>+2800</td></tr><tr><td>France Go De Ina</td><td>33/1</td><td>+3300</td></tr><tr><td>Ram</td><td>40/1</td><td>+4000</td></tr>            </tbody>
        </table>
        <div class="dateUpdated center">
            <em id='updateemp'>  - Updated November 16, 2021 19:00:04 </em>
        </div>
    </div>
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sort_table.js"></script>
    