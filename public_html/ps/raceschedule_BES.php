<div class="table-responsive">

<table id="infoEntries" width="100%" border="0" cellpadding="0" cellspacing="0" summary="Preakness Stakes Race Schedule" class="table table-condensed table-striped table-bordered">                           
                            <tr>
                              <th >Stakes</th>
                              <th >Condition</th>
                              <th >Distance</th>
                              <th >Purse</th>
							  <th >Post Time</th>
                            </tr>
                          <tr>
                              <td >Kattegat's Pride Starter Handicap</td>
                            <td >F&amp;M 3YO&amp;Up</td>
                              <td >1-1/16 Miles</td>
                            <td >$40,000</td>
                              <td >12:00PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Hall of Fame Jockey Challenge Race 1</td>
                              <td>F&amp;M 3YO&amp;Up</td>
                              <td>5 furlongs - Turf</td>
                              <td>$52,000</td>
                              <td>12:31PM</td>
                            </tr>
							  <tr>
                              <td > Lady Legend's for the Cure</td>
                              <td >3YO&amp;Up</td>
                              <td >6 furlongs</td>
                              <td >$52,000</td>
                              <td >01:00PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Hall of Fame Jockey Challenge Race 2</td>
                              <td>F&amp;M 3YO&amp;Up</td>
                              <td>1-1/16 Miles - Turf</td>
                              <td>$52,000</td>
                              <td>01:30PM</td>
                            </tr>
							 <tr>
                              <td >Skipat Stakes</td>
                              <td >F&amp;M 3YO&amp;Up</td>
                              <td >6 furlongs </td>
                              <td >$75,000</td>
                              <td >02:01PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Hall of Fame Jockey Challenge Race 3</td>
                              <td>3YO&amp;Up</td>
                              <td>1-1/16 Miles - Turf</td>
                              <td>$52,000</td>
                              <td>02:34PM</td>
                            </tr>
                            <tr >
                              <td>Rollicking Stakes</td>
                              <td>2YO</td>
                              <td>5 furlongs</td>
                              <td>$75,000</td>
                              <td>3:05PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Hall of Fame Jockey Challenge Race 4</td>
                              <td>F&amp;M 3YO&amp;Up</td>
                              <td>1-1/16 Miles - Turf</td>
                              <td>$55,000</td>
                              <td>3:36PM</td>
                            </tr>
						    <tr >
                              <td>Jim McKay Turf Sprint</td>
                              <td>3YO</td>
                              <td>5 furlongs - Turf</td>
                              <td>$100,000</td>
                              <td>4:16PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Black Eyed Susan (G2)</td>
                              <td>3YO Fillies</td>
                              <td>1-1/8 Miles </td>
                              <td>$500,000</td>
                              <td>4:47PM</td>
                            </tr>
							<tr >
                              <td>Ms. Preakness Stakes</td>
                              <td>3YO Fillies</td>
                              <td>6 furlongs</td>
                              <td>$100,000</td>
                              <td>5:20PM</td>
                            </tr>
							
                            <tr class="odd">
                              <td>Pimlico Special (G#)</td>
                              <td>3YO&Up </td>
                              <td>1-3/16 Miles </td>
                              <td>$300,000</td>
                              <td>5:52PM</td>
                            </tr>
                           
							 <tr>
                              <td>Hilltop Stakes</td>
                              <td>F 3YO</td>
                              <td>1-1/16 Miles - Turf</td>
                              <td>$100,000</td>
                              <td>6:22PM</td>
                            </tr> 
                          </table>
                          </div>