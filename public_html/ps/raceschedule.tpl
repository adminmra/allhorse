<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered">                  
                            <tr>
                              <th width="235" >Stakes</th>
                              <th width="95" >Condition</th>
                              <th width="112" >Distance</th>
                              <th width="72" >Purse</th>
							  <th width="67" >Post Time</th>
                            </tr>
                            <tr>
                              <td class="horse-betting">Maiden</td>
                              <td class="horse-betting">3YO&amp;Up</td>
                              <td class="horse-betting">1-1/16 Miles</td>
                              <td class="horse-betting">$50,000</td>
                              <td class="horse-betting">10:45AM</td>
                            </tr>
                            <tr class="odd">
                              <td>Deputed Testamony Starter Handicap</td>
                              <td>3YO&amp;Up</td>
                              <td>1-1/16 Miles</td>
                              <td>$40,000</td>
                              <td>11:18AM</td>
                            </tr>
							  <tr>
							    <td class="horse-betting"> Purse </td>
                                <td class="horse-betting"> 3YO&amp;Up </td>
                                <td class="horse-betting">1 1/16 mile (T)</td>
                                <td class="horse-betting">$55,000</td>
                                <td class="horse-betting">11:53AM</td>
						    </tr>
                            <tr class="odd">
                              <td class="horse-betting">Good Neighbors Race</td>
                              <td class="horse-betting">3YO&amp;Up</td>
                              <td class="horse-betting"> 5 furlongs (T)</td>
                              <td class="horse-betting">$55,000</td>
                              <td class="horse-betting">12:30PM</td>
                            </tr>
							 <tr>
							   <td class="odd">Maryland Sprint (G3) </td>
                               <td class="odd">3YO&amp;Up </td>
                               <td class="odd">6 furlongs </td>
                               <td class="odd">$150,000</td>
                               <td class="odd">1:09PM</td>
						    </tr>
                            <tr class="odd">
                              <td class="horse-betting">The Very One Satkes</td>
                              <td class="horse-betting">F&amp;M 3YO&amp;Up</td>
                              <td class="horse-betting">5 furlongs (T)</td>
                              <td class="horse-betting">$100,000</td>
                              <td class="horse-betting">1:50PM</td>
                            </tr>
                            <tr >
                              <td class="odd">Chick Lang Stakes</td>
                              <td class="odd">3YO</td>
                              <td class="odd">6 furlongs (T)</td>
                              <td class="odd">$100,000</td>
                              <td class="odd">2:33PM</td>
                            </tr>
                            <tr class="odd">
                              <td class="horse-betting">James Murphy Stakes</td>
                              <td class="horse-betting">3YO</td>
                              <td class="horse-betting">1 Mile (T)</td>
                              <td class="horse-betting">$100,000</td>
                              <td class="horse-betting">3:14PM</td>
                            </tr>
						    <tr >
						      <td class="odd">Gallorette Handicap (G3) </td>
                              <td class="odd">F&amp;M 3YO&amp;Up</td>
                              <td class="odd">1-1/16 Miles - Dirt </td>
                              <td class="odd">$150,000</td>
                              <td class="odd">4:04PM</td>
						    </tr>
                            <tr class="odd">
                              <td class="horse-betting">Sir Barton Stakes</td>
                              <td class="horse-betting">3YO</td>
                              <td class="horse-betting">1-1/16 Miles (T)</td>
                              <td class="horse-betting">$100,000</td>
                              <td class="horse-betting">4:45PM</td>
                            </tr>
							<tr >
							  <td class="odd">Dixie Stakes (G2)</td>
                              <td class="odd">3YO&amp;Up</td>
                              <td class="odd">1-1/16 Miles (T)</td>
                              <td class="odd">$400,000</td>
                              <td class="odd">5:25PM</td>
							</tr>
							
                            <tr class="odd">
                              <td>Preakness Stakes (G1)</td>
                              <td>3YO</td>
                              <td>1-3/16 Miles </td>
                              <td>$1,500,000</td>
                              <td class="horse-betting">6:18PM</td>
                            </tr>
                           
							 <tr>
							   <td class="horse-betting">Maiden</td>
                               <td class="horse-betting">3YO&amp;Up</td>
                               <td class="horse-betting">6 furlongs</td>
                               <td class="horse-betting">$50,000</td>
                               <td class="horse-betting">7:08PM</td>
						    </tr> 
                          </table>
</div>