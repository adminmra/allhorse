<div class="table-responsive">
        <table class="data table table-condensed table-striped table-bordered" title="Preakness Stakes Contenders" summary="The latest contenders for the Preakness Stakes available for wagering now at BUSR." border="1" cellpadding="0" cellspacing="0" width="100%">
            <caption>
            2016 Preakness Stakes Contenders
            </caption>
            <tbody>
            <!--
    <tr>
                    <th colspan="3" class="center">
                    Horses - 2016 Preakness Stakes  - May 21                    </th>
            </tr>
-->
            <!--
    <tr>
                    <th colspan="3" class="center">
                    All Bets Are Action - Run Or Not                    </th>
            </tr>
-->
    <tr class="head_title">
      <th align="center"><!--Team-->Post</th>
      <th>Horse</th>
      <th>Jockey</th>
      <th>Trainer</th>
      <th align="center">Odds</th>
    </tr>
    <tr>
      <td align="center">1</td>
      <td>Cherry Wine </td>
      <td>Corey Lanerie</td><td>Dale Romans</td>
      <td align="center">20/1</td>
    </tr>
    <tr>
        <td align="center">2</td>
        <td>Uncle Lino</td>
        <td>Fernando Perez</td><td>Gary Sherlock</td>
        <td align="center">20/1</td>
    </tr>
    <tr>
          <td align="center">3</td>
          <td>Nyquist</td>
          <td>Mario Gutierrez</td><td>Doug O?Neil</td>
          <td align="center">3/5</td>
    </tr>
    <tr>
            <td align="center">4</td>
            <td>Awesome Speed</td>
            <td>Javian Toledo</td><td>Alan Goldberg</td>
            <td align="center">30/1</td>
    </tr>
    <tr>
              <td align="center">5</td>
              <td>Exaggerator</td>
              <td>Kent Desormeaux </td><td>Keith Desormeaux</td>
              <td align="center">3/1</td>
    </tr>
    <tr>
                <td align="center">6</td>
                <td>Lani</td>
                <td>Yutaka Take</td><td>Mikio Matsunaga</td>
                <td align="center">30/1</td>
    </tr>
    <tr>
                <td align="center">7</td>
                <td>Collected</td>
                <td>Javer Castellano</td><td>Bob Baffert</td>
                <td align="center">10/1</td>
    </tr>
    <tr>
                <td align="center">8</td>
                <td>Laoban</td>
                <td>Florent Geroux</td><td>Eric Guillot</td>
                <td align="center">30/1</td>
    </tr>
    <tr>
                <td align="center">9</td>
                <td>Abiding Star </td>
                <td>J.D. Acosta </td><td>Ned Allar</td>
                <td align="center">30/1</td>
    </tr>
    <tr>
                <td align="center">10</td>
                <td>Fellowship</td>
                <td>Jose Lezcano</td><td>Mark Casse</td>
                <td align="center">30/1</td>
    </tr>
    <tr>
                <td align="center">11</td>
                <td>Stradivari</td>
                <td>John Velazquez</td><td>Todd Pletcher</td>
                <td align="center">8/1</td>
    </tr>                    
    <tr>
                <td class="dateUpdated center" colspan="7">
                    <em id="updateemp">Updated May 18, 2016 05:00:27.</em>
                    BUSR - Official <a href="https://www.usracing.com/preakness-stakes/contenders">Preakness Stakes Contenders</a>. <!-- br>
                    All odds are fixed odds prices. -->
                </td>
                </tr>
            </tbody>
        </table>
    </div>