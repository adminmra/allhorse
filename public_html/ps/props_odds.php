	<div>
		<table class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Preakness Stakes Props"  summary="">
			<caption>Horses - Preakness Stakes - Props</caption>
			<tbody>
			
<tr>
					<th colspan="3" class="center">
					Horses - Preakness Stakes - Props  - Jun 28					</th>
			</tr>

	<tr><th colspan="3" class="center">Medina Spirit Stripped Of 2021 Kentucky Derby Win</th></tr><tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes</td><td>-190</td><td>10/19</td></tr><tr><td>No </td><td>+145</td><td>29/20</td></tr><tr><th colspan="3" class="center"> B.baffert Banned Fr Churchill Downs Rest Of 2021</th></tr><tr><td>Yes</td><td>EV</td><td>EV</td></tr><tr><td>No </td><td>-140</td><td>5/7</td></tr>
			</tbody>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 19:00:06 </em>
                        <!-- BUSR - Official <a href="https://www.usracing.com/preakness-stakes/props">Preakness Stakes Props</a>. -->
                    </td>
                </tr>
            </tfoot>
		</table>
	</div>
	