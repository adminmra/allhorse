    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Preakness Stakes Odds"  summary="The latest odds for the Preakness Stakes available for wagering now at BUSR.">
            <caption>2018
 Preakness Stakes Odds</caption>
            <tbody>
            <!--
    <tr>
                    <th colspan="3" class="center">
                    Horses - Preakness Stakes  - May 19                    </th>
            </tr>
-->
            <!--
    <tr>
                    <th colspan="3" class="center">
                    Run Or Not All Wagers Have Action<br />...                    </th>
            </tr>
-->
    <tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Bravazo</td><td>20/1</td><td>+2000</td></tr><tr><td>Diamond King</td><td>30/1</td><td>+3000</td></tr><tr><td>Good Magic</td><td>13/4</td><td>+325</td></tr><tr><td>Justify</td><td>4/11</td><td>-275</td></tr><tr><td>Lone Sailor</td><td>18/1</td><td>+1800</td></tr><tr><td>Quip</td><td>12/1</td><td>+1200</td></tr><tr><td>Sporting Chance</td><td>33/1</td><td>+3300</td></tr><tr><td>Tenfold</td><td>25/1</td><td>+2500</td></tr>            </tbody>
        </table>
        <div class="dateUpdated center">
            <em id='updateemp'>  - Updated May 31, 2018 12:59:08 </em>
            BUSR - Official <a href="https://www.usracing.com/preakness-stakes/odds">Preakness Stakes Odds</a>. <!-- br />
            All odds are fixed odds prices. -->
        </div>
    </div>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sort_table.js"></script>
    
