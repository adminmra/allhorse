<div id="no-more-tables">
	<table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center" cellpadding="0"
		cellspacing="0" border="0" title="2021 Preakness Stakes Odds"
		summary="2021 Preakness Stakes Odds">

		<caption class="hide-sm">2021 Preakness Stakes Odds</caption>


        <tbody>
		<tr class='head_title hide-sm'>
            <th>PP</th>
            <th>Horse</th>
            <th>Odds</th>
            <th>Trainer</th>
            <th>Jockey</th>
		</tr>
		<tr>
            <td data-title="PP">1</td>
            <td data-title="Horse">Ram</td>
            <td data-title="Odds">30-1</td>
            <td data-title="Trainer">D. Wayne Lukas</td>
            <td data-title="Jockey">Ricardo Santana Jr.</td>
        </tr>
        <tr>
            <td data-title="PP">2</td>
            <td data-title="Horse">Keepmeinmind</td>
            <td data-title="Odds">15-1</td>
            <td data-title="Trainer">Robertino Diodoro</td>
            <td data-title="Jockey">David Cohen</td>
        </tr>
        <tr>
            <td data-title="PP">3</td>
            <td data-title="Horse">Medina Spirit</td>
            <td data-title="Odds">9-5</td>
            <td data-title="Trainer">Bob Baffert</td>
            <td data-title="Jockey">John Velazquez</td>
        </tr>
        <tr>
            <td data-title="PP">4</td>
            <td data-title="Horse">Crowded Trade</td>
            <td data-title="Odds">10-1</td>
            <td data-title="Trainer">Chad Brown</td>
            <td data-title="Jockey">Javier Castellano</td>
        </tr>
        <tr>
            <td data-title="PP">5</td>
            <td data-title="Horse">Midnight Bourbon</td>
            <td data-title="Odds">5-1</td>
            <td data-title="Trainer">Steve Asmussen</td>
            <td data-title="Jockey">Irad Ortiz</td>
        </tr>
        <tr>
            <td data-title="PP">6</td>
            <td data-title="Horse">Rombauer</td>
            <td data-title="Odds">12-1</td>
            <td data-title="Trainer">Michael McCarthy</td>
            <td data-title="Jockey">Flavien Prat</td>
        </tr>
        <tr>
            <td data-title="PP">7</td>
            <td data-title="Horse">France Go De Ina</td>
            <td data-title="Odds">20-1</td>
            <td data-title="Trainer">Hideyuki Mori</td>
            <td data-title="Jockey">Joel Rosario</td>
        </tr>
        <tr>
            <td data-title="PP">8</td>
            <td data-title="Horse">Unbridled Honor</td>
            <td data-title="Odds">15-1</td>
            <td data-title="Trainer">Todd Pletcher</td>
            <td data-title="Jockey">Luis Saez</td>
        </tr>
        <tr>
            <td data-title="PP">9</td>
            <td data-title="Horse">Risk Taking</td>
            <td data-title="Odds">15-1</td>
            <td data-title="Trainer">Chad Brown</td>
            <td data-title="Jockey">Jose Ortiz</td>
        </tr>
        <tr>
            <td data-title="PP">10</td>
            <td data-title="Horse">Concert Tour</td>
            <td data-title="Odds">5-2</td>
            <td data-title="Trainer">Bob Baffert</td>
            <td data-title="Jockey">Mike Smith</td>
        </tr>
	  </tbody>
	</table>


</div>


<style>
	table.ordenable tbody th {
		cursor: pointer;
	}
	@media only screen and (max-width: 800px) {
		#no-more-tables td span {
			text-align: center !important;
		}
		.hide-sm {
		  display: none !important;
		}
	}
</style>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">

<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sorttable_post.js"></script>
<script src="/assets/js/aligncolumn.js"></script>

<script>
	$(document).ready(function () {

		alignColumn([2], 'left');

	});
</script>
