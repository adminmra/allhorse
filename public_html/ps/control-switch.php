
<?php //PREAKENSS Control Switch
$SwitchVariable['variableName'][]= 'PS_Switch_Early';         //Three Weeks Out
$SwitchVariable['startTIME'][]= '2020-09-12 00:00';         // 1 hero image, primary menu, pages metas
$SwitchVariable['endTIME'][]= '2020-05-12 00:00';           // TURN *OFF*   at KD_Switch_Main

$SwitchVariable['variableName'][]= 'PS_Switch_Main';        // Two Weeks out.
$SwitchVariable['startTIME'][]= '2021-05-01 19:01'; 	// full PS hero, primary menu, metas, index, kd & ko odds copy
$SwitchVariable['endTIME'][]= '2021-05-14 23:50';	//Ends day before PS Day

$SwitchVariable['variableName'][]= 'PS_Switch_Full_Site';        //********** Runs Concurrently! ***********  One Week out.
$SwitchVariable['startTIME'][]= '2021-05-10 10:00'; 	// PS Switch Main + states & Misc page copy
$SwitchVariable['endTIME'][]= '2021-05-15 18:54';

$SwitchVariable['variableName'][]= 'PS_Switch_Race_Day';        // 
$SwitchVariable['startTIME'][]= '2021-05-15 00:00'; 	// PS Switch Full site + day of meta changes 
$SwitchVariable['endTIME'][]= '2021-05-15 18:54';

/*$SwitchVariable['variableName'][]= 'PS_Switch_Results';        // Day of Preakness to End of Sunday
$SwitchVariable['startTIME'][]= '2020-05-15 17:42';         // starts at end of the race
$SwitchVariable['endTIME'][]= '2020-05-17 10:00';           //

$SwitchVariable['variableName'][]= 'PS_Clubhouse_Switch_Two_Weeks_Out';
$SwitchVariable['startTIME'][]='2020-09-24 00:00';
$SwitchVariable['endTIME'][]='2020-09-25 17:59';

$SwitchVariable['variableName'][]= 'PS_Clubhouse_Switch_Help_Section';
$SwitchVariable['startTIME'][]='2020-09-25 18:00';
$SwitchVariable['endTIME'][]='2020-10-03 23:59';

$SwitchVariable['variableName'][]= 'PS_Clubhouse_Switch_Blackjack_Tournament';
$SwitchVariable['startTIME'][]='2020-10-04 00:00';
$SwitchVariable['endTIME'][]='2020-10-06 23:59';

/*
	$SwitchVariable['variableName'][]= 'PS_Switch'; 		// End of KD Race to End of Preakness Race
$SwitchVariable['startTIME'][]='2019-05-04 18:56'; 		// TURN *ON*    AT END OF DERBY RACE ***************************** DERBY
$SwitchVariable['endTIME'][]='2019-05-18 18:55';		// TURN *OFF*   AT END OF PREAKNESS RACE ***************************** PREAKNESS

$SwitchVariable['variableName'][]= 'PS_Switch_Phase_1'; // One Week Out to End of Preakness Race
$SwitchVariable['startTIME'][]='2019-05-12 00:00'; 		// One Week Out to End of Preakness Race
$SwitchVariable['endTIME'][]='2019-05-18 18:55';		// TURN *OFF*   AT END OF PREAKNESS RACE ***************************** PREAKNESS

$SwitchVariable['variableName'][]= 'PS_Switch_Results'; // Day of Preakness Race to Week After Belmont
$SwitchVariable['startTIME'][]='2019-05-18 0:00'; 		// Starts Day of Preakness 
$SwitchVariable['endTIME'][]='2019-06-15 23:59';		// End 1 week after Belmont

$SwitchVariable['variableName'][]= 'PS_Switch_Old_Odds'; // Day of Kentucky Derby for 5 days
$SwitchVariable['startTIME'][]='2019-05-18 20:00'; 		// TURN *ON*   AT END OF PREAKNESS ***************************** PREAKNESS 
$SwitchVariable['endTIME'][]='2019-05-23 23:59';		// End 5 days after PS*/

?>