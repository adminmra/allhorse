    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0" title="Preakness Stakes Odds"  summary="The latest odds for the Preakness Stakes available for wagering now at BUSR.">
            <caption>2017 Preakness Stakes Odds</caption>
            <tbody>
            <!--
    <tr>
                    <th colspan="3" class="center">
                    Horses - 2017 Preakness Stakes  - May 20                    </th>
            </tr>
-->
            <!--
    <tr>
                    <th colspan="3" class="center">
                    All Wagers Have Action Whether Horse Runs Or Not                    </th>
            </tr>
-->
    <tr class='head_title'><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Always Dreaming</td><td>-175</td><td>4/7</td></tr><tr><td>Classic Empire</td><td>+250</td><td>5/2</td></tr><tr><td>Lookin At Lee</td><td>+900</td><td>9/1</td></tr><tr><td>Cloud Computing</td><td>+1000</td><td>10/1</td></tr><tr><td>Conquest Mo Money</td><td>+1200</td><td>12/1</td></tr><tr><td>Gunnevera</td><td>+1400</td><td>14/1</td></tr><tr><td>Hence</td><td>+1400</td><td>14/1</td></tr><tr><td>Multiplier</td><td>+2500</td><td>25/1</td></tr><tr><td>Senior Investment</td><td>+3000</td><td>30/1</td></tr><tr><td>Term Of Art</td><td>+3000</td><td>30/1</td></tr>            </tbody>
        </table>
    </div>
    