<?php
	
//This fies is where we update 	
$BasePath= "/home/ah/allhorse/public_html/";

$SmartyDynamicVar=array();

	function getFileContent($file){
		$FileContent='';
		if (file_exists($file)) {
		//$FileContent=file_get_contents($file);
		ob_start();
		include $file;
		$FileContent = ob_get_clean();
		}
				return $FileContent;
	}
	
//$SmartyDynamicVar['BONUS']=trim(getFileContent($BasePath.'bonus.php')); //what pages use this variable? -tf cleanup project
	$SmartyDynamicVar['BONUS-WELCOME']=trim(getFileContent($BasePath.'common/bonus-welcome.php'));
	$SmartyDynamicVar['BONUS-WELCOME-PERCENTAGE']=trim(getFileContent($BasePath.'common/bonus-welcome-percentage.php'));
	$SmartyDynamicVar['BONUS-RETENTION']=trim(getFileContent($BasePath.'common/bonus-retention.php'));
	$SmartyDynamicVar['BONUS-REBATES']=trim(getFileContent($BasePath.'common/bonus-rebates.php'));
	
	$SmartyDynamicVar['BONUS-WELCOME-GHB']=trim(getFileContent($BasePath.'gohorsebetting/promos/bonus-welcome.php'));
	$SmartyDynamicVar['BONUS-WELCOME-PERCENTAGE-GHB']=trim(getFileContent($BasePath.'gohorsebetting/promos/bonus-welcome-percentage.php'));
	$SmartyDynamicVar['BONUS-RETENTION-GHB']=trim(getFileContent($BasePath.'gohorsebetting/promos/bonus-retention.php'));
	$SmartyDynamicVar['BONUS-REBATES-GHB']=trim(getFileContent($BasePath.'gohorsebetting/promos/bonus-rebates.php'));
	
	$SmartyDynamicVar['RACETRACKS']=trim(getFileContent($BasePath.'common/racetracks.php'));
	
	$SmartyDynamicVar['USR-AWARDS-YEAR']=trim(getFileContent($BasePath.'usracing/usr_awards/year.php'));
	$SmartyDynamicVar['SAUDI-CUP-YEAR']=trim(getFileContent($BasePath.'saudi-cup/year.php'));
	$SmartyDynamicVar['SAUDI-CUP-YEARLAST']=trim(getFileContent($BasePath.'saudi-cup/yearlast.php'));
	$SmartyDynamicVar['SAUDI-CUP-DAY']=trim(getFileContent($BasePath.'saudi-cup/day.php'));
	$SmartyDynamicVar['SAUDI-CUP-DATE']=trim(getFileContent($BasePath.'saudi-cup/racedate.php'));
	$SmartyDynamicVar['SAUDI-CUP-RACEDATE']=trim(getFileContent($BasePath.'saudi-cup/racedate.php'));
	$SmartyDynamicVar['SAUDI-CUP-PURSE']=trim(getFileContent($BasePath.'saudi-cup/purse.php'));
	$SmartyDynamicVar['SAUDI-CUP-RUNNING']=trim(getFileContent($BasePath.'saudi-cup/running.php'));	

	$SmartyDynamicVar['KO-DATE']=trim(getFileContent($BasePath.'kd/racedate_ko.php'));	//OAKS
	$SmartyDynamicVar['KO-RACEDATE']=trim(getFileContent($BasePath.'kd/racedate_ko.php'));	//OAKS
	$SmartyDynamicVar['KO-PURSE']=trim(getFileContent($BasePath.'kd/ko_purse.php'));	//OAKS
	
	$SmartyDynamicVar['KD-YEAR']=trim(getFileContent($BasePath.'kd/year.php'));
	$SmartyDynamicVar['KD-YEARLAST']=trim(getFileContent($BasePath.'kd/yearlast.php'));
	$SmartyDynamicVar['KD-DAY']=trim(getFileContent($BasePath.'kd/day.php'));
	$SmartyDynamicVar['KD-DATE']=trim(getFileContent($BasePath.'kd/racedate.php'));
	$SmartyDynamicVar['KD-RACEDATE']=trim(getFileContent($BasePath.'kd/racedate.php'));
	$SmartyDynamicVar['KD-PURSE']=trim(getFileContent($BasePath.'kd/purse.php'));
	$SmartyDynamicVar['KD-RUNNING']=trim(getFileContent($BasePath.'kd/running.php'));
	
	
	$SmartyDynamicVar['PREAKNESS-YEAR']=trim(getFileContent($BasePath.'ps/year.php'));
	$SmartyDynamicVar['PREAKNESS-RACEDATE']=trim(getFileContent($BasePath.'ps/racedate.php'));
	$SmartyDynamicVar['PREAKNESS-DAY']=trim(getFileContent($BasePath.'ps/day.php'));
	$SmartyDynamicVar['PREAKNESS-DATE']=trim(getFileContent($BasePath.'ps/date.php'));
	$SmartyDynamicVar['PREAKNESS-PURSE']=trim(getFileContent($BasePath.'ps/purse.php'));
	$SmartyDynamicVar['PREAKNESS-RUNNNG']=trim(getFileContent($BasePath.'ps/running.php'));
	
	$SmartyDynamicVar['BELMONT-YEAR']=trim(getFileContent($BasePath.'belmont/year.php'));
	$SmartyDynamicVar['BELMONT-YEARLAST']=trim(getFileContent($BasePath.'belmont/yearlast.php'));	
	$SmartyDynamicVar['BELMONT-RACEDATE']=trim(getFileContent($BasePath.'belmont/racedate.php'));
	$SmartyDynamicVar['BELMONT-DAY']=trim(getFileContent($BasePath.'belmont/day.php'));
	$SmartyDynamicVar['BELMONT-DATE']=trim(getFileContent($BasePath.'belmont/day.php'));
	$SmartyDynamicVar['BELMONT-PURSE']=trim(getFileContent($BasePath.'belmont/purse.php'));
	$SmartyDynamicVar['BELMONT-RUNNING']=trim(getFileContent($BasePath.'belmont/running.php'));
	
	$SmartyDynamicVar['BC-YEAR']=trim(getFileContent($BasePath.'breeders/year.php'));
	$SmartyDynamicVar['BC-YEARLAST']=trim(getFileContent($BasePath.'breeders/year-last.php'));	
	$SmartyDynamicVar['BC-FRIDAY']=trim(getFileContent($BasePath.'breeders/day-friday.php'));
	$SmartyDynamicVar['BC-DAY']=trim(getFileContent($BasePath.'breeders/day.php'));
	$SmartyDynamicVar['BC-PURSE']=trim(getFileContent($BasePath.'breeders/purse.php'));	
	$SmartyDynamicVar['BC-PURSE-TOTAL']=trim(getFileContent($BasePath.'breeders/purse_total.php'));		
	$SmartyDynamicVar['BC-TRACK']=trim(getFileContent($BasePath.'breeders/track.php'));		
	
	$SmartyDynamicVar['DWC-YEAR']=trim(getFileContent($BasePath.'dubai/year.php'));
	$SmartyDynamicVar['DWC-YEARLAST']=trim(getFileContent($BasePath.'dubai/year_last.php'));	
	$SmartyDynamicVar['DWC-RACEDATE']=trim(getFileContent($BasePath.'dubai/racedate.php'));
	$SmartyDynamicVar['DWC-DAY']=trim(getFileContent($BasePath.'dubai/date.php'));
	
	$SmartyDynamicVar['PWC-YEAR']=trim(getFileContent($BasePath.'pegasus/year.php'));
	$SmartyDynamicVar['PWC-YEARLAST']=trim(getFileContent($BasePath.'pegasus/yearlast.php'));	
	$SmartyDynamicVar['PWC-RACEDATE']=trim(getFileContent($BasePath.'pegasus/racedate.php'));
	$SmartyDynamicVar['PWC-DAY']=trim(getFileContent($BasePath.'pegasus/date.php'));
	$SmartyDynamicVar['PWC-WINNER-HORSE']=trim(getFileContent($BasePath.'pegasus/winner_horse.php'));		
		

	//print_r($SmartyDynamicVar);
?>
