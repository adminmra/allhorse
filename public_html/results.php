<div class="table-responsive">
<table  class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="The results of the Belmont Stakes">
<caption>2018 Belmont Stakes Results</caption>
	<thead>
    <tr>
      <th>Time</th>
      <th>Results</th>
      <th>Post</th>
      <th>Horse</th>
      <th>Trainer</th>
      <th>Jockey</th>
      </tr>
    </thead>
      <tbody>
    <tr>
      <td>2:28:18</td>
      <td>1</td>
      <td>1</td>
      <td>Justify</td>
      <td>Bob Baffert</td>
      <td>Mike Smith</td>
      </tr>
    <tr>
      <td>&nbsp;</td>
      <td>2</td>
      <td>6</td>
      <td>Gronkowski</td>
      <td>Chad Brown</td>
      <td>Jose Ortiz</td>
      </tr>    <tr>
      <td>&nbsp;</td>
      <td>3</td>
      <td>4</td>
      <td>Hofburg</td>
      <td>Bill Mott</td>
      <td>Irad Ortiz Jr.</td>
      </tr>

    <tr>
      <td>&nbsp;</td>
      <td>4</td>
      <td>8</td>
      <td>Vino Rosso</td>
      <td>Todd Pletcher</td>
      <td>John Velazquez</td>
      </tr>
  </tbody>
</table>
</div>