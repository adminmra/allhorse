
<?php

if ( !defined( 'APP_PATH' ) ) {
	define( 'APP_PATH' , '/home/ah/allhorse/public_html/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

class HorseScraper extends Scraper{

	function __construct(){
		parent::__construct();
		$this->source = '';// content tree sources
	}

	public function scraping_to_database(){

		$homepage = file_get_contents( 'http://www.bloodhorse.com/horse-racing/thoroughbred-racing/leaders/leading-horses/2016' );

		preg_match("/<table[^>]*class=\"DataTable sortable\">(.*?)<\\/table>/si", $homepage, $match);

		$homepagetrainer = file_get_contents( 'http://www.bloodhorse.com/horse-racing/thoroughbred-racing/leaders/leading-trainers/2016' );

		preg_match("/<table[^>]*class=\"DataTable sortable\">(.*?)<\\/table>/si", $homepagetrainer, $matchtrainer);

		$homepagejockey = file_get_contents( 'http://www.bloodhorse.com/horse-racing/thoroughbred-racing/leaders/leading-jockeys/2016' );

		preg_match("/<table[^>]*class=\"DataTable sortable\">(.*?)<\\/table>/si", $homepagejockey, $matchjockey);
		//echo $match[1];
		//print_r($matchtrainer);

		$this->getdata( $match[0] );
		$this->getdatatrainer( $matchtrainer[0] );
		$this->getdatajockey( $matchjockey[0] );

		//include_once "../kbodd/cronkbodd.php";  /// hay un error con este archivo
		print( sprintf(
			$this->console_color->getColoredString( ">>> " , "cyan" ) . "%'.-160s%-s\n\n" ,
			$this->console_color->getColoredString( "Trainer | Jockey" , "red" ) ,
			'SQL'
		) );
	}

	public function scraping_to_xml(){}

	public function scraping_to_tpl(){}

	public static function run() {
		try {
			$instance = new self;
			$instance->scraping_to_database();

		} catch (Exception $e) {
			print_r( $e );
		}
	}

	private function getrow( $elements, $start ) {

		/*for($k=$start; $k<$start+9; $k++)
		{
		echo str_replace('Â','',strip_tags(trim($elements->item($k)->nodeValue)))."--";
		}*/
		//$datedata = explode(' ',str_replace('Â','',strip_tags(trim($elements->item($start)->nodeValue))));
		//echo $month=$datedata[0];
		$sql = "update top_leader set
		name = '".str_replace('Â','',strip_tags(trim($elements->item($start+1)->nodeValue)))."',
		starts = '".str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+2)->nodeValue))))."',
		first = '".str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+3)->nodeValue))))."',
		second = '".str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+4)->nodeValue))))."',
		third = '".str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+5)->nodeValue))))."',
		purse = '".str_replace('Â','',strip_tags(trim($elements->item($start+6)->nodeValue)))."',
		updatedas = '".date('Y-m-d')."'
		where position = '".str_replace('Â','',strip_tags(trim($elements->item($start)->nodeValue)))."' And type='1' ";

		DB::query( $sql );

	}

	private function getrowjockey( $elements, $start ) {

		$sql = "update top_leader set
		name = '".str_replace('Â','',strip_tags(trim($elements->item($start+1)->nodeValue)))."',
		starts = '".str_replace(',','',str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+2)->nodeValue)))))."',
		first = '".str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+3)->nodeValue))))."',
		second = '".str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+4)->nodeValue))))."',
		third = '".str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+5)->nodeValue))))."',
		purse = '".str_replace('Â','',strip_tags(trim($elements->item($start+6)->nodeValue)))."',
		updatedas = '".date('Y-m-d')."'
		where position = '".str_replace('Â','',strip_tags(trim($elements->item($start)->nodeValue)))."' And type='2' ";

		DB::query( $sql );

	}

	private function getdatajockey($contents){
		//$contents = $data3;
		//file_get_contents('internatdata.htm');
		//create a DOM based off of the string from the html table
		$DOM = new DOMDocument;
		$DOM->loadHTML($contents);

		//get all tr and td
		$items = $DOM->getElementsByTagName('tr');
		$tds   = $DOM->getElementsByTagName('td');

		$start=0; //for ($i = 0; $i < $items->length; $i++){
		for ( $i = 0; $i <= 9; $i++ ){
			$this->getrowjockey( $tds, $start );
			//echo "<br />";
			$start = $start + 9;
		}

	}

	private function getrowtrainer( $elements, $start ) {

		$sql = "update top_leader set
		name = '".str_replace('Â','',strip_tags(trim($elements->item($start+1)->nodeValue)))."',
		starts = '".str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+2)->nodeValue))))."',
		first = '".str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+3)->nodeValue))))."',
		second = '".str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+4)->nodeValue))))."',
		third = '".str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+5)->nodeValue))))."',
		purse = '".str_replace('Â','',strip_tags(trim($elements->item($start+6)->nodeValue)))."',
		updatedas = '".date('Y-m-d')."'
		where position = '".str_replace('Â','',strip_tags(trim($elements->item($start)->nodeValue)))."' And type='3' ";

		DB::query( $sql );

	}

	private function getdatatrainer( $contents ){
		//$contents = $data3;
		//file_get_contents('internatdata.htm');
		//create a DOM based off of the string from the html table
		$DOM = new DOMDocument;
		$DOM->loadHTML($contents);

		//get all tr and td
		$items = $DOM->getElementsByTagName('tr');
		$tds   = $DOM->getElementsByTagName('td');

		$start = 0; //for ($i = 0; $i < $items->length; $i++){
		for ($i = 0; $i <= 9; $i++){
			$this->getrowtrainer( $tds, $start );
			//echo "<br />";
			$start = $start + 9;
		}

	}

	private function tdrows( $elements ){
		$str = "";
		for ( $ii =0; $ii < $elements->length; $ii++ ){
			$str .= $elements->item( $ii )->nodeValue . "-";
		}
		return $str;
	}

	private function getdata( $contents ){
		//$contents = $data3;
		//file_get_contents('internatdata.htm');
		//create a DOM based off of the string from the html table
		$DOM = new DOMDocument;
		$DOM->loadHTML( $contents );

		//get all tr and td
		$items = $DOM->getElementsByTagName('tr');
		$tds   = $DOM->getElementsByTagName('td');

		/*echo $items->length;
		echo $tds->length;
		echo $tds->item(0)->nodeValue;
		echo "<pre>";
		print_r($tds);
		echo "</pre>";
		echo "<br />";*/

		$start = 0; //for ($i = 0; $i < $items->length; $i++){
		for ( $i = 0; $i <= 29; $i++ ){
			$this->getrow( $tds, $start );
			//echo "<br />";
			$start = $start+9;
		}
	}

}

HorseScraper::run();

?>
