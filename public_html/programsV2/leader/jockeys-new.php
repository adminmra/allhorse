<?php

if ( !defined( 'APP_PATH' ) ) {
	define( 'APP_PATH' , '/home/ah/scrappers/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

class HorseJockey extends Scraper{
	private $ALWAYSWRITE;
	private $OUTPUTFILEPATH;
	private $IdLeague;
	private $data;
	private $mainTitle;

	function __construct(){
		parent::__construct();
		$this->source         = "https://www.bloodhorse.com/horse-racing/thoroughbred-racing/leaders/jockeys/2018";
		$this->ALWAYSWRITE    = FALSE; //always write eveng with the message "please check back shortly"
		$this->OUTPUTFILEPATH = "/home/ah/allhorse/public_html/generated/leaders/jockeys_new_v1.php"; //the absolute path of the file to be written
		$this->IdLeague       = false;
		$this->data           = array();
		$this->mainTitle      = "";

	}
	
	
	public function php_get( $url )
	{
		$contents = '';
		ini_set( 'user_agent', 'User-Agent: FPLinkChecker/1.2' );
		if( ( $fp = fopen( $url, 'r' ) ) )
		{
			for( ;($data = fread( $fp, 1024 ) ); )
				$contents .= $data;
	
			fclose( $fp );
		}
		elseif( function_exists( 'curl_init' ) )
		{
			$ch = curl_init( );
	
			curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_HEADER, 0 );
			curl_setopt( $ch, CURLOPT_USERAGENT, 'User-Agent: FPLinkChecker/1.2' );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	
			$contents .= curl_exec( $ch );
	
			curl_close( $ch );
		}
		elseif( ( $url_info = parse_url( $url ) ) )
		{
			if( $url_info['scheme'] == 'https' )
				$fp = fsockopen( 'ssl://' . $url_info['host'], 443, $errno, $errstr, 30);
			else
				$fp = fsockopen( $url_info['host'], 80, $errno, $errstr, 30);
	
			if( !$fp )
				return false;
	
			$out = 'GET ' . (isset($url_info['path'])? $url_info['path']: '/') .
				(isset($url_info['query'])? '?' . $url_info['query']: '') .
				" HTTP/1.0\r\n";
			$out .= "Host: {$url_info['host']}\r\n";
			$out .= "User-Agent: FPLinkChecker/1.2\r\n";
			$out .= "Connection: Close\r\n\r\n";
	
			fwrite($fp, $out);
			for( ;!feof( $fp ); )
				$contents .= fgets($fp, 128);
	
			list($headers, $content) = explode( "\r\n\r\n", $contents, 2 );
	
			return $content;
		}
		else
			return false;
	
		return $contents;
	}
	
	public function scraping_to_database(){}

	public function scraping_to_xml(){}

	public function scraping_to_tpl(){
		$html = '';
		//$source = file_get_contents($this->source);
		$source = $this->php_get($this->source);
		if( !empty($source) ){
			preg_match("/<table[^>]*class=\"sortable\">(.*?)<\\/table>/si", $source, $match);

			if( !empty($match) ){
				$results = $this->getdata( $match[0] );

				ob_start();?>

				<table id="infoEntries" class="data table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="0" >
					<tbody>
						<tr>
							<th width="5%" align="center"><strong>#</strong></th>
							<th width="35%"><strong>Name</strong></th>
							<th width="10%"><strong>Starts</strong></th>
							<th width="10%"><strong>1sts</strong></th>
							<th width="10%"><strong>2nds</strong></th>
							<th width="10%"><strong>3rds</strong></th>
							<th width="20%"><strong>Purses</strong></th>
						</tr>
						<?php
						$counter=0;
						foreach($results as $e){
							if(empty($e['name']))
								continue;

							if($counter%2 == 1) {
								$tr_class = 'odd';
							} else {
								$tr_class = '';
							} ?>

							<tr class='<?php echo $tr_class; ?>'>
								<td class="num"><?php echo $e['rank']; ?>.</td>
								<td><?php echo $e['name']; ?></td>
								<td><?php echo $e['starts']; ?></td>
								<td><?php echo $e['first']; ?></td>
								<td><?php echo $e['second']; ?></td>
								<td><?php echo $e['third']; ?></td>
								<td><?php echo $e['purse']; ?></td>
							</tr>
							<?php
							$counter++;
						}

						?>
					</tbody>
				</table>
				<p class='news-date' style='text-align: right;'><em class='updateemp'>(Through <?php echo date("F jS\, Y H:i:s"); ?>)</em></p>
				<?php
				$html = ob_get_clean();
			}
		}

		$this->writeOutputXP($html);
	}

	//new from August 11, 2017
	private function writeOutputXP($contents){
		$updated = "(Through " . date("F jS\, Y H:i:s") . ")";

		if( empty($contents) ){
			$default_contents = "The odds are currently being updated, please check back shortly. <em class='updateemp'>{$updated}</em>";

			if( $this->ALWAYSWRITE || !file_exists($this->OUTPUTFILEPATH) ){
				$final_contents = $default_contents;
			} else {
				$contents = preg_replace("/(<em\sclass=\'updateemp\'>)(.*?)(<\/em>)/", "$1".$updated."$3", file_get_contents($this->OUTPUTFILEPATH) );
				if($contents !== NULL){
					$final_contents = $contents;
				} else {
					$final_contents = $default_contents;
				}
			}
		} else{
			$final_contents = $contents;
		}
		
		$this->as_file_save_data( $final_contents , $this->OUTPUTFILEPATH );
	}

	private function getdata( $contents ){
		//create a DOM based off of the string from the html table
		$DOM = new DOMDocument;
		$DOM->loadHTML( $contents );

		//get all tr and td
		$items = $DOM->getElementsByTagName('tr');
		$tds   = $DOM->getElementsByTagName('td');

		$result = array();

		$start = 0;
		//for ($i = 0; $i < $items->length; $i++){
		for ( $i = 0; $i <= 29; $i++ ){
			$row = $this->getrow( $tds, $start );
			$start = $start+9;

			array_push($result, $row);
		}
		return $result;
	}


	private function getrow( $elements, $start ) {
		$row = array();
		$row['rank']    = strip_tags(trim($elements->item($start)->nodeValue));
		$row['name']    = str_replace('Â','',strip_tags(trim($elements->item($start+1)->nodeValue)));
		$row['starts']  = str_replace(',','',str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+2)->nodeValue)))));
		$row['first']   = str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+3)->nodeValue))));
		$row['second']  = str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+4)->nodeValue))));
		$row['third']   = str_replace('-','0',str_replace('Â','',strip_tags(trim($elements->item($start+5)->nodeValue))));
		$row['purse']   = str_replace('Â','',strip_tags(trim($elements->item($start+6)->nodeValue)));
		return $row;
	}


	public static function run() {
		try {
			$instance = new self;
			$instance->scraping_to_tpl();
		} catch (Exception $e) {
			print( $e->getMessage() );
		}
	}

}

HorseJockey::run();
