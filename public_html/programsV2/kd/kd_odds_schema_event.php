<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
if ( !defined( 'APP_PATH' ) ) {
    define( 'APP_PATH' , '/home/ah/scrappers/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

class KDOdds_Schema extends Scraper{

    private $ALWAYSWRITE;
    private $OUTPUTFILEPATH;
    private $OUTPUT_FILE_XML;
    private $content_xml;
    private $IdLeague;
    private $data;
    private $mainTitle;

    function __construct(){
        parent::__construct();
        $this->source          = "http://ww1.betusracing.ag/odds.xml";
        $this->ALWAYSWRITE     = FALSE; //always write eveng with the message "please check back shortly"
        $this->OUTPUTFILEPATH  = "/home/ah/allhorse/public_html/kd/odds_kd_schema_event.php"; //the absolute path of the file to be written
       // $this->OUTPUT_FILE_XML = "/home/ah/allhorse/public_html/kd/odds_kd.xml";
        $this->IdLeague        = "998";
        $this->data            = array();
        $this->mainTitle       = "";
        $this->content_xml     = "";

        ////  [\<]league\s+[\d\"\w\s\=\+\-\>\)\<\/\:\(]+[</]league[\>]
        // regular exprecion to scrap

    }

    public function scraping_to_database(){}
	public function scraping_to_tpl(){}
	public function scraping_to_xml(){}

    public static function run() {
        try {
            $instance = new self;
            $instance->process();

        } catch ( Exception $e ) {
            print( $e->getMessage() );
        }
    }

    public function process(){
        $this->parseXmlStructureXP();
        $this->generateOutputXP();
    }

    private function parseXmlStructureXP(){

        $ret = false;
        $this->mainTitle = "";
        $this->data = array();
        $xml_string = file_get_contents( $this->source );
        $matches = array();
        preg_match('/<league IdLeague="'.$this->IdLeague.'"[^>]*>(.*?)<\/league>/', $xml_string, $matches);
        if(count($matches)<=0)return $ret;

        $league = $matches[1];
        $leagueObj = simplexml_load_string($matches[0]);
        $this->mainTitle = ucwords(strtolower((string) $leagueObj["Description"]));

        $sections = preg_split("/<banner[^>]*>/", $league); // split by sections
        if(count($sections)<=0) return $ret;

        $banners = array();
        preg_match_all("/<banner[^>]*>/", $league, $banners); // for getting the banners
        if(count($banners)<=0) return $ret;
        $banners = $banners[0];

        for($i=0; $i< count($banners); $i++) {
            $bannerParams =  simplexml_load_string($banners[$i]);
            $section = $sections[$i+1];
            $games = array();

            if($section != ""){
                $g = array();
                preg_match_all("/<game[^>]*>.*?<\/game>/", $section, $g);

                foreach($g[0] as $gamesPlain){
                    $linesPlain = array();
                    preg_match_all("/<line[^>]*>/", $gamesPlain, $linesPlain);
                    $lines = array();

                    foreach ($linesPlain[0] as $line) {
                        $lineParams = simplexml_load_string($line);
                        $lines[] = array(
                            "tmname" => ucwords(strtolower((string)$lineParams[0]["tmname"])),
                            "odds" => (string)$lineParams[0]["odds"],
                            "oddsh" => (string)$lineParams[0]["oddsh"]
                            );
                    }

                    $gameObj = simplexml_load_string("<document>".$gamesPlain."</document>");
                    $games[] = array(
                    "vtm" => ucwords(strtolower((string)$gameObj->game["vtm"])),
                    "htm" => ucwords(strtolower((string)$gameObj->game["htm"])),
                    "lines" => $lines
                    );
                }
            }

            $this->data[] = array(
                "ab" => (string) $bannerParams[0]["ab"],
                "vtm" => ucwords(strtolower((string) $bannerParams[0]["vtm"])),
                "htm" => ucwords(strtolower((string) $bannerParams[0]["htm"])),
                "games" => $games
                );
        }

        return true;
    }

    private function fractionalCalculationXP($oddsh){
        $oodsh_val = intval($oddsh);
        $fractional = 0;

        if(intval($oodsh_val)>0){
            $fractional  = $this->float2ratXP($oodsh_val/100);
        } else if($oodsh_val<0) {
            $fractional  = $this->float2ratXP(100/(-$oodsh_val));
        } else {
            $fractional  = $oddsh;
        }

        return $fractional;
    }

    private function float2ratXP($n, $tolerance = 1.e-6) {
        $h1=1; $h2=0;
        $k1=0; $k2=1;
        $b = 1/$n;
        do {
            $b = 1/$b;
            $a = floor($b);
            $aux = $h1; $h1 = $a*$h1+$h2; $h2 = $aux;
            $aux = $k1; $k1 = $a*$k1+$k2; $k2 = $aux;
            $b = $b-$a;
        } while (abs($n-$h1/$k1) > $n*$tolerance);

        return "$h1/$k1";
    }

    private function writeOutputXP($contents){
//echo $contents;
        $update = " - Updated " . date("F j, Y H:i:s");
        if( $contents == "" ){
            $contents = "The odds are currently being updated, please check back shortly. <em id='updateemp'> $update. </em>";
            if($this->ALWAYSWRITE || !file_exists($this->OUTPUTFILEPATH) ){
                $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
            }
            else{
                $contents = preg_replace("/(<em\sid=\'updateemp\'>)(.*?)(<\/em>)/", "$1 ".$update." $3", file_get_contents($this->OUTPUTFILEPATH) );
                if($contents !== NULL){
                    $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
                }

                return false;
            }
        }
        else{
            $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
        }

    }

    private function generateOutputXP(){

        $output = "";
        if( count( $this->data ) <= 0 ) return $this->writeOutputXP( $output );
        $firstHeader = false;
       // ob_start();
 
    
   $output .= '{literal} <script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type": "SportsEvent",
    "name": "2019 Kentucky Derby",
  "startDate": "2019-05-04T15:30",
  "endDate": "2019-05-04T18:30",
  "location": {
    "@type": "EventVenue",
    "name": "Churchill Downs",
    "telephone": "(502) 636-4400",
	"address": "700 Central Ave, Louisville, KY 40208",
    "geo": {
    "@type": "GeoCoordinates",
    "latitude": "38.19",
    "longitude": "85.45"
    }
  	}, 
    "image": "https://www.usracing.com//img/dubai-world-cup/2017-dubai-world-cup-betting-usracing.jpg",
    "description": "Official 2019 Kentucky Derby Odds and Futures from US Racing.  Bet on the Kentucky Derby on the first Saturday in May at Churchill Downs in Louisville, Kentucky.",
    "competitor": [ ';

        foreach ($this->data as $d) {

            if(count($d["games"])>0){
                foreach($d["games"] as $g){
					$numRows=count($g["lines"]);
                    if(count($g["lines"])>0){
                        if($g["htm"]!="")
                            // echo '<tr><th colspan="3" class="center">'.$g["htm"].'</th></tr>';
                        if(!$firstHeader){
                           // echo "<tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr>";
                            $firstHeader = true;
                        }
						$counterRow=1;
                        foreach($g["lines"] as $l){
                            if ( $l['oddsh'] != '' ) {
                                if($l["tmname"]!=""){$team = $l["tmname"];}
                                else{ $team = $g["vtm"] . " " . $g["htm"]; }
								
								$output .= ' {
										"@type": "SportsTeam",
										"name": "'. $team .'",
										"description": "'. $team .' Odds Offer ('.$this->fractionalCalculationXP($l['oddsh']).') ",    
										"makesOffer": {
										"@type": "Offer",
										"name":"Odds",
										"price": "'.$l['oddsh'].'"  
										}
										}';if($counterRow != $numRows){ $output .= ','; }
									                                
                            }
							$counterRow++;
                        }
                    }
                }
            }
        }
    
    $output .= '  ]
    }
    </script> {/literal}';

//echo $output;
        //$output = ob_get_clean();
       // $this->content_xml = $output;
        //$this->scraping_to_xml(); // XML FILE GENERATIC
        return $this->writeOutputXP($output);

    }

}

KDOdds_Schema::run();

?>
