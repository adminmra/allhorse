	<div class="table-responsive">
		<table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Margin of Victory"  summary="The latest odds for the Kentucky Derby available for wagering now at BUSR.">
			<caption>2018 Kentucky Derby Matchups</caption>
			<tbody>
				<tr>
					<th colspan="3" class="center">
					2018 Kentucky Derby Matchups  - Apr 23					</th>
			</tr>
	<tr><th colspan="3" class="center">Mendelssohn</th></tr><tr class='head_title'><th>Team</th><th>ML</th></tr><tr><th colspan="3" class="center">Audible</th></tr><tr><th colspan="3" class="center">Bolt D'oro</th></tr><tr><th colspan="3" class="center">Good Magic</th></tr><tr><th colspan="3" class="center">Magnum Moon</th></tr><tr><th colspan="3" class="center">Magnum Moon</th></tr><tr><th colspan="3" class="center">Bolt D'oro</th></tr><tr><th colspan="3" class="center">Magnum Moon</th></tr><tr><th colspan="3" class="center">Audible</th></tr><tr><th colspan="3" class="center">Solomini</th></tr><tr><th colspan="3" class="center">Gronkowski</th></tr><tr><th colspan="3" class="center">Firenze Fire</th></tr><tr><th colspan="3" class="center">Bravazo</th></tr>					<tr>
						<td class="dateUpdated center" colspan="5">
							<em id='updateemp'>Updated April 18, 2018 17:32:53.</em>
							BUSR - Official <a href="https://www.usracing.com/kentucky-derby/odds">Kentucky Derby Odds</a>. 

<!-- br />
							All odds are fixed odds prices. -->
						</td>
					</tr>
			</tbody>
		</table>
	</div>
	