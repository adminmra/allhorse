<?php 
ini_set('error_reporting', E_ALL);
ini_set("display_errors", 1); 	

if ( !defined( 'APP_PATH' ) ) {
    define( 'APP_PATH' , realpath( dirname( __FILE__ ) . '/../' ) . DIRECTORY_SEPARATOR );
}

require_once APP_PATH . 'Scraper.php';

class contenders extends Scraper{

    private $ALWAYSWRITE, $OUTPUTFILEPATH, $IdLeague, $data, $mainTitle;

    function __construct(){
        parent::__construct();
        //$this->source         = 'http://www.bloodhorse.com/horse-racing/triple-crown/road-to-the-kentucky-derby';
		//$this->source         = 'https://classic.drf.com/news/kentucky-derby-2019-point-standings';
		$this->source         = '/home/ah/allhorse/public_html/programsV2/kd/contenders.html';
        $this->ALWAYSWRITE    = FALSE; //always write eveng with the message "please check back shortly"
        $this->OUTPUTFILEPATH = "/home/ah/allhorse/public_html/kd/contenders.2019.php"; //the absolute path of the file to be written
        $this->IdLeague       = false;
        $this->data           = array();
        $this->mainTitle      = "";
    }

    public function scraping_to_database(){}
    public function scraping_to_xml(){}

    public function scraping_to_tpl(){
        $html_dom = file_get_html( $this->source );

        $html_dom = $html_dom->find( 'div[class="content"] table[class="ckeditor-styled-table"]' , 0 );

        $data = array(
            'thead_row' => array(),
            'tbody_rows' => array()
        );

       /* $th = $html_dom->find( "thead > tr > th[!width]" );

        if ( count( $th ) < 1 ) { return ; }

        $data['thead_row'][0] = trim( $th[0]->plaintext );//Rank        
        $data['thead_row'][1] = trim( $th[1]->plaintext );//Horse
        $data['thead_row'][2] = trim( $th[2]->plaintext );//Trainer
        $data['thead_row'][3] = trim( $th[3]->plaintext ); //Points                
        $data['thead_row'][4] = "Earnings"; // trim( str_replace( "Career" , '' , $th[4]->plaintext ) );//Trainer */

        foreach ( $html_dom->find( "tbody > tr" ) as $tr ) {
            if ( ! $tr->find( 'td' ) ) { continue; }
            $td = $tr->find( 'td[!class]' );
            $row = array();
            $row[0] = trim( $td[0]->innertext );
            $row[1] = trim( $td[1]->plaintext );            
            $row[2] = trim( $td[2]->plaintext );            
            $row[3] = trim( $td[3]->plaintext );            
            $row[4] = trim( $td[4]->plaintext );            
            array_push( $data['tbody_rows'] , $row );
        }


ob_start();?>
<h2>Kentucky Derby Horses</h2>
 <div id="no-more-tables">
    <table class="data table table-condensed table-striped table-bordered ordenableResult" width="100%" cellpadding="0" cellspacing="0" border="0" summary="Kentucky Derby Contenders">
        <thead>
            <tr>
            <?php
                foreach ( $data['thead_row'] as $th ) {
                    echo "<th>{$th}</th>";
                }
             ?>
            </tr>
        </thead>
        <tbody>
            <?php
			
            foreach ( $data['tbody_rows'] as $tr ) {
				$i=0;
                echo '<tr>';
                foreach ( $tr as $td ) {
                    echo '<td data-title="'.$data['thead_row'][$i].'">'.$td.'</td>';
					$i++;
                }
				
                echo "</tr>";
            }
            ?>
            </tbody>
    </table>
</div>
	<div>
        <table id='infoEntries' width='100%' border='0' cellspacing='0' cellpadding='0' class='table table-condensed table-striped table-bordered' >
        <tbody>
        <tr>
            <td class="dateUpdated center" colspan="4"><em>Updated March 18, 2019</em><br />
<em>+ = Not Triple Crown Nominated</em>
            </td>
        </tr>
        </tbody>
        </table>
        </div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_leader.js"></script>
{/literal}
<?php
        $output = ob_get_clean();
        $this->as_file_save_data( $output , $this->OUTPUTFILEPATH );
    }
	
	function as_file_save_data($data, $file){

	//$file = '/home/desite12mo/public_html/huestech.com/cron/racexml.xml';

  if (!$fp = fopen($file, 'wb')) {
    		return "Could not write to ".$file;
  	}
  	
	fwrite($fp, $data);
  	fclose($fp); 
 
	}
	
	

    public static function run() {
        try {
            $instance = new self;
            $instance->process();
        } catch ( Exception $e ) { print( $e->getMessage() ); }
    }

    public function process(){
        $this->scraping_to_tpl();
    }
}
contenders::run();

?>
