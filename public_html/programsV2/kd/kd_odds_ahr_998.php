<?php

if ( !defined( 'APP_PATH' ) ) {
    define( 'APP_PATH' , '/home/ah/scrappers/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

class KDOddsAHR extends Scraper{

    private $ALWAYSWRITE;
    private $OUTPUTFILEPATH;
    private $OUTPUT_FILE_XML;
    private $content_xml;
    private $IdLeague;
    private $data;
    private $mainTitle;

    function __construct(){
        parent::__construct();
        $this->source          = "http://ww1.betusracing.ag/odds.xml";
        $this->ALWAYSWRITE     = FALSE; //always write eveng with the message "please check back shortly"
        $this->OUTPUTFILEPATH  = "/home/ah/allhorse/public_html/kd/odds_kd_ahr.php"; //the absolute path of the file to be written
        $this->OUTPUT_FILE_XML = "";
        $this->IdLeague        = "998";
        $this->data            = array();
        $this->mainTitle       = "";
        $this->content_xml     = "";

        ////  [\<]league\s+[\d\"\w\s\=\+\-\>\)\<\/\:\(]+[</]league[\>]
        // regular exprecion to scrap

    }

    public function scraping_to_database(){}

    public function scraping_to_xml(){
        $date = date( 'D, d M Y H:i:s +0000' );
        $output = <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="en-ca">
     <title>US Racing | Online Horse Betting</title>
     <author>
         <name>US Racing</name>
         <uri>http://ww1.betusracing.ag/odds.xml</uri>
     </author>
     <updated>{$date}</updated>
     <id>{$date}</id>
     <logo>https://www.usracing.com/img/usracing-sm.png</logo>
     <icon>https://www.usracing.com/themes/favicon.ico</icon>
     <rights>Copyright 2016, US Racing | Online Horse Betting</rights>
     <entry>
         <title>Horses - 2016 Kentucky Derby - May 01 </title>
         <link target="_blank" type="text/html" href="https://www.usracing.com/kentucky-derby/odds"/>
         <updated>{$date}</updated>
         <published>{$date}</published>
         <category term="Sports"/>
         <summary type="html">
            <![CDATA[{$this->content_xml}]]>
         </summary>
         <id>tag:{$date}</id>
     </entry>
 </feed>
EOT;
        $this->as_file_save_data( $output , $this->OUTPUT_FILE_XML );
    }

    public function scraping_to_tpl(){}

    public static function run() {
        try {
            $instance = new self;
            $instance->process();

        } catch ( Exception $e ) {
            print( $e->getMessage() );
        }
    }

    public function process(){
        $this->parseXmlStructureXP();
        $this->generateOutputXP();
    }

    private function parseXmlStructureXP(){

        $ret = false;
        $this->mainTitle = "";
        $this->data = array();
        $xml_string = file_get_contents( $this->source );
        $matches = array();
        preg_match('/<league IdLeague="'.$this->IdLeague.'"[^>]*>(.*?)<\/league>/', $xml_string, $matches);
        if(count($matches)<=0)return $ret;

        $league = $matches[1];
        $leagueObj = simplexml_load_string($matches[0]);
        $this->mainTitle = ucwords(strtolower((string) $leagueObj["Description"]));

        $sections = preg_split("/<banner[^>]*>/", $league); // split by sections
        if(count($sections)<=0) return $ret;

        $banners = array();
        preg_match_all("/<banner[^>]*>/", $league, $banners); // for getting the banners
        if(count($banners)<=0) return $ret;
        $banners = $banners[0];

        for($i=0; $i< count($banners); $i++) {
            $bannerParams =  simplexml_load_string($banners[$i]);
            $section = $sections[$i+1];
            $games = array();

            if($section != ""){
                $g = array();
                preg_match_all("/<game[^>]*>.*?<\/game>/", $section, $g);

                foreach($g[0] as $gamesPlain){
                    $linesPlain = array();
                    preg_match_all("/<line[^>]*>/", $gamesPlain, $linesPlain);
                    $lines = array();

                    foreach ($linesPlain[0] as $line) {
                        $lineParams = simplexml_load_string($line);
                        $lines[] = array(
                            "tmname" => ucwords(strtolower((string)$lineParams[0]["tmname"])),
                            "odds" => (string)$lineParams[0]["odds"],
                            "oddsh" => (string)$lineParams[0]["oddsh"]
                            );
                    }

                    $gameObj = simplexml_load_string("<document>".$gamesPlain."</document>");
                    $games[] = array(
                    "vtm" => ucwords(strtolower((string)$gameObj->game["vtm"])),
                    "htm" => ucwords(strtolower((string)$gameObj->game["htm"])),
                    "lines" => $lines
                    );
                }
            }

            $this->data[] = array(
                "ab" => (string) $bannerParams[0]["ab"],
                "vtm" => ucwords(strtolower((string) $bannerParams[0]["vtm"])),
                "htm" => ucwords(strtolower((string) $bannerParams[0]["htm"])),
                "games" => $games
                );
        }

        return true;
    }

    private function fractionalCalculationXP($oddsh){
        $oodsh_val = intval($oddsh);
        $fractional = 0;

        if(intval($oodsh_val)>0){
            $fractional  = $this->float2ratXP($oodsh_val/100);
        } else if($oodsh_val<0) {
            $fractional  = $this->float2ratXP(100/(-$oodsh_val));
        } else {
            $fractional  = $oddsh;
        }

        return $fractional;
    }

    private function float2ratXP($n, $tolerance = 1.e-6) {
        $h1=1; $h2=0;
        $k1=0; $k2=1;
        $b = 1/$n;
        do {
            $b = 1/$b;
            $a = floor($b);
            $aux = $h1; $h1 = $a*$h1+$h2; $h2 = $aux;
            $aux = $k1; $k1 = $a*$k1+$k2; $k2 = $aux;
            $b = $b-$a;
        } while (abs($n-$h1/$k1) > $n*$tolerance);

        return "$h1/$k1";
    }

    private function writeOutputXP($contents){

        $update = " - Updated " . date("F j, Y H:i:s");
        if( $contents == "" ){
            $contents = "The odds are currently being updated, please check back shortly. <em id='updateemp'> $update. </em>";
            if($this->ALWAYSWRITE || !file_exists($this->OUTPUTFILEPATH) ){
                $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
            }
            else{
                $contents = preg_replace("/(<em\sid=\'updateemp\'>)(.*?)(<\/em>)/", "$1 ".$update." $3", file_get_contents($this->OUTPUTFILEPATH) );
                if($contents !== NULL){
                    $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
                }

                return false;
            }
        }
        else{
            $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
        }

    }

    private function generateOutputXP(){

        $output = "";
        if( count( $this->data ) <= 0 ) return $this->writeOutputXP( $output );
        $firstHeader = false;
		$ODDSAR=array();
        foreach ($this->data as $d) {

            if(count($d["games"])>0){
                foreach($d["games"] as $g){
                    if(count($g["lines"])>0){
                        if($g["htm"]!="")

                        foreach($g["lines"] as $l){
                            if ( $l['oddsh'] != '' ) {
                                if($l["tmname"]!="")$team = $l["tmname"];
                                else $team = $g["vtm"] . " " . $g["htm"];
									$ODDSAR[$team]=$l['oddsh'];
                            }
                        }
                    }
                }
            }
        }
		asort($ODDSAR);
		
		ob_start();

		echo '<div class="leaderstable">';
		$counter =0;
		echo '<div class="oddscell">';
		foreach($ODDSAR as $x=>$x_value)
		   {
		    echo $x ;
		   echo "<br>";
		   		$counter ++;
				if($counter == '5') break;
		   }
		echo '</div>';
		$counter =0;
		echo '<div class="oddscell">';
		foreach($ODDSAR as $x=>$x_value)
		   {
		    echo $this->fractionalCalculationXP($x_value);
		   echo "<br>";
		   		$counter ++;
				if($counter == '5') break;
		   }
		echo '</div>';
		$counter =0;
		echo '<div class="oddscell">';
		foreach($ODDSAR as $x=>$x_value)
		   {
			   if($counter >= 5){
		    echo $x ;
		   echo "<br>";
			   }
		   		$counter ++;
				if($counter == '10') break;
		   }
		echo '</div>';
		$counter =0;
		echo '<div class="oddscell">';
		foreach($ODDSAR as $x=>$x_value)
		   {
			    if($counter >= 5){
		    echo $this->fractionalCalculationXP($x_value);
		   echo "<br>";
				}
		   		$counter ++;
				if($counter == '10') break;
		   }
		echo '</div>';
		echo '</div>';		
        $output = ob_get_clean();
        return $this->writeOutputXP($output);

    }

}

KDOddsAHR::run();

?>
