<?php
ini_set('error_reporting', E_ALL);
ini_set("display_errors", 1);
set_time_limit(0);

if (!defined( 'APP_PATH' )) {
    define('APP_PATH' , '/home/ah/allhorse/public_html/programsV2/');
}

require_once APP_PATH . 'Scraper.php';
require_once APP_PATH . '/simple_html_dom.php';

class BreedersChallangeScraper extends Scraper{
    
    function __construct() {
        parent::__construct();
        $this->source = 'https://members.breederscup.com/challenge/schedule.aspx';
        $this->OUTPUTFILEPATH = "/home/ah/allhorse/public_html/breeders/challenge.php";
    }
    
    private $racetracks_with_details = array(
        'ASC'=>false,
        'CUR'=>false,
        'GOO'=>false,
        'GVA'=>true,
        'LEO'=>false,
        'NEW'=>false,
        'RAN'=>false,
        'SIS'=>false,
        'TOK'=>false,
        'YOR'=>false,
        'AP'=>true,
        'Bel'=>true,
        'CD'=>true,
        'CHS'=>false,
        'Dea'=>false,
        'Dmr'=>true,
        'GP'=>true,
        'HSN'=>false,
        'Kee'=>true,
        'KEN'=>false,
        'Lch'=>false,
        'MON'=>false,
        'Mth'=>true,
        'NKR'=>false,
        'Pal'=>false,
        'SA'=>true,
        'Sar'=>true,
        'WO'=>true);
    
    private  $available_racetracks = array('ASC'=>'ascot',
                                            'CUR'=>'the-curragh',
                                            'GOO'=>'goodwood',
                                            'GVA'=>'jockey-club-brasileiro',
                                            'LEO'=>'leopardstown',
                                            'NEW'=>'newmarket',
                                            'RAN'=>'royal-randwick',
                                            'SIS'=>'hipodromosanisidro',
                                            'TOK'=>'tokyo-jump-stakes',
                                            'YOR'=>'york',
                                            'AP'=>'arlington-park',
                                            'Bel'=>'belmont-park',
                                            'CD'=>'kentucky-downs',
                                            'CHS'=>'club-hipico-de-santiago',
                                            'Dea'=>'Hippodrome de Deauville � Clairefontaine',
                                            'Dmr'=>'del-mar',
                                            'GP' =>'gulfstream-park',
                                            'HSN'=>'Hanshin Racecourse',
                                            'Kee'=>'keeneland',
                                            'KEN'=>'LeKen',
                                            'Lch'=>'?? dont know the name ',
                                            'MON'=>'?? dont know the name',
                                            'Mth'=>'monmouth-park',
                                            'NKR'=>'nakayama',
                                            'Pal'=>'?? dont know the name',
                                            'SA'=>'santa-anita',
                                            'Sar'=>'saratoga ',
                                            'WO'=>'woodbine');
    
    public function scraping_to_database(){}
    
    public function scraping_to_databaseOLDVersion(){}
    
    public function scraping_to_xml(){}
    
    public function scraping_to_tpl(){
        $html = file_get_html( $this->source );
        $queriesArray = array();
        $toReplace = array();
        $skipHead = TRUE;
        $printData="";
        $printData .='<div id="no-more-tables">';
        $printData .='<table id="gradedStakes" class="data table table-condensed table-striped table-bordered ordenable"  cellpadding="0" cellspacing="0" border="0"  >';
        $printData .='<thead><tr >
				<th style="white-space: nowrap; width:13%">Date</th>
				<th>Race</th>
				<th style="white-space: nowrap; width:13%">Track</th>
				<th >Qualifier</th>
				<th >Winner</th>
			</tr></thead><tbody>';
        
        foreach($html->find('table[class=rgMasterTable] tr') as $element){
            if($skipHead){
                $skipHead = FALSE;
                continue;
            }
            
            $i = 0;
            $date = $track = $race = $grade = $distance = $age = $sex = $division = $area = $winner = "";
            
            foreach($element->find('td') as $td){
                
                switch ($i) {
                    case 0:
                        $date = $td->plaintext;
                        $date = DateTime::createFromFormat('m/d/y', $date);
                        $date = $date->format('M d,Y');
                        break;
                    case 1:
                        $track = $td->plaintext;
                        $track = $this->cleandata(trim($track));
                        if ( $track == 'Santa Anita Park') {
                            $track = trim( str_replace( 'Park' , '', $track ) );
                        }
                        $track=strtoupper($track);
                        break;
                    case 2:
                        //$race = mb_convert_encoding($td->plaintext,'UTF-8','HTML-ENTITIES');
                        $race = htmlentities($td->plaintext);//fixing dark rombus question mark bug on scrapped data from breeders cup
                        $race = $this->cleandata(trim($race));
                        if ( $race == 'TVG Pacific Classic') {
                            $race = trim( str_replace( 'TVG' , '', $race ) );
                        }
                        if ( $race == 'William Hill Haskell Invitational') {
                            $race = trim( str_replace( 'William Hill' , '', $race ) );
                        }
                        //$race = trim( str_replace( 'Hípico' , 'H&iacute;pico', $race ) );
                        $toReplace[]='?';
                        $toReplace[]='sponsored by QIPCO';
                        $toReplace[]='Presented By The Japan Racing Association';
                        $toReplace[]='JPMorgan';
                        $toReplace[]='Betfair.com';
                        $race= trim(str_replace( $toReplace, '', $race ));
                        //$race=trim(str_replace('Hípico' ,'H&iacute;pico', $race));;
                        break;
                    case 3:
                        $grade = $this->cleandata(trim($td->plaintext));
                        break;
                    case 4:
                        $distance = $this->cleandata(trim($td->plaintext));
                        break;
                    case 5:
                        $age = $this->cleandata(trim($td->plaintext));
                        break;
                    case 6:
                        $sex = $this->cleandata(trim($td->plaintext));
                        break;
                    case 7:
                        $division = $td->plaintext;
                        $division = $this->cleandata(trim($division));
                        $division = trim( preg_replace("@Longines|TwinSpires|Sentient Jet@", '', $division ) );
                        if($division == 'Filly & Mare Turf'){
                                $division = '<a href="/breeders-cup/filly-mare-turf">Filly and Mare Turf</a>';
                        } else if ($division == 'Mile'){
                                $division = '<a href="/breeders-cup/mile">Mile</a>';
                        } else if ($division == 'Classic'){
                                $division = '<a href="/breeders-cup/classic">Classic</a>';
                        } else if ($division == 'Distaff'){
                                $division = '<a href="/breeders-cup/distaff">Distaff</a>';
                        } else if ($division == 'Turf'){
                                $division = '<a href="/breeders-cup/turf">Turf</a>';
                        } else if ($division == 'Turf Sprint'){
                                $division = '<a href="/breeders-cup/turf-sprint">Turf Sprint</a>';
                        } else if ($division == 'Dirt Mile'){
                                $division = '<a href="/breeders-cup/dirt-mile">Dirt Mile</a>';
                        } else if ($division == 'Juvenile Turf Sprint'){
                                $division = '<a href="/breeders-cup/juvenile-turf-sprint">Juvenile Turf Sprint</a>';
                        } else if ($division == 'Filly & Mare Sprint'){
                                $division = '<a href="/breeders-cup/filly-mare-sprint">Filly & Mare Sprint</a>';
                        } else if ($division == 'Juvenile Fillies Turf'){
                                $division = '<a href="/breeders-cup/juvenile-fillies-turf">Juvenile Fillies Turf</a>';
                        } else if ($division == 'Sprint'){
                                $division = '<a href="/breeders-cup/sprint">Sprint</a>';
                        } else if ($division == 'Juvenile'){
                                $division = '<a href="/breeders-cup/juvenile">Juvenile</a>';
                        } else if ($division == 'Juvenile Fillies'){
                                $division = '<a href="/breeders-cup/juvenile-fillies">Juvenile Fillies</a>';
                        } else if ($division == 'Juvenile Turf'){
                                $division = '<a href="/breeders-cup/juvenile-turf">Juvenile Turf</a>';
                        } else {
                                $division;
                        }
                        break;
                    case 8:
                        $area = $this->cleandata(trim($td->plaintext));
                        break;
                    case 9:
                        $winner = $td->plaintext;
                        $winner = str_replace('&nbsp;', '', $winner);
                        $winner = $this->cleandata(trim($winner));
                        break;
                }
                ++$i;
                
            }
			if(strlen(trim($winner)) > 0){ $printwinner=$winner; }else{ $printwinner='&nbsp;'; }
            $printData .='<tr>';
            $printData .='<td data-title="Date" style="white-space: nowrap;">'.trim($date).'</td>';
            $printData .='<td data-title="Race">'.$race.'</td>';
            //$printData .='<td>'.$track.'</td>';
            $printData .='<td data-title="Track" style="white-space: nowrap;">'.$this->buildRaceTrackLink($track).'</td>';//tracklink
            $printData .='<td data-title="Qualifier">'.$division.'</td>';
            $printData .='<td data-title="Winner">'.$printwinner.'</td>';
            $printData .='</tr>';
            
        }
        
        $printData .='</tbody>';
        $printData .='</table>';
        $printData .='</div>';
        //$printData .='{literal}';
        //$printData .='<style>';
        //$printData .='table.ordenable th{cursor: pointer}';
        //$printData .='</style>';
        $printData .= '<script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>';
        $printData .='<script src="//www.usracing.com/assets/js/sorttable_graded.js"></script>';
		$printData .='<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">';
        //$printData .='{/literal}';
        $this->as_file_save_data($printData, $this->OUTPUTFILEPATH);
    }
    
    /**
     * builds link to available racetrack detail
     * 
     * @param string track racetrack code
     * @return string trak code or link to available race track
     * 
     * @author rolando <rolando.garro@myracingaccount.com> 
     */
    private function buildRaceTrackLink($track){
        $ret = "";
        if(array_key_exists($track,$this->available_racetracks) && $this->racetracks_with_details[$track]){
            $ret = "<a href='/".$this->available_racetracks[$track]."'>".$track."</a>";
        }else{
            $ret = $track;
        }
       return $ret;
    }
    
    public static function run() {
        try {
            $instance = new self;
            $instance->scraping_to_tpl();
        } catch (Exception $e) {
            print( $e->getMessage() );
        }
    }
    
    public function cleandata($string) {
        $string =  utf8_decode( $string ) ;
        //$string =  utf8_encode ($string);
        //htmlentities( $string, null, 'utf-8' );
        //$string = str_replace( '&nbsp;', '', str_replace( 'Â', '', strip_tags( trim($string) ) ) );
        //$string = mysql_real_escape_string($string);
        return $string;
    }
    
    function as_file_save_data($data, $file){
        if (!$fp = fopen($file, 'wb')) {
            return "Could not write to ".$file;
        }
        
        fwrite($fp, $data);
        fclose($fp);
        
    }
    
}

BreedersChallangeScraper::run();
