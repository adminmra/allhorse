
<?php

if ( !defined( 'APP_PATH' ) ) {
	define( 'APP_PATH' , '/home/ah/allhorse/public_html/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

if ( !defined( 'NEWSABSPATH' ) ) {
	define("NEWSABSPATH", "/home/ah/allhorse/public_html/programsV2/news");
}

if ( !defined( 'NEWSPUBLICABSPATH' ) ) {
	define("NEWSPUBLICABSPATH", "/home/ah/usracing.com/htdocs/images/news");
}

require_once NEWSABSPATH . '/cron/commonFunctions.php';
require_once APP_PATH . '/simple_html_dom.php';

class BreedersScheduleScraper extends Scraper{

	function __construct() {
		parent::__construct();
		$this->source = 'https://members.breederscup.com/challenge/schedule.aspx';
	}

	public function scraping_to_database(){
		$html = file_get_html( $this->source );
		$queriesArray = array();
		$skipHead = TRUE;

		foreach($html->find('table[class=rgMasterTable] tr') as $element){
			if($skipHead){
				$skipHead = FALSE;
				continue;
			}

			$i = 0;
			$date = $track = $race = $grade = $distance = $age = $sex = $division = $area = $winner = "";

			foreach($element->find('td') as $td){

				switch ($i) {
					case 0:
						$date = $td->plaintext;
						$date = DateTime::createFromFormat('m/d/y', $date);
						$date = $date->format('Y-m-d');
						break;
					case 1:
						$track = $td->plaintext;
						$track = $this->cleandata(trim($track));
						if ( $track == 'Santa Anita Park') {
							$track = trim( str_replace( 'Park' , '', $track ) );
						}
						$track = DB::escape($track);
						break;
					case 2:
						$race = $td->plaintext;
						$race = $this->cleandata(trim($race));
						if ( $race == 'TVG Pacific Classic') {
							$race = trim( str_replace( 'TVG' , '', $race ) );
						}
						if ( $race == 'William Hill Haskell Invitational') {
							$race = trim( str_replace( 'William Hill' , '', $race ) );
						}
						$race = DB::escape($race);
						break;
					case 3:
						$grade = DB::escape($td->plaintext);
						break;
					case 4:
						$distance = DB::escape($td->plaintext);
						break;
					case 5:
						$age = DB::escape($td->plaintext);
						break;
					case 6:
						$sex = DB::escape($td->plaintext);
						break;
					case 7:
						$division = $td->plaintext;
						$division = $this->cleandata(trim($division));
						$division = trim( preg_replace("@Longines|TwinSpires|Sentient Jet@", '', $division ) );
						$division = DB::escape($division);
						break;
					case 8:
						$area = DB::escape($td->plaintext);;
						break;
					case 9:
						$winner = $td->plaintext;
						$winner = str_replace('&nbsp;', '', $winner);
						$winner = DB::escape($winner);
						break;
				}
				++$i;
			}
			$sql = "
				insert into 2011_breeders_challenge set
				racedate = '$date',
				race = '$race',
				track = '$track',
				division = '$division',
				winner = '$winner' ";
			$queriesArray[] = $sql;	
		}
		

		if( count( $queriesArray ) > 0 ){
			DB::query('truncate table 2011_breeders_challenge');
			foreach($queriesArray as $queryI){
				DB::query( $queryI );
				print( sprintf(
					$this->console_color->getColoredString( ">>> " , "cyan" ) . "%'.-160s%-s\n\n" ,
					$this->console_color->getColoredString( ( isset( $queryI )?  $queryI: '' ) , "red" ) ,
					'SQL'
				) );
			}
		}
	}

	public function scraping_to_databaseOLDVersion(){
		$homepage = file_get_contents( $this->source );
		preg_match_all("/<table[^>]*class=\"views\\-table cols\\-6\" >(.*?)<\\/table>/si", $homepage, $match);

		//print_r($match[0]);
		$months['Jan'] = '01';
		$months['Feb'] = '02';
		$months['Mar'] = '03';
		$months['Apr'] = '04';
		$months['May'] = '05';
		$months['Jun'] = '06';
		$months['Jul'] = '07';
		$months['Aug'] = '08';
		$months['Sep'] = '09';
		$months['Oct'] = '10';
		$months['Nov'] = '11';
		$months['Dec'] = '12';

		$queriesArray = array();
		foreach ($match[0] as $table) {
			$DOM = new DOMDocument;
			$DOM->loadHTML( $table );

			//get all tr and td
			$items = $DOM->getElementsByTagName('tr');
			$tds   = $DOM->getElementsByTagName('td');
			$str   = "";

			$processLast = false;
			for ($ii = 0; $ii < $tds->length; $ii++) {
				switch ($ii % 6) {
					case 0:
						if ($ii > 0) {
							$sql = "insert into 2011_breeders_challenge set
							racedate = '$racedate',
							race = '$raceName',
							track = '$track',
							division = '$division',
							winner = '$winner' ";
							$queriesArray[] = $sql;
						}
						$date     = $tds->item($ii)->nodeValue;
						$datedata = explode(' ', strip_tags(trim($date)));
						$month    = $datedata[0];
						$racedate = date("Y") . "-" . $months[$month] . "-" . ($datedata[1]<10?"0":"") . $datedata[1];
					break;
					case 1:
						$raceName = $tds->item($ii)->nodeValue;
						$raceName = $this->cleandata(trim($raceName));
						if ( $raceName == 'TVG Pacific Classic') {
							$raceName = trim( str_replace( 'TVG' , '', $raceName ) );
						}
						else if ( $raceName == 'William Hill Haskell Invitational') {
							$raceName = trim( str_replace( 'William Hill' , '', $raceName ) );
						}
					break;
					case 2:
						$track = $tds->item($ii)->nodeValue;
						$track = $this->cleandata(trim($track));
						if ( $track == 'Santa Anita Park') {
							$track = trim( str_replace( 'Park' , '', $track ) );
						}
					break;
					case 3:
						$division = $tds->item($ii)->nodeValue;
						$division = $this->cleandata(trim($division));
						$division = trim( preg_replace("@Longines|TwinSpires|Sentient Jet@", '', $division ) );

					break;
					case 4:
					//replay image
					break;
					case 5:
						$winner      = $tds->item($ii)->nodeValue;
						$winner      = $this->cleandata(trim($winner));
						$processLast = true;
					break;
				}
			}
			if ($processLast) {
				$sql = "insert into 2011_breeders_challenge set
						racedate = '$racedate',
						race = '$raceName',
						track = '$track',
						division = '$division',
						winner = '$winner' ";
				$queriesArray[] = $sql;
			}

		}

		if( count( $queriesArray ) > 0 ){
			DB::query('truncate table 2011_breeders_challenge');
			foreach($queriesArray as $queryI){
				//echo "$queryI<br />\n";
				DB::query( $queryI );
				print( sprintf(
					$this->console_color->getColoredString( ">>> " , "cyan" ) . "%'.-160s%-s\n\n" ,
					$this->console_color->getColoredString( ( isset( $queryI )?  $queryI: '' ) , "red" ) ,
					'SQL'
				) );
			}
		}
	}

	public function scraping_to_xml(){}

	public function scraping_to_tpl(){}

	public static function run() {
		try {
			$instance = new self;
			$instance->scraping_to_database();
		} catch (Exception $e) {
			print( $e->getMessage() );
		}
	}

	public function cleandata($string) {
	   $string =  utf8_decode( $string ) ;//htmlentities( $string, null, 'utf-8' );
	   //$string = str_replace( '&nbsp;', '', str_replace( 'Â', '', strip_tags( trim($string) ) ) );
	   //$string = mysql_real_escape_string($string);
	   return $string;
	}
}

BreedersScheduleScraper::run();

?>