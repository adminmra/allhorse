<?php

if ( !defined( 'APP_PATH' ) ) {
    #define( 'APP_PATH' , '/home/ah/allhorse/public_html/programsV2/' );
    define( 'APP_PATH' , '/home/ah/scrappers/programsV2/' );
    #define( 'TEMPLATE_DIR'    , "/home/ah/usracing.com/smarty/templates/includes/" );
}

require_once APP_PATH . 'Scraper.php';


class SourceNew extends Scraper{

    private $ALWAYSWRITE;
    private $OUTPUTFILEPATH;
    private $IdLeague;
    private $data;
    private $mainTitle;

    function __construct(){
        parent::__construct();
        $this->source         = "https://www.harnessracing.com/calendar/stakes-schedule";
        $this->ALWAYSWRITE    = FALSE; //always write eveng with the message "please check back shortly"
        //$this->OUTPUTFILEPATH = "/home/ah/allhorse/public_html/schedules/harness_racing_schedule.php";
        $this->OUTPUTFILEPATH = "/home/ah/allhorse/public_html/generated/racing_schedules/harness_schedules_new.php";
        $this->IdLeague       = false;
        $this->data           = array();
        $this->mainTitle      = "";
    }

    public function scraping_to_database(){}

    public function scraping_to_xml(){}

    public function try_other_step($content, &$entries, &$results){
        $o = explode('<br>', $content);

        $_first = explode('<br />', $o[0]);
        if (empty($_first[0])) array_shift($_first); // small fixed due to misconfigured skeleton
        preg_match('~<b>(.*)</b>~', $_first[1], $stake);
        preg_match('~</b>(.*)~', $_first[1], $year);
        $purse_track = explode('@', $_first[2]);
        $purse = $purse_track[0];
        $track = $purse_track[1];
       // $entries['stake'] = $stake[1];
        //$entries['year'] = $year[1];
        //$entries['purse'] = $purse;
        //$entries['track'] = $track;
        //array_push($results,$entries);


        $ss = explode('<span style="font-weight: bold;">', $content);
        foreach ($ss as $k => $v){
            

			if ($k == 0){ 
	//echo $k . " ---. ". $v;
//echo "<br />";	
		
				            // get stake
            $pieces0 = explode('</div>', $v);
			//print_r($pieces0); echo "<br />";
			$pieces3 = explode('<div>', $pieces0[0]);
			$pieces4 = explode('</b>', $pieces3[1]);
            // get Year
            $pieces1 = explode('<br>', $pieces0[1]);
            // get purse and track
            $pieces2 = explode('@', strip_tags($pieces0[1]));

            $entries['stake'] = strip_tags($pieces4[0]); 
            $entries['year'] = strip_tags($pieces4[1]);
            $entries['purse'] = $pieces2[0];
            $entries['track'] = $pieces2[1];
            array_push($results,$entries);
				
			}else{
            $pieces0 = explode('</div>', $v);
			$pieces3 = explode('</span>', $pieces0[0]);
            // get Year
            $pieces1 = explode('<br>', $pieces0[1]);
            // get purse and track
            $pieces2 = explode('@', strip_tags($pieces0[1]));

            $entries['stake'] = strip_tags($pieces3[0]); 
            $entries['year'] = strip_tags($pieces3[1]);
            $entries['purse'] = $pieces2[0];
            $entries['track'] = $pieces2[1];
            array_push($results,$entries);				
			}
			//continue;
            // get stake
            /* $pieces0 = explode('</span>', $v);
            // get Year
            $pieces1 = explode('<br>', $pieces0[1]);
            // get purse and track
            $pieces2 = explode('@', strip_tags($pieces1[1]));

            $entries['stake'] = $pieces0[0]; 
            $entries['year'] = $pieces1[0];
            $entries['purse'] = $pieces2[0];
            $entries['track'] = $pieces2[1];
            array_push($results,$entries); */
        }
    }

    public function scraping_to_tpl(){
          $html_dom = file_get_html( $this->source );
          if( $html_dom->find(".non-ad-container table",1) == null ){return false;}

     	  $tr_s = $html_dom->find(".non-ad-container table",1)->children();                          

		  $results = array();
          $entries = array();                                                                                 
		  foreach($tr_s as $tr){                                                                 
			 foreach($tr->find("td") as $td){                                                     
				$entries['date'] = $td->find(".raceschedule-day", 0)->plaintext;                         
				if($td->find("i")){                                                                
				  //echo "No major stakes" . "<br>";                                                 
				}                                                                                  
				else{                                                                              
                  $entries['stake'] = $td->find("b",0)->plaintext;
                  $out = array();
				  $check = preg_match('~</b>(.*)<br /> <div>~', $td->innertext, $out);                      
                  if ($check == 0) { 
                     $this->try_other_step($td->innertext, $entries, $results);
                     continue;
                  }
				  $entries['year'] = $out[0];
				  $div_out = $td->find("div",0)->plaintext;                                                   
				  preg_match('/(.*) @ (.*)/', $div_out, $data );
				  $entries['purse'] = $data[1];
				  $entries['track'] = $data[2];
				  array_push($results,$entries);
				  foreach($td->find("span") as $span){                                             
                     $entries['stake'] = $span->plaintext;
					 $year_old = $span->parent()->outertext;                                        
					 preg_match('~</span>(.*)</div>~', $year_old, $y);                           
					 $entries['year'] = $y[0];
					 $div = $span->parent()->next_sibling()->plaintext;                             
					 preg_match('/(.*) @ (.*)/', $div_out, $d); 
					 $entries['purse'] = $d[1];
					 $entries['track'] = $d[2];
					 array_push($results, $entries);
				  }                                                                                
				}                                                                                  
			 }                                                                          
		  }
        
        ob_start();
?>


<style>

{literal}
    /* TABLES AND DATA ================================================================================ */
/*
    #infoEntries, #raceEntries, #raceTimes { margin: 10px 0 0 0; padding: 0; width: 100%; }
    #infoEntries tr, #raceEntries tr, #raceTimes tr, #infoEntries tbody { border: none;}
    #infoEntries th, #raceEntries th { border-right: 1px solid #6ca8ee; padding: 7px 10px; color: #FFF; font-size: 1em; background: #105ca9; }
    #infoEntries td, #raceEntries td {  padding: 10px; font-size: 1em;}
    #infoEntries td.odd, #raceEntries tr.AlternateRow  { background: #e5e5e5; }
    #infoEntries .td-box {  display: block; border-bottom: 1px solid #d9d9d9; padding: 9px 10px; }
    #infoEntries .td-box:hover {  display: block; background: #d4e9ff; }

    #raceTimes table td { padding: 10px; font-size: 1em; border: none; }
    #raceTimes, #raceTimes table { border: none;  }
    #raceTimes table tr.odd { background: #d4e9ff;  }
    #raceTimes td.num { font-weight: bold; }
    #raceTimes tbody, #raceTimes table tbody { border: none; }

    .page-racingschedule #infoEntries td { width:14.2%;}
    .page-racingschedule #infoEntries th { font-size: .917em; }
    */
{/literal}

</style>


<div class="table-responsive">
    <table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="Harness Racing Schedule" summary="This week's harness racing schedule for all US and Canadian racetracks.">
        <!-- <caption>Harness Racing Schedule</caption> -->
         <tbody>
            <!--
            <tr>
                <th colspan="3" class="center"> * Harness Racing Schedule * Horses - 2016 Kentucky Derby  - May 06 </th>
            </tr>
            <tr>
                <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
            </tr>
            -->

           <tr>
             <th width="16%">Date</th>
             <th width="40%">Stakes</th>
             <th>Track</th>
             <th>Age & Sex</th>
             <th>Purse</th>
           </tr>

        <?php
            foreach ( $results as $r ) {
        ?>

                <tr>
                    <td><strong><?php echo date( 'M j, Y', strtotime($r['date']) ); ?></strong></td>
                    <td> <?php echo $r['stake'] ?> </td>
                    <td> <?php echo $r['track']; //echo $this->parse_racetrack_link( $r['track']) ?> </td>
                    <td> <?php echo strip_tags($r['year']) ?> </td>
                    <td> <?php echo $r['purse'] ?> </td>
                </tr>

            <?php
            }
         ?>

        <tr>
            <td class="dateUpdated center" colspan="5">
                <em id='updateemp'>Updated as <?php echo date("F j, Y");?>.</em><!-- US Racing <a href="//www.usracing.com/harness-racing-schedule">Harness Racing Schedule</a> -->
            </td>
        </tr>
        </tbody>
    </table>
    
</div>

<?php
        $contents = ob_get_clean();

        $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
    }

    public static function run() {
        try {
            $instance = new self;
            $instance->scraping_to_tpl();
            echo 'test';
        } catch (Exception $e) {
            print( $e->getMessage() );
        }
    }

    private function parse_racetrack_link( $link_name ){


        $link_name = utf8_encode( $link_name );
        $link_name = trim($link_name);
        #$link_name = str_replace('&nbsp;', '', $link_name);
        $link_name = preg_replace('/&#?[a-z0-9]+;/i', '', $link_name);

        $link_racetrack =  str_replace( "&rsquo;" , '', trim( $link_name ) );
        $link_racetrack =  strtolower( preg_replace( "@-\s.*@" , '', trim( $link_racetrack ) ) );
        $link_racetrack =  preg_replace( "@\s[&]\s|\s+@" , '-', trim( $link_racetrack ) );
        $link_racetrack =  preg_replace( "@-\(.*@" , '', trim( $link_racetrack ) );

        if ( $link_racetrack == 'santa-anita-park' ) {
            $link_racetrack = str_replace('-park', '', $link_racetrack );
        }else if ( $link_racetrack == 'woodbine-raceway' ) {
            $link_racetrack = str_replace('-raceway', '', $link_racetrack );
        }
        else if ( $link_racetrack == 'parx-racing-at-philadelphia-park' ) {
            $link_racetrack = trim( str_replace('-at-philadelphia-park', '', $link_racetrack ) );
        }
        else if ( $link_racetrack == 'the-meadowlands' ) {
            $link_racetrack = trim( str_replace('the-', '', $link_racetrack ) );
        }
        else if ( $link_racetrack == 'hoosier-park-harness' ) {
            $link_racetrack = trim( str_replace('-harness', '', $link_racetrack ) );
        }

        $link_racetrack = trim($link_racetrack);
        $counter = 0;
        $safe = array();
        $path = TEMPLATE_DIR . '../content/racetrack';
        $files = scandir($path);
        foreach($files as $f) {
            if (preg_match('~.tpl$~', $f)) {
                if (preg_match("~^$link_racetrack~", $f)) {
                    $safe[] = str_replace('.tpl', '', $f);
                    $counter++;
                }
            }
        }

        if($counter == 1){
            $link_racetrack = $safe[0];        
        }

        $file_exist = file_exists( TEMPLATE_DIR .'../content/racetrack/'.$link_racetrack.'.tpl' );
        return (  $file_exist ) ? "<a href='//www.usracing.com/".$link_racetrack."'>{$link_name}</a>" : $link_name;
    }

}

SourceNew::run();

?>
