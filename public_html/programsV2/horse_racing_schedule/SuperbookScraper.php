
<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

if ( !defined( 'APP_PATH' ) ) {
	define( 'APP_PATH' , '/home/ah/allhorse/public_html/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

class SuperbookScraper extends Scraper{

	private $debug;
	private $test;
	private $delay;

	function __construct(){
		parent::__construct();
		$this->source = 'https://www.superbook.ag/static/info/_default/horses.html';
		$this->debug  = false;
		$this->test   = false;
		$this->delay  = 5;
	}

	public function scraping_to_database(){

		$html = file_get_html( $this->source );

		$mainStoryDefined = false;
		foreach ( $html->find('table') as $key => $entry ) {
			$dateScrapizq = NULL;
			$dateScrapder = NULL;


			$row = $entry->find('tr');
				foreach ( $row as $r) {
					$cols = $r->find('td');
					foreach($cols as $key => $dato){
						if( ++$key%2 != 0 ){
							if( $var = $dato->find('strong',0) ){
								$dateScrapizq = $var->innertext;
							}
							else {
								$formatText = str_replace("&nbsp;", "", $dato->innertext);

								if(trim($formatText)!=''){
									//Guardar desde aqui en la DB
									$datei = date('Y-m-d',strtotime($dateScrapizq));
									$key = $key = str_replace(array(" ","-"), "", $dato->innertext).$datei;
									$query = "INSERT INTO horse_racing_schedule VALUES (NULL,'$dato->innertext','$datei','','$key')";
									print_r( $query );
									echo "\n";
									DB::query( $query );

								}
							}
						}
						else {
							if( $var = $dato->find('strong',0) ){
								$dateScrapder = $var->innertext;
							}
							else {
								$formatText = str_replace("&nbsp;", "", $dato->innertext);

								if( trim( $formatText ) != '' ){
									$dated = date('Y-m-d',strtotime($dateScrapder));
									$key = str_replace(array(" ","-"), "", $dato->innertext).$dated;
									//Guardar desde aqui en la DB
									print_r( $query );
									echo "\n";
									$query = "INSERT INTO horse_racing_schedule VALUES (NULL,'$dato->innertext', '$dated', '','$key')";
									DB::query( $query );

								}
							}
						}
						print( sprintf(
							$this->console_color->getColoredString( ">>> " , "cyan" ) . "%'.-160s%-s\n\n" ,
							$this->console_color->getColoredString( ( isset( $query )?  $query: '' ) , "red" ) ,
							'SQL'
						) );
					}
				}
			unset($entry);
		}
	}

	public function scraping_to_xml(){}

	public function scraping_to_tpl(){}

	public static function run() {
		try {
			$instance = new self;
			$instance->scraping_to_database();
		} catch (Exception $e) {
			print( $e->getMessage() );
		}
	}

}

SuperbookScraper::run();

?>