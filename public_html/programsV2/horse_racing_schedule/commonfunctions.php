<?php	 		 	

function php_get( $url )
{
	$contents = '';

	ini_set( 'user_agent', 'User-Agent: FPLinkChecker/1.2' );
	if( ( $fp = fopen( $url, 'r' ) ) )
	{
		for( ;($data = fread( $fp, 1024 ) ); )
			$contents .= $data;

		fclose( $fp );
	}
	elseif( function_exists( 'curl_init' ) )
	{
		$ch = curl_init( );

		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_HEADER, 0 );
		curl_setopt( $ch, CURLOPT_USERAGENT, 'User-Agent: FPLinkChecker/1.2' );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

		$contents .= curl_exec( $ch );

		curl_close( $ch );
	}
	elseif( ( $url_info = parse_url( $url ) ) )
	{
		if( $url_info['scheme'] == 'https' )
			$fp = fsockopen( 'ssl://' . $url_info['host'], 443, $errno, $errstr, 30);
		else
			$fp = fsockopen( $url_info['host'], 80, $errno, $errstr, 30);

		if( !$fp )
			return false;

		$out = 'GET ' . (isset($url_info['path'])? $url_info['path']: '/') .
			(isset($url_info['query'])? '?' . $url_info['query']: '') .
			" HTTP/1.0\r\n";
		$out .= "Host: {$url_info['host']}\r\n";
		$out .= "User-Agent: FPLinkChecker/1.2\r\n";
		$out .= "Connection: Close\r\n\r\n";

		fwrite($fp, $out);
		for( ;!feof( $fp ); )
			$contents .= fgets($fp, 128);

		list($headers, $content) = explode( "\r\n\r\n", $contents, 2 );

		return $content;
	}
	else
		return false;

	return $contents;
}

function get_links( $html, $url )
{
    $links    = array();
    $url_info = parse_url( $url );

    preg_match_all( "/<a[\s]+[^>]*?href[\s]?=[\s\"\']+(.*?)[\"\']+.*?>/", $html, $matches );

    if( empty( $url_info['path'] ) )
        $url_info['path'] = '/';

    /* if there is a file at the end of the url get it */
    if( $url_info['path']{strlen( $url_info['path'] ) - 1} != '/' )
        $url_info['path'] = substr( $url_info['path'], 0, strrpos( $url_info['path'], '/' ) + 1 );

    if( substr( $url_info['host'], 0, 4 ) == 'www.' )
        $host = substr( $url_info['host'], 4 ) . '/';
    else
        $host = $url_info['host'];

    for( $i=0; isset( $matches[1][$i] ); $i++ )
    {
        if( $matches[1][$i]{0} != '#' && ! strpos( $matches[1][$i], '@' ) ) // stop #top sort of links and remove emails
        {

            if( strpos( $matches[1][$i], '#' ) )
                $matches[1][$i] = substr( $matches[1][$i], 0, strpos( $matches[1][$i], '#' ) );

            if( $matches[1][$i]{0} == '/' ) // add host to any links
                $links[] = $url_info['scheme'] . '://' . $url_info['host'] . $matches[1][$i];
            elseif( $matches[1][$i]{0} == '.' )
            {
                $done    = true;
                $url     = $matches[1][$i];
                $cur_dir = explode( '/', $url_info['path'] );
                array_shift($cur_dir );
                array_pop($cur_dir );

                for( $j=0; isset( $cur_dir[$j] ); $j++ )
                    $cur_dir[$j] = '/' . $cur_dir[$j];

                for(;$done;)
                {
                    /*  if no more ./ or ../ then it's done */
                    if( $url{0} != '.' )
                    {
                        $links[] = $url_info['scheme'] . '://' . $url_info['host'] . implode( '', $cur_dir ) . '/' . $url;
                        $done    = false;
                    }
                    /* remove same dir as that is the default */
                    elseif( substr( $url, 0, 2 ) == './' )
                        $url = substr( $url, 2 );
                    else
                    {
                        $url = substr( $url, 3 );
                        array_pop( $cur_dir );
                    }
                }
            }
            elseif( substr( $matches[1][$i] , 0, 7 ) != 'http://' && substr( $matches[1][$i] , 0, 8 ) != 'https://' ) // do any links left without root
                $links[] = $url_info['scheme'] . '://' . $url_info['host'] . $url_info['path'] . $matches[1][$i];
            else
            {
				$links[] = $matches[1][$i];
            }
        }
    }

    return $links;
}

function getmonday()
{
$day= date('d');
$month=date('m');
$year= date('y');
$Datediff=(date('N')-1);
$monday['year']= date('Y',mktime('0','0','0',$month,$day-$Datediff,$year));
$monday['month']= date('m',mktime('0','0','0',$month,$day-$Datediff,$year));
$monday['day']= date('d',mktime('0','0','0',$month,$day-$Datediff,$year));
$monday['dates']= date('Y-m-d',mktime('0','0','0',$month,$day-$Datediff,$year));


return $monday;

}


function grabdata($datesar)
{
$date=$datesar['dates'];
$url="http://calendar.ntra.com/liveday.cfm?trk_date=".$datesar['year']."%2D".$datesar['month']."%2D".$datesar['day']."%2000%3A00%3A00%2E0";
//echo $url;
$html = php_get($url);
$links = get_links( $html, $url);
//echo $html;
/*echo "<pre>";
print_r($links);
echo "</pre>";*/
/*preg_match_all ("/<font size=\"-1\">([^`]*?)<\/font>/", $html, $matches);
$chunkedarray=array_chunk($matches[0],10);
	echo "<pre>"5
	print_r($chunkedarray);
	echo "</pre>";*/
	
	$delsql = "delete from futurerace where racedate='".$date."' ";
	mysql_query_w($delsql);
	
	$contar=0;
foreach($links as $daturl)
{
	$sql = " insert into futurerace set  ";
	$html2 = php_get($daturl);
	preg_match_all ("/<td width=\"200\">([^`]*?)<\/td>/", $html2, $matches);
	/*echo "<pre>";
	print_r($matches[0]);
	echo "</pre>";*/
	$count=1;
	for($i=6;$i<count($matches[0]);$i++)
	{
		if($i%3 == 0)
		{
			$sql .=" race".$count." ='".strip_tags(trim($matches[0][$i]))."', ";
			$count++;
		}
	}
	
	preg_match_all ("/<b>([^`]*?)<\/b>/", $html2, $matches);
	/*echo "<pre>";
	print_r($matches[0]);
	echo "</pre>";*/
	if(strlen(trim($matches[0][0])) > 0)
	{
		$sql .=" track ='".strip_tags($matches[0][0])."', ";
		$sql .=" location ='".strip_tags($matches[0][2])."', ";
		$sql .=" scratch ='".strip_tags($matches[0][3])."', ";
		$sql .=" zonetime ='".strip_tags($matches[0][4])."', ";
		$sql .=" entry_draw ='".substr(trim(strip_tags($matches[0][1])),-9)."', ";
		$sql .=" racedate='".$date."' ";
				
		mysql_query_w($sql);
		// echo $sql;
	}
	$contar++;
 }


}//fuction end 

function nextdays($Startdate,$Datediff)
{
$nextday['year']= date('Y',mktime('0','0','0',$Startdate['month'],$Startdate['day']+$Datediff,$Startdate['year']));
$nextday['month']= date('m',mktime('0','0','0',$Startdate['month'],$Startdate['day']+$Datediff,$Startdate['year']));
$nextday['day']= date('d',mktime('0','0','0',$Startdate['month'],$Startdate['day']+$Datediff,$Startdate['year']));
$nextday['dates']= date('Y-m-d',mktime('0','0','0',$Startdate['month'],$Startdate['day']+$Datediff,$Startdate['year']));

return $nextday;
}

function grabforeachday($Startdate)
{
   for($i=0; $i<7; $i++)
	{
		$searchdate=nextdays($Startdate,$i);
		/*echo $i."<br />";
		print_r($searchdate);
			echo "<br />";*/
		grabdata($searchdate);
	}
}

/***********************function to gen xml ********************/
function weekfirstday()
{
	$today = date('w');
	return  date('Y-m-d',mktime(0,0,0,date('m'),date('d')-($today-1),date('Y')));
}

function nextday($date,$dayafter)
{
	$datearray=explode("-",$date);
	return  date('Y-m-d',mktime(0,0,0,$datearray[1],$datearray[2]+$dayafter,$datearray[0]));
	
}

function nextdayformatted($date,$dayafter)
{
	$datearray=explode("-",$date);
	return  date('l F j, Y',mktime(0,0,0,$datearray[1],$datearray[2]+$dayafter,$datearray[0]));
	
}

function formattext($id)
{
	$sql= "select * from futurerace where id = '". $id."' ";;
	$result=mysql_query($sql);
	$data = mysql_fetch_object($result);
	if(strlen(trim($data->location)) > 0)
	{
	$formattxt = "<strong>Location:</strong> ".$data->location." <br>";
	}
	
	if(strlen(trim($data->entry_draw)) > 0)
	{
	$formattxt .= "<b>Entry Draw:</b> ".$data->entry_draw." <br>";
	}
	
	if(strlen(trim($data->scratch)) > 0)
	{
	$formattxt .= "<b>Scratch:</b> ".$data->scratch." <br>";
	}
	
	if(strlen(trim($data->zonetime)) > 0)
	{
	$formattxt .= "<b>Time Zone:</b> ".$data->zonetime." <br>";
	}
	
	$formattxt .= "<b>Race Time:</b> <br>";
	$formattxt .= $data->race1."&nbsp;&nbsp;&nbsp;".$data->race2."&nbsp;&nbsp;&nbsp;".$data->race3."&nbsp;&nbsp;&nbsp;".$data->race4."&nbsp;&nbsp;&nbsp;".$data->race5."&nbsp;&nbsp;&nbsp;".$data->race6."&nbsp;&nbsp;&nbsp;<br>";
	$formattxt .= $data->race7."&nbsp;&nbsp;&nbsp;".$data->race8."&nbsp;&nbsp;&nbsp;".$data->race9."&nbsp;&nbsp;&nbsp;".$data->race10."&nbsp;&nbsp;&nbsp;".$data->race11."&nbsp;&nbsp;&nbsp;".$data->race12."&nbsp;&nbsp;&nbsp;<br>";
	$formattxt .= $data->race13."&nbsp;&nbsp;&nbsp;".$data->race14."&nbsp;&nbsp;&nbsp;".$data->race15."&nbsp;&nbsp;<br>";
	return $formattxt;
}

/*****************************************************************/



?>