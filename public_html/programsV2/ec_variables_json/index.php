<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$us_switchies = '/home/ah/allhorse/public_html/control-switch/index.php';
$betus_switchies = '/home/ah/allhorse/public_html/control-switch/busr/index.php';

for ($i = 0; $i < 2; $i++) {
    $list = [];

    if ($i == 0) {
        $server = 'usracing.com';
        include_once $us_switchies;
    } else {
        $server = 'betusracing.ag';
        include_once $betus_switchies;
    }

    foreach (get_dir_contents('/home/ah/' . $server . '/smarty/templates') as $result) {
        $ext = pathinfo($result, PATHINFO_EXTENSION);
        if ((string) $ext == 'tpl' || (string) $ext == 'php') {
            $j = 0;
            foreach ($SwitchVariable['variableName'] as $variableName) {
                $list = get_list($variableName, $result, $SwitchVariable, $j);
                $j++;
            }
        }
    }

    if ($i == 0) {
        $fp = fopen('/home/ah/usracing.com/htdocs/ec_variables/result.json', 'w');
        fwrite($fp, json_encode($list));
        fclose($fp);
        unset($SwitchVariable);
    } else {
        $fp = fopen('/home/ah/betusracing.ag/httpdocs/ec_variables/result.json', 'w');
        fwrite($fp, json_encode($list));
        fclose($fp);
        unset($SwitchVariable);
    }
}

function get_list($search, $result, $SwitchVariable, $index)
{
    global $list;
    $search = '$' . $search;

    if ($handle = fopen($result, 'r')) {
        $count = 0;
        while (($line = fgets($handle, 4096)) !== false) {
            $count++;
            if (strpos($line, $search) !== false) {
                $foo = new stdClass();
                $foo->path = $result;
                $foo->line = $count;
                $foo->var = $search;
                foreach ($SwitchVariable['variableName'] as $variableName) {
                    $foo->start_time = $SwitchVariable['startTIME'][$index];
                    $foo->end_time = $SwitchVariable['endTIME'][$index];
                }
                $block = get_block($result, "$search}", '{/if');
                $foo->block = htmlspecialchars($block);
                array_push($list, $foo);
            }

        }
        fclose($handle);
    }
    return $list;
}

function get_dir_contents($dir, &$results = array())
{
    $files = scandir($dir);

    foreach ($files as $key => $value) {
        $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
        if (!is_dir($path)) {
            $results[] = $path;
        } else if ($value != '.' && $value != '..') {
            get_dir_contents($path, $results);
            $results[] = $path;
        }
    }

    return $results;
}

function get_block($file, $start, $end)
{
    $string = file_get_contents($file);
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) {
        return '';
    }

    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

ob_start();
include "/home/ah/usracing.com/htdocs/ec_variables/result.json";
$json = ob_get_clean();
echo "usracing.com JSON updated\n";
echo '<pre>';
echo "$json\n";

ob_start();
include "/home/ah/betusracing.ag/httpdocs/ec_variables/result.json";
$json = ob_get_clean();
echo "betusracing.ag JSON updated\n";
echo '<pre>';
echo "$json\n";
?>

<html lang="en">
<title>EC Variables - JSON</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
