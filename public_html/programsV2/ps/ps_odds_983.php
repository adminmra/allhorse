
<?php

if ( !defined( 'APP_PATH' ) ) {
    define( 'APP_PATH' , '/home/ah/scrappers/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

class odds_983 extends Scraper{

    private $ALWAYSWRITE;
    private $OUTPUTFILEPATH;
    private $OUTPUT_FILE_XML;
    private $content_xml;
    private $IdLeague;
    private $data;
    private $mainTitle;

    function __construct(){
        parent::__construct();
        $this->source          = "http://ww1.betusracing.ag/odds.xml";
        $this->ALWAYSWRITE     = FALSE; //always write eveng with the message "please check back shortly"
        $this->OUTPUTFILEPATH  = "/home/ah/allhorse/public_html/ps/odds_983.php"; //the absolute path of the file to be written
        $this->OUTPUT_FILE_XML = "/home/ah/allhorse/public_html/ps/odds_983.xml";
        $this->IdLeague        = "983";
        $this->data            = array();
        $this->mainTitle       = "";
        $this->content_xml     = "";

        ////  [\<]league\s+[\d\"\w\s\=\+\-\>\)\<\/\:\(]+[</]league[\>]
        // regular exprecion to scrap

    }

    public function scraping_to_database(){}

    public function scraping_to_xml(){
        $date = date( 'D, d M Y H:i:s +0000' );
        $output = <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="en-ca">
     <title>US Racing | Online Horse Betting</title>
     <author>
         <name>US Racing</name>
         <uri>http://ww1.betusracing.ag/odds.xml</uri>
     </author>
     <updated>{$date}</updated>
     <id>{$date}</id>
     <logo>https://www.usracing.com/img/usracing-sm.png</logo>
     <icon>https://www.usracing.com/themes/favicon.ico</icon>
     <rights>Copyright 2016, US Racing | Online Horse Betting</rights>
     <entry>
         <title>Horses - 2016 Preakness Stakes - May 01 </title>
         <link target="_blank" type="text/html" href="https://www.usracing.com/preakness-stakes/odds"/>
         <updated>{$date}</updated>
         <published>{$date}</published>
         <category term="Sports"/>
         <summary type="html">
            <![CDATA[{$this->content_xml}]]>
         </summary>
         <id>tag:{$date}</id>
     </entry>
 </feed>
EOT;
        $this->as_file_save_data( $output , $this->OUTPUT_FILE_XML );
    }

    public function scraping_to_tpl(){}

    public static function run() {
        try {
            $instance = new self;
            $instance->process();

        } catch ( Exception $e ) {
            print( $e->getMessage() );
        }
    }

    public function process(){
        $this->parseXmlStructureXP();
        $this->generateOutputXP();
    }

    private function parseXmlStructureXP(){

        $ret = false;
        $this->mainTitle = "";
        $this->data = array();
        $xml_string = file_get_contents( $this->source );
        $matches = array();
        preg_match('/<league IdLeague="'.$this->IdLeague.'"[^>]*>(.*?)<\/league>/', $xml_string, $matches);
        if(count($matches)<=0)return $ret;

        $league = $matches[1];
        $leagueObj = simplexml_load_string($matches[0]);
        $this->mainTitle = ucwords(strtolower((string) $leagueObj["Description"]));

        $sections = preg_split("/<banner[^>]*>/", $league); // split by sections
        if(count($sections)<=0) return $ret;

        $banners = array();
        preg_match_all("/<banner[^>]*>/", $league, $banners); // for getting the banners
        if(count($banners)<=0) return $ret;
        $banners = $banners[0];

        for($i=0; $i< count($banners); $i++) {
            $bannerParams =  simplexml_load_string($banners[$i]);
            $section = $sections[$i+1];
            $games = array();

            if($section != ""){
                $g = array();
                preg_match_all("/<game[^>]*>.*?<\/game>/", $section, $g);

                foreach($g[0] as $gamesPlain){
                    $linesPlain = array();
                    preg_match_all("/<line[^>]*>/", $gamesPlain, $linesPlain);
                    $lines = array();

                    foreach ($linesPlain[0] as $line) {
                        $lineParams = simplexml_load_string($line);
                        $lines[] = array(
                            "tmname" => ucwords(strtolower((string)$lineParams[0]["tmname"])),
                            "odds" => (string)$lineParams[0]["odds"],
                            "oddsh" => (string)$lineParams[0]["oddsh"]
                            );
                    }

                    $gameObj = simplexml_load_string("<document>".$gamesPlain."</document>");
                    $games[] = array(
                    "vtm" => ucwords(strtolower((string)$gameObj->game["vtm"])),
                    "htm" => ucwords(strtolower((string)$gameObj->game["htm"])),
                    "lines" => $lines
                    );
                }
            }

            $this->data[] = array(
                "ab" => (string) $bannerParams[0]["ab"],
                "vtm" => ucwords(strtolower((string) $bannerParams[0]["vtm"])),
                "htm" => ucwords(strtolower((string) $bannerParams[0]["htm"])),
                "games" => $games
                );
        }

        return true;
    }

    private function fractionalCalculationXP($oddsh){
        $oodsh_val = intval($oddsh);
        $fractional = 0;

        if(intval($oodsh_val)>0){
            $fractional  = $this->float2ratXP($oodsh_val/100);
        } else if($oodsh_val<0) {
            $fractional  = $this->float2ratXP(100/(-$oodsh_val));
        } else {
            $fractional  = $oddsh;
        }

        return $fractional;
    }

    private function float2ratXP($n, $tolerance = 1.e-6) {
        $h1=1; $h2=0;
        $k1=0; $k2=1;
        $b = 1/$n;
        do {
            $b = 1/$b;
            $a = floor($b);
            $aux = $h1; $h1 = $a*$h1+$h2; $h2 = $aux;
            $aux = $k1; $k1 = $a*$k1+$k2; $k2 = $aux;
            $b = $b-$a;
        } while (abs($n-$h1/$k1) > $n*$tolerance);

        return "$h1/$k1";
    }

    private function writeOutputXP($contents){

        $update = " - Updated " . date("F j, Y H:i:s");
        if( $contents == "" ){
            $contents = "The odds are currently being updated, please check back shortly. <em id='updateemp'> $update. </em>";
            if($this->ALWAYSWRITE || !file_exists($this->OUTPUTFILEPATH) ){
                $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
            }
            else{
                $contents = preg_replace("/(<em\sid=\'updateemp\'>)(.*?)(<\/em>)/", "$1 ".$update." $3", file_get_contents($this->OUTPUTFILEPATH) );
                if($contents !== NULL){
                    $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
                }

                return false;
            }
        }
        else{
            $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
        }

    }

    private function generateOutputXP(){

        $output = "";
        if( count( $this->data ) <= 0 ) return $this->writeOutputXP( $output );
        $firstHeader = false;
        ob_start();
    ?>

	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Preakness Stakes Odds"
	  }
	</script>

    <div>
        <table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Preakness Stakes Odds"  summary="The latest odds for the Preakness Stakes available for wagering now at BUSR.">
            <caption><?php /*  echo $this->mainTitle; */ include '/home/ah/allhorse/public_html/ps/year.php'; ?> Preakness Stakes Odds</caption>
            <tbody>
    <?php
        foreach ($this->data as $d) {
    ?>
        <!--
    <tr>
                    <th colspan="3" class="center">
                    <?php
                    echo $d["vtm"];
                    if($d["htm"] != "") echo "<br />" . $d["htm"];
                    ?>
                    </th>
            </tr>
-->
    <?php
            if(count($d["games"])>0){
                foreach($d["games"] as $g){
                    if(count($g["lines"])>0){
                        if($g["htm"]!="")
                            // echo '<tr><th colspan="3" class="center">'.$g["htm"].'</th></tr>';
                        if(!$firstHeader){
                            echo "<tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr>";
                            $firstHeader = true;
                        }
                        foreach($g["lines"] as $l){
                            if ( $l['oddsh'] != '' ) {
                                if($l["tmname"]!="")$team = $l["tmname"];
                                else $team = $g["vtm"] . " " . $g["htm"];

                                if(FALSE){
                                    echo "<tr itemscope='' itemtype='http://schema.org/Event'>";
                                    echo "<td itemprop='competitor'>" . $team . "</td>";
                                    echo "<td itemprop='offers'>".$this->fractionalCalculationXP($l['oddsh'])."</td>";
                                    echo "<td itemprop='Scratch'>".$l['oddsh']."</td>";
                                    echo '</tr>';
                                }
                                else{
                                    echo "<tr>";
                                    echo "<td>" . $team . "</td>";
                                    echo "<td>".$this->fractionalCalculationXP($l['oddsh'])."</td>";
                                    echo "<td>".$l['oddsh']."</td>";
                                    echo '</tr>';
                                }
                            }
                        }
                    }
                }
            }
        }
    ?>
            </tbody>
        </table>
        <div class="dateUpdated center">
            <em id='updateemp'>Updated <?php echo date("F jS\, Y H:i:s"); ?>.</em><!-- br />
            All odds are fixed odds prices. -->
        </div>
    </div>
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sort_table.js"></script>
    <?php
        $output = ob_get_clean();
        $this->content_xml = $output;
        $this->scraping_to_xml(); // XML FILE GENERATIC
        return $this->writeOutputXP($output);

    }

}

odds_983::run();

?>
