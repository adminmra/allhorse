<?php

if ( !defined( 'APP_PATH' ) ) {
	define( 'APP_PATH' , '/home/ah/scrappers/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

class Odds1042Scraper extends Scraper{

	private $ALWAYSWRITE;
	private $OUTPUTFILEPATH;
	private $IdLeague;
	private $data;
	private $mainTitle;

	function __construct(){
		parent::__construct();
		$this->source         = "http://ww1.betusracing.ag/odds.xml";
		$this->ALWAYSWRITE    = FALSE; //always write eveng with the message "please check back shortly"
		$this->OUTPUTFILEPATH = "/home/ah/allhorse/public_html/ps/match_races_odds_1042.php"; //the absolute path of the file to be written
		$this->IdLeague       = "1042";
		$this->data           = array();
		$this->mainTitle      = "";
	}

	public function scraping_to_database(){}

	public function scraping_to_xml(){}

	public function scraping_to_tpl(){}

	public static function run() {
		try {
			$instance = new self;
			$instance->process();

		} catch ( Exception $e ) {
			print( $e->getMessage() );
		}
	}

	public function process(){
		$this->parseXmlStructureXP();
		$this->generateOutputXP();
	}

	private function parseXmlStructureXP(){

		$ret = false;
		$this->mainTitle = "";
		$this->data = array();
		$xml_string = file_get_contents( $this->source );
		$matches = array();
		preg_match('/<league IdLeague="'.$this->IdLeague.'"[^>]*>(.*?)<\/league>/', $xml_string, $matches);
		if(count($matches)<=0)return $ret;

		$league = $matches[1];
		$leagueObj = simplexml_load_string($matches[0]);
		$this->mainTitle = ucwords(strtolower((string) $leagueObj["Description"]));

		$sections = preg_split("/<banner[^>]*>/", $league); // split by sections
		if(count($sections)<=0) return $ret;

		$banners = array();
		preg_match_all("/<banner[^>]*>/", $league, $banners); // for getting the banners
		if(count($banners)<=0) return $ret;
		$banners = $banners[0];

		for($i=0; $i< count($banners); $i++) {
			$bannerParams =  simplexml_load_string($banners[$i]);
			$section = $sections[$i+1];
			$games = array();

			if($section != ""){
				$g = array();
				preg_match_all("/<game[^>]*>.*?<\/game>/", $section, $g);

				foreach($g[0] as $gamesPlain){
					$linesPlain = array();
					preg_match_all("/<line[^>]*>/", $gamesPlain, $linesPlain);
					$lines = array();

					foreach ($linesPlain[0] as $line) {
						$lineParams = simplexml_load_string($line);
						$lines[] = array(
							"tmname" => ucwords(strtolower((string)$lineParams[0]["tmname"])),
							"vodds" => (string)$lineParams[0]["vodds"],
							"voddsh" => (string)$lineParams[0]["voddsh"],
							"hoddsh" => (string)$lineParams[0]["hoddsh"]
							);
					}

					$gameObj = simplexml_load_string("<document>".$gamesPlain."</document>");
					$games[] = array(
					"vtm" => ucwords(strtolower((string)$gameObj->game["vtm"])),
					"htm" => ucwords(strtolower((string)$gameObj->game["htm"])),
					"lines" => $lines
					);
				}
			}

			$this->data[] = array(
				"ab" => (string) $bannerParams[0]["ab"],
				"vtm" => ucwords(strtolower((string) $bannerParams[0]["vtm"])),
				"htm" => ucwords(strtolower((string) $bannerParams[0]["htm"])),
				"games" => $games
				);
		}

		return true;
	}

	private function fractionalCalculationXP($voddsh){
		$oodsh_val = intval($voddsh);
		$fractional = 0;

		if(intval($oodsh_val)>0){
			$fractional  = $this->float2ratXP($oodsh_val/100);
		} else if($oodsh_val<0) {
			$fractional  = $this->float2ratXP(100/(-$oodsh_val));
		} else {
			$fractional  = $voddsh;
		}

		return $fractional;
	}

	private function float2ratXP($n, $tolerance = 1.e-6) {
		$h1=1; $h2=0;
		$k1=0; $k2=1;
		$b = 1/$n;
		do {
			$b = 1/$b;
			$a = floor($b);
			$aux = $h1; $h1 = $a*$h1+$h2; $h2 = $aux;
			$aux = $k1; $k1 = $a*$k1+$k2; $k2 = $aux;
			$b = $b-$a;
		} while (abs($n-$h1/$k1) > $n*$tolerance);

		return "$h1/$k1";
	}

	private function writeOutputXP($contents){

		$update = " - Updated " . date("F j, Y H:i:s");

		if($contents == ""){
            $contents = "The odds are currently being updated, please check back shortly. <em id='updateemp'> $update. </em>";
            if($this->ALWAYSWRITE || !file_exists($this->OUTPUTFILEPATH)){
                $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
            }
            else{
                $contents = preg_replace("/(<em\sid=\'updateemp\'>)(.*?)(<\/em>)/", "$1 ".$update." $3", file_get_contents($this->OUTPUTFILEPATH) );
                if($contents !== NULL){
                    $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
                }

                return false;
            }
        }
        else{
            $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
        }

	}

	private function generateOutputXP(){

		$output = "";
		if(count($this->data)<=0) return $this->writeOutputXP($output);

		$firstHeader = false;
		ob_start();
	?>
	<div>
		<table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Preakness Stakes Match Races"  summary="">
			<caption><?php echo $this->mainTitle; ?></caption>
			<tbody>
	<?php
		foreach ($this->data as $d) {
	?>
		
<tr>
					<th colspan="3" class="center">
					<?php
					echo $d["vtm"];
					if($d["htm"] != "") echo "<br />" . $d["htm"];
					?>
					</th>
			</tr>

	<?php
			if(count($d["games"])>0){
                $counter = 0;
				foreach($d["games"] as $g){
					if(count($g["lines"])>0){
						if($g["htm"]!="")
						 	//echo '<tr><th colspan="3" class="center">'.$g["htm"].'</th></tr>';
						if(!$firstHeader){
							echo "<tr class='head_title'><th>Team</th><th colspan='2' class='center'>ML</th></tr>";
							$firstHeader = true;
						}
						foreach($g["lines"] as $l){
                            if ( $l['voddsh'] != '' ) {
								//if($l["tmname"]!="")$team = $l["tmname"];
                                if (($counter % 2) == 0) {
									echo "<tr>";
									echo "<td style='background:white'>" . $g["vtm"] . "</td>";
									echo "<td style='background:white'>".$l['voddsh']."</td>";
									echo '</tr>';
                                    echo "<tr>";
									echo "<td style='background:white'>" . $g["htm"] . "</td>";
									echo "<td style='background:white'>".$l['hoddsh']."</td>";
                                    echo "</tr>";
                                } else {
									echo "<tr>";
									echo "<td style='background:#ddd;'>" . $g["vtm"] . "</td>";
									echo "<td style='background:#ddd;'>".$l['voddsh']."</td>";
									echo '</tr>';
                                    echo "<tr>";
									echo "<td style='background:#ddd;'>" . $g["htm"] . "</td>";
									echo "<td style='background:#ddd;'>".$l['hoddsh']."</td>";
                                    echo "</tr>";
                                }
                                //$counter = $counter+1;
							}
						}
					}
                    $counter = $counter +1;
				}
			}
		}
	?>
					<tr>
						<td class="dateUpdated center" colspan="5">
							<em id='updateemp'>Updated <?php echo date("F j, Y H:i:s");?>.</em>
							<?php /* BUSR - Official <a href="https://www.usracing.com/preakness-stakes/match-races">Preakness Stakes Match</a>. */ ?> 

<!-- br />
							All odds are fixed odds prices. -->
						</td>
					</tr>
			</tbody>
		</table>
	</div>
	<?php
		$output = ob_get_clean();
		return $this->writeOutputXP($output);
	}

}

Odds1042Scraper::run();

/*
$result = $this->DB()->query( "SELECT * FROM newsarchive" );
while( $row = $result->fetch_object() ){
	// $fila es un arreglo asociativo con todos los campos que se pusieron en el select
	print_r( $row );
}
 */


?>
