<?php

if ( !defined( 'APP_PATH' ) ) {
    define( 'APP_PATH' , '/home/ah/scrappers/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

class grand_national_1279 extends Scraper {

    private $ALWAYSWRITE;
    private $OUTPUTFILEPATH;
    private $IdLeague;
    private $data;
    private $mainTitle;

    function __construct(){
        parent::__construct();
        $this->source         = "http://ww1.betusracing.ag/odds.xml";
        $this->ALWAYSWRITE    = FALSE; //always write eveng with the message "please check back shortly"
        $this->OUTPUTFILEPATH = "/home/ah/allhorse/public_html/misc/grand_national_1279.php";
        $this->IdLeague       = "1279";
        $this->data           = array();
        $this->mainTitle      = "";

    }

    public function scraping_to_database(){}

    public function scraping_to_xml(){}

    public function scraping_to_tpl(){}

    public static function run() {
        try {
            $instance = new self;
            $instance->process();
        } catch (Exception $e) {
            print( $e->getMessage() );
        }
    }

    public function process(){
        $this->parseXmlStructureXP();
        $this->generateOutputXP();
    }


    private function parseXmlStructureXP(){
        $ret             = false;
        $this->mainTitle = "";
        $this->data      = array();
        $xml_string      = file_get_contents( $this->source );
        $matches         = array();
        preg_match('/<league IdLeague="'.$this->IdLeague.'"[^>]*>(.*?)<\/league>/', $xml_string, $matches);
        if(count($matches)<=0)return $ret;

        $league          = $matches[1];
        $leagueObj       = simplexml_load_string($matches[0]);
        $this->mainTitle = ucwords(strtolower((string) $leagueObj["Description"]));

        $sections = preg_split("/<banner[^>]*>/", $league); // split by sections
        if(count($sections)<=0) return $ret;

        $banners = array();
        preg_match_all("/<banner[^>]*>/", $league, $banners); // for getting the banners
        if( count( $banners ) <= 0 ) return $ret;
        $banners = $banners[0];

        for($i=0; $i< count($banners); $i++) {
            $bannerParams =  simplexml_load_string($banners[$i]);
            $section      = $sections[$i+1];
            $games        = array();

            if($section != ""){
                $g = array();
                preg_match_all("/<game[^>]*>.*?<\/game>/", $section, $g);

                foreach($g[0] as $gamesPlain){
                    $linesPlain = array();
                    preg_match_all("/<line[^>]*>/", $gamesPlain, $linesPlain);
                    $lines = array();

                    foreach ($linesPlain[0] as $line) {
                        $lineParams = simplexml_load_string($line);
                        $lines[] = array(
                            "tmname" => ucwords(strtolower((string)$lineParams[0]["tmname"])),
                            "odds"   => (string)$lineParams[0]["odds"],
                            "oddsh"  => (string)$lineParams[0]["oddsh"]
                            );
                    }

                    $gameObj = simplexml_load_string("<document>".$gamesPlain."</document>");
                    $games[] = array(
                        "vtm"   => ucwords(strtolower((string)$gameObj->game["vtm"])),
                        "htm"   => ucwords(strtolower((string)$gameObj->game["htm"])),
                        "lines" => $lines
                    );
                }
            }

            $this->data[] = array(
                "ab"    => (string) $bannerParams[0]["ab"],
                "vtm"   => ucwords(strtolower((string) $bannerParams[0]["vtm"])),
                "htm"   => ucwords(strtolower((string) $bannerParams[0]["htm"])),
                "games" => $games
                );
        }

        return true;
    }

    private function fractionalCalculationXP($oddsh){
        $oodsh_val = intval($oddsh);
        $fractional = 0;

        if(intval($oodsh_val)>0){
            $fractional  = $this->float2ratXP($oodsh_val/100);
        } else if($oodsh_val<0) {
            $fractional  = $this->float2ratXP(100/(-$oodsh_val));
        } else {
            $fractional  = $oddsh;
        }

        return $fractional;
    }

    private function float2ratXP($n, $tolerance = 1.e-6) {
        $h1=1; $h2=0;
        $k1=0; $k2=1;
        $b = 1/$n;
        do {
            $b   = 1/$b;
            $a   = floor($b);
            $aux = $h1; $h1 = $a*$h1+$h2; $h2 = $aux;
            $aux = $k1; $k1 = $a*$k1+$k2; $k2 = $aux;
            $b   = $b-$a;
        } while (abs($n-$h1/$k1) > $n*$tolerance);

        return "$h1/$k1";
    }

    private function writeOutputXP($contents){
        $update = " - Updated " . date("F j, Y H:i:s");
        if($contents == ""){
            $contents = "The odds are currently being updated, please check back shortly. <em id='updateemp'> $update. </em>";
            if( $this->ALWAYSWRITE || !file_exists( $this->OUTPUTFILEPATH ) ){
                if( file_put_contents( $this->OUTPUTFILEPATH, $contents ) ){
                    echo "File " , $this->OUTPUTFILEPATH . " was written with no data.\n";
                    return true;
                }
                else{
                    echo "Error while trying to write file " . $this->OUTPUTFILEPATH . " with no data. Please check permissions.\n";
                    return false;
                }
            }
            else{
                $contents = preg_replace("/(<em\sid=\'updateemp\'>)(.*?)(<\/em>)/", "$1 ".$update." $3", file_get_contents( $this->OUTPUTFILEPATH ) );
                if($contents !== NULL){
                    if(file_put_contents( $this->OUTPUTFILEPATH, $contents ) ){
                        echo "Empty output, update record updated.\n";
                        return true;
                    }
                    else{
                        echo "Error while trying to write file " . $this->OUTPUTFILEPATH . " with no data but updating update record. Please check permissions.\n";
                        return false;
                    }
                }

                return false;
            }
        }
        else{
            if(file_put_contents( $this->OUTPUTFILEPATH, $contents ) ){
                print( sprintf(
                    $this->console_color->getColoredString( ">>> " , "cyan" ) . "%'.-155s%-s\n\n" ,
                    $this->console_color->getColoredString( $this->OUTPUTFILEPATH , "red" ) ,
                    'GENERATED'
                ) );
                return true;
            }
            else{
                echo "Error while trying to write file " . $this->OUTPUTFILEPATH . ". Please check permissions.\n";
                return false;
            }
        }

    }

    private function generateOutputXP(){
        $output = "";
        if(count($this->data)<=0) return $this->writeOutputXP($output);

        $firstHeader = false;
        ob_start();
    ?>
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="<?php echo $this->mainTitle; ?>">
            <caption><?php echo str_replace( "Us", "US", $this->mainTitle );//  echo $this->mainTitle; ?></caption>
            <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="3">
                  <em id='updateemp'>Updated <?php echo date("F j, Y H:i:s");?>.</em> <!-- BUSR - Official
                  <a href="//www.usracing.com/odds/us-presidential-election">US Presidential Election Odds</a>. --> <br />
                  <!-- <a href="//www.usracing.com/odds/us-presidential-election">US Presidential Odds</a>, -- All odds are fixed odds prices. -->
                </td>
              </tr>
            </tfoot>
            <tbody>
    <?php
        foreach ($this->data as $d) {
    ?>
           <!-- <tr>
                    <th colspan="3" class="center">
                    <?php
                    //echo  str_replace("Us", "US",   $d["vtm"] );
                    //echo "2016 US Presidential Election - Oct 30";
                    //if($d["htm"] != "") echo "<br />" . $d["htm"];
                    ?>
                    </th>
            </tr> -->
    <?php
            if(count($d["games"])>0){
                foreach($d["games"] as $g){
                    if(count($g["lines"])>0){
                        if($g["htm"]!="")
                            //echo '<tr><th colspan="3" class="center">'.$g["htm"].'</th></tr>';
                        if(!$firstHeader){
                            echo '<tr><th></th><th>Fractional</th><th>American</th></tr>';
                            $firstHeader = true;
                        }
                        foreach($g["lines"] as $l){
                            if($l["tmname"]!="")$team = $l["tmname"];
                            else $team = $g["vtm"] . " " . $g["htm"];

                            if(FALSE){
                                echo "<tr itemscope='' itemtype='http://schema.org/Event'>";
                                echo "<td itemprop='competitor'>" . $team . "</td>";
                                echo "<td itemprop='offers'>".$this->fractionalCalculationXP($l['oddsh'])."</td>";
                                echo "<td itemprop='Scratch'>".$l['oddsh']."</td>";
                                echo '</tr>';
                            }
                            else{
                                echo "<tr>";
                                echo "<td>" . $team . "</td>";
                                echo "<td>".$this->fractionalCalculationXP($l['oddsh'])."</td>";
                                echo "<td>".$l['oddsh']."</td>";
                                echo '</tr>';
                            }
                        }
                    }
                }
            }
        }
    ?>
            </tbody>
        </table>
    </div>
    {literal}
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
        {/literal}
    <?php
        $output = ob_get_clean();
        return $this->writeOutputXP($output);
    }

}

grand_national_1279::run();

?>
