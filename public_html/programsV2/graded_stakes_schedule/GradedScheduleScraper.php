
<?php

if ( !defined( 'APP_PATH' ) ) {
	define( 'APP_PATH' , getenv("HOME") . '/allhorse/public_html/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

class GradedScheduleScraper extends Scraper{

	function __construct(){
		parent::__construct();
		$this->source = "http://www.offtrackbetting.com/graded_stakes_results.html";
	}

	public function scraping_to_database(){
		$homepage = file_get_contents( $this->source );
		//$homepage = file_get_contents('http://www.offtrackbetting.com/graded_stakes_results_2013.html');

		//echo $homepage;
		//preg_match("/<div[^>]*class=\"gradient-container\">(.*?)<\\/div>/si", $homepage, $match);
		//preg_match("/<div[^>]*id=\"gradedStakes_results\"[^>]*class=\"gradedStakes_results\">(.*?)<\\/div>/si", $homepage, $match);
		//echo $match[1];
		preg_match("/<div[^>]*class=\"gradient\\-container\\-border\">(.*?)<\\/div>/si", $homepage, $match);

		//print_r($match);
		$data1 = str_replace('<div id="gradedStakes_results" class="gradedStakes_results">', '', $match[0]);
		$data2 = str_replace('</div>', '', $data1);
		$data3 = str_replace('<p style="text-align:right!important;"><a href="http://www.offtrackbetting.com/graded_stakes_results_2011.html" title="2011 Graded Stakes Race Results">2011 Graded Stakes Race Results</a> | <a href="http://www.offtrackbetting.com/graded_stakes_results_2012.html" title="2012 Graded Stakes Race Results">2012 Graded Stakes Race Results</a></p>', '', $data2);

		//echo $data1;
		$this->getdata($homepage);
	}

	public function scraping_to_xml(){}

	public function scraping_to_tpl(){}

	public static function run() {
		try {
			$instance = new self;
			$instance->scraping_to_database();

		} catch ( Exception $e ) {
			print( $e->getMessage() );
		}
	}

	private function getdata($contents) {
		//$contents = $data3;
		//file_get_contents('internatdata.htm');
		//create a DOM based off of the string from the html table
		$DOM = new DOMDocument;
		$DOM->loadHTML($contents);

		//get all tr and td
		$items = $DOM->getElementsByTagName('tr');
		$tds   = $DOM->getElementsByTagName('td');


		if ( $items->length < 1 || $tds->length < 1 ) {
			echo sprintf(
			$this->console_color->getColoredString( ">>> " , "cyan" ) . "%'.-110s%-s\n" ,
			$this->console_color->getColoredString( "No found data for graded stakes results" , "red" ) ,
			'SQL'
			);
			return;
		}

		/* echo $items->length;
		echo $tds->length;
		echo $tds->item(0)->nodeValue;
		echo "<pre>";
		print_r($tds);
		echo "</pre>";
		echo "<br />"; */


		DB::query('truncate table graded_schedule');
		$start = 0; //for ($i = 0; $i < $items->length; $i++){
		$sw1   = false;
		$cont  = 0;
	    for ($i = 0; $i < ($items->length - 1); $i++) {
			//echo tdrows($tds)."; <br />";
			//echo 	$start;
			//sql
			$this->getrow( $tds, $start, $sw1, $cont);
			//echo "<br />";
			$start = $start + 10;
		}
	}

	private function cleandata($string) {
		$string = htmlentities($string, null, 'utf-8');
		$string = str_replace('&nbsp;', '', str_replace('�', '', strip_tags(trim($string))));
		return $string;
	}

	private function getrow($elements, $start, &$sw1, &$cont) {
		$currentYear   = date( "Y" , strtotime(' +2 day') );
		$months['Jan'] = '01';
		$months['Feb'] = '02';
		$months['Mar'] = '03';
		$months['Apr'] = '04';
		$months['May'] = '05';
		$months['Jun'] = '06';
		$months['Jul'] = '07';
		$months['Aug'] = '08';
		$months['Sep'] = '09';
		$months['Oct'] = '10';
		$months['Nov'] = '11';
		$months['Dec'] = '12';
		/* for($k=$start; $k<$start+10; $k++)
		{
		echo str_replace('�','',strip_tags(trim($elements->item($k)->nodeValue)))."--";
		} */

		//echo "***".strip_tags(trim($elements->item($start)->nodeValue))."***";
		$datedata = explode(' ', trim(strip_tags($elements->item($start)->nodeValue)));
		$month = $datedata[0];

		switch ($month) {
			case 'Jan':
				if ($sw1) {
					$sw1 = false;
					$cont++;
				}
			break;
			case 'Dec':
				$sw1 = true;
			break;
		}

		$yearP = $currentYear + $cont;

		$sql = "insert into graded_schedule set
		racedate = '" .$yearP . "-" . $months[$month] . "-" . $datedata[1] . "',
		track = '" . $this->cleandata(trim($elements->item($start + 1)->nodeValue)) . "',
		racename = '" . $this->cleandata(trim($elements->item($start + 2)->nodeValue)) . "',
		grade = '" . $this->cleandata(trim($elements->item($start + 3)->nodeValue)) . "',
		purse = '" . $this->cleandata(trim($elements->item($start + 4)->nodeValue)) . "',
		age = '" . $this->cleandata(trim($elements->item($start + 5)->nodeValue)) . "',
		ds = '" . $this->cleandata(trim($elements->item($start + 6)->nodeValue)) . "',
		horse = '" . $this->cleandata(trim($elements->item($start + 7)->nodeValue)) . "',
		jockey = '" . $this->cleandata(trim($elements->item($start + 8)->nodeValue)) . "',
		trainer = '" . $this->cleandata(trim($elements->item($start + 9)->nodeValue)) . "' ";

		DB::query($sql);
		echo sprintf(
			$this->console_color->getColoredString( ">>> " , "cyan" ) . "%'.-110s%-s\n" ,
			$this->console_color->getColoredString( $sql , "red" ) ,
			'SQL'
			);
	}

	private function tdrows($elements) {
		$str = "";
		for ($ii = 0; $ii < $elements->length; $ii++) {
			$str .= $elements->item($ii)->nodeValue . "-";
		}
		return $str;
	}
}

GradedScheduleScraper::run();

?>
