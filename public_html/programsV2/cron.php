<?php
ini_set( 'display_errors', 1 );
ini_set( 'log_errors' , 1 );
error_reporting( E_ALL );
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');

define( 'DS'       , DIRECTORY_SEPARATOR );
define( 'APP_PATH' , __DIR__ . DS );    // => /home/ah/allhorse/public_html/programsV2/


// Define import class

require_once APP_PATH . 'graded_stakes_schedule/GradedScheduleScraper.php';
require_once APP_PATH . 'horse_racing_schedule/BetlmScraper.php';
require_once APP_PATH . 'horse_racing_schedule/SuperbookScraper.php';
require_once APP_PATH . 'breeders/BreedersOddsScraper.php'; //Classic 927
//require_once APP_PATH . 'breeders/BreedersDistaffOddsScraper.php';
//require_once APP_PATH . 'breeders/BreedersScheduleScraper.php';
//require_once APP_PATH . 'breeders/BreedersMileOddsScraper.php';
//require_once APP_PATH . 'breeders/breeders_juvenile_fillies_987.php';
//require_once APP_PATH . 'breeders/breeders_juvenile_990.php';
//require_once APP_PATH . 'breeders/breeders_turf_991.php';
//require_once APP_PATH . 'breeders/breeders_sprint_992.php';
require_once APP_PATH . 'kd/Odds926Scraper.php';  //first letter
require_once APP_PATH . 'kd/odds_to_win_998.php';  //odds to win
//require_once APP_PATH . 'misc/OddsApScraper.php';
//require_once APP_PATH . 'misc/Odds_DWTS_Scraper.php'; //Victor Espinoza DWTS
//require_once APP_PATH . 'misc/odds_arc_de_triomphe_scraper.php';
//require_once APP_PATH . 'misc/odds_melbourne_cup_scraper.php';
//require_once APP_PATH . 'misc/LiveInRunningScraper.php';
require_once APP_PATH . 'leader/HorseScraper.php'; //  leader/cronhorse.php // ver en detalle
/*require_once APP_PATH . 'news/BostonHeraldScraper.php';*/
require_once APP_PATH . 'newsxml/GeneraXmlScraper.php';

require_once APP_PATH . 'harness_schedule_scraper.php';
require_once APP_PATH . 'past_performances_scraper.php';

require_once APP_PATH . 'kd/kentucky_oaks_1006.php';
require_once APP_PATH . 'misc/triple_crown_993.php';

require_once APP_PATH . 'kd/winning_jockeys_1005.php';
require_once APP_PATH . 'kd/winning_trainers_1012.php';

require_once APP_PATH . 'kd/kentucky_prep_races.php';

