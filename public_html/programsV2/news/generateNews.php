<?php

/* autho: pold
 */

$title = $this->get_template_vars("title");
$keyword = $this->get_template_vars("keyword");
$limit = $this->get_template_vars("limit");

if(empty($limit) || $limit == 0) $limit = 3;

if(!empty($keyword)){
    $keyword_cond = ' WHERE ';
}

        

set_include_path(dirname(__FILE__));
include_once "./dbconnect_rw.inc";

$query = " SELECT pictureThumb, ContentIdART as ContentId, TitleART as Title, HtmlART as Html, DateCreatedART as DateCreated, newsarchive.timecreate as timecreate, newsarchive.pageuri, Teaser "
        . "FROM newsarticle left join newsarchive on newsarchive.ContentId = newsarticle.ContentIdArt "
        . "WHERE (Title LIKE '%$keyword%') AND (newsarchive.typetosee = 'OTH' or newsarchive.typetosee = 'AHR') "
        . "ORDER BY newsarchive.timecreate desc "
        . "LIMIT 0, $limit";

$results = mysql_query_w($query);

$article_list_result = array();
while ($row = mysql_fetch_assoc($results)) {
    array_push($article_list_result, $row);
}
//print_r($article_list_result);

$output = '<div id="news" class="news">';
$output .= '<div class="headline"><h2>'.$title.'<a target="_blank" href="/news/rss.xml"><i class="fa fa-rss"></i></a></h2></div>';
$output .= '<div class="content">';
foreach($article_list_result as $newsitem){
    $output .= '<dl class="dl-horizontal story" id="news-story-'.$newsitem["ContentId"].'">';
    $output .= '<dd>';
    $output .= '<p class="news-title"><a href="/news/'.$newsitem["pageuri"].'">'.$newsitem["Title"].'</a></p>';
    $output .= '<div class="teaser">'.  _ahr_article_teaser(strip_tags(html_entity_decode($newsitem["Html"])), 0, 215).'...</div>';
    $output .= '<div class="news-date">'.$newsitem["DateCreated"].'</div>';
    $output .= '</dd>';
    $output .= '</dl>';
}
$output .= '</div>';
$output .= '</div>';
$output .= '<div class="blockfooter"><a href="/news" title="Read More News" class="btn btn-primary" rel="nofollow">Read More News<i class="fa fa-angle-right"></i></a></div>';

echo $output;