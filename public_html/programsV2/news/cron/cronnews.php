<?php	 		 	

/**
 * Author: Leopold
 * email: fvillegas@acedevel.com
 * Copyright: usracing.com 2013
 */
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

define("EOF", "<br />\n");
define("NEWSABSPATH", "/home/ah/allhorse/public_html/programs/news");
define("NEWSPUBLICABSPATH", "/home/ah/usracing.com/htdocs/images/news");


require_once NEWSABSPATH . '/dbconnect_rw.inc';
require_once NEWSABSPATH . '/cron/commonFunctions.php';
require_once NEWSABSPATH . '/cron/simplehtmldom_1_5/simple_html_dom.php';

class YahooSportsScrapper {
	var $debug;
	var $test;
	var $delay;

	function __construct($debug = false, $test = false, $delay = 5) {
		$this->debug = $debug;
		$this->test = $test;
		$this->delay = $delay;
	}

	public function main() {
		$xml = simplexml_load_file("http://sports.yahoo.com/rah/rss.xml");
		$issues = array();

		//entries
		$entries = array();
		foreach ($xml->channel->item as $item) {
			$title = $image = $teaser = $url = '';
			$title = trim($item->title);
			$url = trim($item->link);
			$url_tmp = explode("*", $url);
			$url = urldecode($url_tmp[1]);
			//$image = $this->catch_first_image($item->description);
			$date = trim($item->pubDate);
			$teaser = trim(strip_tags($item->description));
			$entries[] = array("title" => $title, "image" => "", "teaser" => $teaser, "url" => $url, "date" => $date);
		}
		$this->randomSortAndProcess($entries, $issues);

		if (count($issues)) {
			$this->y_reportWebmaster($issues);
		}

		echo EOF . "End of the scrapping script." . EOF;
	}

	private function catch_first_image($source) {
		$first_img = '';
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $source, $matches);
		$first_img = $matches[1][0];

		if (empty($first_img)) {
			$first_img = "/path/to/default.png";
		}
		return $first_img;
	}

	private function randomSortAndProcess($entriesArray, &$issues) {
		if (count($entriesArray)) {
			$this->shuffle_assoc($entriesArray);
			foreach ($entriesArray as $item) {
				$this->y_printEntryBasic($item['title'], $item['image'], $item['teaser'], $item['url']);
				if ($this->y_checkURL($item['url'])) {
					$this->y_processEntry($item['title'], $item['image'], $item['teaser'], $item['url'], $issues, $item['date']);
				}
			}
		}
	}

	private function shuffle_assoc(&$array) {
		$keys = array_keys($array);

		shuffle($keys);

		foreach ($keys as $key) {
			$new[$key] = $array[$key];
		}

		$array = $new;

		return true;
	}

	private function y_printEntryBasic($title, $image, $teaser, $url) {
		echo EOF . "Title: " . $title . EOF;
		echo "Picture: " . $image . EOF;
		echo "Teaser: " . $teaser . EOF;
		echo "Url: " . $url . EOF;
	}

	private function y_checkURL($url) {
		$url = trim(strtolower($url));
		if (strpos($url, "http://sports.yahoo.com/news/") !== FALSE) {
			return true;
		} elseif ($url == '') {
			echo "Status: URL is empty. Skipping ..." . EOF;
			return false;
		} else {
			echo "Status: '$url' is an external link, it points to other web address. Skipping ..." . EOF;
			return false;
		}
	}

	private function y_processEntry($title, $image, $teaser, $url, &$issues, $date, $featured = 0) {
		if ($title != "" && $teaser != "" && $url != "") {
			$pageuri = toAscii(urldecode(($title)), '');
			$nrecords = $this->y_getNRecords($pageuri);

			if ($nrecords > 0 && !$this->test) {
				echo "Status: Entry '$pageuri' already scrapped, and already in database. Skipping ..." . EOF;
			} else {
				// New entry, proceed to scrape and store
				$this->y_proceedEntry($url, $title, $pageuri, $issues, $featured, $date, $teaser, $image);
			}
		} else {
			$possible_reason = "Wrong scrapping, probably sources have changed at http://sports.yahoo.com/news/";
			$this->y_reportError($issues, '', "title: " . $title, '', '', $teaser, 'image: ' . $image . " - url: " . $url, '', "", $possible_reason);
		}
	}

	private function y_proceedEntry($url, $title, $pageuri, &$issues, $featured, $date, $teaser = "", $image = "") {
		sleep($this->delay);
		$titlenewentry = $bodynewentry = "";
		$ans1 = $ans2 = false;
		$htmlnewentry = file_get_html($url);
		try {
			if (method_exists($htmlnewentry, "find")) {
				$title = $htmlnewentry->find('.headline', 0)->plaintext;
				$find_headline = $htmlnewentry->find('.headline');

				if ($find_headline) {
					foreach ($find_headline as $fhItem) {
						$titlenewentry = trim($fhItem->plaintext);
						break;
					}
				}
				else
					throw new Exception("I could not get the article title.");
			}
			else
				throw new Exception("I could not get the code and instance an object from $url.");

			$find_article = $htmlnewentry->find('div.body');
			if ($find_article) {
				foreach ($find_article as $faItem) {
					$bodynewentry = trim($faItem->innertext);
					break;
				}
			}
			else
				throw new Exception("I could not get the article body.");

			$datenewentry = trim(str_replace(' PST', '', $date));
			$unixTimeStamp = strtotime($datenewentry);

			//only store when all the data are correct
			if ($unixTimeStamp != FALSE && $pageuri != '' && $titlenewentry != '' && $bodynewentry != '') {
				$datenewentry2 = date("F j, Y", $unixTimeStamp);
				$datenewentry3 = date("Y-m-d H:i:s", $unixTimeStamp);

				$innerhtml = str_get_html($bodynewentry);
				foreach ($innerhtml->find("a") as $anchorItem) {
					$anchorItem->href = null;
				}
				foreach ($innerhtml->find("meta") as $meta) {
					$meta->outertext = "";
				}
				foreach ($innerhtml->find(".yom-ad") as $ad) {
					$ad->outertext = "";
				}
				foreach ($innerhtml->find(".body-slot-mod") as $slot) {
					$slot->outertext = "";
				}
				foreach ($innerhtml->find("ul#topics") as $ul) {
					$ul->outertext = "";
				}

				if ($teaser == '') {
					$descriptionteasure = trim(cleaninputtreasure($innerhtml->innertext));
					$teaser = AbbrHtml($descriptionteasure, '300');
				}

				//get first pic
				$extraContents = '<div>';
				foreach ($htmlnewentry->find('.small-cover-wrap') as $cover) {
					foreach ($cover->find('figure') as $figure) {
						foreach ($figure->find('img') as $fimg) {
							$extraContents .= '<img src="' . $fimg->src . '" alt="' . $fimg->alt . '" /><br />';
							break;
						}
						break;
					}
					foreach ($cover->find('.caption') as $caption) {
						$extraContents .= '<span>' . $caption->plaintext . '</span>';
						break;
					}
					break;
				}
				$extraContents .= '</div>';

				$newEntryId = $this->y_getNewEntryId();

				if ($featured) {
					$bodynewentry = trim($extraContents . $innerhtml->innertext);
					$this->y_createMainPic($newEntryId, $image);
				}
				else
					$bodynewentry = trim($extraContents . $innerhtml->innertext);
				
				$bodynewentry = '<div class="newsArticleWrapper">'.$bodynewentry.'</div>';

				//download pictures and update them in the source
				$picture = $pictureThumb = "";
				$this->y_processImages($bodynewentry, $newEntryId, $picture, $pictureThumb);

				$Sql_insert1 = $Sql_insert2 = "";
				$ans1 = $this->y_putNewsArchive($Sql_insert1, $newEntryId, $title, $teaser, $datenewentry2, $datenewentry3, $pageuri, $picture, $pictureThumb, $featured);
				$ans2 = $this->y_putNewsArticle($Sql_insert2, $newEntryId, $title, $bodynewentry, $pageuri, $datenewentry2);

				if ($ans1 && $ans2) {
					echo "Status: New entry scrapped and saved to database successfully." . EOF;
				} else if ($this->test) {
					echo "Status: New entry is not being written. Test mode." . EOF;
				} else {
					$possible_reason = "Database Problem, maybe wrong credentials, or Database overloaded.";
					$this->y_reportError($issues, $pageuri, $titlenewentry, $datenewentry, $datenewentry2, $teaser, $bodynewentry, $Sql_insert1, $Sql_insert2, $possible_reason);
				}
			} else {
				$possible_reason = "Wrong scrapping, maybe sources have changed at $url.";
				$this->y_reportError($issues, $pageuri, $titlenewentry, $datenewentry, '', $teaser, $bodynewentry, '', '', $possible_reason);
			}
		} catch (Exception $e) {
			$possible_reason = "Wrong scrapping, here is other type of source code at $url. More details: " . $e->getMessage();
			$this->y_reportError($issues, "", $title, date("Today: Y-m-d H:i:s"), '', $teaser, '', '', '', $possible_reason);
		}
	}

	private function y_processImages(&$source, $newEntryId, &$picture, &$pictureThumb) {
		$picture = $pictureThumb = "";
		$dom = new DOMDocument();
		$dom->loadHTML($source);
		
		# remove <!DOCTYPE 
		$dom->removeChild($dom->firstChild);            

		# remove <html><body></body></html> 
		$dom->replaceChild($dom->firstChild->firstChild->firstChild, $dom->firstChild);
		
		$path = NEWSPUBLICABSPATH . "/" . $newEntryId;
		$newSrc = "https://www.usracing.com/images/news/" . $newEntryId;

		$proceed = TRUE;

		$imgs = $dom->getElementsByTagName("img");
		$cont = 1;
		foreach ($imgs as $img) {
			if ($proceed) {
				if (!file_exists($path))
					mkdir($path);
				$proceed = FALSE;
			}
			$oldSrc = $img->getAttribute('src');
			$pathinfo = pathinfo($oldSrc);
			$Imagefile = $path . "/" . $pathinfo['filename'] . ".jpg";
			$imgcontent = file_get_contents($oldSrc);
			file_put_contents($Imagefile, $imgcontent);
			$newSrc .= "/" . $pathinfo['filename'] . ".jpg";
			$img->setAttribute('src', $newSrc);

			if ($cont == 1) {
				$picture = $newSrc;
				$dest = $path . "/" . $pathinfo['filename'] . "-thumb.jpg";
				$this->y_createThumb($Imagefile, $dest);
				$pictureThumb = "https://www.usracing.com/images/news/" . $newEntryId . "/" . $pathinfo['filename'] . "-thumb.jpg";
			}
			++$cont;
		}
		$source = $dom->saveHTML();
	}

	private function y_createMainPic($newEntryId, $image) {
		if ($image != '') {

			$imgcontent = file_get_contents($image);
			$Imagefile = NEWSABSPATH . "/images/" . $newEntryId . ".jpg";
			file_put_contents($Imagefile, $imgcontent);
			$dest = NEWSABSPATH . "/images/" . $newEntryId . "-otb.jpg";
			$this->y_createThumb($Imagefile, $dest);

			return true;
		}
		return false;
	}

	private function y_createThumb($src, $dest) {
		require_once NEWSABSPATH . "/cron/imageresizer.php";
		$sImg = new SimpleImage();
		$sImg->load($src);

		/* $Height = $sImg->getHeight();
		  $width = $sImg->getWidth();
		  $ratio = 150 / $width;
		  $Newheight = $Height * $ratio; */

		$sImg->resizeToWidth(350);
		$sImg->save($dest);
	}

	private function y_getNewEntryId() {
		$sql_highestId = " SELECT MAX(ContentId) as MAXContentIdART FROM newsarchive ";
		$result_highestId = mysql_query_w($sql_highestId);
		$data_highestId = mysql_fetch_object($result_highestId);
		$newEntryId = $data_highestId->MAXContentIdART + 1;
		return $newEntryId;
	}

	private function y_getNRecords($pageuri) {
		$sql = sprintf(" SELECT * FROM newsarchive WHERE pageuri = '%s' AND typetosee = 'OTH' ", $pageuri);
		$result = mysql_query_w($sql);
		$nrecords = mysql_num_rows($result);
		return $nrecords;
	}

	private function y_putNewsArchive(&$Sql_insert, $newEntryId, $title, $teaser, $datenewentry2, $datenewentry3, $pageuri, $picture, $pictureThumb, $featured = 0) {
		$Sql_insert = " INSERT INTO newsarchive SET 
               ContentId ='" . $newEntryId . "',
			   featured ='" . $featured . "',
               Title= '" . cleaninput($title) . "',
               Teaser ='" . cleaninputtreasure($teaser) . "',
               DateCreated = '" . $datenewentry2 . "',
               timecreate ='" . $datenewentry3 . "',
               `doc-id` ='Horse',
               pageuri ='" . trim($pageuri) . "',
			   picture ='" . $picture . "',
			   pictureThumb ='" . $pictureThumb . "',
               typetosee  ='OTH' ";
		if ($this->debug) {
			echo $Sql_insert . EOF;
		}
		if ($this->test) {
			return false;
		}
		return mysql_query_w($Sql_insert);
	}

	private function y_putNewsArticle(&$Sql_insert, $newEntryId, $title, $bodynewentry, $pageuri, $datenewentry2) {
		$Sql_insert = " INSERT INTO newsarticle SET 
				ContentIdART ='" . $newEntryId . "',
				TitleART= '" . cleaninput($title) . "',
				HtmlART = '" . cleaninput($bodynewentry) . "',
				url ='" . $pageuri . "',
				DateCreatedART ='" . $datenewentry2 . "' ";
		if ($this->debug) {
			echo $Sql_insert . EOF;
		}
		if ($this->test) {
			return false;
		}
		return mysql_query_w($Sql_insert);
	}

	private function y_reportError(&$issues, $pageuri, $titlenewentry, $datenewentry, $datenewentry2, $teaser, $bodynewentry, $sql1, $sql2, $possible_reason) {
		echo "Status: There has ocurred an error, and it was reported to Webmaster." . EOF;
		$issues[] = array("pageuri" => $pageuri, "title" => $titlenewentry, "date1" => $datenewentry, "date2" => $datenewentry2, "teaser" => $teaser, "contents" => $bodynewentry, "sql1" => $sql1, "sql2" => $sql2, "possible_reason" => $possible_reason);
	}

	private function y_reportWebmaster($issues) {
		$to = "webmaster.villegas@gmail.com";
		$from = "dr@scrapperusracing.com";
		$subject = "Master, there is a problem while scrapping from foxsports.com";

		$issues_msj = '<table border="1" style="border:1px solid black;">';
		foreach ($issues as $issueI) {
			$issues_msj .= '<tr><td>Reason: </td><td>' . $issueI['possible_reason'] . '</td></tr>';
			$issues_msj .= '<tr><td>Date 1: </td><td>' . $issueI['date1'] . '</td></tr>';
			$issues_msj .= '<tr><td>Date 2: </td><td>' . $issueI['date2'] . '</td></tr>';
			$issues_msj .= '<tr><td>Title: </td><td>' . $issueI['title'] . '</td></tr>';
			$issues_msj .= '<tr><td>Page Uri: </td><td>' . $issueI['pageuri'] . '</td></tr>';
			$issues_msj .= '<tr><td>Teaser: </td><td>' . $issueI['teaser'] . '</td></tr>';
			$issues_msj .= '<tr><td>contents: </td><td>' . $issueI['contents'] . '</td></tr>';
			$issues_msj .= '<tr><td>Sql 1: </td><td>' . $issueI['sql1'] . '</td></tr>';
			$issues_msj .= '<tr><td>Sql 2: </td><td>' . $issueI['sql2'] . '</td></tr>';
			$issues_msj .= '<tr><td colspan="2"><center>-seperator-</center></td></tr>';
		}
		$issues_msj .= '</table>';

		$message = "<html><body bgcolor=\"#DCEEFC\">
					<b>" . date("Y-m-d H:i:s") . "</b> <br> 
					Sorry to report <font color=\"red\">ERRORS</font> Master<br> 
					<a href=\"http://allhorse.com/programs/foxnews/foxnewscronp.php\">FoxNews Scrapper</a><br /> 
					<a href=\"https://www.usracing.com/Feeds/cron.php\">Usracing Feed Processor</a><br />
					<br><br>" . $issues_msj . "
				</body></html>";

		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: $from\r\n";
		//$headers .= "Cc: [email]maa@p-i-s.cXom[/email]"; 
		//$headers .= "Bcc: [email]email@maaking.cXom[/email]"; 

		if (mail($to, $subject, $message, $headers)) {
			echo EOF . "There were some problems, errors were reported to Webmaster." . EOF;
		} else {
			echo "Sending errors to Webmaster failed. Check whether mail destinatary is ok and the mail server is working properly.";
		}
	}

	function __destruct() {
		;
	}

}
//$y_ins = new YahooSportsScrapper(true, true, 3);
$y_ins = new YahooSportsScrapper();
$y_ins->main();
