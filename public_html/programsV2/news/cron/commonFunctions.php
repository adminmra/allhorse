<?php	 		 	

setlocale( LC_ALL, 'en_US.UTF8' );

function toAscii($str, $replace=array(), $delimiter='-') {
	if( !empty($replace) ) {
		$str = str_replace((array)$replace, ' ', $str);
	}

	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

	return $clean;
}

function cleaninput($output) {
	$output = preg_replace('/[^(\x20-\x7F)]*/','', $output);
	//$output =htmlspecialchars($output);
	$output =addslashes($output);
	return $output;
}

function cleaninputlinks($output) {
	$output = preg_replace('/[^(\x20-\x7F)]*/','', $output);
	$output = preg_replace('\b(https?|ftp|file)://[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|]', '', $output);
	//$output =htmlspecialchars($output);
	$output =addslashes($output);
	return $output;
}

function AbbrHtml($Html, $Len) {
	if (strlen($Html) > $Len) {
		return substr($Html, 0, $Len) . '...';
	} else {
		return $Html;
	}

}

function cleaninputtreasure( $output ) {
	$output = preg_replace('/[^(\x20-\x7F)]*/','', $output);
	$output =strip_tags($output);
	$output =htmlspecialchars($output);
	$output =addslashes($output);
	return AbbrHtml($output, '300');
}

?>