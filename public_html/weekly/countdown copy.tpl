
{php}
$startDate = strtotime("17 March 2015 12:00:00");
$endDate  = strtotime("29 August 2015 21:50:00");
{/php}

<div class="countdown margin-bottom-30">
<div class="headline"><h2>The Travers Stakes</h2></div>

<div class="panel">
	<div class="race">{* <span>Race:</span> *} <span>The $1.6 million/$1.25 Million, 146th <a href="/travers-stakes">Travers Stakes</a> is the crown race of <a href="/saratoga">Saratoga Racecourse</a>.</span></div>
	<div class="info"> <p><span>What we like:</span> The purse will be raised to $1.6 million if American Pharoah starts.</p><p><strong>Saratoga is known as the 'Graveyard of Champions': </strong>It's the place where triple crown winners lose.</p></div>
</div>

<div class="clock">
  <div class="item clock_days">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>      
     <div class="topLayer"><canvas id="canvas_days" width="188" height="188"> </canvas></div>      
      <div class="text">
        <p class="val">0</p>
        <p class="time type_days">Days</p>
      </div>
  </div>

  <div class="item clock_hours">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>
      <div class="topLayer"> <canvas id="canvas_hours" width="188" height="188"> </canvas></div>
      <div class="text">
      	<p class="val">0</p>
        <p class="time type_hours">Hours</p>
      </div>
  </div>  

  <div class="item clock_minutes">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>
      <div class="topLayer"><canvas id="canvas_minutes" width="188" height="188"> </canvas></div>
      <div class="text">
        <p class="val">0</p>
        <p class="time type_minutes">Minutes</p>
      </div>
  </div>
  
  <div class="item clock_seconds">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>
      <div class="topLayer"><canvas id="canvas_seconds" width="188" height="188"> </canvas></div>     
      <div class="text">
        <p class="val">0</p>
        <p class="time type_seconds">Seconds</p>
      </div>
  </div>
</div>

<br /><br /><br /><br /><br /><!-- delete these if you use/uncomment the button below -->
<!--<div class="blockfooter"><a class="btn btn-primary" href="/travers-stakes" target="_blank" rel="nofollow">Learn More <i class="fa fa-angle-right"></i></a></div>
</div><!-- end/countdown -->


<!-- Initialize the countdown -->
{literal}
<script type="text/javascript">
$(document).ready(function(){	
JBCountDown({ secondsColor : "#ffdc50", secondsGlow  : "none", minutesColor : "#9cdb7d", minutesGlow  : "none", hoursColor : "#378cff", hoursGlow    : "none",  daysColor    : "#ff6565", daysGlow     : "none", startDate   : "{/literal}{php} echo $startDate; {/php}{literal}", endDate : "{/literal}{php} echo $endDate; {/php}{literal}", now : "{/literal}{php} echo strtotime('now'); {/php}{literal}" });
 });
</script>
{/literal}


