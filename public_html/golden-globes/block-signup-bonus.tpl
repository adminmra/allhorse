<section class="card">
    <div class="card_wrap">
        <div class="card_half card_content">
            <a href="/promos/cash-bonus-10">
                {* <img src="/img/index-kd/icon-kentucky-derby-betting.png" alt="Bet on Kentucky Derby"
                    class="card_icon"> *}
                <img class="card_icon" src="/img/golden-globes/golden-globes-icon.png" alt="Golden Globes Awards Odds Offer Sign Up">
            </a>
            <h2 class="card_heading">Get a Sign Up Bonus up to $250 Cash</h2>
            <h3 class="card_subheading"><p>At BUSR, you are entitled to exceptional new member bonuses.</p>
            <p>For your first deposit with BUSR, you&#39;ll get an additional 10% bonus to your deposit. No Limits! Deposit a minimum of $100 and you could qualify to earn an additional $150! You can only win with BUSR!</p>
            <a href="/promos/cash-bonus-10" class="card_btn">Learn More</a>
        </div>
        <div class="card_half card_hide-mobile">
            <a href="/promos/cash-bonus-10"><img src="/img/golden-globes/signup-bonus.jpg" class="card_img" alt="Golden Globes Awards Odds Offer Sign Up"></a>
        </div>
    </div>
</section>