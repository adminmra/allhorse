<style>
    .first-th {
        width: 70%;
    }
</style>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Who will win the 2020 Golden Globe for Best Actor (Drama)?"
  }
</script>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Who will win the 2020 Golden Globe for Best Actor (Musical or Comedy)?"
  }
</script>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Who will win the 2020 Golden Globe for Best Actress (Drama)?"
  }
</script>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Who will win the 2020 Golden Globe for Best Actress (Musical or Comedy)?"
  }
</script>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Who will win the 2020 Golden Globe for Best Director?"
  }
</script>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "Who will win the 2020 Golden Globe for Best Picture (Drama)?"
  }
</script>
   <div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Ent - Golden Globe Awards">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id='updateemp'>Updated December 20, 2019.</em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Ent - Golden Globe Awards  - Jan 05                    </th>
            </tr>
            -->
    <tr><td colspan="3" class="center"><table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>   <br> <tr><th colspan="3" class="center">2020 Golden Globes - Best Actor (Drama)</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Joaquin Phoenix (Joker)</td><td>1/4</td><td>-400</td></tr><tr><td>Adam Driver (Marriage Story)</td><td>5/2</td><td>+250</td></tr><tr><td>Antonio Banderas (Pain & Glory)</td><td>15/2</td><td>+750</td></tr><tr><td>Christian Bale (Ford V Ferrari)</td><td>15/1</td><td>+1500</td></tr><tr><td>Jonathan Pryce (The Two Popes)</td><td>15/1</td><td>+1500</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>   <br> <tr><th colspan="3" class="center">2020 Golden Globes- Best Actor (Musical Or Comedy)</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Eddie Murphy (Dolemite Is My Name)</td><td>1/2</td><td>-200</td></tr><tr><td>Leonardo Dicaprio (Once Upon A Time In Hollywood)</td><td>3/2</td><td>+150</td></tr><tr><td>Taron Egerton (Rocketman)</td><td>9/2</td><td>+450</td></tr><tr><td>Daniel Craig (Knives Out)</td><td>13/2</td><td>+650</td></tr><tr><td>Roman Griffin Davis (Jojo Rabbit)</td><td>20/1</td><td>+2000</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>   <br> <tr><th colspan="3" class="center">2020 Golden Globes - Best Actress (Drama)</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Renee Zellweger (Judy)</td><td>1/3</td><td>-300</td></tr><tr><td>Scarlett Johansson (Marriage Story)</td><td>17/10</td><td>+170</td></tr><tr><td>Saoirse Ronan (Little Women)</td><td>10/1</td><td>+1000</td></tr><tr><td>Charlize Theron (Bombshell)</td><td>10/1</td><td>+1000</td></tr><tr><td>Cynthia Erivo (Harriet)</td><td>15/1</td><td>+1500</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>   <br> <tr><th colspan="3" class="center">2020 Golden Globes-best Actress(Musical Or Comedy)</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Awkwafina (The Farewell)</td><td>1/6</td><td>-600</td></tr><tr><td>Ana De Armas (Knives Out)</td><td>4/1</td><td>+400</td></tr><tr><td>Beanie Feldstein (Booksmart)</td><td>7/1</td><td>+700</td></tr><tr><td>Emma Thompson (Late Night)</td><td>10/1</td><td>+1000</td></tr><tr><td>Cate Blanchett (Where'd You Go Bernadette)</td><td>15/1</td><td>+1500</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>   <br> <tr><th colspan="3" class="center">2020 Golden Globes - Best Director</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Martin Scorsese (The Irishman)</td><td>5/6</td><td>-120</td></tr><tr><td>Bong Joon Ho (Parasite)</td><td>23/20</td><td>+115</td></tr><tr><td>Quentin Tarantino (Once Upon A Time In Hollywood)</td><td>7/2</td><td>+350</td></tr><tr><td>Sam Mendes (1917)</td><td>7/1</td><td>+700</td></tr><tr><td>Todd Phillips (Joker)</td><td>12/1</td><td>+1200</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o5t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>   <br> <tr><th colspan="3" class="center">2020 Golden Globes - Best Picture (Drama)</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>The Irishman</td><td>1/2</td><td>-200</td></tr><tr><td>Marriage Story</td><td>7/4</td><td>+175</td></tr><tr><td>1917</td><td>5/1</td><td>+500</td></tr><tr><td>Joker</td><td>6/1</td><td>+600</td></tr><tr><td>The Two Popes</td><td>15/1</td><td>+1500</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o6t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>   <br> <tr><th colspan="3" class="center">2020 Golden Globes-best Picture (Musical O Comedy)</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Once Upon A Time In Hollywood</td><td>2/7</td><td>-350</td></tr><tr><td>Jojo Rabbit</td><td>5/2</td><td>+250</td></tr><tr><td>Knives Out</td><td>6/1</td><td>+600</td></tr><tr><td>Rocketman</td><td>10/1</td><td>+1000</td></tr><tr><td>Dolemite Is My Name</td><td>10/1</td><td>+1000</td></tr></tbody></table></td></tr>            </tbody>
        </table>
    </div>
    <script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
    addSort('o2t');
    addSort('o3t');
    addSort('o4t');
    addSort('o5t');
    addSort('o6t');
    addSort('o7t');
    addSort('o8t');
    addSort('o9t');
</script>
