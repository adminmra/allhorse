<table style="height: 162px; width: 100%; border-collapse: collapse; border-style: solid; border-color: #dddddd; text-align: center;" border="1">
<tbody>
<tr style="background-color: #1360a0; color: #fff;">
<th style="height: 18px;">Method</th>
<th style="width: 150px; height: 18px;">Amount</th>
<th style="width: 100px; height: 18px;">Fees</th>
<th style="height: 18px;">Time Frame</th>
</tr>
<tr style="height: 18px;">
<td style="height: 54px;" rowspan="2">International Bank Wire</td>
<td style="height: 18px;">$100 - $500</td>
<td style="height: 18px;">$15</td>
<td style="height: 54px;" rowspan="2">Review: 2 business days + 5 to 7 business days to process</td>
</tr>
<tr style="height: 18px;">
<td style="height: 18px;">$500.01 - $5,000</td>
<td style="height: 18px;">$35</td>
</tr>
<tr style="height: 18px;">
<td style="height: 54px;" rowspan="2">eCheck</td>
<td style="height: 18px;">$50 - $200</td>
<td style="height: 18px;">$5</td>
<td style="height: 54px;" rowspan="2">Review: 2 business days + 10 to 15 business days to process</td>
</tr>
<tr style="height: 18px;">
<td style="height: 18px;">$201 - $500</td>
<td style="height: 18px;">$15</td>
</tr>
<tr style="height: 36px;">
<td style="height: 36px;">Bitcoin</td>
<td style="height: 36px;">$25 - $5,000</td>
<td style="height: 36px;">Free</td>
<td style="height: 36px;">3 Business Days Total<br />(2-days for Internal review and 1-day Processing time)</td>
</tr>
</tbody>
</table>