    <div>
        <noscript></noscript>
        <table class="data table table-condensed table-striped table-bordered order1" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="BOXING - Apr 03">
            <caption>BOXING - Apr 03</caption>
            <caption>WBO Junior Lightweight Title Bout (12 rounds)</caption>
            <caption>@ Caesers Bluewaters, Dubai - Dubai, UAE</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated March 30, 2021.</em>
                    </td>
                </tr>
            </tfoot>
            
            <tbody>
                            <tr><th>Team</th><th>Fractional</th><th>American</th></tr><tr><td>Jamel Herring</td><td>20/23</td><td>-115</td></tr><tr><td>Carl Frampton</td><td>20/23</td><td>-115</td></tr>                        </tbody>
        </table>
    </div>
    