<?php include '/home/ah/allhorse/public_html/autocontrol/libcontrol.menu.php'; ?>
<li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"  >Today's Racing<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
  <!--	<li><a href="/upcoming-horse-races"  >Upcoming Horse Races</a></li>-->
    
   
    <li><a href="/odds" >Today's Entries</a></li>
   <!--  <li><a href="/news/features/race-of-the-week" >Race of the Week</a></li> -->
     <li><a href="/results"  >US Racing Results</a></li> 
     <li><a href="/horse-racing-schedule" >Horse Racing Schedule</a></li>
      <li><a href="/harness-racing-schedule" >Harness Racing Schedule</a></li> 
      <li><a href="/graded-stakes-races"  > Stakes Races Schedule</a></li>
       <li><a href="/breeders-cup/challenge" >Breeders' Cup Challenge</a></li>
    
    <!--   
     
    
    <li><a href="/breeders-cup/odds" >Breeders&#039; Cup Odds</a></li>
          
           <li><a href="/kentucky-derby/props" >Kentucky Derby Props</a></li>
         <li><a href="/kentucky-oaks/odds" >Kentucky Oaks Odds</a></li>
         <li><a href="/breeders-cup/odds" >Breeders' Cup Odds</a></li>
         <li><a href="/bet-on/triple-crown" >Triple Crown Odds</a></li> -->
           <!--<li><a href="/casino/breeders-cup-blackjack-tournament"  >BC Blackjack Tournament</a></li>-->
     <li><a href="/racetracks"  >Racetracks</a></li>
     <!-- <li><a href="/past-performances" >Free Past Performances</a></li> -->
       
     <li><a href="/leaders/horses"  >Top Horses</a></li>
      <li><a href="/leaders/jockeys"  >Top Jockeys</a></li>
       <li><a href="/leaders/trainers"  >Top Trainers</a></li>
      <!-- <li><a href="https://www.guaranteedtipsheet.com/index.asp?afid=usr" target="_blank" rel="nofollow">Guaranteed Picks</a></li> -->
       
       
 <!--       <li><a href="/kentucky-derby/prep-races" >Kentucky Derby Prep Races</a></li> -->
     
<!--      <li><a href="/news/tag/pegasus-world-cup/" >Pegasus World Cup</a></li> -->
     
     
    <!--
  <li><a href="/news"  >News</a></li>
       <li><a href="/horse-racing-videos"  >Horse Racing Videos</a></li>
-->
        <!-- <li><a href="/blog/" >US Racing Blog</a></li>
             <li><a href="/signup/?todays-racing=Signup-Today">Sign Up Today!</a></li> -->
    <!--      
  
    <li><a href="/racetracks"  >Our Racetracks</a></li>
    <li><a href="/news" >Horse Racing News</a></li>
    <li><a href="/about" >About Us</a></li>
    <li><a href="/graded-stakes-races" >Races to Watch</a></li>
    <li><a href="/" title="Online Horse Betting">Online Horse Betting</a>
    <li><a href="/how-to/place-a-bet" title="How To Place A Bet" rel="nofollow">How To Place A Horse Bet</a></li>
    <li><a href="/rebates" title="Horse Raceing Rebates">Rebates</a></li>
    <li><a href="/racetracks" title="Horse Racetracks">Horse Racetracks</a></li>
    <li><a href="/famous-jockeys" title="Famous Horses, Jockeys &amp; Trainers" rel="nofollow">Famous Jockeys</a></li>
    <li><a href="/advance-deposit-wagering" title="Advance Deposit Wagering">Advance Deposit Wagering</a></li>
    <li><a href="/horse-racing-games" title="Horse Racing Games">Horse Racing Game</a></li>
    <li><a href="/haskell-stakes" title="Stakes Races">Stakes Races</a></li>
    <li><a href="/haskell-stakes" title="Haskell Stakes">Haskell Stakes</a></li>
    <li><a href="/travers-stakes" title="Travers Stakes">Travers Stakes</a></li>
    <li><a href="/santa-anita-derby" title="Santa Anita Derby">Santa Anita Derby</a></li>
    <li><a href="/arkansas-derby" title="Arkansas Derby">Arkansas Derby</a></li>
    <li><a href="/illinois-derby" title="Illinois Derby">Illinois Derby</a></li>
    <li><a href="/florida-derby" title="Florida Derby">Florida Derby</a></li>
    <li><a href="/bluegrass-stakes" title="Blue Grass Stakes">Blue Grass Stakes</a></li>
    <li><a href="/racetracks" title="Handicapping">Popular Racetracks</a></li>
    <li><a href="/belmont-park" title="">Belmont Park</a></li>
    <li><a href="/delmar" title="">Delmar</a></li>
    <li><a href="/churchill-downs" title="">Churchill Downs</a></li>
    <li><a href="/gulfstream-park" title="">Gulfstream Park</a></li>
    <li><a href="/pimlico" title="">Pimlico</a></li>
    <li><a href="/santa-anita-park" title="">Santa Anita Park</a></li>
    <li><a href="/saratoga" title="">Saratoga</a></li>
    -->
  </ul>
</li>


<li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"><!-- Latest --> Odds<i class="fa fa-angle-down"></i></a>
  <?php if ($bcODDmenuM=='Y' and $bcjODDmenuM=='Y'){
  echo '<ul class="dropdown-menu newnewOdds" id="double">';
  }
   if ($bcODDmenuM=='Y' and $bcjODDmenuM=='N'){
  echo '<ul class="dropdown-menu newnewOddstwo" id="double">';
  }
   if ($bcODDmenuM=='N' and $bcjODDmenuM=='Y'){
  echo '<ul class="dropdown-menu newnewOddstwo" id="double">';
  }
 if ($bcODDmenuM=='N' and $bcjODDmenuM=='N'){
  echo '<ul class="dropdown-menu" id="double">';
 }
 
 ?>
   
   <!--
 <li><a href="/bet-on/american-pharoah"  >Bet American Pharoah</a></li>
       
-->
<!--<li><a href="/odds/ascot">Ascot Horse Racing Odds</a></li>-->
<!--<li><a href="/odds"  >Today's Horse Racing Odds</a></li> -->
<!-- <li><a href="/preakness-stakes/odds">2018 Preakness Stakes Odds</a></li> -->
<!-- <li><a href="/kentucky-derby/odds">2019 Kentucky Derby Odds</a></li> -->
<!-- <li><a href="/kentucky-derby/trainer-betting"  >Kentucky Derby Odds: Trainers</a></li> -->
<!-- <li><a href="/kentucky-derby/jockey-betting"  >Kentucky Derby Odds: Jockeys</a></li> -->
<!-- <li><a href="/kentucky-derby/props" >Kentucky Derby Odds: Props</a></li> -->
<!-- <li><a href="/kentucky-derby/match-races">Kentucky Derby Odds:  Match Races</a></li> -->
<!-- <li><a href="/kentucky-derby/margin-of-victory">Kentucky Derby Odds: Margin of Victory </a></li> -->
<!-- <li><a href="/kentucky-oaks/odds">Kentucky Oaks Odds</a></li> -->

<!-- <li><a href="/breeders-cup/odds">Breeders' Cup Odds</a></li>
 -->
<!--<li><a href="/melbourne-cup"  >Melbourne Cup Odds</a></li> -->
<!-- <li><a href="/breeders-cup/distaff">Breeders' Cup Distaff</a></li>-->
<!-- <li><a href="/bet-on/triple-crown">Triple Crown Odds</a></li> -->
 <!-- <li><a href="/pegasus-world-cup/odds"  >Pegasus World Cup Odds</a></li> -->
 <!--  <li><a href="/belmont-stakes/odds"  >Belmont Stakes Odds</a></li>     --> 
      
  <!--  <li><a href="/preakness-stakes/odds"  >Preakness Stakes Odds</a></li> -->
<!-- <li><a href="/prix-de-larc-de-triomphe"  >Prix de l'Arc de Triomphe Odds</a></li>-->
<!-- <li><a href="/grand-national"  >Grand National Odds</a></li> -->
			
            <!-- <li><a href="/ascot-gold-cup"  >Royal Ascot Horse Racing Odds</a></li> -->
			<!-- <li><a href="/queen-anne-stakes"  >Queen Anne Stakes Betting Odds</a></li> -->
			<!--<li><a href="/diamond-jubilee-stakes"  >Diamond Jubilee Stakes Betting</a></li> -->

<!-- <li><a href="/dubai-world-cup" >Dubai World Cup Odds</a></li> -->
	
<!-- <li><a href="/bet-on/triple-crown">Triple Crown Odds</a></li> -->
<!--			<li><a href="/odds/us-presidential-election">US Presidential Election Odds</a></li> -->

<!--<li><a href="/breeders-cup/turf">Breeders' Cup Turf Odds</a></li>	-->	 											<!-- Saturday -->

<!-- <li><a href="/breeders-cup/juvenile"  >BC Juvenile Odds</a></li>		-->											<!-- Saturday -->
<!--<li><a href="/breeders-cup/juvenile-fillies-turf"  >BC Juvenile Fillies Turf</a></li>	-->							<!-- Friday -->
<!-- <li><a href="/breeders-cup/distaff"  >BC Distaff Odds</a></li>			 -->											<!-- Friday --> 

<div class="bc-menu-title">All Odds</div>
<!--<li><a href="/odds/ascot">Ascot Horse Racing Odds</a></li>-->
<li><a href="/odds"  >Today's Horse Racing Odds</a></li>
<!-- <li><a href="/preakness-stakes/odds">2018 Preakness Stakes Odds</a></li> -->
<li><a href="/kentucky-derby/odds">2019 Kentucky Derby Odds</a></li>
<!-- <li><a href="/kentucky-derby/trainer-betting"  >Kentucky Derby Odds: Trainers</a></li> -->
<!-- <li><a href="/kentucky-derby/jockey-betting"  >Kentucky Derby Odds: Jockeys</a></li> -->
<!-- <li><a href="/kentucky-derby/props" >Kentucky Derby Odds: Props</a></li> -->
<!-- <li><a href="/kentucky-derby/match-races">Kentucky Derby Odds:  Match Races</a></li> -->
<!-- <li><a href="/kentucky-derby/margin-of-victory">Kentucky Derby Odds: Margin of Victory </a></li> -->
<!-- <li><a href="/kentucky-oaks/odds">Kentucky Oaks Odds</a></li> -->

<!--<li><a href="/breeders-cup/odds">Breeders' Cup Odds</a></li>-->
<!--<li><a href="/melbourne-cup"  >Melbourne Cup Odds</a></li> -->
<!-- <li><a href="/breeders-cup/distaff">Breeders' Cup Distaff</a></li> -->
 <!-- <li><a href="/pegasus-world-cup/odds"  >Pegasus World Cup Odds</a></li> -->

 <!--  <li><a href="/belmont-stakes/odds"  >Belmont Stakes Odds</a></li>     -->

  <!--

    <li><a href="/preakness-stakes/odds"  >Preakness Stakes Odds</a></li>

-->
<li><a href="/bet-on/triple-crown">Triple Crown Odds</a></li>
<!-- <li><a href="/prix-de-larc-de-triomphe"  >Prix de l'Arc de Triomphe Odds</a></li> -->
<li><a href="/grand-national"  >Grand National Odds</a></li>

            <!-- <li><a href="/ascot-gold-cup"  >Royal Ascot Horse Racing Odds</a></li> -->
                        <!-- <li><a href="/queen-anne-stakes"  >Queen Anne Stakes Betting Odds</a></li> -->
                        <!--<li><a href="/diamond-jubilee-stakes"  >Diamond Jubilee Stakes Betting</a></li> -->

 <li><a href="/dubai-world-cup" >Dubai World Cup Odds</a></li>
<li class="bc-nemu-last"><a href="/odds/us-presidential-election">US Presidential Election Odds</a></li>

<?php if ($bcODDmenuM=='Y'){ ?>
<li class="bc-nemu-empty"><a>&nbsp;</a></li>
<li class="bc-nemu-empty"><a>&nbsp;</a></li>
<li class="bc-nemu-empty"><a>&nbsp;</a></li>
<?php } ?>

<?php if ($bcjODDmenuM=='Y'){ ?>
        <div class="bc-menu-title">Breeders' Cup Nov. 2nd</div>
        <li><a href="/breeders-cup/juvenile"  >BC  Juvenile  Odds</a></li>
        <li><a href="/breeders-cup/juvenile-turf" >BC  Juvenile Turf Odds</a></li>
        <li><a href="/breeders-cup/juvenile-fillies-turf"  >BC  Juvenile Fillies Turf Odds</a></li>
        <li><a href="/breeders-cup/juvenile-fillies"  >BC  Juvenile Fillies Odds</a></li>
                <li class="bc-nemu-last"><a href="/breeders-cup/juvenile-turf-sprint" >BC  Juvenile Turf Sprint Odds</a></li>
        <li class="bc-nemu-empty"><a>&nbsp;</a></li>
        <?php if ($bcODDmenuM=='Y'){ ?>
		<li class="bc-nemu-empty"><a>&nbsp;</a></li>
         <li class="bc-nemu-empty"><a>&nbsp;</a></li>
          <li class="bc-nemu-empty"><a>&nbsp;</a></li>
          
          <?php } ?>
          <?php } ?>

<?php if ($bcODDmenuM=='Y'){ ?>
        <div class="bc-menu-title">Breeders' Cup Nov. 3rd</div>

        <li><a href="/breeders-cup/classic"  >Breeders' Cup Odds</a> </li>                                                                                                      <!-- Saturday -->
                <li><a href="/breeders-cup/turf"  >BC Turf  Odds</a></li>
        <li><a href="/breeders-cup/distaff"  >BC Distaff  Odds</a></li>
        <li><a href="/breeders-cup/mile" >BC  Mile Odds</a></li>
        <li><a href="/breeders-cup/sprint"  >BC Sprint Odds</a></li>
        <li><a href="/breeders-cup/filly-mare-turf"  >BC  Filly and Mare Turf Odds</a></li>
        <li><a href="/breeders-cup/dirt-mile"  >BC  Dirt Mile Odds</a></li>
        <li><a href="/breeders-cup/turf-sprint"  >BC  Turf Sprint Odds</a></li>
        <li><a href="/breeders-cup/filly-mare-sprint"  >BC  Filly and Mare Sprint Odds</a></li>

 <?php } ?>

  </ul>
</li>
<!--Horse Racing News ================================================ -->   
<li class="dropdown"><!-- <a href="/news" >Racing News</a> -->
     
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" ><!-- Latest --> News<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">

<li><a href="/news">Latest Racing News </a></li>


<!-- <li><a href="/news/kentucky-derby-road-to-the-roses">Kentucky Derby News</a></li> -->
<!-- <li><a href="/news/tag/kentucky-derby-contenders/">Kentucky Derby Contenders</a></li>  -->
<!-- <li><a href="/news/kentucky-derby-road-to-the-roses/kentucky-derby-workout-report">Kentucky Derby Workouts</a></li>  -->
<!-- <li><a href="/news/kentucky-derby-road-to-the-roses/kentucky-derby-news-notes">Kentucky Derby Notes</a></li>  -->
<!--  <li><a href="/news/winning-speed-figures">Kentucky Derby Speed Ratings</a></li>  -->
<!-- <li><a href="/news/tag/kentucky-derby-handicapping">Kentucky Derby Handicapping</a></li>  -->


 
	
<!-- 	<li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li> -->
<!--	   <li><a href="/preakness-stakes/results" >Preakness Stakes Results</a></li>  
   <li><a href="/belmont-stakes/results" >Belmont Stakes Results</a></li>  
	
	
	<li><a href="/news/tag/preakness-stakes-news">Preakness Stakes News </a></li>  -->
<!--
<li><a href="/news/tag/preakness-stakes-favorites">Preakness Stakes Favorites </a></li> 
<li><a href="/news/tag/preakness-stakes-contenders">Preakness Stakes Contenders </a></li> 
-->
<!-- <li><a href="/news/tag/preakness-stakes-workouts">Preakness Stakes Workouts </a></li>  -->


<!-- <li><a href="/news/breeders-cup/">Breeders' Cup News </a></li>  
<li><a href="/news/tag/triple-crown">Triple Crown News </a></li> -->


	    


<li><a href="/news/features">Featured Stories </a></li>
<li><a href="/news/analysis">Racing Analysis </a></li>
<li><a href="/news/handicapping-reports">Handicapping & Picks </a></li>
<li><a href="/news/harness-racing">Harness Racing</a></li>
<li><a href="/news/recap">Race Recaps</a></li>
<li><a href="/news/horse-racing-cartoon">Day in the Life - Cartoons</a></li>

<!-- <li><a href="/news/breeders-cup">Breeders' Cup</a></li> -->
<!-- <li><a href=""> </a></li>  -->

      
     </ul>
</li>


<?php if ($bcmenuM=='Y' or $bcjmenuM=='Y'){ ?>
<li class="dropdown">
<a href="/breeders-cup" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" >Breeders&#039; Cup <i class="fa fa-angle-down"></i></a>

<?php if ($bcmenuM=='Y' and $bcjmenuM=='Y'){
    echo '<ul class="dropdown-menu newnew" id="double">';
	}
   if ($bcmenuM=='Y' and $bcjmenuM=='N'){ 
   echo '<ul class="dropdown-menu newnewtwo" id="double">';
  }
   if ($bcmenuM=='N' and $bcjmenuM=='Y'){ 
   echo '<ul class="dropdown-menu newnewtwo" id="double">';
  }
   if ($bcmenuM=='N' and $bcjmenuM=='N'){ 
  echo '<ul class="dropdown-menu" id="double">';
  }
  ?>


    
    <!-- <li><a href="/breeders-cup" >2018  Breeders' Cup</a></li>
        <li><a href="/breeders-cup/classic"  >Breeders&#039; Cup Classic </a>															
        <li><a href="/breeders-cup/distaff"  >Breeders&#039; Cup Distaff </a></li>
        <li><a href="/breeders-cup/dirt-mile"  >Breeders&#039; Cup Dirt Mile</a></li>
        <li><a href="/breeders-cup/filly-mare-turf"  >Breeders&#039; Cup Filly and Mare Turf</a></li>
        <li><a href="/breeders-cup/turf-sprint"  >Breeders&#039; Cup Turf Sprint Odds</a></li>
        <li><a href="/breeders-cup/mile" >Breeders&#039; Cup Mile Odds</a></li>
        <li><a href="/breeders-cup/juvenile"  >Breeders&#039; Cup Juvenile </a></li>
        <li><a href="/breeders-cup/juvenile-turf" >Breeders&#039; Cup Juvenile Turf</a></li>
        <li><a href="/breeders-cup/juvenile-fillies-turf"  >Breeders&#039; Cup Juvenile Fillies Turf</a></li>
        <li><a href="/breeders-cup/juvenile-fillies"  >Breeders&#039; Cup Juvenile Fillies Odds</a></li>
        <li><a href="/breeders-cup/turf"  >Breeders&#039; Cup Turf </a></li>
        <li><a href="/breeders-cup/betting" >Breeders&#039; Cup Betting</a></li>
        <li><a href="/news/breeders-cup" >Latest Updates</a>
        <li><a href="/breeders-cup/odds" >Breeders&#039; Cup Odds</a></li>
        <li><a href="/news/tag/breeders-cup-contenders/"  >Breeders&#039; Contenders</a></li>
        <li><a href="/breeders-cup/challenge" >Breeders&#039; Cup Challenge</a></li> -->
        <!-- <li class="border-bot"><a href="/dubai-world-cup" >Dubai World Cup Odds</a></li> -->
        <!-- <li class="border-right"><a href="/bet-on/breeders-cup"  >Bet on Breeders' Cup</a></li> -->

        <div class="bc-menu-title">Breeders' Cup</div>
        <li><a href="/bet-on/breeders-cup"  >Bet on Breeders' Cup</a></li>
        <li><a href="/breeders-cup" >2018  Breeders' Cup</a></li>
        <li><a href="/breeders-cup/betting" >Breeders&#039; Cup Betting</a></li>
        <li><a href="/news/breeders-cup" >Latest Updates</a>
        <li><a href="/breeders-cup/odds" >Breeders&#039; Cup Odds</a></li>
        <li><a href="/news/tag/breeders-cup-contenders/"  >Breeders&#039; Contenders</a></li>
        <li class="bc-nemu-last"><a href="/breeders-cup/challenge" >Breeders&#039; Cup Challenge</a></li>
        <?php if ($bcmenuM=='Y'){ ?>
        <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                <?php } ?>

<!--          <li class="bc-nemu-empty"><a>&nbsp;</a></li>   -->

 <?php if ($bcjmenuM=='Y') { ?>
        <div class="bc-menu-title">Friday Nov. 2nd</div>
        <li><a href="/breeders-cup/juvenile"  > Juvenile </a></li>
        <li><a href="/breeders-cup/juvenile-turf" >Juvenile Turf</a></li>
        <li><a href="/breeders-cup/juvenile-fillies-turf"  >Juvenile Fillies Turf</a></li>
        <li><a href="/breeders-cup/juvenile-fillies"  >Juvenile Fillies</a></li>
        <li class="bc-nemu-last"><a href="/breeders-cup/juvenile-turf-sprint" >Juvenile Turf Sprint</a></li>
        <li class="bc-nemu-empty"><a>&nbsp;</a></li>
        <li class="bc-nemu-empty"><a>&nbsp;</a></li>
        <?php if ($bcmenuM=='Y'){ ?>
         <li class="bc-nemu-empty"><a>&nbsp;</a></li>
          <li class="bc-nemu-empty"><a>&nbsp;</a></li>
		<?php } ?>
         <?php } ?>
         <?php if ($bcmenuM=='Y'){ ?>
         <div class="bc-menu-title">Saturday Nov. 3rd</div>

        <li><a href="/breeders-cup/classic">Classic <!--<span class=“sub-link-hour”>@ 4PM</span>--></a></li>    <!-- <span class=“sub-link-hour”>@ 4PM</span> -->

    <!--<div class="bc-menu-title-lower">@ 4PM</div>  -->                                              <!-- Saturday -->
        <li><a class="turfBC" href="/breeders-cup/turf"  >Turf </a></li>
        <li><a href="/breeders-cup/distaff"  > Distaff </a></li>
        <li><a href="/breeders-cup/mile" >Mile</a></li>
        <li><a href="/breeders-cup/sprint"  >Sprint</a></li>
        <li><a href="/breeders-cup/filly-mare-turf"  >Filly and Mare Turf</a></li>
        <li><a href="/breeders-cup/dirt-mile"  >Dirt Mile</a></li>
        <li><a href="/breeders-cup/turf-sprint"  >Turf Sprint</a></li>
        <li class="bc-nemu-last"><a href="/breeders-cup/filly-mare-sprint"  >Filly and Mare Sprint</a></li>
        <?php } ?>
    </ul>
</li>

<?php } ?>

<!-- How to Bet ================================================ -->   
<!-- <li class="dropdown"><a href="/how-to/bet-on-horses" >How to Bet</a> -->
     
     <li class="dropdown"><!-- <a href="/news" >Racing News</a> -->
     
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" >How to Bet<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">

<li><a href="/how-to/bet-on-horses">How to Bet on Horses </a></li>
<li><a href="/news/horse-betting-101/">Handicapping Tips</a></li>
<!-- <li><a href="https://www.guaranteedtipsheet.com/index.asp?afid=usr" target="_blank" rel="nofollow">Guaranteed Picks</a></li>  -->   
<li><a href="/news/kentucky-derby-betting-cryptocurrency-increases-popularity" >Betting with Bitcoin</a></li>     
    
<!--<ul class="dropdown-menu">

<li><a href="/how-to/wager-on-horses" title="How to wager on Horses">How to Wager on Horses</a></li>
<li><a href="/how-to/bet-on-horses">How to Bet on Horses</a></li>
<li><a href="/how-to/read-the-daily-racing-form">How to Read the Daily Racing Form</a></li>
<li><a href="/how-to/read-past-performances" >Past Performances</a></li>
<li><a href="/horse-betting/beginner-Tips" title="Beginner Horse Betting Tips" >Beginner Tips</a></li>
<li><a href="/horse-racing-terms" title="Horse Racing Terms and Terminology">Horse racing terms</a></li>



<li><a href="/horse-betting/straight-wager" title="How to Place a Straight Wager" rel="nofollow">Straight Wager</a></li>
<li><a href="/horse-betting/win" title="Win, Place and Show Bets" rel="nofollow">Win - </a><a href="/horse-betting/place" title="Win, Place and Show Bets" rel="nofollow"> Place - </a><a href="/horse-betting/Show" title="Win, Place and Show Bets" rel="nofollow">Show</a></li>

<li><a href="/horse-betting/exacta" title="Exacta Horse Bets" rel="nofollow">Exacta</a></li>
<li><a href="/horse-betting/trifecta" title="Trifecta Horse Bets" rel="nofollow">Trifecta</a></li>
<li><a href="/horse-betting/superfecta" title="Superfecta Horse Bets" rel="nofollow">Superfecta</a></li>
<li><a href="/horse-betting/quinella" title="Quinella Horse Bets" rel="nofollow">Quinella</a></li>
<li><a href="/horse-betting/daily-double" title="Daily Double Horse Bets" rel="nofollow">Daily Double</a></li>
<li><a href="/horse-betting/exotic" title="Exotic Horse Bets" rel="nofollow">Exotic Horse Bet</a></li>





<li><a href="#" title="#" rel="nofollow">Instructional Videos</a></li>
<li><a href="/how-to/bet-on-horses" title="" >How to Bet on Horses</a></li>

<li><a href="/how-to/read-the-daily-racing-form" title="">How to Read the Daily Racing Form</a></li>
<li><a href="/how-to/read-past-performances" title="" rel="nofollow">Past Performances</a></li>

<li><a href="#" title="Advanced Horse Bets" rel="nofollow">Advanced Horse Bets</a></li>
<li><a href="/horse-betting/across-the-board" title="Across the Board Bets" rel="nofollow">Across the Board</a></li>
<li><a href="/horse-betting/place-Pick-all" title="Place Pick All Bets" rel="nofollow">Place Pick All</a></li>
<li><a href="/horse-betting/box-bet" title="Box Horse Bets" rel="nofollow">Box Bets</a></li>
<li><a href="/horse-betting/key-a-Horse" title="Key a Horse" rel="nofollow">Key a Horse</a></li>
<li><a href="/horse-betting/pick-three" title="Pick 3 Horse Bets" rel="nofollow">Pick Three</a></li>
<li><a href="/horse-betting/pick-four" title="Pick 4 Bets" rel="nofollow">Pick Four</a></li>
<li><a href="/horse-betting/pick-six" title="Pick 6 Horse Bets" rel="nofollow">Pick Six</a></li>
<li><a href="/horse-racing-terms" title="Horse Racing Terms and Terminology">Horse Racing Terms</a></li>

<li><a href="#" title="Horse Betting 101" rel="nofollow">Horse Betting 101</a></li>
<li><a href="/horse-betting/beginner-Tips" title="Beginner Horse Betting Tips" rel="nofollow">Beginner Tips</a></li>
<li><a href="/legend" title="" rel="nofollow">Horse Racing Legend</a></li>
<li><a href="/horse-betting/thoroughbred-Tips" title="" rel="nofollow">Thoroughbred Tips</a></li>
<li><a href="/horse-betting/quarter-Horse-Tips" title="" rel="nofollow">Quarter Horse Tips</a></li>
<li><a href="/horse-betting/money-management" title="" rel="nofollow">Money Management</a></li>
<li><a href="/horse-betting/claiming-race" title="Claiming Race" >Claiming Race</a></li>
<li><a href="/horse-betting/maiden-race" title="Maiden Race" rel="nofollow">Maiden Race</a></li>
<li><a href="/horse-betting/allowance-race" title="Allowance Races" rel="nofollow">Allowance Races</a></li>
<li><a href="/horse-betting/starter-allowance" title="Starter Allowance" rel="nofollow">Starter Allowance</a></li>


<li><a href="/horse-betting/handicapping-stakes-races" title="Stakes and handicap" rel="nofollow">Stakes &amp; Handicapping</a></li>

    -->     
      
     </ul>
</li>

     
     


<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true" >Promos <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
 <!-- <li><a href="/promos/10-reload-bonus">Belmont Reload Bonus</a></li>-->
    <li><a href="/rebates">8% Rebates</a></li>
     <li><a href="/promos/cash-bonus-10">10% Signup Bonus</a></li>
 <li><a href="/promos/cash-bonus-150">$150 Member Bonus</a></li>
 		<li><a href="/promos/free-bet"  >Free Bets for Members</a></li>
 <!--  <li><a href="/kentucky-derby/free-bet">Free Bet</a></li>  -->
   <!--<li><a href="/promos/free-bet">Free Bet</a></li> -->
 <!--   <li><a href="/breeders-cup/free-bet">Free Breeders' Cup Bet</a></li>  -->
<!--  <li><a href="/belmont-stakes/free-bet">Free Belmont Bet</a></li>-->
   <li><a href="/promos/casino-rebate">50% Casino Cash Back</a></li>
<!--  <li><a href="/promos/blackjack-tournament">Blackjack Tournament</a></li> -->

    <!-- <li><a href="/promos/race-of-the-week">Race of the Week</a></li> -->
     <!-- <li><a href="/promos/blackjack-tournament">KD Blackjack Tournament</a></li> -->
<!--     <li><a href="/refer-a-friend">Refer a Friend Bonus</a></li> -->
<!--     <li><a href="/promos/derby-wager-guide">Kentucky Derby Betting Guide</a></li> 
         <li><a href="/super-bowl" >Super Bowl Deposit Bonus</a></li> -->
     <li><a href="/getting-started">Have a Promo Code?</a></li>
 
  </ul>
</li>
<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true" >Learn More <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
	  <li><a href="/getting-started" >Getting Started</a></li>
    <li><a href="/about" >About Us</a></li> 
    <li><a href="/awards" >Award Winning News</a></li>
    <!--<li><a href="/legal-online-horse-betting" title="Legal Horse Betting">Legal Horse Betting</a></li>-->
    <li><a href="/best-horse-racing-site" >Best Horse Racing Site</a></li>
     <li><a href="/mobile-horse-betting">Mobile Betting</a></li>
     <li><a href="/racetracks">Racetracks Offered</a></li>
     <li><a href="/states-accepted">States Accepted</a></li>
     <!--  <li><a href="/states-accepted">States Accepted</a></li> -->
   <!--  <li><a href="/cash-bonus">$150 Signup Bonus</a></li> -->
    <li><a href="/rebates">Daily Rebates</a></li>
    <!-- <li><a href="/payouts">Super Fast Payouts</a></li> -->
    
   <!--  <li><a href="/getting-started">Have a Promo Code?</a></li> -->
  <!--  <li><a href="#">Tell Your Friends</a></li>
   <li><a href="#">Our Guarantee</a></li>-->
   <li><a href="/support">Contact Us</a></li>
    <li><a href="/signup/?m=Join-Today">Join Today!</a></li>
    <!--<li><a href="/blog" >Blog</a></li>-->
    <!--
     <li><a href="/live-horse-racing" >Live Racing Video</a></li>
    <li><a href="/mobile-horse-betting" >Mobile Betting</a></li>
    <li><a href="/racetracks" title="Racetracks">Racetracks</a></li>
    <li><a href="/today" title="Today&#039;s Tracks">Today&#039;s Tracks</a></li>
    <li><a href="/results" title="Horse Racing Results" >Horse Racing Results</a></li>
    <li><a href="/graded-stakes-races" title="Graded Stakes Races" >Graded Stakes Races</a></li>
    <li><a href="/news" title="Horse Racing News">Horse Racing News</a></li>
    <li><a href="/contact" title="Contact Us">Contact Us</a></li>
    <li><a href="/live-video" title="Live Video.">Live Video</a></li>
    <li><a href="/mobile-betting" title="Mobile Horse Betting">Mobile Betting</a></li>
    <li><a href="/handicapping" title="Horse Racing Handicapping">Handicapping</a></li>
    -->
  </ul>
</li>
