	{literal}
			<script type="application/ld+json">
			{
			  "@context": "http://schema.org",
			  "@type": "Table",
			  "about": "Super Bowl"
			}
		</script> 
	{/literal}
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Superbowl 54 - Coin Toss"  summary="Super Bowl">
            <caption>Superbowl 54 - Coin Toss</caption>
			<tfoot>
                    <tr>
                        <td class="dateUpdated center" colspan="3">
                            <em id='updateemp'>Updated January 29, 2020.</em>
                        </td>
                    </tr>			
			</tfoot>
            <tbody>
               <!--
 <tr>
                    <th colspan="3" class="center">
                    Superbowl 54 - Coin Toss  - Feb 02                    </th>
            </tr>
-->
               <!--
 <tr>
                    <th colspan="3" class="center">
                    Superbowl 54<br />Coin Toss                    </th>
            </tr>
-->
    <tr class='head_title' ><th>Team</th><th>American Odds</th><th>Fractional Odds</th></tr>
    <tr>
    <td>San Francisco 49ers</td>
    <td>21/10</td>
    <td>+210</td>
</tr>
<tr>
    <td>Kansas City Chiefs</td>
    <td>33/10</td>
    <td>+330</td>
</tr>
            </tbody>

        </table>
    </div>
    {literal}
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>

        {/literal}	
    
