    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Sb Passing Props">
            <caption>Patrick Mahomes - Total Passing Yards</caption>
            <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="4">
                  <em id='updateemp'>Updated <?php echo date("F j, Y");?>.</em> 
                </td>
              </tr>
            </tfoot>
            <tbody>
    <tr><th>Team</th><th>Spread</th><th>Moneyline</th><th>Total</th></tr>
    <tr><td>P Mahomes Pass Yds Over 299 ½</td><td></td><td></td><td>-135</td></tr>
   <tr><td>P Mahomes Pass Yds Under 299 ½</td><td></td><td></td><td>+105</td></tr> 
    </tbody>
        </table>
    </div>
    
