    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Sb - Touchdowns">
            <caption>Touchdowns</caption>
            <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="3">
                  <em id='updateemp'>Updated <?php echo date("F j, Y");?>.</em> 
                </td>
              </tr>
            </tfoot>
            <tbody>
    				<tr>
					<th colspan="3" class="center">
					Touchdowns  - Feb 02					</th>
			</tr>
    <tr><th colspan="3" class="center">49ers Vs Chiefs - When Will 1st Touchdown Happen</th></tr><tr><th></th><th>Fractional</th><th>American</th></tr><tr><td>1st Quarter</td><td>1/5</td><td>-500</td></tr><tr><td>2nd Quarter</td><td>16/5</td><td>+320</td></tr><tr><td>3rd Quarter</td><td>18/1</td><td>+1800</td></tr><tr><td>4th Quarter</td><td>35/1</td><td>+3500</td></tr><tr><td>No Td</td><td>50/1</td><td>+5000</td></tr>            </tbody>
        </table>
    </div>
    