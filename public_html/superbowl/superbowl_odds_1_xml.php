<style>
    @media (max-width: 355px) {
        .th_team {
            width: 94px;
        }
    }
</style>

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "about": "Super Bowl LIV",
        "subjectOf": "Super Bowl LIV",
        "keywords": "Super Bowl Odds, Vegas Odds Super Bowl, Bet the Super Bowl, Super Bowl Betting, super bowl 54 predictions, Super Bowl prop bets, Super Bowl Final, super bowl betting, Super Bowl props, Super Bowl best props, Super Bowl promo, Super Bowl Predictions"
    }
</script> 

    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Nfl"  summary="Super Bowl">
            <caption>Super Bowl LIV</caption>
			<tfoot>
                    <tr>
                        <td class="dateUpdated center" colspan="4">
                            <em id='updateemp'>Updated <?php echo date("F j, Y");?>.</em>
                        </td>
                    </tr>			
			</tfoot>
            <tbody>
                <tr class="head_title">
                    <th class="th_team">Team</th>
                    <th>Spread</th>
                    <th>Moneyline</th>
                    <th>Total</th>
                </tr>
                <tr>
                    <td>San Francisco 49ers</td>
                    <td>-105</td>
                    <td>+100</td>
                    <td>-110</td>
                </tr>
                <tr>
                    <td>Kansas City Chiefs</td>
                    <td>-115</td>
                    <td>-125</td>
                    <td>-110</td>
                </tr>
            </tbody>
        </table>
    </div>
    

