function validateEmail(email) { 
	var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return reg.test(email);
}

$(document).ready(function() {
	$(".modalbox").fancybox();
	$("#contact").submit(function() {
		return false;
	});
	
	$("#send").on("click", function(){
		var emailval = $("#email").val();
		var msgval = $("#zipcode").val();
		var msglen = msgval.length;
		var mailvalid = validateEmail(emailval);
		
		if (mailvalid == false) {
			$("#email").addClass("error");
		}
        $("#email").change(function() {
            $("#email").removeClass("error");
        });
			
		if (mailvalid == true) {
			$("#send").replaceWith("<em>submitting...</em>");			
			
			$.ajax({
				url: 'https://www.allhorse.com/capture/index.php',
				dataType:'jsonp',
				data: $("#contact").serialize(),
				success: function(data) {
					if(data.action == "success") {
						$("#contact").fadeOut("fast", function(){
							$(this).before("<p><strong>Success! You have been subscribed, thanks!</strong></p>");
							setTimeout("$.fancybox.close()", 2000);
						
						});
					}
				},
				error: function(request, status, error){
					console.log(status + " : " + error);
				}
			});		
		}
	});	

	$("#sendemail").on("click", function(){

		
		var emailval = $("#email").val();
		var msgval = $("#zipcode").val();
		var msglen = msgval.length;
		var mailvalid = validateEmail(emailval);
		
		if (mailvalid == false) {
			$("#email").addClass("error");
		}
        $("#email").change(function() {
            $("#email").removeClass("error");
        });
		
		if (mailvalid == true) {
			$(".modalbox").click();
			$("#inline").html("<p><strong>Submitted...</strong></p>");
			
			$.ajax({
				url: 'https://www.allhorse.com/capture/index.php',
				dataType:'jsonp',
				data: $("#contact").serialize(),
				success: function(data) {
console.log(data);
					if(data.action == "success") {
						$("#inline").html("<p><strong>Success! You have been subscribed, thanks!</strong></p>");
						$("#email").val('');
						$("#zipcode").val('');
						setTimeout("$.fancybox.close()", 2000);
					}
				},
				error: function(request, status, error){
					$("#inline").html(status + " : " + error);
				}
			});		
		}
	});	
});
