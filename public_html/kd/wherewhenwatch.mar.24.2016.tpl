{literal}
<script type="text/javascript" src="/assets/js/atemay.js"></script>
<script type="text/javascript">
addthisevent.settings({
	license   	: "ammgus1bqzg0il7ntm75",
	mouse		: false,
	css			: false,
	outlook		: {show:true, text:"Outlook <i class='fa fa-plus-square'></i>"},
	google		: {show:true, text:"Google <i class='fa fa-plus-square'></i>"},
	yahoo		: {show:true, text:"Yahoo <i class='fa fa-plus-square'></i>"},
	ical		: {show:true, text:"iCal <i class='fa fa-plus-square'></i>"},
	hotmail		: {show:true, text:"Hotmail <i class='fa fa-plus-square'></i>"},
	facebook	: {show:true, text:"Facebook <i class='fa fa-plus-square'></i>"}
});
</script>
{/literal}

<div class="row infoBlocks">
<div class="col-md-4">
<div class="section">
	<i class="fa fa-calendar"></i>
	<div class="info">
	{* <p>When is the Kentucky Derby?</p>    *}
	<p>The Kentucky Derby is on  {include file='/home/ah/allhorse/public_html/kd/racedate.php'}</p>
	</div>
    <a href="#" title="Add to Calendar" class="addthisevent" rel="nofollow">Add to calendar <i class="fa fa-plus"></i>
	<span class="_start">05-07-2016 17:00:00</span>
	<span class="_end">05-07-2016 18:30:00</span>
	<span class="_zonecode">15</span> <!-- EST US -->
	<span class="_summary">2016 Kentucky Derby - Place My Bet</span>
	<span class="_description">www.usracing.com <br/>First race of the Triple Crown. <br />Races times subject to change.</span>
	<span class="_location">Churchill Downs</span>
	<span class="_organizer">USRacing.com</span>
	<span class="_organizer_email">comments@usracing.com</span>
	<span class="_all_day_event">false</span>
	<span class="_date_format">07/05/2016</span>
	</a>
</div>
</div>

<div class="col-md-4">
<div class="section">
	<i class="fa fa-map-marker"></i>
	<div class="info">

	<p>Place your bet on the Kentucky Derby: <a itemprop="url" href="/kentucky-derby/odds">Odds are Live!</a></p>
    </div> 
    <a href="/signup/?w=Kentucky-Derby" class="btn btn-primary btn-sm btn-red" rel="nofollow">Bet Now!<i class="fa fa-chevron-right"></i></a>   
</div>
</div>

<div class="col-md-4">
<div class="section">
	<i class="glyphicon glyphicon-eye-open"></i>
    <div class="info">
	{* <p>What channel is the Kentucky Derby on?</p> *}
	<p>Watch the Kentucky Derby live on TV with NBC at 5:00 pm EST</p>
    </div>
</div>
</div>
</div>
  