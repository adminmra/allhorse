{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2019 Kentucky Derby Odds"
	  }
	</script>
	{/literal}    
    <div>
    <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Kentucky Derby Odds" summary="The latest odds for the Kentucky Derby available for wagering now at BUSR.">

        <caption> Kentucky Derby Odds</caption>
        <tfoot>
            <tr>
                <td class="dateUpdated center" colspan="4">
                   {*<em id='updateemp'>Updated {php} $tdate = date("M d, Y H:00", time() - 7200); echo $tdate . " EST"; {/php} .</em>*} {*Use this one when close to the races day with manually table *}
                    <em id='updateemp'>Updated May 4, 2019 6:00 EST .</em>
                    <!-- br /> All odds are fixed odds prices. -->
                </td>
            </tr>
        </tfoot>
        <tbody>

            <tr class='head_title'>
                <th>Post</th>
                <th>Horse</th>
                <th>Jockey</th>
                <th>Odds</th>
            </tr>
            <tr>
                <td> 1 </td>
                <td> War of Will </td>
                <td> Tyler Gaffalione </td>
                <td> 15-1 </td>
            </tr>
            <tr>
                <td> 2 </td>
                <td> Tax </td>
                <td> Junior Alvarado </td>
                <td> 20-1 </td>
            </tr>
            <tr>
                <td> 3 </td>
                <td> By My Standards </td>
                <td> Gabriel Saez </td>
                <td> 15-1 </td>
            </tr>
            <tr>
                <td> 4 </td>
                <td> Gray Magician </td>
                <td> Drayden Van Dyke </td>
                <td> 50-1 </td>
            </tr>
            <tr>
                <td> 5 </td>
                <td> Improbable </td>
                <td> Irad Ortiz Jr. </td>
                <td> 5-1 </td>
            </tr>
            <tr>
                <td> 6 </td>
                <td> Vekoma </td>
                <td> Javier Castellano </td>
                <td> 15-1 </td>
            </tr>
            <tr>
                <td> 7 </td>
                <td> Maximum Security </td>
                <td> Luis Saez </td>
                <td> 8-1 </td>
            </tr>
            <tr>
                <td> 8 </td>
                <td> Tacitus </td>
                <td> Jose Ortiz </td>
                <td> 8-1 </td>
            </tr>
            <tr>
                <td> 9 </td>
                <td> Plus Que Parfait </td>
                <td> Ricardo Santana Jr. </td>
                <td> 30-1 </td>
            </tr>
            <tr>
                <td> 10 </td>
                <td> Cutting Humor </td>
                <td> Mike Smith </td>
                <td> 30-1 </td>
            </tr>
            <tr>
                <td><strike>	11	</strike></td>
                <td><strike>	Haikal</strike> </td>
                <td><strike>	Rajiv Maragh</strike> </td>
                <td><strike>	Scratched</strike> </td>
            </tr>
            <tr>
                <td><strike>	12	</strike></td>
                <td><strike>	Omaha Beach	</strike></td>
                <td> <strike>Mike Smith</strike> </td>
                <td><strike>	Scratched </strike></td>
            </tr>
            <tr>
                <td> 13 </td>
                <td> Code of Honor </td>
                <td> John Velazquez </td>
                <td> 12-1 </td>
            </tr>
            <tr>
                <td> 14 </td>
                <td> Win Win Win </td>
                <td> Julian Pimentel </td>
                <td> 12-1 </td>
            </tr>
            <tr>
                <td> 15 </td>
                <td> Master Fencer (JPN) </td>
                <td> Julien Leparoux </td>
                <td> 50-1 </td>
            </tr>
            <tr>
                <td> 16 </td>
                <td> Game Winner </td>
                <td> Joel Rosario </td>
                <td> 9-2 </td>
            </tr>
            <tr>
                <td> 17 </td>
                <td> Roadster </td>
                <td> Florent Geroux </td>
                <td> 5-1 </td>
            </tr>
            <tr>
                <td> 18 </td>
                <td> Long Range Toddy </td>
                <td> Jon Court </td>
                <td> 30-1 </td>
            </tr>
            <tr>
                <td> 19 </td>
                <td> Spinoff </td>
                <td> Manny Franco </td>
                <td> 30-1 </td>
            </tr>
            <tr>
                <td> 20 </td>
                <td> Country House </td>
                <td> Flavien Prat </td>
                <td> 30-1 </td>
            </tr>
            <tr>
                <td> AE-21 </td>
                <td> Bodexpress </td>
                <td> Chris Landeros </td>
                <td> 30-1 </td>
            </tr>
            <!--
        <tr><td>

				</td><td>

				</td><td data-title='Trainer'>

				</td><td>

				</td><td>

				</td></tr>
  -->
        </tbody>
    </table>
</div>
	{literal}
        <style>
            table.ordenable th{cursor: pointer}
			table.ordenable td{ word-wrap: break-word !important; word-break: break-all !important;}
			table.ordenable{ max-width: 100% !important; }
        </style>
				
	{/literal}
    
