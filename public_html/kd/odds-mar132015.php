<?php 
include('/home/ah/usracing.com/smarty/templates/includes/ahr_kd_odds_2014.tpl'); //auto generated nightly - ab


/*
 <div class="table-responsive">
<table  class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Kentucky Derby Odds">
  <tbody>
  <tr >
  <th >#</th>
  <th >Horse</th>
  <th >Odds</th>
  <th>Med</th>
  <th >Jockey / Trainer</th>
  <th>Weight</th>
  </tr>
  <tr   id="1" tabindex="-1" >
    <td   >1</td>
    <td ><strong>Vicar's In Trouble (LA)</strong><br /></td>
    <td>30/1</td>
    <td >L</td>
    <td >R Napravnik / M J Maker</td>
    <td >126</td>
  </tr>
  <tr   id="2" tabindex="-1" >
    <td   >2</td>
    <td ><strong>Harry's Holiday (KY)</strong><br></td>
    <td>50/1</td>
    <td >L</td>
    <td >C J Lanerie / M J Maker</td>
    <td >126</td>
  </tr>
  <tr   id="3" tabindex="-1" >
    <td   >3</td>
    <td ><strong>Uncle Sigh (NY)</strong><br /></td>
    <td>30/1</td>
    <td >L</td>
    <td >I Ortiz, Jr./ G C Contessa</td>
    <td >126</td>
  </tr>
  <tr   id="4" tabindex="-1" >
    <td   >4</td>
    <td ><strong>Danza</strong> <strong>(KY)</strong><br></td>
    <td>10/1</td>
    <td >L</td>
    <td >J Bravo / T A Pletcher</td>
    <td >126</td>
  </tr>
  <tr   id="5" tabindex="-1" >
    <td   >5</td>
    <td ><strong>California Chrome (CA)</strong><br /></td>
    <td>5/2</td>
    <td >L</td>
    <td >V Espinoza / A Sherman</td>
    <td >126</td>
  </tr>
  <tr   id="6" tabindex="-1" >
    <td   >6</td>
    <td ><strong>Samraat</strong> <strong>(NY)</strong><br /></td>
    <td>15/1</td>
    <td >L</td>
    <td >J L Ortiz / R A Violette, Jr.</td>
    <td >126</td>
  </tr>
  <tr   id="7" tabindex="-1" >
    <td   >7</td>
    <td ><strong>We Miss Artie</strong> <strong>(ON)</strong><br></td>
    <td>50/1</td>
    <td >L</td>
    <td >J Castellano / T A Pletcher</td>
    <td >126</td>
  </tr>
  <tr   id="8" tabindex="-1" >
    <td   >8</td>
    <td ><strong>General a Rod (KY)</strong><br></td>
    <td>15/1</td>
    <td >L</td>
    <td >J Rosario / M J Maker</td>
    <td >126</td>
  </tr>
  <tr   id="9" tabindex="-1" >
    <td   >9</td>
    <td ><strong>Vinceremos (KY)</strong><br></td>
    <td>30/1</td>
    <td >L</td>
    <td >J Rocco, Jr. /  / T A Pletcher</td>
    <td >126</td>
  </tr>
  <tr   id="10" tabindex="-1" >
    <td   >10</td>
    <td ><strong>Wildcat Red</strong> <strong>(FL)</strong><br></td>
    <td>15/1</td>
    <td >L</td>
    <td >L Saez / J Garoffalo</td>
    <td >126</td>
  </tr>
  <tr   id="11" tabindex="-1" >
    <td   >11</td>
    <td ><strong>Hoppertunity (KY)</strong><br></td>
    <td>6/1</td>
    <td >L</td>
    <td >M E Smith / B Baffert</td>
    <td >126</td>
  </tr>
  <tr   id="12" tabindex="-1" >
    <td   >12</td>
    <td ><strong>Dance With Fate (FL)</strong><br></td>
    <td>20/1</td>
    <td >L</td>
    <td >C S Nakatani / P Eurton</td>
    <td >126</td>
  </tr>
  <tr   id="13" tabindex="-1" >
    <td   >13</td>
    <td ><strong>Chitu (KY)</strong><br></td>
    <td>20/1</td>
    <td >L</td>
    <td >M Garcia / B Baffert</td>
    <td >126</td>
  </tr>
  <tr   id="14" tabindex="-1" >
    <td   >14</td>
    <td ><strong>Medal Count (KY) </strong><br></td>
    <td>20/1</td>
    <td >L</td>
    <td >R Albarado / D L Romans</td>
    <td >126</td>
  </tr>
  <tr   id="15" tabindex="-1" >
    <td   >15</td>
    <td ><strong>Tapiture (KY)</strong><br></td>
    <td>15/1</td>
    <td >L</td>
    <td >R Santana, Jr. / S M Asmussen</td>
    <td >126</td>
  </tr>
  <tr   id="16" tabindex="-1" >
    <td   >16</td>
    <td >I<strong>ntense Holiday (KY)</strong></td>
    <td>12/1</td>
    <td >L</td>
    <td >J R Velazquez / T A Pletcher</td>
    <td >126</td>
  </tr>
  <tr   id="17" tabindex="-1" >
    <td   >17</td>
    <td ><strong>Commanding Curve (KY)</strong><br></td>
    <td>50/1</td>
    <td >L</td>
    <td >S Bridgmohan / D Stewart</td>
    <td >126</td>
  </tr>
  <tr   id="18" tabindex="-1" >
    <td   >18</td>
    <td ><strong>Candy Boy (KY)</strong><br></td>
    <td>20/1</td>
    <td >L</td>
    <td >G L Stevens / J W Sadler</td>
    <td >126</td>
  </tr>
  <tr   id="19" tabindex="-1" >
    <td   >19</td>
    <td ><strong>Ride on Curlin (KY)</strong><br></td>
    <td>15/1</td>
    <td >L</td>
    <td >C H Borel / W G Gowan</td>
    <td >126</td>
  </tr>
  <tr   id="20" tabindex="-1" >
    <td   >20</td>
    <td ><strong>Wicked Strong (KY)</strong><br></td>
    <td>8/1</td>
    <td >L</td>
    <td >R Maragh / J A Jerkens</td>
    <td >126</td>
  </tr>
 <!--
 <tr>    <td class="dateUpdated center" colspan="6"><em>Updated Feb 6, 2014 - US Racing <a href="http://www.usracing.com/kentucky-derby/odds">Kentucky Derby Odds</a> </em></td>
  </tr>
-->
    </tbody>
</table>
</div>
*/?>