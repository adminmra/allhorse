<!--fix this to the new table--><div class="table-responsive"><table  class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0" title="Kentucky Derby Results" summary="  Kentucky Derby Results">
                                <caption>
                                   2014 Kentucky Derby Results
                                </caption>
                                <tr >
                                  <th>Result</th>
                                  <th>Post</th>
                                  <th>Time</th>
                                  <th>Horse</th>
                                  <th>Jockey</th>
                                  <th>Trainer</th>
                                  <th>Lengths</th>
                                </tr>
                              <tr>
                                <td>1</td>
                                <td>5</td>
                                <td>2:03.66</td>
                                <td>California Chrome</td>
                                <td>Victor Espinoza</td>
                                <td>Art Sherman</td>
                                <td>-</td>
                                </tr>
                                <tr   >
                                  <td>2</td>
                                  <td>17</td>
                                  <td>&nbsp;</td>
                                  <td>Commanding Curve</td>
                                  <td>Shaun Bridgmohan</td>
                                  <td>Dallas Stewart</td>
                                  <td>1 3/4</td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td>4</td>
                                  <td>&nbsp;</td>
                                  <td>Danza</td>
                                  <td>Joe Bravo</td>
                                  <td>Todd Pletcher</td>
                                  <td>3</td>
                                </tr>
                                <tr   >
                                  <td>4</td>
                                  <td>20</td>
                                  <td>&nbsp;</td>
                                  <td>Wicked Strong</td>
                                  <td>Rajiv Maragh</td>
                                  <td>Jimmy Jerkens</td>
                                  <td>5 3/4</td>
                                </tr>
                                <tr>
                                  <td>5</td>
                                  <td>6</td>
                                  <td>&nbsp;</td>
                                  <td>Samraat</td>
                                  <td>Jose Ortiz</td>
                                  <td>Rick Violette Jr.</td>
                                  <td>5 3/4</td>
                                </tr>
                                <tr   >
                                  <td>6</td>
                                  <td>12</td>
                                  <td>&nbsp;</td>
                                  <td>Dance With Fate</td>
                                  <td>Corey Nakatani</td>
                                  <td>Peter Eurton</td>
                                  <td>6 1/4</td>
                                </tr>
                                <tr>
                                  <td>7</td>
                                  <td>19</td>
                                  <td>&nbsp;</td>
                                  <td>Ride On Curlin</td>
                                  <td>Calvin Borel</td>
                                  <td>Billy Gowan</td>
                                  <td>6 3/4</td>
                                </tr>
                                <tr   >
                                  <td>8</td>
                                  <td>14</td>
                                  <td>&nbsp;</td>
                                  <td>Medal Count</td>
                                  <td>Robby Albarado</td>
                                  <td>Dale Romans</td>
                                  <td>7 1/2</td>
                                </tr>
                                <tr>
                                  <td>9</td>
                                  <td>13</td>
                                  <td>&nbsp;</td>
                                  <td>Chitu</td>
                                  <td>Martin Garcia</td>
                                  <td>Bob Baffert</td>
                                  <td>8</td>
                                </tr>
                                <tr   >
                                  <td>10</td>
                                  <td>7</td>
                                  <td>&nbsp;</td>
                                  <td>We Miss Artie</td>
                                  <td>Javier Castellano</td>
                                  <td>Todd Pletcher</td>
                                  <td>8 1/4</td>
                                </tr>
                                <tr>
                                  <td>11</td>
                                  <td>8</td>
                                  <td>&nbsp;</td>
                                  <td>General A Rod</td>
                                  <td>Joel Rosario</td>
                                  <td>Mike Maker</td>
                                  <td>8 1/4</td>
                                </tr>
                                <tr   >
                                  <td>12</td>
                                  <td>16</td>
                                  <td>&nbsp;</td>
                                  <td>Intense Holiday</td>
                                  <td>John Velazquez</td>
                                  <td>Todd Pletcher</td>
                                  <td>9</td>
                                </tr>
                                <tr>
                                  <td>13</td>
                                  <td>18</td>
                                  <td>&nbsp;</td>
                                  <td>Candy Boy</td>
                                  <td>Gary Stevens</td>
                                  <td>John Sadler</td>
                                  <td>11 3/4</td>
                                </tr>
                                <tr   >
                                  <td>14</td>
                                  <td>3</td>
                                  <td>&nbsp;</td>
                                  <td>Uncle Sigh</td>
                                  <td>Irad Ortiz Jr.</td>
                                  <td>Gary Contessa</td>
                                  <td>15</td>
                                </tr>
                                <tr>
                                  <td>15</td>
                                  <td>15</td>
                                  <td>&nbsp;</td>
                                  <td>Tapiture</td>
                                  <td>Ricardo Santana Jr.</td>
                                  <td>Steve Asmussen</td>
                                  <td>16 1/4</td>
                                </tr>
                                <tr   >
                                  <td>16</td>
                                  <td>2</td>
                                  <td>&nbsp;</td>
                                  <td>Harry's Holiday</td>
                                  <td>Corey Lanerie</td>
                                  <td>Mike Maker</td>
                                  <td>22 1/2</td>
                                </tr>
                                <tr>
                                  <td>17</td>
                                  <td>9</td>
                                  <td>&nbsp;</td>
                                  <td>Vinceremos</td>
                                  <td>Joe Rococo Jr.</td>
                                  <td>Todd Pletcher</td>
                                  <td>28</td>
                                </tr>
                                <tr   >
                                  <td>18</td>
                                  <td>10</td>
                                  <td>&nbsp;</td>
                                  <td>Wildcat Red</td>
                                  <td>Luis Saez</td>
                                  <td>Jose Garoffalo</td>
                                  <td>28 1/4</td>
                                </tr>
                                <tr>
                                  <td>19</td>
                                  <td>1</td>
                                  <td>&nbsp;</td>
                                  <td>Vicar's In Trouble</td>
                                  <td>Rosie Napravnik</td>
                                  <td>Mike Maker</td>
                                  <td>38 1/4</td>
                                </tr>
                                <tr>
                                  <td>SCR</td>
                                  <td>11</td>
                                  <td>&nbsp;</td>
                                  <td>Hoppertunity</td>
                                  <td>Mike Smith</td>
                                  <td>Bob Baffert</td>
                                  <td>SCR</td>
                                </tr>
                                											                                
                              </table></div>
<p>
<h2>Kentucky Derby 2014 Payouts</h2>
          <div class="table-responsive"><table  class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0" summary="Kentucky Derby Payouts">
            <thead>
              <tr>
                <th colspan="2">Results</th>
                <th>Win</th>
                <th>Place</th>
                <th>Show</th>
              </tr>
            </thead>
            <tbody>
              <tr >
                <td>5</td>
                <td >California Chrome</td>
                <td>$7.00</td>
                <td >$5.60</td>
                <td >$4.20</td>
              </tr>
              <tr>
                <td>17</td>
                <td >Commanding Curve</td>
                <td></td>
                <td>$31.80</td>
                <td >$15.40</td>
              </tr>
              <tr>
                <td>4</td>
                <td >Danza</td>
                <td></td>
                <td></td>
                <td>$6.00</td>
              </tr>
            </tbody>
          </table>
        </div><p>
    <h2>Kentucky Derby 2014 Exotic Payouts</h2>
         <div class="table-responsive"><table  class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0" summary="Kentucky Derby Exotic Payouts">
            <tbody>
             <tr>
                <td >$2.00</td>
                <td >Exacta</td>
                <td>
                  $340.00
                  
                </td>
              </tr><tr>
                <td >$0.50</td>
                <td >Trifecta</td>
                <td>
                  $856.15
                  
                </td>
              </tr><tr>
                <td >$1.00</td>
                <td >Double</td>
                <td>
                  $4.50
                  
                </td>
              </tr><tr>
                <td >$0.50</td>
                <td >Pick Four</td>
                <td>
                  $394.45
                  
                </td>
              </tr><tr>
                <td >$1.00</td>
                <td >Superfecta</td>
                <td>
                  $7,691.90
                  
                </td>
              </tr><tr>
                <td >$2.00</td>
                <td >Pick Six</td>
                <td>
                  $189.60
                  
                </td>
              </tr><tr>
                <td >$2.00</td>
                <td >Pick Six</td>
                <td>
                  $22,296.40
                  
                </td>
              </tr><tr>
                <td >$0.50</td>
                <td >Pick Three</td>
                <td>
                  $45.15
                  
                </td>
              </tr><tr>
                <td >$0.50</td>
                <td >Pick Five</td>
                <td>
                  $694.95
                  
                </td>
              </tr> <tr>
                <td >$1.00</td>
                <td >Pent/Str Fl</td>
                <td>
                  $149,764.70
                  
                </td>
              </tr>
            </tbody>
          </table>
        </div>
                              
                              
                              
                              
                              
