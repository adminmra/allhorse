<div class="video-responsive">
<iframe class="video"  width="100%" height="461" src="https://www.youtube.com/embed/x2-VM8nx92M?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
{literal}
<style type="text/css" >
.video-responsive{
    overflow:hidden;
    padding-bottom:56.25%;
    position:relative;
    height:0;
}
.video-responsive iframe{
    left:0;
    top:0;
    height:100%;
    width:100%;
    position:absolute;
}
</style>
{/literal}