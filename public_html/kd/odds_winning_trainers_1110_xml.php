    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Kentucky Derby Trainer Odds" summary="The latest odds for the top Trainers for the Kentucky Derby.  Only available at BUSR.">
            <caption> Kentucky Derby Trainer Odds</caption>
            <tbody>
              <!--
  <tr>
                    <th colspan="3" class="center">
                    Horses - 2018 Kentucky Derby Trainer  - May 01                    </th>
            </tr>
-->
    <tr><th colspan="3" class="center">2018 Kentucky Derby Trainer - To Win</th></tr><tr class='head_title'><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Aidan O'brien</td><td>+650</td><td>13/2</td></tr><tr><td>Bill Mott</td><td>+2500</td><td>25/1</td></tr><tr><td>Bob Baffert</td><td>+250</td><td>5/2</td></tr><tr><td>Chad Brown</td><td>+600</td><td>6/1</td></tr><tr><td>D. Wayne Lukas</td><td>+4500</td><td>45/1</td></tr><tr><td>Dale Romans</td><td>+2500</td><td>25/1</td></tr><tr><td>Dallas Stewart</td><td>+7500</td><td>75/1</td></tr><tr><td>Doug O'neill</td><td>+5000</td><td>50/1</td></tr><tr><td>Jason Servis</td><td>+5000</td><td>50/1</td></tr><tr><td>Jerry Hollendorfer</td><td>+3500</td><td>35/1</td></tr><tr><td>Keith Desormeaux</td><td>+1800</td><td>18/1</td></tr><tr><td>Kiaran Mclaughlin</td><td>+1800</td><td>18/1</td></tr><tr><td>Mark Casse</td><td>+1500</td><td>15/1</td></tr><tr><td>Mick Ruis</td><td>+700</td><td>7/1</td></tr><tr><td>Steve Asmussen</td><td>+3500</td><td>35/1</td></tr><tr><td>Todd Pletcher</td><td>+200</td><td>2/1</td></tr><tr><td>Tom Amoss</td><td>+3000</td><td>30/1</td></tr>                    <tr>
                        <td class="dateUpdated center" colspan="5">
                            <em id='updateemp'>  - Updated June 29, 2018 17:40:03 </em>
                            BUSR - Official <a href="https://www.usracing.com/kentucky-derby/trainer-betting">Kentucky Derby Trainer Odds</a>. <!-- br />
                            All odds are fixed odds prices. -->
                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
    