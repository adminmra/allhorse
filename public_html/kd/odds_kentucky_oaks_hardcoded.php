<div id="no-more-tables">
	<table width="682" border="0" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered ordenable justify-center" id="infoEntries" title="Kentucky Oaks Odds" summary="Kentucky Oaks Odds">
		<caption class="hide-sm">Kentucky Oaks Odds</caption>
		<tbody>
			<tr class="head_title hide-sm">
				<th width="18">PP</th>
				<th width="115">Horse</th>
				<th width="47">Odds</th>
				<th width="84">Trainer</th>
				<th width="154">Jockey</th>
				<th width="154">Owner</th>
			</tr>
			<tr>
				<td data-title="PP">1</td>
				<td data-title="Horse">Pauline's Pearl</td>
				<td data-title="Odds">20-1</td>
				<td data-title="Trainer">Steve Asmussen</td>
				<td data-title="Jockey">Ricardo Santana Jr.</td>
				<td data-title="Owner">Stonestreet Stables LLC</td>
			</tr>
			<tr>
				<td data-title="PP">2</td>
				<td data-title="Horse">Maracuja</td>
				<td data-title="Odds">20-1</td>
				<td data-title="Trainer">Rob Atras</td>
				<td data-title="Jockey">Kendrick Carmouche</td>
				<td data-title="Owner">Beach Haven Thoroughbreds</td>
			</tr>
			<tr>
				<td data-title="PP">3</td>
				<td data-title="Horse">Clairiere</td>
				<td data-title="Odds">5-1</td>
				<td data-title="Trainer">Steve Asmussen</td>
				<td data-title="Jockey">Tyler Gaffalione</td>
				<td data-title="Owner">Stonestreet Stables LLC</td>
			</tr>
			<tr>
				<td data-title="PP">4</td>
				<td data-title="Horse">Crazy Beautiful</td>
				<td data-title="Odds">15-1</td>
				<td data-title="Trainer">Kenneth McPeek</td>
				<td data-title="Jockey">Jose Ortiz</td>
				<td data-title="Owner">Phoenix Thoroughbred III</td>
			</tr>
			<tr>
				<td data-title="PP">5</td>
				<td data-title="Horse">Pass the Champagne</td>
				<td data-title="Odds">15-1</td>
				<td data-title="Trainer">George Weaver</td>
				<td data-title="Jockey">Javier Castellano</td>
				<td data-title="Owner">R.A. Hill Stable, Black Type Thoroughbreds, Rock Ridge Racing LLc, BlackRidge Stabls LLc and Brown, James</td>
			</tr>
			<tr>
				<td data-title="PP">6</td>
				<td data-title="Horse">Travel Column</td>
				<td data-title="Odds">3-1</td>
				<td data-title="Trainer">Brad Cox</td>
				<td data-title="Jockey">Florent Geroux</td>
				<td data-title="Owner">OXO Equine LLC</td>
			</tr>
			<tr>
				<td data-title="PP">7</td>
				<td data-title="Horse">Ava's Grace</td>
				<td data-title="Odds">50-1</td>
				<td data-title="Trainer">Robertino Diodoro</td>
				<td data-title="Jockey">David Cohen</td>
				<td data-title="Owner">Cypress Creek Equine</td>
			</tr>
			<tr>
				<td data-title="PP">8</td>
				<td data-title="Horse">Moraz</td>
				<td data-title="Odds">30-1</td>
				<td data-title="Trainer">Michael McCarthy</td>
				<td data-title="Jockey">Flavien Prat</td>
				<td data-title="Owner">Don Alberto Stable</td>
			</tr>
			<tr>
				<td data-title="PP">9</td>
				<td data-title="Horse">Coach</td>
				<td data-title="Odds">50-1</td>
				<td data-title="Trainer">Brad Cox</td>
				<td data-title="Jockey">Luis Saez</td>
				<td data-title="Owner">Kueber Racing, LLC</td>
			</tr>
			<tr>
				<td data-title="PP">10</td>
				<td data-title="Horse">Malathaat</td>
				<td data-title="Odds">5-2</td>
				<td data-title="Trainer">Todd Pletcher</td>
				<td data-title="Jockey">John Velazquez</td>
				<td data-title="Owner">Shadwell Stable</td>
			</tr>
			<tr>
				<td data-title="PP">11</td>
				<td data-title="Horse">Will's Secret</td>
				<td data-title="Odds">30-1</td>
				<td data-title="Trainer">Dallas Stewart</td>
				<td data-title="Jockey">John Court</td>
				<td data-title="Owner">Willis Horton Racing LLC</td>
			</tr>
			<tr>
				<td data-title="PP">12</td>
				<td data-title="Horse">Search Results</td>
				<td data-title="Odds">3-1</td>
				<td data-title="Trainer">Chad Brown</td>
				<td data-title="Jockey">Irad Ortiz</td>
				<td data-title="Owner">Klaravich Sables, Inc.</td>
			</tr>
			<tr>
				<td data-title="PP">13</td>
				<td data-title="Horse">Competitive Speed</td>
				<td data-title="Odds">50-1</td>
				<td data-title="Trainer">Gonzalez Javier</td>
				<td data-title="Jockey">Chris Landeros</td>
				<td data-title="Owner">John C. Minchello</td>
			</tr>
			<tr>
				<td data-title="PP">14</td>
				<td data-title="Horse">Millefeuille</td>
				<td data-title="Odds">20-1</td>
				<td data-title="Trainer">William Mott</td>
				<td data-title="Jockey">Joel Rosario</td>
				<td data-title="Owner">Juddmonte Farms, Inc.</td>
			</tr>
		</tbody>
	</table>
</div>


<style>
	table.ordenable tbody th {
		cursor: pointer;
	}
	@media only screen and (max-width: 800px) {
		#no-more-tables td span {
			text-align: center !important;
		}
		.hide-sm {
		  display: none !important;
		}
	}
</style>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">

<script src="/assets/js/jquery.sortElements.js"></script>
<script src="//www.usracing.com/assets/js/sorttable_post.js"></script>
<script src="//www.usracing.com/assets/js/aligncolumn.js"></script>

<script>
	$(document).ready(function () {
		alignColumn([2], 'left');
	});
</script>
