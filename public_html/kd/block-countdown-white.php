  <section class="countdown-white usr-section">
      <div class="container">
      <h3 class="countdown_heading"><span class="countdown_heading-text-blue blue-text">Countdown to the <span class="normal"> <?php include '/home/ah/allhorse/public_html/kd/running.php' ?> Kentucky Derby</span><p class="kdMobile"> <?php include '/home/ah/allhorse/public_html/kd/running.php' ?> Kentucky Derby</p>  </span></h3>
        <div class="countdown_items">
          <div class="countdown_item blue clock_days">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_days"></canvas>
            </div>
            <div class="text"><span class="countdown_num blue-text val">0</span><span class="countdown_label blue-text time type_days">days</span></div>
          </div>
          <div class="countdown_item blue clock_hours">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_hours"></canvas>
            </div>
            <div class="text"><span class="countdown_num blue-text val">0</span><span class="countdown_label blue-text time type_hours">hours</span></div>
          </div>
          <div class="countdown_item blue clock_minutes">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_minutes"></canvas>
            </div>
            <div class="text"><span class="countdown_num blue-text val">0</span><span class="countdown_label blue-text time type_minutes">minutes</span></div>
          </div>
          <div class="countdown_item blue clock_seconds">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_seconds"></canvas>
            </div>
            <div class="text"><span class="countdown_num blue-text val">0</span><span class="countdown_label blue-text time type_seconds">seconds</span></div>
          </div>
        </div>
      </div>
    </section>
<?php
      $startDate = strtotime("now");
      $endDate  = strtotime("2 May 2020 18:50:00");
?>
    
    <script type="text/javascript">
      $(document).ready(function(){
        JBCountDown({
          secondsColor : "#ffdc50",
          secondsGlow  : "none",
          minutesColor : "#9cdb7d",
          minutesGlow  : "none", 
          hoursColor   : "#378cff", 
          hoursGlow    : "none",  
          daysColor    : "#ff6565", 
          daysGlow     : "none", 
	  startDate   : "<?php echo $startDate ?>", 
	  endDate : "<?php echo $endDate ?>", 
	  now : "<?php echo strtotime('now'); ?>" 
        });
      });
    </script>
    
