<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered">



	<tr>



		<td>Grade II Churchill Downs</td>

		<td>$250,000 - added, seven furlongs, 4-year-olds and up</td>



	</tr>



	<tr>



		<td>Grade III La Troienne</td>

		<td>$150,000 - added, seven furlongs, 3-year-old fillies</td>



	</tr>



	<tr>



		<td>Grade III Distaff Turf Mile</td>

		<td>$150,000 - added, one mile, fillies and mares, 3-year-olds and up</td>



	</tr>



	<tr>



		<td>Grade I Distaff</td>

		<td>$500,000 - added, seven furlongs, fillies and mares, 4-year-olds and up</td>



	</tr>



	<tr>



		<td>Grade I Turf Classic</td>

		<td>$1,000,000 - added, 1 1/8 mile, 3-year-olds and up</td>



	</tr>



	<tr>



		<td>Grade I Kentucky Derby</td>

		<td>$3,000,000 - guaranteed, 1 1/4 mile, 3-year-olds</td>



	</tr>



	<tr>



		<td colspan="2" class="center">&nbsp;</td>



	</tr>



</table>