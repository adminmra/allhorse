{literal}
			<script type="application/ld+json">
			{
			  "@context": "http://schema.org",
			  "@type": "Table",
			  "about": "2020 Kentucky Oaks Odds"
			}
		</script> 
	{/literal}     <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" summary="The latest odds for the Kentucky Oaks available for wagering now at  BUSR.">
            <caption>Kentucky Oaks Odds</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 27, 2020.</em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
    <tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td data-title=' '>Bast</td><td data-title='Fractional'>5/1</td><td data-title='American'>+500</td></tr><tr><td data-title=' '>British Idiom</td><td data-title='Fractional'>12/1</td><td data-title='American'>+1200</td></tr><tr><td data-title=' '>Donna Veloce</td><td data-title='Fractional'>12/1</td><td data-title='American'>+1200</td></tr><tr><td data-title=' '>Taraz</td><td data-title='Fractional'>15/2</td><td data-title='American'>+750</td></tr><tr><td data-title=' '>Finite</td><td data-title='Fractional'>6/1</td><td data-title='American'>+600</td></tr><tr><td data-title=' '>Lake Avenue</td><td data-title='Fractional'>18/1</td><td data-title='American'>+1800</td></tr><tr><td data-title=' '>Blame Debbie</td><td data-title='Fractional'>50/1</td><td data-title='American'>+5000</td></tr><tr><td data-title=' '>Maedean</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>Perfect Alibi</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>Wicked Whisper</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>Comical</td><td data-title='Fractional'>50/1</td><td data-title='American'>+5000</td></tr><tr><td data-title=' '>I Dare U</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Lazy Daisy</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Two Sixty</td><td data-title='Fractional'>50/1</td><td data-title='American'>+5000</td></tr><tr><td data-title=' '>Alandra</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Pass The Plate</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Tempers Rising</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>K P Dreamin</td><td data-title='Fractional'>66/1</td><td data-title='American'>+6600</td></tr><tr><td data-title=' '>Venetian Harbor</td><td data-title='Fractional'>5/1</td><td data-title='American'>+500</td></tr><tr><td data-title=' '>Tonalists Shape</td><td data-title='Fractional'>12/1</td><td data-title='American'>+1200</td></tr><tr><td data-title=' '>Lucrezia</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr>            </tbody>
        </table>
    </div>
	{literal}
        <style>
            table.ordenable th{cursor: pointer}
			table.ordenable td{ word-wrap: break-word !important; word-break: break-all !important;}
			table.ordenable{ max-width: 100% !important; }
        </style>
        <script src="/assets/js/jquery.sortElements.js"></script>
        <script src="/assets/js/sorttable.js"></script>
	{/literal}
    
	 