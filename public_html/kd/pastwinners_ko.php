                        <table cellpadding="0" cellspacing="0" class="data"  title="Kentucky Oaks Winners" summary="The past winners of the Kentucky Oaks" ><caption > Kentucky Oaks Winners</caption>
      <tr>
        <th width="46" >Year</th>
        <th width="105"  >Winner</th>
        <th width="139" >Jockey</th>
        <th width="150" >Trainer</th>
        <th width="52" >Time</th>
        </tr>
 <tr>
<td>2018</td>
<td>Monomoy Girl</td>
<td>Florent Geroux</td>
<td>Brad Cox</td>
<td>1:49.13</td>
</tr>
<tr>
<td>2017</td>
<td>Abel Tasman</td>
<td>Mike E. Smith</td>
<td>Bob Baffert</td>
<td>1:51.62</td>
</tr>
<tr>
<td>2016</td>
<td>Cathryn Sophia</td>
<td>Javier Castellano</td>
<td>John Servis</td>
<td>1:50.53</td>
</tr>
<tr>
<td>2015</td>
<td>Lovely Maria</td>
<td>Kerwin Clark</td>
<td>J. Larry Jones</td>
<td>1:50.45</td>
</tr>
<tr>
<td>2014</td>
<td>Untapable</td>
<td>Rosie Napravnik</td>
<td>Steve Asmussen</td>
<td>1:48.68</td>
</tr>
<tr>
<td>2013</td>
<td>Princess of Sylmar</td>
<td>Mike E. Smith</td>
<td>Todd Pletcher</td>
<td>1:49.17</td>
</tr>       
<tr>
<td>2012</td>
<td>Believe You Can</td>
<td>Rosie Napravnik</td>
<td>J. Larry Jones</td>
<td>1:49.50</td>

</tr> 
       <tr >    <td >2011</td>
    <td><a href="/Animal_Kingdom">Animal Kingdom</a></td>
    <td><a href="/John_R._Velazquez">John R. Velazquez</a></td>
    <td><a href="/H._Graham_Motion">H. Graham Motion</a></td>
    <td>2:02.04</td>
    </tr>
	<tr class="odd">    <td >2010</td>
    <td><a href="/Super_Saver">Super Saver</a></td>
    <td><a href="/Calvin_Borel">Calvin Borel</a></td>
    <td><a href="/Todd_Pletcher">Todd Pletcher</a></td>
    <td>2:04.45</td>
    </tr>
	<tr >    <td >2009</td>
    <td><a href="/Mine_That_Bird">Mine That Bird</a></td>
    <td><a href="/Calvin_Borel">Calvin Borel</a></td>
    <td><a href="/Bennie_Woolley_Jr.">Bennie Woolley Jr.</a></td>
    <td>2:02.66</td>
    </tr>
	<tr class="odd">    <td >2008</td>
    <td><a href="/Big_Brown">Big Brown</a></td>
    <td><a href="/Kent_Desormeaux">Kent Desormeaux</a></td>
    <td>Rick Dutrow</td>
    <td>2:01.82</td>
    </tr>
	<tr >    <td >2007</td>
    <td><a href="/Street_Sense">Street Sense</a></td>
    <td><a href="/Calvin_Borel">Calvin Borel</a></td>
    <td><a href="/Carl_Nafzger">Carl Nafzger</a></td>
    <td>2:02:17</td>
    </tr>
	<tr class="odd">    <td >2006</td>
    <td><a href="/Barbaro">Barbaro</a></td>
    <td><a href="/Edgar_Prado">Edgar Prado</a></td>
    <td><a href="/Michael_Matz">Michael Matz</a></td>
    <td>2:01.36</td>
    </tr>
	<tr >    <td >2005</td>
    <td><a href="/Giacomo">Giacomo</a></td>
    <td><a href="/Mike_Smith">Mike Smith</a></td>
    <td><a href="/John_Shirreffs">John Shirreffs</a></td>
    <td>2:02.75</td>
    </tr>
	<tr class="odd">    <td >2004</td>
    <td><a href="/Smarty_Jones">Smarty Jones</a></td>
    <td><a href="/Stewart_Elliott">Stewart Elliott</a></td>
    <td><a href="/John_Servis">John Servis</a></td>
    <td>2:04.06</td>
    </tr>
	<tr >    <td >2003</td>
    <td><a href="/Funny_Cide">Funny Cide</a></td>
    <td>Jos Santos</td>
    <td><a href="/Barclay_Tagg">Barclay Tagg</a></td>
    <td>2:01.19</td>
    </tr>
	<tr class="odd">    <td >2002</td>
    <td><a href="/War_Emblem">War Emblem</a></td>
    <td><a href="/Victor_Espinoza">Victor Espinoza</a></td>
    <td><a href="/Bob_Baffert">Bob Baffert</a></td>
    <td>2:01.13</td>
    </tr>
	<tr >    <td >2001</td>
    <td><a href="/Monarchos">Monarchos</a></td>
    <td><a href="/Jorge_Chavez">Jorge Chavez</a></td>
    <td>John Ward Jr.</td>
    <td>1:59.97</td>
    </tr>
	<tr class="odd">    <td >2000</td>
    <td><a href="/Fusaichi_Pegasus">Fusaichi Pegasus</a></td>
    <td><a href="/Kent_Desormeaux">Kent Desormeaux</a></td>
    <td><a href="/Neil_Drysdale">Neil Drysdale</a></td>
    <td>2:01</td>
    </tr>
	<tr >    <td >1999</td>
    <td><a href="/Charismatic">Charismatic</a></td>
    <td>Chris Antley</td>
    <td><a href="/D._Wayne_Lukas">D. Wayne Lukas</a></td>
    <td>2:03 1/5</td>
    </tr>
	<tr class="odd">    <td >1998</td>
    <td><a href="/Real_Quiet">Real Quiet</a></td>
    <td><a href="/Kent_Desormeaux">Kent Desormeaux</a></td>
    <td><a href="/Bob_Baffert">Bob Baffert</a></td>
    <td>2:02 1/5</td>
    </tr>
	<tr >    <td >1997</td>
    <td><a href="/Silver_Charm">Silver Charm</a></td>
    <td><a href="/Gary_Stevens">Gary Stevens</a></td>
    <td><a href="/Bob_Baffert">Bob Baffert</a></td>
    <td>2:02 2/5</td>
    </tr>
	<tr class="odd">    <td >1996</td>
    <td><a href="/Grindstone">Grindstone</a></td>
    <td><a href="/Jerry_Bailey">Jerry Bailey</a></td>
    <td><a href="/D._Wayne_Lukas">D. Wayne Lukas</a></td>
    <td>2:01</td>
    </tr>
	<tr >    <td >1995</td>
    <td><a href="/Thunder_Gulch">Thunder Gulch</a></td>
    <td><a href="/Gary_Stevens">Gary Stevens</a></td>
    <td><a href="/D._Wayne_Lukas">D. Wayne Lukas</a></td>
    <td>2:01 1/5</td>
    </tr>
	<tr class="odd">    <td >1994</td>
    <td><a href="/Go_for_Gin">Go for Gin</a></td>
    <td><a href="/Chris_McCarron">Chris McCarron</a></td>
    <td>Nick Zito</td>
    <td>2:03 3/5</td>
    </tr>
	<tr >    <td >1993</td>
    <td><a href="/Sea_Hero">Sea Hero</a></td>
    <td><a href="/Jerry_Bailey">Jerry Bailey</a></td>
    <td>Mack Miller</td>
    <td>2:02 2/5</td>
    </tr>
	<tr class="odd">    <td >1992</td>
    <td><a href="/Lil_E._Tee">Lil E. Tee</a></td>
    <td><a href="/Pat_Day">Pat Day</a></td>
    <td>Lynn Whiting</td>
    <td>2:03</td>
    </tr>
	<tr >    <td >1991</td>
    <td><a href="/Strike_the_Gold">Strike the Gold</a></td>
    <td>Chris Antley</td>
    <td>Nick Zito</td>
    <td>2:03</td>
    </tr>
	<tr class="odd">    <td >1990</td>
    <td><a href="/Unbridled">Unbridled</a></td>
    <td>Craig Perret</td>
    <td><a href="/Carl_Nafzger">Carl Nafzger</a></td>
    <td>2:02</td>
    </tr>
	<tr >    <td >1989</td>
    <td><a href="/Sunday_Silence">Sunday Silence</a></td>
    <td>Pat Valenzuela</td>
    <td>Charlie Whittingham</td>
    <td>2:05</td>
    </tr>
	<tr class="odd">    <td >1988</td>
    <td><a href="/Winning_Colors">Winning Colors</a></td>
    <td><a href="/Gary_Stevens">Gary Stevens</a></td>
    <td><a href="/D._Wayne_Lukas">D. Wayne Lukas</a></td>
    <td>2:02 1/5</td>
    </tr>
	<tr >    <td >1987</td>
    <td><a href="/Alysheba">Alysheba</a></td>
    <td><a href="/Chris_McCarron">Chris McCarron</a></td>
    <td><a href="/Jack_Van_Berg">Jack Van Berg</a></td>
    <td>2:03 2/5</td>
    </tr>
	<tr class="odd">    <td >1986</td>
    <td><a href="/Ferdinand">Ferdinand</a></td>
    <td>Bill Shoemaker</td>
    <td>Charlie Whittingham</td>
    <td>2:02 4/5</td>
    </tr>
	<tr >    <td >1985</td>
    <td><a href="/Spend_a_Buck">Spend a Buck</a></td>
    <td>Angel Cordero, Jr.</td>
    <td>Cam Gambolati</td>
    <td>2:00 1/5</td>
    </tr>
	<tr class="odd">    <td >1984</td>
    <td><a href="/Swale">Swale</a></td>
    <td>Laffit Pincay, Jr.</td>
    <td>Woody Stephens</td>
    <td>2:02 2/5</td>
    </tr>
	<tr >    <td >1983</td>
    <td>Sunnys Halo</td>
    <td><a href="/Eddie_Delahoussaye">Eddie Delahoussaye</a></td>
    <td>David Cross Jr.</td>
    <td>2:02 1/5</td>
    </tr>
	<tr class="odd">    <td >1982</td>
    <td><a href="/Gato_Del_Sol">Gato Del Sol</a></td>
    <td><a href="/Eddie_Delahoussaye">Eddie Delahoussaye</a></td>
    <td>Eddie Gregson</td>
    <td>2:02 2/5</td>
    </tr>
	<tr >    <td >1981</td>
    <td><a href="/Pleasant_Colony">Pleasant Colony</a></td>
    <td>Jorge Velasquez</td>
    <td>John Campo</td>
    <td>2:02</td>
    </tr>
	<tr class="odd">    <td >1980</td>
    <td><a href="/Genuine_Risk">Genuine Risk</a></td>
    <td>Jacinto Vasquez</td>
    <td>LeRoy Jolley</td>
    <td>2:02</td>
    </tr>
	<tr >    <td >1979</td>
    <td><a href="/Spectacular_Bid">Spectacular Bid</a></td>
    <td>Ron Franklin</td>
    <td>Bud Delp</td>
    <td>2:02 2/5</td>
    </tr>
	<tr class="odd">    <td >1978</td>
    <td><a href="/Affirmed">Affirmed</a></td>
    <td>Steve Cauthen</td>
    <td>Laz Barrera</td>
    <td>2:01 1/5</td>
    </tr>
	<tr >    <td >1977</td>
    <td><a href="/Seattle_Slew">Seattle Slew</a></td>
    <td>Jean Cruguet</td>
    <td>Billy Turner</td>
    <td>2:02 1/5</td>
    </tr>
	<tr class="odd">    <td >1976</td>
    <td><a href="/Bold_Forbes">Bold Forbes</a></td>
    <td>Angel Cordero, Jr.</td>
    <td>Laz Barrera</td>
    <td>2:01 3/5</td>
    </tr>
	<tr >    <td >1975</td>
    <td><a href="/Foolish_Pleasure">Foolish Pleasure</a></td>
    <td>Jacinto Vasquez</td>
    <td>LeRoy Jolley</td>
    <td>2:02</td>
    </tr>
	<tr class="odd">    <td >1974</td>
    <td><a href="/Cannonade">Cannonade</a></td>
    <td>Angel Cordero, Jr.</td>
    <td>Woody Stephens</td>
    <td>2:04</td>
    </tr>
	<tr >    <td >1973</td>
    <td><a href="/Secretariat">Secretariat</a></td>
    <td>Ron Turcotte</td>
    <td>Lucien Laurin</td>
    <td>1:59 2/5</td>
    </tr>
	<tr class="odd">    <td >1972</td>
    <td><a href="/Riva_Ridge">Riva Ridge</a></td>
    <td>Ron Turcotte</td>
    <td>Lucien Laurin</td>
    <td>2:01 4/5</td>
    </tr>
	<tr >    <td >1971</td>
    <td><a href="/Canonero_II">Canonero II</a></td>
    <td>Gustavo Avila</td>
    <td>Juan Arias</td>
    <td>2:03 1/5</td>
    </tr>
	<tr class="odd">    <td >1970</td>
    <td><a href="/Dust_Commander">Dust Commander</a></td>
    <td>Mike Manganello</td>
    <td>Don Combs</td>
    <td>2:03 2/5</td>
    </tr>
	<tr >    <td >1969</td>
    <td><a href="/Majestic_Prince">Majestic Prince</a></td>
    <td>Bill Hartack</td>
    <td>Johnny Longden</td>
    <td>2:01 4/5</td>
    </tr>
	<tr class="odd">    <td >1968</td>
    <td><a href="/Forward_Pass">Forward Pass</a></td>
    <td>Ismael Valenzuela</td>
    <td>Henry Forrest</td>
    <td>2:02 1/5</td>
    </tr>
	<tr >    <td >1967</td>
    <td><a href="/Proud_Clarion">Proud Clarion</a></td>
    <td>Bobby Ussery</td>
    <td>Loyd Gentry</td>
    <td>2:00 3/5</td>
    </tr>
	<tr class="odd">    <td >1966</td>
    <td><a href="/Kauai_King">Kauai King</a></td>
    <td>Don Brumfield</td>
    <td>Henry Forrest</td>
    <td>2:02</td>
    </tr>
	<tr >    <td >1965</td>
    <td><a href="/Lucky_Debonair">Lucky Debonair</a></td>
    <td>Bill Shoemaker</td>
    <td>Frank Catrone</td>
    <td>2:01 1/5</td>
    </tr>
	<tr class="odd">    <td >1964</td>
    <td>Northern Dancer</td>
    <td>Bill Hartack</td>
    <td>Horatio Luro</td>
    <td>2:00</td>
    </tr>
	<tr >    <td >1963</td>
    <td><a href="/Chateaugay">Chateaugay</a></td>
    <td>Braulio Baeza</td>
    <td>James Conway</td>
    <td>2:01 4/5</td>
    </tr>
	<tr class="odd">    <td >1962</td>
    <td><a href="/Decidedly">Decidedly</a></td>
    <td>Bill Hartack</td>
    <td>Horatio Luro</td>
    <td>2:00 2/5</td>
    </tr>
	<tr >    <td >1961</td>
    <td><a href="/Carry_Back">Carry Back</a></td>
    <td>John Sellers</td>
    <td>Jack Price</td>
    <td>2:04</td>
    </tr>
	<tr class="odd">    <td >1960</td>
    <td><a href="/Venetian_Way">Venetian Way</a></td>
    <td>Bill Hartack</td>
    <td>Victor Sovinski</td>
    <td>2:02 2/5</td>
    </tr>
	<tr >    <td >1959</td>
    <td><a href="/Tomy_Lee">Tomy Lee</a></td>
    <td>Bill Shoemaker</td>
    <td>Frank Childs</td>
    <td>2:02 1/5</td>
    </tr>
	<tr class="odd">    <td >1958</td>
    <td><a href="/Tim_Tam">Tim Tam</a></td>
    <td>Ismael Valenzuela</td>
    <td>Horace A. "Jimmy" Jones</td>
    <td>2:05</td>
    </tr>
	<tr >    <td >1957</td>
    <td><a href="/Iron_Liege">Iron Liege</a></td>
    <td>Bill Hartack</td>
    <td>Horace A. "Jimmy" Jones</td>
    <td>2:02 1/5</td>
    </tr>
	<tr class="odd">    <td >1956</td>
    <td><a href="/Needles">Needles</a></td>
    <td>David Erb</td>
    <td>Hugh Fontaine</td>
    <td>2:03 2/5</td>
    </tr>
	<tr >    <td >1955</td>
    <td><a href="/Swaps">Swaps</a></td>
    <td>Bill Shoemaker</td>
    <td>Mesh Tenney</td>
    <td>2:01 4/5</td>
    </tr>
	<tr class="odd">    <td >1954</td>
    <td><a href="/Determine">Determine</a></td>
    <td>Raymond York</td>
    <td>Willie Molter</td>
    <td>2:03</td>
    </tr>
	<tr >    <td >1953</td>
    <td><a href="/Dark_Star">Dark Star</a></td>
    <td>Hank Moreno</td>
    <td>Eddie Hayward</td>
    <td>2:02</td>
    </tr>
	<tr class="odd">    <td >1952</td>
    <td><a href="/Hill_Gail">Hill Gail</a></td>
    <td>Eddie Arcaro</td>
    <td>Ben A. Jones</td>
    <td>2:01 3/5</td>
    </tr>
	<tr >    <td >1951</td>
    <td><a href="/Count_Turf">Count Turf</a></td>
    <td>Conn McCreary</td>
    <td>Sol Rutchick</td>
    <td>2:02 3/5</td>
    </tr>
	<tr class="odd">    <td >1950</td>
    <td><a href="/Middleground">Middleground</a></td>
    <td>William Boland</td>
    <td>Max Hirsch</td>
    <td>2:01 3/5</td>
    </tr>
	<tr >    <td >1949</td>
    <td><a href="/Ponder">Ponder</a></td>
    <td>Steve Brooks</td>
    <td>Ben A. Jones</td>
    <td>2:04 1/5</td>
    </tr>
	<tr class="odd">    <td >1948</td>
    <td><a href="/Citation">Citation</a></td>
    <td>Eddie Arcaro</td>
    <td>Ben A. Jones</td>
    <td>2:05 2/5</td>
    </tr>
	<tr >    <td >1947</td>
    <td><a href="/Jet_Pilot">Jet Pilot</a></td>
    <td>Eric Guerin</td>
    <td>Tom Smith</td>
    <td>2:06 4/5</td>
    </tr>
	<tr class="odd">    <td >1946</td>
    <td><a href="/Assault">Assault</a></td>
    <td>Warren Mehrtens</td>
    <td>Max Hirsch</td>
    <td>2:06 3/5</td>
    </tr>
	<tr >    <td >1945</td>
    <td><a href="/Hoop_Jr">Hoop Jr</a></td>
    <td>Eddie Arcaro</td>
    <td>Ivan Parke</td>
    <td>2:07</td>
    </tr>
	<tr class="odd">    <td >1944</td>
    <td><a href="/Pensive">Pensive</a></td>
    <td>Conn McCreary</td>
    <td>Ben A. Jones</td>
    <td>2:04 1/5</td>
    </tr>
	<tr >    <td >1943</td>
    <td><a href="/Count_Fleet">Count Fleet</a></td>
    <td>Johnny Longden</td>
    <td>Don Cameron</td>
    <td>2:04</td>
    </tr>
	<tr class="odd">    <td >1942</td>
    <td><a href="/Shut_Out">Shut Out</a></td>
    <td>Wayne Wright</td>
    <td>John M. Gaver</td>
    <td>2:04 2/5</td>
    </tr>
	<tr >    <td >1941</td>
    <td><a href="/Whirlaway">Whirlaway</a></td>
    <td>Eddie Arcaro</td>
    <td>Ben A. Jones</td>
    <td>2:01 2/5</td>
    </tr>
	<tr class="odd">    <td >1940</td>
    <td><a href="/Gallahadion">Gallahadion</a></td>
    <td>Carroll Bierman</td>
    <td>Roy Waldron</td>
    <td>2:05</td>
    </tr>
	<tr >    <td >1939</td>
    <td><a href="/Johnstown">Johnstown</a></td>
    <td>James Stout</td>
    <td>Sunny Jim Fitzsimmons</td>
    <td>2:03 2/5</td>
    </tr>
	<tr class="odd">    <td >1938</td>
    <td><a href="/Lawrin">Lawrin</a></td>
    <td>Eddie Arcaro</td>
    <td>Ben A. Jones</td>
    <td>2:04 4/5</td>
    </tr>
	<tr >    <td >1937</td>
    <td><a href="/War_Admiral">War Admiral</a></td>
    <td>Charley Kurtsinger</td>
    <td>George Conway</td>
    <td>2:03 1/5</td>
    </tr>
	<tr class="odd">    <td >1936</td>
    <td><a href="/Bold_Venture">Bold Venture</a></td>
    <td>Ira Hanford</td>
    <td>Max Hirsch</td>
    <td>2:03 3/5</td>
    </tr>
	<tr >    <td >1935</td>
    <td><a href="/Omaha">Omaha</a></td>
    <td>Willie Saunders</td>
    <td>Sunny Jim Fitzsimmons</td>
    <td>2:05</td>
    </tr>
	<tr class="odd">    <td >1934</td>
    <td><a href="/Cavalcade">Cavalcade</a></td>
    <td>Mack Garner</td>
    <td>Bob Smith</td>
    <td>2:04</td>
    </tr>
	<tr >    <td >1933</td>
    <td><a href="/Brokers_Tip">Brokers Tip</a></td>
    <td>Don Meade</td>
    <td>Henry J. "Dick" Thompson</td>
    <td>2:06 4/5</td>
    </tr>
	<tr class="odd">    <td >1932</td>
    <td><a href="/Burgoo_King">Burgoo King</a></td>
    <td>Eugene James</td>
    <td>Henry J. "Dick" Thompson</td>
    <td>2:05 1/5</td>
    </tr>
	<tr >    <td >1931</td>
    <td><a href="/Twenty_Grand">Twenty Grand</a></td>
    <td>Charley Kurtsinger</td>
    <td>James Rowe, Jr.</td>
    <td>2:01 4/5</td>
    </tr>
	<tr class="odd">    <td >1930</td>
    <td><a href="/Gallant_Fox">Gallant Fox</a></td>
    <td>Earl Sande</td>
    <td>Sunny Jim Fitzsimmons</td>
    <td>2:07 3/5</td>
    </tr>
	<tr >    <td >1929</td>
    <td><a href="/Clyde_Van_Dusen">Clyde Van Dusen</a></td>
    <td>Linus McAtee</td>
    <td>Clyde Van Dusen</td>
    <td>2:10 4/5</td>
    </tr>
	<tr class="odd">    <td >1928</td>
    <td><a href="/Reigh_Count">Reigh Count</a></td>
    <td>Chick Lang</td>
    <td>Bert Micchell</td>
    <td>2:10 2/5</td>
    </tr>
	<tr >    <td >1927</td>
    <td><a href="/Whiskery">Whiskery</a></td>
    <td>Linus McAtee</td>
    <td>Fred Hopkins</td>
    <td>2:06</td>
    </tr>
	<tr class="odd">    <td >1926</td>
    <td><a href="/Bubbling_Over">Bubbling Over</a></td>
    <td>Albert Johnson</td>
    <td>Henry J. "Dick" Thompson</td>
    <td>2:03 4/5</td>
    </tr>
	<tr >    <td >1925</td>
    <td><a href="/Flying_Ebony">Flying Ebony</a></td>
    <td>Earl Sande</td>
    <td>William Duke</td>
    <td>2:07 3/5</td>
    </tr>
	<tr class="odd">    <td >1924</td>
    <td><a href="/Black_Gold">Black Gold</a></td>
    <td>John D. Mooney</td>
    <td>Hanley Webb</td>
    <td>2:05 1/5</td>
    </tr>
	<tr >    <td >1923</td>
    <td><a href="/Zev">Zev</a></td>
    <td>Earl Sande</td>
    <td>David J. Leary</td>
    <td>2:05 2/5</td>
    </tr>
	<tr class="odd">    <td >1922</td>
    <td><a href="/Morvich">Morvich</a></td>
    <td>Albert Johnson</td>
    <td>Fred Burlew</td>
    <td>2:04 3/5</td>
    </tr>
	<tr >    <td >1921</td>
    <td><a href="/Behave_Yourself">Behave Yourself</a></td>
    <td>Charles Thompson</td>
    <td>Henry J. "Dick" Thompson</td>
    <td>2:04 1/5</td>
    </tr>
	<tr class="odd">    <td >1920</td>
    <td><a href="/Paul_Jones">Paul Jones</a></td>
    <td>Ted Rice</td>
    <td>Billy Garth</td>
    <td>2:09</td>
    </tr>
	<tr >    <td >1919</td>
    <td><a href="/Sir_Barton">Sir Barton</a></td>
    <td>Johnny Loftus</td>
    <td>H. Guy Bedwell</td>
    <td>2:09 4/5</td>
    </tr>
	<tr class="odd">    <td >1918</td>
    <td><a href="/Exterminator">Exterminator</a></td>
    <td>William Knapp</td>
    <td>Henry McDaniel</td>
    <td>2:10 4/5</td>
    </tr>
	<tr >    <td >1917</td>
    <td><a href="/Omar_Khayyam">Omar Khayyam</a></td>
    <td>Charles Borel</td>
    <td>C.T. Patterson</td>
    <td>2:04 3/5</td>
    </tr>
	<tr class="odd">    <td >1916</td>
    <td><a href="/George_Smith">George Smith</a></td>
    <td>Johnny Loftus</td>
    <td>Hollie Hughes</td>
    <td>2:04</td>
    </tr>
	<tr >    <td >1915</td>
    <td><a href="/Regret">Regret</a></td>
    <td>Joe Notter</td>
    <td>James Rowe, Sr.</td>
    <td>2:05 2/5</td>
    </tr>
	<tr class="odd">    <td >1914</td>
    <td><a href="/Old_Rosebud">Old Rosebud</a></td>
    <td>John McCabe</td>
    <td>F. D. Weir</td>
    <td>2:03 2/5</td>
    </tr>
	<tr >    <td >1913</td>
    <td><a href="/Donerail">Donerail</a></td>
    <td>Roscoe Goose</td>
    <td>Thomas Hayes</td>
    <td>2:04 4/5</td>
    </tr>
	<tr class="odd">    <td >1912</td>
    <td><a href="/Worth">Worth</a></td>
    <td>Carol Shilling</td>
    <td>Frank Taylor</td>
    <td>2:09 2/5</td>
    </tr>
	<tr >    <td >1911</td>
    <td><a href="/Meridian">Meridian</a></td>
    <td>George Archibald</td>
    <td>Albert Ewing</td>
    <td>2:05</td>
    </tr>
	<tr class="odd">    <td >1910</td>
    <td><a href="/Donau">Donau</a></td>
    <td>Fred Herbert</td>
    <td>George Ham</td>
    <td>2:06 2/5</td>
    </tr>
	<tr >    <td >1909</td>
    <td><a href="/Wintergreen">Wintergreen</a></td>
    <td>Vincent Powers</td>
    <td>Charles Mack</td>
    <td>2:08 1/5</td>
    </tr>
	<tr class="odd">    <td >1908</td>
    <td><a href="/Stone_Street">Stone Street</a></td>
    <td>Arthur Pickens</td>
    <td>J. W. Hall</td>
    <td>2:15 1/5</td>
    </tr>
	<tr >    <td >1907</td>
    <td><a href="/Pink_Star">Pink Star</a></td>
    <td>Andy Minder</td>
    <td>W. H. Fizer</td>
    <td>2:12 3/5</td>
    </tr>
	<tr class="odd">    <td >1906</td>
    <td><a href="/Sir_Huon">Sir Huon</a></td>
    <td>Roscoe Troxler</td>
    <td>Pete Coyne</td>
    <td>2:08 4/5</td>
    </tr>
	<tr >    <td >1905</td>
    <td><a href="/Agile">Agile</a></td>
    <td>Jack Martin</td>
    <td>Robert Tucker</td>
    <td>2:10 3/4</td>
    </tr>
	<tr class="odd">    <td >1904</td>
    <td><a href="/Elwood">Elwood</a></td>
    <td>Shorty Prior</td>
    <td>C. E. Durnell</td>
    <td>2:08 1/2</td>
    </tr>
	<tr >    <td >1903</td>
    <td><a href="/Judge_Himes">Judge Himes</a></td>
    <td>Hal Booker</td>
    <td>J. P. Mayberry</td>
    <td>2:09</td>
    </tr>
	<tr class="odd">    <td >1902</td>
    <td><a href="/Alan-a-Dale">Alan-a-Dale</a></td>
    <td>Jimmy Winkfield</td>
    <td>T. C. McDowell</td>
    <td>2:08 3/4</td>
    </tr>
	<tr >    <td >1901</td>
    <td><a href="/His_Eminence">His Eminence</a></td>
    <td>Jimmy Winkfield</td>
    <td>F. P. Van Meter</td>
    <td>2:07 3/4</td>
    </tr>
	<tr class="odd">    <td >1900</td>
    <td><a href="/Lieut._Gibson">Lieut. Gibson</a></td>
    <td>Jimmy Boland</td>
    <td>Charles Hughes</td>
    <td>2:06 1/4</td>
    </tr>
	<tr >    <td >1899</td>
    <td><a href="/Manuel">Manuel</a></td>
    <td>Fred Taral</td>
    <td>Robert J. Walden</td>
    <td>2:12</td>
    </tr>
	<tr class="odd">    <td >1898</td>
    <td><a href="/Plaudit">Plaudit</a></td>
    <td>Willie Simms</td>
    <td>John E. Madden</td>
    <td>2:09</td>
    </tr>
	<tr >    <td >1897</td>
    <td><a href="/Typhoon_II">Typhoon II</a></td>
    <td>Buttons Garner</td>
    <td>J. C. Cahn</td>
    <td>2:12 1/2</td>
    </tr>
	<tr class="odd">    <td >1896</td>
    <td><a href="/Ben_Brush">Ben Brush</a></td>
    <td>Willie Simms</td>
    <td>Hardy Campbell</td>
    <td>2:07 3/4</td>
    </tr>
	<tr >    <td >1895</td>
    <td><a href="/Halma">Halma</a></td>
    <td>Soup Perkins</td>
    <td>Byron McClelland</td>
    <td>2:37 1/2</td>
    </tr>
	<tr class="odd">    <td >1894</td>
    <td><a href="/Chant">Chant</a></td>
    <td>Frank Goodale</td>
    <td>Eugene Leigh</td>
    <td>2:41</td>
    </tr>
	<tr >    <td >1893</td>
    <td><a href="/Lookout">Lookout</a></td>
    <td>E. Kunze</td>
    <td>William McDaniel</td>
    <td>2:39 1/4</td>
    </tr>
	<tr class="odd">    <td >1892</td>
    <td><a href="/Azra">Azra</a></td>
    <td>Lonnie Clayton</td>
    <td>John Morris</td>
    <td>2:41 1/2</td>
    </tr>
	<tr >    <td >1891</td>
    <td><a href="/Kingman">Kingman</a></td>
    <td>Isaac Murphy</td>
    <td>Dud Allen</td>
    <td>2:52 1/4</td>
    </tr>
	<tr class="odd">    <td >1890</td>
    <td><a href="/Riley">Riley</a></td>
    <td>Isaac Murphy</td>
    <td>Edward Corrigan</td>
    <td>2:45</td>
    </tr>
	<tr >    <td >1889</td>
    <td><a href="/Spokane">Spokane</a></td>
    <td>Thomas Kiley</td>
    <td>John Rodegap</td>
    <td>2:34 1/2</td>
    </tr>
	<tr class="odd">    <td >1888</td>
    <td><a href="/Macbeth_II">Macbeth II</a></td>
    <td>George Covington</td>
    <td>John Campbell</td>
    <td>2:38</td>
    </tr>
	<tr >    <td >1887</td>
    <td><a href="/Montrose">Montrose</a></td>
    <td>Isaac Lewis</td>
    <td>John McGinty</td>
    <td>2:39 1/4</td>
    </tr>
	<tr class="odd">    <td >1886</td>
    <td><a href="/Ben_Ali">Ben Ali</a></td>
    <td>Paul Duffy</td>
    <td>Jim Murphy</td>
    <td>2:36 1/2</td>
    </tr>
	<tr >    <td >1885</td>
    <td><a href="/Joe_Cotton">Joe Cotton</a></td>
    <td>Babe Henderson</td>
    <td>Alex Perry</td>
    <td>2:37 1/4</td>
    </tr>
	<tr class="odd">    <td >1884</td>
    <td><a href="/Buchanan">Buchanan</a></td>
    <td>Isaac Murphy</td>
    <td>William Bird</td>
    <td>2:40 1/4</td>
    </tr>
	<tr >    <td >1883</td>
    <td><a href="/Leonatus">Leonatus</a></td>
    <td>Billy Donohue</td>
    <td>John McGinty</td>
    <td>2:43</td>
    </tr>
	<tr class="odd">    <td >1882</td>
    <td><a href="/Apollo">Apollo</a></td>
    <td>Babe Hurd</td>
    <td>Green Morris</td>
    <td>2:40</td>
    </tr>
	<tr >    <td >1881</td>
    <td><a href="/Hindoo">Hindoo</a></td>
    <td>Jim McLaughlin</td>
    <td>James Rowe, Sr.</td>
    <td>2:40</td>
    </tr>
	<tr class="odd">    <td >1880</td>
    <td><a href="/Fonso">Fonso</a></td>
    <td>George Lewis</td>
    <td>Tice Hutsell</td>
    <td>2:37 1/2</td>
    </tr>
	<tr >    <td >1879</td>
    <td><a href="/Lord_Murphy">Lord Murphy</a></td>
    <td>Charlie Shauer</td>
    <td>George Rice</td>
    <td>2:37</td>
    </tr>
	<tr class="odd">    <td >1878</td>
    <td><a href="/Day_Star">Day Star</a></td>
    <td>J. Carter</td>
    <td>Lee Paul</td>
    <td>2:37 1/4</td>
    </tr>
	<tr >    <td >1877</td>
    <td><a href="/Baden_Baden">Baden Baden</a></td>
    <td>Billy Walker</td>
    <td>Ed Brown (aka "Dick")</td>
    <td>2:38</td>
    </tr>
	<tr class="odd">    <td >1875</td>
    <td><a href="/Aristides">Aristides</a></td>
    <td>Oliver Lewis</td>
    <td>Ansel Williamson</td>
    <td>2:37 3/4</td>
    </tr>
	  
</table>
