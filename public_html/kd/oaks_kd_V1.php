{literal}	
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2020 Kentucky Oaks Odds"
	  }
	</script>{/literal}	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="The latest odds for the Kentucky Oaks available for wagering now at BUSR.">
			<caption>Kentucky Oaks Odds</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 12, 2020.</em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
			<tbody>

			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Kentucky Oaks  - May 01					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">2020 Kentucky Oaks - To Win</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Bast</td><td>7/2</td><td>+350</td></tr><tr><td>British Idiom</td><td>8/1</td><td>+800</td></tr><tr><td>Donna Veloce</td><td>12/1</td><td>+1200</td></tr><tr><td>Taraz</td><td>10/1</td><td>+1000</td></tr><tr><td>Finite</td><td>16/1</td><td>+1600</td></tr><tr><td>Lake Avenue</td><td>18/1</td><td>+1800</td></tr><tr><td>Blame Debbie</td><td>20/1</td><td>+2000</td></tr><tr><td>Maedean</td><td>20/1</td><td>+2000</td></tr><tr><td>Perfect Alibi</td><td>20/1</td><td>+2000</td></tr><tr><td>Wicked Whisper</td><td>20/1</td><td>+2000</td></tr><tr><td>Comical</td><td>25/1</td><td>+2500</td></tr><tr><td>I Dare U</td><td>25/1</td><td>+2500</td></tr><tr><td>Lazy Daisy</td><td>25/1</td><td>+2500</td></tr><tr><td>Two Sixty</td><td>25/1</td><td>+2500</td></tr><tr><td>Alandra</td><td>33/1</td><td>+3300</td></tr><tr><td>Pass The Plate</td><td>33/1</td><td>+3300</td></tr><tr><td>Tempers Rising</td><td>33/1</td><td>+3300</td></tr><tr><td>K P Dreamin</td><td>66/1</td><td>+6600</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o0t');</script>
{/literal}	
	