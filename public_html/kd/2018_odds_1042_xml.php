	<div>
		<table  class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Margin of Victory"  summary="The latest odds for the Kentucky Derby available for wagering now at BUSR.">
			<caption>2018 Kentucky Derby Matchups</caption>
			<tbody>
				<tr>
					<th colspan="3" class="center">
					2018 Kentucky Derby Matchups  - Apr 23					</th>
			</tr>
	<tr class='head_title'><th>Team</th><th>ML</th></tr><tr><td style="background:white">Justify</td><td style="background:white">-170</td></tr><tr><td style="background:white">Mendelssohn</td><td style="background:white">+140</td></tr><tr><td style="background:#ddd">Justify</td><td style="background:#ddd">-215</td></tr><tr><td style="background:#ddd">Audible</td><td style="background:#ddd">+175</td></tr><tr><td style="background:white">Justify</td><td style="background:white">-170</td></tr><tr><td style="background:white">Bolt D'oro</td><td style="background:white">+140</td></tr><tr><td style="background:#ddd">Justify</td><td style="background:#ddd">-205</td></tr><tr><td style="background:#ddd">Good Magic</td><td style="background:#ddd">+170</td></tr><tr><td style="background:white">Justify</td><td style="background:white">-215</td></tr><tr><td style="background:white">Magnum Moon</td><td style="background:white">+175</td></tr><tr><td style="background:#ddd">Audible</td><td style="background:#ddd">-115</td></tr><tr><td style="background:#ddd">Magnum Moon</td><td style="background:#ddd">-115</td></tr><tr><td style="background:white">Mendelsshohn</td><td style="background:white">-115</td></tr><tr><td style="background:white">Bolt D'oro</td><td style="background:white">-115</td></tr><tr><td style="background:#ddd">Good Magic</td><td style="background:#ddd">-120</td></tr><tr><td style="background:#ddd">Magnum Moon</td><td style="background:#ddd">-110</td></tr><tr><td style="background:white">Good Magic</td><td style="background:white">-120</td></tr><tr><td style="background:white">Audible</td><td style="background:white">-110</td></tr><tr><td style="background:#ddd">My Boy Jack</td><td style="background:#ddd">-115</td></tr><tr><td style="background:#ddd">Solomini</td><td style="background:#ddd">-115</td></tr><tr><td style="background:white">Noble Indy</td><td style="background:white">-120</td></tr><tr><td style="background:white">Gronkowski</td><td style="background:white">-110</td></tr><tr><td style="background:#ddd">Lone Sailor</td><td style="background:#ddd">-115</td></tr><tr><td style="background:#ddd">Firenze Fire</td><td style="background:#ddd">-115</td></tr><tr><td style="background:white">Promises Fulfilled</td><td style="background:white">-115</td></tr><tr><td style="background:white">Bravazo</td><td style="background:white">-115</td></tr>					<tr>
						<td class="dateUpdated center" colspan="5">
							<em id='updateemp'>Updated April 24, 2018 17:44:59.</em>
							BUSR - Official <a href="https://www.usracing.com/kentucky-derby/odds">Kentucky Derby Odds</a>. 

<!-- br />
							All odds are fixed odds prices. -->
						</td>
					</tr>
			</tbody>
		</table>
	</div>
	