    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0" title="Kentucky Derby Jockey Odds" summary="The latest odds for the top Jockeys for the Kentucky Derby.  Only available at BUSR.">
            <caption>Kentucky Derby Jockey Odds</caption>
            <tbody>
               <!--
 <tr>
                    <th colspan="3" class="center">
                    Horses - 2018 Kentucky Derby Jockey  - May 05                    </th>
            </tr>
-->
    <tr><th colspan="3" class="center">2018 Kentucky Derby Jockey - To Win</th></tr><tr class='head_title'><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Paco Lopez</td><td>+6000</td><td>60/1</td></tr><tr><td>Robby Albarado</td><td>+3500</td><td>35/1</td></tr><tr><td>Corey Lanerie</td><td>+5000</td><td>50/1</td></tr><tr><td>Jose Lezcano</td><td>+2500</td><td>25/1</td></tr><tr><td>Javier Castellano</td><td>+700</td><td>7/1</td></tr><tr><td>Jose Ortiz</td><td>+800</td><td>8/1</td></tr><tr><td>Mike Smith</td><td>+350</td><td>7/2</td></tr><tr><td>James Graham</td><td>+5000</td><td>50/1</td></tr><tr><td>Irad Ortiz, Jr.</td><td>+1700</td><td>17/1</td></tr><tr><td>Kent Desormeaux</td><td>+2000</td><td>20/1</td></tr><tr><td>Victor Espinoza</td><td>+600</td><td>6/1</td></tr><tr><td>Junior Alvarado</td><td>+2500</td><td>25/1</td></tr><tr><td>Luis Contreras</td><td>+5000</td><td>50/1</td></tr><tr><td>Ryan Moore</td><td>+550</td><td>11/2</td></tr><tr><td>Drayden Van Dyke</td><td>+4000</td><td>40/1</td></tr><tr><td>Luis Saez</td><td>+700</td><td>7/1</td></tr><tr><td>Flavien Prat</td><td>+2500</td><td>25/1</td></tr><tr><td>John Velazquez</td><td>+1200</td><td>12/1</td></tr><tr><td>Florent Geroux</td><td>+4000</td><td>40/1</td></tr><tr><td>Ricardo Santana</td><td>+5000</td><td>50/1</td></tr><tr><td>Kyle Frey</td><td>+6000</td><td>60/1</td></tr>                    <tr>
                        <td class="dateUpdated center" colspan="5">
                            <em id='updateemp'>  - Updated June 29, 2018 17:40:04 </em>
                            BUSR - Official <a href="https://www.usracing.com/kentucky-derby/jockey-betting">Kentucky Derby Jockey Odds</a>. <!-- br />
                            All odds are fixed odds prices. -->
                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
    