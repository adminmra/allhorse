{literal} <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "ItemList",
        "url": "https://www.usracing.com/kentucky-derby/odds",
        "itemListElement": [  {
												"@type": "Product",
												"name": "A Thread Of Blue",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+9000"
												}
											}, {
												"@type": "Product",
												"name": "Achilles Warrior",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+21500"
												}
											}, {
												"@type": "Product",
												"name": "Admiral Rous",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Admire",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+17500"
												}
											}, {
												"@type": "Product",
												"name": "Alwaysmining",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+2200"
												}
											}, {
												"@type": "Product",
												"name": "American Camp",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+30000"
												}
											}, {
												"@type": "Product",
												"name": "Americandy",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+29000"
												}
											}, {
												"@type": "Product",
												"name": "And Seek",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Angelo's Pride",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Anothertwistafate",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+3200"
												}
											}, {
												"@type": "Product",
												"name": "Aquadini",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22000"
												}
											}, {
												"@type": "Product",
												"name": "Avie's Flatter",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+6000"
												}
											}, {
												"@type": "Product",
												"name": "Award Winner",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+18000"
												}
											}, {
												"@type": "Product",
												"name": "B P Rocket",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Bankit",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+16000"
												}
											}, {
												"@type": "Product",
												"name": "Bellafina",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Big Scott Daddy",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Blenheim Palace",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+27500"
												}
											}, {
												"@type": "Product",
												"name": "Bodexpress",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+2800"
												}
											}, {
												"@type": "Product",
												"name": "Boldor",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+10000"
												}
											}, {
												"@type": "Product",
												"name": "Bourbon War",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+2500"
												}
											}, {
												"@type": "Product",
												"name": "By My Standards",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+2200"
												}
											}, {
												"@type": "Product",
												"name": "Camgo",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Castle Casanova",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Cave Run",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Chase The Ghost",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+21000"
												}
											}, {
												"@type": "Product",
												"name": "Chess Chief",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Classy John",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12500"
												}
											}, {
												"@type": "Product",
												"name": "Code Of Honor",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+1400"
												}
											}, {
												"@type": "Product",
												"name": "Come On Gerry",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+11500"
												}
											}, {
												"@type": "Product",
												"name": "Comedian",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Copper King",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+27500"
												}
											}, {
												"@type": "Product",
												"name": "Corruze",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Counter Offer",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Country House",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+4500"
												}
											}, {
												"@type": "Product",
												"name": "Country Singer",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Curlaway",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+30000"
												}
											}, {
												"@type": "Product",
												"name": "Cutting Humor",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+2800"
												}
											}, {
												"@type": "Product",
												"name": "Dabo",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Derma Louvre",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Dessman",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+11000"
												}
											}, {
												"@type": "Product",
												"name": "Dorrance",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+17500"
												}
											}, {
												"@type": "Product",
												"name": "Doups Point",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+30000"
												}
											}, {
												"@type": "Product",
												"name": "Dream Maker",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+7500"
												}
											}, {
												"@type": "Product",
												"name": "Dull Knife",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Dunph",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Easy Shot",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Everfast",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Extra Hope",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+7000"
												}
											}, {
												"@type": "Product",
												"name": "Eye Cloud",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+32500"
												}
											}, {
												"@type": "Product",
												"name": "Family Biz",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Fayette Warrior",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Federal Case",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Final Jeopardy",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+10000"
												}
											}, {
												"@type": "Product",
												"name": "Five Star General",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Forloveofcountry",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Forty Under",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+11000"
												}
											}, {
												"@type": "Product",
												"name": "Frank'sgunisloaded",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+23000"
												}
											}, {
												"@type": "Product",
												"name": "Frolic More",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+30000"
												}
											}, {
												"@type": "Product",
												"name": "Frosted Ice",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+10000"
												}
											}, {
												"@type": "Product",
												"name": "Fullness Of Time",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+17500"
												}
											}, {
												"@type": "Product",
												"name": "Galilean",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+5000"
												}
											}, {
												"@type": "Product",
												"name": "Game Winner",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+300"
												}
											}, {
												"@type": "Product",
												"name": "Go Away",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+17500"
												}
											}, {
												"@type": "Product",
												"name": "Gray Attempt",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+3500"
												}
											}, {
												"@type": "Product",
												"name": "Gray Magician",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+4000"
												}
											}, {
												"@type": "Product",
												"name": "Green Fleet",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Growth Engine",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Gum Tree Lane",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Gun It",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+16000"
												}
											}, {
												"@type": "Product",
												"name": "Hackberry",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+30000"
												}
											}, {
												"@type": "Product",
												"name": "Haikal",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+2200"
												}
											}, {
												"@type": "Product",
												"name": "Harvey Wallbanger",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+10000"
												}
											}, {
												"@type": "Product",
												"name": "He's Smokin Hot",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+19000"
												}
											}, {
												"@type": "Product",
												"name": "Henley's Joy",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Higgins",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+29000"
												}
											}, {
												"@type": "Product",
												"name": "Hog Creek Hustle",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+10000"
												}
											}, {
												"@type": "Product",
												"name": "Honest Mischief",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12500"
												}
											}, {
												"@type": "Product",
												"name": "Hustle Up",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Identifier",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+17500"
												}
											}, {
												"@type": "Product",
												"name": "Improbable",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+800"
												}
											}, {
												"@type": "Product",
												"name": "Incorrigible",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Inventing Blame",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Instagrand",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+3000"
												}
											}, {
												"@type": "Product",
												"name": "Irish Heatwave",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Jerome Avenue",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+27500"
												}
											}, {
												"@type": "Product",
												"name": "Jersey Agenda",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+14000"
												}
											}, {
												"@type": "Product",
												"name": "Kadens Courage",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Kaziranga",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+26000"
												}
											}, {
												"@type": "Product",
												"name": "King For A Day",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+14000"
												}
											}, {
												"@type": "Product",
												"name": "King Ford",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+13000"
												}
											}, {
												"@type": "Product",
												"name": "King Of Speed",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Kingly",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+11000"
												}
											}, {
												"@type": "Product",
												"name": "Knight's Cross",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Last Judgment",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+10000"
												}
											}, {
												"@type": "Product",
												"name": "Laughing Fox",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12500"
												}
											}, {
												"@type": "Product",
												"name": "Lemniscate",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Lieutenant Dan",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Limonite",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12500"
												}
											}, {
												"@type": "Product",
												"name": "Long Range Toddy",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+1600"
												}
											}, {
												"@type": "Product",
												"name": "Lord Dragon",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+24000"
												}
											}, {
												"@type": "Product",
												"name": "Lucky Lee",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+17500"
												}
											}, {
												"@type": "Product",
												"name": "Lutsky",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Magnificent Mccool",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+21000"
												}
											}, {
												"@type": "Product",
												"name": "Malibu In Motion",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+27500"
												}
											}, {
												"@type": "Product",
												"name": "Malpais",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Manguzi",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Manny Wah",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+10000"
												}
											}, {
												"@type": "Product",
												"name": "Market King",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12500"
												}
											}, {
												"@type": "Product",
												"name": "Masaff",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+26500"
												}
											}, {
												"@type": "Product",
												"name": "Master Fencer",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12500"
												}
											}, {
												"@type": "Product",
												"name": "Maximum Security",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+1000"
												}
											}, {
												"@type": "Product",
												"name": "Mayor Cobb",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Moonster",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "More Ice",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Motagally",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+14000"
												}
											}, {
												"@type": "Product",
												"name": "Mr. Ankeny",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Mr. Money",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+9000"
												}
											}, {
												"@type": "Product",
												"name": "Much Better",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+10000"
												}
											}, {
												"@type": "Product",
												"name": "Mucho Gusto",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+4000"
												}
											}, {
												"@type": "Product",
												"name": "My Mandate",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+19000"
												}
											}, {
												"@type": "Product",
												"name": "Ninth Street",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Nitrous",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12000"
												}
											}, {
												"@type": "Product",
												"name": "Nolo Contesto",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+8000"
												}
											}, {
												"@type": "Product",
												"name": "Not That Brady",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+17500"
												}
											}, {
												"@type": "Product",
												"name": "Nova Lenda",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Olympic Runner",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+14000"
												}
											}, {
												"@type": "Product",
												"name": "Omaha Beach",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+1500"
												}
											}, {
												"@type": "Product",
												"name": "Oncewewerebrothers",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+16500"
												}
											}, {
												"@type": "Product",
												"name": "One Bad Boy",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Our Braintrust",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+8000"
												}
											}, {
												"@type": "Product",
												"name": "Outshine",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+4000"
												}
											}, {
												"@type": "Product",
												"name": "Over Protective",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Overdeliver",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+10000"
												}
											}, {
												"@type": "Product",
												"name": "Owendale",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+10000"
												}
											}, {
												"@type": "Product",
												"name": "Passion Play",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+27500"
												}
											}, {
												"@type": "Product",
												"name": "Plus Que Parfait",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+2500"
												}
											}, {
												"@type": "Product",
												"name": "Pole Setter",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Puttheglassdown",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+19000"
												}
											}, {
												"@type": "Product",
												"name": "Roadster",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+3500"
												}
											}, {
												"@type": "Product",
												"name": "Roiland",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+9000"
												}
											}, {
												"@type": "Product",
												"name": "Romantico",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Rowayton",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+6500"
												}
											}, {
												"@type": "Product",
												"name": "Royal Marine",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12500"
												}
											}, {
												"@type": "Product",
												"name": "Royal Meeting",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Savagery",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12500"
												}
											}, {
												"@type": "Product",
												"name": "Seismic Wave",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22000"
												}
											}, {
												"@type": "Product",
												"name": "Shazier",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+29000"
												}
											}, {
												"@type": "Product",
												"name": "Shining Through",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Shir Khan",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Shootin The Breeze",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Signalman",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+4000"
												}
											}, {
												"@type": "Product",
												"name": "Sir Winston",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Six Shooter",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+11000"
												}
											}, {
												"@type": "Product",
												"name": "Skywire",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "So Alive",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+14000"
												}
											}, {
												"@type": "Product",
												"name": "Soldado",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12500"
												}
											}, {
												"@type": "Product",
												"name": "Somelikeithotbrown",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+6500"
												}
											}, {
												"@type": "Product",
												"name": "Spanish Mission",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Spectacular Gem",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+26000"
												}
											}, {
												"@type": "Product",
												"name": "Spinoff",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+2800"
												}
											}, {
												"@type": "Product",
												"name": "Standard Deviation",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+10000"
												}
											}, {
												"@type": "Product",
												"name": "Still Dreaming",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+24000"
												}
											}, {
												"@type": "Product",
												"name": "Stilts",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+21500"
												}
											}, {
												"@type": "Product",
												"name": "Sueno",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+4000"
												}
											}, {
												"@type": "Product",
												"name": "Synthesis",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Tacitus",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+3000"
												}
											}, {
												"@type": "Product",
												"name": "Tap The Wire",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+17500"
												}
											}, {
												"@type": "Product",
												"name": "Tapit Wise",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Tax",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+2000"
												}
											}, {
												"@type": "Product",
												"name": "The Irish Rover",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "The Right Path",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+17500"
												}
											}, {
												"@type": "Product",
												"name": "Thirsty Betrayal",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+26500"
												}
											}, {
												"@type": "Product",
												"name": "Thomas Shelby",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Tikhvin Flew",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+10000"
												}
											}, {
												"@type": "Product",
												"name": "Tone Broke",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Town Bee",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+35000"
												}
											}, {
												"@type": "Product",
												"name": "Trifor Gold",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "U S Navy Cross",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Van Beethoven",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Vekoma",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+3000"
												}
											}, {
												"@type": "Product",
												"name": "Walker Stalker",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+30000"
												}
											}, {
												"@type": "Product",
												"name": "War Of Will",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+1800"
												}
											}, {
												"@type": "Product",
												"name": "Well Defined",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12500"
												}
											}, {
												"@type": "Product",
												"name": "Wendell Fong",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+16500"
												}
											}, {
												"@type": "Product",
												"name": "Who's In Charge",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Wicked Indeed",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12500"
												}
											}, {
												"@type": "Product",
												"name": "Win Win Win",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+2500"
												}
											}, {
												"@type": "Product",
												"name": "Young Phillip",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Zenden",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+8000"
												}
											}, {
												"@type": "Product",
												"name": "Zeruch",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Western Australia",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+17500"
												}
											}, {
												"@type": "Product",
												"name": "Cabot",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+22500"
												}
											}, {
												"@type": "Product",
												"name": "Cowboy Diplomacy",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+27500"
												}
											}, {
												"@type": "Product",
												"name": "Get The Prize",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12500"
												}
											}, {
												"@type": "Product",
												"name": "Oval Ace",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+14000"
												}
											}, {
												"@type": "Product",
												"name": "Weitblick",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Bandon Wood",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Grumps Little Tots",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+24000"
												}
											}, {
												"@type": "Product",
												"name": "Into Morocco",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Pyron",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+19000"
												}
											}, {
												"@type": "Product",
												"name": "Red Gum",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Play Money",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+35000"
												}
											}, {
												"@type": "Product",
												"name": "Captain Von Trapp",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+19000"
												}
											}, {
												"@type": "Product",
												"name": "Hoffa`s Union",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+9000"
												}
											}, {
												"@type": "Product",
												"name": "Pioneer Dancer",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+30000"
												}
											}, {
												"@type": "Product",
												"name": "Dynamic Racer",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Erlich",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+17500"
												}
											}, {
												"@type": "Product",
												"name": "Jahbath",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Litany",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+27500"
												}
											}, {
												"@type": "Product",
												"name": "Playa Del Puente",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+15000"
												}
											}, {
												"@type": "Product",
												"name": "Promo Code",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Sly",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+27500"
												}
											}, {
												"@type": "Product",
												"name": "Durkin's Call",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Joevia",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+20000"
												}
											}, {
												"@type": "Product",
												"name": "Presto",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+30000"
												}
											}, {
												"@type": "Product",
												"name": "Proverb",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+25000"
												}
											}, {
												"@type": "Product",
												"name": "Sayyaaf",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+27500"
												}
											}, {
												"@type": "Product",
												"name": "Warrior's Charge",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+27500"
												}
											}, {
												"@type": "Product",
												"name": "Spun To Run",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+16000"
												}
											}, {
												"@type": "Product",
												"name": "De Flug",
												"position": 1,
												"url": "https://www.usracing.com",
												"offers": {
													"@type": "Offer",
													"price": "+12500"
												}
											}  ]
    }
    </script> {/literal}