<div id="no-more-tables">
<table id="second" border="0" cellspacing="0" cellpadding="0"  class="data table table-condensed table-striped table-bordered ordenable">
  <thead><tr>
    <th>Wagering</th>
    <th>Pool 1</th>
    <th>Pool 2</th>
    <th>Pool 3</th>
    <th>Total</th>
  </tr></thead>
  <tbody>
  <tr>
    <td data-title="Year">2013</td>
    <td data-title="Pool 1">$621,493</td>
    <td data-title="Pool 2">$407,039</td>
    <td data-title="Pool 3">$402,167</td>
    <td data-title="Total">$1,430,699</td>
  </tr>
  <tr>
    <td data-title="Year">2012</td>
    <td data-title="Pool 1">$631,304#**</td>
    <td data-title="Pool 2">$411,368#</td>
    <td data-title="Pool 3">$427,347#</td>
    <td data-title="Total">$1,470,019***#</td>
  </tr>
  <tr>
    <td data-title="Year">2011</td>
    <td data-title="Pool 1">$592,492#</td>
    <td data-title="Pool 2">$376,699#</td>
    <td data-title="Pool 3">$392,910#</td>
    <td data-title="Total">$1,362,101#</td>
  </tr>
  
  <tr>
    <td data-title="Year">2010</td>
    <td data-title="Pool 1">$466,048#</td>
    <td data-title="Pool 2">$355,853#</td>
    <td data-title="Pool 3">$310,154#</td>
    <td data-title="Total">$1,132,055#</td>
  </tr>
  <tr>
    <td data-title="Year">2009</td>
    <td data-title="Pool 1">$478,721</td>
    <td data-title="Pool 2">$380,420#</td>
    <td data-title="Pool 3">$377,158#</td>
    <td data-title="Total">$1,236,299</td>
  </tr>
  <tr>
    <td data-title="Year">2008</td>
    <td data-title="Pool 1">$439,379</td>
    <td data-title="Pool 2">$325,306</td>
    <td data-title="Pool 3">$291,835</td>
    <td data-title="Total">$1,056,520</td>
  </tr>
  <tr>
    <td data-title="Year">2007</td>
    <td data-title="Pool 1">$520,688</td>
    <td data-title="Pool 2">$379,613</td>
    <td data-title="Pool 3">$465,123</td>
    <td data-title="Total">$1,365,424</td>
  </tr>
  <tr>
    <td data-title="Year">2006</td>
    <td data-title="Pool 1">$552,627</td>
    <td data-title="Pool 2">$464,236</td>
    <td data-title="Pool 3">$454,743</td>
    <td data-title="Total">$1,471,606</td>
  </tr>
  <tr>
    <td data-title="Year">2005</td>
    <td data-title="Pool 1">$620,362**</td>
    <td data-title="Pool 2">$511,655</td>
    <td data-title="Pool 3">$533,973</td>
    <td data-title="Total">$1,665,990****</td>
  </tr>
  <tr>
    <td data-title="Year">2004</td>
    <td data-title="Pool 1">$536,958</td>
    <td data-title="Pool 2">$358,966</td>
    <td data-title="Pool 3">$386,244</td>
    <td data-title="Total">$1,282,168</td>
  </tr>
  <tr>
    <td data-title="Year">2003</td>
    <td data-title="Pool 1">$516,906</td>
    <td data-title="Pool 2">$391,002</td>
    <td data-title="Pool 3">$222,261</td>
    <td data-title="Total">$1,130,169</td>
  </tr>
  <tr>
    <td data-title="Year">2002</td>
    <td data-title="Pool 1">$577,889</td>
    <td data-title="Pool 2">$401,070</td>
    <td data-title="Pool 3">$524,847</td>
    <td data-title="Total">$1,503,806</td>
  </tr>
  <tr>
    <td data-title="Year">2001</td>
    <td data-title="Pool 1">$510,815</td>
    <td data-title="Pool 2">$372,961</td>
    <td data-title="Pool 3">$425,871</td>
    <td data-title="Total">$1,309,647</td>
  </tr>
  <tr>
    <td data-title="Year">2000</td>
    <td data-title="Pool 1">$465,454</td>
    <td data-title="Pool 2">$306,259</td>
    <td data-title="Pool 3">$387,206</td>
    <td data-title="Total">$1,158,919</td>
  </tr>
  <tr>
    <td data-title="Year">1999</td>
    <td data-title="Pool 1">$267,748</td>
    <td data-title="Pool 2">$178,811</td>
    <td data-title="Pool 3">$229,674</td>
    <td data-title="Total">$676,233</td>
  </tr>
  </tbody>


</table>
</div>
<small>**Record individual pool betting    ****record three-pool wagering total # includes exacta pool</small>

