<table class="data" title="Kentucky Derby Betting - Purse" summary=" Kentucky Derby Purse">

   <caption> Kentucky Derby Purse Structure</caption> 

<tbody>

<tr>

      <th width="250">Result</th>

      <th>Purse of $2,180,000</th>

    </tr>

    <tr>

      <td>Winner</td>

      <td>$1,240,000 (62%) </td>

    </tr>

    <tr class="odd">

      <td>Second</td>

      <td>$400,000 (20%) </td>

    </tr>

    <tr>

      <td>Third</td>

      <td>$200,000 (10%) </td>

    </tr>

    <tr class="odd">

      <td>Fourth</td>

      <td>$100,000 (5%) </td>

    </tr>

    <tr>

      <td>Fifth</td>

      <td>$60,000 (3%)</td>

    </tr>

</tbody>

  </table>

  <br>



<table class="data"  summary="Kentucky Derby TV Schedule" title="Kentucky Derby Betting - TV Schedule" width="665">

  <caption>

  {include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Derby TV Schedule

  </caption>

<tbody>

<tr>

<th>Date</th>

<th>Time (ET)</th>



<th>Races</th>

<th>Provider</th>

</tr>

<tr>

<td><strong>May 2</strong></td>

<td>4:00 pm - 5:00 pm</td>

<td>Classic Horse Racing: Kentucky Derby</td>

<td>NBCSN</td>

</tr>

<tr class="odd">

<td></td>

<td>5:00 pm - 6:00 pm</td>

<td> {include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Derby Post-Position Draw</td>

<td>NBCSN</td>

</tr>



<tr>

<td><strong>May 3 </strong></td>

<td>4:00 pm - 5:00 pm</td>

<td> Kentucky Derby Classics</td>

<td>NBCSN</td>



</tr>

<tr class="odd">

<td>&nbsp;</td>

<td>5:00 pm - 5:30 pm</td>

<td>Kentucky Derby Access </td>

<td>NBCSN</td>



</tr>



<tr>

<td><strong>May 4 </strong></td>

<td>8:00 am - 10:00 am</td>

<td>Derby Countdown Show</td>

<td>HRRN</td>

</tr>

  <tr class="odd">

    <td></td>

    <td>9:30 am</td>

    <td>Kentucky Oaks Day Coverage</td>

    <td>HRTV</td>

  </tr>

<tr>

<td>&nbsp;</td>

<td> 4:00 pm - 5:00 pm</td>

<td>Derby Classics</td>

<td>NBCSN</td>



</tr>

  <tr class="odd">

    <td></td>

    <td>5:00 pm - 6:00 pm</td>

    <td>{include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Oaks</td>

    <td>NBCSN</td>

  </tr>





<tr>

<td><strong>May 5 </strong></td>

    <td>9:30 am</td>

    <td>Kentucky Derby Day Coverage</td>

    <td>HRTV</td>

</tr>

<tr class="odd">

<td>&nbsp;</td>

<td>11:00 am - 4:00 p.m</td>

<td>Kentucky Derby Undercard</td>

<td>NBCSN</td>

</tr>

<tr>

<td>&nbsp;</td>

<td>4:00 pm - 5:00 pm</td>

<td>Access at the Kentucky Derby / Red Carpet Special </td>

<td>NBC</td>



</tr>

<tr class="odd">

<td>&nbsp;</td>

<td>5:00 pm - 7:00 pm</td>

<td><strong>{include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Derby</strong></td>

<td>NBC Sports</td>



</tr>

<tr>

<td>&nbsp;</td>

<td>7:00 pm - 8:00 pm</td>

<td>Kentucky Derby Post Show</td>

<td>NBCSN</td>



</tr>

<tr>

<td colspan="8">*All times and dates are subject to change</td>

</tr>

</tbody>

</table>

<p>&nbsp;</p>

  <br>

  <table  class="data" title="Kentucky Derby Betting - Race Schedule"summary="Bet the {include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Derby with this Race Schedule">

    <caption>

   Saturday, {include_php file='/home/ah/allhorse/public_html/kd/date.php'}, KENTUCKY DERBY DAY Race Schedule

      </caption>

<tbody>

<tr>

    <th>Race# </th>

    <th>Post Time </th>

    <th>TV </th>

    <th>Race </th>

    <th>Age/Sex </th>

    <th>Distance </th>

    <th>Surface </th>

  </tr>

  <tr>

    <td>1</td>

    <td>10:30</td>

    <td>HRTV</td>

    <td>MdSpWt</td>

    <td>3up</td>

    <td>6.0</td>

    <td>Dirt</td>

  </tr>

  <tr   class="odd">

    <td>2</td>

    <td>11:00</td>

    <td>HRTV</td>

    <td>MdSpWt</td>

    <td>3up</td>

    <td>8.0</td>

    <td>Dirt</td>

  </tr>

  <tr>

    <td>3</td>

    <td>11:30</td>

    <td>NBCSN</td>

    <td>OC $75k/N1X</td>

    <td>3yo</td>

    <td>8.5</td>

    <td>Dirt</td>

  </tr>

  <tr   class="odd">

    <td>4</td>

    <td>12:04</td>

    <td>NBCSN</td>

    <td>OC $75k/N1X</td>

    <td>3yo</td>

    <td>7.0</td>

    <td>Dirt</td>

  </tr>

  <tr>

    <td>5</td>

    <td>12:38</td>

    <td>NBCSN</td>

    <td>OC $100k/N2X</td>

    <td>3yo</td>

    <td>8.5</td>

    <td>Dirt</td>

  </tr>

  <tr   class="odd">

    <td>6</td>

    <td>1:19</td>

    <td>NBCSN</td>

    <td>G3-Twin Spires Sprint</td>

    <td>4up</td>

    <td>5.0</td>

    <td>Turf</td>

  </tr>

  <tr>

    <td>7</td>

    <td>2:08</td>

    <td>NBCSN</td>

    <td>G1-Humana Distaff</td>

    <td>4up, f&amp;m</td>

    <td>7.0</td>

    <td>Dirt</td>

  </tr>

  <tr   class="odd">

    <td>8</td>

    <td>2:59</td>

    <td>NBCSN</td>

    <td>G2-Churchill Distaff Turf Mile</td>

    <td>4up, f&amp;m</td>

    <td>8.0</td>

    <td>Turf</td>

  </tr>

  <tr>

    <td>9</td>

    <td>3:51</td>

    <td>NBCSN</td>

    <td>G2-Churchill Downs</td>

    <td>4up</td>

    <td>7.0</td>

    <td>Dirt</td>

  </tr>

  <tr   class="odd">

    <td>10</td>

    <td>4:46</td>

    <td>NBC</td>

    <td>G1-Woodford Turf Classic</td>

    <td>4up</td>

    <td>9.0</td>

    <td>Turf</td>

  </tr>

  <tr>

    <td>11</td>

    <td>6:24</td>

    <td>NBC</td>

    <td>G1-Kentucky Derby</td>

    <td>3yo</td>

    <td>10.0</td>

    <td>Dirt</td>

  </tr>

  <tr   class="odd">

    <td>12</td>

    <td>7:20</td>

    <td>HRTV</td>

    <td>OC $80k/N3$</td>

    <td>3up</td>

    <td>8.5</td>

    <td>Turf</td>

  </tr>

  <tr>

    <td>13</td>

    <td>7:50</td>

    <td>HRTV</td>

    <td>Allowance N2$</td>

    <td>3up</td>

    <td>6.5</td>

    <td>Dirt</td>

  </tr> 

</tbody>

 </table>

*$40 general admission
