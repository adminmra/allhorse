{literal}
  <script type="application/ld+json">
 
        {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "name": " Get a Free Bet for the KD!",
        "startDate": "2021-04-30",
        "endDate": "2021-05-01",
        "sport": "Horse Racing",
        "location": {
            "@type": "Place",
            "name" : "Churchill Downs",
            "address": "700 Central Avenue Louisville, KY 40208"
			},
        "url": "https://www.usracing.com/kentucky-derby/betting"
    }
        {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "name": "Bet the 2021 Preakness Stakes",
        "startDate": "2021-05-15",
        "endDate": "2021-05-15",
        "sport": "Horse Racing",
        "location": {
            "@type": "Place",
            "name" : "Pimlico Race Course",
            "address": "5201 Park Heights Avenue Baltimore, MD 21215"
			},
        "url": "https://www.usracing.com/preakness-stakes/betting"
    }
     {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "name": "Bet the 2021 Belmont Stakes",
        "startDate": "2021-06-05",
        "endDate": "2021-06-05",
        "sport": "Horse Racing",
        "location": {
            "@type": "Place",
            "name" : "Belmont Park",
            "address": "2150 Hempstead Turnpike, Elmont, NY 11003"
			},
        "url": "https://www.usracing.com/belmont-stakes/betting"
    }  
</script>
{/literal}