<table  id="horse-betting" class="horse-betting" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td><strong>Kentucky Derby Day Race Schedule</strong></td>
</tr>
<tr>
<td>
<table  id="horse-betting" class="horse-betting" title="Kentucky Derby Race Schedule" summary="A schedule for betting on the Kentucky Derby" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td colspan="8">
<hr size="1">
</td>
</tr>
<tr>
<td>Grade II Churchill Downs</td>
<td>$250,000-added, seven furlongs, 4-year-olds and up</td>
</tr>
<tr>
<td>Grade III La Troienne</td>
<td>$150,000-added, seven furlongs, 3-year-old fillies</td>
</tr>
<tr>
<td>Grade III Distaff Turf Mile</td>
<td>$150,000-added, one mile, fillies and mares, 3-year-olds and up</td>
</tr>
<tr>
<td>Grade I Distaff</td>
<td>$300,000 added, seven furlongs, fillies and mares, 4-year-olds and up</td>
</tr>
<tr>
<td>Grade I Turf Classic</td>
<td>$500,000-added, 1 1/8 mile, 3-year-olds and up</td>
</tr>
<tr>
<td>Grade I Kentucky Derby</td>
<td>$2,000,000-guaranteed, 1 1/4 mile, 3-year-olds</td>
</tr>
<tr>
<td colspan="8">
<hr size="1">
</td>
</tr>
</tbody>
</table>
<div>*$40 general admission</div>
</td>
</tr>
</tbody>
</table>