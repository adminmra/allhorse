

<?php //KD CONTROL SWITCH
$SwitchVariable['variableName'][]= 'KD_Switch_Early';         //One month out
$SwitchVariable['startTIME'][]= '2021-04-06 00:00';         // 1 hero image, primary menu, pages metas
$SwitchVariable['endTIME'][]= '2021-04-19 10:59';           // TURN *OFF*   at KD_Switch_Main

$SwitchVariable['variableName'][]= 'KD_Switch_Main';        // Two Weeks out.
$SwitchVariable['startTIME'][]= '2021-04-19 11:00'; 	// full kd hero, primary menu, metas, index, kd & ko odds copy
$SwitchVariable['endTIME'][]= '2021-04-29 23:59';	//Ends end of KO Day

$SwitchVariable['variableName'][]= 'KD_Switch_Race_Day';        //  Post Oaks until end of KD Day
$SwitchVariable['startTIME'][]= '2021-04-30 00:00'; 	// KD Switch Full site + day of meta changes 
$SwitchVariable['endTIME'][]= '2021-05-01 19:00';

$SwitchVariable['variableName'][]= 'KD_Switch_Full_Site';        //********** Runs Concurrently! ***********  One Week out.
$SwitchVariable['startTIME'][]= '2021-04-24 8:00'; 	// KD Switch Main + states & Misc page copy
$SwitchVariable['endTIME'][]= '2021-05-01 18:59';

$SwitchVariable['variableName'][]= 'KO_Switch_KO_Race_Day';   //********** Runs Concurrently! ************* Day of KO
$SwitchVariable['startTIME'][]= '2021-04-29 8:00';         //Activate oaks Slider Only
$SwitchVariable['endTIME'][]= '2021-04-29 17:51';           // TURN *OFF*   AT END OF OAKS  

$SwitchVariable['variableName'][]= 'KD_Results';        // Day of Kentucky Derby to End of Sunday
$SwitchVariable['startTIME'][]= '2021-04-01 18:50';         // starts at end of the race
$SwitchVariable['endTIME'][]= '2021-04-03 23:59';           // 

/******* Clubhouse Section Switches***********/

$SwitchVariable['variableName'][]= 'KD_Clubhouse_Stage_2_Switch';
$SwitchVariable['startTIME'][]='2020-08-30 23:59'; 
$SwitchVariable['endTIME'][]='2020-09-05 19:02';

$SwitchVariable['variableName'][]= 'KD_Clubhouse_Race_End';
$SwitchVariable['startTIME'][]='2020-09-05 19:02'; 
$SwitchVariable['endTIME'][]='2020-09-05 23:59';

$SwitchVariable['variableName'][]= 'KD_Clubhouse_Stage_3_Switch';
$SwitchVariable['startTIME'][]='2020-09-05 23:59'; 
$SwitchVariable['endTIME'][]='2020-09-09 23:59';

/*$SwitchVariable['variableName'][]= 'KD_KOAKS_Switch'; 	//Removes KO Betting content day before derby
$SwitchVariable['startTIME'][]='2019-04-01 00:00'; 		//April 1st every year.
$SwitchVariable['endTIME'][]='2019-05-03 18:15'; 		// TURN *OFF*   AT END OF OAKS ********************************** OAKS

$SwitchVariable['variableName'][]= 'KD_Switch';  		//Day after DWC to end of KD Race.
$SwitchVariable['startTIME'][]='2019-04-01 00:00'; 		//April 1st every year.
$SwitchVariable['endTIME'][]='2019-05-04 18:55';  		// TURN *OFF*   AT END OF DERBY ********************************* DERBY

$SwitchVariable['variableName'][]= 'KD_PREP_Switch';  	// What is this for? - TF
$SwitchVariable['startTIME'][]='2019-04-01 00:00'; 
$SwitchVariable['endTIME'][]='2019-04-13 23:59';

$SwitchVariable['variableName'][]= 'KD_Switch_Phase_1'; // Day after DWC until 2 weeks out
$SwitchVariable['startTIME'][]='2019-04-01 00:00'; 
$SwitchVariable['endTIME'][]='2019-04-22 23:59';

$SwitchVariable['variableName'][]= 'KD_Switch_Phase_2'; // 2 Weeks out until 1 week out.
$SwitchVariable['startTIME'][]='2019-04-23 00:00'; 
$SwitchVariable['endTIME'][]='2019-04-29 23:59';

$SwitchVariable['variableName'][]= 'KD_Switch_Phase_2_EOR'; // 2 Weeks out until End Of DERBY
$SwitchVariable['startTIME'][]='2019-04-22 10:00'; 
$SwitchVariable['endTIME'][]='2019-05-04 18:55';		// TURN *OFF*   AT END OF DERBY ***************************** DERBY

$SwitchVariable['variableName'][]= 'KD_Switch_Phase_3'; // Week out until day of KO
$SwitchVariable['startTIME'][]='2019-04-30 00:00'; 
$SwitchVariable['endTIME'][]='2019-05-02 23:59';

$SwitchVariable['variableName'][]= 'KD_Switch_Phase_4'; // Kentucky Oaks Short Term Content (index)
$SwitchVariable['startTIME'][]='2019-05-03 00:00'; 		// Starts day of Oaks
$SwitchVariable['endTIME'][]='2019-05-03 18:15'; 		// TURN *OFF*   AT END OF OAKS RACE ***************************** OAKS

$SwitchVariable['variableName'][]= 'KD_Switch_Phase_5'; // End of Kentucky Oaks to End of Derby
$SwitchVariable['startTIME'][]='2019-05-03 18:16'; 		// TURN *ON*    AT END OF OAKS RACE *****************************  OAKS
$SwitchVariable['endTIME'][]='2019-05-04 18:55';		// TURN *OFF*   AT END OF DERBY RACE

$SwitchVariable['variableName'][]= 'KD_Switch_Results'; // Day of Kentucky Derby to End of Belmont
$SwitchVariable['startTIME'][]='2019-05-04 00:00'; 		// Starts Day of Derby 
$SwitchVariable['endTIME'][]='2019-06-15 23:59';		// End 1 week after Belmont

$SwitchVariable['variableName'][]= 'KD_Switch_Old_Odds'; // Day of Kentucky Derby for 5 days
$SwitchVariable['startTIME'][]='2019-05-04 00:00'; 		// TURN *ON*   AT END OF DERBY ***************************** DERBY 
$SwitchVariable['endTIME'][]='2019-05-08 23:59';		// End 5 days after KD


$SwitchVariable['variableName'][]= 'KD_preps_Switch';
$SwitchVariable['startTIME'][]='2019-03-28 12:00'; 		
$SwitchVariable['endTIME'][]='2019-04-11 12:00';

$SwitchVariable['variableName'][]= 'KD_Switch_new';
$SwitchVariable['startTIME'][]='2019-04-11 12:00'; 		
$SwitchVariable['endTIME'][]='2019-05-01 23:59';*/

?>