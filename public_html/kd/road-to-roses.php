        <div class='tableSchema table-responsive'>
        <table id='infoEntries' width='100%' border='0' cellspacing='0' cellpadding='0' class='table table-condensed table-striped table-bordered' title='Old Kentucky Derby Prep Races'>
        <tbody>
        <tr>
            <th>Date</th> <th>Race</th> <!--<th>Race Distance</th>--> <th>Track</th> <th>Winner</th> <th>Points</th>
        </tr>

                <tr>
            <td class="dateUpdated center" colspan="5">
                <em id='updateemp'>Updated August 20, 2019 12:26:44.</em>
                BUSR - Official <a href="//www.usracing.com/kentucky-derby/prep-races">Kentucky Derby Preps</a>. <br />
                All Kentucky Derby Preps Races.
            </td>
        </tr>
        </tbody>
        </table>
        </div>
        