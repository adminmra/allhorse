<div>
<i class="fa fa-calendar"></i>
<p><strong>When:</strong></p>
<p>The <?php include('/home/ah/allhorse/public_html/kd/running.php'); 	 ?> running of the Kentucky Derby !</p>
</div>

<div>
<i class="fa fa-map-marker"></i>
<p><strong>Where:</strong></p>
<p>Churchill Downs Racecourse, Louisville, Kentucky</p>
</div>

<div>
<i class="glyphicon glyphicon-eye-open"></i>
<p><strong>Watch:</strong></p>
<p>Watch the Kentucky Derby live on TV with NBC at 5:00 p.m. Eastern Time</p>
</div>

<div class="note">
<p>The Kentucky Derby is a stakes race for three-year-old thoroughbred horses, staged yearly in Louisville, Kentucky.</p>
</div>