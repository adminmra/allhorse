	<div class="table-responsive">
		<table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Margin of Victory"  summary="The latest odds for the Kentucky Derby available for wagering now at BUSR.">
			<caption>2018 Preakness Matchups</caption>
			<tbody>
				<tr>
					<th colspan="3" class="center">
					2018 Preakness Matchups  - May 19					</th>
			</tr>
				<tr>
					<th colspan="3" class="center">
					2018 Preakness Stakes<br />@ Pimlico Race Track - Baltimore, Md					</th>
			</tr>
	<tr class='head_title'><th>Team</th><th>ML</th></tr><tr><td style="background:white">Quip</td><td style="background:white">-110</td></tr><tr><td style="background:white">Lone Sailor</td><td style="background:white">-120</td></tr><tr><td style="background:#ddd">Quip</td><td style="background:#ddd">-130</td></tr><tr><td style="background:#ddd">Tenfold</td><td style="background:#ddd">EV</td></tr><tr><td style="background:white">Sporting Chance</td><td style="background:white">-180</td></tr><tr><td style="background:white">Diamond King</td><td style="background:white">+150</td></tr><tr><td style="background:#ddd">Sporting Chance</td><td style="background:#ddd">+119</td></tr><tr><td style="background:#ddd">Tenfold</td><td style="background:#ddd">-149</td></tr><tr><td style="background:white">Diamond King</td><td style="background:white">+150</td></tr><tr><td style="background:white">Tenfold</td><td style="background:white">-180</td></tr><tr><td style="background:#ddd">Diamond King</td><td style="background:#ddd">+150</td></tr><tr><td style="background:#ddd">Bravazo</td><td style="background:#ddd">-180</td></tr><tr><td style="background:white">Good Magic</td><td style="background:white">+216</td></tr><tr><td style="background:white">Justify</td><td style="background:white">-276</td></tr><tr><td style="background:#ddd">Tenfold</td><td style="background:#ddd">+116</td></tr><tr><td style="background:#ddd">Bravazo</td><td style="background:#ddd">-146</td></tr><tr><td style="background:white">Quip</td><td style="background:white">-145</td></tr><tr><td style="background:white">Diamond King</td><td style="background:white">+115</td></tr>					<tr>
						<td class="dateUpdated center" colspan="5">
							<em id='updateemp'>  - Updated June 29, 2018 07:07:34 </em>
							BUSR - Official <a href="https://www.usracing.com/kentucky-derby/odds">Kentucky Derby Odds</a>. 

<!-- br />
							All odds are fixed odds prices. -->
						</td>
					</tr>
			</tbody>
		</table>
	</div>
	