{literal} <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "ItemList",
      "itemListElement": [  {
										  "@type": "ListItem",
										  "position": 1,
										  "item": {
											"name": "A Thread Of Blue - Odds (+9000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 2,
										  "item": {
											"name": "Achilles Warrior - Odds (+21500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 3,
										  "item": {
											"name": "Admiral Rous - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 4,
										  "item": {
											"name": "Admire - Odds (+17500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 5,
										  "item": {
											"name": "Alwaysmining - Odds (+2200)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 6,
										  "item": {
											"name": "American Camp - Odds (+30000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 7,
										  "item": {
											"name": "Americandy - Odds (+29000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 8,
										  "item": {
											"name": "And Seek - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 9,
										  "item": {
											"name": "Angelo's Pride - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 10,
										  "item": {
											"name": "Anothertwistafate - Odds (+3200)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 11,
										  "item": {
											"name": "Aquadini - Odds (+22000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 12,
										  "item": {
											"name": "Avie's Flatter - Odds (+6000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 13,
										  "item": {
											"name": "Award Winner - Odds (+18000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 14,
										  "item": {
											"name": "B P Rocket - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 15,
										  "item": {
											"name": "Bankit - Odds (+16000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 16,
										  "item": {
											"name": "Bellafina - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 17,
										  "item": {
											"name": "Big Scott Daddy - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 18,
										  "item": {
											"name": "Blenheim Palace - Odds (+27500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 19,
										  "item": {
											"name": "Bodexpress - Odds (+2800)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 20,
										  "item": {
											"name": "Boldor - Odds (+10000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 21,
										  "item": {
											"name": "Bourbon War - Odds (+2500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 22,
										  "item": {
											"name": "By My Standards - Odds (+2200)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 23,
										  "item": {
											"name": "Camgo - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 24,
										  "item": {
											"name": "Castle Casanova - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 25,
										  "item": {
											"name": "Cave Run - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 26,
										  "item": {
											"name": "Chase The Ghost - Odds (+21000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 27,
										  "item": {
											"name": "Chess Chief - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 28,
										  "item": {
											"name": "Classy John - Odds (+12500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 29,
										  "item": {
											"name": "Code Of Honor - Odds (+1400)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 30,
										  "item": {
											"name": "Come On Gerry - Odds (+11500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 31,
										  "item": {
											"name": "Comedian - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 32,
										  "item": {
											"name": "Copper King - Odds (+27500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 33,
										  "item": {
											"name": "Corruze - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 34,
										  "item": {
											"name": "Counter Offer - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 35,
										  "item": {
											"name": "Country House - Odds (+4500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 36,
										  "item": {
											"name": "Country Singer - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 37,
										  "item": {
											"name": "Curlaway - Odds (+30000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 38,
										  "item": {
											"name": "Cutting Humor - Odds (+2800)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 39,
										  "item": {
											"name": "Dabo - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 40,
										  "item": {
											"name": "Derma Louvre - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 41,
										  "item": {
											"name": "Dessman - Odds (+11000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 42,
										  "item": {
											"name": "Dorrance - Odds (+17500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 43,
										  "item": {
											"name": "Doups Point - Odds (+30000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 44,
										  "item": {
											"name": "Dream Maker - Odds (+7500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 45,
										  "item": {
											"name": "Dull Knife - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 46,
										  "item": {
											"name": "Dunph - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 47,
										  "item": {
											"name": "Easy Shot - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 48,
										  "item": {
											"name": "Everfast - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 49,
										  "item": {
											"name": "Extra Hope - Odds (+7000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 50,
										  "item": {
											"name": "Eye Cloud - Odds (+32500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 51,
										  "item": {
											"name": "Family Biz - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 52,
										  "item": {
											"name": "Fayette Warrior - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 53,
										  "item": {
											"name": "Federal Case - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 54,
										  "item": {
											"name": "Final Jeopardy - Odds (+10000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 55,
										  "item": {
											"name": "Five Star General - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 56,
										  "item": {
											"name": "Forloveofcountry - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 57,
										  "item": {
											"name": "Forty Under - Odds (+11000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 58,
										  "item": {
											"name": "Frank'sgunisloaded - Odds (+23000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 59,
										  "item": {
											"name": "Frolic More - Odds (+30000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 60,
										  "item": {
											"name": "Frosted Ice - Odds (+10000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 61,
										  "item": {
											"name": "Fullness Of Time - Odds (+17500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 62,
										  "item": {
											"name": "Galilean - Odds (+5000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 63,
										  "item": {
											"name": "Game Winner - Odds (+300)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 64,
										  "item": {
											"name": "Go Away - Odds (+17500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 65,
										  "item": {
											"name": "Gray Attempt - Odds (+3500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 66,
										  "item": {
											"name": "Gray Magician - Odds (+4000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 67,
										  "item": {
											"name": "Green Fleet - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 68,
										  "item": {
											"name": "Growth Engine - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 69,
										  "item": {
											"name": "Gum Tree Lane - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 70,
										  "item": {
											"name": "Gun It - Odds (+16000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 71,
										  "item": {
											"name": "Hackberry - Odds (+30000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 72,
										  "item": {
											"name": "Haikal - Odds (+2200)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 73,
										  "item": {
											"name": "Harvey Wallbanger - Odds (+10000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 74,
										  "item": {
											"name": "He's Smokin Hot - Odds (+19000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 75,
										  "item": {
											"name": "Henley's Joy - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 76,
										  "item": {
											"name": "Higgins - Odds (+29000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 77,
										  "item": {
											"name": "Hog Creek Hustle - Odds (+10000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 78,
										  "item": {
											"name": "Honest Mischief - Odds (+12500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 79,
										  "item": {
											"name": "Hustle Up - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 80,
										  "item": {
											"name": "Identifier - Odds (+17500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 81,
										  "item": {
											"name": "Improbable - Odds (+800)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 82,
										  "item": {
											"name": "Incorrigible - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 83,
										  "item": {
											"name": "Inventing Blame - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 84,
										  "item": {
											"name": "Instagrand - Odds (+3000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 85,
										  "item": {
											"name": "Irish Heatwave - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 86,
										  "item": {
											"name": "Jerome Avenue - Odds (+27500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 87,
										  "item": {
											"name": "Jersey Agenda - Odds (+14000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 88,
										  "item": {
											"name": "Kadens Courage - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 89,
										  "item": {
											"name": "Kaziranga - Odds (+26000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 90,
										  "item": {
											"name": "King For A Day - Odds (+14000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 91,
										  "item": {
											"name": "King Ford - Odds (+13000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 92,
										  "item": {
											"name": "King Of Speed - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 93,
										  "item": {
											"name": "Kingly - Odds (+11000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 94,
										  "item": {
											"name": "Knight's Cross - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 95,
										  "item": {
											"name": "Last Judgment - Odds (+10000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 96,
										  "item": {
											"name": "Laughing Fox - Odds (+12500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 97,
										  "item": {
											"name": "Lemniscate - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 98,
										  "item": {
											"name": "Lieutenant Dan - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 99,
										  "item": {
											"name": "Limonite - Odds (+12500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 100,
										  "item": {
											"name": "Long Range Toddy - Odds (+1600)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 101,
										  "item": {
											"name": "Lord Dragon - Odds (+24000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 102,
										  "item": {
											"name": "Lucky Lee - Odds (+17500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 103,
										  "item": {
											"name": "Lutsky - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 104,
										  "item": {
											"name": "Magnificent Mccool - Odds (+21000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 105,
										  "item": {
											"name": "Malibu In Motion - Odds (+27500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 106,
										  "item": {
											"name": "Malpais - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 107,
										  "item": {
											"name": "Manguzi - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 108,
										  "item": {
											"name": "Manny Wah - Odds (+10000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 109,
										  "item": {
											"name": "Market King - Odds (+12500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 110,
										  "item": {
											"name": "Masaff - Odds (+26500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 111,
										  "item": {
											"name": "Master Fencer - Odds (+12500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 112,
										  "item": {
											"name": "Maximum Security - Odds (+1000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 113,
										  "item": {
											"name": "Mayor Cobb - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 114,
										  "item": {
											"name": "Moonster - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 115,
										  "item": {
											"name": "More Ice - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 116,
										  "item": {
											"name": "Motagally - Odds (+14000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 117,
										  "item": {
											"name": "Mr. Ankeny - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 118,
										  "item": {
											"name": "Mr. Money - Odds (+9000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 119,
										  "item": {
											"name": "Much Better - Odds (+10000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 120,
										  "item": {
											"name": "Mucho Gusto - Odds (+4000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 121,
										  "item": {
											"name": "My Mandate - Odds (+19000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 122,
										  "item": {
											"name": "Ninth Street - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 123,
										  "item": {
											"name": "Nitrous - Odds (+12000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 124,
										  "item": {
											"name": "Nolo Contesto - Odds (+8000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 125,
										  "item": {
											"name": "Not That Brady - Odds (+17500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 126,
										  "item": {
											"name": "Nova Lenda - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 127,
										  "item": {
											"name": "Olympic Runner - Odds (+14000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 128,
										  "item": {
											"name": "Omaha Beach - Odds (+1500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 129,
										  "item": {
											"name": "Oncewewerebrothers - Odds (+16500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 130,
										  "item": {
											"name": "One Bad Boy - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 131,
										  "item": {
											"name": "Our Braintrust - Odds (+8000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 132,
										  "item": {
											"name": "Outshine - Odds (+4000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 133,
										  "item": {
											"name": "Over Protective - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 134,
										  "item": {
											"name": "Overdeliver - Odds (+10000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 135,
										  "item": {
											"name": "Owendale - Odds (+10000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 136,
										  "item": {
											"name": "Passion Play - Odds (+27500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 137,
										  "item": {
											"name": "Plus Que Parfait - Odds (+2500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 138,
										  "item": {
											"name": "Pole Setter - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 139,
										  "item": {
											"name": "Puttheglassdown - Odds (+19000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 140,
										  "item": {
											"name": "Roadster - Odds (+3500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 141,
										  "item": {
											"name": "Roiland - Odds (+9000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 142,
										  "item": {
											"name": "Romantico - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 143,
										  "item": {
											"name": "Rowayton - Odds (+6500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 144,
										  "item": {
											"name": "Royal Marine - Odds (+12500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 145,
										  "item": {
											"name": "Royal Meeting - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 146,
										  "item": {
											"name": "Savagery - Odds (+12500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 147,
										  "item": {
											"name": "Seismic Wave - Odds (+22000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 148,
										  "item": {
											"name": "Shazier - Odds (+29000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 149,
										  "item": {
											"name": "Shining Through - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 150,
										  "item": {
											"name": "Shir Khan - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 151,
										  "item": {
											"name": "Shootin The Breeze - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 152,
										  "item": {
											"name": "Signalman - Odds (+4000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 153,
										  "item": {
											"name": "Sir Winston - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 154,
										  "item": {
											"name": "Six Shooter - Odds (+11000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 155,
										  "item": {
											"name": "Skywire - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 156,
										  "item": {
											"name": "So Alive - Odds (+14000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 157,
										  "item": {
											"name": "Soldado - Odds (+12500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 158,
										  "item": {
											"name": "Somelikeithotbrown - Odds (+6500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 159,
										  "item": {
											"name": "Spanish Mission - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 160,
										  "item": {
											"name": "Spectacular Gem - Odds (+26000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 161,
										  "item": {
											"name": "Spinoff - Odds (+2800)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 162,
										  "item": {
											"name": "Standard Deviation - Odds (+10000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 163,
										  "item": {
											"name": "Still Dreaming - Odds (+24000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 164,
										  "item": {
											"name": "Stilts - Odds (+21500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 165,
										  "item": {
											"name": "Sueno - Odds (+4000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 166,
										  "item": {
											"name": "Synthesis - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 167,
										  "item": {
											"name": "Tacitus - Odds (+3000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 168,
										  "item": {
											"name": "Tap The Wire - Odds (+17500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 169,
										  "item": {
											"name": "Tapit Wise - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 170,
										  "item": {
											"name": "Tax - Odds (+2000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 171,
										  "item": {
											"name": "The Irish Rover - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 172,
										  "item": {
											"name": "The Right Path - Odds (+17500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 173,
										  "item": {
											"name": "Thirsty Betrayal - Odds (+26500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 174,
										  "item": {
											"name": "Thomas Shelby - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 175,
										  "item": {
											"name": "Tikhvin Flew - Odds (+10000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 176,
										  "item": {
											"name": "Tone Broke - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 177,
										  "item": {
											"name": "Town Bee - Odds (+35000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 178,
										  "item": {
											"name": "Trifor Gold - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 179,
										  "item": {
											"name": "U S Navy Cross - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 180,
										  "item": {
											"name": "Van Beethoven - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 181,
										  "item": {
											"name": "Vekoma - Odds (+3000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 182,
										  "item": {
											"name": "Walker Stalker - Odds (+30000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 183,
										  "item": {
											"name": "War Of Will - Odds (+1800)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 184,
										  "item": {
											"name": "Well Defined - Odds (+12500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 185,
										  "item": {
											"name": "Wendell Fong - Odds (+16500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 186,
										  "item": {
											"name": "Who's In Charge - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 187,
										  "item": {
											"name": "Wicked Indeed - Odds (+12500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 188,
										  "item": {
											"name": "Win Win Win - Odds (+2500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 189,
										  "item": {
											"name": "Young Phillip - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 190,
										  "item": {
											"name": "Zenden - Odds (+8000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 191,
										  "item": {
											"name": "Zeruch - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 192,
										  "item": {
											"name": "Western Australia - Odds (+17500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 193,
										  "item": {
											"name": "Cabot - Odds (+22500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 194,
										  "item": {
											"name": "Cowboy Diplomacy - Odds (+27500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 195,
										  "item": {
											"name": "Get The Prize - Odds (+12500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 196,
										  "item": {
											"name": "Oval Ace - Odds (+14000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 197,
										  "item": {
											"name": "Weitblick - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 198,
										  "item": {
											"name": "Bandon Wood - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 199,
										  "item": {
											"name": "Grumps Little Tots - Odds (+24000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 200,
										  "item": {
											"name": "Into Morocco - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 201,
										  "item": {
											"name": "Pyron - Odds (+19000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 202,
										  "item": {
											"name": "Red Gum - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 203,
										  "item": {
											"name": "Play Money - Odds (+35000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 204,
										  "item": {
											"name": "Captain Von Trapp - Odds (+19000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 205,
										  "item": {
											"name": "Hoffa`s Union - Odds (+9000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 206,
										  "item": {
											"name": "Pioneer Dancer - Odds (+30000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 207,
										  "item": {
											"name": "Dynamic Racer - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 208,
										  "item": {
											"name": "Erlich - Odds (+17500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 209,
										  "item": {
											"name": "Jahbath - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 210,
										  "item": {
											"name": "Litany - Odds (+27500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 211,
										  "item": {
											"name": "Playa Del Puente - Odds (+15000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 212,
										  "item": {
											"name": "Promo Code - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 213,
										  "item": {
											"name": "Sly - Odds (+27500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 214,
										  "item": {
											"name": "Durkin's Call - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 215,
										  "item": {
											"name": "Joevia - Odds (+20000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 216,
										  "item": {
											"name": "Presto - Odds (+30000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 217,
										  "item": {
											"name": "Proverb - Odds (+25000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 218,
										  "item": {
											"name": "Sayyaaf - Odds (+27500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 219,
										  "item": {
											"name": "Warrior's Charge - Odds (+27500)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 220,
										  "item": {
											"name": "Spun To Run - Odds (+16000)",
											"url": "https://www.usracing.com/"
										  }
										}, {
										  "@type": "ListItem",
										  "position": 221,
										  "item": {
											"name": "De Flug - Odds (+12500)",
											"url": "https://www.usracing.com/"
										  }
										}  ],
      "name": "2019 Kentucky Derby Odds",
      "url": "https://www.usracing.com/"
    }
    </script> {/literal}