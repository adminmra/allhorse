<!--fix this to the new table-->
<h2>2021 Kentucky Derby Results</h2>
<!--fix this to the new table-->
<div id="no-more-tables">
  <table id="sortTable" border="0" cellpadding="0" cellspacing="0"
    class="data table table-bordered table-striped table-condensed ordenableResult">
    <thead>
      <tr>
        <th>Result</th>
        <th>Time</th>
        <th>Post</th>
        <th>Horse</th>
        <th>Jockey</th>
        <th>Trainer</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Result">1</td>
        <td data-title="Time">2:01.01</td>
        <td data-title="PP">8</td>
        <td data-title="Winner">Medina Spirit</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Bob Baffert</td>
      </tr>
      <tr>
        <td data-title="Result">2</td>
        <td data-title="Time">&nbsp;</td>
        <td data-title="PP">7</td>
        <td data-title="Horse">Mandaloun</td>
        <td data-title="Jockey">Florent Geroux</td>
        <td data-title="Trainer">Brad Cox</td>
      </tr>
      <tr>
        <td data-title="Result">3</td>
        <td data-title="Time">&nbsp;</td>
        <td data-title="PP">9</td>
        <td data-title="Horse">Hot Rod Charlie</td>
        <td data-title="Jockey">Flavien Prat</td>
        <td data-title="Trainer">Doug O'Neill</td>
      </tr>
    </tbody>
  </table>
</div>

<h2>2021 Kentucky Derby Payouts</h2>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="data table table-condensed table-striped table-bordered"
    title="Preakness Stakes" summary="Preakness Stakes Payouts. ">
    <tbody>
      <tr>
        <th>PP</th>
        <th>Horses</th>
        <th>Win</th>
        <th>Place</th>
        <th>Show</th>
      </tr>
      <tr>
        <td>8</td>
        <td>Medina Spirit</td>
        <td>$26.20</td>
        <td>$12.00</td>
        <td>$7.60</td>
      </tr>
      <tr>
        <td>7</td>
        <td>Mandaloun</td>
        <td>&nbsp;</td>
        <td>$23.00</td>
        <td>$13.40</td>
      </tr>
      <tr>
        <td>9</td>
        <td>Hot Rod Charlie	</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>$5.20</td>
      </tr>
    </tbody>
  </table>
</div>
<p></p>


<div id="no-more-tables">
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Kentucky Derby Payouts" summary="Kentucky Derby Payouts. ">
    <thead>
      <tr>
        <th>Wager</th>
        <th>Horses</th>
        <th>Denomination</th>
        <th>Payout</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Wager">Exacta</td>
        <td data-title="Horses">8-7</td>
        <td data-title="Denomination">$2.00</td>
        <td data-title="Payout">$503.60</td>
      </tr>
      <tr>
        <td data-title="Wager">Trifecta</td>
        <td data-title="Horses">8-7-9</td>
        <td data-title="Denomination">$1.00</td>
        <td data-title="Payout">$1,696.90</td>
      </tr>
      <tr>
        <td data-title="Wager">Superfecta</td>
        <td data-title="Horses">8-7-9-14</td>
        <td data-title="Denomination">$1.00</td>
        <td data-title="Payout">$9,456.40</td>
      </tr>
      <tr>
        <td data-title="Wager">Double</td>
        <td data-title="Horses">4-18</td>
        <td data-title="Denomination">$1.00</td>
        <td data-title="Payout">$127.00</td>
      </tr>
      <tr>
        <td data-title="Wager">Double</td>
        <td data-title="Horses">4-10</td>
        <td data-title="Denomination">$1.00</td>
        <td data-title="Payout">$8.80</td>
      </tr>
      <tr>
        <td data-title="Wager">Pick 3</td>
        <td data-title="Horses">4-4-18</td>
        <td data-title="Denomination">$0.50</td>
        <td data-title="Payout">$272.85</td>
      </tr>
    </tbody>
  </table>
</div>


<!--
<p>
<h2>Kentucky Derby 2014 Payouts</h2>
          <div data-title="table-responsive"><table  data-title="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0" summary="Kentucky Derby Payouts">
            <thead>
              <tr>
                <th colspan="2">Results</th>
                <th>Win</th>
                <th>Place</th>
                <th>Show</th>
              </tr>
            </thead>
            <tbody>
              <tr >
                <td>5</td>
                <td >California Chrome</td>
                <td>$7.00</td>
                <td >$5.60</td>
                <td >$4.20</td>
              </tr>
              <tr>
                <td>17</td>
                <td >Commanding Curve</td>
                <td></td>
                <td>$31.80</td>
                <td >$15.40</td>
              </tr>
              <tr>
                <td>4</td>
                <td >Danza</td>
                <td></td>
                <td></td>
                <td>$6.00</td>
              </tr>
            </tbody>
          </table>
        </div><p>
    <h2>Kentucky Derby 2014 Exotic Payouts</h2>
         <div data-title="table-responsive"><table  data-title="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0" summary="Kentucky Derby Exotic Payouts">
            <tbody>
             <tr>
                <td >$2.00</td>
                <td >Exacta</td>
                <td>
                  $340.00
                  
                </td>
              </tr><tr>
                <td >$0.50</td>
                <td >Trifecta</td>
                <td>
                  $856.15
                  
                </td>
              </tr><tr>
                <td >$1.00</td>
                <td >Double</td>
                <td>
                  $4.50
                  
                </td>
              </tr><tr>
                <td >$0.50</td>
                <td >Pick Four</td>
                <td>
                  $394.45
                  
                </td>
              </tr><tr>
                <td >$1.00</td>
                <td >Superfecta</td>
                <td>
                  $7,691.90
                  
                </td>
              </tr><tr>
                <td >$2.00</td>
                <td >Pick Six</td>
                <td>
                  $189.60
                  
                </td>
              </tr><tr>
                <td >$2.00</td>
                <td >Pick Six</td>
                <td>
                  $22,296.40
                  
                </td>
              </tr><tr>
                <td >$0.50</td>
                <td >Pick Three</td>
                <td>
                  $45.15
                  
                </td>
              </tr><tr>
                <td >$0.50</td>
                <td >Pick Five</td>
                <td>
                  $694.95
                  
                </td>
              </tr> <tr>
                <td >$1.00</td>
                <td >Pent/Str Fl</td>
                <td>
                  $149,764.70
                  
                </td>
              </tr>
            </tbody>
          </table>
        </div>
                              
                              
                              
                              
                              
-->
{literal}

<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_results.js"></script>
{/literal}