<div id="no-more-tables">
<table id="fourth" border="0" cellspacing="0" cellpadding="0" class="data table table-condensed table-striped table-bordered ordenable">
  <thead>
  <tr>
    <th>Year</th>
    <th>Winner</th>
    <th>Pool 1</th>
    <th>Pool2</th>
    <th>Pool 3</th>
    <th>Payout</th>
  </tr>
  </thead>
<tbody>
  <tr>
    <td data-title="Year">2012</td>
    <td data-title="Winner">Believe You Can</td>
    <td data-title="Pool 1">$74.80</td>
    <td data-title="Pool 2">No Pool</td>
    <td data-title="Pool 3">No Pool</td>
    <td data-title="Payout">$29.60</td>
  </tr>
  <tr>
    <td data-title="Year">2011</td>
    <td data-title="Winner">Plum Pretty</td>
    <td data-title="Pool 1">$21.80 (f)</td>
    <td data-title="Pool 2">No Pool</td>
    <td data-title="Pool 3">No Pool</td>
    <td data-title="Payout">-</td>
  </tr>
  <tr>
    <td data-title="Year">2010</td>
    <td data-title="Winner">Blind Luck</td>
    <td data-title="Pool 1">$10.00</td>
    <td data-title="Pool 2">No Pool</td>
    <td data-title="Pool 3">No Pool</td>
    <td data-title="Payout">$4.60</td>
  </tr>
  <tr>
    <td data-title="Year">2009</td>
    <td data-title="Winner">Rachel Alexandra</td>
    <td data-title="Pool 1">$8.40</td>
    <td data-title="Pool 2">No Pool</td>
    <td data-title="Pool 3">No Pool</td>
    <td data-title="Payout">$2.60*</td>
  </tr>
  <tr>
    <td data-title="Year">2008</td>
    <td data-title="Winner">Proud Spell</td>
    <td data-title="Pool 1">$15.60</td>
    <td data-title="Pool 2">$14.20</td>
    <td data-title="Pool 3">$15.60</td>
    <td data-title="Payout">$8.80*</td>
  </tr>
  <tr>
    <td data-title="Year">2007</td>
    <td data-title="Winner">Rags to Riches</td>
    <td data-title="Pool 1">$9.60</td>
    <td data-title="Pool 2">$10.00*</td>
    <td data-title="Pool 3">$5.00*</td>
    <td data-title="Payout">$5.00*</td>
  </tr>
  <tr>
    <td data-title="Year">2006</td>
    <td data-title="Winner">Lemons Forever</td>
    <td data-title="Pool 1">$9.00 (f)*</td>
    <td data-title="Pool 2">$11.00 (f)*</td>
    <td data-title="Pool 3">$26.00 (f)</td>
    <td data-title="Payout">$96.20</td>
  </tr>
  <tr>
    <td data-title="Year">2005</td>
    <td data-title="Winner">Summerly</td>
    <td data-title="Pool 1">$31.80</td>
    <td data-title="Pool 2">$15.00</td>
    <td data-title="Pool 3">$28.40</td>
    <td data-title="Payout">$11.20</td>
  </tr>
  <tr>
    <td data-title="Year">2004</td>
    <td data-title="Winner">Ashado</td>
    <td data-title="Pool 1">$48.40***</td>
    <td data-title="Pool 2">$21.00</td>
    <td data-title="Pool 3">$17.80</td>
    <td data-title="Payout">$6.60*</td>
  </tr>
  <tr>
    <td data-title="Year">2003</td>
    <td data-title="Winner">Bird Town</td>
    <td data-title="Pool 1">$33.60</td>
    <td data-title="Pool 2">No Pool</td>
    <td data-title="Pool 3">No Pool</td>
    <td data-title="Payout">$38.40</td>
  </tr>
  </tbody>
</table>
</div>
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sorttable_wager.js"></script>
<script type="text/javascript">
addSort_Wager('first'); addSort_Wager('second');
addSort_Wager('third'); addSort_Wager('fourth');
</script>