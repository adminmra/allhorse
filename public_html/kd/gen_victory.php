	<div class="table-responsive">
		<table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Margin of Victory"  summary="">
			<caption>2017 Kentucky Derby - Margin Of Victory</caption>
			<tbody>
				<tr>
					<th colspan="3" class="center">2017 Kentucky Derby - Margin Of Victory</th>
				</tr>
								<tr>
					<td>1 Length To 2 3/4 Lengths</td>
					<td>+200</td>
				</tr>
								<tr>
					<td>3 Lengths To 5 3/4 Lengths</td>
					<td>+225</td>
				</tr>
								<tr>
					<td>1/2 Length To 3/4 Length</td>
					<td>+375</td>
				</tr>
								<tr>
					<td>6 Lengths To 7 3/4 Lengths</td>
					<td>+650</td>
				</tr>
								<tr>
					<td>8 Lengths To 10 3/4 Lengths</td>
					<td>+750</td>
				</tr>
								<tr>
					<td>Head</td>
					<td>+800</td>
				</tr>
								<tr>
					<td>Neck</td>
					<td>+800</td>
				</tr>
								<tr>
					<td>11 Lengths To 14 3/4 Lengths</td>
					<td>+1000</td>
				</tr>
								<tr>
					<td>Nose</td>
					<td>+1000</td>
				</tr>
								<tr>
					<td>15 Lengths Or More</td>
					<td>+1200</td>
				</tr>
								<tr>
					<td>Dead Heat</td>
					<td>+3000</td>
				</tr>
				 
			</tbody>
		</table>
	</div>
	