<h2>2021 Kentucky Oaks Results</h2>
<div id="no-more-tables">
  <table class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0"
    cellspacing="0" border="0" title="Kentucky Oaks Results" summary="  Kentucky Oaks Results">
    <thead>
      <tr>
        <th>Result</th>
        <th>Time</th>
        <th>PP</th>
        <th>Horse</th>
        <th>Jockey</th>
        <th>Trainer</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Result">1</td>
        <td data-title="Time">1:48:99</td>
        <td data-title="PP">10</td>
        <td data-title="Horse">Malathaat</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Todd Pletcher</td>
      </tr>
      <tr>
        <td data-title="Result">2</td>
        <td data-title="Time">&nbsp;</td>
        <td data-title="PP">12</td>
        <td data-title="Horse">Search Results</td>
        <td data-title="Jockey">Irad Ortiz</td>
        <td data-title="Trainer">Chad Brown</td>
      </tr>
      <tr>
        <td data-title="Result">3</td>
        <td data-title="Time">&nbsp;</td>
        <td data-title="PP">9</td>
        <td data-title="Horse">Will's Secret</td>
        <td data-title="Jockey">R. Santana, Jr.</td>
        <td data-title="Trainer">S. Asmussen</td>
      </tr>
    </tbody>
  </table>
</div>
<h2>2021 Kentucky Oaks Payouts</h2>
<div>
  <table border="0" cellspacing="0" cellpadding="0" class="data table table-condensed table-striped table-bordered"
    title="Kentucky Oaks" summary="Kentucky Oaks Payouts. ">
    <tbody>
      <tr>
        <th>PP</th>
        <th>Horses</th>
        <th>Win</th>
        <th>Place</th>
        <th>Show</th>
      </tr>
      <tr>
        <td>10</td>
        <td>Malathaat</td>
        <td>$7.00</td>
        <td>$4.60</td>
        <td>$3.40</td>
      </tr>
      <tr>
        <td>12</td>
        <td>Search Results</td>
        <td></td>
        <td>$6.80</td>
        <td>$5.60</td>
      </tr>
      <tr>
        <td>11</td>
        <td>Will's Secret</td>
        <td></td>
        <td></td>
        <td>$9.60</td>
      </tr>
    </tbody>
  </table>
</div>
<p></p>


<div id="no-more-tables">
  <table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
    title="Breeders' Classic Payouts" summary="Kentucky Oaks Payouts ">
    <thead>
      <tr>
        <th>Wager</th>
        <th>Horses</th>
        <th>Denomination</th>
        <th>Payout</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Wager">Exacta</td>
        <td data-title="Horses">10-12</td>
        <td data-title="Denomination">$1.00</td>
        <td data-title="Payout">$18.90</td>
      </tr>
      <tr>
        <td data-title="Wager">Trifecta</td>
        <td data-title="Horses">10-12-11</td>
        <td data-title="Denomination">$0.50</td>
        <td data-title="Payout">$232.25</td>
      </tr>
      <tr>
        <td data-title="Wager">Superfecta</td>
        <td data-title="Horses">10-12-11-3</td>
        <td data-title="Denomination">$1.00</td>
        <td data-title="Payout">$2,808.70</td>
      </tr>
      <tr>
        <td data-title="Wager">Double</td>
        <td data-title="Horses">8-10</td>
        <td data-title="Denomination">$1.00</td>
        <td data-title="Payout">$17.80</td>
      </tr>
      <tr>
        <td data-title="Wager">Double</td>
        <td data-title="Horses">4-10</td>
        <td data-title="Denomination">$1.00</td>
        <td data-title="Payout">$8.80</td>
      </tr>
      <tr>
        <td data-title="Wager">Pick 3</td>
        <td data-title="Horses">7-8-10</td>
        <td data-title="Denomination">$.50</td>
        <td data-title="Payout">$159.90</td>
      </tr>
    </tbody>
  </table>
</div>



                             
                              <!--  <tr   >								  <td width="5%" align="center" >4</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >14</td> 
							       <td width="14%"   >Shackleford</td>
                                  <td width="14%"  >Dale Romans</td>
                                   <td width="14%"  >Jesus Castanon</td> 
                                </tr>
                                <tr>								  <td width="5%" align="center" >5</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >11</td> 
							       <td width="14%"   >Master Of Hounds</td>
                                  <td width="14%"  >Aidan O'Brien</td>
                                   <td width="14%"  >Garrett Gomez</td> 
                                </tr>
                                <tr   >								  <td width="5%" align="center" >6</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >12</td> 
							       <td width="14%"   >Santiva</td>
                                  <td width="14%"  >Eddie Kenneally</td>
                                   <td width="14%"  >Shaun Bridgmohan</td> 
                                </tr>
                                <tr>								  <td width="5%" align="center" >7</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >2</td> 
							       <td width="14%"   >Brilliant Speed</td>
                                  <td width="14%"  >Tom Albertrani</td>
                                   <td width="14%"  >Joel Rosario</td> 
                                </tr>
                                <tr   >								  <td width="5%" align="center" >8</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >8</td> 
							       <td width="14%"   >Dialed In</td>
                                  <td width="14%"  >Nicholas P. Zito</td>
                                   <td width="14%"  >Julien Leparoux</td> 
                                </tr>
                                <tr>								  <td width="5%" align="center" >9</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >7</td> 
							       <td width="14%"   >Pants On Fire</td>
                                  <td width="14%"  >Kelly Breen</td>
                                   <td width="14%"  >Anna Napravnik</td> 
                                </tr>
                                <tr   >								  <td width="5%" align="center" >10</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >3</td> 
							       <td width="14%"   >Twice the Appeal</td>
                                  <td width="14%"  >Jeff Bonde</td>
                                   <td width="14%"  >Calvin Borel</td> 
                                </tr>
                                <tr>								  <td width="5%" align="center" >11</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >17</td> 
							       <td width="14%"   >Soldat</td>
                                  <td width="14%"  >Kiaran McLaughlin</td>
                                   <td width="14%"  >Alan Garcia</td> 
                                </tr>
                                <tr   >								  <td width="5%" align="center" >12</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >4</td> 
							       <td width="14%"   >Stay Thirsty</td>
                                  <td width="14%"  >Todd Pletcher</td>
                                   <td width="14%"  >Ramon Dominguez</td> 
                                </tr>
                                <tr>								  <td width="5%" align="center" >13</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >9</td> 
							       <td width="14%"   >Derby Kitten</td>
                                  <td width="14%"  >Mike Maker</td>
                                   <td width="14%"  >TBA</td> 
                                </tr>
                                <tr   >								  <td width="5%" align="center" >14</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >5</td> 
							       <td width="14%"   >Decisive Moment</td>
                                  <td width="14%"  >Juan Arias</td>
                                   <td width="14%"  >Kerwin Clark</td> 
                                </tr>
                                <tr>								  <td width="5%" align="center" >15</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >1</td> 
							       <td width="14%"   >Archarcharch</td>
                                  <td width="14%"  >William H Fires</td>
                                   <td width="14%"  >Jon Court</td> 
                                </tr>
                                <tr   >								  <td width="5%" align="center" >16</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >15</td> 
							       <td width="14%"   >Midnight Interlude</td>
                                  <td width="14%"  >Bob Baffert</td>
                                   <td width="14%"  >Victor Espinoza</td> 
                                </tr>
                                <tr>								  <td width="5%" align="center" >17</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >10</td> 
							       <td width="14%"   >Twinspired</td>
                                  <td width="14%"  >Mike Maker</td>
                                   <td width="14%"  >Mike Smith</td> 
                                </tr>
                                <tr   >								  <td width="5%" align="center" >18</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >20</td> 
							       <td width="14%"   >Watch Me Go</td>
                                  <td width="14%"  >Kathleen O' Connell</td>
                                   <td width="14%"  >Rafael Bejarano</td> 
                                </tr>
                                <tr>								  <td width="5%" align="center" >19</td>
								   <td width="5%"  align="center"></td>                          
                                <td width="5%"  >6</td> 
							       <td width="14%"   >Comma To The Top</td>
                                  <td width="14%"  >Peter Miller</td>
                                   <td width="14%"  >Patrick Valenzuela</td> 
                                </tr> -->
                                											                                
                              </table>
                              
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_results.js"></script>
{/literal}                          
                              
