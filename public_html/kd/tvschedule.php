<h2><?php	 	 ?> Kentucky Derby TV Schedule</h2><table id="horse-betting" class="horse-betting" border="0" cellpadding="0" cellspacing="0" title="Kentucky Derby Television Schedule">




<tbody><tr><td><strong>The <?php	 	 ?>  Kentucky Derby</strong></td><td><strong>Saturday May 4</strong></td><td><strong>4:00 - 7:00 PM ET<span>*</span></strong></td><td><strong>NBC</strong></td></tr><tr><td>Derby Classics</td><td>Wednesday May 1</td><td>4:00-5:00 PM ET*</td><td>NBCSN</td></tr><tr><td>Draw</td><td><span>Wednesday May 1</span></td><td>5:00-6:00 PM ET<span>*</span></td><td>NBCSN</td></tr><tr><td>Derby Classics</td><td>Thursday May 2</td><td>4:00-5:00 PM ET<span>*</span></td><td>NBCSN</td></tr><tr><td>Derby Access</td><td>Thursday May 2</td><td><span>5:00-6:00 PM ET<span>*</span></span></td><td>NBCSN</td></tr><tr><td>Derby Classics</td><td>Friday May 3</td><td><span>4:00-5:00 PM ET<span>*</span></span></td><td>NBCSN</td></tr><tr><td>Kentucky Oaks</td><td>Friday May 3</td><td><span>5:00-6:00 PM ET<span>*</span></span></td><td>NBCSN</td></tr><tr><td>Derby Prep</td><td><span>Saturday May 4</span></td><td>11:00-4:00 PM ET<span>*</span></td><td>NBCSN</td></tr><tr><td>Derby Post</td><td><span>Saturday May 4</span></td><td>7:00-7:30 PM ET<span>*</span></td><td>NBCSN</td></tr></tbody></table>

