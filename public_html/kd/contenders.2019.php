<h2>Kentucky Derby Horses</h2>
 <div id="no-more-tables">
    <table class="data table table-condensed table-striped table-bordered ordenableResult" width="100%" cellpadding="0" cellspacing="0" border="0" summary="Kentucky Derby Contenders">
        <thead>
            <tr>
            <th>Rank</th><th>Horse</th><th>Trainer</th><th>Points</th><th>Earnings</th>            </tr>
        </thead>
	<tbody>
		<tr>
			<td data-title="Rank">
				1.</td>
			<td data-title="Horse">
				Tacitus</td>
			<td data-title="Trainer">
				Bill Mott</td>
			<td data-title="Points">
				150</td>
			<td data-title="Earnings">
				$610,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				2.</td>
			<td data-title="Horse">
				Omaha Beach</td>
			<td data-title="Trainer">
				Richard Mandella</td>
			<td data-title="Points">
				137.5</td>
			<td data-title="Earnings">
				$1,050,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				3.</td>
			<td data-title="Horse">
				Vekoma</td>
			<td data-title="Trainer">
				George Weaver</td>
			<td data-title="Points">
				110</td>
			<td data-title="Earnings">
				$747,600</td>
		</tr>
		<tr>
			<td data-title="Rank">
				4.</td>
			<td data-title="Horse">
				Plus Que Parfait</td>
			<td data-title="Trainer">
				Brendan Walsh</td>
			<td data-title="Points">
				104</td>
			<td data-title="Earnings">
				$1,540,400</td>
		</tr>
		<tr>
			<td data-title="Rank">
				5.</td>
			<td data-title="Horse">
				Roadster</td>
			<td data-title="Trainer">
				Bob Baffert</td>
			<td data-title="Points">
				100</td>
			<td data-title="Earnings">
				$636,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				6.</td>
			<td data-title="Horse">
				By My Standards</td>
			<td data-title="Trainer">
				Bret Calhoun</td>
			<td data-title="Points">
				100</td>
			<td data-title="Earnings">
				$600,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				7.</td>
			<td data-title="Horse">
				Maximum Security</td>
			<td data-title="Trainer">
				Jason Servis</td>
			<td data-title="Points">
				100</td>
			<td data-title="Earnings">
				$582,800</td>
		</tr>
		<tr>
			<td data-title="Rank">
				8.</td>
			<td data-title="Horse">
				Game Winner</td>
			<td data-title="Trainer">
				Bob Baffert</td>
			<td data-title="Points">
				85</td>
			<td data-title="Earnings">
				$1,810,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				9.</td>
			<td data-title="Horse">
				Code of Honor</td>
			<td data-title="Trainer">
				Shug McGaughey</td>
			<td data-title="Points">
				74</td>
			<td data-title="Earnings">
				$432,070</td>
		</tr>
		<tr>
			<td data-title="Rank">
				10.</td>
			<td data-title="Horse">
				Haikal</td>
			<td data-title="Trainer">
				Kiaran McLaughlin</td>
			<td data-title="Points">
				70</td>
			<td data-title="Earnings">
				$322,500</td>
		</tr>
		<tr>
			<td data-title="Rank">
				11.</td>
			<td data-title="Horse">
				Improbable</td>
			<td data-title="Trainer">
				Bob Baffert</td>
			<td data-title="Points">
				65</td>
			<td data-title="Earnings">
				$589,520</td>
		</tr>
		<tr>
			<td data-title="Rank">
				12.</td>
			<td data-title="Horse">
				War of Will</td>
			<td data-title="Trainer">
				Mark Casse</td>
			<td data-title="Points">
				60</td>
			<td data-title="Earnings">
				$440,840</td>
		</tr>
		<tr>
			<td data-title="Rank">
				13.</td>
			<td data-title="Horse">
				Long Range Toddy</td>
			<td data-title="Trainer">
				Steve Asmussen</td>
			<td data-title="Points">
				53.5</td>
			<td data-title="Earnings">
				$830,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				14.</td>
			<td data-title="Horse">
				Tax</td>
			<td data-title="Trainer">
				Danny Gargan</td>
			<td data-title="Points">
				52</td>
			<td data-title="Earnings">
				$307,500</td>
		</tr>
		<tr>
			<td data-title="Rank">
				15.</td>
			<td data-title="Horse">
				Cutting Humor</td>
			<td data-title="Trainer">
				Todd Pletcher</td>
			<td data-title="Points">
				50</td>
			<td data-title="Earnings">
				$462,467</td>
		</tr>
		<tr>
			<td data-title="Rank">
				16.</td>
			<td data-title="Horse">
				Win Win Win</td>
			<td data-title="Trainer">
				Mike Trombetta</td>
			<td data-title="Points">
				50</td>
			<td data-title="Earnings">
				$350,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				17.</td>
			<td data-title="Horse">
				Country House</td>
			<td data-title="Trainer">
				Bill Mott</td>
			<td data-title="Points">
				50</td>
			<td data-title="Earnings">
				$220,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				18.</td>
			<td data-title="Horse">
				Gray Magician</td>
			<td data-title="Trainer">
				Peter Miller</td>
			<td data-title="Points">
				41</td>
			<td data-title="Earnings">
				$526,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				19.</td>
			<td data-title="Horse">
				Spinoff</td>
			<td data-title="Trainer">
				Todd Pletcher</td>
			<td data-title="Points">
				40</td>
			<td data-title="Earnings">
				$224,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				20.</td>
			<td data-title="Horse">
				Master Fencer (JPN)</td>
			<td data-title="Trainer">
				Koichi Tsunoda</td>
			<td data-title="Points">
				Japan Road to Derby</td>
			<td data-title="Earnings">
				$234,392</td>
		</tr>
		<tr>
			<td data-title="Rank">
				21.</td>
			<td data-title="Horse">
				Bodexpress</td>
			<td data-title="Trainer">
				Gustavo Delgado</td>
			<td data-title="Points">
				40</td>
			<td data-title="Earnings">
				$188,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				22.</td>
			<td data-title="Horse">
				Signalman</td>
			<td data-title="Trainer">
				Kenny McPeek</td>
			<td data-title="Points">
				38</td>
			<td data-title="Earnings">
				$496,840</td>
		</tr>
		<tr>
			<td data-title="Rank">
				23.</td>
			<td data-title="Horse">
				Anothertwistafate</td>
			<td data-title="Trainer">
				Blaine Wright</td>
			<td data-title="Points">
				38</td>
			<td data-title="Earnings">
				$268,960</td>
		</tr>
		<tr>
			<td data-title="Rank">
				24.</td>
			<td data-title="Horse">
				Sueno</td>
			<td data-title="Trainer">
				Keith Desormeaux</td>
			<td data-title="Points">
				32</td>
			<td data-title="Earnings">
				$285,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				25.</td>
			<td data-title="Horse">
				Bourbon War</td>
			<td data-title="Trainer">
				Mark Hennig</td>
			<td data-title="Points">
				31</td>
			<td data-title="Earnings">
				$137,200</td>
		</tr>
		<tr>
			<td data-title="Rank">
				26.</td>
			<td data-title="Horse">
				Instagrand</td>
			<td data-title="Trainer">
				Jerry Hollendorfer</td>
			<td data-title="Points">
				30</td>
			<td data-title="Earnings">
				$276,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				27.</td>
			<td data-title="Horse">
				Mucho Gusto</td>
			<td data-title="Trainer">
				Bob Baffert</td>
			<td data-title="Points">
				24</td>
			<td data-title="Earnings">
				$286,800</td>
		</tr>
		<tr>
			<td data-title="Rank">
				28.</td>
			<td data-title="Horse">
				Knicks Go</td>
			<td data-title="Trainer">
				Ben Colebrook</td>
			<td data-title="Points">
				20</td>
			<td data-title="Earnings">
				$675,085</td>
		</tr>
		<tr>
			<td data-title="Rank">
				29.</td>
			<td data-title="Horse">
				Owendale</td>
			<td data-title="Trainer">
				Brad Cox</td>
			<td data-title="Points">
				20</td>
			<td data-title="Earnings">
				$124,000</td>
		</tr>
		<tr>
			<td data-title="Rank">
				30.</td>
			<td data-title="Horse">
				Outshine</td>
			<td data-title="Trainer">
				Todd Pletcher</td>
			<td data-title="Points">
				20</td>
			<td data-title="Earnings">
				$74,500</td>
		</tr>
	</tbody>
</table>
</div>
<!-- <div>
<p>+ = Not Triple Crown Nominated
<br />
* = Holds entry in Arkansas Derby or Lexington Stakes on Saturday
</p>
</div> -->
<!--
	<div>
        <table id='infoEntries' width='100%' border='0' cellspacing='0' cellpadding='0' class='table table-condensed table-striped table-bordered' >
        <tbody>
        <tr>
            <td class="dateUpdated center" colspan="4"><em>Updated April 1, 2019</em><br />
<em>+ = Not Triple Crown Nominated</em>
            </td>
        </tr>
        </tbody>
        </table>
        </div>
-->
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_leader.js"></script>
{/literal}
