
  <script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type": "Table",
    "about": "2020 Kentucky Derby Odds"
    }
  </script>
  
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Kentucky Derby Odds"  summary="The latest odds for the Kentucky Derby available for wagering now at Bet US Racing.">
            <caption> Kentucky Derby Odds</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y H:00", time() - 7200); echo $tdate . " EST"; ?> .</em>
                    </td>
                </tr>
            </tfoot>
            <tbody><tr class='head_title'>
        <th style="width: 8%;">PP</th>
              <th style="width: 8%;">Horse</th>
              <th style="width: 8%;">Odds</th>
              <th style="width: 8%;">Jockey</th>
              <th style="width: 13%;">Trainer</th>
      </tr>
          <tr>
              <td data-title="PP">1</td>
              <td data-title="Horse"><strong>Tap It To Win</strong></td>
              <td data-title="Odds">6/1</td>
              <td data-title="Jockey">John R. Velazquez</td>
              <td data-title="Trainer">Mark E. Casse</td>
          </tr>
          <tr>
              <td data-title="PP">2</td>
              <td data-title="Horse"><strong>Sole Volante</strong></td>
              <td data-title="Odds">9/2</td>
              <td data-title="Jockey">Luca Panici</td>
              <td data-title="Trainer">Patrick L. Biancone</td>
          </tr>
          <tr>
              <td data-title="PP">3</td>
              <td data-title="Horse"><strong>Max Player</strong></td>
              <td data-title="Odds">15/1</td>
              <td data-title="Jockey">Joel Rosario</td>
              <td data-title="Trainer">Linda Rice</td>
          </tr>
          <tr>
              <td data-title="PP">4</td>
              <td data-title="Horse"><strong>Modernist</strong></td>
              <td data-title="Odds">15/1</td>
              <td data-title="Jockey">Junior Alvarado</td>
              <td data-title="Trainer">William I. Mott</td>
          </tr>
          <tr>
              <td data-title="PP">5</td>
              <td data-title="Horse"><strong>Farmington Road</strong></td>
              <td data-title="Odds">15/1</td>
              <td data-title="Jockey">Javier Castellano</td>
              <td data-title="Trainer">Todd A. Pletcher</td>
          </tr>
          <tr>
              <td data-title="PP">6</td>
              <td data-title="Horse"><strong>Fore Left</strong></td>
              <td data-title="Odds">30/1</td>
              <td data-title="Jockey">Jose Ortiz</td>
              <td data-title="Trainer">Doug O’Neill</td>
          </tr>
          <tr>
              <td data-title="PP">7</td>
              <td data-title="Horse"><strong>Jungle Runner</strong></td>
              <td data-title="Odds">50/1</td>
              <td data-title="Jockey">Reylu Gutierrez</td>
              <td data-title="Trainer">Steven M. Asmussen</td>
          </tr>
          <tr>
              <td data-title="PP">8</td>
              <td data-title="Horse"><strong>Tiz The Law</strong></td>
              <td data-title="Odds">6/5</td>
              <td data-title="Jockey">Manny Franco</td>
              <td data-title="Trainer">Barclay Tagg</td>
          </tr>
          <tr>
              <td data-title="PP">9</td>
              <td data-title="Horse"><strong>Dr Post</strong></td>
              <td data-title="Odds">5/1</td>
              <td data-title="Jockey">Irad Ortiz, Jr.</td>
              <td data-title="Trainer">Todd A. Pletcher</td>
          </tr>
          <tr>
              <td data-title="PP">10</td>
              <td data-title="Horse"><strong>Pneumatic</strong></td>
              <td data-title="Odds">8/1</td>
              <td data-title="Jockey">Ricardo Santana, Jr.</td>
              <td data-title="Trainer">Steven M. Asmussen</td>
          </tr></tbody>
        </table>
    </div>

    <style>
            table.ordenable th{cursor: pointer}
      table.ordenable td{ word-wrap: break-word !important; word-break: break-all !important;}
      table.ordenable{ max-width: 100% !important; }
    </style>
    <script src="/assets/js/jquery.sortElements.js"></script>
    <script src="/assets/js/sorttable_post.js"></script>
    <script src="/assets/js/aligncolumn.js"></script>
    <script>
        $(document).ready(function () {

            alignColumn([2], 'left');

        });
    </script>
    