{literal}
<script type="text/javascript" src="/assets/js/atemay.js"></script>
<script type="text/javascript">
addthisevent.settings({
	license   	: "ammgus1bqzg0il7ntm75",
	mouse		: false,
	css			: false,
	outlook		: {show:true, text:"Outlook <i class='fa fa-plus-square'></i>"},
	google		: {show:true, text:"Google <i class='fa fa-plus-square'></i>"},
	yahoo		: {show:true, text:"Yahoo <i class='fa fa-plus-square'></i>"},
	ical		: {show:true, text:"iCal <i class='fa fa-plus-square'></i>"},
	hotmail		: {show:true, text:"Hotmail <i class='fa fa-plus-square'></i>"},
	facebook	: {show:true, text:"Facebook <i class='fa fa-plus-square'></i>"}
});
</script>
{/literal}

<div class="row infoBlocks">
<div class="col-md-4">
<div class="section">
	<i class="fa fa-calendar"></i>
	<div class="info">
	 <p><strong>When is the Kentucky Derby?</strong></p>    
	<p>The Kentucky Derby is on  {include_php file='/home/ah/allhorse/public_html/kd/date.php'}</p>
	</div>
    <a href="#" title="Add to Calendar" class="addthisevent" rel="nofollow">Add to my calendar <i class="fa fa-plus"></i>
	<span class="_start">09-05-2020 17:00:00</span>
	<span class="_end">09-09-2020 18:30:00</span>
	<span class="_zonecode">15</span> <!-- EST US -->
	<span class="_summary">2020 Kentucky Derby - Place My Bet</span>
	<span class="_description">www.usracing.com <br/>Place my Kentucky Derby Bet at https://busr.ag.</span>
	<span class="_location">Churchill Downs</span>
	<span class="_organizer">USRacing.com</span>
	<span class="_organizer_email">comments@usracing.com</span>
	<span class="_all_day_event">false</span>
	<span class="_date_format">09/09/2020</span>
	</a>
</div>
</div>

<div class="col-md-4">
<div class="section">
	<i class="fa fa-map-marker"></i>
	<div class="info">

	<p><strong>Where can I bet on the Kentucky Derby? </strong></p><p>At BUSR: <br><a itemprop="url" href="/kentucky-derby/odds">Odds are Live!</a></p>
    </div>
    <p><a href="/signup?ref={$ref}" class="btn btn-primary btn-sm btn-red" rel="nofollow">Bet Now!<i class="fa fa-chevron-right"></i></a>   </p>
</div>
</div>

<div class="col-md-4">
<div class="section">
	<i class="glyphicon glyphicon-eye-open"></i>
    <div class="info">
	<p><strong>What channel is the Kentucky Derby on?</strong></p> 
	<p>Watch the Kentucky Derby live on TV with <a href="http://www.nbc.com/"rel="nofollow" target="_blank">NBC</a> at 5:00 pm EST</p>
    </div>
</div>
</div>
</div>
  