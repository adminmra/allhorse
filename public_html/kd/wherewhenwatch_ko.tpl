
{literal}
<script type="text/javascript" src="/assets/js/atemay.js"></script>
<script type="text/javascript">
addthisevent.settings({
	license   	: "ammgus1bqzg0il7ntm75",
	mouse		: false,
	css			: false,
	outlook		: {show:true, text:"Outlook <i class='fa fa-plus-square'></i>"},
	google		: {show:true, text:"Google <i class='fa fa-plus-square'></i>"},
	yahoo		: {show:true, text:"Yahoo <i class='fa fa-plus-square'></i>"},
	ical		: {show:true, text:"iCal <i class='fa fa-plus-square'></i>"},
	hotmail		: {show:true, text:"Hotmail <i class='fa fa-plus-square'></i>"},
	facebook	: {show:true, text:"Facebook <i class='fa fa-plus-square'></i>"}
});
</script>
{/literal}

<div class="row infoBlocks">
<div class="col-md-4">
<div class="section">
	<i class="fa fa-calendar"></i>
	<div class="info">
	<p><strong>When is the Kentucky Oaks?</strong></p>    <p>The Kentucky Oaks is on {include file='/home/ah/allhorse/public_html/kd/racedate_ko.php'}</p>
	</div>
    <a href="#" title="Add to Calendar" class="addthisevent" rel="nofollow">Add to calendar <i class="fa fa-plus"></i>
	<span class="_start">03-04-2018 17:45:00</span>
	<span class="_end">03-04-2018 18:45:00</span>
	<span class="_zonecode">15</span> <!-- EST US -->
	<span class="_summary">2019 Kentucky Oaks</span>
	<span class="_description">www.usracing.com <br/>Lead-in race to the Kentucky Derby. <br/>Races times subject to change.</span>
	<span class="_location">Churchill Downs</span>
	<span class="_organizer">usracing.com</span>
	<span class="_organizer_email">comments@usracing.com</span>
	<span class="_all_day_event">false</span>
	<span class="_date_format">03/04/2018</span>
    </a>
</div>
</div>

<div class="col-md-4">
<div class="section">
	<i class="fa fa-map-marker"></i>
	<div class="info">

	<p><strong>Where can I bet on the Kentucky Oaks? </strong></p><p>At BUSR: <br><a itemprop="url" href="/kentucky-derby/odds">Odds are Live!</a></p>
    </div>
    <p><a href="/signup?ref=Kentucky-Oaks" class="btn btn-primary btn-sm btn-red" rel="nofollow">Bet Now!<i class="fa fa-chevron-right"></i></a>   </p>
</div>
</div>

<div class="col-md-4">
<div class="section">
	<i class="glyphicon glyphicon-eye-open"></i>
    <div class="info">
	<p><strong>What channel is the Kentucky Oaks on?</strong></p> 
	<p>Watch the Kentucky Derby live on TV with <a href="http://www.nbc.com/"rel="nofollow" target="_blank">NBC</a> at 12:30 pm EST</p>
    </div>
</div>
</div>
</div>