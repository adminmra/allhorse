	<div>
		<table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Kentucky Derby Odds"  summary="The latest odds for the Kentucky Derby available for wagering now at BUSR.">
			<caption>Horses - Kentucky Derby - Props</caption>
			<tbody>
				<tr>
					<th colspan="3" class="center">
					Horses - Kentucky Derby - Props  - May 04					</th>
			</tr>
	<tr><th colspan="3" class="center">2019 Kentucky Derby  -  Margin Of Victory</th></tr><tr class='head_title'><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Dead Heat</td><td>+3000</td><td>30/1</td></tr><tr><td>Nose</td><td>+1000</td><td>10/1</td></tr><tr><td>Head</td><td>+800</td><td>8/1</td></tr><tr><td>Neck</td><td>+800</td><td>8/1</td></tr><tr><td>1/2 Length To 3/4 Length</td><td>+375</td><td>15/4</td></tr><tr><td>1 Length To 2 3/4 Lengths</td><td>+200</td><td>2/1</td></tr><tr><td>3 Lengths To 5 3/4 Lengths</td><td>+225</td><td>9/4</td></tr><tr><td>6 Lengths To 7 3/4 Lengths</td><td>+650</td><td>13/2</td></tr><tr><td>8 Lengths To 10 3/4 Lenghts</td><td>+725</td><td>29/4</td></tr><tr><td>11 Lengths To 14 3/4 Lengths</td><td>+1000</td><td>10/1</td></tr><tr><td>15 Lengths Or More</td><td>+1200</td><td>12/1</td></tr><tr><th colspan="3" class="center">Will Secretariat's Record Of 1:59.4 Be Broken?</th></tr><tr><td>Yes</td><td>+1000</td><td>10/1</td></tr><tr><td>No</td><td>-2500</td><td>1/25</td></tr><tr><th colspan="3" class="center">What Will The First Quarter Mile Be?</th></tr><tr><td>Over 22.8 Seconds</td><td>-120</td><td>5/6</td></tr><tr><td>Under 22.8 Seconds</td><td>-110</td><td>10/11</td></tr><tr><th colspan="3" class="center">Time Of The First Half Mile Be?</th></tr><tr><td>Over 46.8 Seconds</td><td>-110</td><td>10/11</td></tr><tr><td>Under 46.8 Seconds</td><td>-120</td><td>5/6</td></tr><tr><th colspan="3" class="center">Gate The Kentucky Derby Winner Starts From</th></tr><tr><td>1-10</td><td>-300</td><td>1/3</td></tr><tr><td>11-20</td><td>+200</td><td>2/1</td></tr><tr><th colspan="3" class="center">Will The Kentucky Derby Winner Lead Wire To Wire?</th></tr><tr><td>Yes</td><td>+450</td><td>9/2</td></tr><tr><td>No</td><td>-800</td><td>1/8</td></tr><tr><th colspan="3" class="center">Wll The Crowd Attendance Exceed 158,070 Of 2017?</th></tr><tr><td>Yes</td><td>-250</td><td>2/5</td></tr><tr><td>No</td><td>+170</td><td>17/10</td></tr><tr><th colspan="3" class="center">Winning Horse's Name Starts With Letter</th></tr><tr><td>A-i</td><td>+140</td><td>7/5</td></tr><tr><td>J-z</td><td>-180</td><td>5/9</td></tr>					<tr>
						<td class="dateUpdated center" colspan="5">
							<em id='updateemp'>Updated March 20, 2019.</em>

<!-- br />
							All odds are fixed odds prices. -->
						</td>
					</tr>
			</tbody>
		</table>
	</div>
	