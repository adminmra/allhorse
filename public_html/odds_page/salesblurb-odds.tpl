<h1 class="kd_heading">Horse Racing Odds</h1>
<p>These are the latest Morning Line odds for almost all worldwide racetracks. </p>
        {assign var="cta_append" value=" at any of these races"}
        <p>{include file="/home/ah/allhorse/public_html/usracing/calltoaction_text.tpl"}</p>
        <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now</a></p>
        <p>Please note:  The morning odds, also known as the 'Morning Lines' (M/L) are set by the track for the day's races.  During the course of the day up until the actual race, the actual odds may change despite the fact that posted M/L in the racebook does not change. Winners are always paid out at full track odds*. For more information on odds and payouts, please visit the BUSR Track Payouts in the clubhouse. </p>
        <center>
   <p>Live odds can take a moment to load.</p>
			<h2>Latest Morning Line odds</h2>
  </center>
