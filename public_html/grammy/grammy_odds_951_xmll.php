	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2020 Grammy Odds"
	  }
	</script>
    <style>
        .first-th {
            width: 70%;
        }
    </style>
    <div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Ent - Music">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id='updateemp'>Updated December 2, 2019.</em>
                        <!-- br> All odds are fixed odds prices. -->
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Ent - Music  - Jan 26                    </th>
            </tr>
            -->
    <tr><td colspan="3" class="center"><table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>    <tr><th colspan="3" class="center">2019 Grammy Awards - Album Of The Year</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Billie Eilish - When We All Fall Asleep, Where Do </td><td>5/7</td><td>-140</td></tr><tr><td>Ariana Grande - Thank U, Next </td><td>5/2</td><td>+250</td></tr><tr><td>Lizzo - Cuz I Love You </td><td>3/1</td><td>+300</td></tr><tr><td> Lana Del Rey - Norman F***ing Rockwell!</td><td>9/1</td><td>+900</td></tr><tr><td>Vampire Weekend - Father Of The Bride </td><td>16/1</td><td>+1600</td></tr><tr><td>Bon Iver - I, I </td><td>20/1</td><td>+2000</td></tr><tr><td>Lil Nas X - 7 </td><td>25/1</td><td>+2500</td></tr><tr><td>H.e.r. - I Used To Know Her</td><td>30/1</td><td>+3000</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>    <tr><th colspan="3" class="center">2019 Grammy Awards - Song Of The Year</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Lizzo - Truth Hurts</td><td>13/10</td><td>+130</td></tr><tr><td>Billie Eilish - Bad Guy</td><td>2/1</td><td>+200</td></tr><tr><td>Taylor Swift - Lover</td><td>3/1</td><td>+300</td></tr><tr><td>Lewis Capaldi - Someone You Loved</td><td>9/2</td><td>+450</td></tr><tr><td>Lady Gaga - Always Remember Us This Way</td><td>15/2</td><td>+750</td></tr><tr><td>H.e.r - Hard Place</td><td>18/1</td><td>+1800</td></tr><tr><td>Tanya Tucker - Bring My Flowers Now</td><td>20/1</td><td>+2000</td></tr><tr><td>Lana Del Rey - Norman F***ing Rockwell!</td><td>10/1</td><td>+1000</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>    <tr><th colspan="3" class="center">2019 Grammy Awards - Best New Artist</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Billie Eilish</td><td>5/7</td><td>-140</td></tr><tr><td>Lizzo </td><td>5/4</td><td>+125</td></tr><tr><td>Lil Nas X </td><td>8/1</td><td>+800</td></tr><tr><td>Rosalia </td><td>11/1</td><td>+1100</td></tr><tr><td>Maggie Rogers </td><td>20/1</td><td>+2000</td></tr><tr><td>Yola </td><td>25/1</td><td>+2500</td></tr><tr><td>Black Pumas </td><td>30/1</td><td>+3000</td></tr><tr><td>Tank And The Bangas </td><td>35/1</td><td>+3500</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>    <tr><th colspan="3" class="center">2019 Grammy Awards - Record Of The Year</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Lil Nas X Featuring Billy Ray Cyrus - Old Town Roa</td><td>11/10</td><td>+110</td></tr><tr><td>Billie Eilish - Bad Guy</td><td>2/1</td><td>+200</td></tr><tr><td>Lizzo - Truth Hurts </td><td>4/1</td><td>+400</td></tr><tr><td>Ariana Grande - 7 Rings</td><td>6/1</td><td>+600</td></tr><tr><td>Post Malone & Swae Lee - Sunflower </td><td>15/2</td><td>+750</td></tr><tr><td>H.e.r - Hard Place </td><td>10/1</td><td>+1000</td></tr><tr><td>Khalid - Talk</td><td>20/1</td><td>+2000</td></tr><tr><td>Bon Iver - Hey, Ma </td><td>30/1</td><td>+3000</td></tr></tbody></table></td></tr>            </tbody>
        </table>
    </div>
        <script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
    addSort('o2t');
    addSort('o3t');
    addSort('o4t');
</script>
    