	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2020 Grammy Odds"
	  }
	</script>
    <style>
        .first-th {
            width: 70%;
        }
    </style>
    <div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Ent - Music">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id='updateemp'>  - Updated October 16, 2020 17:34:16 </em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Ent - Music  - Jul 31                    </th>
            </tr>
            -->
    <tr><td colspan="3" class="center"><table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>    <tr><th colspan="3" class="center">2020 Acm Awards - Album Of The Year</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Maren Morris - Girl</td><td>2/1</td><td>+200</td></tr><tr><td>Miranda Lambert - Wildcard</td><td>9/4</td><td>+225</td></tr><tr><td>Thomas Rhett - Center Point Road</td><td>11/4</td><td>+275</td></tr><tr><td>Luke Combs - What You See Is What You Get</td><td>4/1</td><td>+400</td></tr><tr><td>Jon Pardi - Heartache Medication</td><td>5/1</td><td>+500</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>    <tr><th colspan="3" class="center">2020 Acm Awards - Entertainer Of The Year</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Carrie Underwood</td><td>9/4</td><td>+225</td></tr><tr><td>Luke Bryan</td><td>11/4</td><td>+275</td></tr><tr><td>Thomas Rhett</td><td>3/1</td><td>+300</td></tr><tr><td>Luke Combs</td><td>7/2</td><td>+350</td></tr><tr><td>Eric Church</td><td>7/2</td><td>+350</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o2t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>    <tr><th colspan="3" class="center">2020 Acm Awards - Female Artist Of The Year</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Carrie Underwood</td><td>9/4</td><td>+225</td></tr><tr><td>Maren Morris</td><td>11/4</td><td>+275</td></tr><tr><td>Kacey Musgraves</td><td>3/1</td><td>+300</td></tr><tr><td>Kelsea Ballerini</td><td>7/2</td><td>+350</td></tr><tr><td>Miranda Lambert</td><td>7/2</td><td>+350</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o3t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>    <tr><th colspan="3" class="center">2020 Acm Awards - Male Artist Of The Year</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>Thomas Rhett</td><td>9/4</td><td>+225</td></tr><tr><td>Luke Combs</td><td>11/4</td><td>+275</td></tr><tr><td>Keith Urban</td><td>3/1</td><td>+300</td></tr><tr><td>Chris Stapleton</td><td>33/10</td><td>+330</td></tr><tr><td>Dierks Bentley</td><td>7/2</td><td>+350</td></tr></tbody></table></td></tr><tr><td colspan="3" class="center"><table id="o4t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"> <tbody>    <tr><th colspan="3" class="center">2020 Acm Awards - Song Of The Year</th>    </tr>    <tr class="sortar">        <th class="first-th" <!--Team-->        </th>        <th>Fractional</th>        <th>American</th>    </tr><tr><td>10000 Hours (dan-shay -justin Bieber)</td><td>9/4</td><td>+225</td></tr><tr><td>Gods Country (blake Shelton)</td><td>3/1</td><td>+300</td></tr><tr><td>Girl Goin Nowhere (ashley Mcbryde)</td><td>3/1</td><td>+300</td></tr><tr><td>One Man Band (old Dominion)</td><td>33/10</td><td>+330</td></tr><tr><td>Some Of It (eric Church</td><td>7/2</td><td>+350</td></tr></tbody></table></td></tr>            </tbody>
        </table>
    </div>
        <script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
    addSort('o2t');
    addSort('o3t');
    addSort('o4t');
</script>
    