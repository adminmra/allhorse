
<?php


if ( !defined('APP_PATH') ) {
	define( 'APP_PATH' , '/home/ah/usracing.com/htdocs/FeedsV2/' );
}

require_once APP_PATH . 'Generator.php';

class BreedersChallengeGenerator extends Generator{

	function __construct(){
		parent::__construct();
	}

	public function news_breeder_races(){
		$currenttime = date('Y-m-d');

		$news_breeder_races = "SELECT * FROM 2011_breeders_challenge WHERE racedate>='$currenttime' ORDER BY racedate";
		$news_breeder_races = DB::query( $news_breeder_races );

		return $this->section_tpl( $news_breeder_races );
	}
	public function old_breeder_races(){
		$currenttime = date('Y-m-d');

		$old_breeder_races = "SELECT * FROM 2011_breeders_challenge WHERE racedate<'$currenttime' ORDER BY racedate DESC";
		$old_breeder_races = DB::query( $old_breeder_races );

		return $this->section_tpl( $old_breeder_races );
	}

	public function content_tpl(){}

	private function section_tpl( $breeder ){

		ob_start();
		?>
		<table width="100%" class="table table-condensed table-striped table-bordered" id="infoEntries" >

			<tr >
				<th width="10%" >Date</th>
				<th>Race</th>
				<th>Track</th>
				<!--th >Distance</th>
				<th >Grade</th>
				<th >Age</th>
				-->
				<th >Qualifier</th>
				<th >Winner</th>
			</tr>
			<?php
			$count = 0;
			while ($rows = $breeder->fetch_object() ) {
				if ($count % 2 == 0) {
					echo "<tr>";
				} else {
					echo "<tr bgcolor='#EBEBEB'>";
				}
				/* Leo's code starts here */
				// /home/ah/usracing.com/smarty/templates/content/stake
				$racename_101613        = stripslashes($rows->race);
				$racenameCleaned_101613 = strtolower($racename_101613);
				$racenameCleaned_101613 = trim(preg_replace("/[^ \w]+/", "", $racenameCleaned_101613));
				$linkForRacename        = '';
				if ($racenameCleaned_101613 !== '') {
					$tmp1_101613 = str_replace(" ", "-", $racenameCleaned_101613);
					$tmp2_101613 = "../../smarty/templates/content/stake/" . $tmp1_101613 . ".tpl";
					if (file_exists($tmp2_101613))
						$linkForRacename = "https://www.usracing.com/" . str_replace(" ", "-", $tmp1_101613);
				}

				$trackname_101613        = stripslashes($rows->track);
				$tracknameCleaned_101613 = strtolower($trackname_101613);
				$tracknameCleaned_101613 = trim(preg_replace("/[^ \w]+/", "", $tracknameCleaned_101613));
				$linkForTrackname        = '';
				if ($tracknameCleaned_101613 !== '') {
					$tmp1_101613 = str_replace(" ", "-", $tracknameCleaned_101613);
					$tmp2_101613 = "../../smarty/templates/content/racetrack/" . $tmp1_101613 . ".tpl";
					if (file_exists($tmp2_101613))
						$linkForTrackname = "https://www.usracing.com/" . str_replace(" ", "-", $tmp1_101613);
				}
				/* Leo's code ends here */

				$Date = explode("-", $rows->racedate);
				?>

				<td  ><?php echo date('M j, Y', mktime(0, 0, 0, $Date[1], $Date[2], $Date[0])); ?></td>
				<td >
					<?php if ($linkForRacename !== '') { ?>
						<a href="<?php	 	 echo $linkForRacename; ?>"><?php	 	 echo $racename_101613; ?></a>
						<?php
					} else {
						echo $racename_101613;
					}
					//echo $rows->race;
					?>
				</td>
				<td >
					<?php
					// section track
					if ($linkForTrackname !== '') { ?>
						<a href="<?php echo $linkForTrackname; ?>"><?php echo $trackname_101613; ?></a>
					<?php
					} else {
						echo utf8_encode( $trackname_101613 );
					}
					//echo $rows->track;
					?>
				</td>
				<td >
					<?php
						// ls -la /home/ah/usracing.com/smarty/templates/content/breeders-cup/ | grep **.tpl
					//echo TEMPLATE_DIR .'../content/breeders-cup/'.strtolower( $rows->division ).'.tpl';
					$link_division =  strtolower( preg_replace( "@\s[&]\s|\s+@" , '-', trim( $rows->division ) ) );
					$file_exist = file_exists( TEMPLATE_DIR .'../content/breeders-cup/'.$link_division.'.tpl' );
					echo (  $file_exist )? "<a href='https://www.usracing.com/breeders-cup/".$link_division."'>{$rows->division}</a>" : $rows->division;
					?>

				</td>
				<!--td ><?php	 echo $rows->distance; ?></td>
				<td ><?php	 	 echo $rows->noms; ?></td>
				<td ><?php	 	 echo $rows->age; ?></td-->
				<td ><?php	 	 echo $rows->winner; ?></td>
			</tr>

			<?php
			$count++;
		}
		?>
		<tr><td colspan="7" align="left">
				* - tentative date<br>
				** - purse includes Breeders' Cup Stakes funds
			</td></tr>

		</table>
		</td>
		</tr>
		</table>

		<?php
		return ob_get_clean();
	}

	public static function run() {

		try {
			$instance = new self;
			$html_block = $instance->news_breeder_races() . $instance->old_breeder_races();
			$instance->as_file_save_data( $html_block , 'ahr_block_breeders_challenge.tpl' );

		} catch ( Exception $e ) {
			print( $e->getMessage() );
		}
	}

}
BreedersChallengeGenerator::run();

?>