
<?php
if ( !defined('APP_PATH') ) {
	define( 'APP_PATH' , '/home/ah/allhorse/public_html/FeedsV2/' );
	define( 'TEMPLATE_DIR'    , "/home/ah/usracing.com/smarty/templates/includes/" );
}

require_once APP_PATH . 'Generator.php';

class AhrHorseRacingScheduleGenerator extends Generator{

	function __construct(){
		parent::__construct();
	}

	public function content_tpl(){

		$today     = jddayofweek ( cal_to_jd(CAL_GREGORIAN, date("m"),date("d"), date("Y")) , 0 );
		$subsdays  = $today-1;
		$adddays   = 7 - $today;
		$startdate = strtotime ( '-'.$subsdays.' day' , strtotime ( date('Y-m-d') ) ) ;
		$enddate   = strtotime ( '+'.$adddays.' day' , strtotime ( date('Y-m-d') ) ) ;
		$startdate = date('Y-m-d',$startdate);

		$enddate = date('Y-m-d',$enddate);

		$sql  = "SELECT * FROM horse_racing_schedule WHERE dateScrap BETWEEN CAST('$startdate' AS DATE)
		AND CAST('$enddate' AS DATE)
		ORDER BY dateScrap ASC, id ASC";
		$result = DB::query( $sql );

		$list_schedule = array();

		while( $data = $result->fetch_array() ) {
			$list_schedule[] = $data;
		}
		ob_start();
		?>

		<div class="schedule">
		<?php
		$last_date ="";
		for ($i=0; $i <count($list_schedule) ; $i++) {
			if($last_date != $list_schedule[$i]['dateScrap']){
				$last_date = $list_schedule[$i]['dateScrap'];
			?>
			<div class="sItem">
				<div class="sDay">
					<i class="fa fa-calendar"></i>
					<?php echo date("l, F d, Y", strtotime($last_date));?>
				</div>
				<div class="sEvents">
			<?php
			}
			?>
				<div class="sRace">
					<span class="race"><?php echo $this->parse_racetrack_link( $list_schedule[$i]['name'] ) ?></span>
				</div>
			<?php
			if(((isset($list_schedule[$i+1]['dateScrap']))&&($last_date != $list_schedule[$i+1]['dateScrap']))||(empty($list_schedule[$i+1]['dateScrap'])))
			{ ?>
				</div>
			</div>
			<?php
			}
		}
		?>
		</div>
		<?php
		return ob_get_clean();
	}

	private function parse_racetrack_link( $link_name ){

		$link_name = utf8_encode( $link_name );
		$link_racetrack =  str_replace( "&rsquo;" , '', trim( $link_name ) );
		$link_racetrack =  strtolower( preg_replace( "@-\s.*@" , '', trim( $link_racetrack ) ) );
		$link_racetrack =  preg_replace( "@\s[&]\s|\s+@" , '-', trim( $link_racetrack ) );
		$link_racetrack =  preg_replace( "@-\(.*@" , '', trim( $link_racetrack ) );

		if ( $link_racetrack == 'santa-anita-park' ) {
			$link_racetrack = str_replace('-park', '', $link_racetrack );
		}
		elseif ( $link_racetrack == 'parx-racing-at-philadelphia-park' ) {
			$link_racetrack = trim( str_replace('-at-philadelphia-park', '', $link_racetrack ) );
		}
		elseif ( $link_racetrack == 'hoosier-park-harness' ) {
			$link_racetrack = trim( str_replace('-harness', '', $link_racetrack ) );
		}

		$file_exist = file_exists( TEMPLATE_DIR .'../content/racetrack/'.$link_racetrack.'.tpl' );

		return (  $file_exist ) ? "<a href='//www.usracing.com/".$link_racetrack."'>{$link_name}</a>" : $link_name;
	}



	public static function run() {
		try {
			$instance = new self;
			$html = $instance->content_tpl();
			//$instance->as_file_save_data( $html , 'ahr_racing_schedule_page2.tpl');
			$instance->as_file_save_data( $html , ALLHORSEPATH . 'public_html/generated/racing_schedules/horse_racing_schedule.php' , TRUE );

		} catch ( Exception $e ) {
			print( $e->getMessage() );
		}
	}

}

AhrHorseRacingScheduleGenerator::run();

?>