<?php
// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);
ini_set("display_errors", 1);

include '/home/ah/allhorse/public_html/FeedsV2/class.xmlreader.php';

$file = '/home/ah/allhorse/public_html/FeedsV2/racexml.xml';
$obj = new xmlreader1($file);
$xml = $obj->parse();
//print_r($xml);
function weekfirstday()
{
  $today = date('w');
  return  date('Y-m-d',mktime(0,0,0,date('m'),date('d')-($today-1),date('Y')));
}

function nextday($date,$dayafter)
{
  $datearray=explode("-",$date);
  return  date('Y-m-d',mktime(0,0,0,$datearray[1],$datearray[2]+$dayafter,$datearray[0]));
}

function nextdayformatted($date,$dayafter)
{
  $datearray=explode("-",$date);
  //return  date('F j, Y',mktime(0,0,0,$datearray[1],$datearray[2]+$dayafter,$datearray[0]));
  return  date('M j',mktime(0,0,0,$datearray[1],$datearray[2]+$dayafter,$datearray[0]));
}

ob_start();
?>
<div class="table-responsive">
<table id="infoEntries"  border="0" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
  <tr>
    <th><strong>Monday<br><?php echo nextdayformatted(weekfirstday(),0); ?></strong></th>
    <th><b>Tuesday<br><?php echo nextdayformatted(weekfirstday(),1); ?></b></th>
    <th><b>Wednesday<br><?php echo nextdayformatted(weekfirstday(),2); ?></b></th>
    <th><b>Thursday<br><?php echo nextdayformatted(weekfirstday(),3); ?></b></th>
    <th><b>Friday<br><?php echo nextdayformatted(weekfirstday(),4); ?></b></th>
    <th><b>Saturday<br><?php echo nextdayformatted(weekfirstday(),5); ?></b></th>
    <th><b>Sunday<br><?php echo nextdayformatted(weekfirstday(),6); ?></b></th>
  </tr>
  <tr ><?php for($i=0;$i<count($xml["schedule"]["#"]["scheduledata"]);$i++) { ?>
    <td>
      <p>
      <?php 
      if(is_array($xml["schedule"]["#"]["scheduledata"][$i]["#"]["track"])) {
        for($j=0;$j<count($xml["schedule"]["#"]["scheduledata"][$i]["#"]["track"]);$j++) { ?>
          <span  style="cursor:pointer; " title="cssbody=[dogvdvbdy] cssheader=[dogvdvhdr] header=[<?php echo  trim($xml["schedule"]["#"]["scheduledata"][$i]["#"]["track"][$j]["#"]);?>] body=[<?php echo trim($xml["schedule"]["#"]["scheduledata"][$i]["#"]["raceschedule"][$j]["#"]); ?>] " ><strong><?php echo  trim($xml["schedule"]["#"]["scheduledata"][$i]["#"]["track"][$j]["#"])."</strong> - ".trim($xml["schedule"]["#"]["scheduledata"][$i]["#"]["firstrace"][$j]["#"])."<br>";?></span>
        <?php } 
      } ?>
      </p>
    </td>
  <?php } ?></tr> 
</table>
</div>

<?php
$body_file = ob_get_contents(); 
ob_end_clean();

//$savefile = "/home/ah/allhorse/public_html/schedules/schedule_table.php";
$savefile = "/home/ah/allhorse/public_html/generated/racing_schedules/schedule_table.php";
if (!file_exists($savefile)) { exit("File not found\n"); }
$_file = fopen($savefile, "w");
fwrite($_file, $body_file);
fclose($_file);

?>