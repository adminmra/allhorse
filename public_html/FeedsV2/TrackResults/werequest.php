<?php	 		 	
error_reporting(E_ALL);
// Same as error_reporting(E_ALL);
ini_set('display_errors', "ON");


class WebRequest {
	var $URL;
	var $Host;
	var $Port;
	var $responseCode;
	var $responseMessage;
	var $_method;
	var $_lastResponse;
	var $LastSent;
	var $_GetArray = array();
	var $_PostArray = array();
	var $endOnBody = false;
	var $timeout = 15;
	var $succeedOnTimeout = false;
	
	var $cookieList;
	var $headerList;

	function WebRequest() {
		$this->Reset();
		$this->cookieList = array();
		$this->headerList = array();
	}

	function AddGetParam($key, $value) {
		$count = $this->_GetArray['__count'];
		$count++;
		$this->_GetArray[$count]['key'] = $key;
		$this->_GetArray[$count]['value'] = $value;
		$this->_GetArray['__count'] = $count;
	}

	function AddPostParam($key, $value) {
		$count = $this->_PostArray['__count'];
		$count++;
		$this->_PostArray[$count]['key'] = $key;
		$this->_PostArray[$count]['value'] = $value;
		$this->_PostArray['__count'] = $count;
	}

	function Content() {
	echo $this->_lastResponse;
		$tempStr = str_replace(chr(13), '', $this->_lastResponse);
		$tempArr = explode(chr(10), $tempStr);
		$chunked = false;
		while (true) {
			$thisLine = trim(strtolower($tempArr[0]));
			if ($thisLine <= ' ') { break; }
			if (strpos($thisLine, '-length') > 0) {
				$ptr = strpos($thisLine, ':');
				$contentLength = trim(substr($thisLine, $ptr + 1, 1024));
			}
			
			if (strpos($thisLine, 'encoding') > 0) {
				// If the encoding is chunked then I have to gather the content differently...
				$chunked = (strpos($thisLine, 'chunked') > 0);
			}
			
			array_shift($tempArr);
		}
		
		array_shift($tempArr);
		$tempStr = trim(implode(chr(10), $tempArr));

		if ($chunked) {
			// Bummer! NN2collect the data in chunks into a new array and send it back...
			// (also, there wont be a "length" value yet)
			$out = '';
			$ptr = strpos($tempStr, chr(10));
			$thisLen = hexdec(trim(substr($tempStr, 0, $ptr)));
			while ($thisLen > 0) {
				$tempStr = trim(substr($tempStr, $ptr + 1, strlen($tempStr)));
				$out .= substr($tempStr, 0, $thisLen);
				$tempStr = trim(substr($tempStr, $thisLen + 1, strlen($tempStr)));
				$ptr = strpos($tempStr, chr(10));
				$thisLen = hexdec(trim(substr($tempStr, 0, $ptr)));
			}
			
			return $out;
		} else return substr($tempStr, 0, $contentLength);
	}
	
	function Cookies() { return $this->cookieList; }

	function __Dispatch() {
		if ($this->URL <= ' ') { return 'Error: WebRequest requires the URL property to be set'; }
		if ($this->Host <= ' ') { return 'Error: WebRequest requires the Host property to be set'; }


		// Build the final URL...
		$finalURL = $this->URL;
		$getCount = $this->_GetArray['__count'];
		if ($getCount >= 0) {
			$sepStr = '?';
			$getStr = '';
			for ($i=0; $i<=$getCount; $i++) {
				$key = trim($this->_GetArray[$i]['key']);
				if ($key <= ' ') { continue; }
				$value = trim(urlencode($this->_GetArray[$i]['value']));
				$getStr .= "$sepStr$key=$value";
				$sepStr = '&';
			}
			
			if (substr($finalURL, -1, 1) == '/') { $finalURL = substr($finalURL, strlen($finalURL) - 1); }
			$finalURL .= $getStr;
		}


		// Build the content portion of the request...
		$postStr = 'No Content';
		$postCount = $this->_PostArray['__count'];
		if ($this->_method == 'POST') {
			$sepStr = '';
			$postStr = '';
			for ($i=0; $i<=$postCount; $i++) {
				$key = trim($this->_PostArray[$i]['key']);
				if ($key <= ' ') { continue; }
				$value = trim(urlencode($this->_PostArray[$i]['value']));
				$postStr .= "$sepStr$key=$value";
				$sepStr = '&';
			}
		}
		
		$requestLen = strlen($postStr);
		
		$cookieStr = '';
		$start = true;
		foreach($this->cookieList as $name=>$value)
		{
			if (!$start) { $cookieStr .= '; '; }
			$cookieStr .= "$name=$value";
			$start = false;
		}
		
		//      Build the actual HTTP request...
		if ($this->_method == 'GET') { $type='text/html'; }
		else { $type="application/x-www-form-urlencoded"; }

		$header = "{$this->_method} $finalURL HTTP/1.1\r\n";
		$header .= "Host: {$this->Host}\r\n";
		$header .= "User-Agent: Mozilla/5.0 (Macintosh; U; PPC Mac OS X; en) AppleWebKit/417.9 (KHTML, like Gecko) Safari/417.8\r\n";
		$header .= "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5\r\n";
		$header .= "Accept-Language: en-us,en;q=0.5\r\n";
		$header .= "Accept-Encoding: \r\n";
		$header .= "Accept-Charset: ISO-8859-1,utf-8:q=0.7,*;q=0.7\r\n";
		if ($cookieStr) { $header .= "Cookie: $cookieStr\r\n"; }
		$header .= "Connection: close\r\n\r\n";
		$header .= "Content-Length: $requestLen\r\n";
		$header .= "Content-Type: $type\r\n";
		$header .= "$postStr\r\n";
		
		$this->LastSent = $header;
		
		// Dispatch it!
		$errno = 0;
		$errstr = '';
		$socket = @fsockopen($this->Host, $this->Port, $errno, $errstr);
		$response = '';
		$endOnStr = ($this->endOnBody) ? '</body' : '</html';
		if ($socket) {
			fputs($socket, trim($header));
			$checkedHeader = false;
			while (!feof($socket)) 
			{ 
   				stream_set_timeout($socket, $this->timeout);
				$thisBlock = fread($socket, 65535);

				$response .= $thisBlock; 
				$testResp .= strtolower($thisBlock);

				$info = stream_get_meta_data($socket);
				if ($info['timed_out'])
				{
					if (($this->succeedOnTimeout) && (strpos($testResp, '</head') > 0)) {
						// Probably an incomplete page and the caller of this function
						// is ready for it...
						break;
					} else {
						$this->responseCode = -1;
						$this->responseMessage = 'Timed Out';
						$this->_lastResponse = '';
						return false;
					}
				}

				if (strlen($response) == 0)
				{
					// If I am here then it means that the socket was closed on me
					// without me receiving anything - otherwise, a complete failure. 

					$this->responseCode = -2;
					$this->responseMessage = 'Bad Request';
					$this->_lastResponse = '';
					return false;
				}


				// Important: Check to see if there is a redirect or a problem...
				// If so, store that and return the redirect page.
				if (!$checkedHeader)
				{
					$testResp = str_replace(chr(13), '', $testResp);
					$stop = strpos($testResp, chr(10) . chr(10));
					if ($stop > 0)
					{
						if (strpos(substr($testResp, 0, $stop), '301 error') > 0)
						{
							$ptr = strpos($testResp, 'location:');
							$response = substr($response, $ptr, strlen($response));
							$ptr = strpos($response, chr(10));
							$response = trim(substr($response, 0, $ptr));
							$this->responseCode = 301;
							$this->responseMessage = $response;
							$this->_lastResponse = '';
							return false;
						}
							
						if (strpos(substr($testResp, 0, $stop), '404 error') > 0) {
							$ptr = strpos($testResp, 'location:');
							$response = 'Page not found';
							$this->responseCode = 404;
							$this->responseMessage = $response;
							$this->_lastResponse = '';
							return false;
						}
						
						$checkedHeader = true;
					}
				}
				
				if (strpos($testResp, $endOnStr) > 0) { break; }
				
			}
			
			fclose($socket);
			
		} else { $response = "Error $errno: $errstr"; }

		$this->responseCode = 200;
		$this->_lastResponse = $response;
		
		// Time to do a little work on the returned packet...
		$this->headerList = array(); // clear it out
		$this->cookieList = array(); // Clear it out as well...
		$lines = explode(chr(10), $response);
		foreach($lines as $line)
		{
			if ($line <= ' ') { break; }
			if (strpos($line, ':') === false) { continue; }
			preg_match('/^([^:]*)\: (.*)$/', $line, $matches);
			$name = $matches[1];
			$value = $matches[2];
			$this->headerList[$name] = $value;
			if (strtolower($name) == 'set-cookie')
			{
				preg_match('/([^=]*)\=([^;]*)/', $value, $matches);
				$name = $matches[1];
				$value = $matches[2];
				$this->cookieList[$name] = $value;
			}
		}
		
		return $response;
	}

	function Get() {
		$this->_method = 'GET';
		return $this->__Dispatch();
	}
	
	function GetCookie($cookieName) { return $this->cookieList[$cookieName]; }
	function GetHeader($headerName) { return $this->headerList[$headerName]; }
	function Headers() { return $this->headerList; }

	function Post() {
		$this->_method = 'POST';
		return $this->__Dispatch();
	}

	function Reset() {
		$this->URL = '';
		$this->Host = '';
		$this->Port = 80;
		$this->_method = 'GET';
		$this->_GetArray['__count'] = -1;
		$this->_PostArray['__count'] = -1;
		$this->_lastResponse = '';
	}

	function SetCookie($cookieName, $cookieValue) { $this->cookieList[$cookieName] = $cookieValue; }

}

$req = new WebRequest();
$req->Host = 'https://sportsbook.gamingsystem.net';
$req->URL = 'https://sportsbook.gamingsystem.net/sportsbook4/www.allhorseracing.com/showbet.cgi';
$req->Get();
echo $req->Content()

?>