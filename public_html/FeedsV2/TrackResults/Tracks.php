<?php	 		 	
//error_reporting(E_ALL);
//phpinfo();
ini_set('display_errors',0);

class DataFeed{
	
	function DataFeed(){
	
		$this->host = '208.158.1.151';
		$this->port = 2766;
		$this->streamSize = 8192;
		$this->TracksFile = $_SERVER['DOCUMENT_ROOT'].'/Feeds/TrackResults/Cache/TrackResults.txt';
		$this->TracksRoot = $_SERVER['DOCUMENT_ROOT'].'/Feeds/TrackResults/Cache/';
		//$this->TracksFile = 'Feeds/TrackResults/Cache/TrackResults.txt';
		//$this->TracksRoot = 'Feeds/TrackResults/Cache/';
		//echo $this->TracksFile;
		$this->CacheTime = 60*20;
	
	}
	
	function Connect()
	{
		
		# try to init a new socket
		$this->sh = @fsockopen($this->host, $this->port);
		@socket_set_timeout ($this->sh,2);
		return $this->sh;

	}
	
	
	function SendCommand($command,$remove_first=true)
	{
	
		
		$this->Connect();
		
		$sh = $this->sh;
		
		
		$data = '';
		
		if($sh && !is_numeric($sh))
		{
		
			fwrite($sh, $command."\r\n");
			# allocate the incoming data stream to the variable $data
			//echo $command;
			if($remove_first)
			{
				$current_line = fgets($sh); // Remove First Line
			}
				
		
			stream_set_blocking($sh, TRUE);
			stream_set_timeout($sh,2);
			$info = stream_get_meta_data($sh);
			
			while (false !== ($char = fgetc($sh))) {
				$data.=$char;
			}
			/*
			while ((!feof($sh)) && (!$info['timed_out'])) {
					$data .= fgets($sh, 4096);
					$info = stream_get_meta_data($sh);
					ob_flush;
					flush();
			} */
			
			fclose($sh);
		}
		else
		{
			
			//print_r($info);
		
			//$data .= fgets($sh, 4096);
			
		}
		
		
			
		
		
		$data = str_replace("\r\n","\n",$data);
		//echo "DATA: ".$data."<br/><br/>";
		
		// REQUEST TIMED OUT.  SERVER UNDER HEAVY LOAD OR NO DATA RETURNED FROM SERVER
		if($info["timed_out"]==1 && strlen($data)==0)
		{
			return false;
		}
		
		// FEEDS DOWN (RETURNING EMPTY RESULT).  HAPPENS AROUND MIDNIGHT
		if(strlen(str_replace("\n","",$data))=="")
		{
			return false;
		}
		
		return $data;
		
	}
	
	
	function WriteCache($File,$data)
	{
	
		$fp = fopen($this->TracksRoot.$File,'w'); //or die("Couldnt Write To Cache");
		fwrite($fp,$data);
		fclose($fp);
	
	}
	
	
	function Cached($File)
	{
	
		if(!file_exists($this->TracksRoot.$File) || filemtime($this->TracksRoot.$File)+$this->CacheTime<time() || filesize($this->TracksRoot.$File)==0)
		{
		
			return false;
		
		}
		else
		{
		
			return true;
		
		}
	
	}
	
	function CachedStatic($File)
	{
		//echo $this->TracksRoot.$File;
		//echo $this->TracksRoot."<br>";
		//echo $File."<br>";
		//echo filesize($this->TracksRoot.$File)."<br>";
		if(!file_exists($this->TracksRoot.$File) || filesize($this->TracksRoot.$File)==0)
		{
		
			return false;
		
		}
		else
		{
		
			return true;
		
		}
	
	}
	
	function ReadCache($File)
	{
	
		$data = file_get_contents($this->TracksRoot.$File);
		return $data;
		
	}
	
	
	function GetTracks($return = 'all')
	{
		$OpenTracks = array();
		$ClosedTracks = array();
		$AllTracks = array();
		$data = '';
		
		
		if(!$this->Cached('TrackResults.txt'))
		{
		
        	$data = $this->SendCommand("list performances available");		// Returns FALSE if Command Timed Out
			
			if($data)
			{
				$this->WriteCache('TrackResults.txt',$data);
			}
			else
			{
			
				return false; // request timed out, or server updating at midnight
				
			}
			
		}
		else
		{
		
			$data = $this->ReadCache('TrackResults.txt');
		
		}
	

			
			$Tracks = explode("\n",$data); // Create Array from Each New Line
			array_pop($Tracks); // Remove Last Empty Item
			array_pop($Tracks); // Remove Last Empty Item
			//print_r($Tracks);
			$this->Tracks = $data;
			
		
            //Loop through each row...
			
            foreach ($Tracks as $Track)
            {
                //Break apart each property in the row...
			  
               $trackProperties = explode(";",$Track);

                //It looks like any race with STARTNET1 isn't being displayed on the production site.
                //if (!strstr("STARNET1",$trackProperties[5]))
                //{
                    //I don't know why we're doing this but this same logic was found in the PHP version of the site.
                    //We check to see if the element is empty; if it is, we use anothe element.
					//echo "Property 12: ".$trackProperties[12]."<br/>";
                    if ($trackProperties[7] == "") $trackProperties[7] = $trackProperties[6];
					
					if($trackProperties[3] != 'Woodbine Harness' and $trackProperties[3] != 'Pompano Park')
					{
                    //populate closeRaces object.
                    if ($trackProperties[12] == "CLOSED")
                        $ClosedTracks[]=array("ID"=>$trackProperties[1], "RACE"=>$trackProperties[7], "TRACK"=>$trackProperties[3],"MTP"=>$trackProperties[11]);
                    //populate openRaces object.
                    if ($trackProperties[12] == "OPEN")
                        $OpenTracks[]=array("ID"=>$trackProperties[1], "RACE"=>$trackProperties[7], "TRACK"=>$trackProperties[3],"MTP"=>$trackProperties[11]);
					}
					
                    $TrackKey = $trackProperties[1];
                    $TrackValue = $trackProperties;
                    //Populate for later use. EX: Key=APM, Value=a $of track properties.
                    $AllTracks[]=array($TrackKey, $TrackValue);
                    //$AllTracks[$trackProperties[1]]=array($TrackKey, $TrackValue);
                //}
            }
      		
			
			$this->OpenTracks = $OpenTracks;
			$this->ClosedTracks = $ClosedTracks;
			$this->AllTracks = $AllTracks;
			
			switch(strtolower($return))
			{
				
				case "open":
					return $this->OpenTracks;
					break;
				case "closed":
					return $this->ClosedTracks;
					break;
				default:
					return $this->AllTracks;
					break;
			
			}
			//var_dump($data);
		//}
		
	
	}
	
	function GetTracksStatic($return = 'all')
	{
		$OpenTracks = array();
		$ClosedTracks = array();
		$AllTracks = array();
		$data = '';
		
		
		if(!$this->CachedStatic('TrackResults.txt'))
		{
		
        	$data = $this->SendCommand("list performances available");		// Returns FALSE if Command Timed Out
			
			if($data)
			{
				$this->WriteCache('TrackResults.txt',$data);
			}
			else
			{
			
				return false; // request timed out, or server updating at midnight
				
			}
			
		}
		else
		{
		
			$data = $this->ReadCache('TrackResults.txt');
		
		}
	

			
			$Tracks = explode("\n",$data); // Create Array from Each New Line
			array_pop($Tracks); // Remove Last Empty Item
			array_pop($Tracks); // Remove Last Empty Item
			//print_r($Tracks);
			$this->Tracks = $data;
			
		
            //Loop through each row...
			
            foreach ($Tracks as $Track)
            {
                //Break apart each property in the row...
			  
               $trackProperties = explode(";",$Track);

                //It looks like any race with STARTNET1 isn't being displayed on the production site.
                //if (!strstr("STARNET1",$trackProperties[5]))
                //{
                    //I don't know why we're doing this but this same logic was found in the PHP version of the site.
                    //We check to see if the element is empty; if it is, we use anothe element.
					//echo "Property 12: ".$trackProperties[12]."<br/>";
                    if ($trackProperties[7] == "") $trackProperties[7] = $trackProperties[6];
					
					if($trackProperties[3] != 'Woodbine Harness' and $trackProperties[3] != 'Pompano Park')
					{
                    //populate closeRaces object.
                    if ($trackProperties[12] == "CLOSED")
                        $ClosedTracks[]=array("ID"=>$trackProperties[1], "RACE"=>$trackProperties[7], "TRACK"=>$trackProperties[3],"MTP"=>$trackProperties[11]);
                    //populate openRaces object.
                    if ($trackProperties[12] == "OPEN")
                        $OpenTracks[]=array("ID"=>$trackProperties[1], "RACE"=>$trackProperties[7], "TRACK"=>$trackProperties[3],"MTP"=>$trackProperties[11]);
					}
					
                    $TrackKey = $trackProperties[1];
                    $TrackValue = $trackProperties;
                    //Populate for later use. EX: Key=APM, Value=a $of track properties.
                    $AllTracks[]=array($TrackKey, $TrackValue);
                    //$AllTracks[$trackProperties[1]]=array($TrackKey, $TrackValue);
                //}
            }
      		
			
			$this->OpenTracks = $OpenTracks;
			$this->ClosedTracks = $ClosedTracks;
			$this->AllTracks = $AllTracks;
			
			switch(strtolower($return))
			{
				
				case "open":
					return $this->OpenTracks;
					break;
				case "closed":
					return $this->ClosedTracks;
					break;
				default:
					return $this->AllTracks;
					break;
			
			}
			//var_dump($data);
		//}
		
	
	}
	

	
	
	
	
	/// <summary>
    /// Get race times for the current track. We also pass the current Race selected
    /// so that while iterating through all races, we can pull out the current Race Details 
    /// and store them in the RaceInfo object.
    /// </summary>
    /// <param name="Id">The current track id.</param>
    /// <param name="Race">The current race</param>
    /// <returns>RaceTime generic list used by the UI.</returns>
    function GetTrackRaceTimes($Id,$Race='')
    {
		$this->GetTracks();
        $Success = false;
        $Command = "list contest ".$Id;
		$File = 'RaceTrackTimes_'.$Id;
		//$sh = $this->Connect();
		
		if(!$this->Cached($File))
		{
		
        	$Data = $this->SendCommand($Command);		// Returns FALSE if Command Timed Out
			
			if($Data)
			{
				$this->WriteCache($File,$Data);
			}
			else
			{
				return false; // server updating around midnight, or server connection timed out
			}
			
		}
		else
		{
		
			$Data = $this->ReadCache($File);
		
		}
		
		//echo "Size: ".strlen($Data);
		$RaceTimes = explode("\n",$Data);			// Create Array of each line
		array_pop($RaceTimes);
		array_pop($RaceTimes);
		$TrackRaceTimes = array();
		

        //List<RaceTime> TrackRaceTimes = new List<RaceTime>();
		
        if ($Data!=false)
        {
            foreach ($RaceTimes as $RaceTime)
            {
                $RaceProperties = explode(';',$RaceTime);
		$RaceStatus = array();
                $RaceStatus[$RaceProperties[2]] = $RaceProperties[8];
                $Status = "";
                
				
                switch($RaceProperties[8])
                {
                    case "RESULTS":
                        $Status = "Results";
                        break;
                    case "FINAL":
                        $Status = "Pending";
                        break;
                    case "OPEN":
                        $Status = "Entries";
                        break;
                    case "CLOSED":
                        $Status = "Entries(CLOSED)";
                        break;
                    case "REMOVED":
                        $Status = "Entries(REMOVED)";
                        break;
                }
				
				
		$MTP = "";
        $sb = "";
        
        $Tracks = $this->AllTracks;
        $CurrentTrack = array(); 
        foreach($Tracks as $Track)
        {
        	if($Track[0]==$Id)
        	{
        		 $CurrentTrack = $Track[1];	
        	}
        }
        
        if($_SERVER["REMOTE_ADDR"]=='75.84.125.133')
		{
			/*print_r($CurrentTrack);
			echo "<br/>Curr[7]: ".$CurrentTrack[7]." - Num2: ".$RaceProperties[2]." - Num8: ".$RaceProperties[8];
			echo "<br/><br/>";*/
		}
					
		if ($CurrentTrack[7] == $RaceProperties[2] && $RaceProperties[8] != "RESULTS")
		{
			$sb.="&#45;&nbsp;";
			$sb.=$CurrentTrack[11];
			$sb.="&nbsp;MTP";
			$MTP = $sb;
		}
		
		
    
                //Here's where we pass the current race info to build our RaceInfo object.
                //if ($Race == $RaceProperties[2])  //GetRaceInfo(RaceProperties, CurrentTrack);
                
                $TrackRaceTimes[]=array($RaceProperties[2], $RaceProperties[7], $Status, $MTP);
				
            }
        }else{
			echo "Timed Out";
		}

        return $TrackRaceTimes;
    }
	
	
	
	
	
	/// <summary>
    /// Get all entries for the current race.
    /// </summary>
    /// <param name="Id">The current track initials.</param>
    /// <param name="Race">The current race.</param>
    /// <returns>RaceEntry generic list used by the UI.</returns>
    function GetRaceEntries($Id, $Race)
    {
        $Success1 = false;
        $Success2 = false;
        

        $Command1 = "";
        $Command2 = "";

        $RaceEntries = array();
		$File = "RaceEntry_".$Id."_".$Race;


		if(!$this->CachedStatic($File))
		{
		
        	$Command1 = "LIST CONTESTANTS ".$Id." ".$Race;
			$EntryResponse = $this->SendCommand($Command1);
			$Success1 = $EntryResponse;
			
			if($Success1)
			{
				$this->WriteCache($File,$EntryResponse);
			}
			else
			{
				//echo "returning false";
				return false; // server updating around midnight, or server connection timed out
			}
			$filefrom= "Not Cached";
			
		}
		else
		{
		
			$EntryResponse = $this->ReadCache($File);
			$Success1 = true;
			$filefrom = "Cached";
		
		}
		
		//echo $filefrom;
        //echo $EntryResponse;
		
		$EntryResponse = explode("\n",$EntryResponse);			// Create Array of each line
		array_pop($EntryResponse);
		array_pop($EntryResponse);
		
		//print_r($EntryResponse);
		
		
		$Count = count($EntryResponse);
		
		if($Count==0)
		{
			//return false;
		}
		
		

        //Store this info to use later...
		$FileTwo = 'ListOdds_'.$Id.'_'.$Race;
        $ContestantInfo = $Success1;
		
		
		if(!$this->CachedStatic($FileTwo))
		{
			//echo "write";
			$Header = "BOOKIE;1.6.2;HBPROD1/PMPROD1;208.158.1.151";
			$Command2 = "LIST ODDS ".$Id." ".$Race;
			$fp = fsockopen('208.158.1.151',2766);
			stream_set_blocking($fp, TRUE);
			stream_set_timeout($fp,3);
			$info = stream_get_meta_data($fp);
			
			fwrite($fp,$Command2."\r\n");
			fread($fp,strlen($Header)+2);  // Remove Bookie Line
			$OddsResponse = fread($fp,1000);
			fclose($fp);
			
		}else{
			//echo "read";
			$OddsResponse = $this->ReadCache($FileTwo);
		
		}
		//$OddsResponse = '';
		
		
		$Success2 = true;
		
		
        if ($Success1 && $Success2)
        {
            $EntryA = array();
            $EntryB = array();

            foreach ($EntryResponse as $Entry)
            {
                $EntryProperties = explode(';',$Entry);
                if (strstr($EntryProperties[5],"NAME="))
				{
                    $EntryA[]=$Entry;
                }
				else
				{
                    $EntryB[]=$Entry;
				}
            }
			
            //$CLArray = $OddsResponse[0].Split(';')[6].Split('/');
			$TempCL = explode(';',$OddsResponse);
			$CLArray = explode('/',$TempCL[6]);

            $EntryCounter = 0;
            
            //This data is craptastic; the same data doesn't always 
            //appear in the same column so we're forced to search for it below...
			
            foreach ($EntryA as $Entry)
            {
				
                $Horse = "";
                $Jockey = "";
                $Trainer = "";
                $CL = "";
                $ML = "";
                $Equip = "";
                $Med = "";
                $Post = "";
                $ClaimPrize = "";
				
				
                $EntryProperties = explode(';',$Entry);
                foreach ($EntryProperties as $Property)
                {
                    if (strstr($Property,"NAME=")) $Horse = str_replace("NAME=","",$Property);
                    if (strstr($Property,"JOCKEY=")) $Jockey = str_replace("JOCKEY=","",$Property);
					if (strstr($Property,"TRAIN=")) $Trainer = str_replace("TRAIN=","",$Property);
					if (strstr($Property,"POST=")) $Post = str_replace("POST=","",$Property);
					if (strstr($Property,"EQUIP=")) $Equip = str_replace("EQUIP=","",$Property);
					if (strstr($Property,"CLAIM=")) $ClaimPrize = str_replace("CLAIM=","",$Property);
					if (strstr($Property,"MED=")) $Med = str_replace("MED=","",$Property);
					
                }

                $EntryNumber = $EntryProperties[4];

                //If the post number is blank, use the entry number.
                if ($Post == "") $Post = $EntryNumber;
				
				$CLTemp = explode(';',$OddsResponse[0]);
                if (isset($CLTemp[7]) && $CLTemp[7] == "FINAL")//ML = SQL QUERY (Don't have SQL access so I can't return this value.
                {
				    $CL = $CLArray[$EntryCounter];
				}
                else 
				{
					$ML = $CLArray[$EntryCounter];
				}

                $EntryCounter++;

                $Wt = ""; //I don't see this in the data we get back and it's not being populated on the site anywhere that I've found. 

                $RaceEntries[]=array($EntryNumber, $Horse, $Jockey, $Trainer, $ML, $CL, $Equip, $Med, $Wt, $this->GetMoney($ClaimPrize), $Post);

				
                if (count($EntryB) > 0)
                {
                    foreach ($EntryB as $Item)
                    {
                        $ItemProperties = explode(';',$Item);
                        if ($ItemProperties[4] == $EntryNumber)
                        {
                            $RaceEntries[]=array(str_replace("DISPLAY=","",$ItemProperties[5]), $Horse, $Jockey, $Trainer, $ML, $CL, $Equip, $Med, $Wt, $this->GetMoney($ClaimPrize), $Post);
                        }
                    }
                }
            }
        }

        return $RaceEntries;
    }
	
	function GetResult($Id, $Race)
    {
		$File = "RaceResult_".$Id."_".$Race;
		$resultdata = array();
		$Success1 = false;
		$ranklist=array();
		if(!$this->CachedStatic($File))
		{
		//echo "writing file";
        	$Command1 = "GET RESULT ".$Id." ".$Race;
			$EntryResponse = $this->SendCommand($Command1);
			$Success1 = $EntryResponse;
			//print_r($EntryResponse);
			if($Success1)
			{
				$this->WriteCache($File,$EntryResponse);
			}
			else
			{
				//echo "returning false";
				return false; // server updating around midnight, or server connection timed out
			}
			
		}
		else
		{
		//echo "read file";
			$EntryResponse = $this->ReadCache($File);
			$Success1 = true;
		
		}
		
		$Result=explode(';',$EntryResponse);
			//print_r($Result);
		if (array_key_exists(5, $Result)) {$ranklist = explode("/",$Result[5]); }
		return $ranklist;
		

	}
	
	
	function GetCachedContest($Id, $Race)
	{
		$Command1 = "LIST CONTESTANTS ".$Id." ".$Race;
			$EntryResponse = $this->SendCommand($Command1);
			//$File="RaceEntry_".$Id."_".$Race;
					//$EntryResponse = $this->ReadCache($File);
		$Entry=explode("\n",$EntryResponse);
		//$EntryList=explode(";",$Entry[2]);
		$contestentname=array();
		//echo "<pre>";
		foreach($Entry as $entry)
		{
			if(strlen(trim($entry))  > 0)
			{
			$EntryA=explode(";",$entry);
				//print_r($EntryA);
					$contestentname[$EntryA[3]]=$EntryA[5];
				
			}
		}
		
		return $contestentname;
	}
	
	function GetPrice($Id, $Race)
	{
			
		$File = "RacePrice_".$Id."_".$Race;
		$resultdata = array();
		$Success1 = false;
		
		if(!$this->CachedStatic($File))
		{
		
        	$Command1 = "LIST PRICE ".$Id." ".$Race;
			$EntryResponse = $this->SendCommand($Command1);
			$Success1 = $EntryResponse;
			//print_r($EntryResponse);
			if($Success1)
			{
				$this->WriteCache($File,$EntryResponse);
			}
			else
			{
				//echo "returning false";
				return false; // server updating around midnight, or server connection timed out
			}
			
		}
		else
		{
		
			$EntryResponse = $this->ReadCache($File);
			$Success1 = true;
		
		}
			
			
			
			
		$Entry=explode("\n",$EntryResponse);
		//$EntryList=explode(";",$Entry[2]);
		$DATAA=array();
		foreach($Entry as $entry)
		{
			if(strlen(trim($entry))  > 0)
			{
			$EntryA=explode(";",$entry);
				$DATAA[]=$EntryA;
					//$contestentname[$EntryA[3]]=$EntryA[5];
				
			}
		}
		
		return $DATAA;
		
	}
	
	function GetTotal($Id, $Race)
	{
			
		$File = "RaceTotal_".$Id."_".$Race;
		$resultdata = array();
		$Success1 = false;
		
		if(!$this->CachedStatic($File))
		{
		
        	$Command1 = "LIST TOTAL ".$Id." ".$Race;
			$EntryResponse = $this->SendCommand($Command1);
			$Success1 = $EntryResponse;
			//print_r($EntryResponse);
			if($Success1)
			{
				$this->WriteCache($File,$EntryResponse);
			}
			else
			{
				//echo "returning false";
				return false; // server updating around midnight, or server connection timed out
			}
			
		}
		else
		{
		
			$EntryResponse = $this->ReadCache($File);
			$Success1 = true;
		
		}
			
			
			
			
		$Entry=explode("\n",$EntryResponse);
		//$EntryList=explode(";",$Entry[2]);
		$DATAA=array();
		foreach($Entry as $entry)
		{
			if(strlen(trim($entry))  > 0)
			{
			$EntryA=explode(";",$entry);
				$DATAA[]=$EntryA;
					//$contestentname[$EntryA[3]]=$EntryA[5];
				
			}
		}
		
		return $DATAA;
		
	}
	
	function GetMoney($ClaimPrize)
	{
		$number = $ClaimPrize/100;
		$formatted = number_format($number, 2);
		return "$".$formatted;
	
	}
	



	function CloseConnection(){
	
		$sh = $this->sh;
		fclose($sh);
		
	}
	

}


if(isset($_GET["printtest"])){

$DataFeed = new DataFeed;
$DataFeed->GetTracks();


// Closed Items
echo "what";
$ClosedTracks = $DataFeed->OpenTracks;
if(isset($_GET["closed"])){
	$ClosedTracks = $DataFeed->ClosedTracks;
}
foreach($ClosedTracks as $Track){
	echo $Track["TRACK"]." ".$Track["RACE"]." ".$Track["MTP"]."<br/>";

}


}



//$DataFeed->CloseConnection();
//$DataFeed->SendCommand("list performances available");


?>