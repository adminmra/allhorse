
<?php

if ( !defined('APP_PATH') ) {
	define( 'APP_PATH' , '/home/ah/usracing.com/htdocs/FeedsV2/' );
}

require_once APP_PATH . 'Generator.php';

class KentuckyDerbyGenerator extends Generator{

	function __construct(){
		parent::__construct();
	}

	public function content_tpl(){

		ob_start();

		return ob_get_clean();
	}

	public function content_odds_2011_tpl(){

		$sql    = "select * from 2011_kentucky_derby order by dat ";
		$result = DB::query($sql);
		ob_start();
		?>

		<table width="100%" height="12" cellpadding="0" cellspacing="0" id="infoEntries" class="table table-condensed table-striped table-bordered" title="Kentucky Derby Odds" summary="2011 Kentucky Derby Odds">
		<tr>
		<th>2011 DERBY CONTENDERS</th>
		<th class="right">ODDS TO WIN</th>

		</tr>
		<?php
		while($data=$result->fetch_object()) {
		$updatedate=explode("-",$data->updatedate);
		$updateas=date("F jS, Y",mktime(0,0,0,$updatedate[1],$updatedate[2],$updatedate[0]));
		?>
		<tr>
		<td><?php	 	 echo $data->dat; ?></td>
		<td class="sortOdds right"><?php	 	 echo $data->odd; ?></td>
		</tr>
		<?php
		}
		?>
		<tr>
		<td colspan="2" class="dateUpdated center"><em>Odds Updated <?php	 	 echo $updateas; ?></em></td>
		</tr>
		</table>
		<?php
		return ob_get_clean();
	}

	public function content_kd_gradedearnings_tpl(){

		// "dbconnect.inc";

		$sql    = "select * from 2011_kentuckyderby_graded_earnings where rank != '' order by id ";
		$result = DB::query( $sql );
		ob_start();
		?>

		<style>
		#col3 #content-box { padding: 0; }
		#infoEntries td, #infoEntries th { padding: 7px;}
		</style>

		<div style="padding: 0 34px;">
		<p>&nbsp;</p>
		<br />
		<h1 class="DarkBlue">2011 Kentucky Derby Graded Earnings List</h1>
		<br />
		</div>

		<table class="table table-condensed table-striped table-bordered" id="infoEntries" title="2011 Kentucky Derby Graded Earnings List" summary="2011 Kentucky Derby Graded Earnings List">
		<tr>
		<th>Rank</th>
		<th>Horse</th>
		<th>Graded Stakes Earnings</th>
		<th>Trainer</th>

		</tr>
		<?php
		while($data=$result->fetch_object() ) {

			$sqlhorse      = " select * from page_url where pagename = '".str_replace(" ","_",trim($data->horse))."' and page = 'horse' ";
			$resulthorse   = DB::query($sqlhorse);
			$numhorse      = $resulthorse->num_rows;

			$sqltrainer    = " select * from page_url where pagename = '".str_replace(" ","_",trim($data->trainer))."' and page = 'trainer' ";
			$resulttrainer = DB::query($sqltrainer);
			$numtrainer    = ( isset( $resulttrainer->num_rows ) ) ? $resulttrainer->num_rows : 0 ;

			$updatedate    = explode("-",$data->updatedate);
			$updateas      = date("F jS, Y",mktime(0,0,0,$updatedate[1],$updatedate[2],$updatedate[0]));
		?>
		<tr>
		<td class="left" ><?php	 	 echo $data->rank; ?></td>
		<td class="left" ><?php	 	 if($numhorse > 0){echo '<a href="/horse?name='.str_replace(" ","_",trim($data->horse)).'">'.$data->horse.'</a>';}else{echo  $data->horse;}?></td>
		<td class="left" ><?php	 	 echo $data->earnings; ?></td>
		<td class="left" ><?php	 	 if($numtrainer > 0){echo "<a href='/trainer?name=".str_replace(" ","_",trim($data->trainer))."'>$data->trainer</a>";}else{echo  $data->trainer;}?></td>
		</tr>
		<?php
		}
		?>
		</table>
		<br>
		<?php
		/*
		$sql = "select * from 2011_kentuckyderby_graded_earnings where rank = '' order by id ";
		$result = mysql_query_w($sql);

		?>
		<div style="padding: 0 34px;">
		<p>&nbsp;</p>
		<br />
		<h1 class="DarkBlue">Out of Kentucky Derby Consideration</h1>
		<br />
		</div>

		<table id="infoEntries">
		<tr>
		<tr>
		<th>Rank</th>
		 <th>Last week</th>
		<th>Horse</th>
		<th>Graded Stakes Earnings</th>
		<th>Owner(s)</th>
		<th>Trainer</th>
		<?php
		while($data=mysql_fetch_object($result))
		{

			  $sqlhorse = " select * from page_url where pagename = '".str_replace(" ","_",trim($data->horse))."' and page = 'horse' ";
			  $resulthorse=mysql_query_w($sqlhorse);
			  $numhorse=mysql_num_rows($resulthorse);

			  $sqltrainer = " select * from page_url where pagename = '".str_replace(" ","_",trim($data->trainer))."' and page = 'trainer' ";
			  $resulttrainer=mysql_query_w($sqltrainer);
			  $numtrainer=mysql_num_rows($resulttrainer);

		$updatedate=explode("-",$data->updatedate);
		$updateas=date("F jS, Y",mktime(0,0,0,$updatedate[1],$updatedate[2],$updatedate[0]));
		?>
		<tr>
		<td class="left" ><?php	 	 echo $data->rank; ?></td>
		 <td><?php	 	 echo $data->lastweek; ?></td>
		<td class="left" ><?php	 	 if($numhorse > 0){echo '<a href="/horse?name='.str_replace(" ","_",trim($data->horse)).'">'.$data->horse.'</a>';}else{echo  $data->horse;}?></td>
		<td class="left" ><?php	 	 echo $data->earnings; ?></td>
		<td class="left" ><?php	 	 echo $data->owner; ?></td>
		<td class="left" ><?php	 	 if($numtrainer > 0){echo "<a href='/trainer?name=".str_replace(" ","_",trim($data->trainer))."'>$data->trainer</a>";}else{echo  $data->trainer;}?></td>


		</tr>
		<?php
		}
		?>

		</tr>
		<tr style="background:#ffffff;">
		<td class="left" colspan="4"></td>
		<td class="sortOdds right"><em style="color:#808080;">Last Updated <?php	 	 echo $updateas; ?></em></td>
		</tr>

		</table>
		<?php	 	 */


		return ob_get_clean();
	}

	public static function run() {
		try {

			$instance = new self;

			/*
			$html = $instance->content_odds_2011_tpl();
			$instance->as_file_save_data( $html , 'ahr_kd_odds_2011.tpl' );
			 */

			$html = $instance->content_kd_gradedearnings_tpl();
			$instance->as_file_save_data( $html , 'ahr_block-kd-gradedearnings.tpl' );

		} catch ( Exception $e ) {
			print( $e->getMessage() );
		}
	}
}

KentuckyDerbyGenerator::run();

?>