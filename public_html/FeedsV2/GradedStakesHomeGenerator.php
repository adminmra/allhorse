
<?php

if ( !defined('APP_PATH') ) {
	define( 'APP_PATH' , __DIR__ . DIRECTORY_SEPARATOR );
}

require_once APP_PATH . 'Generator.php';

class GradedStakesHomeGenerator extends Generator{

	function __construct(){
		parent::__construct();
	}

	public function content_tpl(){

		$template_path = '/themes/';
		$zebra         = TRUE; $idx = 0;
		$output        = "";
		//$sqlgradedhome = DB::query( "SELECT * FROM graded_schedule where racedate >= '".date('Y-m-d')."' Group BY racedate limit 0, 6");
		$sqlgradedhome = DB::query( "SELECT * FROM graded_schedule where racedate >= '".date('Y-m-d')."' Group BY racedate limit 10");
		$idx = 0;
		while($rows= $sqlgradedhome->fetch_object()){
			$Date=explode("-",$rows->racedate);
			//$added_on = date('M d',mktime(0,0,0,$Date[1],$Date[2],$Date[0]));
			$added_on_month = date('M',mktime(0,0,0,$Date[1],$Date[2],$Date[0]));
			$added_on_day   = date('d',mktime(0,0,0,$Date[1],$Date[2],$Date[0]));
			//$sqlgradedhomeracedate= mysql_query_w("SELECT * FROM graded_schedule where racedate = '".$rows->racedate."' ORDER BY track ASC, purse DESC");
			$sqlgradedhomeracedate= DB::query("SELECT *, CONVERT(purse REGEXP '[[:digit:]]+',UNSIGNED INTEGER) AS pursenum FROM graded_schedule where racedate = '".$rows->racedate."' ORDER BY track ASC, pursenum DESC");

			while($rowsdate = $sqlgradedhomeracedate->fetch_object() ){
				$output .= '<div class="item" id="grade-stake-'. $idx .'" itemscope="" itemtype="http://schema.org/Event">';
				$output .= '<div class="dateLarge">'. '<div class="month">'. $this->wrapTag($added_on_month) .'</div>' . '<div class="day">'. $this->wrapTag($added_on_day) . '</div>' . '</div>' ."\n";
				$output .= '<div class="details">' ."\n";
				$output .= '<time style="display:none" itemprop="startDate" content="'.$rows->racedate.'">'.$rows->racedate.'</time>';
				$linkForRacename  = $this->getRaceName($rowsdate->racename);
				$linkForTrackname = $this->getTrackName($rowsdate->track);
				$campoRaceName    = $this->chckLink($linkForRacename, '', '', $rowsdate->racename, 'name');
				$campoTrackName   = $this->chckLink($linkForTrackname, '', '', $rowsdate->track, 'location');

				$output .= '
				<div class="race">
				  <span>RACE:</span>
				  <span style="font-weight:400" itemprop="name">' . $campoRaceName . '</span>
				</div>' .
				'<div class="track">
				  <span>TRACK:</span>'. '<span itemprop="location" itemscope itemtype="http://schema.org/Place"><span style="font-weight:400" itemprop="name">'.$campoTrackName . '</span></span>' .
				'</div>' .
				'<div class="purse">
				  <span>PURSE:</span>'.
				  $rowsdate->purse .
				'</div>' .
				'<div class="info">
				  <span>GRADE / AGE:</span>' .
				  '<span style="font-weight:400" itemprop="description">' . $rowsdate->grade.' / '.$rowsdate->age.'</span>'.
				'</div>'."\n";

				$idx++;
				$output .= '</div>'. "\n"; //details
				$output .= "</div>";  //item

				if($idx >= 9)break(2);
			}
			//$class = ($zebra)? 'stakes-blue' : 'stakes-yellow';
			//$zebra = !$zebra;
			//print '<div class="item" id="grade-stake-'. $idx .'">'."\n". $output .'</div>';
			$idx++;
		}

		ob_start();
		print $output;
		return ob_get_clean();
	}


	public static function run() {
		try {
			$instance = new self;
			$html = $instance->content_tpl();
			$instance->as_file_save_data( $html , ALLHORSEPATH.'public_html/generated/graded_stakes/horse_races_to_watch_schema.php', TRUE ); //TRUE because the path is an absolute path
		} catch ( Exception $e ) {
			print( $e->getMessage() );
		}
	}

	private function getTrackName(&$track){
		$track            = stripslashes($track);
		$tracknameCleaned = strtolower($track);
		$tracknameCleaned = trim(preg_replace("/[^ \w]+/", "", $tracknameCleaned));
		$linkForTrackname = '';
		if($tracknameCleaned!==''){
			$tmp1 = str_replace(" ", "-", $tracknameCleaned);
			$tmp2 = SMARTYPATH."/templates/content/racetrack/".$tmp1.".tpl";
			$tmp3 = SMARTYPATH."/templates/content/racetrack/".$tmp1."-race-course.tpl";
			if(file_exists($tmp2))
				$linkForTrackname = "https://www.usracing.com/".str_replace(" ", "-", $tmp1);
			else if(file_exists($tmp3))
				$linkForTrackname = "https://www.usracing.com/".str_replace(" ", "-", $tmp1)."-race-course";
		}
		return $linkForTrackname;
	}

	private function getRaceName(&$stake){
		$stake           = stripslashes($stake);
		$racenameCleaned = strtolower($stake);
		$racenameCleaned = trim(preg_replace("/[^ \w]+/", "", $racenameCleaned));
		$linkForRacename = '';
		if($racenameCleaned!==''){
			$tmp1 = str_replace(" ", "-", $racenameCleaned);
			$tmp2 = SMARTYPATH."/templates/content/stake/".$tmp1.".tpl";
			$tmp3 = SMARTYPATH."/templates/content/stake/".$tmp1."-stakes.tpl";
			if(file_exists($tmp2))
				$linkForRacename = "https://www.usracing.com/".str_replace(" ", "-", $tmp1);
			else if(file_exists($tmp3))
				$linkForRacename = "https://www.usracing.com/".str_replace(" ", "-", $tmp1)."-stakes";
		}
		return $linkForRacename;
	}

	private function chckLink($link, $openTag, $closeTag, $content, $scheme="none") {
		//<a itemprop="location" itemscope itemtype="http://schema.org/Place" href="' . $linkForTrackname . '" itemprop="url">
		if ($link !== '') {
			$ret = '';
			switch($scheme){
				case "location":
					$ret = $openTag . '<a itemprop="location" itemscope itemtype="http://schema.org/Place" href="' . $link . '" itemprop="url">' . $content . '</a>' . $closeTag;
					break;
				case "name":
					$ret = $openTag . '<a itemprop="url" href="' . $link . '">' . $content . '</a>' . $closeTag;
					break;
				default:
					$ret = $openTag . '<a href="' . $link . '">' . $content . '</a>' . $closeTag;
			}
			return $ret;
		} else {
			return $openTag . $content . $closeTag;
		}
	}

	private function wrapTag($cad, $tagA='<span>', $tagB='</span>'){
		$ret = '';
		$lim = strlen($cad);
		for($i=0; $i<$lim; $i++){
			$ret .= $tagA . $cad[$i] . $tagB;
		}
		return $ret;
	}

}
GradedStakesHomeGenerator::run();
?>
