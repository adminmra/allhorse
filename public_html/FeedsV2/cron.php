<?php
ini_set( 'display_errors' , 1 );
ini_set( 'log_errors'     , 1 );
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting( E_ALL );

define( 'APP_PATH' , __DIR__ . DIRECTORY_SEPARATOR ); // /home/ah/allhorse/public_html/FeedsV2

/*|
  | This file is responsible for invoking the charge of template generator files.
  |
  | The files that handle scrapping inherit global settings Generator.php
  |
  |                   ---------- EXAMPLE ---------
  |
  |     class AhrBlockGradedScheduleGenerator extends Generator {  [CODE_GENERATOR]  }
*/


######   DEFINE PATHS TO TEMPLATE GENERATORS  ( comment line for disable generator )#########

require_once APP_PATH . 'AhrBlockGradedScheduleGenerator.php';
//'BetlmScraper'                  => 'horse_racing_schedule/BetlmScraper.php',
require_once APP_PATH . 'AhrHorseRacingScheduleGenerator.php';
//'NtraScraper'                   => 'horse_racing_schedule/NtraScraper.php',// ver en detalle

//'BreedersOddsScraper'           => 'breeders/BreedersOddsScraper.php',
//'BreedersDistaffOddsScraper'    => 'breeders/BreedersDistaffOddsScraper.php',
require_once APP_PATH . 'BreedersChallengeGenerator.php';
//'Odds926Scraper'                => 'kd/Odds926Scraper.php',

//'OddsApScraper'                 => 'misc/OddsApScraper.php',
//'LiveInRunningScraper'          => 'misc/LiveInRunningScraper.php',
require_once APP_PATH . 'AhrBlockTopLeadersGenerator.php';
require_once APP_PATH . 'AhrBlockRacingNewsGenerator.php';
//'GeneraXmlScraper'              => 'newsxml/GeneraXmlScraper.php'
require_once APP_PATH . 'GradedStakesHomeGenerator.php';
require_once APP_PATH . 'AhrRacingResultsGenerator.php';
require_once APP_PATH . 'AhrTripleCrownGenerator.php';
require_once APP_PATH . 'KentuckyDerbyGenerator.php';

?>
