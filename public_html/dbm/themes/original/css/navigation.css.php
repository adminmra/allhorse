<?php	 		 	
/* vim: set expandtab sw=4 ts=4 sts=4: */
/**
 * Navigation styles for the original theme
 *
 * @package    PhpMyAdmin-theme
 * @subpackage Original
 */

// unplanned execution path
if (! defined('PMA_MINIMUM_COMMON') && ! defined('TESTSUITE')) {
    exit();
}
?>

/******************************************************************************/
/* Navigation */

#pma_navigation {
    background: <?php	 	
    color: <?php	 	
    width: <?php	 	
    overflow: hidden;
    position: fixed;
    top: 0;
    <?php	 	
    height: 100%;
    border-<?php	 	
    z-index: 800;
}

#pma_navigation_content {
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    <?php	 	
    z-index: 0;
    padding-bottom: 1em;
}

#pma_navigation ul {
    margin: 0;
}

#pma_navigation form {
    margin: 0;
    padding: 0;
    display: inline;
}

#pma_navigation select#select_server,
#pma_navigation select#lightm_db {
    width: 100%;
}

/******************************************************************************/
/* specific elements */

#pma_navigation div.pageselector {
    text-align: center;
    margin: 0 0 0;
    margin-<?php	 	
    border-<?php	 	
}

#pma_navigation div#pmalogo {
    <?php	 	 ?>
    background-color: <?php	 	
    padding: .3em;
}

#pma_navigation div#recentTableList {
    text-align: center;
    margin-bottom: 0.5em;
}

#pma_navigation #pmalogo,
#pma_navigation #serverChoice,
#pma_navigation #leftframelinks,
#pma_navigation #recentTableList,
#pma_navigation #databaseList,
#pma_navigation div.pageselector.dbselector {
    text-align:         center;
    margin-bottom:      0.3em;
    padding-bottom:     0.3em;
    border: 0;
}

#pma_navigation #recentTableList select,
#pma_navigation #serverChoice select
 {
    width: 80%;
}

#pma_navigation #recentTableList {
    margin-bottom: 0;
    padding-bottom: 0;
}

#pma_navigation_content > img.throbber {
    display: block;
    margin: 0 auto;
}

/* Navigation tree*/
#pma_navigation_tree {
    margin: 0;
    margin-<?php	 	
    color: #444;
    height: 74%;
    position: relative;
}
#pma_navigation_tree_content {
    width: 100%;
    overflow: hidden;
    overflow-y: auto;
    position: absolute;
    height: 100%;
}
#pma_navigation_tree a {
    color: <?php	 	
}
#pma_navigation_tree a:hover {
    text-decoration: underline;
}
#pma_navigation_tree li.activePointer {
    color: <?php	 	
    background-color: <?php	 	
}
#pma_navigation_tree li.selected {
    color: <?php	 	
    background-color: <?php	 	
}
#pma_navigation_tree ul {
    clear: both;
    padding: 0;
    list-style-type: none;
    margin: 0;
}
#pma_navigation_tree ul ul {
    position: relative;
}
#pma_navigation_tree li {
    white-space: nowrap;
    clear: both;
    min-height: 16px;
}
#pma_navigation_tree img {
    margin: 0;
}
#pma_navigation_tree div.block {
    position: relative;
    width:1.5em;
    height:1.5em;
    min-width: 16px;
    min-height: 16px;
    float: <?php	 	
}
#pma_navigation_tree div.block i,
#pma_navigation_tree div.block b {
    width: 1.5em;
    height: 1.5em;
    min-width: 16px;
    min-height: 8px;
    position: absolute;
    bottom: 0.7em;
    <?php	 	
    z-index: 0;
}
#pma_navigation_tree div.block i {
    border-<?php	 	
    border-bottom: 1px solid #666;
}
#pma_navigation_tree div.block i.first { /* Removes top segment */
    border-<?php	 	
}
#pma_navigation_tree div.block b { /* Bottom segment for the tree element connections */
    display: block;
    height: 0.75em;
    bottom: 0;
    <?php	 	
    border-<?php	 	
}
#pma_navigation_tree div.block a,
#pma_navigation_tree div.block u {
    position: absolute;
    <?php	 	
    top: 50%;
    z-index: 10;
}
#pma_navigation_tree div.block img {
    position: relative;
    top: -0.6em;
    <?php	 	
    margin-<?php	 	
}
#pma_navigation_tree li.last > ul {
    background: none;
}
#pma_navigation_tree li > a, #pma_navigation_tree li > i {
    line-height: 1.5em;
    height: 1.5em;
    padding-<?php	 	
}
#pma_navigation_tree .list_container {
    border-<?php	 	
    margin-<?php	 	
    padding-<?php	 	
}
#pma_navigation_tree .last > .list_container {
    border-<?php	 	
}

/* Fast filter */
li.fast_filter {
    padding-<?php	 	
    margin-<?php	 	
    padding-<?php	 	
    border-<?php	 	
}
li.fast_filter input {
    padding-<?php	 	
    width: 100%;
}
li.fast_filter span {
    position: relative;
    <?php	 	
    padding: 0.2em;
    cursor: pointer;
    font-weight: bold;
    color: #800;
}
li.fast_filter.db_fast_filter {
    border: 0;
}

/* Resize handler */
#pma_navigation_resizer {
    width: 3px;
    height: 100%;
    background-color: #aaa;
    cursor: col-resize;
    position: fixed;
    top: 0;
    <?php	 	
    z-index: 801;
}
#pma_navigation_collapser {
    width: 20px;
    height: 22px;
    line-height: 22px;
    background: #eee;
    color: #555;
    font-weight: bold;
    position: fixed;
    top: 0;
    <?php	 	
    text-align: center;
    cursor: pointer;
    z-index: 800;
    text-shadow: 0px 1px 0px #fff;
    filter: dropshadow(color=#fff, offx=0, offy=1);
    border: 1px solid #888;
}
