<?php	 		 	
/* vim: set expandtab sw=4 ts=4 sts=4: */
/**
 *
 * @package PhpMyAdmin
 */

/**
 *
 */
require_once 'libraries/common.inc.php';

$response = PMA_Response::getInstance();
$header   = $response->getHeader();
$header->enablePrintView();

PMA_Util::checkParameters(array('db'));

/**
 * Defines the url to return to in case of error in a sql statement
 */
$err_url = 'db_sql.php?' . PMA_generate_common_url($db);

/**
 * Settings for relations stuff
 */
$cfgRelation = PMA_getRelationsParam();

/**
 * Gets the list of the table in the current db and informations about these
 * tables if possible
 *
 * @todo merge this speedup _optionaly_ into PMA_DBI_get_tables_full()
 *
// speedup view on locked tables
// Special speedup for newer MySQL Versions (in 4.0 format changed)
if ($cfg['SkipLockedTables'] == true) {
    $result = PMA_DBI_query('SHOW OPEN TABLES FROM ' . PMA_Util::backquote($db) . ';');
    // Blending out tables in use
    if ($result != false && PMA_DBI_num_rows($result) > 0) {
        while ($tmp = PMA_DBI_fetch_row($result)) {
            // if in use memorize tablename
            if (preg_match('@in_use=[1-9]+@i', $tmp[0])) {
                $sot_cache[$tmp[0]] = true;
            }
        }
        PMA_DBI_free_result($result);

        if (isset($sot_cache)) {
            $result      = PMA_DBI_query('SHOW TABLES FROM ' . PMA_Util::backquote($db) . ';', null, PMA_DBI_QUERY_STORE);
            if ($result != false && PMA_DBI_num_rows($result) > 0) {
                while ($tmp = PMA_DBI_fetch_row($result)) {
                    if (! isset($sot_cache[$tmp[0]])) {
                        $sts_result  = PMA_DBI_query('SHOW TABLE STATUS FROM ' . PMA_Util::backquote($db) . ' LIKE \'' . sqlAddSlashes($tmp[0], true) . '\';');
                        $sts_tmp     = PMA_DBI_fetch_assoc($sts_result);
                        $tables[]    = $sts_tmp;
                    } else { // table in use
                        $tables[]    = array('Name' => $tmp[0]);
                    }
                }
                PMA_DBI_free_result($result);
                $sot_ready = true;
            }
        }
        unset($tmp, $result);
    }
}

if (! isset($sot_ready)) {
    $result      = PMA_DBI_query('SHOW TABLE STATUS FROM ' . PMA_Util::backquote($db) . ';');
    if (PMA_DBI_num_rows($result) > 0) {
        while ($sts_tmp = PMA_DBI_fetch_assoc($result)) {
            $tables[] = $sts_tmp;
        }
        PMA_DBI_free_result($result);
        unset($res);
    }
}
 */

/**
 * If there is at least one table, displays the printer friendly view, else
 * an error message
 */
$tables = PMA_DBI_get_tables_full($db);
$num_tables = count($tables);

echo '<br />';

// 1. No table
if ($num_tables == 0) {
    echo __('No tables found in database.');
} else {
// 2. Shows table information
    ?>
<table>
<thead>
<tr>
    <th><?php	 	 ?></th>
    <th><?php	 	 ?></th>
    <th><?php	 	 ?></th>
    <?php	 	
    if ($cfg['ShowStats']) {
        echo '<th>' . __('Size') . '</th>';
    }
    ?>
    <th><?php	 	 ?></th>
</tr>
</thead>
<tbody>
    <?php	 	
    $sum_entries = $sum_size = 0;
    $odd_row = true;
    foreach ($tables as $sts_data) {
        if (PMA_Table::isMerge($db, $sts_data['TABLE_NAME'])
            || strtoupper($sts_data['ENGINE']) == 'FEDERATED'
        ) {
            $merged_size = true;
        } else {
            $merged_size = false;
        }
        $sum_entries += $sts_data['TABLE_ROWS'];
        ?>
<tr class="<?php	 	 ?>">
    <th>
        <?php	 	 ?>
    </th>
        <?php	 	

        if (isset($sts_data['TABLE_ROWS'])) {
            ?>
    <td class="right">
            <?php	 	
            if ($merged_size) {
                echo '<i>' . PMA_Util::formatNumber($sts_data['TABLE_ROWS'], 0) . '</i>' . "\n";
            } else {
                echo PMA_Util::formatNumber($sts_data['TABLE_ROWS'], 0) . "\n";
            }
            ?>
    </td>
    <td class="nowrap">
        <?php	 	 ?>
    </td>
            <?php	 	
            if ($cfg['ShowStats']) {
                $tblsize =  $sts_data['Data_length'] + $sts_data['Index_length'];
                $sum_size += $tblsize;
                list($formated_size, $unit)
                    =  PMA_Util::formatByteDown($tblsize, 3, 1);
                ?>
    <td class="right nowrap">
        <?php	 	 ?>
    </td>
                <?php	 	
            } // end if
        } else {
            ?>
    <td colspan="3" class="center">
        <?php	 	 ?>
    </td>
            <?php	 	
        }
        ?>
    <td>
        <?php	 	
        if (! empty($sts_data['Comment'])) {
            echo htmlspecialchars($sts_data['Comment']);
            $needs_break = '<br />';
        } else {
            $needs_break = '';
        }

        if (! empty($sts_data['Create_time'])
            || ! empty($sts_data['Update_time'])
            || ! empty($sts_data['Check_time'])
        ) {
            echo $needs_break;
            ?>
            <table width="100%">
            <?php	 	

            if (! empty($sts_data['Create_time'])) {
                ?>
                <tr>
                    <td class="right"><?php	 	 ?></td>
                    <td class="right"><?php	 	 ?></td>
                </tr>
                <?php	 	
            }

            if (! empty($sts_data['Update_time'])) {
                ?>
                <tr>
                    <td class="right"><?php	 	 ?></td>
                    <td class="right"><?php	 	 ?></td>
                </tr>
                <?php	 	
            }

            if (! empty($sts_data['Check_time'])) {
                ?>
                <tr>
                    <td class="right"><?php	 	 ?></td>
                    <td class="right"><?php	 	 ?></td>
                </tr>
                <?php	 	
            }
            ?>
            </table>
            <?php	 	
        }
        ?>
    </td>
</tr>
        <?php	 	
    }
    ?>
<tr>
    <th class="center">
        <?php	 	 ?>
    </th>
    <th class="right nowrap">
        <?php	 	 ?>
    </th>
    <th class="center">
        --
    </th>
    <?php	 	
    if ($cfg['ShowStats']) {
        list($sum_formated, $unit)
            = PMA_Util::formatByteDown($sum_size, 3, 1);
        ?>
    <th class="right nowrap">
        <?php	 	 ?>
    </th>
        <?php	 	
    }
    ?>
    <th></th>
</tr>
</tbody>
</table>
    <?php	 	
}

/**
 * Displays the footer
 */
echo PMA_Util::getButton();

echo "<div id='PMA_disable_floating_menubar'></div>\n";
?>
