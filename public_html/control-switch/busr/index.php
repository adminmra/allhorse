<?php
/********** BUSR ONLY 
To create new variable . We need to copy those codes and need to set the correesponding values 

//copy start
$SwitchVariable['variableName'][]= 'SetVariableName';  // Its the variable name of this switch 
$SwitchVariable['startTIME'][]='SetStartTime'; // Its the variable start time. Times are in ET. Its in 24 Hours format . 
$SwitchVariable['endTIME'][]='SetEndTime';  // Its the variable end time. Times are in ET. Its in 24 Hours format . 
//copy end

******/
$SwitchVariable['variableName'][]= 'Default';
$SwitchVariable['startTIME'][]='2019-03-01 00:00'; 
$SwitchVariable['endTIME'][]='2019-12-31 23:59';

$SwitchVariable['variableName'][]= 'BC_Switch';
$SwitchVariable['startTIME'][]='2018-03-01 00:00'; 
$SwitchVariable['endTIME'][]='2018-12-31 23:59';

$SwitchVariable['variableName'][]= 'PG_Switch';
$SwitchVariable['startTIME'][]='2018-03-01 00:00'; 
$SwitchVariable['endTIME'][]='2018-12-31 23:59';

$SwitchVariable['variableName'][]= 'DB_Switch';
$SwitchVariable['startTIME'][]='2019-03-01 00:00'; 
$SwitchVariable['endTIME'][]='2019-03-30 23:59';

$SwitchVariable['variableName'][]= 'DB_Switch_2PM';
$SwitchVariable['startTIME'][]='2019-03-01 00:00'; 
$SwitchVariable['endTIME'][]='2019-03-30 12:50';

$SwitchVariable['variableName'][]= 'KD_KOAKS_Switch';
$SwitchVariable['startTIME'][]='2019-04-01 12:00'; 
$SwitchVariable['endTIME'][]='2019-05-03 18:15';

/*
$SwitchVariable['variableName'][]= 'KD_Switch';
$SwitchVariable['startTIME'][]='2019-04-01 12:00'; 
$SwitchVariable['endTIME'][]='2019-05-04 18:52';

$SwitchVariable['variableName'][]= 'PS_Switch';
$SwitchVariable['startTIME'][]='2019-05-04 18:53'; 
$SwitchVariable['endTIME'][]='2019-05-19 23:59';
*/

$SwitchVariable['variableName'][]= 'BM_Switch';
$SwitchVariable['startTIME'][]='2018-03-01 00:00'; 
$SwitchVariable['endTIME'][]='2018-12-31 23:59';


/******* Clubhouse Section Switches***********/

$SwitchVariable['variableName'][]= 'KD_Clubhouse_Switch';
$SwitchVariable['startTIME'][]='2019-04-24 00:00'; 
$SwitchVariable['endTIME'][]='2019-04-28 13:59';

$SwitchVariable['variableName'][]= 'KD_Clubhouse_Switch_FAQ';
$SwitchVariable['startTIME'][]='2019-04-28 14:00'; 
$SwitchVariable['endTIME'][]='2019-05-04 18:55';

$SwitchVariable['variableName'][]= 'KD_Clubhouse_Switch_SAT';
$SwitchVariable['startTIME'][]='2019-05-04 18:56'; 
$SwitchVariable['endTIME'][]='2019-05-04 23:59';

$SwitchVariable['variableName'][]= 'KD_Clubhouse_Switch_SUN';
$SwitchVariable['startTIME'][]='2019-05-05 00:00'; 
$SwitchVariable['endTIME'][]='2019-05-07 13:59';

$SwitchVariable['variableName'][]= 'KD_Clubhouse_Switch_MON';
$SwitchVariable['startTIME'][]='2019-05-07 14:00'; 
$SwitchVariable['endTIME'][]='2019-05-19 23:59';


$SwitchVariable['variableName'][]= 'PR_Clubhouse_Switch';
$SwitchVariable['startTIME'][]='2018-03-01 00:00'; 
$SwitchVariable['endTIME'][]='2018-12-31 23:59';

$SwitchVariable['variableName'][]= 'BE_Clubhouse_Switch';
$SwitchVariable['startTIME'][]='2018-03-01 00:00'; 
$SwitchVariable['endTIME'][]='2018-12-31 23:59';

$SwitchVariable['variableName'][]= 'BR_Clubhouse_Switch';
$SwitchVariable['startTIME'][]='2018-03-01 00:00'; 
$SwitchVariable['endTIME'][]='2018-12-31 23:59';

$SwitchVariable['variableName'][]= 'PE_Clubhouse_Switch';
$SwitchVariable['startTIME'][]='2018-03-01 00:00'; 
$SwitchVariable['endTIME'][]='2018-12-31 23:59';


$SwitchVariable['variableName'][]= 'DB_Clubhouse_Switch';
$SwitchVariable['startTIME'][]='2019-03-01 00:00'; 
$SwitchVariable['endTIME'][]='2019-03-30 12:50';

$SwitchVariable['variableName'][]= 'KD_Switch_Phase_2-5';
$SwitchVariable['startTIME'][]='2019-04-22 10:00'; 
$SwitchVariable['endTIME'][]='2019-05-04 18:55';

/********** Kentucky Derby and Oaks *****************/

$SwitchVariable['variableName'][]= 'KD_KOAKS_Switch'; 	//Removes KO Betting content day before derby
$SwitchVariable['startTIME'][]='2019-04-01 00:00'; 		//April 1st every year.
$SwitchVariable['endTIME'][]='2019-05-03 20:00'; 		// TURN *OFF*   AT END OF OAKS ********************************** OAKS

$SwitchVariable['variableName'][]= 'KD_Switch';  		//Day after DWC to end of KD Race.
$SwitchVariable['startTIME'][]='2019-04-01 00:00'; 		//April 1st every year.
$SwitchVariable['endTIME'][]='2019-05-04 18:55';  		// TURN *OFF*   AT END OF DERBY ********************************* DERBY

$SwitchVariable['variableName'][]= 'KD_PREP_Switch';  	// What is this for? - TF
$SwitchVariable['startTIME'][]='2019-04-01 00:00'; 
$SwitchVariable['endTIME'][]='2019-04-13 23:59';

$SwitchVariable['variableName'][]= 'KD_Switch_Phase_1'; // Day after DWC until 2 weeks out
$SwitchVariable['startTIME'][]='2019-04-01 00:00'; 
$SwitchVariable['endTIME'][]='2019-04-22 23:59';

$SwitchVariable['variableName'][]= 'KD_Switch_Phase_2'; // 2 Weeks out until 1 week out.
$SwitchVariable['startTIME'][]='2019-04-23 00:00'; 
$SwitchVariable['endTIME'][]='2019-04-29 23:59';

$SwitchVariable['variableName'][]= 'KD_Switch_Phase_2_EOR'; // 2 Weeks out until End Of DERBY
$SwitchVariable['startTIME'][]='2019-04-22 10:00'; 
$SwitchVariable['endTIME'][]='2019-05-04 18:55';		// TURN *OFF*   AT END OF DERBY ***************************** DERBY

$SwitchVariable['variableName'][]= 'KD_Switch_Phase_3'; // Week out until day of KO
$SwitchVariable['startTIME'][]='2019-04-30 00:00'; 
$SwitchVariable['endTIME'][]='2019-05-02 23:59';

$SwitchVariable['variableName'][]= 'KD_Switch_Phase_4'; // Kentucky Oaks Short Term Content (index)
$SwitchVariable['startTIME'][]='2019-05-03 00:00'; 		// Starts day of Oaks
$SwitchVariable['endTIME'][]='2019-05-03 20:00'; 		// TURN *OFF*   AT END OF OAKS RACE ***************************** OAKS

$SwitchVariable['variableName'][]= 'KD_Switch_Phase_5'; // End of Kentucky Oaks to End of Derby
$SwitchVariable['startTIME'][]='2019-05-03 20:00'; 		// TURN *ON*    AT END OF OAKS RACE *****************************  OAKS
$SwitchVariable['endTIME'][]='2019-05-04 18:55';		// TURN *OFF*   AT END OF DERBY RACE

$SwitchVariable['variableName'][]= 'KD_Switch_Results'; // Day of Kentucky Derby to End of Belmont
$SwitchVariable['startTIME'][]='2019-05-04 00:00'; 		// Starts Day of Derby 
$SwitchVariable['endTIME'][]='2019-06-15 23:59';		// End 1 week after Belmont


/********** Preakness Stakes *****************/

$SwitchVariable['variableName'][]= 'PS_Switch'; 		// End of KD Race to End of Preakness Race
$SwitchVariable['startTIME'][]='2019-05-04 18:56'; 		// TURN *ON*    AT END OF DERBY RACE ***************************** DERBY
$SwitchVariable['endTIME'][]='2019-05-18 20:00';		// TURN *OFF*   AT END OF PREAKNESS RACE ***************************** PREAKNESS

$SwitchVariable['variableName'][]= 'PS_Switch_Results'; // Day of Preakness Race to Week After Belmont
$SwitchVariable['startTIME'][]='2019-05-18 20:00'; 		// Starts Day of Preakness 
$SwitchVariable['endTIME'][]='2019-06-15 23:59';		// End 1 week after Belmont

/********** Belmont Stakes *****************/

$SwitchVariable['variableName'][]= 'BS_Switch'; 		// End of Preakness Race to End of Belmont Race
$SwitchVariable['startTIME'][]='2019-05-18 20:00'; 		// TURN *ON*    AT END OF PREAKNESS ***************************** PREAKNESS
$SwitchVariable['endTIME'][]='2018-12-31 23:59';	 	// TURN *OFF*    UPDATE AT END OF BELMONT ***************************** BELMONT

$SwitchVariable['variableName'][]= 'BS_Switch_Results'; // End of Preakness Race to End of Belmont Race
$SwitchVariable['startTIME'][]='2018-06-08 00:00'; 		// Starts Day of Belmont 
$SwitchVariable['endTIME'][]='2019-06-15 23:59';	 	// End 1 week after Belmont



///Busr menu 
/*
$SwitchVariable['variableName'][]= 'TC_MENU_Switch';
$SwitchVariable['startTIME'][]='2019-04-01 10:05'; 
$SwitchVariable['endTIME'][]='2019-06-02 23:59';

$SwitchVariable['variableName'][]= 'KD_MENU_Switch';
$SwitchVariable['startTIME'][]='2019-04-01 10:05'; 
$SwitchVariable['endTIME'][]='2019-05-04 18:59';

$SwitchVariable['variableName'][]= 'PS_MENU_Switch';
$SwitchVariable['startTIME'][]='2019-05-04 19:00'; 
$SwitchVariable['endTIME'][]='2019-05-18 18:59';

$SwitchVariable['variableName'][]= 'BC_MENU_Switch';
$SwitchVariable['startTIME'][]='2019-05-18 19:00'; 
$SwitchVariable['endTIME'][]='2019-06-02 23:59';
*/


//Dubai - Salesblurb section
/*$SwitchVariable['variableName'][]= 'ShowDubaiSalesblurb';
$SwitchVariable['startTIME'][]='2019-03-25 00:00'; 
$SwitchVariable['endTIME'][]='2019-03-11 23:59';

//KDSwitch Start
/*$SwitchVariable['variableName'][]= 'KDSwitch'; /* Its the variable name of this switch */
/*$SwitchVariable['startTIME'][]='2018-10-25 08:10'; /* Its the variable start time. Times are in ET. Its in 24 Hours format . */
/*$SwitchVariable['endTIME'][]='2018-05-04 18:53'; /* Its the variable end time. Times are in ET. Its in 24 Hours format . */
//KDSwitch End

//PSSwitch Start
/*$SwitchVariable['variableName'][]= 'PSSwitch'; /* Its the variable name of this switch */
/*$SwitchVariable['startTIME'][]='2018-10-25 08:10'; /* Its the variable start time. Times are in ET. Its in 24 Hours format . */
/*$SwitchVariable['endTIME'][]='2019-03-18 18:53'; /* Its the variable end time. Times are in ET. Its in 24 Hours format . */
//PSSwitch End

//PegasusSwitch Start - Menu Primary Top and Footer - Section 1
/*$SwitchVariable['variableName'][]= 'PegasusSwitch'; /* Its the variable name of this switch */
/*$SwitchVariable['startTIME'][]='2019-01-14 07:00'; 
$SwitchVariable['endTIME'][]='2019-01-27 23.59'; 
//PegasusSwitch End 

//BCSwitch Start - Text in Hero  - Section 2 
$SwitchVariable['variableName'][]= 'BCSwitch'; 
$SwitchVariable['startTIME'][]='2019-01-14 07:00'; 
$SwitchVariable['endTIME'][]='2019-01-27 23.59'; 
//BCSwitch End

//PegasusSwitch Start - Content - Section 3
$SwitchVariable['variableName'][]= 'PegasusContentSwitch'; 
$SwitchVariable['startTIME'][]='2019-01-14 07:00'; 
$SwitchVariable['endTIME'][]='2019-01-27 23.59';  
//PegasusSwitch End 

//PegasusSwitch Start - ContentBlue - Section 4
$SwitchVariable['variableName'][]= 'PegasusContentblueSwitch'; 
$SwitchVariable['startTIME'][]='2019-01-14 07:00'; 
$SwitchVariable['endTIME'][]='2019-01-27 23.59'; 
//PegasusSwitch End 

//PegasusSwitch Start - ContentBlue - Section 4
$SwitchVariable['variableName'][]= 'PegasusJoinSwitch'; 
$SwitchVariable['startTIME'][]='2019-01-14 07:00'; 
$SwitchVariable['endTIME'][]='2019-01-27 23.59'; 
//PegasusSwitch End 

//PegasusSwitch Start - ContentBlue - Section 6
$SwitchVariable['variableName'][]= 'PegasusEventSwitch'; 
$SwitchVariable['startTIME'][]='2019-01-14 07:00'; 
$SwitchVariable['endTIME'][]='2019-01-27 23.59'; 
//PegasusSwitch End 

// Change "Bet NFL" text to "Bet Super Bowl" on BUSR Club House Icon 
$SwitchVariable['variableName'][]= 'SuperBowlSwitch'; 
$SwitchVariable['startTIME'][]='2019-01-21 07:00'; 
$SwitchVariable['endTIME'][]='2019-02-03 23:59'; 
//NFLSwitch End
// Logo Section

// Change "Bet NFL" text to "Bet Super Bowl" on BUSR Club House Icon 
$SwitchVariable['variableName'][]= 'BUSRLogoChristmas'; 
$SwitchVariable['startTIME'][]='2019-01-18 15:05'; 
$SwitchVariable['endTIME'][]='2019-01-18 15:09'; 
//NFLSwitch End
// Change "Bet NFL" text to "Bet Super Bowl" on BUSR Club House Icon 
$SwitchVariable['variableName'][]= 'BUSRLogoNewYear'; 
$SwitchVariable['startTIME'][]='2019-01-18 15:10'; 
$SwitchVariable['endTIME'][]='2019-01-18 15:14'; 
//NFLSwitch End
// Change "Bet NFL" text to "Bet Super Bowl" on BUSR Club House Icon 
$SwitchVariable['variableName'][]= 'BUSRLogoHalloween'; 
$SwitchVariable['startTIME'][]='2019-01-18 15:15'; 
$SwitchVariable['endTIME'][]='2019-01-18 15:19'; 
//NFLSwitch End


// Change "Bet NFL" text to "Bet Super Bowl" on BUSR Club House Icon 
$SwitchVariable['variableName'][]= 'BUSRLogoThanksgiving'; 
$SwitchVariable['startTIME'][]='2019-01-18 15:20'; 
$SwitchVariable['endTIME'][]='2019-01-18 15:25'; 
//NFLSwitch End


// Change "PegasusIcon" text to "Bet Super Bowl" on BUSR Club House Icon 
$SwitchVariable['variableName'][]= 'BUSRCHPegasus'; 
$SwitchVariable['startTIME'][]='2019-01-26 05:20'; 
$SwitchVariable['endTIME'][]='2019-01-26 17:45'; 
//NFLSwitch End*/


?>