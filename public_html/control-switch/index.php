<?php
/**********
To create new variable . We need to copy those codes and need to set the correesponding values 

//copy start
$SwitchVariable['variableName'][]= 'SetVariableName';  // Its the variable name of this switch 
$SwitchVariable['startTIME'][]='SetStartTime'; // Its the variable start time. Times are in ET. Its in 24 Hours format . 
$SwitchVariable['endTIME'][]='SetEndTime';  // Its the variable end time. Times are in ET. Its in 24 Hours format . 
//copy end

******/

$SwitchVariable['variableName'][]= 'Default';
$SwitchVariable['startTIME'][]='2019-03-01 00:00'; 
$SwitchVariable['endTIME'][]='2020-12-31 23:59';	 //I think id put this 10 years in the future, no the end the year! - TF

/********** Breeders Cup  *****************/

include '/home/ah/allhorse/public_html/breeders/control-switch.php';
/** BC Sub Races End **/
$SwitchVariable['variableName'][]= 'PWC_Switch';
$SwitchVariable['startTIME'][]='2020-01-20 00:00:00'; 
$SwitchVariable['endTIME'][]='2020-01-25 17:34:00';

$SwitchVariable['variableName'][]= 'PWC_Switch_SAT';
$SwitchVariable['startTIME'][]='2020-01-15 00:00:00'; 
$SwitchVariable['endTIME'][]='2020-01-25 17:34:00';

$SwitchVariable['variableName'][]= 'PWC_Switch_SUN';
$SwitchVariable['startTIME'][]='2020-01-15 00:00:00'; 
$SwitchVariable['endTIME'][]='2020-01-26 23:59:59';

$SwitchVariable['variableName'][]= 'PWC_Switch_3WEEK';
$SwitchVariable['startTIME'][]='2020-01-03 12:00'; 
$SwitchVariable['endTIME'][]='2020-01-24 23:59:00';

$SwitchVariable['variableName'][]= 'PWC_Switch_1WEEK';
$SwitchVariable['startTIME'][]='2020-01-03 12:00'; 
$SwitchVariable['endTIME'][]='2020-01-20 23:59';

$SwitchVariable['variableName'][]= 'PWC_Switch_FRIDAY';
$SwitchVariable['startTIME'][]='2020-01-24 12:00'; 
$SwitchVariable['endTIME'][]='2020-01-25 23:59';

$SwitchVariable['variableName'][]= 'PWC_Switch_Footer';
$SwitchVariable['startTIME'][]='2019-01-04 12:00'; 
$SwitchVariable['endTIME'][]='2020-01-25 23:59';

$SwitchVariable['variableName'][]= 'PWC_Switch_DAY';
$SwitchVariable['startTIME'][]='2021-01-22 23:59'; 
$SwitchVariable['endTIME'][]='2021-01-23 18:00';

/// Dubai Swiches

$SwitchVariable['variableName'][]= 'DWC_Switch_OffSeason';  	//This needs to be renamed DWC_Switch -TF
$SwitchVariable['startTIME'][]='2020-03-29 00:00'; 
$SwitchVariable['endTIME'][]='2020-12-31 23:59';

$SwitchVariable['variableName'][]= 'DWC_Switch_3WEEK';
$SwitchVariable['startTIME'][]='2020-03-06 12:00'; 
$SwitchVariable['endTIME'][]='2020-03-27 11:59';

$SwitchVariable['variableName'][]= 'DWC_Switch_FRIDAY';
$SwitchVariable['startTIME'][]='2020-03-27 12:00'; 
$SwitchVariable['endTIME'][]='2020-03-28 23:59';

$SwitchVariable['variableName'][]= 'DWC_Switch_2PM'; //Please note what this is for.
$SwitchVariable['startTIME'][]='2019-03-01 00:00'; 
$SwitchVariable['endTIME'][]='2019-03-30 10:50';

$SwitchVariable['variableName'][]= 'DWC_Switch_Footer';
$SwitchVariable['startTIME'][]='2020-01-25 12:00'; 
$SwitchVariable['endTIME'][]='2020-03-30 23:59';




$SwitchVariable['variableName'][]= 'DWC_Switch_new';  	//This needs to be renamed DWC_Switch -TF
$SwitchVariable['startTIME'][]='2020-02-29 00:00'; 
$SwitchVariable['endTIME'][]='2020-03-28 11:59';

/********** Louisiana *****************/

$SwitchVariable['variableName'][]= 'Louisiana_Switch';  	//This needs to be renamed DWC_Switch -TF
$SwitchVariable['startTIME'][]='2019-02-29 00:00'; 
$SwitchVariable['endTIME'][]='2019-03-21 18:10';


/********** Saudi CUP *****************/

$SwitchVariable['variableName'][]= 'SAUDI_Switch';
$SwitchVariable['startTIME'][]='2021-02-16 00:00'; 
$SwitchVariable['endTIME'][]='2021-02-20 12:40';

$SwitchVariable['variableName'][]= 'SAUDI_Switch_Footer';
$SwitchVariable['startTIME'][]='2020-01-04 12:00'; 
$SwitchVariable['endTIME'][]='2020-03-01 23:59';

$SwitchVariable['variableName'][]= 'SAUDI_Switch_UPCOMING';
$SwitchVariable['startTIME'][]='2021-02-15 12:40'; 
$SwitchVariable['endTIME'][]='2021-02-19 23:59';

$SwitchVariable['variableName'][]= 'SAUDI_Switch_DAY';
$SwitchVariable['startTIME'][]='2021-02-19 00:00'; 
$SwitchVariable['endTIME'][]='2021-02-20 12:40';

/********** Kentucky Derby and Oaks *****************/

include '/home/ah/allhorse/public_html/kd/control-switch.php';


/********** Preakness Stakes *****************/


include '/home/ah/allhorse/public_html/ps/control-switch.php';

/********** Belmont Stakes *****************/

include '/home/ah/allhorse/public_html/belmont/control-switch.php';

/**********   NFL Promo Switch      *****************/
$SwitchVariable['variableName'][]= 'NFL_Promo_Switch';
$SwitchVariable['startTIME'][]='2019-09-02 00:00'; 
$SwitchVariable['endTIME'][]='2019-09-30 23:59';

$SwitchVariable['variableName'][]= 'NFL_Switch';
$SwitchVariable['startTIME'][]='2020-01-29 00:00:00'; 
$SwitchVariable['endTIME'][]='2020-02-02 18:30:00';


/*
$SwitchVariable['variableName'][]= 'dateNow';  //Why is this here?  -TF
$SwitchVariable['startTIME'][]=''; 
$SwitchVariable['endTIME'][]='';
*/
/******* Clubhouse Section Switches***********/

// $SwitchVariable['variableName'][]= 'KD_Clubhouse_Switch';
// $SwitchVariable['startTIME'][]='2019-04-24 00:00'; 
// $SwitchVariable['endTIME'][]='2019-04-28 13:59';

// $SwitchVariable['variableName'][]= 'KD_Clubhouse_Switch_FAQ';
// $SwitchVariable['startTIME'][]='2019-04-28 14:00'; 
// $SwitchVariable['endTIME'][]='2019-05-04 18:55';

// $SwitchVariable['variableName'][]= 'KD_Clubhouse_Switch_SAT';
// $SwitchVariable['startTIME'][]='2019-05-04 18:56'; 
// $SwitchVariable['endTIME'][]='2019-05-04 23:59';

// $SwitchVariable['variableName'][]= 'KD_Clubhouse_Switch_SUN';
// $SwitchVariable['startTIME'][]='2019-05-05 00:00'; 
// $SwitchVariable['endTIME'][]='2019-05-17 23:59';

// $SwitchVariable['variableName'][]= 'KD_Clubhouse_Switch_MON';
// $SwitchVariable['startTIME'][]='2018-05-18 14:00'; 
// $SwitchVariable['endTIME'][]='2018-05-19 23:59';

$SwitchVariable['variableName'][]= 'BS_Clubhouse_Switch';
$SwitchVariable['startTIME'][]='2019-06-01 00:00'; 
$SwitchVariable['endTIME'][]='2019-06-07 23:59';

$SwitchVariable['variableName'][]= 'BS_Clubhouse_Switch_SAT';
$SwitchVariable['startTIME'][]='2019-06-08 00:00'; 
$SwitchVariable['endTIME'][]='2019-06-08 18:42';

$SwitchVariable['variableName'][]= 'BS_Clubhouse_Switch_END';
$SwitchVariable['startTIME'][]='2019-06-08 18:42'; 
$SwitchVariable['endTIME'][]='2019-06-21 23:59';

$SwitchVariable['variableName'][]= 'BC_Clubhouse_Switch';
$SwitchVariable['startTIME'][]='2021-11-01 19:42'; 
$SwitchVariable['endTIME'][]='2021-11-06 17:42';

$SwitchVariable['variableName'][]= 'BC_Clubhouse_SAT_Switch';
$SwitchVariable['startTIME'][]='2019-11-02 20:51'; 
$SwitchVariable['endTIME'][]='2019-11-03 23:59';

$SwitchVariable['variableName'][]= 'BC_Clubhouse_Promos_Switch';
$SwitchVariable['startTIME'][]='2019-10-28 00:00'; 
$SwitchVariable['endTIME'][]='2019-11-02 20:46';

$SwitchVariable['variableName'][]= 'PWC_Clubhouse_Switch';
$SwitchVariable['startTIME'][]='2020-01-20 23:59:00'; 
$SwitchVariable['endTIME'][]='2020-01-24 23:59:00';

$SwitchVariable['variableName'][]= 'PWC_Clubhouse_Switch_SAT';
$SwitchVariable['startTIME'][]='2020-01-24 23:59:00'; 
$SwitchVariable['endTIME'][]='2020-01-25 17:34:00';

$SwitchVariable['variableName'][]= 'PWC_Clubhouse_Switch_SUN';
$SwitchVariable['startTIME'][]='2020-01-25 17:34:00'; 
$SwitchVariable['endTIME'][]='2020-01-26 23:59:00';

$SwitchVariable['variableName'][]= 'PWC_Clubhouse_Switch_PROMOS';
$SwitchVariable['startTIME'][]='2020-01-24 23:59:00';
$SwitchVariable['endTIME'][]='2020-01-26 23:59:00';

$SwitchVariable['variableName'][]= 'DWC_Clubhouse_Switch';
$SwitchVariable['startTIME'][]='2021-03-25 00:00'; 
$SwitchVariable['endTIME'][]='2021-03-27 12:50';

$SwitchVariable['variableName'][]= 'NFL_Clubhouse_Switch';
$SwitchVariable['startTIME'][]='2020-01-28 00:00:00'; 
$SwitchVariable['endTIME'][]='2020-01-30 12:00:00';

$SwitchVariable['variableName'][]= 'NFL_Clubhouse_Switch_SAT';
$SwitchVariable['startTIME'][]='2020-01-30 12:00:00'; 
$SwitchVariable['endTIME'][]='2020-02-02 18:30:00';

//KDSwitch Start
//$SwitchVariable['variableName'][]= 'KDSwitch'; 
//$SwitchVariable['startTIME'][]='2018-10-25 08:10'; 
//$SwitchVariable['endTIME'][]='2019-05-04 18:53'; 
//KDSwitch End

//PSSwitch Start
//$SwitchVariable['variableName'][]= 'PSSwitch'; 
//$SwitchVariable['startTIME'][]='2018-10-25 08:10'; 
//$SwitchVariable['endTIME'][]='2019-05-18 18:53'; 

//PSSwitch End

//BSSwitch Start
//$SwitchVariable['variableName'][]= 'BSSwitch'; 
//$SwitchVariable['startTIME'][]='2018-10-25 08:10'; 
//$SwitchVariable['endTIME'][]='2019-06-08 18:53'; 
//BSSwitch End


//BCSwitch Start
//$SwitchVariable['variableName'][]= 'BCSwitch'; 
//$SwitchVariable['startTIME'][]='2018-10-25 08:10'; 
//$SwitchVariable['endTIME'][]='2019-11-03 18:53'; 
//BCSwitch End

//PegasusSwitch Start
//$SwitchVariable['variableName'][]= 'PegasusSwitch'; 
//$SwitchVariable['startTIME'][]='2019-01-14 07:00'; 
//$SwitchVariable['endTIME'][]='2019-01-26 23:59'; 
//PegasusSwitch End

//PegasusSwitch Start - Top Menu and  Footer Menu
//$SwitchVariable['variableName'][]= 'PegasusSwitch';
//$SwitchVariable['startTIME'][]='2019-01-14 07:00'; 
//$SwitchVariable['endTIME'][]='2019-01-26 23:59'; 

//PegasusSwitch Start - HorseBetting section
//$SwitchVariable['variableName'][]= 'PegasusHBSwitch';
//$SwitchVariable['startTIME'][]='2019-01-14 07:00'; 
//$SwitchVariable['endTIME'][]='2019-01-26 23:59'; 

//PegasusSwitch Start - 10% SIGN UP BONUS section
//$SwitchVariable['variableName'][]= 'Pegasus10signSwitch';
//$SwitchVariable['startTIME'][]='2019-01-14 07:00'; 
//$SwitchVariable['endTIME'][]='2019-01-26 23:59';

//PegasusSwitch Start - 10% SIGN UP BONUS section
//$SwitchVariable['variableName'][]= 'ShowPegasus';
//$SwitchVariable['startTIME'][]='2019-01-26 11:10'; 
//$SwitchVariable['endTIME'][]='2019-01-26 11:11';

//PegasusSwitch Start - 10% SIGN UP BONUS section
/*$SwitchVariable['variableName'][]= 'ShowPegasusOddS';
$SwitchVariable['startTIME'][]='2019-01-26 10:00'; 
$SwitchVariable['endTIME'][]='2019-01-26 17:59';*/

//USElection_Clubhouse_Switch
$SwitchVariable['variableName'][]= 'USElection_Clubhouse_Switch';
$SwitchVariable['startTIME'][]='2020-10-22 21:00'; 
$SwitchVariable['endTIME'][]='2020-11-03 23:59';

$SwitchVariable['variableName'][]= 'BC_Clubhouse_Stage_5_Switch';
$SwitchVariable['startTIME'][]='2020-11-03 23:59'; 
$SwitchVariable['endTIME'][]='2020-11-07 17:30';

$SwitchVariable['variableName'][]= 'BC_Clubhouse_Stage_6_Switch';
$SwitchVariable['startTIME'][]='2020-10-26 08:00'; 
$SwitchVariable['endTIME'][]='2020-11-03 23:59';


?>
