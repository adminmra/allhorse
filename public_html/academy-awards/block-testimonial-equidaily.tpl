<section class="testimonials--blue usr-section">
  <div class="container">
    <blockquote class="testimonial-blue" style="display: flex; justify-content: center;">
      {* <figure class="testimonial-blue_figure"><img src="/img/grammy/equidaily.jpg" alt="Pace Advantage"
          class="testimonial-blue_img" style="background-color: transparent; width: 100px; height: 100px;"></figure> *}
      <div class="testimonial-blue_content">
        <p class="testimonial-blue_p">US Racing offers insightful feature pieces on the industry as well as interesting takes on the handicapping side of the game.</p>
        {* <p class="testimonial-blue_p">For horse racing and blackjack I play at BUSR</p> *}
        <span class="testimonial-blue_name">
          - EQUIDAILY <br>
          {* - Fred Faour - ESPN Radio Host - Houston 97.5FM<br> *}
        </span>
      </div>
    </blockquote>
  </div>
</section>