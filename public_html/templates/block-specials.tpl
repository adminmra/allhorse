{literal}
    <style>
        @media (max-width: 1157px) {
            .breakline {
                display: none;
            }
        }

        @media (min-width: 1200px) and (max-width: 1495px) {
            .breakline {
                display: none;
            }
        }
    </style>
{/literal}
<section class="card">
    <div class="card_wrap">
        <div class="card_half card_hide-mobile">
            <a href="https://www.betusracing.ag/login?ref={$ref}&to=sports?leagueId=4272"> {*league Id of the btn*}
                <img src="/img/pegasus-world-cup/specials.jpg" data-src-img="/img/pegasus-world-cup/specials.jpg" {*Change the image*}
                    alt="2020 Pegasus World Cup Specials" class="card_img"></a></div>
        <div class="card_half card_content">
            <a href="https://www.betusracing.ag/login?ref={$ref}&to=sports?leagueId=4272">{*league Id of the btn*}
                <img class="icon-kentucky-derby-betting card_icon"
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABVAQMAAADUnuB8AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABFJREFUeNpjYBgFo2AUDHIAAAOnAAHnt6kCAAAAAElFTkSuQmCC"> {*Change the image*}
            </a>
            <h2 class="card_heading">Bet on the <br class="breakline"> Template World Cup Specials</h2>
            <p style="margin-bottom: 10px">Change text</p>
            <p><a href="/pegasus-world-cup/odds">Template World Cup Odds</a>, Props and Futures are live, Bet Now!</p>
            <a href="https://www.betusracing.ag/login?ref={$ref}&to=to/racebook?meetingRace=GP*2020/01/25*12" class="card_btn">Bet on the Template World Cup Specials</a>
        </div>
    </div>
</section>