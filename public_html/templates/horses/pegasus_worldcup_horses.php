<script type="application/ld+json">
    { "@context": "http://schema.org", "@type": "Table", "name": "2020 Pegasus World Cup Contenders", "about": { "@type": "Event", "name": "Pegasus World Cup 2020", "location": { "@type": "Place", "name": "Gulfstream Park", "address": { "@type": "PostalAddress", "addressLocality": "Hallandale Beach", "addressRegion": "Florida" } }, "startDate": "2020-01-25", "description": "Latest Pegasus World Cup 2020 odds at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus 2020." }, "keywords": "List of Contenders Pegasus World Cup, Pegasus World Cup Odds, Pegasus World Cup 2020, Pegasus World Cup 2020 Contenders, Pegasus World Cup, Bet On The Pegasus World Cup, Pegasus World Cup Invitational" }
</script>
<div id="no-more-tables">
    <table id="infoEntries" class="table table-condensed table-striped table-bordered ordenableResult" border="0" cellspacing="0" cellpadding="10">
        <thead>

            <tr>
           	<th style="width: 8%;"> PP </th>
                <th> Horse </th>
                <th> Jockey </th>
                <th style="width: 11%;"> Trainer </th>
                <th> Breeder </th>
                <th> Owner </th>
                <th style="width: 11%;"> Earnings </th> 
	   </tr>
        </thead>

        <tbody>
<tr>
    <td data-title="Post-Position"> 1 </td>
    <td data-title="Horse"> True Timber </td>
    <td data-title="Jockey"> Joe Bravo </td>
    <td data-title="Trainer"> Kiaran P. McLaughlin </td>
    <td data-title="Breeder"> Mr. & Mrs. Marc C. Ferrell </td>
    <td data-title="Owner"> Calumet Farm </td>
    <td data-title="Earnings"> $950,550 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 2 </td>
    <td data-title="Horse"> Tax </td>
    <td data-title="Jockey"> Jose Ortiz </td>
    <td data-title="Trainer"> Danny Gargan </td>
    <td data-title="Breeder"> Claiborne Farm & Adele B. Dilschneider </td>
    <td data-title="Owner">  R. A. Hill Stable, Reeves Thoroughbred Racing, Lynch, Hugh and Corms Racing Stable  </td>
    <td data-title="Earnings"> $828,300 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 3 </td>
    <td data-title="Horse"> Diamond Oops </td>
    <td data-title="Jockey"> Julien Leparoux </td>
    <td data-title="Trainer"> Patrick L. Biancone </td>
    <td data-title="Breeder"> Kin Hui Racing Stables LLC </td>
    <td data-title="Owner"> Diamond 100 Racing Club, LLC, Dunne, Amy E., D P Racing LLC and Patrick L. Biancone Racing LLC  </td>
    <td data-title="Earnings"> $646,490 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 4 </td>
    <td data-title="Horse"> Seeking the Soul </td>
    <td data-title="Jockey"> John Velazquez </td>
    <td data-title="Trainer"> Dallas Stewart </td>
    <td data-title="Breeder"> Charles Fipke </td>
    <td data-title="Owner">  Charles E. Fipke  </td>
    <td data-title="Earnings"> $3,420,153 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 5 </td>
    <td data-title="Horse"> Omaha beach </td>
    <td data-title="Jockey"> Mike Smith </td>
    <td data-title="Trainer"> Richard E. Mandella </td>
    <td data-title="Breeder"> Charming Syndicate </td>
    <td data-title="Owner"> Fox Hill Farms, Inc.  </td>
    <td data-title="Earnings"> $1,651,800 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 6 </td>
    <td data-title="Horse"> Higher Power </td>
    <td data-title="Jockey"> Flavien Prat </td>
    <td data-title="Trainer"> John W. Sadler </td>
    <td data-title="Breeder"> Pin Oak Stud, LLC </td>
    <td data-title="Owner"> Hronis Racing LLC  </td>
    <td data-title="Earnings"> $1,376,648 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 7 </td>
    <td data-title="Horse"> War Story </td>
    <td data-title="Jockey"> Joel Rosario </td>
    <td data-title="Trainer"> Elizabeth L. Dobles </td>
    <td data-title="Breeder"> Jack Swain III </td>
    <td data-title="Owner"> Imaginary Stables and Ellis, Glenn </td>
    <td data-title="Earnings"> $2,984,996 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 8 </td>
    <td data-title="Horse"> Mr Freeze </td>
    <td data-title="Jockey"> Luis Saez </td>
    <td data-title="Trainer"> Dale L. Romans </td>
    <td data-title="Breeder"> Siena Farms LLC </td>
    <td data-title="Owner"> Bakke, Jim and Isbister, Gerald  </td>
    <td data-title="Earnings"> $639,110 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 9 </td>
    <td data-title="Horse"> Spun to Run </td>
    <td data-title="Jockey"> Javier Castellano </td>
    <td data-title="Trainer"> Juan Carlos Guerrero </td>
    <td data-title="Breeder"> Sabana Farm </td>
    <td data-title="Owner"> Robert P. Donaldson  </td>
    <td data-title="Earnings"> $1,160,520 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 10 </td>
    <td data-title="Horse"> Mucho Gusto </td>
    <td data-title="Jockey"> Irad Ortiz, Jr. </td>
    <td data-title="Trainer"> Bob Baffert </td>
    <td data-title="Breeder"> Teneri Farm Inc. & Bernardo Alvarez Calderon </td>
    <td data-title="Owner"> Michael Lund Petersen </td>
    <td data-title="Earnings"> $779,800 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 11 </td>
    <td data-title="Horse"> Tenfold </td>
    <td data-title="Jockey"> Tyler Gaffalione </td>
    <td data-title="Trainer"> Steven M. Asmussen </td>
    <td data-title="Breeder"> Winchell Thoroughbreds, LLC </td>
    <td data-title="Owner"> Winchell Thoroughbreds LLC </td>
    <td data-title="Earnings"> $968,390 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 12 </td>
    <td data-title="Horse"> Bodexpress </td>
    <td data-title="Jockey"> Emisael Jaramillo </td>
    <td data-title="Trainer"> Gustavo Delgado </td>
    <td data-title="Breeder"> Martha Jane Mulholland </td>
    <td data-title="Owner"> Top Racing, LLC, Global Thoroughbred and GDS Racing Stable </td>
    <td data-title="Earnings"> $264,000 </td>
</tr>

        </tbody>
    </table>
    
</div>

<style type="text/css">
    table.ordenableResult th {
        cursor: pointer
    }
</style>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">

<script src="/assets/js/sorttable_date.js"></script>
