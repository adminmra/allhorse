<div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Arklow">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="2">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y"); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
			
<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Horse Information</th>
                </tr>
		<tr><td>	Color	</td><td>	Chestnut	</td></tr>
<tr><td>	Birthplace	</td><td>	Kentucky	</td></tr>
<tr><td>	Foal Date	</td><td>	7-Apr-15	</td></tr>
<tr><td>	Trainer:	</td><td>	Richard E. Mandella	</td></tr>
<tr><td>	Breeder	</td><td>	Rosemont Farm LLC	</td></tr>
<tr><td>	Owner:	</td><td>	LNJ Foxwoods	</td></tr>
<tr><td>	Buyer:	</td><td>	Solis/Litt	</td></tr>


            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Career Statistics</th>
                </tr>
        <tr><td>	Starts	</td><td>	11	</td></tr>
<tr><td>	Firsts	</td><td>	3	</td></tr>
<tr><td>	Seconds	</td><td>	3	</td></tr>
<tr><td>	Thirds	</td><td>	1	</td></tr>
<tr><td>	Earnings	</td><td>	$893,549 	</td></tr>

            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="1" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="1" class="center">Achievements</th>
                </tr>

<tr><td>	2019  Longines Breeders' Cup Turf - Winner	</td></tr>
<tr><td>	2019 Allowance Optional Claiming - Winner	</td></tr>

		
            </tbody>
        </table>
    </td>
</tr>

</table>
    </div>
