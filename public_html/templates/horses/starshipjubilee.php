<div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Arklow">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="2">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y"); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
			
<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Horse Information</th>
                </tr>
		<tr><td>	Color	</td><td>	Bay	</td></tr>
<tr><td>	Birthplace	</td><td>	Florida	</td></tr>
<tr><td>	Foal Date	</td><td>	10-Mar-13	</td></tr>
<tr><td>	Trainer:	</td><td>	Kevin Attard	</td></tr>
<tr><td>	Breeder	</td><td>	William P. Sorren	</td></tr>
<tr><td>	Owner:	</td><td>	Blue Heaven Farm 	</td></tr>
<tr><td>	Buyer:	</td><td>	Brookdale Sales	</td></tr>

            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Career Statistics</th>
                </tr>
        <tr><td>	Starts	</td><td>	32	</td></tr>
<tr><td>	Firsts	</td><td>	14	</td></tr>
<tr><td>	Seconds	</td><td>	5	</td></tr>
<tr><td>	Thirds	</td><td>	3	</td></tr>
<tr><td>	Earnings	</td><td>	$1,171,387 	</td></tr>

            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="1" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="1" class="center">Achievements</th>
                </tr>
        <tr><td>	2019 E. P. Taylor Stakes (Gr. 1) - Winner	</td></tr>
<tr><td>	2019 Canadian S. Presented by the Japan Racing Association (Gr. 2) - Winner	</td></tr>
<tr><td>	2019 Sunshine Millions Filly and Mare Turf Stakes (Black Type) - Winner	</td></tr>

		
            </tbody>
        </table>
    </td>
</tr>

</table>
    </div>
