<div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Arklow">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="2">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y"); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
			
<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Horse Information</th>
                </tr>
		<tr><td>	Color	</td><td>	Dark Bay	</td></tr>
<tr><td>	Birthplace	</td><td>	Kentucky	</td></tr>
<tr><td>	Foal Date	</td><td>	15-Apr-13	</td></tr>
<tr><td>	Trainer:	</td><td>	Richard Baltas	</td></tr>
<tr><td>	Breeder	</td><td>	Buck Pond Farm, Inc.	</td></tr>
<tr><td>	Owner:	</td><td>	Baltas, Debby, Baltas, Richard, Dunn, Christopher T., Iavarone, Jules, Iavarone, Michael, McClanahan, Jerry, Robershaw, Ritchie and Taylor, Mark 	</td></tr>
<tr><td>	Buyer:	</td><td>	David Meah	</td></tr>

            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Career Statistics</th>
                </tr>
        <tr><td>	Starts	</td><td>	29	</td></tr>
<tr><td>	Firsts	</td><td>	7	</td></tr>
<tr><td>	Seconds	</td><td>	4	</td></tr>
<tr><td>	Thirds	</td><td>	2	</td></tr>
<tr><td>	Earnings	</td><td>	$1,677,771 	</td></tr>

            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="1" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="1" class="center">Achievements</th>
                </tr>
        <tr><td>	2019 TVG Breeders' Cup Mile - participant	</td></tr>
<tr><td>	2019 Seabiscuit Handicap (Gr. 2) - Winner	</td></tr>
<tr><td>	2019 San Gabriel Stakes (Gr. 2) - Winner	</td></tr>
<tr><td>	2018 Breeders' Cup Mile participant - participant	</td></tr>
<tr><td>	2018 Shadwell Turf Mile Stakes (Gr. 1) - Winner	</td></tr>
<tr><td>	2018 Old Friends Stakes (Black Type) - Winner	</td></tr>

		
            </tbody>
        </table>
    </td>
</tr>

</table>
    </div>
