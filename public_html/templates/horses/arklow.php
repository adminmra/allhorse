<div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Arklow">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="2">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y"); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
			
<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Horse Information</th>
                </tr>
		<tr>
            <td>Color</td>
            <td>Bay</td>
        </tr>
		<tr>
            <td>Birthplace</td>
            <td>Kentucky</td>
        </tr>
        <tr>
            <td>Foal Date</td>
            <td>7-Feb-14</td>
        </tr>
        <tr>
            <td>Trainer:</td>
            <td>Brad H. Cox</td>
        </tr>
		
        <tr>
            <td>Breeder</td>
            <td>ohn R. Penn & Frank Penn</td>
        </tr>
        <tr>
            <td>Owner:</td>
            <td>Donegal Racing, Bulger, Joseph and Coneway, Peter </td>
        </tr>
        <tr>
            <td>Buyer:</td>
            <td>Donegal Racing</td>
        </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Career Statistics</th>
                </tr>
        <tr>
            <td>Starts</td>
            <td>24</td>
        </tr>
        <tr>
            <td>Firsts</td>
            <td>6</td>
        </tr>
        <tr>
            <td>Seconds</td>
            <td>6</td>
        </tr>
        <tr>
            <td>Thirds</td>
            <td>2</td>
        </tr>
        <tr>
            <td>Earnings</td>
            <td>$1,816,382 </td>
        </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="1" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="1" class="center">Achievements</th>
                </tr>
        <tr>
            <td>2019 Longines Breeders' Cup Turf - participant</td>
        </tr>
        <tr>
            <td>2019 Bowling Green Stakes (Gr. 2) - Winner</td>
        </tr>
		<tr>
            <td>2018 Bald Eagle Derby (Black Type) - Winner</td>
        </tr>
        <tr>
            <td>2018 Exacta Systems Dueling Grounds Derby (Listed) - Winner</td>
        </tr>
		<tr>
            <td>2018 Allowance Optional Claiming - Winner</td>
        </tr>
            </tbody>
        </table>
    </td>
</tr>

</table>
    </div>
