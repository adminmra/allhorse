<div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Arklow">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="2">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y"); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
			
<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Horse Information</th>
                </tr>
		<tr><td>	Color	</td><td>	Bay	</td></tr>
<tr><td>	Birthplace	</td><td>	Kentucky	</td></tr>
<tr><td>	Foal Date	</td><td>	5-Apr-16	</td></tr>
<tr><td>	Trainer:	</td><td>	Peter Miller	</td></tr>
<tr><td>	Breeder	</td><td>	Bardy Farm	</td></tr>
<tr><td>	Owner:	</td><td>	Bardy Farm and OG Boss 	</td></tr>
<tr><td>	Buyer:	</td><td>		</td></tr>

            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Career Statistics</th>
                </tr>
        <tr><td>	Starts	</td><td>	9	</td></tr>
<tr><td>	Firsts	</td><td>	4	</td></tr>
<tr><td>	Seconds	</td><td>	3	</td></tr>
<tr><td>	Thirds	</td><td>	1	</td></tr>
<tr><td>	Earnings	</td><td>	$499,460 	</td></tr>

            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="1" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="1" class="center">Achievements</th>
                </tr>
        <tr><td>	2019 Mathis Brothers Mile Stakes (Gr. 2) - Winner	</td></tr>
<tr><td>	2019 Hollywood Derby (Gr. 1) - Winner	</td></tr>
<tr><td>	2019 Qatar Twilight Derby (Gr. 2) - Winner	</td></tr>

		
            </tbody>
        </table>
    </td>
</tr>

</table>
    </div>
