<div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Arklow">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="2">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y"); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
			
<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Horse Information</th>
                </tr>
		<tr><td>	Color	</td><td>	Brown	</td></tr>
<tr><td>	Birthplace	</td><td>	Kentucky	</td></tr>
<tr><td>	Foal Date	</td><td>	6-Apr-15	</td></tr>
<tr><td>	Trainer:	</td><td>	Chad C. Brown	</td></tr>
<tr><td>	Breeder	</td><td>	KatieRich Farms	</td></tr>
<tr><td>	Owner:	</td><td>	OXO Equine LLC 	</td></tr>
<tr><td>	Buyer:	</td><td>	OXO Equine 	</td></tr>

            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Career Statistics</th>
                </tr>
        <tr><td>	Starts	</td><td>	14	</td></tr>
<tr><td>	Firsts	</td><td>	3	</td></tr>
<tr><td>	Seconds	</td><td>	3	</td></tr>
<tr><td>	Thirds	</td><td>	3	</td></tr>
<tr><td>	Earnings	</td><td>	$589,240 	</td></tr>

            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="1" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="1" class="center">Achievements</th>
                </tr>
        <tr>
            <td>2019 Ft. Lauderdale Stakes (Gr. 2) - Winner</td>
        </tr>
        <tr>
            <td>2018 Lecomte Stakes (Gr. 3) - Winner</td>
        </tr>
		
            </tbody>
        </table>
    </td>
</tr>

</table>
    </div>
