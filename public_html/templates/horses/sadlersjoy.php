<div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Arklow">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="2">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y"); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
			
<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Horse Information</th>
                </tr>
		<tr><td>	Color	</td><td>	Chestnut	</td></tr>
<tr><td>	Birthplace	</td><td>	Kentucky	</td></tr>
<tr><td>	Foal Date	</td><td>	8-Mar-13	</td></tr>
<tr><td>	Trainer:	</td><td>	Thomas Albertrani	</td></tr>
<tr><td>	Breeder	</td><td>	Woodslane Farm, LLC.	</td></tr>
<tr><td>	Owner:	</td><td>	Woodslane Farm 	</td></tr>
<tr><td>	Buyer:	</td><td>		</td></tr>


            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Career Statistics</th>
                </tr>
        <tr><td>	Starts	</td><td>	26	</td></tr>
<tr><td>	Firsts	</td><td>	7	</td></tr>
<tr><td>	Seconds	</td><td>	4	</td></tr>
<tr><td>	Thirds	</td><td>	7	</td></tr>
<tr><td>	Earnings	</td><td>	$2,471,360 	</td></tr>


            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="1" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="1" class="center">Achievements</th>
                </tr>
        <tr><td>	2019 Red Smith Stakes (Gr. 3) - Winner	</td></tr>
<tr><td>	2018 Longines Breeders' Cup Turf - 3rd Place	</td></tr>
<tr><td>	2018 Mac Diarmida Stakes (Gr. 2) - Winner	</td></tr>

		
            </tbody>
        </table>
    </td>
</tr>

</table>
    </div>
