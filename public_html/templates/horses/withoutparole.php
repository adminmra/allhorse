<div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Arklow">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="2">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y"); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
			
<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Horse Information</th>
                </tr>
		<tr><td>	Color	</td><td>	Bay	</td></tr>
<tr><td>	Birthplace	</td><td>	Great Britain	</td></tr>
<tr><td>	Foal Date	</td><td>	20-Mar-15	</td></tr>
<tr><td>	Trainer:	</td><td>	Chad C. Brown	</td></tr>
<tr><td>	Breeder	</td><td>	Mr John Gunther	</td></tr>
<tr><td>	Owner:	</td><td>	Gunther, John D. and Gunther, Tanya 	</td></tr>
<tr><td>	Buyer:	</td><td>	Newsells Park Stud Ltd.	</td></tr>

            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Career Statistics</th>
                </tr>
        <tr><td>	Starts	</td><td>	10	</td></tr>
<tr><td>	Firsts	</td><td>	4	</td></tr>
<tr><td>	Seconds	</td><td>	0	</td></tr>
<tr><td>	Thirds	</td><td>	1	</td></tr>
<tr><td>	Earnings	</td><td>	$834,030 	</td></tr>


            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="1" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="1" class="center">Achievements</th>
                </tr>
        <tr><td>	2019 TVG Breeders' Cup Mile - 3rd place	</td></tr>
<tr><td>	2018 St. James's Palace Stakes (Gr. 1) - Winner	</td></tr>
<tr><td>	2018 Matchbook Is Commission Free Heron Stakes - Winner	</td></tr>
<tr><td>	2018 Novice Weight For Age - Winner	</td></tr>

		
            </tbody>
        </table>
    </td>
</tr>

</table>
    </div>
