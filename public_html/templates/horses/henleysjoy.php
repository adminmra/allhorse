<div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Arklow">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="2">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y"); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
			
<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Horse Information</th>
                </tr>
		<tr>
            <td>Color</td>
            <td>Chestnut</td>
        </tr>
		<tr>
            <td>Birthplace</td>
            <td>Kentucky</td>
        </tr>
        <tr>
            <td>Foal Date</td>
            <td>17-Mar-16</td>
        </tr>
        <tr>
            <td>Trainer:</td>
            <td>Michael J. Maker</td>
        </tr>
		
        <tr>
            <td>Breeder</td>
            <td>Kenneth L. Ramsey & Sarah K. Ramsey</td>
        </tr>
        <tr>
            <td>Owner:</td>
            <td>Bloom Racing Stable LLC (Jeffrey Bloom) </td>
        </tr>
        <tr>
            <td>Buyer:</td>
            <td>Bloom Racing Stable  </td>
        </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Career Statistics</th>
                </tr>
        <tr>
            <td>Starts</td>
            <td>16</td>
        </tr>
        <tr>
            <td>Firsts</td>
            <td>4</td>
        </tr>
        <tr>
            <td>Seconds</td>
            <td>3</td>
        </tr>
        <tr>
            <td>Thirds</td>
            <td>0</td>
        </tr>
        <tr>
            <td>Earnings</td>
            <td>$1,070,711</td>
        </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="1" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="1" class="center">Achievements</th>
                </tr>
        <tr>
            <td>2019 Belmont Derby Invitational Stakes (Gr. 1) - Winner</td>
        </tr>
        <tr>
            <td>2018 Breeders' Cup Juvenile Turf - participant </td>
        </tr>
		<tr>
            <td>2018 Pulpit Stakes (Black Type) - Winner</td>
        </tr>
        <tr>
            <td>2018 Kentucky Downs Juvenile Stakes (Black Type) - Winner</td>
        </tr>
            </tbody>
        </table>
    </td>
</tr>

</table>
    </div>
