<div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Arklow">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="2">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y"); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
			
<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Horse Information</th>
                </tr>
		<tr>
            <td>Color</td>
            <td>Bay</td>
        </tr>
		<tr>
            <td>Birthplace</td>
            <td>Ireland</td>
        </tr>
        <tr>
            <td>Foal Date</td>
            <td>13-Mar-15</td>
        </tr>
        <tr>
            <td>Trainer:</td>
            <td>Aidan P. O'Brien</td>
        </tr>
		
        <tr>
            <td>Breeder</td>
            <td>Ecurie Des Monceaux & Skymarc Farm Inc</td>
        </tr>
        <tr>
            <td>Owner:</td>
            <td>Michael Tabor & Derrick Smith & Mrs John Magnier </td>
        </tr>
        <tr>
            <td>Buyer:</td>
            <td>P. & R. Doyle, M.V. Magnier, & Mayfair Speculators </td>
        </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Career Statistics</th>
                </tr>
        <tr>
            <td>Starts</td>
            <td>22</td>
        </tr>
        <tr>
            <td>Firsts</td>
            <td>3</td>
        </tr>
        <tr>
            <td>Seconds</td>
            <td>8</td>
        </tr>
        <tr>
            <td>Thirds</td>
            <td>2</td>
        </tr>
        <tr>
            <td>Earnings</td>
            <td>$4,146,829 </td>
        </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="1" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="1" class="center">Achievements</th>
                </tr>
        <tr>
            <td>2019 Seppelt MacKinnon Stakes (Gr. 1) - Winner</td>
        </tr>
        <tr>
            <td>2018 Maker's Mark Breeders' Cup Filly and Mare Turf - participant</td>
        </tr>
		<tr>
            <td>2018 Ribblesdale Stakes (Gr. 2) - Winner</td>
        </tr>
        <tr>
            <td>2018  Arkle Finance Cheshire Oaks - Winner
</td>
        </tr>
            </tbody>
        </table>
    </td>
</tr>

</table>
    </div>
