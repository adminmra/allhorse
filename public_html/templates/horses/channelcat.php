<div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Arklow">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="2">
                        <em id='updateemp'>Updated <?php $tdate = date("M d, Y"); echo $tdate; ?></em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
			
<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Horse Information</th>
                </tr>
		<tr>
            <td>Color</td>
            <td>Chestnut Horse</td>
        </tr>
		<tr>
            <td>Birthplace</td>
            <td>Kentucky</td>
        </tr>
        <tr>
            <td>Foal Date</td>
            <td>5-Mar-15</td>
        </tr>
        <tr>
            <td>Trainer:</td>
            <td> Todd A. Pletcher</td>
        </tr>
		
        <tr>
            <td>Breeder</td>
            <td>Calumet Farm </td>
        </tr>
        <tr>
            <td>Owner:</td>
            <td>Calumet Farm </td>
        </tr>
        <tr>
            <td>Buyer:</td>
            <td> &nbsp;</td>
        </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="2" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="2" class="center">Career Statistics</th>
                </tr>
        <tr>
            <td>Starts</td>
            <td>20</td>
        </tr>
        <tr>
            <td>Firsts</td>
            <td>5</td>
        </tr>
        <tr>
            <td>Seconds</td>
            <td>2</td>
        </tr>
        <tr>
            <td>Thirds</td>
            <td>5</td>
        </tr>
        <tr>
            <td>Earnings</td>
            <td>$913,992</td>
        </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
    <td colspan="1" class="center">
        <table width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
            <tbody>
                <tr>
                    <th colspan="1" class="center">Achievements</th>
                </tr>
        <tr>
            <td>2019 Longines Breeders' Cup Turf - participant
</td>
        </tr>
        <tr>
            <td>2019 Bowling Green Stakes (Gr. 2) - Winner
</td>
        </tr>
		<tr>
            <td>2018 Bald Eagle Derby (Black Type) - Winner
</td>
        </tr>
        <tr>
            <td>2018 Exacta Systems Dueling Grounds Derby (Listed) - Winner
</td>
        </tr>
		<tr>
            <td>2018 Allowance Optional Claiming - Winner
</td>
        </tr>
        
            </tbody>
        </table>
    </td>
</tr>

</table>
    </div>
