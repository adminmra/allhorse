
<?php

if ( !defined( 'APP_PATH' ) ) {
	define( 'APP_PATH' , '/home/ah/allhorse/public_html/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

class BetlmScraper extends Scraper{

	function __construct(){
		parent::__construct();
		$this->source = "https://app.betlm.ag/static/info/_default/horses.html";
	}

	public function scraping_to_database(){
		$html = file_get_contents( $this->source );
		//get the html returned from the following url

		$html_doc = new DOMDocument();

		libxml_use_internal_errors( TRUE ); //disable libxml errors

		if( !empty( $html ) ) { //if any html is actually returned

			$html_doc = new DOMDocument;

			$html_doc->preserveWhiteSpace = false;

			$html_doc->loadHTML($html);
			libxml_clear_errors(); //remove errors for yucky html


			$html_xpath = new DOMXPath($html_doc);

			$xquery_items = $html_xpath->query("//table[@class='table table-striped table-bordered']/tr/td");
			   /*
			DROP TABLE cron_racing_schedule IF EXISTS;
			CREATE TABLE cron_racing_schedule
			(
			    id_cron_racing_schedule INTEGER PRIMARY KEY AUTO_INCREMENT,
			    schedule_date TEXT,
			    name TEXT,
			    register_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
			);*/
			$total_sql = "TRUNCATE TABLE cron_racing_schedule";
			$result = DB::query($total_sql);

			$total_sql = "INSERT INTO cron_racing_schedule(schedule_date, name, register_date) VALUES ";

			$two_columns = FALSE;
			?>
			<table border="1">
			<?php

			$sql = "";

			$column_date_1 = "";
			$column_date_2 = "";

			for($i=0; $i<$xquery_items->length;$i=$i+2) {

				$header_css = "";
				 $is_header = FALSE;

				if(preg_match("/[0-9]/", $xquery_items->item($i)->nodeValue ) == 1)  {
					$column_date_1 = date("Y-m-d", strtotime($xquery_items->item($i)->nodeValue) );

					if($xquery_items->item($i+1)!=NULL AND preg_match("/[0-9]/", $xquery_items->item($i+1)->nodeValue ) == 1) {
						$column_date_2 = date("Y-m-d", strtotime($xquery_items->item($i+1)->nodeValue) );
						$two_columns = TRUE;
					}
					else {
						$two_columns = FALSE;
					}
					$is_header = TRUE;
					$header_css = "style ='font-weight:bold'";
				}

				if($two_columns==TRUE) {
					if($is_header == FALSE) {
						if(strcasecmp($sql,"")!=0) {
							$sql .=", ";
						}
						$sql .= " ('".$column_date_1."', '".$xquery_items->item($i)->nodeValue."', '".date('Y-m-d')."')";
						$sql .= ", ('".$column_date_2."', '".$xquery_items->item($i+1)->nodeValue."', '".date('Y-m-d')."')";
					}
					?>
					<tr>
						<td <?php echo $header_css?>><?php echo $xquery_items->item($i)->nodeValue." -- ".$column_date_1; ?></td>
						<td <?php echo $header_css?>><?php echo $xquery_items->item($i+1)->nodeValue." -- ".$column_date_2; ?></td>
					</tr>
					<?php
				}
				else {
					if( $is_header == FALSE ) {
						if(strcasecmp($sql,"")!=0) {
							$sql .=", ";
						}
						$sql .= "('".$column_date_1."', '".$xquery_items->item($i)->nodeValue."', '".date('Y-m-d')."')";
					}
					if($xquery_items->item($i+1)!=null) {
						if(strcasecmp($sql,"")!=0) {
							$sql .=", ";
						}
						$sql .= "('".$column_date_1."', '".$xquery_items->item($i+1)->nodeValue."', '".date('Y-m-d')."')";
					}
				    ?>
					<tr>
						<td <?php echo $header_css?>><?php echo ($i)."  ".$xquery_items->item($i)->nodeValue." -- ".$column_date_1; ?></td>
					</tr>
					<tr>
						<td><?php echo ($i+1)."  ".$xquery_items->item($i+1)->nodeValue." -- ".$column_date_1; ?></td>
					</tr>
					<?php
				}
			}?>
			</table>
			<?php


		    $total_sql .= $sql;

		    print( sprintf(
			$this->console_color->getColoredString( ">>> " , "cyan" ) . "%'.-110s%-s\n\n" ,
			$this->console_color->getColoredString( $total_sql , "red" ) ,
			'SQL'
			) );

		    $result = DB::query($total_sql);
		}
	}

	public function scraping_to_xml(){}

	public function scraping_to_tpl(){}

	public static function run() {
		try {
			$instance = new self;
			$instance->scraping_to_database();

		} catch (Exception $e) {
			print( $e->getMessage() );
		}
	}
}

BetlmScraper::run();

?>
