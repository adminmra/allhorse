<?php

if ( !defined( 'APP_PATH' ) ) {
    define( 'APP_PATH' , '/home/ah/allhorse/public_html/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

class kentucky_prep_races extends Scraper{

    private $ALWAYSWRITE;
    private $OUTPUTFILEPATH;
    private $IdLeague;
    private $data;
    private $mainTitle;

    function __construct(){
        parent::__construct();
        $this->source         = "https://www.kentuckyderby.com/horses/prep-races";
        $this->ALWAYSWRITE    = FALSE; //always write eveng with the message "please check back shortly"
        $this->OUTPUTFILEPATH = "/home/ah/allhorse/public_html/kd/prepraces.php"; //the absolute path of the file to be written
        $this->IdLeague       = "";
        $this->data           = array();
        $this->mainTitle      = "";

        //  /home/ah/allhorse/public_html/kd/wherewhenwatch.tpl

    }

    public function scraping_to_database(){}

    public function scraping_to_xml(){}

    public function scraping_to_tpl(){
        $html = file_get_html( $this->source );

        $html = $html->find( "table.data-table" );

        $html = array_pop( $html );

        if ( trim( strtolower( $html->find( "thead tr th" , 0 )->innertext ) ) != "race" ) { return false; }

        $array_preps = array( 'road' => array(), 'old' => array() );

        foreach ( $html->find( "tbody tr" ) as $key => $tr ) {
            $data         = new stdClass;
            $data->date   = ( $tr->find( "td" , 2 ) ) ? trim( $tr->find( "td" , 2 )->innertext ) : "" ;
            if ( $data->date == "" ) { continue; }
            //$data->race = "<a href='/" . array_pop( explode( "/", $tr->find( 'td' , 0 )->find( 'a' , 0 )->href ) ) . "'>" . trim( $tr->find( 'td' , 0 )->find( 'a' , 0 )->innertext ) . "</a>" ;
            $data->race   = ( $tr->find( "td" , 0 )->find( "a" , 0 ) ) ? $tr->find( "td" , 0 )->find( "a" , 0 ) : $tr->find( "td" , 0 ) ;
            $data->race   = trim( $data->race->innertext );
            $data->track  = trim( $tr->find( "td" , 1 )->innertext );
            $data->points = trim( $tr->find( "td" , 3 )->innertext );
            $array_band = ( strtotime( stripslashes( $data->date ) ) >= strtotime( stripslashes( date( "M j, Y" ) ) ) ) ? "road" : "old" ;
            array_push( $array_preps[ $array_band ] , $data );
        }
        $array_preps[ "old" ] = array_reverse( $array_preps[ "old" ] );

        ob_start();?>
        <div class='tableSchema table-responsive'>
        <table id='infoEntries' width='100%' border='0' cellspacing='0' cellpadding='0' class='table table-condensed table-striped table-bordered' title='Road to the Roses Kentucky Derby Prep Races'>
        <tbody>
        <tr> <th>Date</th> <th>Race</th> <!--<th>Race Distance</th>--> <th>Track</th> <th>Points</th> </tr>
        <?php
        foreach ( $array_preps[ "road" ] as $key => $data ) { ?>
            <tr <?php echo ( ++$key%2 == 0 ) ? "class='odd'" : ""; ?> >
                <td> <?php echo $data->date ?> </td> <td> <?php echo $data->race ?> </td>
                <td> <?php echo $data->track ?> </td> <td> <?php echo $data->points ?> </td>
            </tr><?php
        }?>
        </tbody>
        </table>
        </div>

        <div class='tableSchema table-responsive'>
        <table id='infoEntries' width='100%' border='0' cellspacing='0' cellpadding='0' class='table table-condensed table-striped table-bordered' title='Old Kentucky Derby Prep Races'>
        <tbody>
        <tr>
            <th>Date</th> <th>Race</th> <!--<th>Race Distance</th>--> <th>Track</th> <th>Points</th>
        </tr>

        <?php
        foreach ( $array_preps[ "old" ] as $key => $data ) { ?>
            <tr <?php echo ( ++$key%2 == 0 ) ? "class='odd'" : ""; ?> >
                <td> <?php echo $data->date   ?> </td> <td> <?php echo $data->race   ?> </td>
                <td> <?php echo $data->track  ?> </td> <td> <?php echo $data->points ?> </td>
            </tr><?php
        }?>
        <tr>
            <td class="dateUpdated center" colspan="4">
                <em id='updateemp'>Updated <?php echo date("F j, Y H:i:s");?>.</em>
                BUSR - Official <a href="//www.usracing.com/kentucky-derby/prep-races">Kentucky Derby Preps</a>. <br />
                All Kentucky Derby Preps Races.
            </td>
        </tr>
        </tbody>
        </table>
        </div>
        <?php
        $output = ob_get_clean();
        //echo $output;
        $this->writeOutputXP( $output );
        $this->road_to_the_roses_template( $array_preps['road'] );
    }

    public function road_to_the_roses_template( $array_road ){
        $this->OUTPUTFILEPATH = "/home/ah/allhorse/public_html/kd/road-to-roses.php";

        ob_start();?>
        <div class='tableSchema table-responsive'>
        <table id='infoEntries' width='100%' border='0' cellspacing='0' cellpadding='0' class='table table-condensed table-striped table-bordered' title='Road to the Roses Kentucky Derby Prep Races'>
        <tbody>
        <tr> <th>Date</th> <th>Race</th> <!--<th>Race Distance</th>--> <th>Track</th> <th>Points</th> </tr>
        <?php
        foreach ( $array_road as $key => $data ) { ?>
            <tr <?php echo ( ++$key%2 == 0 ) ? "class='odd'" : ""; ?> >
                <td> <?php echo $data->date ?> </td> <td> <?php echo $data->race ?> </td>
                <td> <?php echo $data->track ?> </td> <td> <?php echo $data->points ?> </td>
            </tr><?php
        }?>
        <tr>
            <td class="dateUpdated center" colspan="4">
                <em id='updateemp'>Updated <?php echo date("F j, Y H:i:s");?>.</em>
                BUSR - Official <a href="//www.usracing.com/kentucky-derby/prep-races">Kentucky Derby Preps</a>. <br />
                All Kentucky Derby Preps Races.
            </td>
        </tr>
        </tbody>
        </table>
        </div>
        <?php
        $output = ob_get_clean();
        $this->writeOutputXP( $output );
    }

    public static function run() {
        try {
            $instance = new self;
            $instance->process();

        } catch ( Exception $e ) { print( $e->getMessage() ); }
    }

    public function process(){
        $this->scraping_to_tpl();
    }

    private function writeOutputXP($contents){

        $update = " - Updated " . date("F j, Y H:i:s");
        if($contents == ""){
            $contents = "The odds are currently being updated, please check back shortly. <em id='updateemp'> $update. </em>";
            if($this->ALWAYSWRITE || !file_exists($this->OUTPUTFILEPATH)){
                if(file_put_contents($this->OUTPUTFILEPATH, $contents)){
                    echo "File " , $this->OUTPUTFILEPATH . " was written with no data.\n";
                    return true;
                }
                else{
                    echo "Error while trying to write file " . $this->OUTPUTFILEPATH . " with no data. Please check permissions.\n";
                    return false;
                }
            }
            else{
                $contents = preg_replace("/(<em\sid=\'updateemp\'>)(.*?)(<\/em>)/", "$1 ".$update." $3", file_get_contents($this->OUTPUTFILEPATH) );
                if($contents !== NULL){
                    if(file_put_contents($this->OUTPUTFILEPATH, $contents)){
                        echo "Empty output, update record updated.\n";
                        return true;
                    }
                    else{
                        echo "Error while trying to write file " . $this->OUTPUTFILEPATH . " with no data but updating update record. Please check permissions.\n";
                        return false;
                    }
                }

                return false;
            }
        }
        else{
            if( file_put_contents($this->OUTPUTFILEPATH, $contents) ){
                echo sprintf( $this->console_color->getColoredString( ">>> " , "cyan" ) . "%'.-110s%-s\n" , $this->console_color->getColoredString( $this->OUTPUTFILEPATH , "red" ) , 'GENERATED');
                return true;
            }
            else{
                echo "Error while trying to write file " . $this->OUTPUTFILEPATH . ". Please check permissions.\n";
                return false;
            }
        }

    }

}

kentucky_prep_races::run();
?>