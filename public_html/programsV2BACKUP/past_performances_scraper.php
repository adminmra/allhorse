
<?php

if ( !defined( 'APP_PATH' ) ) {
    define( 'APP_PATH' , '/home/ah/allhorse/public_html/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

class past_performances_scraper extends Scraper{

    private $ALWAYSWRITE;
    private $OUTPUTFILEPATH;
    private $IdLeague;
    private $data;
    private $mainTitle;

    function __construct(){
        parent::__construct();
        $this->source         = "http://statfeed.statfox.com/webservices/Clients/Sportsbook/statfeed.aspx?page=positiveROI";
        $this->ALWAYSWRITE    = FALSE; //always write eveng with the message "please check back shortly"
        $this->OUTPUTFILEPATH = "/home/ah/allhorse/public_html/schedules/past_performances.php";
        $this->IdLeague       = false;
        $this->data           = array();
        $this->mainTitle      = "";
    }

    public function scraping_to_database(){}

    public function scraping_to_xml(){}

    public function scraping_to_tpl(){
        $html_dom = file_get_html( $this->source );
        $table = $html_dom->find( 'table.datatable tbody', 0 );

        $entries = array();

        // First [TR] tag
        $head = null; $records = array();
        foreach ( $table->children( 0 )->find( "td table tbody tr" ) as $key => $tr ) {
            if (  preg_match( '@row\d+@', $tr->getAttribute( 'class' ) ) ) {
                $record = array(
                    'race' => '', 'horse' => '', 'trainer' => '', 'angle' => '', 'starts' => '', 'win' => '', 'roi' => ''
                    );

                $record[ 'race' ]    =  $tr->find( 'td' , 0 )->innertext;
                $record[ 'horse' ]   =  $tr->find( 'td' , 1 )->innertext;
                $record[ 'trainer' ] =  $tr->find( 'td' , 2 )->innertext;
                $record[ 'angle' ]   =  $tr->find( 'td' , 3 )->innertext;
                $record[ 'starts' ]  =  $tr->find( 'td' , 4 )->innertext;
                $record[ 'win' ]     =  $tr->find( 'td' , 5 )->innertext;
                $record[ 'roi' ]     =  $tr->find( 'td' , 6 )->innertext;
                array_push( $records , (object)$record );
            }
            else if ( $tr->find( "th.header4" ) ){
                if ( $head != null ) {
                    array_push( $entries , (object)array( 'head' => (object)$head , 'records' => (object)$records ) );
                    $records = array();
                    $head = array(
                        'date'  => $tr->find( "th.header4" , 0 )->innertext,
                        'title' => str_replace( "&nbsp;" , ' ' , $tr->find( "th.header4" , 1 )->innertext )
                    );
                }
                else{
                    $head = array(
                        'date'  => $tr->find( "th.header4" , 0 )->innertext,
                        'title' => str_replace( "&nbsp;" , ' ' , $tr->find( "th.header4" , 1 )->innertext )
                        );
                }
            }
        }

        if ( count( $entries ) < 1 ) { return; }

        array_push( $entries , (object)array( 'head' => (object)$head , 'records' => (object)$records ) );

        ob_start();
?>

    <?php
        $len_entries = count( $entries );
        foreach ( $entries as $_key => $entry ) { ?>

        <div class="table-responsive">
            <table border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" summary="<?php echo $entry->head->title ?>">
                <caption><span><?php echo $entry->head->date ?></span> * <span><?php echo $entry->head->title ?></span></caption>
                <tbody>
                    <!--
                    <tr>
                        <th colspan="3" class="center"> * Horses - 2016 Kentucky Derby  - May 06 </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
                    </tr>
                    -->

                    <tr>
                        <th>Race</th>
                        <th>Horse</th>
                        <th>Trainer</th>
                        <th>Angle</th>
                        <th>Starts</th>
                        <th>Win%</th>
                        <th>ROI</th>
                    </tr>

                    <?php foreach( $entry->records as $key => $record ) { ?>
                        <tr>
                            <td><?php  echo $record->race ?>  </td>
                            <td><?php  echo $record->horse ?>   </td>
                            <td><?php  echo $record->trainer ?> </td>
                            <td><?php  echo $record->angle ?></td>
                            <td><?php  echo $record->starts ?></td>
                            <td><?php  echo $record->win ?></td>
                            <td><?php  echo $record->roi ?></td>
                        </tr>
                    <?php } ?>

                    <?php if ( $_key == ( $len_entries - 1 )  ): ?>
                        <tr>
                            <td class="dateUpdated center" colspan="7">
                                <em id='updateemp'>Updated <?php echo date("F j, Y");?>.</em> US Racing <a href="www.usracing.com/past-performances">Past Performances </a>.
                            </td>
                        </tr>
                    <?php endif ?>
                </tbody>
            </table>
        </div>

    <?php } ?>

<?php
        $contents = ob_get_clean();

        //$contents = "<h5>Past Performances are temporarily unavailable, please check back soon</h5>";

        if( file_put_contents( $this->OUTPUTFILEPATH, $contents ) ){
            print( sprintf(
                $this->console_color->getColoredString( ">>> " , "cyan" ) . "%'.-160s%-s\n\n" ,
                $this->console_color->getColoredString( $this->OUTPUTFILEPATH , "red" ) ,
                'GENERATED'
            ) );
            return true;
        }
    }

    public static function run() {
        try {
            $instance = new self;
            $instance->scraping_to_tpl();
        } catch (Exception $e) {
            print( $e->getMessage() );
        }
    }
}

past_performances_scraper::run();

?>