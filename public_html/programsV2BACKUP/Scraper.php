<?php

//namespace Scraper;
require_once APP_PATH . 'simple_html_dom.php';
require_once APP_PATH . 'ConsoleColors.php';
require_once APP_PATH . 'Database.php';


define( 'TEMPLATE_DIR'    , "/home/ah/usracing.com/smarty/templates/includes/" );

define( 'EOF'      , "<br />\n");

abstract class Scraper {

	protected $source;
	protected $output;
	protected $html_parser;
	protected $console_color;

	function __construct(){
		$this->source         = null;
		$this->output         = null;
		$this->console_color = new ConsoleColors();
	}

	abstract public function scraping_to_database();

	abstract public function scraping_to_xml();

	abstract public function scraping_to_tpl();
}


?>