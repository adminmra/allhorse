
<?php

ini_set( 'display_errors', 1 );
ini_set( 'log_errors' , 1 );
error_reporting( E_ALL );

if ( !defined( 'APP_PATH' ) ) {
	define( 'APP_PATH' , '/home/ah/allhorse/public_html/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';

class GeneraXmlScraper extends Scraper{

	function __construct(){
		parent::__construct();
	}

	public function scraping_to_database(){}

	public function scraping_to_xml(){

		if ( !defined( "OUTPUTABSPATH" ) ) {
			define("OUTPUTABSPATH", "/home/ah/allhorse/public_html/output/newsxml/");
		}

		$index    = 0;
		$multiplo = 10;
		$prefix   = "news";

		$query = "SELECT timecreate FROM newsarchive ORDER BY timecreate desc LIMIT 0,1";
		$ans   = DB::query( $query );
		$ans   = $ans->fetch_assoc();
		//echo $ans['timecreate'] . EOF;

		$dateFrom = strtotime( $ans['timecreate'] ) - ( 365*24*60*60 );
		$dateFrom = date( 'Y-m-d H:i:s', $dateFrom );
		//echo $dateFrom . EOF;

		$query     = sprintf( " SELECT count(pageuri) AS totalc FROM newsarchive WHERE timecreate>='%s' ", $dateFrom );
		$ans       = DB::query( $query );
		$ans       = $ans->fetch_assoc();
		$totalNews = $ans['totalc'];
		//echo $totalNews . EOF;

		$totalPages = (integer)( $totalNews/10 );
		if( $totalNews%$multiplo > 0 )
			$totalPages++;
		//echo $totalPages . EOF;

		if( $totalPages > 0 )
			array_map( 'unlink', glob(OUTPUTABSPATH."*") ); // WARNING use it carefully

		for ( $page = 1 ; $page <= $totalPages ; $page++) {
			$filename = OUTPUTABSPATH . $prefix . $page . ".xml";
			//echo "Writting ".$filename.EOF;
			$query   = sprintf(" SELECT * FROM newsarchive WHERE timecreate>='%s' ORDER BY timecreate DESC LIMIT %d, %d ", $dateFrom, $index, $multiplo);
			$results = DB::query( $query );

			$xml      = new DOMDocument();
			$xml_root = $xml->createElement("filexml");

			while ($row = $results->fetch_assoc() ) {
				//echo $row['ContentId'].EOF;
				//echo $row['Teaser'].EOF;
				//echo $row['Title'].EOF;
				//echo $row['pageuri'].EOF;
				//echo $row['DateCreated'].EOF;

				$query2  = sprintf("SELECT HtmlArt FROM newsarticle WHERE ContentIdArt=%d", $row['ContentId']);
				$result  = DB::query( $query2 );
				$HtmlArt = $result->fetch_object();

				$xml_item        = $xml->createElement("item");
				$xml_ID          = $xml->createElement("ContentId");
				$xml_Title       = $xml->createElement("Title");
				$xml_Teaser      = $xml->createElement("Teaser");
				$xml_pageuri     = $xml->createElement("pageuri");
				$xml_DateCreated = $xml->createElement("DateCreated");
				$xml_Article     = $xml->createElement("Article");
				$cdataNode       = $xml->createCDATASection($HtmlArt->HtmlArt);

				$xml_ID->nodeValue          = $row['ContentId'];
				$xml_Title->nodeValue       = $row['Title'];
				$xml_Teaser->nodeValue      = $row['Teaser'];
				$xml_pageuri->nodeValue     = $row['pageuri'];
				$xml_DateCreated->nodeValue = $row['DateCreated'];
				$xml_Article->appendChild($cdataNode);

				$xml_item->appendChild( $xml_ID );
				$xml_item->appendChild( $xml_Title );
				$xml_item->appendChild( $xml_Teaser );
				$xml_item->appendChild( $xml_pageuri );
				$xml_item->appendChild( $xml_DateCreated );
				$xml_item->appendChild( $xml_Article );

				$xml_root->appendChild( $xml_item );
			}

			$xml->appendChild( $xml_root );
			$xml->save( $filename );

			print( sprintf(
				$this->console_color->getColoredString( ">>> " , "cyan" ) . "%'.-160s%-s\n\n" ,
				$this->console_color->getColoredString( $filename , "red" ) ,
				'GENERATED'
			) );

			$index += $multiplo;
		}

	}

	public function scraping_to_tpl(){}

	public static function run() {
		try {
			$instance = new self;
			$instance->scraping_to_xml();
		} catch (Exception $e) {
			print_r( $e );

		}
	}

}

GeneraXmlScraper::run();

?>
