
<?php

if ( !defined( 'APP_PATH' ) ) {
    define( 'APP_PATH' , '/home/ah/allhorse/public_html/programsV2/' );
    define( 'TEMPLATE_DIR'    , "/home/ah/usracing.com/smarty/templates/includes/" );
}

require_once APP_PATH . 'Scraper.php';

class harness_racing_scraper extends Scraper{

    private $ALWAYSWRITE;
    private $OUTPUTFILEPATH;
    private $IdLeague;
    private $data;
    private $mainTitle;

    function __construct(){
        parent::__construct();
        $this->source         = "http://www.horsebetting.com/harness-racing/stakes-schedule";
        $this->ALWAYSWRITE    = FALSE; //always write eveng with the message "please check back shortly"
        //$this->OUTPUTFILEPATH = "/home/ah/allhorse/public_html/schedules/harness_racing_schedule.php";
        $this->OUTPUTFILEPATH = "/home/ah/allhorse/public_html/generated/racing_schedules/harness_racing_schedule.php";
        $this->IdLeague       = false;
        $this->data           = array();
        $this->mainTitle      = "";
    }

    public function scraping_to_database(){}

    public function scraping_to_xml(){}

    public function scraping_to_tpl(){
        $html_dom = file_get_html( $this->source );
        $table = $html_dom->find( "#tableSCH tbody tr" );

        $entries = array();
        $date = null;
        $fills = array();

        foreach ( $table as $key => $tr ) {

            if ( $tr->find( 'td' ) ) {
                /*
                if ( $date != trim( $tr->find( 'td' , 0 )->innertext ) ) {
                    array_push(
                        $entries ,
                        (object) array(
                            'date' => $date,
                            'records' => $fills
                            )
                        );
                    $fills = array();
                    $date = trim( $tr->find( 'td' , 0 )->innertext );
                }*/

                $fill = array(
                    'date'    => ucwords( strtolower( $tr->find( 'td', 0 )->innertext ) ),
                    'race'    => ucwords( strtolower( $tr->find( 'td', 1 )->innertext ) ),
                    'track'   => ucwords( strtolower( $tr->find( 'td', 2 )->innertext ) ),
                    'age_sex' => ucwords( strtolower( $tr->find( 'td', 3 )->innertext ) ),
                    'purse'   => ucwords( strtolower( $tr->find( 'td', 4 )->innertext ) )
                );
                //array_push( $fills , (object) $fill );
                array_push( $entries , (object)$fill );
            }
        }

        //$entries = array_slice( $entries , 1 );

        ob_start();
?>


<style>

{literal}
    /* TABLES AND DATA ================================================================================ */
/*
    #infoEntries, #raceEntries, #raceTimes { margin: 10px 0 0 0; padding: 0; width: 100%; }
    #infoEntries tr, #raceEntries tr, #raceTimes tr, #infoEntries tbody { border: none;}
    #infoEntries th, #raceEntries th { border-right: 1px solid #6ca8ee; padding: 7px 10px; color: #FFF; font-size: 1em; background: #105ca9; }
    #infoEntries td, #raceEntries td {  padding: 10px; font-size: 1em;}
    #infoEntries td.odd, #raceEntries tr.AlternateRow  { background: #e5e5e5; }
    #infoEntries .td-box {  display: block; border-bottom: 1px solid #d9d9d9; padding: 9px 10px; }
    #infoEntries .td-box:hover {  display: block; background: #d4e9ff; }

    #raceTimes table td { padding: 10px; font-size: 1em; border: none; }
    #raceTimes, #raceTimes table { border: none;  }
    #raceTimes table tr.odd { background: #d4e9ff;  }
    #raceTimes td.num { font-weight: bold; }
    #raceTimes tbody, #raceTimes table tbody { border: none; }

    .page-racingschedule #infoEntries td { width:14.2%;}
    .page-racingschedule #infoEntries th { font-size: .917em; }
    */
{/literal}

</style>


<div class="table-responsive">
    <table border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" summary="* Harness Racing Schedule">
        <caption>* Harness Racing Schedule</caption>
         <tbody>
            <!--
            <tr>
                <th colspan="3" class="center"> * Harness Racing Schedule * Horses - 2016 Kentucky Derby  - May 06 </th>
            </tr>
            <tr>
                <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
            </tr>
            -->

           <tr>
             <th>Date</th>
             <th>Race</th>
             <th>Track</th>
             <th>Age & Sex</th>
             <th>Purse</th>
           </tr>

        <?php
            foreach ( $entries as $key => $entry ) { ?>

                <tr>
                    <td ><strong><?php echo $entry->date; ?></strong></td>
                    <td> <?php echo $entry->race    ?> </td>
                    <td> <?php echo $this->parse_racetrack_link( $entry->track ) ?> </td>
                    <td> <?php echo $entry->age_sex ?> </td>
                    <td> <?php echo $entry->purse   ?> </td>
                </tr>
            <?php
            }
         ?>

        <tr>
            <td class="dateUpdated center" colspan="5">
                <em id='updateemp'>Updated <?php echo date("F j, Y");?>.</em> US Racing <a href="//www.usracing.com/harness-racing-schedule">Harness Racing Schedule</a>.
            </td>
        </tr>
        </tbody>
    </table>
    <a href="//www.usracing.com" style="color:white;">Online Horse Betting</a>
</div>

<?php
        $contents = ob_get_clean();

        if( file_put_contents( $this->OUTPUTFILEPATH, $contents ) ){
            print( sprintf(
                $this->console_color->getColoredString( ">>> " , "cyan" ) . "%'.-160s%-s\n\n" ,
                $this->console_color->getColoredString( $this->OUTPUTFILEPATH , "red" ) ,
                'GENERATED'
            ) );
            return true;
        }
    }

    public static function run() {
        try {
            $instance = new self;
            $instance->scraping_to_tpl();
        } catch (Exception $e) {
            print( $e->getMessage() );
        }
    }

    private function parse_racetrack_link( $link_name ){


        $link_name = utf8_encode( $link_name );
        $link_racetrack =  str_replace( "&rsquo;" , '', trim( $link_name ) );
        $link_racetrack =  strtolower( preg_replace( "@-\s.*@" , '', trim( $link_racetrack ) ) );
        $link_racetrack =  preg_replace( "@\s[&]\s|\s+@" , '-', trim( $link_racetrack ) );
        $link_racetrack =  preg_replace( "@-\(.*@" , '', trim( $link_racetrack ) );


        if ( $link_racetrack == 'santa-anita-park' ) {
            $link_racetrack = str_replace('-park', '', $link_racetrack );
        }else if ( $link_racetrack == 'woodbine-raceway' ) {
            $link_racetrack = str_replace('-raceway', '', $link_racetrack );
        }
        else if ( $link_racetrack == 'parx-racing-at-philadelphia-park' ) {
            $link_racetrack = trim( str_replace('-at-philadelphia-park', '', $link_racetrack ) );
        }
        else if ( $link_racetrack == 'the-meadowlands' ) {
            $link_racetrack = trim( str_replace('the-', '', $link_racetrack ) );
        }
        else if ( $link_racetrack == 'hoosier-park-harness' ) {
            $link_racetrack = trim( str_replace('-harness', '', $link_racetrack ) );
        }

        $file_exist = file_exists( TEMPLATE_DIR .'../content/racetrack/'.$link_racetrack.'.tpl' );
        return (  $file_exist ) ? "<a href='//www.usracing.com/".$link_racetrack."'>{$link_name}</a>" : $link_name;
    }

}


harness_racing_scraper::run();

?>
