
    
        <div class="table-responsive">
            <table border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="EVD  -  Evangeline Downs Past Performances" summary="The  Past Performances for EVD  -  Evangeline Downs Racetrack for 04/26/2018 ">
                <caption><span>04/26/2018</span> * <span>EVD  -  Evangeline Downs</span></caption>
                <tbody>
                    <!--
                    <tr>
                        <th colspan="3" class="center"> * Horses - 2016 Kentucky Derby  - May 06 </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
                    </tr>
                    -->

                    <tr>
                        <th>Race</th>
                        <th>Horse</th>
                        <th>Trainer</th>
                        <th>Angle</th>
                        <th>Starts</th>
                        <th>Win%</th>
                        <th>ROI</th>
                    </tr>

                                            <tr>
                            <td>8  </td>
                            <td>Grande Basin   </td>
                            <td>Johnston E </td>
                            <td>Dirt Races</td>
                            <td>120</td>
                            <td>0.26</td>
                            <td>2.33</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Guitarist   </td>
                            <td>Gelner S </td>
                            <td>	First with new trainer</td>
                            <td>27</td>
                            <td>0.30</td>
                            <td>2.83</td>
                        </tr>
                    
                                    </tbody>
            </table>
        </div>

    
        <div class="table-responsive">
            <table border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="GG   -  Golden Gate Fields Past Performances" summary="The  Past Performances for GG   -  Golden Gate Fields Racetrack for 04/26/2018 ">
                <caption><span>04/26/2018</span> * <span>GG   -  Golden Gate Fields</span></caption>
                <tbody>
                    <!--
                    <tr>
                        <th colspan="3" class="center"> * Horses - 2016 Kentucky Derby  - May 06 </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
                    </tr>
                    -->

                    <tr>
                        <th>Race</th>
                        <th>Horse</th>
                        <th>Trainer</th>
                        <th>Angle</th>
                        <th>Starts</th>
                        <th>Win%</th>
                        <th>ROI</th>
                    </tr>

                                            <tr>
                            <td>4  </td>
                            <td>Lord of Chaos   </td>
                            <td>Morey W </td>
                            <td>Claiming races</td>
                            <td>199</td>
                            <td>0.25</td>
                            <td>2.05</td>
                        </tr>
                                            <tr>
                            <td>5  </td>
                            <td>Malibu Alex   </td>
                            <td>Morey W </td>
                            <td>Claiming races</td>
                            <td>199</td>
                            <td>0.25</td>
                            <td>2.05</td>
                        </tr>
                                            <tr>
                            <td>7  </td>
                            <td>Silken Queen   </td>
                            <td>Bautista J </td>
                            <td>Layoff 31-60 days</td>
                            <td>38</td>
                            <td>0.26</td>
                            <td>4.59</td>
                        </tr>
                                            <tr>
                            <td>5  </td>
                            <td>Silken Chief   </td>
                            <td>Bautista J </td>
                            <td>Allowance races</td>
                            <td>7</td>
                            <td>0.43</td>
                            <td>4.54</td>
                        </tr>
                                            <tr>
                            <td>4  </td>
                            <td>Gurnick   </td>
                            <td>Calvario S </td>
                            <td>Turf Races</td>
                            <td>20</td>
                            <td>0.30</td>
                            <td>3.11</td>
                        </tr>
                                            <tr>
                            <td>4  </td>
                            <td>Gurnick   </td>
                            <td>Calvario S </td>
                            <td>Route Races</td>
                            <td>43</td>
                            <td>0.30</td>
                            <td>2.67</td>
                        </tr>
                                            <tr>
                            <td>6  </td>
                            <td>If You Like It   </td>
                            <td>Amescua R </td>
                            <td>Record off of a win</td>
                            <td>39</td>
                            <td>0.26</td>
                            <td>2.76</td>
                        </tr>
                                            <tr>
                            <td>6  </td>
                            <td>If You Like It   </td>
                            <td>Amescua R </td>
                            <td>Turf Races</td>
                            <td>20</td>
                            <td>0.40</td>
                            <td>4.73</td>
                        </tr>
                                            <tr>
                            <td>1  </td>
                            <td>Boldly True   </td>
                            <td>Maelfeyt B </td>
                            <td>Sprint Races</td>
                            <td>47</td>
                            <td>0.28</td>
                            <td>2.13</td>
                        </tr>
                                            <tr>
                            <td>1  </td>
                            <td>Boldly True   </td>
                            <td>Maelfeyt B </td>
                            <td>Claiming races</td>
                            <td>37</td>
                            <td>0.30</td>
                            <td>2.14</td>
                        </tr>
                                            <tr>
                            <td>7  </td>
                            <td>Phantom Flyer   </td>
                            <td>Evans H </td>
                            <td>Debut in Maiden Claiming races</td>
                            <td>7</td>
                            <td>0.43</td>
                            <td>27.80</td>
                        </tr>
                                            <tr>
                            <td>7  </td>
                            <td>Phantom Flyer   </td>
                            <td>Evans H </td>
                            <td>Maiden claiming races</td>
                            <td>18</td>
                            <td>0.33</td>
                            <td>14.17</td>
                        </tr>
                                            <tr>
                            <td>5  </td>
                            <td>Awhitesportscoat   </td>
                            <td>Wright B </td>
                            <td>Layoff 31-60 days</td>
                            <td>140</td>
                            <td>0.30</td>
                            <td>2.42</td>
                        </tr>
                    
                                    </tbody>
            </table>
        </div>

    
        <div class="table-responsive">
            <table border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="GP   -  Gulfstream Park Past Performances" summary="The  Past Performances for GP   -  Gulfstream Park Racetrack for 04/26/2018 ">
                <caption><span>04/26/2018</span> * <span>GP   -  Gulfstream Park</span></caption>
                <tbody>
                    <!--
                    <tr>
                        <th colspan="3" class="center"> * Horses - 2016 Kentucky Derby  - May 06 </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
                    </tr>
                    -->

                    <tr>
                        <th>Race</th>
                        <th>Horse</th>
                        <th>Trainer</th>
                        <th>Angle</th>
                        <th>Starts</th>
                        <th>Win%</th>
                        <th>ROI</th>
                    </tr>

                                            <tr>
                            <td>3  </td>
                            <td>Dreaminofauntantil   </td>
                            <td>Rodriguez A </td>
                            <td>Switch from turf to dirt</td>
                            <td>33</td>
                            <td>0.27</td>
                            <td>2.12</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Dreaminofauntantil   </td>
                            <td>Rodriguez A </td>
                            <td>Sprint Races</td>
                            <td>190</td>
                            <td>0.27</td>
                            <td>2.29</td>
                        </tr>
                                            <tr>
                            <td>2  </td>
                            <td>Honor Life   </td>
                            <td>Thomas J </td>
                            <td>Second Start</td>
                            <td>15</td>
                            <td>0.47</td>
                            <td>3.37</td>
                        </tr>
                                            <tr>
                            <td>2  </td>
                            <td>Honor Life   </td>
                            <td>Thomas J </td>
                            <td>Maiden special weight to Maiden claiming</td>
                            <td>7</td>
                            <td>0.43</td>
                            <td>2.40</td>
                        </tr>
                                            <tr>
                            <td>2  </td>
                            <td>Honor Life   </td>
                            <td>Thomas J </td>
                            <td>Switch from turf to dirt</td>
                            <td>8</td>
                            <td>0.50</td>
                            <td>3.30</td>
                        </tr>
                                            <tr>
                            <td>2  </td>
                            <td>Honor Life   </td>
                            <td>Thomas J </td>
                            <td>Maiden claiming races</td>
                            <td>30</td>
                            <td>0.30</td>
                            <td>2.03</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Second Illusion   </td>
                            <td>Gonzalez O </td>
                            <td>Record off of a win</td>
                            <td>39</td>
                            <td>0.26</td>
                            <td>3.04</td>
                        </tr>
                                            <tr>
                            <td>1  </td>
                            <td>Luckbe Tanya   </td>
                            <td>Barboza V Jr </td>
                            <td>Layoff 31-60 days</td>
                            <td>129</td>
                            <td>0.28</td>
                            <td>2.08</td>
                        </tr>
                    
                                    </tbody>
            </table>
        </div>

    
        <div class="table-responsive">
            <table border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="HAW  -  Hawthorne Past Performances" summary="The  Past Performances for HAW  -  Hawthorne Racetrack for 04/26/2018 ">
                <caption><span>04/26/2018</span> * <span>HAW  -  Hawthorne</span></caption>
                <tbody>
                    <!--
                    <tr>
                        <th colspan="3" class="center"> * Horses - 2016 Kentucky Derby  - May 06 </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
                    </tr>
                    -->

                    <tr>
                        <th>Race</th>
                        <th>Horse</th>
                        <th>Trainer</th>
                        <th>Angle</th>
                        <th>Starts</th>
                        <th>Win%</th>
                        <th>ROI</th>
                    </tr>

                                            <tr>
                            <td>6  </td>
                            <td>Dancing Position   </td>
                            <td>Becker S </td>
                            <td>Allowance races</td>
                            <td>82</td>
                            <td>0.33</td>
                            <td>2.08</td>
                        </tr>
                                            <tr>
                            <td>6  </td>
                            <td>Gita's Lad   </td>
                            <td>Becker S </td>
                            <td>Allowance races</td>
                            <td>82</td>
                            <td>0.33</td>
                            <td>2.08</td>
                        </tr>
                                            <tr>
                            <td>1  </td>
                            <td>Back Page Star   </td>
                            <td>Roussel L III </td>
                            <td>	Layoff plus 61 - 180 days</td>
                            <td>42</td>
                            <td>0.26</td>
                            <td>2.19</td>
                        </tr>
                                            <tr>
                            <td>8  </td>
                            <td>Kentucky Shine   </td>
                            <td>De La Mora R </td>
                            <td>Dirt Races</td>
                            <td>4</td>
                            <td>0.75</td>
                            <td>9.60</td>
                        </tr>
                                            <tr>
                            <td>8  </td>
                            <td>Kentucky Shine   </td>
                            <td>De La Mora R </td>
                            <td>Sprint Races</td>
                            <td>4</td>
                            <td>0.75</td>
                            <td>9.60</td>
                        </tr>
                                            <tr>
                            <td>8  </td>
                            <td>Kentucky Shine   </td>
                            <td>De La Mora R </td>
                            <td>Claiming races</td>
                            <td>4</td>
                            <td>0.75</td>
                            <td>9.60</td>
                        </tr>
                                            <tr>
                            <td>2  </td>
                            <td>Mendota   </td>
                            <td>Matthews D </td>
                            <td>Dirt Races</td>
                            <td>58</td>
                            <td>0.28</td>
                            <td>2.41</td>
                        </tr>
                                            <tr>
                            <td>4  </td>
                            <td>Savemethelastdance   </td>
                            <td>Perez M </td>
                            <td>	First start off the claim</td>
                            <td>10</td>
                            <td>0.40</td>
                            <td>3.40</td>
                        </tr>
                    
                                    </tbody>
            </table>
        </div>

    
        <div class="table-responsive">
            <table border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="KEE  -  Keeneland Past Performances" summary="The  Past Performances for KEE  -  Keeneland Racetrack for 04/26/2018 ">
                <caption><span>04/26/2018</span> * <span>KEE  -  Keeneland</span></caption>
                <tbody>
                    <!--
                    <tr>
                        <th colspan="3" class="center"> * Horses - 2016 Kentucky Derby  - May 06 </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
                    </tr>
                    -->

                    <tr>
                        <th>Race</th>
                        <th>Horse</th>
                        <th>Trainer</th>
                        <th>Angle</th>
                        <th>Starts</th>
                        <th>Win%</th>
                        <th>ROI</th>
                    </tr>

                                            <tr>
                            <td>3  </td>
                            <td>El Charro   </td>
                            <td>Cox B </td>
                            <td>2Sprints/Route</td>
                            <td>12</td>
                            <td>0.50</td>
                            <td>4.42</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>El Charro   </td>
                            <td>Cox B </td>
                            <td>Switch from sprint to route</td>
                            <td>93</td>
                            <td>0.31</td>
                            <td>2.35</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>El Charro   </td>
                            <td>Cox B </td>
                            <td>Route Races</td>
                            <td>633</td>
                            <td>0.28</td>
                            <td>2.02</td>
                        </tr>
                                            <tr>
                            <td>5  </td>
                            <td>Mount Calvary   </td>
                            <td>Young T </td>
                            <td>Layoff 31-60 days</td>
                            <td>10</td>
                            <td>0.40</td>
                            <td>13.90</td>
                        </tr>
                                            <tr>
                            <td>5  </td>
                            <td>Mount Calvary   </td>
                            <td>Young T </td>
                            <td>Sprint Races</td>
                            <td>32</td>
                            <td>0.25</td>
                            <td>2.18</td>
                        </tr>
                                            <tr>
                            <td>4  </td>
                            <td>Egoli   </td>
                            <td>Ward W </td>
                            <td>First Start</td>
                            <td>100</td>
                            <td>0.32</td>
                            <td>2.17</td>
                        </tr>
                                            <tr>
                            <td>4  </td>
                            <td>Mucho Amor   </td>
                            <td>Ward W </td>
                            <td>First Start</td>
                            <td>100</td>
                            <td>0.32</td>
                            <td>2.17</td>
                        </tr>
                                            <tr>
                            <td>6  </td>
                            <td>Moonlight Romance   </td>
                            <td>Ward W </td>
                            <td>First Start</td>
                            <td>100</td>
                            <td>0.32</td>
                            <td>2.17</td>
                        </tr>
                                            <tr>
                            <td>6  </td>
                            <td>Shang Shang Shang   </td>
                            <td>Ward W </td>
                            <td>First Start</td>
                            <td>100</td>
                            <td>0.32</td>
                            <td>2.17</td>
                        </tr>
                                            <tr>
                            <td>6  </td>
                            <td>Stellar Stiletto   </td>
                            <td>Macias A </td>
                            <td>Second Start</td>
                            <td>7</td>
                            <td>0.43</td>
                            <td>12.71</td>
                        </tr>
                    
                                    </tbody>
            </table>
        </div>

    
        <div class="table-responsive">
            <table border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="PEN  -  Penn National Past Performances" summary="The  Past Performances for PEN  -  Penn National Racetrack for 04/26/2018 ">
                <caption><span>04/26/2018</span> * <span>PEN  -  Penn National</span></caption>
                <tbody>
                    <!--
                    <tr>
                        <th colspan="3" class="center"> * Horses - 2016 Kentucky Derby  - May 06 </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
                    </tr>
                    -->

                    <tr>
                        <th>Race</th>
                        <th>Horse</th>
                        <th>Trainer</th>
                        <th>Angle</th>
                        <th>Starts</th>
                        <th>Win%</th>
                        <th>ROI</th>
                    </tr>

                                            <tr>
                            <td>2  </td>
                            <td>Business Partner   </td>
                            <td>Houghton T </td>
                            <td>Switch from turf to dirt</td>
                            <td>16</td>
                            <td>0.38</td>
                            <td>4.59</td>
                        </tr>
                                            <tr>
                            <td>8  </td>
                            <td>Surprise Talent   </td>
                            <td>Houghton T </td>
                            <td>First start with blinkers</td>
                            <td>22</td>
                            <td>0.32</td>
                            <td>6.16</td>
                        </tr>
                                            <tr>
                            <td>8  </td>
                            <td>Surprise Talent   </td>
                            <td>Houghton T </td>
                            <td>Blinkers on</td>
                            <td>22</td>
                            <td>0.32</td>
                            <td>6.16</td>
                        </tr>
                                            <tr>
                            <td>4  </td>
                            <td>Duel At Dusk   </td>
                            <td>Pino M </td>
                            <td>Switch from sprint to route</td>
                            <td>33</td>
                            <td>0.33</td>
                            <td>2.72</td>
                        </tr>
                                            <tr>
                            <td>2  </td>
                            <td>Skipper Dancer   </td>
                            <td>Delgado J </td>
                            <td>Dirt Races</td>
                            <td>32</td>
                            <td>0.34</td>
                            <td>3.86</td>
                        </tr>
                                            <tr>
                            <td>2  </td>
                            <td>Skipper Dancer   </td>
                            <td>Delgado J </td>
                            <td>Claiming races</td>
                            <td>21</td>
                            <td>0.43</td>
                            <td>5.16</td>
                        </tr>
                    
                                    </tbody>
            </table>
        </div>

    
        <div class="table-responsive">
            <table border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="PRM  -  Prairie Meadows Past Performances" summary="The  Past Performances for PRM  -  Prairie Meadows Racetrack for 04/26/2018 ">
                <caption><span>04/26/2018</span> * <span>PRM  -  Prairie Meadows</span></caption>
                <tbody>
                    <!--
                    <tr>
                        <th colspan="3" class="center"> * Horses - 2016 Kentucky Derby  - May 06 </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
                    </tr>
                    -->

                    <tr>
                        <th>Race</th>
                        <th>Horse</th>
                        <th>Trainer</th>
                        <th>Angle</th>
                        <th>Starts</th>
                        <th>Win%</th>
                        <th>ROI</th>
                    </tr>

                                            <tr>
                            <td>3  </td>
                            <td>Tre Lee Divine   </td>
                            <td>Morse R </td>
                            <td>Switch from route to sprint</td>
                            <td>33</td>
                            <td>0.30</td>
                            <td>2.96</td>
                        </tr>
                                            <tr>
                            <td>9  </td>
                            <td>Back the Blue   </td>
                            <td>Von Hemel K </td>
                            <td>First Start</td>
                            <td>21</td>
                            <td>0.33</td>
                            <td>4.09</td>
                        </tr>
                    
                                    </tbody>
            </table>
        </div>

    
        <div class="table-responsive">
            <table border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="SA   -  Santa Anita Past Performances" summary="The  Past Performances for SA   -  Santa Anita Racetrack for 04/26/2018 ">
                <caption><span>04/26/2018</span> * <span>SA   -  Santa Anita</span></caption>
                <tbody>
                    <!--
                    <tr>
                        <th colspan="3" class="center"> * Horses - 2016 Kentucky Derby  - May 06 </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
                    </tr>
                    -->

                    <tr>
                        <th>Race</th>
                        <th>Horse</th>
                        <th>Trainer</th>
                        <th>Angle</th>
                        <th>Starts</th>
                        <th>Win%</th>
                        <th>ROI</th>
                    </tr>

                                            <tr>
                            <td>8  </td>
                            <td>Bring On The Band (IRE)   </td>
                            <td>Gallagher P </td>
                            <td>First time starter in North America</td>
                            <td>6</td>
                            <td>0.50</td>
                            <td>7.87</td>
                        </tr>
                                            <tr>
                            <td>8  </td>
                            <td>Bring On The Band (IRE)   </td>
                            <td>Gallagher P </td>
                            <td>	First with new trainer</td>
                            <td>8</td>
                            <td>0.38</td>
                            <td>5.90</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Shaula   </td>
                            <td>Avalos J </td>
                            <td>Layoff 31-60 days</td>
                            <td>32</td>
                            <td>0.41</td>
                            <td>2.74</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Shaula   </td>
                            <td>Avalos J </td>
                            <td>Dirt Races</td>
                            <td>99</td>
                            <td>0.31</td>
                            <td>2.66</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Shaula   </td>
                            <td>Avalos J </td>
                            <td>Sprint Races</td>
                            <td>99</td>
                            <td>0.30</td>
                            <td>2.39</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Shaula   </td>
                            <td>Avalos J </td>
                            <td>Claiming races</td>
                            <td>95</td>
                            <td>0.32</td>
                            <td>2.49</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Southern Treasure   </td>
                            <td>Trela R </td>
                            <td>	Layoff plus 61 - 180 days</td>
                            <td>4</td>
                            <td>0.75</td>
                            <td>6.05</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Southern Treasure   </td>
                            <td>Trela R </td>
                            <td>Dirt Races</td>
                            <td>32</td>
                            <td>0.28</td>
                            <td>2.03</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Southern Treasure   </td>
                            <td>Trela R </td>
                            <td>Claiming races</td>
                            <td>24</td>
                            <td>0.38</td>
                            <td>2.70</td>
                        </tr>
                                            <tr>
                            <td>4  </td>
                            <td>Ib Prospecting   </td>
                            <td>Eurton P </td>
                            <td>Switch from turf to dirt</td>
                            <td>27</td>
                            <td>0.41</td>
                            <td>4.33</td>
                        </tr>
                                            <tr>
                            <td>7  </td>
                            <td>Upper Room   </td>
                            <td>Eurton P </td>
                            <td>Blinkers off</td>
                            <td>7</td>
                            <td>0.57</td>
                            <td>6.50</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Kiss My Lulu   </td>
                            <td>Morey W </td>
                            <td>Claiming races</td>
                            <td>199</td>
                            <td>0.25</td>
                            <td>2.05</td>
                        </tr>
                                            <tr>
                            <td>2  </td>
                            <td>Tidal Effect   </td>
                            <td>Callaghan S </td>
                            <td>Switch from route to sprint</td>
                            <td>15</td>
                            <td>0.40</td>
                            <td>3.82</td>
                        </tr>
                                            <tr>
                            <td>2  </td>
                            <td>Tidal Effect   </td>
                            <td>Callaghan S </td>
                            <td>Maiden claiming races</td>
                            <td>25</td>
                            <td>0.32</td>
                            <td>3.89</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Forthenineteen   </td>
                            <td>Vallejo G </td>
                            <td>	First start off the claim</td>
                            <td>7</td>
                            <td>0.43</td>
                            <td>2.57</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Forthenineteen   </td>
                            <td>Vallejo G </td>
                            <td>Dirt Races</td>
                            <td>68</td>
                            <td>0.25</td>
                            <td>2.28</td>
                        </tr>
                                            <tr>
                            <td>3  </td>
                            <td>Forthenineteen   </td>
                            <td>Vallejo G </td>
                            <td>Sprint Races</td>
                            <td>52</td>
                            <td>0.27</td>
                            <td>2.09</td>
                        </tr>
                                            <tr>
                            <td>1  </td>
                            <td>Giant Mongolian   </td>
                            <td>Ganbat E </td>
                            <td>Record off of a win</td>
                            <td>16</td>
                            <td>0.31</td>
                            <td>8.06</td>
                        </tr>
                                            <tr>
                            <td>7  </td>
                            <td>Mongolian Greywolf   </td>
                            <td>Ganbat E </td>
                            <td>	Layoff plus 61 - 180 days</td>
                            <td>7</td>
                            <td>0.43</td>
                            <td>3.23</td>
                        </tr>
                                            <tr>
                            <td>5  </td>
                            <td>Allaboutmike   </td>
                            <td>Miller P </td>
                            <td>Claiming races</td>
                            <td>218</td>
                            <td>0.29</td>
                            <td>2.07</td>
                        </tr>
                                            <tr>
                            <td>5  </td>
                            <td>Home Run Kitten   </td>
                            <td>Miller P </td>
                            <td>Claiming races</td>
                            <td>218</td>
                            <td>0.29</td>
                            <td>2.07</td>
                        </tr>
                    
                                            <tr>
                            <td class="dateUpdated center" colspan="7">
                                <em id='updateemp'>Updated April 26, 2018.</em> US Racing <a href="https://www.usracing.com/past-performances">Past Performances </a>.
                            </td>
                        </tr>
                                    </tbody>
            </table>
        </div>

    
