<!-- debuggin here -->
  
      <table id="infoEntries" class="horse-betting" border="0" cellpadding="0" cellspacing="0" title="Road to the Roses Kentucky Derby Prep Races" >
    <thead>
    <tr>
              <th>
          Race Date        </th>
              <th>
          Title        </th>
              <th >
          Grade        </th>
              <th >
          Race Distance        </th>
              <th >
          Track        </th>
              <th >
          Points        </th>
          </tr>
  </thead>
  <tbody>
<tr class="odd">
                  <td>&nbsp;
</td>
                  <td>&nbsp;
</td>
                  <td >
            II          </td>
                  <td >
            1 M          </td>
                  <td >
            Newmarket          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr >
                  <td>
            September 29, 2012</td>
                  <td>
            Frontrunner</a>          </td>
                  <td >
            I          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Santa Anita Park          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr class="odd">
                  <td>
            October 6, 2012</td>
                  <td>Foxwoods Champagne          </td>
                  <td >
            I          </td>
                  <td >
            1 M          </td>
                  <td >
            Belmont Park          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr >
                  <td>
            October 6, 2012</td>
                  <td>
           Dixiana Breeders' Futurity</a>          </td>
                  <td >
            I          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Keeneland          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr class="odd">
                  <td>
            October 7, 2012</td>
                  <td>Grey          </td>
                  <td >
            I          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Woodbine          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr >
                  <td>
            November 3, 2012</td>
                  <td>Grey Goose Juvenile          </td>
                  <td >
            I          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Santa Anita Park          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr class="odd">
                  <td>
            November 17, 2012</td>
                  <td>Delta Downs Jackpot Stakes          </td>
                  <td >
            III          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Delta Downs          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr >
                  <td>
            November 24, 2012</td>
                  <td>Remsen Stakes          </td>
                  <td >
            II          </td>
                  <td >
            1 1/8 M          </td>
                  <td >
            Aqueduct          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr class="odd">
                  <td>
            November 24, 2012</td>
                  <td>Kentucky Jockey Club          </td>
                  <td >
            II          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Churchill Downs          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr >
                  <td>
            December 15, 2012</td>
                  <td>CashCall Futurity          </td>
                  <td >
            I          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Hollywood Park          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr class="odd">
                  <td>
            January 5, 2013</td>
                  <td>Sham          </td>
                  <td >
            III          </td>
                  <td >
            1 M          </td>
                  <td >
            Santa Anita Park          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr >
                  <td>
            January 19, 2013</td>
                  <td>Lecomte          </td>
                  <td >
            III          </td>
                  <td >
            1M 70 Y          </td>
                  <td >
            Fair Grounds          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr class="odd">
                  <td>
            January 21, 2013</td>
                  <td>Smarty Jones          </td>
                  <td >
                      </td>
                  <td >
            1 M          </td>
                  <td >
            Oaklawn Park          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr >
                  <td>
            January 26, 2013</td>
                  <td>Holy Bull          </td>
                  <td >
            III          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Gulfstream Park          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr class="odd">
                  <td>
            February 2, 2013</td>
                  <td>Robert B. Lewis          </td>
                  <td >
            II          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Santa Anita Park          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr >
                  <td>
            February 2, 2013</td>
                  <td>Sam F. Davis          </td>
                  <td >
            III          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Tampa Bay          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr class="odd">
                  <td>
            February 2, 2013</td>
                  <td>Withers          </td>
                  <td >
            III          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Aqueduct          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr >
                  <td>
            February 16, 2013</td>
                  <td>El Camino Real Derby          </td>
                  <td >
            III          </td>
                  <td >
            1 1/8 M          </td>
                  <td >
            Golden Gate          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr class="odd">
                  <td>
            February 18, 2013</td>
                  <td>Southwest          </td>
                  <td >
            III          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Oaklawn Park          </td>
                  <td >
            10-4-2-1          </td>
              </tr>
          <tr >
                  <td>
            February 23, 2013</td>
                  <td>Fasig-Tipton Fountain of Youth          </td>
                  <td >
            II          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Gulfstream Park          </td>
                  <td >
            50-20-10-5          </td>
              </tr>
          <tr class="odd">
                  <td>
            February 23, 2013</td>
                  <td>Risen Star          </td>
                  <td >
            II          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Fair Grounds          </td>
                  <td >
            50-20-10-5          </td>
              </tr>
          <tr >
                  <td>
            March 2, 2013</td>
                  <td>Gotham          </td>
                  <td >
            III          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Aqueduct          </td>
                  <td >
            50-20-10-5          </td>
              </tr>
          <tr class="odd">
                  <td>
            March 9, 2013</td>
                  <td>Tampa Bay Derby          </td>
                  <td >
            II          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Tampa Bay          </td>
                  <td >
            50-20-10-5          </td>
              </tr>
          <tr >
                  <td>
            March 9, 2013</td>
                  <td>San Felipe          </td>
                  <td >
            II          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Santa Anita Park          </td>
                  <td >
            50-20-10-5          </td>
              </tr>
          <tr class="odd">
                  <td>
            March 16, 2013</td>
                  <td>Rebel          </td>
                  <td >
            II          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Oaklawn Park          </td>
                  <td >
            50-20-10-5          </td>
              </tr>
          <tr >
                  <td>
            March 23, 2013</td>
                  <td>Spiral          </td>
                  <td >
            III          </td>
                  <td >
            1 1/8 M          </td>
                  <td >
            Turfway Park          </td>
                  <td >
            50-20-10-5          </td>
              </tr>
          <tr class="odd">
                  <td>
            March 24, 2013</td>
                  <td>Sunland Derby          </td>
                  <td >
            III          </td>
                  <td >
            1 1/8 M          </td>
                  <td >
            Sunland Park          </td>
                  <td >
            50-20-10-5          </td>
              </tr>
          <tr >
                  <td>
            March 30, 2013</td>
                  <td>Florida Derby          </td>
                  <td >
            I          </td>
                  <td >
            1 1/8 M          </td>
                  <td >
            Gulfstream Park          </td>
                  <td >
            100-40-20-10          </td>
              </tr>
          <tr class="odd">
                  <td>
            March 30, 2013</td>
                  <td>UAE Derby          </td>
                  <td >
            II          </td>
                  <td >
            1 3/16 M          </td>
                  <td >
            Meydan          </td>
                  <td >
            100-40-20-10          </td>
              </tr>
          <tr >
                  <td>
            March 30, 2013</td>
                  <td>Louisiana Derby          </td>
                  <td >
            II          </td>
                  <td >
            1 1/8 M          </td>
                  <td >
            Fair Grounds          </td>
                  <td >
            100-40-20-10          </td>
              </tr>
          <tr class="odd">
                  <td>
            April 6, 2013</td>
                  <td>Wood Memorial          </td>
                  <td >
            I          </td>
                  <td >
            1 1/8 M          </td>
                  <td >
            Aqueduct          </td>
                  <td >
            100-40-20-10          </td>
              </tr>
          <tr >
                  <td>
            April 6, 2013</td>
                  <td>Santa Anita Derby          </td>
                  <td >
            I          </td>
                  <td >
            1 1/8 M          </td>
                  <td >
            Santa Anita Park          </td>
                  <td >
            100-40-20-10          </td>
              </tr>
          <tr class="odd">
                  <td>
            April 13, 2013</td>
                  <td>Arkansas Derby          </td>
                  <td >
            I          </td>
                  <td >
            1 1/8 M          </td>
                  <td >
            Oaklawn Park          </td>
                  <td >
            100-40-20-10          </td>
              </tr>
          <tr >
                  <td>
            April 13, 2013</td>
                  <td>Blue Grass          </td>
                  <td >
            I          </td>
                  <td >
            1 1/8 M          </td>
                  <td >
            Keeneland          </td>
                  <td >
            100-40-20-10          </td>
              </tr>
          <tr class="odd">
                  <td>
            April 20, 2013</td>
                  <td>Lexington          </td>
                  <td >
            III          </td>
                  <td >
            1 1/16 M          </td>
                  <td >
            Keeneland          </td>
                  <td >
            20-8-4-2          </td>
              </tr>
          <tr class="even views-row-last">
                  <td>
            April 27, 2013</td>
                  <td>Derby Trial          </td>
                  <td >
            III          </td>
                  <td >
            1 M          </td>
                  <td >
            Churchill Downs          </td>
                  <td >
            20-8-4-2          </td></tr></table>
            <a href="http://www.usracing.com" style="color:white;">Online Horse Betting</a>