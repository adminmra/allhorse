    
    <script type="application/ld+json">
          {
                "@context": "http://schema.org",
                "@type": "Table",
                "name": "2020 Pegasus World Cup Turf Odds",
                "about": {
                        "@type": "Event",
                        "name": "Pegasus World Cup Turf 2020",
                        "location": {
                                "@type": "Place",
                                "name": "Gulfstream Park",
                                "address": {
                                        "@type": "PostalAddress",
                                        "addressLocality": "Hallandale Beach",
                                        "addressRegion": "Florida"
                                }
                        },
                        "startDate": "2020-01-25"
                },
                "keywords": "List of Contenders Pegasus World Cup Turf, Pegasus World Cup Turf Odds, Pegasus World Cup 2020, Pegasus World Cup Turf 2020 Contenders, Pegasus World Cup, Bet On The Pegasus World Cup, Pegasus World Cup Invitational"
          }
        </script>

  </style>

    
    <div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Pegasus World Cup Turf">
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <!-- <em id='updateemp'>Updated January 21, 2020.</em> -->
                       <!-- <!-- br> All odds are fixed odds prices. --> -->
                    </td>
                </tr>
            </tfoot>
            <tbody>
            <tr>
                        <td colspan="3" class="center">
                                <table id="o1t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
                                        <tbody>
                                                        <tr>
                                                                <th colspan="3" class="center">Horses - Pegasus World Cup Turf</th>
                                                        </tr>
                                                        <tr class="sortar">
                                                                <th class="first-th" <!--Team-->Horse</th>
                                                                <th>Fractional</th>
                                                                <th>American</th>
                                                        </tr>
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Horses - Pegasus World Cup Turf  - Jan 25                    </th>
            </tr>
            -->



                <!--
            <tr>
                    <th colspan="3" class="center">
                    Run Or Not<br />All Wagers Have Action                    </th>
            </tr>
            -->



    <tr><td>Without Parole</td><td>9/4</td><td>+225</td></tr><tr><td>Magic Wand</td><td>13/4</td><td>+325</td></tr><tr><td>Mo Forza</td><td>5/1</td><td>+500</td></tr><tr><td>Sacred Life</td><td>9/1</td><td>+900</td></tr><tr><td>Instilled Regard</td><td>10/1</td><td>+1000</td></tr><tr><td>Lucullan</td><td>10/1</td><td>+1000</td></tr><tr><td>Arklow</td><td>14/1</td><td>+1400</td></tr><tr><td>Henley`s Joy</td><td>14/1</td><td>+1400</td></tr><tr><td>Sadler`s Joy</td><td>14/1</td><td>+1400</td></tr><tr><td>Starship Jubilee</td><td>10/1</td><td>+1000</td></tr><tr><td>United</td><td>14/1</td><td>+1400</td></tr><tr><td>A Thread Of Blue</td><td>20/1</td><td>+2000</td></tr><tr><td>Zulu Alpha</td><td>20/1</td><td>+2000</td></tr><tr><td>Mr Misunderstood</td><td>20/1</td><td>+2000</td></tr><tr><td>Channel Cat</td><td>25/1</td><td>+2500</td></tr><tr><td>Next Shares</td><td>20/1</td><td>+2000</td></tr><tr><td>Admission Office</td><td>25/1</td><td>+2500</td></tr><tr><td>American Tattoo</td><td>16/1</td><td>+1600</td></tr><tr><td>Tusk</td><td>33/1</td><td>+3300</td></tr>
    </tbody>
                                </table>
                                </td>
                        </tr>
                </tbody>
        </table>
    </div>


    