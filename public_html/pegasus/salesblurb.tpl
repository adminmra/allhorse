{* {if $ShowPegasusOddS}

<!-- ON Pegasus -->
BUSR and the Pegasus World Cup are a great combination. When you sign up you can receive up to
$625 in cash bonus. Use the promocode <strong>PEGASUS</strong> when you make your deposit and receive an extra 25% cash
up to $625. Take advantage of this great offer for the Pegasus World Cup. Who is going to win the Pegasus World Cup?
Place your bet now! The action doesn't stop with BUSR. Join today so you can place your wager on
the Pegasus World Cup! <br>
<br>

{else}

<!-- After Pegasus -->
BUSR and the Pegasus World Cup are a great combination. When you sign up you receive a 10% cash
bonus. Take advantage of this great offer for the Pegasus World Cup along with all the other great horse racing action
throughout the year. Who do you think will win the Pegasus World Cup? Place your bet now! The action doesn't stop with
BUSR. Join today so you can place your wager on the Pegasus World Cup!


{/if} *}

{* BUSR and the Pegasus World Cup are a great combination. When
you sign up you will receive a cash bonus up to $250. Take advantage of
this great offer for the Pegasus World Cup along with all the other great
horse racing action throughout the year. <br><br>Who do you think will win the
Pegasus World Cup? Place your bet now! The action doesn&#39;t stop with BUSR. Join today so you
can place your wager on the Pegasus World Cup! *}

{* Betting on Pegasus World Cup rivals the Super Bowl for the excitement that it generates each year. Even though the race is only 2 minutes long, it consistently draws the attention of horse racing fans as well as people whose only knowledge of horse racing comes from "The Pegasus World Cup."<br> From small bets between friends at their horse racing parties to the crazy big money horse bets at offtrack horse betting parlors, casinos in Las Vegas and online horse betting sites, placing a wager on The Pegasus World Cup can't get any easier.<br> The Pegasus World Cup is a Grade I stakes race for three-year-old thoroughbred horses each year in January. In 2021, the prize purse will offer a $3 million total purse with 2% of the purse donated to Thoroughbred aftercare.<br> Every year, fans descend to South Florida to take part of the spectacle that is The Pegasus World Cup. It draws over 20,000 screaming fans so it is a great way to kick off the racing season. *}

<p>The Pegasus World Cup is a Grade I stakes race for three-year-old thoroughbred horses each year in January.<br>
In 2021, the prize purse will offer a $3 million total purse with 2% of the purse donated to Thoroughbred aftercare.</p>
<p>Every year, fans descend to South Florida to take part in the spectacle that is The Pegasus World Cup. It draws over 20,000 screaming fans so it's a great way to kick off the racing season.</p>
