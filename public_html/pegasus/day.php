<?php
$cdate = date("Y-m-d H:i:s");
if ($cdate > "2019-01-26 23:59:00" && $cdate < "2020-01-25 23:59:00") {
    echo "January 25th";
} else if ($cdate > "2020-01-25 23:59:00" && $cdate < "2021-01-23 17:44:00") {
    echo "January 23rd";
} else if ($cdate > "2021-01-23 17:44:00" && $cdate < "2022-01-29 23:59:00") {
    echo "January 22nd";
} else if ($cdate > "2022-01-29 23:59:00" && $cdate < "2023-01-28 23:59:00") { 
    echo "January 28th";
} else if ($cdate > "2023-01-28 23:59:00" && $cdate < "2024-01-27 23:59:00") {
    echo "January 27th";
} else if ($cdate > "2024-01-27 23:59:00" && $cdate < "2025-01-25 23:59:00") {
    echo "January 25th";
}
?>