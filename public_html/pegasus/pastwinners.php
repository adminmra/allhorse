<h2>Pegasus World Cup Winners</h2>
<div id="no-more-tables">
<table id="sortTable-1" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" summary="Winners of the Pegasus World Cup."> 
  <thead>
    <tr>
      <th width="75px">Year </th>
      <th width="95px">Winner </th>
      <th width="75px">Age </th>
      <th>Jockey </th>
      <th>Trainer </th>
      <th>Owner </th>
      <th width="85px">Time </th>
    </tr>
    </thead>
    <tbody>
    <tr>
      <td data-title="Year">2021</td>
      <td data-title="Winner">Knicks Go</td>
      <td data-title="Age">5</td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">Brad Cox</td>
      <td data-title="Owner">Korea Racing Authority</td>
      <td data-title="Time">1:47.89</td>
    </tr> 
    <tr>
      <td data-title="Year">2020</td>
      <td data-title="Winner">Mucho Gusto</td>
      <td data-title="Age">4</td>
      <td data-title="Jockey">Irad Ortiz, Jr.</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Michael Lund Petersen </td>
      <td data-title="Time">1:48.85</td>
    </tr> 
    <tr>
      <td data-title="Year">2019</td>
      <td data-title="Winner">City of Light</td>
      <td data-title="Age">5</td>
      <td data-title="Jockey">J. Castellano</td>
      <td data-title="Trainer">M. McCarthy</td>
      <td data-title="Owner">Mr. and Mrs. William K Warren, Jr.</td>
      <td data-title="Time">1:47.71</td>
    </tr>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Gun Runner</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Florent Geroux</td>
      <td data-title="Trainer">Steven M. Asmussen</td>
      <td data-title="Owner">Winchell Thoroughbreds LLC &amp; Three Chimneys Farm</td>
      <td data-title="Time">1:47.41 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Arrogate</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Juddmonte Farms</td>
      <td data-title="Time">1:46.83 </td>
    </tr>
  </tbody>
</table></div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>
{/literal}