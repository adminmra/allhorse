<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "Table",
		"name": "2020 Pegasus World Cup Odds",
		"about": {
			"@type": "Event",
			"name": "Pegasus World Cup 2020",
			"location": {
				"@type": "Place",
				"name": "Gulfstream Park",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "Hallandale Beach",
					"addressRegion": "Florida"
				}
			},
			"startDate": "2020-01-25",
			"endDate" : "2020-01-25",
			"offers": {
			"@type": "Offer",
				"url": "https://www.usracing.com/pegasus-world-cup/signup-bonus"
			},
			"description": "Latest Pegasus World Cup 2020 odds at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus 2020.",
			"image": "/img/pegasus-world-cup/pegasus-world-cup-odds-og.jpg"
		},
		"keywords": "List of Contenders Pegasus World Cup, Pegasus World Cup Odds, Pegasus World Cup 2020, Pegasus World Cup 2020 Contenders, Pegasus World Cup, Bet On The Pegasus World Cup, Pegasus World Cup Invitational"
	}
</script>
<style>
	.none {
		display: none;
	}
</style>


<div id="no-more-tables">
	<table class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"
		summary="PPs - Pegasus World Cup">
		<!-- <tfoot>
			<tr>
				<td data-title="PP" class="dateUpdated center" colspan="3">
					<em id='updateemp'>Updated 
					<?php //$tdate = date("M d, Y"); echo $tdate; ?>
				</em>
				</td>
			</tr>
		</tfoot> -->
		<tbody>
			<tr>
			<table id="o0t" width="100%" cellpadding="0" cellspacing="0"
						class="data table table-condensed table-striped table-bordered">
						<tbody>
							<thead>
								<tr>
									<th colspan="5" class="center">Horses  - Pegasus World Cup</th>
								</tr>
								<tr class="sortar">
									<th class="first-th">PP</th>
									<th>Horse</th>
									<th>Jockey</th>
									<th>Fractional</th>
									<th>American</th>
								</tr>
							</thead>
							<tr>
								<td data-title="PP">
									<p class="none">0</p>1
								</td>
								<td data-title="Horse">True Timber</td>
								<td data-title="Jockey">Joe Bravo</td>
								<td data-title="Fractional">15/1</td>
								<td data-title="American">+1500</td>
							</tr>
							<tr>
								<td data-title="PP">
									<p class="none">0</p>2
								</td>
								<td data-title="Horse">Tax</td>
								<td data-title="Jockey">Jose Ortiz</td>
								<td data-title="Fractional">8/1</td>
								<td data-title="American">+800</td>
							</tr>
							<tr>
								<td data-title="PP">
									<p class="none">0</p>3
								</td>
								<td data-title="Horse">Diamond Oops</td>
								<td data-title="Jockey">Julien Leparoux</td>
								<td data-title="Fractional">15/1</td>
								<td data-title="American">+1500</td>
							</tr>
							<tr>
								<td data-title="PP">
									<p class="none">0</p>4
								</td>
								<td data-title="Horse">Seeking the Soul</td>
								<td data-title="Jockey">John Velazquez</td>
								<td data-title="Fractional">30/1</td>
								<td data-title="American">+3000</td>
							</tr>
							<tr>
								<td data-title="PP">
									<p class="none">05</p>SCR
								</td>
								<td data-title="Horse"><strike>Omaha Beach</strike></td>
								<td data-title="Jockey"><strike>Mike Smith</strike></td>
								<td data-title="Fractional"><strike>1/1</strike></td>
								<td data-title="American"><strike>+100</strike></td>
							</tr>
							<tr>
								<td data-title="PP">
									<p class="none">0</p>6
								</td>
								<td data-title="Horse">Higher Power</td>
								<td data-title="Jockey">Flavien Prat</td>
								<td data-title="Fractional">6/1</td>
								<td data-title="American">+600</td>
							</tr>
							<tr>
								<td data-title="PP">
									<p class="none">0</p>7
								</td>
								<td data-title="Horse">War Story</td>
								<td data-title="Jockey">Joel Rosario</td>
								<td data-title="Fractional">30/1</td>
								<td data-title="American">+3000</td>
							</tr>
							<tr>
								<td data-title="PP">
									<p class="none">0</p>8
								</td>
								<td data-title="Horse">Mr Freeze</td>
								<td data-title="Jockey">Luis Saez</td>
								<td data-title="Fractional">20/1</td>
								<td data-title="American">+2000</td>
							</tr>
							<tr>
								<td data-title="PP">
									<p class="none">0</p>SCR
								</td>
								<td data-title="Horse"><strike>Spun to Run</strike></td>
								<td data-title="Jockey"><strike>Javier Castellano<strike></td>
								<td data-title="Fractional"><strike>7/2<strike></td>
								<td data-title="American"><strike>+350<strike></td>
							</tr>
							<tr>
								<td data-title="PP">10</td>
								<td data-title="Horse">Mucho Gusto</td>
								<td data-title="Jockey">Irad Ortiz, Jr.</td>
								<td data-title="Fractional">9/2</td>
								<td data-title="PAmerican">+450</td>
							</tr>
							<tr>
								<td data-title="PP">11</td>
								<td data-title="Horse">Tenfold</td>
								<td data-title="Jockey">Tyler Gaffalione</td>
								<td data-title="Fractional">30/1</td>
								<td data-title="American">+3000</td>
							</tr>
							<tr>
								<td data-title="PP">12</td>
								<td data-title="Horse">Bodexpress</td>
								<td data-title="Jockey">Emisael Jaramillo</td>
								<td data-title="Fractional">30/1</td>
								<td data-title="American">+3000</td>
							</tr>

						</tbody>
					</table>
			</tr>
		</tbody>
	</table>
</div>