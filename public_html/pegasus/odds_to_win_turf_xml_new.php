<script type="application/ld+json">
          {
                "@context": "http://schema.org",
                "@type": "Table",
                "name": "2021 Pegasus World Cup Turf Odds",
                "about": {
                        "@type": "Event",
                        "name": "Pegasus World Cup 2021",
                        "location": {
                                "@type": "Place",
                                "name": "Gulfstream Park",
                                "address": {
                                        "@type": "PostalAddress",
                                        "addressLocality": "Hallandale Beach",
                                        "addressRegion": "Florida"
                                }
                        },
                        "startDate": "2021-01-23",
                        "description": "Latest Pegasus World Cup 2021 odds at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus 2021."
                },
                "keywords": "List of Contenders Pegasus World Cup, Pegasus World Cup Turf Odds, Pegasus World Cup 2021, Pegasus World Cup 2021 Contenders, Pegasus World Cup, Bet On The Pegasus World Cup, Pegasus World Cup Invitational"
          }
        </script>
		
<div id="no-more-tables">
    <table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center" cellpadding="0"
        cellspacing="0" border="0" title="2021 Pegasus World Cup Turf Odds and Post Position"
        summary="The latest odds for the Pegasus World Cup available">
        <caption class="hide-sm">2021 Pegasus World Cup Turf Odds and Post Position</caption>
        
        <tbody>
                <tr class='head_title hide-sm'>
          <th>PP</th>
          <th>Horse</th>
          <th>Jockey</th>
          <th>Trainer</th>
          <th style="width:320px">Owner</th>
		  <th>Odds</th>
            </tr>
   
<tr>
<td data-title=“PP”><center>1</center></td>
<td data-title=“Horse”><center>Next Shares</center></td>
<td data-title="Jockey"><center>Drayden Van Dyke</center></td>
<td data-title=“Trainer”><center>Richard Baltasr</center></td>
<td data-title=“Owner”><center>Debby Baltas, Richard Baltas, Christopher T. Dunn,
Jules Iavarone, Michael Iavarone, Jerry McClanahan, Ritchie Robershaw, and Mark Taylor</center></td>
<td data-title=“Odds”><center>20-1</center></td>
</tr>
<tr>
<td data-title=“PP”><center>2</center></td>
<td data-title=“Horse”><center>Breaking The Rules</center></td>
<td data-title="Jockey"><center>John Velazquez</center></td>
<td data-title=“Trainer”><center>Claude R. McGaughey III</center></td>
<td data-title=“Owner”><center>Phipps Stable</center></td>
<td data-title=“Odds”><center>10-1</center></td>
</tr>
<tr>
<td data-title=“PP”><center>3 </center></td>
<td data-title=“Horse”><center>Storm The Court</center></td>
<td data-title="Jockey"><center>Julien Leparoux</center></td>
<td data-title=“Trainer”><center>Peter Eurton</center></td>
<td data-title=“Owner”><center>Exline-Border Racing, LLC, David A. Bernsen
LLC, Susanna Wilson, and Dan Hudock</center></td>
<td data-title=“Odds”><center>12-1</center></td>
</tr>
<tr>
<td data-title=“PP”><center>4</center></td>
<td data-title=“Horse”><center>North Dakota</center></td>
<td data-title="Jockey"><center>Jose Ortiz</center></td>
<td data-title=“Trainer”><center>Claude R. McGaughey III</center></td>
<td data-title=“Owner”><center>Joseph Allen LLC</center></td>
<td data-title=“Odds”><center>10-1</center></td>
</tr>
<tr>
<td data-title=“PP”><center>5</center></td>
<td data-title=“Horse”><center>Colonel Liam (Win)</center></td>
<td data-title="Jockey"><center>Irad Ortiz Jr.</center></td>
<td data-title=“Trainer”><center>Todd A. Pletcher</center></td>
<td data-title=“Owner”><center>Robert E. and Lawana L. Low</center></td>
<td data-title=“Odds”><center>7-2</center></td>
</tr>
<tr>
<td data-title=“PP”><center>6</center></td>
<td data-title=“Horse”><center>Largent (Place)</center></td>
<td data-title="Jockey"><center>Paco Lopez</center></td>
<td data-title=“Trainer”><center>Todd A. Pletcher</center></td>
<td data-title=“Owner”><center>Eclipse Thoroughbred Partners and Twin Creeks Racing Stables, LLC</center></td>
<td data-title=“Odds”><center>9-2</center></td>
</tr>
<tr>
<td data-title=“PP”><center>7</center></td>
<td data-title=“Horse”><center>Aquaphobia</center></td>
<td data-title="Jockey"><center>Joe Bravo</center></td>
<td data-title=“Trainer”><center>Michael J. Maker</center></td>
<td data-title=“Owner”><center>Paradise Farms Corp., Staudacher, David,
Hooties Racing LLC and Skychai Racing, LLC</center></td>
<td data-title=“Odds”><center>20-1</center></td>
</tr>
<tr>
<td data-title=“PP”><center>8</center></td>
<td data-title=“Horse”><center>Anothertwistafate</center></td>
<td data-title="Jockey"><center>Joel Rosario</center></td>
<td data-title=“Trainer”><center>Peter Mille</center></td>
<td data-title=“Owner”><center>Peter Redekop B. C., Ltd.</center></td>
<td data-title=“Odds”><center>5-1</center></td>
</tr>
<tr>
<td data-title=“PP”><center>9</center></td>
<td data-title=“Horse”><center>Cross Border (Show)</center></td>
<td data-title="Jockey"><center>Tyler Gaffalione</center></td>
<td data-title=“Trainer”><center>Michael J. Maker</center></td>
<td data-title=“Owner”><center>Three Diamonds Farm</center></td>
<td data-title=“Odds”><center>15-1</center></td>
</tr>
<tr>
<td data-title=“PP”><center>10</center></td>
<td data-title=“Horse”><center>Pixelate</center></td>
<td data-title="Jockey"><center>Edgard Zayas</center></td>
<td data-title=“Trainer”><center>Michael Stidham</center></td>
<td data-title=“Owner”><center>Godolphin, LLC</center></td>
<td data-title=“Odds”><center>15-1</center></td>
</tr>
<tr>
<td data-title=“PP”><center>11</center></td>
<td data-title=“Horse”><center>Say The Word</center></td>
<td data-title="Jockey"><center>Flavien Prat</center></td>
<td data-title=“Trainer”><center>Philip D’Amato</center></td>
<td data-title=“Owner”><center>Sam-Son Farm</center></td>
<td data-title=“Odds”><center>6-1</center></td>
</tr>
<tr>
<td data-title=“PP”><center>12 </center></td>
<td data-title=“Horse”><center>Social Paranoia</center></td>
<td data-title="Jockey"><center>Luis Saez</center></td>
<td data-title=“Trainer”><center>Todd A. Pletcher</center></td>
<td data-title=“Owner”><center>The Elkstone Group, LLC</center></td>
<td data-title=“Odds”><center>8-1</center></td>
</tr>

      </tbody>
    </table>
</div>
<style>
    table.ordenable tbody th {
        cursor: pointer;
    }
    @media only screen and (max-width: 800px) {
        #no-more-tables td span {
            text-align: center !important;
        }
        .hide-sm {
          display: none !important;
        }
    }
</style>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">

<!-- <script src="/assets/js/jquery.sortElements.js"></script>
<script src="//www.usracing.com/assets/js/sorttable.js"></script> -->
<script src="//www.usracing.com/assets/js/aligncolumn.js"></script>

<script>
    $(document).ready(function () {

        alignColumn([2], 'left');

    });
</script>









