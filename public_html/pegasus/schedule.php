<table width="100%"  cellpadding="0" cellspacing="0" class="data" title="Breeders' Cup " Summary="Date, Post Time, Distance, Purse for Betting the  Breeders' Cup">
                                            <caption>BREEDERS' CUP <?php	 	 ?>  Races
                                          </caption>
                                          <tr >
										  	<th width="12%" >Date</th>

                                            <!-- <th width="10%" >Post</th> -->
                                            <th width="44%">Race</th>
                                            <th width="19%">Age</th>
                                            <th width="19%">Distance</th>
                                            <th width="10%">Purse</th>
                                          </tr>
                                        <tr>                                        
                                           	 <td >Nov 04</td>

										   <!--  <td ></td> -->
                                            <td >Breeders' Cup Juvenile Sprint</td> 
                                              <!-- <td >Breeders' Cup Juvenile Sprint</td> -->
											<td >2YO</td>
                                            <td >6 Furlongs</td>
                                            <td >$500,000</td>
                                          </tr>

										  
										 <tr bgcolor='#EBEBEB'>                                        
                                           	 <td >Nov 04</td>
										   <!--  <td ></td> -->
                                            <td >Breeders' Cup Marathon</td> 
                                              <!-- <td >Breeders' Cup Marathon</td> -->
											<td >3YO&UP</td>
                                            <td >1 3/4 Miles</td>
                                            <td >$500,000</td>

                                          </tr>
										  
										 <tr>                                        
                                           	 <td >Nov 04</td>
										   <!--  <td ></td> -->
                                            <td >Breeders' Cup Juvenile Fillies Turf </td> 
                                              <!-- <td >Breeders' Cup Juvenile Fillies Turf </td> -->
											<td >2YO</td>
                                            <td >1 Mile</td>

                                            <td >$1,000,000</td>
                                          </tr>
										  
										 <tr bgcolor='#EBEBEB'>                                        
                                           	 <td >Nov 04</td>
										   <!--  <td ></td> -->
                                            <td >Sentient Jet Breeders' Cup Filly & Mare Sprint</td> 
                                              <!-- <td >Sentient Jet Breeders' Cup Filly & Mare Sprint</td> -->
											<td >3YO&UP</td>

                                            <td >7 Furlongs</td>
                                            <td >$1,000,000</td>
                                          </tr>
										  
										 <tr>                                        
                                           	 <td >Nov 04</td>
										   <!--  <td ></td> -->
                                            <td >Emirates Airline Breeders' Cup Filly & Mare Turf </td> 
                                              <!-- <td >Emirates Airline Breeders' Cup Filly & Mare Turf </td> -->

											<td >3YO&UP</td>
                                            <td >1 3/8 Miles</td>
                                            <td >$2,000,000</td>
                                          </tr>
										  
										 <tr bgcolor='#EBEBEB'>                                        
                                           	 <td >Nov 04</td>
										   <!--  <td ></td> -->

                                            <td >Breeders' Cup Ladies' Classic</td> 
                                              <!-- <td >Breeders' Cup Ladies' Classic</td> -->
											<td >3YO&UP</td>
                                            <td >1 1/8 Miles</td>
                                            <td >$2,000,000</td>
                                          </tr>
										  
										 <tr>                                        
                                           	 <td >Nov 05</td>

										   <!--  <td ></td> -->
                                            <td >Breeders' Cup Juvenile Turf </td> 
                                              <!-- <td >Breeders' Cup Juvenile Turf </td> -->
											<td >2YO</td>
                                            <td >1 Mile</td>
                                            <td >$1,000,000</td>
                                          </tr>

										  
										 <tr bgcolor='#EBEBEB'>                                        
                                           	 <td >Nov 05</td>
										   <!--  <td ></td> -->
                                            <td >Breeders' Cup Turf Sprint</td> 
                                              <!-- <td >Breeders' Cup Turf Sprint</td> -->
											<td >3YO&UP</td>
                                            <td >5 Furlongs </td>
                                            <td >$1,000,000</td>

                                          </tr>
										  
										 <tr>                                        
                                           	 <td >Nov 05</td>
										   <!--  <td ></td> -->
                                            <td >Breeders' Cup Dirt Mile</td> 
                                              <!-- <td >Breeders' Cup Dirt Mile</td> -->
											<td >3YO&UP</td>
                                            <td >1 Mile </td>

                                            <td >$1,000,000</td>
                                          </tr>
										  
										 <tr bgcolor='#EBEBEB'>                                        
                                           	 <td >Nov 05</td>
										   <!--  <td ></td> -->
                                            <td >Sentient Jet Breeders' Cup Sprint</td> 
                                              <!-- <td >Sentient Jet Breeders' Cup Sprint</td> -->
											<td >3YO&UP</td>

                                            <td >6 Furlongs </td>
                                            <td >$1,500,000</td>
                                          </tr>
										  
										 <tr>                                        
                                           	 <td >Nov 05</td>
										   <!--  <td ></td> -->
                                            <td >Grey Goose Breeders' Cup Juvenile</td> 
                                              <!-- <td >Grey Goose Breeders' Cup Juvenile</td> -->

											<td >2YO </td>
                                            <td >1 1/16 Miles </td>
                                            <td >$2,000,000 </td>
                                          </tr>
										  
										 <tr bgcolor='#EBEBEB'>                                        
                                           	 <td >Nov 05</td>
										   <!--  <td ></td> -->

                                            <td >Grey Goose Breeders' Cup Juvenile Fillies</td> 
                                              <!-- <td >Grey Goose Breeders' Cup Juvenile Fillies</td> -->
											<td >2YO</td>
                                            <td >1 1/16 Miles</td>
                                            <td >$2,000,000</td>
                                          </tr>
										  
										 <tr>                                        
                                           	 <td >Nov 05</td>

										   <!--  <td ></td> -->
                                            <td >Breeders' Cup Mile</td> 
                                              <!-- <td >Breeders' Cup Mile</td> -->
											<td >3YO&UP</td>
                                            <td >1 Mile</td>
                                            <td >$2,000,000</td>
                                          </tr>

										  
										 <tr bgcolor='#EBEBEB'>                                        
                                           	 <td >Nov 05</td>
										   <!--  <td ></td> -->
                                            <td >Emirates Airline Breeders' Cup Turf</td> 
                                              <!-- <td >Emirates Airline Breeders' Cup Turf</td> -->
											<td >3YO&UP</td>
                                            <td >1 1/2 Miles </td>
                                            <td >$3,000,000</td>

                                          </tr>
										  
										 <tr>                                        
                                           	 <td >Nov 05</td>
										   <!--  <td ></td> -->
                                            <td >Breeders' Cup Classic</td> 
                                              <!-- <td >Breeders' Cup Classic</td> -->
											<td >3YO&UP</td>
                                            <td >1 1/4 Miles</td>

                                            <td >$5,000,000</td>
                                          </tr>
										  
										                                          
                          </table>