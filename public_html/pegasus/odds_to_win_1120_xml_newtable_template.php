    <div id="no-more-tables">
        <table  id="sortTable" border="0" cellpadding="0" cellspacing="0" class="data table table-bordered table-striped table-condensed"  summary="Horses - Pegasus World Cup">
            <caption class="newtabe-caption">Horses - Pegasus World Cup</caption>
            <thead>
                <tr><th>Team</th><th>Fractional</th><th>American</th></tr>            </thead>
            <tbody>
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Horses - Pegasus World Cup  - Jan 26                    </th>
            </tr>
            -->
    <tr><td data-title="Team">Accelerate</td><td data-title="Fractional">9/4</td><td data-title="American">+225</td></tr><tr><td data-title="Team">Mendelssohn</td><td data-title="Fractional">7/1</td><td data-title="American">+700</td></tr><tr><td data-title="Team">Monomoy Girl</td><td data-title="Fractional">6/1</td><td data-title="American">+600</td></tr><tr><td data-title="Team">Audible</td><td data-title="Fractional">6/1</td><td data-title="American">+600</td></tr><tr><td data-title="Team">City Of Light</td><td data-title="Fractional">8/1</td><td data-title="American">+800</td></tr><tr><td data-title="Team">Gunnevera</td><td data-title="Fractional">9/1</td><td data-title="American">+900</td></tr><tr><td data-title="Team">Catholic Boy</td><td data-title="Fractional">10/1</td><td data-title="American">+1000</td></tr><tr><td data-title="Team">Diversify</td><td data-title="Fractional">10/1</td><td data-title="American">+1000</td></tr><tr><td data-title="Team">Mckinzie</td><td data-title="Fractional">10/1</td><td data-title="American">+1000</td></tr><tr><td data-title="Team">Yoshida</td><td data-title="Fractional">12/1</td><td data-title="American">+1200</td></tr><tr><td data-title="Team">Mind Your Biscuits</td><td data-title="Fractional">12/1</td><td data-title="American">+1200</td></tr><tr><td data-title="Team">Seeking The Soul</td><td data-title="Fractional">14/1</td><td data-title="American">+1400</td></tr><tr><td data-title="Team">Catalina Cruiser</td><td data-title="Fractional">16/1</td><td data-title="American">+1600</td></tr><tr><td data-title="Team">Battle Of Midway</td><td data-title="Fractional">25/1</td><td data-title="American">+2500</td></tr><tr><td data-title="Team">Promises Fulfilled</td><td data-title="Fractional">25/1</td><td data-title="American">+2500</td></tr><tr><td data-title="Team">Hofburg</td><td data-title="Fractional">33/1</td><td data-title="American">+3300</td></tr><tr><td data-title="Team">Pavel</td><td data-title="Fractional">40/1</td><td data-title="American">+4000</td></tr><tr><td data-title="Team">Bolt Doro</td><td data-title="Fractional">33/1</td><td data-title="American">+3300</td></tr><tr><td data-title="Team">Gronkowski</td><td data-title="Fractional">40/1</td><td data-title="American">+4000</td></tr>            
<tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id='updateemp'>Updated November 30, 2018.</em>
                        <!-- br> All odds are fixed odds prices. -->
                    </td>
                </tr>
</tbody>
        </table>
    </div>
    {literal}
        <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
        <script src="/assets/js/sorttable-winners.js"></script>
    {/literal}
    
