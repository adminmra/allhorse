<style>
    @media (min-width: 768px) {
        .countdown-icon {
            width: 100px;
        }
    }
</style>
<section class="countdown-section sm">
  <div class="countdown-top-block">
    <div class="countdown-icon icon-left">
      <img src="/img/pegasus-world-cup/pegasus-countdown-icon.png">
    </div>

    <div class="countdown-text">
      <span>Countdown to the <?php include("/home/ah/allhorse/public_html/pegasus/running.php") ?> Pegasus World Cup</span>
    </div>

    <div class="countdown-icon icon-right">
      <img src="/img/pegasus-world-cup/pegasus-countdown-icon.png" style="transform: scaleX(-1);">
    </div>
  </div>

  <div class="countdown-bottom-block">
    <div class="countdown-icon icon-left">
      <img src="/img/pegasus-world-cup/pegasus-countdown-icon.png">
    </div>

    <div class="usr-countdown">
      <div class="timer">
        <span id="cd-days" class="quantity"></span>
        <span class="text">Days</span>
      </div>
      <div class="timer">
        <span id="cd-hours" class="quantity"></span>
        <span class="text">Hours</span>
      </div>
      <div class="timer">
        <span id="cd-minutes" class="quantity"></span>
        <span class="text">Minutes</span>
      </div>
      <div class="timer last">
        <span id="cd-seconds" class="quantity"></span>
        <span class="text">Seconds</span>
      </div>
    </div>

    <div class="countdown-icon icon-right">
      <img src="/img/pegasus-world-cup/pegasus-countdown-icon.png" style="transform: scaleX(-1);">
    </div>
  </div>

</section>

<?php
$cdate = date("Y-m-d H:i:s");
if ($cdate > "2019-01-26 23:59:00" && $cdate < "2020-01-25 23:59:00") {
    $date = "jan 25, 2020 17:40:00";
} else if ($cdate > "2020-01-25 23:59:00" && $cdate < "2021-01-23 17:44:00") {
    $date = "jan 23, 2021 17:40:00";
} else if ($cdate > "2021-01-23 17:44:00" && $cdate < "2022-01-29 23:59:00") {
    $date = "jan 29, 2022 17:40:00";
} else if ($cdate > "2022-01-29 23:59:00" && $cdate < "2023-01-28 23:59:00") { 
    $date = "jan 28, 2023 17:40:00";
} else if ($cdate > "2023-01-28 23:59:00" && $cdate < "2024-01-27 23:59:00") {
    $date = "jan 27, 2024 17:40:00";
} else if ($cdate > "2024-01-27 23:59:00" && $cdate < "2025-01-25 23:59:00") {
    $date = "jan 25, 2025 17:40:00";
}
?>

<script>
    var countDownDate = new Date("<?php echo $date; ?>").getTime();
</script>
<script src="/assets/plugins/countdown/countdown.js"></script>    