<h2>2021 Pegasus World Cup Payouts</h2>
<div>
	<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
		title="Breeders' Classic Payouts" summary="Last year's Payouts of the Breeders' Cup Classic. ">
		<tbody>
			<thead>
				<tr>
					<th>PP</th>
					<th>Horses</th>
					<th>Win</th>
					<th>Place</th>
					<th>Show</th>
				</tr>
			</thead>
			<tr>
				<td>4</td>
				<td>Knicks Go </td>
				<td>$4.60</td>
				<td>$3.60</td>
				<td>$3.00</td>
			</tr>
			<tr>
				<td>5</td>
				<td>Jesus' Team</td>
				<td></td>
				<td>$8.60</td>
				<td>$4.80</td>
			</tr>
			<tr>
				<td>3</td>
				<td>Independence Hall </td>
				<td></td>
				<td></td>
				<td>$10.00</td>
			</tr>
		</tbody>
	</table>
</div>
<p></p>
<div id="no-more-tables">
	<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
		title="Breeders' Classic Payouts" summary="Last year's Payouts of the Breeders' Cup Classic. ">
		<thead>
			<tr>
				<th>Wager</th>
				<th>Horses</th>
				<th>Denomination</th>
				<th>Payout</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td data-title="Wager">Exacta</td>
				<td data-title="Horses">4-5</td>
				<td data-title="Denomination">$2.00</td>
				<td data-title="Payout">$43.00</td>
			</tr>
			<tr>
				<td data-title="Wager">Trifecta</td>
				<td data-title="Horses">4-5-3</td>
				<td data-title="Denomination">$0.50</td>
				<td data-title="Payout">$175.05</td>
			</tr>
			<tr>
				<td data-title="Wager">Superfecta</td>
				<td data-title="Horses">4-5-3-1</td>
				<td data-title="Denomination">$1.00</td>
				<td data-title="Payout">$2,434.00</td>
			</tr>
			<tr>
				<td data-title="Wager">Double</td>
				<td data-title="Horses">5-4</td>
				<td data-title="Denomination">$1.00</td>
				<td data-title="Payout">$9.90</td>
			</tr>
			<tr>
				<td data-title="Wager">Pick 3</td>
				<td data-title="Horses">7-5-4</td>
				<td data-title="Denomination">$0.50</td>
				<td data-title="Payout">$25.75</td>
			</tr>
			<tr>
				<td data-title="Wager">Pick 4</td>
				<td data-title="Horses">6-7-5-4</td>
				<td data-title="Denomination">$0.50</td>
				<td data-title="Payout">$613.20</td>
			</tr>
	
		</tbody>
	</table>
</div>
