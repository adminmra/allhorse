{literal}
    <style>
        @media (max-width: 1157px) {
            .breakline {
                display: none;
            }
        }

        @media (min-width: 1200px) and (max-width: 1495px) {
            .breakline {
                display: none;
            }
        }
    </style>
{/literal}
<section class="card">
    <div class="card_wrap">
        <div class="card_half card_hide-mobile">
            <a href="https://www.betusracing.ag/login?ref={$ref}&to=sports?leagueId=4272">
                <img src="/img/pegasus-world-cup/specials.jpg" data-src-img="/img/pegasus-world-cup/specials.jpg"
                    alt="2021 Pegasus World Cup Specials" class="card_img"></a></div>
        <div class="card_half card_content">
            <a href="https://www.betusracing.ag/login?ref={$ref}&to=sports?leagueId=4272">
                {* <img src="/img/index-kd/icon-bet-kentucky-derby.png" alt="Kentucky Derby Odds:  Trainers"
                    class="card_icon"> *}
                <img class="icon-kentucky-derby-betting card_icon"
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABVAQMAAADUnuB8AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABFJREFUeNpjYBgFo2AUDHIAAAOnAAHnt6kCAAAAAElFTkSuQmCC">
            </a>
            <h2 class="card_heading">Bet on the <br class="breakline"> Pegasus World Cup Specials</h2>
            <p style="margin-bottom: 10px">Anybody can place a bet on which horse will win the Pegasus World Cup but what about placing a bet on place and show?</p>
            <p><a href="/pegasus-world-cup/odds">Pegasus World Cup Odds</a>, Props and Futures are live, Bet Now!</p>
            {* <a href="/signup?ref={$ref}&to=to/racebook?meetingRace=GP*2021/01/23*12" class="card_btn">Bet on the Pegasus World Cup Specials</a> *}
            <a href="/signup?ref={$ref}" class="card_btn">Bet on the Pegasus World Cup Specials</a>
        </div>
    </div>
</section>