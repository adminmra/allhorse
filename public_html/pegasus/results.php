<h2>2021 Pegasus World Cup Results</h2>
<!--fix this to the new table-->
 <div id="no-more-tables">
<table  class="table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0" title="Pegasus World Cup Results" summary="Pegasus World Cup Results">
<thead>
    <tr>
      <th>Results</th>
      <th>Horse</th>
      <th>Trainer</th>
      <th>Jockey</th>
    </tr>
    </thead>
  <tbody>
      <tr>
<td data-title="Results">1st</td>
<td data-title="Horse">Knicks Go</td>
<td data-title="Trainer">Brad Cox</td>
<td data-title="Jockey">Joel Rosario</td>
</tr>
<tr>
<td data-title="Results">2nd</td>
<td data-title="Horse">Jesus' Team</td>
<td data-title="Trainer">Jose D’Angelo</td>
<td data-title="Jockey">Irad Ortiz, Jr.</td>
</tr>
<tr>
<td data-title="Results">3rd</td>
<td data-title="Horse">Independence Hall</td>
<td data-title="Trainer">Michael W. McCarthy</td>
<td data-title="Jockey">Flavien Prat</td>
</tr>
<tr>
<td data-title="Results">4th</td>
<td data-title="Horse">Sleepy Eyes Todd</td>
<td data-title="Trainer">Miguel Angel Silva</td>
<td data-title="Jockey">Jose Ortiz</td>
</tr>
<tr>
<td data-title="Results">5th</td>
<td data-title="Horse">Code of Honor</td>
<td data-title="Trainer">Shug McGaughey</td>
<td data-title="Jockey">Tyler Gaffalione</td>
</tr>
 

  </tbody>
</table>
</div>

<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>
