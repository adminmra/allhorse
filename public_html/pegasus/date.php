<?php
$cdate = date("Y-m-d H:i:s");
if ($cdate > "2019-01-26 23:59:00" && $cdate < "2020-01-25 23:59:00") {
    echo "January 25th, 2020";
} else if ($cdate > "2020-01-25 23:59:00" && $cdate < "2021-01-23 17:44:00") {
    echo "January 23rd, 2021";
} else if ($cdate > "2021-01-23 17:44:00" && $cdate < "2022-01-29 23:59:00") {
    echo "January 22nd, 2022";
} else if ($cdate > "2022-01-29 23:59:00" && $cdate < "2023-01-28 23:59:00") { 
    echo "January 28th, 2023";
} else if ($cdate > "2023-01-28 23:59:00" && $cdate < "2024-01-27 23:59:00") {
    echo "January 27th, 2024";
} else if ($cdate > "2024-01-27 23:59:00" && $cdate < "2025-01-25 23:59:00") {
    echo "January 25th, 2025";
}
?>