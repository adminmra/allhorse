
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"name": "2020 Pegasus World Cup Odds",
		"about": {
			"@type": "Event",
			"name": "Pegasus World Cup 2020",
			"location": {
				"@type": "Place",
				"name": "Gulfstream Park",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "Hallandale Beach",
					"addressRegion": "Florida"
				}
			},
			"startDate": "2020-01-25",
			"description": "Latest Pegasus World Cup 2020 odds at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus 2020."
		},
		"keywords": "List of Contenders Pegasus World Cup, Pegasus World Cup Odds, Pegasus World Cup 2020, Pegasus World Cup 2020 Contenders, Pegasus World Cup, Bet On The Pegasus World Cup, Pegasus World Cup Invitational"
	  }
	</script>


	<div>
        <table  class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Pegasus World Cup">
	<tfoot>
		<tr>
			<td class="dateUpdated center" colspan="3">
				<!-- <em id='updateemp'>Updated 
					<?php //$tdate = date("M d, Y"); echo $tdate; ?>
				</em> -->
			</td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td colspan="3" class="center">
				<table id="o0t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">
					<tbody>
						<tr>
							<th colspan="3" class="center">Horses - Pegasus World Cup</th>
						</tr>
						<tr class="sortar">
							<th class="first-th"<!--Team-->Horse</th>
							<th>Fractional</th>
							<th>American</th>
						</tr>
						<tr>
							<td>Omaha Beach</td>
							<td>1/1</td>
							<td>+110</td>
						</tr>
						<tr>
							<td>Mckinzie</td>
							<td>5/2</td>
							<td>+250</td>
						</tr>
						<tr>
							<td>Higher Power</td>
							<td>11/1</td>
							<td>+1100</td>
						</tr>
						<tr>
							<td>Tacitus</td>
							<td>12/1</td>
							<td>+1200</td>
						</tr>
						<tr>
							<td>Gunnevera</td>
							<td>20/1</td>
							<td>+2000</td>
						</tr>
						<tr>
							<td>Bravazo</td>
							<td>10/1</td>
							<td>+1000</td>
						</tr>
						<tr>
							<td>Spun To Run</td>
							<td>4/1</td>
							<td>+400</td>
						</tr>
						<tr>
							<td>Roadster</td>
							<td>10/1</td>
							<td>+1000</td>
						</tr>
						<tr>
							<td>Master Fencer</td>
							<td>33/1</td>
							<td>+3300</td>
						</tr>
					</tbody>
				</table>	
				</td>
			</tr>
		</tbody>
	</table>
    </div>
