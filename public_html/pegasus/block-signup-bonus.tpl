<section class="card">
    <div class="card_wrap">
        <div class="card_half card_content">
            <a href="/pegasus-world-cup/signup-bonus">
                {* <img src="/img/index-kd/icon-kentucky-derby-betting.png" alt="Bet on Kentucky Derby"
                    class="card_icon"> *}
                <img class="icon-kentucky-derby-betting card_icon"
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABVAQMAAADUnuB8AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABFJREFUeNpjYBgFo2AUDHIAAAOnAAHnt6kCAAAAAElFTkSuQmCC">
            </a>
            <h2 class="card_heading">Sign Up today and get an exclusive welcome package.</h2>
            <h3 class="card_subheading">
            <p style="margin-bottom: 10px">For your first deposit with BUSR, you'll get an additional 10% bonus to your deposit absolutely free. No Limits!</p>
            <p>Plus for the Pegasus World Cup weekend, all your bets at Gulfstream Park qualify for double rebates. That's right! Get up to 16% back on your bets, win or lose.</p>
            <a href="/pegasus-world-cup/signup-bonus" class="card_btn">Learn More</a>
        </div>
        <div class="card_half card_hide-mobile">
            <a href="/pegasus-world-cup/signup-bonus">
                <img src="/img/pegasus-world-cup/signup-offer.jpg" class="card_img" alt="Pegasus World Cup 2020 Signup Offer">
            </a>
        </div>
    </div>
</section>