<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered">
  <tbody>
  <tr>
    <td>Year</td>
    <td>Winner</td>
    <td>Pool 1</td>
    <td>Pool 2</td>
    <td>Pool 3</td>
    <td>Derby Day</td>
  </tr>
  <tr>
    <td>2013</td>
    <td>Orb</td>
    <td>$4.40 (f)</td>
    <td>$26.20</td>
    <td>$29.60</td>
    <td>$12.80</td>
  </tr>
  <tr>
    <td>2012</td>
    <td>I'll Have Another</td>
    <td>$60.20</td>
    <td>$46.20</td>
    <td>$45.60</td>
    <td>$32.60</td>
  </tr>
  <tr>
    <td>2011</td>
    <td>Animal Kingdom</td>
    <td>$6.20 (f)</td>
    <td>$9.40 (f)</td>
    <td>$64.40</td>
    <td>$43.80</td>
  </tr>
  <tr> </tr>
  <tr>
    <td>2010</td>
    <td>Super Saver</td>
    <td>$43.20</td>
    <td>$51.20</td>
    <td>$73.00</td>
    <td>$18.00</td>
  </tr>
  <tr>
    <td>2009</td>
    <td>Mine That Bird</td>
    <td>$5.80 (f) *</td>
    <td>$11.80 (f) *</td>
    <td>$36.80 (f)</td>
    <td>$103.20</td>
  </tr>
  <tr>
    <td>2008</td>
    <td>Big Brown</td>
    <td>$8.60 (f)*</td>
    <td>$15.00 (f)</td>
    <td>$8.60*</td>
    <td>$6.80*</td>
  </tr>
  <tr>
    <td>2007</td>
    <td>Street Sense</td>
    <td>$22.80</td>
    <td>$18.20</td>
    <td>$15.40</td>
    <td>$11.80*</td>
  </tr>
  <tr>
    <td>2006</td>
    <td>Barbaro</td>
    <td>$40.20</td>
    <td>$32.20</td>
    <td>$20.80</td>
    <td>$14.20</td>
  </tr>
  <tr>
    <td>2005</td>
    <td>Giacomo</td>
    <td>$52.00</td>
    <td>$54.20</td>
    <td>$103.60</td>
    <td>$102.60</td>
  </tr>
  <tr>
    <td>2004</td>
    <td>Smarty Jones</td>
    <td>$5.60 (f)</td>
    <td>$10.80 (f)</td>
    <td>$23.60</td>
    <td>$10.20*</td>
  </tr>
  <tr>
    <td>2003</td>
    <td>Funny Cide</td>
    <td>$188.00***</td>
    <td>$120.80</td>
    <td>$23.60</td>
    <td>$27.60</td>
  </tr>
  <tr>
    <td>2002</td>
    <td>War Emblem</td>
    <td>$7.60 (f)*</td>
    <td>$16.00</td>
    <td>$24.00</td>
    <td>$43.00</td>
  </tr>
  <tr>
    <td>2001</td>
    <td>Monarchos</td>
    <td>$36.60</td>
    <td>$13.00</td>
    <td>$15.80</td>
    <td>$23.00</td>
  </tr>
  <tr>
    <td>2000</td>
    <td>Fusaichi Pegasus</td>
    <td>$27.80</td>
    <td>$26.40 (f)</td>
    <td>$ 8.00*</td>
    <td>$ 6.60*</td>
  </tr>
  <tr>
    <td>1999</td>
    <td>Charismatic</td>
    <td>$10.20 (f)*</td>
    <td>$30.20 (f)</td>
    <td>$26.60 (f)</td>
    <td>$64.60</td>
  </tr>
  </tbody>
</table>
</div>
<small>(f) – Mutuel field *Favorite **KDFW single pool wagering record
<br />***Record KDFW win payout | ****KDFW total wagering (all pool) record</small>
