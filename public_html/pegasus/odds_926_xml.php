	<div class="table-responsive">
		<table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Kentucky Derby Odds"  summary="The latest odds for the Kentucky Derby available for wagering now at BUSR.">
			<caption>2017 Kentucky Derby Props</caption>
			<tbody>
			
<tr>
					<th colspan="3" class="center">
					Horses - Kentucky Derby - Props  - May 07					</th>
			</tr>

	<tr><th colspan="3" class="center">2016 Kentucky Derby Winning Time</th></tr><tr class='head_title'><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Over 2:02.50</td><td>EV</td><td>EV</td></tr><tr><td>Under 2:02.50</td><td>-130</td><td>10/13</td></tr><tr><th colspan="3" class="center">2016 Kentucky Derby - Margin Of Victory</th></tr><tr><td>1 Length To 2 3/4 Lengths</td><td>+200</td><td>2/1</td></tr><tr><td>3 Lengths To 5 3/4 Lengths</td><td>+225</td><td>9/4</td></tr><tr><td>1/2 Length To 3/4 Length</td><td>+375</td><td>15/4</td></tr><tr><td>6 Lengths To 7 3/4 Lengths</td><td>+650</td><td>13/2</td></tr><tr><td>8 Lengths To 10 3/4 Lengths</td><td>+750</td><td>15/2</td></tr><tr><td>Head</td><td>+800</td><td>8/1</td></tr><tr><td>Neck</td><td>+800</td><td>8/1</td></tr><tr><td>11 Lengths To 14 3/4 Lengths</td><td>+1000</td><td>10/1</td></tr><tr><td>Nose</td><td>+1000</td><td>10/1</td></tr><tr><td>15 Lengths Or More</td><td>+1200</td><td>12/1</td></tr><tr><td>Dead Heat</td><td>+3000</td><td>30/1</td></tr><tr><th colspan="3" class="center">Will The Derby Winner Win The Belmont</th></tr><tr><td>Yes</td><td>+320</td><td>16/5</td></tr><tr><td>No</td><td>-300</td><td>1/3</td></tr><tr><th colspan="3" class="center">Will The Derby Winner Win The Preakness</th></tr><tr><td>Yes</td><td>+150</td><td>3/2</td></tr><tr><td>No</td><td>-180</td><td>5/9</td></tr>		
<tr>
					<th colspan="3" class="center">
					All Bets Are Action					</th>
			</tr>

	<tr><th colspan="3" class="center">Will A Horse Lead Wire To Wire?</th></tr><tr><td>Yes</td><td>+450</td><td>9/2</td></tr><tr><td>No</td><td>-700</td><td>1/7</td></tr>		
<tr>
					<th colspan="3" class="center">
					Will 3 Separate Horses Win The Kentucky Derby,<br />Preakness Stakes And Belmont Stakes In 2016?					</th>
			</tr>

	<tr><th colspan="3" class="center">3 Separate Horses To Win Each Triple Crown Race</th></tr><tr><td>Yes</td><td>+105</td><td>21/20</td></tr><tr><td>No</td><td>-135</td><td>20/27</td></tr>		
<tr>
					<th colspan="3" class="center">
					Will Secretariats 1973 Record Time Be Beaten<br />In The 2016 Kentucky Derby					</th>
			</tr>

	<tr><th colspan="3" class="center">Will Secretariats 1973 Record (time) Be Beaten?</th></tr><tr><td>Yes Under 1:59.40</td><td>+1000</td><td>10/1</td></tr><tr><td>No Over 1:59.40</td><td>-1300</td><td>1/13</td></tr>					<tr>
						<td class="dateUpdated center" colspan="5">
							<em id='updateemp'>  - Updated January 13, 2017 20:00:22 </em>
							BUSR - Official <a href="https://www.usracing.com/kentucky-derby/odds">Kentucky Derby Odds</a>. 

<!-- br />
							All odds are fixed odds prices. -->
						</td>
					</tr>
			</tbody>
		</table>
	</div>
	
