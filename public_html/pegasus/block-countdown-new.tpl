{literal}
    <style>
        @media (min-width: 768px) {
            .countdown-icon {
                width: 100px;
            }
        }
    </style>
{/literal}
<section class="countdown-section sm">
  <div class="countdown-top-block">
    <div class="countdown-icon icon-left">
      <img src="/img/pegasus-world-cup/pegasus-countdown-icon.png">
    </div>

    <div class="countdown-text">
      <span>Countdown to the {include_php file="/home/ah/allhorse/public_html/pegasus/running.php"} Pegasus World Cup</span>
    </div>

    <div class="countdown-icon icon-right">
      <img src="/img/pegasus-world-cup/pegasus-countdown-icon.png" style="transform: scaleX(-1);">
    </div>
  </div>

  <div class="countdown-bottom-block">
    <div class="countdown-icon icon-left">
      <img src="/img/pegasus-world-cup/pegasus-countdown-icon.png">
    </div>

    <div class="usr-countdown">
      <div class="timer">
        <span id="cd-days" class="quantity"></span>
        <span class="text">Days</span>
      </div>
      <div class="timer">
        <span id="cd-hours" class="quantity"></span>
        <span class="text">Hours</span>
      </div>
      <div class="timer">
        <span id="cd-minutes" class="quantity"></span>
        <span class="text">Minutes</span>
      </div>
      <div class="timer last">
        <span id="cd-seconds" class="quantity"></span>
        <span class="text">Seconds</span>
      </div>
    </div>

    <div class="countdown-icon icon-right">
      <img src="/img/pegasus-world-cup/pegasus-countdown-icon.png" style="transform: scaleX(-1);">
    </div>
  </div>

</section>

{literal}
    <script>
        var countDownDate = new Date("jan 23, 2021 17:34:00").getTime();
    </script>
    <script src="/assets/plugins/countdown/countdown.js"></script>    
{/literal}