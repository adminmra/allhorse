<h2>Pegasus World Cup Turf Winners</h2>
<div id="no-more-tables">
<table id="sortTable-2" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" summary="Winners of the Pegasus World Turf Cup."> 
  <thead>
    <tr>
      <th width="75px">Year </th>
      <th width="95px">Winner </th>
      <th width="75px">Age </th>
      <th>Jockey </th>
      <th>Trainer </th>
      <th>Owner </th>
      <th width="85px">Time </th>
    </tr>
    </thead>
    <tbody>
    <tr>
      <td data-title="Year">2021</td>
      <td data-title="Winner">Colonel Liam</td>
      <td data-title="Age">4</td>
      <td data-title="Jockey">Irad Ortiz Jr.</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Owner">Robert E. and Lawana L. Low</td>
      <td data-title="Time">1:53.09</td>
    </tr>
    <tr>
      <td data-title="Year">2020</td>
      <td data-title="Winner">Zulu Alpha</td>
      <td data-title="Age">7</td>
      <td data-title="Jockey">Tyler Gaffalione</td>
      <td data-title="Trainer">Michael J. Maker</td>
      <td data-title="Owner">Michael M. Hui </td>
      <td data-title="Time">1:51.60</td>
    </tr>
    <tr>
      <td data-title="Year">2019</td>
      <td data-title="Winner">Bricks and Mortar</td>
      <td data-title="Age">5</td>
      <td data-title="Jockey">Irad Ortiz Jr.</td>
      <td data-title="Trainer">Chad C. Brown</td>
      <td data-title="Owner">William H. Lawrence  -  Klaravich Stables Inc</td>
      <td data-title="Time">1:54.6</td>
    </tr>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Heart to Heart</td>
      <td data-title="Age">7</td>
      <td data-title="Jockey">Julien R. Leparoux</td>
      <td data-title="Trainer">Brian A. Lynch</td>
      <td data-title="Owner">Terry Hamilton</td>
      <td data-title="Time">1:47.64</td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Almanaar</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">Chad Brown</td>
      <td data-title="Owner">Shadwell Racing</td>
      <td data-title="Time">1:45.6</td>
    </tr>
  </tbody>
</table></div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>
{/literal}