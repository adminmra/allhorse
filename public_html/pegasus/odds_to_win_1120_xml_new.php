
    <script type="application/ld+json">
          {
                "@context": "http://schema.org",
                "@type": "Table",
                "name": "2021 Pegasus World Cup Odds",
                "about": {
                        "@type": "Event",
                        "name": "Pegasus World Cup 2021",
                        "location": {
                                "@type": "Place",
                                "name": "Gulfstream Park",
                                "address": {
                                        "@type": "PostalAddress",
                                        "addressLocality": "Hallandale Beach",
                                        "addressRegion": "Florida"
                                }
                        },
                        "startDate": "2021-01-23",
                        "description": "Latest Pegasus World Cup 2021 odds at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus 2021."
                },
                "keywords": "List of Contenders Pegasus World Cup, Pegasus World Cup Odds, Pegasus World Cup 2021, Pegasus World Cup 2021 Contenders, Pegasus World Cup, Bet On The Pegasus World Cup, Pegasus World Cup Invitational"
          }
        </script>



<div id="no-more-tables">
    <table id="infoEntries" class="data table table-condensed table-striped table-bordered ordenable justify-center" cellpadding="0"
        cellspacing="0" border="0" title="2021 Pegasus World Cup Odds and Post Position"
        summary="The latest odds for the Pegasus World Cup available">

        <caption class="hide-sm">2021 Pegasus World Cup Odds and Post Position</caption>
      

        <tbody>
                <tr class='head_title hide-sm'>
          <th>PP</th>
          <th>Horse</th>
          <th>Jockey</th>
          <th>Trainer</th>
          <th style="width:320px">Owner</th>
          <th>Odds</th>
            </tr>

<tr>
<td data-title="PP"><center>1</center></td>
<td data-title="Horse"><center>Sleepy Eyes Todd</center></td>
<td data-title="Jockey"><center>Jose Ortiz</center></td>
<td data-title="Trainer"><center>Miguel Angel Silva</center></td>
<td data-title="Owner"><center>Thumbs Up Racing, LLC</center></td>
<td data-title="Odds"><center>8-1</center></td>
</tr>
<tr>
<td data-title="PP"><center>2</center></td>
<td data-title="Horse"><center>Coastal Defense</center></td>
<td data-title="Jockey"><center> Corey Lanerie</center></td>
<td data-title="Trainer"><center>Dale Romans</center></td>
<td data-title="Owner"><center>Albaugh Family Stables LLC and Helen K. Groves Revocable Trust</center></td>
<td data-title="Odds"><center>15-1</center></td>
</tr>
<tr>
<td data-title="PP"><center>3</center></td>
<td data-title="Horse"><center>Independence Hall (Show)</center></td>
<td data-title="Jockey"><center>Flavien Prat</center></td>
<td data-title="Trainer"><center>Michael W. McCarthy</center></td>
<td data-title="Owner"><center>Eclipse Thoroughbred Partners, Twin Creeks Racing Stables, LLC, Verratti, Kathleen and Verratti, Robert N.</center></td>
<td data-title="Odds"><center>20-1</center></td>
</tr>
<tr>
<td data-title="PP"><center>4</center></td>
<td data-title="Horse"><center>Knicks Go (Win)</center></td>
<td data-title="Jockey"><center>Joel Rosario</center></td>
<td data-title="Trainer"><center>Brad Cox</center></td>
<td data-title="Owner"><center>Korea Racing Authority</center></td>
<td data-title="Odds"><center>5-2</center></td>
</tr>
<tr>
<td data-title="PP"><center>5</center></td>
<td data-title="Horse"><center>Jesus' Team (Place)</center></td>
<td data-title="Jockey"><center>Irad Ortiz, Jr.</center></td>
<td data-title="Trainer"><center>Jose D’Angelo</center></td>
<td data-title="Owner"><center>Grupo 7C Racing Stable</center></td>
<td data-title="Odds"><center>8-1</center></td>
</tr>
<tr>
<td data-title="PP"><center>6</center></td>
<td data-title="Horse"><center>Kiss Today Goodbye</center></td>
<td data-title="Jockey"><center>Luis Saez</center></td>
<td data-title="Trainer"><center>J. Eric Kruljac</center></td>
<td data-title="Owner"><center>John Sondereker</center></td>
<td data-title="Odds"><center>10-1</center></td>
</tr>
<tr>
<td data-title="PP"><center>7</center></td>
<td data-title="Horse"><center>Tax</center></td>
<td data-title="Jockey"><center>Luis Saez</center></td>
<td data-title="Trainer"><center>Danny Gargan</center></td>
<td data-title="Owner"><center>R.A. Hill Stable, Reeves Thoroughbred Racing, Hugh Lynch</center></td>
<td data-title="Odds"><center>5-1</center></td>
</tr>
<tr>
<td data-title="PP"><center>8</center></td>
<td data-title="Horse"><center>Harpers First Ride</center></td>
<td data-title="Jockey"><center>Angel Cruz</center></td>
<td data-title="Trainer"><center>Claudio A. Gonzalez</center></td>
<td data-title="Owner"><center>MCA Racing Stable LLc</center></td>
<td data-title="Odds"><center>10-1</center></td>
</tr>
<tr>
<td data-title="PP"><center>9</center></td>
<td data-title="Horse"><center>Last Judgment</center></td>
<td data-title="Jockey"><center>Paco Lopez</center></td>
<td data-title="Trainer"><center>Michael J. Maker</center></td>
<td data-title="Owner"><center>Dubb, Michael, Hornstock, Steve, Bethlehem Stables LLC and Nice Guys Stables</center></td>
<td data-title="Odds"><center>20-1</center></td>
</tr>
<tr>
<td data-title="PP"><center>10</center></td>
<td data-title="Horse"><center>Code of Honor</center></td>
<td data-title="Jockey"><center>Tyler Gaffalione</center></td>
<td data-title="Trainer"><center>Shug McGaughey</center></td>
<td data-title="Owner"><center>W.S. Farish</center></td>
<td data-title="Odds"><center>9-2</center></td>
</tr>
<tr>
<td data-title="PP"><center>11</center></td>
<td data-title="Horse"><center>Mr Freeze</center></td>
<td data-title="Jockey"><center>John Velazquez</center></td>
<td data-title="Trainer"><center>Dale Romans</center></td>
<td data-title="Owner"><center>Jim Bakke, Gerald Isbister</center></td>
<td data-title="Odds"><center>15-1</center></td>
</tr>
<tr>
<td data-title="PP"><center>12</center></td>
<td data-title="Horse"><center>Math Wizard</center></td>
<td data-title="Jockey"><center>Edgard Zayas</center></td>
<td data-title="Trainer"><center>Saffie Joseph Jr.</center></td>
<td data-title="Owner"><center>John Fanelli, Khalid Mishref, Cash is King LLC, LC Racing LLC, Collarmele Vitelli Stables LLC, Ioannis, Zouas, Bassett Stables</center></td>
<td data-title="Odds"><center>20-1</center></td>
</tr>


      </tbody>
    </table>


</div>


<style>
    table.ordenable tbody th {
        cursor: pointer;
    }
    @media only screen and (max-width: 800px) {
        #no-more-tables td span {
            text-align: center !important;
        }
        .hide-sm {
          display: none !important;
        }
    }
</style>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">

<!-- <script src="/assets/js/jquery.sortElements.js"></script>
<script src="//www.usracing.com/assets/js/sorttable.js"></script> -->
<script src="//www.usracing.com/assets/js/aligncolumn.js?v=2021-01-20-13-20"></script>

<script>
    $(document).ready(function () {

        alignColumn([2], 'left');

    });
</script>










