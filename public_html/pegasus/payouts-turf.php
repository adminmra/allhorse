<h2>2021 Pegasus World Cup Turf Payouts</h2>
<div>
	<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
		title="Breeders' Classic Payouts" summary="Last year's Payouts of the Breeders' Cup Classic. ">
		<tbody>
			<thead>
			<tr>
				<th>PP</th>
				<th>Horses</th>
				<th>Win</th>
				<th>Place</th>
				<th>Show</th>
			</tr>
			</thead>
			<tr>
				<td>5</td>
				<td>Colonel Liam</td>
				<td>$7.00</td>
				<td>$4.20</td>
				<td>$3.20</td>
			</tr>
			<tr>
				<td>6</td>
				<td>Largent</td>
				<td></td>
				<td>$5.00</td>
				<td>$3.80</td>
			</tr>
			<tr>
				<td>9</td>
				<td>Cross Border</td>
				<td></td>
				<td></td>
				<td>$6.40</td>
			</tr>
		</tbody>
	</table>
</div>
<p></p>
<div id="no-more-tables">
	<table border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered"
		title="Breeders' Classic Payouts" summary="Last year's Payouts of the Breeders' Cup Classic. ">
		<thead>
			<tr>
				<th>Wager</th>
				<th>Horses</th>
				<th>Denomination</th>
				<th>Payout</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td data-title="Wager">Exacta</td>
				<td data-title="Horses">5-6</td>
				<td data-title="Denomination">$2.00</td>
				<td data-title="Payout">$28.20</td>
			</tr>
			<tr>
				<td data-title="Wager">Trifecta</td>
				<td data-title="Horses">5-6-9</td>
				<td data-title="Denomination">$0.50</td>
				<td data-title="Payout">$96.05</td>
			</tr>
			<tr>
				<td data-title="Wager">Superfecta</td>
				<td data-title="Horses">5-6-9-12</td>
				<td data-title="Denomination">$1.00</td>
				<td data-title="Payout">$1,143.20</td>
			</tr>
			
			<tr>
				<td data-title="Wager">Double</td>
				<td data-title="Horses">7-5</td>
				<td data-title="Denomination">$1.00</td>
				<td data-title="Payout">$23.00</td>
			</tr>
			<tr>
				<td data-title="Wager">Pick 3</td>
				<td data-title="Horses">6-7-5</td>
				<td data-title="Denomination">$0.50</td>
				<td data-title="Payout">$282.55</td>
			</tr>
		
		</tbody>
	</table>
</div>
