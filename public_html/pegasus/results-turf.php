<h2>2021 Pegasus World Cup Turf Results</h2>
<!--fix this to the new table-->
<div id="no-more-tables">
<table  class="table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0" title="Pegasus World Cup Results" summary="Pegasus World Cup Results">
<thead>
    <tr>
      <th>Results</th>
      <th>Horse</th>
      <th>Trainer</th>
      <th>Jockey</th>
    </tr>
    </thead>
  <tbody>
      <tr>
<td data-title="Results">1st</td>
<td data-title="Horse">Colonel Liam</td>
<td data-title="Trainer">Todd A. Pletcher</td>
<td data-title="Jockey">Irad Ortiz Jr.</td>
</tr>
<tr>
<td data-title="Results">2nd</td>
<td data-title="Horse">Largent</td>
<td data-title="Trainer">Todd A. Pletcher</td>
<td data-title="Jockey">Paco Lopez</td>
</tr>
<tr>
<td data-title="Results">3rd</td>
<td data-title="Horse">Cross Border</td>
<td data-title="Trainer">Michael J. Maker</td>
<td data-title="Jockey">Tyler Gaffalione</td>
</tr>
<tr>
<td data-title="Results">4th</td>
<td data-title="Horse">Social Paranoia</td>
<td data-title="Trainer">Todd A. Pletcher</td>
<td data-title="Jockey">Luis Saez</td>
</tr>

  </tbody>
</table>
</div>

<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>
