    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0" title="Kentucky Derby Jockey Odds" summary="The latest odds for the top Jockeys for the Kentucky Derby.  Only available at BUSR.">
            <caption>Kentucky Derby Jockey Odds</caption>
            <tbody>
               <!--
 <tr>
                    <th colspan="3" class="center">
                    Horses - 2017 Kentucky Derby Jockey  - May 01                    </th>
            </tr>
-->
    <tr><th colspan="3" class="center">2017 Kentucky Derby Jockey - Winner</th></tr><tr class='head_title'><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Julien Leparoux</td><td>+600</td><td>6/1</td></tr><tr><td>Mike Smith</td><td>+800</td><td>8/1</td></tr><tr><td>Victor Espinoza</td><td>+800</td><td>8/1</td></tr><tr><td>Gary Stevens</td><td>+900</td><td>9/1</td></tr><tr><td>Brian Hernandez</td><td>+1200</td><td>12/1</td></tr><tr><td>Javier Castellano</td><td>+1200</td><td>12/1</td></tr><tr><td>John R. velazquez</td><td>+1200</td><td>12/1</td></tr><tr><td>Calvin Borel</td><td>+1500</td><td>15/1</td></tr><tr><td>Joel Rosario</td><td>+1500</td><td>15/1</td></tr><tr><td>Jose Ortiz</td><td>+2500</td><td>25/1</td></tr><tr><td>Declan Cannon</td><td>+3000</td><td>30/1</td></tr><tr><td>Fergal Lynch</td><td>+3000</td><td>30/1</td></tr><tr><td>Florent Geroux</td><td>+3500</td><td>35/1</td></tr><tr><td>Rafael Bejarano</td><td>+3500</td><td>35/1</td></tr><tr><td>Antonio Gallardo</td><td>+4000</td><td>40/1</td></tr><tr><td>Channing Hill</td><td>+4000</td><td>40/1</td></tr><tr><td>Manuel Franco</td><td>+4000</td><td>40/1</td></tr><tr><td>Ricardo Santana Jr.</td><td>+4000</td><td>40/1</td></tr><tr><td>Brian Pedroza</td><td>+5000</td><td>50/1</td></tr><tr><td>Joe Bravo</td><td>+5000</td><td>50/1</td></tr>                    <tr>
                        <td class="dateUpdated center" colspan="5">
                            <em id='updateemp'>Updated January 13, 2017 20:00:36.</em>
                            BUSR - Official <a href="https://www.usracing.com/kentucky-derby/jockey-betting">Kentucky Derby Jockey Odds</a>. <!-- br />
                            All odds are fixed odds prices. -->
                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
    