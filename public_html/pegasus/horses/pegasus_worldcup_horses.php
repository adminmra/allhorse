<script type="application/ld+json">
    { "@context": "http://schema.org", "@type": "Table", "name": "2020 Pegasus World Cup Contenders", "about": { "@type": "Event", "name": "Pegasus World Cup 2020", "location": { "@type": "Place", "name": "Gulfstream Park", "address": { "@type": "PostalAddress", "addressLocality": "Hallandale Beach", "addressRegion": "Florida" } }, "startDate": "2020-01-25", "description": "Latest Pegasus World Cup 2020 odds at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus 2020." }, "keywords": "List of Contenders Pegasus World Cup, Pegasus World Cup Odds, Pegasus World Cup 2020, Pegasus World Cup 2020 Contenders, Pegasus World Cup, Bet On The Pegasus World Cup, Pegasus World Cup Invitational" }
</script>
<div id="no-more-tables">
<table
    id="infoEntries"
    class="table table-condensed table-striped table-bordered ordenableResult data"
    title="Pegasus World Cup 2021"
    border="0"
    summary="The 10 horses given first preference (in alphabetical order):"
    cellspacing="0"
    cellpadding="0"
>
    <thead>
        <tr>
            <th style="width: 8%;">Horse</th>
            <th style="width: 8%;">Trainer</th>
            <th style="width: 8%;">Owner</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-title="Horse"><center>Code of Honor</center></td>
            <td data-title="Trainer"><center>Shug McGaughey</center></td>
            <td data-title="Owner"><center>W.S. Farish</center></td>
        </tr>
        <tr>
            <td data-title="Horse"><center>Harpers First Ride</center></td>
            <td data-title="Trainer"><center>Claudio A. Gonzalez</center></td>
            <td data-title="Owner"><center>MCA Racing Stable LLc</center></td>
        </tr>
        <tr>
            <td data-title="Horse"><center>Jesus’ Team</center></td>
            <td data-title="Trainer"><center>Jose D’Angelo</center></td>
            <td data-title="Owner"><center>Grupo 7C Racing Stable</center></td>
        </tr>
        <tr>
            <td data-title="Horse"><center>Kiss Today Goodbye</center></td>
            <td data-title="Trainer"><center>J. Eric Kruljac</center></td>
            <td data-title="Owner"><center>John Sondereker</center></td>
        </tr>
        <tr>
            <td data-title="Horse"><center>Knicks Go</center></td>
            <td data-title="Trainer"><center>Brad Cox</center></td>
            <td data-title="Owner"><center>Korea Racing Authority</center></td>
        </tr>
        <tr>
            <td data-title="Horse"><center>Mr Freeze</center></td>
            <td data-title="Trainer"><center>Dale Romans</center></td>
            <td data-title="Owner"><center>Jim Bakke, Gerald Isbister</center></td>
        </tr>
        <tr>
            <td data-title="Horse"><center>Sleepy Eyes Todd</center></td>
            <td data-title="Trainer"><center>Miguel Angel Silva</center></td>
            <td data-title="Owner"><center>Thumbs Up Racing, LLC</center></td>
        </tr>
        <tr>
            <td data-title="Horse"><center>Tax</center></td>
            <td data-title="Trainer"><center>Danny Gargan</center></td>
            <td data-title="Owner"><center>R.A. Hill Stable, Reeves Thoroughbred Racing, Hugh Lynch</center></td>
        </tr>
        <tr>
            <td data-title="Horse"><center>True Timber</center></td>
            <td data-title="Trainer"><center>Jack Sisterson</center></td>
            <td data-title="Owner"><center>Calumet Farm</center></td>
        </tr>
        <tr>
            <td data-title="Horse"><center>Math Wizard</center></td>
            <td data-title="Trainer"><center>Saffie Joseph Jr.</center></td>
            <td data-title="Owner"><center>John Fanelli, Khalid Mishref, Cash is King LLC, LC Racing LLC, Collarmele Vitelli Stables LLC, Ioannis, Zouas, Bassett Stables</center></td>
        </tr>
        <tr>
            <td data-title="Horse"><center>Independence Hall</center></td>
            <td data-title="Trainer"><center>Michael W. McCarthy</center></td>
            <td data-title="Owner"><center>Eclipse Thoroughbred Partners, Twin Creeks Racing Stables, LLC, Verratti, Kathleen and Verratti, Robert N.</center></td>
        </tr>
        <tr>
            <td data-title="Horse"><center>Coastal Defense</center></td>
            <td data-title="Trainer"><center>Dale Romans</center></td>
            <td data-title="Owner"><center>Albaugh Family Stables LLC and Helen K. Groves Revocable Trust</center></td>
        </tr>
    </tbody>
</table>

    
</div>

<style type="text/css">
    table.ordenableResult th {
        cursor: pointer
    }
</style>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">

<script src="/assets/js/sorttable_date.js"></script>
