<script type="application/ld+json">
    { "@context": "http://schema.org", "@type": "Table", "name": "2020 Pegasus World Cup Turf Contenders", "about": { "@type": "Event", "name": "Pegasus World Cup Turf 2020", "location": { "@type": "Place", "name": "Gulfstream Park", "address": { "@type": "PostalAddress", "addressLocality": "Hallandale Beach", "addressRegion": "Florida" } }, "startDate": "2020-01-25" }, "keywords": "List of Contenders Pegasus World Cup Turf, Pegasus World Cup Turf Odds, Pegasus World Cup 2020, Pegasus World Cup Turf 2020 Contenders, Pegasus World Cup, Bet On The Pegasus World Cup, Pegasus World Cup Invitational" }

</script>
<div id="no-more-tables">
    <table id="infoEntries" class="table table-condensed table-striped table-bordered ordenableResults" border="0" cellspacing="0" cellpadding="10">
        <thead>

            <tr>
		<th style="width: 8%;"> PP </th>
                <th> Horse </th>
		<th> Jockey </th>
                <th style="width: 11%;"> Trainer </th>
                <th> Breeder </th>
                <th> Owner </th>
                <th style="width: 11%;"> Earnings </th>
            </tr>
        </thead>

        <tbody>

<tr>
    <td data-title="Post-Position"> 1 </td>
    <td data-title="Horse"> Zulu Alpha </td>
    <td data-title="Jockey"> Tyler Gaffalione </td>
    <td data-title="Trainer"> Michael J. Maker </td>
    <td data-title="Breeder"> Calumet Farm </td>
    <td data-title="Owner"> Michael M. Hui </td>
    <td data-title="Earnings"> $1,371,674 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 2 </td>
    <td data-title="Horse"> Arklow </td>
    <td data-title="Jockey"> Luis Saez </td>
    <td data-title="Trainer"> Brad H. Cox </td>
    <td data-title="Breeder"> John R. Penn & Frank Penn </td>
    <td data-title="Owner"> Donegal Racing, Bulger, Joseph and Coneway, Peter  </td>
    <td data-title="Earnings"> $1,816,382 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 3 </td>
    <td data-title="Horse"> Without Parole </td>
    <td data-title="Jockey"> Frankie Dettori </td>
    <td data-title="Trainer"> Chad C. Brown </td>
    <td data-title="Breeder"> Mr John Gunther </td>
    <td data-title="Owner"> Gunther, John D. and Gunther, Tanya </td>
    <td data-title="Earnings"> $834,030 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 4 </td>
    <td data-title="Horse"> Sadler’s Joy </td>
    <td data-title="Jockey"> Javier Castellano </td>
    <td data-title="Trainer"> Thomas Albertrani </td>
    <td data-title="Breeder"> Woodslane Farm, LLC. </td>
    <td data-title="Owner"> Woodslane Farm </td>
    <td data-title="Earnings"> $2,471,360 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 5 </td>
    <td data-title="Horse"> Channel Cat </td>
    <td data-title="Jockey"> John Velazquez </td>
    <td data-title="Trainer"> Todd A. Pletcher </td>
    <td data-title="Breeder"> Calumet Farm </td>
    <td data-title="Owner"> Calumet Farm </td>
    <td data-title="Earnings"> $913,992 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 6 </td>
    <td data-title="Horse"> Instilled Regard </td>
    <td data-title="Jockey"> Irad Ortiz, Jr. </td>
    <td data-title="Trainer"> Chad C. Brown </td>
    <td data-title="Breeder"> KatieRich Farms </td>
    <td data-title="Owner"> OXO Equine LLC </td>
    <td data-title="Earnings"> $589,240 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 7 </td>
    <td data-title="Horse"> Admission Office </td>
    <td data-title="Jockey"> Flavien Prat </td>
    <td data-title="Trainer"> Brian A. Lynch </td>
    <td data-title="Breeder"> Mrs. Jerry Amerman </td>
    <td data-title="Owner"> Amerman Racing LLC </td>
    <td data-title="Earnings"> $318,297 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 8 </td>
    <td data-title="Horse"> Henley’s Joy </td>
    <td data-title="Jockey"> Julien Leparoux </td>
    <td data-title="Trainer"> Michael J. Maker </td>
    <td data-title="Breeder"> Kenneth L. Ramsey & Sarah K. Ramsey </td>
    <td data-title="Owner"> Bloom Racing Stable LLC (Jeffrey Bloom) </td>
    <td data-title="Earnings"> $1,070,711 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 9 </td>
    <td data-title="Horse"> Next Shares </td>
    <td data-title="Jockey"> Jose Valdivia, Jr. </td>
    <td data-title="Trainer"> Richard Baltas </td>
    <td data-title="Breeder"> Buck Pond Farm, Inc. </td>
    <td data-title="Owner"> Baltas, Debby, Baltas, Richard, Dunn, Christopher T., Iavarone, Jules, Iavarone, Michael, McClanahan, Jerry, Robershaw, Ritchie and Taylor, Mark </td>
    <td data-title="Earnings"> $1,677,771 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 10 </td>
    <td data-title="Horse"> Mo Forza </td>
    <td data-title="Jockey"> Joel Rosario </td>
    <td data-title="Trainer"> Peter Miller </td>
    <td data-title="Breeder"> Bardy Farm </td>
    <td data-title="Owner"> Bardy Farm and OG Boss </td>
    <td data-title="Earnings"> $499,460 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 11 </td>
    <td data-title="Horse"> Sacred Life </td>
    <td data-title="Jockey"> Jose Ortiz </td>
    <td data-title="Trainer"> Chad C. Brown </td>
    <td data-title="Breeder"> Mr. Viktor Timoshenko & Mr. Andriy Milovanov </td>
    <td data-title="Owner"> Dubb Michael, Madaket Stables LLC, Wonder Stables and Bethlehem Stables LLC Owner Link </td>
    <td data-title="Earnings"> $310,268 </td>
</tr>
<tr>
    <td data-title="Post-Position"> 12 </td>
    <td data-title="Horse"> Magic Wand </td>
    <td data-title="Jockey"> Ryan Moore </td>
    <td data-title="Trainer"> Aidan P. O'Brien </td>
    <td data-title="Breeder"> Ecurie Des Monceaux & Skymarc Farm Inc </td>
    <td data-title="Owner"> Michael Tabor & Derrick Smith & Mrs John Magnier </td>
    <td data-title="Earnings"> $4,146,829 </td>
</tr>

        </tbody>
    </table>
</div>

<style type="text/css">
    table.ordenableResults th {
        cursor: pointer
    }
</style>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_pwc_turf_contenders.js"></script>
