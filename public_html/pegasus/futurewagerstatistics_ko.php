<div class="table-responsive">
<table  border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered">
          <tbody>
            <tr>
              <th>Wagering</th>
              <th>Pool 1</th>
              <th>Pool 2</th>
              <th>Pool 3</th>
              <th>Total</th>
            </tr>
            <tr>
              <td>2011</td>
              <td colspan="3"  align="center">$92,902 (single pool)</td>
              <td>$92,902</td>
            </tr>
            <tr>
              <td>2010</td>
              <td colspan="3"  align="center">$61,901 (single pool)</td>
              <td>$61,901</td>
            </tr>
            <tr>
              <td>2009</td>
              <td colspan="3"  align="center">$72,628 (single pool)</td>
              <td>$72,628</td>
            </tr>
            <tr>
              <td>2008</td>
              <td>$65,219</td>
              <td>$62,503</td>
              <td>$47,116</td>
              <td>$174,838</td>
            </tr>
            <tr>
              <td>2007</td>
              <td>$66,206</td>
              <td>$55,840</td>
              <td>$71,040</td>
              <td>$193,086****</td>
            </tr>
            <tr>
              <td>2006</td>
              <td>$66,548</td>
              <td>$54,054</td>
              <td>$51,212</td>
              <td>$171,814</td>
            </tr>
            <tr>
              <td>2005</td>
              <td>$69,928</td>
              <td>$53,106</td>
              <td>$49,445</td>
              <td>$172,479</td>
            </tr>
            <tr>
              <td>2004</td>
              <td>$66,425</td>
              <td>$46,151</td>
              <td>$45,635</td>
              <td>$158,211</td>
            </tr>
            <tr>
              <td>2003</td>
              <td>$117,368**</td>
              <td>$117,368**</td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
        </div>   