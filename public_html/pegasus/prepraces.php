        <div class='tableSchema table-responsive'>
        <table id='infoEntries' width='100%' border='0' cellspacing='0' cellpadding='0' class='table table-condensed table-striped table-bordered' title='Road to the Roses Kentucky Derby Prep Races'>
        <tbody>
        <tr>
                <!--<th>Date</th> <th>Race</th> <th>Race Distance</th>  <th>Track</th> <th>Points</th>-->
                <th>Date</th>  <th>Race</th> <th>Distance</th> <th>Track</th> <th>Winner</th> <th>1st</th> <th>2nd</th> <th>3rd</th> <th>4th</th> <!--<th>Results</th> <th>Beyer</th> <th>Brisnet<th> <th>EQB<th> <th>TFUS</th> <th>Optix<th>-->
        </tr>
                    <tr  >
                <td>09-17-16 </td>
                <td>Iroquois </td>
                <td>1 1/16 m </td>
                <td>CD </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>Not This Time</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr class='odd' >
                <td>10-01-16 </td>
                <td>FrontRunner </td>
                <td>1 1/16 m </td>
                <td>SA </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>Gormley</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr  >
                <td>10-08-16 </td>
                <td>Champagne </td>
                <td>1 m </td>
                <td>Bel </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>Practical Joke</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr class='odd' >
                <td>10-08-16 </td>
                <td>Breeders&#39; Futurity </td>
                <td>1 1/16 m </td>
                <td>Kee </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>Classic Empire</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr  >
                <td>11-05-16 </td>
                <td>Breeders&#39; Cup Juvenile </td>
                <td>1 1/16 m </td>
                <td>SA </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>Classic Empire</td>
                <td>20</td>
                <td>8</td>
                <td>4</td>
                <td>2</td>

            </tr>            <tr class='odd' >
                <td>11-19-16 </td>
                <td>Delta Downs Jackpot </td>
                <td>1 1/16 m </td>
                <td>Ded </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>Gunnevera</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr  >
                <td>11-26-16 </td>
                <td>Cattleya Sho </td>
                <td>1 m </td>
                <td>Tok </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>Mont Saint Legame</td>
                <td>40</td>
                <td>16</td>
                <td>8</td>
                <td>4</td>

            </tr>            <tr class='odd' >
                <td>11-26-16 </td>
                <td>Remsen </td>
                <td>1 1/8 m </td>
                <td>Aqu </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>Mo Town</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr  >
                <td>11-26-16 </td>
                <td>Kentucky Jockey Club </td>
                <td>1 1/16 m </td>
                <td>CD </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>McCraken</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr class='odd' >
                <td>12-10-16 </td>
                <td>Los Alamitos Futurity </td>
                <td>1 1/16 m </td>
                <td>Lrc </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>Mastery</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr  >
                <td>01-02-17 </td>
                <td>Jerome </td>
                <td>1 m 70 yds </td>
                <td>Aqu </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>El Areeb</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr class='odd' >
                <td>01-07-17 </td>
                <td>Sham </td>
                <td>1 m </td>
                <td>SA </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>Gormley</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr  >
                <td>01-16-17 </td>
                <td>Smarty Jones </td>
                <td>1 m </td>
                <td>OP </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr class='odd' >
                <td>01-21-17 </td>
                <td>Lecomte </td>
                <td>1 m 70 yds </td>
                <td>FG </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr  >
                <td>02-04-17 </td>
                <td>Withers </td>
                <td>1 1/16 m </td>
                <td>Aqu </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr class='odd' >
                <td>02-04-17 </td>
                <td>Holy Bull </td>
                <td>1 1/16 m </td>
                <td>GP </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr  >
                <td>02-04-17 </td>
                <td>Robert B. Lewis </td>
                <td>1 1/16 m </td>
                <td>SA </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr class='odd' >
                <td>02-11-17 </td>
                <td>Sam F. Davis </td>
                <td>1 1/16 m </td>
                <td>Tam </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr  >
                <td>02-18-17 </td>
                <td>El Camino Real Derby </td>
                <td>1 1/8 m (S) </td>
                <td>GG </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr class='odd' >
                <td>02-19-17 </td>
                <td>Hyacinth </td>
                <td>1 m </td>
                <td>Tok </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>50</td>
                <td>20</td>
                <td>10</td>
                <td>5</td>

            </tr>            <tr  >
                <td>02-20-17 </td>
                <td>Southwest </td>
                <td>1 1/16 m </td>
                <td>OP </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>            <tr class='odd' >
                <td>02-25-17 </td>
                <td>Risen Star </td>
                <td>1 1/16 m </td>
                <td>FG </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>50</td>
                <td>20</td>
                <td>10</td>
                <td>5</td>

            </tr>            <tr  >
                <td>03-04-17 </td>
                <td>Fountain of Youth </td>
                <td>1 1/16 m </td>
                <td>GP </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>50</td>
                <td>20</td>
                <td>10</td>
                <td>5</td>

            </tr>            <tr class='odd' >
                <td>03-04-17 </td>
                <td>Gotham </td>
                <td>1 1/16 m </td>
                <td>Aqu </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>50</td>
                <td>20</td>
                <td>10</td>
                <td>5</td>

            </tr>            <tr  >
                <td>03-11-17 </td>
                <td>San Felipe </td>
                <td>1 1/16 m </td>
                <td>SA </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>50</td>
                <td>20</td>
                <td>10</td>
                <td>5</td>

            </tr>            <tr class='odd' >
                <td>03-11-17 </td>
                <td>Tampa Bay Derby </td>
                <td>1 1/16 m </td>
                <td>Tam </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>50</td>
                <td>20</td>
                <td>10</td>
                <td>5</td>

            </tr>            <tr  >
                <td>03-18-17 </td>
                <td>Rebel </td>
                <td>1 1/16 m </td>
                <td>OP </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>50</td>
                <td>20</td>
                <td>10</td>
                <td>5</td>

            </tr>            <tr class='odd' >
                <td>03-25-17 </td>
                <td>U.A.E. Derby </td>
                <td>1 3/16 m </td>
                <td>Mey </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>100</td>
                <td>40</td>
                <td>20</td>
                <td>10</td>

            </tr>            <tr  >
                <td>03-25-17 </td>
                <td>Spiral </td>
                <td>1 1/8 m (S) </td>
                <td>TP </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>50</td>
                <td>20</td>
                <td>10</td>
                <td>5</td>

            </tr>            <tr class='odd' >
                <td>03-26-17 </td>
                <td>Sunland Derby </td>
                <td>1 1/8 m </td>
                <td>Sun </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>50</td>
                <td>20</td>
                <td>10</td>
                <td>5</td>

            </tr>            <tr  >
                <td>04-01-17 </td>
                <td>Louisiana Derby </td>
                <td>1 1/8 m </td>
                <td>FG </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>100</td>
                <td>40</td>
                <td>20</td>
                <td>10</td>

            </tr>            <tr class='odd' >
                <td>04-01-17 </td>
                <td>Florida Derby </td>
                <td>1 1/8 m </td>
                <td>GP </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>100</td>
                <td>40</td>
                <td>20</td>
                <td>10</td>

            </tr>            <tr  >
                <td>04-08-17 </td>
                <td>Wood Memorial </td>
                <td>1 1/8 m </td>
                <td>Aqu </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>100</td>
                <td>40</td>
                <td>20</td>
                <td>10</td>

            </tr>            <tr class='odd' >
                <td>04-08-17 </td>
                <td>Blue Grass </td>
                <td>1 1/8 m </td>
                <td>Kee </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>100</td>
                <td>40</td>
                <td>20</td>
                <td>10</td>

            </tr>            <tr  >
                <td>04-08-17 </td>
                <td>Santa Anita Derby </td>
                <td>1 1/8 m </td>
                <td>SA </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>100</td>
                <td>40</td>
                <td>20</td>
                <td>10</td>

            </tr>            <tr class='odd' >
                <td>04-15-17 </td>
                <td>Arkansas Derby </td>
                <td>1 1/8 m </td>
                <td>OP </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>100</td>
                <td>40</td>
                <td>20</td>
                <td>10</td>

            </tr>            <tr  >
                <td>04-15-17 </td>
                <td>Lexington </td>
                <td>1 1/16 m </td>
                <td>Kee </td><!--</td> <td> 
Notice: Undefined property: stdClass::$points in /home/ah/scrappers/programsV2/kd/kentucky_prep_races_2016.php on line 104
 </td>-->
                <td>TBD</td>
                <td>10</td>
                <td>4</td>
                <td>2</td>
                <td>1</td>

            </tr>        </tbody>
        </table>
        </div>

        <div class='tableSchema table-responsive'>
        <table id='infoEntries' width='100%' border='0' cellspacing='0' cellpadding='0' class='table table-condensed table-striped table-bordered' title='Old Kentucky Derby Prep Races'>
        <tbody>
        <!--
        <tr>
            <th>Date</th> <th>Race</th> < ! - -<th>Race Distance</th> - - > <th>Track</th> <th>Points</th>
        </tr>

                 -->
        <tr>
            <td class="dateUpdated center" colspan="4">
                <em id='updateemp'>Updated January 13, 2017 20:00:38.</em>
                BUSR - Official <a href="//www.usracing.com/kentucky-derby/prep-races">Kentucky Derby Preps</a>. <br />
                All Kentucky Derby Preps Races.
            </td>
        </tr>
        </tbody>
        </table>
        </div>
        