    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Kentucky Derby Trainer Odds" summary="The latest odds for the top Trainers for the Kentucky Derby.  Only available at BUSR.">
            <caption> Kentucky Derby Trainer Odds</caption>
            <tbody>
              <!--
  <tr>
                    <th colspan="3" class="center">
                    Horses - 2017 Kentucky Derby Trainer  - May 01                    </th>
            </tr>
-->
    <tr><th colspan="3" class="center">2017 Kentucky Derby Trainer - Winner</th></tr><tr class='head_title'><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Mark Casse</td><td>+600</td><td>6/1</td></tr><tr><td>Bob Baffert</td><td>+800</td><td>8/1</td></tr><tr><td>Antonio Sano</td><td>+1500</td><td>15/1</td></tr><tr><td>Chad C. Brown</td><td>+1500</td><td>15/1</td></tr><tr><td>Todd Pletcher</td><td>+1000</td><td>10/1</td></tr><tr><td>Kiaran Mclaughlin</td><td>+2500</td><td>25/1</td></tr><tr><td>Graham Motion</td><td>+2500</td><td>25/1</td></tr><tr><td>Ian Wilkes</td><td>+2500</td><td>25/1</td></tr><tr><td>Patrick Byrne</td><td>+3000</td><td>30/1</td></tr><tr><td>John Shirreffs</td><td>+4000</td><td>40/1</td></tr><tr><td>David Cannizzo</td><td>+4000</td><td>40/1</td></tr><tr><td>Steven Asmussen</td><td>+4000</td><td>40/1</td></tr><tr><td>Thomas Albertrani</td><td>+4000</td><td>40/1</td></tr><tr><td>Wayne Catalano</td><td>+4000</td><td>40/1</td></tr><tr><td>Dallas Stewart</td><td>+4000</td><td>40/1</td></tr><tr><td>Arnaud Delacour</td><td>+4500</td><td>45/1</td></tr><tr><td>Larry J. Jones</td><td>+4500</td><td>45/1</td></tr><tr><td>Anthony Dutrow</td><td>+5000</td><td>50/1</td></tr><tr><td>George Arnold Ii</td><td>+5000</td><td>50/1</td></tr>                    <tr>
                        <td class="dateUpdated center" colspan="5">
                            <em id='updateemp'>Updated January 13, 2017 20:00:37.</em>
                            BUSR - Official <a href="https://www.usracing.com/kentucky-derby/trainer-betting">Kentucky Derby Trainer Odds</a>. <!-- br />
                            All odds are fixed odds prices. -->
                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
    