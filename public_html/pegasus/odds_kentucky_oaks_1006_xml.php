    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Kentucky Oaks Odds"  summary="The latest odds for the Kentucky Oaks available for wagering now at BUSR.">
            <caption>2016 Kentucky Oaks Odds</caption>
            <tbody>
               <!--
 <tr>
                    <th colspan="3" class="center">
                    Horses - 2016 Kentucky Oaks - Odds  - May 06                    </th>
            </tr>
-->
               <!--
 <tr>
                    <th colspan="3" class="center">
                    2016 Kentucky Oaks - Fixed Odds<br />All Bets Are Action - Run Or Not                    </th>
            </tr>
-->
    <tr class='head_title' ><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Rachels Valentina</td><td>+300</td><td>3/1</td></tr><tr><td>Cathryn Sophia</td><td>+350</td><td>7/2</td></tr><tr><td>Land Over Sea</td><td>+200</td><td>2/1</td></tr><tr><td>Lewis Bay</td><td>+550</td><td>11/2</td></tr><tr><td>Weep No More</td><td>+500</td><td>5/1</td></tr><tr><td>Go Maggie Go</td><td>+1100</td><td>11/1</td></tr><tr><td>Terra Promessa</td><td>+900</td><td>9/1</td></tr><tr><td>Mokat</td><td>+1800</td><td>18/1</td></tr><tr><td>Royal Obsession</td><td>+2000</td><td>20/1</td></tr><tr><td>Taxable</td><td>+2000</td><td>20/1</td></tr><tr><td>Paola Queen</td><td>+2800</td><td>28/1</td></tr><tr><td>Dothraki Queen</td><td>+2800</td><td>28/1</td></tr><tr><td>Dream Dance</td><td>+2800</td><td>28/1</td></tr><tr><td>Mo Damour</td><td>+2800</td><td>28/1</td></tr><tr><td>Venus Valentine</td><td>+3000</td><td>30/1</td></tr>                    <tr>
                        <td class="dateUpdated center" colspan="5">
                            <em id='updateemp'>  - Updated January 13, 2017 20:00:33 </em>
                            BUSR - Official <a href="https://www.usracing.com/kentucky-oaks/odds">Kentucky Oaks Odds</a>. <!-- br />
                            All odds are fixed odds prices. -->
                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
    