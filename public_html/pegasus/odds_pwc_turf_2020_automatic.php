<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "Table",
		"name": "2020 Pegasus World Cup Turf Odds",
		"about": {
			"@type": "Event",
			"name": "Pegasus World Cup Turf 2020",
			"location": {
				"@type": "Place",
				"name": "Gulfstream Park",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "Hallandale Beach",
					"addressRegion": "Florida"
				}
			},
			"startDate": "2020-01-25",
			"endDate" : "2020-01-25",
			"offers": {
				"@type": "Offer",
				"url": "https://www.usracing.com/pegasus-world-cup/signup-bonus"
			},
			"description": "Latest Pegasus World Cup 2020 odds at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus Turf 2020.",
			"image": "/img/pegasus-world-cup/pegasus-world-cup-odds-og.jpg"
		},
		"keywords": "List of Contenders Pegasus World Cup Turf, Pegasus World Cup Turf Odds, Pegasus World Cup 2020, Pegasus World Cup Turf 2020 Contenders, Pegasus World Cup, Bet On The Pegasus World Cup, Pegasus World Cup Invitational"
	}
</script>
<style>
	.none {
		display: none;
	}
</style>



<div id="no-more-tables">
	<table class="oddstable" width="100%" cellpadding="0" cellspacing="0" border="0" summary="Horses - Pegasus World Cup Turf">
		<!-- <tfoot>
		<tr>
			<td class="dateUpdated center" colspan="3">
				<em id='updateemp'> 
					<?php //$tdate = date("M d, Y"); echo $tdate; ?>
				</em>
			</td>
		</tr>
	</tfoot> -->
		<tbody>
			<tr>
				<table id="o1t" width="100%" cellpadding="0" cellspacing="0"
					class="data table table-condensed table-striped table-bordered">
					<tbody>
						<thead>
						<tr>
							<th colspan="5" class="center">Horses - Pegasus World Cup Turf</th>
						</tr>
							<tr class="sortar">
								<th class="first-th">PP</th>
								<th>Horse</th>
								<th>Jockey</th>
								<th>Fractional</th>
								<th>American</th>
							</tr>
						</thead>
						<tr>
							<td data-title="PP">
								<p class="none">0</p>1
							</td>
							<td data-title="Horse">Zulu Alpha</td>
							<td data-title="Jockey">Tyler Gaffalione</td>
							<td data-title="Fractional">16/1</td>
							<td data-title="American">+1600</td>
						</tr>
						<tr>
							<td data-title="PP">
								<p class="none">0</p>2
							</td>
							<td data-title="Horse">Arklow</td>
							<td data-title="Jockey">Luis Saez</td>
							<td data-title="Fractional">10/1</td>
							<td data-title="American">+1000</td>
						</tr>
						<tr>
							<td data-title="PP">
								<p class="none">0</p>3
							</td>
							<td data-title="Horse">Without Parole</td>
							<td data-title="Jockey">Frankie Dettori</td>
							<td data-title="Fractional">11/4</td>
							<td data-title="American">+275</td>
						</tr>
						<tr>
							<td data-title="PP">
								<p class="none">0</p>4
							</td>
							<td data-title="Horse">Sadler’s Joy</td>
							<td data-title="Jockey">Javier Castellano</td>
							<td data-title="Fractional">12/1</td>
							<td data-title="American">+1200</td>
						</tr>
						<tr>
							<td data-title="PP">
								<p class="none">0</p>5
							</td>
							<td data-title="Horse">Channel Cat</td>
							<td data-title="Jockey">John Velazquez</td>
							<td data-title="Fractional">16/1</td>
							<td data-title="American">+1600</td>
						</tr>
						<tr>
							<td data-title="PP">
								<p class="none">0</p>6
							</td>
							<td data-title="Horse">Instilled Regard</td>
							<td data-title="Jockey">Irad Ortiz Jr</td>
							<td data-title="Fractional">10/1</td>
							<td data-title="American">+1000</td>
						</tr>
						<tr>
							<td data-title="PP">
								<p class="none">0</p>7
							</td>
							<td data-title="Horse">Admission Office</td>
							<td data-title="Jockey">Jose L. Ortiz</td>
							<td data-title="Fractional">40/1</td>
							<td data-title="American">+4000</td>
						</tr>
						<tr>
							<td data-title="PP">
								<p class="none">0</p>8
							</td>
							<td data-title="Horse">Henley’s Joy</td>
							<td data-title="Jockey">Julien Leparoux</td>
							<td data-title="Fractional">25/1</td>
							<td data-title="American">+2500</td>
						</tr>
						<tr>
							<td data-title="PP">
								<p class="none">0</p>9
							</td>
							<td data-title="Horse">Next Shares</td>
							<td data-title="Jockey">Jose Valdivia, Jr.</td>
							<td data-title="Fractional">25/1</td>
							<td data-title="American">+2500</td>
						</tr>
						<tr>
							<td data-title="PP">10</td>
							<td data-title="Horse">Mo Forza</td>
							<td data-title="Jockey">Joel Rosario</td>
							<td data-title="Fractional">5/1</td>
							<td data-title="American">+500</td>
						</tr>
						<tr>
							<td data-title="PP">11</td>
							<td data-title="Horse">Sacred Life</td>
							<td data-title="Jockey">Jose Ortiz</td>
							<td data-title="Fractional">10/1</td>
							<td data-title="American">+1000</td>
						</tr>
						<tr>
							<td data-title="PP">12</td>
							<td data-title="Horse">Magic Wand</td>
							<td data-title="Jockey">Ryan Moore</td>
							<td data-title="Fractional">13/4</td>
							<td data-title="American">+325</td>
						</tr>

					</tbody>
				</table>
			</tr>
		</tbody>
	</table>
</div>