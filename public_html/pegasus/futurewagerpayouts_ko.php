<div class="table-responsive">
<table  border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered">
  <tbody>
  <tr>
    <td>Year</td>
    <td>Wagering Payout</td>
    <td>Pool 1</td>
    <td>Pool2</td>
    <td>Pool 3</td>
    <td>Payout</td>
  </tr>
  <tr>
    <td>2012</td>
    <td>Believe You Can</td>
    <td>$74.80</td>
    <td>No Pool</td>
    <td>No Pool</td>
    <td>$29.60</td>
  </tr>
  <tr>
    <td>2011</td>
    <td>Plum Pretty</td>
    <td>$21.80 (f)</td>
    <td>No Pool</td>
    <td>No Pool</td>
    <td>-</td>
  </tr>
  <tr>
    <td>2010</td>
    <td>Blind Luck</td>
    <td>$10.00</td>
    <td>No Pool</td>
    <td>No Pool</td>
    <td>$4.60</td>
  </tr>
  <tr>
    <td>2009</td>
    <td>Rachel Alexandra</td>
    <td>$8.40</td>
    <td>No Pool</td>
    <td>No Pool</td>
    <td>$2.60*</td>
  </tr>
  <tr>
    <td>2008</td>
    <td>Proud Spell</td>
    <td>$15.60</td>
    <td>$14.20</td>
    <td>$15.60</td>
    <td>$8.80*</td>
  </tr>
  <tr>
    <td>2007</td>
    <td>Rags to Riches</td>
    <td>$9.60</td>
    <td>$10.00*</td>
    <td>$5.00*</td>
    <td>$5.00*</td>
  </tr>
  <tr>
    <td>2006</td>
    <td>Lemons Forever</td>
    <td>$9.00 (f)*</td>
    <td>$11.00 (f)*</td>
    <td>$26.00 (f)</td>
    <td>$96.20</td>
  </tr>
  <tr>
    <td>2005</td>
    <td>Summerly</td>
    <td>$31.80</td>
    <td>$15.00</td>
    <td>$28.40</td>
    <td>$11.20</td>
  </tr>
  <tr>
    <td>2004</td>
    <td>Ashado</td>
    <td>$48.40***</td>
    <td>$21.00</td>
    <td>$17.80</td>
    <td>$6.60*</td>
  </tr>
  <tr>
    <td>2003</td>
    <td>Bird Town</td>
    <td>$33.60</td>
    <td>No Pool</td>
    <td>No Pool</td>
    <td>$38.40</td>
  </tr>
  </tbody>
</table>
</div>