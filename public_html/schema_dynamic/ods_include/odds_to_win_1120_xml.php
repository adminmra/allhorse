    <div class="table-responsive">
        <table  border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Pegasus World Cup">
            <caption>Horses - Pegasus World Cup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id='updateemp'>Updated November 19, 2018.</em>
                        <!-- br> All odds are fixed odds prices. -->
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Horses - Pegasus World Cup  - Jan 26                    </th>
            </tr>
            -->
    <tr><th>Team</th><th>Fractional</th><th>American</th></tr><tr><td>Accelerate</td><td>9/4</td><td>+225</td></tr><tr><td>Mendelssohn</td><td>7/1</td><td>+700</td></tr><tr><td>Monomoy Girl</td><td>6/1</td><td>+600</td></tr><tr><td>Audible</td><td>6/1</td><td>+600</td></tr><tr><td>City Of Light</td><td>8/1</td><td>+800</td></tr><tr><td>Gunnevera</td><td>9/1</td><td>+900</td></tr><tr><td>Catholic Boy</td><td>10/1</td><td>+1000</td></tr><tr><td>Diversify</td><td>10/1</td><td>+1000</td></tr><tr><td>Mckinzie</td><td>10/1</td><td>+1000</td></tr><tr><td>Yoshida</td><td>12/1</td><td>+1200</td></tr><tr><td>Mind Your Biscuits</td><td>12/1</td><td>+1200</td></tr><tr><td>Seeking The Soul</td><td>14/1</td><td>+1400</td></tr><tr><td>Catalina Cruiser</td><td>16/1</td><td>+1600</td></tr><tr><td>Battle Of Midway</td><td>25/1</td><td>+2500</td></tr><tr><td>Promises Fulfilled</td><td>25/1</td><td>+2500</td></tr><tr><td>Hofburg</td><td>33/1</td><td>+3300</td></tr><tr><td>Pavel</td><td>40/1</td><td>+4000</td></tr><tr><td>Bolt Doro</td><td>33/1</td><td>+3300</td></tr><tr><td>Gronkowski</td><td>40/1</td><td>+4000</td></tr>            </tbody>
        </table>
    </div>
    {literal}
        <style>
            table.ordenable th{cursor: pointer}
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sort_table.js"></script>
    {/literal}
    
