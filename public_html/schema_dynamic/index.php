<form action="" method="get" target=""> 
  url: <input type="text" name="urlget" size="100">
  <p>NOTE: When submit field is empty the default page is index or use another valid url. for example (https://www.usracing.com/pegasus-world-cup/odds)</p>
  <input type="submit" value="Submit">
</form>

<?php
$urlget = 'https://www.usracing.com';
if (isset($_POST["urlget"])) {
  $urlget = $_POST["urlget"];
}

$page_content = file_get_contents($urlget);

//echo $page_content ; 
//echo "<br>";

$dom_obj = new DOMDocument();
$dom_obj->loadHTML($page_content);
$meta_val_image = "/img/usracing.png"; 
$meta_val_url = "https://www.usracing.com";
$meta_val_title = "US Racing | Online Horse Betting";
$meta_val_description = "US Racing provides online horse betting. Bet on horses, sports and casino games.  Over 200 racetracks and rebates paid daily. Get a 10% Cash Bonus added to your first deposit.";
$meta_val_keywords = "online horse betting, horse racing, bet on derby, otb, offtrack, kentucky derby betting, preakness stakes, belmont stakes, horse betting, odds";

foreach($dom_obj->getElementsByTagName('meta') as $meta) {

  if($meta->getAttribute('property')=='og:image'){ 
      $meta_val_image = $meta->getAttribute('content');
  }
  if($meta->getAttribute('property')=='og:url'){ 
      $meta_val_url = $meta->getAttribute('content');
  }
  if($meta->getAttribute('property')=='og:title'){ 
      $meta_val_title = $meta->getAttribute('content');
  }
  if($meta->getAttribute('property')=='og:description'){ 
      $meta_val_description = $meta->getAttribute('content');
  }
  if($meta->getAttribute('name')=='keywords'){ 
      $meta_val_keywords = $meta->getAttribute('content');
  }
}

//from xml
$xml=simplexml_load_file("https://allhorse.com/schema_dynamic/autocontrol/libcontrolpage.xml") or die("Error: Cannot create object");
	$starttime = $xml->libfiles[0]->starttime ;

	$endtime = $xml->libfiles[0]->endtime ;

//end from xml

  //from xml places
$xml=simplexml_load_file("https://allhorse.com/schema_dynamic/autocontrol_places/libcontrolpage.xml") or die("Error: Cannot create object");
  $streetAddress = $xml->libfiles[0]->streetAddress ;
  $addressLocality = $xml->libfiles[0]->addressLocality ;
  $addressRegion = $xml->libfiles[0]->addressRegion ;
  $postalCode = $xml->libfiles[0]->postalCode ;
  $addressCountry = $xml->libfiles[0]->addressCountry ;


////////////////////////end from xml places
    

//////////////////results from metatags

//exclude httpp path
$meta_https = explode('http', $meta_val_image);

if(!$meta_https[0]){
  $image = $meta_val_image;
}else{
  $image = "https://www.usracing.com".$meta_val_image;
}

$url = $meta_val_url;
$title = $meta_val_title;
$description = $meta_val_description;
$keywords = $meta_val_keywords;
//////////////////end results from metatags 

$schema_format = '
<script type="application/ld+json">

{
  "@context": "http://schema.org",
  "@type": "Event",
  "name": "'.$title.'",
  "description": "'.$description.'",
  "image": "'.$image.'",
  "startDate": "'.$starttime.'", 
  "endDate": "'.$endtime.'",
  "performer": {
    "@type": "PerformingGroup",
    "name": "'.$title.'"
  },
  "location": {
    "@type": "Place",
    "name": "'.$title.'",
    "address": { 
      "@type": "PostalAddress",
      "streetAddress": "'.$streetAddress.'",
      "addressLocality": "'.$addressLocality.'",
      "addressRegion": "'.$addressRegion.'",
      "postalCode": "'.$postalCode.'",
      "addressCountry": "'.$addressCountry.'"
    }
  }
}

</script>
' ;
?>
<h4>Event schema</h4>
<?php
$showhtml = htmlentities($schema_format);


echo '<textarea cols="100%" rows="25">'.$showhtml.'</textarea>';


$url = 'ods_include/odds_to_win_1120_xml.php';
$content = file_get_contents($url);
$first_step = explode( '<tbody>' , $content );
$second_step = explode("</tbody>" , $first_step[1] );
//echo "<table>";
//echo $second_step[0]; 
//echo "</table>";

  //$htmlContent = file_get_contents("odds_to_win_1120_xml.php");
  $htmlContent = $second_step[0]; 
  $DOM = new DOMDocument();
  $DOM->loadHTML($htmlContent);
  
  $Header = $DOM->getElementsByTagName('th');
  $Detail = $DOM->getElementsByTagName('td');

    //#Get header name of the table
  foreach($Header as $NodeHeader) 
  {
    $aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
  }
  //print_r($aDataTableHeaderHTML); die();

  //#Get row data/detail table without header name as key
  $i = 0;
  $j = 0;
  foreach($Detail as $sNodeDetail) 
  {
    $aDataTableDetailHTML[$j][] = trim($sNodeDetail->textContent);
    $i = $i + 1;
    $j = $i % count($aDataTableHeaderHTML) == 0 ? $j + 1 : $j;
  }
  //print_r($aDataTableDetailHTML); die();
  
  //#Get row data/detail table with header name as key and outer array index as row number
  for($i = 0; $i < count($aDataTableDetailHTML); $i++)
  {
    for($j = 0; $j < count($aDataTableHeaderHTML); $j++)
    {
      $aTempData[$i][$aDataTableHeaderHTML[$j]] = $aDataTableDetailHTML[$i][$j];
    }
  }
  $aDataTableDetailHTML = $aTempData; unset($aTempData);

  $tableschema .= '<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Table",
  "about": "Odds Pegasus"
}
</script>
<div itemscope itemtype="http://schema.org/Table">
  <h2 itemprop="about">Odds Pegasus</h2>
  <table>
    <tr>
    <th>Team</th><th>Fractional</th></tr> 
    ';
foreach ($aDataTableDetailHTML as $row)
{
    $tableschema .= "<tr> 
    ";
    $tableschema .= "<td>" . $row['Team'] . "</td> ";   
    $tableschema .= "<td>" . $row['Fractional'] . "</td> ";
    $tableschema .= "
    </tr>
    ";
}
$tableschema .= '  </table>
</div>'; 
?>
<h4>Table schema pegasus</h4>
<?php
$showhtml2 = htmlentities($tableschema);   


echo '<textarea cols="100%" rows="25">'.$showhtml2.'</textarea>';
 
?>
<ul>
  <li>
    url to dates : <a href="https://allhorse.com/schema_dynamic/autocontrol/adminpage.php">Autocontrol by Arnab in schema dev</a>
  </li>
  <li>
    url to places : <a href="https://allhorse.com/schema_dynamic/autocontrol_places/adminpage.php">Autocontrol de places Pegasus</a>
  </li>
</ul>

