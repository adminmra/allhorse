<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr"><head>
<title>2019 Pegasus World Cup Odds  | Pegasus Betting</title>
<link rel="image_src" href="/themes/images/thumb.jpg" title="US Racing" />
<link type="text/css" rel="stylesheet" media="print" href="/themes/yaml/core/slim_print_base.css?1" />

<meta name="description" content="Now Live! Official 2019 Pegasus World Cup Odds available for betting.  The most racing bets anywhere: bet on jockeys, trainers and contenders." />
<meta name="keywords" content="pegasus world cup, pegasus odds, gulfstream park, santa anita, world&#39;s richest race" />
<meta property="og:title" content="Pegasus World Cup" />
<meta property="og:image" content="/img/pegasus-world-cup/2017-pegasus-world-cup.jpg" />
<meta property="og:description" content="Official 2019 Pegasus World Cup Odds available for betting.  The most racing bets anywhere: bet on jockeys, trainers and contenders." />



  

<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/style.css?v1" />
<link rel="stylesheet" type="text/css" href="/assets/css/custom_me.css" />

<link rel="stylesheet" type="text/css" href="/assets/css/app.css" />

<link rel="stylesheet" type="text/css" href="/assets/css/index-ps.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/index-bs.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/index-becca.css" />


<link rel="stylesheet" type="text/css" href="/assets/css/plugins.css?v=2" />
<!--[if IE 9]>
     <link rel="stylesheet" type="text/css" href="/assets/css/ie9.css" />
<![endif]-->

<script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script type="text/javascript" async="" src="https://ssl.luckyorange.com/w.js"></script><script src="//bat.bing.com/bat.js" async=""></script><script src="https://connect.facebook.net/signals/config/1710529292584739?v=2.8.33&amp;r=stable" async=""></script><script src="https://connect.facebook.net/signals/plugins/identity.js?v=2.8.33" async=""></script><script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script><script async="" type="text/javascript" src="/news/wp-content/themes/usracing/js/put5qvj.js"></script>
<script async="" type="text/javascript">try{Typekit.load();}catch(e){}</script>
<link href="//fonts.googleapis.com/css?family=Lato:400,300,700,900" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet" type="text/css" />


<!-- hide nav script -->

<!-- <script>
var senseSpeed = 35;
var previousScroll = 0;
$(window).scroll(function(event){
   var scroller = $(this).scrollTop();
   if (scroller-senseSpeed > previousScroll){
      $("#nav.is_stuck").filter(':not(:animated)').slideUp();
   } else if (scroller+senseSpeed < previousScroll) {
      $("#nav.is_stuck").filter(':not(:animated)').slideDown();
   }
   previousScroll = scroller;
});
</script>-->






<!--<script type="text/javascript" src="/assets/plugins/jquery-1.10.2.min.js"></script>-->
<!--[if lt IE 9]>
	 <link rel="stylesheet" type="text/css" href="/assets/plugins/iealert/alert.css" />
     <script type='text/javascript' src="/assets/js/html5.js"></script>
     <script type="text/javascript" src="/assets/plugins/iealert/iealert.min.js"></script>
     <script type="text/javascript">$(document).ready(function() { $("body").iealert({  closeBtn: true,  overlayClose: true }); });</script>
<![endif]-->

<script type="application/ld+json">
    {  "@context" : "http://schema.org", "@type" : "WebSite", "name" : "US Racing", "url" : "https://www.usracing.com"
    }
</script>

 


   <script src="//code.jquery.com/jquery-1.12.0.min.js"></script> 
  <!-- <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->

  <!-- <script src="/assets/js/jquery-1.12.0.custom.js"></script> -->
  <!-- <script src="/assets/js/jquery-1.12.0.custom.min.js"></script> -->
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

 
<script src="/assets/js/index-kd-tinysort.js"></script>





	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-126793368-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-126793368-1');
</script>


<style>





@media screen and (max-width:767px) and (min-width:0px) {
.sidr ul li ul li {
    background: #fff;
    line-height: 40px;
    border-bottom: 0px solid #fff;
}

  .bc-nemu-empty{
    border-bottom: 0px solid #fff;
  }
}

</style>




<link rel="apple-touch-icon-precomposed" href="icon/usr-logo-apple-touch-iphone.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="icon/usr-logo-apple-touch-ipad.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="icon/usr-logo-apple-touch-iphone4.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="icon/usr-logo-apple-touch-ipad-retina.png" />

<style type="text/css">.addthisevent {visibility:hidden;}.addthisevent-drop ._url,.addthisevent-drop ._start,.addthisevent-drop ._end,.addthisevent-drop ._zonecode,.addthisevent-drop ._summary,.addthisevent-drop ._description,.addthisevent-drop ._location,.addthisevent-drop ._organizer,.addthisevent-drop ._organizer_email,.addthisevent-drop ._attendees,.addthisevent-drop ._facebook_event,.addthisevent-drop ._all_day_event {display:none!important;}</style><style type="text/css">.addthisevent {visibility:hidden;}.addthisevent-drop ._url,.addthisevent-drop ._start,.addthisevent-drop ._end,.addthisevent-drop ._zonecode,.addthisevent-drop ._summary,.addthisevent-drop ._description,.addthisevent-drop ._location,.addthisevent-drop ._organizer,.addthisevent-drop ._organizer_email,.addthisevent-drop ._attendees,.addthisevent-drop ._facebook_event,.addthisevent-drop ._all_day_event {display:none!important;}</style><script charset="utf-8" src="https://assets.ubembed.com/universalscript/releases/v0.176.4/bundle.js"></script><style type="text/css">.ub-emb-iframe-wrapper{display:none;position:relative;vertical-align:middle}.ub-emb-iframe-wrapper.ub-emb-visible{display:inline-block}.ub-emb-iframe-wrapper .ub-emb-close{background-color:hsla(0,0%,100%,.6);border:0;border-radius:50%;color:#525151;cursor:pointer;font-family:Arial,sans-serif;font-size:20px;font-weight:400;height:20px;line-height:1;outline:none;padding:0;position:absolute;right:10px;text-align:center;top:10px;transition:transform .2s ease-in-out,background-color .2s ease-in-out;width:20px;z-index:1}.ub-emb-iframe-wrapper.ub-emb-mobile .ub-emb-close{transition:none}.ub-emb-iframe-wrapper .ub-emb-close:before{content:"";height:40px;position:absolute;right:-10px;top:-10px;width:40px}.ub-emb-iframe-wrapper .ub-emb-close:hover{-ms-transform:scale(1.2);background-color:#fff;transform:scale(1.2)}.ub-emb-iframe-wrapper .ub-emb-close:active,.ub-emb-iframe-wrapper .ub-emb-close:focus{background:hsla(0,0%,100%,.35);color:#444;outline:none}.ub-emb-iframe-wrapper .ub-emb-iframe{border:0;max-height:100%;max-width:100%}.ub-emb-overlay .ub-emb-iframe-wrapper .ub-emb-iframe{box-shadow:0 0 12px rgba(0,0,0,.3),0 1px 5px rgba(0,0,0,.2)}.ub-emb-overlay .ub-emb-iframe-wrapper.ub-emb-mobile{max-width:100vw}</style><style type="text/css">.ub-emb-overlay{transition:visibility .3s step-end;visibility:hidden}.ub-emb-overlay.ub-emb-visible{transition:none;visibility:visible}.ub-emb-overlay .ub-emb-backdrop,.ub-emb-overlay .ub-emb-scroll-wrapper{opacity:0;position:fixed;transition:opacity .3s ease,z-index .3s step-end;z-index:-1}.ub-emb-overlay .ub-emb-backdrop{background:rgba(0,0,0,.6);bottom:-1000px;left:-1000px;right:-1000px;top:-1000px}.ub-emb-overlay .ub-emb-scroll-wrapper{-webkit-overflow-scrolling:touch;bottom:0;box-sizing:border-box;left:0;overflow:auto;padding:30px;right:0;text-align:center;top:0;white-space:nowrap;width:100%}.ub-emb-overlay .ub-emb-scroll-wrapper:before{content:"";display:inline-block;height:100%;vertical-align:middle}.ub-emb-overlay.ub-emb-mobile .ub-emb-scroll-wrapper{padding:30px 0}.ub-emb-overlay.ub-emb-visible .ub-emb-backdrop,.ub-emb-overlay.ub-emb-visible .ub-emb-scroll-wrapper{opacity:1;transition:opacity .4s ease;z-index:2147483647}</style><style type="text/css">.ub-emb-bar{transition:visibility .2s step-end;visibility:hidden}.ub-emb-bar.ub-emb-visible{transition:none;visibility:visible}.ub-emb-bar .ub-emb-bar-frame{left:0;position:fixed;right:0;text-align:center;transition:bottom .2s ease-in-out,top .2s ease-in-out,z-index .2s step-end;z-index:-1}.ub-emb-bar.ub-emb-ios .ub-emb-bar-frame{right:auto;width:100%}.ub-emb-bar.ub-emb-visible .ub-emb-bar-frame{transition:bottom .3s ease-in-out,top .3s ease-in-out;z-index:2147483646}.ub-emb-bar .ub-emb-close{bottom:0;margin:auto 0;top:0}.ub-emb-bar:not(.ub-emb-mobile) .ub-emb-close{right:20px}</style><style type="text/css">.addthisevent {visibility:hidden;}.addthisevent-drop ._url,.addthisevent-drop ._start,.addthisevent-drop ._end,.addthisevent-drop ._zonecode,.addthisevent-drop ._summary,.addthisevent-drop ._description,.addthisevent-drop ._location,.addthisevent-drop ._organizer,.addthisevent-drop ._organizer_email,.addthisevent-drop ._attendees,.addthisevent-drop ._facebook_event,.addthisevent-drop ._all_day_event {display:none!important;}</style><style type="text/css">.addthisevent {visibility:hidden;}.addthisevent-drop ._url,.addthisevent-drop ._start,.addthisevent-drop ._end,.addthisevent-drop ._zonecode,.addthisevent-drop ._summary,.addthisevent-drop ._description,.addthisevent-drop ._location,.addthisevent-drop ._organizer,.addthisevent-drop ._organizer_email,.addthisevent-drop ._attendees,.addthisevent-drop ._facebook_event,.addthisevent-drop ._all_day_event {display:none!important;}</style><style type="text/css">.addthisevent {visibility:hidden;}.addthisevent-drop ._url,.addthisevent-drop ._start,.addthisevent-drop ._end,.addthisevent-drop ._zonecode,.addthisevent-drop ._summary,.addthisevent-drop ._description,.addthisevent-drop ._location,.addthisevent-drop ._organizer,.addthisevent-drop ._organizer_email,.addthisevent-drop ._attendees,.addthisevent-drop ._facebook_event,.addthisevent-drop ._all_day_event {display:none!important;}</style></head>
<body class="not-front  page page-kentuckyderby-odds section-kentuckyderby-odds ">
<!-- ClickTale Top part -->
<script type="text/javascript">
var WRInitTime=(new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->


<!-- test 1 -->
<!-- Top -->
<div id="top" class="top">
 <div class="container">
	 <div class="navbar-header pull-left">
		<!--
    <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
    <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
    <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>-->

    <a id="or" class="navbar-toggle collapsed"><span class="fa fa-bars" aria-hidden="true" style="font-size: 29px; margin:2px 0px 0px -10px; padding: 10px;"></span><i class="glyphicon glyphicon-remove"></i></a>
	
	
	</div>

  <a class="logo logo-lrg" href="/"><img id="logo-header" src="/img/usracing.png" alt="Online Horse Betting" /></a>
 <a class="logo logo-sm " href="/"><img src="/img/usracing-sm.png" alt="Online Hose Betting" /></a>
  <!---->
    <div class="login-header">
    

    <a class="btn btn-sm btn-red" id="signupBtn" href="/signup?ref=pegasus-world-cup-odds" rel="nofollow">
        <span>Bet Now    </span> <i class="glyphicon glyphicon-pencil"></i>
    </a>
</div>  

 </div>
</div><!--/top-->

<!-- Nav -->
<div id="nav">
	<a id="navigation" name="navigation"></a>
	<div class="navbar navbar-default" role="navigation">
	<div class="container">


	<!-- Toggle NAV 
	<div class="navbar-header">
		<!--
    <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
    <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
    <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>
    <a id="or" class="navbar-toggle collapsed"><span>EXPLORE</span><i class="glyphicon glyphicon-remove"></i></a>
	</div>-->

    <!-- Nav Itms -->
    <div class="collapse navbar-collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav nav-justified">
        <li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Today&#39;s Racing<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
      
   
    <li><a href="/odds">Today&#39;s Entries</a></li>
        <li><a href="/results">US Racing Results</a></li> 
     <li><a href="/horse-racing-schedule">Horse Racing Schedule</a></li>
      <li><a href="/harness-racing-schedule">Harness Racing Schedule</a></li> 
      <li><a href="/graded-stakes-races"> Stakes Races Schedule</a></li>
          
       
     
    
                    <li><a href="/racetracks">Racetracks</a></li>
            
     <li><a href="/leaders/horses">Top Horses</a></li>
      <li><a href="/leaders/jockeys">Top Jockeys</a></li>
       <li><a href="/leaders/trainers">Top Trainers</a></li>
             
       
      
     
     
                  </ul>
</li>


<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"> Odds<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu" id="double">
   
    <li><a href="/odds">Today&#39;s Horse Racing Odds</a></li>
<li><a href="/kentucky-derby/odds">2019 Kentucky Derby Odds</a></li> 

      
      
  
      
  <li><a href="/bet-on/triple-crown">Triple Crown Odds</a></li>
<li><a href="/pegasus-world-cup/odds">Pegasus World Cup Odds</a></li>
<li><a href="/grand-national">Grand National Odds</a></li>
			
            						
 <li><a href="/dubai-world-cup">Dubai World Cup Odds</a></li>
<li class="bc-nemu-last"><a href="/odds/us-presidential-election">US Presidential Election Odds</a></li>

                                                                    
  </ul>
</li>
<!--Horse Racing News ================================================ -->   
<li class="dropdown">     
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"> News<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">

<li><a href="/news">Latest Racing News </a></li>




 
	


	    


<li><a href="/news/features">Featured Stories </a></li>
<li><a href="/news/analysis">Racing Analysis </a></li>
<li><a href="/news/handicapping-reports">Handicapping &amp; Picks </a></li>
<li><a href="/news/harness-racing">Harness Racing</a></li>
<li><a href="/news/recap">Race Recaps</a></li>
<li><a href="/news/horse-racing-cartoon">Day in the Life - Cartoons</a></li>





	    

      
      
      
    
      
      
      
      
     </ul>
</li>

     
     



<!-- How to Bet ================================================ -->   
     
     <li class="dropdown">     
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">How to Bet<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">

<li><a href="/how-to/bet-on-horses">How to Bet on Horses </a></li>
<li><a href="/news/horse-betting-101/">Handicapping Tips</a></li>
<!-- <li><a href="https://www.guaranteedtipsheet.com/index.asp?afid=usr" target="_blank" rel="nofollow">Guaranteed Picks</a></li>  -->   
<li><a href="/news/kentucky-derby-betting-cryptocurrency-increases-popularity">Betting with Bitcoin</a></li>     
    
     
      
     </ul>
</li>

     
     


<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true">Promos <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
     <li><a href="/rebates">8% Rebates</a></li>
     <li><a href="/promos/cash-bonus-10">10% Signup Bonus</a></li>
 <li><a href="/promos/cash-bonus-150">$150 Member Bonus</a></li>
     
           </ul>
</li>
<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true">Learn More <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
	  
    <li><a href="/about">About Us</a></li> 
    <li><a href="/awards">Award Winning News</a></li>
        <li><a href="/best-horse-racing-site">Best Horse Racing Sites</a></li>
     <li><a href="/mobile-horse-betting">Mobile Betting</a></li>
     <li><a href="/racetracks">Racetracks</a></li>
		 <li><a href="/rebates">Rebates</a></li>
		 <li><a href="/support">Contact Us</a></li>
                 
        
        
	 <li><a href="/getting-started">Getting Started</a></li>
    <li><a href="/signup/?m=Join-Today">Join a Racebook!</a></li>
          </ul>
</li>

    	</ul>
   </div><!-- /navbar-collapse -->
  </div>
 </div>
</div> <!--/#nav-->


<!-- test 2 -->
  
<div class="container-fluid">
	
		<a href="/signup?ref=pegasus-world-cup" rel="nofollow"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/pegasus-world-cup/pegasus-world-cup.jpg" alt="Bet on the Pegasus World Cup" /></a>
		
		<a href="/signup?ref=pegasus-world-cup" rel="nofollow"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/pegasus-world-cup/2017-pegasus-world-cup.jpg" alt="Bet on the Pegasus World Cup" />  <p></p> </a>
</div>      
<div id="main" class="container">

<div class="row">


<div id="left-col" class="col-md-9">
         
                                        
          
<div class="headline"><h1>Pegasus World Cup</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p><strong><i>bet  	★ usracing</i> is the best place you can bet on the Pegasus Word Cup</strong>. Only at <i>bet  	★ usracing</i>, you get great future odds with amazing payouts on the  <a href="/pegasus-world-cup/odds">Pegasus World Cup</a>, <a href="/kentucky-derby/odds">Kentucky Derby </a> and even the leading <a href="/kentucky-derby/jockey-betting">Derby jockeys</a> and <a href="/kentucky-derby/trainer-betting">trainers</a>. Bet Now. </p>
<p>
<script type="text/javascript" src="/assets/js/atemay.js"></script>
<script type="text/javascript">
addthisevent.settings({
	license   	: "ammgus1bqzg0il7ntm75",
	mouse		: false,
	css			: false,
	outlook		: {show:true, text:"Outlook <i class='fa fa-plus-square'></i>"},
	google		: {show:true, text:"Google <i class='fa fa-plus-square'></i>"},
	yahoo		: {show:true, text:"Yahoo <i class='fa fa-plus-square'></i>"},
	ical		: {show:true, text:"iCal <i class='fa fa-plus-square'></i>"},
	hotmail		: {show:true, text:"Hotmail <i class='fa fa-plus-square'></i>"},
	facebook	: {show:true, text:"Facebook <i class='fa fa-plus-square'></i>"}
});
</script>


</p><div class="row infoBlocks">
<div class="col-md-4">
<div class="section">
	<i class="fa fa-calendar"></i>
	<div class="info">
	 <p><strong>When is the Pegasus World Cup?</strong></p>    
	<p>The Pegasus World Cup is on  Saturday, January 26th</p>
	</div>
    <a href="#" title="" class="addthisevent-drop" rel="nofollow" id="atedrop1">Add to my calendar <i class="fa fa-plus"></i>
	<span class="_start" style="display: none;">01-26-2019 18:00:00</span>
	<span class="_end" style="display: none;">01-26-2019 20:00:00</span>
	<span class="_zonecode" style="display: none;">15</span> <!-- EST US -->
	<span class="_summary" style="display: none;">2019 Pegasus World Cup - Place My Bet at Betusracing.ag</span>
	<span class="_location" style="display: none;">Gulfstream Park</span>
	<span class="_organizer" style="display: none;">USRacing.com</span>
	<span class="_organizer_email" style="display: none;">comments@usracing.com</span>
	<span class="_all_day_event" style="display: none;">false</span>
	<span class="_date_format" style="display: none;">01/26/2019</span>
	<span id="atedrop1-drop" class="addthisevent_dropdown"><span class="ateoutlook" data-ref="1" onclick="addthisevent.cli(this,&#39;outlook&#39;,&#39;&amp;dstart=01-26-2019%2018%3A00%3A00&amp;dend=01-26-2019%2020%3A00%3A00&amp;dzone=15&amp;dsum=2019%20Pegasus%20World%20Cup%20-%20Place%20My%20Bet%20at%20Betusracing.ag&amp;dloca=Gulfstream%20Park&amp;dorga=USRacing.com&amp;dorgaem=comments%40usracing.com&amp;dallday=false&amp;dateformat=01%2F26%2F2019&amp;credits=false&#39;);">Outlook <i class="fa fa-plus-square"></i></span><span class="ategoogle" data-ref="1" onclick="addthisevent.cli(this,&#39;google&#39;,&#39;&amp;dstart=01-26-2019%2018%3A00%3A00&amp;dend=01-26-2019%2020%3A00%3A00&amp;dzone=15&amp;dsum=2019%20Pegasus%20World%20Cup%20-%20Place%20My%20Bet%20at%20Betusracing.ag&amp;dloca=Gulfstream%20Park&amp;dorga=USRacing.com&amp;dorgaem=comments%40usracing.com&amp;dallday=false&amp;dateformat=01%2F26%2F2019&amp;credits=false&#39;);">Google <i class="fa fa-plus-square"></i></span><span class="ateyahoo" data-ref="1" onclick="addthisevent.cli(this,&#39;yahoo&#39;,&#39;&amp;dstart=01-26-2019%2018%3A00%3A00&amp;dend=01-26-2019%2020%3A00%3A00&amp;dzone=15&amp;dsum=2019%20Pegasus%20World%20Cup%20-%20Place%20My%20Bet%20at%20Betusracing.ag&amp;dloca=Gulfstream%20Park&amp;dorga=USRacing.com&amp;dorgaem=comments%40usracing.com&amp;dallday=false&amp;dateformat=01%2F26%2F2019&amp;credits=false&#39;);">Yahoo <i class="fa fa-plus-square"></i></span><span class="atehotmail" data-ref="1" onclick="addthisevent.cli(this,&#39;hotmail&#39;,&#39;&amp;dstart=01-26-2019%2018%3A00%3A00&amp;dend=01-26-2019%2020%3A00%3A00&amp;dzone=15&amp;dsum=2019%20Pegasus%20World%20Cup%20-%20Place%20My%20Bet%20at%20Betusracing.ag&amp;dloca=Gulfstream%20Park&amp;dorga=USRacing.com&amp;dorgaem=comments%40usracing.com&amp;dallday=false&amp;dateformat=01%2F26%2F2019&amp;credits=false&#39;);">Hotmail <i class="fa fa-plus-square"></i></span><span class="ateical" data-ref="1" onclick="addthisevent.cli(this,&#39;ical&#39;,&#39;&amp;dstart=01-26-2019%2018%3A00%3A00&amp;dend=01-26-2019%2020%3A00%3A00&amp;dzone=15&amp;dsum=2019%20Pegasus%20World%20Cup%20-%20Place%20My%20Bet%20at%20Betusracing.ag&amp;dloca=Gulfstream%20Park&amp;dorga=USRacing.com&amp;dorgaem=comments%40usracing.com&amp;dallday=false&amp;dateformat=01%2F26%2F2019&amp;credits=false&#39;);">iCal <i class="fa fa-plus-square"></i></span></span></a>
</div>
</div>

<div class="col-md-4">
<div class="section">
	<i class="fa fa-map-marker"></i>
	<div class="info">

	<p><strong>Where can I bet on the Pegasus World Cup? </strong></p><p>At <i>bet  	★ usracing</i> <br /><a itemprop="url" href="/pegasus-world-cup/odds">Odds are Live!</a></p>
    </div>
    <p><a href="/signup?ref=pegasus-world-cup-www" class="btn btn-primary btn-sm btn-red" rel="nofollow">Bet Now!<i class="fa fa-chevron-right"></i></a>   </p>
</div>
</div>

<div class="col-md-4">
<div class="section">
	<i class="glyphicon glyphicon-eye-open"></i>
    <div class="info">
	<p><strong>What channel is the Pegasus World Cup on?</strong></p> 
	<p>Watch the Pegasus World Cup live on TV with <a href="http://www.nbc.com/" rel="nofollow" target="_blank">NBC</a> at 6:00 pm EST</p>
    </div>
</div>
</div>
</div>
   <p></p>



<h2> 2019  Pegasus World Cup Odds</h2>
<p>What are the Pegasus World Cup Odds for 2019?</p>

<p>    </p><div class="table-responsive">
        <table border="1" class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" summary="Horses - Pegasus World Cup">
            <caption>Horses - Pegasus World Cup</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="5">
                        <em id="updateemp">Updated November 19, 2018.</em>
                        <!-- br /> All odds are fixed odds prices. -->
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <!--
            <tr>
                    <th colspan="3" class="center">
                    Horses - Pegasus World Cup  - Jan 26                    </th>
            </tr>
            -->
    <tr><th>Team <i class="fa fa-sort" title="Sort by this column"></i></th><th>Fractional <i class="fa fa-sort" title="Sort by this column"></i></th><th>American <i class="fa fa-sort" title="Sort by this column"></i></th></tr><tr><td>Accelerate</td><td>9/4</td><td>+225</td></tr><tr><td>Audible</td><td>6/1</td><td>+600</td></tr><tr><td>Battle Of Midway</td><td>25/1</td><td>+2500</td></tr><tr><td>Bolt Doro</td><td>33/1</td><td>+3300</td></tr><tr><td>Catalina Cruiser</td><td>16/1</td><td>+1600</td></tr><tr><td>Catholic Boy</td><td>10/1</td><td>+1000</td></tr><tr><td>City Of Light</td><td>8/1</td><td>+800</td></tr><tr><td>Diversify</td><td>10/1</td><td>+1000</td></tr><tr><td>Gronkowski</td><td>40/1</td><td>+4000</td></tr><tr><td>Gunnevera</td><td>9/1</td><td>+900</td></tr><tr><td>Hofburg</td><td>33/1</td><td>+3300</td></tr><tr><td>Mckinzie</td><td>10/1</td><td>+1000</td></tr><tr><td>Mendelssohn</td><td>7/1</td><td>+700</td></tr><tr><td>Mind Your Biscuits</td><td>12/1</td><td>+1200</td></tr><tr><td>Monomoy Girl</td><td>6/1</td><td>+600</td></tr><tr><td>Pavel</td><td>40/1</td><td>+4000</td></tr><tr><td>Promises Fulfilled</td><td>25/1</td><td>+2500</td></tr><tr><td>Seeking The Soul</td><td>14/1</td><td>+1400</td></tr><tr><td>Yoshida</td><td>12/1</td><td>+1200</td></tr>            </tbody>
        </table>
    </div>
    
        <style>
            table.ordenable th{cursor: pointer}
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sort_table.js"></script>
    
    <p></p><p>Place your bets now and your odds are locked in. These odds are only available for a limited time!</p>
<p align="center"><a href="/signup?ref=pegasus-world-cup-odds" rel="nofollow" class="btn-xlrg ">Bet Now on the Pegasus World Cup</a></p>  <br />
<br />
<p>The Pegasus World Cup is the brainchild of Frank Stronach, the chairman of the Stronach Group.  With a $12 Million dollar purse, it is the richest race in horse racing-- before, those bragging rights belonged to the <a href="/dubai-world-cup">Dubai World Cup</a>.  The winner of the Pegasus World Cup wins $7 million, the rest is divided amongst the other finishers.  The race will take place on the dirt track at <a href="/gulfstream-park">Gulfstream Park</a> in Hallandale Beach, Florida.  How long is the Pegasus World Cup?  The Pegasus World Cup is 1 1/8 miles long.  There are a total of 12 entries for the race.  As an interesting twist, each of these entries were purchased for $1 million dollars a piece!  Those people who purchased the entries will share in the profits the race generates.</p>
<p>If you want to attend the race in person, make sure to bring a fat wallet or purse-- general admission is $100.  Yes. One hundred dollars. Great way to get the general public interested in the race, right? (Please note sarcasm).</p>
<p>Okay, why is it called the Pegasus World Cup?  If you have been to Gulfstream Park to bet on the races, you would have seen the massive 110-foot-tall sculpture that Frank Stronach said represents the eternal struggle between good (in this case, the winged horse of Greek mythology, Pegasus) and bad (evil dragon spitting fire).  So, what did this statute cost?  $30 million dollars. For. A. Statue.  Hmmmmm.  Okay.  According to Jerry Iannelli of the Miami New Times, a Gulfstream employee said, &quot;I think it&#39;s an absolute waste of money. This used to be the top place for racing, but now the money&#39;s being put into something ridiculous. This place used to be all about horseracing. Now it&#39;s about something else. It&#39;s a shame all that money&#39;s gone into something like that.&quot;   Perhaps the inaugural Pegasus World Cup will renew interest in horse racing.  From our sources, we hear that the Pegasus World Cup will be run at sunny <a href="/santa-anita">Santa Anita Park</a> in 2018.  They don&#39;t have a 100 ft dragon there.  Yet.</p>
<p>Pegasus by the Numbers (credit to the Miami Herald)
</p><ul>
<li><strong>What:</strong> Pegasus slaying a dragon</li>

<li><strong>Idea conceived:</strong> April 2011</li>

<li><strong>Dimensions:</strong> 110 feet tall, 115 feet wide, 200 feet long</li>

<li><strong>Materials:</strong> Pegasus, 330 tons of steel and 132 tons of bronze; dragon, 110 tons of steel and 132 tons of bronze</li>

<li><strong>Journey to Hallandale Beach:</strong> Hundreds of bronze pieces were packed in 26 shipping containers and sent by boat which took about six weeks. The steel was packed in 23 containers and also sent by boat.</li>

<li><strong>Nuts and bolts:</strong> 18,000 screws</li>

<li><strong>Workers: </strong>More than 200, including four translators</li>

<li><strong>Cost:</strong> About $30 million</li>

<li><strong>Construction:</strong> Took about eight months</li>
</ul>

 <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

<div class="calltoaction">



<h2 class="animated tada">Bet the<br />Pegasus World Cup!</h2>
<h3>Exclusive odds and more.</h3>

<span id="object" class="animated bounceInDown" style="visibility: visible;"><i class="animated floating fa fa-map-marker"></i></span>
  <form class="form-signin" id="s_form-signup" method="post" action="" role="form" novalidate="novalidate">
	<div class="form-group account-group"><input type="hidden" name="ref" value="pegasus-world-cup-odds" id="s_ref" /></div>
	<div class="form-group account-group"><input type="hidden" name="cta" value="" /></div>
	<div class="form-group account-group"><input type="hidden" name="cta_sub" value="" /></div>
	<div class="form-group account-group">	
	  <input class="form-control" placeholder="First Name" name="first_name" required="" type="text" id="s_firstname" />
      <small class="error" style="display: none;" id="s_fn_error">Please fill out this field</small>
      </div>
	  <div class="form-group account-group">	
	  <input class="form-control" placeholder="Last Name" name="last_name" required="" type="text" id="s_lastname" />
      <small class="error" style="display: none;" id="s_ln_error">Please fill out this field</small>
      </div>
	  <div class="form-group account-group">	
	  <input class="form-control" placeholder="Email address" required="" name="email" type="email" id="s_email" />
      <small class="error" style="display: none;" id="s_em_error">Please fill out this field</small>
      </div>
				
				
	  <div class="form-group login-group">	
	  <input name="step" value="front" type="hidden" />
	  <center>
       <button type="submit" class="btn btn-sm btn-red" id="signupBtn">   Sign Up       <i class="fa fa-angle-right animated fadeInLeft" style="visibility: visible;"></i></button>
      </center></div>
	</form>
</div>

<script type="text/javascript">
$( document ).ready(function() {
$(".calltoaction #object").delay(500).queue(function(next) {
  $(this).addClass("animated bounceInDown").css("visibility", "visible");
  next();
}).delay(600).queue(function(next) {
  $(".calltoaction h2").addClass("animated tada"); 
  next();
}).delay(600).queue(function(next) {
  $(".calltoaction .fa-angle-right").addClass("animated fadeInLeft").css("visibility", "visible");; 
  next();
});
});
</script>
 

 

</div><!-- end: #right-col --> 

 
</div><!-- end/row --><p></p><div class="disclaimer">
<div class="headline"><h2>Disclaimer</h2></div>
<p style="margin-bottom:0;">Pegasus World Cup is a registered trademark of The Stronach Group or its affiliates (collectively Stronach Group). Stronach Group does not sponsor or endorse, and is not associated or affiliated with US Racing or its products, services or promotions.  US Racing provides free information, odds, facts and commentary about the Preakness Stakes and horse racing and betting, in general. Third party marks may be referenced in a transformative, editorial, informational, nominative, critical, analytical or comparative context. US Racing may reference marks belonging to third parties pursuant to our right to engage in fair use, fair comment, statutory fair use or trade mark fair use doctrine.  As such, US Racing does not contribute to any dilution of any trade or service marks. US Racing provides this information in an effort to educate and grow the sport of thoroughbred racing in North America with an emphasis on attracting new fans of the sport.
</p>
</div> 
</div><!-- /#container --> 
<!-- === Footer ================================================== -->
<div class="footer">
<div class="container">

    <div class="row">

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Horse Betting</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
          <li><a href="/off-track-betting">Off Track Betting</a></li>
          <li><a href="/online-horse-wagering">Online Horse Wagering</a></li>
          <li><a href="/odds">Horse Racing Odds</a></li>
          <li><a href="/bet-on-horses">Bet on Horses</a></li>
         <!-- <li><a href="/advance-deposit-wagering">Advance Deposit Wagering</a></li> -->
          <li><a href="/harness-racing">Harness Racing</a></li>
          <li><a href="/hong-kong-racing">Hong Kong Racing</a></li>
          <li><a href="/pegasus-world-cup/odds">Pegasus World Cup</a></li>
          <li><a href="/texas">Texas Horse Betting</a></li>
                    <li><a href="/royal-ascot">Royal Ascot Betting</a></li>

        <li><a href="/dubai-world-cup">Dubai World Cup Betting</a></li> 


<!--<li><a href="/santa-anita" >Santa Anita Horse Racing</a></li>
<li><a href="/keeneland" >Keeneland Horse Racing</a>  </li>
<li><a href="/horse-racing-schedule">Horse Racing Schedule</a></li>
<li><a href="/delta-downs" >Delta Downs Horse Racing</a></li>
<li><a href="http://www.pickthewinner.com" >Pick the Winner</a>  </li>
<li><a href="http://www.breederscupbetting.com" >Breeders Cup Betting</a>  </li>-->
    
    <li><a href="/breeders-cup/betting">Breeders&#39; Cup Betting</a>
     </li><li><a href="/breeders-cup/odds">Breeders&#39; Cup Odds</a>
                 </li></ul>
      </div> <!--/col-md-3-->

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Kentucky Derby</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
	         <li><a href="/kentucky-derby/betting">Kentucky Derby Betting</a></li>
          <li><a href="/kentucky-derby/odds">Kentucky Derby Odds</a></li>
          <li><a href="/kentucky-derby/future-wager">Kentucky Derby Future Wager</a></li>
          <li><a href="/bet-on/kentucky-derby">Bet on Kentucky Derby</a></li>
          <li><a href="/road-to-the-roses">Road to the Roses</a></li>
          <li><a href="/kentucky-derby/contenders">Kentucky Derby Contenders</a></li>
          <li><a href="/kentucky-derby/winners">Kentucky Derby Winners</a></li>
          <li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li>
          <li><a href="/twin-spires">Twin Spires Betting</a></li>



        </ul>
      </div>
        <!--/col-md-3-->
	        <div class="col-md-3 margin-bottom-30">
	        <div class="headline">
	          	<h3>Triple Crown</h3>
	        </div>
		        <ul class="list-unstyled margin-bottom-20">
		        <!--	<li><a href="/kentucky-derby" >Kentucky Derby</a></li>-->
		        	<li><a href="/preakness-stakes">Preakness Stakes</a></li>
		        	<li><a href="/bet-on/preakness-stakes">Bet on Preakness Stakes </a></li>
		        	<li><a href="/preakness-stakes/betting">Preakness Stakes Betting</a></li>
		        	<li><a href="/preakness-stakes/odds">Preakness Stakes Odds</a></li>
					<li><a href="/preakness-stakes/results">Preakness Stakes Results</a></li>
		          	<li><a href="/preakness-stakes/winners">Preakness Stakes Winners</a></li>
		           <li><a href="/belmont-stakes">Belmont Stakes</a></li>
		           <li><a href="/bet-on/belmont-stakes">Bet on Belmont Stakes </a></li>
		        	<li><a href="/belmont-stakes/betting">Belmont Stakes Betting</a></li>
		        	<li><a href="/belmont-stakes/odds">Belmont Stakes Odds</a></li>
					<!--<li><a href="/belmont-stakes/results" >Belmont Stakes Results</a></li>
		          	<li><a href="/belmont-stakes/winners" >Belmont Stakes Winners</a></li> -->
		          	<li><a href="/bet-on/triple-crown">Triple Crown Betting</a></li>
		        </ul>
			</div>
      <!--/col-md-3-->



      <!--/col-md-3-->
				   <!--<div class="col-md-3 margin-bottom-30">
				        <div class="headline">
				          <h3>US Horse Racing</h3>
				        </div>
				        <ul class="list-unstyled margin-bottom-20">
				        	<li><a href="/about" >About Us</a></li>-->

				          <!--	<li><a href="/cash-bonus" >CASH BONUSES</a></li>
				       <li><a href="#" >Our Guarantee</a></li>
				          	<li><a href="/faqs" >FAQs</a></li>
				            <li><a href="/how-to/bet-on-horses" >How to Bet at US Racing</a></li>-->

				<!--<li><a href="/signup/" >Sign Up Today</a></li>
				        </ul>
						</div> -->
      <!--/col-md-3-->

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Follow</h3>
        </div>

        <ul class="social-icons">
          <li><a href="https://www.facebook.com/usracingtoday" data-original-title="Facebook" title="Like us on Facebook" class="social_facebook"></a></li>
          <li><a href="https://twitter.com/betusracing" data-original-title="Twitter" title="Follow us on Twitter" class="social_twitter"></a></li>
          <li><a href="https://plus.google.com/+Usracingcom" data-original-title="Google+" title="Google+" class="social_google" rel="publisher"></a></li>
          <!--
          <li class="social_pintrest" title="Pin this Page"><a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.usracing.com&media=http%3A%2F%2Fwww.usracing.com%2Fimg%2Fusracing-pintrest.jpg&description=US%20Racing%20-%20America%27s%20Best%20in%20Off%20Track%20Betting%20(OTB).%0ASimply%20the%20easiest%20site%20for%20online%20horse%20racing."  data-pin-do="buttonBookmark" data-pin-config="none" data-original-title="Pintrest" ></a></li>
          -->
        </ul>
        <br />   <br />
        <div class="headline">
          <h3>US  Racing</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
        	<li><a href="/about">About Us</a></li>
          <!--	<li><a href="/cash-bonus" >CASH BONUSES</a></li>
       <li><a href="#" >Our Guarantee</a></li>
          	<li><a href="/faqs" >FAQs</a></li>
            <li><a href="/how-to/bet-on-horses" >How to Bet at US Racing</a></li>-->
<li><a href="/signup?ref=pegasus-world-cup-odds" rel="nofollow">Sign Up Today</a></li>
<li><a href="/usracing-reviews">USRacing.com Reviews</a></li>
<!--<li><a href="http://www.cafepress.com/betusracing" rel="nofollow" >Shop for US Racing Gear</a></li>-->

        </ul>



      </div> <!--/col-md-3-->

</div><!--/row-->



<div class="row friends">
	<center>Proudly featured on:
<img src="/img/as-seen-on.png" alt="US Racing As Seen On" class="img-responsive" style="width:95%; max-width:1400px;" /><center>


     <!--
   <div class="col-md-9">
        <a class="ntra"></a>
        <a target="_blank" class="bloodhorse"></a>
        <a class="equibase"></a>
        <a class="amazon"></a>
        <a class="credit" ></a>
        <a class="ga"></a>

        </div>
       <div class="col-md-3">
        <a href="/" class="us" title="US Online Horse Racing"></a>
-->
        </center></center></div>
</div>
<!-- /row/friends -->


</div><!--/container-->
<!--/footer-->

<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <span class="brand"><br /><p>US Racing is not a racebook or ADW, and does not accept or place wagers of any type. This website does not endorse or encourage illegal gambling. All information provided by this website is for entertainment purposes only. This website assumes no responsibility for the actions by and makes no representation or endorsement of any activities offered by any reviewed racebook or ADW. Please confirm the wagering regulations in your jurisdiction as they vary from state to state, province to province and country to country.  Any use of this information in violation of federal, state, provincial or local laws is strictly prohibited.</p>
        
        <p>Copyright 2018  <a href="/">US Racing</a>, All Rights Reserved.    | <a href="/privacy" rel="nofollow">Privacy Policy</a> |  <a href="/terms" rel="nofollow">Terms and Conditions</a> | <a href="/disclaimer" rel="nofollow">Disclaimer</a> | <a href="/responsible-gaming" rel="nofollow">Responsible Gambling</a> | <a href="/preakness-stakes/betting">Preakness Stakes Betting</a> | <a href="/belmont-stakes/betting">Belmont Stakes Betting</a> | <a href="/kentucky-derby/betting">Kentucky Derby Betting</a>    </p></span></div>
    </div><!--/row-->

  </div><!--/container-->
</div><!--/copyright-->

   

<div class="wp-hide">

<!--<script async type="text/javascript" src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>-->
<!--<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>-->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/plugins/sidr/jquery.sidr.min.js"></script>
<script src="/assets/js/scripts-footer-min.js"></script>
<!--<script type="text/javascript" src="/assets/plugins/hover-dropdown.min.js"></script>
<script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/assets/plugins/psScrollbar/psScrollw_mw.min.js"></script>
<script type="text/javascript" src="/assets/plugins/sidr/jquery.sidr.min.js"></script>
<script type="text/javascript" src="/assets/js/app.js"></script>
<script type="text/javascript" src="/assets/js/isIndexImported.js"></script>
 -->
<!-- Google Analytics -->
<!--<script type="text/javascript" src="/assets/js/google-analytics.js"></script> -->
<script async="" src="//www.google-analytics.com/analytics.js"></script>
<!-- End Google Analytics -->

<!-- Facebook Pixel Code -->
<!--<script type="text/javascript" src="/assets/js/facebook-pixel-code.js"></script> -->
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1710529292584739&ev=PageView&noscript=1" alt="Facebook"/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<!-- Bing Ads RP  -->
<!--<script type="text/javascript" src="/assets/js/bingAdsRp.js"></script> -->
<noscript><img alt="Bing" src="//bat.bing.com/action/0?ti=4047476&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

<!--<script type="text/javascript" src="/assets/js/luckyorange.js"></script> -->

<!--  Unbounce --->

<script src="//d6782401e72a400fba6ff09252aa6316.js.ubembed.com" async=""></script>
	
<!-- / Unbounce -->


<!--[if lt IE 9]>
      <script type="text/javascript" src="/assets/js/respond.js"></script>
<![endif]-->
<!-- Pin It - Pinterest
<script type="text/javascript" async src="/blog/wp-content/themes/usracing/js/pinit.js"></script>-->

<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-742771-29', 'auto', {'allowLinker': true});
  ga('require', 'displayfeatures');
  ga('require', 'linker');
  ga('linker:autoLink', ['betusracing.ag'] );
  ga('send', 'pageview');

</script>
-->

</div>

<div id="left-menu" class="sidr left"><div class="sidr-inner">undefined</div></div><div id="left-menu-main" class="sidr left"><div class="sidr-inner">undefined</div></div><div id="nav-side" class="sidr left"><div class="sidr-inner">
    <ul class="nav navbar-nav nav-justified">
        <li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Today&#39;s Racing<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
      
   
    <li><a href="/odds">Today&#39;s Entries</a></li>
        <li><a href="/results">US Racing Results</a></li> 
     <li><a href="/horse-racing-schedule">Horse Racing Schedule</a></li>
      <li><a href="/harness-racing-schedule">Harness Racing Schedule</a></li> 
      <li><a href="/graded-stakes-races"> Stakes Races Schedule</a></li>
          
       
     
    
                    <li><a href="/racetracks">Racetracks</a></li>
            
     <li><a href="/leaders/horses">Top Horses</a></li>
      <li><a href="/leaders/jockeys">Top Jockeys</a></li>
       <li><a href="/leaders/trainers">Top Trainers</a></li>
             
       
      
     
     
                  </ul>
</li>


<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"> Odds<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu" id="double">
   
    <li><a href="/odds">Today&#39;s Horse Racing Odds</a></li>
<li><a href="/kentucky-derby/odds">2019 Kentucky Derby Odds</a></li> 

      
      
  
      
  <li><a href="/bet-on/triple-crown">Triple Crown Odds</a></li>
<li><a href="/pegasus-world-cup/odds">Pegasus World Cup Odds</a></li>
<li><a href="/grand-national">Grand National Odds</a></li>
			
            						
 <li><a href="/dubai-world-cup">Dubai World Cup Odds</a></li>
<li class="bc-nemu-last"><a href="/odds/us-presidential-election">US Presidential Election Odds</a></li>

                                                                    
  </ul>
</li>
<!--Horse Racing News ================================================ -->   
<li class="dropdown">     
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"> News<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">

<li><a href="/news">Latest Racing News </a></li>




 
	


	    


<li><a href="/news/features">Featured Stories </a></li>
<li><a href="/news/analysis">Racing Analysis </a></li>
<li><a href="/news/handicapping-reports">Handicapping &amp; Picks </a></li>
<li><a href="/news/harness-racing">Harness Racing</a></li>
<li><a href="/news/recap">Race Recaps</a></li>
<li><a href="/news/horse-racing-cartoon">Day in the Life - Cartoons</a></li>





	    

      
      
      
    
      
      
      
      
     </ul>
</li>

     
     



<!-- How to Bet ================================================ -->   
     
     <li class="dropdown">     
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">How to Bet<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">

<li><a href="/how-to/bet-on-horses">How to Bet on Horses </a></li>
<li><a href="/news/horse-betting-101/">Handicapping Tips</a></li>
<!-- <li><a href="https://www.guaranteedtipsheet.com/index.asp?afid=usr" target="_blank" rel="nofollow">Guaranteed Picks</a></li>  -->   
<li><a href="/news/kentucky-derby-betting-cryptocurrency-increases-popularity">Betting with Bitcoin</a></li>     
    
     
      
     </ul>
</li>

     
     


<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true">Promos <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
     <li><a href="/rebates">8% Rebates</a></li>
     <li><a href="/promos/cash-bonus-10">10% Signup Bonus</a></li>
 <li><a href="/promos/cash-bonus-150">$150 Member Bonus</a></li>
     
           </ul>
</li>
<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true">Learn More <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
	  
    <li><a href="/about">About Us</a></li> 
    <li><a href="/awards">Award Winning News</a></li>
        <li><a href="/best-horse-racing-site">Best Horse Racing Sites</a></li>
     <li><a href="/mobile-horse-betting">Mobile Betting</a></li>
     <li><a href="/racetracks">Racetracks</a></li>
		 <li><a href="/rebates">Rebates</a></li>
		 <li><a href="/support">Contact Us</a></li>
                 
        
        
	 <li><a href="/getting-started">Getting Started</a></li>
    <li><a href="/signup/?m=Join-Today">Join a Racebook!</a></li>
          </ul>
</li>

    	</ul>
   </div></div><div id="topcontrol" title="Scroll Back to Top" style="position: fixed; bottom: 5px; right: 5px; opacity: 0; cursor: pointer;"><img src="/img/up.png" style="width:51px; height:42px" alt="up" /></div><div style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon0.39266586070880294"><img style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon0.653810755815357" width="0" height="0" alt="" src="https://bat.bing.com/action/0?ti=4047476&amp;Ver=2&amp;mid=1880a691-3f0b-468d-efee-3faca5ea5810&amp;pi=1001431019&amp;lg=en-US&amp;sw=1024&amp;sh=1024&amp;sc=24&amp;tl=2019%20Pegasus%20World%20Cup%20Odds%20%7C%20Pegasus%20Betting&amp;kw=pegasus%20world%20cup,%20pegasus%20odds,%20gulfstream%20park,%20santa%20anita,%20world&#39;s%20richest%20race&amp;p=https%3A%2F%2Fwww.usracing.com%2Fpegasus-world-cup%2Fodds&amp;r=&amp;lt=20&amp;evt=pageLoad&amp;msclkid=N&amp;rn=276972" /></div><div class="ub-emb-container"><div></div></div></body></html>
