<?php
$url = 'odds_to_win_1120_xml.php';
$content = file_get_contents($url);
$first_step = explode( '<tbody>' , $content );
$second_step = explode("</tbody>" , $first_step[1] );
//echo "<table>";
//echo $second_step[0]; 
//echo "</table>";

	//$htmlContent = file_get_contents("odds_to_win_1120_xml.php");
	$htmlContent = $second_step[0];	
	$DOM = new DOMDocument();
	$DOM->loadHTML($htmlContent);
	
	$Header = $DOM->getElementsByTagName('th');
	$Detail = $DOM->getElementsByTagName('td');

    //#Get header name of the table
	foreach($Header as $NodeHeader) 
	{
		$aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
	}
	//print_r($aDataTableHeaderHTML); die();

	//#Get row data/detail table without header name as key
	$i = 0;
	$j = 0;
	foreach($Detail as $sNodeDetail) 
	{
		$aDataTableDetailHTML[$j][] = trim($sNodeDetail->textContent);
		$i = $i + 1;
		$j = $i % count($aDataTableHeaderHTML) == 0 ? $j + 1 : $j;
	}
	//print_r($aDataTableDetailHTML); die();
	
	//#Get row data/detail table with header name as key and outer array index as row number
	for($i = 0; $i < count($aDataTableDetailHTML); $i++)
	{
		for($j = 0; $j < count($aDataTableHeaderHTML); $j++)
		{
			$aTempData[$i][$aDataTableHeaderHTML[$j]] = $aDataTableDetailHTML[$i][$j];
		}
	}
	$aDataTableDetailHTML = $aTempData; unset($aTempData);

	echo '<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Table",
  "about": "Odds Pegasus"
}
</script>
<div itemscope itemtype="http://schema.org/Table">
  <h2 itemprop="about">Odds Pegasus</h2>
	<table>
    <tr><th>Team</th><th>Fractional</th></tr>';
foreach ($aDataTableDetailHTML as $row)
{
    echo "<tr>";
    echo "<td>" . $row['Team'] . "</td>";
    echo "<td>" . $row['Fractional'] . "</td>";
    echo "</tr>";
}
echo '  </table>
</div>';
	//print_r($aDataTableDetailHTML); die();
?>