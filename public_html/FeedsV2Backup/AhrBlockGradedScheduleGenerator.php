
<?php

if ( !defined('APP_PATH') ) {
	define( 'APP_PATH' , '/home/ah/allhorse/public_html/FeedsV2/' );
}

require_once APP_PATH . 'Generator.php';

class AhrBlockGradedScheduleGenerator extends Generator{

	function __construct(){
		parent::__construct();
	}

	public function content_tpl(){

		$sql_top= "SELECT * FROM graded_schedule ORDER BY racedate ";
		$result_top=DB::query($sql_top);
		ob_start();
		?>
		<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="3" cellpadding="3" width="100%">
		<tbody>
		<tr>
		<th width="10%"><strong>Date</strong></th>
		<th width="12%"><strong>Stakes</strong></th>
		<th width="12%"><strong>Track</strong></th>
		<th width="10%"><strong>Grade</strong></th>
		<th width="10%"><strong>Purses</strong></th>
		<th width="10%"><strong>Age/Sex</strong></th>
		<th width="10%"><strong>DS</strong></th>
		<th width="12%"><strong>Horse</strong></th>
		<th width="12%"><strong>Jockey</strong></th>
		<th width="12%"><strong>Trainer</strong></th>
		</tr>
		<?php
		$counter = 0;
		while( $data_top=$result_top->fetch_object() ) {
			$updatedate = explode( "-" , $data_top->racedate );
			$updateas   = date( "M d", mktime( 0, 0, 0, $updatedate[1], $updatedate[2], $updatedate[0] ) );
			if( $counter%2 == 1 ) {
				echo '<tr class="odd">';
			}
			else {
				echo '<tr>';
			}
		?>
			<td class="num"><?php echo $updateas; ?></td>
			<td><?php	 	 echo stripslashes($data_top->racename); ?></td>
			<td><?php	 	 echo $data_top->track; ?></td>
			<td><?php	 	 echo $data_top->grade; ?></td>
			<td><?php	 	 echo $data_top->purse; ?></td>
			<td><?php	 	 echo $data_top->age; ?></td>
			<td><?php	 	 echo $data_top->ds; ?></td>
			<td><?php	 	 echo trim($data_top->horse); ?></td>
			<td><?php	 	 echo trim($data_top->jockey); ?></td>
			<td><?php	 	 echo trim($data_top->trainer); ?></td>
		</tr>
			<?php
			$counter++;
		}

		?>
		</tbody>
		</table>
		<?php
		return ob_get_clean();
	}

	public function graded_schedule_new(){
		ob_start();
		?>

		<div class="table-responsive">
			<?php
				$currenttime = date('Y-m-d');
				$sql_top     = "SELECT * FROM graded_schedule WHERE racedate>='$currenttime' ORDER BY racedate ";
				$result_top  = DB::query( $sql_top );
				echo $this->generateGradedStakeRacesTable( $result_top, false );
			?>
		</div>

		<?php
		return ob_get_clean();
	}

	public function graded_schedule_old(){
		ob_start();
		?>
		<div class="table-responsive">
			<?php
				$currenttime = date('Y-m-d');
				$sql_top     = "SELECT * FROM graded_schedule WHERE racedate<'$currenttime' ORDER BY racedate DESC";
				$result_top  = DB::query( $sql_top );
				echo $this->generateGradedStakeRacesTable( $result_top );
			?>
		</div>
		<?php

		return ob_get_clean();
	}



	public static function run() {
		try {

			$instance = new self;

			$html_block = $instance->content_tpl();
			//$instance->as_file_save_data( $html_block , 'ahr_block_graded-schedule.tpl' );
			$instance->as_file_save_data( $html_block , ALLHORSEPATH .  'public_html/generated/graded_stakes/graded_stakes_races_full.php', TRUE );

			$html_block = $instance->graded_schedule_new();
			//$instance->as_file_save_data( $html_block , 'ahr_block_graded-schedule-new.tpl' );
			$instance->as_file_save_data( $html_block , ALLHORSEPATH . 'public_html/generated/graded_stakes/graded_stakes_races_schema.php', TRUE );

			$html_block = $instance->graded_schedule_old();
			//$instance->as_file_save_data( $html_block , 'ahr_block_graded-schedule-old.tpl' );
			$instance->as_file_save_data( $html_block , ALLHORSEPATH . 'public_html/generated/graded_stakes/graded_stakes_races_ran_schema.php', TRUE );
		} catch ( Exception $e ) {
			print( $e->getMessage() );
		}
	}

	private function checkWordsNComplete($cad) {
		$cad_arr = explode(" ", $cad);
		$wc_words = count($cad_arr);
		$new_cad = "";
		for ($i = 0; $i < $wc_words; ++$i) {
			$cur = $cad_arr[$i];
			switch ($cur) {
				case "S": case "s":
					$cur = "Stakes";
					break;
				case "H": case "h":
					$cur = "Handicap";
					break;
			}
			$new_cad .= $cur;
			if($i+1< $wc_words)
				$new_cad .= " ";
		}
		return $new_cad;
	}

	private function getTrackName(&$track) {
		$track = stripslashes($track);
		$tracknameCleaned = strtolower($track);
		$tracknameCleaned = trim(preg_replace("/[^ \w]+/", "", $tracknameCleaned));
		$linkForTrackname = '';
		if ($tracknameCleaned !== '') {
			$tmp1 = str_replace(" ", "-", $tracknameCleaned);
			$tmp2 = SMARTYPATH . "/templates/content/racetrack/" . $tmp1 . ".tpl";
			$tmp3 = SMARTYPATH . "/templates/content/racetrack/" . $tmp1 . "-race-course.tpl";
			if (file_exists($tmp2))
				$linkForTrackname = SITEABSOLUTEURL . str_replace(" ", "-", $tmp1);
			else if (file_exists($tmp3))
				$linkForTrackname = SITEABSOLUTEURL . str_replace(" ", "-", $tmp1) . "-race-course";
		}
		return $linkForTrackname;
	}

	private function getRaceName(&$stake) {
		$stake = stripslashes($stake);
		$racenameCleaned = strtolower($stake);
		$racenameCleaned = trim(preg_replace("/[^ \w]+/", "", $racenameCleaned));
		$linkForRacename = '';
		if ($racenameCleaned !== '') {
			$tmp1 = str_replace(" ", "-", $racenameCleaned);
			$tmp2 = SMARTYPATH . "/templates/content/stake/" . $tmp1 . ".tpl";
			$tmp3 = SMARTYPATH . "/templates/content/stake/" . $tmp1 . "-stakes.tpl";
			if (file_exists($tmp2))
				$linkForRacename = SITEABSOLUTEURL . str_replace(" ", "-", $tmp1);
			else if (file_exists($tmp3))
				$linkForRacename = SITEABSOLUTEURL . str_replace(" ", "-", $tmp1) . "-stakes";
		}
		return $linkForRacename;
	}

	private function chckLink($link, $openTag, $closeTag, $content, $scheme="none") {
		//<a itemprop="location" itemscope itemtype="http://schema.org/Place" href="' . $linkForTrackname . '" itemprop="url">
		if ($link !== '') {
			$ret = '';
			switch($scheme){
				case "location":
					$ret = $openTag . '<a itemprop="location" itemscope itemtype="http://schema.org/Place" href="' . $link . '" itemprop="url">' . $content . '</a>' . $closeTag;
					break;
				case "name":
					$ret = $openTag . '<a itemprop="url" href="' . $link . '">' . $content . '</a>' . $closeTag;
					break;
				default:
					$ret = $openTag . '<a href="' . $link . '">' . $content . '</a>' . $closeTag;
			}
			return $ret;
		} else {
			return $openTag . $content . $closeTag;
		}
	}

	private function generateGradedStakeRacesTable($result_top, $horsecol=true){
		?>
		<table class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="0" width="100%">
			<tbody>
				<tr>
					<th><strong>Date</strong></th>
					<th><strong>Stakes</strong></th>
					<th><strong>Track</strong></th>
					<!-- <th width="10%"><strong>Grade</strong></th> -->
					<th><strong>Purses</strong></th>
					<!-- <th width="10%"><strong>Age/Sex</strong></th>
					<th width="10%"><strong>DS</strong></th> -->
					<?php
					if($horsecol){
						echo "<th><strong>Horse</strong></th>";
					}
					?>
					<!-- <th width="12%"><strong>Jockey</strong></th>
					<th width="12%"><strong>Trainer</strong></th> -->
				</tr>
				<?php
				$counter = 0;
				#$blacklist = array("arlington park", "calder", "churchill downs", "fairgrounds", "hoosier", "indiana downs", "miami valley", "oaklawn park", "the meadows", "pimlico", "gulfstream park", "golden gate fields", "santa anita", "meadowlands", "lone star park", "los alamitos", "fairplex", "barrets", "del mar", "ferndale", "fresno", "kentucky downs", "laurel park", "monmouth park", "pleasanton", "portland meadows", "sacramento", "santa rosa", "stockton", "tampa bay downs", "timonium", "los alamitos tbred");
				$blacklist = array();
				while ($data_top = $result_top->fetch_object() ) {
					if(in_array(strtolower(trim($data_top->track)), $blacklist)) continue;
					$updatedate = explode("-", $data_top->racedate);
					$updateas = date("M d", mktime(0, 0, 0, $updatedate[1], $updatedate[2], $updatedate[0]));
					if ($counter % 2 == 1) {
						echo '<tr itemscope="" itemtype="http://schema.org/Event" class="odd">';
					} else {
						echo '<tr itemscope="" itemtype="http://schema.org/Event">';
					}
					$linkForRacename  = $this->getRaceName($data_top->racename);
					$linkForTrackname = $this->getTrackName($data_top->track);
					$campoRaceName    = $this->chckLink($linkForRacename, '', '', $this->checkWordsNComplete($data_top->racename), 'name');
					$campoTrackName   = $this->chckLink($linkForTrackname, '', '', $data_top->track, 'location');
					?>
					<td><time itemprop="startDate" content="<?php echo date("Y-m-d", strtotime($updateas)); ?>"><?php echo $updateas; ?></time></td>
					<td itemprop="name"><?php echo $campoRaceName; ?></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><?php echo $campoTrackName; ?></span><!-- <?php echo $data_top->track; ?> --></td>
					<!-- <td><?php	 	 echo $data_top->grade; ?></td> -->
					<td itemprop="description"><?php	 	 echo $data_top->purse; ?></td>
					<!-- <td><?php	 	 echo $data_top->age; ?></td> -->
					<!-- 	<td><?php	 	 echo $data_top->ds; ?></td> -->
					<?php
					if($horsecol){
						echo "<td>". ucwords(strtolower(trim($data_top->horse))) . "</td>";
					}
					?>
					<!-- 	<td><?php	 	 echo trim($data_top->jockey); ?></td>
					<td><?php	 	 echo trim($data_top->trainer); ?></td> -->
				</tr>
				<?php
				$counter++;
			}

			?>
			</tbody>
		</table>
		<?php
	}
}

AhrBlockGradedScheduleGenerator::run();
?>