<?php	 		 	
ini_set('display_errors',1);
error_reporting(E_ALL);

if($_SERVER['DOCUMENT_ROOT']==''){
	$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(dirname(__FILE__)));
}

function MiniCloseTrack()
{

		$MSG = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/FeedData/FeedsConfigMsg.txt');
		$MsgContent=explode("=;",trim($MSG));
		//print_r($ConfigDBVar);
		//echo "here";
		if($MsgContent[0]=='dead')
		{
						//echo "<p>".stripslashes($ConfigDBVar["user_conf_msg"])."</p>";	
				$OpenTracksTable = '<li class="first leaf"><div class="racename">'.stripslashes($MsgContent[1]).'</div><li>';
						$ClosedTracksTable = '<li class="first leaf"><div class="racename">'.stripslashes($MsgContent[1]).'</div><li>';
		
		}
		else
		{
					include_once($_SERVER['DOCUMENT_ROOT'].'/Feeds/TrackResults/Tracks.php');
					$DataFeed = new DataFeed;
					$Success = $DataFeed->GetTracksStatic();
			
					$OpenTracksTable = '';
					$ClosedTracksTable = '';
					//echo $Success;
					if($Success)
					{
						if(empty($DataFeed->ClosedTracks)){
			
							$ClosedTracksTable = '
							<li class="first leaf"><div class="racename noclosed">No Closed Tracks. Please Check Back Later.</div><li>';
			
						}else{
			
								$ClosedTracks = $DataFeed->ClosedTracks;
								$i=0;
								foreach($ClosedTracks as $Track){
									if (strpos($Track["TRACK"], "Miami Jai-Alai") !== false){
										continue;
									}
										if($i==0){$class='first leaf';}else{$class='leaf';}
										$i++;
			
			
										// $Track["TRACK"] , $Track["RACE"] , $Track["MTP"]
			
										$ClosedTracksTable .= '<li class="'.$class.'"><a href="/todayshorseracing?id='.$Track["ID"].'&race='.$Track["RACE"].'&racename='.$Track["TRACK"].'" title="' . str_replace("'","", $Track["TRACK"]) . ' Results for Race '. $Track["RACE"] . '"><div class="racename">'.$Track["TRACK"].'</div></a></li>';
			
									}
								}
					}
					
		}
		
		return $ClosedTracksTable;

}
//echo MiniCloseTrack();

function MiniOpenTrack()
{

		//require_once $_SERVER['DOCUMENT_ROOT'].'/Feeds/TrackResults/FeedsConfigMsg.php';
		//global $ConfigDBVar;
		$MSG = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/FeedData/FeedsConfigMsg.txt');
		$MsgContent=explode("=;",trim($MSG));
		if($MsgContent[0]=='dead')
		{
						//echo "<p>".stripslashes($ConfigDBVar["user_conf_msg"])."</p>";	
				$OpenTracksTable = '<li class="first leaf"><div class="racename">'.stripslashes($MsgContent[1]).'</div><li>';
						$ClosedTracksTable = '<li class="first leaf"><div class="racename">'.stripslashes($MsgContent[1]).'</div><li>';
		
		}
		else
		{
					include_once($_SERVER['DOCUMENT_ROOT'].'/Feeds/TrackResults/Tracks.php');
					$DataFeed = new DataFeed;
					$Success = $DataFeed->GetTracks();
			
					$OpenTracksTable = '';
					$ClosedTracksTable = '';
					//echo $Success;
					if($Success)
					{
						if(empty($DataFeed->OpenTracks)){
			
							$OpenTracksTable = '
							<li class="first leaf"><div class="racename noopen">No Open Tracks. Check Back Later.</div><li>';
			
						}else{
			
								$OpenTracks = $DataFeed->OpenTracks;
								$i=0;
								foreach($OpenTracks as $Track){
									if (strpos($Track["TRACK"], "Miami Jai-Alai") !== false){
										continue;
									}
										if($i==0){$class='first leaf';}else{$class='leaf';}
										$i++;
			
			
										// $Track["TRACK"] , $Track["RACE"] , $Track["MTP"]
			
										$OpenTracksTable .= '<li class="'.$class.'"><a href="/todayshorseracing?id='.$Track["ID"].'&race='.$Track["RACE"].'&racename='.$Track["TRACK"].'" title="' . str_replace("'","", $Track["TRACK"]) . ' Results for Race '. $Track["RACE"] . '"><div class="racename">'.$Track["TRACK"].'</div><div class="raceinfo">Race #'.$Track["RACE"].' - <span style="color:#7f0000;">'.$Track["MTP"].' MTP</span></div></a></li>';
			
									}
								}
					}
					
		}
		
		return $OpenTracksTable;

}

function OpenTrackTable()
{
	

		$OpenTracksTable = ' <table width="100%" border="0" cellspacing="0" cellpadding="0">
										
												  <thead>
													<tr class="tracks-titlebar">
													  <th class="width50perc">TRACKS</th>
													  <th><span title="Minutes To Post">MTP</span></th>
													  <th>RACE</th>
													  <th class="width67"></th>
													</tr>
										
												  </thead>
												  <tbody>
													<tr class="tracks-spacer">
													  <td colspan="4" ></td>
													</tr>        
										';

		$ClosedTracksTable = '';
		/*$sql2 ="select * from outguard where id = '2'";
		$result2=mysql_query_w($sql2);
		$data2=mysql_fetch_object($result2);
		//echo $data2->status;*/
		//require_once $_SERVER['DOCUMENT_ROOT'].'/Feeds/TrackResults/FeedsConfigMsg.php';
		//global $ConfigDBVar;
		$fileConfigPath=$_SERVER['DOCUMENT_ROOT'].'/FeedData/FeedsConfigMsg.txt';
		$MSG = file_get_contents($fileConfigPath);
		$MsgContent=explode("=;",trim($MSG));
		if($MsgContent[0]=='dead')
		{
										
				$OpenTracksTable .= '<tr class="tracks-cell"><td class="width50perc" colspan="4">'.stripslashes($MsgContent[1]).'</td></tr>';
						$ClosedTracksTable .= '<tr class="tracks-cell"><td class="width50perc" colspan="4">'.stripslashes($MsgContent[1]).'</td></tr>';
		}
		else
		{
					include_once($_SERVER['DOCUMENT_ROOT'].'/Feeds/TrackResults/Tracks.php');
					$DataFeed = new DataFeed;
					$Success = $DataFeed->GetTracksStatic();
			
					
					$ClosedTracksTable = '';
					//echo $Success;
					if($Success)
					{
						if(empty($DataFeed->OpenTracks)){
			
							$OpenTracksTable .= '
							<tr class="tracks-cell"><td class="width50perc" colspan="4">No Open Tracks. Check Back Later.</td></tr>';
			
						}else{
			
								$OpenTracks = $DataFeed->OpenTracks;
								$i=0;
								foreach($OpenTracks as $Track){
									if (strpos($Track["TRACK"], "Miami Jai-Alai") !== false){
										continue;
									}
										//if($i==0){$class='first leaf';}else{$class='leaf';}
										
										if($i >= 8)
										{
											break;
										}
										
										$i++;
										
										$OpenTracksTable .='<tr class="tracks-cell">';
										$OpenTracksTable .='<td class="width50perc"><a href="/todayshorseracing?id='.$Track["ID"].'&race='.$Track["RACE"].'&racename='.$Track["TRACK"].'" title="' . str_replace("'","", $Track["TRACK"]) . '">'.$Track["TRACK"].'</a></td>
          <td>'.$Track["MTP"].'</td>

          <td>'.$Track["RACE"].'</td>
          <td class="width67"><a href="/racebook"><img src="/themes/images/betnow-default.gif" onmouseover="this.src=\'/themes/images/betnow-hover.gif\'" onmouseout="this.src=\'/themes/images/betnow-default.gif\';" alt="Bet Now" /></a></td>';
										// $Track["TRACK"] , $Track["RACE"] , $Track["MTP"]
			
										//$OpenTracksTable .= '<li class="'.$class.'"><a href="/todays-horseracing?id='.$Track["ID"].'&race='.$Track["RACE"].'&racename='.$Track["TRACK"].'" title="' . str_replace("'","", $Track["TRACK"]) . ' Results for Race '. $Track["RACE"] . '"><div class="racename">'.$Track["TRACK"].'</div><div class="raceinfo">Race #'.$Track["RACE"].'-'.$Track["MTP"].' MTP</div></a></li>';
											$OpenTracksTable .='</tr>';
									}//foreach
								}//else 
					}
					else
					{
						$sqlmsg= "select * from create_message where id='104'";
						$resultmsg=mysql_query_w($sqlmsg);
						$datamsg=mysql_fetch_object($resultmsg);
				
						$OpenTracksTable .= '<tr class="tracks-cell"><td class="width50perc" colspan="4">'.stripslashes($datamsg->user_conf_msg).'</td></tr>';
						$ClosedTracksTable .= '<tr class="tracks-cell"><td class="width50perc" colspan="4">'.stripslashes($datamsg->user_conf_msg).'</td></tr>';
					}//else Sucess
					
					
					
		}// else DB
		$OpenTracksTable .=' </tbody></table>';
		return $OpenTracksTable;
}

function CloseTrackTable()
{

		$OpenTracksTable = ' <table width="100%" border="0" cellspacing="0" cellpadding="0">
										
												  <thead>
													<tr class="tracks-titlebar">
													  <th class="width50perc">TRACKS</th>
													  <th><span title="Minutes To Post">MTP</span></th>
													  <th>RACE</th>
													  <th class="width67"></th>
													</tr>
										
												  </thead>
												  <tbody>
													<tr class="tracks-spacer">
													  <td colspan="4" ></td>
													</tr>        
										';
	$ClosedTracksTable ="";

		//require_once $_SERVER['DOCUMENT_ROOT'].'/Feeds/TrackResults/FeedsConfigMsg.php';
		//global $ConfigDBVar;
		$MSG = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/FeedData/FeedsConfigMsg.txt');
		$MsgContent=explode("=;",trim($MSG));
		if($MsgContent[0]=='dead')
		{
										
				$OpenTracksTable .= '<tr class="tracks-cell"><td class="width50perc" colspan="4">'.stripslashes($MsgContent[1]).'</td></tr>';
						$ClosedTracksTable .= '<tr class="tracks-cell"><td class="width50perc" colspan="4">'.stripslashes($MsgContent[1]).'</td></tr>';
		}
		else
		{
					include_once($_SERVER['DOCUMENT_ROOT'].'/Feeds/TrackResults/Tracks.php');
					$DataFeed = new DataFeed;
					$Success = $DataFeed->GetTracksStatic();
			
					
					$ClosedTracksTable = '';
					//echo $Success;
					if($Success)
					{
						if(empty($DataFeed->ClosedTracks)){
			
							$OpenTracksTable .= '
							<tr class="tracks-cell"><td class="width50perc" colspan="4">No Closed Tracks. Check Back Later.</td></tr>';
			
						}else{
			
								$OpenTracks = $DataFeed->ClosedTracks;
								$i=0;
								foreach($OpenTracks as $Track){
									if (strpos($Track["TRACK"], "Miami Jai-Alai") !== false){
										continue;
									}
										//if($i==0){$class='first leaf';}else{$class='leaf';}
										//$i++;
										if($i >=8)
										{
											break;
										}
										
										$i++;
										
										$OpenTracksTable .='<tr class="tracks-cell">';
										$OpenTracksTable .='<td class="width50perc"><a href="/todayshorseracing?id='.$Track["ID"].'&race='.$Track["RACE"].'&racename='.$Track["TRACK"].'" title="' . str_replace("'","", $Track["TRACK"]) . '">'.$Track["TRACK"].'</a></td>
          <td>'.$Track["MTP"].'</td>

          <td>'.$Track["RACE"].'</td>
          <td class="width67"><a href="/racebook"><img src="/themes/images/betnow-default.gif" onmouseover="this.src=\'/themes/images/betnow-hover.gif\'" onmouseout="this.src=\'/themes/images/betnow-default.gif\';" alt="Bet Now" /></a></td>';
										// $Track["TRACK"] , $Track["RACE"] , $Track["MTP"]
			
										//$OpenTracksTable .= '<li class="'.$class.'"><a href="/todays-horseracing?id='.$Track["ID"].'&race='.$Track["RACE"].'&racename='.$Track["TRACK"].'" title="' . str_replace("'","", $Track["TRACK"]) . ' Results for Race '. $Track["RACE"] . '"><div class="racename">'.$Track["TRACK"].'</div><div class="raceinfo">Race #'.$Track["RACE"].'-'.$Track["MTP"].' MTP</div></a></li>';
											$OpenTracksTable .='</tr>';
									}//foreach
								}//else 
					}
					else
					{
						$sqlmsg= "select * from create_message where id='104'";
						$resultmsg=mysql_query_w($sqlmsg);
						$datamsg=mysql_fetch_object($resultmsg);
				
						$OpenTracksTable .= '<tr class="tracks-cell"><td class="width50perc" colspan="4">'.stripslashes($datamsg->user_conf_msg).'</td></tr>';
						$ClosedTracksTable .= '<tr class="tracks-cell"><td class="width50perc" colspan="4">'.stripslashes($datamsg->user_conf_msg).'</td></tr>';
					}//else Sucess
					
					
					
		}// else DB
		$OpenTracksTable .=' </tbody></table>';
		return $OpenTracksTable;
}




//echo OpenTrackTable();
?>
