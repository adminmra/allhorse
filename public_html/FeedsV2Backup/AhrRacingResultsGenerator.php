
<?php
if ( !defined('APP_PATH') ) {
	define( 'APP_PATH' , '/home/ah/usracing.com/htdocs/FeedsV2/' );
}

require_once APP_PATH . 'Generator.php';

class AhrRacingResultsGenerator extends Generator{

	function __construct(){
		parent::__construct();
	}

	public function content_tpl(){
		ob_start();
		?>

		<style>
		.not-front #content-box { padding: 0;}
		</style>
		<table class="table table-condensed table-striped table-bordered"   id="infoEntries" summary="Horse Racing Graded Stakes Schedule">
			<tbody>
				<tr >
					<th valign="top"  >Date</th>
					<th  >Graded Stakes </th>
				</tr>
				<?php
				$sql_schedule = "select * from schedule where type = '4' and display = 'Y'";
				$res_schedule=DB::query( $sql_schedule );
				$counter = 0;
				while( $data_schedule = $res_schedule->fetch_object() ) {
					if( $counter%2 == 0 ) {
						echo '<tr class="odd" >';
					}
					else {
						echo '<tr>';
					}
					?>

					 <td valign="top" width="10%" ><strong><?php	 	 $scdate = explode(" ",$data_schedule->addedon); $fscdate =explode("-",$scdate[0]);
					 	echo date('M',mktime(0,0,0,$fscdate[1],$fscdate[2],$fscdate[0]))." ".date('d',mktime(0,0,0,$fscdate[1],$fscdate[2],$fscdate[0])); ?></strong></td>
					 <td  ><?php	 	 echo nl2br($data_schedule->data); ?></td>
					</tr>
					<?php
					$counter++;
				}
				?>
			</tbody>
		</table>
		<?php
		return ob_get_clean();
	}



	public static function run() {
		try {

			$instance = new self;
			$html_block = $instance->content_tpl();
			$instance->as_file_save_data( $html_block , 'ahr_racing_results_page.tpl' );
		} catch ( Exception $e ) {
			print( $e->getMessage() );
		}
	}

}

AhrRacingResultsGenerator::run();

?>
