
<?php
if ( !defined('APP_PATH') ) {
	define( 'APP_PATH' , '/home/ah/allhorse/public_html/FeedsV2/' );
}

require_once APP_PATH . 'Generator.php';

class AhrBlockTopLeadersGenerator extends Generator{

	function __construct(){
		parent::__construct();
	}

	public function content_tpl(){

		$template_path = '/themes/';
		$sql_j         = "SELECT * FROM top_leader where type = '2' and position = '1' ";
		$result_j      = DB::query( $sql_j );
		$data_j        = $result_j->fetch_object();

		$sql_h         = "SELECT * FROM top_leader where type = '1' and position = '1' ";
		$result_h      = DB::query( $sql_h );
		$data_h        = $result_h->fetch_object();

		$sql_t         = "SELECT * FROM top_leader where type = '3' and position = '1' ";
		$result_t      = DB::query($sql_t);
		$data_t        = $result_t->fetch_object();

		ob_start();
		?>
		<div id="leaders-content">

		    <div class="horses">
		      <div class="leaders-header">Horses</div>
		      <div class="leaders-info">
		      <div class="leaders-name">1. <?php echo stripslashes($data_h->name); ?>
		      	<a data-toggle="collapse" data-parent="#leaders-content" href="#leadersItem1" class="btn btn-simple collapsed">
		      	<i class="fa fa-angle-down"></i>
		      	</a>
		      </div>
		      <div id="leadersItem1" class="leaders-table collapse">

		          <table width="100%" border="0" cellpadding="0" cellspacing="0">
		            <tbody>
		              <tr class="bold">
		                <td class="width53">Starts</td>
		                <td class="width53">1sts</td>
		                <td class="width53">2nds</td>
		                <td class="width53">3rds</td>
		                <td class="width113">Purses</td>
		              </tr>
		              <tr>
						<td id="j-starts"><?php	 	 echo $data_h->starts; ?></td>
		                <td id="j-1sts"><?php	 	 echo $data_h->first; ?></td>
		                <td id="j-2nds"><?php	 	 echo $data_h->second; ?></td>
		                <td id="j-3rds"><?php	 	 echo $data_h->third; ?></td>
		                <td id="j-purses"><?php	 	 echo $data_h->purse; ?></td>
		              </tr>
		            </tbody>
		          </table>
		        </div><!-- /leadersItem2 -->
		      </div><!-- /leaders-info -->
		      <div class="blockfooter">
		       <a href="/leaders/horses" title="Top Ten Horses" class="btn btn-primary">Top Ten Horses <i class="fa fa-angle-right"></i></a>
			   </div>

		    </div><!-- /horses -->

		    <div class="jockeys">
		      <div class="leaders-header">Jockeys</div>
		      <div class="leaders-info">
		      <div class="leaders-name">1. <?php	 	 echo stripslashes($data_j->name); ?> <a  data-toggle="collapse" data-parent="#leaders-content" href="#leadersItem2" class="btn btn-simple collapsed"><i class="fa fa-angle-down"></i></a></div>
		      <div id="leadersItem2" class="leaders-table collapse">

		          <table width="100%" border="0" cellpadding="0" cellspacing="0">
		            <tbody>
		              <tr class="bold">
		                <td class="width53">Starts</td>
		                <td class="width53">1sts</td>
		                <td class="width53">2nds</td>
		                <td class="width53">3rds</td>
		                <td class="width113">Purses</td>
		              </tr>
		              <tr>
		                <td id="j-starts"><?php	 	 echo $data_j->starts; ?></td>
		                <td id="j-1sts"><?php	 	 echo $data_j->first; ?></td>
		                <td id="j-2nds"><?php	 	 echo $data_j->second; ?></td>
		                <td id="j-3rds"><?php	 	 echo $data_j->third; ?></td>
		                <td id="j-purses"><?php	 	 echo $data_j->purse; ?></td>
		              </tr>
		            </tbody>
		          </table>
		        </div><!-- /leadersItem1 -->
		      </div><!-- /leaders-info -->
		     <div class="blockfooter">
		     <a href="/leaders/jockeys" title="Top Ten Jockeys" class="btn btn-primary">Top Ten Jockeys <i class="fa fa-angle-right"></i></a>
		     </div>

		    </div><!-- /jockeys -->



		    <div class="trainers">
		      <div class="leaders-header">Trainers</div>
		      <div class="leaders-info">
		      <div class="leaders-name">1. <?php	 	 echo stripslashes($data_t->name); ?> <a  data-toggle="collapse" data-parent="#leaders-content" href="#leadersItem3" class="btn btn-simple collapsed"><i class="fa fa-angle-down"></i></a></div>
		      <div id="leadersItem3" class="leaders-table collapse">

		          <table width="100%" border="0" cellpadding="0" cellspacing="0">
		            <tbody>
		              <tr class="bold">
		                <td class="width53">Starts</td>
		                <td class="width53">1sts</td>
		                <td class="width53">2nds</td>
		                <td class="width53">3rds</td>
		                <td class="width113">Purses</td>
		              </tr>
		              <tr>
						<td id="j-starts"><?php	 	 echo $data_t->starts; ?></td>
		                <td id="j-1sts"><?php	 	 echo $data_t->first; ?></td>
		                <td id="j-2nds"><?php	 	 echo $data_t->second; ?></td>
		                <td id="j-3rds"><?php	 	 echo $data_t->third; ?></td>
		                <td id="j-purses"><?php	 	 echo $data_t->purse; ?></td>
		              </tr>
		            </tbody>
		          </table>
		        </div><!-- /leadersItem3 -->
		      </div><!-- /leaders-info -->
		      <div class="blockfooter">
		      <a href="/leaders/trainers" title="Top Ten Trainers" class="btn btn-primary">Top Ten Trainers <i class="fa fa-angle-right"></i></a>
		      </div>

		    </div><!-- /trainers -->

		</div><!-- /leaders-content -->

		<?php
		return ob_get_clean();
	}

	public function block_top_horse(){

		$sql_top    = "SELECT * FROM top_leader where type = '1' ORDER BY position ";
		$result_top = DB::query( $sql_top );
		ob_start();
		?>
		<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
		<tbody>
		<tr>
		<td class="header" width="5%" align="center"><strong>#</strong></td>
		<td class="header" width="35%"><strong>Name</strong></td>
		<td class="header" width="10%"><strong>Starts</strong></td>
		<td class="header" width="10%"><strong>1sts</strong></td>
		<td class="header" width="10%"><strong>2nds</strong></td>
		<td class="header" width="10%"><strong>3rds</strong></td>
		<td class="header" width="20%"><strong>Purses</strong></td>
		</tr>
		<?php
		$counter = 0;
		while( $data_top = $result_top->fetch_object() ) {
		$updatedate = explode("-",$data_top->updatedas);
		$updateas   = date("F jS, Y",mktime(0,0,0,$updatedate[1],$updatedate[2],$updatedate[0]));
		if( $counter%2 == 1 ) {
			echo '<tr class="odd">';
		}
		else {
			echo '<tr>';
		}
		?>
			<td class="num"><?php	 	 echo $data_top->position; ?>.</td>
			<td><?php	 	 echo stripslashes($data_top->name); ?></td>
			<td><?php	 	 echo $data_top->starts; ?></td>
			<td><?php	 	 echo $data_top->first; ?></td>
			<td><?php	 	 echo $data_top->second; ?></td>
			<td><?php	 	 echo $data_top->third; ?></td>
			<td><?php	 	 echo $data_top->purse; ?></td>
		</tr>
		<?php
			$counter++;
		}

		?>
		</tbody>
		</table>
		<p class="news-date" style="text-align: right;"><em>(Through <?php echo $updateas; ?>)</em></p>
		<?php
		return ob_get_clean();

	}

	public function block_top_jockey(){
		$sql_top= "SELECT * FROM top_leader where type = '2' ORDER BY position ";
		$result_top=DB::query($sql_top);
		ob_start();
		?>
		<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
		<tbody>
		<tr>
		<td class="header" width="5%" align="center"><strong>#</strong></td>
		<td class="header" width="35%"><strong>Name</strong></td>
		<td class="header" width="10%"><strong>Starts</strong></td>
		<td class="header" width="10%"><strong>1sts</strong></td>
		<td class="header" width="10%"><strong>2nds</strong></td>
		<td class="header" width="10%"><strong>3rds</strong></td>
		<td class="header" width="20%"><strong>Purses</strong></td>
		</tr>
		<?php
		$counter=0;
		while($data_top=$result_top->fetch_object())
		{
		$updatedate=explode("-",$data_top->updatedas);
		$updateas=date("F jS, Y",mktime(0,0,0,$updatedate[1],$updatedate[2],$updatedate[0]));
		if($counter%2 == 1)
		{
			echo '<tr class="odd">';
		}
		else
		{
			echo '<tr>';
		}
		?>
			<td class="num"><?php	 	 echo $data_top->position; ?>.</td>
			<td><?php	 	 echo stripslashes($data_top->name); ?></td>
			<td><?php	 	 echo $data_top->starts; ?></td>
			<td><?php	 	 echo $data_top->first; ?></td>
			<td><?php	 	 echo $data_top->second; ?></td>
			<td><?php	 	 echo $data_top->third; ?></td>
			<td><?php	 	 echo $data_top->purse; ?></td>
		</tr>
		<?php
		$counter++;
		}

		?>
		</tbody>
		</table>
		<p class="news-date" style="text-align: right;"><em>(Through <?php	 	 echo $updateas; ?>)</em></p>
		<?php
		return ob_get_clean();
	}

	public function block_top_trainer(){

		$sql_top    = "SELECT * FROM top_leader where type = '3' ORDER BY position ";
		$result_top = DB::query( $sql_top );
		ob_start();
		?>
		<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
		<tbody>
		<tr>
		<td class="header" width="5%" align="center"><strong>#</strong></td>
		<td class="header" width="35%"><strong>Name</strong></td>
		<td class="header" width="10%"><strong>Starts</strong></td>
		<td class="header" width="10%"><strong>1sts</strong></td>
		<td class="header" width="10%"><strong>2nds</strong></td>
		<td class="header" width="10%"><strong>3rds</strong></td>
		<td class="header" width="20%"><strong>Purses</strong></td>
		</tr>
		<?php
		$counter=0;
		while( $data_top = $result_top->fetch_object() ) {
		$updatedate=explode("-",$data_top->updatedas);
		$updateas=date("F jS, Y",mktime(0,0,0,$updatedate[1],$updatedate[2],$updatedate[0]));
		if($counter%2 == 1) {
			echo '<tr class="odd">';
		}
		else {
			echo '<tr>';
		}
		?>
			<td class="num"><?php	 	 echo $data_top->position; ?>.</td>
			<td><?php	 	 echo stripslashes($data_top->name); ?></td>
			<td><?php	 	 echo $data_top->starts; ?></td>
			<td><?php	 	 echo $data_top->first; ?></td>
			<td><?php	 	 echo $data_top->second; ?></td>
			<td><?php	 	 echo $data_top->third; ?></td>
			<td><?php	 	 echo $data_top->purse; ?></td>
		</tr>
		<?php
		$counter++;
		}

		?>
		</tbody>
		</table>
		<p class="news-date" style="text-align: right;"><em>(Through <?php	 	 echo $updateas; ?>)</em></p>
		<?php
		return ob_get_clean();
	}

	public static function run() {
		try {

			$instance   = new self;
			$html_block = $instance->content_tpl();
			//$instance->as_file_save_data( $html_block , 'ahr_block_top_leaders.tpl' );
			$instance->as_file_save_data( $html_block , ALLHORSEPATH . 'public_html/generated/leaders/leaders.php' , TRUE );

			$html_block = $instance->block_top_horse();
			//$instance->as_file_save_data( $html_block , 'ahr_block_top-horse.tpl' );
			$instance->as_file_save_data( $html_block , ALLHORSEPATH . 'public_html/generated/leaders/horses.php' , TRUE );

			$html_block = $instance->block_top_jockey();
			//$instance->as_file_save_data( $html_block , 'ahr_block_top-jockey.tpl' );
			$instance->as_file_save_data( $html_block , ALLHORSEPATH . 'public_html/generated/leaders/jockeys.php' , TRUE );

			$html_block = $instance->block_top_trainer();
			//$instance->as_file_save_data( $html_block , 'ahr_block_top-trainer.tpl' );
			$instance->as_file_save_data( $html_block , ALLHORSEPATH . 'public_html/generated/leaders/trainers.php' , TRUE );
		} catch ( Exception $e ) {
			print( $e->getMessage() );
		}
	}

}

AhrBlockTopLeadersGenerator::run();

?>