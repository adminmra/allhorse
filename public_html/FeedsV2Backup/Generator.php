s<?php

//namespace Generator;
define( 'ALLHORSEPATH', '/home/ah/allhorse/' );
define( 'SMARTYPATH'      , "/home/ah/usracing.com/smarty" );
define( 'SITEABSOLUTEURL' , "http://www.usracing.com/" );
define( 'TEMPLATE_DIR'    , "/home/ah/usracing.com/smarty/templates/includes/" );
define( 'DS'       , DIRECTORY_SEPARATOR );
define( 'EOF'      , "<br />\n");

require_once APP_PATH . 'simple_html_dom.php';
require_once APP_PATH . 'ConsoleColors.php';
require_once APP_PATH . 'Database.php';
require_once APP_PATH . 'Database2.php';

abstract class Generator {

	protected $source;
	protected $output;
	protected $colors;
	function __construct(){
		$this->source      = null;
		$this->output      = null;
		$this->colors = new ConsoleColors();
	}

	abstract public function content_tpl();

	protected function as_file_save_data( $data, $dest, $is_absolute_path = FALSE ){

		$file = TEMPLATE_DIR . $dest;

		if($is_absolute_path){
			$file = $dest;
			$dirname = dirname($file);
			if (!is_dir($dirname)){
			    if(!mkdir($dirname, 0755, true)){
			    	echo sprintf( $this->colors->getColoredString( ">>> " , "cyan" ) . "%'.-110s%-s\n" , $this->colors->getColoredString( $file , "red" ) , 'NOT GENERATED. Directory not created.');
			    	return FALSE;
			    }
			}
		}

		$data = preg_replace('/drupal\//','',$data);
		$data = preg_replace('/<script/','{literal}<script',$data);
		$data = preg_replace('/<\/script>/','</script>{/literal}',$data);
		$data = preg_replace('/<style/','{literal}<style',$data);
		$data = preg_replace('/<\/style>/','</style>{/literal}',$data);

	  	if ( !$fp = fopen( $file, 'wb' ) ){
	  		echo sprintf( $this->colors->getColoredString( ">>> " , "cyan" ) . "%'.-110s%-s\n" , $this->colors->getColoredString( $file , "red" ) , 'NOT GENERATED. File not created.');
	  		return "Could not write to ".$file;
	  	}

		fwrite($fp, $data);
		fclose( $fp );

		echo sprintf( $this->colors->getColoredString( ">>> " , "cyan" ) . "%'.-110s%-s\n" , $this->colors->getColoredString( $file , "red" ) , 'GENERATED');
	}

	protected function nextdayformatted( $date, $dayafter ) {
		$datearray = explode( "-" , $date );
		return  date( 'F j, Y' , mktime( 0, 0, 0, $datearray[1], $datearray[2]+$dayafter, $datearray[0] ) );
	}

	protected function weekfirstday() {
		$today = date( 'w' );
		return  date( 'Y-m-d', mktime( 0, 0, 0, date('m'), date('d')-($today-1), date( 'Y' ) ) );
	}

	protected function nextday( $date, $dayafter ) {
		$datearray = explode( "-", $date );
		return  date( 'Y-m-d', mktime( 0, 0, 0, $datearray[1], $datearray[2]+$dayafter, $datearray[0] ) );
	}

	protected function ahr_abbr_html($Html, $Len) {
		if ( strlen( $Html ) > $Len ) {
			return substr( $Html, 0, $Len ) . '...';
		} else {
			return $Html;
		}
	}

}

