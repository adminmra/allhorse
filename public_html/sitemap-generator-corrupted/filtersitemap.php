<?php	 		 	
/*
 * Stract urls from sitemap.xml
 * author: fv
 */


// Here we do the lazy man's way of using file_get_contents
$url = 'http://allhorse.com/sitemap-generator/sitemaps/sitemap-usracing.xml';

$strXml = @file_get_contents($url);
if (false == $strXml)
    die('Could not open url. Check your spelling and try again');

// So simple using SimpleXml  
//Generating new Sitemap.xml
$sitemap = @new SimpleXmlElement($strXml);
$new_entries = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
$new_entries .= "<urlset
                               xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"
                                  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
                                  xsi:schemaLocation=\"
                                        http://www.sitemaps.org/schemas/sitemap/0.9
                                        http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n";
$new_entries .= "<!-- created with Filter Sitemap Usracing -->\n";

$filtered = "<h2>Filtered</h2>";
$added = "<h2>Added</h2>";
$modified = "<h2>Modified</h2>";
$i = 0;
foreach ($sitemap->url as $url) {
    if (strstr($url->loc, '/horse?') || strstr($url->loc, '/trainer?') || strstr($url->loc, '/jockey?')) {
        $filtered .= $url->loc . '<br />';
    } else {
        $added .= $i . '. ' . $url->loc . '<br />';
        if (strstr($url->loc, '/racetrack/') || strstr($url->loc, '/state/') || strstr($url->loc, '/stake/')) {

            if (strstr($url->loc, '/racetrack/'))
                $new_url = str_replace('/racetrack/', '/', $url->loc);
            if (strstr($url->loc, '/state/'))
                $new_url = str_replace('/state/', '/', $url->loc);
            if (strstr($url->loc, '/stake/'))
                $new_url = str_replace('/stake/', '/', $url->loc);

            $modified .= $url->loc . ' -to- ' . $new_url . '<br />';
            $new_entries .= "<url>\n";
            $new_entries .= "<loc>" . $new_url . "</loc>\n";
            $new_entries .= "<lastmod>" . $url->lastmod . "</lastmod>\n";
            $new_entries .= "<changefreq>" . $url->changefreq . "</changefreq>\n";
            $new_entries .= "<priority>" . $url->priority . "</priority>\n";
            $new_entries .= "</url>";
        }else {
            $new_entries .= "<url>\n";
            $new_entries .= "<loc>" . $url->loc . "</loc>\n";
            $new_entries .= "<lastmod>" . $url->lastmod . "</lastmod>\n";
            $new_entries .= "<changefreq>" . $url->changefreq . "</changefreq>\n";
            $new_entries .= "<priority>" . $url->priority . "</priority>\n";
            $new_entries .= "</url>\n";
        }
        $i++;
    }
}

$new_entries .= "</urlset>";

$filename = '/home/ah/usracing.com/htdocs/sitemap.xml';
$link = fopen($filename, 'w');
fwrite($link, $new_entries);
fclose($link);

echo $filtered;
echo $added;
echo $modified;

echo '<br />Done!!';