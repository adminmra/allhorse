



<?php
$today = date('Y-m-d');
$date1 = date('Y-m-d', strtotime(' - 1 days'));
$date2 = date('Y-m-d', strtotime(' - 2 days'));
$date3 = date('Y-m-d', strtotime(' - 3 days'));
$date4 = date('Y-m-d', strtotime(' - 4 days'));
$date5 = date('Y-m-d', strtotime(' - 5 days'));
$date6 = date('Y-m-d', strtotime(' - 6 days'));
$date7 = date('Y-m-d', strtotime(' - 7 days'));
$date = array($today,$date1,$date2,$date3,$date4,$date5,$date6);


$todayM = date('M-d');
$date1M = date('M-d', strtotime(' - 1 days'));
$date2M = date('M-d', strtotime(' - 2 days'));
$date3M = date('M-d', strtotime(' - 3 days'));
$date4M = date('M-d', strtotime(' - 4 days'));
$date5M = date('M-d', strtotime(' - 5 days'));
$date6M = date('M-d', strtotime(' - 6 days'));
$date7M = date('M-d', strtotime(' - 7 days'));
$dateM = array($todayM,$date1M,$date2M,$date3M,$date4M,$date5M,$date6M);




echo "<div class='menus'>";
echo "<h2 class='date-title'>RESULTS BY DATE</h2>";
echo "<div class='menu_date'>";
for($i=0; $i<7; $i++){

    if($i != 0){
    echo "<div class='button'>";
    echo "<a class='date' id='d{$i}'>".$dateM[$i]."</a>";
    echo "</div>";
    }else{
        echo "<div class='button selected'>";
        echo "<a class='date' id='d{$i}'>".$dateM[$i]."</a>";
        echo "</div>";
    }
}
echo "</div>";
echo "</div>";



echo "<div class='tables'>";
for($i=0; $i<7; $i++){
    if($i != 0){
        $file = $date[$i].'.php';
        echo '<div class="off d'.$i.'">';
        include($file);
        echo "</div>";
    }else{
        echo '<div class="on d'.$i.'">';
        include("usr_results.php");
        echo "</div>";
    }
}

echo "</div>";
?>