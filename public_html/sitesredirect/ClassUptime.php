<?php

class up_time_monitor{
	
	private $APIKEY;
	private $GETMONITORURL;
	private $EDITMONITORURL;
	private $Format;
	
	function __construct(){
        $this->APIKEY = "u97585-b2f4324c82ecb664bcb39c70";
        $this->GETMONITORURL = "https://api.uptimerobot.com/v2/getMonitors";
		$this->EDITMONITORURL = "https://api.uptimerobot.com/v2/editMonitor";
        $this->Format = "xml";
    }
	
	
	function GetMonitor($siteid){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL =>  $this->GETMONITORURL,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "api_key=".$this->APIKEY."&format=xml&logs=0&monitors=".$siteid,
		  CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: application/x-www-form-urlencoded"
		  ),
		));
		 
		$response = curl_exec($curl);
		$err = curl_error($curl);
		 
		curl_close($curl);
		 
		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  //echo $response;
			
			$xml = simplexml_load_string($response);
			//echo "<pre>";
		 //print_r($xml);
		return $xml->monitor->attributes()->status;
		}

	}
	
	function changeUptime($siteid, $status){
		
		$curl = curl_init();
 
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $this->EDITMONITORURL,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "api_key=".$this->APIKEY."&format=xml&id=".$siteid."&status=".$status,
			  CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/x-www-form-urlencoded"
			  ),
			));
			 
			$response = curl_exec($curl);
			$err = curl_error($curl);
			 
			curl_close($curl);
			 
			if ($err) {
			  //echo "cURL Error #:" . $err;
			  return false;
			} else {
			  //echo $response;
				//$xml = simplexml_load_string($response);
			return true;
			 // echo "<pre>";
			 //print_r($xml);
			}
		
	}
	
}

//$uptime = new up_time_monitor;
//echo $uptime->GetMonitor('780446740');
/*echo "<br />";
echo $uptime->changeUptime('780446740', '1');
echo "<br />";
echo $uptime->GetMonitor('780446740');
*/
?>