
    <section style="background-image:url(/public/assets/images/hero.jpg)" class="hero">
      <div class="container">
        <div class="hero_content has-account">
          <div class="block-center">
            <div class="block-center_content">
                <a href="/promos"
                    style=" color: #fff; font-family: 'Lato', Arial, 'Helvetica Neue', Helvetica, sans-serif; font-weight: 700; line-height: 1.1; text-transform: uppercase; margin: 0 0 15px; text-decoration: none;">
               <h1 class="hero_title">Bet on Saudi Cup</h1>{*resumes after races*}
                    <h2 class="hero_subtitle">20% Sign Up Bonus</h2>{*resumes after races*}
                    <h2 class="hero_subtitle">$150 Racebook Cashback</h2>{*resumes after races*}
                    <h2 class="hero_subtitle">8% Rebate</h2>{*resumes after races*}


                </a>
                <a href="/signup" class="hero_join account">Join Now</a>

            </div>
          </div>
        </div>

        <div class="hero_account">
          <div class="open-account">
            <h3 class="open-account_title">Open an Account</h3>
            <div class="open-account_form">
              <form action="" method="post" id="aform_signup">
                <div class="aform_field">
                  <input type="text" placeholder="First Name" name="first_name" id="s_firstname">
                  <small class="error" style="display: none;" id="s_fn_error">Please fill out this field</small>
                </div>
                <div class="aform_field">
                  <input type="text" placeholder="Last Name" name="last_name" id="s_lastname">
                  <small class="error" style="display: none;" id="s_ln_error">Please fill out this field</small>
                </div>
                <div class="aform_field">
                  <input type="text" placeholder="Email Address" name="email" id="s_email">
                  <small class="error" style="display: none;" id="s_em_error">Please fill out this field</small>
                </div>
                <div class="aform_btns">
                  <button type="submit" class="aform_submit">Sign-up now</button>

                </div>
              </form>
            </div>
          </div>
        </div>
        </div>
        </section> 


        <section class="steps">
          <div class="container">
            <div class="block-center">
              <div class="block-center_content">
                <div class="step"><span class="step_number">01</span><span class="step_label"><span
                      class="step_label-text">Create your<strong>Account</strong></span></span></div>
                <div class="step"><span class="step_number">02</span><span class="step_label"><span
                      class="step_label-text">Make a<strong>Deposit</strong></span></span></div>
                <div class="step"><span class="step_number">03</span><span class="step_label"><span
                      class="step_label-text">Start<strong>Winning</strong></span></span></div>
              </div>
            </div>
          </div>
        </section>



        <section class="friends">
          <div class="container">
            <div class="block-center">
              <div class="block-center_content"><img src="/public/assets/images/friends.png" alt="" class="friends_img">
              </div>
            </div>
          </div>
        </section>

        <section class="card undefined">
          <div class="card_wrap">
            <div class="card_half card_hide-mobile"><img src="/public/assets/images/bet-horse.jpg"
                data-src-img="/public/assets/images/bet-horse.jpg" alt="Bet on Horses" class="card_img"></div>

            <div class="card_half card_content">
            <h2 class="card_heading">SIGN UP TODAY AND GET</h2>
            <p>A 20% Sign up Cash Bonus, up to 8% in Rebates, Exclusive Odds to Win Bet on your favorite horse racing events during the year. Qualify to receive a $150 Racebook Cashback bonus. Earn up to 8% in rebate and receive a 20% Sign up Bonus! Place a wager on any sports game or play the best casino games online!</p>
                <a href="/signup?ref=index" class="card_btn-red">I Want to Join</a>
            </div>
          </div>
        </section>