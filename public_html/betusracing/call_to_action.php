    <section class="card-large">
      <div class="container">
        <div class="block-center">
          <div class="block-center_content">
            <h2 class="card-large_heading">Get in on the Exciting Racing Action at <br><img src="/public/assets/images/busr_logo_images/busr_new_logo/busr.png" width="250px" ></h2><a href="/signup?ref={$ref}" class="card-large_btn">REGISTER NOW</a>
          </div>
        </div>
      </div>
    </section>