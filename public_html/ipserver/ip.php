<?PHP
header('Content-Type: application/json');

require_once('functions.php');

//date time and time zone
$date_time = date("Y-m-d H:i:s");
$time_zone = date('e') . ' (' . date('T') . date('P').')';

//servers version
$php_version        = phpversion();
$server_version     = $_SERVER['SERVER_SOFTWARE'];

//apache users
$apache_user    = exec('whoami');

//show server
$server_ip  = $_SERVER["SERVER_ADDR"];
$lb_ip      = $_SERVER['REMOTE_ADDR'];

//file info
$filename = "ip.php";

$server_name    = server_info($server_ip);
$lb_name        = lb_info($lb_ip);

$array_gitlog = null;

$array_git = array(
    'branch' => null,
    'log'   =>  $array_gitlog,
    );

$results = array(
    'server_name'       => $server_name,
    'server_ip'         => $server_ip,
    'lb_name'           => $lb_name,
    'lb_ip'             => $lb_ip,
    'date_time'         => $date_time,
    'time_zone'         => $time_zone,
    'php_version'       => $php_version,
    'server_version'    => $server_version,
    'apache_user'       => $apache_user,
    'git'               => $array_git
);

$results = json_encode($results);

print_r($results);
?>
