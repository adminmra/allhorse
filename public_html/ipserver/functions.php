<?php
function dirList ($directory, $sortOrder){

    //Get each file and add its details to two arrays
    $results = array();
    $handler = opendir($directory);
    while ($file = readdir($handler)) {  
        if ($file != '.' && $file != '..' && $file != "robots.txt" && $file != ".htaccess"){
            $currentModified = filectime($directory."/".$file);
            $file_names[] = $file;
            $file_dates[] = $currentModified;
        }    
    }
       closedir($handler);

    //Sort the date array by preferred order
    if ($sortOrder == "newestFirst"){
        arsort($file_dates);
    }else{
        asort($file_dates);
    }
    
    //Match file_names array to file_dates array
    $file_names_Array = array_keys($file_dates);
    foreach ($file_names_Array as $idx => $name) $name=$file_names[$name];
    $file_dates = array_merge($file_dates);
    
    $i = 0;

    //Loop through dates array and then echo the list
    foreach ($file_dates as $file_dates){
        $date = $file_dates;
        $j = $file_names_Array[$i];
        $file = $file_names[$j];
        $i++;
            
        echo  "File name: $file - Date Added: $date. <br/>";        
    }

}

function server_info($server_ip){

    switch ($server_ip) {

        case "35.183.230.76":
            $server_name = "Mail";
            break;
        case "198.72.119.53":
            $server_name = "Hades";
            break;
        case "172.22.0.7":
            $server_name = "Hades";
            break;
        case "172.31.20.101":
            $server_name = "Falcon";
            break;
        case "172.31.3.47":
            $server_name = "Han-Canada";
            break;
        case "172.31.1.173":
            $server_name = "Leia-USR-bak";
            break;
        case "52.60.221.184":
            $server_name = "Leia-USR-bak";
            break;
        case "52.60.233.101":
            $server_name = "R2D2";
            break;
        case "172.31.7.187":
            $server_name = "R2D2";
            break;
        case "172.31.7.70":
            $server_name = "CleanDev";
            break;
        case "172.31.30.223":
            $server_name = "Yoda";
            break;
        case "172.31.1.114":
            $server_name = "Stage";
            break;
        case "172.31.4.100":
            $server_name = "Backup";
            break;
        case "54.241.198.154":
            $server_name = "Backup";
            break;
        case "52.8.209.101":
            $server_name = "Bitbucket";
            break;
        case "54.193.54.236":
            $server_name = "Cronworker";
            break;
        case "172.31.30.100":
            $server_name = "Test";
            break;
        case "174.142.39.41":
            $server_name = "Luke";
            break;
        case "144.217.42.145":
            $server_name = "Ashoka";
            break;
        case '172.31.3.229':
            $server_name = "Palpatine";
            break;
        case '10.15.160.62':
            $server_name = "Hadesv1";
            break;
        case '10.15.160.63':
            $server_name = "Lukev1";
            break;
        case '10.15.160.61':
            $server_name = "Ashokav1";
            break;
        case '167.114.151.153':
            $server_name = "Hadesv1";
            break;
        case '167.114.151.152':
            $server_name = "Ashokav1";
            break;
	    case "54.219.165.243":
	        $server_name = "Falcon";
	        break;
	    case "52.60.47.46":
	        $server_name = "BUSR Dev";
	        break;
	    case "198.72.105.213":
	        $server_name = "MotoCA1";
	        break;
	    case "168.197.96.135":
	        $server_name = "MotoPA1";
	        break;
	    case "107.180.51.237":
	        $server_name = "Lando";
	        break;
	    case "174.142.48.173":
	        $server_name = "MyB - SmarterMail";
	        break;
	    case "52.52.26.175":
	        $server_name = "C3PO";
	        break;
	    case "52.60.70.252":
	        $server_name = "Yoda";
	        break;
	    case "35.183.194.66":
	        $server_name = "Palpatine";
	        break;

            //busr public
        case "35.183.66.215":
            $server_name = "Busr-1";
            break;
        case "52.60.103.225":
            $server_name = "Busr-2";
            break;
        case "3.96.28.254":
            $server_name = "Busr-3";
            break;
            
            //busr local
        case "172.31.12.21":
            $server_name = "Busr-1";
            break;
        case "172.31.0.252":
            $server_name = "Busr-2";
            break;
        case "172.31.8.133":
            $server_name = "Busr-3";
            break;

        default:
            $server_name = "Unknown";
            break;
    }
    return $server_name;
} 

function lb_info($lb_ip){

    switch ($lb_ip) {
        case "167.114.31.1":
            $lb_name = "Motoca3";
            break;
        case "168.197.96.140":
            $lb_name = "Motopa1";
            break;
        case "64.252.173.131":
            $lb_name = "CloudFront";
            break;
        case "10.0.253.114":
            $lb_name = "gateway";
            break;
        case "10.15.8.111":
            $lb_name = "motovh";
            break;
        case "190.106.75.170":
            $lb_name = "CROffice-1";
            break;
        default:
            $lb_name = "Unknown";
            break;
     }
     return $lb_name;
}

?>