    <div>
        <noscript></noscript>
        <table class="data table table-condensed table-striped table-bordered order1" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="2022 Senate General Election">
            <caption>2022 Senate General Election</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated August 18, 2021.</em>
                    </td>
                </tr>
            </tfoot>
            
            <tbody>
        <tr><th>Team</th><th>Fractional</th><th>American</th></tr><tr><td>Rubio</td><td>1/5</td><td>-500</td></tr><tr><td>Demings</td><td>3/1</td><td>+300</td></tr>                    </tbody>
        </table>
    </div>
    