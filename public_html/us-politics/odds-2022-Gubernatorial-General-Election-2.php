    <div>
        <noscript></noscript>
        <table class="data table table-condensed table-striped table-bordered order1" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="2022 Gubernatorial General Election">
            <caption>2022 Gubernatorial General Election</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated August 18, 2021.</em>
                    </td>
                </tr>
            </tfoot>
            
            <tbody>
        <tr><th>Team</th><th>Fractional</th><th>American</th></tr><tr><td>Desantis</td><td>1/4</td><td>-400</td></tr><tr><td>Fried</td><td>7/2</td><td>+350</td></tr>                    </tbody>
        </table>
    </div>
    