    <div>
        <noscript></noscript>
        <table class="data table table-condensed table-striped table-bordered order1" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="US Senate Elections – Florida">
            <caption>US Senate Elections – Florida</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated August 18, 2021.</em>
                    </td>
                </tr>
            </tfoot>
            
            <tbody>
        <tr><th>Team</th><th>Fractional</th><th>American</th></tr><tr><td>Republicans</td><td>2/7</td><td>-350</td></tr><tr><td>Democrats</td><td>9/4</td><td>+225</td></tr>                    </tbody>
        </table>
    </div>
    