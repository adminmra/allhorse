    <section class="card">
      <div class="card_wrap">
          <div class="card_half card_hide-mobile">
            <a href="/promos/cash-bonus-150">{*change the path of the link*}
                <img src="/img/states/cash-bonus-150.jpg" class="card_img" alt="Horse Racing 2020 $150 Cash Bonus Offer">{*change the path the img in src*}
            </a>
        </div>
       <div class="card_half card_content">
            <a href="/promos/cash-bonus-150">{*change the path of the link*}
                  <img class="icon-bet-kentucky-derby card_icon" alt="Horse Racing 2020 $150 Cash Bonus Offer" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABVAQMAAADUnuB8AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABFJREFUeNpjYBgFo2AUDHIAAAOnAAHnt6kCAAAAAElFTkSuQmCC">
	        </a>
            <h2 class="card_heading">$150 CASH BONUS!</h2>
            <h3 class="card_subheading">
            <p style="margin-bottom: 50px">If you wager  at least $500 at <a href="/signup?ref={$ref2}" rel="nofollow">BUSR</a> within 30 days of registering your account, we will automatically deposit an extra $150 into your account!</p>
            <a href="/promos/cash-bonus-150" class="card_btn">Learn More</a>
        </div>
    </section>