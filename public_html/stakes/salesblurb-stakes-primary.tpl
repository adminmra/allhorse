<p>With <a href="/signup?ref={$ref2}" rel="nofollow">BUSR</a> you can get an outstanding <a href="/promos/cash-bonus-20">New Member Cash Bonus</a> up to $500 on your first deposit for all your horse betting action.</p>
<p>And that's not all...<br>
You can feel more in control by getting up to <a href="/rebates">8% rebate </a>on all horse betting action, paid directly to your player account the very next day! Also, you may qualify for an amazing <a href="/promos/cash-bonus-150">$150 Cash Bonus</a>!</p>
