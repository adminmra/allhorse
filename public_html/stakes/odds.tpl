{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}



{literal}
<!-- <link rel="stylesheet" href="/assets/css/sbstyle.css"> -->
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}

{literal}
  <script type="application/ld+json">
	 {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='dubai kd usr-section']/div[@class='container']/div[@class='kd_content']/div/table[@class='oddstable']/tbody/tr[1]/td[@class='center']/table[@id='o3t']/captions"
    }
	</script>
  <script type="application/ld+json">
	 {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='dubai kd usr-section']/div[@class='container']/div[@class='kd_content']/div/table[@class='oddstable']/tbody/tr[2]/td[@class='center']/table[@id='o1t']/caption"
    }
	</script>
  <script type="application/ld+json">
	 {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='dubai kd usr-section']/div[@class='container']/div[@class='kd_content']/div/table[@class='oddstable']/tbody/tr[3]/td[@class='center']/table[@id='o2t']/caption"
    }
	</script>
  <script type="application/ld+json">
	 {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='dubai kd usr-section']/div[@class='container']/div[@class='kd_content']/div/table[@class='oddstable']/tbody/tr[4]/td[@class='center']/table[@id='o0t']/caption"
    }
	</script>
  <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "location": {
            "@type": "Place",
            "address": {
                "@type": "PostalAddress"
            }
        },
        "name": "Dubai World Cup",
        "startDate": "2020-03-28",
        "endDate": "2020-03-28",
        "image": [
          "https://www.usracing.com/img/dubai-world-cup/dubai-world-cup-lg.jpg",
          "https://www.usracing.com/img/dubai-world-cup/dubai-world-cup-md.jpg",
          "https://www.usracing.com/img/dubai-world-cup/dubai-world-cup-sm.jpg"
        ],
        "description": "The Dubai World Cup",
        "offers": {
            "@type": "Offer",
            "name": "NEW MEMBERS GET UP TO $500 CASH!",
            "price": "500.00",
            "priceCurrency": "USD",
            "url": "https://www.usracing.com/promos/cash-bonus-20",
            "availability": "http://schema.org/InStock"
        },
        "sport": "Horse Racing",
        "url": "https://www.usracing.com/dubai-world-cup/odds"
    }
	</script>
{/literal}


{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
        }

        @media (max-width: 324px) {
          .usr-section .container {
            padding: 0px;
          }
        }
     </style>
{/literal}

<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
</div>

    {include file="/home/ah/allhorse/public_html/dubai/block-countdown.tpl"}
    <section class="dubai kd usr-section">
  <div class="container">
    <div class="kd_content">
      <h1 class="kd_heading">{$h1}</h1>
      <h3 class="kd_subheading">{$h3}</h3>
      <p>{include file='/home/ah/allhorse/public_html/dubai/salesblurb.tpl'} </p>
      <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet on the Dubai World Cup</a></p>

        {* <h2 class="table-title">{include file='/home/ah/allhorse/public_html/saudi-cup/year.php'} Dubai World Cup Odds and Contenders</h2> *}
        <p>{include_php file='/home/ah/allhorse/public_html/dubai/odds_dubai_1005_xml_new.php'}</p>
        <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet on the Dubai World Cup</a></p>
        <h2>Get your Picks Ready for {include_php file='/home/ah/allhorse/public_html/dubai/date.php'}.</h2>
<p>Analyze our current <b>Dubai World Cup Betting Odds</b>, and get your wagering action on your favorite horses!</p>
<p>Also remember that as we add new horses and remove others, the <b>Dubai World Cup Betting Odds</b> will be changing. So, remember that when you place a future wager, the odds you’re currently taking won’t vary, even when the actual line continues to move.</p>
<p>Good luck and see you on {include_php file='/home/ah/allhorse/public_html/dubai/day.php'}!</p>
    </div>
  </div>
</section>

  <!------------------------------------------------------------------>

{include file="/home/ah/allhorse/public_html/dubai/block-testimonial.tpl"}
{include file="/home/ah/allhorse/public_html/dubai/block-signup-bonus.tpl"}
{include file="/home/ah/allhorse/public_html/dubai/block-exciting.tpl"}


  <!------------------------------------------------------------------>

