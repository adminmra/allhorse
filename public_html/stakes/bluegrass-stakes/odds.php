<div id="no-more-tables">
<table class="data table table-condensed table-striped table-bordered ordenableResult" title="Blue Grass Stakes Odds" border="0" summary="Blue Grass Stakes Odds" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <th>Post</th>
            <th>Horse</th>
            <th>Trainer</th>
            <th>Jockey</th>
            <th>Odds</th>
        </tr>
    </thead>
    <tbody>
        <caption class="hide-sm">Blue Grass Stakes Odds</caption>
        <tr>
            <td data-title="Post">1</td>
            <td data-title="Horse">Shivaree</td>
            <td data-title="Trainer">Ralph Nicks</td>
            <td data-title="Jockey">John Velazquez</td>
            <td data-title="Odds">8-1</td>
        </tr>
        <tr>
            <td data-title="Post">2</td>
            <td data-title="Horse">Finnick the Fierce</td>
            <td data-title="Trainer">Rey Hernandez</td>
            <td data-title="Jockey">Jose Ortiz</td>
            <td data-title="Odds">12-1</td>
        </tr>
        <tr>
            <td data-title="Post">3</td>
            <td data-title="Horse">Art Collector</td>
            <td data-title="Trainer">Tom Drury Jr.</td>
            <td data-title="Jockey">Brian Hernandez Jr.</td>
            <td data-title="Odds">6-1</td>
        </tr>
        <tr>
            <td data-title="Post">4</td>
            <td data-title="Horse">Mr. Big News</td>
            <td data-title="Trainer">Bret Calhoun</td>
            <td data-title="Jockey">Mitchell Murrill</td>
            <td data-title="Odds">10-1</td>
        </tr>
        <tr>
            <td data-title="Post">5</td>
            <td data-title="Horse">Man in the Can</td>
            <td data-title="Trainer">Ron Moquett</td>
            <td data-title="Jockey">Tyler Gaffalione</td>
            <td data-title="Odds">12-1</td>
        </tr>
        <tr>
            <td data-title="Post">6</td>
            <td data-title="Horse">Hard Lighting</td>
            <td data-title="Trainer">Alexis Delgado</td>
            <td data-title="Jockey">Rafael Bejarano</td>
            <td data-title="Odds">50-1</td>
        </tr>
        <tr>
            <td data-title="Post">7</td>
            <td data-title="Horse">Swiss Skydiver</td>
            <td data-title="Trainer">Kenny McPeek</td>
            <td data-title="Jockey">Mike Smith</td>
            <td data-title="Odds">3-1</td>
        </tr>
        <tr>
            <td data-title="Post">8</td>
            <td data-title="Horse">Basin</td>
            <td data-title="Trainer">Steve Asmussen</td>
            <td data-title="Jockey">Ricardo Santana Jr.</td>
            <td data-title="Odds">8-1</td>
        </tr>
        <tr>
            <td data-title="Post">9</td>
            <td data-title="Horse">Attachment Rate</td>
            <td data-title="Trainer">Dale Romans</td>
            <td data-title="Jockey">Luis Saez</td>
            <td data-title="Odds">20-1</td>
        </tr>
        <tr>
            <td data-title="Post">10</td>
            <td data-title="Horse">Rushie</td>
            <td data-title="Trainer">Michael McCarthy</td>
            <td data-title="Jockey">Javier Castellano</td>
            <td data-title="Odds">5-1</td>
        </tr>
        <tr>
            <td data-title="Post">11</td>
            <td data-title="Horse">Hunt the Front</td>
            <td data-title="Trainer">Nick Zito</td>
            <td data-title="Jockey">Corey Lanerie</td>
            <td data-title="Odds">20-1</td>
        </tr>
        <tr>
            <td data-title="Post">12</td>
            <td data-title="Horse">Enforceable</td>
            <td data-title="Trainer">Mark Casse</td>
            <td data-title="Jockey">Joel Rosario</td>
            <td data-title="Odds">8-1</td>
        </tr>
        <tr>
            <td data-title="Post">13</td>
            <td data-title="Horse">Tiesto</td>
            <td data-title="Trainer">Bill Mott</td>
            <td data-title="Jockey">Flavien Prat</td>
            <td data-title="Odds">15-1</td>
        </tr>
    </tbody>
</table>
</div>

 <script src="/assets/js/sorttable_results.js"></script>

