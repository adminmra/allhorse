<p>
The <strong>Arkansas Derby Stakes</strong> is the second leg of the <strong>Big 3 Pick 3</strong>, a $1 minimum national wager where horse bettors will try to pick the winner of three important prep races for three-year-olds planning on running in the <a title="Kentucky Derby" href="/kentucky-derby/odds"><strong>Kentucky Derby</strong></a>. and the prestigious Triple Crown. Two favorite horses in recent years, Smarty Jones and Afleet Alex, both captured the <strong>Arkansas Derby</strong> and finished their careers as top three-year-olds. The Arkansas Derby has really gained in stature because of this, keeping its reputation as a vital race for any Kentucky Derby and <strong>Triple Crown</strong> hopeful. The race is being held at <a title="Oaklawn Park" href="/oaklawn-park"><strong>Oaklawn Park</strong></a>, which was incorporated in 1904 and one of the select few that features the finest in live thoroughbred horse racing in America.
</p>

<p><a href='/signup?ref={$ref2}'><strong> Sign up</strong></a> with <a href='/signup?ref={$ref3}'>BUSR </a> and <a href='/signup?ref={$ref4}'>place your bets </a> now!</p>




 