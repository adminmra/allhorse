<h2>Arkansas Derby Betting | Quick Facts</h2>
 <p>1- The <strong>Arkansas Derby</strong> is one of the most highly-attended races in the United States, with crowds exceeding 60,000 each year. Only the famous <a title="Kentucky Derby" href="/kentucky-derby/odds"><strong>Kentucky Derby</strong></a>, Preakness, Kentucky Oaks, Belmont Stakes and Breeders' Cup attract similar large crowds.</p>
<p>2-Back in 1976, Elocutionist became the first Arkansas winner to go on to win the <a title="Triple Crown" href="/bet-on/triple-crown"><strong>Triple Crown</strong></a>.</p>
<p>3-Another two colts, Smarty Jones (2004) and American Pharoah (2015) managed to win both the Arkansas Derby and the Kentucky Derby.</p>




