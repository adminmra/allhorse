{literal}
<script type="text/javascript" src="/assets/js/atemay.js"></script>
<script type="text/javascript">
addthisevent.settings({
	license   	: "ammgus1bqzg0il7ntm75",
	mouse		: false,
	css			: false,
	outlook		: {show:true, text:"Outlook <i class='fa fa-plus-square'></i>"},
	google		: {show:true, text:"Google <i class='fa fa-plus-square'></i>"},
	yahoo		: {show:true, text:"Yahoo <i class='fa fa-plus-square'></i>"},
	ical		: {show:true, text:"iCal <i class='fa fa-plus-square'></i>"},
	hotmail		: {show:true, text:"Hotmail <i class='fa fa-plus-square'></i>"},
	facebook	: {show:true, text:"Facebook <i class='fa fa-plus-square'></i>"}
});
</script>
{/literal}


<div class="row infoBlocksBC" itemscope itemtype="http://schema.org/Event">

<div class="col-md-4">
<div class="section">
	<i class="fa fa-calendar circle"></i>
    
	<div class="info">
	<p>The <a itemprop="url" title="Bet on Prix de l'Arc de Triomphe" href="/prix-de-larc-de-triomphe"> <meta itemprop="name" content="Bet on the {include file='/home/ah/allhorse/public_html/stakes/prix-de-larc-de-triomphe/year.php'} Prix de l'Arc de Triomphe - Odds are live!" />
  		     <meta itemprop="description" content="The {include file='/home/ah/allhorse/public_html/stakes/prix-de-larc-de-triomphe/running.php'} Annual Prix de l'Arc de Triomphe, where the best of the best race." /> Prix de l'Arc de Triomphe</a> is on {include_php file='/home/ah/allhorse/public_html/stakes/prix-de-larc-de-triomphe/racedate.php'}
    </p>
	</div>
    <a href="#" title="Add to Calendar" class="addthisevent" rel="nofollow">Add to calendar <i class="fa fa-plus"></i>
	<span class="_start">10-04-2020 14:00:00</span>
	<span class="_end">10-04-2020 18:00:00</span>
	<span class="_zonecode">15</span> <!-- EST US -->
	<span class="_summary">{include file='/home/ah/allhorse/public_html/stakes/prix-de-larc-de-triomphe/year.php'} Prix de l'Arc de Triomphe</span>
	<span class="_description">www.usracing.com <br/>Place your bets on the Prix de l'Arc de Triomphe at Betusracing.ag.<br/>Races times subject to change.</span>
	<span class="_location">Longchamp Racecourse, Paris, France</span>
	<span class="_organizer">USRacing.com</span>
	<span class="_organizer_email">comments@usracing.com</span>
	<span class="_all_day_event">true</span>
	<span class="_date_format">04/10/2020</span>
	</a>
</div>
</div>

<div class="col-md-4">
<div class="section">
	<i class="fa fa-map-marker circle"></i>
	<div class="info">
	<p>Who Will Win the Prix de l'Arc de Triomphe? <a href="/prix-de-larc-de-triomphe">Odds are Live!</a></p>
    </div>
    <a href="/signup?ref=prix-de-larc-de-triomphe" class="btn btn-primary btn-sm btn-red" rel="nofollow">Bet Now!<i class="fa fa-chevron-right"></i></a>
</div>
</div>
<meta itemprop="endDate" content="2020-10-04T18:00"> 
<meta itemprop="performer" content="US Racing | Online Horse Betting"> 
<meta itemprop="image" content="https://www.usracing.com/img/prix-de-larc-de-triomphe/og_prix-de-larc-de-triomphe_USR.jpg"> 
    <div  style="display: none;" itemprop="location" itemscope itemtype="http://schema.org/Place">
        <span itemprop="name">Longchamp Racecourse</span>
        <div class="address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            <span itemprop="streetAddress">2 Route des Tribunes</span><br>
            <span itemprop="addressLocality">Paris</span>, <span itemprop="addressRegion">France</span> <span itemprop="postalCode">75016</span>
        </div>
    </div>
        <div style="display: none;" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
        <meta itemprop="price" content="150">
        <meta itemprop="priceCurrency" content="USD">
        <meta itemprop="url" content="https://www.usracing.com/promos/cash-bonus-150">
        <meta itemprop="availability" content="https://schema.org/InStock">
        <meta itemprop="validFrom" content="2015-10-01T00:01">
    </div>
{*
<div class="col-md-4">
<div class="section">
	<i class="fa fa-map-marker"></i>
	<div class="info">
	<p itemprop="location" itemscope itemtype="http://schema.org/Place">Located at {include file='/home/ah/allhorse/public_html/breeders/location.php'}</p>
    </div>
    <a href="/keeneland" class="btn btn-primary btn-sm" rel="nofollow">Go to map<i class="fa fa-chevron-right"></i></a>
</div>
</div>
*}

<div class="col-md-4">
<div class="section">
	<i class="glyphicon glyphicon-eye-open circle"></i>
    <div class="info">
	<p>Watch on NBC Sports on Sunday {include_php file='/home/ah/allhorse/public_html/stakes/prix-de-larc-de-triomphe/racedate.php'}</p>
    </div>
</div>
</div>
</div>
