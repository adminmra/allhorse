<section class="card">
    <div class="card_wrap">
        <div class="card_half card_content">
           <a href="/rebates">
                <img class="icon-bet-kentucky-derby-jockeys card_icon" alt="Horse Racing 2020 Rebates Offer"
                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkAQMAAABKLAcXAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRQE8AAAV4AAHYKaK4AAAAAElFTkSuQmCC">
              </a>
            <h2 class="card_heading">UP TO 8% HORSE BETTING REBATE!</h2>
            <p style="margin-bottom: 10px">Feel more in control thanks to an outstanding rebate bonus!</p>
            <p>Make your straight bets with <a href="/signup?ref={$ref2}" rel="nofollow">BUSR</a>, and the next day, you'll find a 3% rebate in your account! You can also bet on anything except for Win, Place and Show, and you'll find up to 8% rebate!</p>
            <a href="/rebates" class="card_btn">Learn More</a>
        </div>
        <div class="card_half card_hide-mobile">
            <a href="/signup?ref={$ref2}" rel="nofollow">{*change the path of the link*}
                <img src="/img/states/rebates.jpg" class="card_img" alt="Horse Racing 2020 Rebates Offer">{*change the path the img in src*}
            </a>
        </div>
    </div>
</section>