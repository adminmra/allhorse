<section class="card">
    <div class="card_wrap">
        <div class="card_half card_content">
            <a href="https://www.usracing.com/rebates">{*change the path of the link*}
                <img class="icon-kentucky-derby-betting card_icon"
                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABVAQMAAADUnuB8AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABFJREFUeNpjYBgFo2AUDHIAAAOnAAHnt6kCAAAAAElFTkSuQmCC">{*change the path the img in src*}
            </a>
            <h2 class="card_heading">8% HORSE BETTING REBATE!</h2>
            <h3 class="card_subheading">
            <p style="margin-bottom: 10px">Get compensated on your losses for straight and exotic wagers!</p>
            <p>Make your straight bets, and the next day, you'll find a 3% rebate in your account! You can also bet on anything except for Win, Place and Show, and you'll find an 8% rebate!</p>
            <a href="https://www.usracing.com/rebates" class="card_btn">Learn More</a>
        </div>
        <div class="card_half card_hide-mobile">
            <a href="/signup?ref=horse-racing">{*change the path of the link*}
                <img src="/img/pegasus-world-cup/signup-offer.jpg" class="card_img" alt="Horse Racing 2020 Signup Offer">{*change the path the img in src*}
            </a>
        </div>
    </div>
</section>