    <section class="card">
      <div class="card_wrap">
        <div class="card_half card_hide-mobile">
	        <a href="/kentucky-derby/trainer-betting">
		        <img src="/img/index-kd/bet-kentucky-derby.jpg" data-src-img="/img/index-kd/bet-kentucky-derby.jpg" alt="Kentucky Derby Odds:  Trainers"  class="card_img"></a></div>
        <div class="card_half card_content">
	        <a href="/kentucky-derby/trainer-betting">
	        {* <img src="/img/index-kd/icon-bet-kentucky-derby.png"alt="Kentucky Derby Odds:  Trainers"  class="card_icon"> *}
          <img class="icon-bet-kentucky-derby card_icon" alt="Kentucky Derby Odds:  Trainers" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFIAAABdAQMAAAAMtKMNAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABJJREFUeNpjYBgFo2AUjALqAwAEXAABxG45GAAAAABJRU5ErkJggg==">
	        </a>
          <h2 class="card_heading">$150 Cash Bonus!</h2>
          <p>If you play at least $500 in cumulative wagering within 30 days of registering your account, we will automatically deposit an extra $150 into your account!</p>
          <a href="https://dev.usracing.com/promos/cash-bonus-150" class="card_btn">Learn More</a>
        </div>
      </div>
    </section>
