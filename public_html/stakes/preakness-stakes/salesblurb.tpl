{if $KD_Switch} 

<p><strong>BUSR</strong> is the best place you can <a href="/preakness-stakes">bet on the Preakness Stakes</a>. Not only are over 256 different types of bets you can make, you'll get a <a href="/preakness-stakes/free-bet">FREE Bet</a>, and a <a href="/promos/cash-bonus-10">welcome bonus</a> up to $250!</p>

<p>

  You'll also get great fixed odds with amazing payouts on the leading <a href="/preakness-stakes">Kentucky Oaks</a> and <a href="/kentucky-derby/contenders">Derby horses</a> and even the leading <a href="/kentucky-derby/jockey-betting">Jockeys</a> and <a href="/kentucky-derby/trainer-betting">Trainers</a>.  Bet on the <a href="/kentucky-derby/margin-of-victory">margin of victory</a>, head-to-head <a href="/kentucky-derby/match-races">matchups</a>,  the <a href="/kentucky-derby/props">winning time vs Secretariat's</a> world record, a <a href="/kentucky-derby/props"> Triple Crown contention</a> and more. </p>

<p>The <a href="/kentucky-derby/odds">{include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Derby Odds</a> are live. Time to place your <a href="/signup?ref={$ref}" rel="nofollow">bet now!</a></p> 

{else}
{if $dateNow <= '2020-03-30 23:59:59'}
<p>The 146th <a href="/signup?ref={$ref2}"> Kentucky Derby</a> has been rescheduled to run on {include file='/home/ah/allhorse/public_html/kd/racedate.php'}. Due to the global pandemic of COVID-19, <strong>Churchill Downs</strong> has moved the race to ensure the fans will be able to attend and the race can run in all it's glory. </p>
{/if}

<p><a href="/signup?ref={$ref3}">BUSR</a> is the best place you can <a href="/signup?ref={$ref4}">bet on the Preakness Stakes</a>. There are over 256 different types of bets you can place from the comfort of your computer or mobile device.  You'll get great future odds with amazing payouts.  </p>



{/if}