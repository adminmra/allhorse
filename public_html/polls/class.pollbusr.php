<?php 

class PollsBUSR{
	
	public $XDIMENSION;
	public $YDIMENSION;
	public $CHARTXDIMENSION;
	public $CHARTYDIMENSION;	
	public $XYDIMENSION;
	public $source;
	public $xmlSource;
	public $XTableHeader;
	public $YTableHeader;
	public $TitleTableHeader;
	public $XTableHeaderDisplay;
	public $YTableHeaderDisplay;	
	
	
	function __construct(){		
		$this->XDIMENSION           = array();
		$this->YDIMENSION           = array();
		$this->XYDIMENSION          = array();	
		$this->XTableHeader           = array();
		$this->YTableHeader          = array();	
		$this->XTableHeaderDisplay	=array();
		$this->YTableHeaderDisplay	=array();		
	}
	
	
	public function ExtractData(){
		$xml_string = $this->xmlSource;
		$counter=0;
		foreach($xml_string->Response as $KEYVAR => $allVars){
		$CHARTYDIMENSION=$this->CHARTYDIMENSION;
		$CHARTXDIMENSION=$this->CHARTXDIMENSION;
		$this->YDIMENSION[]=strval($allVars->$CHARTYDIMENSION);
		$this->XDIMENSION[]=strval($allVars->$CHARTXDIMENSION);
		$this->XYDIMENSION[strval($allVars->$CHARTYDIMENSION)][strval($allVars->$CHARTXDIMENSION)][]=1;
		$counter++;
		}		
	}
	
	
/*	public function process(){
		$this->ExtractData();
		echo "<pre>";
		print_r($this->YDIMENSION);
		print_r($this->XDIMENSION);
		print_r($this->XYDIMENSION);
		echo "</pre>";
	} */
	
	public function TotalPercent($countvote,$countTotal){
		if($countTotal > 0){
	return round(($countvote * 100)/$countTotal, 0, PHP_ROUND_HALF_DOWN);
		}else { return '0'; }
	}
	
	public function output($y,$x){
		$UniqueY=array_count_values($this->YDIMENSION);
		$UniqueX=array_count_values($this->XDIMENSION);
		return $this->TotalPercent(count(@$this->XYDIMENSION[$y][$x]), @$UniqueX[$x])." %";		
	}
	
	public function outputTABLE(){
		$this->ExtractData();
		$countVal=0;
		$printHTML= '';
		$printHTML .= '<div id="no-more-tables">';
		$printHTML .= '<table class="data table table-condensed table-striped table-bordered poll-table" width="100%" cellpadding="0" cellspacing="0" border="0" >';
		$printHTML .= '<thead>';
		$printHTML .= '<tr><th class="poll-table-heading">'.$this->TitleTableHeader.'</th>';
		if(count($this->XTableHeaderDisplay) > 0){
			foreach($this->XTableHeaderDisplay as $headVal){
			$printHTML .= '<th class="poll-table-head">'.$headVal.'</th>';	
			}
		}else{
			foreach($this->XTableHeader as $headVal){
			$printHTML .= '<th class="poll-table-head">'.$headVal.'</th>';	
			}			
		}
		$printHTML .= '</tr>';
		$printHTML .= '</thead>';
		$printHTML .= '<tbody>';
		$counterRow=0;
		foreach($this->YTableHeader as $Yval){
		$printHTML .= '<tr>';
		if(count($this->YTableHeaderDisplay) > 0){
		$printHTML .= '<th class="poll-table-contesttant">'.$this->YTableHeaderDisplay[$counterRow].'</th>';
		}else{
		$printHTML .= '<th class="poll-table-contesttant">'.$Yval.'</th>';			
		}
			 foreach($this->XTableHeader as $headVal){
				$printHTML .= '<td class="poll-table-data">'.$this->output($Yval,$headVal).'</td>';	
			 }
		$printHTML .= '</tr>';
		$counterRow++;
		}
		$printHTML .= '</tbody>';
		$printHTML .= '</table>';
		$printHTML .= '</div>';
		
		return $printHTML;
	}	
	
	public function outputTABLERAW(){
		$countVal=0;
		$this->ExtractData();
		$UniqueY=array_count_values($this->YDIMENSION);
		$UniqueX=array_count_values($this->XDIMENSION);
		$printHTML= '';
		$printHTML .= '<div id="no-more-tables">';
		$printHTML .= '<table class="data table table-condensed table-striped table-bordered poll-table" width="100%" cellpadding="0" cellspacing="0" border="0" >';
		$printHTML .= '<thead>';
		$printHTML .= '<tr><th class="poll-table-heading">'.$this->TitleTableHeader.'</th>';
		if(count($this->XTableHeaderDisplay) > 0){
			foreach($this->XTableHeaderDisplay as $headVal){
			$printHTML .= '<th class="poll-table-head">'.$headVal.'</th>';	
			}
		}else{
			foreach($this->XTableHeader as $headVal){
			$printHTML .= '<th class="poll-table-head">'.$headVal.'</th>';	
			}			
		}
		$printHTML .= '<th>Total</th>';
		$printHTML .= '</tr>';
		$printHTML .= '</thead>';
		$printHTML .= '<tbody>';
		$counterRow=0;
		foreach($this->YTableHeader as $Yval){
		$printHTML .= '<tr>';
		if(count($this->YTableHeaderDisplay) > 0){
		$printHTML .= '<th class="poll-table-contesttant">'.$this->YTableHeaderDisplay[$counterRow].'</th>';
		}else{
		$printHTML .= '<th class="poll-table-contesttant">'.$Yval.'</th>';			
		}
		
			 foreach($this->XTableHeader as $headVal){
				$printHTML .= '<td class="poll-table-data">'.count(@$this->XYDIMENSION[$Yval][$headVal]).'</td>';	
			 }
		$printHTML .= '<td class="poll-table-data">'.@$UniqueY[$Yval].'</td>';
		$printHTML .= '</tr>';
		$counterRow++;
		}
		$printHTML .= '<tr><th class="poll-table-heading">Total</th>';
			$totalRecordX=0;
			foreach($this->XTableHeader as $headVal){	
				$totalRecordX=$totalRecordX+@$UniqueX[$headVal];			
				$printHTML .= '<td class="poll-table-data">'.@$UniqueX[$headVal].'</td>';	
			 }
		$printHTML .= '<td class="poll-table-data">'.$totalRecordX.'</td>';
		$printHTML .= '</tr>';			 
		$printHTML .= '</tbody>';
		$printHTML .= '</table>';
		$printHTML .= '</div>';
		
		return $printHTML;
	}	
	
		
	
	
}


?>