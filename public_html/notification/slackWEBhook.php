<?php
/*$type = "Info";
$channel= "#it-alerts-bot";
$message="Test Message For WEB ";
$domain="N/A";

sendAlertToSlack($type, $channel, $message, $domain);*/

function sendAlertToSlack($type, $channel, $message, $domain) {
/* Parameters:     type (Alert, Info, Error, Action, Warning).
				   channel -> #it-alerts-bot .
				   message -> Message to be send.
				   domain -> domain from the message was sent. */
	if($channel == "#it-alerts-bot"){
		$channelURL="https://hooks.slack.com/services/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";
	}else{ exit; }
				   
				   
    $webHookUrl = $channelURL;
    $data = new stdClass();
    $data->text = "*MSG TYPE* - ".$type." \r\n*Domain* - ".$domain."  \r\n ".$message;

    $data = json_encode($data);

    // Create a new cURL resource
    $chWebHook = curl_init($webHookUrl);
    // Attach encoded JSON string to the POST fields
	curl_setopt($chWebHook, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($chWebHook, CURLOPT_POSTFIELDS, $data);
    // Set the content type to application/json
    curl_setopt($chWebHook, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    // Return response instead of outputting
    curl_setopt($chWebHook, CURLOPT_RETURNTRANSFER, true);
    // Execute the POST request
	curl_setopt($chWebHook, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($data))                                                                       
	); 
    $result = curl_exec($chWebHook);
	
}

?>