
<html>
<head>
	<style>
		#modal-background{
			display: none;
			position: absolute;
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
			background: black;
			background-color: rgba(0, 0, 0, 0.9);
			progid: alpha(opacity = 50);
		}


		#modal{
			width: 352px;
			height: 328px;
			position: absolute;
			left: 5%;
			right: 5%;
			top: 5%;
			padding: 10px;
			background: ;
		}

		#modal iframe{
			height: 22em;
			width: 20em;
		}

		#close{
			position: absolute;
			top: 2em;
			right: 1em;
		}
	</style>

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
</head>
<body>
	<div id="modal-background">
		<div id="modal">
			<!-- Modal Content here -->
			<iframe src="http://www.betusracing.ag/login_form_iframe.php" width="100%" height="900"></iframe>
			<!-- <iframe src="http://betusracing.local/login_form_iframe.php" height="320px"></iframe> -->
			<span id="close"></span>
		</div>
	</div>
	<!-- You page goes here -->
	<button id="open">Open</button>

	<script type="text/javascript">
		$(function(){
			$("#open").click(function(){
				$("#open").hide();
				$("#modal-background").fadeIn();
			})
			$("#close").click(function(){
				$("#modal-background").fadeOut(function(){
					$("#open").show();
				});
			})
		})
	</script>
</body>
</html>