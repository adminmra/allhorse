    <div>
        <noscript></noscript>
        <table class="data table table-condensed table-striped table-bordered order1" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="BOXING - Apr 03">
            <caption>MMA - UFC - Jun 12</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated June 9, 2021.</em>
                    </td>
                </tr>
            </tfoot>
            
            <tbody>
            <tr><th>Team</th><th>Fractional</th><th>American</th></tr><tr><td>Marvin Vettori</td><td>2/1</td><td>+200</td></tr><tr><td>Israel Adesanya</td><td>5/13</td><td>-260</td></tr>            </tbody>
        </table>
    </div>
    