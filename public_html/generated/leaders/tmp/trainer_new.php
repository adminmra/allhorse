		<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
		<tbody>
		<tr>
		<td class="header" width="5%" align="center"><strong>#</strong></td>
		<td class="header" width="35%"><strong>Name</strong></td>
		<td class="header" width="10%"><strong>Starts</strong></td>
		<td class="header" width="10%"><strong>1sts</strong></td>
		<td class="header" width="10%"><strong>2nds</strong></td>
		<td class="header" width="10%"><strong>3rds</strong></td>
		<td class="header" width="20%"><strong>Purses</strong></td>
		</tr>
		<tr>			<td class="num">1.</td>
			<td>Chad C. Brown</td>
			<td>763</td>
			<td>182</td>
			<td>146</td>
			<td>106</td>
			<td>$23,134,394</td>
		</tr>
		<tr class="odd">			<td class="num">2.</td>
			<td>Todd A. Pletcher</td>
			<td>1,213</td>
			<td>274</td>
			<td>189</td>
			<td>170</td>
			<td>$21,177,262</td>
		</tr>
		<tr>			<td class="num">3.</td>
			<td>Mark E. Casse</td>
			<td>1,334</td>
			<td>235</td>
			<td>202</td>
			<td>182</td>
			<td>$17,946,005</td>
		</tr>
		<tr class="odd">			<td class="num">4.</td>
			<td>Bob   Baffert</td>
			<td>412</td>
			<td>108</td>
			<td>81</td>
			<td>50</td>
			<td>$15,864,326</td>
		</tr>
		<tr>			<td class="num">5.</td>
			<td>Steven M. Asmussen</td>
			<td>1,600</td>
			<td>317</td>
			<td>250</td>
			<td>212</td>
			<td>$14,829,340</td>
		</tr>
		<tr class="odd">			<td class="num">6.</td>
			<td>Michael J. Maker</td>
			<td>1,152</td>
			<td>221</td>
			<td>175</td>
			<td>142</td>
			<td>$11,561,312</td>
		</tr>
		<tr>			<td class="num">7.</td>
			<td>Doug F. O'Neill</td>
			<td>719</td>
			<td>106</td>
			<td>110</td>
			<td>111</td>
			<td>$10,329,329</td>
		</tr>
		<tr class="odd">			<td class="num">8.</td>
			<td>William I. Mott</td>
			<td>691</td>
			<td>95</td>
			<td>93</td>
			<td>89</td>
			<td>$9,800,970</td>
		</tr>
		<tr>			<td class="num">9.</td>
			<td>Jerry   Hollendorfer</td>
			<td>1,078</td>
			<td>188</td>
			<td>145</td>
			<td>156</td>
			<td>$9,775,311</td>
		</tr>
		<tr class="odd">			<td class="num">10.</td>
			<td>Kiaran P. McLaughlin</td>
			<td>402</td>
			<td>73</td>
			<td>69</td>
			<td>61</td>
			<td>$9,459,289</td>
		</tr>
		<tr>			<td class="num">11.</td>
			<td>Rudy R. Rodriguez</td>
			<td>726</td>
			<td>147</td>
			<td>113</td>
			<td>106</td>
			<td>$7,944,644</td>
		</tr>
		<tr class="odd">			<td class="num">12.</td>
			<td>H. Graham Motion</td>
			<td>596</td>
			<td>103</td>
			<td>95</td>
			<td>86</td>
			<td>$7,452,021</td>
		</tr>
		<tr>			<td class="num">13.</td>
			<td>Philip   D'Amato</td>
			<td>542</td>
			<td>110</td>
			<td>79</td>
			<td>68</td>
			<td>$7,433,742</td>
		</tr>
		<tr class="odd">			<td class="num">14.</td>
			<td>Christophe   Clement</td>
			<td>539</td>
			<td>104</td>
			<td>84</td>
			<td>78</td>
			<td>$7,036,658</td>
		</tr>
		<tr>			<td class="num">15.</td>
			<td>Brad H. Cox</td>
			<td>545</td>
			<td>151</td>
			<td>93</td>
			<td>76</td>
			<td>$6,288,972</td>
		</tr>
		<tr class="odd">			<td class="num">16.</td>
			<td>Karl   Broberg</td>
			<td>1,566</td>
			<td>367</td>
			<td>280</td>
			<td>211</td>
			<td>$5,627,269</td>
		</tr>
		<tr>			<td class="num">17.</td>
			<td>Linda   Rice</td>
			<td>492</td>
			<td>136</td>
			<td>76</td>
			<td>74</td>
			<td>$5,209,171</td>
		</tr>
		<tr class="odd">			<td class="num">18.</td>
			<td>David   Jacobson</td>
			<td>497</td>
			<td>85</td>
			<td>85</td>
			<td>77</td>
			<td>$4,720,153</td>
		</tr>
		<tr>			<td class="num">19.</td>
			<td>Richard   Baltas</td>
			<td>460</td>
			<td>79</td>
			<td>73</td>
			<td>61</td>
			<td>$4,676,419</td>
		</tr>
		<tr class="odd">			<td class="num">20.</td>
			<td>Robertino   Diodoro</td>
			<td>933</td>
			<td>242</td>
			<td>168</td>
			<td>115</td>
			<td>$4,642,888</td>
		</tr>
		<tr>			<td class="num">21.</td>
			<td>Dale L. Romans</td>
			<td>433</td>
			<td>56</td>
			<td>66</td>
			<td>47</td>
			<td>$4,549,369</td>
		</tr>
		<tr class="odd">			<td class="num">22.</td>
			<td>Peter   Miller</td>
			<td>507</td>
			<td>98</td>
			<td>77</td>
			<td>69</td>
			<td>$4,482,498</td>
		</tr>
		<tr>			<td class="num">23.</td>
			<td>J. Keith Desormeaux</td>
			<td>227</td>
			<td>30</td>
			<td>39</td>
			<td>33</td>
			<td>$4,476,232</td>
		</tr>
		<tr class="odd">			<td class="num">24.</td>
			<td>Claude R. McGaughey III</td>
			<td>260</td>
			<td>53</td>
			<td>36</td>
			<td>32</td>
			<td>$4,399,817</td>
		</tr>
		<tr>			<td class="num">25.</td>
			<td>W. Bret Calhoun</td>
			<td>689</td>
			<td>146</td>
			<td>97</td>
			<td>91</td>
			<td>$4,356,635</td>
		</tr>
		<tr class="odd">			<td class="num">26.</td>
			<td>James A. Jerkens</td>
			<td>199</td>
			<td>40</td>
			<td>42</td>
			<td>21</td>
			<td>$4,259,123</td>
		</tr>
		<tr>			<td class="num">27.</td>
			<td>Patricia   Farro</td>
			<td>972</td>
			<td>140</td>
			<td>156</td>
			<td>148</td>
			<td>$4,239,527</td>
		</tr>
		<tr class="odd">			<td class="num">28.</td>
			<td>Peter   Eurton</td>
			<td>305</td>
			<td>44</td>
			<td>54</td>
			<td>33</td>
			<td>$4,235,430</td>
		</tr>
		<tr>			<td class="num">29.</td>
			<td>Jorge   Navarro</td>
			<td>487</td>
			<td>146</td>
			<td>82</td>
			<td>61</td>
			<td>$4,167,895</td>
		</tr>
		<tr class="odd">			<td class="num">30.</td>
			<td>Joe   Sharp</td>
			<td>736</td>
			<td>111</td>
			<td>118</td>
			<td>104</td>
			<td>$4,159,605</td>
		</tr>
				</tbody>
		</table>
		<p class="news-date" style="text-align: right;"><em>(Through March 27th , 2017)</em></p>

		