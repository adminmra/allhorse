

		<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
		<tbody>
		<tr>
		<td class="header" width="5%" align="center"><strong>#</strong></td>
		<td class="header" width="35%"><strong>Name</strong></td>
		<td class="header" width="10%"><strong>Starts</strong></td>
		<td class="header" width="10%"><strong>1sts</strong></td>
		<td class="header" width="10%"><strong>2nds</strong></td>
		<td class="header" width="10%"><strong>3rds</strong></td>
		<td class="header" width="20%"><strong>Purses</strong></td>
		</tr>
		<tr>			<td class="num">1.</td>
			<td>Arrogate</td>
			<td>6</td>
			<td>5</td>
			<td>0</td>
			<td>1</td>
			<td>$4,084,600</td>
		</tr>
		<tr class="odd">			<td class="num">2.</td>
			<td>Nyquist</td>
			<td>6</td>
			<td>3</td>
			<td>0</td>
			<td>1</td>
			<td>$3,575,600</td>
		</tr>
		<tr>			<td class="num">3.</td>
			<td>Exaggerator</td>
			<td>9</td>
			<td>3</td>
			<td>2</td>
			<td>1</td>
			<td>$2,598,000</td>
		</tr>
		<tr class="odd">			<td class="num">4.</td>
			<td>Songbird</td>
			<td>8</td>
			<td>7</td>
			<td>1</td>
			<td>0</td>
			<td>$2,210,000</td>
		</tr>
		<tr>			<td class="num">5.</td>
			<td>Highland Reel (IRE)</td>
			<td>1</td>
			<td>1</td>
			<td>0</td>
			<td>0</td>
			<td>$2,200,000</td>
		</tr>
		<tr class="odd">			<td class="num">6.</td>
			<td>California Chrome</td>
			<td>6</td>
			<td>5</td>
			<td>1</td>
			<td>0</td>
			<td>$2,090,000</td>
		</tr>
		<tr>			<td class="num">7.</td>
			<td>Flintshire (GB)</td>
			<td>5</td>
			<td>3</td>
			<td>2</td>
			<td>0</td>
			<td>$2,000,000</td>
		</tr>
		<tr class="odd">			<td class="num">8.</td>
			<td>Gun Runner</td>
			<td>9</td>
			<td>4</td>
			<td>2</td>
			<td>2</td>
			<td>$1,970,880</td>
		</tr>
		<tr>			<td class="num">9.</td>
			<td>Beholder</td>
			<td>6</td>
			<td>3</td>
			<td>3</td>
			<td>0</td>
			<td>$1,720,000</td>
		</tr>
		<tr class="odd">			<td class="num">10.</td>
			<td>Tourist</td>
			<td>6</td>
			<td>2</td>
			<td>1</td>
			<td>2</td>
			<td>$1,633,750</td>
		</tr>
		<tr>			<td class="num">11.</td>
			<td>Creator</td>
			<td>8</td>
			<td>3</td>
			<td>1</td>
			<td>1</td>
			<td>$1,582,000</td>
		</tr>
		<tr class="odd">			<td class="num">12.</td>
			<td>Classic Empire</td>
			<td>5</td>
			<td>4</td>
			<td>0</td>
			<td>0</td>
			<td>$1,485,920</td>
		</tr>
		<tr>			<td class="num">13.</td>
			<td>Tepin</td>
			<td>7</td>
			<td>5</td>
			<td>2</td>
			<td>0</td>
			<td>$1,476,360</td>
		</tr>
		<tr class="odd">			<td class="num">14.</td>
			<td>Frosted</td>
			<td>4</td>
			<td>2</td>
			<td>0</td>
			<td>1</td>
			<td>$1,460,000</td>
		</tr>
		<tr>			<td class="num">15.</td>
			<td>Hoppertunity</td>
			<td>7</td>
			<td>2</td>
			<td>0</td>
			<td>1</td>
			<td>$1,337,750</td>
		</tr>
		<tr class="odd">			<td class="num">16.</td>
			<td>Cupid</td>
			<td>8</td>
			<td>4</td>
			<td>1</td>
			<td>0</td>
			<td>$1,334,103</td>
		</tr>
		<tr>			<td class="num">17.</td>
			<td>Champagne Room</td>
			<td>5</td>
			<td>2</td>
			<td>1</td>
			<td>1</td>
			<td>$1,286,600</td>
		</tr>
		<tr class="odd">			<td class="num">18.</td>
			<td>Forever Unbridled</td>
			<td>6</td>
			<td>3</td>
			<td>1</td>
			<td>2</td>
			<td>$1,280,000</td>
		</tr>
		<tr>			<td class="num">19.</td>
			<td>Melatonin</td>
			<td>5</td>
			<td>3</td>
			<td>1</td>
			<td>0</td>
			<td>$1,266,000</td>
		</tr>
		<tr class="odd">			<td class="num">20.</td>
			<td>Connect</td>
			<td>6</td>
			<td>5</td>
			<td>0</td>
			<td>0</td>
			<td>$1,244,000</td>
		</tr>
		<tr>			<td class="num">21.</td>
			<td>Effinex</td>
			<td>8</td>
			<td>2</td>
			<td>1</td>
			<td>1</td>
			<td>$1,200,000</td>
		</tr>
		<tr class="odd">			<td class="num">22.</td>
			<td>Drefong</td>
			<td>4</td>
			<td>4</td>
			<td>0</td>
			<td>0</td>
			<td>$1,170,800</td>
		</tr>
		<tr>			<td class="num">23.</td>
			<td>Catch a Glimpse</td>
			<td>8</td>
			<td>5</td>
			<td>1</td>
			<td>0</td>
			<td>$1,154,033</td>
		</tr>
		<tr class="odd">			<td class="num">24.</td>
			<td>Cathryn Sophia</td>
			<td>7</td>
			<td>4</td>
			<td>0</td>
			<td>3</td>
			<td>$1,139,720</td>
		</tr>
		<tr>			<td class="num">25.</td>
			<td>A. P. Indian</td>
			<td>7</td>
			<td>6</td>
			<td>0</td>
			<td>0</td>
			<td>$1,135,000</td>
		</tr>
		<tr class="odd">			<td class="num">26.</td>
			<td>Cavorting</td>
			<td>4</td>
			<td>3</td>
			<td>1</td>
			<td>0</td>
			<td>$1,125,000</td>
		</tr>
		<tr>			<td class="num">27.</td>
			<td>Queen's Trust (GB)</td>
			<td>1</td>
			<td>1</td>
			<td>0</td>
			<td>0</td>
			<td>$1,100,000</td>
		</tr>
		<tr class="odd">			<td class="num">28.</td>
			<td>Miss Temple City</td>
			<td>6</td>
			<td>3</td>
			<td>0</td>
			<td>0</td>
			<td>$1,057,000</td>
		</tr>
		<tr>			<td class="num">29.</td>
			<td>Da Big Hoss</td>
			<td>7</td>
			<td>5</td>
			<td>0</td>
			<td>0</td>
			<td>$968,985</td>
		</tr>
		<tr class="odd">			<td class="num">30.</td>
			<td>I'm a Chatterbox</td>
			<td>6</td>
			<td>3</td>
			<td>1</td>
			<td>0</td>
			<td>$965,000</td>
		</tr>
				</tbody>
		</table>
		<p class="news-date" style="text-align: right;"><em>(Through March 27th , 2017)</em></p>
		