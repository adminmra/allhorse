
		<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
		<tbody>
		<tr>
		<td class="header" width="5%" align="center"><strong>#</strong></td>
		<td class="header" width="35%"><strong>Name</strong></td>
		<td class="header" width="10%"><strong>Starts</strong></td>
		<td class="header" width="10%"><strong>1sts</strong></td>
		<td class="header" width="10%"><strong>2nds</strong></td>
		<td class="header" width="10%"><strong>3rds</strong></td>
		<td class="header" width="20%"><strong>Purses</strong></td>
		</tr>
		<tr>			<td class="num">1.</td>
			<td>Javier Castellano</td>
			<td>1418</td>
			<td>300</td>
			<td>236</td>
			<td>222</td>
			<td>$26,827,966</td>
		</tr>
		<tr class="odd">			<td class="num">2.</td>
			<td>Irad Ortiz, Jr.</td>
			<td>1482</td>
			<td>306</td>
			<td>273</td>
			<td>228</td>
			<td>$23,146,129</td>
		</tr>
		<tr>			<td class="num">3.</td>
			<td>Jose L. Ortiz</td>
			<td>1563</td>
			<td>351</td>
			<td>240</td>
			<td>226</td>
			<td>$22,895,822</td>
		</tr>
		<tr class="odd">			<td class="num">4.</td>
			<td>John R. Velazquez</td>
			<td>1019</td>
			<td>195</td>
			<td>138</td>
			<td>134</td>
			<td>$18,538,421</td>
		</tr>
		<tr>			<td class="num">5.</td>
			<td>Florent Geroux</td>
			<td>1105</td>
			<td>214</td>
			<td>148</td>
			<td>147</td>
			<td>$17,570,675</td>
		</tr>
		<tr class="odd">			<td class="num">6.</td>
			<td>Joel Rosario</td>
			<td>970</td>
			<td>156</td>
			<td>139</td>
			<td>161</td>
			<td>$16,438,814</td>
		</tr>
		<tr>			<td class="num">7.</td>
			<td>Julien R. Leparoux</td>
			<td>873</td>
			<td>165</td>
			<td>111</td>
			<td>102</td>
			<td>$13,841,149</td>
		</tr>
		<tr class="odd">			<td class="num">8.</td>
			<td>Mike E. Smith</td>
			<td>333</td>
			<td>54</td>
			<td>47</td>
			<td>44</td>
			<td>$13,574,605</td>
		</tr>
		<tr>			<td class="num">9.</td>
			<td>Luis Saez</td>
			<td>1456</td>
			<td>212</td>
			<td>222</td>
			<td>188</td>
			<td>$13,254,416</td>
		</tr>
		<tr class="odd">			<td class="num">10.</td>
			<td>Manuel Franco</td>
			<td>1463</td>
			<td>190</td>
			<td>195</td>
			<td>189</td>
			<td>$12,316,261</td>
		</tr>
		<tr>			<td class="num">11.</td>
			<td>Flavien Prat</td>
			<td>924</td>
			<td>169</td>
			<td>156</td>
			<td>137</td>
			<td>$12,244,384</td>
		</tr>
		<tr class="odd">			<td class="num">12.</td>
			<td>Rafael Bejarano</td>
			<td>846</td>
			<td>187</td>
			<td>163</td>
			<td>112</td>
			<td>$12,185,866</td>
		</tr>
		<tr>			<td class="num">13.</td>
			<td>Mario Gutierrez</td>
			<td>564</td>
			<td>69</td>
			<td>91</td>
			<td>77</td>
			<td>$9,779,134</td>
		</tr>
		<tr class="odd">			<td class="num">14.</td>
			<td>Paco Lopez</td>
			<td>1104</td>
			<td>262</td>
			<td>185</td>
			<td>160</td>
			<td>$9,609,589</td>
		</tr>
		<tr>			<td class="num">15.</td>
			<td>Kendrick Carmouche</td>
			<td>1116</td>
			<td>143</td>
			<td>154</td>
			<td>148</td>
			<td>$9,456,108</td>
		</tr>
		<tr class="odd">			<td class="num">16.</td>
			<td>Junior Alvarado</td>
			<td>808</td>
			<td>110</td>
			<td>121</td>
			<td>98</td>
			<td>$9,323,499</td>
		</tr>
		<tr>			<td class="num">17.</td>
			<td>Corey J. Lanerie</td>
			<td>1243</td>
			<td>206</td>
			<td>187</td>
			<td>145</td>
			<td>$9,020,542</td>
		</tr>
		<tr class="odd">			<td class="num">18.</td>
			<td>Kent J. Desormeaux</td>
			<td>577</td>
			<td>106</td>
			<td>98</td>
			<td>77</td>
			<td>$8,947,861</td>
		</tr>
		<tr>			<td class="num">19.</td>
			<td>Trevor McCarthy</td>
			<td>1422</td>
			<td>271</td>
			<td>253</td>
			<td>205</td>
			<td>$8,629,854</td>
		</tr>
		<tr class="odd">			<td class="num">20.</td>
			<td>Robby Albarado</td>
			<td>1164</td>
			<td>170</td>
			<td>159</td>
			<td>181</td>
			<td>$8,262,495</td>
		</tr>
		<tr>			<td class="num">21.</td>
			<td>Eurico Rosa Da Silva</td>
			<td>803</td>
			<td>205</td>
			<td>144</td>
			<td>124</td>
			<td>$8,154,594</td>
		</tr>
		<tr class="odd">			<td class="num">22.</td>
			<td>Ricardo Santana, Jr.</td>
			<td>983</td>
			<td>147</td>
			<td>160</td>
			<td>116</td>
			<td>$8,119,231</td>
		</tr>
		<tr>			<td class="num">23.</td>
			<td>Jose Lezcano</td>
			<td>901</td>
			<td>105</td>
			<td>114</td>
			<td>130</td>
			<td>$7,789,581</td>
		</tr>
		<tr class="odd">			<td class="num">24.</td>
			<td>Brian Joseph Hernandez, Jr.</td>
			<td>1007</td>
			<td>155</td>
			<td>166</td>
			<td>121</td>
			<td>$7,743,741</td>
		</tr>
		<tr>			<td class="num">25.</td>
			<td>Antonio A. Gallardo</td>
			<td>1494</td>
			<td>331</td>
			<td>231</td>
			<td>209</td>
			<td>$7,286,714</td>
		</tr>
		<tr class="odd">			<td class="num">26.</td>
			<td>Tyler Baze</td>
			<td>990</td>
			<td>144</td>
			<td>148</td>
			<td>158</td>
			<td>$7,151,575</td>
		</tr>
		<tr>			<td class="num">27.</td>
			<td>Joseph Talamo</td>
			<td>714</td>
			<td>94</td>
			<td>83</td>
			<td>88</td>
			<td>$6,383,537</td>
		</tr>
		<tr class="odd">			<td class="num">28.</td>
			<td>Joe Bravo</td>
			<td>492</td>
			<td>75</td>
			<td>74</td>
			<td>55</td>
			<td>$6,381,109</td>
		</tr>
		<tr>			<td class="num">29.</td>
			<td>Tyler Gaffalione</td>
			<td>1316</td>
			<td>204</td>
			<td>211</td>
			<td>194</td>
			<td>$6,376,450</td>
		</tr>
		<tr class="odd">			<td class="num">30.</td>
			<td>Luis Contreras</td>
			<td>856</td>
			<td>144</td>
			<td>144</td>
			<td>129</td>
			<td>$6,153,582</td>
		</tr>
				</tbody>
		</table>
		<p class="news-date" style="text-align: right;"><em>(Through March 27th , 2017)</em></p>
		