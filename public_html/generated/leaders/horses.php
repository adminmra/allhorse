		<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
		<tbody>
		<tr>
		<td class="header" width="5%" align="center"><strong>#</strong></td>
		<td class="header" width="35%"><strong>Name</strong></td>
		<td class="header" width="10%"><strong>Starts</strong></td>
		<td class="header" width="10%"><strong>1sts</strong></td>
		<td class="header" width="10%"><strong>2nds</strong></td>
		<td class="header" width="10%"><strong>3rds</strong></td>
		<td class="header" width="20%"><strong>Purses</strong></td>
		</tr>
		<tr>			<td class="num">1.</td>
			<td>Arrogate</td>
			<td>6</td>
			<td>5</td>
			<td>0</td>
			<td>1</td>
			<td>$4,084,600</td>
		</tr>
		<tr class="odd">			<td class="num">2.</td>
			<td>Nyquist</td>
			<td>6</td>
			<td>3</td>
			<td>0</td>
			<td>1</td>
			<td>$3,575,600</td>
		</tr>
		<tr>			<td class="num">3.</td>
			<td>Exaggerator</td>
			<td>9</td>
			<td>3</td>
			<td>2</td>
			<td>1</td>
			<td>$2,598,000</td>
		</tr>
		<tr class="odd">			<td class="num">4.</td>
			<td>Songbird</td>
			<td>8</td>
			<td>7</td>
			<td>1</td>
			<td>0</td>
			<td>$2,210,000</td>
		</tr>
		<tr>			<td class="num">5.</td>
			<td>Highland Reel (IRE)</td>
			<td>1</td>
			<td>1</td>
			<td>0</td>
			<td>0</td>
			<td>$2,200,000</td>
		</tr>
		<tr class="odd">			<td class="num">6.</td>
			<td>California Chrome</td>
			<td>5</td>
			<td>4</td>
			<td>1</td>
			<td>0</td>
			<td>$2,040,000</td>
		</tr>
		<tr>			<td class="num">7.</td>
			<td>Flintshire (GB)</td>
			<td>5</td>
			<td>3</td>
			<td>2</td>
			<td>0</td>
			<td>$2,000,000</td>
		</tr>
		<tr class="odd">			<td class="num">8.</td>
			<td>Gun Runner</td>
			<td>9</td>
			<td>4</td>
			<td>2</td>
			<td>2</td>
			<td>$1,970,880</td>
		</tr>
		<tr>			<td class="num">9.</td>
			<td>Beholder</td>
			<td>6</td>
			<td>3</td>
			<td>3</td>
			<td>0</td>
			<td>$1,720,000</td>
		</tr>
		<tr class="odd">			<td class="num">10.</td>
			<td>Tourist</td>
			<td>6</td>
			<td>2</td>
			<td>1</td>
			<td>2</td>
			<td>$1,633,750</td>
		</tr>
				</tbody>
		</table>
		<p class="news-date" style="text-align: right;"><em>(Through November 29th, 2016)</em></p>
		