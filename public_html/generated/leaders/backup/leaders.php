		<div id="leaders-content">

		    <div class="horses">
		      <div class="leaders-header">Horses</div>
		      <div class="leaders-info">
		      <div class="leaders-name">1. Arrogate		      	<a data-toggle="collapse" data-parent="#leaders-content" href="#leadersItem1" class="btn btn-simple collapsed">
		      	<i class="fa fa-angle-down"></i>
		      	</a>
		      </div>
		      <div id="leadersItem1" class="leaders-table collapse">

		          <table width="100%" border="0" cellpadding="0" cellspacing="0">
		            <tbody>
		              <tr class="bold">
		                <td class="width53">Starts</td>
		                <td class="width53">1sts</td>
		                <td class="width53">2nds</td>
		                <td class="width53">3rds</td>
		                <td class="width113">Purses</td>
		              </tr>
		              <tr>
						<td id="j-starts">6</td>
		                <td id="j-1sts">5</td>
		                <td id="j-2nds">0</td>
		                <td id="j-3rds">1</td>
		                <td id="j-purses">$4,084,600</td>
		              </tr>
		            </tbody>
		          </table>
		        </div><!-- /leadersItem2 -->
		      </div><!-- /leaders-info -->
		      <div class="blockfooter">
		       <a href="/leaders/horses" title="Top Ten Horses" class="btn btn-primary">Top Ten Horses <i class="fa fa-angle-right"></i></a>
			   </div>

		    </div><!-- /horses -->

		    <div class="jockeys">
		      <div class="leaders-header">Jockeys</div>
		      <div class="leaders-info">
		      <div class="leaders-name">1. Javier Castellano <a  data-toggle="collapse" data-parent="#leaders-content" href="#leadersItem2" class="btn btn-simple collapsed"><i class="fa fa-angle-down"></i></a></div>
		      <div id="leadersItem2" class="leaders-table collapse">

		          <table width="100%" border="0" cellpadding="0" cellspacing="0">
		            <tbody>
		              <tr class="bold">
		                <td class="width53">Starts</td>
		                <td class="width53">1sts</td>
		                <td class="width53">2nds</td>
		                <td class="width53">3rds</td>
		                <td class="width113">Purses</td>
		              </tr>
		              <tr>
		                <td id="j-starts">1352</td>
		                <td id="j-1sts">287</td>
		                <td id="j-2nds">222</td>
		                <td id="j-3rds">216</td>
		                <td id="j-purses">$26,116,902</td>
		              </tr>
		            </tbody>
		          </table>
		        </div><!-- /leadersItem1 -->
		      </div><!-- /leaders-info -->
		     <div class="blockfooter">
		     <a href="/leaders/jockeys" title="Top Ten Jockeys" class="btn btn-primary">Top Ten Jockeys <i class="fa fa-angle-right"></i></a>
		     </div>

		    </div><!-- /jockeys -->



		    <div class="trainers">
		      <div class="leaders-header">Trainers</div>
		      <div class="leaders-info">
		      <div class="leaders-name">1. Chad C. Brown <a  data-toggle="collapse" data-parent="#leaders-content" href="#leadersItem3" class="btn btn-simple collapsed"><i class="fa fa-angle-down"></i></a></div>
		      <div id="leadersItem3" class="leaders-table collapse">

		          <table width="100%" border="0" cellpadding="0" cellspacing="0">
		            <tbody>
		              <tr class="bold">
		                <td class="width53">Starts</td>
		                <td class="width53">1sts</td>
		                <td class="width53">2nds</td>
		                <td class="width53">3rds</td>
		                <td class="width113">Purses</td>
		              </tr>
		              <tr>
						<td id="j-starts">725</td>
		                <td id="j-1sts">176</td>
		                <td id="j-2nds">138</td>
		                <td id="j-3rds">103</td>
		                <td id="j-purses">$22,565,325</td>
		              </tr>
		            </tbody>
		          </table>
		        </div><!-- /leadersItem3 -->
		      </div><!-- /leaders-info -->
		      <div class="blockfooter">
		      <a href="/leaders/trainers" title="Top Ten Trainers" class="btn btn-primary">Top Ten Trainers <i class="fa fa-angle-right"></i></a>
		      </div>

		    </div><!-- /trainers -->

		</div><!-- /leaders-content -->

		