

		<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
		<tbody>
		<tr>
		<td class="header" width="5%" align="center"><strong>#</strong></td>
		<td class="header" width="35%"><strong>Name</strong></td>
		<td class="header" width="10%"><strong>Starts</strong></td>
		<td class="header" width="10%"><strong>1sts</strong></td>
		<td class="header" width="10%"><strong>2nds</strong></td>
		<td class="header" width="10%"><strong>3rds</strong></td>
		<td class="header" width="20%"><strong>Purses</strong></td>
		</tr>
		<tr>			<td class="num">1.</td>
			<td>Nyquist</td>
			<td>4</td>
			<td>3</td>
			<td>0</td>
			<td>1</td>
			<td>$3,505,600</td>
		</tr>
		<tr class="odd">			<td class="num">2.</td>
			<td>Exaggerator</td>
			<td>6</td>
			<td>2</td>
			<td>2</td>
			<td>1</td>
			<td>$1,988,000</td>
		</tr>
		<tr>			<td class="num">3.</td>
			<td>Creator</td>
			<td>6</td>
			<td>3</td>
			<td>1</td>
			<td>1</td>
			<td>$1,540,000</td>
		</tr>
		<tr class="odd">			<td class="num">4.</td>
			<td>Gun Runner</td>
			<td>3</td>
			<td>2</td>
			<td>0</td>
			<td>1</td>
			<td>$1,040,000</td>
		</tr>
		<tr>			<td class="num">5.</td>
			<td>Cathryn Sophia</td>
			<td>5</td>
			<td>3</td>
			<td>0</td>
			<td>2</td>
			<td>$929,720</td>
		</tr>
		<tr class="odd">			<td class="num">6.</td>
			<td>Stanford</td>
			<td>4</td>
			<td>1</td>
			<td>2</td>
			<td>0</td>
			<td>$844,400</td>
		</tr>
		<tr>			<td class="num">7.</td>
			<td>Melatonin</td>
			<td>3</td>
			<td>2</td>
			<td>1</td>
			<td>0</td>
			<td>$786,000</td>
		</tr>
		<tr class="odd">			<td class="num">8.</td>
			<td>Forever Unbridled</td>
			<td>3</td>
			<td>2</td>
			<td>1</td>
			<td>0</td>
			<td>$785,000</td>
		</tr>
		<tr>			<td class="num">9.</td>
			<td>Cavorting</td>
			<td>3</td>
			<td>2</td>
			<td>1</td>
			<td>0</td>
			<td>$725,000</td>
		</tr>
		<tr class="odd">			<td class="num">10.</td>
			<td>Outwork</td>
			<td>4</td>
			<td>2</td>
			<td>1</td>
			<td>0</td>
			<td>$671,800</td>
		</tr>
		<tr>			<td class="num">11.</td>
			<td>Frosted</td>
			<td>1</td>
			<td>1</td>
			<td>0</td>
			<td>0</td>
			<td>$670,000</td>
		</tr>
		<tr class="odd">			<td class="num">12.</td>
			<td>Brody's Cause</td>
			<td>4</td>
			<td>1</td>
			<td>0</td>
			<td>0</td>
			<td>$645,000</td>
		</tr>
		<tr>			<td class="num">13.</td>
			<td>Destin</td>
			<td>5</td>
			<td>2</td>
			<td>1</td>
			<td>0</td>
			<td>$618,000</td>
		</tr>
		<tr class="odd">			<td class="num">14.</td>
			<td>Mohaymen</td>
			<td>4</td>
			<td>2</td>
			<td>0</td>
			<td>0</td>
			<td>$607,850</td>
		</tr>
		<tr>			<td class="num">15.</td>
			<td>Tepin</td>
			<td>4</td>
			<td>4</td>
			<td>0</td>
			<td>0</td>
			<td>$602,280</td>
		</tr>
		<tr class="odd">			<td class="num">16.</td>
			<td>Cupid</td>
			<td>5</td>
			<td>2</td>
			<td>1</td>
			<td>0</td>
			<td>$592,157</td>
		</tr>
		<tr>			<td class="num">17.</td>
			<td>Suddenbreakingnews</td>
			<td>5</td>
			<td>1</td>
			<td>1</td>
			<td>0</td>
			<td>$587,000</td>
		</tr>
		<tr class="odd">			<td class="num">18.</td>
			<td>Effinex</td>
			<td>2</td>
			<td>1</td>
			<td>0</td>
			<td>1</td>
			<td>$570,000</td>
		</tr>
		<tr>			<td class="num">19.</td>
			<td>Celestine</td>
			<td>3</td>
			<td>2</td>
			<td>0</td>
			<td>1</td>
			<td>$566,680</td>
		</tr>
		<tr class="odd">			<td class="num">20.</td>
			<td>Catch a Glimpse</td>
			<td>4</td>
			<td>4</td>
			<td>0</td>
			<td>0</td>
			<td>$535,700</td>
		</tr>
		<tr>			<td class="num">21.</td>
			<td>Flintshire (GB)</td>
			<td>1</td>
			<td>1</td>
			<td>0</td>
			<td>0</td>
			<td>$535,000</td>
		</tr>
		<tr class="odd">			<td class="num">22.</td>
			<td>Carina Mia</td>
			<td>3</td>
			<td>2</td>
			<td>0</td>
			<td>0</td>
			<td>$520,280</td>
		</tr>
		<tr>			<td class="num">23.</td>
			<td>Tom's Ready</td>
			<td>5</td>
			<td>1</td>
			<td>2</td>
			<td>0</td>
			<td>$519,000</td>
		</tr>
		<tr class="odd">			<td class="num">24.</td>
			<td>Land Over Sea</td>
			<td>5</td>
			<td>1</td>
			<td>3</td>
			<td>0</td>
			<td>$508,500</td>
		</tr>
		<tr>			<td class="num">25.</td>
			<td>Tara's Tango</td>
			<td>4</td>
			<td>2</td>
			<td>0</td>
			<td>2</td>
			<td>$504,000</td>
		</tr>
		<tr class="odd">			<td class="num">26.</td>
			<td>Cherry Wine</td>
			<td>5</td>
			<td>1</td>
			<td>1</td>
			<td>1</td>
			<td>$502,800</td>
		</tr>
		<tr>			<td class="num">27.</td>
			<td>Songbird</td>
			<td>3</td>
			<td>3</td>
			<td>0</td>
			<td>0</td>
			<td>$480,000</td>
		</tr>
		<tr class="odd">			<td class="num">28.</td>
			<td>Salutos Amigos</td>
			<td>6</td>
			<td>2</td>
			<td>2</td>
			<td>0</td>
			<td>$448,500</td>
		</tr>
		<tr>			<td class="num">29.</td>
			<td>Da Big Hoss</td>
			<td>4</td>
			<td>3</td>
			<td>0</td>
			<td>0</td>
			<td>$440,880</td>
		</tr>
		<tr class="odd">			<td class="num">30.</td>
			<td>What a View</td>
			<td>3</td>
			<td>3</td>
			<td>0</td>
			<td>0</td>
			<td>$437,500</td>
		</tr>
				</tbody>
		</table>
		<p class="news-date" style="text-align: right;"><em>(Through June 16th , 2016)</em></p>
		