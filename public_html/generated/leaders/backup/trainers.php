		<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
		<tbody>
		<tr>
		<td class="header" width="5%" align="center"><strong>#</strong></td>
		<td class="header" width="35%"><strong>Name</strong></td>
		<td class="header" width="10%"><strong>Starts</strong></td>
		<td class="header" width="10%"><strong>1sts</strong></td>
		<td class="header" width="10%"><strong>2nds</strong></td>
		<td class="header" width="10%"><strong>3rds</strong></td>
		<td class="header" width="20%"><strong>Purses</strong></td>
		</tr>
		<tr>			<td class="num">1.</td>
			<td>Chad C. Brown</td>
			<td>725</td>
			<td>176</td>
			<td>138</td>
			<td>103</td>
			<td>$22,565,325</td>
		</tr>
		<tr class="odd">			<td class="num">2.</td>
			<td>Todd A. Pletcher</td>
			<td>1</td>
			<td>247</td>
			<td>173</td>
			<td>158</td>
			<td>$20,031,767</td>
		</tr>
		<tr>			<td class="num">3.</td>
			<td>Mark E. Casse</td>
			<td>1</td>
			<td>224</td>
			<td>189</td>
			<td>177</td>
			<td>$17,496,885</td>
		</tr>
		<tr class="odd">			<td class="num">4.</td>
			<td>Bob Baffert</td>
			<td>378</td>
			<td>102</td>
			<td>75</td>
			<td>44</td>
			<td>$15,227,193</td>
		</tr>
		<tr>			<td class="num">5.</td>
			<td>Steven M. Asmussen</td>
			<td>1</td>
			<td>294</td>
			<td>241</td>
			<td>192</td>
			<td>$14,231,714</td>
		</tr>
		<tr class="odd">			<td class="num">6.</td>
			<td>Michael J. Maker</td>
			<td>1</td>
			<td>198</td>
			<td>149</td>
			<td>126</td>
			<td>$10,892,533</td>
		</tr>
		<tr>			<td class="num">7.</td>
			<td>Jerry Hollendorfer</td>
			<td>525</td>
			<td>94</td>
			<td>69</td>
			<td>79</td>
			<td>$4,224,579</td>
		</tr>
		<tr class="odd">			<td class="num">8.</td>
			<td>William I. Mott</td>
			<td>643</td>
			<td>83</td>
			<td>91</td>
			<td>82</td>
			<td>$9,379,643</td>
		</tr>
		<tr>			<td class="num">9.</td>
			<td>Kiaran P. McLaughlin</td>
			<td>364</td>
			<td>65</td>
			<td>63</td>
			<td>57</td>
			<td>$9,069,576</td>
		</tr>
		<tr class="odd">			<td class="num">10.</td>
			<td>Jerry Hollendorfer</td>
			<td>986</td>
			<td>174</td>
			<td>131</td>
			<td>139</td>
			<td>$8,983,310</td>
		</tr>
				</tbody>
		</table>
		<p class="news-date" style="text-align: right;"><em>(Through November 29th, 2016)</em></p>
		