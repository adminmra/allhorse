
				<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
					<tbody>
						<tr>
							<td class="header" width="5%" align="center"><strong>#</strong></td>
							<td class="header" width="35%"><strong>Name</strong></td>
							<td class="header" width="10%"><strong>Starts</strong></td>
							<td class="header" width="10%"><strong>1sts</strong></td>
							<td class="header" width="10%"><strong>2nds</strong></td>
							<td class="header" width="10%"><strong>3rds</strong></td>
							<td class="header" width="20%"><strong>Purses</strong></td>
						</tr>
						
							<tr class=''>
								<td class="num">1.</td>
								<td>Arrogate</td>
								<td>3</td>
								<td>1</td>
								<td>1</td>
								<td>0</td>
								<td>$7,218,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">2.</td>
								<td>Shaman Ghost</td>
								<td>4</td>
								<td>2</td>
								<td>2</td>
								<td>0</td>
								<td>$2,520,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">3.</td>
								<td>Always Dreaming</td>
								<td>7</td>
								<td>4</td>
								<td>0</td>
								<td>1</td>
								<td>$2,320,600</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">4.</td>
								<td>Gun Runner</td>
								<td>4</td>
								<td>4</td>
								<td>0</td>
								<td>0</td>
								<td>$1,650,700</td>
							</tr>
							
							<tr class=''>
								<td class="num">5.</td>
								<td>Irap</td>
								<td>9</td>
								<td>3</td>
								<td>3</td>
								<td>1</td>
								<td>$1,600,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">6.</td>
								<td>Girvin</td>
								<td>8</td>
								<td>3</td>
								<td>2</td>
								<td>0</td>
								<td>$1,561,792</td>
							</tr>
							
							<tr class=''>
								<td class="num">7.</td>
								<td>West Coast</td>
								<td>8</td>
								<td>6</td>
								<td>2</td>
								<td>0</td>
								<td>$1,543,800</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">8.</td>
								<td>Abel Tasman</td>
								<td>6</td>
								<td>3</td>
								<td>3</td>
								<td>0</td>
								<td>$1,407,200</td>
							</tr>
							
							<tr class=''>
								<td class="num">9.</td>
								<td>Tapwrit</td>
								<td>6</td>
								<td>2</td>
								<td>1</td>
								<td>0</td>
								<td>$1,165,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">10.</td>
								<td>Neolithic</td>
								<td>3</td>
								<td>1</td>
								<td>0</td>
								<td>2</td>
								<td>$1,129,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">11.</td>
								<td>Cloud Computing</td>
								<td>6</td>
								<td>2</td>
								<td>1</td>
								<td>1</td>
								<td>$1,114,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">12.</td>
								<td>It Tiz Well</td>
								<td>8</td>
								<td>4</td>
								<td>2</td>
								<td>1</td>
								<td>$1,097,600</td>
							</tr>
							
							<tr class=''>
								<td class="num">13.</td>
								<td>Beach Patrol</td>
								<td>6</td>
								<td>2</td>
								<td>1</td>
								<td>1</td>
								<td>$1,069,975</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">14.</td>
								<td>Oscar Performance</td>
								<td>6</td>
								<td>3</td>
								<td>0</td>
								<td>1</td>
								<td>$1,067,500</td>
							</tr>
							
							<tr class=''>
								<td class="num">15.</td>
								<td>Classic Empire</td>
								<td>4</td>
								<td>1</td>
								<td>1</td>
								<td>1</td>
								<td>$1,034,300</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">16.</td>
								<td>Mor Spirit</td>
								<td>4</td>
								<td>3</td>
								<td>1</td>
								<td>0</td>
								<td>$982,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">17.</td>
								<td>Songbird</td>
								<td>3</td>
								<td>2</td>
								<td>1</td>
								<td>0</td>
								<td>$980,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">18.</td>
								<td>Imperative</td>
								<td>3</td>
								<td>2</td>
								<td>0</td>
								<td>0</td>
								<td>$970,425</td>
							</tr>
							
							<tr class=''>
								<td class="num">19.</td>
								<td>Irish War Cry</td>
								<td>7</td>
								<td>2</td>
								<td>1</td>
								<td>0</td>
								<td>$966,660</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">20.</td>
								<td>World Approval</td>
								<td>5</td>
								<td>4</td>
								<td>0</td>
								<td>0</td>
								<td>$943,600</td>
							</tr>
							
							<tr class=''>
								<td class="num">21.</td>
								<td>Practical Joke</td>
								<td>6</td>
								<td>2</td>
								<td>2</td>
								<td>1</td>
								<td>$931,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">22.</td>
								<td>Sadler's Joy</td>
								<td>7</td>
								<td>2</td>
								<td>1</td>
								<td>3</td>
								<td>$882,480</td>
							</tr>
							
							<tr class=''>
								<td class="num">23.</td>
								<td>Keen Ice</td>
								<td>3</td>
								<td>1</td>
								<td>1</td>
								<td>0</td>
								<td>$870,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">24.</td>
								<td>Collected</td>
								<td>4</td>
								<td>4</td>
								<td>0</td>
								<td>0</td>
								<td>$826,800</td>
							</tr>
							
							<tr class=''>
								<td class="num">25.</td>
								<td>Gormley</td>
								<td>6</td>
								<td>2</td>
								<td>0</td>
								<td>0</td>
								<td>$790,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">26.</td>
								<td>Stellar Wind</td>
								<td>3</td>
								<td>3</td>
								<td>0</td>
								<td>0</td>
								<td>$780,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">27.</td>
								<td>Lady Eli</td>
								<td>4</td>
								<td>3</td>
								<td>1</td>
								<td>0</td>
								<td>$770,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">28.</td>
								<td>Lookin At Lee</td>
								<td>8</td>
								<td>0</td>
								<td>1</td>
								<td>3</td>
								<td>$753,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">29.</td>
								<td>Holy Helena</td>
								<td>5</td>
								<td>3</td>
								<td>1</td>
								<td>0</td>
								<td>$746,530</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">30.</td>
								<td>Gunnevera</td>
								<td>7</td>
								<td>2</td>
								<td>2</td>
								<td>1</td>
								<td>$736,200</td>
							</tr>
												</tbody>
				</table>
				<p class='news-date' style='text-align: right;'><em class='updateemp'>(Through October 6th, 2017 08:00:38)</em></p>
				