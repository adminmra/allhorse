<div id="no-more-tables">
				<table id="infoEntries" class="table table-condensed table-striped table-bordered ordenableResult" border="0" cellspacing="0" cellpadding="10" >
                <thead>
                    <tr>
                        <th><strong>#</strong></th>
                        <th><strong>Name</strong></th>
                        <th><strong>Starts</strong></th>
                        <th><strong>1sts</strong></th>
                        <th><strong>2nds</strong></th>
                        <th><strong>3rds</strong></th>
                        <th><strong>Purses</strong></th>
                    </tr>
                </thead>
					<tbody><tr>
		<td class="num" data-title="Rank">1.</td>
		<td data-title="Name">Joel Rosario</td>
		<td data-title="Starts">1,016</td>
		<td data-title="1sts">214</td>
		<td data-title="2nds">149</td>
		<td data-title="3rds">144</td>
		<td data-title="Purses">$31,378,963</td>
	</tr><tr>
		<td class="num" data-title="Rank">2.</td>
		<td data-title="Name">Irad Ortiz, Jr.</td>
		<td data-title="Starts">1,367</td>
		<td data-title="1sts">314</td>
		<td data-title="2nds">266</td>
		<td data-title="3rds">194</td>
		<td data-title="Purses">$27,560,153</td>
	</tr><tr>
		<td class="num" data-title="Rank">3.</td>
		<td data-title="Name">Luis Saez</td>
		<td data-title="Starts">1,447</td>
		<td data-title="1sts">246</td>
		<td data-title="2nds">228</td>
		<td data-title="3rds">193</td>
		<td data-title="Purses">$23,726,490</td>
	</tr><tr>
		<td class="num" data-title="Rank">4.</td>
		<td data-title="Name">Jose L. Ortiz</td>
		<td data-title="Starts">1,243</td>
		<td data-title="1sts">231</td>
		<td data-title="2nds">205</td>
		<td data-title="3rds">190</td>
		<td data-title="Purses">$22,113,165</td>
	</tr><tr>
		<td class="num" data-title="Rank">5.</td>
		<td data-title="Name">Flavien Prat</td>
		<td data-title="Starts">859</td>
		<td data-title="1sts">233</td>
		<td data-title="2nds">189</td>
		<td data-title="3rds">126</td>
		<td data-title="Purses">$22,016,853</td>
	</tr><tr>
		<td class="num" data-title="Rank">6.</td>
		<td data-title="Name">Florent Geroux</td>
		<td data-title="Starts">779</td>
		<td data-title="1sts">161</td>
		<td data-title="2nds">117</td>
		<td data-title="3rds">100</td>
		<td data-title="Purses">$17,147,405</td>
	</tr><tr>
		<td class="num" data-title="Rank">7.</td>
		<td data-title="Name">Tyler Gaffalione</td>
		<td data-title="Starts">1,254</td>
		<td data-title="1sts">232</td>
		<td data-title="2nds">171</td>
		<td data-title="3rds">161</td>
		<td data-title="Purses">$16,925,561</td>
	</tr><tr>
		<td class="num" data-title="Rank">8.</td>
		<td data-title="Name">John R. Velazquez</td>
		<td data-title="Starts">625</td>
		<td data-title="1sts">101</td>
		<td data-title="2nds">78</td>
		<td data-title="3rds">87</td>
		<td data-title="Purses">$16,286,799</td>
	</tr><tr>
		<td class="num" data-title="Rank">9.</td>
		<td data-title="Name">Ricardo Santana, Jr.</td>
		<td data-title="Starts">911</td>
		<td data-title="1sts">151</td>
		<td data-title="2nds">112</td>
		<td data-title="3rds">125</td>
		<td data-title="Purses">$16,205,998</td>
	</tr><tr>
		<td class="num" data-title="Rank">10.</td>
		<td data-title="Name">Manuel Franco</td>
		<td data-title="Starts">1,200</td>
		<td data-title="1sts">165</td>
		<td data-title="2nds">181</td>
		<td data-title="3rds">166</td>
		<td data-title="Purses">$12,671,962</td>
	</tr><tr>
		<td class="num" data-title="Rank">11.</td>
		<td data-title="Name">Paco Lopez</td>
		<td data-title="Starts">1,238</td>
		<td data-title="1sts">292</td>
		<td data-title="2nds">226</td>
		<td data-title="3rds">186</td>
		<td data-title="Purses">$11,087,264</td>
	</tr><tr>
		<td class="num" data-title="Rank">12.</td>
		<td data-title="Name">Brian Joseph Hernandez, Jr.</td>
		<td data-title="Starts">857</td>
		<td data-title="1sts">142</td>
		<td data-title="2nds">128</td>
		<td data-title="3rds">121</td>
		<td data-title="Purses">$10,524,385</td>
	</tr><tr>
		<td class="num" data-title="Rank">13.</td>
		<td data-title="Name">Juan J. Hernandez</td>
		<td data-title="Starts">837</td>
		<td data-title="1sts">160</td>
		<td data-title="2nds">123</td>
		<td data-title="3rds">137</td>
		<td data-title="Purses">$9,724,122</td>
	</tr><tr>
		<td class="num" data-title="Rank">14.</td>
		<td data-title="Name">Umberto Rispoli</td>
		<td data-title="Starts">623</td>
		<td data-title="1sts">123</td>
		<td data-title="2nds">132</td>
		<td data-title="3rds">99</td>
		<td data-title="Purses">$9,347,679</td>
	</tr><tr>
		<td class="num" data-title="Rank">15.</td>
		<td data-title="Name">Eric Cancel</td>
		<td data-title="Starts">960</td>
		<td data-title="1sts">137</td>
		<td data-title="2nds">142</td>
		<td data-title="3rds">145</td>
		<td data-title="Purses">$8,997,529</td>
	</tr><tr>
		<td class="num" data-title="Rank">16.</td>
		<td data-title="Name">Javier Castellano</td>
		<td data-title="Starts">667</td>
		<td data-title="1sts">91</td>
		<td data-title="2nds">89</td>
		<td data-title="3rds">92</td>
		<td data-title="Purses">$8,874,098</td>
	</tr><tr>
		<td class="num" data-title="Rank">17.</td>
		<td data-title="Name">Junior Alvarado</td>
		<td data-title="Starts">747</td>
		<td data-title="1sts">92</td>
		<td data-title="2nds">133</td>
		<td data-title="3rds">102</td>
		<td data-title="Purses">$8,140,519</td>
	</tr><tr>
		<td class="num" data-title="Rank">18.</td>
		<td data-title="Name">Kendrick Carmouche</td>
		<td data-title="Starts">771</td>
		<td data-title="1sts">130</td>
		<td data-title="2nds">114</td>
		<td data-title="3rds">110</td>
		<td data-title="Purses">$8,100,390</td>
	</tr><tr>
		<td class="num" data-title="Rank">19.</td>
		<td data-title="Name">Dylan Davis</td>
		<td data-title="Starts">778</td>
		<td data-title="1sts">108</td>
		<td data-title="2nds">101</td>
		<td data-title="3rds">117</td>
		<td data-title="Purses">$7,832,137</td>
	</tr><tr>
		<td class="num" data-title="Rank">20.</td>
		<td data-title="Name">Jose Lezcano</td>
		<td data-title="Starts">669</td>
		<td data-title="1sts">93</td>
		<td data-title="2nds">85</td>
		<td data-title="3rds">102</td>
		<td data-title="Purses">$7,829,001</td>
	</tr><tr>
		<td class="num" data-title="Rank">21.</td>
		<td data-title="Name">Edgard J. Zayas</td>
		<td data-title="Starts">1,269</td>
		<td data-title="1sts">205</td>
		<td data-title="2nds">224</td>
		<td data-title="3rds">180</td>
		<td data-title="Purses">$7,696,963</td>
	</tr><tr>
		<td class="num" data-title="Rank">22.</td>
		<td data-title="Name">Adam Beschizza</td>
		<td data-title="Starts">875</td>
		<td data-title="1sts">134</td>
		<td data-title="2nds">130</td>
		<td data-title="3rds">104</td>
		<td data-title="Purses">$7,454,878</td>
	</tr><tr>
		<td class="num" data-title="Rank">23.</td>
		<td data-title="Name">James Graham</td>
		<td data-title="Starts">912</td>
		<td data-title="1sts">130</td>
		<td data-title="2nds">134</td>
		<td data-title="3rds">102</td>
		<td data-title="Purses">$7,417,854</td>
	</tr><tr>
		<td class="num" data-title="Rank">24.</td>
		<td data-title="Name">David Cabrera</td>
		<td data-title="Starts">995</td>
		<td data-title="1sts">190</td>
		<td data-title="2nds">180</td>
		<td data-title="3rds">136</td>
		<td data-title="Purses">$6,969,623</td>
	</tr><tr>
		<td class="num" data-title="Rank">25.</td>
		<td data-title="Name">Abel Cedillo</td>
		<td data-title="Starts">878</td>
		<td data-title="1sts">129</td>
		<td data-title="2nds">154</td>
		<td data-title="3rds">130</td>
		<td data-title="Purses">$6,811,692</td>
	</tr><tr>
		<td class="num" data-title="Rank">26.</td>
		<td data-title="Name">Julien R. Leparoux</td>
		<td data-title="Starts">530</td>
		<td data-title="1sts">60</td>
		<td data-title="2nds">63</td>
		<td data-title="3rds">68</td>
		<td data-title="Purses">$6,004,809</td>
	</tr><tr>
		<td class="num" data-title="Rank">27.</td>
		<td data-title="Name">Joseph Talamo</td>
		<td data-title="Starts">517</td>
		<td data-title="1sts">57</td>
		<td data-title="2nds">59</td>
		<td data-title="3rds">79</td>
		<td data-title="Purses">$5,733,868</td>
	</tr><tr>
		<td class="num" data-title="Rank">28.</td>
		<td data-title="Name">Emisael Jaramillo</td>
		<td data-title="Starts">920</td>
		<td data-title="1sts">173</td>
		<td data-title="2nds">131</td>
		<td data-title="3rds">108</td>
		<td data-title="Purses">$5,685,009</td>
	</tr><tr>
		<td class="num" data-title="Rank">29.</td>
		<td data-title="Name">Corey J. Lanerie</td>
		<td data-title="Starts">774</td>
		<td data-title="1sts">84</td>
		<td data-title="2nds">83</td>
		<td data-title="3rds">101</td>
		<td data-title="Purses">$5,562,045</td>
	</tr><tr>
		<td class="num" data-title="Rank">30.</td>
		<td data-title="Name">Ramon A. Vazquez</td>
		<td data-title="Starts">638</td>
		<td data-title="1sts">118</td>
		<td data-title="2nds">106</td>
		<td data-title="3rds">79</td>
		<td data-title="Purses">$5,550,296</td>
	</tr>					</tbody>
				</table> </div>
        <table id="infoEntries" width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" >
        <tbody>
        <tr>
            <td class="dateUpdated center" colspan="4">
                <em id="updateemp">Updated November 16th, 2021.</em>
            </td>
        </tr>
        </tbody>
        </table>
                {literal}       
    <style type="text/css">
        table.ordenableResult th{cursor: pointer}
    </style>
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_results.js"></script>
        {/literal}