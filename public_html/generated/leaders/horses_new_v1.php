<table id="infoEntries" class="data table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" >
					<tbody>
						<tr>
							<th width="5%" align="center"><strong>#</strong></th>
							<th width="35%"><strong>Name</strong></th>
							<th width="10%"><strong>Starts</strong></th>
							<th width="10%"><strong>1sts</strong></th>
							<th width="10%"><strong>2nds</strong></th>
							<th width="10%"><strong>3rds</strong></th>
							<th width="20%"><strong>Purses</strong></th>
						</tr><tr class"">
		<td class="num">1.</td>
		<td>Knicks Go</td>
		<td>6</td>
		<td>5</td>
		<td>0</td>
		<td>0</td>
		<td>$5,824,140</td>
	</tr><tr class"odd">
		<td class="num">2.</td>
		<td>Medina Spirit</td>
		<td>9</td>
		<td>4</td>
		<td>4</td>
		<td>1</td>
		<td>$3,520,000</td>
	</tr><tr class"">
		<td class="num">3.</td>
		<td>Essential Quality</td>
		<td>7</td>
		<td>5</td>
		<td>0</td>
		<td>1</td>
		<td>$3,420,000</td>
	</tr><tr class"odd">
		<td class="num">4.</td>
		<td>Yibir (GB)</td>
		<td>2</td>
		<td>2</td>
		<td>0</td>
		<td>0</td>
		<td>$2,615,000</td>
	</tr><tr class"">
		<td class="num">5.</td>
		<td>Hot Rod Charlie</td>
		<td>7</td>
		<td>2</td>
		<td>1</td>
		<td>2</td>
		<td>$2,087,500</td>
	</tr><tr class"odd">
		<td class="num">6.</td>
		<td>Letruska</td>
		<td>8</td>
		<td>6</td>
		<td>1</td>
		<td>0</td>
		<td>$1,945,540</td>
	</tr><tr class"">
		<td class="num">7.</td>
		<td>Silver State</td>
		<td>7</td>
		<td>4</td>
		<td>1</td>
		<td>1</td>
		<td>$1,693,000</td>
	</tr><tr class"odd">
		<td class="num">8.</td>
		<td>Malathaat</td>
		<td>5</td>
		<td>3</td>
		<td>1</td>
		<td>1</td>
		<td>$1,563,000</td>
	</tr><tr class"">
		<td class="num">9.</td>
		<td>Mandaloun</td>
		<td>6</td>
		<td>3</td>
		<td>1</td>
		<td>1</td>
		<td>$1,560,000</td>
	</tr><tr class"odd">
		<td class="num">10.</td>
		<td>Echo Zulu</td>
		<td>4</td>
		<td>4</td>
		<td>0</td>
		<td>0</td>
		<td>$1,480,000</td>
	</tr><tr class"">
		<td class="num">11.</td>
		<td>Aloha West</td>
		<td>9</td>
		<td>5</td>
		<td>2</td>
		<td>0</td>
		<td>$1,311,068</td>
	</tr><tr class"odd">
		<td class="num">12.</td>
		<td>Corniche</td>
		<td>3</td>
		<td>3</td>
		<td>0</td>
		<td>0</td>
		<td>$1,262,000</td>
	</tr><tr class"">
		<td class="num">13.</td>
		<td>Clairiere</td>
		<td>8</td>
		<td>2</td>
		<td>2</td>
		<td>2</td>
		<td>$1,203,500</td>
	</tr><tr class"odd">
		<td class="num">14.</td>
		<td>Colonel Liam</td>
		<td>4</td>
		<td>3</td>
		<td>0</td>
		<td>0</td>
		<td>$1,137,600</td>
	</tr><tr class"">
		<td class="num">15.</td>
		<td>Maxfield</td>
		<td>6</td>
		<td>3</td>
		<td>2</td>
		<td>1</td>
		<td>$1,061,640</td>
	</tr><tr class"odd">
		<td class="num">16.</td>
		<td>Jackie's Warrior</td>
		<td>7</td>
		<td>4</td>
		<td>1</td>
		<td>1</td>
		<td>$1,051,400</td>
	</tr><tr class"">
		<td class="num">17.</td>
		<td>Loves Only You (JPN)</td>
		<td>1</td>
		<td>1</td>
		<td>0</td>
		<td>0</td>
		<td>$1,040,000</td>
	</tr><tr class"odd">
		<td class="num">17.</td>
		<td>Marche Lorraine (JPN)</td>
		<td>1</td>
		<td>1</td>
		<td>0</td>
		<td>0</td>
		<td>$1,040,000</td>
	</tr><tr class"">
		<td class="num">17.</td>
		<td>Space Blues (IRE)</td>
		<td>1</td>
		<td>1</td>
		<td>0</td>
		<td>0</td>
		<td>$1,040,000</td>
	</tr><tr class"odd">
		<td class="num">20.</td>
		<td>Shedaresthedevil</td>
		<td>6</td>
		<td>4</td>
		<td>0</td>
		<td>1</td>
		<td>$1,037,940</td>
	</tr><tr class"">
		<td class="num">21.</td>
		<td>Life Is Good</td>
		<td>5</td>
		<td>4</td>
		<td>1</td>
		<td>0</td>
		<td>$1,025,000</td>
	</tr><tr class"odd">
		<td class="num">22.</td>
		<td>Midnight Bourbon</td>
		<td>8</td>
		<td>1</td>
		<td>4</td>
		<td>1</td>
		<td>$995,500</td>
	</tr><tr class"">
		<td class="num">23.</td>
		<td>Art Collector</td>
		<td>5</td>
		<td>3</td>
		<td>0</td>
		<td>0</td>
		<td>$917,925</td>
	</tr><tr class"odd">
		<td class="num">24.</td>
		<td>Domestic Spending (GB)</td>
		<td>3</td>
		<td>2</td>
		<td>1</td>
		<td>0</td>
		<td>$911,200</td>
	</tr><tr class"">
		<td class="num">25.</td>
		<td>Rombauer</td>
		<td>4</td>
		<td>2</td>
		<td>0</td>
		<td>2</td>
		<td>$890,000</td>
	</tr><tr class"odd">
		<td class="num">26.</td>
		<td>Ce Ce</td>
		<td>6</td>
		<td>4</td>
		<td>0</td>
		<td>1</td>
		<td>$888,000</td>
	</tr><tr class"">
		<td class="num">27.</td>
		<td>Search Results</td>
		<td>6</td>
		<td>4</td>
		<td>1</td>
		<td>1</td>
		<td>$864,000</td>
	</tr><tr class"odd">
		<td class="num">28.</td>
		<td>Concert Tour</td>
		<td>5</td>
		<td>3</td>
		<td>0</td>
		<td>1</td>
		<td>$856,600</td>
	</tr><tr class"">
		<td class="num">29.</td>
		<td>Gamine</td>
		<td>5</td>
		<td>4</td>
		<td>0</td>
		<td>1</td>
		<td>$851,900</td>
	</tr><tr class"odd">
		<td class="num">30.</td>
		<td>Super Stock</td>
		<td>7</td>
		<td>2</td>
		<td>0</td>
		<td>1</td>
		<td>$846,915</td>
	</tr></tbody>
	</table>
	<p class="news-date" style="text-align: right;"><em class="updateemp">(Through November 16th, 2021)</em></p>