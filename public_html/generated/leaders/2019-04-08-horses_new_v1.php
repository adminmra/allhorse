

				<table id="infoEntries" class="data table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" >
					<tbody>
						<tr>
							<th width="5%" align="center"><strong>#</strong></th>
							<th width="35%"><strong>Name</strong></th>
							<th width="10%"><strong>Starts</strong></th>
							<th width="10%"><strong>1sts</strong></th>
							<th width="10%"><strong>2nds</strong></th>
							<th width="10%"><strong>3rds</strong></th>
							<th width="20%"><strong>Purses</strong></th>
						</tr>
						
							<tr class=''>
								<td class="num">1.</td>
								<td>City of Light</td>
								<td>1</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>$4,000,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">2.</td>
								<td>Bricks and Mortar</td>
								<td>2</td>
								<td>2</td>
								<td>0</td>
								<td>0</td>
								<td>$2,836,250</td>
							</tr>
							
							<tr class=''>
								<td class="num">3.</td>
								<td>Seeking the Soul</td>
								<td>1</td>
								<td>0</td>
								<td>1</td>
								<td>0</td>
								<td>$1,250,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">4.</td>
								<td>Accelerate</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>1</td>
								<td>$900,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">5.</td>
								<td>Magic Wand (IRE)</td>
								<td>1</td>
								<td>0</td>
								<td>1</td>
								<td>0</td>
								<td>$796,875</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">6.</td>
								<td>Bravazo</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$700,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">7.</td>
								<td>Maximum Security</td>
								<td>3</td>
								<td>3</td>
								<td>0</td>
								<td>0</td>
								<td>$638,600</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">8.</td>
								<td>Vekoma</td>
								<td>2</td>
								<td>1</td>
								<td>0</td>
								<td>1</td>
								<td>$637,600</td>
							</tr>
							
							<tr class=''>
								<td class="num">9.</td>
								<td>Roadster</td>
								<td>2</td>
								<td>2</td>
								<td>0</td>
								<td>0</td>
								<td>$634,200</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">10.</td>
								<td>By My Standards</td>
								<td>3</td>
								<td>2</td>
								<td>0</td>
								<td>1</td>
								<td>$630,310</td>
							</tr>
							
							<tr class=''>
								<td class="num">11.</td>
								<td>Tacitus</td>
								<td>2</td>
								<td>2</td>
								<td>0</td>
								<td>0</td>
								<td>$610,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">12.</td>
								<td>Delta Prince</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>1</td>
								<td>$575,521</td>
							</tr>
							
							<tr class=''>
								<td class="num">13.</td>
								<td>Catapult</td>
								<td>2</td>
								<td>0</td>
								<td>1</td>
								<td>0</td>
								<td>$566,975</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">14.</td>
								<td>Audible</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$550,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">15.</td>
								<td>Long Range Toddy</td>
								<td>3</td>
								<td>1</td>
								<td>1</td>
								<td>1</td>
								<td>$530,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">16.</td>
								<td>Omaha Beach</td>
								<td>3</td>
								<td>2</td>
								<td>1</td>
								<td>0</td>
								<td>$494,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">17.</td>
								<td>Bellafina</td>
								<td>3</td>
								<td>3</td>
								<td>0</td>
								<td>0</td>
								<td>$480,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">18.</td>
								<td>Cutting Humor</td>
								<td>3</td>
								<td>1</td>
								<td>1</td>
								<td>0</td>
								<td>$471,267</td>
							</tr>
							
							<tr class=''>
								<td class="num">19.</td>
								<td>Next Shares</td>
								<td>3</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>$470,351</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">20.</td>
								<td>Channel Maker</td>
								<td>2</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$452,108</td>
							</tr>
							
							<tr class=''>
								<td class="num">21.</td>
								<td>Midnight Bisou</td>
								<td>2</td>
								<td>2</td>
								<td>0</td>
								<td>0</td>
								<td>$390,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">22.</td>
								<td>War of Will</td>
								<td>3</td>
								<td>2</td>
								<td>0</td>
								<td>0</td>
								<td>$370,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">23.</td>
								<td>Gift Box</td>
								<td>1</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>$355,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">24.</td>
								<td>Dubby Dubbie</td>
								<td>3</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$350,890</td>
							</tr>
							
							<tr class=''>
								<td class="num">25.</td>
								<td>Fahan Mura</td>
								<td>2</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$350,351</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">26.</td>
								<td>Game Winner</td>
								<td>2</td>
								<td>0</td>
								<td>2</td>
								<td>0</td>
								<td>$350,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">27.</td>
								<td>Yoshida (JPN)</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$350,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">28.</td>
								<td>Aerolithe (JPN)</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$350,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">29.</td>
								<td>Code of Honor</td>
								<td>3</td>
								<td>1</td>
								<td>0</td>
								<td>1</td>
								<td>$332,070</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">30.</td>
								<td>Out for a Spin</td>
								<td>3</td>
								<td>2</td>
								<td>0</td>
								<td>1</td>
								<td>$330,150</td>
							</tr>
												</tbody>
				</table>
				<p class='news-date' style='text-align: right;'><em class='updateemp'>(Through April 8th, 2019)</em></p>
				