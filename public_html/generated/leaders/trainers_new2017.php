				<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
					<tbody>
						<tr>
							<td class="header" width="5%" align="center"><strong>#</strong></td>
							<td class="header" width="35%"><strong>Name</strong></td>
							<td class="header" width="10%"><strong>Starts</strong></td>
							<td class="header" width="10%"><strong>1sts</strong></td>
							<td class="header" width="10%"><strong>2nds</strong></td>
							<td class="header" width="10%"><strong>3rds</strong></td>
							<td class="header" width="20%"><strong>Purses</strong></td>
						</tr>
						
							<tr class=''>
								<td class="num">1.</td>
								<td>Chad C. Brown</td>
								<td>820</td>
								<td>213</td>
								<td>162</td>
								<td>128</td>
								<td>$26,202,164</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">2.</td>
								<td>Todd A. Pletcher</td>
								<td>1,053</td>
								<td>253</td>
								<td>170</td>
								<td>142</td>
								<td>$22,444,161</td>
							</tr>
							
							<tr class=''>
								<td class="num">3.</td>
								<td>Bob Baffert</td>
								<td>313</td>
								<td>83</td>
								<td>57</td>
								<td>38</td>
								<td>$21,112,912</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">4.</td>
								<td>Steven M. Asmussen</td>
								<td>1,909</td>
								<td>380</td>
								<td>327</td>
								<td>267</td>
								<td>$19,650,969</td>
							</tr>
							
							<tr class=''>
								<td class="num">5.</td>
								<td>Mark E. Casse</td>
								<td>1,323</td>
								<td>233</td>
								<td>208</td>
								<td>174</td>
								<td>$17,420,200</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">6.</td>
								<td>Michael J. Maker</td>
								<td>1,109</td>
								<td>216</td>
								<td>172</td>
								<td>141</td>
								<td>$10,988,867</td>
							</tr>
							
							<tr class=''>
								<td class="num">7.</td>
								<td>Jerry Hollendorfer</td>
								<td>971</td>
								<td>174</td>
								<td>158</td>
								<td>150</td>
								<td>$9,542,402</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">8.</td>
								<td>William I. Mott</td>
								<td>706</td>
								<td>104</td>
								<td>110</td>
								<td>77</td>
								<td>$9,238,920</td>
							</tr>
							
							<tr class=''>
								<td class="num">9.</td>
								<td>Brad H. Cox</td>
								<td>779</td>
								<td>204</td>
								<td>136</td>
								<td>105</td>
								<td>$8,833,028</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">10.</td>
								<td>Doug F. O'Neill</td>
								<td>652</td>
								<td>90</td>
								<td>96</td>
								<td>83</td>
								<td>$7,966,843</td>
							</tr>
							
							<tr class=''>
								<td class="num">11.</td>
								<td>H. Graham Motion</td>
								<td>609</td>
								<td>96</td>
								<td>96</td>
								<td>90</td>
								<td>$7,964,658</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">12.</td>
								<td>Peter Miller</td>
								<td>583</td>
								<td>124</td>
								<td>90</td>
								<td>80</td>
								<td>$7,380,416</td>
							</tr>
							
							<tr class=''>
								<td class="num">13.</td>
								<td>Linda Rice</td>
								<td>664</td>
								<td>166</td>
								<td>106</td>
								<td>94</td>
								<td>$7,103,014</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">14.</td>
								<td>Christophe Clement</td>
								<td>504</td>
								<td>94</td>
								<td>89</td>
								<td>79</td>
								<td>$6,952,265</td>
							</tr>
							
							<tr class=''>
								<td class="num">15.</td>
								<td>Rudy R. Rodriguez</td>
								<td>781</td>
								<td>147</td>
								<td>138</td>
								<td>112</td>
								<td>$6,798,719</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">16.</td>
								<td>Karl Broberg</td>
								<td>1,846</td>
								<td>412</td>
								<td>302</td>
								<td>275</td>
								<td>$6,282,578</td>
							</tr>
							
							<tr class=''>
								<td class="num">17.</td>
								<td>Kiaran P. McLaughlin</td>
								<td>432</td>
								<td>73</td>
								<td>64</td>
								<td>55</td>
								<td>$6,147,524</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">18.</td>
								<td>Jorge Navarro</td>
								<td>492</td>
								<td>149</td>
								<td>69</td>
								<td>78</td>
								<td>$6,044,134</td>
							</tr>
							
							<tr class=''>
								<td class="num">19.</td>
								<td>Philip D'Amato</td>
								<td>510</td>
								<td>90</td>
								<td>92</td>
								<td>51</td>
								<td>$5,970,084</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">20.</td>
								<td>James A. Jerkens</td>
								<td>189</td>
								<td>34</td>
								<td>37</td>
								<td>29</td>
								<td>$5,572,571</td>
							</tr>
							
							<tr class=''>
								<td class="num">21.</td>
								<td>Robertino Diodoro</td>
								<td>970</td>
								<td>220</td>
								<td>162</td>
								<td>126</td>
								<td>$5,209,676</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">22.</td>
								<td>Richard Baltas</td>
								<td>511</td>
								<td>87</td>
								<td>79</td>
								<td>67</td>
								<td>$4,977,119</td>
							</tr>
							
							<tr class=''>
								<td class="num">23.</td>
								<td>Jason Servis</td>
								<td>391</td>
								<td>112</td>
								<td>64</td>
								<td>60</td>
								<td>$4,967,662</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">24.</td>
								<td>Joe Sharp</td>
								<td>517</td>
								<td>99</td>
								<td>76</td>
								<td>64</td>
								<td>$4,554,157</td>
							</tr>
							
							<tr class=''>
								<td class="num">25.</td>
								<td>Kenneth G. McPeek</td>
								<td>470</td>
								<td>54</td>
								<td>56</td>
								<td>82</td>
								<td>$4,447,522</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">26.</td>
								<td>Ian R. Wilkes</td>
								<td>465</td>
								<td>58</td>
								<td>70</td>
								<td>72</td>
								<td>$4,409,257</td>
							</tr>
							
							<tr class=''>
								<td class="num">27.</td>
								<td>Jeremiah C. Englehart</td>
								<td>774</td>
								<td>199</td>
								<td>116</td>
								<td>114</td>
								<td>$4,366,448</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">28.</td>
								<td>Wesley A. Ward</td>
								<td>498</td>
								<td>108</td>
								<td>83</td>
								<td>64</td>
								<td>$4,147,609</td>
							</tr>
							
							<tr class=''>
								<td class="num">29.</td>
								<td>Claude R. McGaughey III</td>
								<td>341</td>
								<td>54</td>
								<td>55</td>
								<td>56</td>
								<td>$4,105,453</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">30.</td>
								<td>Brian A. Lynch</td>
								<td>265</td>
								<td>43</td>
								<td>43</td>
								<td>41</td>
								<td>$4,048,617</td>
							</tr>
												</tbody>
				</table>
				<p class='news-date' style='text-align: right;'><em class='updateemp'>(Through January 25th, 2018 15:35:08)</em></p>

				