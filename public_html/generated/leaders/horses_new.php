

				<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
					<tbody>
						<tr>
							<td class="header" width="5%" align="center"><strong>#</strong></td>
							<td class="header" width="35%"><strong>Name</strong></td>
							<td class="header" width="10%"><strong>Starts</strong></td>
							<td class="header" width="10%"><strong>1sts</strong></td>
							<td class="header" width="10%"><strong>2nds</strong></td>
							<td class="header" width="10%"><strong>3rds</strong></td>
							<td class="header" width="20%"><strong>Purses</strong></td>
						</tr>
						
							<tr class=''>
								<td class="num">1.</td>
								<td>Gun Runner</td>
								<td>1</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>$7,000,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">2.</td>
								<td>Justify</td>
								<td>6</td>
								<td>6</td>
								<td>0</td>
								<td>0</td>
								<td>$3,798,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">3.</td>
								<td>West Coast</td>
								<td>1</td>
								<td>0</td>
								<td>1</td>
								<td>0</td>
								<td>$1,600,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">4.</td>
								<td>Monomoy Girl</td>
								<td>4</td>
								<td>4</td>
								<td>0</td>
								<td>0</td>
								<td>$1,359,200</td>
							</tr>
							
							<tr class=''>
								<td class="num">5.</td>
								<td>Gunnevera</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>1</td>
								<td>$1,300,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">6.</td>
								<td>Fear the Cowboy</td>
								<td>4</td>
								<td>0</td>
								<td>0</td>
								<td>3</td>
								<td>$1,203,500</td>
							</tr>
							
							<tr class=''>
								<td class="num">7.</td>
								<td>Magnum Moon</td>
								<td>5</td>
								<td>4</td>
								<td>0</td>
								<td>0</td>
								<td>$1,177,800</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">8.</td>
								<td>Good Magic</td>
								<td>4</td>
								<td>1</td>
								<td>1</td>
								<td>1</td>
								<td>$1,128,400</td>
							</tr>
							
							<tr class=''>
								<td class="num">9.</td>
								<td>War Story</td>
								<td>4</td>
								<td>1</td>
								<td>2</td>
								<td>0</td>
								<td>$1,017,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">10.</td>
								<td>Audible</td>
								<td>3</td>
								<td>2</td>
								<td>0</td>
								<td>1</td>
								<td>$1,003,520</td>
							</tr>
							
							<tr class=''>
								<td class="num">11.</td>
								<td>Accelerate</td>
								<td>4</td>
								<td>3</td>
								<td>1</td>
								<td>0</td>
								<td>$925,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">12.</td>
								<td>Something Awesome</td>
								<td>5</td>
								<td>3</td>
								<td>0</td>
								<td>1</td>
								<td>$901,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">13.</td>
								<td>Bee Jersey</td>
								<td>4</td>
								<td>4</td>
								<td>0</td>
								<td>0</td>
								<td>$861,400</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">14.</td>
								<td>Seeking the Soul</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$850,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">15.</td>
								<td>City of Light</td>
								<td>3</td>
								<td>2</td>
								<td>0</td>
								<td>1</td>
								<td>$750,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">16.</td>
								<td>Giant Expectations</td>
								<td>2</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$686,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">17.</td>
								<td>Vino Rosso</td>
								<td>5</td>
								<td>1</td>
								<td>0</td>
								<td>1</td>
								<td>$672,500</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">18.</td>
								<td>Noble Indy</td>
								<td>5</td>
								<td>2</td>
								<td>0</td>
								<td>1</td>
								<td>$666,400</td>
							</tr>
							
							<tr class=''>
								<td class="num">19.</td>
								<td>Hi Happy (ARG)</td>
								<td>4</td>
								<td>2</td>
								<td>0</td>
								<td>2</td>
								<td>$652,900</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">20.</td>
								<td>Singing Bullet</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$650,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">21.</td>
								<td>Sharp Azteca</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$650,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">22.</td>
								<td>Collected</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$650,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">23.</td>
								<td>Stellar Wind</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$650,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">24.</td>
								<td>Toast of New York</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
								<td>$650,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">25.</td>
								<td>Bravazo</td>
								<td>6</td>
								<td>2</td>
								<td>1</td>
								<td>0</td>
								<td>$640,600</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">26.</td>
								<td>My Boy Jack</td>
								<td>5</td>
								<td>2</td>
								<td>0</td>
								<td>2</td>
								<td>$592,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">27.</td>
								<td>Spring Quality</td>
								<td>2</td>
								<td>1</td>
								<td>1</td>
								<td>0</td>
								<td>$565,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">28.</td>
								<td>Core Beliefs</td>
								<td>6</td>
								<td>2</td>
								<td>1</td>
								<td>3</td>
								<td>$535,360</td>
							</tr>
							
							<tr class=''>
								<td class="num">29.</td>
								<td>Fourstar Crook</td>
								<td>3</td>
								<td>2</td>
								<td>1</td>
								<td>0</td>
								<td>$520,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">30.</td>
								<td>Midnight Bisou</td>
								<td>4</td>
								<td>3</td>
								<td>0</td>
								<td>1</td>
								<td>$511,000</td>
							</tr>
												</tbody>
				</table>
				<p class='news-date' style='text-align: right;'><em class='updateemp'>(Through April 8th, 2019 19:24:11)</em></p>
				