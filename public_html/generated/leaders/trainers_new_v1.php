<table id="infoEntries" class="data table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" >
					<tbody>
						<tr>
							<th width="5%" align="center"><strong>#</strong></th>
							<th width="35%"><strong>Name</strong></th>
							<th width="10%"><strong>Starts</strong></th>
							<th width="10%"><strong>1sts</strong></th>
							<th width="10%"><strong>2nds</strong></th>
							<th width="10%"><strong>3rds</strong></th>
							<th width="20%"><strong>Earnings</strong></th>
						</tr><tr class"">
		<td class="num">1.</td>
		<td>Brad H. Cox</td>
		<td>889</td>
		<td>235</td>
		<td>161</td>
		<td>128</td>
		<td>$29,509,700</td>
	</tr><tr class"odd">
		<td class="num">2.</td>
		<td>Steven M. Asmussen</td>
		<td>2,167</td>
		<td>414</td>
		<td>321</td>
		<td>329</td>
		<td>$28,563,553</td>
	</tr><tr class"">
		<td class="num">3.</td>
		<td>Chad C. Brown</td>
		<td>779</td>
		<td>183</td>
		<td>147</td>
		<td>119</td>
		<td>$20,864,967</td>
	</tr><tr class"odd">
		<td class="num">4.</td>
		<td>Todd A. Pletcher</td>
		<td>822</td>
		<td>173</td>
		<td>140</td>
		<td>110</td>
		<td>$19,619,222</td>
	</tr><tr class"">
		<td class="num">5.</td>
		<td>Mark E. Casse</td>
		<td>1,206</td>
		<td>201</td>
		<td>169</td>
		<td>146</td>
		<td>$13,690,369</td>
	</tr><tr class"odd">
		<td class="num">6.</td>
		<td>Michael J. Maker</td>
		<td>1,195</td>
		<td>195</td>
		<td>196</td>
		<td>173</td>
		<td>$13,509,972</td>
	</tr><tr class"">
		<td class="num">7.</td>
		<td>Bob Baffert</td>
		<td>302</td>
		<td>93</td>
		<td>60</td>
		<td>43</td>
		<td>$13,493,482</td>
	</tr><tr class"odd">
		<td class="num">8.</td>
		<td>William I. Mott</td>
		<td>536</td>
		<td>97</td>
		<td>86</td>
		<td>75</td>
		<td>$11,035,735</td>
	</tr><tr class"">
		<td class="num">9.</td>
		<td>Christophe Clement</td>
		<td>499</td>
		<td>98</td>
		<td>84</td>
		<td>71</td>
		<td>$8,893,342</td>
	</tr><tr class"odd">
		<td class="num">10.</td>
		<td>Karl Broberg</td>
		<td>1,743</td>
		<td>351</td>
		<td>269</td>
		<td>259</td>
		<td>$7,811,225</td>
	</tr><tr class"">
		<td class="num">11.</td>
		<td>Wesley A. Ward</td>
		<td>412</td>
		<td>120</td>
		<td>59</td>
		<td>55</td>
		<td>$7,666,435</td>
	</tr><tr class"odd">
		<td class="num">12.</td>
		<td>Saffie A. Joseph, Jr.</td>
		<td>761</td>
		<td>172</td>
		<td>127</td>
		<td>103</td>
		<td>$7,652,090</td>
	</tr><tr class"">
		<td class="num">13.</td>
		<td>Jamie Ness</td>
		<td>983</td>
		<td>270</td>
		<td>201</td>
		<td>137</td>
		<td>$7,237,052</td>
	</tr><tr class"odd">
		<td class="num">14.</td>
		<td>Peter Miller</td>
		<td>590</td>
		<td>108</td>
		<td>93</td>
		<td>82</td>
		<td>$6,735,497</td>
	</tr><tr class"">
		<td class="num">15.</td>
		<td>Philip D'Amato</td>
		<td>445</td>
		<td>89</td>
		<td>75</td>
		<td>63</td>
		<td>$6,663,860</td>
	</tr><tr class"odd">
		<td class="num">16.</td>
		<td>Brendan P. Walsh</td>
		<td>415</td>
		<td>69</td>
		<td>54</td>
		<td>62</td>
		<td>$6,411,815</td>
	</tr><tr class"">
		<td class="num">17.</td>
		<td>Robertino Diodoro</td>
		<td>798</td>
		<td>196</td>
		<td>156</td>
		<td>93</td>
		<td>$6,373,392</td>
	</tr><tr class"odd">
		<td class="num">18.</td>
		<td>Doug F. O'Neill</td>
		<td>595</td>
		<td>85</td>
		<td>81</td>
		<td>94</td>
		<td>$6,219,045</td>
	</tr><tr class"">
		<td class="num">19.</td>
		<td>Michael W. McCarthy</td>
		<td>331</td>
		<td>53</td>
		<td>43</td>
		<td>62</td>
		<td>$6,160,487</td>
	</tr><tr class"odd">
		<td class="num">20.</td>
		<td>John W. Sadler</td>
		<td>388</td>
		<td>78</td>
		<td>50</td>
		<td>58</td>
		<td>$5,827,095</td>
	</tr><tr class"">
		<td class="num">21.</td>
		<td>Charles Appleby</td>
		<td>18</td>
		<td>9</td>
		<td>3</td>
		<td>1</td>
		<td>$5,818,966</td>
	</tr><tr class"odd">
		<td class="num">22.</td>
		<td>Kenneth G. McPeek</td>
		<td>369</td>
		<td>53</td>
		<td>45</td>
		<td>49</td>
		<td>$5,771,405</td>
	</tr><tr class"">
		<td class="num">23.</td>
		<td>W. Bret Calhoun</td>
		<td>564</td>
		<td>100</td>
		<td>102</td>
		<td>75</td>
		<td>$5,604,165</td>
	</tr><tr class"odd">
		<td class="num">24.</td>
		<td>Rudy R. Rodriguez</td>
		<td>492</td>
		<td>85</td>
		<td>69</td>
		<td>57</td>
		<td>$4,942,459</td>
	</tr><tr class"">
		<td class="num">25.</td>
		<td>Claudio A. Gonzalez</td>
		<td>723</td>
		<td>143</td>
		<td>123</td>
		<td>108</td>
		<td>$4,739,795</td>
	</tr><tr class"odd">
		<td class="num">26.</td>
		<td>Richard Baltas</td>
		<td>371</td>
		<td>56</td>
		<td>46</td>
		<td>48</td>
		<td>$4,686,603</td>
	</tr><tr class"">
		<td class="num">27.</td>
		<td>Claude R. McGaughey III</td>
		<td>308</td>
		<td>55</td>
		<td>34</td>
		<td>45</td>
		<td>$4,661,646</td>
	</tr><tr class"odd">
		<td class="num">28.</td>
		<td>Michael J. Trombetta</td>
		<td>629</td>
		<td>96</td>
		<td>105</td>
		<td>99</td>
		<td>$4,624,727</td>
	</tr><tr class"">
		<td class="num">29.</td>
		<td>Michael Stidham</td>
		<td>409</td>
		<td>60</td>
		<td>74</td>
		<td>58</td>
		<td>$4,482,276</td>
	</tr><tr class"odd">
		<td class="num">30.</td>
		<td>Kelly J. Breen</td>
		<td>488</td>
		<td>74</td>
		<td>75</td>
		<td>71</td>
		<td>$4,465,538</td>
	</tr></tbody>
	</table>
	<p class="news-date" style="text-align: right;"><em class="updateemp">(Through November 16th, 2021)</em></p>