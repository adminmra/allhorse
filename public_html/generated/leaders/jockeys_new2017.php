
				<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
					<tbody>
						<tr>
							<td class="header" width="5%" align="center"><strong>#</strong></td>
							<td class="header" width="35%"><strong>Name</strong></td>
							<td class="header" width="10%"><strong>Starts</strong></td>
							<td class="header" width="10%"><strong>1sts</strong></td>
							<td class="header" width="10%"><strong>2nds</strong></td>
							<td class="header" width="10%"><strong>3rds</strong></td>
							<td class="header" width="20%"><strong>Purses</strong></td>
						</tr>
						
							<tr class=''>
								<td class="num">1.</td>
								<td>Jose L. Ortiz</td>
								<td>1408</td>
								<td>270</td>
								<td>259</td>
								<td>211</td>
								<td>$27,318,875</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">2.</td>
								<td>Javier Castellano</td>
								<td>1270</td>
								<td>223</td>
								<td>234</td>
								<td>186</td>
								<td>$25,099,317</td>
							</tr>
							
							<tr class=''>
								<td class="num">3.</td>
								<td>Irad Ortiz, Jr.</td>
								<td>1492</td>
								<td>317</td>
								<td>285</td>
								<td>227</td>
								<td>$22,925,086</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">4.</td>
								<td>John R. Velazquez</td>
								<td>946</td>
								<td>206</td>
								<td>128</td>
								<td>127</td>
								<td>$20,780,733</td>
							</tr>
							
							<tr class=''>
								<td class="num">5.</td>
								<td>Mike E. Smith</td>
								<td>275</td>
								<td>70</td>
								<td>38</td>
								<td>45</td>
								<td>$20,540,871</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">6.</td>
								<td>Joel Rosario</td>
								<td>1010</td>
								<td>170</td>
								<td>161</td>
								<td>170</td>
								<td>$17,975,708</td>
							</tr>
							
							<tr class=''>
								<td class="num">7.</td>
								<td>Florent Geroux</td>
								<td>997</td>
								<td>177</td>
								<td>133</td>
								<td>143</td>
								<td>$14,418,901</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">8.</td>
								<td>Luis Saez</td>
								<td>1449</td>
								<td>236</td>
								<td>206</td>
								<td>197</td>
								<td>$13,403,109</td>
							</tr>
							
							<tr class=''>
								<td class="num">9.</td>
								<td>Julien R. Leparoux</td>
								<td>800</td>
								<td>136</td>
								<td>106</td>
								<td>102</td>
								<td>$13,390,756</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">10.</td>
								<td>Manuel Franco</td>
								<td>1327</td>
								<td>187</td>
								<td>175</td>
								<td>188</td>
								<td>$12,681,817</td>
							</tr>
							
							<tr class=''>
								<td class="num">11.</td>
								<td>Flavien Prat</td>
								<td>835</td>
								<td>164</td>
								<td>134</td>
								<td>145</td>
								<td>$12,608,813</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">12.</td>
								<td>Tyler Gaffalione</td>
								<td>1448</td>
								<td>294</td>
								<td>236</td>
								<td>216</td>
								<td>$11,088,517</td>
							</tr>
							
							<tr class=''>
								<td class="num">13.</td>
								<td>Luis Contreras</td>
								<td>1093</td>
								<td>221</td>
								<td>164</td>
								<td>145</td>
								<td>$10,391,155</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">14.</td>
								<td>Paco Lopez</td>
								<td>1051</td>
								<td>190</td>
								<td>162</td>
								<td>135</td>
								<td>$9,606,435</td>
							</tr>
							
							<tr class=''>
								<td class="num">15.</td>
								<td>Corey J. Lanerie</td>
								<td>1297</td>
								<td>231</td>
								<td>185</td>
								<td>207</td>
								<td>$9,447,710</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">16.</td>
								<td>Ricardo Santana, Jr.</td>
								<td>960</td>
								<td>150</td>
								<td>153</td>
								<td>104</td>
								<td>$8,923,191</td>
							</tr>
							
							<tr class=''>
								<td class="num">17.</td>
								<td>Eurico Rosa Da Silva</td>
								<td>826</td>
								<td>203</td>
								<td>186</td>
								<td>121</td>
								<td>$8,528,236</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">18.</td>
								<td>Brian Joseph Hernandez, Jr.</td>
								<td>912</td>
								<td>133</td>
								<td>147</td>
								<td>131</td>
								<td>$8,317,228</td>
							</tr>
							
							<tr class=''>
								<td class="num">19.</td>
								<td>Kendrick Carmouche</td>
								<td>928</td>
								<td>136</td>
								<td>111</td>
								<td>104</td>
								<td>$7,786,022</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">20.</td>
								<td>Jose Lezcano</td>
								<td>857</td>
								<td>113</td>
								<td>104</td>
								<td>95</td>
								<td>$7,474,218</td>
							</tr>
							
							<tr class=''>
								<td class="num">21.</td>
								<td>Robby Albarado</td>
								<td>912</td>
								<td>118</td>
								<td>129</td>
								<td>133</td>
								<td>$7,394,733</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">22.</td>
								<td>Kent J. Desormeaux</td>
								<td>542</td>
								<td>109</td>
								<td>87</td>
								<td>58</td>
								<td>$7,348,961</td>
							</tr>
							
							<tr class=''>
								<td class="num">23.</td>
								<td>Tyler Baze</td>
								<td>977</td>
								<td>130</td>
								<td>160</td>
								<td>134</td>
								<td>$7,338,754</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">24.</td>
								<td>Rafael Bejarano</td>
								<td>674</td>
								<td>125</td>
								<td>111</td>
								<td>102</td>
								<td>$7,280,393</td>
							</tr>
							
							<tr class=''>
								<td class="num">25.</td>
								<td>Edgard J. Zayas</td>
								<td>1247</td>
								<td>236</td>
								<td>193</td>
								<td>161</td>
								<td>$7,215,196</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">26.</td>
								<td>Rajiv Maragh</td>
								<td>828</td>
								<td>97</td>
								<td>95</td>
								<td>101</td>
								<td>$6,919,020</td>
							</tr>
							
							<tr class=''>
								<td class="num">27.</td>
								<td>Ramon A. Vazquez</td>
								<td>1123</td>
								<td>260</td>
								<td>193</td>
								<td>167</td>
								<td>$6,566,948</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">28.</td>
								<td>Nik Juarez</td>
								<td>1144</td>
								<td>188</td>
								<td>154</td>
								<td>150</td>
								<td>$6,470,219</td>
							</tr>
							
							<tr class=''>
								<td class="num">29.</td>
								<td>Emisael Jaramillo</td>
								<td>1150</td>
								<td>233</td>
								<td>203</td>
								<td>114</td>
								<td>$5,992,670</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">30.</td>
								<td>Rafael Manuel Hernandez</td>
								<td>936</td>
								<td>166</td>
								<td>129</td>
								<td>118</td>
								<td>$5,775,807</td>
							</tr>
												</tbody>
				</table>
				<p class='news-date' style='text-align: right;'><em class='updateemp'>(Through January 25th, 2018 15:35:06)</em></p>
				