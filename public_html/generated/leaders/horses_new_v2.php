<div id="no-more-tables">
				<table id="infoEntries" class="table table-condensed table-striped table-bordered ordenableResult" border="0" cellspacing="0" cellpadding="10" >
                <thead>
                    <tr>
                        <th><strong>#</strong></th>
                        <th><strong>Name</strong></th>
                        <th><strong>Starts</strong></th>
                        <th><strong>1sts</strong></th>
                        <th><strong>2nds</strong></th>
                        <th><strong>3rds</strong></th>
                        <th><strong>Purses</strong></th>
                    </tr>
                </thead>
					<tbody><tr>
		<td class="num" data-title="Rank">1.</td>
		<td data-title="Name">Knicks Go</td>
		<td data-title="Starts">6</td>
		<td data-title="1sts">5</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">0</td>
		<td data-title="Purses">$5,824,140</td>
	</tr><tr>
		<td class="num" data-title="Rank">2.</td>
		<td data-title="Name">Medina Spirit</td>
		<td data-title="Starts">9</td>
		<td data-title="1sts">4</td>
		<td data-title="2nds">4</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$3,520,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">3.</td>
		<td data-title="Name">Essential Quality</td>
		<td data-title="Starts">7</td>
		<td data-title="1sts">5</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$3,420,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">4.</td>
		<td data-title="Name">Yibir (GB)</td>
		<td data-title="Starts">2</td>
		<td data-title="1sts">2</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">0</td>
		<td data-title="Purses">$2,615,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">5.</td>
		<td data-title="Name">Hot Rod Charlie</td>
		<td data-title="Starts">7</td>
		<td data-title="1sts">2</td>
		<td data-title="2nds">1</td>
		<td data-title="3rds">2</td>
		<td data-title="Purses">$2,087,500</td>
	</tr><tr>
		<td class="num" data-title="Rank">6.</td>
		<td data-title="Name">Letruska</td>
		<td data-title="Starts">8</td>
		<td data-title="1sts">6</td>
		<td data-title="2nds">1</td>
		<td data-title="3rds">0</td>
		<td data-title="Purses">$1,945,540</td>
	</tr><tr>
		<td class="num" data-title="Rank">7.</td>
		<td data-title="Name">Silver State</td>
		<td data-title="Starts">7</td>
		<td data-title="1sts">4</td>
		<td data-title="2nds">1</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$1,693,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">8.</td>
		<td data-title="Name">Malathaat</td>
		<td data-title="Starts">5</td>
		<td data-title="1sts">3</td>
		<td data-title="2nds">1</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$1,563,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">9.</td>
		<td data-title="Name">Mandaloun</td>
		<td data-title="Starts">6</td>
		<td data-title="1sts">3</td>
		<td data-title="2nds">1</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$1,560,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">10.</td>
		<td data-title="Name">Echo Zulu</td>
		<td data-title="Starts">4</td>
		<td data-title="1sts">4</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">0</td>
		<td data-title="Purses">$1,480,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">11.</td>
		<td data-title="Name">Aloha West</td>
		<td data-title="Starts">9</td>
		<td data-title="1sts">5</td>
		<td data-title="2nds">2</td>
		<td data-title="3rds">0</td>
		<td data-title="Purses">$1,311,068</td>
	</tr><tr>
		<td class="num" data-title="Rank">12.</td>
		<td data-title="Name">Corniche</td>
		<td data-title="Starts">3</td>
		<td data-title="1sts">3</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">0</td>
		<td data-title="Purses">$1,262,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">13.</td>
		<td data-title="Name">Clairiere</td>
		<td data-title="Starts">8</td>
		<td data-title="1sts">2</td>
		<td data-title="2nds">2</td>
		<td data-title="3rds">2</td>
		<td data-title="Purses">$1,203,500</td>
	</tr><tr>
		<td class="num" data-title="Rank">14.</td>
		<td data-title="Name">Colonel Liam</td>
		<td data-title="Starts">4</td>
		<td data-title="1sts">3</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">0</td>
		<td data-title="Purses">$1,137,600</td>
	</tr><tr>
		<td class="num" data-title="Rank">15.</td>
		<td data-title="Name">Maxfield</td>
		<td data-title="Starts">6</td>
		<td data-title="1sts">3</td>
		<td data-title="2nds">2</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$1,061,640</td>
	</tr><tr>
		<td class="num" data-title="Rank">16.</td>
		<td data-title="Name">Jackie's Warrior</td>
		<td data-title="Starts">7</td>
		<td data-title="1sts">4</td>
		<td data-title="2nds">1</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$1,051,400</td>
	</tr><tr>
		<td class="num" data-title="Rank">17.</td>
		<td data-title="Name">Loves Only You (JPN)</td>
		<td data-title="Starts">1</td>
		<td data-title="1sts">1</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">0</td>
		<td data-title="Purses">$1,040,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">17.</td>
		<td data-title="Name">Marche Lorraine (JPN)</td>
		<td data-title="Starts">1</td>
		<td data-title="1sts">1</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">0</td>
		<td data-title="Purses">$1,040,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">17.</td>
		<td data-title="Name">Space Blues (IRE)</td>
		<td data-title="Starts">1</td>
		<td data-title="1sts">1</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">0</td>
		<td data-title="Purses">$1,040,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">20.</td>
		<td data-title="Name">Shedaresthedevil</td>
		<td data-title="Starts">6</td>
		<td data-title="1sts">4</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$1,037,940</td>
	</tr><tr>
		<td class="num" data-title="Rank">21.</td>
		<td data-title="Name">Life Is Good</td>
		<td data-title="Starts">5</td>
		<td data-title="1sts">4</td>
		<td data-title="2nds">1</td>
		<td data-title="3rds">0</td>
		<td data-title="Purses">$1,025,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">22.</td>
		<td data-title="Name">Midnight Bourbon</td>
		<td data-title="Starts">8</td>
		<td data-title="1sts">1</td>
		<td data-title="2nds">4</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$995,500</td>
	</tr><tr>
		<td class="num" data-title="Rank">23.</td>
		<td data-title="Name">Art Collector</td>
		<td data-title="Starts">5</td>
		<td data-title="1sts">3</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">0</td>
		<td data-title="Purses">$917,925</td>
	</tr><tr>
		<td class="num" data-title="Rank">24.</td>
		<td data-title="Name">Domestic Spending (GB)</td>
		<td data-title="Starts">3</td>
		<td data-title="1sts">2</td>
		<td data-title="2nds">1</td>
		<td data-title="3rds">0</td>
		<td data-title="Purses">$911,200</td>
	</tr><tr>
		<td class="num" data-title="Rank">25.</td>
		<td data-title="Name">Rombauer</td>
		<td data-title="Starts">4</td>
		<td data-title="1sts">2</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">2</td>
		<td data-title="Purses">$890,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">26.</td>
		<td data-title="Name">Ce Ce</td>
		<td data-title="Starts">6</td>
		<td data-title="1sts">4</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$888,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">27.</td>
		<td data-title="Name">Search Results</td>
		<td data-title="Starts">6</td>
		<td data-title="1sts">4</td>
		<td data-title="2nds">1</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$864,000</td>
	</tr><tr>
		<td class="num" data-title="Rank">28.</td>
		<td data-title="Name">Concert Tour</td>
		<td data-title="Starts">5</td>
		<td data-title="1sts">3</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$856,600</td>
	</tr><tr>
		<td class="num" data-title="Rank">29.</td>
		<td data-title="Name">Gamine</td>
		<td data-title="Starts">5</td>
		<td data-title="1sts">4</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$851,900</td>
	</tr><tr>
		<td class="num" data-title="Rank">30.</td>
		<td data-title="Name">Super Stock</td>
		<td data-title="Starts">7</td>
		<td data-title="1sts">2</td>
		<td data-title="2nds">0</td>
		<td data-title="3rds">1</td>
		<td data-title="Purses">$846,915</td>
	</tr>					</tbody>
				</table> </div>
        <table id="infoEntries" width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" >
        <tbody>
        <tr>
            <td class="dateUpdated center" colspan="4">
                <em id="updateemp">Updated November 16th, 2021.</em>
            </td>
        </tr>
        </tbody>
        </table>
                {literal}       
    <style type="text/css">
        table.ordenableResult th{cursor: pointer}
    </style>
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_results.js"></script>
        {/literal}