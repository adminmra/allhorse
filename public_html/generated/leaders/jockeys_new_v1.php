<table id="infoEntries" class="data table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" >
					<tbody>
						<tr>
							<th width="5%" align="center"><strong>#</strong></th>
							<th width="35%"><strong>Name</strong></th>
							<th width="10%"><strong>Starts</strong></th>
							<th width="10%"><strong>1sts</strong></th>
							<th width="10%"><strong>2nds</strong></th>
							<th width="10%"><strong>3rds</strong></th>
							<th width="20%"><strong>Purses</strong></th>
						</tr><tr class"">
		<td class="num">1.</td>
		<td>Joel Rosario</td>
		<td>1,016</td>
		<td>214</td>
		<td>149</td>
		<td>144</td>
		<td>$31,378,963</td>
	</tr><tr class"odd">
		<td class="num">2.</td>
		<td>Irad Ortiz, Jr.</td>
		<td>1,367</td>
		<td>314</td>
		<td>266</td>
		<td>194</td>
		<td>$27,560,153</td>
	</tr><tr class"">
		<td class="num">3.</td>
		<td>Luis Saez</td>
		<td>1,447</td>
		<td>246</td>
		<td>228</td>
		<td>193</td>
		<td>$23,726,490</td>
	</tr><tr class"odd">
		<td class="num">4.</td>
		<td>Jose L. Ortiz</td>
		<td>1,243</td>
		<td>231</td>
		<td>205</td>
		<td>190</td>
		<td>$22,113,165</td>
	</tr><tr class"">
		<td class="num">5.</td>
		<td>Flavien Prat</td>
		<td>859</td>
		<td>233</td>
		<td>189</td>
		<td>126</td>
		<td>$22,016,853</td>
	</tr><tr class"odd">
		<td class="num">6.</td>
		<td>Florent Geroux</td>
		<td>779</td>
		<td>161</td>
		<td>117</td>
		<td>100</td>
		<td>$17,147,405</td>
	</tr><tr class"">
		<td class="num">7.</td>
		<td>Tyler Gaffalione</td>
		<td>1,254</td>
		<td>232</td>
		<td>171</td>
		<td>161</td>
		<td>$16,925,561</td>
	</tr><tr class"odd">
		<td class="num">8.</td>
		<td>John R. Velazquez</td>
		<td>625</td>
		<td>101</td>
		<td>78</td>
		<td>87</td>
		<td>$16,286,799</td>
	</tr><tr class"">
		<td class="num">9.</td>
		<td>Ricardo Santana, Jr.</td>
		<td>911</td>
		<td>151</td>
		<td>112</td>
		<td>125</td>
		<td>$16,205,998</td>
	</tr><tr class"odd">
		<td class="num">10.</td>
		<td>Manuel Franco</td>
		<td>1,200</td>
		<td>165</td>
		<td>181</td>
		<td>166</td>
		<td>$12,671,962</td>
	</tr><tr class"">
		<td class="num">11.</td>
		<td>Paco Lopez</td>
		<td>1,238</td>
		<td>292</td>
		<td>226</td>
		<td>186</td>
		<td>$11,087,264</td>
	</tr><tr class"odd">
		<td class="num">12.</td>
		<td>Brian Joseph Hernandez, Jr.</td>
		<td>857</td>
		<td>142</td>
		<td>128</td>
		<td>121</td>
		<td>$10,524,385</td>
	</tr><tr class"">
		<td class="num">13.</td>
		<td>Juan J. Hernandez</td>
		<td>837</td>
		<td>160</td>
		<td>123</td>
		<td>137</td>
		<td>$9,724,122</td>
	</tr><tr class"odd">
		<td class="num">14.</td>
		<td>Umberto Rispoli</td>
		<td>623</td>
		<td>123</td>
		<td>132</td>
		<td>99</td>
		<td>$9,347,679</td>
	</tr><tr class"">
		<td class="num">15.</td>
		<td>Eric Cancel</td>
		<td>960</td>
		<td>137</td>
		<td>142</td>
		<td>145</td>
		<td>$8,997,529</td>
	</tr><tr class"odd">
		<td class="num">16.</td>
		<td>Javier Castellano</td>
		<td>667</td>
		<td>91</td>
		<td>89</td>
		<td>92</td>
		<td>$8,874,098</td>
	</tr><tr class"">
		<td class="num">17.</td>
		<td>Junior Alvarado</td>
		<td>747</td>
		<td>92</td>
		<td>133</td>
		<td>102</td>
		<td>$8,140,519</td>
	</tr><tr class"odd">
		<td class="num">18.</td>
		<td>Kendrick Carmouche</td>
		<td>771</td>
		<td>130</td>
		<td>114</td>
		<td>110</td>
		<td>$8,100,390</td>
	</tr><tr class"">
		<td class="num">19.</td>
		<td>Dylan Davis</td>
		<td>778</td>
		<td>108</td>
		<td>101</td>
		<td>117</td>
		<td>$7,832,137</td>
	</tr><tr class"odd">
		<td class="num">20.</td>
		<td>Jose Lezcano</td>
		<td>669</td>
		<td>93</td>
		<td>85</td>
		<td>102</td>
		<td>$7,829,001</td>
	</tr><tr class"">
		<td class="num">21.</td>
		<td>Edgard J. Zayas</td>
		<td>1,269</td>
		<td>205</td>
		<td>224</td>
		<td>180</td>
		<td>$7,696,963</td>
	</tr><tr class"odd">
		<td class="num">22.</td>
		<td>Adam Beschizza</td>
		<td>875</td>
		<td>134</td>
		<td>130</td>
		<td>104</td>
		<td>$7,454,878</td>
	</tr><tr class"">
		<td class="num">23.</td>
		<td>James Graham</td>
		<td>912</td>
		<td>130</td>
		<td>134</td>
		<td>102</td>
		<td>$7,417,854</td>
	</tr><tr class"odd">
		<td class="num">24.</td>
		<td>David Cabrera</td>
		<td>995</td>
		<td>190</td>
		<td>180</td>
		<td>136</td>
		<td>$6,969,623</td>
	</tr><tr class"">
		<td class="num">25.</td>
		<td>Abel Cedillo</td>
		<td>878</td>
		<td>129</td>
		<td>154</td>
		<td>130</td>
		<td>$6,811,692</td>
	</tr><tr class"odd">
		<td class="num">26.</td>
		<td>Julien R. Leparoux</td>
		<td>530</td>
		<td>60</td>
		<td>63</td>
		<td>68</td>
		<td>$6,004,809</td>
	</tr><tr class"">
		<td class="num">27.</td>
		<td>Joseph Talamo</td>
		<td>517</td>
		<td>57</td>
		<td>59</td>
		<td>79</td>
		<td>$5,733,868</td>
	</tr><tr class"odd">
		<td class="num">28.</td>
		<td>Emisael Jaramillo</td>
		<td>920</td>
		<td>173</td>
		<td>131</td>
		<td>108</td>
		<td>$5,685,009</td>
	</tr><tr class"">
		<td class="num">29.</td>
		<td>Corey J. Lanerie</td>
		<td>774</td>
		<td>84</td>
		<td>83</td>
		<td>101</td>
		<td>$5,562,045</td>
	</tr><tr class"odd">
		<td class="num">30.</td>
		<td>Ramon A. Vazquez</td>
		<td>638</td>
		<td>118</td>
		<td>106</td>
		<td>79</td>
		<td>$5,550,296</td>
	</tr></tbody>
	</table>
	<p class="news-date" style="text-align: right;"><em class="updateemp">(Through November 16th, 2021)</em></p>