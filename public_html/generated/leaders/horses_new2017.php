

				<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
					<tbody>
						<tr>
							<td class="header" width="5%" align="center"><strong>#</strong></td>
							<td class="header" width="35%"><strong>Name</strong></td>
							<td class="header" width="10%"><strong>Starts</strong></td>
							<td class="header" width="10%"><strong>1sts</strong></td>
							<td class="header" width="10%"><strong>2nds</strong></td>
							<td class="header" width="10%"><strong>3rds</strong></td>
							<td class="header" width="20%"><strong>Purses</strong></td>
						</tr>
						
							<tr class=''>
								<td class="num">1.</td>
								<td>Arrogate</td>
								<td>4</td>
								<td>1</td>
								<td>1</td>
								<td>0</td>
								<td>$7,338,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">2.</td>
								<td>Gun Runner</td>
								<td>5</td>
								<td>5</td>
								<td>0</td>
								<td>0</td>
								<td>$4,950,700</td>
							</tr>
							
							<tr class=''>
								<td class="num">3.</td>
								<td>Shaman Ghost</td>
								<td>4</td>
								<td>2</td>
								<td>2</td>
								<td>0</td>
								<td>$2,520,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">4.</td>
								<td>Always Dreaming</td>
								<td>7</td>
								<td>4</td>
								<td>0</td>
								<td>1</td>
								<td>$2,320,600</td>
							</tr>
							
							<tr class=''>
								<td class="num">5.</td>
								<td>Talismanic (GB)</td>
								<td>1</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>$2,200,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">6.</td>
								<td>West Coast</td>
								<td>9</td>
								<td>6</td>
								<td>2</td>
								<td>1</td>
								<td>$2,083,800</td>
							</tr>
							
							<tr class=''>
								<td class="num">7.</td>
								<td>World Approval</td>
								<td>6</td>
								<td>5</td>
								<td>0</td>
								<td>0</td>
								<td>$2,043,600</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">8.</td>
								<td>Collected</td>
								<td>6</td>
								<td>4</td>
								<td>1</td>
								<td>1</td>
								<td>$1,882,800</td>
							</tr>
							
							<tr class=''>
								<td class="num">9.</td>
								<td>Beach Patrol</td>
								<td>7</td>
								<td>2</td>
								<td>2</td>
								<td>1</td>
								<td>$1,749,975</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">10.</td>
								<td>Abel Tasman</td>
								<td>7</td>
								<td>3</td>
								<td>4</td>
								<td>0</td>
								<td>$1,747,200</td>
							</tr>
							
							<tr class=''>
								<td class="num">11.</td>
								<td>Irap</td>
								<td>9</td>
								<td>3</td>
								<td>3</td>
								<td>1</td>
								<td>$1,600,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">12.</td>
								<td>Forever Unbridled</td>
								<td>3</td>
								<td>3</td>
								<td>0</td>
								<td>0</td>
								<td>$1,595,280</td>
							</tr>
							
							<tr class=''>
								<td class="num">13.</td>
								<td>Girvin</td>
								<td>8</td>
								<td>3</td>
								<td>2</td>
								<td>0</td>
								<td>$1,561,792</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">14.</td>
								<td>Sharp Azteca</td>
								<td>6</td>
								<td>4</td>
								<td>2</td>
								<td>0</td>
								<td>$1,290,490</td>
							</tr>
							
							<tr class=''>
								<td class="num">15.</td>
								<td>Roy H</td>
								<td>6</td>
								<td>5</td>
								<td>1</td>
								<td>0</td>
								<td>$1,270,900</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">16.</td>
								<td>Battle of Midway</td>
								<td>10</td>
								<td>5</td>
								<td>2</td>
								<td>2</td>
								<td>$1,249,949</td>
							</tr>
							
							<tr class=''>
								<td class="num">17.</td>
								<td>Caledonia Road</td>
								<td>3</td>
								<td>2</td>
								<td>1</td>
								<td>0</td>
								<td>$1,229,800</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">18.</td>
								<td>Good Magic</td>
								<td>3</td>
								<td>1</td>
								<td>2</td>
								<td>0</td>
								<td>$1,216,600</td>
							</tr>
							
							<tr class=''>
								<td class="num">19.</td>
								<td>Neolithic</td>
								<td>4</td>
								<td>1</td>
								<td>1</td>
								<td>2</td>
								<td>$1,169,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">20.</td>
								<td>Tapwrit</td>
								<td>6</td>
								<td>2</td>
								<td>1</td>
								<td>0</td>
								<td>$1,165,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">21.</td>
								<td>Cloud Computing</td>
								<td>6</td>
								<td>2</td>
								<td>1</td>
								<td>1</td>
								<td>$1,114,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">22.</td>
								<td>Wuheida (GB)</td>
								<td>1</td>
								<td>1</td>
								<td>0</td>
								<td>0</td>
								<td>$1,100,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">23.</td>
								<td>It Tiz Well</td>
								<td>8</td>
								<td>4</td>
								<td>2</td>
								<td>1</td>
								<td>$1,097,600</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">24.</td>
								<td>Sadler's Joy</td>
								<td>8</td>
								<td>2</td>
								<td>1</td>
								<td>3</td>
								<td>$1,082,480</td>
							</tr>
							
							<tr class=''>
								<td class="num">25.</td>
								<td>Oscar Performance</td>
								<td>7</td>
								<td>3</td>
								<td>0</td>
								<td>1</td>
								<td>$1,067,500</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">26.</td>
								<td>Practical Joke</td>
								<td>8</td>
								<td>2</td>
								<td>2</td>
								<td>2</td>
								<td>$1,056,000</td>
							</tr>
							
							<tr class=''>
								<td class="num">27.</td>
								<td>Classic Empire</td>
								<td>4</td>
								<td>1</td>
								<td>1</td>
								<td>1</td>
								<td>$1,034,300</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">28.</td>
								<td>War Story</td>
								<td>7</td>
								<td>1</td>
								<td>0</td>
								<td>1</td>
								<td>$1,024,910</td>
							</tr>
							
							<tr class=''>
								<td class="num">29.</td>
								<td>Keen Ice</td>
								<td>4</td>
								<td>1</td>
								<td>2</td>
								<td>0</td>
								<td>$1,020,000</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">30.</td>
								<td>Mor Spirit</td>
								<td>5</td>
								<td>3</td>
								<td>1</td>
								<td>0</td>
								<td>$992,000</td>
							</tr>
												</tbody>
				</table>
				<p class='news-date' style='text-align: right;'><em class='updateemp'>(Through January 25th, 2018 15:35:03)</em></p>
				