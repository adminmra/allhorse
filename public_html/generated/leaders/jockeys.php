		<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="10" width="685">
		<tbody>
		<tr>
		<td class="header" width="5%" align="center"><strong>#</strong></td>
		<td class="header" width="35%"><strong>Name</strong></td>
		<td class="header" width="10%"><strong>Starts</strong></td>
		<td class="header" width="10%"><strong>1sts</strong></td>
		<td class="header" width="10%"><strong>2nds</strong></td>
		<td class="header" width="10%"><strong>3rds</strong></td>
		<td class="header" width="20%"><strong>Purses</strong></td>
		</tr>
		<tr>			<td class="num">1.</td>
			<td>Javier Castellano</td>
			<td>1352</td>
			<td>287</td>
			<td>222</td>
			<td>216</td>
			<td>$26,116,902</td>
		</tr>
		<tr class="odd">			<td class="num">2.</td>
			<td>Irad Ortiz, Jr.</td>
			<td>1389</td>
			<td>283</td>
			<td>256</td>
			<td>212</td>
			<td>$22,110,838</td>
		</tr>
		<tr>			<td class="num">3.</td>
			<td>Jose L. Ortiz</td>
			<td>1437</td>
			<td>320</td>
			<td>216</td>
			<td>209</td>
			<td>$21,460,455</td>
		</tr>
		<tr class="odd">			<td class="num">4.</td>
			<td>John R. Velazquez</td>
			<td>970</td>
			<td>188</td>
			<td>130</td>
			<td>128</td>
			<td>$18,125,136</td>
		</tr>
		<tr>			<td class="num">5.</td>
			<td>Florent Geroux</td>
			<td>1031</td>
			<td>195</td>
			<td>135</td>
			<td>137</td>
			<td>$16,783,440</td>
		</tr>
		<tr class="odd">			<td class="num">6.</td>
			<td>Joel Rosario</td>
			<td>922</td>
			<td>149</td>
			<td>133</td>
			<td>151</td>
			<td>$15,861,865</td>
		</tr>
		<tr>			<td class="num">7.</td>
			<td>Julien R. Leparoux</td>
			<td>832</td>
			<td>161</td>
			<td>102</td>
			<td>100</td>
			<td>$13,568,304</td>
		</tr>
		<tr class="odd">			<td class="num">8.</td>
			<td>Mike E. Smith</td>
			<td>314</td>
			<td>51</td>
			<td>45</td>
			<td>41</td>
			<td>$13,067,373</td>
		</tr>
		<tr>			<td class="num">9.</td>
			<td>Luis Saez</td>
			<td>1368</td>
			<td>189</td>
			<td>205</td>
			<td>183</td>
			<td>$12,548,339</td>
		</tr>
		<tr class="odd">			<td class="num">10.</td>
			<td>Rafael Bejarano</td>
			<td>837</td>
			<td>186</td>
			<td>163</td>
			<td>110</td>
			<td>$12,095,101</td>
		</tr>
				</tbody>
		</table>
		<p class="news-date" style="text-align: right;"><em>(Through November 29th, 2016)</em></p>
		