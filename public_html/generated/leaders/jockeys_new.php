
				<table id="infoEntries" class="data table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="0" width="685">
					<tbody>
						<tr>
							<td class="header" width="5%" align="center"><strong>#</strong></td>
							<td class="header" width="35%"><strong>Name</strong></td>
							<td class="header" width="10%"><strong>Starts</strong></td>
							<td class="header" width="10%"><strong>1sts</strong></td>
							<td class="header" width="10%"><strong>2nds</strong></td>
							<td class="header" width="10%"><strong>3rds</strong></td>
							<td class="header" width="20%"><strong>Purses</strong></td>
						</tr>
						
							<tr class=''>
								<td class="num">1.</td>
								<td>Florent Geroux</td>
								<td>562</td>
								<td>119</td>
								<td>89</td>
								<td>65</td>
								<td>$13,725,222</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">2.</td>
								<td>Jose L. Ortiz</td>
								<td>769</td>
								<td>143</td>
								<td>121</td>
								<td>111</td>
								<td>$12,945,049</td>
							</tr>
							
							<tr class=''>
								<td class="num">3.</td>
								<td>Irad Ortiz, Jr.</td>
								<td>842</td>
								<td>191</td>
								<td>152</td>
								<td>122</td>
								<td>$12,380,411</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">4.</td>
								<td>Javier Castellano</td>
								<td>536</td>
								<td>108</td>
								<td>106</td>
								<td>73</td>
								<td>$11,076,805</td>
							</tr>
							
							<tr class=''>
								<td class="num">5.</td>
								<td>Luis Saez</td>
								<td>756</td>
								<td>143</td>
								<td>115</td>
								<td>129</td>
								<td>$9,586,849</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">6.</td>
								<td>John R. Velazquez</td>
								<td>386</td>
								<td>87</td>
								<td>57</td>
								<td>37</td>
								<td>$9,457,753</td>
							</tr>
							
							<tr class=''>
								<td class="num">7.</td>
								<td>Mike E. Smith</td>
								<td>134</td>
								<td>30</td>
								<td>23</td>
								<td>17</td>
								<td>$7,940,364</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">8.</td>
								<td>Ricardo Santana, Jr.</td>
								<td>530</td>
								<td>109</td>
								<td>83</td>
								<td>81</td>
								<td>$7,650,269</td>
							</tr>
							
							<tr class=''>
								<td class="num">9.</td>
								<td>Manuel Franco</td>
								<td>647</td>
								<td>120</td>
								<td>112</td>
								<td>102</td>
								<td>$7,475,856</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">10.</td>
								<td>Tyler Gaffalione</td>
								<td>763</td>
								<td>144</td>
								<td>117</td>
								<td>118</td>
								<td>$6,352,932</td>
							</tr>
							
							<tr class=''>
								<td class="num">11.</td>
								<td>Flavien Prat</td>
								<td>422</td>
								<td>94</td>
								<td>73</td>
								<td>63</td>
								<td>$6,184,980</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">12.</td>
								<td>Joel Rosario</td>
								<td>391</td>
								<td>59</td>
								<td>73</td>
								<td>58</td>
								<td>$5,901,664</td>
							</tr>
							
							<tr class=''>
								<td class="num">13.</td>
								<td>Junior Alvarado</td>
								<td>408</td>
								<td>89</td>
								<td>56</td>
								<td>68</td>
								<td>$4,869,255</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">14.</td>
								<td>Kendrick Carmouche</td>
								<td>520</td>
								<td>83</td>
								<td>89</td>
								<td>71</td>
								<td>$4,835,547</td>
							</tr>
							
							<tr class=''>
								<td class="num">15.</td>
								<td>Dylan Davis</td>
								<td>580</td>
								<td>80</td>
								<td>87</td>
								<td>90</td>
								<td>$4,791,064</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">16.</td>
								<td>Paco Lopez</td>
								<td>443</td>
								<td>91</td>
								<td>76</td>
								<td>51</td>
								<td>$4,547,246</td>
							</tr>
							
							<tr class=''>
								<td class="num">17.</td>
								<td>Drayden Van Dyke</td>
								<td>312</td>
								<td>64</td>
								<td>45</td>
								<td>42</td>
								<td>$4,403,941</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">18.</td>
								<td>Corey J. Lanerie</td>
								<td>567</td>
								<td>93</td>
								<td>112</td>
								<td>92</td>
								<td>$4,402,604</td>
							</tr>
							
							<tr class=''>
								<td class="num">19.</td>
								<td>Trevor McCarthy</td>
								<td>610</td>
								<td>65</td>
								<td>101</td>
								<td>92</td>
								<td>$3,845,430</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">20.</td>
								<td>Joe Bravo</td>
								<td>292</td>
								<td>67</td>
								<td>44</td>
								<td>38</td>
								<td>$3,790,310</td>
							</tr>
							
							<tr class=''>
								<td class="num">21.</td>
								<td>Brian Joseph Hernandez, Jr.</td>
								<td>509</td>
								<td>74</td>
								<td>71</td>
								<td>65</td>
								<td>$3,618,604</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">22.</td>
								<td>Julien R. Leparoux</td>
								<td>366</td>
								<td>50</td>
								<td>54</td>
								<td>41</td>
								<td>$3,593,747</td>
							</tr>
							
							<tr class=''>
								<td class="num">23.</td>
								<td>Tyler Baze</td>
								<td>441</td>
								<td>57</td>
								<td>78</td>
								<td>61</td>
								<td>$3,541,960</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">24.</td>
								<td>Joseph Talamo</td>
								<td>383</td>
								<td>65</td>
								<td>52</td>
								<td>55</td>
								<td>$3,476,621</td>
							</tr>
							
							<tr class=''>
								<td class="num">25.</td>
								<td>David Cabrera</td>
								<td>646</td>
								<td>116</td>
								<td>95</td>
								<td>107</td>
								<td>$3,429,049</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">26.</td>
								<td>Edgard J. Zayas</td>
								<td>737</td>
								<td>97</td>
								<td>114</td>
								<td>108</td>
								<td>$3,342,447</td>
							</tr>
							
							<tr class=''>
								<td class="num">27.</td>
								<td>Kent J. Desormeaux</td>
								<td>262</td>
								<td>46</td>
								<td>28</td>
								<td>47</td>
								<td>$3,215,756</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">28.</td>
								<td>Emisael Jaramillo</td>
								<td>538</td>
								<td>105</td>
								<td>71</td>
								<td>62</td>
								<td>$3,190,585</td>
							</tr>
							
							<tr class=''>
								<td class="num">29.</td>
								<td>Gary L. Stevens</td>
								<td>240</td>
								<td>39</td>
								<td>31</td>
								<td>34</td>
								<td>$3,187,082</td>
							</tr>
							
							<tr class='odd'>
								<td class="num">30.</td>
								<td>David Cohen</td>
								<td>485</td>
								<td>66</td>
								<td>47</td>
								<td>71</td>
								<td>$3,138,260</td>
							</tr>
												</tbody>
				</table>
				<p class='news-date' style='text-align: right;'><em class='updateemp'>(Through April 8th, 2019 19:24:17)</em></p>
				