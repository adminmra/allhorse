<div id="no-more-tables">
				<table id="infoEntries" class="table table-condensed table-striped table-bordered ordenableResult" border="0" cellspacing="0" cellpadding="10" >
                <thead>
                    <tr>
                        <th><strong>#</strong></th>
                        <th><strong>Name</strong></th>
                        <th><strong>Starts</strong></th>
                        <th><strong>1sts</strong></th>
                        <th><strong>2nds</strong></th>
                        <th><strong>3rds</strong></th>
                        <th><strong>Earnings</strong></th>
                    </tr>
                </thead>
					<tbody><tr>
		<td class="num" data-title="Rank">1.</td>
		<td data-title="Name">Brad H. Cox</td>
		<td data-title="Starts">889</td>
		<td data-title="1sts">235</td>
		<td data-title="2nds">161</td>
		<td data-title="3rds">128</td>
		<td data-title="Earnings">$29,509,700</td>
	</tr><tr>
		<td class="num" data-title="Rank">2.</td>
		<td data-title="Name">Steven M. Asmussen</td>
		<td data-title="Starts">2,167</td>
		<td data-title="1sts">414</td>
		<td data-title="2nds">321</td>
		<td data-title="3rds">329</td>
		<td data-title="Earnings">$28,563,553</td>
	</tr><tr>
		<td class="num" data-title="Rank">3.</td>
		<td data-title="Name">Chad C. Brown</td>
		<td data-title="Starts">779</td>
		<td data-title="1sts">183</td>
		<td data-title="2nds">147</td>
		<td data-title="3rds">119</td>
		<td data-title="Earnings">$20,864,967</td>
	</tr><tr>
		<td class="num" data-title="Rank">4.</td>
		<td data-title="Name">Todd A. Pletcher</td>
		<td data-title="Starts">822</td>
		<td data-title="1sts">173</td>
		<td data-title="2nds">140</td>
		<td data-title="3rds">110</td>
		<td data-title="Earnings">$19,619,222</td>
	</tr><tr>
		<td class="num" data-title="Rank">5.</td>
		<td data-title="Name">Mark E. Casse</td>
		<td data-title="Starts">1,206</td>
		<td data-title="1sts">201</td>
		<td data-title="2nds">169</td>
		<td data-title="3rds">146</td>
		<td data-title="Earnings">$13,690,369</td>
	</tr><tr>
		<td class="num" data-title="Rank">6.</td>
		<td data-title="Name">Michael J. Maker</td>
		<td data-title="Starts">1,195</td>
		<td data-title="1sts">195</td>
		<td data-title="2nds">196</td>
		<td data-title="3rds">173</td>
		<td data-title="Earnings">$13,509,972</td>
	</tr><tr>
		<td class="num" data-title="Rank">7.</td>
		<td data-title="Name">Bob Baffert</td>
		<td data-title="Starts">302</td>
		<td data-title="1sts">93</td>
		<td data-title="2nds">60</td>
		<td data-title="3rds">43</td>
		<td data-title="Earnings">$13,493,482</td>
	</tr><tr>
		<td class="num" data-title="Rank">8.</td>
		<td data-title="Name">William I. Mott</td>
		<td data-title="Starts">536</td>
		<td data-title="1sts">97</td>
		<td data-title="2nds">86</td>
		<td data-title="3rds">75</td>
		<td data-title="Earnings">$11,035,735</td>
	</tr><tr>
		<td class="num" data-title="Rank">9.</td>
		<td data-title="Name">Christophe Clement</td>
		<td data-title="Starts">499</td>
		<td data-title="1sts">98</td>
		<td data-title="2nds">84</td>
		<td data-title="3rds">71</td>
		<td data-title="Earnings">$8,893,342</td>
	</tr><tr>
		<td class="num" data-title="Rank">10.</td>
		<td data-title="Name">Karl Broberg</td>
		<td data-title="Starts">1,743</td>
		<td data-title="1sts">351</td>
		<td data-title="2nds">269</td>
		<td data-title="3rds">259</td>
		<td data-title="Earnings">$7,811,225</td>
	</tr><tr>
		<td class="num" data-title="Rank">11.</td>
		<td data-title="Name">Wesley A. Ward</td>
		<td data-title="Starts">412</td>
		<td data-title="1sts">120</td>
		<td data-title="2nds">59</td>
		<td data-title="3rds">55</td>
		<td data-title="Earnings">$7,666,435</td>
	</tr><tr>
		<td class="num" data-title="Rank">12.</td>
		<td data-title="Name">Saffie A. Joseph, Jr.</td>
		<td data-title="Starts">761</td>
		<td data-title="1sts">172</td>
		<td data-title="2nds">127</td>
		<td data-title="3rds">103</td>
		<td data-title="Earnings">$7,652,090</td>
	</tr><tr>
		<td class="num" data-title="Rank">13.</td>
		<td data-title="Name">Jamie Ness</td>
		<td data-title="Starts">983</td>
		<td data-title="1sts">270</td>
		<td data-title="2nds">201</td>
		<td data-title="3rds">137</td>
		<td data-title="Earnings">$7,237,052</td>
	</tr><tr>
		<td class="num" data-title="Rank">14.</td>
		<td data-title="Name">Peter Miller</td>
		<td data-title="Starts">590</td>
		<td data-title="1sts">108</td>
		<td data-title="2nds">93</td>
		<td data-title="3rds">82</td>
		<td data-title="Earnings">$6,735,497</td>
	</tr><tr>
		<td class="num" data-title="Rank">15.</td>
		<td data-title="Name">Philip D'Amato</td>
		<td data-title="Starts">445</td>
		<td data-title="1sts">89</td>
		<td data-title="2nds">75</td>
		<td data-title="3rds">63</td>
		<td data-title="Earnings">$6,663,860</td>
	</tr><tr>
		<td class="num" data-title="Rank">16.</td>
		<td data-title="Name">Brendan P. Walsh</td>
		<td data-title="Starts">415</td>
		<td data-title="1sts">69</td>
		<td data-title="2nds">54</td>
		<td data-title="3rds">62</td>
		<td data-title="Earnings">$6,411,815</td>
	</tr><tr>
		<td class="num" data-title="Rank">17.</td>
		<td data-title="Name">Robertino Diodoro</td>
		<td data-title="Starts">798</td>
		<td data-title="1sts">196</td>
		<td data-title="2nds">156</td>
		<td data-title="3rds">93</td>
		<td data-title="Earnings">$6,373,392</td>
	</tr><tr>
		<td class="num" data-title="Rank">18.</td>
		<td data-title="Name">Doug F. O'Neill</td>
		<td data-title="Starts">595</td>
		<td data-title="1sts">85</td>
		<td data-title="2nds">81</td>
		<td data-title="3rds">94</td>
		<td data-title="Earnings">$6,219,045</td>
	</tr><tr>
		<td class="num" data-title="Rank">19.</td>
		<td data-title="Name">Michael W. McCarthy</td>
		<td data-title="Starts">331</td>
		<td data-title="1sts">53</td>
		<td data-title="2nds">43</td>
		<td data-title="3rds">62</td>
		<td data-title="Earnings">$6,160,487</td>
	</tr><tr>
		<td class="num" data-title="Rank">20.</td>
		<td data-title="Name">John W. Sadler</td>
		<td data-title="Starts">388</td>
		<td data-title="1sts">78</td>
		<td data-title="2nds">50</td>
		<td data-title="3rds">58</td>
		<td data-title="Earnings">$5,827,095</td>
	</tr><tr>
		<td class="num" data-title="Rank">21.</td>
		<td data-title="Name">Charles Appleby</td>
		<td data-title="Starts">18</td>
		<td data-title="1sts">9</td>
		<td data-title="2nds">3</td>
		<td data-title="3rds">1</td>
		<td data-title="Earnings">$5,818,966</td>
	</tr><tr>
		<td class="num" data-title="Rank">22.</td>
		<td data-title="Name">Kenneth G. McPeek</td>
		<td data-title="Starts">369</td>
		<td data-title="1sts">53</td>
		<td data-title="2nds">45</td>
		<td data-title="3rds">49</td>
		<td data-title="Earnings">$5,771,405</td>
	</tr><tr>
		<td class="num" data-title="Rank">23.</td>
		<td data-title="Name">W. Bret Calhoun</td>
		<td data-title="Starts">564</td>
		<td data-title="1sts">100</td>
		<td data-title="2nds">102</td>
		<td data-title="3rds">75</td>
		<td data-title="Earnings">$5,604,165</td>
	</tr><tr>
		<td class="num" data-title="Rank">24.</td>
		<td data-title="Name">Rudy R. Rodriguez</td>
		<td data-title="Starts">492</td>
		<td data-title="1sts">85</td>
		<td data-title="2nds">69</td>
		<td data-title="3rds">57</td>
		<td data-title="Earnings">$4,942,459</td>
	</tr><tr>
		<td class="num" data-title="Rank">25.</td>
		<td data-title="Name">Claudio A. Gonzalez</td>
		<td data-title="Starts">723</td>
		<td data-title="1sts">143</td>
		<td data-title="2nds">123</td>
		<td data-title="3rds">108</td>
		<td data-title="Earnings">$4,739,795</td>
	</tr><tr>
		<td class="num" data-title="Rank">26.</td>
		<td data-title="Name">Richard Baltas</td>
		<td data-title="Starts">371</td>
		<td data-title="1sts">56</td>
		<td data-title="2nds">46</td>
		<td data-title="3rds">48</td>
		<td data-title="Earnings">$4,686,603</td>
	</tr><tr>
		<td class="num" data-title="Rank">27.</td>
		<td data-title="Name">Claude R. McGaughey III</td>
		<td data-title="Starts">308</td>
		<td data-title="1sts">55</td>
		<td data-title="2nds">34</td>
		<td data-title="3rds">45</td>
		<td data-title="Earnings">$4,661,646</td>
	</tr><tr>
		<td class="num" data-title="Rank">28.</td>
		<td data-title="Name">Michael J. Trombetta</td>
		<td data-title="Starts">629</td>
		<td data-title="1sts">96</td>
		<td data-title="2nds">105</td>
		<td data-title="3rds">99</td>
		<td data-title="Earnings">$4,624,727</td>
	</tr><tr>
		<td class="num" data-title="Rank">29.</td>
		<td data-title="Name">Michael Stidham</td>
		<td data-title="Starts">409</td>
		<td data-title="1sts">60</td>
		<td data-title="2nds">74</td>
		<td data-title="3rds">58</td>
		<td data-title="Earnings">$4,482,276</td>
	</tr><tr>
		<td class="num" data-title="Rank">30.</td>
		<td data-title="Name">Kelly J. Breen</td>
		<td data-title="Starts">488</td>
		<td data-title="1sts">74</td>
		<td data-title="2nds">75</td>
		<td data-title="3rds">71</td>
		<td data-title="Earnings">$4,465,538</td>
	</tr>					</tbody>
				</table> </div>
        <table id="infoEntries" width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" >
        <tbody>
        <tr>
            <td class="dateUpdated center" colspan="4">
                <em id="updateemp">Updated November 16th, 2021.</em>
            </td>
        </tr>
        </tbody>
        </table>
                {literal}       
    <style type="text/css">
        table.ordenableResult th{cursor: pointer}
    </style>
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_results.js"></script>
        {/literal}