

<style>

{literal}
    /* TABLES AND DATA ================================================================================ */
/*
    #infoEntries, #raceEntries, #raceTimes { margin: 10px 0 0 0; padding: 0; width: 100%; }
    #infoEntries tr, #raceEntries tr, #raceTimes tr, #infoEntries tbody { border: none;}
    #infoEntries th, #raceEntries th { border-right: 1px solid #6ca8ee; padding: 7px 10px; color: #FFF; font-size: 1em; background: #105ca9; }
    #infoEntries td, #raceEntries td {  padding: 10px; font-size: 1em;}
    #infoEntries td.odd, #raceEntries tr.AlternateRow  { background: #e5e5e5; }
    #infoEntries .td-box {  display: block; border-bottom: 1px solid #d9d9d9; padding: 9px 10px; }
    #infoEntries .td-box:hover {  display: block; background: #d4e9ff; }

    #raceTimes table td { padding: 10px; font-size: 1em; border: none; }
    #raceTimes, #raceTimes table { border: none;  }
    #raceTimes table tr.odd { background: #d4e9ff;  }
    #raceTimes td.num { font-weight: bold; }
    #raceTimes tbody, #raceTimes table tbody { border: none; }

    .page-racingschedule #infoEntries td { width:14.2%;}
    .page-racingschedule #infoEntries th { font-size: .917em; }
    */
{/literal}

</style>


<div class="table-responsive">
    <table border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="Harness Racing Schedule" summary="This week's harness racing schedule for all US and Canadian racetracks.">
        <caption>Harness Racing Schedule</caption>
         <tbody>
            <!--
            <tr>
                <th colspan="3" class="center"> * Harness Racing Schedule * Horses - 2016 Kentucky Derby  - May 06 </th>
            </tr>
            <tr>
                <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
            </tr>
            -->

           <tr>
             <th width="16%">Date</th>
             <th width="40%">Stakes</th>
             <th>Track</th>
             <th>Age & Sex</th>
             <th>Purse</th>
           </tr>

        
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> Tompkins Geers </td>
                    <td>  </td>
                    <td>  3YOFP   $77,000 @ Tioga  Tompkins Geers 3YOCP  $68,000 @ Tioga  INSS Final 3YOFP  $75,000 @ Hoosier  INSS Cons 3YOFP  $20,000 @ Hoosier  INSS 3YOCP  $20,000 @ Hoosier  NJSS 2YOCP  $20,000 @ Meadowlands  NJSS 2YOFP  $20,000 @ Meadowlands  NJSS 2YOCT  $20,000 @ Meadowlands  NJSS 2YOFT  $20,000 @ Meadowlands  Landmark 2YOCP  $14,000 @ Goshen Historic  Landmark 2YOFP  $6,500 @ Goshen Historic  Landmark 2YOCT  $11,000 @ Goshen Historic  Landmark 2YOFT  $6,500 @ Goshen Historic  Landmark 3YOCP  $14,000 @ Goshen Historic  Landmark 3YOFP  $8,000 @ Goshen Historic  Landmark 3YOCT  $15,000 @ Goshen Historic  Landmark 3YOFT  $9,000 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> Tompkins Geers </td>
                    <td>  </td>
                    <td>  3YOCP  $68,000 @ Tioga   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> INSS Final  </td>
                    <td>  </td>
                    <td> 3YOFP  $75,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> INSS Cons  </td>
                    <td>  </td>
                    <td> 3YOFP  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> INSS  </td>
                    <td>  </td>
                    <td> 3YOCP  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> NJSS  </td>
                    <td>  </td>
                    <td> 2YOCP  $20,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> NJSS  </td>
                    <td>  </td>
                    <td> 2YOFP  $20,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> NJSS  </td>
                    <td>  </td>
                    <td> 2YOCT  $20,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> NJSS  </td>
                    <td>  </td>
                    <td> 2YOFT  $20,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> Landmark  </td>
                    <td>  </td>
                    <td> 2YOCP  $14,000 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> Landmark  </td>
                    <td>  </td>
                    <td> 2YOFP  $6,500 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> Landmark  </td>
                    <td>  </td>
                    <td> 2YOCT  $11,000 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> Landmark  </td>
                    <td>  </td>
                    <td> 2YOFT  $6,500 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> Landmark  </td>
                    <td>  </td>
                    <td> 3YOCP  $14,000 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> Landmark  </td>
                    <td>  </td>
                    <td> 3YOFP  $8,000 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> Landmark  </td>
                    <td>  </td>
                    <td> 3YOCT  $15,000 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 29, 2018</strong></td>
                    <td> Landmark  </td>
                    <td>  </td>
                    <td> 3YOFT  $9,000 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 30, 2018</strong></td>
                    <td> Ben Franklin Final </td>
                    <td>  </td>
                    <td>  3&UP   $500,000 @ Pocono  M. Hempt Memorial Final 3YOP  $500,000 @ Pocono  Beal Memorial Final 3YOT  $500,000 @ Pocono  Lynch Memorial Final 3YOFP  $300,000 @ Pocono  Cleveland Trotting Classic 4&amp;UT  $175,000 @ Northfield  Excelsior 3YOCP  $15,000 @ Goshen Historic  Excelsior 3YOFP  $15,000 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 30, 2018</strong></td>
                    <td> M. Hempt Memorial Final  </td>
                    <td>  </td>
                    <td> 3YOP  $500,000 @ Pocono   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 30, 2018</strong></td>
                    <td> Beal Memorial Final </td>
                    <td>  </td>
                    <td>  3YOT  $500,000 @ Pocono   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 30, 2018</strong></td>
                    <td> Lynch Memorial Final  </td>
                    <td>  </td>
                    <td> 3YOFP  $300,000 @ Pocono   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 30, 2018</strong></td>
                    <td> Cleveland Trotting Classic </td>
                    <td>  </td>
                    <td>  4&amp;UT  $175,000 @ Northfield   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 30, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 3YOCP  $15,000 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jun 30, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 3YOFP  $15,000 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 1, 2018</strong></td>
                    <td> PA All-Stars </td>
                    <td>  </td>
                    <td>  2YOCP   $75,000 @ Pocono  Excelsior 3YOCT  $15,000 @ Goshen Historic  Excelsior 3YOFT  $15,000 @ Goshen Historic  Kin Pace Elim 3YOFP  $8,000 @ Clinton   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 1, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 3YOCT  $15,000 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 1, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 3YOFT  $15,000 @ Goshen Historic   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 1, 2018</strong></td>
                    <td> Kin Pace Elim  </td>
                    <td>  </td>
                    <td> 3YOFP  $8,000 @ Clinton   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 2, 2018</strong></td>
                    <td> OHSS</b> 2YOCT</div>   <div>$40,000 @ Northfield</div>  <div><span style="font-weight: bold;">PA Stallion</span> 2YOFT</div>  <div>$20,000 @ Meadows</div>  <div><b>OHSS </td>
                    <td>  </td>
                    <td>  2YOCT   $40,000 @ Northfield  PA Stallion 2YOFT  $20,000 @ Meadows  OHSS 2YOCT   $40,000 @ Northfield  PA Stallion 2YOFT  $20,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 2, 2018</strong></td>
                    <td> PA Stallion </td>
                    <td>  </td>
                    <td>  2YOFT  $20,000 @ Meadows  OHSS 2YOCT   $40,000 @ Northfield   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 2, 2018</strong></td>
                    <td> PA Stallion </td>
                    <td>  </td>
                    <td>  2YOFT  $20,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 3, 2018</strong></td>
                    <td> PASS </td>
                    <td>  </td>
                    <td>  3YOFT   $175,000 @ Philadelphia  PA Stallion 3YOFT  $20,000 @ Philadelphia  PASS 2YOFT  $165,000 @ Meadows  NYSS 2YOFT  $135,000 @ Yonkers  Excelsior 2YOFT  $15,000 @ Yonkers  OHSS 2YOFT  $40,000 @ Northfield  INSS 2YOFT  $20,000 @ Hoosier  Buckeye Stallion 2YOCT  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 3, 2018</strong></td>
                    <td> PA Stallion </td>
                    <td>  </td>
                    <td>  3YOFT  $20,000 @ Philadelphia   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 3, 2018</strong></td>
                    <td> PASS  </td>
                    <td>  </td>
                    <td> 2YOFT  $165,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 3, 2018</strong></td>
                    <td> NYSS  </td>
                    <td>  </td>
                    <td> 2YOFT  $135,000 @ Yonkers   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 3, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 2YOFT  $15,000 @ Yonkers   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 3, 2018</strong></td>
                    <td> OHSS  </td>
                    <td>  </td>
                    <td> 2YOFT  $40,000 @ Northfield   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 3, 2018</strong></td>
                    <td> INSS  </td>
                    <td>  </td>
                    <td> 2YOFT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 3, 2018</strong></td>
                    <td> Buckeye Stallion </td>
                    <td>  </td>
                    <td>  2YOCT  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 4, 2018</strong></td>
                    <td> NYSS </td>
                    <td>  </td>
                    <td>  2YOCP   $95,000 @ Monticello  Excelsior 2YOCP  $15,000 @ Monticello  INSS Final 3YOFT  $75,000 @ Hoosier  INSS Cons 3YOFT  $20,000 @ Hoosier  INSS 2YOFP  $20,000 @ Hoosier  OHSS 2YOCP  $40,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 4, 2018</strong></td>
                    <td> Excelsior </td>
                    <td>  </td>
                    <td>  2YOCP  $15,000 @ Monticello   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 4, 2018</strong></td>
                    <td> INSS Final </td>
                    <td>  </td>
                    <td>  3YOFT  $75,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 4, 2018</strong></td>
                    <td> INSS Cons </td>
                    <td>  </td>
                    <td>  3YOFT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 4, 2018</strong></td>
                    <td> INSS  </td>
                    <td>  </td>
                    <td> 2YOFP  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 4, 2018</strong></td>
                    <td> OHSS  </td>
                    <td>  </td>
                    <td> 2YOCP  $40,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 5, 2018</strong></td>
                    <td> PA Stallion </td>
                    <td>  </td>
                    <td>  2YOFP   $20,000 @ Meadows  INSS 2YOCP  $20,000 @ Hoosier  Buckeye Stallion 2YOCP  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 5, 2018</strong></td>
                    <td> INSS  </td>
                    <td>  </td>
                    <td> 2YOCP  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 5, 2018</strong></td>
                    <td> Buckeye Stallion </td>
                    <td>  </td>
                    <td>  2YOCP  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> Nadia Lobell </td>
                    <td>  </td>
                    <td>  3YOFP   $150,000 @ Hoosier  INSS Final 3YOCP  $75,000 @ Hoosier  INSS Final 3YOCT  $75,000 @ Hoosier  INSS Cons 3YOCP  $20,000 @ Hoosier  INSS Cons 3YOCT  $20,000 @ Hoosier  NYSS 2YOFP  $130,000 @ Yonkers  Excelsior 2YOFP  $15,000 @ Yonkers  OHSS 2YOFP  $40,000 @ Scioto  NJSS 2YOCP  $20,000 @ Meadowlands  NJSS 2YOFP  $20,000 @ Meadowlands  NJSS 2YOCT  $20,000 @ Meadowlands  NJSS 2YOFT  $20,000 @ Meadowlands  Kindergarten 2YOCP  $10,000 @ Meadowlands  Kindergarten 2YOFP  $10,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> INSS Final  </td>
                    <td>  </td>
                    <td> 3YOCP  $75,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> INSS Final  </td>
                    <td>  </td>
                    <td> 3YOCT  $75,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> INSS Cons </td>
                    <td>  </td>
                    <td>  3YOCP  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> INSS Cons  </td>
                    <td>  </td>
                    <td> 3YOCT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> NYSS  </td>
                    <td>  </td>
                    <td> 2YOFP  $130,000 @ Yonkers   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 2YOFP  $15,000 @ Yonkers   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> OHSS  </td>
                    <td>  </td>
                    <td> 2YOFP  $40,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> NJSS  </td>
                    <td>  </td>
                    <td> 2YOCP  $20,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> NJSS  </td>
                    <td>  </td>
                    <td> 2YOFP  $20,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> NJSS  </td>
                    <td>  </td>
                    <td> 2YOCT  $20,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> NJSS  </td>
                    <td>  </td>
                    <td> 2YOFT  $20,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> Kindergarten  </td>
                    <td>  </td>
                    <td> 2YOCP  $10,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 6, 2018</strong></td>
                    <td> Kindergarten  </td>
                    <td>  </td>
                    <td> 2YOFP  $10,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 7, 2018</strong></td>
                    <td> Graduate Final </td>
                    <td>  </td>
                    <td>  4YOP   $250,000 @ Meadowlands  Graduate Final 4YOT  $250,000 @ Meadowlands  Meadowlands Pace Elim 3YOP  $50,000 @ Meadowlands  Reynolds 3YOFT  $42,000 @ Meadowlands  PASS 2YOFP  $165,000 @ Meadows  OHSS 3YOCP  $40,000 @ Northfield  Sheppard Elim 2YOCP  $25,000 @ Yonkers  INSS 2YOCT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 7, 2018</strong></td>
                    <td> Graduate Final </td>
                    <td>  </td>
                    <td>  4YOT  $250,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 7, 2018</strong></td>
                    <td> Meadowlands Pace Elim </td>
                    <td>  </td>
                    <td>  3YOP  $50,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 7, 2018</strong></td>
                    <td> Reynolds  </td>
                    <td>  </td>
                    <td> 3YOFT  $42,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 7, 2018</strong></td>
                    <td> PASS  </td>
                    <td>  </td>
                    <td> 2YOFP  $165,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 7, 2018</strong></td>
                    <td> OHSS  </td>
                    <td>  </td>
                    <td> 3YOCP  $40,000 @ Northfield   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 7, 2018</strong></td>
                    <td> Sheppard Elim  </td>
                    <td>  </td>
                    <td> 2YOCP  $25,000 @ Yonkers   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 7, 2018</strong></td>
                    <td> INSS  </td>
                    <td>  </td>
                    <td> 2YOCT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 8, 2018</strong></td>
                    <td> NYSS </td>
                    <td>  </td>
                    <td>  3YOCP   $105,000 @ Buffalo  Excelsior 3YOCP  $15,000 @ Buffalo  Kin Pace Final 3YOFP  $60,000 @ Clinton  MSRF 3YOCT  $20,000 @ Ocean  MSRF 3YOFP  $20,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 8, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 3YOCP  $15,000 @ Buffalo   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 8, 2018</strong></td>
                    <td> Kin Pace Final  </td>
                    <td>  </td>
                    <td> 3YOFP  $60,000 @ Clinton   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 8, 2018</strong></td>
                    <td> MSRF  </td>
                    <td>  </td>
                    <td> 3YOCT  $20,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 8, 2018</strong></td>
                    <td> MSRF  </td>
                    <td>  </td>
                    <td> 3YOFP  $20,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 9, 2018</strong></td>
                    <td> ONSS Gold </td>
                    <td>  </td>
                    <td>  2YOFP   $180,000 @ Mohawk  NYSS 2YOCT  $95,000 @ Monticello  Excelsior 2YOCT  $15,000 @ Monticello  PA All-Stars 2YOCT  $75,000 @ Pocono  OHSS 3YOCT  $40,000 @ Northfield  MSRF 3YOCP  $20,000 @ Ocean  MSRF 3YOFT  $20,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 9, 2018</strong></td>
                    <td> NYSS  </td>
                    <td>  </td>
                    <td> 2YOCT  $95,000 @ Monticello   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 9, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 2YOCT  $15,000 @ Monticello   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 9, 2018</strong></td>
                    <td> PA All-Stars  </td>
                    <td>  </td>
                    <td> 2YOCT  $75,000 @ Pocono   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 9, 2018</strong></td>
                    <td> OHSS  </td>
                    <td>  </td>
                    <td> 3YOCT  $40,000 @ Northfield   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 9, 2018</strong></td>
                    <td> MSRF  </td>
                    <td>  </td>
                    <td> 3YOCP  $20,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 9, 2018</strong></td>
                    <td> MSRF  </td>
                    <td>  </td>
                    <td> 3YOFT  $20,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 10, 2018</strong></td>
                    <td> INSS Final </td>
                    <td>  </td>
                    <td>  2YOFT   $75,000 @ Hoosier  INSS Cons 2YOFT  $20,000 @ Hoosier  PA All-Stars 2YOFT  $75,000 @ Pocono  Buckeye Stallion 2YOFP  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 10, 2018</strong></td>
                    <td> INSS Cons </td>
                    <td>  </td>
                    <td>  2YOFT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 10, 2018</strong></td>
                    <td> PA All-Stars </td>
                    <td>  </td>
                    <td>  2YOFT  $75,000 @ Pocono   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 10, 2018</strong></td>
                    <td> Buckeye Stallion </td>
                    <td>  </td>
                    <td>  2YOFP  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 11, 2018</strong></td>
                    <td> NYSS </td>
                    <td>  </td>
                    <td>  2YOFT   $95,000 @ Buffalo  Excelsior 2YOFT  $15,000 @ Buffalo  INSS Final 2YOFP  $75,000 @ Hoosier  INSS Cons 2YOFP  $20,000 @ Hoosier  Buckeye Stallion 3YOCT  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 11, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 2YOFT  $15,000 @ Buffalo   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 11, 2018</strong></td>
                    <td> INSS Final  </td>
                    <td>  </td>
                    <td> 2YOFP  $75,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 11, 2018</strong></td>
                    <td> INSS Cons  </td>
                    <td>  </td>
                    <td> 2YOFP  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 11, 2018</strong></td>
                    <td> Buckeye Stallion  </td>
                    <td>  </td>
                    <td> 3YOCT  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 12, 2018</strong></td>
                    <td> ONSS Gold </td>
                    <td>  </td>
                    <td>  2YOCP   $180,000 @ Mohawk  NYSS 3YOFP  $105,000 @ Vernon  Excelsior 3YOFP  $15,000 @ Vernon  INSS Final 2YOCP  $75,000 @ Hoosier  INSS Cons 2YOCP  $20,000 @ Hoosier  Buckeye Stallion 2YOFT  $15,000 @ Scioto   </td>
                    <td>   	</div>   </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 12, 2018</strong></td>
                    <td> NYSS  </td>
                    <td>  </td>
                    <td> 3YOFP  $105,000 @ Vernon   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 12, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 3YOFP  $15,000 @ Vernon   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 12, 2018</strong></td>
                    <td> INSS Final  </td>
                    <td>  </td>
                    <td> 2YOCP  $75,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 12, 2018</strong></td>
                    <td> INSS Cons  </td>
                    <td>  </td>
                    <td> 2YOCP  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 12, 2018</strong></td>
                    <td> Buckeye Stallion </td>
                    <td>  </td>
                    <td>  2YOFT  $15,000 @ Scioto    	   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 13, 2018</strong></td>
                    <td> NJSS Final </td>
                    <td>  </td>
                    <td>  2YOCP   $75,000 @ Meadowlands  NJSS Final 2YOFP  $75,000 @ Meadowlands  NJSS Final 2YOCT  $75,000 @ Meadowlands  NJSS Final 2YOFT  $75,000 @ Meadowlands  Kindergarten 2YOCT  $10,000 @ Meadowlands  Kindergarten 2YOFT  $10,000 @ Meadowlands  OHSS 3YOFT  $40,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 13, 2018</strong></td>
                    <td> NJSS Final  </td>
                    <td>  </td>
                    <td> 2YOFP  $75,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 13, 2018</strong></td>
                    <td> NJSS Final  </td>
                    <td>  </td>
                    <td> 2YOCT  $75,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 13, 2018</strong></td>
                    <td> NJSS Final  </td>
                    <td>  </td>
                    <td> 2YOFT  $75,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 13, 2018</strong></td>
                    <td> Kindergarten </td>
                    <td>  </td>
                    <td>  2YOCT  $10,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 13, 2018</strong></td>
                    <td> Kindergarten  </td>
                    <td>  </td>
                    <td> 2YOFT  $10,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 13, 2018</strong></td>
                    <td> OHSS  </td>
                    <td>  </td>
                    <td> 3YOFT  $40,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 14, 2018</strong></td>
                    <td> Meadowlands Pace Final </td>
                    <td>  </td>
                    <td>  3YOP   $650,000 @ Meadowlands  W.R. Haughton 3&amp;UP  $425,000 @ Meadowlands  Hambletonian Maturity 4YOT  $400,000 @ Meadowlands  Stanley Dancer 3YOT  $320,000 @ Meadowlands  Del Miller 3YOFT  $250,000 @ Meadowlands  Golden Girls 3&amp;UF&amp;MP  $200,000 @ Meadowlands  Mistletoe Shalee 3YOFP  $165,000 @ Meadowlands  Miss Versatility 3&amp;UF&amp;MT  $40,000 @ Meadowlands  PASS 2YOCP  $175,000 @ Pocono  Sheppard Final 2YOCP  $100,000 @ Yonkers  INSS Final 2YOCT  $75,000 @ Hoosier  INSS Cons 2YOCT  $20,000 @ Hoosier  OHSS 3YOFP  $40,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 14, 2018</strong></td>
                    <td> W.R. Haughton </td>
                    <td>  </td>
                    <td>  3&amp;UP  $425,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 14, 2018</strong></td>
                    <td> Hambletonian Maturity </td>
                    <td>  </td>
                    <td>  4YOT  $400,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 14, 2018</strong></td>
                    <td> Stanley Dancer  </td>
                    <td>  </td>
                    <td> 3YOT  $320,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 14, 2018</strong></td>
                    <td> Del Miller  </td>
                    <td>  </td>
                    <td> 3YOFT  $250,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 14, 2018</strong></td>
                    <td> Golden Girls </td>
                    <td>  </td>
                    <td>  3&amp;UF&amp;MP  $200,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 14, 2018</strong></td>
                    <td> Mistletoe Shalee  </td>
                    <td>  </td>
                    <td> 3YOFP  $165,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 14, 2018</strong></td>
                    <td> Miss Versatility </td>
                    <td>  </td>
                    <td>  3&amp;UF&amp;MT  $40,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 14, 2018</strong></td>
                    <td> PASS  </td>
                    <td>  </td>
                    <td> 2YOCP  $175,000 @ Pocono   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 14, 2018</strong></td>
                    <td> Sheppard Final  </td>
                    <td>  </td>
                    <td> 2YOCP  $100,000 @ Yonkers   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 14, 2018</strong></td>
                    <td> INSS Final  </td>
                    <td>  </td>
                    <td> 2YOCT  $75,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 14, 2018</strong></td>
                    <td> INSS Cons </td>
                    <td>  </td>
                    <td>  2YOCT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 14, 2018</strong></td>
                    <td> OHSS  </td>
                    <td>  </td>
                    <td> 3YOFP  $40,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 15, 2018</strong></td>
                    <td> ONSS Gold </td>
                    <td>  </td>
                    <td>  2YOFT   $180,000 @ Georgian  NYSS 2YOFP  $95,000 @ Tioga  Excelsior 2YOFP  $15,000 @ Tioga  MDSS Final 3YOCT  $55,000 @ Ocean  MDSS Final 3YOFP  $55,000 @ Ocean  PA Stallion 2YOCP  $20,000 @ Pocono   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 15, 2018</strong></td>
                    <td> NYSS  </td>
                    <td>  </td>
                    <td> 2YOFP  $95,000 @ Tioga   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 15, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 2YOFP  $15,000 @ Tioga   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 15, 2018</strong></td>
                    <td> MDSS Final  </td>
                    <td>  </td>
                    <td> 3YOCT  $55,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 15, 2018</strong></td>
                    <td> MDSS Final </td>
                    <td>  </td>
                    <td>  3YOFP  $55,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 15, 2018</strong></td>
                    <td> PA Stallion </td>
                    <td>  </td>
                    <td>  2YOCP  $20,000 @ Pocono   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 16, 2018</strong></td>
                    <td> ONSS Gold </td>
                    <td>  </td>
                    <td>  2YOCT   $180,000 @ Mohawk  NYSS 3YOCT  $105,000 @ Monticello  Excelsior 3YOCT  $15,000 @ Monticello  MDSS Final 3YOCP  $55,000 @ Ocean  MDSS Final 3YOFT  $55,000 @ Ocean  OHSS 2YOCP  $40,000 @ Northfield  PA Stallion 2YOCT  $20,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 16, 2018</strong></td>
                    <td> NYSS  </td>
                    <td>  </td>
                    <td> 3YOCT  $105,000 @ Monticello   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 16, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 3YOCT  $15,000 @ Monticello   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 16, 2018</strong></td>
                    <td> MDSS Final </td>
                    <td>  </td>
                    <td>  3YOCP  $55,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 16, 2018</strong></td>
                    <td> MDSS Final </td>
                    <td>  </td>
                    <td>  3YOFT  $55,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 16, 2018</strong></td>
                    <td> OHSS  </td>
                    <td>  </td>
                    <td> 2YOCP  $40,000 @ Northfield   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 16, 2018</strong></td>
                    <td> PA Stallion </td>
                    <td>  </td>
                    <td>  2YOCT  $20,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 17, 2018</strong></td>
                    <td> PASS </td>
                    <td>  </td>
                    <td>  2YOCT   $165,000 @ Meadows  PA All-Stars 2YOFP  $75,000 @ Pocono  OHSS 2YOFP  $40,000 @ Northfield  Buckeye Stallion 2YOCP  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 17, 2018</strong></td>
                    <td> PA All-Stars </td>
                    <td>  </td>
                    <td>  2YOFP  $75,000 @ Pocono   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 17, 2018</strong></td>
                    <td> OHSS  </td>
                    <td>  </td>
                    <td> 2YOFP  $40,000 @ Northfield   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 17, 2018</strong></td>
                    <td> Buckeye Stallion </td>
                    <td>  </td>
                    <td>  2YOCP  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 18, 2018</strong></td>
                    <td> ONSS </td>
                    <td>  </td>
                    <td>  3YOCT   $180,000 @ Grand River  PASS 2YOFT  $175,000 @ Philadelphia  PA Stallion 2YOFT  $20,000 @ Philadelphia  NYSS 2YOCP  $95,000 @ Buffalo  Excelsior 2YOCP  $15,000 @ Buffalo  Buckeye Stallion 2YOCT  $15,000 @ Scioto  Buckeye Stallion 3YOFT  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 18, 2018</strong></td>
                    <td> PASS  </td>
                    <td>  </td>
                    <td> 2YOFT  $175,000 @ Philadelphia   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 18, 2018</strong></td>
                    <td> PA Stallion </td>
                    <td>  </td>
                    <td>  2YOFT  $20,000 @ Philadelphia   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 18, 2018</strong></td>
                    <td> NYSS  </td>
                    <td>  </td>
                    <td> 2YOCP  $95,000 @ Buffalo   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 18, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 2YOCP  $15,000 @ Buffalo   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 18, 2018</strong></td>
                    <td> Buckeye Stallion </td>
                    <td>  </td>
                    <td>  2YOCT  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 18, 2018</strong></td>
                    <td> Buckeye Stallion  </td>
                    <td>  </td>
                    <td> 3YOFT  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 19, 2018</strong></td>
                    <td> NYSS </td>
                    <td>  </td>
                    <td>  3YOFT   $105,000 @ Vernon  Excelsior 3YOFT  $15,000 @ Vernon  PA Stallion 3YOCT  $20,000 @ Philadelphia  Buckeye Stallion 2YOFP  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 19, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 3YOFT  $15,000 @ Vernon   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 19, 2018</strong></td>
                    <td> PA Stallion </td>
                    <td>  </td>
                    <td>  3YOCT  $20,000 @ Philadelphia   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 19, 2018</strong></td>
                    <td> Buckeye Stallion  </td>
                    <td>  </td>
                    <td> 2YOFP  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 20, 2018</strong></td>
                    <td> ONSS Gold </td>
                    <td>  </td>
                    <td>  2YOFP   $180,000 @ Mohawk  ONSS Gold 3YOFP  $180,000 @ Mohawk  PASS 3YOCT  $175,000 @ Philadelphia  NYSS 2YOCT  $95,000 @ Tioga  Excelsior 2YOCT  $15,000 @ Tioga  OHSS 2YOCT  $40,000 @ Scioto  OHSS 2YOFT  $40,000 @ Scioto  Kindergarten 2YOCP  $10,000 @ Meadowlands  Kindergarten 2YOFP  $10,000 @ Meadowlands  Kindergarten 2YOCT  $10,000 @ Meadowlands  Kindergarten 2YOFT  $10,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 20, 2018</strong></td>
                    <td> ONSS Gold  </td>
                    <td>  </td>
                    <td> 3YOFP  $180,000 @ Mohawk   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 20, 2018</strong></td>
                    <td> PASS  </td>
                    <td>  </td>
                    <td> 3YOCT  $175,000 @ Philadelphia   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 20, 2018</strong></td>
                    <td> NYSS  </td>
                    <td>  </td>
                    <td> 2YOCT  $95,000 @ Tioga   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 20, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 2YOCT  $15,000 @ Tioga   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 20, 2018</strong></td>
                    <td> OHSS  </td>
                    <td>  </td>
                    <td> 2YOCT  $40,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 20, 2018</strong></td>
                    <td> OHSS  </td>
                    <td>  </td>
                    <td> 2YOFT  $40,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 20, 2018</strong></td>
                    <td> Kindergarten  </td>
                    <td>  </td>
                    <td> 2YOCP  $10,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 20, 2018</strong></td>
                    <td> Kindergarten  </td>
                    <td>  </td>
                    <td> 2YOFP  $10,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 20, 2018</strong></td>
                    <td> Kindergarten  </td>
                    <td>  </td>
                    <td> 2YOCT  $10,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 20, 2018</strong></td>
                    <td> Kindergarten  </td>
                    <td>  </td>
                    <td> 2YOFT  $10,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 21, 2018</strong></td>
                    <td> Joe Gerrity </td>
                    <td>  </td>
                    <td>  3&UP   $260,000 @ Saratoga  NYSS 3YOCP  $157,000 @ Saratoga  Excelsior 3YOCP  $15,000 @ Saratoga  ONSS Gold 3YOFT  $180,000 @ Georgian  PASS 2YOCP  $165,000 @ Meadows  Adios Elim 3YOCP  $25,000 @ Meadows  PA Stallion 2YOCP  $20,000 @ Meadows  Tompkins Geers 3YOCT  $59,000 @ Meadowlands  Tompkins Geers 3YOFT  $59,000 @ Meadowlands  INSS 3YOFP  $20,000 @ Hoosier  INSS 3YOCT  $20,000 @ Hoosier  Buckeye Stallion 3YOCP  $15,000 @ Scioto  </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 21, 2018</strong></td>
                    <td> NYSS  </td>
                    <td>  </td>
                    <td> 3YOCP  $157,000 @ Saratoga   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 21, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 3YOCP  $15,000 @ Saratoga   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 21, 2018</strong></td>
                    <td> ONSS Gold  </td>
                    <td>  </td>
                    <td> 3YOFT  $180,000 @ Georgian   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 21, 2018</strong></td>
                    <td> PASS  </td>
                    <td>  </td>
                    <td> 2YOCP  $165,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 21, 2018</strong></td>
                    <td> Adios Elim </td>
                    <td>  </td>
                    <td>  3YOCP  $25,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 21, 2018</strong></td>
                    <td> PA Stallion  </td>
                    <td>  </td>
                    <td> 2YOCP  $20,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 21, 2018</strong></td>
                    <td> Tompkins Geers </td>
                    <td>  </td>
                    <td>  3YOCT  $59,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 21, 2018</strong></td>
                    <td> Tompkins Geers </td>
                    <td>  </td>
                    <td>  3YOFT  $59,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 21, 2018</strong></td>
                    <td> INSS  </td>
                    <td>  </td>
                    <td> 3YOFP  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 21, 2018</strong></td>
                    <td> INSS  </td>
                    <td>  </td>
                    <td> 3YOCT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 21, 2018</strong></td>
                    <td> Buckeye Stallion </td>
                    <td>  </td>
                    <td>  3YOCP  $15,000 @ Scioto  </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 22, 2018</strong></td>
                    <td> ONSS Gold </td>
                    <td>  </td>
                    <td>  3YOCP   $180,000 @ Rideau Carleton  MDSS 4&amp;5OP  $25,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 22, 2018</strong></td>
                    <td> MDSS  </td>
                    <td>  </td>
                    <td> 4&amp;5OP  $25,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 23, 2018</strong></td>
                    <td> ONSS Gold </td>
                    <td>  </td>
                    <td>  2YOCP   $180,000 @ Mohawk  NYSS 2YOFT  $95,000 @ Batavia  Excelsior 2YOFT  $15,000 @ Batavia  MDSS 4&amp;5OT  $25,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 23, 2018</strong></td>
                    <td> NYSS  </td>
                    <td>  </td>
                    <td> 2YOFT  $95,000 @ Batavia   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 23, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 2YOFT  $15,000 @ Batavia   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 23, 2018</strong></td>
                    <td> MDSS  </td>
                    <td>  </td>
                    <td> 4&amp;5OT  $25,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 24, 2018</strong></td>
                    <td> NYSS </td>
                    <td>  </td>
                    <td>  2YOFP   $130,000 @ Yonkers  Excelsior 2YOFP  $15,000 @ Yonkers  INSS 2YOCT  $20,000 @ Hoosier  INSS 2YOFT  $20,000 @ Hoosier  INSS 3YOFT  $20,000 @ Hoosier  Buckeye Stallion 2YOFT  $15,000 @ Painesville   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 24, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 2YOFP  $15,000 @ Yonkers   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 24, 2018</strong></td>
                    <td> INSS  </td>
                    <td>  </td>
                    <td> 2YOCT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 24, 2018</strong></td>
                    <td> INSS  </td>
                    <td>  </td>
                    <td> 2YOFT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 24, 2018</strong></td>
                    <td> INSS  </td>
                    <td>  </td>
                    <td> 3YOFT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 24, 2018</strong></td>
                    <td> Buckeye Stallion </td>
                    <td>  </td>
                    <td>  2YOFT  $15,000 @ Painesville   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 25, 2018</strong></td>
                    <td> NYSS </td>
                    <td>  </td>
                    <td>  3YOFP   $105,000 @ Batavia  Excelsior 3YOFP  $15,000 @ Batavia  INSS 3YOCP  $20,000 @ Hoosier  INSS 2YOCP  $20,000 @ Hoosier  INSS 2YOFP  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 25, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 3YOFP  $15,000 @ Batavia   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 25, 2018</strong></td>
                    <td> INSS  </td>
                    <td>  </td>
                    <td> 3YOCP  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 25, 2018</strong></td>
                    <td> INSS  </td>
                    <td>  </td>
                    <td> 2YOCP  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 25, 2018</strong></td>
                    <td> INSS  </td>
                    <td>  </td>
                    <td> 2YOFP  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 26, 2018</strong></td>
                    <td> DSBF Final </td>
                    <td>  </td>
                    <td>  3YOCP   $100,000 @ Harrington  DSBF Final 3YOFP  $100,000 @ Harrington  DSBF Final 3YOCT  $100,000 @ Harrington  DSBF Final 3YOFT  $100,000 @ Harrington  Buckeye Stallion 3YOFP  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 26, 2018</strong></td>
                    <td> DSBF Final  </td>
                    <td>  </td>
                    <td> 3YOFP  $100,000 @ Harrington   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 26, 2018</strong></td>
                    <td> DSBF Final </td>
                    <td>  </td>
                    <td>  3YOCT  $100,000 @ Harrington   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 26, 2018</strong></td>
                    <td> DSBF Final  </td>
                    <td>  </td>
                    <td> 3YOFT  $100,000 @ Harrington   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 26, 2018</strong></td>
                    <td> Buckeye Stallion  </td>
                    <td>  </td>
                    <td> 3YOFP  $15,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 27, 2018</strong></td>
                    <td> ONSS Gold </td>
                    <td>  </td>
                    <td>  2YOFT   $180,000 @ Mohawk  NYSS 3YOCT  $144,000 @ Yonkers  Excelsior 3YOCT  $15,000 @ Yonkers  Ohio State Fair 2YOFP  $140,000 @ Scioto  Ohio State Fair 2YOFT  $140,000 @ Scioto  Arden Downs 2YOCT  $50,000 @ Meadows  Arden Downs 2YOFT  $50,000 @ Meadows  Peter Haughton Elim 2YOCT  $20,000 @ Meadowlands  Jim Doherty Elim 2YOFT  $20,000 @ Meadowlands  Robert Carey 3YOCP  $15,000 @ Hawthorne   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 27, 2018</strong></td>
                    <td> NYSS  </td>
                    <td>  </td>
                    <td> 3YOCT  $144,000 @ Yonkers   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 27, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 3YOCT  $15,000 @ Yonkers   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 27, 2018</strong></td>
                    <td> Ohio State Fair  </td>
                    <td>  </td>
                    <td> 2YOFP  $140,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 27, 2018</strong></td>
                    <td> Ohio State Fair  </td>
                    <td>  </td>
                    <td> 2YOFT  $140,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 27, 2018</strong></td>
                    <td> Arden Downs </td>
                    <td>  </td>
                    <td>  2YOCT  $50,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 27, 2018</strong></td>
                    <td> Arden Downs  </td>
                    <td>  </td>
                    <td> 2YOFT  $50,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 27, 2018</strong></td>
                    <td> Peter Haughton Elim  </td>
                    <td>  </td>
                    <td> 2YOCT  $20,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 27, 2018</strong></td>
                    <td> Jim Doherty Elim  </td>
                    <td>  </td>
                    <td> 2YOFT  $20,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 27, 2018</strong></td>
                    <td> Robert Carey </td>
                    <td>  </td>
                    <td>  3YOCP  $15,000 @ Hawthorne   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 28, 2018</strong></td>
                    <td> Adios Final </td>
                    <td>  </td>
                    <td>  3YOCP   $400,000 @ Meadows  Adioo Volo 3YOFP  $125,000 @ Meadows  Arden Downs 2YOCP  $58,000 @ Meadows  Arden Downs 3YOFT  $58,000 @ Meadows  Arden Downs 3YOCT  $45,000 @ Meadows  Arden Downs 2YOFP  $48,000 @ Meadows  Ohio State Fair 2YOCP  $160,000 @ Scioto  Ohio State Fair 2YOCT  $120,000 @ Scioto  Reynolds 3YOCT  $59,000 @ Meadowlands  Hambletonian Oaks Elim 3YOFT  $35,000 @ Meadowlands  Dream Of Glory Elim 3YOT  $8,000 @ Hanover   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 28, 2018</strong></td>
                    <td> Adioo Volo  </td>
                    <td>  </td>
                    <td> 3YOFP  $125,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 28, 2018</strong></td>
                    <td> Arden Downs </td>
                    <td>  </td>
                    <td>  2YOCP  $58,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 28, 2018</strong></td>
                    <td> Arden Downs </td>
                    <td>  </td>
                    <td>  3YOFT  $58,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 28, 2018</strong></td>
                    <td> Arden Downs </td>
                    <td>  </td>
                    <td>  3YOCT  $45,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 28, 2018</strong></td>
                    <td> Arden Downs </td>
                    <td>  </td>
                    <td>  2YOFP  $48,000 @ Meadows   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 28, 2018</strong></td>
                    <td> Ohio State Fair  </td>
                    <td>  </td>
                    <td> 2YOCP  $160,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 28, 2018</strong></td>
                    <td> Ohio State Fair </td>
                    <td>  </td>
                    <td>  2YOCT  $120,000 @ Scioto   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 28, 2018</strong></td>
                    <td> Reynolds  </td>
                    <td>  </td>
                    <td> 3YOCT  $59,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 28, 2018</strong></td>
                    <td> Hambletonian Oaks Elim </td>
                    <td>  </td>
                    <td>  3YOFT  $35,000 @ Meadowlands   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 28, 2018</strong></td>
                    <td> Dream Of Glory Elim </td>
                    <td>  </td>
                    <td>  3YOT  $8,000 @ Hanover   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 29, 2018</strong></td>
                    <td> ONSS Gold </td>
                    <td>  </td>
                    <td>  2YOCT   $180,000 @ Georgian  NYSS 2YOCP  $95,000 @ Tioga  Excelsior 2YOCP  $15,000 @ Tioga  MSRF 2YOFP  $20,000 @ Ocean  MSRF 2YOCT  $20,000 @ Ocean  Erwin Dygert 3YOCT  $15,000 @ Hawthorne  Beulah Dygert 3YOFT  $15,000 @ Hawthorne   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 29, 2018</strong></td>
                    <td> NYSS  </td>
                    <td>  </td>
                    <td> 2YOCP  $95,000 @ Tioga   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 29, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 2YOCP  $15,000 @ Tioga   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 29, 2018</strong></td>
                    <td> MSRF  </td>
                    <td>  </td>
                    <td> 2YOFP  $20,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 29, 2018</strong></td>
                    <td> MSRF  </td>
                    <td>  </td>
                    <td> 2YOCT  $20,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 29, 2018</strong></td>
                    <td> Erwin Dygert </td>
                    <td>  </td>
                    <td>  3YOCT  $15,000 @ Hawthorne   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 29, 2018</strong></td>
                    <td> Beulah Dygert </td>
                    <td>  </td>
                    <td>  3YOFT  $15,000 @ Hawthorne   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 30, 2018</strong></td>
                    <td> NYSS </td>
                    <td>  </td>
                    <td>  3YOCP   $144,000 @ Yonkers  Excelsior 3YOCP  $15,000 @ Yonkers  MSRF 2YOCP  $20,000 @ Ocean  MSRF 2YOFT  $20,000 @ Ocean  Battle of Waterloo Elim 2YOP  $15,000 @ Grand River  Battle of the Belles Elim 2YOFP  $12,000 @ Grand River   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 30, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 3YOCP  $15,000 @ Yonkers   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 30, 2018</strong></td>
                    <td> MSRF  </td>
                    <td>  </td>
                    <td> 2YOCP  $20,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 30, 2018</strong></td>
                    <td> MSRF  </td>
                    <td>  </td>
                    <td> 2YOFT  $20,000 @ Ocean   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 30, 2018</strong></td>
                    <td> Battle of Waterloo Elim </td>
                    <td>  </td>
                    <td>  2YOP  $15,000 @ Grand River   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 30, 2018</strong></td>
                    <td> Battle of the Belles Elim </td>
                    <td>  </td>
                    <td>  2YOFP  $12,000 @ Grand River   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 31, 2018</strong></td>
                    <td> NYSS </td>
                    <td>  </td>
                    <td>  2YOCT   $142,000 @ Saratoga  Excelsior 2YOCT  $15,000 @ Saratoga  INSS Final 2YOCT  $75,000 @ Hoosier  INSS Final 2YOFT  $75,000 @ Hoosier  INSS Cons 2YOCT  $20,000 @ Hoosier  INSS Cons 2YOFT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 31, 2018</strong></td>
                    <td> Excelsior  </td>
                    <td>  </td>
                    <td> 2YOCT  $15,000 @ Saratoga   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 31, 2018</strong></td>
                    <td> INSS Final </td>
                    <td>  </td>
                    <td>  2YOCT  $75,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 31, 2018</strong></td>
                    <td> INSS Final </td>
                    <td>  </td>
                    <td>  2YOFT  $75,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 31, 2018</strong></td>
                    <td> INSS Cons </td>
                    <td>  </td>
                    <td>  2YOCT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
                <tr>
                    <td><strong>Jul 31, 2018</strong></td>
                    <td> INSS Cons  </td>
                    <td>  </td>
                    <td> 2YOFT  $20,000 @ Hoosier   </td>
                    <td>  </td>
                </tr>

            
        <tr>
            <td class="dateUpdated center" colspan="5">
                <em id='updateemp'>Updated ss June 29, 2018.</em> US Racing <a href="//www.usracing.com/harness-racing-schedule">Harness Racing Schedule</a>.
            </td>
        </tr>
        </tbody>
    </table>
    <a href="//www.usracing.com" style="color:white;">Online Horse Betting</a>
</div>

