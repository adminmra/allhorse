

<style>

{literal}
    /* TABLES AND DATA ================================================================================ */
/*
    #infoEntries, #raceEntries, #raceTimes { margin: 10px 0 0 0; padding: 0; width: 100%; }
    #infoEntries tr, #raceEntries tr, #raceTimes tr, #infoEntries tbody { border: none;}
    #infoEntries th, #raceEntries th { border-right: 1px solid #6ca8ee; padding: 7px 10px; color: #FFF; font-size: 1em; background: #105ca9; }
    #infoEntries td, #raceEntries td {  padding: 10px; font-size: 1em;}
    #infoEntries td.odd, #raceEntries tr.AlternateRow  { background: #e5e5e5; }
    #infoEntries .td-box {  display: block; border-bottom: 1px solid #d9d9d9; padding: 9px 10px; }
    #infoEntries .td-box:hover {  display: block; background: #d4e9ff; }

    #raceTimes table td { padding: 10px; font-size: 1em; border: none; }
    #raceTimes, #raceTimes table { border: none;  }
    #raceTimes table tr.odd { background: #d4e9ff;  }
    #raceTimes td.num { font-weight: bold; }
    #raceTimes tbody, #raceTimes table tbody { border: none; }

    .page-racingschedule #infoEntries td { width:14.2%;}
    .page-racingschedule #infoEntries th { font-size: .917em; }
    */
{/literal}

</style>


<div class="table-responsive">
    <table border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="Harness Racing Schedule" summary="This week's harness racing schedule for all US and Canadian racetracks.">
        <caption>* Harness Racing Schedule</caption>
         <tbody>
            <!--
            <tr>
                <th colspan="3" class="center"> * Harness Racing Schedule * Horses - 2016 Kentucky Derby  - May 06 </th>
            </tr>
            <tr>
                <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
            </tr>
            -->

           <tr>
             <th>Date</th>
             <th>Race</th>
             <th>Track</th>
             <th>Age & Sex</th>
             <th>Purse</th>
           </tr>

        
        <tr>
            <td class="dateUpdated center" colspan="5">
                <em id='updateemp'>Updated June 9, 2016.</em> US Racing <a href="//www.usracing.com/harness-racing-schedule">Harness Racing Schedule</a>.
            </td>
        </tr>
        </tbody>
    </table>
    <a href="//www.usracing.com" style="color:white;">Online Horse Betting</a>
</div>

