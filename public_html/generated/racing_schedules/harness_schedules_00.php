

<style>

{literal}
    /* TABLES AND DATA ================================================================================ */
/*
    #infoEntries, #raceEntries, #raceTimes { margin: 10px 0 0 0; padding: 0; width: 100%; }
    #infoEntries tr, #raceEntries tr, #raceTimes tr, #infoEntries tbody { border: none;}
    #infoEntries th, #raceEntries th { border-right: 1px solid #6ca8ee; padding: 7px 10px; color: #FFF; font-size: 1em; background: #105ca9; }
    #infoEntries td, #raceEntries td {  padding: 10px; font-size: 1em;}
    #infoEntries td.odd, #raceEntries tr.AlternateRow  { background: #e5e5e5; }
    #infoEntries .td-box {  display: block; border-bottom: 1px solid #d9d9d9; padding: 9px 10px; }
    #infoEntries .td-box:hover {  display: block; background: #d4e9ff; }

    #raceTimes table td { padding: 10px; font-size: 1em; border: none; }
    #raceTimes, #raceTimes table { border: none;  }
    #raceTimes table tr.odd { background: #d4e9ff;  }
    #raceTimes td.num { font-weight: bold; }
    #raceTimes tbody, #raceTimes table tbody { border: none; }

    .page-racingschedule #infoEntries td { width:14.2%;}
    .page-racingschedule #infoEntries th { font-size: .917em; }
    */
{/literal}

</style>


<div class="table-responsive">
    <table border="1" class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="Harness Racing Schedule" summary="This week's harness racing schedule for all US and Canadian racetracks.">
        <caption>Harness Racing Schedule</caption>
         <tbody>
            <!--
            <tr>
                <th colspan="3" class="center"> * Harness Racing Schedule * Horses - 2016 Kentucky Derby  - May 06 </th>
            </tr>
            <tr>
                <th colspan="3" class="center">Kentucky Derby - Odds To Win</th>
            </tr>
            -->

           <tr>
             <th width="16%">Date</th>
             <th width="40%">Stakes</th>
             <th>Track</th>
             <th>Age & Sex</th>
             <th>Purse</th>
           </tr>

        
                <tr>
                    <td><strong>Sep 28, 2017</strong></td>
                    <td> Bluegrass </td>
                    <td> Red Mile </td>
                    <td>  2YOCT </td>
                    <td>  $300,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 28, 2017</strong></td>
                    <td> INSS </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  3YOCT </td>
                    <td> $20,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 28, 2017</strong></td>
                    <td> INSS </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  3YOFP </td>
                    <td> $20,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 29, 2017</strong></td>
                    <td> Bluegrass </td>
                    <td> Red Mile </td>
                    <td>  2YOFT </td>
                    <td>  $350,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 29, 2017</strong></td>
                    <td> Dayton Trotting Derby </td>
                    <td> Dayton </td>
                    <td>  4&amp;UT </td>
                    <td> $160,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 29, 2017</strong></td>
                    <td> Dayton Pacing Derby </td>
                    <td> Dayton </td>
                    <td>  4&amp;UP </td>
                    <td> $150,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 29, 2017</strong></td>
                    <td> Renaissance Final </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  2YOFP </td>
                    <td> $60,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 29, 2017</strong></td>
                    <td> NJ SDF Final </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  3YOFT </td>
                    <td> $50,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 29, 2017</strong></td>
                    <td> Simpson </td>
                    <td> Philadelphia </td>
                    <td>  2YOFT </td>
                    <td> $70,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 29, 2017</strong></td>
                    <td> Simpson </td>
                    <td> Philadelphia </td>
                    <td>  2YOFP </td>
                    <td> $51,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 29, 2017</strong></td>
                    <td> INSS </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  2YOCP </td>
                    <td> $20,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 29, 2017</strong></td>
                    <td> INSS </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  2YOFP </td>
                    <td> $20,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 29, 2017</strong></td>
                    <td> INSS </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  2YOFT </td>
                    <td> $20,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 30, 2017</strong></td>
                    <td> Bluegrass </td>
                    <td> Red Mile </td>
                    <td>  2YOCP </td>
                    <td>  $400,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 30, 2017</strong></td>
                    <td> Bluegrass </td>
                    <td> Red Mile </td>
                    <td>  2YOFP </td>
                    <td> $300,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 30, 2017</strong></td>
                    <td> Renaissance Final </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  2YOP </td>
                    <td> $75,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 30, 2017</strong></td>
                    <td> NJ SDF </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  3YOCT </td>
                    <td> $10,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 30, 2017</strong></td>
                    <td> Keystone Classic </td>
                    <td> Meadows </td>
                    <td>  3YOFP </td>
                    <td> $72,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 30, 2017</strong></td>
                    <td> Buckeye Stallion Ser. Final </td>
                    <td> <a href='//www.usracing.com/northfield-park'>Northfield</a> </td>
                    <td>  2YOCP </td>
                    <td> $40,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 30, 2017</strong></td>
                    <td> Buckeye Stallion Ser. Final </td>
                    <td> <a href='//www.usracing.com/northfield-park'>Northfield</a> </td>
                    <td>  2YOFP </td>
                    <td> $40,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 30, 2017</strong></td>
                    <td> Buckeye Stallion Ser. Final </td>
                    <td> <a href='//www.usracing.com/northfield-park'>Northfield</a> </td>
                    <td>  3YOCP </td>
                    <td> $40,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 30, 2017</strong></td>
                    <td> Buckeye Stallion Ser. Final </td>
                    <td> <a href='//www.usracing.com/northfield-park'>Northfield</a> </td>
                    <td>  3YOFP </td>
                    <td> $40,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 30, 2017</strong></td>
                    <td> Buckeye Stallion Ser. Final </td>
                    <td> <a href='//www.usracing.com/northfield-park'>Northfield</a> </td>
                    <td>  2YOCT  </td>
                    <td> $40,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 30, 2017</strong></td>
                    <td> Buckeye Stallion Ser. Final </td>
                    <td> <a href='//www.usracing.com/northfield-park'>Northfield</a> </td>
                    <td>  2YOFT  </td>
                    <td> $40,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 30, 2017</strong></td>
                    <td> Buckeye Stallion Ser. Final </td>
                    <td> <a href='//www.usracing.com/northfield-park'>Northfield</a> </td>
                    <td>  3YOCT  </td>
                    <td> $40,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Sep 30, 2017</strong></td>
                    <td> Buckeye Stallion Ser. Final </td>
                    <td> <a href='//www.usracing.com/northfield-park'>Northfield</a> </td>
                    <td>  3YOFT  </td>
                    <td> $40,000  </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 1, 2017</strong></td>
                    <td> Bluegrass </td>
                    <td> Red Mile </td>
                    <td>  3YOCP  </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 1, 2017</strong></td>
                    <td> Bluegrass   </td>
                    <td> Red Mile </td>
                    <td> 3YOFP </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 1, 2017</strong></td>
                    <td> Bluegrass   </td>
                    <td> Red Mile </td>
                    <td> 3YOCT </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 1, 2017</strong></td>
                    <td> Bluegrass   </td>
                    <td> Red Mile </td>
                    <td> 3YOFT </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 1, 2017</strong></td>
                    <td> Simpson   </td>
                    <td> Red Mile </td>
                    <td> 2YOCP </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 1, 2017</strong></td>
                    <td> Simpson   </td>
                    <td> Red Mile </td>
                    <td> 2YOCT </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 2, 2017</strong></td>
                    <td> ONSS Gold </td>
                    <td> <a href='//www.usracing.com/mohawk-raceway'>Mohawk</a> </td>
                    <td>  3YOFP  </td>
                    <td> $180,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 2, 2017</strong></td>
                    <td> ONSS Gold   </td>
                    <td> <a href='//www.usracing.com/mohawk-raceway'>Mohawk</a> </td>
                    <td> 3YOCP </td>
                    <td> $180,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 3, 2017</strong></td>
                    <td> ONSS Gold </td>
                    <td> <a href='//www.usracing.com/western-fair-raceway'>Western Fair</a> </td>
                    <td>  2YOCP  </td>
                    <td> $180,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 4, 2017</strong></td>
                    <td> ONSS Gold </td>
                    <td> <a href='//www.usracing.com/mohawk-raceway'>Mohawk</a> </td>
                    <td>  2YOFT  </td>
                    <td> $180,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 4, 2017</strong></td>
                    <td> DSBF Final   </td>
                    <td> <a href='//www.usracing.com/mohawk-raceway'>Mohawk</a> </td>
                    <td> 2YOCP </td>
                    <td> $180,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 4, 2017</strong></td>
                    <td> DSBF Final   </td>
                    <td> <a href='//www.usracing.com/mohawk-raceway'>Mohawk</a> </td>
                    <td> 2YOFP </td>
                    <td> $180,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 4, 2017</strong></td>
                    <td> DSBF Final   </td>
                    <td> <a href='//www.usracing.com/mohawk-raceway'>Mohawk</a> </td>
                    <td> 2YOCT </td>
                    <td> $180,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 4, 2017</strong></td>
                    <td> DSBF Final   </td>
                    <td> <a href='//www.usracing.com/mohawk-raceway'>Mohawk</a> </td>
                    <td> 2YOFT </td>
                    <td> $180,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 4, 2017</strong></td>
                    <td> INSS Final   </td>
                    <td> <a href='//www.usracing.com/mohawk-raceway'>Mohawk</a> </td>
                    <td> 2YOCT </td>
                    <td> $180,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 4, 2017</strong></td>
                    <td> INSS Cons.   </td>
                    <td> <a href='//www.usracing.com/mohawk-raceway'>Mohawk</a> </td>
                    <td> 2YOCT </td>
                    <td> $180,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 5, 2017</strong></td>
                    <td> Int. Stallion </td>
                    <td> Red Mile </td>
                    <td>  2YOCT  </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 5, 2017</strong></td>
                    <td> ONSS Gold  </td>
                    <td> Red Mile </td>
                    <td>  2YOCT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 5, 2017</strong></td>
                    <td> ONSS Gold  </td>
                    <td> Red Mile </td>
                    <td>  2YOFP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 5, 2017</strong></td>
                    <td> INSS Final   </td>
                    <td> Red Mile </td>
                    <td> 3YOCT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 5, 2017</strong></td>
                    <td> INSS Final   </td>
                    <td> Red Mile </td>
                    <td> 3YOFP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 5, 2017</strong></td>
                    <td> INSS Cons.   </td>
                    <td> Red Mile </td>
                    <td> 3YOCT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 5, 2017</strong></td>
                    <td> INSS Cons.  </td>
                    <td> Red Mile </td>
                    <td>  3YOFP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 5, 2017</strong></td>
                    <td> INSS   </td>
                    <td> Red Mile </td>
                    <td> 4&amp;UH&amp;GT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 5, 2017</strong></td>
                    <td> INSS   </td>
                    <td> Red Mile </td>
                    <td> 4&amp;UMT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 5, 2017</strong></td>
                    <td> NJ SDF   </td>
                    <td> Red Mile </td>
                    <td> 2YOCT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 6, 2017</strong></td>
                    <td> Int. Stallion </td>
                    <td> Red Mile </td>
                    <td>  2YOFT  </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 6, 2017</strong></td>
                    <td> INSS Final   </td>
                    <td> Red Mile </td>
                    <td> 2YOCP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 6, 2017</strong></td>
                    <td> INSS Final  </td>
                    <td> Red Mile </td>
                    <td>  2YOFP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 6, 2017</strong></td>
                    <td> INSS Final   </td>
                    <td> Red Mile </td>
                    <td> 2YOFT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 6, 2017</strong></td>
                    <td> INSS Cons.  </td>
                    <td> Red Mile </td>
                    <td>  2YOCP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 6, 2017</strong></td>
                    <td> INSS Cons.   </td>
                    <td> Red Mile </td>
                    <td> 2YOFP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 6, 2017</strong></td>
                    <td> INSS Cons.   </td>
                    <td> Red Mile </td>
                    <td> 2YOFT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 6, 2017</strong></td>
                    <td> INSS   </td>
                    <td> Red Mile </td>
                    <td> 4&amp;UH&amp;GP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 6, 2017</strong></td>
                    <td> INSS   </td>
                    <td> Red Mile </td>
                    <td> 4&amp;UMP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 6, 2017</strong></td>
                    <td> NJ SDF   </td>
                    <td> Red Mile </td>
                    <td> 2YOFP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 6, 2017</strong></td>
                    <td> NJ SDF  </td>
                    <td> Red Mile </td>
                    <td>  2YOFT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> Int. Stallion </td>
                    <td> Red Mile </td>
                    <td>  2YOCP  </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> Int. Stallion   </td>
                    <td> Red Mile </td>
                    <td> 2YOFP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> Allerage/TVG  </td>
                    <td> Red Mile </td>
                    <td>  3&amp;UP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> Allerage/TVG   </td>
                    <td> Red Mile </td>
                    <td> 3&amp;UT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> PA Fair Final   </td>
                    <td> Red Mile </td>
                    <td> 2YOCP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> PA Fair Final   </td>
                    <td> Red Mile </td>
                    <td> 2YOFP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> PA Fair Final  </td>
                    <td> Red Mile </td>
                    <td>  2YOCT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> PA Fair Final   </td>
                    <td> Red Mile </td>
                    <td> 2YOFT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> PA Fair Final   </td>
                    <td> Red Mile </td>
                    <td> 3YOCP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> PA Fair Final  </td>
                    <td> Red Mile </td>
                    <td>  3YOFP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> PA Fair Final  </td>
                    <td> Red Mile </td>
                    <td>  3YOCT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> PA Fair Final   </td>
                    <td> Red Mile </td>
                    <td> 3YOFT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> NJ SDF Final   </td>
                    <td> Red Mile </td>
                    <td> 3YOCT </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> NJ Stbd. Futurity   </td>
                    <td> Red Mile </td>
                    <td> 3YOP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 7, 2017</strong></td>
                    <td> NJ SDF  </td>
                    <td> Red Mile </td>
                    <td>  2YOCP </td>
                    <td> $300,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 8, 2017</strong></td>
                    <td> Kentucky Futurity </td>
                    <td> Red Mile </td>
                    <td>  3YOT  </td>
                    <td> $500,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 8, 2017</strong></td>
                    <td> Tattersalls   </td>
                    <td> Red Mile </td>
                    <td> 3YOP </td>
                    <td> $500,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 8, 2017</strong></td>
                    <td> KY Filly Futurity   </td>
                    <td> Red Mile </td>
                    <td> 3YOFT </td>
                    <td> $500,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 8, 2017</strong></td>
                    <td> Glen Garnsey Mem   </td>
                    <td> Red Mile </td>
                    <td> 3YOFP </td>
                    <td> $500,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 8, 2017</strong></td>
                    <td> Allerage/TVG  </td>
                    <td> Red Mile </td>
                    <td>  3&amp;Uf&amp;MP </td>
                    <td> $500,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 8, 2017</strong></td>
                    <td> Allerage/TVG  </td>
                    <td> Red Mile </td>
                    <td>  3&amp;Uf&amp;MT </td>
                    <td> $500,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 12, 2017</strong></td>
                    <td> Madison County </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  2YOFP  </td>
                    <td> $65,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 12, 2017</strong></td>
                    <td> Madison County   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 2YOFT </td>
                    <td> $65,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 12, 2017</strong></td>
                    <td> Circle City  </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  3YOFP </td>
                    <td> $65,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 12, 2017</strong></td>
                    <td> Circle City  </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  3YOFT </td>
                    <td> $65,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 12, 2017</strong></td>
                    <td> NJ SDF   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 2YOCT </td>
                    <td> $65,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> INSS Super Final </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  2YOCP  </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> INSS Super Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 2YOFP </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> INSS Super Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 2YOCT </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> INSS Super Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 2YOFT </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> INSS Super Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 3YOCP </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> INSS Super Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 3YOFP </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> INSS Super Final  </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  3YOCT </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> INSS Super Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 3YOFT </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> INSS Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 4&amp;UH&amp;GP </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> INSS Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 4&amp;UH&amp;GT </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> INSS Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 4&amp;UMP </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> INSS Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 4&amp;UMT </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> NJ Stbd. Futurity  </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  3YOT </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> NJ SDF  </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  2YOFP </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 13, 2017</strong></td>
                    <td> NJ SDF  </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  2YOFT </td>
                    <td> $200,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> International Trot </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td>  3&UT  </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> NYSS Final   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 2YOCP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> NYSS Final   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 2YOFP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> NYSS Final   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 2YOCT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> NYSS Final   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 2YOFT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> NYSS Final   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 3YOCP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> NYSS Final   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 3YOFP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> NYSS Final   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 3YOCT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> NYSS Final   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 3YOFT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> Yonkers Inv.   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 3&amp;UP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> Yonkers Inv.   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 3&amp;UT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> ONSS Gold Super Final  </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td>  2YOCP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> ONSS Gold Super Final  </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td>  2YOCT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> ONSS Gold Super Final  </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td>  2YOFP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> ONSS Gold Super Final   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 2YOFT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> ONSS Gold Super Final   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 3YOCP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> ONSS Gold Super Final   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 3YOCT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> ONSS Gold Super Final  </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td>  3YOFP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> ONSS Gold Super Final  </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td>  3YOFT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> Courageous Lady  </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td>  3YOFP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> OHSS   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 2YOCP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> OHSS   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 2YOFP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> OHSS   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 2YOCT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> OHSS   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 2YOFT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> OHSS   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 3YOCP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> OHSS   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 3YOFP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> OHSS   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 3YOCT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> OHSS   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 3YOFT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> Madison County  </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td>  2YOCP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> Madison County  </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td>  2YOCT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> Circle City  </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td>  3YOCP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> Circle City   </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td> 3YOCT </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 14, 2017</strong></td>
                    <td> NJ SDF  </td>
                    <td> <a href='//www.usracing.com/yonkers'>Yonkers</a> </td>
                    <td>  2YOCP </td>
                    <td> $1,000,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 16, 2017</strong></td>
                    <td> NYSS Cons. </td>
                    <td> Monticello </td>
                    <td>  2YOCP  </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 16, 2017</strong></td>
                    <td> NYSS Cons.   </td>
                    <td> Monticello </td>
                    <td> 2YOFP </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 16, 2017</strong></td>
                    <td> NYSS Cons.   </td>
                    <td> Monticello </td>
                    <td> 2YOCT </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 16, 2017</strong></td>
                    <td> NYSS Cons.  </td>
                    <td> Monticello </td>
                    <td>  2YOFT </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 16, 2017</strong></td>
                    <td> NYSS Cons.   </td>
                    <td> Monticello </td>
                    <td> 3YOCP </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 16, 2017</strong></td>
                    <td> NYSS Cons.   </td>
                    <td> Monticello </td>
                    <td> 3YOFP </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 16, 2017</strong></td>
                    <td> NYSS Cons.   </td>
                    <td> Monticello </td>
                    <td> 3YOCT </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 16, 2017</strong></td>
                    <td> NYSS Cons.   </td>
                    <td> Monticello </td>
                    <td> 3YOFT </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 19, 2017</strong></td>
                    <td> NJ SDF Final </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  2YOCT  </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 20, 2017</strong></td>
                    <td> NJ SDF Final </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  2YOFP  </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 20, 2017</strong></td>
                    <td> NJ SDF Final   </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td> 2YOFT </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 20, 2017</strong></td>
                    <td> Breeders Crown Elim  </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  2YOFP </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 20, 2017</strong></td>
                    <td> Breeders Crown Elim  </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  2YOFT </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 20, 2017</strong></td>
                    <td> Breeders Crown Elim   </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td> 3YOFP </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 20, 2017</strong></td>
                    <td> Breeders Crown Elim  </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  3YOFT </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 20, 2017</strong></td>
                    <td> Breeders Crown Elim  </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  4&amp;UMP </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 20, 2017</strong></td>
                    <td> Breeders Crown Elim  </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  4&amp;UMT </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 21, 2017</strong></td>
                    <td> NJ SDF Final </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  2YOCP  </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 21, 2017</strong></td>
                    <td> Breeders Crown Elim  </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  2YOCP </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 21, 2017</strong></td>
                    <td> Breeders Crown Elim   </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td> 2YOCT </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 21, 2017</strong></td>
                    <td> Breeders Crown Elim  </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  3YOCP </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 21, 2017</strong></td>
                    <td> Breeders Crown Elim  </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  3YOCT </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 21, 2017</strong></td>
                    <td> Breeders Crown Elim  </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  3&amp;UP </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 21, 2017</strong></td>
                    <td> Breeders Crown Elim  </td>
                    <td> <a href='//www.usracing.com/freehold'>Freehold</a> </td>
                    <td>  4&amp;UH&amp;GT </td>
                    <td> $50,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 26, 2017</strong></td>
                    <td> Kindergarten </td>
                    <td> <a href='//www.usracing.com/vernon-downs'>Vernon</a> </td>
                    <td>  2YOCP  </td>
                    <td> $10,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 26, 2017</strong></td>
                    <td> Kindergarten   </td>
                    <td> <a href='//www.usracing.com/vernon-downs'>Vernon</a> </td>
                    <td> 2YOFP </td>
                    <td> $10,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 26, 2017</strong></td>
                    <td> Kindergarten   </td>
                    <td> <a href='//www.usracing.com/vernon-downs'>Vernon</a> </td>
                    <td> 2YOCT </td>
                    <td> $10,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 26, 2017</strong></td>
                    <td> Kindergarten   </td>
                    <td> <a href='//www.usracing.com/vernon-downs'>Vernon</a> </td>
                    <td> 2YOFT </td>
                    <td> $10,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 27, 2017</strong></td>
                    <td> Breeders Crown Final </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  2YOFP  </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 27, 2017</strong></td>
                    <td> Breeders Crown Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 2YOFT </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 27, 2017</strong></td>
                    <td> Breeders Crown Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 3YOFP </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 27, 2017</strong></td>
                    <td> Breeders Crown Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 3YOFT </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 27, 2017</strong></td>
                    <td> Breeders Crown Final  </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  4&amp;UMP </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 27, 2017</strong></td>
                    <td> Breeders Crown Final  </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  4&amp;UMT </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 27, 2017</strong></td>
                    <td> Pegasus   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 3YOFP </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 27, 2017</strong></td>
                    <td> Pegasus   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 3YOFT </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 27, 2017</strong></td>
                    <td> NJ Stbd. Futurity  </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  2YOT </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 28, 2017</strong></td>
                    <td> Breeders Crown Final </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  2YOCP  </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 28, 2017</strong></td>
                    <td> Breeders Crown Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 2YOCT </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 28, 2017</strong></td>
                    <td> Breeders Crown Final  </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  3YOCP </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 28, 2017</strong></td>
                    <td> Breeders Crown Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 3YOCT </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 28, 2017</strong></td>
                    <td> Breeders Crown Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 4&amp;UH&amp;GT </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 28, 2017</strong></td>
                    <td> Breeders Crown Final   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 3&amp;UP </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 28, 2017</strong></td>
                    <td> Pegasus   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 3YOCP </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 28, 2017</strong></td>
                    <td> Pegasus   </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td> 3YOCT </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 28, 2017</strong></td>
                    <td> NJ Stbd. Futurity  </td>
                    <td> <a href='//www.usracing.com/hoosier-park'>Hoosier</a> </td>
                    <td>  2YOP </td>
                    <td> $600,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 29, 2017</strong></td>
                    <td> MDSS Final </td>
                    <td> <a href='//www.usracing.com/rosecroft'>Rosecroft</a> </td>
                    <td>  2YOCP  </td>
                    <td> $40,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 29, 2017</strong></td>
                    <td> MDSS Final  </td>
                    <td> <a href='//www.usracing.com/rosecroft'>Rosecroft</a> </td>
                    <td>  2YOFP </td>
                    <td> $40,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 29, 2017</strong></td>
                    <td> MDSS Final   </td>
                    <td> <a href='//www.usracing.com/rosecroft'>Rosecroft</a> </td>
                    <td> 2YOCT </td>
                    <td> $40,000 </td>
                </tr>

            
                <tr>
                    <td><strong>Oct 29, 2017</strong></td>
                    <td> MDSS Final  </td>
                    <td> <a href='//www.usracing.com/rosecroft'>Rosecroft</a> </td>
                    <td>  2YOFT </td>
                    <td> $40,000 </td>
                </tr>

            
        <tr>
            <td class="dateUpdated center" colspan="5">
                <em id='updateemp'>Updated September 28, 2017.</em> US Racing <a href="//www.usracing.com/harness-racing-schedule">Harness Racing Schedule</a>.
            </td>
        </tr>
        </tbody>
    </table>
    <a href="//www.usracing.com" style="color:white;">Online Horse Betting</a>
</div>

