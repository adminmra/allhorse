<div id="no-more-tables">
    <table  border="1" class="data table table-condensed table-striped table-bordered ordenableResult" width="100%" cellpadding="0" cellspacing="0" title="Harness Racing Schedule" summary="This week's harness racing schedule for all US and Canadian racetracks.">
<thead>
           <tr>
             <th width="16%">Date</th>
             <th width="40%">Stakes</th>
             <th>Track</th>
             <th>Age & Sex</th>
             <th>Purse</th>
           </tr>
           </thead>

         <tbody>        



  <tr>
    <td data-title="Date">Nov 16,2021</td>
    <td data-title="Stakes">DSBF 2nd Leg </td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">2 C&amp;G T</td>
    <td data-title="Purse">$60,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 16,2021</td>
    <td data-title="Stakes">DSBF 2nd Leg </td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">2 F T</td>
    <td data-title="Purse">$40,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 16,2021</td>
    <td data-title="Stakes">ELECTION NIGHT SER. Final </td>
    <td data-title="Track">Northfield Park</td>
    <td data-title="Age &amp; Sex">2&amp;UP H&amp;G T</td>
    <td data-title="Purse">$25,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 17,2021</td>
    <td data-title="Stakes">DSBF 2nd Leg </td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">2 F P</td>
    <td data-title="Purse">$40,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 17,2021</td>
    <td data-title="Stakes">AUTUMN LEAVES SER. Final </td>
    <td data-title="Track">Northfield Park</td>
    <td data-title="Age &amp; Sex">2&amp;UP F&amp;M T</td>
    <td data-title="Purse">$36,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 17,2021</td>
    <td data-title="Stakes">PROGRESS PACE Elim</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 OPEN P</td>
    <td data-title="Purse">$35,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 18,2021</td>
    <td data-title="Stakes">THE STAR DESTROYER Div</td>
    <td data-title="Track">Harrah's Hoosier Park</td>
    <td data-title="Age &amp; Sex">3 C&amp;G P</td>
    <td data-title="Purse">$50,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 18,2021</td>
    <td data-title="Stakes">THE STAR DESTROYER Div</td>
    <td data-title="Track">Harrah's Hoosier Park</td>
    <td data-title="Age &amp; Sex">3 C&amp;G T</td>
    <td data-title="Purse">$50,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 18,2021</td>
    <td data-title="Stakes">THE STAR DESTROYER Div</td>
    <td data-title="Track">Harrah's Hoosier Park</td>
    <td data-title="Age &amp; Sex">3 F P</td>
    <td data-title="Purse">$50,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 18,2021</td>
    <td data-title="Stakes">THE STAR DESTROYER Div</td>
    <td data-title="Track">Harrah's Hoosier Park</td>
    <td data-title="Age &amp; Sex">3 F T</td>
    <td data-title="Purse">$50,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 19,2021</td>
    <td data-title="Stakes">HARVEST SER. (ONT) Final</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 C&amp;G P</td>
    <td data-title="Purse">$40,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 19,2021</td>
    <td data-title="Stakes">HARVEST SER. (ONT) Final</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 C&amp;G T</td>
    <td data-title="Purse">$40,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">GOVERNOR'S CUP Top $</td>
    <td data-title="Track">The Meadowlands</td>
    <td data-title="Age &amp; Sex">2 C&amp;G P</td>
    <td data-title="Purse">$400,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">VALLEY VICTORY Top $</td>
    <td data-title="Track">The Meadowlands</td>
    <td data-title="Age &amp; Sex">2 C&amp;G T</td>
    <td data-title="Purse">$400,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">THREE DIAMONDS Top $</td>
    <td data-title="Track">The Meadowlands</td>
    <td data-title="Age &amp; Sex">2 F P</td>
    <td data-title="Purse">$400,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">GOLDSMITH MAID Top $</td>
    <td data-title="Track">The Meadowlands</td>
    <td data-title="Age &amp; Sex">2 F T</td>
    <td data-title="Purse">$400,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">TVG Final</td>
    <td data-title="Track">The Meadowlands</td>
    <td data-title="Age &amp; Sex">3&amp;UP OPEN P</td>
    <td data-title="Purse">$350,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">TVG Final</td>
    <td data-title="Track">The Meadowlands</td>
    <td data-title="Age &amp; Sex">3&amp;UP OPEN T</td>
    <td data-title="Purse">$350,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">TVG Final</td>
    <td data-title="Track">The Meadowlands</td>
    <td data-title="Age &amp; Sex">3&amp;UP F&amp;M P</td>
    <td data-title="Purse">$175,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">TVG Final</td>
    <td data-title="Track">The Meadowlands</td>
    <td data-title="Age &amp; Sex">3&amp;UP F&amp;M T</td>
    <td data-title="Purse">$175,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">GOVERNOR'S CUP Conso</td>
    <td data-title="Track">The Meadowlands</td>
    <td data-title="Age &amp; Sex">2 C&amp;G P</td>
    <td data-title="Purse">$80,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">VALLEY VICTORY Conso</td>
    <td data-title="Track">The Meadowlands</td>
    <td data-title="Age &amp; Sex">2 C&amp;G T</td>
    <td data-title="Purse">$80,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">THREE DIAMONDS Conso</td>
    <td data-title="Track">The Meadowlands</td>
    <td data-title="Age &amp; Sex">2 F P</td>
    <td data-title="Purse">$80,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">GOLDSMITH MAID Conso</td>
    <td data-title="Track">The Meadowlands</td>
    <td data-title="Age &amp; Sex">2 F T</td>
    <td data-title="Purse">$80,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">HALLOWEEN SER. Final</td>
    <td data-title="Track">Northfield Park</td>
    <td data-title="Age &amp; Sex">2&amp;UP F&amp;M P</td>
    <td data-title="Purse">$25,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 20,2021</td>
    <td data-title="Stakes">HARVEST SER. (ONT) 1st Leg</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 F P</td>
    <td data-title="Purse">$17,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 22,2021</td>
    <td data-title="Stakes">DSBF Final</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">2 C&amp;G P</td>
    <td data-title="Purse">$100,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 22,2021</td>
    <td data-title="Stakes">HARVEST SER. (ONT) 1st Leg</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 F T</td>
    <td data-title="Purse">$17,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 23,2021</td>
    <td data-title="Stakes">DSBF Final</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">2 C&amp;G T</td>
    <td data-title="Purse">$100,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 23,2021</td>
    <td data-title="Stakes">DSBF Final</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">2 F T</td>
    <td data-title="Purse">$100,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 24,2021</td>
    <td data-title="Stakes">PROGRESS PACE Final</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 OPEN P</td>
    <td data-title="Purse">$300,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 24,2021</td>
    <td data-title="Stakes">DSBF Final</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">2 F P</td>
    <td data-title="Purse">$100,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 25,2021</td>
    <td data-title="Stakes">THANKSGIVING CLASSIC Div</td>
    <td data-title="Track">Harrah's Hoosier Park</td>
    <td data-title="Age &amp; Sex">3 C&amp;G P</td>
    <td data-title="Purse">$60,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 25,2021</td>
    <td data-title="Stakes">THANKSGIVING CLASSIC Div</td>
    <td data-title="Track">Harrah's Hoosier Park</td>
    <td data-title="Age &amp; Sex">3 C&amp;G T</td>
    <td data-title="Purse">$60,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 25,2021</td>
    <td data-title="Stakes">THANKSGIVING CLASSIC Div</td>
    <td data-title="Track">Harrah's Hoosier Park</td>
    <td data-title="Age &amp; Sex">3 F P</td>
    <td data-title="Purse">$60,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 25,2021</td>
    <td data-title="Stakes">THANKSGIVING CLASSIC Div</td>
    <td data-title="Track">Harrah's Hoosier Park</td>
    <td data-title="Age &amp; Sex">3 F T</td>
    <td data-title="Purse">$60,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 27,2021</td>
    <td data-title="Stakes">HARVEST SER. (ONT) 2nd Leg</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 F P</td>
    <td data-title="Purse">$17,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 29,2021</td>
    <td data-title="Stakes">DSBF 1st Leg</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 F T</td>
    <td data-title="Purse">$20,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 29,2021</td>
    <td data-title="Stakes">HARVEST SER. (ONT) 2nd Leg</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 F T</td>
    <td data-title="Purse">$17,000</td>
  </tr>
  <tr>
    <td data-title="Date">Nov 30,2021</td>
    <td data-title="Stakes">DSBF 1st Leg</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 C&amp;G T</td>
    <td data-title="Purse">$20,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 1,2021</td>
    <td data-title="Stakes">DSBF 1st Leg</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 F P</td>
    <td data-title="Purse">$20,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 2,2021</td>
    <td data-title="Stakes">DSBF 1st Leg</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 C&amp;G P</td>
    <td data-title="Purse">$20,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 4,2021</td>
    <td data-title="Stakes">HARVEST SER. (ONT) Final</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 F P</td>
    <td data-title="Purse">$40,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 6,2021</td>
    <td data-title="Stakes">HARVEST SER. (ONT) Final</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 F T</td>
    <td data-title="Purse">$40,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 6,2021</td>
    <td data-title="Stakes">DSBF 2nd Leg</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 F T</td>
    <td data-title="Purse">$20,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 7,2021</td>
    <td data-title="Stakes">DSBF 2nd Leg</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 C&amp;G T</td>
    <td data-title="Purse">$20,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 8,2021</td>
    <td data-title="Stakes">DSBF 2nd Leg</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 F P</td>
    <td data-title="Purse">$20,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 9,2021</td>
    <td data-title="Stakes">DSBF 2nd Leg</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 C&amp;G P</td>
    <td data-title="Purse">$20,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 10,2021</td>
    <td data-title="Stakes">NIAGARA 1st Leg</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 F P</td>
    <td data-title="Purse">$17,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 11,2021</td>
    <td data-title="Stakes">VALEDICTORY 1st Leg</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 C&amp;G P</td>
    <td data-title="Purse">$17,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 16,2021</td>
    <td data-title="Stakes">DSBF Final</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 C&amp;G P</td>
    <td data-title="Purse">$100,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 16,2021</td>
    <td data-title="Stakes">DSBF Final</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 C&amp;G T</td>
    <td data-title="Purse">$100,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 16,2021</td>
    <td data-title="Stakes">DSBF Final</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 F P</td>
    <td data-title="Purse">$100,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 16,2021</td>
    <td data-title="Stakes">DSBF Final</td>
    <td data-title="Track">Dover Downs</td>
    <td data-title="Age &amp; Sex">3 F T</td>
    <td data-title="Purse">$100,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 17,2021</td>
    <td data-title="Stakes">NIAGARA 2nd Leg</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 F P</td>
    <td data-title="Purse">$17,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 18,2021</td>
    <td data-title="Stakes">VALEDICTORY 2nd Leg</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 C&amp;G P</td>
    <td data-title="Purse">$17,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 26,2021</td>
    <td data-title="Stakes">VALEDICTORY Final</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 C&amp;G P</td>
    <td data-title="Purse">$30,000</td>
  </tr>
  <tr>
    <td data-title="Date">Dec 26,2021</td>
    <td data-title="Stakes">NIAGARA Final</td>
    <td data-title="Track">Woodbine Mohawk Park</td>
    <td data-title="Age &amp; Sex">3 F P</td>
    <td data-title="Purse">$30,000</td>
  </tr>   

        </tbody>
        </table>
    
</div>

<!--
	<div>
        <table id='infoEntries' width='100%' border='0' cellspacing='0' cellpadding='0' class='table table-condensed table-striped table-bordered' >
        <tbody>
        <tr>
            <td class="dateUpdated center">
                <em id='updateemp'>Updated May 20, 2019</em>
            </td>
        </tr>
        </tbody>
        </table>
        </div>
	-->

{literal}
    <style type="text/css">
        table.ordenableResult th{cursor: pointer}
    </style>
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sorttable_date.js"></script>
{/literal}
