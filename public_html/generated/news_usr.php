            <style type="text/css">
                @media screen and (min-width: 320px ) and (max-width: 640px) {
                    .media-left {
                        display: block;
                    }
                    .media-left img { width: 100%; }
                    .media-body .media-inside p { padding: 9px; }
                    .media-body .media-inside .media-heading { margin-top: 12px;text-align: center; }
                }
                @media screen and (min-width: 737px ) and (max-width: 900px) {
                    .table-container article.media {
                        width: 450px;
                        margin-left: auto;
                        margin-right: auto;
                    }
                    .media-left {
                        display: block;
                        padding-right: 0;
                    }
                    .media-left img { width: 100%; }
                    .media-body .media-inside p { padding: 9px; }
                    .media-body .media-inside .media-heading { margin-top: 12px;text-align: center; }
                }
            </style>
                    <article class="media thumbnail">
                <div class="media-left">
                                    <a href="https://www.usracing.com/news/horse-betting-101/betting-101-difference-probability-profitability" title="Betting 101: The Difference Between Probability &#038; Profitability">
                        <img class="media-object img-thumbnail" src="https://www.usracing.com/news/wp-content/uploads/2016/01/650013-show-bc-betting-ticket-300x225.jpg" alt="" />
                    </a>
                                    </div>
                <div class="media-body">
                    <div class="media-inside">
                        <h3 class="media-heading"><a href="https://www.usracing.com/news/horse-betting-101/betting-101-difference-probability-profitability">Betting 101: The Difference Between Probability &#038; Profitability</a></h3>
                        <p>If there is one mistake that I see both new and veteran handicappers make time and time again it is confusing probability with profitability — often in very inconsistent and haphazard ways. For instance, most bettors know that the post-time favorite wins approximately 1/3 of the time, making it a highly predictive factor. In fact, [&hellip;]</p>
                    </div>
                </div>
            </article>
                    <article class="media thumbnail">
                <div class="media-left">
                                    <a href="https://www.usracing.com/news/horse-betting-101/money-management-techniques" title="Money Management Techniques">
                        <img class="media-object img-thumbnail" src="https://www.usracing.com/news/wp-content/uploads/2017/06/CORB0817-300x225.jpg" alt="" />
                    </a>
                                    </div>
                <div class="media-body">
                    <div class="media-inside">
                        <h3 class="media-heading"><a href="https://www.usracing.com/news/horse-betting-101/money-management-techniques">Money Management Techniques</a></h3>
                        <p>Ask bettors why they lose and/or what keeps them from making more money gambling and, chances are, most will point to a lack of money management acumen — “my betting sucks” being a popular, if not exactly eloquent, refrain. “The number one reason most sports gamblers lose isn&#8217;t because of poor picks, but instead is [&hellip;]</p>
                    </div>
                </div>
            </article>
                    <article class="media thumbnail">
                <div class="media-left">
                                    <a href="https://www.usracing.com/news/harness-racing/elimination-extravaganza-pocono-downs-dominates-weekend-harness-fun" title="Elimination Extravaganza at Pocono Downs Dominates Weekend Harness Fun">
                        <img class="media-object img-thumbnail" src="https://www.usracing.com/news/wp-content/uploads/2017/06/Fear-The-Dragon_harnesslink-300x225.jpg" alt="" />
                    </a>
                                    </div>
                <div class="media-body">
                    <div class="media-inside">
                        <h3 class="media-heading"><a href="https://www.usracing.com/news/harness-racing/elimination-extravaganza-pocono-downs-dominates-weekend-harness-fun">Elimination Extravaganza at Pocono Downs Dominates Weekend Harness Fun</a></h3>
                        <p>Elimination races are fun to watch, kind of like watching a Little League baseball game. Betting on them, however, is like being the dad watching his kid play in a Little League game, shouting from the stands for him to stop jamming his index finger up his nose or else he’ll never make it to [&hellip;]</p>
                    </div>
                </div>
            </article>
                    <article class="media thumbnail">
                <div class="media-left">
                                    <a href="https://www.usracing.com/news/diamond-jubilee-stakes-highlights-final-day-royal-ascot" title="Diamond Jubilee Stakes Highlights Final Day of Royal Ascot">
                        <img class="media-object img-thumbnail" src="https://www.usracing.com/news/wp-content/uploads/2017/06/The-Right-Man_Jesse-Caris-300x225.jpg" alt="" />
                    </a>
                                    </div>
                <div class="media-body">
                    <div class="media-inside">
                        <h3 class="media-heading"><a href="https://www.usracing.com/news/diamond-jubilee-stakes-highlights-final-day-royal-ascot">Diamond Jubilee Stakes Highlights Final Day of Royal Ascot</a></h3>
                        <p>Saturday marks the final day of the prestigious annual Royal Ascot meeting at Ascot Racecourse outside London and after a week of fallen records, both in temperatures and course records, only one Group 1 race remains on the six-race closing day card. The weather has returned to more typical London-type conditions and a light drizzle [&hellip;]</p>
                    </div>
                </div>
            </article>
                    <article class="media thumbnail">
                <div class="media-left">
                                    <a href="https://www.usracing.com/news/dartmouth-seeks-defend-title-hardwicke" title="Dartmouth Seeks to Defend Title in Hardwicke">
                        <img class="media-object img-thumbnail" src="https://www.usracing.com/news/wp-content/uploads/2017/06/Dartmouth_Woodbine-Entertainment-300x225.jpg" alt="" />
                    </a>
                                    </div>
                <div class="media-body">
                    <div class="media-inside">
                        <h3 class="media-heading"><a href="https://www.usracing.com/news/dartmouth-seeks-defend-title-hardwicke">Dartmouth Seeks to Defend Title in Hardwicke</a></h3>
                        <p>A field of 14 will go postward in the third race on closing day at Royal Ascot — the 1 ½-mile Hardwicke Stakes (GII) for 4-year-olds and upward. The Queen’s Dartmouth, who won this event last year, is back to defend his title off a win in the Yorkshire Cup (GII) at York last month. [&hellip;]</p>
                    </div>
                </div>
            </article>
                    <article class="media thumbnail">
                <div class="media-left">
                                    <a href="https://www.usracing.com/news/horse-betting-101/astounding-truth-first-time-starters" title="The Astounding Truth About First-Time Starters">
                        <img class="media-object img-thumbnail" src="https://www.usracing.com/news/wp-content/uploads/2016/05/AroundTurn-300x225.jpg" alt="" />
                    </a>
                                    </div>
                <div class="media-body">
                    <div class="media-inside">
                        <h3 class="media-heading"><a href="https://www.usracing.com/news/horse-betting-101/astounding-truth-first-time-starters">The Astounding Truth About First-Time Starters</a></h3>
                        <p>One of the great mysteries of the world — less decipherable to some than quantum theory, chatty dentists or the “poke” option on Facebook — is first-time starters. For those who are equally befuddled by the plot twists on &#8220;Keeping Up with the Kardashians&#8221;, first-time starters are horses that have never raced before and they [&hellip;]</p>
                    </div>
                </div>
            </article>
                    <article class="media thumbnail">
                <div class="media-left">
                                    <a href="https://www.usracing.com/news/recap/highland-reel-proves-best-prince-waless-ascot" title="Highland Reel Proves Best in Prince of Wales’s at Ascot">
                        <img class="media-object img-thumbnail" src="https://www.usracing.com/news/wp-content/uploads/2017/06/Highland-Reel_Ascot-Racecourse2-300x225.jpg" alt="" />
                    </a>
                                    </div>
                <div class="media-body">
                    <div class="media-inside">
                        <h3 class="media-heading"><a href="https://www.usracing.com/news/recap/highland-reel-proves-best-prince-waless-ascot">Highland Reel Proves Best in Prince of Wales’s at Ascot</a></h3>
                        <p>Magnier, Tabor and Smith’s Longines Breeders’ Cup Turf (GI) hero Highland Reel, who may be the most well-traveled racehorse in the world, having been to America, the Far East and Australia, returned home to Great Britain and captured the richest race of the current Royal Ascot meeting, taking top honors by 1 ¼ lengths in [&hellip;]</p>
                    </div>
                </div>
            </article>
                    <article class="media thumbnail">
                <div class="media-left">
                                    <a href="https://www.usracing.com/news/gold-cup-highlights-third-day-royal-ascot" title="Gold Cup Highlights Third Day of Royal Ascot">
                        <img class="media-object img-thumbnail" src="https://www.usracing.com/news/wp-content/uploads/2016/06/Ascot5-300x225.png" alt="" />
                    </a>
                                    </div>
                <div class="media-body">
                    <div class="media-inside">
                        <h3 class="media-heading"><a href="https://www.usracing.com/news/gold-cup-highlights-third-day-royal-ascot">Gold Cup Highlights Third Day of Royal Ascot</a></h3>
                        <p>Of the six races set for Royal Ascot on Thursday, the about 2 ½-mile Gold Cup (GI) is the main attraction and, in the race, which used to be known as the “Ascot Gold Cup” but was renamed to just “Gold Cup” in honor of Queen Elizabeth II’s birthday last year, is probably Europe’s most [&hellip;]</p>
                    </div>
                </div>
            </article>
        