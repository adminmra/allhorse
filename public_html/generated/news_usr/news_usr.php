<?php
$news = file_get_contents('https://www.usracing.com/news/index.php');
$doc = new DOMDocument();

$doc->loadHTML($news);
$element_list = get_element_by_tag_name($doc,'article');
$a= convert_dom_to_array($element_list);
$array = convert_content_to_array($a);

for ($i = 0; $i <= sizeof($array); $i++){
    $linktoPost = $array[$i]["div"]["0"]["a"]["@attributes"]["href"];
    $image = $array[$i]["div"]["0"]["a"]["img"]["@attributes"]["src"];
    $title = $array[$i]["div"]["2"]["h3"]["a"];
    $newDescription = $array[$i]["div"]["2"]["div"]["1"];
	
	
    $posted = date("Y/m/d");
    if ( $newDescription ) {  
    
    echo("<article class=\"media thumbnail\">");
    echo("<div class=\"media-left\">");
    echo("<a href=\"https:". $linktoPost . " \" title = \"" . $title ."\">");
    echo("<img class=\"media-object img-thumbnail\" src=\"https:" . $image . "\" alt=\"\" >");
    echo("</a>");
    echo("</div>");
    echo("<div class=\"media-body\">");
    echo("<div class=\"media-inside\">");
    echo("<h3 class=\"media-heading\"><a href=\"https:" . $linktoPost . "\"> " . $title . "</a></h3>");
    echo("<p>". $newDescription . "</p>");
    
    echo("</div>");
    echo("</div>");
    echo("</article>");
    //print_r($array[$i]["div"]["2"]["div"]["1"]);
    } 
}

function get_element_by_tag_name($doc,$tagname){
    return $doc->getElementsByTagName($tagname);
}
function convert_dom_to_array($dom){
    return iterator_to_array($dom);
}
function convert_content_to_array($content){
    $arr = array();
    foreach ( $content as $c ){
        $arr[] = json_decode(json_encode(clean_up_xml((array)simplexml_import_dom($c))),1);
    }
    return $arr;
}
function clean_up_xml($xml){
    foreach ($xml as $k=>$v){
        if ( is_object($v) || is_array($v) ) {
            $xml[$k] = clean_up_xml((array)$v);
        } else {
             $xml[$k] = preg_replace('/[^(\x20-\x7F)\x0A\x0D]*/','',$v);
        }
    }
    return $xml;
}
?>
