         <div id="no-more-tables">
		<table id="gradedStakes" class="table table-condensed table-striped table-bordered ordenableResult" border="0" cellspacing="0" cellpadding="0" width="100%">
			<thead>
				<tr>
					<th width="16%"><strong>Date</strong></th>
					<th><strong>Stakes</strong></th>
					<th><strong>Track</strong></th>
					<!-- <th width="10%"><strong>Grade</strong></th> -->
					<th><strong>Purses</strong></th>
					<!-- <th width="10%"><strong>Age/Sex</strong></th>
					<th width="10%"><strong>DS</strong></th> -->
										<!-- <th width="12%"><strong>Jockey</strong></th>
					<th width="12%"><strong>Trainer</strong></th> -->
				</tr>
                </thead>
                <tbody>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Red Smith					</td>

					<td  data-title="Track" >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Chilukki Stakes					</td>

					<td  data-title="Track" >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Native Diver Stakes					</td>

					<td  data-title="Track" >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Tokyo Sports Hai Nisai Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Mile Championship					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,362,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Falls City					</td>

					<td  data-title="Track" >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Red Carpet Stakes					</td>

					<td  data-title="Track" >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>11f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Comely Stakes					</td>

					<td  data-title="Track" >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Clark Presented by Norton HealthCare					</td>

					<td  data-title="Track" >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$750,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hollywood Turf Cup					</td>

					<td  data-title="Track" >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Long Island					</td>

					<td  data-title="Track" >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Golden Rod Stakes					</td>

					<td  data-title="Track" >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kentucky Jockey Club Stakes					</td>

					<td  data-title="Track" >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hollywood Derby					</td>

					<td  data-title="Track" >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Jimmy Durante Stakes					</td>

					<td  data-title="Track" >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Seabiscuit Handicap					</td>

					<td  data-title="Track" >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Radio Nikkei Hai Kyoto Nisai Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$678,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kennedy Road Stakes					</td>

					<td  data-title="Track" >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$175,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fall Highweight Handicap					</td>

					<td  data-title="Track" >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Cecil B. DeMille Stakes					</td>

					<td  data-title="Track" >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Matriarch Stakes					</td>

					<td  data-title="Track" >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Japan Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$6,172,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Keihan Hai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$808,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Grey Stakes					</td>

					<td  data-title="Track" >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8.5f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Mazarine Stakes					</td>

					<td  data-title="Track" >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>8.5f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Cigar Mile Handicap					</td>

					<td  data-title="Track" >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$750,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Demoiselle Stakes					</td>

					<td  data-title="Track" >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Go for Wand Handicap					</td>

					<td  data-title="Track" >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Remsen Stakes					</td>

					<td  data-title="Track" >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Challenge Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Stayers Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,283,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>18f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Starlet					</td>

					<td  data-title="Track" >
						<span  >Los Alamitos</span>
						<!-- Los Alamitos -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Champions Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,058,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Bayakoa Stakes					</td>

					<td  data-title="Track" >
						<span  >Los Alamitos</span>
						<!-- Los Alamitos -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Valedictory Stakes					</td>

					<td  data-title="Track" >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Mr. Prospector Stakes					</td>

					<td  data-title="Track" >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Chunichi Shimbun Hai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Los Alamitos Futurity					</td>

					<td  data-title="Track" >
						<span  >Los Alamitos</span>
						<!-- Los Alamitos -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 12, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Capella Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$737,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 12, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hanshin Juvenile Fillies					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,336,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fort Lauderdale Stakes					</td>

					<td  data-title="Track" >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Sugar Swirl Stakes					</td>

					<td  data-title="Track" >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Suwannee River Stakes					</td>

					<td  data-title="Track" >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Turquoise Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$737,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 19, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Asahi Hai Futurity Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,450,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hanshin Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,383,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>7f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Arima Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$6,172,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						American Oaks					</td>

					<td  data-title="Track" >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						La Brea Stakes					</td>

					<td  data-title="Track" >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Mathis Brothers Mile					</td>

					<td  data-title="Track" >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Runhappy Malibu Stakes					</td>

					<td  data-title="Track" >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						San Antonio Stakes					</td>

					<td  data-title="Track" >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						San Gabriel Stakes					</td>

					<td  data-title="Track" >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Dec 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hopeful Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,450,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Dec 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Robert J. Frankel Stakes					</td>

					<td  data-title="Track" >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
							</tbody>
		</table>
        </div>
        	<div>
        <table id='infoEntries' width='100%' border='0' cellspacing='0' cellpadding='0' class='table table-condensed table-striped table-bordered' >
        <tbody>
        <tr>
            <td class="dateUpdated center">
                <em id='updateemp'>Updated November 16, 2021.</em>
            </td>
        </tr>
        </tbody>
        </table>
        </div>
		