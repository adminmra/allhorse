                        <div class="stakes-blue" id="grade-stake-1">
                        <div class="graded-date"><span class="bold">Nov 20</span></div>
                        <div class="graded-race">
                    
                        <p><span class="bold">Aqueduct:</span>  Red Smith ,$200,000 ,3&amp;up ,11f Turf</p>
                        
                        
                        <p><span class="bold">Churchill Downs:</span>  Chilukki Stakes ,$300,000 ,3&amp;up f/m ,8f Dirt</p>
                        
                        
                        <p><span class="bold">Del Mar:</span>  Native Diver Stakes ,$100,000 ,3&amp;up ,9f Dirt</p>
                        
                        
                        <p><span class="bold">Japan Racing Association:</span>  Tokyo Sports Hai Nisai Stakes ,$780,000 ,2yo ,9f Turf</p>
                        
                        </div>
                     </div>                        <div class="stakes-yellow" id="grade-stake-2">
                        <div class="graded-date"><span class="bold">Nov 21</span></div>
                        <div class="graded-race">
                    
                        <p><span class="bold">Japan Racing Association:</span>  Mile Championship ,$2,362,000 ,3&amp;up ,8f Turf</p>
                        
                        </div>
                     </div>                        <div class="stakes-blue" id="grade-stake-3">
                        <div class="graded-date"><span class="bold">Nov 25</span></div>
                        <div class="graded-race">
                    
                        <p><span class="bold">Churchill Downs:</span>  Falls City ,$500,000 ,3&amp;up f/m ,9f Dirt</p>
                        
                        
                        <p><span class="bold">Del Mar:</span>  Red Carpet Stakes ,$100,000 ,3&amp;up f/m ,11f Turf</p>
                        
                        </div>
                     </div>                        <div class="stakes-yellow" id="grade-stake-4">
                        <div class="graded-date"><span class="bold">Nov 26</span></div>
                        <div class="graded-race">
                    
                        <p><span class="bold">Aqueduct:</span>  Comely Stakes ,$200,000 ,3yo f ,9f Dirt</p>
                        
                        
                        <p><span class="bold">Churchill Downs:</span>  Clark Presented by Norton HealthCare ,$750,000 ,3&amp;up ,9f Dirt</p>
                        
                        
                        <p><span class="bold">Del Mar:</span>  Hollywood Turf Cup ,$250,000 ,3&amp;up ,12f Turf</p>
                        
                        </div>
                     </div>