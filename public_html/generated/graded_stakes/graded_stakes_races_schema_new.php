
		<div class="table-responsive">
					<table class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="0" width="100%">
			<tbody>
				<tr>
					<th width="16%"><strong>Date</strong></th> 
					<th><strong>Stakes</strong></th>
					<th><strong>Track</strong></th>
					<!-- <th width="10%"><strong>Grade</strong></th> -->
					<th width="16%"><strong>Purses</strong></th>
					<!-- <th width="10%"><strong>Age/Sex</strong></th>
					<th width="10%"><strong>DS</strong></th> -->
										<!-- <th width="12%"><strong>Jockey</strong></th>
					<th width="12%"><strong>Trainer</strong></th> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 13, 2018						</time>
					</td>

					<td  >
						Apple Blossom Handicap					</td>

					<td  >
						<span > Oaklawn Park </span> 
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$700,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 13, 2018						</time>
					</td>

					<td  >
						Fantasy Stakes					</td>

					<td  >
						<span  >Oaklawn Park</span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 14, 2018						</time>
					</td>

					<td  >
						Oaklawn Handicap					</td>

					<td  >
						<span  >Oaklawn Park</span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$750,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 14, 2018						</time>
					</td>

					<td  >
						Count Fleet Sprint Handicap					</td>

					<td  >
						<span  >Oaklawn Park</span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 14, 2018						</time>
					</td>

					<td  >
						Arkansas Derby					</td>

					<td  >
						<span  >Oaklawn Park</span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,000,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 14, 2018						</time>
					</td>

					<td  >
						Stonestreet Lexington Stakes					</td>

					<td  >
						<span  >Keeneland</span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 14, 2018						</time>
					</td>

					<td  >
						Coolmore Jenny Wiley Stakes					</td>

					<td  >
						<span  >Keeneland</span>
						<!-- Keeneland -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$350,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 14, 2018						</time>
					</td>

					<td  >
						Ben Ali Stakes					</td>

					<td  >
						<span  >Keeneland</span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->


				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 14, 2018						</time>
					</td>

					<td  >
						Arlington Cup					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$676,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 15, 2018						</time>
					</td>

					<td  >
						Antares Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$639,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 15, 2018						</time>
					</td>

					<td  >
						Satsuki Sho (Japanese Two Thousand Guineas)					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,971,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 20, 2018						</time>
					</td>

					<td  >
						Hilliard Lyons Doubledogdare Stakes					</td>

					<td  >
						<span  >Keeneland</span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 21, 2018						</time>
					</td>

					<td  >
						Temple Gwathmey Steeplechase					</td>

					<td  >
						<span  >Middleburg</span>
						<!-- Middleburg -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$75,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>20f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 21, 2018						</time>
					</td>

					<td  >
						Dixiana Elkhorn Stakes					</td>

					<td  >
						<span  >Keeneland</span>
						<!-- Keeneland -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 21, 2018						</time>
					</td>

					<td  >
						Fukushima Himba Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$676,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 21, 2018						</time>
					</td>

					<td  >
						Charles Town Classic					</td>

					<td  >
						<span  >Hollywood Casino At Charles Town Races</span>
						<!-- Hollywood Casino At Charles Town Races -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$1,200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 22, 2018						</time>
					</td>

					<td  >
						Flora Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$929,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 22, 2018						</time>
					</td>

					<td  >
						Yomiuri Milers Cup					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$1,059,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 27, 2018						</time>
					</td>

					<td  >
						Bewitch Stakes					</td>

					<td  >
						<span  >Keeneland</span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 28, 2018						</time>
					</td>

					<td  >
						Whimsical Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$125,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 28, 2018						</time>
					</td>

					<td  >
						Aoba Sho					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$971,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 29, 2018						</time>
					</td>

					<td  >
						Tenno Sho Spring					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$2,685,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>16f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 2, 2018						</time>
					</td>

					<td  >
						Longines Sagaro Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$81,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>16f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 2, 2018						</time>
					</td>

					<td  >
						Merriebelle Stable Pavilion Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$108,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>6f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 4, 2018						</time>
					</td>

					<td  >
						Turf Sprint					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 4, 2018						</time>
					</td>

					<td  >
						Longines Kentucky Oaks					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,000,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 4, 2018						</time>
					</td>

					<td  >
						La Troienne Stakes					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$350,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 4, 2018						</time>
					</td>

					<td  >
						Eight Belles Presented by Kentucky Trailer					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 4, 2018						</time>
					</td>

					<td  >
						Edgewood Presented by Forcht Bank					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 4, 2018						</time>
					</td>

					<td  >
						Alysheba Stakes					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 5, 2018						</time>
					</td>

					<td  >
						Senorita Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 5, 2018						</time>
					</td>

					<td  >
						Kyoto Shimbun Hai					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$971,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>11f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 5, 2018						</time>
					</td>

					<td  >
						David Semmes Memorial Hurdle					</td>

					<td  >
						<span  >Great Meadow</span>
						<!-- Great Meadow -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$75,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>17f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 5, 2018						</time>
					</td>

					<td  >
						Pat Day Mile					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 5, 2018						</time>
					</td>

					<td  >
						Old Forester Turf Classic					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$500,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 5, 2018						</time>
					</td>

					<td  >
						Longines Churchill Distaff Turf Mile					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->



					<td  >
						$300,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 5, 2018						</time>
					</td>

					<td  >
						Kentucky Derby					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$2,000,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 5, 2018						</time>
					</td>

					<td  >
						Humana Distaff					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 5, 2018						</time>
					</td>

					<td  >
						Churchill Downs					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$500,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 5, 2018						</time>
					</td>

					<td  >
						American Turf					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 5, 2018						</time>
					</td>

					<td  >
						Westchester Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 5, 2018						</time>
					</td>

					<td  >
						Sheepshead Bay Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>11f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 5, 2018						</time>
					</td>

					<td  >
						Fort Marcy Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 6, 2018						</time>
					</td>

					<td  >
						Adoration Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 6, 2018						</time>
					</td>

					<td  >
						Steve Sexton Mile Stakes					</td>

					<td  >
						<span  >Lone Star Park</span>
						<!-- Lone Star Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">

					<td>
						<time  >
							May 6, 2018						</time>
					</td>

					<td  >
						Niigata Daishoten					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$726,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 6, 2018						</time>
					</td>

					<td  >
						NHK Mile Cup					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,874,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 6, 2018						</time>
					</td>

					<td  >
						Ruffian Handicap					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 12, 2018						</time>
					</td>

					<td  >
						Lazaro Barrera Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 12, 2018						</time>
					</td>

					<td  >
						Calvin Houghland Iroquois					</td>

					<td  >
						<span  >Percy Warner</span>
						<!-- Percy Warner -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>24f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 12, 2018						</time>
					</td>

					<td  >
						Keio Hai Spring Cup					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$1,059,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 12, 2018						</time>
					</td>

					<td  >
						Vagrancy Handicap					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 12, 2018						</time>
					</td>

					<td  >
						Peter Pan Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$350,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 12, 2018						</time>
					</td>

					<td  >
						Beaugay Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 12, 2018						</time>
					</td>

					<td  >
						Hanshin Cup Stakes					</td>



					<td  >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 13, 2018						</time>
					</td>

					<td  >
						Victoria Mile					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,874,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 13, 2018						</time>
					</td>

					<td  >
						Marine Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$125,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 18, 2018						</time>
					</td>

					<td  >
						Pimlico Special					</td>

					<td  >
						<span  >Pimlico</span>
						<!-- Pimlico -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 18, 2018						</time>
					</td>

					<td  >
						Black-Eyed Susan Stakes					</td>

					<td  >
						<span  >Pimlico</span>
						<!-- Pimlico -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 18, 2018						</time>
					</td>

					<td  >
						Allaire duPont Distaff Stakes					</td>

					<td  >
						<span  >Pimlico</span>
						<!-- Pimlico -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 18, 2018						</time>
					</td>

					<td  >
						Adena Springs Miss Preakness Stakes					</td>

					<td  >
						<span  >Pimlico</span>
						<!-- Pimlico -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 19, 2018						</time>
					</td>

					<td  >
						Selene Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$125,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 19, 2018						</time>
					</td>

					<td  >
						National Hunt Cup Hurdle					</td>

					<td  >
						<span  >Radnor</span>
						<!-- Radnor -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$50,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>19f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 19, 2018						</time>
					</td>

					<td  >
						Preakness Stakes					</td>

					<td  >
						<span  >Pimlico</span>
						<!-- Pimlico -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,500,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 19, 2018						</time>
					</td>

					<td  >
						Maryland Sprint Stakes					</td>

					<td  >
						<span  >Pimlico</span>
						<!-- Pimlico -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 19, 2018						</time>
					</td>

					<td  >
						Gallorette Stakes					</td>

					<td  >
						<span  >Pimlico</span>
						<!-- Pimlico -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 19, 2018						</time>
					</td>

					<td  >
						Dixie Stakes					</td>

					<td  >
						<span  >Pimlico</span>
						<!-- Pimlico -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 19, 2018						</time>
					</td>

					<td  >
						Heian Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$639,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 19, 2018						</time>
					</td>

					<td  >
						Louisville Handicap					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 19, 2018						</time>
					</td>

					<td  >
						Soaring Softly Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 19, 2018						</time>
					</td>

					<td  >
						Arlington Matron					</td>

					<td  >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 20, 2018						</time>
					</td>

					<td  >
						Yushun Himba (Japanese Oaks)					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,971,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 26, 2018						</time>
					</td>

					<td  >
						Eclipse Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$175,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 26, 2018						</time>
					</td>

					<td  >
						The Gold Cup at Santa Anita					</td>


					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$500,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 26, 2018						</time>
					</td>

					<td  >
						Gamely Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 26, 2018						</time>
					</td>

					<td  >
						Charles Whittingham Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 26, 2018						</time>
					</td>

					<td  >
						Salvator Mile Stakes					</td>

					<td  >
						<span  >Monmouth Park</span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 26, 2018						</time>
					</td>

					<td  >
						Monmouth Stakes					</td>

					<td  >
						<span  >Monmouth Park</span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 26, 2018						</time>
					</td>

					<td  >
						Winning Colors Stakes					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 26, 2018						</time>
					</td>

					<td  >
						Arlington Classic Stakes					</td>

					<td  >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 27, 2018						</time>
					</td>

					<td  >
						Nassau Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$175,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 27, 2018						</time>
					</td>

					<td  >
						Desert Stormer Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 27, 2018						</time>
					</td>

					<td  >
						Lone Star Park Handicap					</td>

					<td  >
						<span  >Lone Star Park</span>
						<!-- Lone Star Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 27, 2018						</time>
					</td>

					<td  >
						Tokyo Yushun (Japanese Derby)					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$3,570,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 27, 2018						</time>
					</td>

					<td  >
						Meguro Kinen					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$1,017,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 28, 2018						</time>
					</td>

					<td  >
						All American Stakes					</td>

					<td  >
						<span  >Golden Gate Fields</span>
						<!-- Golden Gate Fields -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							May 28, 2018						</time>
					</td>

					<td  >
						Monrovia Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>abt 6.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							May 28, 2018						</time>
					</td>

					<td  >
						Shoemaker Mile					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 2, 2018						</time>
					</td>

					<td  >
						Connaught Cup Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$175,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 2, 2018						</time>
					</td>

					<td  >
						Beholder Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 2, 2018						</time>
					</td>

					<td  >
						Penn Mile					</td>

					<td  >
						<span  >Penn National</span>
						<!-- Penn National -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$500,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 2, 2018						</time>
					</td>

					<td  >
						Naruo Kinen					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$726,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 2, 2018						</time>
					</td>

					<td  >
						Aristides					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 2, 2018						</time>
					</td>

					<td  >
						Pennine Ridge					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 3, 2018						</time>
					</td>

					<td  >
						Yasuda Kinen					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,971,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 7, 2018						</time>
					</td>

					<td  >
						Intercontinental					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 7, 2018						</time>
					</td>

					<td  >
						Wonder Again					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 8, 2018						</time>
					</td>

					<td  >
						True North Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 8, 2018						</time>
					</td>

					<td  >
						New York Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$600,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 8, 2018						</time>
					</td>

					<td  >
						Belmont Gold Cup Invitational					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>16f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 9, 2018						</time>
					</td>

					<td  >
						Honeymoon Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 9, 2018						</time>
					</td>

					<td  >
						Old Forester Mint Julep					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 9, 2018						</time>
					</td>

					<td  >
						Woody Stephens Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 9, 2018						</time>
					</td>

					<td  >
						Woodford Reserve Manhattan Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,000,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 9, 2018						</time>
					</td>

					<td  >
						Runhappy Metropolitan Handicap					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 9, 2018						</time>
					</td>

					<td  >
						Ogden Phipps Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$750,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 9, 2018						</time>
					</td>

					<td  >
						Longines Just a Game Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$700,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 9, 2018						</time>
					</td>

					<td  >
						Jaipur Invitational					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 9, 2018						</time>
					</td>

					<td  >
						Brooklyn Invitational					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 9, 2018						</time>
					</td>

					<td  >
						Belmont Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,500,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>12f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 9, 2018						</time>
					</td>

					<td  >
						Acorn Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$700,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 10, 2018						</time>
					</td>

					<td  >
						Hendrie Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$125,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6.5f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 10, 2018						</time>
					</td>

					<td  >
						Affirmed Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 10, 2018						</time>
					</td>

					<td  >
						Mermaid Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$639,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 10, 2018						</time>
					</td>

					<td  >
						Epsom Cup					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$726,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 16, 2018						</time>
					</td>

					<td  >
						Summertime Oaks					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 16, 2018						</time>
					</td>

					<td  >
						Wise Dan Stakes					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 16, 2018						</time>
					</td>

					<td  >
						Stephen Foster Handicap					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$500,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 16, 2018						</time>
					</td>

					<td  >
						Regret Stakes					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 16, 2018						</time>
					</td>

					<td  >
						Matt Winn Stakes					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 16, 2018						</time>
					</td>

					<td  >
						Fleur de Lis Handicap					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 17, 2018						</time>
					</td>

					<td  >
						Poker Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 17, 2018						</time>
					</td>

					<td  >
						Hakodate Sprint Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$700,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 17, 2018						</time>
					</td>

					<td  >
						Unicorn Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$625,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 17, 2018						</time>
					</td>

					<td  >
						Trillium Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$125,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 19, 2018						</time>
					</td>

					<td  >
						Queen Anne Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$810,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 19, 2018						</time>
					</td>

					<td  >
						Coventry Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$203,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>6f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 20, 2018						</time>
					</td>

					<td  >
						Duke of Cambridge Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$236,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 20, 2018						</time>
					</td>

					<td  >
						Jersey Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$122,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 20, 2018						</time>
					</td>

					<td  >
						Queen Mary Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$149,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->

				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 21, 2018						</time>
					</td>

					<td  >
						Ribblesdale Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$270,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 21, 2018						</time>
					</td>

					<td  >
						Norfolk Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$135,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 21, 2018						</time>
					</td>

					<td  >
						Hampton Court Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$122,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 21, 2018						</time>
					</td>

					<td  >
						Gold Cup					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$675,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>20f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 22, 2018						</time>
					</td>

					<td  >
						Albany Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$122,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>6f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 22, 2018						</time>
					</td>

					<td  >
						Commonwealth Cup					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$675,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>6f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 22, 2018						</time>
					</td>

					<td  >
						Coronation Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$675,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 22, 2018						</time>
					</td>

					<td  >
						King Edward VII Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$304,000					</td>

					<!-- <td>3yo c/g</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 23, 2018						</time>
					</td>

					<td  >
						Singspiel Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$125,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 23, 2018						</time>
					</td>

					<td  >
						Ohio Derby					</td>

					<td  >
						<span  >Thistledown</span>
						<!-- Thistledown -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$500,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->

										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 23, 2018						</time>
					</td>

					<td  >
						San Carlos Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 23, 2018						</time>
					</td>

					<td  >
						Hardwicke Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$304,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 23, 2018						</time>
					</td>

					<td  >
						Diamond Jubilee Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$810,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 23, 2018						</time>
					</td>

					<td  >
						Chicago Handicap					</td>

					<td  >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>7f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 24, 2018						</time>
					</td>

					<td  >
						Takarazuka Kinen					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$2,685,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 24, 2018						</time>
					</td>

					<td  >
						Wilshire Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 30, 2018						</time>
					</td>

					<td  >
						United Nations Stakes					</td>

					<td  >
						<span  >Monmouth Park</span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 30, 2018						</time>
					</td>

					<td  >
						Dance Smartly Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$175,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 30, 2018						</time>
					</td>

					<td  >
						Highlander Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 30, 2018						</time>
					</td>

					<td  >
						King Edward Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$175,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 30, 2018						</time>
					</td>

					<td  >
						Philip H. Iselin Stakes					</td>

					<td  >
						<span  >Monmouth Park</span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 30, 2018						</time>
					</td>

					<td  >
						Eatontown Stakes					</td>

					<td  >
						<span  >Monmouth Park</span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 30, 2018						</time>
					</td>

					<td  >
						Smile Sprint Handicap					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 30, 2018						</time>
					</td>

					<td  >
						Princess Rooney Handicap					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jun 30, 2018						</time>
					</td>

					<td  >
						Bashford Manor Stakes					</td>

					<td  >
						<span  >Churchill Downs</span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jun 30, 2018						</time>
					</td>

					<td  >
						Mother Goose Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 1, 2018						</time>
					</td>

					<td  >
						Dominion Day Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$125,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 1, 2018						</time>
					</td>

					<td  >
						Radio Nikkei Sho					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$676,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 1, 2018						</time>
					</td>

					<td  >
						CBC Sho					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$700,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 3, 2018						</time>
					</td>

					<td  >
						Dr. James Penny Memorial					</td>

					<td  >
						<span  >Parx Racing</span>
						<!-- Parx Racing -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 5, 2018						</time>
					</td>

					<td  >
						Iowa Oaks					</td>

					<td  >
						<span  >Prairie Meadows</span>
						<!-- Prairie Meadows -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 6, 2018						</time>
					</td>

					<td  >
						Prairie Meadows Cornhusker Handicap					</td>

					<td  >
						<span  >Prairie Meadows</span>
						<!-- Prairie Meadows -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 7, 2018						</time>
					</td>

					<td  >
						American Derby					</td>

					<td  >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 7, 2018						</time>
					</td>

					<td  >
						Parx Dash					</td>

					<td  >
						<span  >Parx Racing</span>
						<!-- Parx Racing -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 7, 2018						</time>
					</td>

					<td  >
						Robert G. Dick Memorial Stakes					</td>

					<td  >
						<span  >Delaware Park</span>
						<!-- Delaware Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>11f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 7, 2018						</time>
					</td>

					<td  >
						Delaware Oaks					</td>

					<td  >
						<span  >Delaware Park</span>
						<!-- Delaware Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 7, 2018						</time>
					</td>

					<td  >
						Suburban					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$700,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 7, 2018						</time>
					</td>

					<td  >
						Dwyer Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 7, 2018						</time>
					</td>

					<td  >
						Belmont Sprint Championship					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$350,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 7, 2018						</time>
					</td>

					<td  >
						Belmont Oaks Invitational					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,000,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 7, 2018						</time>
					</td>

					<td  >
						Belmont Derby Invitational					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 7, 2018						</time>
					</td>

					<td  >
						Stars and Stripes					</td>

					<td  >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 7, 2018						</time>
					</td>

					<td  >
						Modesty Handicap					</td>

					<td  >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 7, 2018						</time>
					</td>

					<td  >
						Arlington Handicap					</td>

					<td  >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 8, 2018						</time>
					</td>

					<td  >
						Tanabata Sho					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$726,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 8, 2018						</time>
					</td>

					<td  >
						Procyon Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$639,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 8, 2018						</time>
					</td>

					<td  >
						Victory Ride Stakes					</td>

					<td  >
						<span  >Belmont Park</span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 14, 2018						</time>
					</td>

					<td  >
						Fred Cowley MBE Memorial Summer Mile Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->


					<td  >
						$176,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 14, 2018						</time>
					</td>

					<td  >
						Delaware Handicap					</td>

					<td  >
						<span  >Delaware Park</span>
						<!-- Delaware Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$750,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>10f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 14, 2018						</time>
					</td>

					<td  >
						Kent Stakes					</td>

					<td  >
						<span  >Delaware Park</span>
						<!-- Delaware Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 14, 2018						</time>
					</td>

					<td  >
						Indiana Derby					</td>

					<td  >
						<span  >Indiana Grand</span>
						<!-- Indiana Grand -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$500,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 14, 2018						</time>
					</td>

					<td  >
						Indiana Oaks					</td>

					<td  >
						<span  >Indiana Grand</span>
						<!-- Indiana Grand -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 15, 2018						</time>
					</td>

					<td  >
						Ontario Matron Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$125,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 15, 2018						</time>
					</td>

					<td  >
						Hakodate Kinen					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$726,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 20, 2018						</time>
					</td>

					<td  >
						Lake George Stakes					</td>

					<td  >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 20, 2018						</time>
					</td>

					<td  >
						Schuylerville Stakes					</td>

					<td  >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 21, 2018						</time>
					</td>

					<td  >
						Ontario Colleen Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$125,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 21, 2018						</time>
					</td>

					<td  >
						Sanford Stakes					</td>

					<td  >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 21, 2018						</time>
					</td>

					<td  >
						Diana Stakes					</td>

					<td  >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$500,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 21, 2018						</time>
					</td>

					<td  >
						San Diego Handicap					</td>

					<td  >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 21, 2018						</time>
					</td>

					<td  >
						San Clemente Stakes					</td>

					<td  >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 22, 2018						</time>
					</td>

					<td  >
						Nijinsky Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$175,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 22, 2018						</time>
					</td>

					<td  >
						Coaching Club American Oaks					</td>

					<td  >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 22, 2018						</time>
					</td>

					<td  >
						Hakodate Nisai Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$550,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>6f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 22, 2018						</time>
					</td>

					<td  >
						Chukyo Kinen					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$700,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 22, 2018						</time>
					</td>

					<td  >
						Eddie Read Stakes					</td>

					<td  >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 25, 2018						</time>
					</td>

					<td  >
						Cougar II Handicap					</td>

					<td  >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 25, 2018						</time>
					</td>

					<td  >
						Honorable Miss Handicap					</td>

					<td  >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 26, 2018						</time>
					</td>

					<td  >
						A.P. Smithwick Memorial Steeplechase Handicap					</td>

					<td  >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>16.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 28, 2018						</time>
					</td>

					<td  >
						Jim Dandy Stakes					</td>

					<td  >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$600,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 28, 2018						</time>
					</td>

					<td  >
						Bowling Green					</td>

					<td  >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 28, 2018						</time>
					</td>

					<td  >
						Amsterdam Stakes					</td>

					<td  >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 28, 2018						</time>
					</td>

					<td  >
						Alfred G. Vanderbilt Handicap					</td>

					<td  >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$350,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 28, 2018						</time>
					</td>

					<td  >
						Bing Crosby Stakes					</td>

					<td  >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 28, 2018						</time>
					</td>

					<td  >
						Princess Margaret Keeneland Stakes					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$68,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>6f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 28, 2018						</time>
					</td>

					<td  >
						King George VI &amp; Queen Elizabeth Stakes (sponsored by QIPCO)					</td>

					<td  >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,688,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 29, 2018						</time>
					</td>

					<td  >
						Royal North Stakes					</td>

					<td  >
						<span  >Woodbine</span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$175,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 29, 2018						</time>
					</td>

					<td  >
						Shuvee Handicap					</td>

					<td  >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 29, 2018						</time>
					</td>

					<td  >
						WinStar Matchmaker Stakes					</td>

					<td  >
						<span  >Monmouth Park</span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 29, 2018						</time>
					</td>

					<td  >
						Oceanport Stakes					</td>

					<td  >
						<span  >Monmouth Park</span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 29, 2018						</time>
					</td>

					<td  >
						Monmouth Cup Stakes					</td>

					<td  >
						<span  >Monmouth Park</span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 29, 2018						</time>
					</td>

					<td  >
						Molly Pitcher Stakes					</td>

					<td  >
						<span  >Monmouth Park</span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 29, 2018						</time>
					</td>

					<td  >
						Haskell Invitational					</td>

					<td  >
						<span  >Monmouth Park</span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,000,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 29, 2018						</time>
					</td>

					<td  >
						Queen Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$639,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 29, 2018						</time>
					</td>

					<td  >
						Ibis Summer Dash					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$700,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>5f Turf</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jul 29, 2018						</time>
					</td>

					<td  >
						Clement L. Hirsch Stakes					</td>

					<td  >
						<span  >Del Mar</span>
						<!-- Del Mar -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jul 29, 2018						</time>
					</td>

					<td  >
						Vigil Stakes					</td>

					<td  >
						<span  >Woodbine</span> 
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$125,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f AW</td> -->
										<!-- 	<td></td>
					<td></td> -->
				</tr>
							</tbody>
		</table>
				</div> 

		 