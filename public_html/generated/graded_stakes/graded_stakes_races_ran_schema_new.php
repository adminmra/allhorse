		<div class="table-responsive">
					<table class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="0" width="100%">
			<tbody>
				<tr>
					<th width="16%"><strong>Date</strong></th>
					<th><strong>Stakes</strong></th>
					<th><strong>Track</strong></th>
					<!-- <th width="10%"><strong>Grade</strong></th> -->
					<th><strong>Purses</strong></th>
					<!-- <th width="10%"><strong>Age/Sex</strong></th>
					<th width="10%"><strong>DS</strong></th> -->
					<th><strong>Horse</strong></th>					<!-- <th width="12%"><strong>Jockey</strong></th>
					<th width="12%"><strong>Trainer</strong></th> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 8, 2018						</time> 
					</td>

					<td  >
						Beaumont Stakes					</td>

					<td  >
						<span  >Keeneland</span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>abt 7f Dirt</td> -->
					<td>Gas Station Sushi</td>					<!-- 	<td>C. Nakatani</td>
					<td>R. Baltas</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 8, 2018						</time>
					</td>

					<td  >
						Appalachian Stakes					</td>

					<td  >
						<span  >Keeneland</span>
						<!-- Keeneland -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Rushing Fall</td>					<!-- 	<td>J. Castellano</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 8, 2018						</time>
					</td>

					<td  >
						Oka Sho (Japanese One Thousand Guineas)					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,874,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Dernier Or (jpn)</td>					<!-- 	<td>K. Ikezoe</td>
					<td>Y. Ikee</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 8, 2018						</time>
					</td>

					<td  >
						Tokyo City Cup					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Dirt</td> -->
					<td>Hoppertunity</td>					<!-- 	<td>F. Prat</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						Carter Handicap					</td>

					<td  >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>Army Mule</td>					<!-- 	<td>J. Bravo</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						Hanshin Himba Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$984,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Denko Ange (jpn)</td>					<!-- 	<td>M. Ebina</td>
					<td>Y. Arakawa</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						Wood Memorial Stakes					</td>

					<td  >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$750,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>Vino Rosso</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						Gazelle Stakes					</td>

					<td  >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>My Miss Lilly</td>					<!-- 	<td>J. Bravo</td>
					<td>M. Hennig</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						Excelsior Stakes					</td>

					<td  >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>Discreet Lover</td>					<!-- 	<td>M. Franco</td>
					<td>U. St. Lewis</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						Bay Shore Stakes					</td>

					<td  >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>National Flag</td>					<!-- 	<td>F. Prat</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						New Zealand Trophy					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$971,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Icefjord (jpn)</td>					<!-- 	<td>H. Kitamura</td>
					<td>Y. Muto</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						Central Bank Ashland Stakes					</td>

					<td  >
						<span  >Keeneland</span>
						<!-- Keeneland -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$500,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Monomoy Girl</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						Santa Anita Derby					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,000,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>Justify</td>					<!-- 	<td>M. Smith</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						Santa Anita Oaks					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Midnight Bisou</td>					<!-- 	<td>M. Smith</td>
					<td>W. Spawr</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						Royal Heroine Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Beau Recall</td>					<!-- 	<td>J. Rosario</td>
					<td>S. Callaghan</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						Providencia Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td>Fatale Bere</td>					<!-- 	<td>J. Rosario</td>
					<td>L. Powell</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						Toyota Blue Grass Stakes					</td>

					<td  >
						<span  >Keeneland</span>
						<!-- Keeneland -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$1,000,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>Good Magic</td>					<!-- 	<td>J. Ortiz</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 7, 2018						</time>
					</td>

					<td  >
						Shakertown Stakes					</td>

					<td  >
						<span  >Keeneland</span>
						<!-- Keeneland -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>5.5f Turf</td> -->
					<td>Bound For Nowhere</td>					<!-- 	<td>J. Garcia</td>
					<td>W. Ward</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 6, 2018						</time>
					</td>

					<td  >
						Transylvania Stakes					</td>

					<td  >
						<span  >Keeneland</span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td>Analize It</td>					<!-- 	<td>J. Ortiz</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Apr 6, 2018						</time>
					</td>

					<td  >
						Distaff Handicap					</td>

					<td  >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>Holiday Disguise</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>L. Rice</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Apr 1, 2018						</time>
					</td>

					<td  >
						Osaka Hai					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$2,142,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td>Satono Noblesse (jpn)</td>					<!-- 	<td>H. Miyuki</td>
					<td>Y. Ikee</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 31, 2018						</time>
					</td>

					<td  >
						San Francisco Mile					</td>

					<td  >
						<span  >Golden Gate Fields</span>
						<!-- Golden Gate Fields -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Flamboyant</td>					<!-- 	<td>J. Couton</td>
					<td>P. Gallagher</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 31, 2018						</time>
					</td>

					<td  >
						Gulfstream Park Mile					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td>Conquest Big E</td>					<!-- 	<td>J. Batista</td>
					<td>D. Hurtak</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 31, 2018						</time>
					</td>

					<td  >
						Gulfstream Park Oaks					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Coach Rocks</td>					<!-- 	<td>L. Saez</td>
					<td>D. Romans</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 31, 2018						</time>
					</td>

					<td  >
						Honey Fox Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Lull</td>					<!-- 	<td>J. Ortiz</td>
					<td>C. Clement</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 31, 2018						</time>
					</td>

					<td  >
						Pan American Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td>Hi Happy</td>					<!-- 	<td>L. Saez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 31, 2018						</time>
					</td>

					<td  >
						Florida Derby					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,000,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>Audible</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 31, 2018						</time>
					</td>

					<td  >
						Lord Derby Challenge Trophy					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$700,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Maltese Apogee (jpn)</td>					<!-- 	<td>Y. Shibata</td>
					<td>M. Horii</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 25, 2018						</time>
					</td>

					<td  >
						March Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$639,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>Sakura Record (jpn)</td>					<!-- 	<td>Y. Shibata</td>
					<td>T. Tajima</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 25, 2018						</time>
					</td>

					<td  >
						Takamatsunomiya Kinen					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,971,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td>Snow Dragon (jpn)</td>					<!-- 	<td>T. Ono</td>
					<td>N. Takagi</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 25, 2018						</time>
					</td>

					<td  >
						Sunland Derby					</td>

					<td  >
						<span  >Sunland Park</span>
						<!-- Sunland Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$800,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>Runaway Ghost</td>					<!-- 	<td>T. Hebert</td>
					<td>T. Fincher</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 24, 2018						</time>
					</td>

					<td  >
						New Orleans Handicap					</td>

					<td  >
						<span  >Fair Grounds</span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>Good Samaritan</td>					<!-- 	<td>J. Rosario</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 24, 2018						</time>
					</td>

					<td  >
						Muniz Memorial Handicap					</td>

					<td  >
						<span  >Fair Grounds</span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>abt 9f Turf</td> -->
					<td>Synchrony</td>					<!-- 	<td>J. Bravo</td>
					<td>M. Stidham</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 24, 2018						</time>
					</td>

					<td  >
						Fair Grounds Oaks					</td>

					<td  >
						<span  >Fair Grounds</span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Chocolate Martini</td>					<!-- 	<td>M. Murrill</td>
					<td>T. Amoss</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 24, 2018						</time>
					</td>

					<td  >
						Louisiana Derby					</td>

					<td  >
						<span  >Fair Grounds</span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$1,000,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>Noble Indy</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 24, 2018						</time>
					</td>

					<td  >
						Mainichi Hai					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$676,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td>Blast Onepiece (jpn)</td>					<!-- 	<td>K. Ikezoe</td>
					<td>M. Otake</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 24, 2018						</time>
					</td>

					<td  >
						Nikkei Sho					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$1,199,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12.5f Turf</td> -->
					<td>Shonan Bach (jpn)</td>					<!-- 	<td>F. Minarik</td>
					<td>H. Uehara</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 24, 2018						</time>
					</td>

					<td  >
						Santa Monica Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>Selcourt</td>					<!-- 	<td>T. Baze</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 24, 2018						</time>
					</td>

					<td  >
						San Luis Rey Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td>Itsinthepost</td>					<!-- 	<td>T. Baze</td>
					<td>J. Mullins</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 18, 2018						</time>
					</td>

					<td  >
						Hanshin Daishoten					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$1,199,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>15f Turf</td> -->
					<td>Shiho (jpn)</td>					<!-- 	<td>F. Komaki</td>
					<td>K. Sasada</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 18, 2018						</time>
					</td>

					<td  >
						Spring Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$971,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td>Happy Grin (jpn)</td>					<!-- 	<td>T. Ono</td>
					<td>J. Tanaka</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 18, 2018						</time>
					</td>

					<td  >
						Santa Ana Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td>Madam Dancealot</td>					<!-- 	<td>C. Nakatani</td>
					<td>R. Baltas</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 17, 2018						</time>
					</td>

					<td  >
						Inside Information Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>Ivy Bell</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 17, 2018						</time>
					</td>

					<td  >
						Falcon Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$676,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Turf</td> -->
					<td>Taisei Avenir (jpn)</td>					<!-- 	<td>Y. Kitamura</td>
					<td>M. Nishimura</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 17, 2018						</time>
					</td>

					<td  >
						Flower Cup					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$625,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td>Bucket List (jpn)</td>					<!-- 	<td>F. Minarik</td>
					<td>F. Takahashi</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 17, 2018						</time>
					</td>

					<td  >
						Azeri Stakes					</td>

					<td  >
						<span  >Oaklawn Park</span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$350,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Martini Glass</td>					<!-- 	<td>P. Lopez</td>
					<td>K. Nations</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 17, 2018						</time>
					</td>

					<td  >
						Rebel Stakes					</td>

					<td  >
						<span  >Oaklawn Park</span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$900,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Magnum Moon</td>					<!-- 	<td>L. Saez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 17, 2018						</time>
					</td>

					<td  >
						Bourbonette Oaks					</td>

					<td  >
						<span  >Turfway Park</span>
						<!-- Turfway Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f AW</td> -->
					<td>Go Noni Go</td>					<!-- 	<td>T. Gaffalione</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 11, 2018						</time>
					</td>

					<td  >
						Kinko Sho					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$1,112,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td>Action Star (jpn)</td>					<!-- 	<td>K. Dazai</td>
					<td>H. Otonashi</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 10, 2018						</time>
					</td>

					<td  >
						Gotham Stakes					</td>

					<td  >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td>Enticed</td>					<!-- 	<td>J. Alvarado</td>
					<td>K. McLaughlin</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 10, 2018						</time>
					</td>

					<td  >
						Nakayama Himba Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$639,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td>Halteclere (jpn)</td>					<!-- 	<td>H. Mayuzumi</td>
					<td>E. Nakano</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 10, 2018						</time>
					</td>

					<td  >
						Honeybee Stakes					</td>

					<td  >
						<span  >Oaklawn Park</span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Cosmic Burst</td>					<!-- 	<td>R. Eramia</td>
					<td>D. Von Hemel</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 10, 2018						</time>
					</td>

					<td  >
						Santa Anita Handicap					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$600,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Dirt</td> -->
					<td>Accelerate</td>					<!-- 	<td>V. Espinoza</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 10, 2018						</time>
					</td>

					<td  >
						Triple Bend Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>City Of Light</td>					<!-- 	<td>D. Van Dyke</td>
					<td>M. McCarthy</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 10, 2018						</time>
					</td>

					<td  >
						Florida Oaks					</td>

					<td  >
						<span  >Tampa Bay Downs</span>
						<!-- Tampa Bay Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td>Andina Del Sur</td>					<!-- 	<td>J. Leparoux</td>
					<td>T. Albertrani</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 10, 2018						</time>
					</td>

					<td  >
						Hillsborough Stakes					</td>

					<td  >
						<span  >Tampa Bay Downs</span>
						<!-- Tampa Bay Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$225,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td>Fourstar Crook</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 10, 2018						</time>
					</td>

					<td  >
						Lambholm South Tampa Bay Derby					</td>

					<td  >
						<span  >Tampa Bay Downs</span>
						<!-- Tampa Bay Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Quip</td>					<!-- 	<td>F. Geroux</td>
					<td>R. Brisset</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 4, 2018						</time>
					</td>

					<td  >
						Yayoi Sho					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$971,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td>Asakusa Spot (jpn)</td>					<!-- 	<td>M. Muto</td>
					<td>H. Toda</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 3, 2018						</time>
					</td>

					<td  >
						Herecomesthebride Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td>Thewayiam</td>					<!-- 	<td>J. Ortiz</td>
					<td>H. Motion</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 3, 2018						</time>
					</td>

					<td  >
						Palm Beach Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td>Maraud</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 3, 2018						</time>
					</td>

					<td  >
						Very One Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9.5f Turf</td> -->
					<td>Holy Helena</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>J. Jerkens</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 3, 2018						</time>
					</td>

					<td  >
						Fountain of Youth Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Promises Fulfilled</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>D. Romans</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 3, 2018						</time>
					</td>

					<td  >
						Ocean Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$726,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td>Lord Quest (jpn)</td>					<!-- 	<td>K. Miura</td>
					<td>S. Kojima</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 3, 2018						</time>
					</td>

					<td  >
						Tulip Sho					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$929,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Red Landini (jpn)</td>					<!-- 	<td>S. Hamanaka</td>
					<td>S. Ishizaka</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 3, 2018						</time>
					</td>

					<td  >
						Santa Ysabel Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Midnight Bisou</td>					<!-- 	<td>M. Smith</td>
					<td>W. Spawr</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 3, 2018						</time>
					</td>

					<td  >
						Gulfstream Park Sprint					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
					<td>Classic Rock</td>					<!-- 	<td>L. Saez</td>
					<td>K. Ritvo</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Mar 3, 2018						</time>
					</td>

					<td  >
						Davona Dale Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td>Fly So High</td>					<!-- 	<td>J. Ortiz</td>
					<td>C. McGaughey III</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Mar 3, 2018						</time>
					</td>

					<td  >
						Canadian Turf Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Hogy</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 25, 2018						</time>
					</td>

					<td  >
						Nakayama Kinen					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$1,112,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td>Persian Knight (jpn)</td>					<!-- 	<td>M. Demuro</td>
					<td>Y. Ikee</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 25, 2018						</time>
					</td>

					<td  >
						Hankyu Hai					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$726,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Turf</td> -->
					<td>Moon Crest (jpn)</td>					<!-- 	<td>K. Ogino</td>
					<td>M. Honda</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 24, 2018						</time>
					</td>

					<td  >
						Daytona Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>abt 6.5f Turf</td> -->
					<td>Conquest Tsunami</td>					<!-- 	<td>V. Espinoza</td>
					<td>P. Miller</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 19, 2018						</time>
					</td>

					<td  >
						Southwest Stakes					</td>

					<td  >
						<span  >Oaklawn Park</span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$500,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>My Boy Jack</td>					<!-- 	<td>K. Desormeaux</td>
					<td>J. Desormeaux</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 19, 2018						</time>
					</td>

					<td  >
						Razorback Handicap					</td>

					<td  >
						<span  >Oaklawn Park</span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$500,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Hawaakom</td>					<!-- 	<td>C. Lanerie</td>
					<td>W. Hawley</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 19, 2018						</time>
					</td>

					<td  >
						Royal Delta Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td>Martini Glass</td>					<!-- 	<td>P. Lopez</td>
					<td>K. Nations</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 18, 2018						</time>
					</td>

					<td  >
						February Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$1,785,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td>T M Jinsoku (jpn)</td>					<!-- 	<td>Y. Furukawa</td>
					<td>K. Kihara</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 18, 2018						</time>
					</td>

					<td  >
						Kokura Daishoten					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$726,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td>Hit The Target (jpn)</td>					<!-- 	<td>F. Komaki</td>
					<td>K. Kato</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 18, 2018						</time>
					</td>

					<td  >
						Las Flores Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td>Selcourt</td>					<!-- 	<td>T. Baze</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 17, 2018						</time>
					</td>

					<td  >
						Fair Grounds Handicap					</td>

					<td  >
						<span  >Fair Grounds</span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>abt 9f Turf</td> -->
					<td>Synchrony</td>					<!-- 	<td>J. Bravo</td>
					<td>M. Stidham</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 17, 2018						</time>
					</td>

					<td  >
						Mineshaft Handicap					</td>

					<td  >
						<span  >Fair Grounds</span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>The Player</td>					<!-- 	<td>C. Borel</td>
					<td>W. Bradley</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 17, 2018						</time>
					</td>

					<td  >
						Rachel Alexandra Stakes					</td>

					<td  >
						<span  >Fair Grounds</span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Monomoy Girl</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 17, 2018						</time>
					</td>

					<td  >
						Risen Star Stakes					</td>

					<td  >
						<span  >Fair Grounds</span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Bravazo</td>					<!-- 	<td>M. Mena</td>
					<td>D. Lukas</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 17, 2018						</time>
					</td>

					<td  >
						Diamond Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$726,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>17f Turf</td> -->
					<td>Saimon Tornare (jpn)</td>					<!-- 	<td>T. Ito</td>
					<td>K. Kato</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 17, 2018						</time>
					</td>

					<td  >
						Kyoto Himba Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$639,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Turf</td> -->
					<td>Hautvillers (jpn)</td>					<!-- 	<td>H. Shii</td>
					<td>T. Konno</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 17, 2018						</time>
					</td>

					<td  >
						Barbara Fritchie Stakes					</td>

					<td  >
						<span  >Laurel Park</span>
						<!-- Laurel Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>Ms Locust Point</td>					<!-- 	<td>J. Vargas, Jr.</td>
					<td>J. Servis</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 17, 2018						</time>
					</td>

					<td  >
						General George Stakes					</td>

					<td  >
						<span  >Laurel Park</span>
						<!-- Laurel Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>Something Awesome</td>					<!-- 	<td>E. Trujillo</td>
					<td>J. Corrales</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 17, 2018						</time>
					</td>

					<td  >
						Bayakoa Stakes					</td>

					<td  >
						<span  >Oaklawn Park</span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Streamline</td>					<!-- 	<td>G. Stevens</td>
					<td>B. Williamson</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 12, 2018						</time>
					</td>

					<td  >
						Queen Cup					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$625,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Hathor (jpn)</td>					<!-- 	<td>H. Yoshida</td>
					<td>S. Homma</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 11, 2018						</time>
					</td>

					<td  >
						Kyoto Kinen					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$1,112,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td>Mozu Katchan (jpn)</td>					<!-- 	<td>M. Demuro</td>
					<td>I. Sameshima</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 10, 2018						</time>
					</td>

					<td  >
						Gulfstream Park Turf Handicap					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$300,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td>Heart To Heart</td>					<!-- 	<td>J. Leparoux</td>
					<td>B. Lynch</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 10, 2018						</time>
					</td>

					<td  >
						San Vicente Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>Kanthaka</td>					<!-- 	<td>F. Prat</td>
					<td>J. Hollendorfer</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 10, 2018						</time>
					</td>

					<td  >
						Santa Maria Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Unique Bella</td>					<!-- 	<td>M. Smith</td>
					<td>J. Hollendorfer</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 10, 2018						</time>
					</td>

					<td  >
						Thunder Road Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Om</td>					<!-- 	<td>F. Prat</td>
					<td>D. Hendricks</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 10, 2018						</time>
					</td>

					<td  >
						Lambholm South Endeavour Stakes					</td>

					<td  >
						<span  >Tampa Bay Downs</span>
						<!-- Tampa Bay Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$175,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td>Dona Bruja</td>					<!-- 	<td>J. Ortiz</td>
					<td>I. Correas, IV</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 10, 2018						</time>
					</td>

					<td  >
						Sam F. Davis Stakes					</td>

					<td  >
						<span  >Tampa Bay Downs</span>
						<!-- Tampa Bay Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Flameaway</td>					<!-- 	<td>J. Lezcano</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 10, 2018						</time>
					</td>

					<td  >
						Tampa Bay Stakes					</td>

					<td  >
						<span  >Tampa Bay Downs</span>
						<!-- Tampa Bay Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$175,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td>World Approval</td>					<!-- 	<td>J. Velazquez</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 4, 2018						</time>
					</td>

					<td  >
						Kisaragi Sho					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$676,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td>Russet (jpn)</td>					<!-- 	<td>Y. Fujioka</td>
					<td>Y. Shono</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 4, 2018						</time>
					</td>

					<td  >
						Tokyo Shimbun Hai					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$700,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Kluger (jpn)</td>					<!-- 	<td>S. Hamanaka</td>
					<td>T. Takano</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 4, 2018						</time>
					</td>

					<td  >
						Las Virgenes Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td>Dream Tree</td>					<!-- 	<td>D. Van Dyke</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 3, 2018						</time>
					</td>

					<td  >
						Withers Stakes					</td>

					<td  >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$250,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>Avery Island</td>					<!-- 	<td>J. Bravo</td>
					<td>K. McLaughlin</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 3, 2018						</time>
					</td>

					<td  >
						Dania Beach Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Speed Franco</td>					<!-- 	<td>E. Jaramillo</td>
					<td>G. Delgado</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 3, 2018						</time>
					</td>

					<td  >
						Forward Gal Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>Take Charge Paula</td>					<!-- 	<td>P. Lopez</td>
					<td>K. McLaughlin</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 3, 2018						</time>
					</td>

					<td  >
						Holy Bull Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$350,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Audible</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 3, 2018						</time>
					</td>

					<td  >
						Swale Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>Strike Power</td>					<!-- 	<td>L. Saez</td>
					<td>M. Hennig</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 3, 2018						</time>
					</td>

					<td  >
						Sweetest Chant Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Thewayiam</td>					<!-- 	<td>J. Ortiz</td>
					<td>H. Motion</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 3, 2018						</time>
					</td>

					<td  >
						Palos Verdes Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td>Roy H</td>					<!-- 	<td>K. Desormeaux</td>
					<td>P. Miller</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 3, 2018						</time>
					</td>

					<td  >
						Robert B. Lewis Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$150,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Lombo</td>					<!-- 	<td>F. Prat</td>
					<td>M. Pender</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Feb 3, 2018						</time>
					</td>

					<td  >
						San Marcos Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td>Itsinthepost</td>					<!-- 	<td>T. Baze</td>
					<td>J. Mullins</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Feb 3, 2018						</time>
					</td>

					<td  >
						San Pasqual Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>Accelerate</td>					<!-- 	<td>V. Espinoza</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jan 28, 2018						</time>
					</td>

					<td  >
						Houston Ladies Classic					</td>

					<td  >
						<span  >Sam Houston</span>
						<!-- Sam Houston -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$400,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td>Tiger Moth</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jan 28, 2018						</time>
					</td>

					<td  >
						John B. Connally Turf Cup					</td>

					<td  >
						<span  >Sam Houston</span>
						<!-- Sam Houston -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td>Bigger Picture</td>					<!-- 	<td>J. Ortiz</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jan 28, 2018						</time>
					</td>

					<td  >
						Negishi Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$676,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>Best Warrior (usa)</td>					<!-- 	<td>C. Lemaire</td>
					<td>S. Ishizaka</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jan 28, 2018						</time>
					</td>

					<td  >
						Silk Road Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$700,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td>Fine Needle (jpn)</td>					<!-- 	<td>Y. Kawada</td>
					<td>Y. Takahashi</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jan 27, 2018						</time>
					</td>

					<td  >
						W.L. McKnight Handicap					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td>Oscar Nominated</td>					<!-- 	<td>J. Ortiz</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jan 27, 2018						</time>
					</td>

					<td  >
						Pegasus World Cup Invitational					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>I</td> -->

					<td  >
						$16,000,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>Gun Runner</td>					<!-- 	<td>F. Geroux</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jan 27, 2018						</time>
					</td>

					<td  >
						La Prevoyante Handicap					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td>Texting</td>					<!-- 	<td>J. Castellano</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jan 27, 2018						</time>
					</td>

					<td  >
						Fred W. Hooper					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$125,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td>Tommy Macho</td>					<!-- 	<td>L. Saez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jan 27, 2018						</time>
					</td>

					<td  >
						Toboggan Stakes					</td>

					<td  >
						<span  >Aqueduct</span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>Great Stuff</td>					<!-- 	<td>D. Davis</td>
					<td>D. Jacobson</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jan 21, 2018						</time>
					</td>

					<td  >
						American Jockey Club Cup					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$1,112,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td>Legend Cellar (jpn)</td>					<!-- 	<td>C. Lemaire</td>
					<td>T. Kimura</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jan 21, 2018						</time>
					</td>

					<td  >
						Tokai Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$984,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td>Asukano Roman (jpn)</td>					<!-- 	<td>K. Dazai</td>
					<td>Y. Kawamura</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jan 20, 2018						</time>
					</td>

					<td  >
						Las Cienegas Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>abt 6.5f Turf</td> -->
					<td>Coniah</td>					<!-- 	<td>K. Desormeaux</td>
					<td>W. Morey</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jan 15, 2018						</time>
					</td>

					<td  >
						Megahertz Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Madame Stripes</td>					<!-- 	<td>K. Desormeaux</td>
					<td>N. Drysdale</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jan 14, 2018						</time>
					</td>

					<td  >
						Keisei Hai					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$676,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td>Gambler (jpn)</td>					<!-- 	<td>A. Tsumura</td>
					<td>M. Saito</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jan 14, 2018						</time>
					</td>

					<td  >
						Nikkei Shinshun Hai					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$1,017,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td>Mondo Intero (jpn)</td>					<!-- 	<td>C. Lemaire</td>
					<td>T. Tezuka</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jan 13, 2018						</time>
					</td>

					<td  >
						Fort Lauderdale Stakes					</td>

					<td  >
						<span  >Gulfstream Park</span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td>Shining Copper</td>					<!-- 	<td>J. Ortiz</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jan 13, 2018						</time>
					</td>

					<td  >
						Lecomte Stakes					</td>

					<td  >
						<span  >Fair Grounds</span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.32f Dirt</td> -->
					<td>Instilled Regard</td>					<!-- 	<td>J. Castellano</td>
					<td>J. Hollendorfer</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jan 13, 2018						</time>
					</td>

					<td  >
						Aichi Hai					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$639,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td>Kinsho Yukihime (jpn)</td>					<!-- 	<td>S. Akiyama</td>
					<td>H. Nakamura</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jan 8, 2018						</time>
					</td>

					<td  >
						Shinzan Kinen					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$676,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Cassius (jpn)</td>					<!-- 	<td>S. Hamanaka</td>
					<td>H. Shimizu</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jan 7, 2018						</time>
					</td>

					<td  >
						Fairy Stakes					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$625,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Thistle (jpn)</td>					<!-- 	<td>T. Ono</td>
					<td>K. Shinkai</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jan 7, 2018						</time>
					</td>

					<td  >
						Santa Ynez Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td>Midnight Bisou</td>					<!-- 	<td>M. Smith</td>
					<td>W. Spawr</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jan 6, 2018						</time>
					</td>

					<td  >
						Kyoto Kimpai					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$726,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td>Masahaya Dream (jpn)</td>					<!-- 	<td>T. Iwasaki</td>
					<td>T. Konno</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jan 6, 2018						</time>
					</td>

					<td  >
						Sham Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$100,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td>Mckinzie</td>					<!-- 	<td>M. Smith</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="odd">
					<td>
						<time  >
							Jan 6, 2018						</time>
					</td>

					<td  >
						San Gabriel Stakes					</td>

					<td  >
						<span  >Santa Anita</span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td>Itsinthepost</td>					<!-- 	<td>T. Baze</td>
					<td>J. Mullins</td> -->
				</tr>
				<tr class="">
					<td>
						<time  >
							Jan 6, 2018						</time>
					</td>

					<td  >
						Nakayama Kimpai					</td>

					<td  >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  >
						$726,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td>Tosen Matakoiya (jpn)</td>					<!-- 	<td>H. Uchida</td>
					<td>Y. Kato</td> -->
				</tr>
							</tbody>
		</table>
				</div>
		