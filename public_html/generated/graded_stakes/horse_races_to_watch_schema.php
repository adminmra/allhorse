<div class="item" id="grade-stake-0" itemscope="" itemtype="http://schema.org/Event"><div class="dateLarge"><div class="month"><span>N</span><span>o</span><span>v</span></div><div class="day"><span>2</span><span>0</span></div></div>
<div class="details">
<time style="display:none" itemprop="startDate" content="2021-11-20">2021-11-20</time>
				<div class="race">
				  <span>RACE:</span>
				  <span style="font-weight:400" itemprop="name">Red Smith</span>
				</div><div class="track">
				  <span>TRACK:</span><span itemprop="location" itemscope itemtype="http://schema.org/Place"><span style="font-weight:400" itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="https://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></span></div><div class="purse">
				  <span>PURSE:</span>$200,000</div><div class="info">
				  <span>GRADE / AGE:</span><span style="font-weight:400" itemprop="description">II / 3&amp;up</span></div>
</div>
</div><div class="item" id="grade-stake-1" itemscope="" itemtype="http://schema.org/Event"><div class="dateLarge"><div class="month"><span>N</span><span>o</span><span>v</span></div><div class="day"><span>2</span><span>0</span></div></div>
<div class="details">
<time style="display:none" itemprop="startDate" content="2021-11-20">2021-11-20</time>
				<div class="race">
				  <span>RACE:</span>
				  <span style="font-weight:400" itemprop="name">Chilukki Stakes</span>
				</div><div class="track">
				  <span>TRACK:</span><span itemprop="location" itemscope itemtype="http://schema.org/Place"><span style="font-weight:400" itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="https://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></span></div><div class="purse">
				  <span>PURSE:</span>$300,000</div><div class="info">
				  <span>GRADE / AGE:</span><span style="font-weight:400" itemprop="description">III / 3&amp;up f/m</span></div>
</div>
</div><div class="item" id="grade-stake-2" itemscope="" itemtype="http://schema.org/Event"><div class="dateLarge"><div class="month"><span>N</span><span>o</span><span>v</span></div><div class="day"><span>2</span><span>0</span></div></div>
<div class="details">
<time style="display:none" itemprop="startDate" content="2021-11-20">2021-11-20</time>
				<div class="race">
				  <span>RACE:</span>
				  <span style="font-weight:400" itemprop="name">Native Diver Stakes</span>
				</div><div class="track">
				  <span>TRACK:</span><span itemprop="location" itemscope itemtype="http://schema.org/Place"><span style="font-weight:400" itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="https://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></span></div><div class="purse">
				  <span>PURSE:</span>$100,000</div><div class="info">
				  <span>GRADE / AGE:</span><span style="font-weight:400" itemprop="description">III / 3&amp;up</span></div>
</div>
</div><div class="item" id="grade-stake-3" itemscope="" itemtype="http://schema.org/Event"><div class="dateLarge"><div class="month"><span>N</span><span>o</span><span>v</span></div><div class="day"><span>2</span><span>0</span></div></div>
<div class="details">
<time style="display:none" itemprop="startDate" content="2021-11-20">2021-11-20</time>
				<div class="race">
				  <span>RACE:</span>
				  <span style="font-weight:400" itemprop="name">Tokyo Sports Hai Nisai Stakes</span>
				</div><div class="track">
				  <span>TRACK:</span><span itemprop="location" itemscope itemtype="http://schema.org/Place"><span style="font-weight:400" itemprop="name">Japan Racing Association</span></span></div><div class="purse">
				  <span>PURSE:</span>$780,000</div><div class="info">
				  <span>GRADE / AGE:</span><span style="font-weight:400" itemprop="description">II / 2yo</span></div>
</div>
</div><div class="item" id="grade-stake-5" itemscope="" itemtype="http://schema.org/Event"><div class="dateLarge"><div class="month"><span>N</span><span>o</span><span>v</span></div><div class="day"><span>2</span><span>1</span></div></div>
<div class="details">
<time style="display:none" itemprop="startDate" content="2021-11-21">2021-11-21</time>
				<div class="race">
				  <span>RACE:</span>
				  <span style="font-weight:400" itemprop="name">Mile Championship</span>
				</div><div class="track">
				  <span>TRACK:</span><span itemprop="location" itemscope itemtype="http://schema.org/Place"><span style="font-weight:400" itemprop="name">Japan Racing Association</span></span></div><div class="purse">
				  <span>PURSE:</span>$2,362,000</div><div class="info">
				  <span>GRADE / AGE:</span><span style="font-weight:400" itemprop="description">I / 3&amp;up</span></div>
</div>
</div><div class="item" id="grade-stake-7" itemscope="" itemtype="http://schema.org/Event"><div class="dateLarge"><div class="month"><span>N</span><span>o</span><span>v</span></div><div class="day"><span>2</span><span>5</span></div></div>
<div class="details">
<time style="display:none" itemprop="startDate" content="2021-11-25">2021-11-25</time>
				<div class="race">
				  <span>RACE:</span>
				  <span style="font-weight:400" itemprop="name">Falls City</span>
				</div><div class="track">
				  <span>TRACK:</span><span itemprop="location" itemscope itemtype="http://schema.org/Place"><span style="font-weight:400" itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="https://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></span></div><div class="purse">
				  <span>PURSE:</span>$500,000</div><div class="info">
				  <span>GRADE / AGE:</span><span style="font-weight:400" itemprop="description">II / 3&amp;up f/m</span></div>
</div>
</div><div class="item" id="grade-stake-8" itemscope="" itemtype="http://schema.org/Event"><div class="dateLarge"><div class="month"><span>N</span><span>o</span><span>v</span></div><div class="day"><span>2</span><span>5</span></div></div>
<div class="details">
<time style="display:none" itemprop="startDate" content="2021-11-25">2021-11-25</time>
				<div class="race">
				  <span>RACE:</span>
				  <span style="font-weight:400" itemprop="name">Red Carpet Stakes</span>
				</div><div class="track">
				  <span>TRACK:</span><span itemprop="location" itemscope itemtype="http://schema.org/Place"><span style="font-weight:400" itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="https://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></span></div><div class="purse">
				  <span>PURSE:</span>$100,000</div><div class="info">
				  <span>GRADE / AGE:</span><span style="font-weight:400" itemprop="description">III / 3&amp;up f/m</span></div>
</div>
</div>