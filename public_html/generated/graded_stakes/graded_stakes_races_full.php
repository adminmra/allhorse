		<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="3" cellpadding="3" width="100%">
		<tbody>
		<tr>
		<th width="10%"><strong>Date</strong></th>
		<th width="12%"><strong>Stakes</strong></th>
		<th width="12%"><strong>Track</strong></th>
		<th width="10%"><strong>Grade</strong></th>
		<th width="10%"><strong>Purses</strong></th>
		<th width="10%"><strong>Age/Sex</strong></th>
		<th width="10%"><strong>DS</strong></th>
		<th width="12%"><strong>Horse</strong></th>
		<th width="12%"><strong>Jockey</strong></th>
		<th width="12%"><strong>Trainer</strong></th>
		</tr>
		<tr>			<td class="num">Jan 1, 2021</td>
			<td>Joe Hernandez Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>6.5f Turf</td>
			<td>HEMBREE</td>
			<td>J. Rosario</td>
			<td>P. Miller</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 2, 2021</td>
			<td>San Gabriel Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>9f Turf</td>
			<td>ANOTHERTWISTAFATE</td>
			<td>J. Rosario</td>
			<td>P. Miller</td>
		</tr>
			<tr>			<td class="num">Jan 2, 2021</td>
			<td>Sham Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo</td>
			<td>8f Dirt</td>
			<td>LIFE IS GOOD</td>
			<td>M. Smith</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 3, 2021</td>
			<td>Santa Ynez Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>7f Dirt</td>
			<td>KALYPSO</td>
			<td>J. Rosario</td>
			<td>B. Baffert</td>
		</tr>
			<tr>			<td class="num">Jan 5, 2021</td>
			<td>Kyoto Kimpai</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>4&amp;up</td>
			<td>8f Turf</td>
			<td>PEACE ONE PARADIS (JPN)</td>
			<td>Y. Fukunaga</td>
			<td>M. Otake</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 5, 2021</td>
			<td>Nakayama Kimpai</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>4&amp;up</td>
			<td>10f Turf</td>
			<td>MOUNT GOLD (JPN)</td>
			<td>H. Uchida</td>
			<td>Y. Ikee</td>
		</tr>
			<tr>			<td class="num">Jan 9, 2021</td>
			<td>Tropical Turf</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up</td>
			<td>8f Turf</td>
			<td>RIDE A COMET</td>
			<td>T. Gaffalione</td>
			<td>M. Casse</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 9, 2021</td>
			<td>La Canada Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$200,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>SANENUS</td>
			<td>U. Rispoli</td>
			<td>M. McCarthy</td>
		</tr>
			<tr>			<td class="num">Jan 10, 2021</td>
			<td>Shinzan Kinen</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$780,000</td>
			<td>3yo</td>
			<td>8f Turf</td>
			<td>TOKAI KING (JPN)</td>
			<td>R. Wada</td>
			<td>T. Nishihashi</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 11, 2021</td>
			<td>Fairy Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$721,000</td>
			<td>3yo f</td>
			<td>8f Turf</td>
			<td>SHADOW FAX (JPN)</td>
			<td>T. Ono</td>
			<td>K. Miyata</td>
		</tr>
			<tr>			<td class="num">Jan 16, 2021</td>
			<td>Lecomte Stakes</td>
			<td>Fair Grounds</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>MIDNIGHT BOURBON</td>
			<td>J. Talamo</td>
			<td>S. Asmussen</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 16, 2021</td>
			<td>Louisiana Stakes</td>
			<td>Fair Grounds</td>
			<td>III</td>
			<td>$125,000</td>
			<td>4&amp;up</td>
			<td>8.5f Dirt</td>
			<td>TITLE READY</td>
			<td>B. Hernandez, Jr.</td>
			<td>D. Stewart</td>
		</tr>
			<tr>			<td class="num">Jan 16, 2021</td>
			<td>Aichi Hai</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$737,000</td>
			<td>4&amp;up f/m</td>
			<td>10f Turf</td>
			<td>TAGANO ASWAD (JPN)</td>
			<td>Y. Kitamura</td>
			<td>T. Igarashi</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 17, 2021</td>
			<td>Keisei Hai</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$780,000</td>
			<td>3yo</td>
			<td>10f Turf</td>
			<td>TEN BAGGER (JPN)</td>
			<td>K. Tosaki</td>
			<td>K. Fujioka</td>
		</tr>
			<tr>			<td class="num">Jan 17, 2021</td>
			<td>Nikkei Shinshun Hai</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,173,000</td>
			<td>4&amp;up</td>
			<td>11f Turf</td>
			<td>VALERIO (JPN)</td>
			<td>R. Wada</td>
			<td>I. Aizawa</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 17, 2021</td>
			<td>Astra Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up f/m</td>
			<td>12f Turf</td>
			<td>QUICK</td>
			<td>U. Rispoli</td>
			<td>J. Sadler</td>
		</tr>
			<tr>			<td class="num">Jan 18, 2021</td>
			<td>Megahertz Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up f/m</td>
			<td>8f Turf</td>
			<td>MUCHO UNUSUAL</td>
			<td>J. Rosario</td>
			<td>T. Yakteen</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 23, 2021</td>
			<td>Fred W. Hooper</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$125,000</td>
			<td>4&amp;up</td>
			<td>8f Dirt</td>
			<td>PERFORMER</td>
			<td>J. Rosario</td>
			<td>C. McGaughey III</td>
		</tr>
			<tr>			<td class="num">Jan 23, 2021</td>
			<td>Inside Information Stakes</td>
			<td>Gulfstream Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up f/m</td>
			<td>7f Dirt</td>
			<td>PACIFIC GALE</td>
			<td>J. Velazquez</td>
			<td>J. Kimmel</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 23, 2021</td>
			<td>La Prevoyante</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$125,000</td>
			<td>4&amp;up f/m</td>
			<td>12f Turf</td>
			<td>ALWAYS SHOPPING</td>
			<td>I. Ortiz, Jr.</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">Jan 23, 2021</td>
			<td>Pegasus World Cup Invitational</td>
			<td>Gulfstream Park</td>
			<td>I</td>
			<td>$3,000,000</td>
			<td>4&amp;up</td>
			<td>9f Dirt</td>
			<td>KNICKS GO</td>
			<td>J. Rosario</td>
			<td>B. Cox</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 23, 2021</td>
			<td>Pegasus World Cup Turf Invitational</td>
			<td>Gulfstream Park</td>
			<td>I</td>
			<td>$1,000,000</td>
			<td>4&amp;up</td>
			<td>9.5f Turf</td>
			<td>COLONEL LIAM</td>
			<td>I. Ortiz, Jr.</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">Jan 23, 2021</td>
			<td>William L. McKnight</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up</td>
			<td>12f Turf</td>
			<td>TIDE OF THE SEA</td>
			<td>T. Gaffalione</td>
			<td>M. Maker</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 24, 2021</td>
			<td>American Jockey Club Cup</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,283,000</td>
			<td>4&amp;up</td>
			<td>11f Turf</td>
			<td>TAGANO DIAMANTE (JPN)</td>
			<td>A. Tsumura</td>
			<td>I. Sameshima</td>
		</tr>
			<tr>			<td class="num">Jan 24, 2021</td>
			<td>Tokai Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,135,000</td>
			<td>4&amp;up</td>
			<td>9f Dirt</td>
			<td>T O FORCE (JPN)</td>
			<td>K. Kokubun</td>
			<td>I. Okada</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 30, 2021</td>
			<td>Toboggan Stakes</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up</td>
			<td>7f Dirt</td>
			<td>AMERICAN POWER</td>
			<td>K. Carmouche</td>
			<td>R. Atras</td>
		</tr>
			<tr>			<td class="num">Jan 30, 2021</td>
			<td>Forward Gal Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo f</td>
			<td>7f Dirt</td>
			<td>ZAAJEL</td>
			<td>L. Saez</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 30, 2021</td>
			<td>Holy Bull Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>GREATEST HONOUR</td>
			<td>J. Ortiz</td>
			<td>C. McGaughey III</td>
		</tr>
			<tr>			<td class="num">Jan 30, 2021</td>
			<td>Swale Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo</td>
			<td>7f Dirt</td>
			<td>DRAIN THE CLOCK</td>
			<td>E. Zayas</td>
			<td>S. Joseph, Jr.</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 30, 2021</td>
			<td>Sweetest Chant Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo f</td>
			<td>8f Turf</td>
			<td>WHITE FROST</td>
			<td>J. Alvarado</td>
			<td>W. Mott</td>
		</tr>
			<tr>			<td class="num">Jan 30, 2021</td>
			<td>Robert B. Lewis Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>MEDINA SPIRIT</td>
			<td>A. Cedillo</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 30, 2021</td>
			<td>San Pasqual Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>9f Dirt</td>
			<td>EXPRESS TRAIN</td>
			<td>J. Hernandez</td>
			<td>J. Shirreffs</td>
		</tr>
			<tr>			<td class="num">Jan 31, 2021</td>
			<td>Negishi Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$780,000</td>
			<td>4&amp;up</td>
			<td>7f Dirt</td>
			<td>TIME FLYER (JPN)</td>
			<td>C. Lemaire</td>
			<td>K. Matsuda</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 31, 2021</td>
			<td>Silk Road Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$808,000</td>
			<td>4&amp;up</td>
			<td>6f Turf</td>
			<td>KAISER MELANGE (JPN)</td>
			<td>K. Dazai</td>
			<td>E. Nakano</td>
		</tr>
			<tr>			<td class="num">Jan 31, 2021</td>
			<td>Houston Ladies Classic</td>
			<td>Sam Houston</td>
			<td>III</td>
			<td>$300,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>LETRUSKA</td>
			<td>J. Castanon</td>
			<td>F. Gutierrez</td>
		</tr>
			<tr class="odd">			<td class="num">Jan 31, 2021</td>
			<td>John B. Connally Turf Cup</td>
			<td>Sam Houston</td>
			<td>III</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>12f Turf</td>
			<td>SPOOKY CHANNEL</td>
			<td>J. Leparoux</td>
			<td>B. Lynch</td>
		</tr>
			<tr>			<td class="num">Feb 6, 2021</td>
			<td>Withers Stakes</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$250,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>RISK TAKING</td>
			<td>E. Cancel</td>
			<td>C. Brown</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 6, 2021</td>
			<td>Suwannee River Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up f/m</td>
			<td>9f Turf</td>
			<td>GREAT ISLAND</td>
			<td>I. Ortiz, Jr.</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Feb 6, 2021</td>
			<td>San Marcos Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>10f Turf</td>
			<td>MASTEROFOXHOUNDS</td>
			<td>J. Rosario</td>
			<td>R. Baltas</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 6, 2021</td>
			<td>San Vicente Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3yo</td>
			<td>7f Dirt</td>
			<td>CONCERT TOUR</td>
			<td>J. Rosario</td>
			<td>B. Baffert</td>
		</tr>
			<tr>			<td class="num">Feb 6, 2021</td>
			<td>Thunder Road Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up</td>
			<td>8f Turf</td>
			<td>HIT THE ROAD</td>
			<td>U. Rispoli</td>
			<td>D. Blacker</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 6, 2021</td>
			<td>Lambholm South Endeavour Stakes</td>
			<td>Tampa Bay Downs</td>
			<td>III</td>
			<td>$175,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f Turf</td>
			<td>COUNTERPARTY RISK</td>
			<td>J. Velazquez</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Feb 6, 2021</td>
			<td>Sam F. Davis Stakes</td>
			<td>Tampa Bay Downs</td>
			<td>III</td>
			<td>$250,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>CANDY MAN ROCKET</td>
			<td>J. Alvarado</td>
			<td>W. Mott</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 6, 2021</td>
			<td>Tampa Bay Stakes</td>
			<td>Tampa Bay Downs</td>
			<td>III</td>
			<td>$175,000</td>
			<td>4&amp;up</td>
			<td>8.5f Turf</td>
			<td>GET SMOKIN</td>
			<td>J. Alvarado</td>
			<td>T. Bush</td>
		</tr>
			<tr>			<td class="num">Feb 7, 2021</td>
			<td>Kisaragi Sho</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$780,000</td>
			<td>3yo</td>
			<td>10f Turf</td>
			<td>YOHO LAKE (JPN)</td>
			<td>Y. Take</td>
			<td>Y. Tomomichi</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 7, 2021</td>
			<td>Tokyo Shimbun Hai</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$808,000</td>
			<td>4&amp;up</td>
			<td>8f Turf</td>
			<td>PRODIGAL SON (JPN)</td>
			<td>Y. Fujioka</td>
			<td>S. Kunieda</td>
		</tr>
			<tr>			<td class="num">Feb 13, 2021</td>
			<td>Fair Grounds  Stakes</td>
			<td>Fair Grounds</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up</td>
			<td>9f Turf</td>
			<td>CAPTIVATING MOON</td>
			<td>M. Pedroza</td>
			<td>C. Block</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 13, 2021</td>
			<td>Mineshaft Stakes</td>
			<td>Fair Grounds</td>
			<td>III</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>8.5f Dirt</td>
			<td>MAXFIELD</td>
			<td>F. Geroux</td>
			<td>B. Walsh</td>
		</tr>
			<tr>			<td class="num">Feb 13, 2021</td>
			<td>Rachel Alexandra Stakes</td>
			<td>Fair Grounds</td>
			<td>II</td>
			<td>$300,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>CLAIRIERE</td>
			<td>J. Talamo</td>
			<td>S. Asmussen</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 13, 2021</td>
			<td>Risen Star Stakes</td>
			<td>Fair Grounds</td>
			<td>II</td>
			<td>$400,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>MANDALOUN</td>
			<td>F. Geroux</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">Feb 13, 2021</td>
			<td>Gulfstream Park Sprint</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up</td>
			<td>6f Dirt</td>
			<td>MISCHEVIOUS ALEX</td>
			<td>I. Ortiz, Jr.</td>
			<td>S. Joseph, Jr.</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 13, 2021</td>
			<td>Gulfstream Park Turf Sprint</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up</td>
			<td>5f Turf</td>
			<td>LEINSTER</td>
			<td>L. Saez</td>
			<td>G. Arnold, II</td>
		</tr>
			<tr>			<td class="num">Feb 13, 2021</td>
			<td>Queen Cup</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$721,000</td>
			<td>3yo f</td>
			<td>8f Turf</td>
			<td>STELLARIA (JPN)</td>
			<td>Y. Fukunaga</td>
			<td>T. Saito</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 13, 2021</td>
			<td>Santa Monica Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up f/m</td>
			<td>7f Dirt</td>
			<td>MERNEITH</td>
			<td>E. Maldonado</td>
			<td>B. Baffert</td>
		</tr>
			<tr>			<td class="num">Feb 14, 2021</td>
			<td>Kyodo News Service Hai</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$780,000</td>
			<td>3yo</td>
			<td>9f Turf</td>
			<td>PLATINA TREASURE (JPN)</td>
			<td>H. Tanabe</td>
			<td>S. Kunieda</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 14, 2021</td>
			<td>Kyoto Kinen</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,283,000</td>
			<td>4&amp;up</td>
			<td>11f Turf</td>
			<td>JINAMBO (JPN)</td>
			<td>Y. Iwata</td>
			<td>N. Hori</td>
		</tr>
			<tr>			<td class="num">Feb 20, 2021</td>
			<td>Royal Delta Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>QUEEN NEKIA</td>
			<td>C. Lanerie</td>
			<td>S. Joseph, Jr.</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 20, 2021</td>
			<td>Diamond Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>4&amp;up</td>
			<td>17f Turf</td>
			<td>MISS MAMMA MIA (JPN)</td>
			<td>F. Matsuwaka</td>
			<td>R. Terashima</td>
		</tr>
			<tr>			<td class="num">Feb 20, 2021</td>
			<td>Kyoto Himba Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$737,000</td>
			<td>4&amp;up f/m</td>
			<td>7f Turf</td>
			<td>I LOVE TAILOR (JPN)</td>
			<td>Y. Iwata</td>
			<td>H. Kawachi</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 20, 2021</td>
			<td>General George Stakes</td>
			<td>Laurel Park</td>
			<td>III</td>
			<td>$250,000</td>
			<td>4&amp;up</td>
			<td>7f Dirt</td>
			<td>SHARE THE RIDE</td>
			<td>V. Carrasco</td>
			<td>M. Penaloza</td>
		</tr>
			<tr>			<td class="num">Feb 20, 2021</td>
			<td>Runhappy Barbara Fritchie Stakes</td>
			<td>Laurel Park</td>
			<td>III</td>
			<td>$250,000</td>
			<td>4&amp;up f/m</td>
			<td>7f Dirt</td>
			<td>HIBISCUS PUNCH</td>
			<td>H. Karamanos</td>
			<td>J. Nixon</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 21, 2021</td>
			<td>February Stakes</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,058,000</td>
			<td>4&amp;up</td>
			<td>8f Dirt</td>
			<td>WONDER LIDER (JPN)</td>
			<td>N. Yokoyama</td>
			<td>S. Yasuda</td>
		</tr>
			<tr>			<td class="num">Feb 21, 2021</td>
			<td>Kokura Daishoten</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>4&amp;up</td>
			<td>9f Turf</td>
			<td>DIRNDL (JPN)</td>
			<td>T. Danno</td>
			<td>Y Okumura</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 27, 2021</td>
			<td>Canadian Turf Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$125,000</td>
			<td>4&amp;up</td>
			<td>8.5f Turf</td>
			<td>VENEZUELAN HUG</td>
			<td>J. Alvarado</td>
			<td>D. Gargan</td>
		</tr>
			<tr>			<td class="num">Feb 27, 2021</td>
			<td>Davona Dale Stakes</td>
			<td>Gulfstream Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>8f Dirt</td>
			<td>WHOLEBODEMEISTER</td>
			<td>E. Zayas</td>
			<td>J. Avila</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 27, 2021</td>
			<td>Fountain of Youth Stakes</td>
			<td>Gulfstream Park</td>
			<td>II</td>
			<td>$300,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>GREATEST HONOUR</td>
			<td>J. Ortiz</td>
			<td>C. McGaughey III</td>
		</tr>
			<tr>			<td class="num">Feb 27, 2021</td>
			<td>Gulfstream Park Mile</td>
			<td>Gulfstream Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>8f Dirt</td>
			<td>FEARLESS</td>
			<td>I. Ortiz, Jr.</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 27, 2021</td>
			<td>Herecomesthebride Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo f</td>
			<td>8.5f Turf</td>
			<td>CON LIMA</td>
			<td>I. Ortiz, Jr.</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">Feb 27, 2021</td>
			<td>Honey Fox Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$125,000</td>
			<td>4&amp;up f/m</td>
			<td>8f Turf</td>
			<td>GOT STORMY</td>
			<td>T. Gaffalione</td>
			<td>M. Casse</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 27, 2021</td>
			<td>Mac Diarmida Stakes</td>
			<td>Gulfstream Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>11f Turf</td>
			<td>PHANTOM CURRENCY</td>
			<td>P. Lopez</td>
			<td>B. Lynch</td>
		</tr>
			<tr>			<td class="num">Feb 27, 2021</td>
			<td>The Very One Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$125,000</td>
			<td>4&amp;up f/m</td>
			<td>9.5f Turf</td>
			<td>ANTOINETTE</td>
			<td>J. Ortiz</td>
			<td>W. Mott</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 27, 2021</td>
			<td>Razorback Handicap</td>
			<td>Oaklawn Park</td>
			<td>III</td>
			<td>$600,000</td>
			<td>4&amp;up</td>
			<td>8.5f Dirt</td>
			<td>MYSTIC GUIDE</td>
			<td>L. Saez</td>
			<td>M. Stidham</td>
		</tr>
			<tr>			<td class="num">Feb 27, 2021</td>
			<td>Southwest Stakes</td>
			<td>Oaklawn Park</td>
			<td>III</td>
			<td>$750,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>ESSENTIAL QUALITY</td>
			<td>L. Saez</td>
			<td>B. Cox</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 28, 2021</td>
			<td>Hankyu Hai</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>4&amp;up</td>
			<td>7f Turf</td>
			<td>SEIZINGER (JPN)</td>
			<td>K. Kokubun</td>
			<td>K. Makita</td>
		</tr>
			<tr>			<td class="num">Feb 28, 2021</td>
			<td>Nakayama Kinen</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,383,000</td>
			<td>4&amp;up</td>
			<td>9f Turf</td>
			<td>SUN APPLETON (JPN)</td>
			<td>Y. Shibata</td>
			<td>E. Nakano</td>
		</tr>
			<tr class="odd">			<td class="num">Feb 28, 2021</td>
			<td>Bayakoa Stakes</td>
			<td>Oaklawn Park</td>
			<td>III</td>
			<td>$250,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>MONOMOY GIRL</td>
			<td>F. Geroux</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">Mar 6, 2021</td>
			<td>Gotham Stakes</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$300,000</td>
			<td>3yo</td>
			<td>8f Dirt</td>
			<td>WEYBURN</td>
			<td>T. McCarthy</td>
			<td>J. Jerkens</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 6, 2021</td>
			<td>Tom Fool Handicap</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>6f Dirt</td>
			<td>CHATEAU</td>
			<td>K. Carmouche</td>
			<td>R. Atras</td>
		</tr>
			<tr>			<td class="num">Mar 6, 2021</td>
			<td>Ocean Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>4&amp;up</td>
			<td>6f Turf</td>
			<td>LOVING ANSWER (JPN)</td>
			<td>M. Katsuura</td>
			<td>K. Ishizaka</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 6, 2021</td>
			<td>Tulip Sho</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,072,000</td>
			<td>3yo f</td>
			<td>8f Turf</td>
			<td>MEIKEI YELL (JPN)</td>
			<td>Y. Take</td>
			<td>H. Take</td>
		</tr>
			<tr>			<td class="num">Mar 6, 2021</td>
			<td>Frank E. Kilroe Mile</td>
			<td>Santa Anita</td>
			<td>I</td>
			<td>$400,000</td>
			<td>4&amp;up</td>
			<td>8f Turf</td>
			<td>HIT THE ROAD</td>
			<td>F. Geroux</td>
			<td>D. Blacker</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 6, 2021</td>
			<td>San Carlos Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>7f Dirt</td>
			<td>BRICKYARD RIDE</td>
			<td>A. Centeno</td>
			<td>C. Lewis</td>
		</tr>
			<tr>			<td class="num">Mar 6, 2021</td>
			<td>San Felipe Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$300,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>LIFE IS GOOD</td>
			<td>M. Smith</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 6, 2021</td>
			<td>Santa Anita Handicap</td>
			<td>Santa Anita</td>
			<td>I</td>
			<td>$400,000</td>
			<td>4&amp;up</td>
			<td>10f Dirt</td>
			<td>IDOL</td>
			<td>J. Rosario</td>
			<td>R. Baltas</td>
		</tr>
			<tr>			<td class="num">Mar 6, 2021</td>
			<td>Challenger Stakes</td>
			<td>Tampa Bay Downs</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up</td>
			<td>8.5f Dirt</td>
			<td>LAST JUDGEMENT</td>
			<td>D. Centeno</td>
			<td>M. Maker</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 6, 2021</td>
			<td>Florida Oaks</td>
			<td>Tampa Bay Downs</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>8.5f Turf</td>
			<td>DOMAIN EXPERTISE</td>
			<td>A. Gallardo</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Mar 6, 2021</td>
			<td>Hillsborough Stakes</td>
			<td>Tampa Bay Downs</td>
			<td>II</td>
			<td>$225,000</td>
			<td>4&amp;up f/m</td>
			<td>9f Turf</td>
			<td>MICHELINE</td>
			<td>L. Saez</td>
			<td>M. Stidham</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 6, 2021</td>
			<td>Lambholm South Tampa Bay Derby</td>
			<td>Tampa Bay Downs</td>
			<td>II</td>
			<td>$400,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>HELIUM</td>
			<td>J. Ferrer</td>
			<td>M. Casse</td>
		</tr>
			<tr>			<td class="num">Mar 7, 2021</td>
			<td>Yayoi Sho</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,120,000</td>
			<td>3yo</td>
			<td>10f Turf</td>
			<td>SO VALIANT (JPN)</td>
			<td>T. Ono</td>
			<td>M. Otake</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 7, 2021</td>
			<td>Santa Ysabel Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>BEAUTIFUL GIFT</td>
			<td>J. Velazquez</td>
			<td>B. Baffert</td>
		</tr>
			<tr>			<td class="num">Mar 13, 2021</td>
			<td>Azeri Stakes</td>
			<td>Oaklawn Park</td>
			<td>II</td>
			<td>$350,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>SHEDARESTHEDEVIL</td>
			<td>F. Geroux</td>
			<td>B. Cox</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 13, 2021</td>
			<td>Rebel Stakes</td>
			<td>Oaklawn Park</td>
			<td>II</td>
			<td>$1,000,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>CONCERT TOUR</td>
			<td>J. Rosario</td>
			<td>B. Baffert</td>
		</tr>
			<tr>			<td class="num">Mar 13, 2021</td>
			<td>Beholder Mile</td>
			<td>Santa Anita</td>
			<td>I</td>
			<td>$300,000</td>
			<td>4&amp;up f/m</td>
			<td>8f Dirt</td>
			<td>SWISS SKYDIVER</td>
			<td>R. Albarado</td>
			<td>K. McPeek</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 14, 2021</td>
			<td>Kinko Sho</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,383,000</td>
			<td>4&amp;up</td>
			<td>10f Turf</td>
			<td>KISEKI (JPN)</td>
			<td>M. Demuro</td>
			<td>Y. Tsujino</td>
		</tr>
			<tr>			<td class="num">Mar 20, 2021</td>
			<td>Fair Grounds Oaks</td>
			<td>Fair Grounds</td>
			<td>II</td>
			<td>$400,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>TRAVEL COLUMN</td>
			<td>F. Geroux</td>
			<td>B. Cox</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 20, 2021</td>
			<td>Louisiana Derby</td>
			<td>Fair Grounds</td>
			<td>II</td>
			<td>$1,000,000</td>
			<td>3yo</td>
			<td>9.5f Dirt</td>
			<td>HOT ROD CHARLIE</td>
			<td>J. Rosario</td>
			<td>L. Mora</td>
		</tr>
			<tr>			<td class="num">Mar 20, 2021</td>
			<td>Muniz Memorial Classic</td>
			<td>Fair Grounds</td>
			<td>II</td>
			<td>$300,000</td>
			<td>4&amp;up</td>
			<td>9f Turf</td>
			<td>COLONEL LIAM</td>
			<td>I. Ortiz, Jr.</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 20, 2021</td>
			<td>New Orleans Classic</td>
			<td>Fair Grounds</td>
			<td>II</td>
			<td>$400,000</td>
			<td>4&amp;up</td>
			<td>9f Dirt</td>
			<td>CHESS CHIEF</td>
			<td>L. Saez</td>
			<td>D. Stewart</td>
		</tr>
			<tr>			<td class="num">Mar 20, 2021</td>
			<td>Falcon Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$780,000</td>
			<td>3yo</td>
			<td>7f Turf</td>
			<td>ROOKS NEST (JPN)</td>
			<td>H. Miyuki</td>
			<td>T. Hamada</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 20, 2021</td>
			<td>Flower Cup</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$721,000</td>
			<td>3yo f</td>
			<td>9f Turf</td>
			<td>UBERLEBEN (JPN)</td>
			<td>Y. Tannai</td>
			<td>T. Tezuka</td>
		</tr>
			<tr>			<td class="num">Mar 20, 2021</td>
			<td>San Luis Rey Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up</td>
			<td>12f Turf</td>
			<td>UNITED</td>
			<td>F. Prat</td>
			<td>R. Mandella</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 21, 2021</td>
			<td>Hanshin Daishoten</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,383,000</td>
			<td>4&amp;up</td>
			<td>15f Turf</td>
			<td>MEISHO TENGEN (JPN)</td>
			<td>M. Sakai</td>
			<td>K. Ikezoe</td>
		</tr>
			<tr>			<td class="num">Mar 21, 2021</td>
			<td>Spring Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,120,000</td>
			<td>3yo</td>
			<td>9f Turf</td>
			<td>NISHINO OIKAZE (JPN)</td>
			<td>M. Katsuura</td>
			<td>Y. Takeichi</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 27, 2021</td>
			<td>Florida Derby</td>
			<td>Gulfstream Park</td>
			<td>I</td>
			<td>$750,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>KNOWN AGENDA</td>
			<td>I. Ortiz, Jr.</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">Mar 27, 2021</td>
			<td>Gulfstream Park Oaks</td>
			<td>Gulfstream Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>CRAZY BEAUTIFUL</td>
			<td>J. Ortiz</td>
			<td>K. McPeek</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 27, 2021</td>
			<td>Orchid Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up f/m</td>
			<td>11f Turf</td>
			<td>WAR LIKE GODDESS</td>
			<td>J. Leparoux</td>
			<td>W. Mott</td>
		</tr>
			<tr>			<td class="num">Mar 27, 2021</td>
			<td>Pan American Stakes</td>
			<td>Gulfstream Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>12f Turf</td>
			<td>CHURN N BURN</td>
			<td>J. Leparoux</td>
			<td>I. Wilkes</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 27, 2021</td>
			<td>Mainichi Hai</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$780,000</td>
			<td>3yo</td>
			<td>9f Turf</td>
			<td>WESTONBIRT (JPN)</td>
			<td>Y. Kokubun</td>
			<td>T. Yoshioka</td>
		</tr>
			<tr>			<td class="num">Mar 27, 2021</td>
			<td>Nikkei Sho</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,383,000</td>
			<td>4&amp;up</td>
			<td>12.5f Turf</td>
			<td>HUMIDOR (JPN)</td>
			<td>Y. Yoshida</td>
			<td>H. Kotegawa</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 27, 2021</td>
			<td>Santa Ana Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up f/m</td>
			<td>10f Turf</td>
			<td>GOING TO VEGAS</td>
			<td>U. Rispoli</td>
			<td>R. Baltas</td>
		</tr>
			<tr>			<td class="num">Mar 28, 2021</td>
			<td>March Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$737,000</td>
			<td>4&amp;up</td>
			<td>9f Dirt</td>
			<td>MEMORY KO (JPN)</td>
			<td>Y. Furukawa</td>
			<td>M. Matsunaga</td>
		</tr>
			<tr class="odd">			<td class="num">Mar 28, 2021</td>
			<td>Takamatsunomiya Kinen</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,362,000</td>
			<td>4&amp;up</td>
			<td>6f Turf</td>
			<td>LAUDA SION (JPN)</td>
			<td>M. Demuro</td>
			<td>T. Saito</td>
		</tr>
			<tr>			<td class="num">Apr 2, 2021</td>
			<td>Distaff Handicap</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up f/m</td>
			<td>7f Dirt</td>
			<td>PARIS LIGHTS</td>
			<td>J. Alvarado</td>
			<td>W. Mott</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 2, 2021</td>
			<td>Beaumont Stakes</td>
			<td>Keeneland</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3yo f</td>
			<td>abt 7f Dirt</td>
			<td>TWENTY CARAT</td>
			<td>L. Saez</td>
			<td>W. Ward</td>
		</tr>
			<tr>			<td class="num">Apr 2, 2021</td>
			<td>Transylvania Stakes</td>
			<td>Keeneland</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3yo</td>
			<td>8.5f Turf</td>
			<td>SCARLETT SKY</td>
			<td>J. Rosario</td>
			<td>C. McGaughey III</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 3, 2021</td>
			<td>Bay Shore Stakes</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3yo</td>
			<td>7f Dirt</td>
			<td>DRAIN THE CLOCK</td>
			<td>I. Ortiz, Jr.</td>
			<td>S. Joseph, Jr.</td>
		</tr>
			<tr>			<td class="num">Apr 3, 2021</td>
			<td>Carter Handicap</td>
			<td>Aqueduct</td>
			<td>I</td>
			<td>$300,000</td>
			<td>4&amp;up</td>
			<td>7f Dirt</td>
			<td>MISCHEVIOUS ALEX</td>
			<td>I. Ortiz, Jr.</td>
			<td>S. Joseph, Jr.</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 3, 2021</td>
			<td>Excelsior Stakes</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up</td>
			<td>9f Dirt</td>
			<td>MODERNIST</td>
			<td>J. Alvarado</td>
			<td>W. Mott</td>
		</tr>
			<tr>			<td class="num">Apr 3, 2021</td>
			<td>Gazelle Stakes</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$250,000</td>
			<td>3yo f</td>
			<td>9f Dirt</td>
			<td>SEARCH RESULTS</td>
			<td>I. Ortiz, Jr.</td>
			<td>C. Brown</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 3, 2021</td>
			<td>Wood Memorial Stakes</td>
			<td>Aqueduct</td>
			<td>II</td>
			<td>$750,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>BOURBONIC</td>
			<td>K. Carmouche</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">Apr 3, 2021</td>
			<td>Lord Derby Challenge Trophy</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$808,000</td>
			<td>4&amp;up</td>
			<td>8f Turf</td>
			<td>SHONAN RISE (JPN)</td>
			<td>H. Uchida</td>
			<td>H. Uehara</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 3, 2021</td>
			<td>Appalachian Stakes</td>
			<td>Keeneland</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>8f Turf</td>
			<td>JOUSTER</td>
			<td>L. Saez</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">Apr 3, 2021</td>
			<td>Central Bank Ashland Stakes</td>
			<td>Keeneland</td>
			<td>I</td>
			<td>$400,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>MALATHAAT</td>
			<td>J. Rosario</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 3, 2021</td>
			<td>Commonwealth Stakes</td>
			<td>Keeneland</td>
			<td>III</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>7f Dirt</td>
			<td>FLAGSTAFF</td>
			<td>J. Rosario</td>
			<td>J. Sadler</td>
		</tr>
			<tr>			<td class="num">Apr 3, 2021</td>
			<td>Madison Stakes</td>
			<td>Keeneland</td>
			<td>I</td>
			<td>$300,000</td>
			<td>4&amp;up f/m</td>
			<td>7f Dirt</td>
			<td>KIMARI</td>
			<td>J. Rosario</td>
			<td>W. Ward</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 3, 2021</td>
			<td>Shakertown Stakes</td>
			<td>Keeneland</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>5.5f Turf</td>
			<td>BOUND FOR NOWHERE</td>
			<td>J. Rosario</td>
			<td>W. Ward</td>
		</tr>
			<tr>			<td class="num">Apr 3, 2021</td>
			<td>Toyota Blue Grass Stakes</td>
			<td>Keeneland</td>
			<td>II</td>
			<td>$800,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>ESSENTIAL QUALITY</td>
			<td>L. Saez</td>
			<td>B. Cox</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 3, 2021</td>
			<td>Runhappy Santa Anita Derby</td>
			<td>Santa Anita</td>
			<td>I</td>
			<td>$750,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>ROCK YOUR WORLD</td>
			<td>U. Rispoli</td>
			<td>J. Sadler</td>
		</tr>
			<tr>			<td class="num">Apr 3, 2021</td>
			<td>Santa Anita Oaks</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$400,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>SOOTHSAY</td>
			<td>F. Prat</td>
			<td>R. Mandella</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 4, 2021</td>
			<td>Osaka Hai</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,443,000</td>
			<td>4&amp;up</td>
			<td>10f Turf</td>
			<td>BRAVAS (JPN)</td>
			<td>K. Miura</td>
			<td>Y. Tomomichi</td>
		</tr>
			<tr>			<td class="num">Apr 4, 2021</td>
			<td>Las Flores Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up f/m</td>
			<td>6f Dirt</td>
			<td>GAMINE</td>
			<td>J. Velazquez</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 10, 2021</td>
			<td>Hanshin Himba Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,135,000</td>
			<td>4&amp;up f/m</td>
			<td>8f Turf</td>
			<td>MEISHO GLOCKE (JPN)</td>
			<td>S. Hamanaka</td>
			<td>Y. Arakawa</td>
		</tr>
			<tr>			<td class="num">Apr 10, 2021</td>
			<td>New Zealand Trophy</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,120,000</td>
			<td>3yo</td>
			<td>8f Turf</td>
			<td>GOLD CHALICE (JPN)</td>
			<td>K. Tanaka</td>
			<td>K. Take</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 10, 2021</td>
			<td>Ben Ali Stakes</td>
			<td>Keeneland</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up</td>
			<td>9f Dirt</td>
			<td>SILVER DUST</td>
			<td>A. Beschizza</td>
			<td>W. Calhoun</td>
		</tr>
			<tr>			<td class="num">Apr 10, 2021</td>
			<td>Coolmore Jenny Wiley Stakes</td>
			<td>Keeneland</td>
			<td>I</td>
			<td>$300,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f Turf</td>
			<td>JULIET FOXTROT</td>
			<td>T. Gaffalione</td>
			<td>B. Cox</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 10, 2021</td>
			<td>Stonestreet Lexington Stakes</td>
			<td>Keeneland</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>KING FURY</td>
			<td>B. Hernandez, Jr.</td>
			<td>K. McPeek</td>
		</tr>
			<tr>			<td class="num">Apr 10, 2021</td>
			<td>Arkansas Derby</td>
			<td>Oaklawn Park</td>
			<td>I</td>
			<td>$1,000,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>SUPER STOCK</td>
			<td>R. Santana, Jr.</td>
			<td>S. Asmussen</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 10, 2021</td>
			<td>Count Fleet Sprint Handicap</td>
			<td>Oaklawn Park</td>
			<td>III</td>
			<td>$500,000</td>
			<td>4&amp;up</td>
			<td>6f Dirt</td>
			<td>C Z ROCKET</td>
			<td>F. Geroux</td>
			<td>P. Miller</td>
		</tr>
			<tr>			<td class="num">Apr 11, 2021</td>
			<td>Oka Sho</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,160,000</td>
			<td>3yo f</td>
			<td>8f Turf</td>
			<td>AKAITORINO MUSUME (JPN)</td>
			<td>T. Yokoyama</td>
			<td>S. Kunieda</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 16, 2021</td>
			<td>Baird Doubledogdare Stakes</td>
			<td>Keeneland</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>BONNY SOUTH</td>
			<td>F. Geroux</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">Apr 17, 2021</td>
			<td>Arlington Cup</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$780,000</td>
			<td>3yo</td>
			<td>8f Turf</td>
			<td>ADMIRE SAGE (JPN)</td>
			<td>K. Ikezoe</td>
			<td>Y. Tomomichi</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 17, 2021</td>
			<td>Apple Blossom Handicap</td>
			<td>Oaklawn Park</td>
			<td>I</td>
			<td>$1,000,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>LETRUSKA</td>
			<td>F. Gutierrez</td>
			<td>F. Gutierrez</td>
		</tr>
			<tr>			<td class="num">Apr 17, 2021</td>
			<td>Oaklawn Handicap</td>
			<td>Oaklawn Park</td>
			<td>II</td>
			<td>$1,000,000</td>
			<td>4&amp;up</td>
			<td>9f Dirt</td>
			<td>SILVER STATE</td>
			<td>R. Santana, Jr.</td>
			<td>F. Gutierrez</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 17, 2021</td>
			<td>Californian Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>9f Dirt</td>
			<td>ROYAL SHIP</td>
			<td>M. Smith</td>
			<td>R. Mandella</td>
		</tr>
			<tr>			<td class="num">Apr 18, 2021</td>
			<td>Antares Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$737,000</td>
			<td>4&amp;up</td>
			<td>9f Dirt</td>
			<td>DANON SPLENDOR (JPN)</td>
			<td>A. Saito</td>
			<td>T. Yasuda</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 18, 2021</td>
			<td>Satsuki Sho</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,273,000</td>
			<td>3yo</td>
			<td>10f Turf</td>
			<td>DEEP MONSTER (JPN)</td>
			<td>K. Tosaki</td>
			<td>Y. Ikee</td>
		</tr>
			<tr>			<td class="num">Apr 18, 2021</td>
			<td>Kona Gold Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up</td>
			<td>6.5f Dirt</td>
			<td>CEZANNE</td>
			<td>F. Prat</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 18, 2021</td>
			<td>Tokyo City Cup</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up</td>
			<td>12f Dirt</td>
			<td>TIZAMAGICIAN</td>
			<td>F. Prat</td>
			<td>R. Mandella</td>
		</tr>
			<tr>			<td class="num">Apr 23, 2021</td>
			<td>Bewitch Stakes</td>
			<td>Keeneland</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up f/m</td>
			<td>12f Turf</td>
			<td>WAR LIKE GODDESS</td>
			<td>J. Leparoux</td>
			<td>W. Mott</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 24, 2021</td>
			<td>San Francisco Mile</td>
			<td>Golden Gate Fields</td>
			<td>III</td>
			<td>$250,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>WHISPER NOT</td>
			<td>G. Franco</td>
			<td>R. Baltas</td>
		</tr>
			<tr>			<td class="num">Apr 24, 2021</td>
			<td>Fukushima Himba Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$780,000</td>
			<td>4&amp;up f/m</td>
			<td>9f Turf</td>
			<td>PALLAS ATHENA (JPN)</td>
			<td>R. Sakai</td>
			<td>M. Takayanagi</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 24, 2021</td>
			<td>Santa Margarita Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up f/m</td>
			<td>8f Dirt</td>
			<td>AS TIME GOES BY</td>
			<td>M. Smith</td>
			<td>B. Baffert</td>
		</tr>
			<tr>			<td class="num">Apr 25, 2021</td>
			<td>Flora Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,072,000</td>
			<td>3yo f</td>
			<td>10f Turf</td>
			<td>SNOW HALATION (JPN)</td>
			<td>G. Maruyama</td>
			<td>T. Kanari</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 25, 2021</td>
			<td>Yomiuri Milers Cup</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,221,000</td>
			<td>4&amp;up</td>
			<td>8f Turf</td>
			<td>AIR LOLONOIS (JPN)</td>
			<td>K. Ikezoe</td>
			<td>K. Sasada</td>
		</tr>
			<tr>			<td class="num">Apr 30, 2021</td>
			<td>Alysheba Stakes</td>
			<td>Churchill Downs</td>
			<td>II</td>
			<td>$400,000</td>
			<td>4&amp;up</td>
			<td>8.5f Dirt</td>
			<td>MAXFIELD</td>
			<td>J. Ortiz</td>
			<td>B. Walsh</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 30, 2021</td>
			<td>Edgewood Stakes</td>
			<td>Churchill Downs</td>
			<td>II</td>
			<td>$300,000</td>
			<td>3yo f</td>
			<td>8.5f Turf</td>
			<td>GIFT LIST</td>
			<td>J. Castellano</td>
			<td>B. Lynch</td>
		</tr>
			<tr>			<td class="num">Apr 30, 2021</td>
			<td>Eight Belles</td>
			<td>Churchill Downs</td>
			<td>II</td>
			<td>$300,000</td>
			<td>3yo f</td>
			<td>7f Dirt</td>
			<td>OBLIGATORY</td>
			<td>J. Ortiz</td>
			<td>W. Mott</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 30, 2021</td>
			<td>Kentucky Oaks</td>
			<td>Churchill Downs</td>
			<td>I</td>
			<td>$1,250,000</td>
			<td>3yo f</td>
			<td>9f Dirt</td>
			<td>MALATHAAT</td>
			<td>J. Velazquez</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">Apr 30, 2021</td>
			<td>La Troienne Stakes</td>
			<td>Churchill Downs</td>
			<td>I</td>
			<td>$500,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>SHEDARESTHEDEVIL</td>
			<td>F. Geroux</td>
			<td>B. Cox</td>
		</tr>
			<tr class="odd">			<td class="num">Apr 30, 2021</td>
			<td>Turf Sprint</td>
			<td>Churchill Downs</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3&amp;up</td>
			<td>5.5f Turf</td>
			<td>FAST BOAT</td>
			<td>I. Ortiz, Jr.</td>
			<td>J. Sharp</td>
		</tr>
			<tr>			<td class="num">May 1, 2021</td>
			<td>Fort Marcy Stakes</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>9f Turf</td>
			<td>TRIBHUVAN</td>
			<td>E. Cancel</td>
			<td>C. Brown</td>
		</tr>
			<tr class="odd">			<td class="num">May 1, 2021</td>
			<td>Sheepshead Bay Stakes</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up f/m</td>
			<td>11f Turf</td>
			<td>MAGIC ATTITUDE</td>
			<td>T. McCarthy</td>
			<td>A. Delacour</td>
		</tr>
			<tr>			<td class="num">May 1, 2021</td>
			<td>Westchester Stakes</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>8f Dirt</td>
			<td>DR POST</td>
			<td>M. Franco</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">May 1, 2021</td>
			<td>American Turf</td>
			<td>Churchill Downs</td>
			<td>II</td>
			<td>$500,000</td>
			<td>3yo</td>
			<td>8.5f Turf</td>
			<td>DU JOUR</td>
			<td>F. Prat</td>
			<td>B. Baffert</td>
		</tr>
			<tr>			<td class="num">May 1, 2021</td>
			<td>Churchill Downs Stakes</td>
			<td>Churchill Downs</td>
			<td>I</td>
			<td>$500,000</td>
			<td>4&amp;up</td>
			<td>7f Dirt</td>
			<td>FLAGSTAFF</td>
			<td>L. Saez</td>
			<td>J. Sadler</td>
		</tr>
			<tr class="odd">			<td class="num">May 1, 2021</td>
			<td>Derby City Distaff Stakes</td>
			<td>Churchill Downs</td>
			<td>I</td>
			<td>$500,000</td>
			<td>4&amp;up f/m</td>
			<td>7f Dirt</td>
			<td>GAMINE</td>
			<td>J. Velazquez</td>
			<td>B. Baffert</td>
		</tr>
			<tr>			<td class="num">May 1, 2021</td>
			<td>Kentucky Derby</td>
			<td>Churchill Downs</td>
			<td>I</td>
			<td>$3,000,000</td>
			<td>3yo</td>
			<td>10f Dirt</td>
			<td>MEDINA SPIRIT</td>
			<td>J. Velazquez</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">May 1, 2021</td>
			<td>Longines Churchill Distaff Turf Mile</td>
			<td>Churchill Downs</td>
			<td>II</td>
			<td>$500,000</td>
			<td>4&amp;up f/m</td>
			<td>8f Turf</td>
			<td>BLOWOUT</td>
			<td>F. Prat</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">May 1, 2021</td>
			<td>Old Forester Bourbon Turf Classic</td>
			<td>Churchill Downs</td>
			<td>I</td>
			<td>$1,000,000</td>
			<td>4&amp;up</td>
			<td>9f Turf</td>
			<td>COLONEL LIAM</td>
			<td>I. Ortiz, Jr.</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">May 1, 2021</td>
			<td>Aoba Sho</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,120,000</td>
			<td>3yo</td>
			<td>12f Turf</td>
			<td>KINGSTON BOY (JPN)</td>
			<td>C. Lemaire</td>
			<td>K. Fujisawa</td>
		</tr>
			<tr>			<td class="num">May 1, 2021</td>
			<td>Senorita Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo f</td>
			<td>8f Turf</td>
			<td>MADONE</td>
			<td>J. Hernandez</td>
			<td>S. Callaghan</td>
		</tr>
			<tr class="odd">			<td class="num">May 2, 2021</td>
			<td>Ruffian Stakes</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>4&amp;up f/m</td>
			<td>8f Dirt</td>
			<td>VAULT</td>
			<td>J. Rosario</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">May 2, 2021</td>
			<td>Tenno Sho Spring</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$3,096,000</td>
			<td>4&amp;up</td>
			<td>16f Turf</td>
			<td>WORLD PREMIERE (JPN)</td>
			<td>Y. Fukunaga</td>
			<td>Y. Tomomichi</td>
		</tr>
			<tr class="odd">			<td class="num">May 8, 2021</td>
			<td>Peter Pan Stakes</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>PROMISE KEEPER</td>
			<td>L. Saez</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">May 8, 2021</td>
			<td>Runhappy Stakes</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up</td>
			<td>6f Dirt</td>
			<td>FIRENZE FIRE</td>
			<td>I. Ortiz, Jr.</td>
			<td>K. Breen</td>
		</tr>
			<tr class="odd">			<td class="num">May 8, 2021</td>
			<td>Vagrancy</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up f/m</td>
			<td>6.5f Dirt</td>
			<td>VICTIM OF LOVE</td>
			<td>J. Rosario</td>
			<td>T. Beattie</td>
		</tr>
			<tr>			<td class="num">May 8, 2021</td>
			<td>Kyoto Shimbun Hai</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,120,000</td>
			<td>3yo</td>
			<td>11f Turf</td>
			<td>BREAKUP (JPN)</td>
			<td>T. Danno</td>
			<td>Y. Kuroiwa</td>
		</tr>
			<tr class="odd">			<td class="num">May 8, 2021</td>
			<td>Santa Barbara Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up f/m</td>
			<td>12f Turf</td>
			<td>NEIGE BLANCHE</td>
			<td>J. Hernandez</td>
			<td>L. Powell</td>
		</tr>
			<tr>			<td class="num">May 9, 2021</td>
			<td>NHK Mile Cup</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,160,000</td>
			<td>3yo</td>
			<td>8f Turf</td>
			<td>RAYMOND BAROWS (JPN)</td>
			<td>S. Hamanaka</td>
			<td>H. Uemura</td>
		</tr>
			<tr class="odd">			<td class="num">May 9, 2021</td>
			<td>Niigata Daishoten</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>4&amp;up</td>
			<td>10f Turf</td>
			<td>POTAGER (JPN)</td>
			<td>A. Nishimura</td>
			<td>Y. Tomomichi</td>
		</tr>
			<tr>			<td class="num">May 14, 2021</td>
			<td>Allaire duPont Distaff Stakes</td>
			<td>Pimlico</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up f/m</td>
			<td>9f Dirt</td>
			<td>SPICE IS NICE</td>
			<td>J. Velazquez</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">May 14, 2021</td>
			<td>Black-Eyed Susan Stakes</td>
			<td>Pimlico</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3yo f</td>
			<td>9f Dirt</td>
			<td>ARMY WIFE</td>
			<td>J. Rosario</td>
			<td>M. Maker</td>
		</tr>
			<tr>			<td class="num">May 14, 2021</td>
			<td>Miss Preakness Stakes</td>
			<td>Pimlico</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3yo f</td>
			<td>6f Dirt</td>
			<td>RED GHOST</td>
			<td>J. Velazquez</td>
			<td>W. Ward</td>
		</tr>
			<tr class="odd">			<td class="num">May 14, 2021</td>
			<td>Pimlico Special</td>
			<td>Pimlico</td>
			<td>III</td>
			<td>$250,000</td>
			<td>3&amp;up</td>
			<td>9.5f Dirt</td>
			<td>LAST JUDGEMENT</td>
			<td>J. Ortiz</td>
			<td>M. Maker</td>
		</tr>
			<tr>			<td class="num">May 15, 2021</td>
			<td>Soaring Softly Stakes</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo f</td>
			<td>7f Turf</td>
			<td>BYE BYE</td>
			<td>E. Cancel</td>
			<td>C. Clement</td>
		</tr>
			<tr class="odd">			<td class="num">May 15, 2021</td>
			<td>Louisville Stakes</td>
			<td>Churchill Downs</td>
			<td>III</td>
			<td>$125,000</td>
			<td>4&amp;up</td>
			<td>12f Turf</td>
			<td>ARKLOW</td>
			<td>F. Geroux</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">May 15, 2021</td>
			<td>Keio Hai Spring Cup</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,221,000</td>
			<td>4&amp;up</td>
			<td>7f Turf</td>
			<td>AIR ALMAS (USA)</td>
			<td>K. Matsuyama</td>
			<td>M. Ikezoe</td>
		</tr>
			<tr class="odd">			<td class="num">May 15, 2021</td>
			<td>Chick Lang Stakes</td>
			<td>Pimlico</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3yo</td>
			<td>6f Dirt</td>
			<td>MIGHTY MISCHIEF</td>
			<td>R. Santana, Jr.</td>
			<td>S. Asmussen</td>
		</tr>
			<tr>			<td class="num">May 15, 2021</td>
			<td>Dinner Party Stakes</td>
			<td>Pimlico</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3&amp;up</td>
			<td>8.5f Turf</td>
			<td>SOMELIKEITHOTBROWN</td>
			<td>J. Ortiz</td>
			<td>M. Maker</td>
		</tr>
			<tr class="odd">			<td class="num">May 15, 2021</td>
			<td>Gallorette Stakes</td>
			<td>Pimlico</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up f/m</td>
			<td>8.5f Turf</td>
			<td>MEAN MARY</td>
			<td>L. Saez</td>
			<td>H. Motion</td>
		</tr>
			<tr>			<td class="num">May 15, 2021</td>
			<td>Maryland Sprint Stakes</td>
			<td>Pimlico</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up</td>
			<td>6f Dirt</td>
			<td>SPECIAL RESERVE</td>
			<td>I. Ortiz, Jr.</td>
			<td>M. Maker</td>
		</tr>
			<tr class="odd">			<td class="num">May 15, 2021</td>
			<td>Preakness Stakes</td>
			<td>Pimlico</td>
			<td>I</td>
			<td>$1,000,000</td>
			<td>3yo</td>
			<td>9.5f Dirt</td>
			<td>ROMBAUER</td>
			<td>F. Prat</td>
			<td>M. McCarthy</td>
		</tr>
			<tr>			<td class="num">May 15, 2021</td>
			<td>Lazaro Barrera Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo</td>
			<td>6.5f Dirt</td>
			<td>THE CHOSEN VRON</td>
			<td>U. Rispoli</td>
			<td>J. Kruljac</td>
		</tr>
			<tr class="odd">			<td class="num">May 16, 2021</td>
			<td>Victoria Mile</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,160,000</td>
			<td>4&amp;up f/m</td>
			<td>8f Turf</td>
			<td>RESISTENCIA (JPN)</td>
			<td>Y. Take</td>
			<td>T. Matsushita</td>
		</tr>
			<tr>			<td class="num">May 22, 2021</td>
			<td>Winning Colors Stakes</td>
			<td>Churchill Downs</td>
			<td>III</td>
			<td>$125,000</td>
			<td>4&amp;up f/m</td>
			<td>6f Dirt</td>
			<td>SCONSIN</td>
			<td>T. Gaffalione</td>
			<td>G. Foley</td>
		</tr>
			<tr class="odd">			<td class="num">May 22, 2021</td>
			<td>Heian Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$737,000</td>
			<td>4&amp;up</td>
			<td>9.5f Dirt</td>
			<td>SUAVE ARAMIS (JPN)</td>
			<td>D. Matsuda</td>
			<td>N. Sugai</td>
		</tr>
			<tr>			<td class="num">May 22, 2021</td>
			<td>Santa Maria Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>AS TIME GOES BY</td>
			<td>M. Smith</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">May 23, 2021</td>
			<td>Yushun Himba</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,273,000</td>
			<td>3yo f</td>
			<td>12f Turf</td>
			<td>THROUGH SEVEN SEAS (JPN)</td>
			<td>K. Tosaki</td>
			<td>T. Ozeki</td>
		</tr>
			<tr>			<td class="num">May 29, 2021</td>
			<td>Pennine Ridge</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3yo</td>
			<td>9f Turf</td>
			<td>SAINTHOOD</td>
			<td>J. Rosario</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">May 29, 2021</td>
			<td>Matt Winn Stakes</td>
			<td>Churchill Downs</td>
			<td>III</td>
			<td>$125,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>FULSOME</td>
			<td>F. Geroux</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">May 29, 2021</td>
			<td>Charles Whittingham Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td>AWARD WINNER</td>
			<td>J. Hernandez</td>
			<td>D. Hofmans</td>
		</tr>
			<tr class="odd">			<td class="num">May 29, 2021</td>
			<td>Daytona Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>4&amp;up</td>
			<td>6.5f Turf</td>
			<td>BOMBARD</td>
			<td>F. Prat</td>
			<td>R. Mandella</td>
		</tr>
			<tr>			<td class="num">May 29, 2021</td>
			<td>Triple Bend Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>7f Dirt</td>
			<td>MAGIC ON TAP</td>
			<td>J. Hernandez</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">May 30, 2021</td>
			<td>Meguro Kinen</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,173,000</td>
			<td>4&amp;up</td>
			<td>12.5f Turf</td>
			<td>GOLD GEAR (JPN)</td>
			<td>H. Tanabe</td>
			<td>K. Ito</td>
		</tr>
			<tr>			<td class="num">May 30, 2021</td>
			<td>Tokyo Yushun</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$4,115,000</td>
			<td>3yo</td>
			<td>12f Turf</td>
			<td>WONDERFUL TOWN (JPN)</td>
			<td>R. Wada</td>
			<td>Y. Takahashi</td>
		</tr>
			<tr class="odd">			<td class="num">May 30, 2021</td>
			<td>Summertime Oaks</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>CRAZY BEAUTIFUL</td>
			<td>M. Smith</td>
			<td>K. McPeek</td>
		</tr>
			<tr>			<td class="num">May 31, 2021</td>
			<td>Steve Sexton Mile Stakes</td>
			<td>Lone Star Park</td>
			<td>III</td>
			<td>$400,000</td>
			<td>3&amp;up</td>
			<td>8f Dirt</td>
			<td>MO MOSA</td>
			<td>R. Vazquez</td>
			<td>M. Maker</td>
		</tr>
			<tr class="odd">			<td class="num">May 31, 2021</td>
			<td>Gamely Stakes</td>
			<td>Santa Anita</td>
			<td>I</td>
			<td>$300,000</td>
			<td>3&amp;up f/m</td>
			<td>9f Turf</td>
			<td>MAXIM RATE</td>
			<td>J. Hernandez</td>
			<td>S. Callaghan</td>
		</tr>
			<tr>			<td class="num">May 31, 2021</td>
			<td>Hollywood Gold Cup</td>
			<td>Santa Anita</td>
			<td>I</td>
			<td>$300,000</td>
			<td>3&amp;up</td>
			<td>10f Dirt</td>
			<td>COUNTRY GRAMMER</td>
			<td>F. Prat</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">May 31, 2021</td>
			<td>Shoemaker Mile Stakes</td>
			<td>Santa Anita</td>
			<td>I</td>
			<td>$300,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>SMOOTH LIKE STRAIGHT</td>
			<td>U. Rispoli</td>
			<td>M. McCarthy</td>
		</tr>
			<tr>			<td class="num">Jun 3, 2021</td>
			<td>Intercontinental Stakes</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>4&amp;up f/m</td>
			<td>7f Turf</td>
			<td>CHANGE OF CONTROL</td>
			<td>C. Hernandez</td>
			<td>M. Lovell</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 3, 2021</td>
			<td>Wonder Again Stakes</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>9f Turf</td>
			<td>CON LIMA</td>
			<td>F. Prat</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">Jun 4, 2021</td>
			<td>New York Stakes</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$750,000</td>
			<td>4&amp;up f/m</td>
			<td>10f Turf</td>
			<td>MEAN MARY</td>
			<td>L. Saez</td>
			<td>H. Motion</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 4, 2021</td>
			<td>True North Stakes</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$300,000</td>
			<td>4&amp;up</td>
			<td>6.5f Dirt</td>
			<td>FIRENZE FIRE</td>
			<td>J. Ortiz</td>
			<td>K. Breen</td>
		</tr>
			<tr>			<td class="num">Jun 5, 2021</td>
			<td>Acorn Stakes</td>
			<td>Belmont Park</td>
			<td>I</td>
			<td>$500,000</td>
			<td>3yo f</td>
			<td>8f Dirt</td>
			<td>SEARCH RESULTS</td>
			<td>J. Castellano</td>
			<td>C. Brown</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 5, 2021</td>
			<td>Belmont Stakes</td>
			<td>Belmont Park</td>
			<td>I</td>
			<td>$1,500,000</td>
			<td>3yo</td>
			<td>12f Dirt</td>
			<td>ESSENTIAL QUALITY</td>
			<td>L. Saez</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">Jun 5, 2021</td>
			<td>Brooklyn Stakes</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$400,000</td>
			<td>4&amp;up</td>
			<td>12f Dirt</td>
			<td>LONE ROCK</td>
			<td>R. Vazquez</td>
			<td>R. Diodoro</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 5, 2021</td>
			<td>Jaipur Stakes</td>
			<td>Belmont Park</td>
			<td>I</td>
			<td>$400,000</td>
			<td>3&amp;up</td>
			<td>6f Turf</td>
			<td>CASA CREED</td>
			<td>J. Alvarado</td>
			<td>W. Mott</td>
		</tr>
			<tr>			<td class="num">Jun 5, 2021</td>
			<td>Just a Game Stakes</td>
			<td>Belmont Park</td>
			<td>I</td>
			<td>$500,000</td>
			<td>4&amp;up f/m</td>
			<td>8f Turf</td>
			<td>ALTHIQA</td>
			<td>M. Smith</td>
			<td>C. Appleby</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 5, 2021</td>
			<td>Manhattan Stakes</td>
			<td>Belmont Park</td>
			<td>I</td>
			<td>$750,000</td>
			<td>4&amp;up</td>
			<td>10f Turf</td>
			<td>DOMESTIC SPENDING</td>
			<td>F. Prat</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Jun 5, 2021</td>
			<td>Metropolitan Handicap</td>
			<td>Belmont Park</td>
			<td>I</td>
			<td>$1,000,000</td>
			<td>3&amp;up</td>
			<td>8f Dirt</td>
			<td>SILVER STATE</td>
			<td>R. Santana, Jr.</td>
			<td>S. Asmussen</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 5, 2021</td>
			<td>Ogden Phipps Stakes</td>
			<td>Belmont Park</td>
			<td>I</td>
			<td>$500,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>LETRUSKA</td>
			<td>J. Ortiz</td>
			<td>F. Gutierrez</td>
		</tr>
			<tr>			<td class="num">Jun 5, 2021</td>
			<td>Woody Stephens Stakes</td>
			<td>Belmont Park</td>
			<td>I</td>
			<td>$400,000</td>
			<td>3yo</td>
			<td>7f Dirt</td>
			<td>DRAIN THE CLOCK</td>
			<td>J. Ortiz</td>
			<td>S. Joseph, Jr.</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 5, 2021</td>
			<td>Naruo Kinen</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td>BLAST ONEPIECE (JPN)</td>
			<td>Y. Iwata</td>
			<td>M. Otake</td>
		</tr>
			<tr>			<td class="num">Jun 5, 2021</td>
			<td>Monmouth Stakes</td>
			<td>Monmouth Park</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up</td>
			<td>9f Turf</td>
			<td>DEVAMANI</td>
			<td>N. Juarez</td>
			<td>C. Brown</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 6, 2021</td>
			<td>Yasuda Kinen</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,362,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>DAIWA CAGNEY (JPN)</td>
			<td>S. Ishibashi</td>
			<td>T. Kikuzawa</td>
		</tr>
			<tr>			<td class="num">Jun 12, 2021</td>
			<td>Old Forester Mint Julep Stakes</td>
			<td>Churchill Downs</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f Turf</td>
			<td>MINTD</td>
			<td>R. Santana, Jr.</td>
			<td>B. Walsh</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 12, 2021</td>
			<td>Salvator Mile Stakes</td>
			<td>Monmouth Park</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up</td>
			<td>8f Dirt</td>
			<td>INFORMATIVE</td>
			<td>J. Ferrer</td>
			<td>U. St. Lewis</td>
		</tr>
			<tr>			<td class="num">Jun 13, 2021</td>
			<td>Epsom Cup</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>3&amp;up</td>
			<td>9f Turf</td>
			<td>MOIRA AITHON (JPN)</td>
			<td>K. Kikuzawa</td>
			<td>T. Kikuzawa</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 13, 2021</td>
			<td>Hakodate Sprint Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$808,000</td>
			<td>3&amp;up</td>
			<td>6f Turf</td>
			<td>SENSHO YUTO (JPN)</td>
			<td>Y. Nakai</td>
			<td>K. Sasada</td>
		</tr>
			<tr>			<td class="num">Jun 13, 2021</td>
			<td>Affirmed Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>THE CHOSEN VRON</td>
			<td>U. Rispoli</td>
			<td>J. Kruljac</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 15, 2021</td>
			<td>Coventry Stakes</td>
			<td>Ascot</td>
			<td>II</td>
			<td>$137,000</td>
			<td>2yo</td>
			<td>6f Turf</td>
			<td>BERKSHIRE SHADOW</td>
			<td>O. Murphy</td>
			<td>A. Balding</td>
		</tr>
			<tr>			<td class="num">Jun 15, 2021</td>
			<td>Queen Anne Stakes</td>
			<td>Ascot</td>
			<td>I</td>
			<td>$548,000</td>
			<td>4&amp;up</td>
			<td>8f Turf</td>
			<td>PALACE PIER</td>
			<td>L. Dettori</td>
			<td>J &amp; T Gosden</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 16, 2021</td>
			<td>Duke of Cambridge Stakes</td>
			<td>Ascot</td>
			<td>II</td>
			<td>$192,000</td>
			<td>4&amp;up f/m</td>
			<td>8f Turf</td>
			<td>INDIE ANGEL</td>
			<td>L. Dettori</td>
			<td>J &amp; T Gosden</td>
		</tr>
			<tr>			<td class="num">Jun 16, 2021</td>
			<td>Queen Mary Stakes</td>
			<td>Ascot</td>
			<td>II</td>
			<td>$110,000</td>
			<td>2yo f</td>
			<td>5f Turf</td>
			<td>QUICK SUZY</td>
			<td>G. Carroll</td>
			<td>G. Cromwell</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 17, 2021</td>
			<td>Gold Cup Stakes</td>
			<td>Ascot</td>
			<td>I</td>
			<td>$480,000</td>
			<td>4&amp;up</td>
			<td>20f Turf</td>
			<td>SUBJECTIVIST</td>
			<td>J. Fanning</td>
			<td>M. Johnston</td>
		</tr>
			<tr>			<td class="num">Jun 17, 2021</td>
			<td>Hampton Court Stakes</td>
			<td>Ascot</td>
			<td>III</td>
			<td>$103,000</td>
			<td>3yo</td>
			<td>10f Turf</td>
			<td>MOHAAFETH</td>
			<td>J. Crowley</td>
			<td>W. Haggas</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 17, 2021</td>
			<td>Norfolk Stakes</td>
			<td>Ascot</td>
			<td>II</td>
			<td>$110,000</td>
			<td>2yo</td>
			<td>5f Turf</td>
			<td>PERFECT POWER</td>
			<td>P. Hanagan</td>
			<td>R. Fahey</td>
		</tr>
			<tr>			<td class="num">Jun 17, 2021</td>
			<td>Ribblesdale Stakes</td>
			<td>Ascot</td>
			<td>II</td>
			<td>$219,000</td>
			<td>3yo f</td>
			<td>12f Turf</td>
			<td>LOVING DREAM</td>
			<td>R. Havlin</td>
			<td>J &amp; T Gosden</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 18, 2021</td>
			<td>Albany Stakes</td>
			<td>Ascot</td>
			<td>III</td>
			<td>$89,000</td>
			<td>2yo f</td>
			<td>6f Turf</td>
			<td>SANDRINE</td>
			<td>D. Probert</td>
			<td>A. Balding</td>
		</tr>
			<tr>			<td class="num">Jun 18, 2021</td>
			<td>Commonwealth Cup</td>
			<td>Ascot</td>
			<td>I</td>
			<td>$480,000</td>
			<td>3yo</td>
			<td>6f Turf</td>
			<td>CAMPANELLE</td>
			<td>L. Dettori</td>
			<td>W. Ward</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 18, 2021</td>
			<td>Coronation Stakes</td>
			<td>Ascot</td>
			<td>I</td>
			<td>$480,000</td>
			<td>3yo f</td>
			<td>8f Turf</td>
			<td>ALCOHOL FREE</td>
			<td>O. Murphy</td>
			<td>A. Balding</td>
		</tr>
			<tr>			<td class="num">Jun 18, 2021</td>
			<td>King Edward VII Stakes</td>
			<td>Ascot</td>
			<td>II</td>
			<td>$219,000</td>
			<td>3yo c/g</td>
			<td>12f Turf</td>
			<td>ALENQUER</td>
			<td>T. Marquand</td>
			<td>W. Haggas</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 19, 2021</td>
			<td>Diamond Jubilee Stakes</td>
			<td>Ascot</td>
			<td>I</td>
			<td>$959,000</td>
			<td>4&amp;up</td>
			<td>6f Turf</td>
			<td>DREAM OF DREAMS</td>
			<td>R. Moore</td>
			<td>S. Stoute</td>
		</tr>
			<tr>			<td class="num">Jun 19, 2021</td>
			<td>Hardwicke Stakes</td>
			<td>Ascot</td>
			<td>II</td>
			<td>$219,000</td>
			<td>4&amp;up</td>
			<td>12f Turf</td>
			<td>WONDERFUL TONIGHT</td>
			<td>W. Buick</td>
			<td>D. Menuisier</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 19, 2021</td>
			<td>Jersey Stakes</td>
			<td>Ascot</td>
			<td>III</td>
			<td>$103,000</td>
			<td>3yo</td>
			<td>7f Turf</td>
			<td>CREATIVE FORCE</td>
			<td>J. Doyle</td>
			<td>C. Appleby</td>
		</tr>
			<tr>			<td class="num">Jun 19, 2021</td>
			<td>Whimsical Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up f/m</td>
			<td>6f AW</td>
			<td>BOARDROOM</td>
			<td>L. Contreras</td>
			<td>J. Carroll</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 20, 2021</td>
			<td>Poker</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$250,000</td>
			<td>4&amp;up</td>
			<td>8f Turf</td>
			<td>OLEKSANDRA</td>
			<td>J. Rosario</td>
			<td>N. Drysdale</td>
		</tr>
			<tr>			<td class="num">Jun 20, 2021</td>
			<td>Mermaid Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$737,000</td>
			<td>3&amp;up f/m</td>
			<td>10f Turf</td>
			<td>SHAMROCK HILL (JPN)</td>
			<td>T. Fujikake</td>
			<td>S. Sasaki</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 20, 2021</td>
			<td>Unicorn Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$721,000</td>
			<td>3yo</td>
			<td>8f Dirt</td>
			<td>CA VA (JPN)</td>
			<td>Y. Ishikawa</td>
			<td>H. Uemura</td>
		</tr>
			<tr>			<td class="num">Jun 20, 2021</td>
			<td>Eatontown Stakes</td>
			<td>Monmouth Park</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up f/m</td>
			<td>8.5f Turf</td>
			<td>VIGILANTES WAY</td>
			<td>P. Lopez</td>
			<td>C. McGaughey III</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 20, 2021</td>
			<td>American Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>RESTRAINED VENGENCE</td>
			<td>T. Baze</td>
			<td>V. Brinkerhoff</td>
		</tr>
			<tr>			<td class="num">Jun 20, 2021</td>
			<td>Jacques Cartier Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up</td>
			<td>6f AW</td>
			<td>SOUPER STONEHENGE</td>
			<td>P. Husbands</td>
			<td>M. Casse</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 26, 2021</td>
			<td>Chicago Stakes</td>
			<td>Arlington</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up f/m</td>
			<td>7f AW</td>
			<td>ABBY HATCHER</td>
			<td>A. Achard</td>
			<td>A. Meah</td>
		</tr>
			<tr>			<td class="num">Jun 26, 2021</td>
			<td>Mother Goose Stakes</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>ZAAJEL</td>
			<td>J. Rosario</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 26, 2021</td>
			<td>Bashford Manor Stakes</td>
			<td>Churchill Downs</td>
			<td>III</td>
			<td>$150,000</td>
			<td>2yo</td>
			<td>6f Dirt</td>
			<td>DOUBLE THUNDER</td>
			<td>J. Velazquez</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">Jun 26, 2021</td>
			<td>Fleur de Lis</td>
			<td>Churchill Downs</td>
			<td>II</td>
			<td>$300,000</td>
			<td>4&amp;up f/m</td>
			<td>9f Dirt</td>
			<td>LETRUSKA</td>
			<td>J. Ortiz</td>
			<td>F. Gutierrez</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 26, 2021</td>
			<td>Stephen Foster</td>
			<td>Churchill Downs</td>
			<td>II</td>
			<td>$600,000</td>
			<td>4&amp;up</td>
			<td>9f Dirt</td>
			<td>MAXFIELD</td>
			<td>J. Ortiz</td>
			<td>B. Walsh</td>
		</tr>
			<tr>			<td class="num">Jun 26, 2021</td>
			<td>Wise Dan Stakes</td>
			<td>Churchill Downs</td>
			<td>II</td>
			<td>$300,000</td>
			<td>4&amp;up</td>
			<td>8.5f Turf</td>
			<td>SET PIECE</td>
			<td>F. Geroux</td>
			<td>B. Cox</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 26, 2021</td>
			<td>Ohio Derby</td>
			<td>Thistledown</td>
			<td>III</td>
			<td>$500,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>MASQUEPARADE</td>
			<td>M. Mena</td>
			<td>A. Stall, Jr.</td>
		</tr>
			<tr>			<td class="num">Jun 26, 2021</td>
			<td>Trillium Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f AW</td>
			<td>SOUPER ESCAPE</td>
			<td>L. Contreras</td>
			<td>M. Trombetta</td>
		</tr>
			<tr class="odd">			<td class="num">Jun 27, 2021</td>
			<td>Takarazuka Kinen</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$3,096,000</td>
			<td>3&amp;up</td>
			<td>11f Turf</td>
			<td>CADENAS (JPN)</td>
			<td>S. Hamanaka</td>
			<td>K. Nakatake</td>
		</tr>
			<tr>			<td class="num">Jul 1, 2021</td>
			<td>Dominion Day Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up</td>
			<td>8.5f AW</td>
			<td>MIGHTY HEART</td>
			<td>D. Fukumoto</td>
			<td>J. Carroll</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 2, 2021</td>
			<td>Iowa Oaks</td>
			<td>Prairie Meadows</td>
			<td>III</td>
			<td>$225,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>ARMY WIFE</td>
			<td>J. Rosario</td>
			<td>M. Maker</td>
		</tr>
			<tr>			<td class="num">Jul 2, 2021</td>
			<td>Prairie Meadows Cornhusker Handicap</td>
			<td>Prairie Meadows</td>
			<td>III</td>
			<td>$300,000</td>
			<td>3&amp;up</td>
			<td>9f Dirt</td>
			<td>KNICKS GO</td>
			<td>J. Rosario</td>
			<td>B. Cox</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 3, 2021</td>
			<td>Suburban</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$400,000</td>
			<td>4&amp;up</td>
			<td>10f Dirt</td>
			<td>MAX PLAYER</td>
			<td>R. Santana, Jr.</td>
			<td>S. Asmussen</td>
		</tr>
			<tr>			<td class="num">Jul 3, 2021</td>
			<td>Delaware Oaks</td>
			<td>Delaware Park</td>
			<td>III</td>
			<td>$300,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>CRAZY BEAUTIFUL</td>
			<td>M. Smith</td>
			<td>K. McPeek</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 3, 2021</td>
			<td>Kent Stakes</td>
			<td>Delaware Park</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3yo</td>
			<td>9f Turf</td>
			<td>YES THIS TIME</td>
			<td>J. Bravo</td>
			<td>K. Breen</td>
		</tr>
			<tr>			<td class="num">Jul 3, 2021</td>
			<td>Princess Rooney Invitational</td>
			<td>Gulfstream Park</td>
			<td>II</td>
			<td>$350,000</td>
			<td>3&amp;up f/m</td>
			<td>7f Dirt</td>
			<td>CE CE</td>
			<td>V. Espinoza</td>
			<td>M. McCarthy</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 3, 2021</td>
			<td>Smile Sprint Invitational</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>6f Dirt</td>
			<td>MILES AHEAD</td>
			<td>V. Espinoza</td>
			<td>E. Plesa, Jr.</td>
		</tr>
			<tr>			<td class="num">Jul 4, 2021</td>
			<td>John A. Nerud</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$250,000</td>
			<td>4&amp;up</td>
			<td>7f Dirt</td>
			<td>MIND CONTROL</td>
			<td>J. Velazquez</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 4, 2021</td>
			<td>CBC Sho</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$808,000</td>
			<td>3&amp;up</td>
			<td>6f Turf</td>
			<td>A WILL A WAY (JPN)</td>
			<td>K. Matsuyama</td>
			<td>T. Takano</td>
		</tr>
			<tr>			<td class="num">Jul 4, 2021</td>
			<td>Radio Nikkei Sho</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$780,000</td>
			<td>3yo</td>
			<td>9f Turf</td>
			<td>WORLD REVIVAL (JPN)</td>
			<td>A. Tsumura</td>
			<td>K. Makita</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 4, 2021</td>
			<td>Los Alamitos Derby</td>
			<td>Los Alamitos</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>CLASSIER</td>
			<td>M. Smith</td>
			<td>B. Baffert</td>
		</tr>
			<tr>			<td class="num">Jul 5, 2021</td>
			<td>Dwyer Stakes</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$250,000</td>
			<td>3yo</td>
			<td>8f Dirt</td>
			<td>FIRST CAPTAIN</td>
			<td>J. Ortiz</td>
			<td>C. McGaughey III</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 5, 2021</td>
			<td>Great Lady M. Stakes</td>
			<td>Los Alamitos</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up f/m</td>
			<td>6.5f Dirt</td>
			<td>GAMINE</td>
			<td>J. Velazquez</td>
			<td>B. Baffert</td>
		</tr>
			<tr>			<td class="num">Jul 7, 2021</td>
			<td>Indiana Derby</td>
			<td>Indiana Grand</td>
			<td>III</td>
			<td>$300,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>MR. WIRELESS</td>
			<td>R. Vazquez</td>
			<td>W. Calhoun</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 7, 2021</td>
			<td>Indiana Oaks</td>
			<td>Indiana Grand</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>SOOTHSAY</td>
			<td>F. Prat</td>
			<td>R. Mandella</td>
		</tr>
			<tr>			<td class="num">Jul 10, 2021</td>
			<td>Victory Ride Stakes</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3yo f</td>
			<td>6.5f Dirt</td>
			<td>SOUPER SENSATIONAL</td>
			<td>F. Prat</td>
			<td>M. Casse</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 10, 2021</td>
			<td>Delaware Handicap</td>
			<td>Delaware Park</td>
			<td>II</td>
			<td>$400,000</td>
			<td>3&amp;up f/m</td>
			<td>10f Dirt</td>
			<td>MISS MARISSA</td>
			<td>D. Centeno</td>
			<td>J. Ryerson</td>
		</tr>
			<tr>			<td class="num">Jul 10, 2021</td>
			<td>Robert G. Dick Memorial Stakes</td>
			<td>Delaware Park</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up f/m</td>
			<td>11f Turf</td>
			<td>DALIKA</td>
			<td>M. Mena</td>
			<td>A. Stall, Jr.</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 10, 2021</td>
			<td>Selene Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3yo f</td>
			<td>8.5f AW</td>
			<td>OUR FLASH DRIVE</td>
			<td>P. Husbands</td>
			<td>M. Casse</td>
		</tr>
			<tr>			<td class="num">Jul 11, 2021</td>
			<td>Procyon Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$737,000</td>
			<td>3&amp;up</td>
			<td>8.5f Dirt</td>
			<td>MEISHO UZUMASA (JPN)</td>
			<td>A. Saito</td>
			<td>T. Yasuda</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 11, 2021</td>
			<td>Tanabata Sho</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td>MOUNT GOLD (JPN)</td>
			<td>M. Iwata</td>
			<td>Y. Ikee</td>
		</tr>
			<tr>			<td class="num">Jul 11, 2021</td>
			<td>Marine Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3yo</td>
			<td>8.5f AW</td>
			<td>EASY TIME</td>
			<td>R. Hernandez</td>
			<td>M. Casse</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 15, 2021</td>
			<td>Quick Call Stakes</td>
			<td>Saratoga Race Course</td>
			<td>III</td>
			<td>$120,000</td>
			<td>3yo</td>
			<td>5.5f Turf</td>
			<td>GOLDEN PAL</td>
			<td>I. Ortiz, Jr.</td>
			<td>W. Ward</td>
		</tr>
			<tr>			<td class="num">Jul 15, 2021</td>
			<td>Schuylerville Stakes</td>
			<td>Saratoga Race Course</td>
			<td>III</td>
			<td>$150,000</td>
			<td>2yo f</td>
			<td>6f Dirt</td>
			<td>PRETTY BIRDIE</td>
			<td>L. Saez</td>
			<td>N. Casse</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 16, 2021</td>
			<td>Forbidden Apple</td>
			<td>Saratoga Race Course</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up</td>
			<td>8f Turf</td>
			<td>RINALDI</td>
			<td>L. Saez</td>
			<td>H. Bond</td>
		</tr>
			<tr>			<td class="num">Jul 17, 2021</td>
			<td>Arlington Stakes</td>
			<td>Arlington</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up</td>
			<td>9.5f Turf</td>
			<td>BIZEE CHANNEL</td>
			<td>J. Loveberry</td>
			<td>L. Rivelli</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 17, 2021</td>
			<td>Modesty Stakes</td>
			<td>Arlington</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up f/m</td>
			<td>9.5f Turf</td>
			<td>NAVAL LAUGHTER</td>
			<td>S. Doyle</td>
			<td>C. Davis</td>
		</tr>
			<tr>			<td class="num">Jul 17, 2021</td>
			<td>San Diego Handicap</td>
			<td>Del Mar</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3&amp;up</td>
			<td>8.5f Dirt</td>
			<td>EXPRESS TRAIN</td>
			<td>J. Hernandez</td>
			<td>J. Shirreffs</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 17, 2021</td>
			<td>Hakodate Nisai Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$635,000</td>
			<td>2yo</td>
			<td>6f Turf</td>
			<td>LOVE ME DOLL (JPN)</td>
			<td>Y. Furukawa</td>
			<td>H. Kakugawa</td>
		</tr>
			<tr>			<td class="num">Jul 17, 2021</td>
			<td>Haskell Stakes</td>
			<td>Monmouth Park</td>
			<td>I</td>
			<td>$1,000,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>MANDALOUN</td>
			<td>F. Geroux</td>
			<td>B. Cox</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 17, 2021</td>
			<td>Molly Pitcher Stakes</td>
			<td>Monmouth Park</td>
			<td>III</td>
			<td>$250,000</td>
			<td>3&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>GRACEFUL PRINCESS</td>
			<td>J. Rosario</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">Jul 17, 2021</td>
			<td>Monmouth Cup Stakes</td>
			<td>Monmouth Park</td>
			<td>III</td>
			<td>$300,000</td>
			<td>3&amp;up</td>
			<td>9f Dirt</td>
			<td>DR POST</td>
			<td>J. Rosario</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 17, 2021</td>
			<td>United Nations Stakes</td>
			<td>Monmouth Park</td>
			<td>I</td>
			<td>$500,000</td>
			<td>3&amp;up</td>
			<td>11f Turf</td>
			<td>TRIBHUVAN</td>
			<td>F. Prat</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Jul 17, 2021</td>
			<td>WinStar Matchmaker Stakes</td>
			<td>Monmouth Park</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up f/m</td>
			<td>9f Turf</td>
			<td>GREAT ISLAND</td>
			<td>J. Rosario</td>
			<td>C. Brown</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 17, 2021</td>
			<td>Diana Stakes</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$500,000</td>
			<td>4&amp;up f/m</td>
			<td>9f Turf</td>
			<td>ALTHIQA</td>
			<td>M. Franco</td>
			<td>C. Appleby</td>
		</tr>
			<tr>			<td class="num">Jul 17, 2021</td>
			<td>Sanford Stakes</td>
			<td>Saratoga Race Course</td>
			<td>III</td>
			<td>$150,000</td>
			<td>2yo</td>
			<td>6f Dirt</td>
			<td>WIT</td>
			<td>I. Ortiz, Jr.</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 18, 2021</td>
			<td>Cougar II Stakes</td>
			<td>Del Mar</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up</td>
			<td>12f Dirt</td>
			<td>TIZAMAGICIAN</td>
			<td>F. Prat</td>
			<td>R. Mandella</td>
		</tr>
			<tr>			<td class="num">Jul 18, 2021</td>
			<td>Chukyo Kinen</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$808,000</td>
			<td>3&amp;up</td>
			<td>9f Turf</td>
			<td>CLAVEL (JPN)</td>
			<td>N. Yokoyama</td>
			<td>S. Yasuda</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 18, 2021</td>
			<td>Hakodate Kinen</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td>MEINER VIRTUS (JPN)</td>
			<td>Y. Tannai</td>
			<td>T. Miya</td>
		</tr>
			<tr>			<td class="num">Jul 23, 2021</td>
			<td>Carraig Insurance British Valiant Stakes</td>
			<td>Ascot</td>
			<td>III</td>
			<td>$82,000</td>
			<td>3&amp;up f/m</td>
			<td>8f Turf</td>
			<td>DREAMLOPER</td>
			<td>O. Murphy</td>
			<td>E. Walker</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 23, 2021</td>
			<td>Lake George Stakes</td>
			<td>Saratoga Race Course</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3yo f</td>
			<td>8f Turf</td>
			<td>TECHNICAL ANALYSIS</td>
			<td>J. Ortiz</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Jul 24, 2021</td>
			<td>King George VI &amp; Queen Elizabeth QIPCO Stakes</td>
			<td>Ascot</td>
			<td>I</td>
			<td>$1,199,000</td>
			<td>3&amp;up</td>
			<td>12f Turf</td>
			<td>ADAYAR</td>
			<td>W. Buick</td>
			<td>C. Appleby</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 24, 2021</td>
			<td>Princess Margaret Keeneland Stakes</td>
			<td>Ascot</td>
			<td>III</td>
			<td>$55,000</td>
			<td>2yo f</td>
			<td>6f Turf</td>
			<td>ZAIN CLAUDETTE</td>
			<td>R. Dawson</td>
			<td>I Mohammed</td>
		</tr>
			<tr>			<td class="num">Jul 24, 2021</td>
			<td>Eddie Read Stakes</td>
			<td>Del Mar</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3&amp;up</td>
			<td>9f Turf</td>
			<td>UNITED</td>
			<td>F. Prat</td>
			<td>R. Mandella</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 24, 2021</td>
			<td>San Clemente Stakes</td>
			<td>Del Mar</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>8f Turf</td>
			<td>MADONE</td>
			<td>J. Hernandez</td>
			<td>S. Callaghan</td>
		</tr>
			<tr>			<td class="num">Jul 24, 2021</td>
			<td>Caress Stakes</td>
			<td>Saratoga Race Course</td>
			<td>III</td>
			<td>$200,000</td>
			<td>4&amp;up f/m</td>
			<td>5.5f Turf</td>
			<td>CARAVEL</td>
			<td>I. Ortiz, Jr.</td>
			<td>E. Merryman</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 24, 2021</td>
			<td>Coaching Club American Oaks</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$500,000</td>
			<td>3yo f</td>
			<td>9f Dirt</td>
			<td>MARACUJA</td>
			<td>R. Santana, Jr.</td>
			<td>R. Atras</td>
		</tr>
			<tr>			<td class="num">Jul 24, 2021</td>
			<td>Nassau Stakes</td>
			<td>Woodbine</td>
			<td>II</td>
			<td>$175,000</td>
			<td>4&amp;up f/m</td>
			<td>8f Turf</td>
			<td>JOLIE OLIMPICA</td>
			<td>L. Contreras</td>
			<td>J. Carroll</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 25, 2021</td>
			<td>Ibis Summer Dash</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$808,000</td>
			<td>3&amp;up</td>
			<td>5f Turf</td>
			<td>MOME JOHDA (JPN)</td>
			<td>Y. Kido</td>
			<td>Y. Ishige</td>
		</tr>
			<tr>			<td class="num">Jul 25, 2021</td>
			<td>Shuvee Stakes</td>
			<td>Saratoga Race Course</td>
			<td>III</td>
			<td>$200,000</td>
			<td>4&amp;up f/m</td>
			<td>9f Dirt</td>
			<td>ROYAL FLAG</td>
			<td>J. Rosario</td>
			<td>C. Brown</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 31, 2021</td>
			<td>Bing Crosby Stakes</td>
			<td>Del Mar</td>
			<td>I</td>
			<td>$300,000</td>
			<td>3&amp;up</td>
			<td>6f Dirt</td>
			<td>DR. SCHIVEL</td>
			<td>F. Prat</td>
			<td>M. Glatt</td>
		</tr>
			<tr>			<td class="num">Jul 31, 2021</td>
			<td>Monmouth Oaks</td>
			<td>Monmouth Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>LEADER OF THE BAND</td>
			<td>F. Pennington</td>
			<td>J. Servis</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 31, 2021</td>
			<td>Alfred G. Vanderbilt Handicap</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$350,000</td>
			<td>3&amp;up</td>
			<td>6f Dirt</td>
			<td>LEXITONIAN</td>
			<td>J. Lezcano</td>
			<td>J. Sisterson</td>
		</tr>
			<tr>			<td class="num">Jul 31, 2021</td>
			<td>Bowling Green</td>
			<td>Saratoga Race Course</td>
			<td>II</td>
			<td>$250,000</td>
			<td>4&amp;up</td>
			<td>11f Turf</td>
			<td>CROSS BORDER</td>
			<td>L. Saez</td>
			<td>M. Maker</td>
		</tr>
			<tr class="odd">			<td class="num">Jul 31, 2021</td>
			<td>Jim Dandy Stakes</td>
			<td>Saratoga Race Course</td>
			<td>II</td>
			<td>$600,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>ESSENTIAL QUALITY</td>
			<td>L. Saez</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">Aug 1, 2021</td>
			<td>Clement L. Hirsch Stakes</td>
			<td>Del Mar</td>
			<td>I</td>
			<td>$300,000</td>
			<td>3&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>SHEDARESTHEDEVIL</td>
			<td>F. Geroux</td>
			<td>B. Cox</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 1, 2021</td>
			<td>Queen Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$737,000</td>
			<td>3&amp;up f/m</td>
			<td>9f Turf</td>
			<td>SHAMROCK HILL (JPN)</td>
			<td>T. Danno</td>
			<td>S. Sasaki</td>
		</tr>
			<tr>			<td class="num">Aug 1, 2021</td>
			<td>Royal North Stakes</td>
			<td>Woodbine</td>
			<td>II</td>
			<td>$175,000</td>
			<td>4&amp;up f/m</td>
			<td>6f Turf</td>
			<td>AMALFI COAST</td>
			<td>J. Stein</td>
			<td>K. Attard</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 1, 2021</td>
			<td>Vigil Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>4&amp;up</td>
			<td>6f AW</td>
			<td>SOUPER STONEHENGE</td>
			<td>P. Husbands</td>
			<td>M. Casse</td>
		</tr>
			<tr>			<td class="num">Aug 6, 2021</td>
			<td>National Museum of Racing Hall of Fame</td>
			<td>Saratoga Race Course</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3yo</td>
			<td>8f Turf</td>
			<td>PUBLIC SECTOR</td>
			<td>F. Prat</td>
			<td>C. Brown</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 6, 2021</td>
			<td>Troy Stakes</td>
			<td>Saratoga Race Course</td>
			<td>III</td>
			<td>$200,000</td>
			<td>4&amp;up</td>
			<td>5.5f Turf</td>
			<td>FAST BOAT</td>
			<td>T. Gaffalione</td>
			<td>J. Sharp</td>
		</tr>
			<tr>			<td class="num">Aug 7, 2021</td>
			<td>Best Pal Stakes</td>
			<td>Del Mar</td>
			<td>II</td>
			<td>$200,000</td>
			<td>2yo</td>
			<td>6f Dirt</td>
			<td>PAPPACAP</td>
			<td>J. Bravo</td>
			<td>M. Casse</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 7, 2021</td>
			<td>Yellow Ribbon Handicap</td>
			<td>Del Mar</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up f/m</td>
			<td>8.5f Turf</td>
			<td>PRINCESS GRACE</td>
			<td>K. Desormeaux</td>
			<td>M. Stidham</td>
		</tr>
			<tr>			<td class="num">Aug 7, 2021</td>
			<td>West Virginia Derby</td>
			<td>Mountaineer</td>
			<td>III</td>
			<td>$500,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>MR. WIRELESS</td>
			<td>R. Vazquez</td>
			<td>W. Calhoun</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 7, 2021</td>
			<td>Glens Falls</td>
			<td>Saratoga Race Course</td>
			<td>II</td>
			<td>$250,000</td>
			<td>4&amp;up f/m</td>
			<td>12f Turf</td>
			<td>WAR LIKE GODDESS</td>
			<td>J. Leparoux</td>
			<td>W. Mott</td>
		</tr>
			<tr>			<td class="num">Aug 7, 2021</td>
			<td>Longines Test Stakes</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$500,000</td>
			<td>3yo f</td>
			<td>7f Dirt</td>
			<td>BELLA SOFIA</td>
			<td>L. Saez</td>
			<td>R. Rodriguez</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 7, 2021</td>
			<td>Whitney Stakes</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$1,000,000</td>
			<td>4&amp;up</td>
			<td>9f Dirt</td>
			<td>KNICKS GO</td>
			<td>J. Rosario</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">Aug 8, 2021</td>
			<td>La Jolla Stakes</td>
			<td>Del Mar</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3yo</td>
			<td>8.5f Turf</td>
			<td>ZOFFARELLI</td>
			<td>D. Van Dyke</td>
			<td>J. Mullins</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 8, 2021</td>
			<td>Elm Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$737,000</td>
			<td>3&amp;up</td>
			<td>8.5f Dirt</td>
			<td>WESTERLUND (JPN)</td>
			<td>Y. Fujioka</td>
			<td>S. Sasaki</td>
		</tr>
			<tr>			<td class="num">Aug 8, 2021</td>
			<td>Leopard Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$823,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>LE CORSAIRE (JPN)</td>
			<td>S. Ishibashi</td>
			<td>N. Hori</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 8, 2021</td>
			<td>Adirondack Stakes</td>
			<td>Saratoga Race Course</td>
			<td>II</td>
			<td>$200,000</td>
			<td>2yo f</td>
			<td>6.5f Dirt</td>
			<td>WICKED HALO</td>
			<td>J. Ortiz</td>
			<td>S. Asmussen</td>
		</tr>
			<tr>			<td class="num">Aug 8, 2021</td>
			<td>Saratoga Oaks Invitational</td>
			<td>Saratoga Race Course</td>
			<td>III</td>
			<td>$700,000</td>
			<td>3yo f</td>
			<td>9.5f Turf</td>
			<td>CON LIMA</td>
			<td>F. Prat</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 14, 2021</td>
			<td>Bruce D.</td>
			<td>Arlington</td>
			<td>I</td>
			<td>$300,000</td>
			<td>3yo</td>
			<td>8f Turf</td>
			<td>POINT ME BY</td>
			<td>L. Saez</td>
			<td>E. Kenneally</td>
		</tr>
			<tr>			<td class="num">Aug 14, 2021</td>
			<td>Mister D.</td>
			<td>Arlington</td>
			<td>I</td>
			<td>$600,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td>TWO EMMYS</td>
			<td>J. Graham</td>
			<td>H. Robertson</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 14, 2021</td>
			<td>Pucker Up Stakes</td>
			<td>Arlington</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo f</td>
			<td>9f Turf</td>
			<td>SHANTISARA</td>
			<td>F. Prat</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Aug 14, 2021</td>
			<td>Fourstardave Handicap</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$500,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>GOT STORMY</td>
			<td>T. Gaffalione</td>
			<td>M. Casse</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 14, 2021</td>
			<td>Saratoga Special</td>
			<td>Saratoga Race Course</td>
			<td>II</td>
			<td>$200,000</td>
			<td>2yo</td>
			<td>6.5f Dirt</td>
			<td>HIGH OAK</td>
			<td>J. Alvarado</td>
			<td>W. Mott</td>
		</tr>
			<tr>			<td class="num">Aug 15, 2021</td>
			<td>Longacres Mile</td>
			<td>Emerald Downs</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up</td>
			<td>8f Dirt</td>
			<td>BACKGROUND</td>
			<td>R. Bowen</td>
			<td>M. Puhich</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 15, 2021</td>
			<td>Kokura Kinen</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td>GRAND SPEED (JPN)</td>
			<td>R. Wada</td>
			<td>M. Nishimura</td>
		</tr>
			<tr>			<td class="num">Aug 15, 2021</td>
			<td>Sekiya Kinen</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$808,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>CHRISTIE (JPN)</td>
			<td>M. Demuro</td>
			<td>H. Sugiyama</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 15, 2021</td>
			<td>King Edward Stakes</td>
			<td>Woodbine</td>
			<td>II</td>
			<td>$175,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>OLYMPIC RUNNER</td>
			<td>R. Hernandez</td>
			<td>M. Casse</td>
		</tr>
			<tr>			<td class="num">Aug 19, 2021</td>
			<td>Jonathan Sheppard Steeplechase</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$150,000</td>
			<td>4&amp;up</td>
			<td>19f Turf</td>
			<td>THE MEAN QUEEN</td>
			<td>T. Garner</td>
			<td>K. Brion</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 20, 2021</td>
			<td>Rancho Bernardo Handicap</td>
			<td>Del Mar</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up f/m</td>
			<td>6.5f Dirt</td>
			<td>EDGEWAY</td>
			<td>J. Bravo</td>
			<td>J. Sadler</td>
		</tr>
			<tr>			<td class="num">Aug 21, 2021</td>
			<td>Del Mar Handicap</td>
			<td>Del Mar</td>
			<td>II</td>
			<td>$300,000</td>
			<td>3&amp;up</td>
			<td>11f Turf</td>
			<td>ASTRONAUT</td>
			<td>V. Espinoza</td>
			<td>J. Shirreffs</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 21, 2021</td>
			<td>Del Mar Mile</td>
			<td>Del Mar</td>
			<td>II</td>
			<td>$300,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>MO FORZA</td>
			<td>F. Prat</td>
			<td>P. Miller</td>
		</tr>
			<tr>			<td class="num">Aug 21, 2021</td>
			<td>Pacific Classic</td>
			<td>Del Mar</td>
			<td>I</td>
			<td>$750,000</td>
			<td>3&amp;up</td>
			<td>10f Dirt</td>
			<td>TRIPOLI</td>
			<td>T. Pereira</td>
			<td>J. Sadler</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 21, 2021</td>
			<td>Torrey Pines Stakes</td>
			<td>Del Mar</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo f</td>
			<td>8f Dirt</td>
			<td>PRIVATE MISSION</td>
			<td>F. Prat</td>
			<td>B. Baffert</td>
		</tr>
			<tr>			<td class="num">Aug 21, 2021</td>
			<td>Philip H. Iselin Stakes</td>
			<td>Monmouth Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>8.5f Dirt</td>
			<td>CODE OF HONOR</td>
			<td>P. Lopez</td>
			<td>C. McGaughey III</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 21, 2021</td>
			<td>Alabama Stakes</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$600,000</td>
			<td>3yo f</td>
			<td>10f Dirt</td>
			<td>MALATHAAT</td>
			<td>J. Velazquez</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">Aug 21, 2021</td>
			<td>Lake Placid</td>
			<td>Saratoga Race Course</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>8.5f Turf</td>
			<td>TECHNICAL ANALYSIS</td>
			<td>J. Ortiz</td>
			<td>C. Brown</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 21, 2021</td>
			<td>Seaway Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up f/m</td>
			<td>7f AW</td>
			<td>BOARDROOM</td>
			<td>L. Contreras</td>
			<td>J. Carroll</td>
		</tr>
			<tr>			<td class="num">Aug 21, 2021</td>
			<td>Singspiel Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td>CORELLI</td>
			<td>K. Kimura</td>
			<td>J. Thomas</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 22, 2021</td>
			<td>Green Flash Handicap</td>
			<td>Del Mar</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up</td>
			<td>5f Turf</td>
			<td>LIEUTENANT DAN</td>
			<td>G. Franco</td>
			<td>S. Miyadi</td>
		</tr>
			<tr>			<td class="num">Aug 22, 2021</td>
			<td>Kitakyushu Kinen</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$808,000</td>
			<td>3&amp;up</td>
			<td>6f Turf</td>
			<td>YOKA YOKA (JPN)</td>
			<td>H. Miyuki</td>
			<td>K. Tani</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 22, 2021</td>
			<td>Sapporo Kinen</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,450,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td>SODASHI (JPN)</td>
			<td>Y. Yoshida</td>
			<td>N. Sugai</td>
		</tr>
			<tr>			<td class="num">Aug 22, 2021</td>
			<td>Dance Smartly Stakes</td>
			<td>Woodbine</td>
			<td>II</td>
			<td>$175,000</td>
			<td>3&amp;up f/m</td>
			<td>10f Turf</td>
			<td>MUTAMAKINA</td>
			<td>D. Davis</td>
			<td>C. Clement</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 22, 2021</td>
			<td>Highlander Stakes</td>
			<td>Woodbine</td>
			<td>I</td>
			<td>$400,000</td>
			<td>3&amp;up</td>
			<td>6f Turf</td>
			<td>SILENT POST</td>
			<td>J. Stein</td>
			<td>N. Gonzalez</td>
		</tr>
			<tr>			<td class="num">Aug 22, 2021</td>
			<td>Ontario Colleen Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3yo f</td>
			<td>8f Turf</td>
			<td>OUR FLASH DRIVE</td>
			<td>P. Husbands</td>
			<td>M. Casse</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 24, 2021</td>
			<td>Smarty Jones Stakes</td>
			<td>Parx Racing</td>
			<td>III</td>
			<td>$300,000</td>
			<td>3yo</td>
			<td>8.5f Dirt</td>
			<td>FULSOME</td>
			<td>F. Geroux</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">Aug 27, 2021</td>
			<td>Charles Town Classic</td>
			<td>Hollywood Casino At Charles Town Races</td>
			<td>II</td>
			<td>$800,000</td>
			<td>3&amp;up</td>
			<td>9f Dirt</td>
			<td>ART COLLECTOR</td>
			<td>L. Saez</td>
			<td>W. Mott</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 27, 2021</td>
			<td>Charles Town Oaks</td>
			<td>Hollywood Casino At Charles Town Races</td>
			<td>III</td>
			<td>$400,000</td>
			<td>3yo f</td>
			<td>7f Dirt</td>
			<td>R ADIOS JERSEY</td>
			<td>P. Lopez</td>
			<td>G. Baxter</td>
		</tr>
			<tr>			<td class="num">Aug 28, 2021</td>
			<td>Ballerina Handicap</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$500,000</td>
			<td>3&amp;up f/m</td>
			<td>7f Dirt</td>
			<td>GAMINE</td>
			<td>J. Velazquez</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 28, 2021</td>
			<td>Ballston Spa</td>
			<td>Saratoga Race Course</td>
			<td>II</td>
			<td>$400,000</td>
			<td>4&amp;up f/m</td>
			<td>8.5f Turf</td>
			<td>VIADERA</td>
			<td>J. Rosario</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Aug 28, 2021</td>
			<td>Forego</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$600,000</td>
			<td>4&amp;up</td>
			<td>7f Dirt</td>
			<td>YAUPON</td>
			<td>R. Santana, Jr.</td>
			<td>S. Asmussen</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 28, 2021</td>
			<td>Personal Ensign</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$600,000</td>
			<td>4&amp;up f/m</td>
			<td>9f Dirt</td>
			<td>LETRUSKA</td>
			<td>I. Ortiz, Jr.</td>
			<td>F. Gutierrez</td>
		</tr>
			<tr>			<td class="num">Aug 28, 2021</td>
			<td>Sword Dancer</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$750,000</td>
			<td>4&amp;up</td>
			<td>12f Turf</td>
			<td>GUFO</td>
			<td>J. Rosario</td>
			<td>C. Clement</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 28, 2021</td>
			<td>Travers Stakes</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$1,250,000</td>
			<td>3yo</td>
			<td>10f Dirt</td>
			<td>ESSENTIAL QUALITY</td>
			<td>L. Saez</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">Aug 29, 2021</td>
			<td>Keeneland Cup</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>3&amp;up</td>
			<td>6f Turf</td>
			<td>LEI HALIA (JPN)</td>
			<td>H. Kameda</td>
			<td>T. Tajima</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 29, 2021</td>
			<td>Niigata Nisai Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$635,000</td>
			<td>2yo</td>
			<td>8f Turf</td>
			<td>SERIFOS (JPN)</td>
			<td>Y. Kawada</td>
			<td>M. Nakauchida</td>
		</tr>
			<tr>			<td class="num">Aug 31, 2021</td>
			<td>New Kent County Virginia Derby</td>
			<td>Colonial Downs</td>
			<td>III</td>
			<td>$250,000</td>
			<td>3yo</td>
			<td>9f Turf</td>
			<td>WOOTTON ASSET</td>
			<td>J. Ortiz</td>
			<td>H. Motion</td>
		</tr>
			<tr class="odd">			<td class="num">Aug 31, 2021</td>
			<td>Parx Dash</td>
			<td>Parx Racing</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>5f Turf</td>
			<td>THE CRITICAL WAY</td>
			<td>P. Lopez</td>
			<td>J. Delgado</td>
		</tr>
			<tr>			<td class="num">Sep 1, 2021</td>
			<td>With Anticipation Stakes</td>
			<td>Saratoga Race Course</td>
			<td>III</td>
			<td>$150,000</td>
			<td>2yo</td>
			<td>8.5f Turf</td>
			<td>COINAGE</td>
			<td>J. Alvarado</td>
			<td>M. Casse</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 4, 2021</td>
			<td>Del Mar Derby</td>
			<td>Del Mar</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3yo</td>
			<td>9f Turf</td>
			<td>NONE ABOVE THE LAW</td>
			<td>J. Bravo</td>
			<td>P. Miller</td>
		</tr>
			<tr>			<td class="num">Sep 4, 2021</td>
			<td>John C. Mabee Stakes</td>
			<td>Del Mar</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up f/m</td>
			<td>9f Turf</td>
			<td>GOING TO VEGAS</td>
			<td>F. Prat</td>
			<td>R. Baltas</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 4, 2021</td>
			<td>Sapporo Nisai Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$635,000</td>
			<td>2yo</td>
			<td>9f Turf</td>
			<td>GEOGLYPH (JPN)</td>
			<td>C. Lemaire</td>
			<td>T. Iwato</td>
		</tr>
			<tr>			<td class="num">Sep 4, 2021</td>
			<td>Flower Bowl</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$600,000</td>
			<td>4&amp;up f/m</td>
			<td>11f Turf</td>
			<td>WAR LIKE GODDESS</td>
			<td>J. Leparoux</td>
			<td>W. Mott</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 4, 2021</td>
			<td>Jockey Club Gold Cup</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$1,000,000</td>
			<td>3&amp;up</td>
			<td>10f Dirt</td>
			<td>MAX PLAYER</td>
			<td>R. Santana, Jr.</td>
			<td>S. Asmussen</td>
		</tr>
			<tr>			<td class="num">Sep 4, 2021</td>
			<td>Prioress Stakes</td>
			<td>Saratoga Race Course</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3yo f</td>
			<td>6f Dirt</td>
			<td>CILLA</td>
			<td>T. Gaffalione</td>
			<td>C. Baker</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 4, 2021</td>
			<td>Saranac Stakes</td>
			<td>Saratoga Race Course</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3yo</td>
			<td>8.5f Turf</td>
			<td>PUBLIC SECTOR</td>
			<td>I. Ortiz, Jr.</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Sep 5, 2021</td>
			<td>Del Mar Debutante Stakes</td>
			<td>Del Mar</td>
			<td>I</td>
			<td>$300,000</td>
			<td>2yo f</td>
			<td>7f Dirt</td>
			<td>GRACE ADLER</td>
			<td>F. Prat</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 5, 2021</td>
			<td>Kokura Nisai Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$635,000</td>
			<td>2yo</td>
			<td>6f Turf</td>
			<td>NAMURA CLAIR (JPN)</td>
			<td>S. Hamanaka</td>
			<td>K. Hasegawa</td>
		</tr>
			<tr>			<td class="num">Sep 5, 2021</td>
			<td>Niigata Kinen</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td>MEINER FANRONG (JPN)</td>
			<td>M. Demuro</td>
			<td>T. Tezuka</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 5, 2021</td>
			<td>Spinaway Stakes</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$300,000</td>
			<td>2yo f</td>
			<td>7f Dirt</td>
			<td>ECHO ZULU</td>
			<td>R. Santana, Jr.</td>
			<td>S. Asmussen</td>
		</tr>
			<tr>			<td class="num">Sep 6, 2021</td>
			<td>Runhappy Del Mar Futurity</td>
			<td>Del Mar</td>
			<td>I</td>
			<td>$300,000</td>
			<td>2yo</td>
			<td>7f Dirt</td>
			<td>PINEHURST</td>
			<td>M. Smith</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 6, 2021</td>
			<td>WinStar Mint Million</td>
			<td>Kentucky Downs</td>
			<td>III</td>
			<td>$1,000,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>PIXELATE</td>
			<td>J. Rosario</td>
			<td>M. Stidham</td>
		</tr>
			<tr>			<td class="num">Sep 6, 2021</td>
			<td>Bernard Baruch Handicap</td>
			<td>Saratoga Race Course</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>8.5f Turf</td>
			<td>TELL YOUR DADDY</td>
			<td>J. Velazquez</td>
			<td>T. Morley</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 6, 2021</td>
			<td>Hopeful Stakes</td>
			<td>Saratoga Race Course</td>
			<td>I</td>
			<td>$300,000</td>
			<td>2yo</td>
			<td>7f Dirt</td>
			<td>GUNITE</td>
			<td>R. Santana, Jr.</td>
			<td>S. Asmussen</td>
		</tr>
			<tr>			<td class="num">Sep 11, 2021</td>
			<td>Canadian Derby</td>
			<td>Century Mile Racetrack</td>
			<td>III</td>
			<td>$125,000</td>
			<td>3yo</td>
			<td>10f Dirt</td>
			<td>UNCHARACTERISTIC</td>
			<td>A. Marti</td>
			<td>R. VanOverschot</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 11, 2021</td>
			<td>Shion Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$721,000</td>
			<td>3yo f</td>
			<td>10f Turf</td>
			<td>FINE ROUGE (JPN)</td>
			<td>Y. Fukunaga</td>
			<td>T. Iwato</td>
		</tr>
			<tr>			<td class="num">Sep 11, 2021</td>
			<td>Calumet Turf Cup</td>
			<td>Kentucky Downs</td>
			<td>II</td>
			<td>$1,000,000</td>
			<td>3&amp;up</td>
			<td>12f Turf</td>
			<td>IMPERADOR</td>
			<td>J. Talamo</td>
			<td>P. Lobo</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 11, 2021</td>
			<td>Franklin-Simpson Stakes</td>
			<td>Kentucky Downs</td>
			<td>II</td>
			<td>$600,000</td>
			<td>3yo</td>
			<td>6.5f Turf</td>
			<td>THE LIR JET</td>
			<td>T. Gaffalione</td>
			<td>B. Walsh</td>
		</tr>
			<tr>			<td class="num">Sep 11, 2021</td>
			<td>Kentucky Downs Ladies Sprint</td>
			<td>Kentucky Downs</td>
			<td>III</td>
			<td>$600,000</td>
			<td>3&amp;up f/m</td>
			<td>6.5f Turf</td>
			<td>IN GOOD SPIRITS</td>
			<td>J. Velazquez</td>
			<td>A. Stall, Jr.</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 11, 2021</td>
			<td>Kentucky Downs Ladies Turf</td>
			<td>Kentucky Downs</td>
			<td>III</td>
			<td>$750,000</td>
			<td>3&amp;up f/m</td>
			<td>8f Turf</td>
			<td>PRINCESS GRACE</td>
			<td>F. Geroux</td>
			<td>M. Stidham</td>
		</tr>
			<tr>			<td class="num">Sep 11, 2021</td>
			<td>Kentucky Downs Turf Sprint</td>
			<td>Kentucky Downs</td>
			<td>III</td>
			<td>$1,000,000</td>
			<td>3&amp;up</td>
			<td>6f Turf</td>
			<td>GEAR JOCKEY</td>
			<td>J. Lezcano</td>
			<td>G. Arnold, II</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 11, 2021</td>
			<td>Seagram Cup Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up</td>
			<td>8.5f AW</td>
			<td>TAP IT TO WIN</td>
			<td>R. Hernandez</td>
			<td>M. Casse</td>
		</tr>
			<tr>			<td class="num">Sep 12, 2021</td>
			<td>Centaur Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,221,000</td>
			<td>3&amp;up</td>
			<td>6f Turf</td>
			<td>RESISTENCIA (JPN)</td>
			<td>C. Lemaire</td>
			<td>T. Matsushita</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 12, 2021</td>
			<td>Keisei Hai Autumn Handicap</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$808,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>CATEDRAL (JPN)</td>
			<td>K. Tosaki</td>
			<td>M. Ikezoe</td>
		</tr>
			<tr>			<td class="num">Sep 18, 2021</td>
			<td>Iroquois</td>
			<td>Churchill Downs</td>
			<td>III</td>
			<td>$300,000</td>
			<td>2yo</td>
			<td>8.5f Dirt</td>
			<td>MAJOR GENERAL</td>
			<td>J. Castellano</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 18, 2021</td>
			<td>Locust Grove</td>
			<td>Churchill Downs</td>
			<td>III</td>
			<td>$400,000</td>
			<td>3&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>SHEDARESTHEDEVIL</td>
			<td>F. Geroux</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">Sep 18, 2021</td>
			<td>Pocahontas</td>
			<td>Churchill Downs</td>
			<td>III</td>
			<td>$300,000</td>
			<td>2yo f</td>
			<td>8.5f Dirt</td>
			<td>HIDDEN CONNECTION</td>
			<td>R. Gutierrez</td>
			<td>W. Calhoun</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 18, 2021</td>
			<td>Frank J. De Francis Memorial Dash</td>
			<td>Laurel Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>6f Dirt</td>
			<td>JALEN JOURNEY</td>
			<td>F. Lynch</td>
			<td>S. Asmussen</td>
		</tr>
			<tr>			<td class="num">Sep 18, 2021</td>
			<td>Canadian Stakes</td>
			<td>Woodbine</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3&amp;up f/m</td>
			<td>9f Turf</td>
			<td>LA DRAGONTEA</td>
			<td>J. Rosario</td>
			<td>C. Clement</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 18, 2021</td>
			<td>Pattison Canadian International</td>
			<td>Woodbine</td>
			<td>I</td>
			<td>$600,000</td>
			<td>3&amp;up</td>
			<td>12f Turf</td>
			<td>WALTON SPECIAL</td>
			<td>L. Dettori</td>
			<td>C. Appleby</td>
		</tr>
			<tr>			<td class="num">Sep 18, 2021</td>
			<td>Ricoh Woodbine Mile</td>
			<td>Woodbine</td>
			<td>I</td>
			<td>$1,000,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>TOWN CRUISE</td>
			<td>D. Fukumoto</td>
			<td>B. Greer</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 19, 2021</td>
			<td>Rose Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,072,000</td>
			<td>3yo f</td>
			<td>10f Turf</td>
			<td>ANDVARANAUT (JPN)</td>
			<td>Y. Fukunaga</td>
			<td>M. Ikezoe</td>
		</tr>
			<tr>			<td class="num">Sep 19, 2021</td>
			<td>Natalma Stakes</td>
			<td>Woodbine</td>
			<td>I</td>
			<td>$400,000</td>
			<td>2yo f</td>
			<td>8f Turf</td>
			<td>WILD BEAUTY</td>
			<td>L. Dettori</td>
			<td>C. Appleby</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 19, 2021</td>
			<td>Summer Stakes</td>
			<td>Woodbine</td>
			<td>I</td>
			<td>$400,000</td>
			<td>2yo</td>
			<td>8f Turf</td>
			<td>ALBAHR</td>
			<td>L. Dettori</td>
			<td>C. Appleby</td>
		</tr>
			<tr>			<td class="num">Sep 20, 2021</td>
			<td>St. Lite Kinen</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,120,000</td>
			<td>3yo</td>
			<td>11f Turf</td>
			<td>ASAMANO ITAZURA (JPN)</td>
			<td>H. Tanabe</td>
			<td>T. Tezuka</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 25, 2021</td>
			<td>Athenia</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3&amp;up f/m</td>
			<td>9f Turf</td>
			<td>POCKET SQUARE</td>
			<td>I. Ortiz, Jr.</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Sep 25, 2021</td>
			<td>Kelso Handicap</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$300,000</td>
			<td>3&amp;up</td>
			<td>8f Dirt</td>
			<td>LIFE IS GOOD</td>
			<td>I. Ortiz, Jr.</td>
			<td>T. Pletcher</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 25, 2021</td>
			<td>Dogwood</td>
			<td>Churchill Downs</td>
			<td>III</td>
			<td>$275,000</td>
			<td>3yo f</td>
			<td>7f Dirt</td>
			<td>CARRIBEAN CAPER</td>
			<td>C. Hernandez</td>
			<td>A. Stall, Jr.</td>
		</tr>
			<tr>			<td class="num">Sep 25, 2021</td>
			<td>Cotillion Stakes</td>
			<td>Parx Racing</td>
			<td>I</td>
			<td>$1,000,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>CLAIRIERE</td>
			<td>R. Santana, Jr.</td>
			<td>S. Asmussen</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 25, 2021</td>
			<td>Greenwood Cup</td>
			<td>Parx Racing</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>12f Dirt</td>
			<td>MAGIC MICHAEL</td>
			<td>F. Pennington</td>
			<td>J. Ness</td>
		</tr>
			<tr>			<td class="num">Sep 25, 2021</td>
			<td>Turf Monster</td>
			<td>Parx Racing</td>
			<td>III</td>
			<td>$300,000</td>
			<td>3&amp;up</td>
			<td>5f Turf</td>
			<td>HOLLYWOOD TALENT</td>
			<td>R. Santana, Jr.</td>
			<td>J. Vazquez</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 25, 2021</td>
			<td>Bold Venture Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up</td>
			<td>6.5f AW</td>
			<td>PINK LLOYD</td>
			<td>R. Hernandez</td>
			<td>R. Tiller</td>
		</tr>
			<tr>			<td class="num">Sep 26, 2021</td>
			<td>Gallant Bloom Handicap</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3&amp;up f/m</td>
			<td>6.5f Dirt</td>
			<td>BELLA SOFIA</td>
			<td>L. Saez</td>
			<td>R. Rodriguez</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 26, 2021</td>
			<td>All Comers Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,383,000</td>
			<td>3&amp;up</td>
			<td>11f Turf</td>
			<td>WIN MARILYN (JPN)</td>
			<td>T. Yokoyama</td>
			<td>T. Tezuka</td>
		</tr>
			<tr>			<td class="num">Sep 26, 2021</td>
			<td>Kobe Shimbun Hai</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,120,000</td>
			<td>3yo</td>
			<td>11f Turf</td>
			<td>STELLA VELOCE (JPN)</td>
			<td>H. Yoshida</td>
			<td>N. Sugai</td>
		</tr>
			<tr class="odd">			<td class="num">Sep 26, 2021</td>
			<td>Oklahoma Derby</td>
			<td>Remington Park</td>
			<td>III</td>
			<td>$400,000</td>
			<td>3yo</td>
			<td>9f Dirt</td>
			<td>WARRANT</td>
			<td>J. Rosario</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">Sep 26, 2021</td>
			<td>Remington Park Oaks</td>
			<td>Remington Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>8.5f Dirt</td>
			<td>LADY MYSTIFY</td>
			<td>F. Prat</td>
			<td>P. Eurton</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 1, 2021</td>
			<td>American Pharoah Stakes</td>
			<td>Santa Anita</td>
			<td>I</td>
			<td>$300,000</td>
			<td>2yo</td>
			<td>8.5f Dirt</td>
			<td>CORNICHE</td>
			<td>M. Smith</td>
			<td>B. Baffert</td>
		</tr>
			<tr>			<td class="num">Oct 1, 2021</td>
			<td>Eddie D Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>6.5f Turf</td>
			<td>LIEUTENANT DAN</td>
			<td>G. Franco</td>
			<td>S. Miyadi</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 2, 2021</td>
			<td>Belmont Turf Sprint Invitational</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>6f Turf</td>
			<td>ARREST ME RED</td>
			<td>I. Ortiz, Jr.</td>
			<td>W. Ward</td>
		</tr>
			<tr>			<td class="num">Oct 2, 2021</td>
			<td>Champagne Stakes</td>
			<td>Belmont Park</td>
			<td>I</td>
			<td>$500,000</td>
			<td>2yo</td>
			<td>8f Dirt</td>
			<td>JACK CHRISTOPHER</td>
			<td>J. Ortiz</td>
			<td>C. Brown</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 2, 2021</td>
			<td>Miss Grillo Stakes</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>2yo f</td>
			<td>8.5f Turf</td>
			<td>SAIL BY</td>
			<td>J. Alvarado</td>
			<td>L. Gyarmati</td>
		</tr>
			<tr>			<td class="num">Oct 2, 2021</td>
			<td>Woodward</td>
			<td>Belmont Park</td>
			<td>I</td>
			<td>$500,000</td>
			<td>3&amp;up</td>
			<td>9f Dirt</td>
			<td>ART COLLECTOR</td>
			<td>L. Saez</td>
			<td>W. Mott</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 2, 2021</td>
			<td>Ack Ack</td>
			<td>Churchill Downs</td>
			<td>III</td>
			<td>$300,000</td>
			<td>3&amp;up</td>
			<td>8f Dirt</td>
			<td>PLAINSMAN</td>
			<td>J. Rosario</td>
			<td>B. Cox</td>
		</tr>
			<tr>			<td class="num">Oct 2, 2021</td>
			<td>Lukas Classic</td>
			<td>Churchill Downs</td>
			<td>III</td>
			<td>$400,000</td>
			<td>3&amp;up</td>
			<td>9f Dirt</td>
			<td>KNICKS GO</td>
			<td>J. Rosario</td>
			<td>B. Cox</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 2, 2021</td>
			<td>Sirius Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$737,000</td>
			<td>3&amp;up</td>
			<td>9.5f Dirt</td>
			<td>SUNRISE HOPE (JPN)</td>
			<td>H. Miyuki</td>
			<td>T. Hatsuki</td>
		</tr>
			<tr>			<td class="num">Oct 2, 2021</td>
			<td>Awesome Again Stakes</td>
			<td>Santa Anita</td>
			<td>I</td>
			<td>$300,000</td>
			<td>3&amp;up</td>
			<td>9f Dirt</td>
			<td>MEDINA SPIRIT</td>
			<td>J. Velazquez</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 2, 2021</td>
			<td>City of Hope Mile</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>MO FORZA</td>
			<td>F. Prat</td>
			<td>P. Miller</td>
		</tr>
			<tr>			<td class="num">Oct 2, 2021</td>
			<td>John Henry Turf Championship</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td>UNITED</td>
			<td>F. Prat</td>
			<td>R. Mandella</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 2, 2021</td>
			<td>Rodeo Drive Stakes</td>
			<td>Santa Anita</td>
			<td>I</td>
			<td>$300,000</td>
			<td>3&amp;up f/m</td>
			<td>10f Turf</td>
			<td>GOING TO VEGAS</td>
			<td>U. Rispoli</td>
			<td>R. Baltas</td>
		</tr>
			<tr>			<td class="num">Oct 2, 2021</td>
			<td>Santa Anita Sprint Championship</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>6f Dirt</td>
			<td>DR. SHIVEL</td>
			<td>F. Prat</td>
			<td>M. Glatt</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 3, 2021</td>
			<td>Fasig-Tipton Waya Stakes</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$300,000</td>
			<td>3&amp;up f/m</td>
			<td>11f Turf</td>
			<td>MY SISTER NAT</td>
			<td>J. Ortiz</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Oct 3, 2021</td>
			<td>Frizette Stakes</td>
			<td>Belmont Park</td>
			<td>I</td>
			<td>$400,000</td>
			<td>2yo f</td>
			<td>8f Dirt</td>
			<td>ECHO ZULU</td>
			<td>R. Santana, Jr.</td>
			<td>S. Asmussen</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 3, 2021</td>
			<td>Pilgrim Stakes</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>2yo</td>
			<td>8.5f Turf</td>
			<td>ANNAPOLIS</td>
			<td>I. Ortiz, Jr.</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">Oct 3, 2021</td>
			<td>Sprinters Stakes</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,362,000</td>
			<td>3&amp;up</td>
			<td>6f Turf</td>
			<td>PIXIE KNIGHT (JPN)</td>
			<td>Y. Fukunaga</td>
			<td>H. Otonashi</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 3, 2021</td>
			<td>Chillingworth Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up f/m</td>
			<td>6.5f Dirt</td>
			<td>CE CE</td>
			<td>V. Espinoza</td>
			<td>M. McCarthy</td>
		</tr>
			<tr>			<td class="num">Oct 3, 2021</td>
			<td>Zenyatta Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td>PRIVATE MISSION</td>
			<td>F. Prat</td>
			<td>B. Baffert</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 3, 2021</td>
			<td>Ontario Fashion Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up f/m</td>
			<td>6f AW</td>
			<td>AMALFI COAST</td>
			<td>J. Stein</td>
			<td>K. Attard</td>
		</tr>
			<tr>			<td class="num">Oct 8, 2021</td>
			<td>Phoenix Stakes</td>
			<td>Keeneland</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3&amp;up</td>
			<td>6f Dirt</td>
			<td>SPECIAL RESERVE</td>
			<td>J. Rosario</td>
			<td>M. Maker</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 9, 2021</td>
			<td>Joe Hirsch Turf Classic</td>
			<td>Belmont Park</td>
			<td>I</td>
			<td>$500,000</td>
			<td>3&amp;up</td>
			<td>12f Turf</td>
			<td>ROCKEMPEROR</td>
			<td>J. Castellano</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Oct 9, 2021</td>
			<td>Matron Stakes</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$150,000</td>
			<td>2yo f</td>
			<td>6f Turf</td>
			<td>BUBBLE ROCK</td>
			<td>I. Ortiz, Jr.</td>
			<td>B. Cox</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 9, 2021</td>
			<td>Vosburgh Stakes</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3&amp;up</td>
			<td>6f Dirt</td>
			<td>FOLLOWING SEA</td>
			<td>J. Rosario</td>
			<td>T. Pletcher</td>
		</tr>
			<tr>			<td class="num">Oct 9, 2021</td>
			<td>Saudi Arabia Royal Cup</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$678,000</td>
			<td>2yo</td>
			<td>8f Turf</td>
			<td>COMMAND LINE (JPN)</td>
			<td>C. Lemaire</td>
			<td>S. Kunieda</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 9, 2021</td>
			<td>First Lady Stakes</td>
			<td>Keeneland</td>
			<td>I</td>
			<td>$400,000</td>
			<td>3&amp;up f/m</td>
			<td>8f Turf</td>
			<td>BLOWOUT</td>
			<td>F. Prat</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Oct 9, 2021</td>
			<td>Keeneland Turf Mile</td>
			<td>Keeneland</td>
			<td>I</td>
			<td>$750,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>IN LOVE</td>
			<td>A. Achard</td>
			<td>P. Lobo</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 9, 2021</td>
			<td>Woodford</td>
			<td>Keeneland</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>5.5f Turf</td>
			<td>GOLDEN PAL</td>
			<td>J. Velazquez</td>
			<td>W. Ward</td>
		</tr>
			<tr>			<td class="num">Oct 10, 2021</td>
			<td>Beldame Stakes</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3&amp;up f/m</td>
			<td>9f Dirt</td>
			<td>ROYAL FLAG</td>
			<td>J. Rosario</td>
			<td>C. Brown</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 10, 2021</td>
			<td>Futurity Stakes</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$150,000</td>
			<td>2yo</td>
			<td>6f Turf</td>
			<td>SLIPSTREAM</td>
			<td>J. Rosario</td>
			<td>C. Clement</td>
		</tr>
			<tr>			<td class="num">Oct 10, 2021</td>
			<td>Knickerbocker</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up</td>
			<td>9f Turf</td>
			<td>SACRED LIFE</td>
			<td>J. Ortiz</td>
			<td>C. Brown</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 10, 2021</td>
			<td>Kyoto Daishoten</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,383,000</td>
			<td>3&amp;up</td>
			<td>12f Turf</td>
			<td>MAKAHIKI (JPN)</td>
			<td>K. Fujioka</td>
			<td>Y. Tomomichi</td>
		</tr>
			<tr>			<td class="num">Oct 10, 2021</td>
			<td>Mainichi Okan</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$1,383,000</td>
			<td>3&amp;up</td>
			<td>9f Turf</td>
			<td>SCHNELL MEISTER (GER)</td>
			<td>C. Lemaire</td>
			<td>T. Tezuka</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 10, 2021</td>
			<td>Bourbon Stakes</td>
			<td>Keeneland</td>
			<td>II</td>
			<td>$200,000</td>
			<td>2yo</td>
			<td>8.5f Turf</td>
			<td>TIZ THE BOMB</td>
			<td>B. Hernandez, Jr.</td>
			<td>K. McPeek</td>
		</tr>
			<tr>			<td class="num">Oct 10, 2021</td>
			<td>Juddmonte Spinster Stakes</td>
			<td>Keeneland</td>
			<td>I</td>
			<td>$500,000</td>
			<td>3&amp;up f/m</td>
			<td>9f Dirt</td>
			<td>LETRUSKA</td>
			<td>I. Ortiz, Jr.</td>
			<td>F. Gutierrez</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 10, 2021</td>
			<td>Durham Cup Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up</td>
			<td>8.5f AW</td>
			<td>SPECIAL FORCES</td>
			<td>J. Stein</td>
			<td>K. Attard</td>
		</tr>
			<tr>			<td class="num">Oct 10, 2021</td>
			<td>Ontario Matron Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up f/m</td>
			<td>8.5f AW</td>
			<td>ART OF ALMOST</td>
			<td>E. Wilson</td>
			<td>M. Casse</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 13, 2021</td>
			<td>Jessamine Stakes</td>
			<td>Keeneland</td>
			<td>II</td>
			<td>$200,000</td>
			<td>2yo f</td>
			<td>8.5f Turf</td>
			<td>CALIFORNIA ANGEL</td>
			<td>R. Bejarano</td>
			<td>G. Leonard, III</td>
		</tr>
			<tr>			<td class="num">Oct 15, 2021</td>
			<td>Buffalo Trace Franklin County Stakes</td>
			<td>Keeneland</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up f/m</td>
			<td>5.5f Turf</td>
			<td>CHANGE OF CONTROL</td>
			<td>C. Hernandez</td>
			<td>M. Lovell</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 16, 2021</td>
			<td>Sands Point Stakes</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>9f Turf</td>
			<td>FLUFFY SOCKS</td>
			<td>J. Rosario</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Oct 16, 2021</td>
			<td>Fuchu Himba Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,135,000</td>
			<td>3&amp;up f/m</td>
			<td>9f Turf</td>
			<td>SHADOW DIVA (JPN)</td>
			<td>Y. Fukunaga</td>
			<td>M. Saito</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 16, 2021</td>
			<td>Queen Elizabeth II Challenge Cup</td>
			<td>Keeneland</td>
			<td>I</td>
			<td>$500,000</td>
			<td>3yo f</td>
			<td>9f Turf</td>
			<td>SHANTISARA</td>
			<td>F. Prat</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Oct 17, 2021</td>
			<td>Shuka Sho</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,058,000</td>
			<td>3yo f</td>
			<td>10f Turf</td>
			<td>AKAITORINO MUSUME (JPN)</td>
			<td>K. Tosaki</td>
			<td>S. Kunieda</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 17, 2021</td>
			<td>E.P. Taylor Stakes</td>
			<td>Woodbine</td>
			<td>I</td>
			<td>$600,000</td>
			<td>3&amp;up f/m</td>
			<td>10f Turf</td>
			<td>MUTAMAKINA</td>
			<td>D. Davis</td>
			<td>C. Clement</td>
		</tr>
			<tr>			<td class="num">Oct 22, 2021</td>
			<td>Sycamore Stakes</td>
			<td>Keeneland</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up</td>
			<td>12f Turf</td>
			<td>SPOOKY CHANNEL</td>
			<td>J. Leparoux</td>
			<td>J. Barkley</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 23, 2021</td>
			<td>Hill Prince</td>
			<td>Belmont Park</td>
			<td>II</td>
			<td>$400,000</td>
			<td>3yo</td>
			<td>9f Turf</td>
			<td>PUBLIC SECTOR</td>
			<td>I. Ortiz, Jr.</td>
			<td>C. Brown</td>
		</tr>
			<tr>			<td class="num">Oct 23, 2021</td>
			<td>Noble Damsel</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3&amp;up f/m</td>
			<td>8f Turf</td>
			<td>SHIFTY SHE</td>
			<td>E. Gonzalez</td>
			<td>S. Joseph, Jr.</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 23, 2021</td>
			<td>Fuji Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,221,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td>SONGLINE (JPN)</td>
			<td>K. Ikezoe</td>
			<td>T. Hayashi</td>
		</tr>
			<tr>			<td class="num">Oct 23, 2021</td>
			<td>Raven Run Stakes</td>
			<td>Keeneland</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3yo f</td>
			<td>7f Dirt</td>
			<td>CARAMEL SWIRL</td>
			<td>J. Alvarado</td>
			<td>W. Mott</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 23, 2021</td>
			<td>Hendrie Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up f/m</td>
			<td>6.5f AW</td>
			<td>OUR SECRET AGENT</td>
			<td>K. Kimura</td>
			<td>M. Casse</td>
		</tr>
			<tr>			<td class="num">Oct 24, 2021</td>
			<td>Kikuka Sho</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,469,000</td>
			<td>3yo</td>
			<td>15f Turf</td>
			<td>TITLEHOLDER (JPN)</td>
			<td>T. Yokoyama</td>
			<td>T. Kurita</td>
		</tr>
			<tr class="odd">			<td class="num">Oct 24, 2021</td>
			<td>Rood &amp; Riddle Dowager Stakes</td>
			<td>Keeneland</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up f/m</td>
			<td>12f Turf</td>
			<td>SUMMER IN SARATOGA</td>
			<td>C. Lanerie</td>
			<td>J. Sharp</td>
		</tr>
			<tr>			<td class="num">Oct 29, 2021</td>
			<td>Valley View Stakes</td>
			<td>Keeneland</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3yo f</td>
			<td>8.5f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Oct 30, 2021</td>
			<td>Artemis Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$602,000</td>
			<td>2yo f</td>
			<td>8f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Oct 30, 2021</td>
			<td>Swan Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,221,000</td>
			<td>3&amp;up</td>
			<td>7f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Oct 30, 2021</td>
			<td>Hagyard Fayette Stakes</td>
			<td>Keeneland</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>9f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Oct 30, 2021</td>
			<td>Autumn Miss Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3yo f</td>
			<td>8f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Oct 31, 2021</td>
			<td>Bold Ruler Handicap</td>
			<td>Belmont Park</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>7f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Oct 31, 2021</td>
			<td>Tenno Sho Autumn</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$3,096,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Oct 31, 2021</td>
			<td>Twilight Derby</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3yo</td>
			<td>9f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Oct 31, 2021</td>
			<td>Ontario Derby</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3yo</td>
			<td>9f AW</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 6, 2021</td>
			<td>Turnback the Alarm Handicap</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up f/m</td>
			<td>9f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 6, 2021</td>
			<td>Goldikova Stakes</td>
			<td>Del Mar</td>
			<td>II</td>
			<td>$300,000</td>
			<td>3&amp;up f/m</td>
			<td>8f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 6, 2021</td>
			<td>Thoroughbred Aftercare Alliance Stakes</td>
			<td>Del Mar</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3&amp;up</td>
			<td>13f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 6, 2021</td>
			<td>Fantasy Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$602,000</td>
			<td>2yo f</td>
			<td>7f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 6, 2021</td>
			<td>Keio Hai Nisai Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$780,000</td>
			<td>2yo</td>
			<td>7f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 7, 2021</td>
			<td>Nashua Stakes</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$150,000</td>
			<td>2yo</td>
			<td>8f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 7, 2021</td>
			<td>Copa Republica Argentina</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,173,000</td>
			<td>3&amp;up</td>
			<td>12.5f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 7, 2021</td>
			<td>Miyako Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$780,000</td>
			<td>3&amp;up</td>
			<td>9f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 13, 2021</td>
			<td>Daily Hai Nisai Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$780,000</td>
			<td>2yo</td>
			<td>8f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 13, 2021</td>
			<td>Musashino Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$780,000</td>
			<td>3&amp;up</td>
			<td>8f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 13, 2021</td>
			<td>Bessarabian Stakes</td>
			<td>Woodbine</td>
			<td>II</td>
			<td>$175,000</td>
			<td>3&amp;up f/m</td>
			<td>7f AW</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 13, 2021</td>
			<td>Maple Leaf Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up f/m</td>
			<td>10f AW</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 14, 2021</td>
			<td>Bob Hope Stakes</td>
			<td>Del Mar</td>
			<td>III</td>
			<td>$100,000</td>
			<td>2yo</td>
			<td>7f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 14, 2021</td>
			<td>Fukushima Kinen</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 14, 2021</td>
			<td>Queen Elizabeth II Cup</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,160,000</td>
			<td>3&amp;up f/m</td>
			<td>11f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 14, 2021</td>
			<td>Autumn Stakes</td>
			<td>Woodbine</td>
			<td>II</td>
			<td>$175,000</td>
			<td>3&amp;up</td>
			<td>8.5f AW</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 20, 2021</td>
			<td>Red Smith</td>
			<td>Aqueduct</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>11f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 20, 2021</td>
			<td>Chilukki Stakes</td>
			<td>Churchill Downs</td>
			<td>III</td>
			<td>$300,000</td>
			<td>3&amp;up f/m</td>
			<td>8f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 20, 2021</td>
			<td>Native Diver Stakes</td>
			<td>Del Mar</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up</td>
			<td>9f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 20, 2021</td>
			<td>Tokyo Sports Hai Nisai Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$780,000</td>
			<td>2yo</td>
			<td>9f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 21, 2021</td>
			<td>Mile Championship</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,362,000</td>
			<td>3&amp;up</td>
			<td>8f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 25, 2021</td>
			<td>Falls City</td>
			<td>Churchill Downs</td>
			<td>II</td>
			<td>$500,000</td>
			<td>3&amp;up f/m</td>
			<td>9f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 25, 2021</td>
			<td>Red Carpet Stakes</td>
			<td>Del Mar</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up f/m</td>
			<td>11f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 26, 2021</td>
			<td>Comely Stakes</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3yo f</td>
			<td>9f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 26, 2021</td>
			<td>Clark Presented by Norton HealthCare</td>
			<td>Churchill Downs</td>
			<td>I</td>
			<td>$750,000</td>
			<td>3&amp;up</td>
			<td>9f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 26, 2021</td>
			<td>Hollywood Turf Cup</td>
			<td>Del Mar</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3&amp;up</td>
			<td>12f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 27, 2021</td>
			<td>Long Island</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$400,000</td>
			<td>3&amp;up f/m</td>
			<td>12f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 27, 2021</td>
			<td>Golden Rod Stakes</td>
			<td>Churchill Downs</td>
			<td>II</td>
			<td>$400,000</td>
			<td>2yo f</td>
			<td>8.5f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 27, 2021</td>
			<td>Kentucky Jockey Club Stakes</td>
			<td>Churchill Downs</td>
			<td>II</td>
			<td>$400,000</td>
			<td>2yo</td>
			<td>8.5f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 27, 2021</td>
			<td>Hollywood Derby</td>
			<td>Del Mar</td>
			<td>I</td>
			<td>$400,000</td>
			<td>3yo</td>
			<td>9f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 27, 2021</td>
			<td>Jimmy Durante Stakes</td>
			<td>Del Mar</td>
			<td>III</td>
			<td>$100,000</td>
			<td>2yo f</td>
			<td>8f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 27, 2021</td>
			<td>Seabiscuit Handicap</td>
			<td>Del Mar</td>
			<td>II</td>
			<td>$250,000</td>
			<td>3&amp;up</td>
			<td>8.5f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 27, 2021</td>
			<td>Radio Nikkei Hai Kyoto Nisai Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$678,000</td>
			<td>2yo</td>
			<td>10f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 27, 2021</td>
			<td>Kennedy Road Stakes</td>
			<td>Woodbine</td>
			<td>II</td>
			<td>$175,000</td>
			<td>3&amp;up</td>
			<td>6f AW</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 28, 2021</td>
			<td>Fall Highweight Handicap</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>6f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 28, 2021</td>
			<td>Cecil B. DeMille Stakes</td>
			<td>Del Mar</td>
			<td>III</td>
			<td>$100,000</td>
			<td>2yo</td>
			<td>8f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 28, 2021</td>
			<td>Matriarch Stakes</td>
			<td>Del Mar</td>
			<td>I</td>
			<td>$400,000</td>
			<td>3&amp;up f/m</td>
			<td>8f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 28, 2021</td>
			<td>Japan Cup</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$6,172,000</td>
			<td>3&amp;up</td>
			<td>12f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 28, 2021</td>
			<td>Keihan Hai</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$808,000</td>
			<td>3&amp;up</td>
			<td>6f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Nov 28, 2021</td>
			<td>Grey Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>2yo</td>
			<td>8.5f AW</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Nov 28, 2021</td>
			<td>Mazarine Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>2yo f</td>
			<td>8.5f AW</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 4, 2021</td>
			<td>Cigar Mile Handicap</td>
			<td>Aqueduct</td>
			<td>I</td>
			<td>$750,000</td>
			<td>3&amp;up</td>
			<td>8f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 4, 2021</td>
			<td>Demoiselle Stakes</td>
			<td>Aqueduct</td>
			<td>II</td>
			<td>$250,000</td>
			<td>2yo f</td>
			<td>9f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 4, 2021</td>
			<td>Go for Wand Handicap</td>
			<td>Aqueduct</td>
			<td>III</td>
			<td>$250,000</td>
			<td>3&amp;up f/m</td>
			<td>8f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 4, 2021</td>
			<td>Remsen Stakes</td>
			<td>Aqueduct</td>
			<td>II</td>
			<td>$250,000</td>
			<td>2yo</td>
			<td>9f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 4, 2021</td>
			<td>Challenge Cup</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 4, 2021</td>
			<td>Stayers Stakes</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,283,000</td>
			<td>3&amp;up</td>
			<td>18f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 4, 2021</td>
			<td>Starlet</td>
			<td>Los Alamitos</td>
			<td>I</td>
			<td>$300,000</td>
			<td>2yo f</td>
			<td>8.5f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 5, 2021</td>
			<td>Champions Cup</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$2,058,000</td>
			<td>3&amp;up</td>
			<td>9f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 5, 2021</td>
			<td>Bayakoa Stakes</td>
			<td>Los Alamitos</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up f/m</td>
			<td>8.5f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 5, 2021</td>
			<td>Valedictory Stakes</td>
			<td>Woodbine</td>
			<td>III</td>
			<td>$150,000</td>
			<td>3&amp;up</td>
			<td>12f AW</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 11, 2021</td>
			<td>Mr. Prospector Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up</td>
			<td>7f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 11, 2021</td>
			<td>Chunichi Shimbun Hai</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$838,000</td>
			<td>3&amp;up</td>
			<td>10f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 11, 2021</td>
			<td>Los Alamitos Futurity</td>
			<td>Los Alamitos</td>
			<td>II</td>
			<td>$200,000</td>
			<td>2yo</td>
			<td>8.5f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 12, 2021</td>
			<td>Capella Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$737,000</td>
			<td>3&amp;up</td>
			<td>6f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 12, 2021</td>
			<td>Hanshin Juvenile Fillies</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$1,336,000</td>
			<td>2yo f</td>
			<td>8f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 18, 2021</td>
			<td>Fort Lauderdale Stakes</td>
			<td>Gulfstream Park</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>9f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 18, 2021</td>
			<td>Sugar Swirl Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up f/m</td>
			<td>6f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 18, 2021</td>
			<td>Suwannee River Stakes</td>
			<td>Gulfstream Park</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up f/m</td>
			<td>8f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 18, 2021</td>
			<td>Turquoise Stakes</td>
			<td>Japan Racing Association</td>
			<td>III</td>
			<td>$737,000</td>
			<td>3&amp;up f/m</td>
			<td>8f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 19, 2021</td>
			<td>Asahi Hai Futurity Stakes</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$1,450,000</td>
			<td>2yo</td>
			<td>8f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 25, 2021</td>
			<td>Hanshin Cup</td>
			<td>Japan Racing Association</td>
			<td>II</td>
			<td>$1,383,000</td>
			<td>3&amp;up</td>
			<td>7f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 26, 2021</td>
			<td>Arima Kinen</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$6,172,000</td>
			<td>3&amp;up</td>
			<td>12.5f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 26, 2021</td>
			<td>American Oaks</td>
			<td>Santa Anita</td>
			<td>I</td>
			<td>$300,000</td>
			<td>3yo f</td>
			<td>10f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 26, 2021</td>
			<td>La Brea Stakes</td>
			<td>Santa Anita</td>
			<td>I</td>
			<td>$300,000</td>
			<td>3yo f</td>
			<td>7f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 26, 2021</td>
			<td>Mathis Brothers Mile</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3yo</td>
			<td>8f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 26, 2021</td>
			<td>Runhappy Malibu Stakes</td>
			<td>Santa Anita</td>
			<td>I</td>
			<td>$300,000</td>
			<td>3yo</td>
			<td>7f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 26, 2021</td>
			<td>San Antonio Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>8.5f Dirt</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 26, 2021</td>
			<td>San Gabriel Stakes</td>
			<td>Santa Anita</td>
			<td>II</td>
			<td>$200,000</td>
			<td>3&amp;up</td>
			<td>9f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr>			<td class="num">Dec 28, 2021</td>
			<td>Hopeful Stakes</td>
			<td>Japan Racing Association</td>
			<td>I</td>
			<td>$1,450,000</td>
			<td>2yo</td>
			<td>10f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
			<tr class="odd">			<td class="num">Dec 31, 2021</td>
			<td>Robert J. Frankel Stakes</td>
			<td>Santa Anita</td>
			<td>III</td>
			<td>$100,000</td>
			<td>3&amp;up f/m</td>
			<td>9f Turf</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
					</tbody>
		 <tfoot>
                	<tr>
                    		<td class="dateUpdated center" colspan="5">
                        	<em id='updateemp'>Updated November 16, 2021.</em>
                    		</td>
                	</tr>
            	</tfoot>
		</table>
		