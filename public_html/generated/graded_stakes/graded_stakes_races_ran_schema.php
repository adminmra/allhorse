         <div id="no-more-tables">
		<table id="gradedResults" class="table table-condensed table-striped table-bordered ordenableResult" border="0" cellspacing="0" cellpadding="0" width="100%">
			<thead>
				<tr>
					<th width="16%"><strong>Date</strong></th>
					<th><strong>Stakes</strong></th>
					<th><strong>Track</strong></th>
					<!-- <th width="10%"><strong>Grade</strong></th> -->
					<th><strong>Purses</strong></th>
					<!-- <th width="10%"><strong>Age/Sex</strong></th>
					<th width="10%"><strong>DS</strong></th> -->
					<th><strong>Horse</strong></th>					<!-- <th width="12%"><strong>Jockey</strong></th>
					<th width="12%"><strong>Trainer</strong></th> -->
				</tr>
                </thead>
                <tbody>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Bob Hope Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fukushima Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Queen Elizabeth II Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,160,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/autumn-stakes">Autumn Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$175,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f AW</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Daily Hai Nisai Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Musashino Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Bessarabian Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$175,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>7f AW</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/maple-leaf-stakes">Maple Leaf Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>10f AW</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/nashua-stakes">Nashua Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/aqueduct">Aqueduct</a></span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Copa Republica Argentina					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,173,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12.5f Turf</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Miyako Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/turnback-the-alarm-handicap">Turnback the Alarm Handicap</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/aqueduct">Aqueduct</a></span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/goldikova-stakes">Goldikova Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Thoroughbred Aftercare Alliance Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>13f Dirt</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Nov 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fantasy Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$602,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>7f Turf</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Nov 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Keio Hai Nisai Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>7f Turf</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/bold-ruler-handicap">Bold Ruler Handicap</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Tenno Sho Autumn					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$3,096,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/twilight-derby">Twilight Derby</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Ontario Derby					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f AW</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Artemis Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$602,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Swan Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,221,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>7f Turf</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/hagyard-fayette-stakes">Hagyard Fayette Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/autumn-miss-stakes">Autumn Miss Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 29, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Valley View Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse"></td>					<!-- 	<td></td>
					<td></td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kikuka Sho					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,469,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>15f Turf</td> -->
					<td data-title="Horse">Titleholder (jpn)</td>					<!-- 	<td>T. Yokoyama</td>
					<td>T. Kurita</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Rood &amp; Riddle Dowager Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Summer In Saratoga</td>					<!-- 	<td>C. Lanerie</td>
					<td>J. Sharp</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hill Prince					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Public Sector</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Noble Damsel					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Shifty She</td>					<!-- 	<td>E. Gonzalez</td>
					<td>S. Joseph, Jr.</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fuji Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,221,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Songline (jpn)</td>					<!-- 	<td>K. Ikezoe</td>
					<td>T. Hayashi</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/raven-run-stakes">Raven Run Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Caramel Swirl</td>					<!-- 	<td>J. Alvarado</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hendrie Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6.5f AW</td> -->
					<td data-title="Horse">Our Secret Agent</td>					<!-- 	<td>K. Kimura</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 22, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Sycamore Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Spooky Channel</td>					<!-- 	<td>J. Leparoux</td>
					<td>J. Barkley</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Shuka Sho					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,058,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Akaitorino Musume (jpn)</td>					<!-- 	<td>K. Tosaki</td>
					<td>S. Kunieda</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/ep-taylor-stakes">E.P. Taylor Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$600,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Mutamakina</td>					<!-- 	<td>D. Davis</td>
					<td>C. Clement</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 16, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Sands Point Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Fluffy Socks</td>					<!-- 	<td>J. Rosario</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 16, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fuchu Himba Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,135,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Shadow Diva (jpn)</td>					<!-- 	<td>Y. Fukunaga</td>
					<td>M. Saito</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 16, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Queen Elizabeth II Challenge Cup					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Shantisara</td>					<!-- 	<td>F. Prat</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Buffalo Trace Franklin County Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>5.5f Turf</td> -->
					<td data-title="Horse">Change Of Control</td>					<!-- 	<td>C. Hernandez</td>
					<td>M. Lovell</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Jessamine Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">California Angel</td>					<!-- 	<td>R. Bejarano</td>
					<td>G. Leonard, III</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Beldame Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Royal Flag</td>					<!-- 	<td>J. Rosario</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Futurity Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Slipstream</td>					<!-- 	<td>J. Rosario</td>
					<td>C. Clement</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Knickerbocker					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Sacred Life</td>					<!-- 	<td>J. Ortiz</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kyoto Daishoten					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,383,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Makahiki (jpn)</td>					<!-- 	<td>K. Fujioka</td>
					<td>Y. Tomomichi</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Mainichi Okan					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$1,383,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Schnell Meister (ger)</td>					<!-- 	<td>C. Lemaire</td>
					<td>T. Tezuka</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Bourbon Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Tiz The Bomb</td>					<!-- 	<td>B. Hernandez, Jr.</td>
					<td>K. McPeek</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Juddmonte Spinster Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Letruska</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>F. Gutierrez</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Durham Cup Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f AW</td> -->
					<td data-title="Horse">Special Forces</td>					<!-- 	<td>J. Stein</td>
					<td>K. Attard</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Ontario Matron Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f AW</td> -->
					<td data-title="Horse">Art Of Almost</td>					<!-- 	<td>E. Wilson</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 9, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Joe Hirsch Turf Classic					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Rockemperor</td>					<!-- 	<td>J. Castellano</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 9, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Matron Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Bubble Rock</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 9, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Vosburgh Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Following Sea</td>					<!-- 	<td>J. Rosario</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 9, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Saudi Arabia Royal Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$678,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Command Line (jpn)</td>					<!-- 	<td>C. Lemaire</td>
					<td>S. Kunieda</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 9, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						First Lady Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Blowout</td>					<!-- 	<td>F. Prat</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 9, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Keeneland Turf Mile					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$750,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">In Love</td>					<!-- 	<td>A. Achard</td>
					<td>P. Lobo</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 9, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Woodford					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>5.5f Turf</td> -->
					<td data-title="Horse">Golden Pal</td>					<!-- 	<td>J. Velazquez</td>
					<td>W. Ward</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 8, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Phoenix Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Special Reserve</td>					<!-- 	<td>J. Rosario</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fasig-Tipton Waya Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">My Sister Nat</td>					<!-- 	<td>J. Ortiz</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Frizette Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Echo Zulu</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Pilgrim Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Annapolis</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Sprinters Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,362,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Pixie Knight (jpn)</td>					<!-- 	<td>Y. Fukunaga</td>
					<td>H. Otonashi</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Chillingworth Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
					<td data-title="Horse">Ce Ce</td>					<!-- 	<td>V. Espinoza</td>
					<td>M. McCarthy</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Zenyatta Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Private Mission</td>					<!-- 	<td>F. Prat</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/ontario-fashion-stakes">Ontario Fashion Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6f AW</td> -->
					<td data-title="Horse">Amalfi Coast</td>					<!-- 	<td>J. Stein</td>
					<td>K. Attard</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Belmont Turf Sprint Invitational					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Arrest Me Red</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>W. Ward</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Champagne Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Jack Christopher</td>					<!-- 	<td>J. Ortiz</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Miss Grillo Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Sail By</td>					<!-- 	<td>J. Alvarado</td>
					<td>L. Gyarmati</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Woodward					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Art Collector</td>					<!-- 	<td>L. Saez</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Ack Ack					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Plainsman</td>					<!-- 	<td>J. Rosario</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Lukas Classic					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Knicks Go</td>					<!-- 	<td>J. Rosario</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Sirius Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$737,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9.5f Dirt</td> -->
					<td data-title="Horse">Sunrise Hope (jpn)</td>					<!-- 	<td>H. Miyuki</td>
					<td>T. Hatsuki</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Awesome Again Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Medina Spirit</td>					<!-- 	<td>J. Velazquez</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						City of Hope Mile					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Mo Forza</td>					<!-- 	<td>F. Prat</td>
					<td>P. Miller</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						John Henry Turf Championship					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">United</td>					<!-- 	<td>F. Prat</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Rodeo Drive Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Going To Vegas</td>					<!-- 	<td>U. Rispoli</td>
					<td>R. Baltas</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Santa Anita Sprint Championship					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Dr. Shivel</td>					<!-- 	<td>F. Prat</td>
					<td>M. Glatt</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Oct 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						American Pharoah Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Corniche</td>					<!-- 	<td>M. Smith</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Oct 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Eddie D Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6.5f Turf</td> -->
					<td data-title="Horse">Lieutenant Dan</td>					<!-- 	<td>G. Franco</td>
					<td>S. Miyadi</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Gallant Bloom Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
					<td data-title="Horse">Bella Sofia</td>					<!-- 	<td>L. Saez</td>
					<td>R. Rodriguez</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						All Comers Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,383,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Win Marilyn (jpn)</td>					<!-- 	<td>T. Yokoyama</td>
					<td>T. Tezuka</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kobe Shimbun Hai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,120,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Stella Veloce (jpn)</td>					<!-- 	<td>H. Yoshida</td>
					<td>N. Sugai</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Oklahoma Derby					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/remington-park">Remington Park</a></span>
						<!-- Remington Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Warrant</td>					<!-- 	<td>J. Rosario</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Remington Park Oaks					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/remington-park">Remington Park</a></span>
						<!-- Remington Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Lady Mystify</td>					<!-- 	<td>F. Prat</td>
					<td>P. Eurton</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Athenia					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Pocket Square</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kelso Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Life Is Good</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Dogwood					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$275,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Carribean Caper</td>					<!-- 	<td>C. Hernandez</td>
					<td>A. Stall, Jr.</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Cotillion Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/parx-racing">Parx Racing</a></span>
						<!-- Parx Racing -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Clairiere</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Greenwood Cup					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/parx-racing">Parx Racing</a></span>
						<!-- Parx Racing -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Dirt</td> -->
					<td data-title="Horse">Magic Michael</td>					<!-- 	<td>F. Pennington</td>
					<td>J. Ness</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Turf Monster					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/parx-racing">Parx Racing</a></span>
						<!-- Parx Racing -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>5f Turf</td> -->
					<td data-title="Horse">Hollywood Talent</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>J. Vazquez</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Bold Venture Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6.5f AW</td> -->
					<td data-title="Horse">Pink Lloyd</td>					<!-- 	<td>R. Hernandez</td>
					<td>R. Tiller</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						St. Lite Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,120,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Asamano Itazura (jpn)</td>					<!-- 	<td>H. Tanabe</td>
					<td>T. Tezuka</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 19, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Rose Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,072,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Andvaranaut (jpn)</td>					<!-- 	<td>Y. Fukunaga</td>
					<td>M. Ikezoe</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 19, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Natalma Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Wild Beauty</td>					<!-- 	<td>L. Dettori</td>
					<td>C. Appleby</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 19, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Summer Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Albahr</td>					<!-- 	<td>L. Dettori</td>
					<td>C. Appleby</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Iroquois					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Major General</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Locust Grove					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Shedaresthedevil</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Pocahontas					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Hidden Connection</td>					<!-- 	<td>R. Gutierrez</td>
					<td>W. Calhoun</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Frank J. De Francis Memorial Dash					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/laurel-park">Laurel Park</a></span>
						<!-- Laurel Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Jalen Journey</td>					<!-- 	<td>F. Lynch</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Canadian Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">La Dragontea</td>					<!-- 	<td>J. Rosario</td>
					<td>C. Clement</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/pattison-canadian-international">Pattison Canadian International</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$600,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Walton Special</td>					<!-- 	<td>L. Dettori</td>
					<td>C. Appleby</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/ricoh-woodbine-mile">Ricoh Woodbine Mile</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Town Cruise</td>					<!-- 	<td>D. Fukumoto</td>
					<td>B. Greer</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 12, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Centaur Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,221,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Resistencia (jpn)</td>					<!-- 	<td>C. Lemaire</td>
					<td>T. Matsushita</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 12, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Keisei Hai Autumn Handicap					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$808,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Catedral (jpn)</td>					<!-- 	<td>K. Tosaki</td>
					<td>M. Ikezoe</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Canadian Derby					</td>

					<td  data-title="Track" >
						<span  >Century Mile Racetrack</span>
						<!-- Century Mile Racetrack -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$125,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Dirt</td> -->
					<td data-title="Horse">Uncharacteristic</td>					<!-- 	<td>A. Marti</td>
					<td>R. VanOverschot</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Shion Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$721,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Fine Rouge (jpn)</td>					<!-- 	<td>Y. Fukunaga</td>
					<td>T. Iwato</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Calumet Turf Cup					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/kentucky-downs">Kentucky Downs</a></span>
						<!-- Kentucky Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Imperador</td>					<!-- 	<td>J. Talamo</td>
					<td>P. Lobo</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Franklin-Simpson Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/kentucky-downs">Kentucky Downs</a></span>
						<!-- Kentucky Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$600,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>6.5f Turf</td> -->
					<td data-title="Horse">The Lir Jet</td>					<!-- 	<td>T. Gaffalione</td>
					<td>B. Walsh</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kentucky Downs Ladies Sprint					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/kentucky-downs">Kentucky Downs</a></span>
						<!-- Kentucky Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$600,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6.5f Turf</td> -->
					<td data-title="Horse">In Good Spirits</td>					<!-- 	<td>J. Velazquez</td>
					<td>A. Stall, Jr.</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kentucky Downs Ladies Turf					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/kentucky-downs">Kentucky Downs</a></span>
						<!-- Kentucky Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$750,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Princess Grace</td>					<!-- 	<td>F. Geroux</td>
					<td>M. Stidham</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kentucky Downs Turf Sprint					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/kentucky-downs">Kentucky Downs</a></span>
						<!-- Kentucky Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Gear Jockey</td>					<!-- 	<td>J. Lezcano</td>
					<td>G. Arnold, II</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Seagram Cup Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f AW</td> -->
					<td data-title="Horse">Tap It To Win</td>					<!-- 	<td>R. Hernandez</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Runhappy Del Mar Futurity					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Pinehurst</td>					<!-- 	<td>M. Smith</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						WinStar Mint Million					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/kentucky-downs">Kentucky Downs</a></span>
						<!-- Kentucky Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Pixelate</td>					<!-- 	<td>J. Rosario</td>
					<td>M. Stidham</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Bernard Baruch Handicap					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Tell Your Daddy</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Morley</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hopeful Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Gunite</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Del Mar Debutante Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Grace Adler</td>					<!-- 	<td>F. Prat</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kokura Nisai Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$635,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Namura Clair (jpn)</td>					<!-- 	<td>S. Hamanaka</td>
					<td>K. Hasegawa</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Niigata Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Meiner Fanrong (jpn)</td>					<!-- 	<td>M. Demuro</td>
					<td>T. Tezuka</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Spinaway Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Echo Zulu</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Del Mar Derby					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">None Above The Law</td>					<!-- 	<td>J. Bravo</td>
					<td>P. Miller</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						John C. Mabee Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Going To Vegas</td>					<!-- 	<td>F. Prat</td>
					<td>R. Baltas</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Sapporo Nisai Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$635,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Geoglyph (jpn)</td>					<!-- 	<td>C. Lemaire</td>
					<td>T. Iwato</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Flower Bowl					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$600,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">War Like Goddess</td>					<!-- 	<td>J. Leparoux</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Jockey Club Gold Cup					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Dirt</td> -->
					<td data-title="Horse">Max Player</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Prioress Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Cilla</td>					<!-- 	<td>T. Gaffalione</td>
					<td>C. Baker</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Sep 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Saranac Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Public Sector</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Sep 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						With Anticipation Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Coinage</td>					<!-- 	<td>J. Alvarado</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						New Kent County Virginia Derby					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/colonial-downs">Colonial Downs</a></span>
						<!-- Colonial Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Wootton Asset</td>					<!-- 	<td>J. Ortiz</td>
					<td>H. Motion</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Parx Dash					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/parx-racing">Parx Racing</a></span>
						<!-- Parx Racing -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>5f Turf</td> -->
					<td data-title="Horse">The Critical Way</td>					<!-- 	<td>P. Lopez</td>
					<td>J. Delgado</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 29, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Keeneland Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Lei Halia (jpn)</td>					<!-- 	<td>H. Kameda</td>
					<td>T. Tajima</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 29, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Niigata Nisai Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$635,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Serifos (jpn)</td>					<!-- 	<td>Y. Kawada</td>
					<td>M. Nakauchida</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Ballerina Handicap					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Gamine</td>					<!-- 	<td>J. Velazquez</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Ballston Spa					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Viadera</td>					<!-- 	<td>J. Rosario</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Forego					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$600,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Yaupon</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Personal Ensign					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$600,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Letruska</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>F. Gutierrez</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/sword-dancer">Sword Dancer</a>					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$750,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Gufo</td>					<!-- 	<td>J. Rosario</td>
					<td>C. Clement</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/travers-stakes">Travers Stakes</a>					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,250,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Dirt</td> -->
					<td data-title="Horse">Essential Quality</td>					<!-- 	<td>L. Saez</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Charles Town Classic					</td>

					<td  data-title="Track" >
						<span  >Hollywood Casino At Charles Town Races</span>
						<!-- Hollywood Casino At Charles Town Races -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$800,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Art Collector</td>					<!-- 	<td>L. Saez</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Charles Town Oaks					</td>

					<td  data-title="Track" >
						<span  >Hollywood Casino At Charles Town Races</span>
						<!-- Hollywood Casino At Charles Town Races -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">R Adios Jersey</td>					<!-- 	<td>P. Lopez</td>
					<td>G. Baxter</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Smarty Jones Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/parx-racing">Parx Racing</a></span>
						<!-- Parx Racing -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Fulsome</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 22, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Green Flash Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>5f Turf</td> -->
					<td data-title="Horse">Lieutenant Dan</td>					<!-- 	<td>G. Franco</td>
					<td>S. Miyadi</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 22, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kitakyushu Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$808,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Yoka Yoka (jpn)</td>					<!-- 	<td>H. Miyuki</td>
					<td>K. Tani</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 22, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Sapporo Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,450,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Sodashi (jpn)</td>					<!-- 	<td>Y. Yoshida</td>
					<td>N. Sugai</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 22, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Dance Smartly Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$175,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Mutamakina</td>					<!-- 	<td>D. Davis</td>
					<td>C. Clement</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 22, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Highlander Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Silent Post</td>					<!-- 	<td>J. Stein</td>
					<td>N. Gonzalez</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 22, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Ontario Colleen Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Our Flash Drive</td>					<!-- 	<td>P. Husbands</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Del Mar Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Astronaut</td>					<!-- 	<td>V. Espinoza</td>
					<td>J. Shirreffs</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Del Mar Mile					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Mo Forza</td>					<!-- 	<td>F. Prat</td>
					<td>P. Miller</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/pacific-classic">Pacific Classic</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$750,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Dirt</td> -->
					<td data-title="Horse">Tripoli</td>					<!-- 	<td>T. Pereira</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Torrey Pines Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Private Mission</td>					<!-- 	<td>F. Prat</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Philip H. Iselin Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/monmouth-park">Monmouth Park</a></span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Code Of Honor</td>					<!-- 	<td>P. Lopez</td>
					<td>C. McGaughey III</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Alabama Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$600,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>10f Dirt</td> -->
					<td data-title="Horse">Malathaat</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Lake Placid					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Technical Analysis</td>					<!-- 	<td>J. Ortiz</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Seaway Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>7f AW</td> -->
					<td data-title="Horse">Boardroom</td>					<!-- 	<td>L. Contreras</td>
					<td>J. Carroll</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Singspiel Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Corelli</td>					<!-- 	<td>K. Kimura</td>
					<td>J. Thomas</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Rancho Bernardo Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
					<td data-title="Horse">Edgeway</td>					<!-- 	<td>J. Bravo</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 19, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Jonathan Sheppard Steeplechase					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>19f Turf</td> -->
					<td data-title="Horse">The Mean Queen</td>					<!-- 	<td>T. Garner</td>
					<td>K. Brion</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Longacres Mile					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/emerald-downs">Emerald Downs</a></span>
						<!-- Emerald Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Background</td>					<!-- 	<td>R. Bowen</td>
					<td>M. Puhich</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kokura Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Grand Speed (jpn)</td>					<!-- 	<td>R. Wada</td>
					<td>M. Nishimura</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Sekiya Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$808,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Christie (jpn)</td>					<!-- 	<td>M. Demuro</td>
					<td>H. Sugiyama</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						King Edward Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$175,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Olympic Runner</td>					<!-- 	<td>R. Hernandez</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Bruce D.					</td>

					<td  data-title="Track" >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Point Me By</td>					<!-- 	<td>L. Saez</td>
					<td>E. Kenneally</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Mister D.					</td>

					<td  data-title="Track" >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$600,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Two Emmys</td>					<!-- 	<td>J. Graham</td>
					<td>H. Robertson</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Pucker Up Stakes					</td>

					<td  data-title="Track" >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Shantisara</td>					<!-- 	<td>F. Prat</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fourstardave Handicap					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Got Stormy</td>					<!-- 	<td>T. Gaffalione</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Saratoga Special					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
					<td data-title="Horse">High Oak</td>					<!-- 	<td>J. Alvarado</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 8, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						La Jolla Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Zoffarelli</td>					<!-- 	<td>D. Van Dyke</td>
					<td>J. Mullins</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 8, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Elm Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$737,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Westerlund (jpn)</td>					<!-- 	<td>Y. Fujioka</td>
					<td>S. Sasaki</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 8, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Leopard Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$823,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Le Corsaire (jpn)</td>					<!-- 	<td>S. Ishibashi</td>
					<td>N. Hori</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 8, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Adirondack Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
					<td data-title="Horse">Wicked Halo</td>					<!-- 	<td>J. Ortiz</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 8, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Saratoga Oaks Invitational					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$700,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9.5f Turf</td> -->
					<td data-title="Horse">Con Lima</td>					<!-- 	<td>F. Prat</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Best Pal Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Pappacap</td>					<!-- 	<td>J. Bravo</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Yellow Ribbon Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Princess Grace</td>					<!-- 	<td>K. Desormeaux</td>
					<td>M. Stidham</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						West Virginia Derby					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/mountaineer">Mountaineer</a></span>
						<!-- Mountaineer -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Mr. Wireless</td>					<!-- 	<td>R. Vazquez</td>
					<td>W. Calhoun</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Glens Falls					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">War Like Goddess</td>					<!-- 	<td>J. Leparoux</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Longines Test Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Bella Sofia</td>					<!-- 	<td>L. Saez</td>
					<td>R. Rodriguez</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Whitney Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Knicks Go</td>					<!-- 	<td>J. Rosario</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						National Museum of Racing Hall of Fame					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Public Sector</td>					<!-- 	<td>F. Prat</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Troy Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>5.5f Turf</td> -->
					<td data-title="Horse">Fast Boat</td>					<!-- 	<td>T. Gaffalione</td>
					<td>J. Sharp</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Clement L. Hirsch Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Shedaresthedevil</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Queen Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$737,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Shamrock Hill (jpn)</td>					<!-- 	<td>T. Danno</td>
					<td>S. Sasaki</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Aug 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Royal North Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$175,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Amalfi Coast</td>					<!-- 	<td>J. Stein</td>
					<td>K. Attard</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Aug 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Vigil Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f AW</td> -->
					<td data-title="Horse">Souper Stonehenge</td>					<!-- 	<td>P. Husbands</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Bing Crosby Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Dr. Schivel</td>					<!-- 	<td>F. Prat</td>
					<td>M. Glatt</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Monmouth Oaks					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/monmouth-park">Monmouth Park</a></span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Leader Of The Band</td>					<!-- 	<td>F. Pennington</td>
					<td>J. Servis</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Alfred G. Vanderbilt Handicap					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$350,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Lexitonian</td>					<!-- 	<td>J. Lezcano</td>
					<td>J. Sisterson</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Bowling Green					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Cross Border</td>					<!-- 	<td>L. Saez</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Jim Dandy Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$600,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Essential Quality</td>					<!-- 	<td>L. Saez</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Ibis Summer Dash					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$808,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>5f Turf</td> -->
					<td data-title="Horse">Mome Johda (jpn)</td>					<!-- 	<td>Y. Kido</td>
					<td>Y. Ishige</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Shuvee Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Royal Flag</td>					<!-- 	<td>J. Rosario</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						King George VI &amp; Queen Elizabeth QIPCO Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,199,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Adayar</td>					<!-- 	<td>W. Buick</td>
					<td>C. Appleby</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Princess Margaret Keeneland Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$55,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Zain Claudette</td>					<!-- 	<td>R. Dawson</td>
					<td>I Mohammed</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Eddie Read Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">United</td>					<!-- 	<td>F. Prat</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						San Clemente Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Madone</td>					<!-- 	<td>J. Hernandez</td>
					<td>S. Callaghan</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Caress Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>5.5f Turf</td> -->
					<td data-title="Horse">Caravel</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>E. Merryman</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Coaching Club American Oaks					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Maracuja</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>R. Atras</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Nassau Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$175,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Jolie Olimpica</td>					<!-- 	<td>L. Contreras</td>
					<td>J. Carroll</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Carraig Insurance British Valiant Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$82,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Dreamloper</td>					<!-- 	<td>O. Murphy</td>
					<td>E. Walker</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Lake George Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Technical Analysis</td>					<!-- 	<td>J. Ortiz</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Cougar II Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f Dirt</td> -->
					<td data-title="Horse">Tizamagician</td>					<!-- 	<td>F. Prat</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Chukyo Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$808,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Clavel (jpn)</td>					<!-- 	<td>N. Yokoyama</td>
					<td>S. Yasuda</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hakodate Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Meiner Virtus (jpn)</td>					<!-- 	<td>Y. Tannai</td>
					<td>T. Miya</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Arlington Stakes					</td>

					<td  data-title="Track" >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9.5f Turf</td> -->
					<td data-title="Horse">Bizee Channel</td>					<!-- 	<td>J. Loveberry</td>
					<td>L. Rivelli</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Modesty Stakes					</td>

					<td  data-title="Track" >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9.5f Turf</td> -->
					<td data-title="Horse">Naval Laughter</td>					<!-- 	<td>S. Doyle</td>
					<td>C. Davis</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						San Diego Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/del-mar">Del Mar</a></span>
						<!-- Del Mar -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Express Train</td>					<!-- 	<td>J. Hernandez</td>
					<td>J. Shirreffs</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hakodate Nisai Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$635,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Love Me Doll (jpn)</td>					<!-- 	<td>Y. Furukawa</td>
					<td>H. Kakugawa</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/haskell-stakes">Haskell Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/monmouth-park">Monmouth Park</a></span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Mandaloun</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Molly Pitcher Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/monmouth-park">Monmouth Park</a></span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Graceful Princess</td>					<!-- 	<td>J. Rosario</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Monmouth Cup Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/monmouth-park">Monmouth Park</a></span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Dr Post</td>					<!-- 	<td>J. Rosario</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						United Nations Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/monmouth-park">Monmouth Park</a></span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Tribhuvan</td>					<!-- 	<td>F. Prat</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						WinStar Matchmaker Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/monmouth-park">Monmouth Park</a></span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Great Island</td>					<!-- 	<td>J. Rosario</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Diana Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Althiqa</td>					<!-- 	<td>M. Franco</td>
					<td>C. Appleby</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Sanford Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Wit</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 16, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Forbidden Apple					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Rinaldi</td>					<!-- 	<td>L. Saez</td>
					<td>H. Bond</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Quick Call Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$120,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>5.5f Turf</td> -->
					<td data-title="Horse">Golden Pal</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>W. Ward</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Schuylerville Stakes					</td>

					<td  data-title="Track" >
						<span  >Saratoga Race Course</span>
						<!-- Saratoga Race Course -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Pretty Birdie</td>					<!-- 	<td>L. Saez</td>
					<td>N. Casse</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Procyon Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$737,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Meisho Uzumasa (jpn)</td>					<!-- 	<td>A. Saito</td>
					<td>T. Yasuda</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Tanabata Sho					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Mount Gold (jpn)</td>					<!-- 	<td>M. Iwata</td>
					<td>Y. Ikee</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Marine Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f AW</td> -->
					<td data-title="Horse">Easy Time</td>					<!-- 	<td>R. Hernandez</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Victory Ride Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
					<td data-title="Horse">Souper Sensational</td>					<!-- 	<td>F. Prat</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Delaware Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/delaware-park">Delaware Park</a></span>
						<!-- Delaware Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>10f Dirt</td> -->
					<td data-title="Horse">Miss Marissa</td>					<!-- 	<td>D. Centeno</td>
					<td>J. Ryerson</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Robert G. Dick Memorial Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/delaware-park">Delaware Park</a></span>
						<!-- Delaware Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Dalika</td>					<!-- 	<td>M. Mena</td>
					<td>A. Stall, Jr.</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Selene Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f AW</td> -->
					<td data-title="Horse">Our Flash Drive</td>					<!-- 	<td>P. Husbands</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/indiana-derby">Indiana Derby</a>					</td>

					<td  data-title="Track" >
						<span  >Indiana Grand</span>
						<!-- Indiana Grand -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Mr. Wireless</td>					<!-- 	<td>R. Vazquez</td>
					<td>W. Calhoun</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Indiana Oaks					</td>

					<td  data-title="Track" >
						<span  >Indiana Grand</span>
						<!-- Indiana Grand -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Soothsay</td>					<!-- 	<td>F. Prat</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Dwyer Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">First Captain</td>					<!-- 	<td>J. Ortiz</td>
					<td>C. McGaughey III</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Great Lady M. Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/los-alamitos">Los Alamitos</a></span>
						<!-- Los Alamitos -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
					<td data-title="Horse">Gamine</td>					<!-- 	<td>J. Velazquez</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						John A. Nerud					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Mind Control</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						CBC Sho					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$808,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">A Will A Way (jpn)</td>					<!-- 	<td>K. Matsuyama</td>
					<td>T. Takano</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Radio Nikkei Sho					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">World Revival (jpn)</td>					<!-- 	<td>A. Tsumura</td>
					<td>K. Makita</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Los Alamitos Derby					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/los-alamitos">Los Alamitos</a></span>
						<!-- Los Alamitos -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Classier</td>					<!-- 	<td>M. Smith</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Suburban					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Dirt</td> -->
					<td data-title="Horse">Max Player</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Delaware Oaks					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/delaware-park">Delaware Park</a></span>
						<!-- Delaware Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Crazy Beautiful</td>					<!-- 	<td>M. Smith</td>
					<td>K. McPeek</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kent Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/delaware-park">Delaware Park</a></span>
						<!-- Delaware Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Yes This Time</td>					<!-- 	<td>J. Bravo</td>
					<td>K. Breen</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Princess Rooney Invitational					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$350,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Ce Ce</td>					<!-- 	<td>V. Espinoza</td>
					<td>M. McCarthy</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Smile Sprint Invitational					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Miles Ahead</td>					<!-- 	<td>V. Espinoza</td>
					<td>E. Plesa, Jr.</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Iowa Oaks					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/prairie-meadows">Prairie Meadows</a></span>
						<!-- Prairie Meadows -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$225,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Army Wife</td>					<!-- 	<td>J. Rosario</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jul 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Prairie Meadows Cornhusker Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/prairie-meadows">Prairie Meadows</a></span>
						<!-- Prairie Meadows -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Knicks Go</td>					<!-- 	<td>J. Rosario</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jul 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Dominion Day Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f AW</td> -->
					<td data-title="Horse">Mighty Heart</td>					<!-- 	<td>D. Fukumoto</td>
					<td>J. Carroll</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Takarazuka Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$3,096,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Cadenas (jpn)</td>					<!-- 	<td>S. Hamanaka</td>
					<td>K. Nakatake</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Chicago Stakes					</td>

					<td  data-title="Track" >
						<span  >Arlington</span>
						<!-- Arlington -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>7f AW</td> -->
					<td data-title="Horse">Abby Hatcher</td>					<!-- 	<td>A. Achard</td>
					<td>A. Meah</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Mother Goose Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Zaajel</td>					<!-- 	<td>J. Rosario</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Bashford Manor Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Double Thunder</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fleur de Lis					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Letruska</td>					<!-- 	<td>J. Ortiz</td>
					<td>F. Gutierrez</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Stephen Foster					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$600,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Maxfield</td>					<!-- 	<td>J. Ortiz</td>
					<td>B. Walsh</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Wise Dan Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Set Piece</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Ohio Derby					</td>

					<td  data-title="Track" >
						<span  >Thistledown</span>
						<!-- Thistledown -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Masqueparade</td>					<!-- 	<td>M. Mena</td>
					<td>A. Stall, Jr.</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 26, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Trillium Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f AW</td> -->
					<td data-title="Horse">Souper Escape</td>					<!-- 	<td>L. Contreras</td>
					<td>M. Trombetta</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Poker					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Oleksandra</td>					<!-- 	<td>J. Rosario</td>
					<td>N. Drysdale</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Mermaid Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$737,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Shamrock Hill (jpn)</td>					<!-- 	<td>T. Fujikake</td>
					<td>S. Sasaki</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Unicorn Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$721,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Ca Va (jpn)</td>					<!-- 	<td>Y. Ishikawa</td>
					<td>H. Uemura</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Eatontown Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/monmouth-park">Monmouth Park</a></span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Vigilantes Way</td>					<!-- 	<td>P. Lopez</td>
					<td>C. McGaughey III</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						American Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Restrained Vengence</td>					<!-- 	<td>T. Baze</td>
					<td>V. Brinkerhoff</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Jacques Cartier Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f AW</td> -->
					<td data-title="Horse">Souper Stonehenge</td>					<!-- 	<td>P. Husbands</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 19, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/diamond-jubilee-stakes">Diamond Jubilee Stakes</a>					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$959,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Dream Of Dreams</td>					<!-- 	<td>R. Moore</td>
					<td>S. Stoute</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 19, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hardwicke Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$219,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Wonderful Tonight</td>					<!-- 	<td>W. Buick</td>
					<td>D. Menuisier</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 19, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Jersey Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$103,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Turf</td> -->
					<td data-title="Horse">Creative Force</td>					<!-- 	<td>J. Doyle</td>
					<td>C. Appleby</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 19, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Whimsical Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/woodbine">Woodbine</a></span>
						<!-- Woodbine -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6f AW</td> -->
					<td data-title="Horse">Boardroom</td>					<!-- 	<td>L. Contreras</td>
					<td>J. Carroll</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Albany Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$89,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Sandrine</td>					<!-- 	<td>D. Probert</td>
					<td>A. Balding</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/commonwealth-cup">Commonwealth Cup</a>					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$480,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Campanelle</td>					<!-- 	<td>L. Dettori</td>
					<td>W. Ward</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Coronation Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$480,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Alcohol Free</td>					<!-- 	<td>O. Murphy</td>
					<td>A. Balding</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						King Edward VII Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$219,000					</td>

					<!-- <td>3yo c/g</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Alenquer</td>					<!-- 	<td>T. Marquand</td>
					<td>W. Haggas</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Gold Cup Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$480,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>20f Turf</td> -->
					<td data-title="Horse">Subjectivist</td>					<!-- 	<td>J. Fanning</td>
					<td>M. Johnston</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hampton Court Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$103,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Mohaafeth</td>					<!-- 	<td>J. Crowley</td>
					<td>W. Haggas</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Norfolk Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$110,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>5f Turf</td> -->
					<td data-title="Horse">Perfect Power</td>					<!-- 	<td>P. Hanagan</td>
					<td>R. Fahey</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Ribblesdale Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$219,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Loving Dream</td>					<!-- 	<td>R. Havlin</td>
					<td>J &amp; T Gosden</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 16, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Duke of Cambridge Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$192,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Indie Angel</td>					<!-- 	<td>L. Dettori</td>
					<td>J &amp; T Gosden</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 16, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Queen Mary Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$110,000					</td>

					<!-- <td>2yo f</td> -->
					<!-- 	<td>5f Turf</td> -->
					<td data-title="Horse">Quick Suzy</td>					<!-- 	<td>G. Carroll</td>
					<td>G. Cromwell</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Coventry Stakes					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$137,000					</td>

					<!-- <td>2yo</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Berkshire Shadow</td>					<!-- 	<td>O. Murphy</td>
					<td>A. Balding</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/queen-anne-stakes">Queen Anne Stakes</a>					</td>

					<td  data-title="Track" >
						<span  >Ascot</span>
						<!-- Ascot -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$548,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Palace Pier</td>					<!-- 	<td>L. Dettori</td>
					<td>J &amp; T Gosden</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Epsom Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Moira Aithon (jpn)</td>					<!-- 	<td>K. Kikuzawa</td>
					<td>T. Kikuzawa</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hakodate Sprint Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$808,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Sensho Yuto (jpn)</td>					<!-- 	<td>Y. Nakai</td>
					<td>K. Sasada</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Affirmed Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">The Chosen Vron</td>					<!-- 	<td>U. Rispoli</td>
					<td>J. Kruljac</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 12, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Old Forester Mint Julep Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Mintd</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>B. Walsh</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 12, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Salvator Mile Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/monmouth-park">Monmouth Park</a></span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Informative</td>					<!-- 	<td>J. Ferrer</td>
					<td>U. St. Lewis</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Yasuda Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,362,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Daiwa Cagney (jpn)</td>					<!-- 	<td>S. Ishibashi</td>
					<td>T. Kikuzawa</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Acorn Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Search Results</td>					<!-- 	<td>J. Castellano</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Belmont Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,500,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>12f Dirt</td> -->
					<td data-title="Horse">Essential Quality</td>					<!-- 	<td>L. Saez</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Brooklyn Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Dirt</td> -->
					<td data-title="Horse">Lone Rock</td>					<!-- 	<td>R. Vazquez</td>
					<td>R. Diodoro</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Jaipur Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Casa Creed</td>					<!-- 	<td>J. Alvarado</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Just a Game Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Althiqa</td>					<!-- 	<td>M. Smith</td>
					<td>C. Appleby</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Manhattan Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$750,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Domestic Spending</td>					<!-- 	<td>F. Prat</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Metropolitan Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Silver State</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Ogden Phipps Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Letruska</td>					<!-- 	<td>J. Ortiz</td>
					<td>F. Gutierrez</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Woody Stephens Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Drain The Clock</td>					<!-- 	<td>J. Ortiz</td>
					<td>S. Joseph, Jr.</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Naruo Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Blast Onepiece (jpn)</td>					<!-- 	<td>Y. Iwata</td>
					<td>M. Otake</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Monmouth Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/monmouth-park">Monmouth Park</a></span>
						<!-- Monmouth Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Devamani</td>					<!-- 	<td>N. Juarez</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						New York Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$750,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Mean Mary</td>					<!-- 	<td>L. Saez</td>
					<td>H. Motion</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						True North Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
					<td data-title="Horse">Firenze Fire</td>					<!-- 	<td>J. Ortiz</td>
					<td>K. Breen</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jun 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Intercontinental Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Turf</td> -->
					<td data-title="Horse">Change Of Control</td>					<!-- 	<td>C. Hernandez</td>
					<td>M. Lovell</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jun 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Wonder Again Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Con Lima</td>					<!-- 	<td>F. Prat</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Steve Sexton Mile Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/lone-star-park">Lone Star Park</a></span>
						<!-- Lone Star Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Mo Mosa</td>					<!-- 	<td>R. Vazquez</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Gamely Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Maxim Rate</td>					<!-- 	<td>J. Hernandez</td>
					<td>S. Callaghan</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hollywood Gold Cup					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Dirt</td> -->
					<td data-title="Horse">Country Grammer</td>					<!-- 	<td>F. Prat</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Shoemaker Mile Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Smooth Like Straight</td>					<!-- 	<td>U. Rispoli</td>
					<td>M. McCarthy</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Meguro Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,173,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12.5f Turf</td> -->
					<td data-title="Horse">Gold Gear (jpn)</td>					<!-- 	<td>H. Tanabe</td>
					<td>K. Ito</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Tokyo Yushun					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$4,115,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Wonderful Town (jpn)</td>					<!-- 	<td>R. Wada</td>
					<td>Y. Takahashi</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Summertime Oaks					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Crazy Beautiful</td>					<!-- 	<td>M. Smith</td>
					<td>K. McPeek</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 29, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Pennine Ridge					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Sainthood</td>					<!-- 	<td>J. Rosario</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 29, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Matt Winn Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$125,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Fulsome</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 29, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Charles Whittingham Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Award Winner</td>					<!-- 	<td>J. Hernandez</td>
					<td>D. Hofmans</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 29, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Daytona Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6.5f Turf</td> -->
					<td data-title="Horse">Bombard</td>					<!-- 	<td>F. Prat</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 29, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Triple Bend Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Magic On Tap</td>					<!-- 	<td>J. Hernandez</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Yushun Himba					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,273,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Through Seven Seas (jpn)</td>					<!-- 	<td>K. Tosaki</td>
					<td>T. Ozeki</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 22, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Winning Colors Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$125,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Sconsin</td>					<!-- 	<td>T. Gaffalione</td>
					<td>G. Foley</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 22, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Heian Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$737,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9.5f Dirt</td> -->
					<td data-title="Horse">Suave Aramis (jpn)</td>					<!-- 	<td>D. Matsuda</td>
					<td>N. Sugai</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 22, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Santa Maria Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">As Time Goes By</td>					<!-- 	<td>M. Smith</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 16, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Victoria Mile					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,160,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Resistencia (jpn)</td>					<!-- 	<td>Y. Take</td>
					<td>T. Matsushita</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Soaring Softly Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f Turf</td> -->
					<td data-title="Horse">Bye Bye</td>					<!-- 	<td>E. Cancel</td>
					<td>C. Clement</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Louisville Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$125,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Arklow</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Keio Hai Spring Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,221,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Turf</td> -->
					<td data-title="Horse">Air Almas (usa)</td>					<!-- 	<td>K. Matsuyama</td>
					<td>M. Ikezoe</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Chick Lang Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/pimlico">Pimlico</a></span>
						<!-- Pimlico -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Mighty Mischief</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Dinner Party Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/pimlico">Pimlico</a></span>
						<!-- Pimlico -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Somelikeithotbrown</td>					<!-- 	<td>J. Ortiz</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Gallorette Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/pimlico">Pimlico</a></span>
						<!-- Pimlico -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Mean Mary</td>					<!-- 	<td>L. Saez</td>
					<td>H. Motion</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Maryland Sprint Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/pimlico">Pimlico</a></span>
						<!-- Pimlico -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Special Reserve</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Preakness Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/pimlico">Pimlico</a></span>
						<!-- Pimlico -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9.5f Dirt</td> -->
					<td data-title="Horse">Rombauer</td>					<!-- 	<td>F. Prat</td>
					<td>M. McCarthy</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 15, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Lazaro Barrera Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
					<td data-title="Horse">The Chosen Vron</td>					<!-- 	<td>U. Rispoli</td>
					<td>J. Kruljac</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Allaire duPont Distaff Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/pimlico">Pimlico</a></span>
						<!-- Pimlico -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Spice Is Nice</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Black-Eyed Susan Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/pimlico">Pimlico</a></span>
						<!-- Pimlico -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Army Wife</td>					<!-- 	<td>J. Rosario</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Miss Preakness Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/pimlico">Pimlico</a></span>
						<!-- Pimlico -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Red Ghost</td>					<!-- 	<td>J. Velazquez</td>
					<td>W. Ward</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Pimlico Special					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/pimlico">Pimlico</a></span>
						<!-- Pimlico -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9.5f Dirt</td> -->
					<td data-title="Horse">Last Judgement</td>					<!-- 	<td>J. Ortiz</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 9, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						NHK Mile Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,160,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Raymond Barows (jpn)</td>					<!-- 	<td>S. Hamanaka</td>
					<td>H. Uemura</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 9, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Niigata Daishoten					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Potager (jpn)</td>					<!-- 	<td>A. Nishimura</td>
					<td>Y. Tomomichi</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 8, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Peter Pan Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Promise Keeper</td>					<!-- 	<td>L. Saez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 8, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Runhappy Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Firenze Fire</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>K. Breen</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 8, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Vagrancy					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
					<td data-title="Horse">Victim Of Love</td>					<!-- 	<td>J. Rosario</td>
					<td>T. Beattie</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 8, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kyoto Shimbun Hai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,120,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Breakup (jpn)</td>					<!-- 	<td>T. Danno</td>
					<td>Y. Kuroiwa</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 8, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Santa Barbara Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Neige Blanche</td>					<!-- 	<td>J. Hernandez</td>
					<td>L. Powell</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Ruffian Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Vault</td>					<!-- 	<td>J. Rosario</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Tenno Sho Spring					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$3,096,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>16f Turf</td> -->
					<td data-title="Horse">World Premiere (jpn)</td>					<!-- 	<td>Y. Fukunaga</td>
					<td>Y. Tomomichi</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fort Marcy Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Tribhuvan</td>					<!-- 	<td>E. Cancel</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Sheepshead Bay Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Magic Attitude</td>					<!-- 	<td>T. McCarthy</td>
					<td>A. Delacour</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Westchester Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/belmont-park">Belmont Park</a></span>
						<!-- Belmont Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Dr Post</td>					<!-- 	<td>M. Franco</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						American Turf					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Du Jour</td>					<!-- 	<td>F. Prat</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Churchill Downs Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Flagstaff</td>					<!-- 	<td>L. Saez</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Derby City Distaff Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Gamine</td>					<!-- 	<td>J. Velazquez</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kentucky Derby					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$3,000,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Dirt</td> -->
					<td data-title="Horse">Medina Spirit</td>					<!-- 	<td>J. Velazquez</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Longines Churchill Distaff Turf Mile					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Blowout</td>					<!-- 	<td>F. Prat</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Old Forester Bourbon Turf Classic					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Colonel Liam</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							May 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Aoba Sho					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,120,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Kingston Boy (jpn)</td>					<!-- 	<td>C. Lemaire</td>
					<td>K. Fujisawa</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							May 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Senorita Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Madone</td>					<!-- 	<td>J. Hernandez</td>
					<td>S. Callaghan</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Alysheba Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Maxfield</td>					<!-- 	<td>J. Ortiz</td>
					<td>B. Walsh</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Edgewood Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Gift List</td>					<!-- 	<td>J. Castellano</td>
					<td>B. Lynch</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Eight Belles					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Obligatory</td>					<!-- 	<td>J. Ortiz</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kentucky Oaks					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,250,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Malathaat</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						La Troienne Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Shedaresthedevil</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Turf Sprint					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/churchill-downs">Churchill Downs</a></span>
						<!-- Churchill Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>5.5f Turf</td> -->
					<td data-title="Horse">Fast Boat</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>J. Sharp</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Flora Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,072,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Snow Halation (jpn)</td>					<!-- 	<td>G. Maruyama</td>
					<td>T. Kanari</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 25, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Yomiuri Milers Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,221,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Air Lolonois (jpn)</td>					<!-- 	<td>K. Ikezoe</td>
					<td>K. Sasada</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						San Francisco Mile					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/golden-gate-fields">Golden Gate Fields</a></span>
						<!-- Golden Gate Fields -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Whisper Not</td>					<!-- 	<td>G. Franco</td>
					<td>R. Baltas</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fukushima Himba Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Pallas Athena (jpn)</td>					<!-- 	<td>R. Sakai</td>
					<td>M. Takayanagi</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Santa Margarita Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">As Time Goes By</td>					<!-- 	<td>M. Smith</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Bewitch Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">War Like Goddess</td>					<!-- 	<td>J. Leparoux</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Antares Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$737,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Danon Splendor (jpn)</td>					<!-- 	<td>A. Saito</td>
					<td>T. Yasuda</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Satsuki Sho					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,273,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Deep Monster (jpn)</td>					<!-- 	<td>K. Tosaki</td>
					<td>Y. Ikee</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kona Gold Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6.5f Dirt</td> -->
					<td data-title="Horse">Cezanne</td>					<!-- 	<td>F. Prat</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Tokyo City Cup					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Dirt</td> -->
					<td data-title="Horse">Tizamagician</td>					<!-- 	<td>F. Prat</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Arlington Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Admire Sage (jpn)</td>					<!-- 	<td>K. Ikezoe</td>
					<td>Y. Tomomichi</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Apple Blossom Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/oaklawn-park">Oaklawn Park</a></span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Letruska</td>					<!-- 	<td>F. Gutierrez</td>
					<td>F. Gutierrez</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Oaklawn Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/oaklawn-park">Oaklawn Park</a></span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Silver State</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>F. Gutierrez</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Californian Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Royal Ship</td>					<!-- 	<td>M. Smith</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 16, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Baird Doubledogdare Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Bonny South</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Oka Sho					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,160,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Akaitorino Musume (jpn)</td>					<!-- 	<td>T. Yokoyama</td>
					<td>S. Kunieda</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hanshin Himba Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,135,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Meisho Glocke (jpn)</td>					<!-- 	<td>S. Hamanaka</td>
					<td>Y. Arakawa</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						New Zealand Trophy					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,120,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Gold Chalice (jpn)</td>					<!-- 	<td>K. Tanaka</td>
					<td>K. Take</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Ben Ali Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Silver Dust</td>					<!-- 	<td>A. Beschizza</td>
					<td>W. Calhoun</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Coolmore Jenny Wiley Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Juliet Foxtrot</td>					<!-- 	<td>T. Gaffalione</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Stonestreet Lexington Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">King Fury</td>					<!-- 	<td>B. Hernandez, Jr.</td>
					<td>K. McPeek</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/arkansas-derby">Arkansas Derby</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/oaklawn-park">Oaklawn Park</a></span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Super Stock</td>					<!-- 	<td>R. Santana, Jr.</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Count Fleet Sprint Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/oaklawn-park">Oaklawn Park</a></span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$500,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">C Z Rocket</td>					<!-- 	<td>F. Geroux</td>
					<td>P. Miller</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Osaka Hai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,443,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Bravas (jpn)</td>					<!-- 	<td>K. Miura</td>
					<td>Y. Tomomichi</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 4, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Las Flores Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Gamine</td>					<!-- 	<td>J. Velazquez</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Bay Shore Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/aqueduct">Aqueduct</a></span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Drain The Clock</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>S. Joseph, Jr.</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Carter Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/aqueduct">Aqueduct</a></span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Mischevious Alex</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>S. Joseph, Jr.</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Excelsior Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/aqueduct">Aqueduct</a></span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Modernist</td>					<!-- 	<td>J. Alvarado</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Gazelle Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/aqueduct">Aqueduct</a></span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Search Results</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Wood Memorial Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/aqueduct">Aqueduct</a></span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$750,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Bourbonic</td>					<!-- 	<td>K. Carmouche</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Lord Derby Challenge Trophy					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$808,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Shonan Rise (jpn)</td>					<!-- 	<td>H. Uchida</td>
					<td>H. Uehara</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Appalachian Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Jouster</td>					<!-- 	<td>L. Saez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Central Bank Ashland Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Malathaat</td>					<!-- 	<td>J. Rosario</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Commonwealth Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Flagstaff</td>					<!-- 	<td>J. Rosario</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Madison Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Kimari</td>					<!-- 	<td>J. Rosario</td>
					<td>W. Ward</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Shakertown Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>5.5f Turf</td> -->
					<td data-title="Horse">Bound For Nowhere</td>					<!-- 	<td>J. Rosario</td>
					<td>W. Ward</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Toyota Blue Grass Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$800,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Essential Quality</td>					<!-- 	<td>L. Saez</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Runhappy Santa Anita Derby					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$750,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Rock Your World</td>					<!-- 	<td>U. Rispoli</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Santa Anita Oaks					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Soothsay</td>					<!-- 	<td>F. Prat</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Distaff Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/aqueduct">Aqueduct</a></span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Paris Lights</td>					<!-- 	<td>J. Alvarado</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Apr 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Beaumont Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>abt 7f Dirt</td> -->
					<td data-title="Horse">Twenty Carat</td>					<!-- 	<td>L. Saez</td>
					<td>W. Ward</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Apr 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Transylvania Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/keeneland">Keeneland</a></span>
						<!-- Keeneland -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Scarlett Sky</td>					<!-- 	<td>J. Rosario</td>
					<td>C. McGaughey III</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						March Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$737,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Memory Ko (jpn)</td>					<!-- 	<td>Y. Furukawa</td>
					<td>M. Matsunaga</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Takamatsunomiya Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,362,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Lauda Sion (jpn)</td>					<!-- 	<td>M. Demuro</td>
					<td>T. Saito</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/florida-derby">Florida Derby</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$750,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Known Agenda</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Gulfstream Park Oaks					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Crazy Beautiful</td>					<!-- 	<td>J. Ortiz</td>
					<td>K. McPeek</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Orchid Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">War Like Goddess</td>					<!-- 	<td>J. Leparoux</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Pan American Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Churn N Burn</td>					<!-- 	<td>J. Leparoux</td>
					<td>I. Wilkes</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Mainichi Hai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Westonbirt (jpn)</td>					<!-- 	<td>Y. Kokubun</td>
					<td>T. Yoshioka</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Nikkei Sho					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,383,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12.5f Turf</td> -->
					<td data-title="Horse">Humidor (jpn)</td>					<!-- 	<td>Y. Yoshida</td>
					<td>H. Kotegawa</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Santa Ana Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Going To Vegas</td>					<!-- 	<td>U. Rispoli</td>
					<td>R. Baltas</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hanshin Daishoten					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,383,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>15f Turf</td> -->
					<td data-title="Horse">Meisho Tengen (jpn)</td>					<!-- 	<td>M. Sakai</td>
					<td>K. Ikezoe</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Spring Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,120,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Nishino Oikaze (jpn)</td>					<!-- 	<td>M. Katsuura</td>
					<td>Y. Takeichi</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fair Grounds Oaks					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/fair-grounds">Fair Grounds</a></span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Travel Column</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/louisiana-derby">Louisiana Derby</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/fair-grounds">Fair Grounds</a></span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9.5f Dirt</td> -->
					<td data-title="Horse">Hot Rod Charlie</td>					<!-- 	<td>J. Rosario</td>
					<td>L. Mora</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Muniz Memorial Classic					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/fair-grounds">Fair Grounds</a></span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Colonel Liam</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						New Orleans Classic					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/fair-grounds">Fair Grounds</a></span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Chess Chief</td>					<!-- 	<td>L. Saez</td>
					<td>D. Stewart</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Falcon Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Turf</td> -->
					<td data-title="Horse">Rooks Nest (jpn)</td>					<!-- 	<td>H. Miyuki</td>
					<td>T. Hamada</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Flower Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$721,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Uberleben (jpn)</td>					<!-- 	<td>Y. Tannai</td>
					<td>T. Tezuka</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						San Luis Rey Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">United</td>					<!-- 	<td>F. Prat</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kinko Sho					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,383,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Kiseki (jpn)</td>					<!-- 	<td>M. Demuro</td>
					<td>Y. Tsujino</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Azeri Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/oaklawn-park">Oaklawn Park</a></span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$350,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Shedaresthedevil</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/rebel-stakes">Rebel Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/oaklawn-park">Oaklawn Park</a></span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Concert Tour</td>					<!-- 	<td>J. Rosario</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Beholder Mile					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Swiss Skydiver</td>					<!-- 	<td>R. Albarado</td>
					<td>K. McPeek</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Yayoi Sho					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,120,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">So Valiant (jpn)</td>					<!-- 	<td>T. Ono</td>
					<td>M. Otake</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Santa Ysabel Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Beautiful Gift</td>					<!-- 	<td>J. Velazquez</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/gotham-stakes">Gotham Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/aqueduct">Aqueduct</a></span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Weyburn</td>					<!-- 	<td>T. McCarthy</td>
					<td>J. Jerkens</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Tom Fool Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/aqueduct">Aqueduct</a></span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Chateau</td>					<!-- 	<td>K. Carmouche</td>
					<td>R. Atras</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Ocean Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Loving Answer (jpn)</td>					<!-- 	<td>M. Katsuura</td>
					<td>K. Ishizaka</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Tulip Sho					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,072,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Meikei Yell (jpn)</td>					<!-- 	<td>Y. Take</td>
					<td>H. Take</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Frank E. Kilroe Mile					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Hit The Road</td>					<!-- 	<td>F. Geroux</td>
					<td>D. Blacker</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						San Carlos Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Brickyard Ride</td>					<!-- 	<td>A. Centeno</td>
					<td>C. Lewis</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						San Felipe Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Life Is Good</td>					<!-- 	<td>M. Smith</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Santa Anita Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Dirt</td> -->
					<td data-title="Horse">Idol</td>					<!-- 	<td>J. Rosario</td>
					<td>R. Baltas</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Challenger Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/tampa-bay-downs">Tampa Bay Downs</a></span>
						<!-- Tampa Bay Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Last Judgement</td>					<!-- 	<td>D. Centeno</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Florida Oaks					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/tampa-bay-downs">Tampa Bay Downs</a></span>
						<!-- Tampa Bay Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Domain Expertise</td>					<!-- 	<td>A. Gallardo</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Mar 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hillsborough Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/tampa-bay-downs">Tampa Bay Downs</a></span>
						<!-- Tampa Bay Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$225,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Micheline</td>					<!-- 	<td>L. Saez</td>
					<td>M. Stidham</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Mar 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Lambholm South Tampa Bay Derby					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/tampa-bay-downs">Tampa Bay Downs</a></span>
						<!-- Tampa Bay Downs -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Helium</td>					<!-- 	<td>J. Ferrer</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Hankyu Hai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Turf</td> -->
					<td data-title="Horse">Seizinger (jpn)</td>					<!-- 	<td>K. Kokubun</td>
					<td>K. Makita</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Nakayama Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,383,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Sun Appleton (jpn)</td>					<!-- 	<td>Y. Shibata</td>
					<td>E. Nakano</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 28, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Bayakoa Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/oaklawn-park">Oaklawn Park</a></span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Monomoy Girl</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Canadian Turf Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$125,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Venezuelan Hug</td>					<!-- 	<td>J. Alvarado</td>
					<td>D. Gargan</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Davona Dale Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Wholebodemeister</td>					<!-- 	<td>E. Zayas</td>
					<td>J. Avila</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fountain of Youth Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Greatest Honour</td>					<!-- 	<td>J. Ortiz</td>
					<td>C. McGaughey III</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Gulfstream Park Mile					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Fearless</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Herecomesthebride Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Con Lima</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Honey Fox Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$125,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Got Stormy</td>					<!-- 	<td>T. Gaffalione</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Mac Diarmida Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Phantom Currency</td>					<!-- 	<td>P. Lopez</td>
					<td>B. Lynch</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						The Very One Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$125,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9.5f Turf</td> -->
					<td data-title="Horse">Antoinette</td>					<!-- 	<td>J. Ortiz</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Razorback Handicap					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/oaklawn-park">Oaklawn Park</a></span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$600,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Mystic Guide</td>					<!-- 	<td>L. Saez</td>
					<td>M. Stidham</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 27, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Southwest Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/oaklawn-park">Oaklawn Park</a></span>
						<!-- Oaklawn Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$750,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Essential Quality</td>					<!-- 	<td>L. Saez</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						February Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$2,058,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Wonder Lider (jpn)</td>					<!-- 	<td>N. Yokoyama</td>
					<td>S. Yasuda</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 21, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kokura Daishoten					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Dirndl (jpn)</td>					<!-- 	<td>T. Danno</td>
					<td>Y Okumura</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Royal Delta Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Queen Nekia</td>					<!-- 	<td>C. Lanerie</td>
					<td>S. Joseph, Jr.</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Diamond Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>17f Turf</td> -->
					<td data-title="Horse">Miss Mamma Mia (jpn)</td>					<!-- 	<td>F. Matsuwaka</td>
					<td>R. Terashima</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kyoto Himba Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$737,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Turf</td> -->
					<td data-title="Horse">I Love Tailor (jpn)</td>					<!-- 	<td>Y. Iwata</td>
					<td>H. Kawachi</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						General George Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/laurel-park">Laurel Park</a></span>
						<!-- Laurel Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Share The Ride</td>					<!-- 	<td>V. Carrasco</td>
					<td>M. Penaloza</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 20, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Runhappy Barbara Fritchie Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/laurel-park">Laurel Park</a></span>
						<!-- Laurel Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Hibiscus Punch</td>					<!-- 	<td>H. Karamanos</td>
					<td>J. Nixon</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kyodo News Service Hai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Platina Treasure (jpn)</td>					<!-- 	<td>H. Tanabe</td>
					<td>S. Kunieda</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 14, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kyoto Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,283,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Jinambo (jpn)</td>					<!-- 	<td>Y. Iwata</td>
					<td>N. Hori</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fair Grounds  Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/fair-grounds">Fair Grounds</a></span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Captivating Moon</td>					<!-- 	<td>M. Pedroza</td>
					<td>C. Block</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Mineshaft Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/fair-grounds">Fair Grounds</a></span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Maxfield</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Walsh</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/rachel-alexandra-stakes">Rachel Alexandra Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/fair-grounds">Fair Grounds</a></span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Clairiere</td>					<!-- 	<td>J. Talamo</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/risen-star-stakes">Risen Star Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/fair-grounds">Fair Grounds</a></span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$400,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Mandaloun</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Gulfstream Park Sprint					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Dirt</td> -->
					<td data-title="Horse">Mischevious Alex</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>S. Joseph, Jr.</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Gulfstream Park Turf Sprint					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>5f Turf</td> -->
					<td data-title="Horse">Leinster</td>					<!-- 	<td>L. Saez</td>
					<td>G. Arnold, II</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Queen Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$721,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Stellaria (jpn)</td>					<!-- 	<td>Y. Fukunaga</td>
					<td>T. Saito</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 13, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/santa-monica-stakes">Santa Monica Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Merneith</td>					<!-- 	<td>E. Maldonado</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kisaragi Sho					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Yoho Lake (jpn)</td>					<!-- 	<td>Y. Take</td>
					<td>Y. Tomomichi</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 7, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Tokyo Shimbun Hai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$808,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Prodigal Son (jpn)</td>					<!-- 	<td>Y. Fujioka</td>
					<td>S. Kunieda</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Withers Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/aqueduct">Aqueduct</a></span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Risk Taking</td>					<!-- 	<td>E. Cancel</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Suwannee River Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Great Island</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						San Marcos Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Masterofoxhounds</td>					<!-- 	<td>J. Rosario</td>
					<td>R. Baltas</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						San Vicente Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Concert Tour</td>					<!-- 	<td>J. Rosario</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Thunder Road Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Hit The Road</td>					<!-- 	<td>U. Rispoli</td>
					<td>D. Blacker</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Lambholm South Endeavour Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/tampa-bay-downs">Tampa Bay Downs</a></span>
						<!-- Tampa Bay Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$175,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Counterparty Risk</td>					<!-- 	<td>J. Velazquez</td>
					<td>C. Brown</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Feb 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Sam F. Davis Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/tampa-bay-downs">Tampa Bay Downs</a></span>
						<!-- Tampa Bay Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$250,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Candy Man Rocket</td>					<!-- 	<td>J. Alvarado</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Feb 6, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Tampa Bay Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/tampa-bay-downs">Tampa Bay Downs</a></span>
						<!-- Tampa Bay Downs -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$175,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f Turf</td> -->
					<td data-title="Horse">Get Smokin</td>					<!-- 	<td>J. Alvarado</td>
					<td>T. Bush</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Negishi Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Time Flyer (jpn)</td>					<!-- 	<td>C. Lemaire</td>
					<td>K. Matsuda</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Silk Road Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$808,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f Turf</td> -->
					<td data-title="Horse">Kaiser Melange (jpn)</td>					<!-- 	<td>K. Dazai</td>
					<td>E. Nakano</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Houston Ladies Classic					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/sam-houston">Sam Houston</a></span>
						<!-- Sam Houston -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$300,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Letruska</td>					<!-- 	<td>J. Castanon</td>
					<td>F. Gutierrez</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 31, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						John B. Connally Turf Cup					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/sam-houston">Sam Houston</a></span>
						<!-- Sam Houston -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Spooky Channel</td>					<!-- 	<td>J. Leparoux</td>
					<td>B. Lynch</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Toboggan Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/aqueduct">Aqueduct</a></span>
						<!-- Aqueduct -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">American Power</td>					<!-- 	<td>K. Carmouche</td>
					<td>R. Atras</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/forward-gal-stakes">Forward Gal Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Zaajel</td>					<!-- 	<td>L. Saez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/holy-bull-stakes">Holy Bull Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Greatest Honour</td>					<!-- 	<td>J. Ortiz</td>
					<td>C. McGaughey III</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Swale Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Drain The Clock</td>					<!-- 	<td>E. Zayas</td>
					<td>S. Joseph, Jr.</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Sweetest Chant Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">White Frost</td>					<!-- 	<td>J. Alvarado</td>
					<td>W. Mott</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Robert B. Lewis Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Medina Spirit</td>					<!-- 	<td>A. Cedillo</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 30, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/san-pasqual-stakes">San Pasqual Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Express Train</td>					<!-- 	<td>J. Hernandez</td>
					<td>J. Shirreffs</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						American Jockey Club Cup					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,283,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Tagano Diamante (jpn)</td>					<!-- 	<td>A. Tsumura</td>
					<td>I. Sameshima</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 24, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Tokai Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,135,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">T O Force (jpn)</td>					<!-- 	<td>K. Kokubun</td>
					<td>I. Okada</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fred W. Hooper					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$125,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Performer</td>					<!-- 	<td>J. Rosario</td>
					<td>C. McGaughey III</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Inside Information Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Pacific Gale</td>					<!-- 	<td>J. Velazquez</td>
					<td>J. Kimmel</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						La Prevoyante					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$125,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Always Shopping</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Pegasus World Cup Invitational					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$3,000,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Dirt</td> -->
					<td data-title="Horse">Knicks Go</td>					<!-- 	<td>J. Rosario</td>
					<td>B. Cox</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Pegasus World Cup Turf Invitational					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>I</td> -->

					<td  data-title="Purses" >
						$1,000,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9.5f Turf</td> -->
					<td data-title="Horse">Colonel Liam</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 23, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						William L. McKnight					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$150,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Tide Of The Sea</td>					<!-- 	<td>T. Gaffalione</td>
					<td>M. Maker</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 18, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Megahertz Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Mucho Unusual</td>					<!-- 	<td>J. Rosario</td>
					<td>T. Yakteen</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Keisei Hai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Ten Bagger (jpn)</td>					<!-- 	<td>K. Tosaki</td>
					<td>K. Fujioka</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Nikkei Shinshun Hai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$1,173,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>11f Turf</td> -->
					<td data-title="Horse">Valerio (jpn)</td>					<!-- 	<td>R. Wada</td>
					<td>I. Aizawa</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 17, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Astra Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>12f Turf</td> -->
					<td data-title="Horse">Quick</td>					<!-- 	<td>U. Rispoli</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 16, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/lecomte-stakes">Lecomte Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/fair-grounds">Fair Grounds</a></span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Midnight Bourbon</td>					<!-- 	<td>J. Talamo</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 16, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Louisiana Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/fair-grounds">Fair Grounds</a></span>
						<!-- Fair Grounds -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$125,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Title Ready</td>					<!-- 	<td>B. Hernandez, Jr.</td>
					<td>D. Stewart</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 16, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Aichi Hai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$737,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Tagano Aswad (jpn)</td>					<!-- 	<td>Y. Kitamura</td>
					<td>T. Igarashi</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 11, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Fairy Stakes					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$721,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Shadow Fax (jpn)</td>					<!-- 	<td>T. Ono</td>
					<td>K. Miyata</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 10, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Shinzan Kinen					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$780,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Tokai King (jpn)</td>					<!-- 	<td>R. Wada</td>
					<td>T. Nishihashi</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 9, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Tropical Turf					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/gulfstream-park">Gulfstream Park</a></span>
						<!-- Gulfstream Park -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Ride A Comet</td>					<!-- 	<td>T. Gaffalione</td>
					<td>M. Casse</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 9, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/la-canada-stakes">La Canada Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f Dirt</td> -->
					<td data-title="Horse">Sanenus</td>					<!-- 	<td>U. Rispoli</td>
					<td>M. McCarthy</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Kyoto Kimpai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f Turf</td> -->
					<td data-title="Horse">Peace One Paradis (jpn)</td>					<!-- 	<td>Y. Fukunaga</td>
					<td>M. Otake</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 5, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Nakayama Kimpai					</td>

					<td  data-title="Track" >
						<span  >Japan Racing Association</span>
						<!-- Japan Racing Association -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$838,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f Turf</td> -->
					<td data-title="Horse">Mount Gold (jpn)</td>					<!-- 	<td>H. Uchida</td>
					<td>Y. Ikee</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 3, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/santa-ynez-stakes">Santa Ynez Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f Dirt</td> -->
					<td data-title="Horse">Kalypso</td>					<!-- 	<td>J. Rosario</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/san-gabriel-stakes">San Gabriel Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f Turf</td> -->
					<td data-title="Horse">Anothertwistafate</td>					<!-- 	<td>J. Rosario</td>
					<td>P. Miller</td> -->
				</tr>
				<tr class="odd">
					<td data-title="Date">
						<time  >
							Jan 2, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						<a href="https://www.usracing.com/sham-stakes">Sham Stakes</a>					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>III</td> -->

					<td  data-title="Purses" >
						$100,000					</td>

					<!-- <td>3yo</td> -->
					<!-- 	<td>8f Dirt</td> -->
					<td data-title="Horse">Life Is Good</td>					<!-- 	<td>M. Smith</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr class="">
					<td data-title="Date">
						<time  >
							Jan 1, 2021						</time>
					</td>

					<td  data-title="Stakes" >
						Joe Hernandez Stakes					</td>

					<td  data-title="Track" >
						<span  ><a href="https://www.usracing.com/santa-anita">Santa Anita</a></span>
						<!-- Santa Anita -->
					</td>

					<!-- <td>II</td> -->

					<td  data-title="Purses" >
						$200,000					</td>

					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6.5f Turf</td> -->
					<td data-title="Horse">Hembree</td>					<!-- 	<td>J. Rosario</td>
					<td>P. Miller</td> -->
				</tr>
							</tbody>
		</table>
        </div>
        	<div>
        <table id='infoEntries' width='100%' border='0' cellspacing='0' cellpadding='0' class='table table-condensed table-striped table-bordered' >
        <tbody>
        <tr>
            <td class="dateUpdated center">
                <em id='updateemp'>Updated November 16, 2021.</em>
            </td>
        </tr>
        </tbody>
        </table>
        </div>
		