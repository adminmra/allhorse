	
	<script type="application/ld+json">
		{
		  "@context": "http://schema.org",
		  "@type": "Table",
		  "about": "Kentucky Derby Trainer Odds"
		}
    </script> 
	
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Kentucky Derby Trainer Odds" summary="The latest odds for the top Trainers for the Kentucky Derby.  Only available at BUSR.">
            <caption> Kentucky Derby Trainer Odds</caption>
            <tbody>
              <!--
  <tr>
                    <th colspan="3" class="center">
                    Horses - Kentucky Derby Trainer  - May 04                    </th>
            </tr>
-->
              <!--
  <tr>
                    <th colspan="3" class="center">
                    No Runner No Bet                    </th>
            </tr>
-->
    <tr class='head_title'><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Mark E. Casse</td><td>+2500</td><td>25/1</td></tr><tr><td>Danny Gargan</td><td>+2500</td><td>25/1</td></tr><tr><td>W. Bret Calhoun</td><td>+1800</td><td>18/1</td></tr><tr><td>Peter Miller</td><td>+5500</td><td>55/1</td></tr><tr><td>Bob Baffert</td><td>-135</td><td>20/27</td></tr><tr><td>George Weaver</td><td>+2000</td><td>20/1</td></tr><tr><td>Jason Servis</td><td>+600</td><td>6/1</td></tr><tr><td>William I. Mott</td><td>+500</td><td>5/1</td></tr><tr><td>Brendan P. Walsh</td><td>+4000</td><td>40/1</td></tr><tr><td>Todd A. Pletcher</td><td>+1000</td><td>10/1</td></tr><tr><td>Claude R. Mcgaughey Iii</td><td>+1800</td><td>18/1</td></tr><tr><td>Michael J. Trombetta</td><td>+2000</td><td>20/1</td></tr><tr><td>Koichi Tsunoda</td><td>+6000</td><td>60/1</td></tr><tr><td>Steven M. Asmussen</td><td>+4000</td><td>40/1</td></tr><tr><td>Gustavo Delgado</td><td>+6000</td><td>60/1</td></tr>            </tbody>
			<tfoot>
		<!-- <tr>
                        <td class="dateUpdated center"  colspan="3" >
                            <em id='updateemp'>  Updated February 3, 2020 </em>
                        </td>
                    </tr> -->
            </tfoot>
        </table>
    </div>
    
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
 	
        		
    