	
	<script type="application/ld+json">
			{
			  "@context": "http://schema.org",
			  "@type": "Table",
			  "about": "2019 Kentucky Derby - Margin Of Victory"
			}
    </script> 
	
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Kentucky Derby - Props">
           <!-- <caption></caption> -->
           <!-- <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="3">
                  <em id='updateemp'>  Updated February 3, 2020 </em> 
                </td>
              </tr>
            </tfoot> -->
            <tbody>
    			<!--	<tr>
					<th colspan="3" class="center">
					Horses - Kentucky Derby - Props  - May 04					</th>
			</tr> -->
    <caption>2019 Kentucky Derby  -  Margin Of Victory</caption><tr><th></th><th>Fractional</th><th>American</th></tr><tr><td>Dead Heat</td><td>30/1</td><td>+3000</td></tr><tr><td>Nose</td><td>10/1</td><td>+1000</td></tr><tr><td>Head</td><td>8/1</td><td>+800</td></tr><tr><td>Neck</td><td>8/1</td><td>+800</td></tr><tr><td>1/2 Length To 3/4 Length</td><td>15/4</td><td>+375</td></tr><tr><td>1 Length To 2 3/4 Lengths</td><td>2/1</td><td>+200</td></tr><tr><td>3 Lengths To 5 3/4 Lengths</td><td>9/4</td><td>+225</td></tr><tr><td>6 Lengths To 7 3/4 Lengths</td><td>13/2</td><td>+650</td></tr><tr><td>8 Lengths To 10 3/4 Lenghts</td><td>29/4</td><td>+725</td></tr><tr><td>11 Lengths To 14 3/4 Lengths</td><td>10/1</td><td>+1000</td></tr><tr><td>15 Lengths Or More</td><td>12/1</td><td>+1200</td></tr>            </tbody>
        </table>
    </div>
    
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>

        
    