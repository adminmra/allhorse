	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "Kentucky Derby Odds:  Props"
	  }
	</script>
	<div>
		<table class="oddstable" width="100%" cellpadding="0" cellspacing="0"  summary="Kentucky Derby Betting: Props">
			<caption>Horses - Special Props</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>  - Updated November 16, 2021 </em>
                        <br><!--All odds are fixed odds prices.-->
                    </td>
                </tr> 
            </tfoot>
			<tbody>
			<tr>
					<th colspan="3" class="center oddsheader">
					Horses - Special Props  - Jan 01					</th>
			</tr>
            
	<tr><td  colspan="3" class="center">
    <table id="o20t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Total Spectators At The 2021 Kentucky Derby?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Over 44500</td><td>20/23</td><td>-115</td></tr><tr><td>Under 44500</td><td>20/23</td><td>-115</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o21t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Triple Crown Races Be Run In Normal Order In 2021?</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes (  Kd, Preakness, Belmont)</td><td>1/8</td><td>-800</td></tr><tr><td>No ( Any Other Order)</td><td>4/1</td><td>+400</td></tr></tbody></table></td></tr><tr><td  colspan="3" class="center">
    <table id="o22t" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered"><tbody><tr><th colspan="3" class="center">Any 2021 Triple Crown Race Held At New Location</th></tr><tr  class="sortar"><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Yes - A Race Run At New Location</td><td>5/2</td><td>+250</td></tr><tr><td>No - Usual Venues Used</td><td>2/9</td><td>-450</td></tr></tbody></table></td></tr>			</tbody>
		</table>
	</div>
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
addSort('o20t');addSort('o21t');addSort('o22t');</script>

	