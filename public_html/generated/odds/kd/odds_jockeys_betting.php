
	<script type="application/ld+json">
		{
		  "@context": "http://schema.org",
		  "@type": "Table",
		  "about": "Kentucky Derby Jockey Odds"
		}
    </script> 
		
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Kentucky Derby Jockey Odds" summary="The latest odds for the top Trainers for the Kentucky Derby.  Only available at BUSR.">
            <caption> Kentucky Derby Jockey Odds</caption>
            <tbody>
              <!--
  <tr>
                    <th colspan="3" class="center">
                    Horses - Kentucky Derby Jockey  - May 04                    </th>
            </tr>
-->
              <!--
  <tr>
                    <th colspan="3" class="center">
                    No Runner No Bet                    </th>
            </tr>
-->
    <tr class='head_title'><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Tyler Gaffalione</td><td>+1600</td><td>16/1</td></tr><tr><td>Junior Alvarado</td><td>+2000</td><td>20/1</td></tr><tr><td>Gabriel Saez</td><td>+1600</td><td>16/1</td></tr><tr><td>Garden Van Dyke</td><td>+5000</td><td>50/1</td></tr><tr><td>Irad Ortiz Jr.</td><td>+550</td><td>11/2</td></tr><tr><td>Javier Castellano</td><td>+2000</td><td>20/1</td></tr><tr><td>Luis Saez</td><td>+800</td><td>8/1</td></tr><tr><td>Jose Ortiz</td><td>+600</td><td>6/1</td></tr><tr><td>Ricardo Santana Jr.</td><td>+4000</td><td>40/1</td></tr><tr><td>Mike Smith</td><td>+450</td><td>9/2</td></tr><tr><td>John Velazquez</td><td>+1500</td><td>15/1</td></tr><tr><td>Julian Pimentel</td><td>+2000</td><td>20/1</td></tr><tr><td>Julien Leparoux</td><td>+5500</td><td>55/1</td></tr><tr><td>Joel Rosario</td><td>+450</td><td>9/2</td></tr><tr><td>Florent Geroux</td><td>+550</td><td>11/2</td></tr><tr><td>Jon Court</td><td>+4000</td><td>40/1</td></tr><tr><td>Manny Franco</td><td>+2200</td><td>22/1</td></tr><tr><td>Flavien Prat</td><td>+4500</td><td>45/1</td></tr><tr><td>Chris Landeros</td><td>+3300</td><td>33/1</td></tr>            </tbody>
			<tfoot>
		<!-- <tr>
                        <td class="dateUpdated center"  colspan="3" >
                            <em id='updateemp'>  Updated February 3, 2020 </em>
                        </td>
                    </tr> -->
            </tfoot>
        </table>
    </div>
    
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
 

        		
    