			<script type="application/ld+json">
			{
			  "@context": "http://schema.org",
			  "@type": "Table",
			  "about": "2020 Kentucky Oaks Odds"
			}
		</script> 
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  title="Kentucky Oaks Odds"  summary="The latest odds for the Kentucky Oaks available for wagering now at Bet US Racing.">
            <caption>2021 Kentucky Oaks Odds</caption>
			<tfoot>
                    <tr>
                        <td class="dateUpdated center" colspan="3">
                            <em id='updateemp'>  - Updated November 16, 2021 19:00:17 </em>
                        </td>
                    </tr>			
			</tfoot>
            <tbody>
               <!--
 <tr>
                    <th colspan="3" class="center">
                    Horses - Kentucky Oaks  - Apr 30                    </th>
            </tr>
-->
    <tr class='head_title' ><th><!--Team--></th><th>American Odds</th><th>Fractional Odds</th></tr><tr><td>Travel Column</td><td>+325</td><td>13/4</td></tr><tr><td>Malathaat</td><td>+275</td><td>11/4</td></tr><tr><td>Crazy Beautiful</td><td>+650</td><td>13/2</td></tr><tr><td>Clairiere</td><td>+700</td><td>7/1</td></tr><tr><td>Search Results</td><td>+450</td><td>9/2</td></tr><tr><td>Millefeuille</td><td>+1400</td><td>14/1</td></tr><tr><td>Paulines Pearl</td><td>+1400</td><td>14/1</td></tr><tr><td>Wills Secret</td><td>+2500</td><td>25/1</td></tr><tr><td>Pass The Champagne</td><td>+700</td><td>7/1</td></tr><tr><td>Kalypso</td><td>+2800</td><td>28/1</td></tr><tr><td>Moraz</td><td>+3300</td><td>33/1</td></tr><tr><td>Maracuja</td><td>+3000</td><td>30/1</td></tr><tr><td>Coach</td><td>+3500</td><td>35/1</td></tr><tr><td>Competitive Speed</td><td>+3000</td><td>30/1</td></tr><tr><td>Spritz</td><td>+4000</td><td>40/1</td></tr>
            </tbody>

        </table>
    </div>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js" defer></script>
        <script src="//www.usracing.com/assets/js/sorttable.js" defer></script>

    