	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2022 Kentucky Derby Odds"
	  }
	</script>
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable justify-center" width="100%" cellpadding="0" cellspacing="0" border="0" title="2022 Kentucky Derby Odds"  summary="The latest odds for the Kentucky Derby available for wagering now at Bet US Racing.">
            <!--<caption>2016 Kentucky Derby Odds</caption>-->
            <caption>2022 Kentucky Derby Odds</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated November 16th, 2021 19:00:13.</em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
            <!--
    <tr>
                    <th colspan="3" class="center">
                    Horses - Kentucky Derby  - May 07                    </th>
            </tr>
-->
    <tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td data-title=' '>Corniche</td><td data-title='Fractional'>12/1</td><td data-title='American'>+1200</td></tr><tr><td data-title=' '>Jack Christopher</td><td data-title='Fractional'>14/1</td><td data-title='American'>+1400</td></tr><tr><td data-title=' '>Echo Zulu</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>Rattle And Roll</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>Winning Map</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>Pinehurst</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Paooacap</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Tiz The Bomb</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Barossa</td><td data-title='Fractional'>30/1</td><td data-title='American'>+3000</td></tr><tr><td data-title=' '>Commandperformance</td><td data-title='Fractional'>30/1</td><td data-title='American'>+3000</td></tr><tr><td data-title=' '>Authorize</td><td data-title='Fractional'>30/1</td><td data-title='American'>+3000</td></tr>            </tbody>
        </table>
    </div>
        <style>
            table.ordenable th{cursor: pointer}
			table.ordenable td{ word-wrap: break-word !important; word-break: break-all !important;}
			table.ordenable{ max-width: 100% !important; }
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js" defer></script>
        <script src="//www.usracing.com/assets/js/sorttable.js" defer></script>
    