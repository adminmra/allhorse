	<div>
		<table  class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"   summary="The latest odds for the Kentucky Derby available for wagering now at Bet US Racing.">
			<caption>Horses - Kentucky Derby Matchups</caption>
			<tbody>
				<tr>
					<th colspan="3" class="center">
					Horses - Kentucky Derby Matchups  - Sep 05					</th>
			</tr>
				<tr>
					<th colspan="3" class="center">
					Kentucky Derby<br />@ Churchill Downs, Louisville, Ky					</th>
			</tr>
	<tr class='head_title'><th style='text-align:center; ' >Team</th><th style='text-align:center; '>ML</th></tr><tr><td style="background:white">Honor A. P.</td><td style="background:white">-160</td></tr><tr><td style="background:white">Authentic</td><td style="background:white">+135</td></tr><tr><td style="background:#ddd">Sole Volante</td><td style="background:#ddd">+115</td></tr><tr><td style="background:#ddd">Money Moves</td><td style="background:#ddd">-135</td></tr><tr><td style="background:white">Max Player</td><td style="background:white">-190</td></tr><tr><td style="background:white">Enforceable</td><td style="background:white">+160</td></tr><tr><td style="background:#ddd">Necker Island</td><td style="background:#ddd">+200</td></tr><tr><td style="background:#ddd">Attachment Rate</td><td style="background:#ddd">-250</td></tr><tr><td style="background:white">Enforceable</td><td style="background:white">-145</td></tr><tr><td style="background:white">Storm The Court</td><td style="background:white">+125</td></tr><tr><td style="background:#ddd">Attachment Rate</td><td style="background:#ddd">-210</td></tr><tr><td style="background:#ddd">Winning Impression</td><td style="background:#ddd">+175</td></tr><tr><td style="background:white">Max Player</td><td style="background:white">-235</td></tr><tr><td style="background:white">Storm The Court</td><td style="background:white">+190</td></tr><tr><td style="background:#ddd">South Bend</td><td style="background:#ddd">-150</td></tr><tr><td style="background:#ddd">Necker Island</td><td style="background:#ddd">+130</td></tr><tr><td style="background:white">Authentic</td><td style="background:white">+340</td></tr><tr><td style="background:white">Tiz The Law</td><td style="background:white">-440</td></tr><tr><td style="background:#ddd">Ny Traffic</td><td style="background:#ddd">+330</td></tr><tr><td style="background:#ddd">Tiz The Law</td><td style="background:#ddd">-430</td></tr><tr><td style="background:white">Authentic</td><td style="background:white">-125</td></tr><tr><td style="background:white">Ny Traffic</td><td style="background:white">+105</td></tr><tr><td style="background:#ddd">Tiz The Law</td><td style="background:#ddd">-160</td></tr><tr><td style="background:#ddd">Field</td><td style="background:#ddd">+135</td></tr>					<tr>
						<td class="dateUpdated center" colspan="5">
							<em id='updateemp'>  - Updated October 16, 2020 17:33:19 </em>
							Bet US Racing - Official <a href="https://www.usracing.com/kentucky-derby/odds">Kentucky Derby Odds</a>. <br />
							<!--All odds are fixed odds prices.-->
						</td>
					</tr>
			</tbody>
		</table>
	</div>
	