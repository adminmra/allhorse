<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>        
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Avery Island </td>
      <td data-title="Jockey">Joe Bravo </td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Owner">Godolphin</td>
      <td data-title="Time">1:37.58 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Hemsworth</td>
      <td data-title="Jockey">Antonio Gallardo</td>
      <td data-title="Trainer">Thomas Albertrani</td>
      <td data-title="Owner">Godolphin</td>
      <td data-title="Time">1:38.70 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Mohaymen</td>
      <td data-title="Jockey">Junior Alvarado</td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Owner">Shadwell Stable</td>
      <td data-title="Time">1:36.01 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Blofeld</td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Glencrest Farm/JSM Equine </td>
      <td data-title="Time">1:38.89 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Cairo Prince</td>
      <td data-title="Jockey">Luis Saez</td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Owner">Namcook Stables/Braverman/et al </td>
      <td data-title="Time">1:37.59 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Violence</td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Black Rock Stables </td>
      <td data-title="Time">1:35.32 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Vexor</td>
      <td data-title="Jockey">David Cohen</td>
      <td data-title="Trainer">John Kimmel</td>
      <td data-title="Owner">Goldmark Farm </td>
      <td data-title="Time">1:10.71 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">To Honor and Serve</td>
      <td data-title="Jockey">Jose Lezcano</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Owner">Live Oak Plantation </td>
      <td data-title="Time">1:35.86 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Buddy's Saint</td>
      <td data-title="Jockey">Jose Lezcano</td>
      <td data-title="Trainer">Bruce N. Levine</td>
      <td data-title="Owner">Kingfield Stables </td>
      <td data-title="Time">1:35.67 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Break Water Edison</td>
      <td data-title="Jockey">Alan Garcia</td>
      <td data-title="Trainer">John Kimmel</td>
      <td data-title="Owner">Eli Gindi</td>
      <td data-title="Time">1:35.86 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Etched</td>
      <td data-title="Jockey">Alan Garcia</td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Owner">Darley Stable</td>
      <td data-title="Time">1:36.96 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Day Pass</td>
      <td data-title="Jockey">John R. Velazquez</td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Owner">Darley Stable</td>
      <td data-title="Time">1:36.09 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Bluegrass Cat</td>
      <td data-title="Jockey">John R. Velazquez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Owner">WinStar Farm</td>
      <td data-title="Time">1:38.02 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Rockport Harbor</td>
      <td data-title="Jockey">Stewart Elliott</td>
      <td data-title="Trainer">John Servis</td>
      <td data-title="Owner">Fox Hill Farms Inc. </td>
      <td data-title="Time">1:36.60 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Read the Footnotes</td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Richard Violette Jr.</td>
      <td data-title="Owner">Klaravich Stables </td>
      <td data-title="Time">1:36.40 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Added Edge</td>
      <td data-title="Jockey">Patrick Husbands</td>
      <td data-title="Trainer">Mark Casse</td>
      <td data-title="Owner">Team Valor</td>
      <td data-title="Time">1:36.60 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Listen Here </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Owner">Kim Nardelli </td>
      <td data-title="Time">1:37.60 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Ommadon</td>
      <td data-title="Jockey">Aaron Gryder</td>
      <td data-title="Trainer">Thomas M. Walsh</td>
      <td data-title="Owner">Sorin Stables </td>
      <td data-title="Time">1:36.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Mass Market</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Ben W. Perkins Jr.</td>
      <td data-title="Owner">New Farm </td>
      <td data-title="Time">1:38.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Doneraile Court</td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Nicholas P. Zito</td>
      <td data-title="Owner">J. Magnier</td>
      <td data-title="Time">1:36.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Coronado's Quest</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">C. R. McGaughey III</td>
      <td data-title="Owner">Stuart S. Janney III </td>
      <td data-title="Time">1:37.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Jules</td>
      <td data-title="Jockey">Jose Santos</td>
      <td data-title="Trainer">Alan E. Goldberg</td>
      <td data-title="Owner">Jayeff B Stables</td>
      <td data-title="Time">1:36.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner"><em>No Race</em></td>
      <td data-title="Jockey"></td>
      <td data-title="Trainer"></td>
      <td data-title="Owner"></td>
      <td data-title="Time"></td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Devious Course</td>
      <td data-title="Jockey">Frank Alvarado</td>
      <td data-title="Trainer">H. James Bond</td>
      <td data-title="Owner">Rudlein Stable </td>
      <td data-title="Time">1:37.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Popol's Gold</td>
      <td data-title="Jockey">Herb McCauley</td>
      <td data-title="Trainer">William H. Turner Jr.</td>
      <td data-title="Owner">Stephen Stavrides </td>
      <td data-title="Time">1:46.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Dalhart</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Thomas Bohannan</td>
      <td data-title="Owner">Loblolly Stable</td>
      <td data-title="Time">1:44.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Pine Bluff</td>
      <td data-title="Jockey">Craig Perret</td>
      <td data-title="Trainer">Thomas Bohannan</td>
      <td data-title="Owner">Loblolly Stable</td>
      <td data-title="Time">1:46.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Kyle's Our Man</td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">John M. Veitch</td>
      <td data-title="Owner">Darby Dan Farm</td>
      <td data-title="Time">1:45.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Champagneforashley</td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">Howard M. Tesher</td>
      <td data-title="Owner">Lions Head Farm </td>
      <td data-title="Time">1:45.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Traskwood</td>
      <td data-title="Jockey">Angel Cordero Jr.</td>
      <td data-title="Trainer">George R. Arnold II</td>
      <td data-title="Owner">Loblolly Stable</td>
      <td data-title="Time">1:45.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Cougarized</td>
      <td data-title="Jockey">Jose Santos</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Lloyd R. French Jr. </td>
      <td data-title="Time">1:46.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Bold Summit</td>
      <td data-title="Jockey">Chris Antley</td>
      <td data-title="Trainer">Richard E. Dutrow Jr.</td>
      <td data-title="Owner">Alvin J. Akman </td>
      <td data-title="Time">1:45.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Raja's Revenge</td>
      <td data-title="Jockey">Robbie Davis</td>
      <td data-title="Trainer">Frank LaBoccetta</td>
      <td data-title="Owner">Edward Anchel </td>
      <td data-title="Time">1:44.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Stone White</td>
      <td data-title="Jockey">Robbie Davis</td>
      <td data-title="Trainer">Gilbert Puentes</td>
      <td data-title="Owner">Gilbert Puentes</td>
      <td data-title="Time">1:38.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Don Rickles</td>
      <td data-title="Jockey">Angel Cordero Jr.</td>
      <td data-title="Trainer">John Parisella</td>
      <td data-title="Owner">Theodore M. Sabarese </td>
      <td data-title="Time">1:38.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">I Enclose</td>
      <td data-title="Jockey">Ruben Hernandez</td>
      <td data-title="Trainer">Edward I. Kelly</td>
      <td data-title="Owner">Brookfield Farms </td>
      <td data-title="Time">1:37.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Our Escapade</td>
      <td data-title="Jockey">Don MacBeth</td>
      <td data-title="Trainer">Roger Laurin</td>
      <td data-title="Owner">Reginald N. Webster</td>
      <td data-title="Time">1:36.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">A Run</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Larry S. Barrera</td>
      <td data-title="Owner">Arron U. Jones </td>
      <td data-title="Time">1:37.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Googoplex</td>
      <td data-title="Jockey">Laffit Pincay Jr.</td>
      <td data-title="Trainer">Pancho Martin</td>
      <td data-title="Owner">Robert N. Lehmann</td>
      <td data-title="Time">1:36.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Instrument Landing</td>
      <td data-title="Jockey">Jeffrey Fell</td>
      <td data-title="Trainer">David A. Whiteley</td>
      <td data-title="Owner">Pen-Y-Bryn Farm</td>
      <td data-title="Time">1:37.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Quadratic</td>
      <td data-title="Jockey">Eddie Maple</td>
      <td data-title="Trainer">Woody Stephens</td>
      <td data-title="Owner">August Belmont IV</td>
      <td data-title="Time">1:35.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Nearly On Time</td>
      <td data-title="Jockey">Jacinto Vazquez</td>
      <td data-title="Trainer">Leroy Jolley</td>
      <td data-title="Owner">Mrs. Moody Jolley</td>
      <td data-title="Time">1:35.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Lord Henribee</td>
      <td data-title="Jockey">Eddie Maple</td>
      <td data-title="Trainer">William H. Turner Jr.</td>
      <td data-title="Owner">Milton Ritzenberg </td>
      <td data-title="Time">1:35.60 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}