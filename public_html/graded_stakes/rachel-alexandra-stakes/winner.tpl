<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Monomoy Girl</td>
      <td data-title="Jockey">Flont Geroux</td>
      <td data-title="Trainer">Brad H. Cox</td>
      <td data-title="Time">1:43.26 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Farrell </td>
      <td data-title="Jockey">Channing Hill</td>
      <td data-title="Trainer">Wayne Catalano</td>
      <td data-title="Time">1:44.09 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Venus Valentine </td>
      <td data-title="Jockey">Corey J. Lanerie</td>
      <td data-title="Trainer">Thomas M. Amoss</td>
      <td data-title="Time">1:45.24 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">I'm a Chatterbox</td>
      <td data-title="Jockey">Florent Geroux</td>
      <td data-title="Trainer">J. Larry Jones</td>
      <td data-title="Time">1:44.10 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Untapable</td>
      <td data-title="Jockey">Rosie Napravnik</td>
      <td data-title="Trainer">Steven Asmussen</td>
      <td data-title="Time">1:43.64 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Unlimited Budget </td>
      <td data-title="Jockey">Rosie Napravnik</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">1:45.38 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Summer Applause </td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Bret Calhoun</td>
      <td data-title="Time">1:43.30 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Kathmanblu </td>
      <td data-title="Jockey">Julien Leparoux</td>
      <td data-title="Trainer">Kenneth McPeek</td>
      <td data-title="Time">1:45.13 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Jody Slew </td>
      <td data-title="Jockey">Miguel Mena</td>
      <td data-title="Trainer">Bret Calhoun</td>
      <td data-title="Time">1:45.80 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">War Echo </td>
      <td data-title="Jockey">Shaun Bridgmohan</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Time">1:45.20 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Indian Blessing</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:43.75 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Appealing Zophie </td>
      <td data-title="Jockey">John R. Velazquez</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Time">1:44.13 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Baghdaria </td>
      <td data-title="Jockey">Cliff Berry</td>
      <td data-title="Trainer">Bret Calhoun</td>
      <td data-title="Time">1:46.07 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Summerly</td>
      <td data-title="Jockey">Donnie Meche</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Time">1:43.79 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Shadow Cast </td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Neil J. Howard</td>
      <td data-title="Time">1:46.82 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Belle of Perintown </td>
      <td data-title="Jockey">Calvin Borel</td>
      <td data-title="Trainer">Eddie Kenneally</td>
      <td data-title="Time">1:44.48 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Take Charge Lady</td>
      <td data-title="Jockey">Jon Court</td>
      <td data-title="Trainer">Kenneth McPeek</td>
      <td data-title="Time">1:42.09 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Lakenheath </td>
      <td data-title="Jockey">Corey Lanerie</td>
      <td data-title="Trainer">Gene Cilio</td>
      <td data-title="Time">1:46.09 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Shawnee Country </td>
      <td data-title="Jockey">Donnie Meche</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:45.11 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Silverbulletday</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:44.36 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Cool Dixie </td>
      <td data-title="Jockey">Ronald Ardoin</td>
      <td data-title="Trainer">Louie J. Roussel III</td>
      <td data-title="Time">1:43.38 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Blushing K. D.</td>
      <td data-title="Jockey">Lonnie Meche</td>
      <td data-title="Trainer">Sam David Jr.</td>
      <td data-title="Time">1:42.48 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Up Dip </td>
      <td data-title="Jockey">Curt Bourque</td>
      <td data-title="Trainer">Albert Stall Jr.</td>
      <td data-title="Time">1:44.61 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Legendary Priness </td>
      <td data-title="Jockey">Christopher Emigh</td>
      <td data-title="Trainer">Louie Roussel III</td>
      <td data-title="Time">1:44.42 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Playcaller </td>
      <td data-title="Jockey">Ronald Ardoin</td>
      <td data-title="Trainer">Thomas M. Amoss</td>
      <td data-title="Time">1:44.31 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Bright Penny</td>
      <td data-title="Jockey">Ronald Ardoin</td>
      <td data-title="Trainer">William Badgett Jr.</td>
      <td data-title="Time">1:44.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Prospectors Delite</td>
      <td data-title="Jockey">Bobby J. Walker Jr.</td>
      <td data-title="Trainer">Neil J. Howard</td>
      <td data-title="Time">1:43.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Nalees Pin </td>
      <td data-title="Jockey">Kenneth Bourque</td>
      <td data-title="Trainer">Larry Robideaux Jr.</td>
      <td data-title="Time">1:46.50 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Windansea </td>
      <td data-title="Jockey">Randy Romero</td>
      <td data-title="Trainer">Steven L. Morguelan</td>
      <td data-title="Time">1:46.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Exquisite Mistress </td>
      <td data-title="Jockey">Calvin Borel</td>
      <td data-title="Trainer">Joseph E. Broussard</td>
      <td data-title="Time">1:46.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">False Glitter </td>
      <td data-title="Jockey">Shane Romero</td>
      <td data-title="Trainer">Louie Roussel III</td>
      <td data-title="Time">1:47.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Out of the Bid </td>
      <td data-title="Jockey">Kurt Bourque</td>
      <td data-title="Trainer">Grover Delp</td>
      <td data-title="Time">1:47.20 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}