<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/graded-stakes/haskell-stakes/haskell-stakes-betting.jpg" alt="Haskell Stakes  Off Track Betting - OTB" />
    <figure class="rsCaption" data-move-effect="left" data-speed="200">American Pharoah for the win? </figure>
   
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/graded-stakes/haskell-stakes/haskell-stakes-horse-betting.jpg" alt="Haskell Stakes  Horse Betting" />
    <figure class="rsCaption">Sunset at Montmouth</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/graded-stakes/haskell-stakes/bet-on-haskell-stakes.jpg" alt="Bet on Haskell Stakes"  />
    <figure class="rsCaption">The Haskell Invitational Stakes Trophy</figure>
   </div>


   
  </div>
 </div>
