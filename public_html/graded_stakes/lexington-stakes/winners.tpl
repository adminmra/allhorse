<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>        
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2019 </td>
      <td data-title="Winner">Owendale </td>
      <td data-title="Jockey">Florent Geroux</td>
      <td data-title="Trainer">Brad Cox </td>
      <td data-title="Owner">Rupp Racing </td>
      <td data-title="Time">1:44.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">My Boy Jack </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">J. Keith Desormeaux</td>
      <td data-title="Owner">Monomoy Stables LLC </td>
      <td data-title="Time">1:44.22 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Senior Investment </td>
      <td data-title="Jockey">Channing Hill </td>
      <td data-title="Trainer">Kenneth McPeek</td>
      <td data-title="Owner">Fern Circle Stables </td>
      <td data-title="Time">1:45.05 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Collected</td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Speedway Stable </td>
      <td data-title="Time">1:43.33 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Divining Rod</td>
      <td data-title="Jockey">Julien Leparoux</td>
      <td data-title="Trainer">Arnaud Delacour</td>
      <td data-title="Owner">Lael Stables</td>
      <td data-title="Time">1:43.29 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Mr. Speaker </td>
      <td data-title="Jockey">Jose Lezcano</td>
      <td data-title="Trainer">Claude R. McGaughey III</td>
      <td data-title="Owner">Phipps Stable</td>
      <td data-title="Time">1:44.18 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Winning Cause </td>
      <td data-title="Jockey">Julien Leparoux</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Alto Racing </td>
      <td data-title="Time">1:43.93 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">All Squared Away </td>
      <td data-title="Jockey">Julio Garcia</td>
      <td data-title="Trainer">Wesley Ward</td>
      <td data-title="Owner">Altamira Racing Stable </td>
      <td data-title="Time">1:42.55 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Derby Kitten </td>
      <td data-title="Jockey">Julien Leparoux</td>
      <td data-title="Trainer">Mike Maker</td>
      <td data-title="Owner">Ken and Sarah Ramsey</td>
      <td data-title="Time">1:42.03 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Exhi</td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Wertheimer et Frere</td>
      <td data-title="Time">1:44.38 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Advice </td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">WinStar Farm LLC</td>
      <td data-title="Time">1:43.33 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Behindatthebar </td>
      <td data-title="Jockey">David Flores</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Padua Stables. S. K. Sanan, <em>et al.</em></td>
      <td data-title="Time">1:42.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Slew's Tizzy </td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Gregory J. Fox</td>
      <td data-title="Owner">Joseph LaCombe Stable</td>
      <td data-title="Time">1:43.20 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Showing Up</td>
      <td data-title="Jockey">Cornelio Velasquez</td>
      <td data-title="Trainer">Barclay Tagg</td>
      <td data-title="Owner">Lael Stables </td>
      <td data-title="Time">1:46.42 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Coin Silver </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Peachtree Stable </td>
      <td data-title="Time">1:45.76 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Quintons Gold Rush </td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">Padua Stables &amp; Jay Manoogian </td>
      <td data-title="Time">1:43.82 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Scrimshaw </td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Bob &amp; Beverly Lewis</td>
      <td data-title="Time">1:45.47 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Proud Citizen </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">R.Baker/D.Cornstein/W.Mack </td>
      <td data-title="Time">1:44.58 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Keats </td>
      <td data-title="Jockey">Larry Melancon</td>
      <td data-title="Trainer">Niall M. O'Callaghan</td>
      <td data-title="Owner">Henry E. Pabst </td>
      <td data-title="Time">1:43.54 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Unshaded </td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">Carl Nafzger</td>
      <td data-title="Owner">James Tafel, LLC </td>
      <td data-title="Time">1:43.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Charismatic</td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Bob &amp; Beverly Lewis</td>
      <td data-title="Time">1:41.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Classic Cat</td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">David C. Cross Jr.</td>
      <td data-title="Owner">Gary M. Garber </td>
      <td data-title="Time">1:42.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Touch Gold</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">David Hofmans</td>
      <td data-title="Owner">Stonerside Stable</td>
      <td data-title="Time">1:43.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">City by Night </td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">Patrick B. Byrne</td>
      <td data-title="Owner">Nancy &amp; Richard Kaster </td>
      <td data-title="Time">1:42.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Star Standard </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Nicholas P. Zito</td>
      <td data-title="Owner">W. J. Condren &amp; J. Cornacchia </td>
      <td data-title="Time">1:45.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Southern Rhythm </td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">James O. Keefer</td>
      <td data-title="Owner">William Heiligbrodt, <em>et al.</em></td>
      <td data-title="Time">1:45.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Grand Jewel </td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Neil J. Howard</td>
      <td data-title="Owner">W. S. Farish III &amp; W. S. Kilroy </td>
      <td data-title="Time">1:43.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">My Luck Runs North </td>
      <td data-title="Jockey">Ricardo Lopez</td>
      <td data-title="Trainer">Angel M. Medina</td>
      <td data-title="Owner">Melvin A. Benitez </td>
      <td data-title="Time">1:44 .00 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Hansel</td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Frank L. Brothers</td>
      <td data-title="Owner">Lazy Lane Farms </td>
      <td data-title="Time">1:42.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Home at Last </td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Carl Nafzger</td>
      <td data-title="Owner">Russell L. Reineman Stable </td>
      <td data-title="Time">1:43.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Notation </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">James W. Murphy</td>
      <td data-title="Owner">Joan C. Johnson, <em>et al.</em></td>
      <td data-title="Time">1:44.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Risen Star</td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">Louie J. Roussel III</td>
      <td data-title="Owner">Louie Roussel &amp; Ronnie Lamarque </td>
      <td data-title="Time">1:42.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">War </td>
      <td data-title="Jockey">Herb McCauley</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Tom Gentry </td>
      <td data-title="Time">1:44.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Wise Times </td>
      <td data-title="Jockey">Keith Allen</td>
      <td data-title="Trainer">Philip A. Gleaves</td>
      <td data-title="Owner">Russell L. Reineman Stable </td>
      <td data-title="Time">1:44.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Stephan's Odyssey </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Woody Stephens</td>
      <td data-title="Owner">Henryk de Kwiatkowski</td>
      <td data-title="Time">1:42.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">He Is a Great Deal </td>
      <td data-title="Jockey">Julio C. Espinoza</td>
      <td data-title="Trainer">Bernard S. Flint</td>
      <td data-title="Owner">B.Flint &amp; M. Palmisano </td>
      <td data-title="Time">1:45.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Highland Park </td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">Anthony L. Basile</td>
      <td data-title="Owner">Bwamazon Farm &amp; B.Jones</td>
      <td data-title="Time">1:44.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Stage Reviewer </td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Lucien Laurin</td>
      <td data-title="Owner">Mrs. Lucien Laurin</td>
      <td data-title="Time">1:44.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Swinging Light </td>
      <td data-title="Jockey">Dan Delahoussaye</td>
      <td data-title="Trainer">James P. Conway</td>
      <td data-title="Owner">Edgar Zantker </td>
      <td data-title="Time">1:44.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Rockhill Native</td>
      <td data-title="Jockey">John Oldham</td>
      <td data-title="Trainer">Herbert K. Stevens</td>
      <td data-title="Owner">Harry A. Oak </td>
      <td data-title="Time">1:43.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Pianist </td>
      <td data-title="Jockey">Michael Morgan</td>
      <td data-title="Trainer">Peter M. Howe</td>
      <td data-title="Owner">Pillar Farm </td>
      <td data-title="Time">1:43.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Sensitive Prince </td>
      <td data-title="Jockey">Mickey Solomone</td>
      <td data-title="Trainer">H. Allen Jerkens</td>
      <td data-title="Owner">Joseph Taub </td>
      <td data-title="Time">1:44.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Giboulee </td>
      <td data-title="Jockey">Jeffrey Fell</td>
      <td data-title="Trainer">Jacques Dumas</td>
      <td data-title="Owner">Jean-Louis Levesque</td>
      <td data-title="Time">1:42.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">No Link </td>
      <td data-title="Jockey">Darrel McHargue</td>
      <td data-title="Trainer">Douglas Davis, Jr.</td>
      <td data-title="Owner">Fred Selz &amp; Gus Blass </td>
      <td data-title="Time">1:43.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Master Derby</td>
      <td data-title="Jockey">Darrel McHargue</td>
      <td data-title="Trainer">William E. Adams</td>
      <td data-title="Owner">Golden Chance Farm </td>
      <td data-title="Time">1:42.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Satan's Hills </td>
      <td data-title="Jockey">David Whited</td>
      <td data-title="Trainer">Peter W. Salmen, Jr.</td>
      <td data-title="Owner">Crimson King Farm </td>
      <td data-title="Time">1:43.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Our Native</td>
      <td data-title="Jockey">Don Brumfield</td>
      <td data-title="Trainer">William Resseguet, Jr.</td>
      <td data-title="Owner">M.Prichard &amp; W.Resseguet </td>
      <td data-title="Time">1:42.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1938 - 1972</td>
      <td data-title="Winner"><em>No Race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Owner">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>                        
    </tr>
    <tr>
      <td data-title="Year">1937 </td>
      <td data-title="Winner">Co-Sport </td>
      <td data-title="Jockey">Porter Roberts</td>
      <td data-title="Trainer">Robert L. Dotter</td>
      <td data-title="Owner">Bert Friend </td>
      <td data-title="Time">1:12.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1936 </td>
      <td data-title="Winner">White Tie </td>
      <td data-title="Jockey">Joseph Cowley </td>
      <td data-title="Trainer">John M. Gaver, Sr.</td>
      <td data-title="Owner">Manhasset Stable</td>
      <td data-title="Time">1:12.00</td>
    </tr>
  </tbody>
</table>

</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}