<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%"
    cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th width="70px">Age</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="75px">Time</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Year">2018 </td>
        <td data-title="Winner">Take Charge Paula </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Paco Lopez </td>
        <td data-title="Trainer">Kiaran McLaughlin</td>
        <td data-title="Time">1:23.47 </td>
      </tr>
      <tr>
        <td data-title="Year">2017 </td>
        <td data-title="Winner">Tequilita </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Luis Saez</td>
        <td data-title="Trainer">Michael R. Matz</td>
        <td data-title="Time">1:24.36 </td>
      </tr>
      <tr>
        <td data-title="Year">2016 </td>
        <td data-title="Winner">Cathryn Sophia</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">John Servis</td>
        <td data-title="Time">1:22.04 </td>
      </tr>
      <tr>
        <td data-title="Year">2015 </td>
        <td data-title="Winner">Birdatthewire </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Irad Ortiz, Jr.</td>
        <td data-title="Trainer">Dale L. Romans</td>
        <td data-title="Time">1:24.92 </td>
      </tr>
      <tr>
        <td data-title="Year">2014 </td>
        <td data-title="Winner">Onlyforyou</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Javier Castellano</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Time">1:22.50 </td>
      </tr>
      <tr>
        <td data-title="Year">2013 </td>
        <td data-title="Winner">Kauai Katie </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Time">1:22.13 </td>
      </tr>
      <tr>
        <td data-title="Year">2012 </td>
        <td data-title="Winner">Broadway's Alibi </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Time">1:21.94 </td>
      </tr>
      <tr>
        <td data-title="Year">2011 </td>
        <td data-title="Winner">Pomeroys Pistol </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Paco Lopez</td>
        <td data-title="Trainer">Amy Tarrant</td>
        <td data-title="Time">1:22.89 </td>
      </tr>
      <tr>
        <td data-title="Year">2010 </td>
        <td data-title="Winner">Bickersons </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Joe Bravo</td>
        <td data-title="Trainer">Kelly Breen</td>
        <td data-title="Time">1:22.35 </td>
      </tr>
      <tr>
        <td data-title="Year">2009 </td>
        <td data-title="Winner">Frolic's Dream </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Jere Bridgmohan</td>
        <td data-title="Trainer">Martin D. Wolfson</td>
        <td data-title="Time">1:24.15 </td>
      </tr>
      <tr>
        <td data-title="Year">2008 </td>
        <td data-title="Winner">Bsharpsonata </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Eric Camacho</td>
        <td data-title="Trainer">Timothy Saltzman</td>
        <td data-title="Time">1:23.09 </td>
      </tr>
      <tr>
        <td data-title="Year">2007 </td>
        <td data-title="Winner">Forever Together</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Elvis Trujillo</td>
        <td data-title="Trainer">John Sheppard</td>
        <td data-title="Time">1:22.60 </td>
      </tr>
      <tr>
        <td data-title="Year">2006 </td>
        <td data-title="Winner">Miraculous Miss </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Jeremy Rose</td>
        <td data-title="Trainer">Steve Klesaris</td>
        <td data-title="Time">1:22.78 </td>
      </tr>
      <tr>
        <td data-title="Year">2005 </td>
        <td data-title="Winner">Letgomyecho </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Javier Castellano</td>
        <td data-title="Trainer">George R. Arnold II</td>
        <td data-title="Time">1:23.24 </td>
      </tr>
      <tr>
        <td data-title="Year">2004 </td>
        <td data-title="Winner">Madcap Escapade</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Jerry D. Bailey</td>
        <td data-title="Trainer">Frank L. Brothers</td>
        <td data-title="Time">1:22.97 </td>
      </tr>
      <tr>
        <td data-title="Year">2003 </td>
        <td data-title="Winner">Midnight Cry </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Edgar Prado</td>
        <td data-title="Trainer">Kenneth McPeek</td>
        <td data-title="Time">1:22.55 </td>
      </tr>
      <tr>
        <td data-title="Year">2002 </td>
        <td data-title="Winner">Take the Cake </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Rene Douglas</td>
        <td data-title="Trainer">Carl Nafzger</td>
        <td data-title="Time">1:25.47 </td>
      </tr>
      <tr>
        <td data-title="Year">2001 </td>
        <td data-title="Winner">Gold Mover </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Jerry D. Bailey</td>
        <td data-title="Trainer">Mark A. Hennig</td>
        <td data-title="Time">1:22.43 </td>
      </tr>
      <tr>
        <td data-title="Year">2000 </td>
        <td data-title="Winner">Miss Inquistive </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Tommy Turner</td>
        <td data-title="Trainer">Frank Passero, Jr.</td>
        <td data-title="Time">1:22.25 </td>
      </tr>
      <tr>
        <td data-title="Year">1999 </td>
        <td data-title="Winner">China Storm </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Time">1:23.69 </td>
      </tr>
      <tr>
        <td data-title="Year">1998 </td>
        <td data-title="Winner">Uanme </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Shane Sellers</td>
        <td data-title="Trainer">Dallas Stewart</td>
        <td data-title="Time">1:24.56 </td>
      </tr>
      <tr>
        <td data-title="Year">1997 </td>
        <td data-title="Winner">Glitter Woman </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">Shug McGaughey</td>
        <td data-title="Time">1:21.76 </td>
      </tr>
      <tr>
        <td data-title="Year">1996 </td>
        <td data-title="Winner">Mindy Gayle </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Julie Krone</td>
        <td data-title="Trainer">Julian Canet</td>
        <td data-title="Time">1:24.54 </td>
      </tr>
      <tr>
        <td data-title="Year">1995 </td>
        <td data-title="Winner">Chaposa Springs </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Heberto Castillo, Jr.</td>
        <td data-title="Trainer">Martin D. Wolfson</td>
        <td data-title="Time">1:24.18 </td>
      </tr>
      <tr>
        <td data-title="Year">1994 </td>
        <td data-title="Winner">Mynameispanama </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Marco Castaneda</td>
        <td data-title="Trainer">Odin Londono</td>
        <td data-title="Time">1:22.97 </td>
      </tr>
      <tr>
        <td data-title="Year">1993 </td>
        <td data-title="Winner">Sum Runner </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Randy Romero</td>
        <td data-title="Trainer">Donald Winfree</td>
        <td data-title="Time">1:23.67 </td>
      </tr>
      <tr>
        <td data-title="Year">1992 </td>
        <td data-title="Winner">Spinning Round </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Jose A. Santos</td>
        <td data-title="Trainer">James E. Baker</td>
        <td data-title="Time">1:24.85 </td>
      </tr>
      <tr>
        <td data-title="Year">1991 </td>
        <td data-title="Winner">Withallprobability </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Craig Perret</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Time">1:22.50 </td>
      </tr>
      <tr>
        <td data-title="Year">1990 </td>
        <td data-title="Winner">Charon</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Earlie Fires</td>
        <td data-title="Trainer">Gene Navarro</td>
        <td data-title="Time">1:24.80 </td>
      </tr>
      <tr>
        <td data-title="Year">1989 </td>
        <td data-title="Winner">Open Mind</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Angel Cordero, Jr.</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Time">1:24.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1988 </td>
        <td data-title="Winner">On To Royalty </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Craig Perret</td>
        <td data-title="Trainer">Ben W. Perkins, Jr.</td>
        <td data-title="Time">1:23.20 </td>
      </tr>
      <tr>
        <td data-title="Year">1987 </td>
        <td data-title="Winner">Added Elegance </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Jacinto Vasquez</td>
        <td data-title="Trainer">Frank Gomez</td>
        <td data-title="Time">1:24.60 </td>
      </tr>
      <tr>
        <td data-title="Year">1986 </td>
        <td data-title="Winner">Noranc </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Herb McCauley</td>
        <td data-title="Trainer">Joe Provost </td>
        <td data-title="Time">1:23.80 </td>
      </tr>
      <tr>
        <td data-title="Year">1985 </td>
        <td data-title="Winner">Lucy Manette </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Craig Perret</td>
        <td data-title="Trainer">J. Willard Thompson</td>
        <td data-title="Time">1:23.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1984 </td>
        <td data-title="Winner">Miss Oceana</td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Eddie Maple</td>
        <td data-title="Trainer">Woody Stephens</td>
        <td data-title="Time">1:22.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1983 </td>
        <td data-title="Winner">Unaccompanied </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Robert Woodhouse</td>
        <td data-title="Trainer">Dennis Ebert </td>
        <td data-title="Time">1:23.40 </td>
      </tr>
      <tr>
        <td data-title="Year">1982</td>
        <td data-title="Winner">Trove </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Larry Saumell</td>
        <td data-title="Trainer">Woody Stephens</td>
        <td data-title="Time">1:22.80 </td>
      </tr>
      <tr>
        <td data-title="Year">1982</td>
        <td data-title="Winner">All Manners </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Odin Londono</td>
        <td data-title="Trainer">Duke Davis </td>
        <td data-title="Time">1:23.00 </td>
      </tr>
      <tr>
        <td data-title="Year">1981 </td>
        <td data-title="Winner">Dame Mysterieuse </td>
        <td data-title="Age">3 </td>
        <td data-title="Jockey">Jean-Luc Samyn</td>
        <td data-title="Trainer">Woody Stephens</td>
        <td data-title="Time">1:22.20 </td>
      </tr>
    </tbody>
  </table>
</div>
{literal}
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sorttable-winners.js"></script>
{/literal}