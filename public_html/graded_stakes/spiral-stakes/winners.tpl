<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>        
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2019 </td>
      <td data-title="Winner">Somelikeithotbrown </td>
      <td data-title="Jockey">Tyler Gaffalione</td>
      <td data-title="Trainer">Michael J. Maker</td>
      <td data-title="Owner">Skychai Racing LLC </td>
      <td data-title="Time">1:52.05 </td>
    </tr>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Blended Citizen </td>
      <td data-title="Jockey">Kyle Frey </td>
      <td data-title="Trainer">Douglas F. O'Neill</td>
      <td data-title="Owner">G. Hall &amp; Sanjay Racing </td>
      <td data-title="Time">1:50.15 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Fast and Accurate </td>
      <td data-title="Jockey">Tyler Gaffalione</td>
      <td data-title="Trainer">Michael J. Maker</td>
      <td data-title="Owner">Kendall E. Hansen </td>
      <td data-title="Time">1:50:96 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Oscar Nominated</td>
      <td data-title="Jockey">Brian J. Hernandez, Jr.</td>
      <td data-title="Trainer">Michael J. Maker</td>
      <td data-title="Owner">Kenneth and Sarah Ramsey </td>
      <td data-title="Time">1:51.12 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Dubai Sky</td>
      <td data-title="Jockey">Jose Lezcano</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Owner">Three Chimneys Farm and Besilu Stables </td>
      <td data-title="Time">1:50.26 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">We Miss Artie</td>
      <td data-title="Jockey">John R. Velazquez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Ken and Sarah Ramsey </td>
      <td data-title="Time">1:51.98 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Black Onyx</td>
      <td data-title="Jockey">Joe Bravo</td>
      <td data-title="Trainer">Kelly Breen</td>
      <td data-title="Owner">Sterling Racing </td>
      <td data-title="Time">1:51.98 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Went the Day Well</td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">H. Graham Motion</td>
      <td data-title="Owner">Team Valor/Ford </td>
      <td data-title="Time">1:51.33 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Animal Kingdom</td>
      <td data-title="Jockey">Alan Garcia</td>
      <td data-title="Trainer">H. Graham Motion</td>
      <td data-title="Owner">Team Valor</td>
      <td data-title="Time">1:52.32 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Dean's Kitten</td>
      <td data-title="Jockey">Cornelio Velasquez</td>
      <td data-title="Trainer">Michael J. Maker</td>
      <td data-title="Owner">Ken and Sarah Ramsey </td>
      <td data-title="Time">1:50.59 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Hold Me Back</td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Owner">WinStar Farm</td>
      <td data-title="Time">1:49.63 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Adriano</td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">H. Graham Motion</td>
      <td data-title="Owner">Courtlandt Farms </td>
      <td data-title="Time">1:50.20 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Hard Spun</td>
      <td data-title="Jockey">Mario G. Pino</td>
      <td data-title="Trainer">J. Larry Jones</td>
      <td data-title="Owner">Fox Hill Farms Inc. </td>
      <td data-title="Time">1:49.41 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">With A City</td>
      <td data-title="Jockey">Brice Blanc</td>
      <td data-title="Trainer">Michael J. Maker</td>
      <td data-title="Owner">Equirace.com </td>
      <td data-title="Time">1:51.11 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Flower Alley</td>
      <td data-title="Jockey">Jorge Chavez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Owner">Melnyk Racing Stables</td>
      <td data-title="Time">1:50.33 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Sinister G</td>
      <td data-title="Jockey">Paul Toscano</td>
      <td data-title="Trainer">John Toscano, Jr.</td>
      <td data-title="Owner">John T. Toscano III et al. </td>
      <td data-title="Time">1:50.71 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">New York Hero</td>
      <td data-title="Jockey">Norberto Arroyo, Jr.</td>
      <td data-title="Trainer">Jennifer Pedersen</td>
      <td data-title="Owner">Paraneck Stable</td>
      <td data-title="Time">1:50.68 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Perfect Drift</td>
      <td data-title="Jockey">Ed Delahoussaye</td>
      <td data-title="Trainer">Murray W. Johnson</td>
      <td data-title="Owner">Stonecrest Farm </td>
      <td data-title="Time">1:48.83 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Balto Star</td>
      <td data-title="Jockey">Mark Guidry</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Owner">Anstu Stable </td>
      <td data-title="Time">1:47.23 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Globalize</td>
      <td data-title="Jockey">Francisco Torres</td>
      <td data-title="Trainer">Jerry Hollendorfer</td>
      <td data-title="Owner">Howard Litt et al. </td>
      <td data-title="Time">1:49.16 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Stephen Got Even</td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Owner">Stephen Hilbert </td>
      <td data-title="Time">1:49.03 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Event of the Year</td>
      <td data-title="Jockey">Russell Baze</td>
      <td data-title="Trainer">Jerry Hollendorfer</td>
      <td data-title="Owner">Golden Eagle Farm</td>
      <td data-title="Time">1:47.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Concerto</td>
      <td data-title="Jockey">Carlos Marquez, Jr.</td>
      <td data-title="Trainer">John J. Tammaro III</td>
      <td data-title="Owner">Kinsman Stable</td>
      <td data-title="Time">1:48.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Roar</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">C. R. McGaughey III</td>
      <td data-title="Owner">Dilschneider</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Serena's Song</td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Bob &amp; Beverly Lewis</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Polar Expedition</td>
      <td data-title="Jockey">Curt Bourque</td>
      <td data-title="Trainer">Hugh H. Robertson</td>
      <td data-title="Owner">James Cody </td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Prairie Bayou</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Thomas Bohannan</td>
      <td data-title="Owner">Loblolly Stable</td>
      <td data-title="Time">1:50.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Lil E. Tee</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Lynn S. Whiting</td>
      <td data-title="Owner">W. Cal Partee</td>
      <td data-title="Time">1:53.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Hansel</td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Frank L. Brothers</td>
      <td data-title="Owner">Lazy Lane Farm </td>
      <td data-title="Time">1:46.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Summer Squall</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Neil J. Howard</td>
      <td data-title="Owner">Dogwood Stable</td>
      <td data-title="Time">1:49.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Western Playboy </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Harvey L. Vanier</td>
      <td data-title="Owner">Nancy Vanier</td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Kingpost </td>
      <td data-title="Jockey">Eugene Sipus, Jr.</td>
      <td data-title="Trainer">Dianne Carpenter</td>
      <td data-title="Owner">Mark Warner </td>
      <td data-title="Time">1:50.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">J. T.'s Pet </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Lynn S. Whiting</td>
      <td data-title="Owner">W. Cal Partee</td>
      <td data-title="Time">1:42.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Broad Brush</td>
      <td data-title="Jockey">Vince Bracciale, Jr.</td>
      <td data-title="Trainer">Richard W. Small</td>
      <td data-title="Owner">Robert E. Meyerhoff</td>
      <td data-title="Time">1:44.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Banner Bob </td>
      <td data-title="Jockey">Keith Allen</td>
      <td data-title="Trainer">Jerome Sarner, Jr.</td>
      <td data-title="Owner">Sharon &amp; W. J. Walsh </td>
      <td data-title="Time">1:42.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">At The Threshold </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Lynn S. Whiting</td>
      <td data-title="Owner">W. Cal Partee</td>
      <td data-title="Time">1:42.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Marfa </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Beal/French, Jr./Lukas</td>
      <td data-title="Time">1:42.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Good N' Dusty </td>
      <td data-title="Jockey">Michael Moran</td>
      <td data-title="Trainer">Jasper Adams</td>
      <td data-title="Owner">Larry R. Lehman </td>
      <td data-title="Time">1:44.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Mythical Ruler </td>
      <td data-title="Jockey">Kevin Wirth</td>
      <td data-title="Trainer">Fred Wirth</td>
      <td data-title="Owner">Price &amp; Risen </td>
      <td data-title="Time">1:38.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1980</td>
      <td data-title="Winner">Spruce Needles </td>
      <td data-title="Jockey">Mark Sellers</td>
      <td data-title="Trainer">William E. Adams</td>
      <td data-title="Owner">Golden Chance Farm</td>
      <td data-title="Time">1:36.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1980</td>
      <td data-title="Winner">Major Run </td>
      <td data-title="Jockey">Mark Sellers</td>
      <td data-title="Trainer">William E. Adams</td>
      <td data-title="Owner">Golden Chance Farm</td>
      <td data-title="Time">1:37.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Lot O' Gold </td>
      <td data-title="Jockey">Don Brumfield</td>
      <td data-title="Trainer">William E. Adams</td>
      <td data-title="Owner">Frederick E. Lehmann</td>
      <td data-title="Time">1:37.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1978</td>
      <td data-title="Winner">Raymond Earl </td>
      <td data-title="Jockey">Julio Espinoza</td>
      <td data-title="Trainer">William E. Adams</td>
      <td data-title="Owner">Robert N. Lehmann</td>
      <td data-title="Time">1:38.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1978</td>
      <td data-title="Winner">Five Star General </td>
      <td data-title="Jockey">Julio Espinoza</td>
      <td data-title="Trainer">William E. Adams</td>
      <td data-title="Owner">Golden Chance Farm</td>
      <td data-title="Time">1:37.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1977</td>
      <td data-title="Winner">Bob's Dusty </td>
      <td data-title="Jockey">Julio Espinoza</td>
      <td data-title="Trainer">William E. Adams</td>
      <td data-title="Owner">Robert N. Lehmann</td>
      <td data-title="Time">1:38.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1977</td>
      <td data-title="Winner">Smiley's Dream </td>
      <td data-title="Jockey">W. Destefona</td>
      <td data-title="Trainer">William E. Adams</td>
      <td data-title="Owner">Golden Chance Farm</td>
      <td data-title="Time">1:39.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Inca Roca </td>
      <td data-title="Jockey">William Nemeti</td>
      <td data-title="Trainer">A. T. Skinner</td>
      <td data-title="Owner">C. Jarrell </td>
      <td data-title="Time">1:37.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Ambassador's Image </td>
      <td data-title="Jockey">E. Snell</td>
      <td data-title="Trainer">D. Ball</td>
      <td data-title="Owner">Donamire Farm </td>
      <td data-title="Time">1:38.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Naughty Jake </td>
      <td data-title="Jockey">German Vasquez</td>
      <td data-title="Trainer">Jake Bachelor</td>
      <td data-title="Owner">Mildred Bachelor </td>
      <td data-title="Time">1:40.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1974</td>
      <td data-title="Winner">King of Rome </td>
      <td data-title="Jockey">Keith Wirth</td>
      <td data-title="Trainer">Fred Wirth</td>
      <td data-title="Owner">Fred Wirth </td>
      <td data-title="Time">1:45.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1974</td>
      <td data-title="Winner">Aglorite </td>
      <td data-title="Jockey">Keith Wirth</td>
      <td data-title="Trainer">Fred Wirth</td>
      <td data-title="Owner">A.  Risen Jr. </td>
      <td data-title="Time">1:44.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1973</td>
      <td data-title="Winner">Jacks Chevron </td>
      <td data-title="Jockey">Mickey Solomone</td>
      <td data-title="Trainer">C. C. Patrick</td>
      <td data-title="Owner">C. C. Patrick </td>
      <td data-title="Time">1:41.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1973</td>
      <td data-title="Winner">Bootlegger's Pet </td>
      <td data-title="Jockey">Kerry Wirth</td>
      <td data-title="Trainer">Carl Sitgraves</td>
      <td data-title="Owner">W. Hines/C. Sitgraves </td>
      <td data-title="Time">1:42.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Big Dot </td>
      <td data-title="Jockey">Jim Murchison</td>
      <td data-title="Trainer">James Eckrosh</td>
      <td data-title="Owner">E. Lowrence </td>
      <td data-title="Time">1:38.80 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}