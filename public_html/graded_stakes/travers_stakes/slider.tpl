<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/graded-stakes/travers-stakes/travers-stakes-betting.jpg" alt="Travers Stakes  Off Track Betting - OTB" />
    <figure class="rsCaption" data-move-effect="left" data-speed="200">2014 Travers Stakes Winner V.E. Day </figure>
   
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/saratoga/saratoga-offtrack-betting.jpg" alt="Travers Stakes  Horse Betting" />
    <figure class="rsCaption">Sunset at Saratoga</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/saratoga/saratoga-race-course-front.jpg" alt="Bet on Travers Stakes"  />
    <figure class="rsCaption">Saratoga Race Course</figure>
   </div>


   
  </div>
 </div>
