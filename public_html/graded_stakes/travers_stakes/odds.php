<div class="table-responsive">
<table  class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Travers Stakes Odds" >
<tbody>
<tr>
<th >PP</th>
<th >Travers Stakes Odds - Race 11</th>
<th >JOCKEY</th>
<th >TRAINER</th>
<th>ODDS TO WIN</th>
</tr>

<tr>
<td >1</td>
<td >Upstart</td>
<td >I. Ortiz, Jr.</td>
<td >R. Violette, Jr.</td>
<td >15/1</td>
</tr>

<tr>
<td >2</td>
<td >American Pharoah</td>
<td >V. Espinoza</td>
<td >B. Baffert</td>
<td >1/5</td>
</tr>

<tr>
<td >3</td>
<td >Mid Ocean</td>
<td >M. Franco</td>
<td >G. Weaver</td>
<td >50/1</td>
</tr>

<tr>
<td >4</td>
<td >Texas Red</td>
<td >K. Desormeaux</td>
<td >J. Desormeaux</td>
<td >8/1</td>
</tr>

<tr>
<td >5</td>
<td >Frammento</td>
<td >J. Ortiz</td>
<td >N. Zito</td>
<td >30/1</td>
</tr>

<tr>
<td >6</td>
<td >Frosted</td>
<td >J. Rosario</td>
<td >K. McLaughlin</td>
<td >12/1</td>
</tr>

<tr>
<td >7</td>
<td >Keen Ice</td>
<td >J. Castellano</td>
<td >D. Romans</td>
<td >12/1</td>
</tr>

<tr>
<td >8</td>
<td >Tale of Verve</td>
<td >G. Stevens</td>
<td >D. Stewart</td>
<td >30/1</td>
</tr>

<tr>
<td >9</td>
<td >King of New York</td>
<td >J. Leparoux</td>
<td >K. McPeek</td>
<td >50/1</td>
</tr>

<tr>
<td >10</td>
<td >Smart Transition</td>
<td >J. Velazquez</td>
<td >J. Shirreffs</td>
<td >20/1</td>
</tr>


<!--<td >&nbsp;</td>
<td >&nbsp;</td>
<td >&nbsp;</td>
<td >&nbsp;</td>
<td >&nbsp;</td>
</tr>-->

<tr>      <td class="dateUpdated center" colspan="5"><em>Updated August 28, 2015. BUSR - Official <a href="http://www.usracing.com/travers-stakes">Travers Stakes Odds</a>. <br>Bet on American Pharoah to win the Travers or who might upset him in a surprise!  Frosted, anyone? </em></td>
  </tr>
    </tbody>
</table>
</div>
