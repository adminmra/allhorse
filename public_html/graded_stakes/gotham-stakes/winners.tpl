{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
{/literal}
<div id="no-more-tables">
    <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%"
        cellpadding="0" zcellspacing="0" border="0" title="Gotham Stakes Past Winners">
        <thead>
            <tr>
                <th width="69px">Year</th>
                <th>Winner</th>
                <th>Jockey</th>
                <th>Trainer</th>
                <th>Owner</th>
                <th width="75px">Time</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Year">2018</td>
                <td data-title="Winner">Enticed</td>
                <td data-title="Jockey">Junior Alvarado</td>
                <td data-title="Trainer">Kiaran P. McLaughlin</td>
                <td data-title="Owner">Godolphin Racing, LLC</td>
                <td data-title="Time">1:38.24</td>
            </tr>
            <tr>
                <td data-title="Year">2017</td>
                <td data-title="Winner">J Boys Echo</td>
                <td data-title="Jockey">Robby Albarado</td>
                <td data-title="Trainer">Dale L. Romans</td>
                <td data-title="Owner">Albaugh Family Stables LLC</td>
                <td data-title="Time">1:46.34</td>
            </tr>
            <tr>
                <td data-title="Year">2016</td>
                <td data-title="Winner">Shagaf</td>
                <td data-title="Jockey">Irad Ortiz, Jr</td>
                <td data-title="Trainer">Chad C. Brown</td>
                <td data-title="Owner">Shadwell Stable</td>
                <td data-title="Time">1:45.90</td>
            </tr>
            <tr>
                <td data-title="Year">2015</td>
                <td data-title="Winner">El Kabeir</td>
                <td data-title="Jockey">Charles C. Lopez</td>
                <td data-title="Trainer">John P. Terranova, II</td>
                <td data-title="Owner">Zayat Stables</td>
                <td data-title="Time">1:45.56</td>
            </tr>
            <tr>
                <td data-title="Year">2014</td>
                <td data-title="Winner">Samraat</td>
                <td data-title="Jockey">Jose Ortiz</td>
                <td data-title="Trainer">Richard Violette Jr.</td>
                <td data-title="Owner">My Meadowview Farm</td>
                <td data-title="Time">1:44.44</td>
            </tr>
            <tr>
                <td data-title="Year">2013</td>
                <td data-title="Winner">Vyjack</td>
                <td data-title="Jockey">Joel Rosario</td>
                <td data-title="Trainer">Rudy R. Rodriguez</td>
                <td data-title="Owner">Pick Six Racing</td>
                <td data-title="Time">1:44.09</td>
            </tr>
            <tr>
                <td data-title="Year">2012</td>
                <td data-title="Winner">Hansen</td>
                <td data-title="Jockey">Ramon Dominguez</td>
                <td data-title="Trainer">Mike Maker</td>
                <td data-title="Owner">Dr. Kendall Hansen</td>
                <td data-title="Time">1:43.84</td>
            </tr>
            <tr>
                <td data-title="Year">2011</td>
                <td data-title="Winner">Stay Thirsty</td>
                <td data-title="Jockey">Ramon Dominguez</td>
                <td data-title="Trainer">Todd Pletcher</td>
                <td data-title="Owner">Repole Stable</td>
                <td data-title="Time">1:44.78</td>
            </tr>
            <tr>
                <td data-title="Year">2010</td>
                <td data-title="Winner">Awesome Act</td>
                <td data-title="Jockey">Julien Leparoux</td>
                <td data-title="Trainer">Jeremy Noseda</td>
                <td data-title="Owner">Susan Roy/Vinery Stables</td>
                <td data-title="Time">1:43.85</td>
            </tr>
            <tr>
                <td data-title="Year">2009</td>
                <td data-title="Winner">I Want Revenge</td>
                <td data-title="Jockey">Joseph Talamo</td>
                <td data-title="Trainer">Jeff Mullins</td>
                <td data-title="Owner">David J. Lanzman</td>
                <td data-title="Time">1:42.65</td>
            </tr>
            <tr>
                <td data-title="Year">2008</td>
                <td data-title="Winner">Visionaire</td>
                <td data-title="Jockey">Jose Lezcano</td>
                <td data-title="Trainer">Michael Matz</td>
                <td data-title="Owner">Team Valor/Vision Racing</td>
                <td data-title="Time">1:44.60</td>
            </tr>
            <tr>
                <td data-title="Year">2007</td>
                <td data-title="Winner">Cowtown Cat</td>
                <td data-title="Jockey">Ramon Dominguez</td>
                <td data-title="Trainer">Todd A. Pletcher</td>
                <td data-title="Owner">Gulf Coast Farms/WinStar</td>
                <td data-title="Time">1:44.75</td>
            </tr>
            <tr>
                <td data-title="Year">2006</td>
                <td data-title="Winner">Like Now</td>
                <td data-title="Jockey">Fernando Jara</td>
                <td data-title="Trainer">Kiaran McLaughlin</td>
                <td data-title="Owner">John J. Dillon</td>
                <td data-title="Time">1:43.17</td>
            </tr>
            <tr>
                <td data-title="Year">2005</td>
                <td data-title="Winner">Survivalist</td>
                <td data-title="Jockey">Richard Migliore</td>
                <td data-title="Trainer">C. R. McGaughey III</td>
                <td data-title="Owner">Phipps Stable</td>
                <td data-title="Time">1:35.61</td>
            </tr>
            <tr>
                <td data-title="Year">2004</td>
                <td data-title="Winner">Saratoga County</td>
                <td data-title="Jockey">Javier Castellano</td>
                <td data-title="Trainer">George Weaver</td>
                <td data-title="Owner">Evelyn M. Pollard</td>
                <td data-title="Time">1:35.53</td>
            </tr>
            <tr>
                <td data-title="Year">2003</td>
                <td data-title="Winner">Alysweep</td>
                <td data-title="Jockey">Richard Migliore</td>
                <td data-title="Trainer">Patrick L. Reynolds</td>
                <td data-title="Owner">M. Dubb / M. Doneson</td>
                <td data-title="Time">1:40.60</td>
            </tr>
            <tr>
                <td data-title="Year">2002</td>
                <td data-title="Winner">Mayakovsky</td>
                <td data-title="Jockey">Edgar Prado</td>
                <td data-title="Trainer">Patrick Biancone</td>
                <td data-title="Owner">Michael Tabor</td>
                <td data-title="Time">1:34.90</td>
            </tr>
            <tr>
                <td data-title="Year">2001</td>
                <td data-title="Winner">Richly Blended</td>
                <td data-title="Jockey">Rick Wilson</td>
                <td data-title="Trainer">Ben Perkins, Jr.</td>
                <td data-title="Owner">Raymond Dweck</td>
                <td data-title="Time">1:35.14</td>
            </tr>
            <tr>
                <td data-title="Year">2000</td>
                <td data-title="Winner">Red Bullet</td>
                <td data-title="Jockey">Alex Solis</td>
                <td data-title="Trainer">Joseph Orseno</td>
                <td data-title="Owner">Stronach Stables</td>
                <td data-title="Time">1:34.27</td>
            </tr>
            <tr>
                <td data-title="Year">1999</td>
                <td data-title="Winner">Badge</td>
                <td data-title="Jockey">Shaun Bridgmohan</td>
                <td data-title="Trainer">Joseph Aquillino</td>
                <td data-title="Owner">Southbelle Stable</td>
                <td data-title="Time">1:34.72</td>
            </tr>
            <tr>
                <td data-title="Year">1998</td>
                <td data-title="Winner">Wasatch</td>
                <td data-title="Jockey">Jerry D. Bailey</td>
                <td data-title="Trainer">Martin D. Wolfson</td>
                <td data-title="Owner">Frank Calabrese</td>
                <td data-title="Time">1:36.56</td>
            </tr>
            <tr>
                <td data-title="Year">1997</td>
                <td data-title="Winner">Smokin Mel</td>
                <td data-title="Jockey">John Velazquez</td>
                <td data-title="Trainer">John DeStefano, Jr.</td>
                <td data-title="Owner">S. Port / E. Watchel</td>
                <td data-title="Time">1:34.38</td>
            </tr>
            <tr>
                <td data-title="Year">1996</td>
                <td data-title="Winner">Romano Gucci</td>
                <td data-title="Jockey">Julie Krone</td>
                <td data-title="Trainer">Richard E. Dutrow</td>
                <td data-title="Owner">Herbert Kushner</td>
                <td data-title="Time">1:34.40</td>
            </tr>
            <tr>
                <td data-title="Year">1995</td>
                <td data-title="Winner">Talkin Man</td>
                <td data-title="Jockey">Mike E. Smith</td>
                <td data-title="Trainer">Roger Attfield</td>
                <td data-title="Owner">Stollery &amp; Kinghaven</td>
                <td data-title="Time">1:36.82</td>
            </tr>
            <tr>
                <td data-title="Year">1994</td>
                <td data-title="Winner">Irgun</td>
                <td data-title="Jockey">Jerry D. Bailey</td>
                <td data-title="Trainer">Steven W. Young</td>
                <td data-title="Owner">B. &amp; M. Chase</td>
                <td data-title="Time">1:36.27</td>
            </tr>
            <tr>
                <td data-title="Year">1993</td>
                <td data-title="Winner">As Indicated</td>
                <td data-title="Jockey">Caesar Bisono</td>
                <td data-title="Trainer">Richard Schosberg</td>
                <td data-title="Owner">Heatherwood Farm</td>
                <td data-title="Time">1:36.24</td>
            </tr>
            <tr>
                <td data-title="Year">1992</td>
                <td data-title="Winner">Lure</td>
                <td data-title="Jockey">Mike E. Smith</td>
                <td data-title="Trainer">C. R. McGaughey III</td>
                <td data-title="Owner">Claiborne Farm</td>
                <td data-title="Time">1:35.63</td>
            </tr>
            <tr>
                <td data-title="Year">1992</td>
                <td data-title="Winner">Devil His Due</td>
                <td data-title="Jockey">Herb McCauley</td>
                <td data-title="Trainer">H. Allen Jerkens</td>
                <td data-title="Owner">Lion Crest Stable</td>
                <td data-title="Time">1:35.63</td>
            </tr>
            <tr>
                <td data-title="Year">1991</td>
                <td data-title="Winner">Kyle's Our Man</td>
                <td data-title="Jockey">Angel Cordero, Jr.</td>
                <td data-title="Trainer">John M. Veitch</td>
                <td data-title="Owner">Darby Dan Farm</td>
                <td data-title="Time">1:34.69</td>
            </tr>
            <tr>
                <td data-title="Year">1990</td>
                <td data-title="Winner">Thirty Six Red</td>
                <td data-title="Jockey">Mike E. Smith</td>
                <td data-title="Trainer">Nick Zito</td>
                <td data-title="Owner">B. Giles Brophy</td>
                <td data-title="Time">1:33.80</td>
            </tr>
            <tr>
                <td data-title="Year">1989</td>
                <td data-title="Winner">Easy Goer</td>
                <td data-title="Jockey">Pat Day</td>
                <td data-title="Trainer">C. R. McGaughey III</td>
                <td data-title="Owner">Ogden Phipps</td>
                <td data-title="Time">1:32.40</td>
            </tr>
            <tr>
                <td data-title="Year">1988</td>
                <td data-title="Winner">Private Terms</td>
                <td data-title="Jockey">Chris Antley</td>
                <td data-title="Trainer">Charles Hadry</td>
                <td data-title="Owner">Locust Hill Farm</td>
                <td data-title="Time">1:34.80</td>
            </tr>
            <tr>
                <td data-title="Year">1987</td>
                <td data-title="Winner">Gone West</td>
                <td data-title="Jockey">Robbie Davis</td>
                <td data-title="Trainer">Woody Stephens</td>
                <td data-title="Owner">Hickory Tree Stable</td>
                <td data-title="Time">1:34.60</td>
            </tr>
            <tr>
                <td data-title="Year">1986</td>
                <td data-title="Winner">Mogambo</td>
                <td data-title="Jockey">Jacinto Vasquez</td>
                <td data-title="Trainer">Leroy Jolley</td>
                <td data-title="Owner">Peter M. Brant</td>
                <td data-title="Time">1:34.60</td>
            </tr>
            <tr>
                <td data-title="Year">1985</td>
                <td data-title="Winner">Eternal Prince</td>
                <td data-title="Jockey">Richard Migliore</td>
                <td data-title="Trainer">John J. Lenzini, Jr.</td>
                <td data-title="Owner">Brian J. Hurst, Jr.</td>
                <td data-title="Time">1:35.80</td>
            </tr>
            <tr>
                <td data-title="Year">1984</td>
                <td data-title="Winner">Bear Hunt</td>
                <td data-title="Jockey">Don MacBeth</td>
                <td data-title="Trainer">Roger Laurin</td>
                <td data-title="Owner">Taylor Purchase Farm</td>
                <td data-title="Time">1:40.40</td>
            </tr>
            <tr>
                <td data-title="Year">1983</td>
                <td data-title="Winner">Assault Landing</td>
                <td data-title="Jockey">Vince Bracciale, Jr.</td>
                <td data-title="Trainer">Charles Hadry</td>
                <td data-title="Owner">Locust Hill Farm</td>
                <td data-title="Time">1:35.80</td>
            </tr>
            <tr>
                <td data-title="Year">1983</td>
                <td data-title="Winner">Chas Conerly</td>
                <td data-title="Jockey">Jeffrey Fell</td>
                <td data-title="Trainer">Mervin Marks</td>
                <td data-title="Owner">Daniel Lavezzo, Jr.</td>
                <td data-title="Time">1:36.60
                </td>
            </tr>
            <tr>
                <td data-title="Year">1982</td>
                <td data-title="Winner">Air Forbes Won</td>
                <td data-title="Jockey">Michael Venezia</td>
                <td data-title="Trainer">Frank LaBoccetta</td>
                <td data-title="Owner">Edward Anchel</td>
                <td data-title="Time">1:35.60</td>
            </tr>
            <tr>
                <td data-title="Year">1981</td>
                <td data-title="Winner">Proud Appeal</td>
                <td data-title="Jockey">Jeffrey Fell</td>
                <td data-title="Trainer">Stanley M. Hough</td>
                <td data-title="Owner">Malcolm H. Winfield</td>
                <td data-title="Time">1:33.60</td>
            </tr>
            <tr>
                <td data-title="Year">1980</td>
                <td data-title="Winner">Colonel Moran</td>
                <td data-title="Jockey">Jorge Velasquez</td>
                <td data-title="Trainer">Thomas J. Kelly</td>
                <td data-title="Owner">Townsend B. Martin</td>
                <td data-title="Time">1:37.00</td>
            </tr>
            <tr>
                <td data-title="Year">1979</td>
                <td data-title="Winner">General Assembly</td>
                <td data-title="Jockey">Jacinto Vasquez</td>
                <td data-title="Trainer">Leroy Jolley</td>
                <td data-title="Owner">Bertram R. Firestone</td>
                <td data-title="Time">1:43.60</td>
            </tr>
            <tr>
                <td data-title="Year">1978</td>
                <td data-title="Winner">Slap Jack</td>
                <td data-title="Jockey">Jorge Velasquez</td>
                <td data-title="Trainer">Lou M. Goldfine</td>
                <td data-title="Owner">Jerry Frankel</td>
                <td data-title="Time">1:38.60
                </td>
            </tr>
            <tr>
                <td data-title="Year">1977</td>
                <td data-title="Winner">Cormorant</td>
                <td data-title="Jockey">Danny Wright</td>
                <td data-title="Trainer">James P. Simpson</td>
                <td data-title="Owner">Charles Berry, Jr.</td>
                <td data-title="Time">1:43.60</td>
            </tr>
            <tr>
                <td data-title="Year">1976</td>
                <td data-title="Winner">Zen</td>
                <td data-title="Jockey">Jacinto Vasquez</td>
                <td data-title="Trainer">David A. Whiteley</td>
                <td data-title="Owner">Pen-Y-Bryn Farm</td>
                <td data-title="Time">1:35.60</td>
            </tr>
            <tr>
                <td data-title="Year">1975</td>
                <td data-title="Winner">Laramie Trail</td>
                <td data-title="Jockey">Mike Venezia</td>
                <td data-title="Trainer">Jose A. Martin</td>
                <td data-title="Owner">Joseph M. Roebling</td>
                <td data-title="Time">1:38.00</td>
            </tr>
            <tr>
                <td data-title="Year">1975</td>
                <td data-title="Winner">Singh</td>
                <td data-title="Jockey">Angel Cordero, Jr.</td>
                <td data-title="Trainer">John W. Russell</td>
                <td data-title="Owner">Cynthia Phipps</td>
                <td data-title="Time">1:37.00</td>
            </tr>
            <tr>
                <td data-title="Year">1974</td>
                <td data-title="Winner">Stonewalk</td>
                <td data-title="Jockey">Miguel A. Rivera</td>
                <td data-title="Trainer">Daniel Lopez</td>
                <td data-title="Owner">Timberland Stable</td>
                <td data-title="Time">1:36.00</td>
            </tr>
            <tr>
                <td data-title="Year">1974</td>
                <td data-title="Winner">Rube The Great</td>
                <td data-title="Jockey">Miguel A. Rivera</td>
                <td data-title="Trainer">Frank Martin</td>
                <td data-title="Owner">Sigmund Sommer</td>
                <td data-title="Time">1:35.20</td>
            </tr>
            <tr>
                <td data-title="Year">1973</td>
                <td data-title="Winner">Secretariat</td>
                <td data-title="Jockey">Ron Turcotte</td>
                <td data-title="Trainer">Lucien Laurin</td>
                <td data-title="Owner">Meadow Stable</td>
                <td data-title="Time">1:33.40</td>
            </tr>
            <tr>
                <td data-title="Year">1972</td>
                <td data-title="Winner">Freetex</td>
                <td data-title="Jockey">Chuck Baltazar</td>
                <td data-title="Trainer">William T. Raymond</td>
                <td data-title="Owner">Middletown Stable</td>
                <td data-title="Time">1:36.40</td>
            </tr>
            <tr>
                <td data-title="Year">1971</td>
                <td data-title="Winner">Good Behaving</td>
                <td data-title="Jockey">Ron Turcotte</td>
                <td data-title="Trainer">John P. Campo</td>
                <td data-title="Owner">Neil Hellman</td>
                <td data-title="Time">1:36.00</td>
            </tr>
            <tr>
                <td data-title="Year">1970</td>
                <td data-title="Winner">Native Royalty</td>
                <td data-title="Jockey">Angel Cordero, Jr.</td>
                <td data-title="Trainer">John T. Davis</td>
                <td data-title="Owner">Happy Valley Farm</td>
                <td data-title="Time">1:36.20</td>
            </tr>
            <tr>
                <td data-title="Year">1969</td>
                <td data-title="Winner">Dike</td>
                <td data-title="Jockey">Jorge Velasquez</td>
                <td data-title="Trainer">Lucien Laurin</td>
                <td data-title="Owner">Claiborne Farm</td>
                <td data-title="Time">1:34.80</td>
            </tr>
            <tr>
                <td data-title="Year">1968</td>
                <td data-title="Winner">Verbatim</td>
                <td data-title="Jockey">John L. Rotz</td>
                <td data-title="Trainer">James P. Conway</td>
                <td data-title="Owner">Elmendorf Farm</td>
                <td data-title="Time">1:34.00</td>
            </tr>
            <tr>
                <td data-title="Year">1967</td>
                <td data-title="Winner">Dr. Fager</td>
                <td data-title="Jockey">Manuel Ycaza</td>
                <td data-title="Trainer">John A. Nerud</td>
                <td data-title="Owner">artan Stable</td>
                <td data-title="Time">1:35.20</td>
            </tr>
            <tr>
                <td data-title="Year">1966</td>
                <td data-title="Winner">Stupendous</td>
                <td data-title="Jockey">Pete Anderson</td>
                <td data-title="Trainer">Edward A. Neloy</td>
                <td data-title="Owner">Wheatley Stable</td>
                <td data-title="Time">1:34.60</td>
            </tr>
            <tr>
                <td data-title="Year">1965</td>
                <td data-title="Winner">Flag Raiser</td>
                <td data-title="Jockey">Bobby Ussery</td>
                <td data-title="Trainer">Hirsch Jacobs</td>
                <td data-title="Owner">Isidor Bieber</td>
                <td data-title="Time">1:36.60</td>
            </tr>
            <tr>
                <td data-title="Year">1964</td>
                <td data-title="Winner">Mr. Moonlight</td>
                <td data-title="Jockey">Jimmy Combest</td>
                <td data-title="Trainer">Nick Combest</td>
                <td data-title="Owner">Mrs. Magruder Dent</td>
                <td data-title="Time">1:37.20</td>
            </tr>
            <tr>
                <td data-title="Year">1963</td>
                <td data-title="Winner">Debbysman</td>
                <td data-title="Jockey">Larry Adams</td>
                <td data-title="Trainer">Robert Dotter</td>
                <td data-title="Owner">Gustave Ring</td>
                <td data-title="Time">1:34.60</td>
            </tr>
            <tr>
                <td data-title="Year">1962</td>
                <td data-title="Winner">Jaipur</td>
                <td data-title="Jockey">Bill Shoemaker</td>
                <td data-title="Trainer">Bert Mulholland</td>
                <td data-title="Owner">George D. Widener, Jr.</td>
                <td data-title="Time">1:37.00</td>
            </tr>
            <tr>
                <td data-title="Year">1961</td>
                <td data-title="Winner">Ambiopoise</td>
                <td data-title="Jockey">Bobby Ussery</td>
                <td data-title="Trainer">Thomas M. Waller</td>
                <td data-title="Owner">Robert Lehman</td>
                <td data-title="Time">1:35.80</td>
            </tr>
            <tr>
                <td data-title="Year">1960</td>
                <td data-title="Winner">John William</td>
                <td data-title="Jockey">Sam Boulmetis</td>
                <td data-title="Trainer">G. Auerbach</td>
                <td data-title="Owner">Merrick Stable</td>
                <td data-title="Time">1:36.40</td>
            </tr>
            <tr>
                <td data-title="Year">1959</td>
                <td data-title="Winner">Atoll</td>
                <td data-title="Jockey">Eddie Arcaro</td>
                <td data-title="Trainer">Ray Metcalf</td>
                <td data-title="Owner">Elkcam Stable</td>
                <td data-title="Time">1:43.00</td>
            </tr>
            <tr>
                <td data-title="Year">1958</td>
                <td data-title="Winner">Oh Johnny</td>
                <td data-title="Jockey">William Boland</td>
                <td data-title="Trainer">Norman R. McLeod</td>
                <td data-title="Owner">Mrs. Wallace Gilroy</td>
                <td data-title="Time">1:43.60</td>
            </tr>
            <tr>
                <td data-title="Year">1957</td>
                <td data-title="Winner">Mister Jive</td>
                <td data-title="Jockey">Hedley Woodhouse</td>
                <td data-title="Trainer">George M. Carter</td>
                <td data-title="Owner">J. L. Appelbaum</td>
                <td data-title="Time">1:45.60</td>
            </tr>
            <tr>
                <td data-title="Year">1956</td>
                <td data-title="Winner">Career Boy</td>
                <td data-title="Jockey">Eric Guerin</td>
                <td data-title="Trainer">Sylvester Veitch</td>
                <td data-title="Owner">C. V. Whitney</td>
                <td data-title="Time">1:45.60</td>
            </tr>
            <tr>
                <td data-title="Year">1955</td>
                <td data-title="Winner">Go Lightly</td>
                <td data-title="Jockey">Joe Culmone</td>
                <td data-title="Trainer">James McGee</td>
                <td data-title="Owner">Roslyn Farm</td>
                <td data-title="Time">1:46.60</td>
            </tr>
            <tr>
                <td data-title="Year">1954</td>
                <td data-title="Winner">Fisherman</td>
                <td data-title="Jockey">Hedley Woodhouse</td>
                <td data-title="Trainer">Sylvester Veitch</td>
                <td data-title="Owner">C. V. Whitney</td>
                <td data-title="Time">1:46.20
                </td>
            </tr>
            <tr>
                <td data-title="Year">1953</td>
                <td data-title="Winner">Native Dancer</td>
                <td data-title="Jockeyn">Eric Guerin</td>
                <td data-title="Trainer">William C. Winfrey</td>
                <td data-title="Owner">Alfred G. Vanderbilt II</td>
                <td data-title="Time">1:44.20</td>
            </tr>
            <tr>
                <td data-title="Year">1953</td>
                <td data-title="Winner">Laffango</td>
                <td data-title="Jockey">Nick Shuk</td>
                <td data-title="Trainer">Merritt A. Buxton</td>
                <td data-title="Owner">Trio Stable</td>
                <td data-title="Time">1:44.00</td>
            </tr>
        </tbody>
    </table>
</div>

{literal}
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}