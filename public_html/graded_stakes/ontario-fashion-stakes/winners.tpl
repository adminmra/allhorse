<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th width="75px">Time</th>
        </tr>
    </thead>
    <tbody>
<tr>
   <td data-title="Year">2017</td>
   <td data-title="Winner">Moonlit Promise</td>
<td data-title="Time">1:09.06</td>
</tr>
<tr>
   <td data-title="Year">2016</td>
<td data-title="Winner">Southern Ring</td>
<td data-title="Time">1:08.65</td>
</tr>
<tr>
   <td data-title="Year">2015</td>
<td data-title="Winner">Cactus Kris</td>
<td data-title="Time">1:10.06</td>
</tr>
<tr>
   <td data-title="Year">2014</td>
<td data-title="Winner">Native Bombshell</td>
<td data-title="Time">1:09.48</td>
</tr>
<tr>
   <td data-title="Year">2013</td>
<td data-title="Winner">Youcan'tcatchme</td>
<td data-title="Time">1:10.15</td>
</tr>
<tr>
   <td data-title="Year">2012</td>
<td data-title="Winner">Atlantic Hurricane</td>
<td data-title="Time">1:09.57</td>
</tr>
<tr>
   <td data-title="Year">2011</td>
<td data-title="Winner">Atlantic Hurricane</td>
<td data-title="Time">1:09.23</td>
</tr>
<tr>
   <td data-title="Year">2010</td>
<td data-title="Winner">Indian Apple Is</td>
<td data-title="Time">1:09.76</td>
</tr>
<tr>
   <td data-title="Year">2009</td>
<td data-title="Winner">Tribal Belle</td>
<td data-title="Time">1:08.84</td>
</tr>
<tr>
   <td data-title="Year">2008</td>
<td data-title="Winner">Porte Bonheur</td>
<td data-title="Time">1:09.64</td>
</tr>
<tr>
   <td data-title="Year">2007</td>
<td data-title="Winner">Financingavailable</td>
<td data-title="Time">1:08.56</td>
</tr>
<tr>
   <td data-title="Year">2006</td>
<td data-title="Winner">Shot Gun Ela</td>
<td data-title="Time">1:11.79</td>
</tr>
<tr>
   <td data-title="Year">2005</td>
<td data-title="Winner">Colonial Surprise</td>
<td data-title="Time">1:10.89</td>
</tr>
<tr>
   <td data-title="Year">2004</td>
<td data-title="Winner">Winter Garden</td>
<td data-title="Time">1:10.35</td>
</tr>
<tr>
   <td data-title="Year">2003</td>
<td data-title="Winner">Winter Garden</td>
<td data-title="Time">1:10.59</td>
</tr>
<tr>
   <td data-title="Year">2002</td>
<td data-title="Winner">Feather</td>
<td data-title="Time">1:10.82</td>
</tr>
<tr>
   <td data-title="Year">2001</td>
<td data-title="Winner">Feather</td>
<td data-title="Time">1:11.11</td>
</tr>
<tr>
   <td data-title="Year">2000</td>
<td data-title="Winner">Torrid Affair</td>
<td data-title="Time">1:10.82</td>
</tr>
<tr>
   <td data-title="Year">1999</td>
<td data-title="Winner">Appealing Phylly</td>
<td data-title="Time">1:10.94</td>
</tr>
<tr>
   <td data-title="Year">1998</td>
<td data-title="Winner">Bimini Blues</td>
<td data-title="Time">1:10.80</td>
</tr>
<tr>
   <td data-title="Year">1997</td>
<td data-title="Winner">Bimini Blues</td>
<td data-title="Time">1:10.00</td>
</tr>
<tr>
   <td data-title="Year">1996</td>
<td data-title="Winner">Dial A Song</td>
<td data-title="Time">1:12.20</td>
</tr>
<tr>
   <td data-title="Year">1995</td>
<td data-title="Winner">Countess Steffi</td>
<td data-title="Time">1:10.40</td>
</tr>
<tr>
   <td data-title="Year">1994</td>
<td data-title="Winner">Early Blaze</td>
<td data-title="Time">1:10.40</td>
</tr>
<tr>
   <td data-title="Year">1993</td>
<td data-title="Winner">Sing And Swing</td>
<td data-title="Time">1:10.40</td>
</tr>
<tr>
   <td data-title="Year">1992</td>
<td data-title="Winner">Apelia</td>
<td data-title="Time">1:10.00</td>
</tr>
<tr>
   <td data-title="Year">1991</td>
<td data-title="Winner">Illeria</td>
<td data-title="Time">1:11.20</td>
</tr>
<tr>
   <td data-title="Year">1990</td>
<td data-title="Winner">Victorious Trick</td>
<td data-title="Time">1:10.60</td>
</tr>
<tr>
   <td data-title="Year">1989</td>
<td data-title="Winner">Bientot Gold</td>
<td data-title="Time">1:11.00</td>
</tr>
<tr>
   <td data-title="Year">1988</td>
<td data-title="Winner">Zadracarta</td>
<td data-title="Time">1:10.60</td>
</tr>
<tr>
   <td data-title="Year">1987</td>
<td data-title="Winner">Ruling Angel</td>
<td data-title="Time">1:11.20</td>
</tr>
<tr>
   <td data-title="Year">1986</td>
<td data-title="Winner">Futurette</td>
<td data-title="Time">1:10.60</td>
</tr>
<tr>
   <td data-title="Year">1985</td>
<td data-title="Winner">Summer Mood</td>
<td data-title="Time">1:10.80</td>
</tr>
<tr>
   <td data-title="Year">1984</td>
<td data-title="Winner">Summer Mood</td>
<td data-title="Time">1:10.60</td>
</tr>
<tr>
   <td data-title="Year">1983</td>
<td data-title="Winner">Lil Ol Gal</td>
<td data-title="Time">1:13.60</td>
</tr>
<tr>
   <td data-title="Year">1982</td>
<td data-title="Winner">Avowal</td>
<td data-title="Time">1:11.20</td>
</tr>
</tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}