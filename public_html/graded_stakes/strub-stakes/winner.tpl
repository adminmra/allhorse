<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>        
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Shakin It Up</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Cardoza/Pegram </td>
      <td data-title="Time">1:41.86 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Guilt Trip</td>
      <td data-title="Jockey">Joseph Talamo</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Gary &amp; Mary West </td>
      <td data-title="Time">1:48.97 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Ultimate Eagle</td>
      <td data-title="Jockey">Martin Pedroza</td>
      <td data-title="Trainer">Michael Pender</td>
      <td data-title="Owner">B.J. Wright </td>
      <td data-title="Time">1:47.08 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Twirling Candy</td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">John W. Sadler</td>
      <td data-title="Owner">Craig Family Trust</td>
      <td data-title="Time">1:46.53 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Jeranimo</td>
      <td data-title="Jockey">Martin Garcia</td>
      <td data-title="Trainer">Michael Pender</td>
      <td data-title="Owner">B.J. Wright </td>
      <td data-title="Time">1:47.83 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Cowboy Cal</td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Robert and Janice McNair </td>
      <td data-title="Time">1:48.22 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Monterey Jazz</td>
      <td data-title="Jockey">David Flores</td>
      <td data-title="Trainer">Craig Dollase</td>
      <td data-title="Owner">A&amp;R Stables/Class Stable </td>
      <td data-title="Time">1:45.65 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Arson Squad</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Bruce Headley</td>
      <td data-title="Owner">Jay Em Ess Stable </td>
      <td data-title="Time">1:48.65 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">High Limit</td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Owner">Gary &amp; Mary West Stables </td>
      <td data-title="Time">1:49.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Rock Hard Ten</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Richard Mandella</td>
      <td data-title="Owner">M. Paulson</td>
      <td data-title="Time">1:49.24 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Domestic Dispute</td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Paddy Gallagher</td>
      <td data-title="Owner">Bienstock &amp; Winner </td>
      <td data-title="Time">1:49.08 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Medaglia d'Oro</td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Owner">Edmund A. Gann</td>
      <td data-title="Time">1:48.04 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Mizzen Mast</td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Owner">Juddmonte Farms</td>
      <td data-title="Time">1:47.25 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Wooden Phone</td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Durant Helzer &amp; Helzer </td>
      <td data-title="Time">1:48.43 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">General Challenge</td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Golden Eagle Farm</td>
      <td data-title="Time">1:48.81 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Event of the Year</td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Richard Mandella</td>
      <td data-title="Owner">Golden Eagle Farm</td>
      <td data-title="Time">1:47.65 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Silver Charm</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Bob &amp; Beverly Lewis</td>
      <td data-title="Time">1:47.27 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Victory Speech</td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Michael Tabor</td>
      <td data-title="Time">2:01.50 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Helmsman</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Wallace Dollase</td>
      <td data-title="Owner">Horizon Stable, et al. </td>
      <td data-title="Time">2:02.76 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Dare And Go</td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Richard Mandella</td>
      <td data-title="Owner">La Presle Farm</td>
      <td data-title="Time">2:00.15 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Diazo</td>
      <td data-title="Jockey">Laffit Pincay Jr.</td>
      <td data-title="Trainer">Bill Shoemaker</td>
      <td data-title="Owner">Allen E. Paulson</td>
      <td data-title="Time">2:00.33 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Siberian Summer</td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Ronald McAnally</td>
      <td data-title="Owner">Buckram Oak Farm</td>
      <td data-title="Time">2:00.78 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Best Pal</td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Gary F. Jones</td>
      <td data-title="Owner">Golden Eagle Farm</td>
      <td data-title="Time">1:59.95 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Defensive Play</td>
      <td data-title="Jockey">Jose Santos</td>
      <td data-title="Trainer">C. R. McGaughey III</td>
      <td data-title="Owner">Juddmonte Farms</td>
      <td data-title="Time">2:00.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Flying Continental</td>
      <td data-title="Jockey">Corey Black</td>
      <td data-title="Trainer">Jay M. Robbins</td>
      <td data-title="Owner">Jack Kent Cooke</td>
      <td data-title="Time">2:01.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Nasr El Arab</td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Charlie Whittingham</td>
      <td data-title="Owner">Sheikh Mohammed</td>
      <td data-title="Time">2:02.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Alysheba</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Jack Van Berg</td>
      <td data-title="Owner">Dorothy &amp; Pam Scharbauer </td>
      <td data-title="Time">2:00.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Snow Chief</td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Melvin F. Stute</td>
      <td data-title="Owner">Grinstead &amp; Rochelle </td>
      <td data-title="Time">2:00.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Nostalgia's Star</td>
      <td data-title="Jockey">Fernando Toro</td>
      <td data-title="Trainer">Jay M. Robbins</td>
      <td data-title="Owner">Duckett, Hinds &amp; Robbins </td>
      <td data-title="Time">2:03.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Precisionist</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">L. R. Fenstermaker</td>
      <td data-title="Owner">Fred W. Hooper</td>
      <td data-title="Time">2:00.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Desert Wine</td>
      <td data-title="Jockey">Ed Delahoussaye</td>
      <td data-title="Trainer">Jerry M. Fanning</td>
      <td data-title="Owner">Cardiff Stud Farm/T90 Ranch </td>
      <td data-title="Time">2:02.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Swing Till Dawn</td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Charles Marikian</td>
      <td data-title="Owner">Paniolo Ranch, et al. </td>
      <td data-title="Time">2:02.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">It's the One</td>
      <td data-title="Jockey">Walter Guerra</td>
      <td data-title="Trainer">Laz Barrera</td>
      <td data-title="Owner">Amin Saiden </td>
      <td data-title="Time">2:00.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Super Moment</td>
      <td data-title="Jockey">Fernando Toro</td>
      <td data-title="Trainer">Ronald McAnally</td>
      <td data-title="Owner">Elmendorf Farm</td>
      <td data-title="Time">2:01.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Spectacular Bid</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Bud Delp</td>
      <td data-title="Owner">Hawksworth Farm </td>
      <td data-title="Time">1:57.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Affirmed</td>
      <td data-title="Jockey">Laffit Pincay Jr.</td>
      <td data-title="Trainer">Laz Barrera</td>
      <td data-title="Owner">Harbor View Farm</td>
      <td data-title="Time">2:01.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Mr. Redoy</td>
      <td data-title="Jockey">Darrel McHargue</td>
      <td data-title="Trainer">A. Thomas Doyle</td>
      <td data-title="Owner">Felty J. Yoder </td>
      <td data-title="Time">2:01.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Kirby Lane</td>
      <td data-title="Jockey">Sandy Hawley</td>
      <td data-title="Trainer">Laz Barrera</td>
      <td data-title="Owner">Gedney Farms</td>
      <td data-title="Time">2:00.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">George Navonod</td>
      <td data-title="Jockey">Fernando Toro</td>
      <td data-title="Trainer">Gordon Campbell</td>
      <td data-title="Owner">Navonod Stable </td>
      <td data-title="Time">2:12.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Stardust Mel</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Charlie Whittingham</td>
      <td data-title="Owner">Marjorie L. Everett</td>
      <td data-title="Time">2:04.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Ancient Title</td>
      <td data-title="Jockey">Laffit Pincay Jr.</td>
      <td data-title="Trainer">Keith Stucki Sr.</td>
      <td data-title="Owner">Ethel Kirkland </td>
      <td data-title="Time">2:00.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Royal Owl</td>
      <td data-title="Jockey">Johnny Sellers</td>
      <td data-title="Trainer">John G. Canty</td>
      <td data-title="Owner">Royal Oaks Farm/Owl Stable </td>
      <td data-title="Time">2:04.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Unconscious</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">John G. Canty</td>
      <td data-title="Owner">Arthur A. Seeligson Jr.</td>
      <td data-title="Time">2:00.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">War Heim</td>
      <td data-title="Jockey">Johnny Sellers</td>
      <td data-title="Trainer">Dale Landers</td>
      <td data-title="Owner">Hazel Huffman </td>
      <td data-title="Time">2:00.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Snow Sporting</td>
      <td data-title="Jockey">Laffit Pincay Jr.</td>
      <td data-title="Trainer">Warren Stute</td>
      <td data-title="Owner">Clement L. Hirsch</td>
      <td data-title="Time">1:48.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Dignitas † </td>
      <td data-title="Jockey">Fernando Alvarez</td>
      <td data-title="Trainer">James W. Maloney</td>
      <td data-title="Owner">William H. Perry</td>
      <td data-title="Time">2:02.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Most Host</td>
      <td data-title="Jockey">William Harmatz</td>
      <td data-title="Trainer">Gene Cleveland</td>
      <td data-title="Owner">Mmes. Bishop-Robbins </td>
      <td data-title="Time">2:04.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Drin</td>
      <td data-title="Jockey">Laffit Pincay Jr.</td>
      <td data-title="Trainer">Charlie Whittingham</td>
      <td data-title="Owner">Howard B. Keck</td>
      <td data-title="Time">2:02.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Bold Bidder</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Randy Sechrest</td>
      <td data-title="Owner">John R. Gaines (lessee) </td>
      <td data-title="Time">1:59.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Duel</td>
      <td data-title="Jockey">Manuel Ycaza</td>
      <td data-title="Trainer">James W. Maloney</td>
      <td data-title="Owner">Claiborne Farm</td>
      <td data-title="Time">2:00.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Gun Bow</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Edward A. Neloy</td>
      <td data-title="Owner">Gedney Farms</td>
      <td data-title="Time">1:59.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1963 </td>
      <td data-title="Winner">Crimson Satan</td>
      <td data-title="Jockey">Herb Hinojosa</td>
      <td data-title="Trainer">J. W. King</td>
      <td data-title="Owner">Crimson King Farm</td>
      <td data-title="Time">2:00.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">Four-and-Twenty</td>
      <td data-title="Jockey">Johnny Longden</td>
      <td data-title="Trainer">Vance Longden</td>
      <td data-title="Owner">Alberta Ranches, Ltd.</td>
      <td data-title="Time">2:01.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1961 </td>
      <td data-title="Winner">Prove It</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Mesh Tenney</td>
      <td data-title="Owner">Rex C. Ellsworth</td>
      <td data-title="Time">2:01.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1960 </td>
      <td data-title="Winner">First Landing</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Casey Hayes</td>
      <td data-title="Owner">Meadow Stable</td>
      <td data-title="Time">2:00.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1959 </td>
      <td data-title="Winner">Hillsdale</td>
      <td data-title="Jockey">Tommy Barrow</td>
      <td data-title="Trainer">Martin L. Fallon</td>
      <td data-title="Owner">C. W. Smith Enterprises </td>
      <td data-title="Time">2:02.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1958 </td>
      <td data-title="Winner">Round Table</td>
      <td data-title="Jockey">William Harmatz</td>
      <td data-title="Trainer">William Molter</td>
      <td data-title="Owner">Kerr Stable </td>
      <td data-title="Time">2:01.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1957 </td>
      <td data-title="Winner">Spinney</td>
      <td data-title="Jockey">William Harmatz</td>
      <td data-title="Trainer">Reggie Cornell</td>
      <td data-title="Owner">Louis R. Rowan</td>
      <td data-title="Time">2:04.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1956 </td>
      <td data-title="Winner">Trackmaster</td>
      <td data-title="Jockey">Ralph Neves</td>
      <td data-title="Trainer">Ted Saladin</td>
      <td data-title="Owner">M/M Hal Seley</td>
      <td data-title="Time">2:04.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1955 </td>
      <td data-title="Winner">Determine † </td>
      <td data-title="Jockey">Raymond York</td>
      <td data-title="Trainer">William Molter</td>
      <td data-title="Owner">Andrew J. Crevolin</td>
      <td data-title="Time">2:00.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1954 </td>
      <td data-title="Winner">Apple Valley</td>
      <td data-title="Jockey">Merlin Volzke</td>
      <td data-title="Trainer">Robert H. McDaniel</td>
      <td data-title="Owner">Mrs. A. W. Ryan </td>
      <td data-title="Time">2:08.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1953 </td>
      <td data-title="Winner">Mark-Ye-Well</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Horace A. Jones</td>
      <td data-title="Owner">Calumet Farm</td>
      <td data-title="Time">2:03.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1952 </td>
      <td data-title="Winner">Intent</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">William J. Hirsch</td>
      <td data-title="Owner">Brookfield Farm</td>
      <td data-title="Time">2:02.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1951 </td>
      <td data-title="Winner">Great Circle</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Warren Stute</td>
      <td data-title="Owner">Yolo Stable </td>
      <td data-title="Time">2:00.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1950 </td>
      <td data-title="Winner">Ponder</td>
      <td data-title="Jockey">Steve Brooks</td>
      <td data-title="Trainer">Horace A. Jones</td>
      <td data-title="Owner">Calumet Farm</td>
      <td data-title="Time">2:02.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1949 </td>
      <td data-title="Winner">Ace Admiral</td>
      <td data-title="Jockey">John Gilbert</td>
      <td data-title="Trainer">William Molter</td>
      <td data-title="Owner">Maine Chance Farm</td>
      <td data-title="Time">2:02.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1948 </td>
      <td data-title="Winner">Flashco</td>
      <td data-title="Jockey">Jack Westrope</td>
      <td data-title="Trainer">George Reeves</td>
      <td data-title="Owner">F. Frankel </td>
      <td data-title="Time">2:03.20 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}