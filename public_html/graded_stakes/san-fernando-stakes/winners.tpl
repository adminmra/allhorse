<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>        
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Fed Biz</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Kaleem Shah, Inc </td>
      <td data-title="Time">1:42.34 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Tapizar</td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">Winchell Thoroughbreds, LLC </td>
      <td data-title="Time">1:41.98 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Indian Firewater</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Karl Watson, M. Pegram, Paul Weitman </td>
      <td data-title="Time">1:41.53 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Papa Clem</td>
      <td data-title="Jockey">Tyler Baze</td>
      <td data-title="Trainer">Gary Stute</td>
      <td data-title="Owner">Bo Hirsch </td>
      <td data-title="Time">1:42.64 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Nownownow</td>
      <td data-title="Jockey">Joe Talamo</td>
      <td data-title="Trainer">Patrick L. Biancone</td>
      <td data-title="Owner">Fab Oak Stable </td>
      <td data-title="Time">1:41.45 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Air Command</td>
      <td data-title="Jockey">Aaron Gryder</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Faisal Salm</td>
      <td data-title="Time">1:40.16 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Awesome Gem</td>
      <td data-title="Jockey">Tyler Baze</td>
      <td data-title="Trainer">Craig Dollase</td>
      <td data-title="Owner">West Point Thoroughbreds</td>
      <td data-title="Time">1:41.90 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Unbridled Energy</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Richard E. Mandella</td>
      <td data-title="Owner">Doug Cauthen </td>
      <td data-title="Time">1:44.33 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Minister Eric</td>
      <td data-title="Jockey">Rene Douglas</td>
      <td data-title="Trainer">Richard E. Mandella</td>
      <td data-title="Owner">Diamond A Racing </td>
      <td data-title="Time">1:42.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">During</td>
      <td data-title="Jockey">David Flores</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">James McIngvale</td>
      <td data-title="Time">1:41.63 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Pass Rush</td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Patrick B. Byrne</td>
      <td data-title="Owner">Michael Tabor</td>
      <td data-title="Time">1:42.97 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Western Pride</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">James K. Chapman</td>
      <td data-title="Owner">Chapman &amp; McArthur </td>
      <td data-title="Time">1:41.30 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Tiznow</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Jay M. Robbins</td>
      <td data-title="Owner">Michael Cooper, et al. </td>
      <td data-title="Time">1:42.05 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Saint's Honor</td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Craig Dollase</td>
      <td data-title="Owner">Stephan G. Herold </td>
      <td data-title="Time">1:41.94 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Dixie Dot Com</td>
      <td data-title="Jockey">David Flores</td>
      <td data-title="Trainer">William Morey, Jr.</td>
      <td data-title="Owner">Chaiken, Chaiken &amp; Heller </td>
      <td data-title="Time">1:41.06 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Silver Charm</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Bob &amp; Beverly Lewis</td>
      <td data-title="Time">1:41.94 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Northern Afleet</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">David E. Hofmans</td>
      <td data-title="Owner">Anderson &amp; Waranch </td>
      <td data-title="Time">1:48.59 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Helmsman</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Wallace Dollase</td>
      <td data-title="Owner">Horizon Stable, Jarvis </td>
      <td data-title="Time">1:48.87 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Wekiva Springs</td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Robert B. Hess, Jr.</td>
      <td data-title="Owner">Donald R. Dizney &amp; English </td>
      <td data-title="Time">1:48.59 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Zignew</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Daniel Smithwick</td>
      <td data-title="Owner">Jack Kent Cooke</td>
      <td data-title="Time">1:47.87 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Bertrando</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Owner">Marshall Nahem</td>
      <td data-title="Time">1:51.22 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Best Pal</td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Gary F. Jones</td>
      <td data-title="Owner">Golden Eagle Farm</td>
      <td data-title="Time">1:48.25 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">In Excess</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Bruce L. Jackson</td>
      <td data-title="Owner">Jack J. Munari</td>
      <td data-title="Time">1:46.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Flying Continental</td>
      <td data-title="Jockey">Corey Black</td>
      <td data-title="Trainer">Jay M. Robbins</td>
      <td data-title="Owner">Jack Kent Cooke</td>
      <td data-title="Time">1:47.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Mi Preferido</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Laz Barrera</td>
      <td data-title="Owner">Barrera &amp; Saiden </td>
      <td data-title="Time">1:47.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">On the Line</td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Eugene V. Klein</td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Variety Road</td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Bruce Headley</td>
      <td data-title="Owner">Kjell H. Qvale</td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Right Con</td>
      <td data-title="Jockey">Rafael Meza</td>
      <td data-title="Trainer">Melvin F. Stute</td>
      <td data-title="Owner">William R. Hawn</td>
      <td data-title="Time">1:48.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Precisionist</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Ross Fenstermaker</td>
      <td data-title="Owner">Fred W. Hooper</td>
      <td data-title="Time">1:47.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Interco</td>
      <td data-title="Jockey">Patrick Valenzuela</td>
      <td data-title="Trainer">Ted West</td>
      <td data-title="Owner">David I. Sofro </td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Wavering Monarch</td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">Laz Barrera</td>
      <td data-title="Owner">Glencrest Farm</td>
      <td data-title="Time">1:50.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">It's the One</td>
      <td data-title="Jockey">Walter Guerra</td>
      <td data-title="Trainer">Laz Barrera</td>
      <td data-title="Owner">Amin Saiden </td>
      <td data-title="Time">1:47.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Doonesbury</td>
      <td data-title="Jockey">Sandy Hawley</td>
      <td data-title="Trainer">Barney Willis</td>
      <td data-title="Owner">Jones, Roffe, et al. </td>
      <td data-title="Time">1:47.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Spectacular Bid</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Bud Delp</td>
      <td data-title="Owner">Hawksworth Farm</td>
      <td data-title="Time">1:48.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Radar Ahead</td>
      <td data-title="Jockey">Darrel McHargue</td>
      <td data-title="Trainer">Gary F. Jones</td>
      <td data-title="Owner">Sidney H. Vail </td>
      <td data-title="Time">1:48.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Text</td>
      <td data-title="Jockey">Fernando Toro</td>
      <td data-title="Trainer">Vincent Clyne</td>
      <td data-title="Owner">Elmendorf Farm</td>
      <td data-title="Time">1:49.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Kirby Lane</td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Laz Barrera</td>
      <td data-title="Owner">Gedney Farms</td>
      <td data-title="Time">1:47.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Pocket Park</td>
      <td data-title="Jockey">Steve Cauthen</td>
      <td data-title="Trainer">Ron McAnally</td>
      <td data-title="Owner">Elmendorf Farm</td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Messenger of Song</td>
      <td data-title="Jockey">Jerry Lambert</td>
      <td data-title="Trainer">Gordon C. Campbell</td>
      <td data-title="Owner">Bernard J. Ridder</td>
      <td data-title="Time">1:48.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Stardust Mel</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Charlie Whittingham</td>
      <td data-title="Owner">Marjorie L. Everett</td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">First Back</td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">Ron McAnally</td>
      <td data-title="Owner">Post Time Stable </td>
      <td data-title="Time">1:46.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Ancient Title</td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Keith L. Stucki, Sr.</td>
      <td data-title="Owner">Ethel Kirkland</td>
      <td data-title="Time">1:47.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Bicker</td>
      <td data-title="Jockey">Glenn Brogan</td>
      <td data-title="Trainer">Robert Wingfield</td>
      <td data-title="Owner">Green Thumb Farm Stable </td>
      <td data-title="Time">1:48.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Autobiography (DH) </td>
      <td data-title="Jockey">Eddie Belmonte</td>
      <td data-title="Trainer">Pancho Martin</td>
      <td data-title="Owner">Sigmund Sommer</td>
      <td data-title="Time">1:47.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Triple Bend (DH) </td>
      <td data-title="Jockey">Donald Pierce</td>
      <td data-title="Trainer">Vance Longden</td>
      <td data-title="Owner">Frank M. McMahon</td>
      <td data-title="Time">1:47.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Willowick</td>
      <td data-title="Jockey">Eddie Belmonte</td>
      <td data-title="Trainer">Joseph Manzi</td>
      <td data-title="Owner">Robert E. Hibbert</td>
      <td data-title="Time">1:48.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey"></td>
      <td data-title="Trainer"></td>
      <td data-title="Owner"></td>
      <td data-title="Time"></td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Cavamore</td>
      <td data-title="Jockey">Eddie Belmonte</td>
      <td data-title="Trainer">Buster Millerick</td>
      <td data-title="Owner">Mrs. M. Troy Jones </td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Damascus</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Frank Whiteley, Jr.</td>
      <td data-title="Owner">Edith W. Bancroft</td>
      <td data-title="Time">1:48.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Buckpasser</td>
      <td data-title="Jockey">Braulio Baeza</td>
      <td data-title="Trainer">Edward A. Neloy</td>
      <td data-title="Owner">Ogden Phipps</td>
      <td data-title="Time">1:48.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Isle of Greece</td>
      <td data-title="Jockey">Walter Blum</td>
      <td data-title="Trainer">Hirsch Jacobs</td>
      <td data-title="Owner">Ethel D. Jacobs</td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Hill Rise</td>
      <td data-title="Jockey">Donald Pierce</td>
      <td data-title="Trainer">William B. Finnegan</td>
      <td data-title="Owner">El Peco Ranch</td>
      <td data-title="Time">1:48.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Nevada Battler</td>
      <td data-title="Jockey">Manuel Ycaza</td>
      <td data-title="Trainer">George D. Adams</td>
      <td data-title="Owner">J. Kel Houssels</td>
      <td data-title="Time">1:48.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Gun Bow</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Edward A. Neloy</td>
      <td data-title="Owner">Gedney Farms</td>
      <td data-title="Time">1:47.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1963 </td>
      <td data-title="Winner">Crimson Satan</td>
      <td data-title="Jockey">Herb Hinojosa</td>
      <td data-title="Trainer">Charles Kerr</td>
      <td data-title="Owner">Crimson King Farm </td>
      <td data-title="Time">1:47.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">Four-and-Twenty</td>
      <td data-title="Jockey">Johnny Longden</td>
      <td data-title="Trainer">Vance Longden</td>
      <td data-title="Owner">Alberta Ranches, Ltd.</td>
      <td data-title="Time">1:48.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1961 </td>
      <td data-title="Winner">Prove It</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Mesh Tenney</td>
      <td data-title="Owner">Rex C. Ellsworth</td>
      <td data-title="Time">1:47.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1960 </td>
      <td data-title="Winner">King O'Turf</td>
      <td data-title="Jockey">Angel Valenzuela</td>
      <td data-title="Trainer">Ron McAnally</td>
      <td data-title="Owner">M/M J. Ross Clark II </td>
      <td data-title="Time">1:50.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1959 </td>
      <td data-title="Winner">Hillsdale</td>
      <td data-title="Jockey">Tommy Barrow</td>
      <td data-title="Trainer">Martin L. Fallon, Jr.</td>
      <td data-title="Owner">Clarence Whitted Smith</td>
      <td data-title="Time">1:42.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1958 </td>
      <td data-title="Winner">Round Table</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">William Molter</td>
      <td data-title="Owner">Kerr Stables</td>
      <td data-title="Time">1:42.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1957 </td>
      <td data-title="Winner">Holandes II</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Bob R. Roberts</td>
      <td data-title="Owner">Wood, De Benedetti, Roberts </td>
      <td data-title="Time">1:43.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1956 </td>
      <td data-title="Winner">Beau Busher</td>
      <td data-title="Jockey">Jack Westrope</td>
      <td data-title="Trainer">Dale Landers</td>
      <td data-title="Owner">Sunnyside Stable </td>
      <td data-title="Time">1:43.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1955 </td>
      <td data-title="Winner">Poona II</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Red McDaniel</td>
      <td data-title="Owner">Helbush Farms </td>
      <td data-title="Time">1:40.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1954 </td>
      <td data-title="Winner">By Zeus</td>
      <td data-title="Jockey">Jack Westrope</td>
      <td data-title="Trainer">William J. Hirsch</td>
      <td data-title="Owner">Cynthia Lasker</td>
      <td data-title="Time">1:49.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1953 </td>
      <td data-title="Winner">Mark-Ye-Well</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Horace A. Jones</td>
      <td data-title="Owner">Calumet Farm</td>
      <td data-title="Time">1:44.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1952 </td>
      <td data-title="Winner">Counterpoint</td>
      <td data-title="Jockey">Dave Gorman</td>
      <td data-title="Trainer">Sylvester Veitch</td>
      <td data-title="Owner">C. V. Whitney</td>
      <td data-title="Time">1:45.20</td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}