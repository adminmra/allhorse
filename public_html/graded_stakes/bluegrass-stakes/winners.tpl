{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
{/literal}
<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Blue Grass Stakes Past Winners">
    <thead>
      <tr>
        <th width="48px"><strong>Year</strong></th>
        <th width="135px"><strong>Winner</strong></th>
        <th width="140px"><strong>Jockey</strong></th>
        <th width="150px"><strong>Trainer</strong></th>
        <th width="170px"><strong>Owner</strong></th>
        <th width="70px"><strong>Time</strong></th>
        </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Year">2013</td>
        <td data-title="Winner">Java's War</td>
        <td data-title="Jockey">Julien Leparoux</td>
        <td data-title="Trainer">Kenneth G. McPeek</td>
        <td data-title="Owner">Charles E. Fipke</td>
        <td data-title="Time">1:50.27</td>
        </tr>
      <tr>
        <td data-title="Year">2012</td>
        <td data-title="Winner">Dullahan</td>
        <td data-title="Jockey">Kent Desormeaux</td>
        <td data-title="Trainer">Dale Romans</td>
        <td data-title="Owner">Donegal Racing</td>
        <td data-title="Time">1:47.94</td>
        </tr>
      <tr>
        <td data-title="Year">2011</td>
        <td data-title="Winner">Brilliant Speed</td>
        <td data-title="Jockey">Joel Rosario</td>
        <td data-title="Trainer">Thomas Albertrani</td>
        <td data-title="Owner">Live Oak Plantation</td>
        <td data-title="Time">1:50.92</td>
        </tr>
      <tr>
        <td data-title="Year">2010</td>
        <td data-title="Winner">Stately Victor</td>
        <td data-title="Jockey">Alan Garcia</td>
        <td data-title="Trainer">Michael J. Maker</td>
        <td data-title="Owner">F. Thomas &amp; Jack Conway</td>
        <td data-title="Time">1:48.69</td>
        </tr>
      <tr>
        <td data-title="Year">2009</td>
        <td data-title="Winner">General Quarters</td>
        <td data-title="Jockey">Eibar Coa</td>
        <td data-title="Trainer">Thomas R. McCarthy</td>
        <td data-title="Owner">Thomas R. McCarthy</td>
        <td data-title="Time">1:49.26</td>
        </tr>
      <tr>
        <td data-title="Year">2008</td>
        <td data-title="Winner">Monba</td>
        <td data-title="Jockey">Edgar Prado</td>
        <td data-title="Trainer">Todd A. Pletcher</td>
        <td data-title="Owner">Starlight Stables et al.</td>
        <td data-title="Time">1:49.71</td>
        </tr>
      <tr>
        <td data-title="Year">2007</td>
        <td data-title="Winner">Dominican</td>
        <td data-title="Jockey">Rafael Bejarano</td>
        <td data-title="Trainer">Darrin Miller</td>
        <td data-title="Owner">Silverton Hill LLC</td>
        <td data-title="Time">1:51.33</td>
        </tr>
      <tr>
        <td data-title="Year">2006</td>
        <td data-title="Winner">Sinister Minister</td>
        <td data-title="Jockey">Garrett Gomez</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Mercedes Stable et al.</td>
        <td data-title="Time">1:48.85</td>
        </tr>
      <tr>
        <td data-title="Year">2005</td>
        <td data-title="Winner">Bandini</td>
        <td data-title="Jockey">John R. Velazquez</td>
        <td data-title="Trainer">Todd A. Pletcher</td>
        <td data-title="Owner">Michael Tabor</td>
        <td data-title="Time">1:48.85</td>
        </tr>
      <tr>
        <td data-title="Year">2004</td>
        <td data-title="Winner">The Cliff's Edge</td>
        <td data-title="Jockey">Shane Sellers</td>
        <td data-title="Trainer">Nick Zito</td>
        <td data-title="Owner">Robert V. LaPenta</td>
        <td data-title="Time">1:49.42</td>
        </tr>
      <tr>
        <td data-title="Year">2003</td>
        <td data-title="Winner">Peace Rules</td>
        <td data-title="Jockey">Edgar Prado</td>
        <td data-title="Trainer">Robert J. Frankel</td>
        <td data-title="Owner">Edmund A. Gann</td>
        <td data-title="Time">1:51.73</td>
        </tr>
      <tr>
        <td data-title="Year">2002</td>
        <td data-title="Winner">Harlan's Holiday</td>
        <td data-title="Jockey">Edgar Prado</td>
        <td data-title="Trainer">Ken McPeek</td>
        <td data-title="Owner">Starlight Stable</td>
        <td data-title="Time">1:51.51</td>
        </tr>
      <tr>
        <td data-title="Year">2001</td>
        <td data-title="Winner">Millennium Wind</td>
        <td data-title="Jockey">Laffit Pincay, Jr.</td>
        <td data-title="Trainer">David Hofmans</td>
        <td data-title="Owner">D &amp; J Heerensperger</td>
        <td data-title="Time">1:48.32</td>
        </tr>
      <tr>
        <td data-title="Year">2000</td>
        <td data-title="Winner">High Yield</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Tabor/Lewis/Magnier</td>
        <td data-title="Time">1:48.60</td>
        </tr>
      <tr>
        <td data-title="Year">1999</td>
        <td data-title="Winner">Menifee</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">W. Elliott Walden</td>
        <td data-title="Owner">Hancock / Stone</td>
        <td data-title="Time">1:48.60</td>
        </tr>
      <tr>
        <td data-title="Year">1998</td>
        <td data-title="Winner">Halory Hunter</td>
        <td data-title="Jockey">Gary Stevens</td>
        <td data-title="Trainer">Nick Zito</td>
        <td data-title="Owner">Celtic Pride Stable</td>
        <td data-title="Time">1:47.80</td>
        </tr>
      <tr>
        <td data-title="Year">1997</td>
        <td data-title="Winner">Pulpit</td>
        <td data-title="Jockey">Shane Sellers</td>
        <td data-title="Trainer">Frank L. Brothers</td>
        <td data-title="Owner">Claiborne Farm</td>
        <td data-title="Time">1:49.80</td>
        </tr>
      <tr>
        <td data-title="Year">1996</td>
        <td data-title="Winner">Skip Away</td>
        <td data-title="Jockey">Shane Sellers</td>
        <td data-title="Trainer">Sonny Hine</td>
        <td data-title="Owner">Carolyn Hine</td>
        <td data-title="Time">1:47.20</td>
        </tr>
      <tr>
        <td data-title="Year">1995</td>
        <td data-title="Winner">Wild Syn</td>
        <td data-title="Jockey">Randy Romero</td>
        <td data-title="Trainer">Thomas K. Arnemann</td>
        <td data-title="Owner">Jurgen K. Arnemann</td>
        <td data-title="Time">1:49.20</td>
        </tr>
      <tr>
        <td data-title="Year">1994</td>
        <td data-title="Winner">Holy Bull</td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">Warren A. Croll, Jr.</td>
        <td data-title="Owner">Warren A. Croll, Jr.</td>
        <td data-title="Time">1:50.00</td>
        </tr>
      <tr>
        <td data-title="Year">1993</td>
        <td data-title="Winner">Prairie Bayou</td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">Tom Bohannan</td>
        <td data-title="Owner">Loblolly Stable</td>
        <td data-title="Time">1:49.60</td>
        </tr>
      <tr>
        <td data-title="Year">1992</td>
        <td data-title="Winner">Pistols and Roses</td>
        <td data-title="Jockey">Jacinto Vasquez</td>
        <td data-title="Trainer">George Gianos</td>
        <td data-title="Owner">Willis Family Stables</td>
        <td data-title="Time">1:49.00</td>
        </tr>
      <tr>
        <td data-title="Year">1991</td>
        <td data-title="Winner">Strike the Gold</td>
        <td data-title="Jockey">Chris Antley</td>
        <td data-title="Trainer">Nick Zito</td>
        <td data-title="Owner">BCC Stable</td>
        <td data-title="Time">1:48.40</td>
        </tr>
      <tr>
        <td data-title="Year">1990</td>
        <td data-title="Winner">Summer Squall</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">Neil J. Howard</td>
        <td data-title="Owner">Dogwood Stable</td>
        <td data-title="Time">1:48.60</td>
        </tr>
    </tbody>
  </table>
</div>
{literal}
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}