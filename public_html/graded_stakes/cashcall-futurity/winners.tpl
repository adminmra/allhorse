{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
{/literal}
<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%"
    cellpadding="0" zcellspacing="0" border="0" title="Florida Derby Past Winners">
    <thead>
      <tr>
        <th width="69px">Year</th>
        <th width="135px">Winner</th>
        <th width="140px">Jockey</th>
        <th width="150px">Trainer</th>
        <th>Owner</th>
        <th width="75px">Time</th>
      </tr>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Year">2018</td>
        <td data-title="Winner">Improbable</td>
        <td data-title="Jockey">Drayden VanDyke</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">WinStar Farm LLC, China Horse Club International Ltd. and Starlight Racing</td>
        <td data-title="Time">1:41.18</td>
      </tr>
      <tr>
        <td data-title="Year">2017</td>
        <td data-title="Winner">McKinzie</td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Watson, Pegram &amp; Wellman</td>
        <td data-title="Time">1:42.57</td>
      </tr>
      <tr>
        <td data-title="Year">2016</td>
        <td data-title="Winner">Mastery</td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Cheyenne Stables</td>
        <td data-title="Time">1:41.56</td>
      </tr>
      <tr>
        <td data-title="Year">2015</td>
        <td data-title="Winner">Mor Spirit</td>
        <td data-title="Jockey">Gary Stevens</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Michael Lund Petersen</td>
        <td data-title="Time">1:43.54</td>
      </tr>
      <tr>
        <td data-title="Year">2014</td>
        <td data-title="Winner">Dortmund</td>
        <td data-title="Jockey">Martin Garcia</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Kaleem Shah</td>
        <td data-title="Time">1:40.86</td>
      </tr>
      <tr>
        <td data-title="Year">2013</td>
        <td data-title="Winner">Shared Belief</td>
        <td data-title="Jockey">Corey Nakatani</td>
        <td data-title="Trainer">Jerry Hollendorfer</td>
        <td data-title="Owner">Jungle Racing, Hollendorfer, Todaro, et al.</td>
        <td data-title="Time">1:42.16</td>
      </tr>
      <tr>
        <td data-title="Year">2012</td>
        <td data-title="Winner">Violence</td>
        <td data-title="Jockey">Javier Castellano</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Owner">Black Rock Thoroughbreds</td>
        <td data-title="Time">1:43.50</td>
      </tr>
      <tr>
        <td data-title="Year">2011</td>
        <td data-title="Winner">Liaison</td>
        <td data-title="Jockey">Rafael Bejarano</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Arnold Zetcher</td>
        <td data-title="Time">1:42.86
        </td>
      </tr>
      <tr>
        <td data-title="Year">2010</td>
        <td data-title="Winner">Comma to the Top</td>
        <td data-title="Jockey">Corey Nakatani</td>
        <td data-title="Trainer">Peter Miller</td>
        <td data-title="Owner">Barber/Birnbaum/Tsujihara</td>
        <td data-title="Time">1:44.72</td>
      </tr>
      <tr>
        <td data-title="Year">2009</td>
        <td data-title="Winner">Lookin at Lucky</td>
        <td data-title="Jockey">Garrett Gomez</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Michael E. Pegram</td>
        <td data-title="Time">1:43.30</td>
      </tr>
      <tr>
        <td data-title="Year">2008</td>
        <td data-title="Winner">Pioneerof the Nile</td>
        <td data-title="Jockey">Garrett Gomez</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Zayat Stables</td>
        <td data-title="Time">1:41.95</td>
      </tr>
      <tr>
        <td data-title="Year">2007</td>
        <td data-title="Winner">Into Mischief</td>
        <td data-title="Jockey">Victor Espinoza</td>
        <td data-title="Trainer">Richard Mandella</td>
        <td data-title="Owner">B. Wayne Hughes</td>
        <td data-title="Time">1:40.82</td>
      </tr>
      <tr>
        <td data-title="Year">2006</td>
        <td data-title="Winner">Stormello</td>
        <td data-title="Jockey">Kent Desormeaux</td>
        <td data-title="Trainer">William L. Currin</td>
        <td data-title="Owner">W. L. Currin &amp; A. Eisman</td>
        <td data-title="Time">1:42.19</td>
      </tr>
      <tr>
        <td data-title="Year">2005</td>
        <td data-title="Winner">Brother Derek</td>
        <td data-title="Jockey">Alex Solis</td>
        <td data-title="Trainer">Dan Hendricks</td>
        <td data-title="Owner">Cecil N. Peacock</td>
        <td data-title="Time">1:42.02</td>
      </tr>
      <tr>
        <td data-title="Year">2004</td>
        <td data-title="Winner">Declan's Moon</td>
        <td data-title="Jockey">Victor Espinoza</td>
        <td data-title="Trainer">Ronald W. Ellis</td>
        <td data-title="Owner">Jay Em Ess Stable</td>
        <td data-title="Time">1:41.63</td>
      </tr>
      <tr>
        <td data-title="Year">2003</td>
        <td data-title="Winner">Lion Heart</td>
        <td data-title="Jockey">Mike E. Smith</td>
        <td data-title="Trainer">Patrick Biancone</td>
        <td data-title="Owner">Smith &amp; Tabor</td>
        <td data-title="Time">1:42.80</td>
      </tr>
      <tr>
        <td data-title="Year">2002</td>
        <td data-title="Winner">Toccet</td>
        <td data-title="Jockey">Jorge Chavez</td>
        <td data-title="Trainer">John F. Scanlan</td>
        <td data-title="Owner">Dan Borislow</td>
        <td data-title="Time">1:41.26</td>
      </tr>
      <tr>
        <td data-title="Year">2001</td>
        <td data-title="Winner">Siphonic</td>
        <td data-title="Jockey">Jerry D. Bailey</td>
        <td data-title="Trainer">David Hofmans</td>
        <td data-title="Owner">Amerman Racing Stables</td>
        <td data-title="Time">1:42.09</td>
      </tr>
      <tr>
        <td data-title="Year">2000</td>
        <td data-title="Winner">Point Given</td>
        <td data-title="Jockey">Gary Stevens</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">The Thoroughbred Corp.</td>
        <td data-title="Time">1:42.21</td>
      </tr>
      <tr>
        <td data-title="Year">1999</td>
        <td data-title="Winner">Captain Steve</td>
        <td data-title="Jockey">Robby Albarado</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Michael E. Pegram</td>
        <td data-title="Time">1:43.27</td>
      </tr>
      <tr>
        <td data-title="Year">1998</td>
        <td data-title="Winner">Tactical Cat</td>
        <td data-title="Jockey">Laffit Pincay, Jr.</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Overbrook Farm</td>
        <td data-title="Time">1:42.63</td>
      </tr>
      <tr>
        <td data-title="Year">1997</td>
        <td data-title="Winner">Real Quiet</td>
        <td data-title="Jockey">Kent Desormeaux</td>
        <td data-title="Trainer">Bob Baffert</td>
        <td data-title="Owner">Michael E. Pegram</td>
        <td data-title="Time">1:41.34</td>
      </tr>
      <tr>
        <td data-title="Year">1996</td>
        <td data-title="Winner">Swiss Yodeler</td>
        <td data-title="Jockey">Alex Solis</td>
        <td data-title="Trainer">Mike Harrington</td>
        <td data-title="Owner">Heinz Steinmann</td>
        <td data-title="Time">1:42.70</td>
      </tr>
      <tr>
        <td data-title="Year">1995</td>
        <td data-title="Winner">Matty G</td>
        <td data-title="Jockey">Alex Solis</td>
        <td data-title="Trainer">Ron McAnally</td>
        <td data-title="Owner">Jack &amp; Joan Goodwin</td>
        <td data-title="Time">1:41.75</td>
      </tr>
      <tr>
        <td data-title="Year">1994</td>
        <td data-title="Winner">Afternoon Deelites</td>
        <td data-title="Jockey">Kent Desormeaux</td>
        <td data-title="Trainer">Richard Mandella</td>
        <td data-title="Owner">Burt Bacharach</td>
        <td data-title="Time">1:40.74</td>
      </tr>
      <tr>
        <td data-title="Year">1993</td>
        <td data-title="Winner">Valiant Nature</td>
        <td data-title="Jockey">Laffit Pincay, Jr.</td>
        <td data-title="Trainer">Ron McAnally</td>
        <td data-title="Owner">Verne Winchell</td>
        <td data-title="Time">1:40.78</td>
      </tr>
      <tr>
        <td data-title="Year">1992</td>
        <td data-title="Winner">River Special</td>
        <td data-title="Jockey">Laffit Pincay, Jr.</td>
        <td data-title="Trainer">Robert B. Hess, Jr.</td>
        <td data-title="Owner">Golden Eagle Farm</td>
        <td data-title="Time">1:43.27</td>
      </tr>
      <tr>
        <td data-title="Year">1991</td>
        <td data-title="Winner">A.P. Indy</td>
        <td data-title="Jockey">Ed Delahoussaye</td>
        <td data-title="Trainer">Neil D. Drysdale</td>
        <td data-title="Owner">Tomonori Tsurumaki</td>
        <td data-title="Time">1:42.85</td>
      </tr>
      <tr>
        <td data-title="Year">1990</td>
        <td data-title="Winner">Best Pal</td>
        <td data-title="Jockey">Jose A. Santos</td>
        <td data-title="Trainer">Gary F. Jones</td>
        <td data-title="Owner">Golden Eagle Farm</td>
        <td data-title="Time">1:35.40</td>
      </tr>
      <tr>
        <td data-title="Year">1989</td>
        <td data-title="Winner">Grand Canyon</td>
        <td data-title="Jockey">Angel Cordero, Jr.</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Overbrook Farm</td>
        <td data-title="Time">1:33.00</td>
      </tr>
      <tr>
        <td data-title="Year">1988</td>
        <td data-title="Winner">King Glorious</td>
        <td data-title="Jockey">Chris McCarron</td>
        <td data-title="Trainer">Jerry Hollendorfer</td>
        <td data-title="Owner">Four M Stable/Halo Farms</td>
        <td data-title="Time">1:35.60</td>
      </tr>
      <tr>
        <td data-title="Year">1987</td>
        <td data-title="Winner">Tejano</td>
        <td data-title="Jockey">Laffit Pincay, Jr.</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Lloyd R. French, Jr.</td>
        <td data-title="Time">1:34.60</td>
      </tr>
      <tr>
        <td data-title="Year">1986</td>
        <td data-title="Winner">Temperate Sil</td>
        <td data-title="Jockey">Bill Shoemaker</td>
        <td data-title="Trainer">Charles Whittingham</td>
        <td data-title="Owner">Frankfurt Stables &amp; Whittingham</td>
        <td data-title="Time">1:36.10</td>
      </tr>
      <tr>
        <td data-title="Year">1985</td>
        <td data-title="Winner">Snow Chief</td>
        <td data-title="Jockey">Alex Solis</td>
        <td data-title="Trainer">Melvin F. Stute</td>
        <td data-title="Owner">C. Grinstead / B. Rochelle</td>
        <td data-title="Time">1:34.20</td>
      </tr>
      <tr>
        <td data-title="Year">1984</td>
        <td data-title="Winner">Stephans Odyssey</td>
        <td data-title="Jockey">Eddie Maple</td>
        <td data-title="Trainer">Woody Stephens</td>
        <td data-title="Owner">Henryk de Kwiatkowski</td>
        <td data-title="Time">1:43.40</td>
      </tr>
      <tr>
        <td data-title="Year">1983</td>
        <td data-title="Winner">Fali Time</td>
        <td data-title="Jockey">Sandy Hawley</td>
        <td data-title="Trainer">Gary F. Jones</td>
        <td data-title="Owner">J. Mamakos/Dr. M. Stubrin</td>
        <td data-title="Time">1:41.60</td>
      </tr>
      <tr>
        <td data-title="Year">1982</td>
        <td data-title="Winner">Roving Boy</td>
        <td data-title="Jockey">Ed Delahoussaye</td>
        <td data-title="Trainer">Joseph Manzi</td>
        <td data-title="Owner">Robert E. Hibbert</td>
        <td data-title="Time">1:41.80</td>
      </tr>
      <tr>
        <td data-title="Year">1981</td>
        <td data-title="Winner">Stalwart</td>
        <td data-title="Jockey">Chris McCarron</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Marvin L. Warner</td>
        <td data-title="Time">1:47.80</td>
      </tr>
    </tbody>
  </table>
</div>

{literal}
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}