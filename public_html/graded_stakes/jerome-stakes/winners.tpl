<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" >
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2019 </td>
      <td data-title="Winner">Mind Control </td>
      <td data-title="Jockey">John Velazquez </td>
      <td data-title="Trainer">Gregory D. Sacco</td>
      <td data-title="Time">1:39.06 </td>
    </tr>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Firenze Fire </td>
      <td data-title="Jockey">Manuel Franco </td>
      <td data-title="Trainer">Jason Servis</td>
      <td data-title="Time">1:42.88 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">El Areeb</td>
      <td data-title="Jockey">Trevor McCarthy</td>
      <td data-title="Trainer">Cathal A. Lynch</td>
      <td data-title="Time">1:46.17 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Flexibility</td>
      <td data-title="Jockey">Irad Ortiz, Jr.</td>
      <td data-title="Trainer">Chad C. Brown</td>
      <td data-title="Time">1:42.98 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">El Kabeir</td>
      <td data-title="Jockey">Charles C. Lopez</td>
      <td data-title="Trainer">John P. Terranova II</td>
      <td data-title="Time">1:44.69 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Noble Moon</td>
      <td data-title="Jockey">Irad Ortiz, Jr.</td>
      <td data-title="Trainer">Leah Gyarmati</td>
      <td data-title="Time">1:45.08 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Vyjack</td>
      <td data-title="Jockey">Cornelio Velasquez</td>
      <td data-title="Trainer">Rudy R. Rodriguez</td>
      <td data-title="Time">1:40.67 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">The Lumber Guy</td>
      <td data-title="Jockey">Michael J. Luzzi</td>
      <td data-title="Trainer">Michael E. Hushion</td>
      <td data-title="Time">1:36.04 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Adios Charlie </td>
      <td data-title="Jockey">Rajiv Maragh</td>
      <td data-title="Trainer">Stanley M. Hough</td>
      <td data-title="Time">1:36.81 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Girolamo</td>
      <td data-title="Jockey">Alan Garcia</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Time">1:33.81 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Tale of Ekati</td>
      <td data-title="Jockey">Cornelio Velasquez</td>
      <td data-title="Trainer">Barclay Tagg</td>
      <td data-title="Time">1:36.17 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Daaher </td>
      <td data-title="Jockey">Mike Luzzi</td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Time">1:34.28 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Discreet Cat</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Time">1:36.46 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Silver Train</td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Richard E. Dutrow, Jr.</td>
      <td data-title="Time">1:34.24 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Teton Forest </td>
      <td data-title="Jockey">Shaun Bridgmohan</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:35.74 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">During </td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:36.32 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Boston Common </td>
      <td data-title="Jockey">Jorge Chavez</td>
      <td data-title="Trainer">Michael V. Pino</td>
      <td data-title="Time">1:36.12 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Express Tour </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Time">1:34.57 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Fusaichi Pegasus</td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Neil D. Drysdale</td>
      <td data-title="Time">1:34.07 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Doneraile Court </td>
      <td data-title="Jockey">Chris Antley</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Time">1:35.63 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Limit Out </td>
      <td data-title="Jockey">Jean-Luc Samyn</td>
      <td data-title="Trainer">H. Allen Jerkens</td>
      <td data-title="Time">1:36.22 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Richter Scale </td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">Patrick B. Byrne</td>
      <td data-title="Time">1:35.88 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Why Change </td>
      <td data-title="Jockey">Chuck C. Lopez</td>
      <td data-title="Trainer">Joseph Pierce, Jr.</td>
      <td data-title="Time">1:34.22 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">French Deputy </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Neil D. Drysdale</td>
      <td data-title="Time">1:33.53 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Prenup </td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Mark A. Hennig</td>
      <td data-title="Time">1:34.59 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Schossberg </td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Phil England</td>
      <td data-title="Time">1:35.53 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Furiously </td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">C. R. McGaughey III</td>
      <td data-title="Time">1:34.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Scan </td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">Flint S. Schulhofer</td>
      <td data-title="Time">1:34.09 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Housebuster</td>
      <td data-title="Jockey">Craig Perret</td>
      <td data-title="Trainer">Warren A. Croll, Jr.</td>
      <td data-title="Time">1:34.14 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">De Roche </td>
      <td data-title="Jockey">Dennis Carr</td>
      <td data-title="Trainer">Thomas Bohannan</td>
      <td data-title="Time">1:34.56 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Evening Kris </td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Philip Gleaves</td>
      <td data-title="Time">1:37.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Afleet</td>
      <td data-title="Jockey">Gary Stahlbaum</td>
      <td data-title="Trainer">Phil England</td>
      <td data-title="Time">1:33.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Ogygian</td>
      <td data-title="Jockey">Walter Guerra</td>
      <td data-title="Trainer">Jan H. Nerud</td>
      <td data-title="Time">1:34.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Creme Fraiche</td>
      <td data-title="Jockey">Eddie Maple</td>
      <td data-title="Trainer">Woody Stephens</td>
      <td data-title="Time">1:34.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Is Your Pleasure </td>
      <td data-title="Jockey">Don MacBeth</td>
      <td data-title="Trainer">Edward I. Kelly, Sr.</td>
      <td data-title="Time">1:35.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">A Phenomemon </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">Angel Penna Jr.</td>
      <td data-title="Time">1:35.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Fit To Fight</td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">MacKenzie Miller</td>
      <td data-title="Time">1:35.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Noble Nashua</td>
      <td data-title="Jockey">Cash Asmussen</td>
      <td data-title="Trainer">Jose A. Martin</td>
      <td data-title="Time">1:33.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Jaklin Klugman</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Leroy Jolley</td>
      <td data-title="Time">1:34.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Czaravich </td>
      <td data-title="Jockey">Jean Cruguet</td>
      <td data-title="Trainer">William H. Turner, Jr.</td>
      <td data-title="Time">1:35.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Sensitive Prince </td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">H. Allen Jerkens</td>
      <td data-title="Time">1:36.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Broadway Forli † </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Homer C. Pardue</td>
      <td data-title="Time">1:36.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Dance Spell</td>
      <td data-title="Jockey">Ruben Hernandez</td>
      <td data-title="Trainer">James W. Maloney</td>
      <td data-title="Time">1:35.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Guards Up </td>
      <td data-title="Jockey">Carlos Lopez</td>
      <td data-title="Trainer">Thomas H. Heard, Jr.</td>
      <td data-title="Time">1:34.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Stonewalk </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">Daniel J. Lopez</td>
      <td data-title="Time">1:34.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Step Nicely </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">H. Allen Jerkens</td>
      <td data-title="Time">1:34.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">True Knight </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">Lou Rondinello</td>
      <td data-title="Time">1:36.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Tinajero </td>
      <td data-title="Jockey">Eddie Belmonte</td>
      <td data-title="Trainer">Laz Barrera</td>
      <td data-title="Time">1:35.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Great Mystery </td>
      <td data-title="Jockey">Phil Grimm</td>
      <td data-title="Trainer">Lynn S. Whiting</td>
      <td data-title="Time">1:34.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Mr. Leader </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">MacKenzie Miller</td>
      <td data-title="Time">1:36.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Iron Ruler </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">Edward J. Yowell</td>
      <td data-title="Time">1:35.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">High Tribute </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Victor J. Nickerson</td>
      <td data-title="Time">1:34.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Bold and Brave </td>
      <td data-title="Jockey">Braulio Baeza</td>
      <td data-title="Trainer">Edward A. Neloy</td>
      <td data-title="Time">1:38.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Bold Bidder</td>
      <td data-title="Jockey">Eldon Nelson</td>
      <td data-title="Trainer">Randy Sechrest</td>
      <td data-title="Time">1:36.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Irvkup </td>
      <td data-title="Jockey">John L. Rotz</td>
      <td data-title="Trainer">Burley Parke</td>
      <td data-title="Time">1:35.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1963 </td>
      <td data-title="Winner">Chateaugay</td>
      <td data-title="Jockey">Braulio Baeza</td>
      <td data-title="Trainer">James P. Conway</td>
      <td data-title="Time">1:36.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">Black Beard </td>
      <td data-title="Jockey">Braulio Baeza</td>
      <td data-title="Trainer">James P. Conway</td>
      <td data-title="Time">1:34.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1961 </td>
      <td data-title="Winner">Carry Back</td>
      <td data-title="Jockey">Johnny Sellers</td>
      <td data-title="Trainer">Jack A. Price</td>
      <td data-title="Time">1:36.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1960 </td>
      <td data-title="Winner">Kelso</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Carl Hanford</td>
      <td data-title="Time">1:34.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1959 </td>
      <td data-title="Winner">Intentionally</td>
      <td data-title="Jockey">Manuel Ycaza</td>
      <td data-title="Trainer">Edward Kelly, Sr.</td>
      <td data-title="Time">1:35.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1958 </td>
      <td data-title="Winner">Warhead </td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Kay Erik Jensen</td>
      <td data-title="Time">1:37.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1957 </td>
      <td data-title="Winner">Bold Ruler</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">James Fitzsimmons</td>
      <td data-title="Time">1:35.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1956 </td>
      <td data-title="Winner">Reneged </td>
      <td data-title="Jockey">Angel Valenzuela</td>
      <td data-title="Trainer">Homer C. Pardue</td>
      <td data-title="Time">1:35.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1955 </td>
      <td data-title="Winner">Traffic Judge</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Woody Stephens</td>
      <td data-title="Time">1:35.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1954 </td>
      <td data-title="Winner">Martyr </td>
      <td data-title="Jockey">S. Small </td>
      <td data-title="Trainer">G. Carey Winfrey</td>
      <td data-title="Time">1:35.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1953 </td>
      <td data-title="Winner">Navy Page </td>
      <td data-title="Jockey">Nick Shuk</td>
      <td data-title="Trainer">Gordon J. McCann</td>
      <td data-title="Time">1:37.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1952 </td>
      <td data-title="Winner">Tom Fool</td>
      <td data-title="Jockey">Ted Atkinson</td>
      <td data-title="Trainer">John M. Gaver, Sr.</td>
      <td data-title="Time">1:37.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1951 </td>
      <td data-title="Winner">Alerted </td>
      <td data-title="Jockey">Ovie Scurlock</td>
      <td data-title="Trainer">Jimmy Penrod</td>
      <td data-title="Time">1:36.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1950 </td>
      <td data-title="Winner">Hill Prince</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Casey Hayes</td>
      <td data-title="Time">1:35.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1949 </td>
      <td data-title="Winner">Capot</td>
      <td data-title="Jockey">Ted Atkinson</td>
      <td data-title="Trainer">John M. Gaver, Sr.</td>
      <td data-title="Time">1:36.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1948 </td>
      <td data-title="Winner">Coaltown</td>
      <td data-title="Jockey">Newbold Pierson</td>
      <td data-title="Trainer">Horace A. Jones</td>
      <td data-title="Time">1:36.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1947 </td>
      <td data-title="Winner">Donor</td>
      <td data-title="Jockey">Job Dean Jessop</td>
      <td data-title="Trainer">George P. Odom</td>
      <td data-title="Time">1:37.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1946 </td>
      <td data-title="Winner">Mahout </td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Oscar White</td>
      <td data-title="Time">1:37.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1945 </td>
      <td data-title="Winner">Buzfuz </td>
      <td data-title="Jockey">Tommy Luther</td>
      <td data-title="Trainer">Joe Rosen</td>
      <td data-title="Time">1:37.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1944 </td>
      <td data-title="Winner">Occupy</td>
      <td data-title="Jockey">Otto Grohs</td>
      <td data-title="Trainer">Burley Parke</td>
      <td data-title="Time">1:37.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1943 </td>
      <td data-title="Winner">Slide Rule</td>
      <td data-title="Jockey">Jack Westrope</td>
      <td data-title="Trainer">Cecil Wilhelm</td>
      <td data-title="Time">1:37.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1942 </td>
      <td data-title="Winner">King's Abbey </td>
      <td data-title="Jockey">Carroll Bierman</td>
      <td data-title="Trainer">Graceton Philpot</td>
      <td data-title="Time">1:36.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1941 </td>
      <td data-title="Winner">Stimady </td>
      <td data-title="Jockey">Porter Roberts</td>
      <td data-title="Trainer">Robert R. Tilden </td>
      <td data-title="Time">1:37.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1940 </td>
      <td data-title="Winner">Roman </td>
      <td data-title="Jockey">Wayne D. Wright</td>
      <td data-title="Trainer">Daniel E. Stewart</td>
      <td data-title="Time">1:37.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1939 </td>
      <td data-title="Winner">Easy Mon </td>
      <td data-title="Jockey">Leon Haas</td>
      <td data-title="Trainer">Ben A. Jones</td>
      <td data-title="Time">1:35.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1938 </td>
      <td data-title="Winner">Cravat</td>
      <td data-title="Jockey">Alfred Robertson</td>
      <td data-title="Trainer">Walter Burrows</td>
      <td data-title="Time">1:36.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1937 </td>
      <td data-title="Winner">Pasha </td>
      <td data-title="Jockey">Lester Balaski</td>
      <td data-title="Trainer">J. Thomas Taylor</td>
      <td data-title="Time">1:38.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1936 </td>
      <td data-title="Winner">Goldeneye </td>
      <td data-title="Jockey">Ira Hanford</td>
      <td data-title="Trainer">A. A. Baroni</td>
      <td data-title="Time">1:36.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1935 </td>
      <td data-title="Winner">Good Harvest </td>
      <td data-title="Jockey">Sam Renick</td>
      <td data-title="Trainer">Bud Stotler</td>
      <td data-title="Time">1:36.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1934 </td>
      <td data-title="Winner">Kievex </td>
      <td data-title="Jockey">Wayne D. Wright</td>
      <td data-title="Trainer">J. J. Waldron</td>
      <td data-title="Time">1:37.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1933 </td>
      <td data-title="Winner">Golden Way </td>
      <td data-title="Jockey">Mack Garner</td>
      <td data-title="Trainer">Pete Coyne</td>
      <td data-title="Time">1:38.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1932 </td>
      <td data-title="Winner">Larranaga </td>
      <td data-title="Jockey">Raymond Workman</td>
      <td data-title="Trainer">Edward J. Bennett</td>
      <td data-title="Time">1:37.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1931 </td>
      <td data-title="Winner">Ironclad </td>
      <td data-title="Jockey">Lester Pichon</td>
      <td data-title="Trainer">Preston M. Burch</td>
      <td data-title="Time">1:37.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1930 </td>
      <td data-title="Winner">Mr. Sponge </td>
      <td data-title="Jockey">Mack Garner</td>
      <td data-title="Trainer">Henry McDaniel</td>
      <td data-title="Time">1:37.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1929 </td>
      <td data-title="Winner">Soul of Honor </td>
      <td data-title="Jockey">George Fields</td>
      <td data-title="Trainer">Robert A. Smith</td>
      <td data-title="Time">1:36.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1928 </td>
      <td data-title="Winner">Sun Edwin </td>
      <td data-title="Jockey">Linus McAtee</td>
      <td data-title="Trainer">Louis Feustel</td>
      <td data-title="Time">1:36.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1927 </td>
      <td data-title="Winner">Osmand </td>
      <td data-title="Jockey">Earl Sande</td>
      <td data-title="Trainer">G. Hamilton Keene</td>
      <td data-title="Time">1:38.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1926 </td>
      <td data-title="Winner">Croyden </td>
      <td data-title="Jockey">Linus McAtee</td>
      <td data-title="Trainer">James G. Rowe, Sr.</td>
      <td data-title="Time">1:38.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1925 </td>
      <td data-title="Winner">Primrose </td>
      <td data-title="Jockey">John Maiben</td>
      <td data-title="Trainer">Thomas J. Healey</td>
      <td data-title="Time">1:38.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1924 </td>
      <td data-title="Winner">Priscilla Ruley </td>
      <td data-title="Jockey">John Maiben</td>
      <td data-title="Trainer">James Fitzsimmons</td>
      <td data-title="Time">1:37.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1923 </td>
      <td data-title="Winner">Chery Pie </td>
      <td data-title="Jockey">Frank Coltiletti</td>
      <td data-title="Trainer">James G. Rowe, Sr.</td>
      <td data-title="Time">1:35.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1922 </td>
      <td data-title="Winner">Kai-Sang </td>
      <td data-title="Jockey">Laverne Fator</td>
      <td data-title="Trainer">Sam Hildreth</td>
      <td data-title="Time">1:37.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1921 </td>
      <td data-title="Winner">Tryster </td>
      <td data-title="Jockey">Frank Coltiletti</td>
      <td data-title="Trainer">James G. Rowe, Sr.</td>
      <td data-title="Time">1:38.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1920 </td>
      <td data-title="Winner">Busy Signal </td>
      <td data-title="Jockey">Clarence Kummer</td>
      <td data-title="Trainer">William A. Hurley</td>
      <td data-title="Time">1:37.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1919 </td>
      <td data-title="Winner">Thunderclap </td>
      <td data-title="Jockey">Laverne Fator</td>
      <td data-title="Trainer">Sam Hildreth</td>
      <td data-title="Time">1:39.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1918 </td>
      <td data-title="Winner">Sunny Slope </td>
      <td data-title="Jockey">John Callahan</td>
      <td data-title="Trainer">Not found </td>
      <td data-title="Time">1:38.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1917 </td>
      <td data-title="Winner">Bally </td>
      <td data-title="Jockey">Lawrence Lyke</td>
      <td data-title="Trainer">James H. McCormack</td>
      <td data-title="Time">1:38.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1916 </td>
      <td data-title="Winner">Spur</td>
      <td data-title="Jockey">Johnny Loftus</td>
      <td data-title="Trainer">James H. McCormack</td>
      <td data-title="Time">1:39.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1915 </td>
      <td data-title="Winner">Trial By Jury </td>
      <td data-title="Jockey">Thomas McTaggart</td>
      <td data-title="Trainer">J. Simon Healy</td>
      <td data-title="Time">1:38.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1914 </td>
      <td data-title="Winner">Stromboli </td>
      <td data-title="Jockey">Clarence Turner</td>
      <td data-title="Trainer">Sam Hildreth</td>
      <td data-title="Time">1:36.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1909 </td>
      <td data-title="Winner">Fitz Herbert</td>
      <td data-title="Jockey">Eddie Dugan</td>
      <td data-title="Trainer">Sam Hildreth</td>
      <td data-title="Time">2:11.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1908 </td>
      <td data-title="Winner">Fair Play</td>
      <td data-title="Jockey">Clifford Gilbert</td>
      <td data-title="Trainer">Sam Hildreth</td>
      <td data-title="Time">2:10.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1907 </td>
      <td data-title="Winner">Perseverance </td>
      <td data-title="Jockey">D. R. "Puddin" McDaniel</td>
      <td data-title="Trainer">John W. Rogers</td>
      <td data-title="Time">2:13.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1906 </td>
      <td data-title="Winner">Ironsides </td>
      <td data-title="Jockey">Herman Radtke</td>
      <td data-title="Trainer">John W. Rogers</td>
      <td data-title="Time">2:10.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1905 </td>
      <td data-title="Winner">Bedouin </td>
      <td data-title="Jockey">Willie Shaw</td>
      <td data-title="Trainer">John Huggins</td>
      <td data-title="Time">2:10.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1904 </td>
      <td data-title="Winner">Ostrich </td>
      <td data-title="Jockey">William Crimmins</td>
      <td data-title="Trainer">Frank Lightfoot</td>
      <td data-title="Time">2:13.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1903 </td>
      <td data-title="Winner">Eugenia Burch</td>
      <td data-title="Jockey">Grover Fuller</td>
      <td data-title="Trainer">W. P. Maxwell </td>
      <td data-title="Time">2:15.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1902 </td>
      <td data-title="Winner">Hermis</td>
      <td data-title="Jockey">Ted Rice</td>
      <td data-title="Trainer">James H. McCormick</td>
      <td data-title="Time">2:06.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1901 </td>
      <td data-title="Winner">Blues </td>
      <td data-title="Jockey">Willie Shaw</td>
      <td data-title="Trainer">Frank D. Weir</td>
      <td data-title="Time">2:05.75 </td>
    </tr>
    <tr>
      <td data-title="Year">1900 </td>
      <td data-title="Winner">Alcedo </td>
      <td data-title="Jockey">Patrick McCue</td>
      <td data-title="Trainer">James H. McCormick</td>
      <td data-title="Time">2:07.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1899 </td>
      <td data-title="Winner">King Barleycorn </td>
      <td data-title="Jockey">George M. Odom</td>
      <td data-title="Trainer">Edward Heffner</td>
      <td data-title="Time">2:09.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1898 </td>
      <td data-title="Winner">Handball </td>
      <td data-title="Jockey">Nash Turner</td>
      <td data-title="Trainer">Frank McCabe</td>
      <td data-title="Time">2:06.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1897 </td>
      <td data-title="Winner">Rensselaer </td>
      <td data-title="Jockey">E. Hewitt </td>
      <td data-title="Trainer">Henry Harris</td>
      <td data-title="Time">2:07.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1896 </td>
      <td data-title="Winner">Soufflé</td>
      <td data-title="Jockey">Joe Hill</td>
      <td data-title="Trainer">J. Healy </td>
      <td data-title="Time">2:09.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1895 </td>
      <td data-title="Winner">Counter Tenor </td>
      <td data-title="Jockey">Willie Simms</td>
      <td data-title="Trainer">William Lakeland</td>
      <td data-title="Time">1:54.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1894 </td>
      <td data-title="Winner">Rubicon </td>
      <td data-title="Jockey">William Midgely</td>
      <td data-title="Trainer">Henry Harris</td>
      <td data-title="Time">2:09.75 </td>
    </tr>
    <tr>
      <td data-title="Year">1893 </td>
      <td data-title="Winner">Young Arion </td>
      <td data-title="Jockey">John Lamely</td>
      <td data-title="Trainer">William Lakeland</td>
      <td data-title="Time">2:08.75 </td>
    </tr>
    <tr>
      <td data-title="Year">1892 </td>
      <td data-title="Winner">Tammany </td>
      <td data-title="Jockey">Edward Garrison</td>
      <td data-title="Trainer">Matthew Byrnes</td>
      <td data-title="Time">2:36.25 </td>
    </tr>
    <tr>
      <td data-title="Year">1891 </td>
      <td data-title="Winner">Picknicker </td>
      <td data-title="Jockey">Alonzo Clayton</td>
      <td data-title="Trainer">Louis Stuart</td>
      <td data-title="Time">2:22.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1890 </td>
      <td data-title="Winner">Tournament </td>
      <td data-title="Jockey">William Hayward</td>
      <td data-title="Trainer">Matthew M. Allen</td>
      <td data-title="Time">2:16.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1889 </td>
      <td data-title="Winner">Longstreet</td>
      <td data-title="Jockey">Isaac Burns Murphy</td>
      <td data-title="Trainer">Hardy Campbell, Jr.</td>
      <td data-title="Time">3:11.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1888 </td>
      <td data-title="Winner">Prince Royal </td>
      <td data-title="Jockey">Edward Garrison</td>
      <td data-title="Trainer">James G. Rowe, Sr.</td>
      <td data-title="Time">3:10.25 </td>
    </tr>
    <tr>
      <td data-title="Year">1887 </td>
      <td data-title="Winner">Firenze</td>
      <td data-title="Jockey">Edward Garrison</td>
      <td data-title="Trainer">Matthew Byrnes</td>
      <td data-title="Time">3:09.75 </td>
    </tr>
    <tr>
      <td data-title="Year">1886 </td>
      <td data-title="Winner">The Bard</td>
      <td data-title="Jockey">William Hayward</td>
      <td data-title="Trainer">John Huggins</td>
      <td data-title="Time">N/A </td>
    </tr>
    <tr>
      <td data-title="Year">1885 </td>
      <td data-title="Winner">Longview </td>
      <td data-title="Jockey">William Fitzpatrick</td>
      <td data-title="Trainer">Not found </td>
      <td data-title="Time">3:20.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1884 </td>
      <td data-title="Winner">Water Lily </td>
      <td data-title="Jockey">Matthew Feakes</td>
      <td data-title="Trainer">Not found </td>
      <td data-title="Time">3:16.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1883 </td>
      <td data-title="Winner">George Kinney</td>
      <td data-title="Jockey">William Fitzpatrick</td>
      <td data-title="Trainer">James G. Rowe, Sr.</td>
      <td data-title="Time">3:19.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1882 </td>
      <td data-title="Winner">Charley B. </td>
      <td data-title="Jockey">Jim McLaughlin</td>
      <td data-title="Trainer">James G. Rowe, Sr.</td>
      <td data-title="Time">3:21.50 </td>
    </tr>
    <tr>
      <td data-title="Year">1881 </td>
      <td data-title="Winner">Barrett </td>
      <td data-title="Jockey">Matthew Feakes</td>
      <td data-title="Trainer">Matthew Byrnes</td>
      <td data-title="Time">3:13.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1880 </td>
      <td data-title="Winner">Grenada</td>
      <td data-title="Jockey">Lloyd Hughes</td>
      <td data-title="Trainer">R. Wyndham Walden</td>
      <td data-title="Time">3:12.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1879 </td>
      <td data-title="Winner">Monitor </td>
      <td data-title="Jockey">Lloyd Hughes</td>
      <td data-title="Trainer">R. Wyndham Walden</td>
      <td data-title="Time">3:12.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1878 </td>
      <td data-title="Winner">Duke of Magenta</td>
      <td data-title="Jockey">Lloyd Hughes</td>
      <td data-title="Trainer">R. Wyndham Walden</td>
      <td data-title="Time">3:11.50 </td>
    </tr>
    <tr>
      <td data-title="Year">1877 </td>
      <td data-title="Winner">Bazil </td>
      <td data-title="Jockey">George Evans</td>
      <td data-title="Trainer">Jacob Pincus</td>
      <td data-title="Time">3:43.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1876 </td>
      <td data-title="Winner">Charley Howard </td>
      <td data-title="Jockey">W. Lakewood </td>
      <td data-title="Trainer">David McDaniel</td>
      <td data-title="Time">3:47.75 </td>
    </tr>
    <tr>
      <td data-title="Year">1875 </td>
      <td data-title="Winner">Aristides</td>
      <td data-title="Jockey">Robert Swim</td>
      <td data-title="Trainer">Ansel Williamson</td>
      <td data-title="Time">3:43.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1874 </td>
      <td data-title="Winner">Acrobat </td>
      <td data-title="Jockey">J. Sparling </td>
      <td data-title="Trainer">Charles S. Lloyd</td>
      <td data-title="Time">3:37.75 </td>
    </tr>
    <tr>
      <td data-title="Year">1873 </td>
      <td data-title="Winner">Tom Bowling </td>
      <td data-title="Jockey">Robert Swim</td>
      <td data-title="Trainer">Ansel Williamson</td>
      <td data-title="Time">3:40.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1872 </td>
      <td data-title="Winner">Joe Daniels</td>
      <td data-title="Jockey">James Rowe, Sr.</td>
      <td data-title="Trainer">David McDaniel</td>
      <td data-title="Time">3:40.25 </td>
    </tr>
    <tr>
      <td data-title="Year">1871 </td>
      <td data-title="Winner">Harry Bassett</td>
      <td data-title="Jockey">James Rowe, Sr.</td>
      <td data-title="Trainer">David McDaniel</td>
      <td data-title="Time">3:54.75 </td>
    </tr>
    <tr>
      <td data-title="Year">1870 </td>
      <td data-title="Winner">Kingfisher</td>
      <td data-title="Jockey">Burns </td>
      <td data-title="Trainer">Raleigh Colston Sr.</td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1869 </td>
      <td data-title="Winner">Glenelg</td>
      <td data-title="Jockey">Charles Miller</td>
      <td data-title="Trainer">Jacob Pincus</td>
      <td data-title="Time">1:48.50 </td>
    </tr>
    <tr>
      <td data-title="Year">1868 </td>
      <td data-title="Winner">Bayonet </td>
      <td data-title="Jockey">Charles Miller</td>
      <td data-title="Trainer">T. G. Moore</td>
      <td data-title="Time">1:45.25 </td>
    </tr>
    <tr>
      <td data-title="Year">1867 </td>
      <td data-title="Winner">Metairie </td>
      <td data-title="Jockey">Patsy Hennessey </td>
      <td data-title="Trainer">Jacob Pincus</td>
      <td data-title="Time">1:49.25 </td>
    </tr>
    <tr>
      <td data-title="Year">1866 </td>
      <td data-title="Winner">Watson </td>
      <td data-title="Jockey">Abe Hawkins</td>
      <td data-title="Trainer">Jacob Pincus</td>
      <td data-title="Time">1:48.75 </td>
    </tr>
  </tbody>
</table>

</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}