  <div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th width="70px">Age</th>        
        <th>Jockey</th>
        <th>Trainer</th>      
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Village King </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">John R. Velazquez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">1:52.95 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Spring Quality </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Graham Motion</td>
      <td data-title="Time">2:17.72 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Bigger Picture</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jose L. Ortiz</td>
      <td data-title="Trainer">Michael J. Maker</td>
      <td data-title="Time">2:15.46 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Mr. Maybe</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Irad Ortiz Jr.</td>
      <td data-title="Trainer">Chad C. Brown</td>
      <td data-title="Time">2:17.49 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Dynamic Sky</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Cornelio Velasquez</td>
      <td data-title="Trainer">Mark E. Casse</td>
      <td data-title="Time">2:18.85 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Imagining</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">Claude R. McGaughey III</td>
      <td data-title="Time">2:18.43 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Boisterous</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Shug McGaughey</td>
      <td data-title="Time">2:19.38 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Boisterous</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Alan Garcia</td>
      <td data-title="Trainer">Shug McGaughey</td>
      <td data-title="Time">2:20.26 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Grassy</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Joe Bravo</td>
      <td data-title="Trainer">Christophe Clement</td>
      <td data-title="Time">2:20.06 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Expansion </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Channing Hill</td>
      <td data-title="Trainer">Chad Brown</td>
      <td data-title="Time">2:18.93 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Strike a Deal</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Chuck C. Lopez</td>
      <td data-title="Trainer">Alan E. Goldberg</td>
      <td data-title="Time">2:23.28 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Dave </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Joe Bravo</td>
      <td data-title="Trainer">Barclay Tagg</td>
      <td data-title="Time">2:21.45 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Naughty New Yorker </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jean-Luc Samyn</td>
      <td data-title="Trainer">Patrick J. Kelly</td>
      <td data-title="Time">2:04.52 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">King's Drama </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">2:15.37 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Dreadnaught </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jean-Luc Samyn</td>
      <td data-title="Trainer">Thomas Voss</td>
      <td data-title="Time">2:18.87 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Balto Star </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Time">2:18.86 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Evening Attire</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Shaun Bridgmohan</td>
      <td data-title="Trainer">Patrick J. Kelly</td>
      <td data-title="Time">2:14.81 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Mr. Pleasentfar </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">Martin D. Wolfson</td>
      <td data-title="Time">2:16.94 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Cetewayo </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Richard Migliore</td>
      <td data-title="Trainer">Michael Dickinson</td>
      <td data-title="Time">2:17.93 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Monarch's Maze </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Joe Bravo</td>
      <td data-title="Trainer">Patrick J. Kelly</td>
      <td data-title="Time">2:14.44 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Musical Ghost </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Mark A. Hennig</td>
      <td data-title="Time">2:15.53 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Instant Friendship </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Leo O'Brien</td>
      <td data-title="Time">2:17.08 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Mr. Bluebird </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">John C. Kimmel</td>
      <td data-title="Time">2:15.35 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Flag Down </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">Christophe Clement</td>
      <td data-title="Time">2:22.03 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Franchise Player </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Dale Beckner</td>
      <td data-title="Trainer">Philip G. Johnson</td>
      <td data-title="Time">2:20.53 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Royal Mountain Inn </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Julie Krone</td>
      <td data-title="Trainer">Barclay Tagg</td>
      <td data-title="Time">1:59.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Montserrat </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Julie Krone</td>
      <td data-title="Trainer">W. Elliott Walden</td>
      <td data-title="Time">2:00.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Who's To Pay </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">MacKenzie Miller</td>
      <td data-title="Time">1:58.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Yankee Affair</td>
      <td data-title="Age">8 </td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">Henry L. Carroll</td>
      <td data-title="Time">2:00.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Rambo Dancer </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">Leroy Jolley</td>
      <td data-title="Time">2:01.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Pay the Butler</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Robbie Davis</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">2:01.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Theatrical</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Time">2:00.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Equalize </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Walter Guerra</td>
      <td data-title="Trainer">Jan H. Nerud</td>
      <td data-title="Time">2:02.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Divulge </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jean Cruguet</td>
      <td data-title="Trainer">Gasper Moschera</td>
      <td data-title="Time">1:59.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Sharannpour </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Angel Cordero Jr.</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">2:04.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Hero's Honor </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">MacKenzie Miller</td>
      <td data-title="Time">2:02.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Super Sunrise </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Craig Perret</td>
      <td data-title="Trainer">Warren A. Croll Jr.</td>
      <td data-title="Time">2:06.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Thunder Puddles </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jean-Luc Samyn</td>
      <td data-title="Trainer">John P. Campo</td>
      <td data-title="Time">2:06.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Highland Blade</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">David A. Whiteley</td>
      <td data-title="Time">2:06.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Match the Hatch </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Kenny Skinner</td>
      <td data-title="Trainer">Philip G. Johnson</td>
      <td data-title="Time">1:59.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Marquee Universal </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Hector Pilar</td>
      <td data-title="Trainer">Alec Bullock</td>
      <td data-title="Time">1:58.80 </td>
    </tr>
    <tr bgcolor="#eeeeee">
      <td data-title="Year">1979 </td>
      <td data-title="Winner"><em>No race</em></td>
      <td data-title="Age">&nbsp;</td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Tiller</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jeffrey Fell</td>
      <td data-title="Trainer">David A. Whiteley</td>
      <td data-title="Time">2:00.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Clout </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">George Martens</td>
      <td data-title="Trainer">William O. Hicks</td>
      <td data-title="Time">1:40.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Quick Card </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Angel Cordero Jr.</td>
      <td data-title="Trainer">Thomas J. Kelly</td>
      <td data-title="Time">1:39.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Erwin Boy </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Ron Turcotte</td>
      <td data-title="Trainer">James P. Conway</td>
      <td data-title="Time">2:01.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Telefonico </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Craig Perret</td>
      <td data-title="Trainer">Lou M. Goldfine</td>
      <td data-title="Time">2:03.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Take Off </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Ron Turcotte</td>
      <td data-title="Trainer">Frank Catrone</td>
      <td data-title="Time">2:00.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Red Reality </td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">MacKenzie Miller</td>
      <td data-title="Time">2:13.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">New Alibhai </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Frank Iannelli</td>
      <td data-title="Trainer">Ralph W. McIlvain</td>
      <td data-title="Time">2:02.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Drumtop</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Chuck Baltazar</td>
      <td data-title="Trainer">Roger Laurin</td>
      <td data-title="Time">2:16.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Drumtop</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Angel Cordero Jr.</td>
      <td data-title="Trainer">Roger Laurin</td>
      <td data-title="Time">2:20.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Majetta </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Angel Cordero Jr.</td>
      <td data-title="Trainer">Angel Penna Sr.</td>
      <td data-title="Time">2:19.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">High Hat </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eddie Belmonte</td>
      <td data-title="Trainer">Kay E. Jensen</td>
      <td data-title="Time">2:20.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Ginger Fizz </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Fernando Toro</td>
      <td data-title="Trainer">Thomas J. Kelly</td>
      <td data-title="Time">1:55.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Spoon Bait </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">John L. Rotz</td>
      <td data-title="Trainer">Ivor G. Balding</td>
      <td data-title="Time">1:54.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Tenacle </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Bobby Ussery</td>
      <td data-title="Trainer">Homer C. Pardue</td>
      <td data-title="Time">1:55.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Will I Rule </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Braulio Baeza</td>
      <td data-title="Trainer">Joseph Nash</td>
      <td data-title="Time">1:55.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1963 </td>
      <td data-title="Winner">Vimy Ridge </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Sam Boulmetis</td>
      <td data-title="Trainer">Thomas J. Barry</td>
      <td data-title="Time">1:59.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">Wise Ship </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Heliodoro Gustines</td>
      <td data-title="Trainer">Jacob Byer</td>
      <td data-title="Time">2:16.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1961 </td>
      <td data-title="Winner">Wolfram </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Ismael Valenzuela</td>
      <td data-title="Trainer">Burley Parke</td>
      <td data-title="Time">2:15.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1960 </td>
      <td data-title="Winner">North Pole II </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Sam Boulmetis Sr.</td>
      <td data-title="Trainer">Sidney J. Smith</td>
      <td data-title="Time">2:15.00</td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}