<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/graded-stakes/sword-dancer/sword-dancer-off-track-betting.jpg" alt="Sword Dancer Stakes  Off Track Betting - OTB" />
    <figure class="rsCaption" data-move-effect="left" data-speed="200">2014 Sword Dancer Invitational Handicap Winner Main Sequence </figure>
   
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/saratoga/saratoga-horse-betting.jpg" alt="Sword Dancer Stakes  Horse Betting" />
    <figure class="rsCaption"> Saratoga </figure>
   </div>


   
  </div>
 </div>
