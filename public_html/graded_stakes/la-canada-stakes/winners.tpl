<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Mopotism </td>
      <td data-title="Jockey">Mario Gutierrez</td>
      <td data-title="Trainer">Doug F. O'Neill</td>
      <td data-title="Time">1:37.29 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Vale Dori (ARG) </td>
      <td data-title="Jockey">Mike E Smith</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:44.95 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Taris</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Simon Callaghan</td>
      <td data-title="Time">1:44.38 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Thegirlinthatsong</td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Jerry Hollendorfer</td>
      <td data-title="Time">1:43.84 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Spellbound</td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">Richard Mandella</td>
      <td data-title="Time">1:43.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">More Chocolate</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">John W. Sadler</td>
      <td data-title="Time">1:42.59 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Include Me Out </td>
      <td data-title="Jockey">Joseph Talamo</td>
      <td data-title="Trainer">Ronald Ellis</td>
      <td data-title="Time">1:41.89 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Always a Princess</td>
      <td data-title="Jockey">Martin Garcia</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:48.36 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Striking Dancer</td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Ken McPeek</td>
      <td data-title="Time">1:48.48 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Life Is Sweet</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">John Shirreffs</td>
      <td data-title="Time">1:49.70 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Dawn After Dawn </td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">John W. Sadler</td>
      <td data-title="Time">1:50.37 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Balance </td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">David Hofmans</td>
      <td data-title="Time">1:49.41 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Seafree </td>
      <td data-title="Jockey">Patrick Valenzuela</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:50.04 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Tarlow </td>
      <td data-title="Jockey">Patrick Valenzuela</td>
      <td data-title="Trainer">John Shirreffs</td>
      <td data-title="Time">1:48.64 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Cat Fighter </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:50.41 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Got Koko</td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Bruce Headley</td>
      <td data-title="Time">1:48.41 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Summer Colony </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Mark A. Hennig</td>
      <td data-title="Time">1:49.26 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Spain</td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:49.74 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Scholars Studio </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Martin F. Jones</td>
      <td data-title="Time">1:49.14 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Manistique</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">John Shirreffs</td>
      <td data-title="Time">1:48.81 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Fleet Lady </td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Jerry Hollendorfer</td>
      <td data-title="Time">1:48.59 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Belle's Flag </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Darrell Vienna</td>
      <td data-title="Time">1:48.28 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Jewel Princess</td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Wallace Dollase</td>
      <td data-title="Time">1:49.42 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Dianes Halo </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">David Cross, Jr.</td>
      <td data-title="Time">1:49.35 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Stalcreek </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">David Bernstein</td>
      <td data-title="Time">1:48.85 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Alysbelle </td>
      <td data-title="Jockey">Ed Delahoussaye</td>
      <td data-title="Trainer">Jack Van Berg</td>
      <td data-title="Time">1:49.85 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Exchange </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Bill Spawr</td>
      <td data-title="Time">1:49.96 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Fit To Scout </td>
      <td data-title="Jockey">Julio A. Garcia</td>
      <td data-title="Trainer">Jack Van Berg</td>
      <td data-title="Time">1:48.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Gorgeous </td>
      <td data-title="Jockey">Ed Delahoussaye</td>
      <td data-title="Trainer">Neil D. Drysdale</td>
      <td data-title="Time">1:50.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Goodbye Halo</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Charles E. Whittingham</td>
      <td data-title="Time">1:54.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Hollywood Glitter </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Gary F. Jones</td>
      <td data-title="Time">1:49.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Family Style </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Lady's Secret</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:49.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Mitterand</td>
      <td data-title="Jockey">Ed Delahoussaye</td>
      <td data-title="Trainer">Randy Winick</td>
      <td data-title="Time">1:48.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Sweet Diane </td>
      <td data-title="Jockey">Ray Sibille</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:49.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Avigaition </td>
      <td data-title="Jockey">Ed Delahoussaye</td>
      <td data-title="Trainer">Vivian Pulliam</td>
      <td data-title="Time">1:49.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Safe Play </td>
      <td data-title="Jockey">Don Brumfield</td>
      <td data-title="Trainer">Harvey L. Vanier</td>
      <td data-title="Time">1:47.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Summer Siren </td>
      <td data-title="Jockey">Marco Castaneda</td>
      <td data-title="Trainer">Lou Carno</td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Glorious Song</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Gerry Belanger</td>
      <td data-title="Time">1:47.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">B. Thoughtful </td>
      <td data-title="Jockey">Donald Pierce</td>
      <td data-title="Trainer">Robert L. Wheeler</td>
      <td data-title="Time">1:48.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Taisez Vous</td>
      <td data-title="Jockey">Donald Pierce</td>
      <td data-title="Trainer">Robert L. Wheeler</td>
      <td data-title="Time">1:49.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Lucie Manet </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Jaime Villagomez</td>
      <td data-title="Time">1:48.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Raise Your Skirts </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Barney Willis</td>
      <td data-title="Time">1:48.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Chris Evert</td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">Joseph A. Trovato</td>
      <td data-title="Time">1:41.60 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}