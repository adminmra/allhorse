
{php}
$startDate = strtotime("13 September 2015 12:00:00");
$endDate  = strtotime("19 September 2015 23:47:00");
{/php}

<div class="countdown margin-bottom-30">
<div class="headline"><h2>Pennsylvania Derby</h2></div>

<div class="panel">
	<div class="race">{* <span>Race:</span> *} <span>The $1,000,000 Pennsylvania Derby{*<a href="/pennsylvania-derby">Pennsylvania Derby</a>*} is a Graded stakes races for 3 year-olds.  The 9 furlong dirt race is held at Parx Racing and Casino{*<a href="/woodbine">Parx Racing and Casino</a>*} in Bensalem, Pennsylvania.</span></div>
	<div class="info"> <p><span>What we like:</span>Betting on the Pennsylvania Derby online is fast and easy.</p><p><strong>Where can I bet on the Pennsylvania Derby? </strong>BUSR!</p></div>
</div>

<div class="clock">
  <div class="item clock_days">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>      
     <div class="topLayer"><canvas id="canvas_days" width="188" height="188"> </canvas></div>      
      <div class="text">
        <p class="val">0</p>
        <p class="time type_days">Days</p>
      </div>
  </div>

  <div class="item clock_hours">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>
      <div class="topLayer"> <canvas id="canvas_hours" width="188" height="188"> </canvas></div>
      <div class="text">
      	<p class="val">0</p>
        <p class="time type_hours">Hours</p>
      </div>
  </div>  

  <div class="item clock_minutes">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>
      <div class="topLayer"><canvas id="canvas_minutes" width="188" height="188"> </canvas></div>
      <div class="text">
        <p class="val">0</p>
        <p class="time type_minutes">Minutes</p>
      </div>
  </div>
  
  <div class="item clock_seconds">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>
      <div class="topLayer"><canvas id="canvas_seconds" width="188" height="188"> </canvas></div>     
      <div class="text">
        <p class="val">0</p>
        <p class="time type_seconds">Seconds</p>
      </div>
  </div>
</div>

<br /><br /><br /><br /><br /><!-- delete these if you use/uncomment the button below -->
<!--<div class="blockfooter"><a class="btn btn-primary" href="/travers-stakes" target="_blank" rel="nofollow">Learn More <i class="fa fa-angle-right"></i></a></div>
</div><!-- end/countdown -->


<!-- Initialize the countdown -->
{literal}
<script type="text/javascript">
$(document).ready(function(){	
JBCountDown({ secondsColor : "#ffdc50", secondsGlow  : "none", minutesColor : "#9cdb7d", minutesGlow  : "none", hoursColor : "#378cff", hoursGlow    : "none",  daysColor    : "#ff6565", daysGlow     : "none", startDate   : "{/literal}{php} echo $startDate; {/php}{literal}", endDate : "{/literal}{php} echo $endDate; {/php}{literal}", now : "{/literal}{php} echo strtotime('now'); {/php}{literal}" });
 });
</script>
{/literal}


