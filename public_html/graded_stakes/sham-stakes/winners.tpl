<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>        
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2019 </td>
      <td data-title="Winner">Gunmetal Gray </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Jerry Hollendorfer</td>
      <td data-title="Owner">Hollendorfer, LLC, Pearl Racing and West Point Thoroughbreds </td>
      <td data-title="Time">1:38.96 </td>
    </tr>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">McKinzie </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Watson, Karl, Pegram, Michael E. and Weitman, Paul </td>
      <td data-title="Time">1:36.58 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Gormley</td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">John A. Shirreffs</td>
      <td data-title="Owner">Mr. &amp; Mrs. Jerome Moss </td>
      <td data-title="Time">1:35.89 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Collected</td>
      <td data-title="Jockey">Martin Garcia</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Speedway Stable </td>
      <td data-title="Time">1:38.00 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Calculator</td>
      <td data-title="Jockey">Elvis Trujillo</td>
      <td data-title="Trainer">Peter Miller</td>
      <td data-title="Owner">Richard C. Pell </td>
      <td data-title="Time">1:34.88 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Midnight Hawk</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Hill 'n' Dale/Sikura/Pegram et al. </td>
      <td data-title="Time">1:36.48 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Goldencents</td>
      <td data-title="Jockey">Kevin Krigger</td>
      <td data-title="Trainer">Doug F. O'Neill</td>
      <td data-title="Owner">W.C. Racing/Kenney/RAP Racing </td>
      <td data-title="Time">1:36.50 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Out of Bounds</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Eoin Harty</td>
      <td data-title="Owner">Darley Stud</td>
      <td data-title="Time">1:34.56 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Tapizar</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">Winchell Thoroughbreds LLC </td>
      <td data-title="Time">1:40.38 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Alphie's Bet</td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Alexis Barba</td>
      <td data-title="Owner">Peter Johnson Sr</td>
      <td data-title="Time">1:48.72 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">The Pamplemousse</td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Julio C. Canani</td>
      <td data-title="Owner">Alex Solis II/Jeffrey Strauss </td>
      <td data-title="Time">1:47.86 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Colonel John</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Eoin G. Harty</td>
      <td data-title="Owner">WinStar Farm LLC</td>
      <td data-title="Time">1:50.15 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Ravel</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Michael McCarthy</td>
      <td data-title="Owner">Michael Tabor/Derrick Smith </td>
      <td data-title="Time">1:48.91 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Bob and John</td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Stonerside Stable</td>
      <td data-title="Time">1:49.15 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Going Wild</td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Bob &amp; Beverly Lewis</td>
      <td data-title="Time">1:50.18 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Master David</td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Owner">Georgica Stable et al. </td>
      <td data-title="Time">1:49.20 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Man Among Men </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Gary Mandella</td>
      <td data-title="Owner">Hubbard and Sczesny </td>
      <td data-title="Time">1:48.39 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">USS Tinosa</td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Jerry Hollendorfer</td>
      <td data-title="Owner">Abruzzo &amp; Thiriot </td>
      <td data-title="Time">1:49.11 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Wild and Wise </td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Golden Eagle Farm</td>
      <td data-title="Time">1:50.51 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}