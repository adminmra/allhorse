<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>        
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2019 </td>
      <td data-title="Winner">Bellafina </td>
      <td data-title="Jockey">Flavian Pratt </td>
      <td data-title="Trainer">Simon Callaghan </td>
      <td data-title="Owner">Kaleem Shah, Inc. </td>
      <td data-title="Time">1:22:00 </td>
    </tr>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Midnight Bisou </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">William Spawr </td>
      <td data-title="Owner">Allen Racing LLC and Bloom Racing Stable LLC (Jeffrey Bloom) </td>
      <td data-title="Time">1:23.40 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Unique Bella</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Jerry Hollendorfer</td>
      <td data-title="Owner">Don Alberto Stable </td>
      <td data-title="Time">1:22.21 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Forever Darling</td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Richard Baltas</td>
      <td data-title="Owner">Baltas/Hebert Bloodstock/J K Racing </td>
      <td data-title="Time">1:16.25 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Seduire</td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Jerry Hollendorfer</td>
      <td data-title="Owner">Regis Farms </td>
      <td data-title="Time">1:15.37 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Awesome Baby</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Kaleem Shah, Inc. </td>
      <td data-title="Time">1:16.62 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Renee's Titan </td>
      <td data-title="Jockey">Tyler Baze</td>
      <td data-title="Trainer">Doug F. O'Neill</td>
      <td data-title="Owner">Zillah Reddam </td>
      <td data-title="Time">1:16.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Reneesgotzip </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Peter Miller </td>
      <td data-title="Owner">Lanni Family Trust </td>
      <td data-title="Time">1:15.25 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">California Nectar </td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Doug O'Neill</td>
      <td data-title="Owner">Suarez Racing, Inc. </td>
      <td data-title="Time">1:21.34 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Amen Hallelujah</td>
      <td data-title="Jockey">Chantal Sutherland</td>
      <td data-title="Trainer">Richard E. Dutrow, Jr.</td>
      <td data-title="Owner">IEAH Stables/Whizway Farms </td>
      <td data-title="Time">1:21.39 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Alpha Kitten </td>
      <td data-title="Jockey">Tyler Baze</td>
      <td data-title="Trainer">John W. Sadler</td>
      <td data-title="Owner">Ann &amp; Jerry Moss</td>
      <td data-title="Time">1:21.84 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Indian Blessing</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Patti &amp; Hal J. Earnhardt III </td>
      <td data-title="Time">1:19.89 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Jump On In </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">John W. Sadler</td>
      <td data-title="Owner">C R K Stable </td>
      <td data-title="Time">1:23.45 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Dance Daily </td>
      <td data-title="Jockey">Jon Court</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Bob &amp; Beverly Lewis</td>
      <td data-title="Time">1:23.34 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Sharp Lisa </td>
      <td data-title="Jockey">Tyler Baze</td>
      <td data-title="Trainer">Douglas F. O'Neill</td>
      <td data-title="Owner">Suarez Racing, et al. </td>
      <td data-title="Time">1:23.10 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Yearly Report </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Golden Eagle Farm</td>
      <td data-title="Time">1:21.11 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Elloluv </td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Craig Dollase</td>
      <td data-title="Owner">J. Paul Reddam</td>
      <td data-title="Time">1:23.03 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Dancing </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Jenine Sahadi</td>
      <td data-title="Owner">Howard J. Baker </td>
      <td data-title="Time">1:23.07 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Golden Ballet </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Jenine Sahadi</td>
      <td data-title="Owner">Team Valor &amp; Heiligbrodt Racing </td>
      <td data-title="Time">1:22.30 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Penny Blues </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">Edwin J. Gregson</td>
      <td data-title="Owner">Buffalo Wallet </td>
      <td data-title="Time">1:23.38 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Honest Lady </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Owner">Juddmonte Farms</td>
      <td data-title="Time">1:21.67 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Nijinsky's Passion </td>
      <td data-title="Jockey">Corey Black</td>
      <td data-title="Trainer">Tim Pinfield</td>
      <td data-title="Owner">Imagination Stable </td>
      <td data-title="Time">1:23.15 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Queen of Money </td>
      <td data-title="Jockey">David Flores</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Michael E. Pegram</td>
      <td data-title="Time">1:22.55 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Raw Gold </td>
      <td data-title="Jockey">Chris Antley</td>
      <td data-title="Trainer">David Hofmans</td>
      <td data-title="Owner">Ridder Thoroughbred Stable</td>
      <td data-title="Time">1:22.66 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Serena's Song</td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Bob &amp; Beverly Lewis</td>
      <td data-title="Time">1:21.45 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Tricky Code </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Gary F. Jones</td>
      <td data-title="Owner">Kallenberg Thoroughbred, Inc. </td>
      <td data-title="Time">1:22.16 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Fit To Lead </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Caesar Dominguez</td>
      <td data-title="Owner">Birnbaum, Walski, Walski, et al. </td>
      <td data-title="Time">1:22.55 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Looie Capote </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Eyal Klayman</td>
      <td data-title="Owner">Martin L. or Lois E. Gellman </td>
      <td data-title="Time">1:23.42 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Brazen </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Jerry M. Fanning</td>
      <td data-title="Owner">Olin B. Gentry</td>
      <td data-title="Time">1:23.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Fit to Scout </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Jack Van Berg</td>
      <td data-title="Owner">Robert M. Snell </td>
      <td data-title="Time">1:23.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Hot Novel </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">Fabio Nor</td>
      <td data-title="Owner">Joanne H. Nor </td>
      <td data-title="Time">1:22.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Goodbye Halo</td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">Charles E. Whittingham</td>
      <td data-title="Owner">Arthur B. Hancock III</td>
      <td data-title="Time">1:23.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Very Subtle</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Melvin F. Stute</td>
      <td data-title="Owner">C. Grinstead &amp; B. Rochelle </td>
      <td data-title="Time">1:22.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Sari's Heroine </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Melvin F. Stute</td>
      <td data-title="Owner">C. Grinstead &amp; B. Rochelle </td>
      <td data-title="Time">1:23.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Wising Up </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Sam E. Stevens </td>
      <td data-title="Time">1:23.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Gene's Lady </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">M/M Eugene V. Klein</td>
      <td data-title="Time">1:23.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Boo La Boo </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Melvin F. Stute</td>
      <td data-title="Owner">Cale, Liebau &amp; Davis </td>
      <td data-title="Time">1:23.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">A Lucky Sign </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Bruce Headley</td>
      <td data-title="Owner">Headley, Johnston, et al. </td>
      <td data-title="Time">1:23.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Flying Partner </td>
      <td data-title="Jockey">Ray Sibille</td>
      <td data-title="Trainer">Charles E. Whittingham</td>
      <td data-title="Owner">Buckland Farm</td>
      <td data-title="Time">1:22.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Past Forgetting </td>
      <td data-title="Jockey">Sandy Hawley</td>
      <td data-title="Trainer">Gordon C. Campbell</td>
      <td data-title="Owner">Bernard J. Ridder</td>
      <td data-title="Time">1:22.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Table Hands </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Willard L. Proctor</td>
      <td data-title="Owner">Brant</td>
      <td data-title="Time">1:22.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Terlingua</td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">B. Beal &amp; L. R. French </td>
      <td data-title="Time">1:21.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Grenzen </td>
      <td data-title="Jockey">Darrel McHargue</td>
      <td data-title="Trainer">Loren Rettele</td>
      <td data-title="Owner">D. Delaplaine/J. Schaffer/J. Woolsey </td>
      <td data-title="Time">1:22.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Wavy Waves </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Julius E. Tinsley, Jr.</td>
      <td data-title="Owner">Fred W. Hooper</td>
      <td data-title="Time">1:22.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Daisy Do </td>
      <td data-title="Jockey">Sandy Hawley</td>
      <td data-title="Trainer">Hector O. Palma</td>
      <td data-title="Owner">M/M. Robert Levey </td>
      <td data-title="Time">1:22.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Raise Your Skirts </td>
      <td data-title="Jockey">William Mahorney</td>
      <td data-title="Trainer">Barney Willis</td>
      <td data-title="Owner">Jacqueline Woolsey &amp; Virginia Willis </td>
      <td data-title="Time">1:22.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Modus Vivendi </td>
      <td data-title="Jockey">Donald Pierce</td>
      <td data-title="Trainer">Gordon C. Campbell</td>
      <td data-title="Owner">Bernard J. Ridder</td>
      <td data-title="Time">1:22.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Tallahto </td>
      <td data-title="Jockey">Jorge Tejeira</td>
      <td data-title="Trainer">Charles E. Whittingham</td>
      <td data-title="Owner">Elizabeth A. Keck</td>
      <td data-title="Time">1:21.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Susan's Girl</td>
      <td data-title="Jockey">Victor Tejeda</td>
      <td data-title="Trainer">John W. Russell</td>
      <td data-title="Owner">Fred W. Hooper</td>
      <td data-title="Time">1:21.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Turkish Trousers</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Charles E. Whittingham</td>
      <td data-title="Owner">Elizabeth A. Keck</td>
      <td data-title="Time">1:23.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Opening Bid </td>
      <td data-title="Jockey">Donald Pierce</td>
      <td data-title="Trainer">Lou Glauburg</td>
      <td data-title="Owner">M/M John J. Elmore </td>
      <td data-title="Time">1:22.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Poona Downs </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Lou Glauburg</td>
      <td data-title="Owner">M/M John J. Elmore </td>
      <td data-title="Time">1:24.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Allie's Serenade </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">William J. Hirsch</td>
      <td data-title="Owner">Mereworth Farm</td>
      <td data-title="Time">1:22.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Mira Femme </td>
      <td data-title="Jockey">Walter Blum</td>
      <td data-title="Trainer">Irv Guiney</td>
      <td data-title="Owner">Verne H. Winchell</td>
      <td data-title="Time">1:24.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Spearfish </td>
      <td data-title="Jockey">Donald Pierce</td>
      <td data-title="Trainer">William A. Peterson</td>
      <td data-title="Owner">Michael H. Silver </td>
      <td data-title="Time">1:16.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Respected </td>
      <td data-title="Jockey">Manuel Ycaza</td>
      <td data-title="Trainer">James W. Maloney</td>
      <td data-title="Owner">William H. Perry</td>
      <td data-title="Time">1:16.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Face The Facts </td>
      <td data-title="Jockey">Manuel Ycaza</td>
      <td data-title="Trainer">James W. Maloney</td>
      <td data-title="Owner">William H. Perry</td>
      <td data-title="Time">1:15.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1963 </td>
      <td data-title="Winner">Nalee </td>
      <td data-title="Jockey">Kenneth Church</td>
      <td data-title="Trainer">Pete Groos</td>
      <td data-title="Owner">Edith Ferguson Cardy</td>
      <td data-title="Time">1:16.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">Don't Linger </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Mesh Tenney</td>
      <td data-title="Owner">Rex C. Ellsworth</td>
      <td data-title="Time">1:17.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1961 </td>
      <td data-title="Winner">Het's Pet </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">J. G. Evenson</td>
      <td data-title="Owner">M/M John Eyraud </td>
      <td data-title="Time">1:16.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1960 </td>
      <td data-title="Winner">Solid Thought </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">J. Lee Mosbacher</td>
      <td data-title="Owner">Mosbacher &amp; Leach </td>
      <td data-title="Time">1:18.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1959 </td>
      <td data-title="Winner">Silver Spoon</td>
      <td data-title="Jockey">Raymond York</td>
      <td data-title="Trainer">Robert L. Wheeler</td>
      <td data-title="Owner">C. V. Whitney</td>
      <td data-title="Time">1:17.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1958 </td>
      <td data-title="Winner">Zev's Joy </td>
      <td data-title="Jockey">George Taniguchi</td>
      <td data-title="Trainer">Roy H. Highley</td>
      <td data-title="Owner">Harvey F. Dick </td>
      <td data-title="Time">1:17.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1957 </td>
      <td data-title="Winner">Sully's Trail </td>
      <td data-title="Jockey">George Taniguchi</td>
      <td data-title="Trainer">Warren Stute</td>
      <td data-title="Owner">Mrs. Ann Peppers </td>
      <td data-title="Time">1:10.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1956 </td>
      <td data-title="Winner">Neva T. </td>
      <td data-title="Jockey">George Taniguchi</td>
      <td data-title="Trainer">R. L. Anderson</td>
      <td data-title="Owner">Chastain &amp; Elmore </td>
      <td data-title="Time">1:11.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1955 </td>
      <td data-title="Winner">In Reserve </td>
      <td data-title="Jockey">Johnny Longden</td>
      <td data-title="Trainer">Vance Longden</td>
      <td data-title="Owner">Alberta Ranches, Ltd.</td>
      <td data-title="Time">1:22.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1954 </td>
      <td data-title="Winner">Sweet As Honey </td>
      <td data-title="Jockey">Johnny Longden</td>
      <td data-title="Trainer">Ellwood B. Johnston</td>
      <td data-title="Owner">M/M. Ellwood B. Johnston </td>
      <td data-title="Time">1:23.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1953 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey"></td>
      <td data-title="Trainer"></td>
      <td data-title="Owner"></td>
      <td data-title="Time"></td>
    </tr>
    <tr>
      <td data-title="Year">1952 </td>
      <td data-title="Winner">Lap Full </td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Horace A. Jones</td>
      <td data-title="Owner">Calumet Farm</td>
      <td data-title="Time">1:11.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1952 </td>
      <td data-title="Winner">Last Greetings </td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">C. Ralph West</td>
      <td data-title="Owner">Clifford Mooers </td>
      <td data-title="Time">1:11.20 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}