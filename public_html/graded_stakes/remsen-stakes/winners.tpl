<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>        
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Catholic Boy </td>
      <td data-title="Jockey">Manuel Franco </td>
      <td data-title="Trainer">Jonathan Thomas </td>
      <td data-title="Owner">Robert V. LaPenta </td>

      <td data-title="Time">1:52.50 </td>

    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Mo Town </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Anthony W. Dutrow</td>
      <td data-title="Owner">M. Tabor, D. Smith, Team D </td>

      <td data-title="Time">1:51.58 </td>

    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Mohaymen</td>
      <td data-title="Jockey">Junior Alvarado</td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Owner">Shadwell Stable</td>

      <td data-title="Time">1:50.69 </td>

    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Leave the Light On </td>
      <td data-title="Jockey">Jose L. Ortiz</td>
      <td data-title="Trainer">Chad C. Brown</td>
      <td data-title="Owner">Klaravich Stables/Lawrence </td>

      <td data-title="Time">1:51.06 </td>

    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Honor Code</td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">C. R. McGaughey III</td>
      <td data-title="Owner">Lane's End Farm/Dell Ridge Racing LLC </td>

      <td data-title="Time">1:52.92 </td>

    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Overanalyze </td>
      <td data-title="Jockey">Ramon Dominguez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Repole Stable </td>

      <td data-title="Time">1:50.13 </td>

    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">O'Prado Again </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Dale Romans</td>
      <td data-title="Owner">Donegal Racing </td>

      <td data-title="Time">1:52.07 </td>

    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">To Honor and Serve </td>
      <td data-title="Jockey">John Velazquez </td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Owner">Live Oak Plantation </td>

      <td data-title="Time">1:50.03 </td>

    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Buddy's Saint</td>
      <td data-title="Jockey">Jose Lezcano</td>
      <td data-title="Trainer">Bruce N. Levine</td>
      <td data-title="Owner">Kingfield Stables </td>

      <td data-title="Time">1:52.95 </td>

    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Old Fashioned</td>
      <td data-title="Jockey">Ramon Dominguez </td>
      <td data-title="Trainer">J. Larry Jones</td>
      <td data-title="Owner">Fox Hill Farms, Inc. </td>

      <td data-title="Time">1:50.33 </td>

    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Court Vision</td>
      <td data-title="Jockey">Eibar Coa</td>
      <td data-title="Trainer">William I. Mott </td>
      <td data-title="Owner">WinStar Farm</td>

      <td data-title="Time">1:52.48 </td>

    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Nobiz Like Shobiz</td>
      <td data-title="Jockey">Cornelio Velasquez</td>
      <td data-title="Trainer">Barclay Tagg</td>
      <td data-title="Owner">Elizabeth Valando</td>

      <td data-title="Time">1:48.82 </td>

    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Bluegrass Cat</td>
      <td data-title="Jockey">John Velazquez </td>
      <td data-title="Trainer">Todd A. Pletcher </td>
      <td data-title="Owner">WinStar Farm </td>

      <td data-title="Time">1:52.20 </td>

    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Rockport Harbor </td>
      <td data-title="Jockey">Stewart Elliott</td>
      <td data-title="Trainer">John Servis</td>
      <td data-title="Owner">Fox Hill Farms, Inc. </td>

      <td data-title="Time">1:48.80 </td>

    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Read the Footnotes </td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Richard A. Violette Jr.</td>
      <td data-title="Owner">Klaravich Stables, Inc. </td>

      <td data-title="Time">1:50.60 </td>

    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Toccet </td>
      <td data-title="Jockey">Jorge Chavez</td>
      <td data-title="Trainer">John F. Scanlan </td>
      <td data-title="Owner">Dan Borislow</td>

      <td data-title="Time">1:50.40 </td>

    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Saarland </td>
      <td data-title="Jockey">John R. Velazquez </td>
      <td data-title="Trainer">C. R. McGaughey III </td>
      <td data-title="Owner">Cynthia Phipps </td>

      <td data-title="Time">1:51.20 </td>

    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Windsor Castle </td>
      <td data-title="Jockey">Robbie Davis</td>
      <td data-title="Trainer">Frank A. Alexander</td>
      <td data-title="Owner">Frank A. Alexander </td>

      <td data-title="Time">1:51.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Greenwood Lake </td>
      <td data-title="Jockey">Jean-Luc Samyn</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Owner">Dee Conway et al. </td>

      <td data-title="Time">1:50.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Comeonmom </td>
      <td data-title="Jockey">Joe Bravo</td>
      <td data-title="Trainer">Robert A. Barbara </td>
      <td data-title="Owner">Sabine Stable </td>

      <td data-title="Time">1:49.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Coronado's Quest</td>
      <td data-title="Jockey">Mike Smith</td>
      <td data-title="Trainer">C. R. McGaughey III </td>
      <td data-title="Owner">Stuart S. Janney III </td>

      <td data-title="Time">1:52.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">The Silver Move </td>
      <td data-title="Jockey">Richard Migliore</td>
      <td data-title="Trainer">Linda L. Rice</td>
      <td data-title="Owner">Earl Silver </td>

      <td data-title="Time">1:53.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Tropicool </td>
      <td data-title="Jockey">Jorge Chavez </td>
      <td data-title="Trainer">John C. Kimmel</td>
      <td data-title="Owner">Kenneth Ellenberg </td>

      <td data-title="Time">1:50.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Thunder Gulch</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Michael Tabor </td>

      <td data-title="Time">1:53.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Go for Gin</td>
      <td data-title="Jockey">Jerry Bailey </td>
      <td data-title="Trainer">Nick Zito </td>
      <td data-title="Owner">William J. Condren </td>

      <td data-title="Time">1:52.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Silver of Silver </td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">Stanley R. Shapoff </td>
      <td data-title="Owner">Chevalier Stable </td>

      <td data-title="Time">1:50.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Pine Bluff</td>
      <td data-title="Jockey">Craig Perret</td>
      <td data-title="Trainer">Thomas Bohannan</td>
      <td data-title="Owner">Loblolly Stable</td>

      <td data-title="Time">1:50.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Scan </td>
      <td data-title="Jockey">Jerry Bailey </td>
      <td data-title="Trainer">Flint S. Schulhofer</td>
      <td data-title="Owner">William Haggin Perry</td>

      <td data-title="Time">1:52.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Yonder </td>
      <td data-title="Jockey">Eddie Maple</td>
      <td data-title="Trainer">Woody Stephens</td>
      <td data-title="Owner">Claiborne Farm</td>

      <td data-title="Time">1:51.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Fast Play </td>
      <td data-title="Jockey">Angel Cordero Jr.</td>
      <td data-title="Trainer">C. R. McGaughey III </td>
      <td data-title="Owner">Ogden Phipps</td>

      <td data-title="Time">1:50.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Batty </td>
      <td data-title="Jockey">Jose Santos</td>
      <td data-title="Trainer">Suzanne H. Jenkins </td>
      <td data-title="Owner">Joseph Daniero </td>

      <td data-title="Time">1:52.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Java Gold</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">MacKenzie Miller</td>
      <td data-title="Owner">Rokeby Stable</td>

      <td data-title="Time">1:49.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Pillaster </td>
      <td data-title="Jockey">Angel Cordero Jr. </td>
      <td data-title="Trainer">Leroy Jolley</td>
      <td data-title="Owner">Peter M. Brant</td>

      <td data-title="Time">1:49.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Stone White</td>
      <td data-title="Jockey">Robbie Davis </td>
      <td data-title="Trainer">Gilbert Puentes </td>
      <td data-title="Owner">Gilbert Puentes </td>

      <td data-title="Time">1:53.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Dr. Carter </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">John M. Veitch</td>
      <td data-title="Owner">Frances A. Genter</td>

      <td data-title="Time">1:49.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Pax In Bello </td>
      <td data-title="Jockey">Jeffrey Fell</td>
      <td data-title="Trainer">Steve Jerkens </td>
      <td data-title="Owner">Mrs. Arnold Willcox </td>

      <td data-title="Time">1:50.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Laser Light </td>
      <td data-title="Jockey">Eddie Maple </td>
      <td data-title="Trainer">Patrick J. Kelly </td>
      <td data-title="Owner">Live Oak Racing </td>

      <td data-title="Time">1:50.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Pleasant Colony</td>
      <td data-title="Jockey">Vincent Bracciale Jr.</td>
      <td data-title="Trainer">P. O'Donnell Lee </td>
      <td data-title="Owner">Buckland Farm</td>

      <td data-title="Time">1:50.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Plugged Nickle</td>
      <td data-title="Jockey">Buck Thornburg </td>
      <td data-title="Trainer">Thomas J. Kelly</td>
      <td data-title="Owner">John M. Schiff</td>

      <td data-title="Time">1:50.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Instrument Landing </td>
      <td data-title="Jockey">Jeffrey Fell </td>
      <td data-title="Trainer">David A. Whiteley</td>
      <td data-title="Owner">Pen-Y-Bryn Farm</td>

      <td data-title="Time">1:50.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Believe It</td>
      <td data-title="Jockey">Eddie Maple </td>
      <td data-title="Trainer">Woody Stephens </td>
      <td data-title="Owner">Hickory Tree Stable</td>

      <td data-title="Time">1:47.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Royal Ski</td>
      <td data-title="Jockey">Jack Kurtz </td>
      <td data-title="Trainer">John J. Lenzini Jr.</td>
      <td data-title="Owner">Gerry Cheevers</td>

      <td data-title="Time">1:50.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Hang Ten </td>
      <td data-title="Jockey">Laffit Pincay Jr.</td>
      <td data-title="Trainer">Woody Stephens </td>
      <td data-title="Owner">Mill House Stable</td>

      <td data-title="Time">1:49.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">El Pitirre </td>
      <td data-title="Jockey">Mike Venezia</td>
      <td data-title="Trainer">Lazaro S. Barrera</td>
      <td data-title="Owner">Enrique Ubarri </td>

      <td data-title="Time">1:49.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Heavy Mayonnaise </td>
      <td data-title="Jockey">Chuck Baltazar</td>
      <td data-title="Trainer">David Erb</td>
      <td data-title="Owner">William Trotter II </td>

      <td data-title="Time">1:51.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Kinsman Hope </td>
      <td data-title="Jockey">Eddie Maple </td>
      <td data-title="Trainer">John L. Cotter </td>
      <td data-title="Owner">Kinship Stable </td>

      <td data-title="Time">1:37.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Key To The Mint</td>
      <td data-title="Jockey">Braulio Baeza</td>
      <td data-title="Trainer">J. Elliott Burch</td>
      <td data-title="Owner">Rokeby Stable</td>

      <td data-title="Time">1:36.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Jim French</td>
      <td data-title="Jockey">Angel Cordero Jr. </td>
      <td data-title="Trainer">John P. Campo</td>
      <td data-title="Owner">Frank Caldwell </td>

      <td data-title="Time">1:36.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Protanto </td>
      <td data-title="Jockey">Jorge Velasquez </td>
      <td data-title="Trainer">MacKenzie Miller </td>
      <td data-title="Owner">Cragwood Stables</td>

      <td data-title="Time">1:35.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Palauli </td>
      <td data-title="Jockey">Larry Adams</td>
      <td data-title="Trainer">Frank Y. Whiteley, Jr.</td>
      <td data-title="Owner">Powhatan Stable</td>

      <td data-title="Time">1:36.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Salerno</td>
      <td data-title="Jockey">Manuel Ycaza</td>
      <td data-title="Trainer">William H. Turner, Jr.</td>
      <td data-title="Owner">James P. Mills </td>

      <td data-title="Time">1:38.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Damascus</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Frank Y. Whiteley, Jr. </td>
      <td data-title="Owner">Edith W. Bancroft</td>

      <td data-title="Time">1:37.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Gary G. </td>
      <td data-title="Jockey">Braulio Baeza </td>
      <td data-title="Trainer">Rudolph L. Coyne </td>
      <td data-title="Owner">Harbor View Farm</td>

      <td data-title="Time">1:36.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Sum Up </td>
      <td data-title="Jockey">Don Pierce</td>
      <td data-title="Trainer">Edward A. Neloy</td>
      <td data-title="Owner">Elmendorf Farm</td>

      <td data-title="Time">1:33.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1963 </td>
      <td data-title="Winner">Northern Dancer</td>
      <td data-title="Jockey">Manuel Ycaza </td>
      <td data-title="Trainer">Horatio Luro</td>
      <td data-title="Owner">Windfields Farm</td>

      <td data-title="Time">1:35.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">Rocky Link </td>
      <td data-title="Jockey">John Rotz</td>
      <td data-title="Trainer">Stephen A. DiMauro</td>
      <td data-title="Owner">Golden Triangle Stable </td>

      <td data-title="Time">1:36.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1961 </td>
      <td data-title="Winner">Figaro Bob </td>
      <td data-title="Jockey">John Rotz </td>
      <td data-title="Trainer">Eugene Jacobs </td>
      <td data-title="Owner">Alexander J. Ostriker </td>

      <td data-title="Time">1:36.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1960 </td>
      <td data-title="Winner">Carry Back</td>
      <td data-title="Jockey">Johnny Sellers</td>
      <td data-title="Trainer">Jack A. Price</td>
      <td data-title="Owner">Katherine Price </td>

      <td data-title="Time">1:36.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1959 </td>
      <td data-title="Winner">Victoria Park</td>
      <td data-title="Jockey">Eric Guerin</td>
      <td data-title="Trainer">Horatio Luro </td>
      <td data-title="Owner">Windfields Farm </td>

      <td data-title="Time">1:37.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1958 </td>
      <td data-title="Winner">Atoll </td>
      <td data-title="Jockey">John Ruane</td>
      <td data-title="Trainer">Raymond Metcalf</td>
      <td data-title="Owner">Elkcam Stable (Mackle brothers) </td>

      <td data-title="Time">1:44.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1957 </td>
      <td data-title="Winner">Misty Flight </td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">James E. Fitzsimmons</td>
      <td data-title="Owner">Wheatley Stable</td>

      <td data-title="Time">1:45.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1956 </td>
      <td data-title="Winner">Ambehaving </td>
      <td data-title="Jockey">Logan Batcheller </td>
      <td data-title="Trainer">Dr. John Lee </td>
      <td data-title="Owner">Bohemia Stable</td>

      <td data-title="Time">1:45.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1955 </td>
      <td data-title="Winner">Nail </td>
      <td data-title="Jockey">Hedley Woodhouse</td>
      <td data-title="Trainer">George M. Odom</td>
      <td data-title="Owner">Josephine Widener Bigelow </td>

      <td data-title="Time">1:45.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1954 </td>
      <td data-title="Winner">Roman Patrol </td>
      <td data-title="Jockey">Douglas Dodson</td>
      <td data-title="Trainer">Joseph H. Pierce Jr.</td>
      <td data-title="Owner">Pin Oak Farm (J. S. Abercrombie) </td>

      <td data-title="Time">1:48.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1953 </td>
      <td data-title="Winner">Galdar </td>
      <td data-title="Jockey">Jimmy Nicols</td>
      <td data-title="Trainer">Edward M. O'Brien </td>
      <td data-title="Owner">Edward M. O'Brien </td>

      <td data-title="Time">1:46.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1952 </td>
      <td data-title="Winner">Social Outcast</td>
      <td data-title="Jockey">Eric Guerin </td>
      <td data-title="Trainer">William C. Winfrey</td>
      <td data-title="Owner">Alfred G. Vanderbilt</td>

      <td data-title="Time">1:45.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1951 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Owner">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>                
    </tr>
    <tr>
      <td data-title="Year">1950 </td>
      <td data-title="Winner">Repetoire </td>
      <td data-title="Jockey">Kenneth Church</td>
      <td data-title="Trainer">Albert Jensen </td>
      <td data-title="Owner">Nora A. Mikell </td>

      <td data-title="Time">1:11.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1949 </td>
      <td data-title="Winner">Lights Up </td>
      <td data-title="Jockey">Ovie Scurlock</td>
      <td data-title="Trainer">Winbert F. Mulholland</td>
      <td data-title="Owner">George D. Widener Jr.</td>

      <td data-title="Time">1:13.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1948 </td>
      <td data-title="Winner">Eternal World </td>
      <td data-title="Jockey">Ted Atkinson</td>
      <td data-title="Trainer">Frank Catrone</td>
      <td data-title="Owner">Allen T. Simmons</td>

      <td data-title="Time">1:00.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1947 </td>
      <td data-title="Winner">Big If </td>
      <td data-title="Jockey">Charles Givens </td>
      <td data-title="Trainer">James W. Maloney</td>
      <td data-title="Owner">Mrs. J. T. Maloney </td>

      <td data-title="Time">1:47.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1946 </td>
      <td data-title="Winner">Phalanx</td>
      <td data-title="Jockey">Ruperto Donoso</td>
      <td data-title="Trainer">Sylvester Veitch</td>
      <td data-title="Owner">C. V. Whitney</td>

      <td data-title="Time">1:45.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1945 </td>
      <td data-title="Winner">Lord Boswell</td>
      <td data-title="Jockey">Eric Guerin </td>
      <td data-title="Trainer">Tom Smith</td>
      <td data-title="Owner">Maine Chance Farm</td>

      <td data-title="Time">1:11.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1944 </td>
      <td data-title="Winner">Great Power</td>
      <td data-title="Jockey">Eddie Arcaro </td>
      <td data-title="Trainer">Preston M. Burch</td>
      <td data-title="Owner">Brookmeade Stable</td>

      <td data-title="Time">1:12.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1944 </td>
      <td data-title="Winner">War Jeep</td>
      <td data-title="Jockey">Albert Snider</td>
      <td data-title="Trainer">Tom Smith </td>
      <td data-title="Owner">Maine Chance Farm </td>

      <td data-title="Time">1:12.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1943-1 </td>
      <td data-title="Winner">Bellwether </td>
      <td data-title="Jockey">Johnny Longden</td>
      <td data-title="Trainer">Joseph A. Kramer </td>
      <td data-title="Owner">Cain Hoy Stable</td>

      <td data-title="Time">1:13.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1943-2 </td>
      <td data-title="Winner">Black Badge </td>
      <td data-title="Jockey">Wayne Wright</td>
      <td data-title="Trainer">Frank E. Childs</td>
      <td data-title="Owner">Abraham Hirschberg </td>

      <td data-title="Time">1:11.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1942 </td>
      <td data-title="Winner">Blue Swords</td>
      <td data-title="Jockey">Carroll Bierman</td>
      <td data-title="Trainer">Walter A. Kelley </td>
      <td data-title="Owner">Allen T. Simmons </td>

      <td data-title="Time">1:12.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1941 </td>
      <td data-title="Winner">Apache </td>
      <td data-title="Jockey">James Stout</td>
      <td data-title="Trainer">James E. Fitzsimmons </td>
      <td data-title="Owner">Belair Stud</td>

      <td data-title="Time">1:12.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1940 </td>
      <td data-title="Winner">Harvard Square</td>
      <td data-title="Jockey">Buddy Haas</td>
      <td data-title="Trainer">Max Hirsch</td>
      <td data-title="Owner">W. Arnold Hanger</td>

      <td data-title="Time">1:11.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1940 </td>
      <td data-title="Winner">Mettlesome</td>
      <td data-title="Jockey">Alfred Robertson</td>
      <td data-title="Trainer">Hugh L. Lafontaine </td>
      <td data-title="Owner">Brookmeade Stable </td>

      <td data-title="Time">1:11.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1939 </td>
      <td data-title="Winner">Camp Verde </td>
      <td data-title="Jockey">Don Meade</td>
      <td data-title="Trainer">Lewis T. Whitehill </td>
      <td data-title="Owner">T. P. Morgan </td>

      <td data-title="Time">1:11.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1938 </td>
      <td data-title="Winner">Johnstown</td>
      <td data-title="Jockey">James Stout </td>
      <td data-title="Trainer">James E. Fitzsimmons </td>
      <td data-title="Owner">Belair Stud </td>

      <td data-title="Time">1:11.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1937 </td>
      <td data-title="Winner">Bourbon King </td>
      <td data-title="Jockey">Charles Kurtsinger</td>
      <td data-title="Trainer">Duval A. Headley</td>
      <td data-title="Owner">Hal Price Headley</td>

      <td data-title="Time">1:12.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1936 </td>
      <td data-title="Winner">Clodion </td>
      <td data-title="Jockey">John Gilbert</td>
      <td data-title="Trainer">Walter A. Carter </td>
      <td data-title="Owner">Walter A. Carter </td>

      <td data-title="Time">1:12.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1935 </td>
      <td data-title="Winner">The Fighter </td>
      <td data-title="Jockey">Eddie Arcaro </td>
      <td data-title="Trainer">Robert McGarvey</td>
      <td data-title="Owner">Milky Way Farm Stable</td>

      <td data-title="Time">1:12.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1934 </td>
      <td data-title="Winner">Esposa</td>
      <td data-title="Jockey">Earl Porter </td>
      <td data-title="Trainer">Matthew P. Brady</td>
      <td data-title="Owner">Middleburg Stable (William Ziegler Jr.) </td>

      <td data-title="Time">1:13.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1933 </td>
      <td data-title="Winner">Sgt. Byrne </td>
      <td data-title="Jockey">Alfred Robertson </td>
      <td data-title="Trainer">James Ritchie </td>
      <td data-title="Owner">John Simonetti </td>

      <td data-title="Time">1:12.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1932 </td>
      <td data-title="Winner">Quel Jeu </td>
      <td data-title="Jockey">Jack Long </td>
      <td data-title="Trainer">George M. Odom </td>
      <td data-title="Owner">Arden Farm</td>

      <td data-title="Time">1:11.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1931 </td>
      <td data-title="Winner">Cambal </td>
      <td data-title="Jockey">Eddie Ambrose</td>
      <td data-title="Trainer">John J. Hastings </td>
      <td data-title="Owner">Mrs. George U. Harris </td>

      <td data-title="Time">1:13.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1930 </td>
      <td data-title="Winner">Vander Pool</td>
      <td data-title="Jockey">Whitey Abel</td>
      <td data-title="Trainer">D. R. "Puddin" McDaniel </td>
      <td data-title="Owner">Tennessee Stable (Agnes Allen) </td>

      <td data-title="Time">1:13.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1929 </td>
      <td data-title="Winner">Flying Heels</td>
      <td data-title="Jockey">Willie Kelsay</td>
      <td data-title="Trainer">Henry McDaniel</td>
      <td data-title="Owner">Gifford A. Cochran</td>

      <td data-title="Time">1:12.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1928 </td>
      <td data-title="Winner">Chatford </td>
      <td data-title="Jockey">Clarence Watters </td>
      <td data-title="Trainer">Andrew Walker </td>
      <td data-title="Owner">Dave Lederer </td>

      <td data-title="Time">1:12.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1927 </td>
      <td data-title="Winner">Excalibur </td>
      <td data-title="Jockey">George Ellis </td>
      <td data-title="Trainer">Clyde S. Phillips</td>
      <td data-title="Owner">Greentree Stable</td>

      <td data-title="Time">1:12.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1926 </td>
      <td data-title="Winner">Sweepster </td>
      <td data-title="Jockey">Laverne Fator</td>
      <td data-title="Trainer">Sam Hildreth</td>
      <td data-title="Owner">Rancocas Stable</td>

      <td data-title="Time">1:12.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1925 </td>
      <td data-title="Winner">Timmara </td>
      <td data-title="Jockey">Harold Thurber</td>
      <td data-title="Trainer">John O. Burttschell </td>
      <td data-title="Owner">T. W. O'Brien </td>

      <td data-title="Time">1:13.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1924 </td>
      <td data-title="Winner">Master Charlie</td>
      <td data-title="Jockey">George Babin </td>
      <td data-title="Trainer">Andrew G. Blakely </td>
      <td data-title="Owner">William Daniel </td>

      <td data-title="Time">1:11.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1923 </td>
      <td data-title="Winner">Ladkin</td>
      <td data-title="Jockey">Harold Thurber </td>
      <td data-title="Trainer">Louis Feustel</td>
      <td data-title="Owner">August Belmont Jr.</td>

      <td data-title="Time">1:12.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1922 </td>
      <td data-title="Winner">Tall Timber </td>
      <td data-title="Jockey">James Butwell</td>
      <td data-title="Trainer">T. J. Healey</td>
      <td data-title="Owner">Richard T. Wilson Jr.</td>

      <td data-title="Time">1:12.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1921 </td>
      <td data-title="Winner">Missionary </td>
      <td data-title="Jockey">Andy Schuttinger</td>
      <td data-title="Trainer">Kimball Patterson </td>
      <td data-title="Owner">Lexington Stable (E. F. Simms &amp; H. W. Oliver) </td>

      <td data-title="Time">1:13.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1920 </td>
      <td data-title="Winner">Grey Lag</td>
      <td data-title="Jockey">Lavelle Ensor</td>
      <td data-title="Trainer">Sam Hildreth </td>
      <td data-title="Owner">Sam Hildreth </td>

      <td data-title="Time">1:13.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1919 </td>
      <td data-title="Winner">Pilgrim </td>
      <td data-title="Jockey">Charles Fairbrother </td>
      <td data-title="Trainer">Thomas Welsh</td>
      <td data-title="Owner">Joseph E. Widener</td>

      <td data-title="Time">1:12.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1918 </td>
      <td data-title="Winner">War Pennant </td>
      <td data-title="Jockey">Johnny Loftus</td>
      <td data-title="Trainer">Walter B. Jennings</td>
      <td data-title="Owner">A. Kingsley Macomber</td>

      <td data-title="Time">1:13.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1909 1917</td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Owner">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>                
    </tr>
    <tr>
      <td data-title="Year">1908 </td>
      <td data-title="Winner">The Turk </td>
      <td data-title="Jockey">Carroll Shilling</td>
      <td data-title="Trainer">Richard O. Miller</td>
      <td data-title="Owner">Silver Brook Farm </td>

      <td data-title="Time">1:08.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1907 </td>
      <td data-title="Winner">King Cobalt </td>
      <td data-title="Jockey">Eddie Dugan</td>
      <td data-title="Trainer">Algernon W. Claxon </td>
      <td data-title="Owner">Brownleigh Park Stable </td>

      <td data-title="Time">1:07.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1906 </td>
      <td data-title="Winner">Frank Gill </td>
      <td data-title="Jockey">Joe Notter</td>
      <td data-title="Trainer">John I. Smith </td>
      <td data-title="Owner">J. L. McGinnis </td>

      <td data-title="Time">1:08.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1905 </td>
      <td data-title="Winner">Jacobite </td>
      <td data-title="Jockey">Joseph A. Jones </td>
      <td data-title="Trainer">A. Jack Joyner</td>
      <td data-title="Owner">Sydney Paget</td>

      <td data-title="Time">1:07.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1904 </td>
      <td data-title="Winner">Dandelion </td>
      <td data-title="Jockey">Willie Davis </td>
      <td data-title="Trainer">John E. Madden</td>
      <td data-title="Owner">Francis R. Hitchcock</td>

      <td data-title="Time">1:06.80 </td>

    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}