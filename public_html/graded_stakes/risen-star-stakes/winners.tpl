<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>        
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Bravazo</td>
      <td data-title="Jockey">Miguel Mena </td>
      <td data-title="Trainer">D. Wayne Lukas </td>
      <td data-title="Owner">Calumet Farm </td>
      <td data-title="Time">1:42.95 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Girvin</td>
      <td data-title="Jockey">Brian Hernandez, Jr. </td>
      <td data-title="Trainer">Joe Sharp </td>
      <td data-title="Owner">Brad Grady </td>
      <td data-title="Time">1:43.08 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Gun Runner</td>
      <td data-title="Jockey">Florent Geroux</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">Winchell Thoroughbreds/Three Chimneys Farm</td>
      <td data-title="Time">1:43.94 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">International Star </td>
      <td data-title="Jockey">Miguel Mena</td>
      <td data-title="Trainer">Michael J. Maker</td>
      <td data-title="Owner">Sarah &amp; Ken Ramsey </td>
      <td data-title="Time">1:43.82 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Intense Holiday </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Todd Pletcher </td>
      <td data-title="Owner">Brereton C. Jones</td>
      <td data-title="Time">1:43.86 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Ive Struck a Nerve </td>
      <td data-title="Jockey">James Graham</td>
      <td data-title="Trainer">J. Keith Desormeaux</td>
      <td data-title="Owner">Big Chief Racing </td>
      <td data-title="Time">1:44.52 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">El Padrino </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Owner">Let's Go Stable </td>
      <td data-title="Time">1:42.96 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Mucho Macho Man</td>
      <td data-title="Jockey">Rajiv Maragh</td>
      <td data-title="Trainer">Katherine Ritvo</td>
      <td data-title="Owner">Reeves Thoroughbred Racing/<br>
        Dream Team Racing Stable </td>
      <td data-title="Time">1:43.98 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Discreetly Mine</td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Owner">E. Paul Robsham Stables, LLC </td>
      <td data-title="Time">1:44.88 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Friesan Fire</td>
      <td data-title="Jockey">Gabriel Saez</td>
      <td data-title="Trainer">J. Larry Jones</td>
      <td data-title="Owner">Vinery</td>
      <td data-title="Time">1:45.11 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Pyro</td>
      <td data-title="Jockey">Shaun Bridgmohan</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">Winchell Thoroughbreds</td>
      <td data-title="Time">1:44.68 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Notional</td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Doug O'Neill</td>
      <td data-title="Owner">J. Paul Reddam</td>
      <td data-title="Time">1:44.18 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Lawyer Ron</td>
      <td data-title="Jockey">John McKee</td>
      <td data-title="Trainer">Robert E. Holthus</td>
      <td data-title="Owner">James T. Hines, Jr. </td>
      <td data-title="Time">1:43.13 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Scipion </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Patrick L. Biancone</td>
      <td data-title="Owner">Virginia Kraft Payson</td>
      <td data-title="Time">1:44.54 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Gradepoint </td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Neil J. Howard</td>
      <td data-title="Owner">Mt. Brilliant Stable/W. S. Farish</td>
      <td data-title="Time">1:45.36 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Badge of Silver</td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Ronny Werner</td>
      <td data-title="Owner">Ken &amp; Sarah Ramsey</td>
      <td data-title="Time">1:42.99 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Repent</td>
      <td data-title="Jockey">Tony D'Amico</td>
      <td data-title="Trainer">Kenneth McPeek</td>
      <td data-title="Owner">Select Stable </td>
      <td data-title="Time">1:43.86 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Dollar Bill</td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Dallas Stewart</td>
      <td data-title="Owner">Gary &amp; Mary West </td>
      <td data-title="Time">1:43.45 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Exchange Rate </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Padua Stables</td>
      <td data-title="Time">1:44.25 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Ecton Park</td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">W. Elliott Walden</td>
      <td data-title="Owner">Mark H. Stanley </td>
      <td data-title="Time">1:44.83 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Comic Strip</td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">Neil J. Howard</td>
      <td data-title="Owner">Elkins/Farish/Humphrey </td>
      <td data-title="Time">1:44.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Open Forum </td>
      <td data-title="Jockey">Donna Barton</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Bob &amp; Beverly Lewis</td>
      <td data-title="Time">1:44.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Zarbs Magic</td>
      <td data-title="Jockey">E. J. Perrodin</td>
      <td data-title="Trainer">Bret Thomas</td>
      <td data-title="Owner">Foxwood Plantation </td>
      <td data-title="Time">1:42.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1995</td>
      <td data-title="Winner">Knockadoon </td>
      <td data-title="Jockey">Willie Martinez</td>
      <td data-title="Trainer">Anthony Reinstedler</td>
      <td data-title="Owner">William K. Warren, Jr. </td>
      <td data-title="Time">1:45.44 </td>
    </tr>
    <tr>
      <td data-title="Year">1995</td>
      <td data-title="Winner">Beavers Nose </td>
      <td data-title="Jockey">K. Bourque</td>
      <td data-title="Trainer"></td>
      <td data-title="Owner"></td>
      <td data-title="Time">1:45.22 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Fly Cry </td>
      <td data-title="Jockey">R. Ardoin</td>
      <td data-title="Trainer"></td>
      <td data-title="Owner">Al Prats &amp; Wayne Simpson </td>
      <td data-title="Time">1:43.02 </td>
    </tr>
    <tr>
      <td data-title="Year">1993</td>
      <td data-title="Winner">Dixieland Heat</td>
      <td data-title="Jockey">Randy Romero</td>
      <td data-title="Trainer">Gerald Romero</td>
      <td data-title="Owner">Leland Cook </td>
      <td data-title="Time">1:43.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1993</td>
      <td data-title="Winner">Dry Bean </td>
      <td data-title="Jockey">Aaron Gryder</td>
      <td data-title="Trainer"></td>
      <td data-title="Owner"></td>
      <td data-title="Time">1:43.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Line in the Sand </td>
      <td data-title="Jockey">Shane Romero</td>
      <td data-title="Trainer"></td>
      <td data-title="Owner">Lane's End Farm</td>
      <td data-title="Time">1:45.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Big Courage </td>
      <td data-title="Jockey">Tammy Fox</td>
      <td data-title="Trainer"></td>
      <td data-title="Owner"></td>
      <td data-title="Time">1:46.70 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Genuine Meaning </td>
      <td data-title="Jockey">J. Hirdes</td>
      <td data-title="Trainer"></td>
      <td data-title="Owner"></td>
      <td data-title="Time">1:40.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1989</td>
      <td data-title="Winner">Nooo Problema </td>
      <td data-title="Jockey">Shane Romero</td>
      <td data-title="Trainer"></td>
      <td data-title="Owner"></td>
      <td data-title="Time">1:42.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1989</td>
      <td data-title="Winner">Dispersal </td>
      <td data-title="Jockey">B. Walker</td>
      <td data-title="Trainer"></td>
      <td data-title="Owner"></td>
      <td data-title="Time">1:42.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Risen Star</td>
      <td data-title="Jockey">Shane Romero</td>
      <td data-title="Trainer">Louie Roussel III</td>
      <td data-title="Owner">Louie Roussel III </td>
      <td data-title="Time">1:40.00 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}