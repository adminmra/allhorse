<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2019 </td>
      <td data-title="Winner">War of Will </td>
      <td data-title="Jockey">Tyler Gaffalione</td>
      <td data-title="Trainer">Mark E. Casse</td>
      <td data-title="Owner">Gary Barber </td>
      <td data-title="Time">1:43.44 </td>
    </tr>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Instilled Regard </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Jerry Hollendorfer</td>
      <td data-title="Owner">OXO Equine LLC </td>
      <td data-title="Time">1:42.59 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Guest Suite </td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Neil J. Howard </td>
      <td data-title="Owner">Farish, W.S. and Kilroy, Lora Jean </td>
      <td data-title="Time">1:43.20 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Mo Tom </td>
      <td data-title="Jockey">Corey Lanerie</td>
      <td data-title="Trainer">Thomas M. Amoss</td>
      <td data-title="Owner">G M B Racing </td>
      <td data-title="Time">1:43.18 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">International Star </td>
      <td data-title="Jockey">Miguel Mena</td>
      <td data-title="Trainer">Michael J. Maker</td>
      <td data-title="Owner">Kenneth and Sarah Ramsey</td>
      <td data-title="Time">1:43.62 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Vicar's In Trouble </td>
      <td data-title="Jockey">Rosie Napravnik</td>
      <td data-title="Trainer">Mike Maker</td>
      <td data-title="Owner">Kenneth and Sarah Ramsey</td>
      <td data-title="Time">1:42.57 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Oxbow</td>
      <td data-title="Jockey">Jon Court</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Bluegrass Hall</td>
      <td data-title="Time">1:43.30 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Mr. Bowling</td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">J. Larry Jones</td>
      <td data-title="Owner">Brereton Jones</td>
      <td data-title="Time">1:43.49 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Wilkinson</td>
      <td data-title="Jockey">Garrett K. Gomez</td>
      <td data-title="Trainer">Neil J. Howard</td>
      <td data-title="Owner">Gaillardia Racing LLC </td>
      <td data-title="Time">1:40.97 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Ron the Greek</td>
      <td data-title="Jockey">James Graham </td>
      <td data-title="Trainer">Thomas M. Amoss</td>
      <td data-title="Owner">Jack T. Hammer </td>
      <td data-title="Time">1:40.09 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Friesan Fire</td>
      <td data-title="Jockey">Gabriel Saez</td>
      <td data-title="Trainer">J. Larry Jones</td>
      <td data-title="Owner">Vinery</td>
      <td data-title="Time">1:37.67 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Z Fortune </td>
      <td data-title="Jockey">Shaun Bridgmohan</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">Zayat Stables </td>
      <td data-title="Time">1:37.79 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Hard Spun</td>
      <td data-title="Jockey">Mario Pino</td>
      <td data-title="Trainer">J. Larry Jones</td>
      <td data-title="Owner">Fox Hill Farms</td>
      <td data-title="Time">1:37.87 </td>
    </tr>
    <tr>
      <td data-title="Year">2006</td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Owner">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>                  
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Storm Surge </td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Dallas Stewart</td>
      <td data-title="Owner">Overbrook Farm</td>
      <td data-title="Time">1:39.34 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Fire Slam </td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">David M. Carroll</td>
      <td data-title="Owner">Stan Fulton </td>
      <td data-title="Time">1:38.48 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Saintly Look </td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">Dallas Stewart</td>
      <td data-title="Owner">William A. Carl </td>
      <td data-title="Time">1:37.62 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Easyfromthegitgo </td>
      <td data-title="Jockey">Donnie Meche</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">J. Cassels/R. Zollars </td>
      <td data-title="Time">1:37.98 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Sam Lord's Castle </td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Josie Carroll</td>
      <td data-title="Owner">Eugene Melnyk</td>
      <td data-title="Time">1:37.98 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Noble Ruler </td>
      <td data-title="Jockey">Larry Melancon</td>
      <td data-title="Trainer">Niall O'Callaghan</td>
      <td data-title="Owner">Fsim Racing Stables </td>
      <td data-title="Time">1:39.11 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Some Actor </td>
      <td data-title="Jockey">Eddie Martin Jr.</td>
      <td data-title="Trainer">Thomas M. Amoss</td>
      <td data-title="Owner">James McIngvale </td>
      <td data-title="Time">1:38.59 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}