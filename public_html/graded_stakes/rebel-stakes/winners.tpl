<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>        
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2019 </td>
      <td data-title="Winner">Omaha Beach</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Richard E. Mandella</td>
      <td data-title="Owner">Fox Hill Farms </td>
      <td data-title="Time">1:42.42 </td>

    </tr>
 {*   <tr>
      <td data-title="Year">2019 </td>
      <td data-title="Winner">Long Range Toddy </td>
      <td data-title="Jockey">Jon Court</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">Willis Horton Racing LLC </td>
      <td data-title="Time">1:42.49 </td>

    </tr> *}
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Magnum Moon</td>
      <td data-title="Jockey">Luis Saez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Lawana &amp; Robert Low </td>
      <td data-title="Time">1:42.68 </td>

    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Malagacy</td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Sumaya U.S. Stable </td>
      <td data-title="Time">1:43.00 </td>

    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Cupid</td>
      <td data-title="Jockey">Martin Garcia</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Magnier &amp; Smith </td>
      <td data-title="Time">1:43.84 </td>

    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">American Pharoah</td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Zayat Stables</td>
      <td data-title="Time">1:45.78 </td>

    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Hoppertunity</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Watson/Pegram/Weitman </td>
      <td data-title="Time">1:43.90 </td>

    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Will Take Charge</td>
      <td data-title="Jockey">Jon Court</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Willis D. Horton </td>
      <td data-title="Time">1:45.18 </td>

    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Secret Circle</td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Watson/Pegram/Weitman </td>
      <td data-title="Time">1:44.55 </td>

    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">The Factor</td>
      <td data-title="Jockey">Martin Garcia</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Fog City Stable &amp; G. Bolton </td>
      <td data-title="Time">1:42.19 </td>

    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Lookin at Lucky</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Mike Pegram/Watson/Weitman </td>
      <td data-title="Time">1:43.06 </td>

    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Win Willy</td>
      <td data-title="Jockey">Cliff Berry</td>
      <td data-title="Trainer">McLean Robertson</td>
      <td data-title="Owner">Jer-Mar Stable LLC </td>
      <td data-title="Time">1:44.41 </td>

    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Sierra Sunset </td>
      <td data-title="Jockey">Chris Emigh</td>
      <td data-title="Trainer">Jeff Bonde</td>
      <td data-title="Owner">Lebherz/Mariani/Schmitt/Wirth </td>
      <td data-title="Time">1:43.88 </td>

    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Curlin</td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">Padua Stables et al. </td>
      <td data-title="Time">1:44.70 </td>

    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Lawyer Ron</td>
      <td data-title="Jockey">John McKee</td>
      <td data-title="Trainer">Robert E. Holthus</td>
      <td data-title="Owner">James T. Hines, Jr. </td>
      <td data-title="Time">1:44.09 </td>

    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Greater Good </td>
      <td data-title="Jockey">John McKee</td>
      <td data-title="Trainer">Robert E. Holthus</td>
      <td data-title="Owner">Lewis Lakin </td>
      <td data-title="Time">1:44.92 </td>

    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Smarty Jones</td>
      <td data-title="Jockey">Stewart Elliott</td>
      <td data-title="Trainer">John Servis</td>
      <td data-title="Owner">Roy &amp; Patricia Chapman </td>
      <td data-title="Time">1:42.07 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Crowned King </td>
      <td data-title="Jockey">Chandra Rennie</td>
      <td data-title="Trainer">Billy McKeever</td>
      <td data-title="Owner">McKeever Racing Stable </td>
      <td data-title="Time">1:44.00 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Windward Passage</td>
      <td data-title="Jockey">Donnie Meche</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">Team Valor</td>
      <td data-title="Time">1:45.06 </td>

    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Crafty Shaw</td>
      <td data-title="Jockey">Joe Johnson</td>
      <td data-title="Trainer">Peter M. Vestal</td>
      <td data-title="Owner">Lucky Seven Stable </td>
      <td data-title="Time">1:43.82 </td>

    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Snuck In </td>
      <td data-title="Jockey">Cash Asmussen</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">Ackerley Bros. Farm </td>
      <td data-title="Time">1:42.99 </td>

    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Etbauer </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Steve J. Wren</td>
      <td data-title="Owner">K &amp; H. Biggs </td>
      <td data-title="Time">1:44.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Victory Gallop</td>
      <td data-title="Jockey">Eibar Coa</td>
      <td data-title="Trainer">W. Elliott Walden</td>
      <td data-title="Owner">Prestonwood Farm Inc. </td>
      <td data-title="Time">1:44.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Phantom On Tour</td>
      <td data-title="Jockey">Larry Melancon</td>
      <td data-title="Trainer">Lynn S. Whiting</td>
      <td data-title="Owner">W. Cal Partee</td>
      <td data-title="Time">1:42.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Ide </td>
      <td data-title="Jockey">Craig Perret</td>
      <td data-title="Trainer">Peter M. Vestal</td>
      <td data-title="Owner">Willmott Stables </td>
      <td data-title="Time">1:44.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Mystery Storm </td>
      <td data-title="Jockey">Craig Perret</td>
      <td data-title="Trainer">Larry Robideaux</td>
      <td data-title="Owner">David Beard </td>
      <td data-title="Time">1:44.40 </td>

    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Judge T C </td>
      <td data-title="Jockey">Joe Johnson</td>
      <td data-title="Trainer">Gary G. Hartlage</td>
      <td data-title="Owner">Bea &amp; R. H. Roberts </td>
      <td data-title="Time">1:44.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Dalhart </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Tom Bohannan</td>
      <td data-title="Owner">Loblolly Stable</td>
      <td data-title="Time">1:42.20 </td>

    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Pine Bluff</td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Tom Bohannan</td>
      <td data-title="Owner">Loblolly Stable</td>
      <td data-title="Time">1:42.80 </td>

    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Quintana </td>
      <td data-title="Jockey">David Guillory</td>
      <td data-title="Trainer">David C. Cross</td>
      <td data-title="Owner">Gary M. Garber </td>
      <td data-title="Time">1:42.60 </td>

    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Nuits St. Georges </td>
      <td data-title="Jockey">Jamie Bruin</td>
      <td data-title="Trainer">Douglas Peterson</td>
      <td data-title="Owner">Photini Jaffe </td>
      <td data-title="Time">1:46.00 </td>

    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Manastash Ridge </td>
      <td data-title="Jockey">Antonio Castanon</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Tayhill Stables </td>
      <td data-title="Time">1:43.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Sea Trek </td>
      <td data-title="Jockey">Patrick A. Johnson</td>
      <td data-title="Trainer">Gary G. Hartlage</td>
      <td data-title="Owner">Diana Stable </td>
      <td data-title="Time">1:42.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Demons Begone</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Philip M. Hauswald</td>
      <td data-title="Owner">Loblolly Stable</td>
      <td data-title="Time">1:41.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Rare Brick </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Tony Foyt</td>
      <td data-title="Owner">Pin Oak Stable &amp; A. J. Foyt</td>
      <td data-title="Time">1:43.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Clever Allemont </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Lynn S. Whiting</td>
      <td data-title="Owner">W. Cal Partee</td>
      <td data-title="Time">1:44.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Vanlandingham</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">C. R. McGaughey III</td>
      <td data-title="Owner">Loblolly Stable</td>
      <td data-title="Time">1:41.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Sunny's Halo</td>
      <td data-title="Jockey">Larry Snyder</td>
      <td data-title="Trainer">David C. Cross Jr.</td>
      <td data-title="Owner">David J. Foster </td>
      <td data-title="Time">1:42.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Bold Style </td>
      <td data-title="Jockey">Larry Snyder</td>
      <td data-title="Trainer">Jack Van Berg</td>
      <td data-title="Owner">L. Mayer </td>
      <td data-title="Time">1:43.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Bold Ego</td>
      <td data-title="Jockey">John Lively</td>
      <td data-title="Trainer">Dennis Werre </td>
      <td data-title="Owner">Double B Ranch &amp; J. Kidd </td>
      <td data-title="Time">1:41.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Temperence Hill</td>
      <td data-title="Jockey">Darrell Haire</td>
      <td data-title="Trainer">Joseph B. Cantey</td>
      <td data-title="Owner">Loblolly Stable</td>
      <td data-title="Time">1:42.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Lucy's Axe </td>
      <td data-title="Jockey">Eddie Maple</td>
      <td data-title="Trainer">Joseph B. Cantey</td>
      <td data-title="Owner">Mrs. L. Close </td>
      <td data-title="Time">1:45.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Chop Chop Tomahawk </td>
      <td data-title="Jockey">Larry Snyder</td>
      <td data-title="Trainer">E. Jackson</td>
      <td data-title="Owner">Daybreak Farm </td>
      <td data-title="Time">1:42.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">United Holme </td>
      <td data-title="Jockey">Jorge Tejeira</td>
      <td data-title="Trainer">Robert E. Holthus</td>
      <td data-title="Owner">Lofton &amp; Robert E. Holthus </td>
      <td data-title="Time">1:42.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Riverside Sam </td>
      <td data-title="Jockey">Garth Patterson</td>
      <td data-title="Trainer">E. Jackson</td>
      <td data-title="Owner">Daybreak Farm </td>
      <td data-title="Time">1:41.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Promised City </td>
      <td data-title="Jockey">David E. Whited</td>
      <td data-title="Trainer">Larry Spraker</td>
      <td data-title="Owner">Big I Farm </td>
      <td data-title="Time">1:44.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Perfect Aim </td>
      <td data-title="Jockey">Geary E. Louviere</td>
      <td data-title="Trainer">Harold Tinker</td>
      <td data-title="Owner">W. Cal Partee</td>
      <td data-title="Time">1:44.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Game Lad </td>
      <td data-title="Jockey">A. Herrera</td>
      <td data-title="Trainer">David R. Vance</td>
      <td data-title="Owner">Dan Lasater</td>
      <td data-title="Time">1:41.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Our Trade Winds </td>
      <td data-title="Jockey">Lonnie Ray</td>
      <td data-title="Trainer">Robert E. Holthus</td>
      <td data-title="Owner">R. Mitchell </td>
      <td data-title="Time">1:42.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Compte Deux </td>
      <td data-title="Jockey">Don Brumfield</td>
      <td data-title="Trainer">Ike K. Mourar</td>
      <td data-title="Owner">Robert E. Lehmann</td>
      <td data-title="Time">1:44.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Admiral's Shield </td>
      <td data-title="Jockey">Stanley A. Spencer</td>
      <td data-title="Trainer">Harvey L. Vanier</td>
      <td data-title="Owner">W. C. Robinson </td>
      <td data-title="Time">1:44.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Traffic Mark </td>
      <td data-title="Jockey">Phil I. Grimm</td>
      <td data-title="Trainer">Ronnie G. Warren</td>
      <td data-title="Owner">M/M Robert F. Roberts </td>
      <td data-title="Time">1:42.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Etony </td>
      <td data-title="Jockey">J. Pagano </td>
      <td data-title="Trainer">H. Holcomb </td>
      <td data-title="Owner">G. B. Newbill </td>
      <td data-title="Time">1:43.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Betemight </td>
      <td data-title="Jockey">Henry Moreno</td>
      <td data-title="Trainer">Doug M. Davis, Jr.</td>
      <td data-title="Owner">G. M. Holtsinger &amp; M. Davis </td>
      <td data-title="Time">1:40.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Timely Move </td>
      <td data-title="Jockey">M. Freed </td>
      <td data-title="Trainer">Charlie Vinci </td>
      <td data-title="Owner">Cantor &amp; Hill </td>
      <td data-title="Time">1:42.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Swift Ruler </td>
      <td data-title="Jockey">Larry Spraker</td>
      <td data-title="Trainer">Gin L. Collins</td>
      <td data-title="Owner">E. Allen </td>
      <td data-title="Time">1:44.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Spike Nail </td>
      <td data-title="Jockey">Ronnie J. Campbell</td>
      <td data-title="Trainer">Robert E. Holthus</td>
      <td data-title="Owner">C. O. Viar </td>
      <td data-title="Time">1:43.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1963 </td>
      <td data-title="Winner">Devil It Is </td>
      <td data-title="Jockey">Robert Gallimore</td>
      <td data-title="Trainer">G. Alberico </td>
      <td data-title="Owner">Bar-Al-Mar Stable </td>
      <td data-title="Time">1:45.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">Treasury Note </td>
      <td data-title="Jockey">Wayne Chambers</td>
      <td data-title="Trainer">Henry Forrest</td>
      <td data-title="Owner">J. C. Pollard </td>
      <td data-title="Time">1:42.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1961 </td>
      <td data-title="Winner">Bass Clef </td>
      <td data-title="Jockey">Ronald Baldwin</td>
      <td data-title="Trainer">Anthony W. Rupelt</td>
      <td data-title="Owner">Vera E. Smith </td>
      <td data-title="Time">1:42.40 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}