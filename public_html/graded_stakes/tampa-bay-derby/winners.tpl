<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>        
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Quip </td>
      <td data-title="Jockey">Florent Geroux</td>
      <td data-title="Trainer">Rodolphe Brisset</td>
      <td data-title="Owner">WinStar Farm/China Horse Club </td>
      <td data-title="Time">1:44.72 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Tapwrit</td>
      <td data-title="Jockey">Jose Ortiz</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Eclipse T-breds &amp; Robert LaPenta </td>
      <td data-title="Time">1:42.36 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Destin </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Twin Creeks Racing Stables </td>
      <td data-title="Time">1:42.82 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Carpe Diem </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Stonestreet Stables</td>
      <td data-title="Time">1:43.60 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Ring Weekend </td>
      <td data-title="Jockey">Daniel Centeno</td>
      <td data-title="Trainer">H. Graham Motion</td>
      <td data-title="Owner">St. Elias Stable/West Point Thoroughbreds </td>
      <td data-title="Time">1:43.71 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Verrazano</td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">M. Tabor, Ms. Magnier, Derrick Smith </td>
      <td data-title="Time">1:43.96 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Prospective </td>
      <td data-title="Jockey">Luis Contreras</td>
      <td data-title="Trainer">Mark Casse</td>
      <td data-title="Owner">John Oxley </td>
      <td data-title="Time">1:43.35 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Watch Me Go </td>
      <td data-title="Jockey">Luis Garcia</td>
      <td data-title="Trainer">Kathleen O'Connell</td>
      <td data-title="Owner">Gilbert G. Campbell </td>
      <td data-title="Time">1:44.25 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Odysseus </td>
      <td data-title="Jockey">Rajiv Maragh</td>
      <td data-title="Trainer">Thomas Albertrani</td>
      <td data-title="Owner">Padua Stables </td>
      <td data-title="Time">1:44.31 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Musket Man </td>
      <td data-title="Jockey">Daniel Centeno</td>
      <td data-title="Trainer">Derek Ryan</td>
      <td data-title="Owner">Eric Fein/Vic Carlson </td>
      <td data-title="Time">1:43.67 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Big Truck</td>
      <td data-title="Jockey">Eibar Coa</td>
      <td data-title="Trainer">Barclay Tagg</td>
      <td data-title="Owner">Eric Fein </td>
      <td data-title="Time">1:44.25 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Street Sense</td>
      <td data-title="Jockey">Calvin Borel</td>
      <td data-title="Trainer">Carl Nafzger</td>
      <td data-title="Owner">James B. Tafel</td>
      <td data-title="Time">1:43.11 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Deputy Glitters </td>
      <td data-title="Jockey">Jose Lezcano</td>
      <td data-title="Trainer">Tom Albertrani</td>
      <td data-title="Owner">Joseph Lacombe Stable</td>
      <td data-title="Time">1:44.26 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Sun King</td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Owner">Tracy Farmer </td>
      <td data-title="Time">1:43.98 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Limehouse</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Owner">Dogwood Stable</td>
      <td data-title="Time">1:43.99 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Region of Merit </td>
      <td data-title="Jockey">Eibar Coa</td>
      <td data-title="Trainer">Christophe Clement</td>
      <td data-title="Owner">Calumet Farm</td>
      <td data-title="Time">1:44.61 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Equality</td>
      <td data-title="Jockey">Ramon Dominguez</td>
      <td data-title="Trainer">H. Graham Motion</td>
      <td data-title="Owner">Pin Oak Farm</td>
      <td data-title="Time">1:43.66 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Burning Roma</td>
      <td data-title="Jockey">Richard Migliore</td>
      <td data-title="Trainer">Heather A. Giglio</td>
      <td data-title="Owner">Harold L. Queen </td>
      <td data-title="Time">1:44.30 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Wheelaway</td>
      <td data-title="Jockey">Richard Migliore</td>
      <td data-title="Trainer">John C. Kimmel</td>
      <td data-title="Owner">C. Kimmel / P. Solondz </td>
      <td data-title="Time">1:43.90 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Pineaff</td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">Kenneth McPeek</td>
      <td data-title="Owner">Roy &amp; Joyce Monroe </td>
      <td data-title="Time">1:45.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Parade Ground</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Neil J. Howard</td>
      <td data-title="Owner">W. S. Farish / S. Hilbert </td>
      <td data-title="Time">1:45.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Zede </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Owner">Allen E. Paulson</td>
      <td data-title="Time">1:44.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Thundering Storm </td>
      <td data-title="Jockey">Jorge Guerra</td>
      <td data-title="Trainer">Don R. Rice</td>
      <td data-title="Owner">Deborrah Artz </td>
      <td data-title="Time">1:43.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Gadzook </td>
      <td data-title="Jockey">Gary Boulanger</td>
      <td data-title="Trainer">J. Bert Sonnier</td>
      <td data-title="Owner">Du-Zee Stables </td>
      <td data-title="Time">1:45.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Prix de Crouton </td>
      <td data-title="Jockey">Mickey Walls</td>
      <td data-title="Trainer">Lorne Berg</td>
      <td data-title="Owner">Lorne &amp; Kathleen Berg</td>
      <td data-title="Time">1:46.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Marco Bay </td>
      <td data-title="Jockey">Ron Allen, Jr.</td>
      <td data-title="Trainer">Sarah Lundy</td>
      <td data-title="Owner">Jay Shaw </td>
      <td data-title="Time">1:44.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Careful Gesture </td>
      <td data-title="Jockey">Robert N. Lester</td>
      <td data-title="Trainer">Elbert Dixon</td>
      <td data-title="Owner">Cecilia Dixon </td>
      <td data-title="Time">1:45.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Speedy Cure </td>
      <td data-title="Jockey">Ricardo Lopez</td>
      <td data-title="Trainer">Harvey Culp</td>
      <td data-title="Owner">Susan B. Fisher </td>
      <td data-title="Time">1:46.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Champagneforashley</td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">Howard M. Tesher</td>
      <td data-title="Owner">Baker/Kaskel/Feinbloom </td>
      <td data-title="Time">1:44.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Storm Predictions </td>
      <td data-title="Jockey">Steve Gaffalione</td>
      <td data-title="Trainer">Luis Olivares</td>
      <td data-title="Owner">Three G Stables </td>
      <td data-title="Time">1:43.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Cefis</td>
      <td data-title="Jockey">Eddie Maple</td>
      <td data-title="Trainer">Woody Stephens</td>
      <td data-title="Owner">Ryehill Farm/ R. Kirkham </td>
      <td data-title="Time">1:44.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Phantom Jet </td>
      <td data-title="Jockey">Keith K. Allen</td>
      <td data-title="Trainer">Phillip Gleaves</td>
      <td data-title="Owner">Aisco Stables </td>
      <td data-title="Time">1:43.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">My Prince Charming </td>
      <td data-title="Jockey">Craig Perret</td>
      <td data-title="Trainer">Newcomb Green</td>
      <td data-title="Owner">Aronow Stable </td>
      <td data-title="Time">1:46.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Regal Remark </td>
      <td data-title="Jockey">Jeffrey Fell</td>
      <td data-title="Trainer">James E. Day</td>
      <td data-title="Owner">Sam-Son Farm</td>
      <td data-title="Time">1:46.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Bold Southerner </td>
      <td data-title="Jockey">Wayne Crews</td>
      <td data-title="Trainer">Paul Maxwell</td>
      <td data-title="Owner">Rusticwoods Farm </td>
      <td data-title="Time">1:44.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Morganmorganmorgan </td>
      <td data-title="Jockey">Willie Rodriguez</td>
      <td data-title="Trainer">Art Monteiro</td>
      <td data-title="Owner">Morgan Bishop </td>
      <td data-title="Time">1:47.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Reinvested </td>
      <td data-title="Jockey">Rick D. Luhr</td>
      <td data-title="Trainer">Stanley M. Hough</td>
      <td data-title="Owner">Harbor View Farm</td>
      <td data-title="Time">1:45.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Paristo </td>
      <td data-title="Jockey">David Ashcroft</td>
      <td data-title="Trainer">George Handy</td>
      <td data-title="Owner">Belmont Farm </td>
      <td data-title="Time">1:45.40 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}