<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Audible </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">1.41.92 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Irish War Cry</td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">H. Graham Motion</td>
      <td data-title="Time">1:42.52 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Mohaymen</td>
      <td data-title="Jockey">Junior Alvarado</td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Time">1:42.07 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Upstart </td>
      <td data-title="Jockey">Jose L. Ortiz</td>
      <td data-title="Trainer">Richard A. Violette Jr.</td>
      <td data-title="Time">1:43.61 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Cairo Prince </td>
      <td data-title="Jockey">Luis Saez</td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Time">1:42.16 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Itsmyluckyday</td>
      <td data-title="Jockey">Elvis Trujillo</td>
      <td data-title="Trainer">Eddie Plesa Jr.</td>
      <td data-title="Time">1:41.81 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Algorithms </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">1:36.17 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Dialed In</td>
      <td data-title="Jockey">Julien Leparoux</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Time">1:35.19 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Winslow Homer</td>
      <td data-title="Jockey">Ramón Domínguez</td>
      <td data-title="Trainer">Tony Dutrow</td>
      <td data-title="Time">1:35.97 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Saratoga Sinner </td>
      <td data-title="Jockey">Julien Leparoux</td>
      <td data-title="Trainer">Eddie Kenneally</td>
      <td data-title="Time">1:51.45 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Hey Byrn </td>
      <td data-title="Jockey">Chuck Lopez</td>
      <td data-title="Trainer">Edward Plesa, Jr.</td>
      <td data-title="Time">1:58.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Nobiz Like Shobiz</td>
      <td data-title="Jockey">Cornelio Velásquez</td>
      <td data-title="Trainer">Barclay Tagg</td>
      <td data-title="Time">1:35.47 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Barbaro</td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Michael R. Matz</td>
      <td data-title="Time">1:49.31 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Closing Argument</td>
      <td data-title="Jockey">Cornelio Velásquez</td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Time">1:50.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Second Of June </td>
      <td data-title="Jockey">Cornelio Velásquez</td>
      <td data-title="Trainer">William J. Cesare</td>
      <td data-title="Time">1:43.00 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Offlee Wild</td>
      <td data-title="Jockey">Mark Guidry</td>
      <td data-title="Trainer">Richard Dutrow, Jr.</td>
      <td data-title="Time">1:43.00 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Booklet </td>
      <td data-title="Jockey">Eibar Coa</td>
      <td data-title="Trainer">John T. Ward, Jr.</td>
      <td data-title="Time">1:46.16 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Radical Riley </td>
      <td data-title="Jockey">Eduardo Núñez</td>
      <td data-title="Trainer">James Hatchett</td>
      <td data-title="Time">1:46.06 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Hal's Hope </td>
      <td data-title="Jockey">Roger Velez</td>
      <td data-title="Trainer">Harold Rose</td>
      <td data-title="Time">1:44.52 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Grits'n Hard Toast </td>
      <td data-title="Jockey">Robbie Davis</td>
      <td data-title="Trainer">T.V. Smith</td>
      <td data-title="Time">1:45.32 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Cape Town </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:44.15 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Arthur L. </td>
      <td data-title="Jockey">John R. Velazquez</td>
      <td data-title="Trainer">Luis Olivares</td>
      <td data-title="Time">1:42.93 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Cobra King </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Mike Puype</td>
      <td data-title="Time">1:43.42 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Suave Prospect </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Time">1:44.03 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Go for Gin</td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Time">1:41.62 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Pride of Burkaan </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Leo Azpurua</td>
      <td data-title="Time">1:44.74 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Waki Warrior </td>
      <td data-title="Jockey">Earlie Fires</td>
      <td data-title="Trainer">William G. Huffman</td>
      <td data-title="Time">1:44.32 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Shoot To Kill </td>
      <td data-title="Jockey">Wigberto S. Ramos </td>
      <td data-title="Trainer">Linda L. Rice</td>
      <td data-title="Time">1:43.54 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}