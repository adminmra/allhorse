<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>        
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Vino Rosso </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Repole Stable and St. Elias Stable </td>
      <td data-title="Time">1:49.79 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Irish War Cry</td>
      <td data-title="Jockey">Rajiv Maragh</td>
      <td data-title="Trainer">H. Graham Motion</td>
      <td data-title="Owner">Isabelle de Tomaso </td>
      <td data-title="Time">1:50.91 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Outwork</td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Repole Stable</td>
      <td data-title="Time">1:52.92 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Frosted</td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Owner">Godolphin Racing</td>
      <td data-title="Time">1:50.31 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Wicked Strong</td>
      <td data-title="Jockey">Rajiv Maragh</td>
      <td data-title="Trainer">James Jerkens</td>
      <td data-title="Owner">Centennial Farms </td>
      <td data-title="Time">1:49.31 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Verrazano</td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Owner">Let's Go Stables </td>
      <td data-title="Time">1:50.27 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Gemologist</td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">WinStar Farm</td>
      <td data-title="Time">1:50.96 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Toby's Corner</td>
      <td data-title="Jockey">Eddie Castro</td>
      <td data-title="Trainer">H. Graham Motion</td>
      <td data-title="Owner">Dianne Cotter </td>
      <td data-title="Time">1:49.93 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Eskendereya</td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Owner">Zayat Stables</td>
      <td data-title="Time">1:49.97 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">I Want Revenge</td>
      <td data-title="Jockey">Joseph Talamo</td>
      <td data-title="Trainer">Jeff Mullins</td>
      <td data-title="Owner">IEAH</td>
      <td data-title="Time">1:49.49 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Tale of Ekati</td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Barclay Tagg</td>
      <td data-title="Owner">Charles E. Fipke</td>
      <td data-title="Time">1:52.35 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Nobiz Like Shobiz</td>
      <td data-title="Jockey">Cornelio Velasquez</td>
      <td data-title="Trainer">Barclay Tagg</td>
      <td data-title="Owner">Elizabeth J. Valando</td>
      <td data-title="Time">1:49.46 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Bob and John</td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Stonerside Stable</td>
      <td data-title="Time">1:51.54 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Bellamy Road</td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Owner">Kinsman Stable</td>
      <td data-title="Time">1:47.16 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Tapit</td>
      <td data-title="Jockey">Ramon A. Dominguez</td>
      <td data-title="Trainer">Michael W. Dickinson</td>
      <td data-title="Owner">Ronald Winchell </td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Empire Maker</td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Owner">Juddmonte Farms</td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Buddha</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">H. James Bond</td>
      <td data-title="Owner">Gary and Mary West </td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Congaree</td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Stonerside Stable</td>
      <td data-title="Time">1:47.80 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Fusaichi Pegasus</td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Neil D. Drysdale</td>
      <td data-title="Owner">Fusao Sekiguchi</td>
      <td data-title="Time">1:47.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Adonis</td>
      <td data-title="Jockey">Jorge Chavez</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Owner">Paraneck Stable</td>
      <td data-title="Time">1:47.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Coronado's Quest</td>
      <td data-title="Jockey">Robbie Davis</td>
      <td data-title="Trainer">C. R. McGaughey III</td>
      <td data-title="Owner">Stuart S. Janney III </td>
      <td data-title="Time">1:47.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Captain Bodgit</td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Gary Capuano</td>
      <td data-title="Owner">Team Valor</td>
      <td data-title="Time">1:48.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Unbridled's Song</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">James T. Ryerson</td>
      <td data-title="Owner">Paraneck Stable</td>
      <td data-title="Time">1:49.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Talkin Man</td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">Roger Attfield</td>
      <td data-title="Owner">Kinghaven Farms</td>
      <td data-title="Time">1:49.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Irgun</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Steven W. Young</td>
      <td data-title="Owner">B.&amp; M. Chase </td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Storm Tower</td>
      <td data-title="Jockey">Rick Wilson</td>
      <td data-title="Trainer">Ben W. Perkins, Jr.</td>
      <td data-title="Owner">Char-Mari Stable </td>
      <td data-title="Time">1:48.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Devil His Due</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">H. Allen Jerkens</td>
      <td data-title="Owner">Lion Crest Stable </td>
      <td data-title="Time">1:49.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Cahill Road</td>
      <td data-title="Jockey">Craig Perret</td>
      <td data-title="Trainer">Scotty Schulhofer</td>
      <td data-title="Owner">Frances A. Genter</td>
      <td data-title="Time">1:48.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Thirty Six Red</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Owner">B. Giles Brophy</td>
      <td data-title="Time">1:50.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Easy Goer</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">C. R. McGaughey III</td>
      <td data-title="Owner">Ogden Phipps</td>
      <td data-title="Time">1:50.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Private Terms</td>
      <td data-title="Jockey">Chris Antley</td>
      <td data-title="Trainer">Charles Hadry</td>
      <td data-title="Owner">Locust Hill Farm</td>
      <td data-title="Time">1:47.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Gulch</td>
      <td data-title="Jockey">Jose Santos</td>
      <td data-title="Trainer">LeRoy Jolley</td>
      <td data-title="Owner">Peter M. Brant</td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Broad Brush</td>
      <td data-title="Jockey">Vincent Bracciale, Jr.</td>
      <td data-title="Trainer">Richard W. Small</td>
      <td data-title="Owner">Robert E. Meyerhoff</td>
      <td data-title="Time">1:50.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Eternal Prince</td>
      <td data-title="Jockey">Richard Migliore</td>
      <td data-title="Trainer">John J. Lenzini, Jr.</td>
      <td data-title="Owner">Brian J. Hurst </td>
      <td data-title="Time">1:48.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Leroy S.</td>
      <td data-title="Jockey">Jean Cruguet</td>
      <td data-title="Trainer">Jan Nerud</td>
      <td data-title="Owner">John A. Nerud</td>
      <td data-title="Time">1:51.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Bounding Basque</td>
      <td data-title="Jockey">Gregg McCarron</td>
      <td data-title="Trainer">Woody Sedlacek</td>
      <td data-title="Owner">Jacques Wimpfheimer </td>
      <td data-title="Time">1:51.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Slew o' Gold</td>
      <td data-title="Jockey">Eddie Maple</td>
      <td data-title="Trainer">Sidney Watters, Jr.</td>
      <td data-title="Owner">Equusequity Stable </td>
      <td data-title="Time">1:51.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Air Forbes Won</td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">Frank LaBoccetta</td>
      <td data-title="Owner">Edward Anchel </td>
      <td data-title="Time">1:51.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Pleasant Colony</td>
      <td data-title="Jockey">Jeffrey Fell</td>
      <td data-title="Trainer">John P. Campo</td>
      <td data-title="Owner">Buckland Farm</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Plugged Nickle</td>
      <td data-title="Jockey">Buck Thornburg</td>
      <td data-title="Trainer">Thomas J. Kelly</td>
      <td data-title="Owner">John M. Schiff</td>
      <td data-title="Time">1:50.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Instrument Landing</td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">David A. Whiteley</td>
      <td data-title="Owner">Pen-Y-Bryn Farm</td>
      <td data-title="Time">1:49.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Believe It</td>
      <td data-title="Jockey">Eddie Maple</td>
      <td data-title="Trainer">Woody Stephens</td>
      <td data-title="Owner">Hickory Tree Stable</td>
      <td data-title="Time">1:49.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Seattle Slew</td>
      <td data-title="Jockey">Jean Cruguet</td>
      <td data-title="Trainer">William H. Turner, Jr.</td>
      <td data-title="Owner">Karen L. Taylor </td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Bold Forbes</td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">Laz Barrera</td>
      <td data-title="Owner">E. Rodriguez Tizol</td>
      <td data-title="Time">1:47.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Foolish Pleasure</td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">LeRoy Jolley</td>
      <td data-title="Owner">John L. Greer</td>
      <td data-title="Time">1:48.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Flip Sal</td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">Stephen A. DiMauro</td>
      <td data-title="Owner">Salvatore Tufano </td>
      <td data-title="Time">1:51.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Rube The Great</td>
      <td data-title="Jockey">Miguel A. Rivera</td>
      <td data-title="Trainer">Pancho Martin</td>
      <td data-title="Owner">Sigmund Sommer</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Angle Light</td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">Lucien Laurin</td>
      <td data-title="Owner">Edwin Whittaker </td>
      <td data-title="Time">1:49.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Upper Case</td>
      <td data-title="Jockey">Ron Turcotte</td>
      <td data-title="Trainer">Lucien Laurin</td>
      <td data-title="Owner">Meadow Stable</td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Good Behaving</td>
      <td data-title="Jockey">Chuck Baltazar</td>
      <td data-title="Trainer">John P. Campo</td>
      <td data-title="Owner">Neil Hellman </td>
      <td data-title="Time">1:49.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Personality</td>
      <td data-title="Jockey">Eddie Belmonte</td>
      <td data-title="Trainer">John W. Jacobs</td>
      <td data-title="Owner">Ethel D. Jacobs</td>
      <td data-title="Time">1:49.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Dike</td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">Lucien Laurin</td>
      <td data-title="Owner">Claiborne Farm</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Dancer's Image</td>
      <td data-title="Jockey">Bobby Ussery</td>
      <td data-title="Trainer">Lou Cavalaris, Jr.</td>
      <td data-title="Owner">Peter D. Fuller</td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Damascus</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Frank Y. Whiteley, Jr.</td>
      <td data-title="Owner">Edith W. Bancroft</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Amberoid</td>
      <td data-title="Jockey">William Boland</td>
      <td data-title="Trainer">Lucien Laurin</td>
      <td data-title="Owner">Reginald N. Webster</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Flag Raiser</td>
      <td data-title="Jockey">Bobby Ussery</td>
      <td data-title="Trainer">Hirsch Jacobs</td>
      <td data-title="Owner">Isidor Bieber</td>
      <td data-title="Time">1:50.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Quadrangle</td>
      <td data-title="Jockey">Bill Hartack</td>
      <td data-title="Trainer">J. Elliott Burch</td>
      <td data-title="Owner">Rokeby Stable</td>
      <td data-title="Time">1:50.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1963 </td>
      <td data-title="Winner">No Robbery</td>
      <td data-title="Jockey">John L. Rotz</td>
      <td data-title="Trainer">John M. Gaver, Sr.</td>
      <td data-title="Owner">Greentree Stable</td>
      <td data-title="Time">1:49.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">Admiral's Voyage</td>
      <td data-title="Jockey">Braulio Baeza</td>
      <td data-title="Trainer">Charles R. Parke</td>
      <td data-title="Owner">Fred W. Hooper</td>
      <td data-title="Time">1:49.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1961 </td>
      <td data-title="Winner">Globemaster</td>
      <td data-title="Jockey">John L. Rotz</td>
      <td data-title="Trainer">Thomas J. Kelly</td>
      <td data-title="Owner">Leonard P. Sasso </td>
      <td data-title="Time">1:50.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1960 </td>
      <td data-title="Winner">Francis S.</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Burley Parke</td>
      <td data-title="Owner">Harbor View Farm</td>
      <td data-title="Time">1:50.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1959 </td>
      <td data-title="Winner">Manassa Mauler</td>
      <td data-title="Jockey">Ray Broussard</td>
      <td data-title="Trainer">Pancho Martin</td>
      <td data-title="Owner">Emil Dolce </td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1958 </td>
      <td data-title="Winner">Jewel's Reward</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Ivan H. Parke</td>
      <td data-title="Owner">Maine Chance Farm</td>
      <td data-title="Time">1:50.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1957 </td>
      <td data-title="Winner">Bold Ruler</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Jim Fitzsimmons</td>
      <td data-title="Owner">Wheatley Stable</td>
      <td data-title="Time">1:48.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1956 </td>
      <td data-title="Winner">Head Man</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Sylvester Veitch</td>
      <td data-title="Owner">C. V. Whitney</td>
      <td data-title="Time">1:50.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1955 </td>
      <td data-title="Winner">Nashua</td>
      <td data-title="Jockey">Ted Atkinson</td>
      <td data-title="Trainer">Jim Fitzsimmons</td>
      <td data-title="Owner">Belair Stud</td>
      <td data-title="Time">1:50.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1954 </td>
      <td data-title="Winner">Correlation</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Noble Threewitt</td>
      <td data-title="Owner">Robert S. Lytle </td>
      <td data-title="Time">1:50.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1953 </td>
      <td data-title="Winner">Native Dancer</td>
      <td data-title="Jockey">Eric Guerin</td>
      <td data-title="Trainer">William C. Winfrey</td>
      <td data-title="Owner">Alfred G. Vanderbilt II</td>
      <td data-title="Time">1:50.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1952 </td>
      <td data-title="Winner">Master Fiddle</td>
      <td data-title="Jockey">David Gorman</td>
      <td data-title="Trainer">Sol Rutchick</td>
      <td data-title="Owner">Myhelen Stable </td>
      <td data-title="Time">1:52.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1951 </td>
      <td data-title="Winner">Repetoire</td>
      <td data-title="Jockey">Pete McLean</td>
      <td data-title="Trainer">Albert Jensen</td>
      <td data-title="Owner">Nora A. Mikell </td>
      <td data-title="Time">1:44.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1950 </td>
      <td data-title="Winner">Hill Prince</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Casey Hayes</td>
      <td data-title="Owner">Christopher Chenery</td>
      <td data-title="Time">1:43.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1949 </td>
      <td data-title="Winner">Olympia</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Ivan H. Parke</td>
      <td data-title="Owner">Fred W. Hooper</td>
      <td data-title="Time">1:45.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1948 </td>
      <td data-title="Winner">My Request</td>
      <td data-title="Jockey">Douglas Dodson</td>
      <td data-title="Trainer">James P. Conway</td>
      <td data-title="Owner">Mrs. Ben F. Whitaker</td>
      <td data-title="Time">1:46.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1947 </td>
      <td data-title="Winner">Phalanx</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Sylvester Veitch</td>
      <td data-title="Owner">C. V. Whitney</td>
      <td data-title="Time">1:43.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1947 </td>
      <td data-title="Winner">I Will</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Sidney Jacobs</td>
      <td data-title="Owner">Jaclyn Stable</td>
      <td data-title="Time">1:45.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1946 </td>
      <td data-title="Winner">Assault</td>
      <td data-title="Jockey">Warren Mehrtens</td>
      <td data-title="Trainer">Max Hirsch</td>
      <td data-title="Owner">King Ranch</td>
      <td data-title="Time">1:46.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1945 </td>
      <td data-title="Winner">Jeep</td>
      <td data-title="Jockey">Arnold Kirkland</td>
      <td data-title="Trainer">Lydell T. Ruff</td>
      <td data-title="Owner">C. V. Whitney</td>
      <td data-title="Time">1:45.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1945 </td>
      <td data-title="Winner">Hoop Jr.</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Ivan H. Parke</td>
      <td data-title="Owner">Fred W. Hooper</td>
      <td data-title="Time">1:45.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1944 </td>
      <td data-title="Winner">Lucky Draw</td>
      <td data-title="Jockey">Johnny Longden</td>
      <td data-title="Trainer">Bert Mulholland</td>
      <td data-title="Owner">George D. Widener, Jr.</td>
      <td data-title="Time">1:46.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1944 </td>
      <td data-title="Winner">Stir Up</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">John M. Gaver, Sr.</td>
      <td data-title="Owner">Greentree Stable</td>
      <td data-title="Time">1:44.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1943 </td>
      <td data-title="Winner">Count Fleet</td>
      <td data-title="Jockey">Johnny Longden</td>
      <td data-title="Trainer">Don Cameron</td>
      <td data-title="Owner">Fannie Hertz</td>
      <td data-title="Time">1:43.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1942 </td>
      <td data-title="Winner">Requested</td>
      <td data-title="Jockey">Wayne D. Wright</td>
      <td data-title="Trainer">Blackie McCoole</td>
      <td data-title="Owner">Mrs. Ben F. Whitaker</td>
      <td data-title="Time">1:45.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1941 </td>
      <td data-title="Winner">Market Wise</td>
      <td data-title="Jockey">Don Meade</td>
      <td data-title="Trainer">George W. Carroll</td>
      <td data-title="Owner">Louis Tufano </td>
      <td data-title="Time">1:45.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1940 </td>
      <td data-title="Winner">Dit</td>
      <td data-title="Jockey">Leon Haas</td>
      <td data-title="Trainer">Max Hirsch</td>
      <td data-title="Owner">W. Arnold Hanger</td>
      <td data-title="Time">1:45.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1939 </td>
      <td data-title="Winner">Johnstown</td>
      <td data-title="Jockey">James Stout</td>
      <td data-title="Trainer">Jim Fitzsimmons</td>
      <td data-title="Owner">Belair Stud</td>
      <td data-title="Time">1:42.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1938 </td>
      <td data-title="Winner">Fighting Fox</td>
      <td data-title="Jockey">James Stout</td>
      <td data-title="Trainer">Jim Fitzsimmons</td>
      <td data-title="Owner">Belair Stud</td>
      <td data-title="Time">1:43.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1937 </td>
      <td data-title="Winner">Melodist</td>
      <td data-title="Jockey">Johnny Longden</td>
      <td data-title="Trainer">Jim Fitzsimmons</td>
      <td data-title="Owner">Wheatley Stable</td>
      <td data-title="Time">1:42.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1936 </td>
      <td data-title="Winner">Teufel</td>
      <td data-title="Jockey">Herbert Litzenberger</td>
      <td data-title="Trainer">Jim Fitzsimmons</td>
      <td data-title="Owner">Wheatley Stable</td>
      <td data-title="Time">1:43.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1935 </td>
      <td data-title="Winner">Today</td>
      <td data-title="Jockey">Raymond Workman</td>
      <td data-title="Trainer">Thomas J. Healey</td>
      <td data-title="Owner">C. V. Whitney</td>
      <td data-title="Time">1:42.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1934 </td>
      <td data-title="Winner">High Quest</td>
      <td data-title="Jockey">Dominick Bellizzi</td>
      <td data-title="Trainer">Robert A. Smith</td>
      <td data-title="Owner">Brookmeade Stable</td>
      <td data-title="Time">1:43.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1933 </td>
      <td data-title="Winner">Mr. Khayyam</td>
      <td data-title="Jockey">Pete Walls</td>
      <td data-title="Trainer">Matthew Peter Brady</td>
      <td data-title="Owner">Catawba Farm </td>
      <td data-title="Time">1:42.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1932 </td>
      <td data-title="Winner">Universe</td>
      <td data-title="Jockey">Linus McAtee</td>
      <td data-title="Trainer">Joseph Bauer</td>
      <td data-title="Owner">Thomas M. Cassidy </td>
      <td data-title="Time">1:43.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1931 </td>
      <td data-title="Winner">Twenty Grand</td>
      <td data-title="Jockey">Charley Kurtsinger</td>
      <td data-title="Trainer">James G. Rowe, Jr.</td>
      <td data-title="Owner">Greentree Stable</td>
      <td data-title="Time">1:42.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1930 </td>
      <td data-title="Winner">Gallant Fox</td>
      <td data-title="Jockey">Earl Sande</td>
      <td data-title="Trainer">Jim Fitzsimmons</td>
      <td data-title="Owner">Belair Stud</td>
      <td data-title="Time">1:43.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1929 </td>
      <td data-title="Winner">Essare</td>
      <td data-title="Jockey">Mack Garner</td>
      <td data-title="Trainer">Joe Johnson</td>
      <td data-title="Owner">Jacques Stable </td>
      <td data-title="Time">1:44.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1928 </td>
      <td data-title="Winner">Distraction</td>
      <td data-title="Jockey">Danny McAuliffe</td>
      <td data-title="Trainer">George Tappen</td>
      <td data-title="Owner">Wheatley Stable</td>
      <td data-title="Time">1:46.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1927 </td>
      <td data-title="Winner">Saxon</td>
      <td data-title="Jockey">George Ellis</td>
      <td data-title="Trainer">James G. Rowe, Sr.</td>
      <td data-title="Owner">Greentree Stable</td>
      <td data-title="Time">1:43.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1926 </td>
      <td data-title="Winner">Pompey</td>
      <td data-title="Jockey">Bennie Breuning</td>
      <td data-title="Trainer">William H. Karrick</td>
      <td data-title="Owner">William R. Coe</td>
      <td data-title="Time">1:42.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1925 </td>
      <td data-title="Winner">Backbone</td>
      <td data-title="Jockey">Ivan H. Parke</td>
      <td data-title="Trainer">James G. Rowe, Sr.</td>
      <td data-title="Owner">H. P. Whitney</td>
      <td data-title="Time">1:43.40 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}