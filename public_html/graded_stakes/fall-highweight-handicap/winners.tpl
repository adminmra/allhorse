<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th width="70px">Age</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Life in Shambles </td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">Irad Ortiz Jr.</td>
      <td data-title="Trainer">Jason Servis</td>
      <td data-title="Time">1:12.07 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Stallwalkin' Dude </td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">David Jacobson </td>
      <td data-title="Time">1:11.11 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Heaven's Runway </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Junior Alvarado</td>
      <td data-title="Trainer">Rudy R. Rodriguez</td>
      <td data-title="Time">1:10.19 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Green Grato </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Kendrick Carmouche</td>
      <td data-title="Trainer">Gaston Grant</td>
      <td data-title="Time">1:09.95 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Salutos Amigos </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Cornelio Velasquez</td>
      <td data-title="Trainer">David Jacobson</td>
      <td data-title="Time">1:10.78 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Palace </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Cornelio Velasquez</td>
      <td data-title="Trainer">Linda Rice </td>
      <td data-title="Time">1:09.82 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Caixa Eletronica</td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">1:08.69 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Sunrise Smarty</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Ramon Dominguez</td>
      <td data-title="Trainer">Michael Hushion</td>
      <td data-title="Time">1:09.97 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Endless Circle</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Rudy Rodriguez</td>
      <td data-title="Time">1:09.79 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Cherokee Country</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jose Lezcano</td>
      <td data-title="Trainer">Ramon Preciado</td>
      <td data-title="Time">1:09.29 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Fabulous Strike</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Ramon Dominguez</td>
      <td data-title="Trainer">Todd Beattie</td>
      <td data-title="Time">1:09.02 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Grand Champion</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eibar Coa</td>
      <td data-title="Trainer">James A. Jerkens</td>
      <td data-title="Time">1:09.90 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Afrashad</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Richard Migliore</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Time">1:11.05 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Attila's Storm</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Pablo Morales</td>
      <td data-title="Trainer">Richard Schosberg</td>
      <td data-title="Time">1:09.30 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Thunder Touch</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Time">1:09.83 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Bossanova</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">H. James Bond</td>
      <td data-title="Time">1:10.06 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">True Direction</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Carlos Morales</td>
      <td data-title="Time">1:09.62 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Yonaguska</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jose Santos</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:09.60 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Kashatreya</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Octavio Vergara</td>
      <td data-title="Trainer">John O. Hertler</td>
      <td data-title="Time">1:11.03 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Richter Scale</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jorge Chavez</td>
      <td data-title="Trainer">Mary Jo Lohmeier</td>
      <td data-title="Time">1:09.05 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Punch Line </td>
      <td data-title="Age">8 </td>
      <td data-title="Jockey">Jorge Chavez</td>
      <td data-title="Trainer">William H. Turner Jr.</td>
      <td data-title="Time">1:10.07 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Royal Haven</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Richard Migliore</td>
      <td data-title="Trainer">Gasper Moschera</td>
      <td data-title="Time">1:10.68 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Victor Avenue</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jorge Chavez</td>
      <td data-title="Trainer">Dale Romans</td>
      <td data-title="Time">1:09.24 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Jess C's Whirl</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jorge Chavez</td>
      <td data-title="Trainer">John H. Forbes</td>
      <td data-title="Time">1:09.87 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Chimes Band</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:11.37 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Fly So Free</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Flint S. Schulhofer</td>
      <td data-title="Time">1:09.41 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Salt Lake</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:09.07 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Senor Speedy</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jorge Chavez</td>
      <td data-title="Trainer">Alfredo Callejas</td>
      <td data-title="Time">1:08.62 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Carson City</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:09.8 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Sewickley</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Randy Romero</td>
      <td data-title="Trainer">Flint S. Schulhofer</td>
      <td data-title="Time">1:09.6 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Parlay Me</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Randy Romero</td>
      <td data-title="Trainer">James E. Picou</td>
      <td data-title="Time">1:09.6 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Purple Mountain</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Richard Migliore</td>
      <td data-title="Trainer">Woodrow Sedlacek</td>
      <td data-title="Time">1:08.8 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Funistrada</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Robbie Davis</td>
      <td data-title="Trainer">Jan H. Nerud</td>
      <td data-title="Time">1:09.4 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Mt. Livermore</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:10.6 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Mamaison</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Richard Mandella</td>
      <td data-title="Time">1:10.2 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Chas Conerly</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jimmy Miranda</td>
      <td data-title="Trainer">Allen H. Jerkens</td>
      <td data-title="Time">1:10.4 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Gold Beauty</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Don Brumfield</td>
      <td data-title="Trainer">William D. Curtis</td>
      <td data-title="Time">1:09.2 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Piedmont Pete</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Kenny Black</td>
      <td data-title="Trainer">Ron Alfano</td>
      <td data-title="Time">1:10.4 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">King's Fashion</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jean-Luc Samyn</td>
      <td data-title="Trainer">H. Allen Jerkens</td>
      <td data-title="Time">1:10.6 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Whatsyourpleasure</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Angel Cordero Jr.</td>
      <td data-title="Trainer">John W. Russell</td>
      <td data-title="Time">1:09.6 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">What A Summer</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Angel Cordero Jr.</td>
      <td data-title="Trainer">Leroy Jolley</td>
      <td data-title="Time">1:09.4 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">What A Summer</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">Leroy Jolley</td>
      <td data-title="Time">1:10.0 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Relent</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Ruben Hernandez</td>
      <td data-title="Trainer">Sidney Watters Jr.</td>
      <td data-title="Time">1:09.8 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Honorable Miss</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Frank Y. Whiteley Jr.</td>
      <td data-title="Time">1:10.0 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Honorable Miss</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">Frank Y. Whiteley Jr.</td>
      <td data-title="Time">1:09.8 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Piamem</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Miguel A. Rivera</td>
      <td data-title="Trainer">Lazaro S. Barrera</td>
      <td data-title="Time">1:10.4 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">King's Bishop</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eddie Maple</td>
      <td data-title="Trainer">H. Allen Jerkens</td>
      <td data-title="Time">1:09.4 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Chou Croute</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">John L. Rotz</td>
      <td data-title="Trainer">Robert G. Dunham</td>
      <td data-title="Time">1:10.0 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Shut Eye</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Angel Cordero Jr.</td>
      <td data-title="Trainer">Jack M. Bradley</td>
      <td data-title="Time">1:10.8 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Ta Wee</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">John L. Rotz</td>
      <td data-title="Trainer">Flint S. Schulhofer</td>
      <td data-title="Time">1:10.4 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Ta Wee</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">John L. Rotz</td>
      <td data-title="Trainer">Flint S. Schulhofer</td>
      <td data-title="Time">1:10.2 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">More Scents</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Angel Cordero Jr.</td>
      <td data-title="Trainer">Philip G. Johnson</td>
      <td data-title="Time">1:11.8 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Indulto</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">John L. Rotz</td>
      <td data-title="Trainer">Max Hirsch</td>
      <td data-title="Time">1:12.6 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Impressive</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Kenneth Knapp</td>
      <td data-title="Trainer">Edward A. Neloy</td>
      <td data-title="Time">1:10.4 </td>
    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Pack Trip</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Manuel Ycaza</td>
      <td data-title="Trainer">John A. Nerud</td>
      <td data-title="Time">1:13.8 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}