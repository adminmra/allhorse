<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="60px">Year</th>
        <th>Winner</th>
        <th width="60px">Age</th>        
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="70px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2019 </td>
      <td data-title="Winner">Next Shares </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">Richard Baltas </td>
      <td data-title="Time">1:48.61 </td>
    </tr>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Itsinthepost </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Tyler Baze </td>
      <td data-title="Trainer">Jeff Mullins </td>
      <td data-title="Time">1:47.59 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Blue Tone</td>
      <td data-title="Age">8 </td>
      <td data-title="Jockey">Kent J. Desormeaux</td>
      <td data-title="Trainer">Robert B. Hess, Jr.</td>
      <td data-title="Time">1:49.82 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Flamboyant (FR) </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Brice Blanc</td>
      <td data-title="Trainer">Patrick Gallagher</td>
      <td data-title="Time">1:46.64 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Finnegans Wake</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">Peter Miller</td>
      <td data-title="Time">1:48.38 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Jeranimo</td>
      <td data-title="Age">8 </td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Michael Pender</td>
      <td data-title="Time">1:46.00 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Jeranimo</td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Michael Pender</td>
      <td data-title="Time">1:46.50 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Norvsky </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Donald Warren</td>
      <td data-title="Time">1:47.03 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Age">&nbsp;</td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Jeranimo </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Michael Pender</td>
      <td data-title="Time">1:47.20 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Proudinsky </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Humberton Ascanio</td>
      <td data-title="Time">1:46.91 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Proudinsky </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:48.39 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Daytona </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Dan L. Hendricks</td>
      <td data-title="Time">1:47.57 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">King's Drama</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">David Flores</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:48.53 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Badge of Silver </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:50.02 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Truly A Judge </td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">Martin Pedroza</td>
      <td data-title="Trainer">David Bernstein</td>
      <td data-title="Time">1:48.90 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Age">&nbsp;</td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Redattore </td>
      <td data-title="Age">8 </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Richard E. Mandella</td>
      <td data-title="Time">1:48.17 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Grammarian </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jose Valdivia, Jr.</td>
      <td data-title="Trainer">C. Beau Greely</td>
      <td data-title="Time">1:48.12 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Irish Prize </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Neil D. Drysdale</td>
      <td data-title="Time">1:50.56 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Irish Prize </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Neil D. Drysdale</td>
      <td data-title="Time">1:47.88 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Brave Act </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Ronald McAnally</td>
      <td data-title="Time">1:49.25 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Age">&nbsp;</td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Brave Act </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Goncalino Almeida</td>
      <td data-title="Trainer">Ronald McAnally</td>
      <td data-title="Time">1:46.78 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Martiniquais </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:48.42 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Rainbow Blues </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Charles Whittingham</td>
      <td data-title="Time">1:46.89 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Romarin </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Richard E. Mandella</td>
      <td data-title="Time">1:49.69 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Romarin </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Richard E. Mandella</td>
      <td data-title="Time">1:49.36 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Earl of Barking </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Richard L. Cross</td>
      <td data-title="Time">1:48.64 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Star of Cozzene</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Mark A. Hennig</td>
      <td data-title="Time">1:48.33 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Classic Fame</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">Gary F. Jones</td>
      <td data-title="Time">1:46.69 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Age">&nbsp;</td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">In Excess</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Bruce L. Jackson</td>
      <td data-title="Time">1:47.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Wretham </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Neil D. Drysdale</td>
      <td data-title="Time">1:46.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Simply Majestic </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">John Parisella</td>
      <td data-title="Time">1:47.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Conquering Hero </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Darrell Vienna</td>
      <td data-title="Time">1:50.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Nostalgia&rsquo;s Star </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">John W. Russell</td>
      <td data-title="Time">1:51.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Yashgan </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">John Sullivan</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Dahar </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Fernando Toro</td>
      <td data-title="Trainer">Charles Whittingham</td>
      <td data-title="Time">1:47.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Prince Florimund </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Charles Whittingham</td>
      <td data-title="Time">1:48.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Beldale Lustre </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">John Gosden</td>
      <td data-title="Time">1:48.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Greenwood Star </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Donald Pierce</td>
      <td data-title="Trainer">John W. Russell</td>
      <td data-title="Time">1:47.20 </td>
    </tr>
    <tr bgcolor="#eeeeee">
      <td data-title="Year">1982 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Age">&nbsp;</td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">The Bart </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">John Sullivan</td>
      <td data-title="Time">1:48.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Premier Ministre </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Laz Barrera</td>
      <td data-title="Time">1:48.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">John Henry</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Darrel McHargue</td>
      <td data-title="Trainer">Ronald McAnally</td>
      <td data-title="Time">1:49.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Fluorescent Light </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">Angel Penna, Sr.</td>
      <td data-title="Time">1:47.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Mr. Redoy </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Sandy Hawley</td>
      <td data-title="Trainer">Tommy Doyle</td>
      <td data-title="Time">1:48.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Riot in Paris </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Charles Whittingham</td>
      <td data-title="Time">1:50.00 </td>
    </tr>
    <tr bgcolor="#eeeeee">
      <td data-title="Year">1976 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Age">&nbsp;</td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Zanthe </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Sandy Hawley</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:47.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Fair Test </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Angel Santiago</td>
      <td data-title="Trainer">Vincent Clyne</td>
      <td data-title="Time">1:50.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Astray </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">Paul K. Parker</td>
      <td data-title="Time">1:48.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Kentuckian </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Donald Pierce</td>
      <td data-title="Trainer">David A. Whiteley</td>
      <td data-title="Time">1:47.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Big Shot II </td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">Eddie Belmonte</td>
      <td data-title="Trainer">Pancho Martin</td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Cougar II</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Charles Whittingham</td>
      <td data-title="Time">1:52.60 </td>
    </tr>
    <tr bgcolor="#eeeeee">
      <td data-title="Year">1970 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Age">&nbsp;</td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Easy Mark </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eddie Belmonte</td>
      <td data-title="Trainer">Laz Barrera</td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Rivet </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Merlin Volzke</td>
      <td data-title="Trainer">Dale Landers</td>
      <td data-title="Time">1:50.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Flag </td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">Walter Blum</td>
      <td data-title="Trainer">Wayne B. Stucki</td>
      <td data-title="Time">1:47.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Perfect Sky </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Alvaro Pineda</td>
      <td data-title="Trainer">Charles Whittingham</td>
      <td data-title="Time">1:55.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Biggs </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">William Harmatz</td>
      <td data-title="Trainer">Leonard Dorfman</td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Marlin Bay </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Manuel Ycaza</td>
      <td data-title="Trainer">A. W. Beuzeville</td>
      <td data-title="Time">1:48.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1963 </td>
      <td data-title="Winner">Dusky Damion </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Ismael Valenzuela</td>
      <td data-title="Trainer">William B. Finnegan</td>
      <td data-title="Time">1:48.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">Art Market </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Ismael Valenzuela</td>
      <td data-title="Trainer">Hirsch Jacobs</td>
      <td data-title="Time">1:48.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1961 </td>
      <td data-title="Winner">Geechee Lou </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Johnny Longden</td>
      <td data-title="Trainer">Gilbert Guariglia</td>
      <td data-title="Time">1:46.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1960 </td>
      <td data-title="Winner">Eddie Schmidt </td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">Alex Maese</td>
      <td data-title="Trainer">Frank L. Carr</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1959 </td>
      <td data-title="Winner">MacBern </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Henry Moreno</td>
      <td data-title="Trainer">William J. Hirsch</td>
      <td data-title="Time">2:00.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1958 </td>
      <td data-title="Winner">Tall Chief II </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">William Harmatz</td>
      <td data-title="Trainer">William Molter</td>
      <td data-title="Time">1:59.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1957 </td>
      <td data-title="Winner">Corn Husker </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">George Taniguchi</td>
      <td data-title="Trainer">Charles Whittingham</td>
      <td data-title="Time">2:00.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1956 </td>
      <td data-title="Winner">Star of Ross </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Rogelio Trejos</td>
      <td data-title="Trainer">William B. Finnegan</td>
      <td data-title="Time">2:01.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1955 </td>
      <td data-title="Winner">St. Vincent</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Johnny Longden</td>
      <td data-title="Trainer">Vance Longden</td>
      <td data-title="Time">2:00.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1954 </td>
      <td data-title="Winner">Determine</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Raymond York</td>
      <td data-title="Trainer">William Molter</td>
      <td data-title="Time">1:24.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1953 </td>
      <td data-title="Winner">Decorated </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Johnny Longden</td>
      <td data-title="Trainer">William Molter</td>
      <td data-title="Time">1:23.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1952 </td>
      <td data-title="Winner">Windy City II</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Willie Alavardo</td>
      <td data-title="Time">1:22.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1947 - 1951</td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Age">&nbsp;</td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1946 </td>
      <td data-title="Winner">Sun Lady </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Hubert Trent</td>
      <td data-title="Trainer">Andrew G.  Blakely</td>
      <td data-title="Time">1:10.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1945 </td>
      <td data-title="Winner">Vain Prince </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">George Woolf</td>
      <td data-title="Trainer">Clyde Phillips</td>
      <td data-title="Time">1:11.40 </td>
    </tr>
    <tr bgcolor="#eeeeee">
      <td data-title="Year">1939 </td>
      <td data-title="Winner"><em>no race 1939-1944</em></td>
      <td data-title="Age">&nbsp;</td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1938 </td>
      <td data-title="Winner">Morning Breeze </td>
      <td data-title="Age">2 </td>
      <td data-title="Jockey">Ned Merritt</td>
      <td data-title="Trainer">Hurst Philpot</td>
      <td data-title="Time">0:33.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1937 </td>
      <td data-title="Winner">Rolling Ball </td>
      <td data-title="Age">2 </td>
      <td data-title="Jockey">George Woolf</td>
      <td data-title="Trainer">Walter W. Taylor</td>
      <td data-title="Time">0:34.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1936 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Age">&nbsp;</td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1935 </td>
      <td data-title="Winner">Forced Landing </td>
      <td data-title="Age">2 </td>
      <td data-title="Jockey">Alfred Robertson</td>
      <td data-title="Trainer">John A. Healey</td>
      <td data-title="Time">0:34.60 </td>
    </tr>
  </tbody>
</table>

</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}