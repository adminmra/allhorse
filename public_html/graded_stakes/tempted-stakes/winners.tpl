<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>  
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Daisy </td>
      <td data-title="Jockey">Kendrick Carmouche</a></td>
      <td data-title="Trainer">John Servis</a></td>
      <td data-title="Time">1:37.13 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Miss Sky Warrior</a></td>
      <td data-title="Jockey">Paco Lopez</a></td>
      <td data-title="Trainer">Kelly J. Breen</a></td>
      <td data-title="Time">1:38.62 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Big World</a></td>
      <td data-title="Jockey">Jose L. Ortiz</a></td>
      <td data-title="Trainer">Anthony W. Dutrow</a></td>
      <td data-title="Time">1:37.19 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Jacaranda</a></td>
      <td data-title="Jockey">Jose L. Ortiz</a></td>
      <td data-title="Trainer">Michael E. Hushion</a></td>
      <td data-title="Time">1:40.24 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Stopchargingmaria</a></td>
      <td data-title="Jockey">Javier Castellano</a></td>
      <td data-title="Trainer">Todd Pletcher</a></td>
      <td data-title="Time">1:38.77 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">My Happy Face</a></td>
      <td data-title="Jockey">Jose Lezcano</a></td>
      <td data-title="Trainer">H. James Bond</a></td>
      <td data-title="Time">1:36.32 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Believe You Can</a></td>
      <td data-title="Jockey">Gabriel Saez</a></td>
      <td data-title="Trainer">Larry Jones</a></td>
      <td data-title="Time">1:11.20 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Full Moon Blues</a></td>
      <td data-title="Jockey">Malcolm Franklin</a></td>
      <td data-title="Trainer">Timothy Tullock Jr.</a></td>
      <td data-title="Time">1:38.56 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Ailalea</a></td>
      <td data-title="Jockey">Eibar Coa</a></td>
      <td data-title="Trainer">Todd Pletcher</a></td>
      <td data-title="Time">1:36.75 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Livin Lovin</a></td>
      <td data-title="Jockey">Ramon Dominguez</a></td>
      <td data-title="Trainer">Steve Klesaris</a></td>
      <td data-title="Time">1:36.89 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Elusive Lady</a></td>
      <td data-title="Jockey">Eibar Coa</a></td>
      <td data-title="Trainer">John Kimmel</a></td>
      <td data-title="Time">1:38.40 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Successful Outlook</a></td>
      <td data-title="Jockey">Joe Bravo</a></td>
      <td data-title="Trainer">Scott Blasi</a></td>
      <td data-title="Time">1:37.36 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Better Now</a></td>
      <td data-title="Jockey">Javier Castellano</a></td>
      <td data-title="Trainer">Mark Hennig</a></td>
      <td data-title="Time">1:38.35 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Summer Raven</a></td>
      <td data-title="Jockey">Stewart Elliott</a></td>
      <td data-title="Trainer">Todd Pletcher</a></td>
      <td data-title="Time">1:36.09 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">La Reina</a></td>
      <td data-title="Jockey">John Velazquez</a></td>
      <td data-title="Trainer">Claude McGaughey III</a></td>
      <td data-title="Time">1:36.15 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Chimichurri</a></td>
      <td data-title="Jockey">John Velazquez</a></td>
      <td data-title="Trainer">Todd Pletcher</a></td>
      <td data-title="Time">1:37.52 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Smok'n Frolic</a></td>
      <td data-title="Jockey">John Velazquez</a></td>
      <td data-title="Trainer">Todd Pletcher</a></td>
      <td data-title="Time">1:37.77 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Two Item Limit</a></td>
      <td data-title="Jockey">Richard Migliore</a></td>
      <td data-title="Trainer">Stephen DiMauro</a></td>
      <td data-title="Time">1:38.53 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Shawnee Country</a></td>
      <td data-title="Jockey">Jorge Chavez</a></td>
      <td data-title="Trainer">D. Wayne Lukas</a></td>
      <td data-title="Time">1:38.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Oh What a Windfall</a></td>
      <td data-title="Jockey">Jerry Bailey</a></td>
      <td data-title="Trainer">Claude McGaughey III</a></td>
      <td data-title="Time">1:39.84 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Dancing With Ruth</a></td>
      <td data-title="Jockey">Tommy Turner</a></td>
      <td data-title="Trainer">Benjamin Perkins Jr.</a></td>
      <td data-title="Time">1:37.55 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Ajina</a></td>
      <td data-title="Jockey">Jerry Bailey</a></td>
      <td data-title="Trainer">William Mott</a></td>
      <td data-title="Time">1:36.59 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner"><em>No Race</em></td>
      <td data-title="Jockey"></td>
      <td data-title="Trainer"></td>
      <td data-title="Time"></td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Special Broad</a></td>
      <td data-title="Jockey">Julie Krone</a></td>
      <td data-title="Trainer">Richard W. Small</a></td>
      <td data-title="Time">1:37.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Sovereign Kitty</a></td>
      <td data-title="Jockey">John Velazquez</a></td>
      <td data-title="Trainer">Richard Schosberg</a></td>
      <td data-title="Time">1:46.84 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">True Affair</a></td>
      <td data-title="Jockey">Joe Bravo</a></td>
      <td data-title="Trainer">Gary Contessa</a></td>
      <td data-title="Time">1:47.48 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Deputation</a></td>
      <td data-title="Jockey">David Lidberg</a></td>
      <td data-title="Trainer">Claude McGaughey III</a></td>
      <td data-title="Time">1:46.74 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}