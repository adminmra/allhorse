<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Discovery Handicap Past Winners">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Plainsman </td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">Brad H. Cox </td>
      <td data-title="Time">1:52.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Control Group </td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">Rudy R. Rodriguez </td>
      <td data-title="Time">1:52.83 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Sticksstatelydude </td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">Kiaran P. McLaughlin</td>
      <td data-title="Time">1:50.58 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Tommy Macho</td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Time">1:51.67 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Protonico</td>
      <td data-title="Jockey">Joe Bravo</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">1:51.92 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Romansh</td>
      <td data-title="Jockey">Jose Ortiz</td>
      <td data-title="Trainer">Thomas Albertrani</td>
      <td data-title="Time">1:48.28 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Called to Serve</td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">Nick Canani</td>
      <td data-title="Time">1:49.77 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Redeemed</td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Rick Dutrow Jr.</td>
      <td data-title="Time">1:49.22 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Stormy's Majesty</td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Domenic Galluscio</td>
      <td data-title="Time">1:50.02 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Haynesfield</td>
      <td data-title="Jockey">Ramon Dominguez</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Time">1:50.10 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Wishful Tomcat </td>
      <td data-title="Jockey">Ramon Dominguez</td>
      <td data-title="Trainer">Gary C. Contessa</td>
      <td data-title="Time">1:53.28 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Now a Victor </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Michael Trombetta</td>
      <td data-title="Time">1:50.15 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Roman Dynasty </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Time">1:51.11 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Magna Graduate </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Time">1:41.35 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Zakocity </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Patrick L. Reynolds</td>
      <td data-title="Time">1:49.78 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">During </td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:51.18 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Saint Marden </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Michael R. Matz</td>
      <td data-title="Time">1:49.13 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Evening Attire</td>
      <td data-title="Jockey">Shaun Bridgmohan</td>
      <td data-title="Trainer">Patrick J. Kelly</td>
      <td data-title="Time">1:48.62 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Left Bank</td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Time">1:47.30 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Adonis </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Time">1:50.11 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Early Warning </td>
      <td data-title="Jockey">Jorge F. Chavez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Time">1:48.94 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Mr. Sinatra </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Gasper Moschera</td>
      <td data-title="Time">1:49.55 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Gold Fever </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Claude R. McGaughey III</td>
      <td data-title="Time">1:49.01 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Michael's Star </td>
      <td data-title="Jockey">Julie Krone</td>
      <td data-title="Trainer">Guadalupe Preciado</td>
      <td data-title="Time">1:50.34 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Serious Spender </td>
      <td data-title="Jockey">Jorge F. Chavez</td>
      <td data-title="Trainer">Dominick A. Schettino</td>
      <td data-title="Time">1:51.24 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Prospector's Flag </td>
      <td data-title="Jockey">Jorge F. Chavez</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Time">1:52.30 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">New Deal </td>
      <td data-title="Jockey">Robbie Davis</td>
      <td data-title="Trainer">William Badgett, Jr</td>
      <td data-title="Time">1:48.08 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Upon My Soul </td>
      <td data-title="Jockey">Jean-Luc Samyn</td>
      <td data-title="Trainer">Richard Violette, Jr.</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Sports View </td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Tricky Creek </td>
      <td data-title="Jockey">Craig Perret</td>
      <td data-title="Trainer">George R. Arnold III</td>
      <td data-title="Time">1:50.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Dynaformer</td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:50.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Parochial </td>
      <td data-title="Jockey">Julie Krone</td>
      <td data-title="Trainer">Robert Klesaris</td>
      <td data-title="Time">1:51.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Moment of Hope </td>
      <td data-title="Jockey">Mike Venezia</td>
      <td data-title="Trainer">Bob Dunham</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Proud Truth</td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">John M. Veitch</td>
      <td data-title="Time">1:49.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Key to the Moon</td>
      <td data-title="Jockey">Dan Beckon</td>
      <td data-title="Trainer">Gil Rowntree</td>
      <td data-title="Time">1:50.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Country Pine </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Thomas L. Rondinello</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Trenchant </td>
      <td data-title="Jockey">Jean-Luc Samyn</td>
      <td data-title="Trainer">Philip G. Johnson</td>
      <td data-title="Time">1:50.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Princelet </td>
      <td data-title="Jockey">Eddie Maple</td>
      <td data-title="Trainer">John P. Campo</td>
      <td data-title="Time">1:51.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Fappiano</td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">Jan H. Nerud</td>
      <td data-title="Time">1:50.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Belle's Gold </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">Walter A. Kelley</td>
      <td data-title="Time">1:48.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Sorry Lookin </td>
      <td data-title="Jockey">Roger Velez</td>
      <td data-title="Trainer">Pete D. Anderson</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Cox's Ridge</td>
      <td data-title="Jockey">Eddie Maple</td>
      <td data-title="Trainer">Joseph B. Cantey</td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Wise Philip </td>
      <td data-title="Jockey">Daryl Montoya</td>
      <td data-title="Trainer">William Boland</td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Dr. Emil </td>
      <td data-title="Jockey">Braulio Baeza</td>
      <td data-title="Trainer">William F. Schmitt</td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Rube the Great</td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">Pancho Martin</td>
      <td data-title="Time">1:48.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Green Gambados </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">Laz Barrera</td>
      <td data-title="Time">1:48.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Forego</td>
      <td data-title="Jockey">Heliodoro Gustines</td>
      <td data-title="Trainer">Eddie Hayward</td>
      <td data-title="Time">1:47.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Forage </td>
      <td data-title="Jockey">Eddie Maple</td>
      <td data-title="Trainer">James W. Maloney</td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Autobiography</td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Pancho Martin</td>
      <td data-title="Time">1:50.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Burd Alane </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">Victor J. Nickerson</td>
      <td data-title="Time">1:47.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Hydrologist </td>
      <td data-title="Jockey">Doug Thomas</td>
      <td data-title="Trainer">Roger Laurin</td>
      <td data-title="Time">1:47.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Ardoise </td>
      <td data-title="Jockey">Eddie Belmonte</td>
      <td data-title="Trainer">Angel Penna, Sr.</td>
      <td data-title="Time">1:49.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Bold Hour</td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Bert Mulholland</td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Deck Hand † </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Scotty Schulhofer</td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Sammyren </td>
      <td data-title="Jockey">Heliodoro Gustines</td>
      <td data-title="Trainer">Burley Parke</td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Roman Brother</td>
      <td data-title="Jockey">Fernando Alvarez</td>
      <td data-title="Trainer">Burley Parke</td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1963 </td>
      <td data-title="Winner">Quest Link </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Robert D. Wingfield</td>
      <td data-title="Time">1:49.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">Comic </td>
      <td data-title="Jockey">Larry Adams</td>
      <td data-title="Trainer">James E. Fitzsimmons</td>
      <td data-title="Time">1:48.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1961 </td>
      <td data-title="Winner">Ambiopoise </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Thomas M. Waller</td>
      <td data-title="Time">1:48.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1960 </td>
      <td data-title="Winner">Kelso</td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Carl Hanford</td>
      <td data-title="Time">1:48.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1959 </td>
      <td data-title="Winner">Middle Brother </td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">E. Barry Ryan</td>
      <td data-title="Time">1:50.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1958 </td>
      <td data-title="Winner">Warhead </td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Kay Erik Jensen</td>
      <td data-title="Time">1:51.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1957 </td>
      <td data-title="Winner">Ben Lomand </td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">James E. Ryan</td>
      <td data-title="Time">1:49.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1956 </td>
      <td data-title="Winner">Reneged </td>
      <td data-title="Jockey">Ismael Valenzuela</td>
      <td data-title="Trainer">Homer C. Pardue</td>
      <td data-title="Time">1:48.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1955 </td>
      <td data-title="Winner">Westward Ho </td>
      <td data-title="Jockey">Conn McCreary</td>
      <td data-title="Trainer">not found </td>
      <td data-title="Time">1:52.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1954 </td>
      <td data-title="Winner">Chevation </td>
      <td data-title="Jockey">Eric Guerin</td>
      <td data-title="Trainer">Richard E. Handlen</td>
      <td data-title="Time">1:50.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1953 </td>
      <td data-title="Winner">Level Lea</td>
      <td data-title="Jockey">Bennie Green</td>
      <td data-title="Trainer">James E. Fitzsimmons</td>
      <td data-title="Time">1:52.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1952 </td>
      <td data-title="Winner">Ancestor </td>
      <td data-title="Jockey">Ted Atkinson</td>
      <td data-title="Trainer">James E. Fitzsimmons</td>
      <td data-title="Time">1:50.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1951 </td>
      <td data-title="Winner">Alerted </td>
      <td data-title="Jockey">Ovie Scurlock</td>
      <td data-title="Trainer">Jimmy Penrod</td>
      <td data-title="Time">1:50.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1950 </td>
      <td data-title="Winner">Sunglow </td>
      <td data-title="Jockey">Douglas Dodson</td>
      <td data-title="Trainer">Preston M. Burch</td>
      <td data-title="Time">1:49.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1949 </td>
      <td data-title="Winner">Prophets Thumb </td>
      <td data-title="Jockey">Dave Gorman</td>
      <td data-title="Trainer">Max Hirsch</td>
      <td data-title="Time">1:52.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1948 </td>
      <td data-title="Winner">Better Self </td>
      <td data-title="Jockey">Dave Gorman</td>
      <td data-title="Trainer">Max Hirsch</td>
      <td data-title="Time">1:53.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1947 </td>
      <td data-title="Winner">Cosmic Bomb</td>
      <td data-title="Jockey">Ovie Scurlock</td>
      <td data-title="Trainer">Willie Booth</td>
      <td data-title="Time">1:51.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1946 </td>
      <td data-title="Winner">Mighty Story </td>
      <td data-title="Jockey">Basil James</td>
      <td data-title="Trainer">Burley Parke</td>
      <td data-title="Time">1:51.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1945 </td>
      <td data-title="Winner">War Jeep</td>
      <td data-title="Jockey">Arnold Kirkland</td>
      <td data-title="Trainer">Tom Smith</td>
      <td data-title="Time">1:51.40</td>
    </tr>
  </tbody>
</table>

</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}