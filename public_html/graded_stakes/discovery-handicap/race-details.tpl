<table class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
cellspacing="0" border="0">
    <thead>
        <tr>
            <th>Location</th>
            <th>Distance</th>
            <th>Track</th>
            <th>Qualification</th>
            <th>Weight</th>
            <th>Purse</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><a href="/aqueduct">Aqueduct</a> Racetrack</td>
            <td>1 1/2 miles (9 furlongs)</td>
            <td>Dirt, left-handed</td>
            <td>Three-year-olds</td>
            <td>Gr. III</td>
            <td>$100,000</td>
        </tr>
    </tbody>
</table>