<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th width="70px">Age</th>        
        <th>Jockey</th>
        <th>Trainer</th>     
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2019</td>
      <td data-title="Winner">Marley's Freedom </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Drayden Van Dyke</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:22.34 </td>
    </tr>
    <tr>
      <td data-title="Year">2018</td>
      <td data-title="Winner">Selcourt </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Tyler Baze</td>
      <td data-title="Trainer">John W. Sadler</td>
      <td data-title="Time">1:21.00 </td>
    </tr>
    <tr>
      <td data-title="Year">2017</td>
      <td data-title="Winner">Finest City </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Ian Kruljac </td>
      <td data-title="Time">1:21.49 </td>
    </tr>
    <tr>
      <td data-title="Year">2016</td>
      <td data-title="Winner">Lost Bus</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Fernando Hernandez Perez</td>
      <td data-title="Trainer">Gary Sherlock</td>
      <td data-title="Time">1:21.78 </td>
    </tr>
    <tr>
      <td data-title="Year">2015</td>
      <td data-title="Winner">Sam's Sister</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Elvis Trujillo</td>
      <td data-title="Trainer">Jerry Hollendorfer</td>
      <td data-title="Time">1:22.42 </td>
    </tr>
    <tr>
      <td data-title="Year">2014</td>
      <td data-title="Winner">Scherzinger</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Jerry Hollendorfer</td>
      <td data-title="Time">1:22.00 </td>
    </tr>
    <tr>
      <td data-title="Year">2013</td>
      <td data-title="Winner">Teddy's Promise</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">Ron Ellis</td>
      <td data-title="Time">1:22.64 </td>
    </tr>
    <tr>
      <td data-title="Year">2012</td>
      <td data-title="Winner">Home Sweet Aspen</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">John W. Sadler</td>
      <td data-title="Time">1:21.42 </td>
    </tr>
    <tr>
      <td data-title="Year">2011</td>
      <td data-title="Winner">Switch</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">John W. Sadler</td>
      <td data-title="Time">1:20.70 </td>
    </tr>
    <tr>
      <td data-title="Year">2010</td>
      <td data-title="Winner">Gabby's Golden Gal</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Martin Garcia</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:21.25 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Ventura</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Garrett K. Gomez</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:21.61 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Intangaroo</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Alonso Quinonez</td>
      <td data-title="Trainer">Gary Sherlock</td>
      <td data-title="Time">1:20.71 </td>
    </tr>
    <tr>
      <td data-title="Year">2007</td>
      <td data-title="Winner">Pussycat Doll</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:22.32 </td>
    </tr>
    <tr>
      <td data-title="Year">2006</td>
      <td data-title="Winner">Behaving Badly</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:21.93 </td>
    </tr>
    <tr>
      <td data-title="Year">2005</td>
      <td data-title="Winner">Salt Champ (ARG) </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Richard Mandella</td>
      <td data-title="Time">1:22.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2004</td>
      <td data-title="Winner">Island Fashion</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Marcelo Polanco</td>
      <td data-title="Time">1:21.37 </td>
    </tr>
    <tr>
      <td data-title="Year">2003</td>
      <td data-title="Winner">Affluent</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Ron McAnally</td>
      <td data-title="Time">1:22.77 </td>
    </tr>
    <tr>
      <td data-title="Year">2002</td>
      <td data-title="Winner">Kalookan Queen</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Bruce Headley</td>
      <td data-title="Time">1:22.37 </td>
    </tr>
    <tr>
      <td data-title="Year">2001</td>
      <td data-title="Winner">Nany's Sweep</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Kathy Walsh</td>
      <td data-title="Time">1:22.50 </td>
    </tr>
    <tr>
      <td data-title="Year">2000</td>
      <td data-title="Winner">Honest Lady</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:21.45 </td>
    </tr>
    <tr>
      <td data-title="Year">1999</td>
      <td data-title="Winner">Stop Traffic</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Corey Black</td>
      <td data-title="Trainer">Richard Mandella</td>
      <td data-title="Time">1::22.17 </td>
    </tr>
    <tr>
      <td data-title="Year">1998</td>
      <td data-title="Winner">Exotic Wood</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Ronald W. Ellis</td>
      <td data-title="Time">1:21.07 </td>
    </tr>
    <tr>
      <td data-title="Year">1997</td>
      <td data-title="Winner">Toga Toga Toga</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Julio A. Garcia</td>
      <td data-title="Trainer">Eduardo Inda</td>
      <td data-title="Time">1:23.27 </td>
    </tr>
    <tr>
      <td data-title="Year">1996</td>
      <td data-title="Winner">Serena's Song</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:21.56 </td>
    </tr>
    <tr>
      <td data-title="Year">1995</td>
      <td data-title="Winner">Key Phrase</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Chris Antley</td>
      <td data-title="Trainer">Ronald W. Ellis</td>
      <td data-title="Time">1:22.82 </td>
    </tr>
    <tr>
      <td data-title="Year">1994</td>
      <td data-title="Winner">Southern Truce</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Roger Stein</td>
      <td data-title="Time">1:21.44 </td>
    </tr>
    <tr>
      <td data-title="Year">1993</td>
      <td data-title="Winner">Freedom Cry</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Gary F. Jones</td>
      <td data-title="Time">1:21.78 </td>
    </tr>
    <tr>
      <td data-title="Year">1992</td>
      <td data-title="Winner">Laramie Moon</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">Neil D. Drysdale</td>
      <td data-title="Time">1:22.66 </td>
    </tr>
    <tr>
      <td data-title="Year">1991</td>
      <td data-title="Winner">Devil's Orchid</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Russell Baze</td>
      <td data-title="Trainer">Richard Mandella</td>
      <td data-title="Time">1:21.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1990</td>
      <td data-title="Winner">Stormy But Valid</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Brian A. Mayberry</td>
      <td data-title="Time">1:22.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1989</td>
      <td data-title="Winner">Miss Brio </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">Neil D. Drysdale</td>
      <td data-title="Time">1:21.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1988</td>
      <td data-title="Winner">Pine Tree Lane </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:23.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1987</td>
      <td data-title="Winner">Pine Tree Lane </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:21.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1986</td>
      <td data-title="Winner">Her Royalty </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Bruce Headley</td>
      <td data-title="Time">1:21.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1985</td>
      <td data-title="Winner">Lovlier Linda </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Willard L. Proctor</td>
      <td data-title="Time">1:22.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1984</td>
      <td data-title="Winner">Bara Lass </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Walter Guerra</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:22.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1983</td>
      <td data-title="Winner">Past Forgetting </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Gordon Campbell</td>
      <td data-title="Time">1:23.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1982</td>
      <td data-title="Winner">Past Forgetting </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Gordon Campbell</td>
      <td data-title="Time">1:20.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1981</td>
      <td data-title="Winner">Parsley </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:23.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1980</td>
      <td data-title="Winner">Flack Flack </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:23.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1979</td>
      <td data-title="Winner">Grenzen </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Loren Rettele</td>
      <td data-title="Time">1:21.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1978</td>
      <td data-title="Winner">Winter Solstice </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Darrel McHargue</td>
      <td data-title="Trainer">Gordon C. Campbell</td>
      <td data-title="Time">1:21.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1977</td>
      <td data-title="Winner">Hail Hilarious </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Don Pierce</td>
      <td data-title="Trainer">Neil D. Drysdale</td>
      <td data-title="Time">1:22.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1976</td>
      <td data-title="Winner">Gay Style </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Don Pierce</td>
      <td data-title="Trainer">Charles Whittingham</td>
      <td data-title="Time">1:22.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1975</td>
      <td data-title="Winner">Sister Fleet </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Fernando Toro</td>
      <td data-title="Trainer">R. King </td>
      <td data-title="Time">1:21.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1974</td>
      <td data-title="Winner">Tizna</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Fernando Toro</td>
      <td data-title="Trainer">Henry M. Moreno</td>
      <td data-title="Time">1:24.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1973</td>
      <td data-title="Winner">Chou Croute </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">John L. Rotz</td>
      <td data-title="Trainer">Bob G. Dunham</td>
      <td data-title="Time">1:23.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1972</td>
      <td data-title="Winner">Typecast</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Tommy Doyle</td>
      <td data-title="Time">1:21.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1971</td>
      <td data-title="Winner">Manta </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Farrell W. Jones</td>
      <td data-title="Time">1:22.20 </td>
    </tr>
    <tr bgcolor="#eeeeee">
      <td data-title="Year">1970</td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Age"></td>
      <td data-title="Jockey"></td>
      <td data-title="Trainer"></td>
      <td data-title="Time"></td>
    </tr>
    <tr>
      <td data-title="Year">1969</td>
      <td data-title="Winner">Gamely</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Wayne Harris</td>
      <td data-title="Trainer">James W. Maloney</td>
      <td data-title="Time">1:23.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1968</td>
      <td data-title="Winner">Amerigo Lady</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Don Pierce</td>
      <td data-title="Trainer">Robert L. Wheeler</td>
      <td data-title="Time">1:23.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1967</td>
      <td data-title="Winner">Miss Moona </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Steve Ippolito</td>
      <td data-title="Time">1:22.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1966</td>
      <td data-title="Winner">Batteur </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Eddie Belmonte</td>
      <td data-title="Trainer">James W. Maloney</td>
      <td data-title="Time">1:22.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1965</td>
      <td data-title="Winner">Face The Facts </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Manuel Ycaza</td>
      <td data-title="Trainer">James W. Maloney</td>
      <td data-title="Time">1:22.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1965</td>
      <td data-title="Winner">Chop House </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Ismael Valenzuela</td>
      <td data-title="Trainer">Ted Saladin</td>
      <td data-title="Time">1:22.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1964</td>
      <td data-title="Winner">Chop House </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Braulio Baeza</td>
      <td data-title="Trainer">Ted Saladin</td>
      <td data-title="Time">1:22.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1963</td>
      <td data-title="Winner">Table Mate </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Warren Stute</td>
      <td data-title="Time">1:22.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1962</td>
      <td data-title="Winner">Perizade </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Rudy Campas</td>
      <td data-title="Trainer">Noble Threewitt</td>
      <td data-title="Time">1:22.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1961</td>
      <td data-title="Winner">Taboo </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">John H. Adams</td>
      <td data-title="Time">1:23.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1961</td>
      <td data-title="Winner">Swiss Roll </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Robert L. Wheeler</td>
      <td data-title="Time">1:22.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1960</td>
      <td data-title="Winner">Silver Spoon</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">Robert L. Wheeler</td>
      <td data-title="Time">1:23.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1959</td>
      <td data-title="Winner">Bug Brush </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Angel Valenzuela</td>
      <td data-title="Trainer">Robert L. Wheeler</td>
      <td data-title="Time">1:23.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1958</td>
      <td data-title="Winner">Market Basket </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Raymond York</td>
      <td data-title="Trainer">J. M. Phillips</td>
      <td data-title="Time">1:22.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1957</td>
      <td data-title="Winner">Mary Machree </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">George Taniguchi</td>
      <td data-title="Trainer">William B. Finnegan</td>
      <td data-title="Time">1:22.00 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}