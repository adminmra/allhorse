<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>        
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Noble Indy </td>
      <td data-title="Jockey">John R. Velazquez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">WinStar Farm</td>
      <td data-title="Time">1:50.28 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Girvin </td>
      <td data-title="Jockey">Brian Hernandez Jr.</td>
      <td data-title="Trainer">Joe Sharp</td>
      <td data-title="Owner">Brad Grady</td>
      <td data-title="Time">1:49.77 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Gun Runner</td>
      <td data-title="Jockey">Florent Geroux</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">Three Chimneys</td>
      <td data-title="Time">1:51.06 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">International Star </td>
      <td data-title="Jockey">Miguel Mena</td>
      <td data-title="Trainer">Michael Maker</td>
      <td data-title="Owner">Kenneth &amp; Sarah Ramsey</td>
      <td data-title="Time">1:50.67 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Vicar's in Trouble </td>
      <td data-title="Jockey">Rosie Napravnik</td>
      <td data-title="Trainer">Michael Maker</td>
      <td data-title="Owner">Kenneth &amp; Sarah Ramsey</td>
      <td data-title="Time">1:50.77 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Revolutionary </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">WinStar Farm</td>
      <td data-title="Time">1:50.28 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Hero of Order </td>
      <td data-title="Jockey">Eddie Martin Jr.</td>
      <td data-title="Trainer">Gennadi Dorochenko</td>
      <td data-title="Owner">Raut LLC </td>
      <td data-title="Time">1:50.13 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Pants on Fire</td>
      <td data-title="Jockey">Rosie Napravnik</td>
      <td data-title="Trainer">Kelly Breen</td>
      <td data-title="Owner">George &amp; Lori Hall </td>
      <td data-title="Time">1:49.92 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Mission Impazible </td>
      <td data-title="Jockey">Rajiv Maragh</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Twin Creeks Rac.Stable </td>
      <td data-title="Time">1:50.32 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Friesan Fire</td>
      <td data-title="Jockey">Gabriel Saez</td>
      <td data-title="Trainer">J. Larry Jones</td>
      <td data-title="Owner">Fox Hill Farm</td>
      <td data-title="Time">1:43.46 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Pyro</td>
      <td data-title="Jockey">Sh.Bridgmohan</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">Winchell Thoroughbreds </td>
      <td data-title="Time">1:44.44 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Circular Quay</td>
      <td data-title="Jockey">John R. Velazquez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Owner">Michael Tabor</td>
      <td data-title="Time">1:43.01 </td>
    </tr>
    <tr>
      <td data-title="Year">1895</td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Owner">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">High Limit </td>
      <td data-title="Jockey">Ramon Dominguez</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Owner">Gary &amp; Mary West </td>
      <td data-title="Time">1:42.74 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Wimbledon</td>
      <td data-title="Jockey">Javier Santiago</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">James McIngvale </td>
      <td data-title="Time">1:42.71 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Peace Rules</td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Owner">Edmund A. Gann</td>
      <td data-title="Time">1:42.67 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Repent</td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Kenneth McPeek</td>
      <td data-title="Owner">Select Stable </td>
      <td data-title="Time">1:43.86 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Fifty Stars</td>
      <td data-title="Jockey">Donnie Meche</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">J. Cassells / R. Zollars </td>
      <td data-title="Time">1:44.76 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Mighty </td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">Frank L. Brothers</td>
      <td data-title="Owner">Aspiration Stable </td>
      <td data-title="Time">1:43.29 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Kimberlite Pipe</td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Dallas Stewart</td>
      <td data-title="Owner">J. D. Gunther </td>
      <td data-title="Time">1:43.56 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Comic Strip</td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">Neil J. Howard</td>
      <td data-title="Owner">William S. Farish III</td>
      <td data-title="Time">1:43.36 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Crypto Star</td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Wayne Catalano</td>
      <td data-title="Owner">Darrell &amp; Evelyn Yates </td>
      <td data-title="Time">1:42.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Grindstone</td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Overbrook Farm</td>
      <td data-title="Time">1:42.79 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Petionville </td>
      <td data-title="Jockey">Chris Antley</td>
      <td data-title="Trainer">Randy Bradshaw</td>
      <td data-title="Owner">Everest Stables </td>
      <td data-title="Time">1:42.96 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Kandaly</td>
      <td data-title="Jockey">Craig Perret</td>
      <td data-title="Trainer">Louie J. Roussel III</td>
      <td data-title="Owner">Louie J. Roussel III</td>
      <td data-title="Time">1:42.86 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Dixieland Heat</td>
      <td data-title="Jockey">Randy Romero</td>
      <td data-title="Trainer">Gerald Romero</td>
      <td data-title="Owner">Leland Cook </td>
      <td data-title="Time">1:44.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Line In The Sand </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Neil J. Howard</td>
      <td data-title="Owner">William S. Farish III</td>
      <td data-title="Time">1:43.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Richman </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Owner">William F. Lucas </td>
      <td data-title="Time">1:44.50 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Heaven Again </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Vincent Timphony</td>
      <td data-title="Owner">K. Timphony et al. </td>
      <td data-title="Time">1:43.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Dispersal </td>
      <td data-title="Jockey">Jose Santos</td>
      <td data-title="Trainer">Bud Delp</td>
      <td data-title="Owner">Henry &amp; Teresa Mayerhoff</td>
      <td data-title="Time">1:43.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Risen Star</td>
      <td data-title="Jockey">Shane Romero</td>
      <td data-title="Trainer">Louie J. Roussel III</td>
      <td data-title="Owner">R. Lamarque/L. J. Roussel III</td>
      <td data-title="Time">1:42.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">J. T.'s Pet </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Lynn S. Whiting</td>
      <td data-title="Owner">W. Cal Partee</td>
      <td data-title="Time">1:51.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Country Light </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">Steve C. Penrod</td>
      <td data-title="Owner">Cherry Valley Farm</td>
      <td data-title="Time">1:50.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Violado </td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">William E. Burch</td>
      <td data-title="Owner">Due Process Stable</td>
      <td data-title="Time">1:50.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Taylor's Special </td>
      <td data-title="Jockey">Sam Maple</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Owner">William F. Lucas </td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Balboa Native </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Owner">Robert N. Spreen </td>
      <td data-title="Time">1:50.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">El Baba </td>
      <td data-title="Jockey">Don Brumfield</td>
      <td data-title="Trainer">Dewey P. Smith</td>
      <td data-title="Owner">Dorothy D. Brown</td>
      <td data-title="Time">1:50.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Woodchopper</td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">Robert J. Reinacher, Jr.</td>
      <td data-title="Owner">Greentree Stable</td>
      <td data-title="Time">1:50.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Prince Valiant </td>
      <td data-title="Jockey">Mike A. Gonzalez</td>
      <td data-title="Trainer">John M. Gaver, Jr.</td>
      <td data-title="Owner">Greentree Stable</td>
      <td data-title="Time">1:50.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Golden Act</td>
      <td data-title="Jockey">Sandy Hawley</td>
      <td data-title="Trainer">Loren Rettele</td>
      <td data-title="Owner">W. H. Oldknow &amp; R. W. Phipps </td>
      <td data-title="Time">1:51.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Esop's Foibles </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Loren Rettele</td>
      <td data-title="Owner">Jerry Frankel </td>
      <td data-title="Time">1:50.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Clev Er Tell </td>
      <td data-title="Jockey">Ray Broussard</td>
      <td data-title="Trainer">Homer C. Pardue</td>
      <td data-title="Owner">J. R. Straus &amp; I. Proler </td>
      <td data-title="Time">1:48.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Johnny Appleseed </td>
      <td data-title="Jockey">Marco Castaneda</td>
      <td data-title="Trainer">John M. Gaver, Jr.</td>
      <td data-title="Owner">Greentree Stable</td>
      <td data-title="Time">1:49.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Master Derb</td>
      <td data-title="Jockey">Darrel McHargue</td>
      <td data-title="Trainer">William E. Adams</td>
      <td data-title="Owner">Golden Chance Farm </td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Sellout </td>
      <td data-title="Jockey">Marco Castaneda</td>
      <td data-title="Trainer">James P. Conway</td>
      <td data-title="Owner">Jean Pancoast </td>
      <td data-title="Time">1:51.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Leo's Pisces </td>
      <td data-title="Jockey">Robert Breen</td>
      <td data-title="Trainer">Abe Simoff </td>
      <td data-title="Owner">Joseph Taub </td>
      <td data-title="Time">1:51.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">No Le Hace</td>
      <td data-title="Jockey">Phil Rubbicco </td>
      <td data-title="Trainer">Homer C. Pardue</td>
      <td data-title="Owner">Joseph W. Strauss </td>
      <td data-title="Time">1:52.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Northfields </td>
      <td data-title="Jockey">Walter Blum</td>
      <td data-title="Trainer">Woody Stephens</td>
      <td data-title="Owner">John M. Olin</td>
      <td data-title="Time">1:50.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Jim's Alibhi </td>
      <td data-title="Jockey">Ronnie Baldwin </td>
      <td data-title="Trainer">James J. Eckrosh </td>
      <td data-title="Owner">J. Eckrosh &amp;  Est. of F. Akin </td>
      <td data-title="Time">1:55.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">King of the Castle </td>
      <td data-title="Jockey">Carlos H. Marquez </td>
      <td data-title="Trainer">Oliver Cutshaw </td>
      <td data-title="Owner">Ogden Phipps</td>
      <td data-title="Time">1:52.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Kentucky Sherry </td>
      <td data-title="Jockey">Jimmy Combest</td>
      <td data-title="Trainer">Alcee Richard </td>
      <td data-title="Owner">Dorothy D. Brown </td>
      <td data-title="Time">1:50.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Ask the Fare </td>
      <td data-title="Jockey">Donald Holmes </td>
      <td data-title="Trainer">Jere R. Smith, Sr. </td>
      <td data-title="Owner">Holiday Stable </td>
      <td data-title="Time">1:50.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Blue Skyer </td>
      <td data-title="Jockey">Ray Broussard </td>
      <td data-title="Trainer">James A. Padgett </td>
      <td data-title="Owner">M. Padgett &amp; H. Grant </td>
      <td data-title="Time">1:50.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Dapper Delegate </td>
      <td data-title="Jockey">John Heckmann</td>
      <td data-title="Trainer">Alcee Richard </td>
      <td data-title="Owner">Dorothy D. Brown </td>
      <td data-title="Time">1:50.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Grecian Princess </td>
      <td data-title="Jockey">Kelly Broussard </td>
      <td data-title="Trainer">Peter C. Keiser </td>
      <td data-title="Owner">Theodore D. Buhl </td>
      <td data-title="Time">1:50.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1963 </td>
      <td data-title="Winner">City Line </td>
      <td data-title="Jockey">Robert L. Baird</td>
      <td data-title="Trainer">John O. Meaux </td>
      <td data-title="Owner">T. Alie Grissom</td>
      <td data-title="Time">1:50.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">Admiral's Voyage </td>
      <td data-title="Jockey">Ray Broussard </td>
      <td data-title="Trainer">Charles R. Parke</td>
      <td data-title="Owner">Fred W. Hooper</td>
      <td data-title="Time">1:52.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1961 </td>
      <td data-title="Winner">Bass Clef </td>
      <td data-title="Jockey">Ronnie Baldwin </td>
      <td data-title="Trainer">Anthony W. Rupelt </td>
      <td data-title="Owner">Mrs. V. E. Smith </td>
      <td data-title="Time">1:50.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1960 </td>
      <td data-title="Winner">Tony Graff </td>
      <td data-title="Jockey">Wayne Chambers</td>
      <td data-title="Trainer">Nicholas Graffagnini </td>
      <td data-title="Owner">Anthony Graffagnini </td>
      <td data-title="Time">1:52.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1959 </td>
      <td data-title="Winner">Master Palynch </td>
      <td data-title="Jockey">Ray Broussard </td>
      <td data-title="Trainer">Elmer Kalensky </td>
      <td data-title="Owner">S. Sair &amp; B. Hatskin </td>
      <td data-title="Time">1:49.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1958 </td>
      <td data-title="Winner">Royal Union </td>
      <td data-title="Jockey">John Heckmann </td>
      <td data-title="Trainer">Frank Sanders </td>
      <td data-title="Owner">Reverie Knoll Farm </td>
      <td data-title="Time">1:52.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1957 </td>
      <td data-title="Winner">Federal Hill </td>
      <td data-title="Jockey">Willie Carstens </td>
      <td data-title="Trainer">Stanley M. Rieser</td>
      <td data-title="Owner">Clifford Lussky </td>
      <td data-title="Time">1:49.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1956 </td>
      <td data-title="Winner">Reaping Right </td>
      <td data-title="Jockey">Robert L. Baird </td>
      <td data-title="Trainer">Vester R. Wright</td>
      <td data-title="Owner">T. Alie Grissom </td>
      <td data-title="Time">1:51.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1955 </td>
      <td data-title="Winner">Roman Patrol </td>
      <td data-title="Jockey">Douglas Dodson</td>
      <td data-title="Trainer">Joseph H. Pierce, Jr.</td>
      <td data-title="Owner">Pin Oak Farm</td>
      <td data-title="Time">1:49.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1954 </td>
      <td data-title="Winner">Gigantic </td>
      <td data-title="Jockey">Rich McLaughlin </td>
      <td data-title="Trainer">John B. Theall</td>
      <td data-title="Owner">Joe W. Brown</td>
      <td data-title="Time">1:53.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1953 </td>
      <td data-title="Winner">Matagorda </td>
      <td data-title="Jockey">Paul J. Bailey</td>
      <td data-title="Trainer">John B. Theall </td>
      <td data-title="Owner">Joe W. Brown </td>
      <td data-title="Time">1:51.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1952 </td>
      <td data-title="Winner">Gushing Oil </td>
      <td data-title="Jockey">Al Popara </td>
      <td data-title="Trainer">Joseph Jansen </td>
      <td data-title="Owner">Sam E. Wilson Jr. </td>
      <td data-title="Time">1:51.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1951 </td>
      <td data-title="Winner">Whirling Bat </td>
      <td data-title="Jockey">Pete Anderson</td>
      <td data-title="Trainer">Thomas G. May </td>
      <td data-title="Owner">Thomas G. May </td>
      <td data-title="Time">1:53.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1950 </td>
      <td data-title="Winner">Greek Ship </td>
      <td data-title="Jockey">Con Errico</td>
      <td data-title="Trainer">Preston M. Burch</td>
      <td data-title="Owner">Brookmeade Stable</td>
      <td data-title="Time">1:51.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1949 </td>
      <td data-title="Winner">Rookwood </td>
      <td data-title="Jockey">John Delahoussaye </td>
      <td data-title="Trainer">John B. Theall </td>
      <td data-title="Owner">Joe W. Brown </td>
      <td data-title="Time">1:51.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1948 </td>
      <td data-title="Winner">Bovard </td>
      <td data-title="Jockey">Willie Saunders</td>
      <td data-title="Trainer">James J. Rowan </td>
      <td data-title="Owner">Sylvester W. Labrot </td>
      <td data-title="Time">1:51.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1947 </td>
      <td data-title="Winner">Carolyn A. </td>
      <td data-title="Jockey">Ronnie Nash</td>
      <td data-title="Trainer">James P. Conway </td>
      <td data-title="Owner">Ben F. Whitaker </td>
      <td data-title="Time">1:57.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1946 </td>
      <td data-title="Winner">Pellicle </td>
      <td data-title="Jockey">Andrew LoTurco </td>
      <td data-title="Trainer">E. Leigh Cotton </td>
      <td data-title="Owner">Hal Price Headley</td>
      <td data-title="Time">1:52.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1945</td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Owner">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1944 </td>
      <td data-title="Winner">Olympic Zenith </td>
      <td data-title="Jockey">Nick Jemas </td>
      <td data-title="Trainer">Edward L. Snyder </td>
      <td data-title="Owner">William G. Helis Sr.</td>
      <td data-title="Time">1:54.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1943 </td>
      <td data-title="Winner">Amber Light </td>
      <td data-title="Jockey">Johnny Longden</td>
      <td data-title="Trainer">Jack C. Hodgins </td>
      <td data-title="Owner">Dixiana</td>
      <td data-title="Time">1:52.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1942-1940</td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Owner">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1939 </td>
      <td data-title="Winner">Day Off </td>
      <td data-title="Jockey">Eddie Arcaro</td>
      <td data-title="Trainer">E. Leigh Cotton </td>
      <td data-title="Owner">Greentree Stable</td>
      <td data-title="Time">1:52.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1938 </td>
      <td data-title="Winner">Wise Fox</td>
      <td data-title="Jockey">Johnny Longden </td>
      <td data-title="Trainer">Alfred G. Tarn</td>
      <td data-title="Owner">Alfred G. Tarn </td>
      <td data-title="Time">1:51.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1937 </td>
      <td data-title="Winner">Grey Count </td>
      <td data-title="Jockey">Charles Corbett </td>
      <td data-title="Trainer">Anthony Pelleteri </td>
      <td data-title="Owner">Millsdale Stable </td>
      <td data-title="Time">1:50.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1936 </td>
      <td data-title="Winner">Rushaway</td>
      <td data-title="Jockey">Johnny Longden </td>
      <td data-title="Trainer">Alfred G. Tarn </td>
      <td data-title="Owner">Alfred G. Tarn </td>
      <td data-title="Time">1:50.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1935 </td>
      <td data-title="Winner">McCarthy </td>
      <td data-title="Jockey">Paul Keester </td>
      <td data-title="Trainer">Thomas E. Keating </td>
      <td data-title="Owner">Morrison &amp; Keating </td>
      <td data-title="Time">1:54.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1934 </td>
      <td data-title="Winner">Hickory Lad </td>
      <td data-title="Jockey">Jack Westrope</td>
      <td data-title="Trainer">O. L. Foster </td>
      <td data-title="Owner">William C. Reichert </td>
      <td data-title="Time">1:53.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1933 </td>
      <td data-title="Winner">Col. Hatfield </td>
      <td data-title="Jockey">Carl Meyer </td>
      <td data-title="Trainer">John Parmalee </td>
      <td data-title="Owner">M. B. Cohen </td>
      <td data-title="Time">1:56.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1932 </td>
      <td data-title="Winner">Lucky Tom </td>
      <td data-title="Jockey">Anthony Pascuma </td>
      <td data-title="Trainer">Henry C. Riddle </td>
      <td data-title="Owner">J. J. Robinson </td>
      <td data-title="Time">1:53.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1931 </td>
      <td data-title="Winner">Spanish Play </td>
      <td data-title="Jockey">Charles Landolt </td>
      <td data-title="Trainer">George Land </td>
      <td data-title="Owner">Knebelkamp &amp; Morris </td>
      <td data-title="Time">1:51.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1930 </td>
      <td data-title="Winner">Michigan Boy </td>
      <td data-title="Jockey">Jimmy Shelton </td>
      <td data-title="Trainer">J. Butler </td>
      <td data-title="Owner">John L. Pontius </td>
      <td data-title="Time">2:00.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1929 </td>
      <td data-title="Winner">Calf Roper </td>
      <td data-title="Jockey">Frank Coltiletti</td>
      <td data-title="Trainer">Charles E. Durnell</td>
      <td data-title="Owner">Three D's Stock Farm Stable </td>
      <td data-title="Time">1:56.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1928 </td>
      <td data-title="Winner">Jack Higgins </td>
      <td data-title="Jockey">Charles E. Allen </td>
      <td data-title="Trainer">William J. Curran </td>
      <td data-title="Owner">William J. Curran </td>
      <td data-title="Time">1:52.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1927 </td>
      <td data-title="Winner">Boo </td>
      <td data-title="Jockey">G. Johnson </td>
      <td data-title="Trainer">William A. Hurley</td>
      <td data-title="Owner">Idle Hour Stock Farm Stable</td>
      <td data-title="Time">1:51.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1926 </td>
      <td data-title="Winner">Bagenbaggage</td>
      <td data-title="Jockey">Eric Blind</td>
      <td data-title="Trainer">William A. Hurley </td>
      <td data-title="Owner">Idle Hour Stock Farm Stable </td>
      <td data-title="Time">1:51.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1925 </td>
      <td data-title="Winner">Quatrain </td>
      <td data-title="Jockey">Harry Stutts </td>
      <td data-title="Trainer">Thomas J. Harmon </td>
      <td data-title="Owner">Frederick Johnson </td>
      <td data-title="Time">1:56.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1924 </td>
      <td data-title="Winner">Black Gold</td>
      <td data-title="Jockey">J. D. Mooney</td>
      <td data-title="Trainer">Hanley Webb</td>
      <td data-title="Owner">Rosa L. Hoots </td>
      <td data-title="Time">1:57.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1923 </td>
      <td data-title="Winner">Amole </td>
      <td data-title="Jockey">J. D. Mooney </td>
      <td data-title="Trainer">Frank P. Letellier </td>
      <td data-title="Owner">Southland Stable </td>
      <td data-title="Time">1:57.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1922 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Owner">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1921 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Owner">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1920 </td>
      <td data-title="Winner">Damask </td>
      <td data-title="Jockey">Eddie Ambrose</td>
      <td data-title="Trainer">Mose Goldblatt </td>
      <td data-title="Owner">Harry Payne Whitney</td>
      <td data-title="Time">1:51.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1919-1909</td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Owner">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1908 </td>
      <td data-title="Winner">Meelick </td>
      <td data-title="Jockey">Eddie Dugan</td>
      <td data-title="Trainer">Sam Hildreth</td>
      <td data-title="Owner">Sam Hildreth </td>
      <td data-title="Time">1:51.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1907 </td>
      <td data-title="Winner">Montgomery </td>
      <td data-title="Jockey">Carroll Shilling</td>
      <td data-title="Trainer">Frank E. Brown </td>
      <td data-title="Owner">Emil Hertz </td>
      <td data-title="Time">1:53.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1906 </td>
      <td data-title="Winner">Guiding Star </td>
      <td data-title="Jockey">Jack Martin</td>
      <td data-title="Trainer">Sam Hildreth </td>
      <td data-title="Owner">Sam Hildreth </td>
      <td data-title="Time">1:54.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1905 </td>
      <td data-title="Winner">Right Royal </td>
      <td data-title="Jockey">Jack Martin </td>
      <td data-title="Trainer">Roger Minton </td>
      <td data-title="Owner">Morris L. Hayman </td>
      <td data-title="Time">1:59.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1904 </td>
      <td data-title="Winner">Ostrich </td>
      <td data-title="Jockey">Grover Fuller</td>
      <td data-title="Trainer"></td>
      <td data-title="Owner">G. L. Richards </td>
      <td data-title="Time">1:52.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1903 </td>
      <td data-title="Winner">Witfull </td>
      <td data-title="Jockey">Willie Gannon </td>
      <td data-title="Trainer">Sam Hildreth </td>
      <td data-title="Owner">Sam Hildreth </td>
      <td data-title="Time">2:07.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1902 </td>
      <td data-title="Winner">Lord Quex </td>
      <td data-title="Jockey">Jimmy Winkfield</td>
      <td data-title="Trainer">Albert Simons</td>
      <td data-title="Owner">Albert Simons </td>
      <td data-title="Time">2:00.25 </td>
    </tr>
    <tr>
      <td data-title="Year">1901 </td>
      <td data-title="Winner">Henry Clay Rye </td>
      <td data-title="Jockey">Harry Cochran </td>
      <td data-title="Trainer"></td>
      <td data-title="Owner">Wallace C. Fessenden </td>
      <td data-title="Time">1:55.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1900 </td>
      <td data-title="Winner">Prince of Veronia </td>
      <td data-title="Jockey">Eddie McJoynt </td>
      <td data-title="Trainer">Robert J. Walden</td>
      <td data-title="Owner">David H. Morris</td>
      <td data-title="Time">2:00.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1899 </td>
      <td data-title="Winner">King Barleycorn </td>
      <td data-title="Jockey">William Martin </td>
      <td data-title="Trainer">Edward W. Heffner</td>
      <td data-title="Owner">Edward W. Heffner </td>
      <td data-title="Time">1:54.25 </td>
    </tr>
    <tr>
      <td data-title="Year">1898 </td>
      <td data-title="Winner">Presbyterian </td>
      <td data-title="Jockey">Tommy Burns</td>
      <td data-title="Trainer">George Walker </td>
      <td data-title="Owner">John W. Schorr</td>
      <td data-title="Time">1:56.50 </td>
    </tr>
    <tr>
      <td data-title="Year">1897 </td>
      <td data-title="Winner">Meadowthorpe </td>
      <td data-title="Jockey">Tommy Murphy </td>
      <td data-title="Trainer"></td>
      <td data-title="Owner">John W. Schorr </td>
      <td data-title="Time"></td>
    </tr>
    <tr>
      <td data-title="Year">1896</td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Owner">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>    
    <tr>
      <td data-title="Year">1895</td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Owner">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1894 </td>
      <td data-title="Winner">Buckwa </td>
      <td data-title="Jockey">Roy Williams </td>
      <td data-title="Trainer"></td>
      <td data-title="Owner">T. H. Stevens </td>
      <td data-title="Time">1:51.00 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}