<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th width="70px">Age</th>
        <th>Jockey</th>
        <th>Trainer</th>     
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Flipcup</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Luis Contreras </td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Time">2:03.42 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Strut the Course </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Luis Contreras</td>
      <td data-title="Trainer">Barb Minshall</td>
      <td data-title="Time">2:02.74 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Moonlight Beauty </td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">Gary Boulanger</td>
      <td data-title="Trainer">John P. LeBlanc, Jr.</td>
      <td data-title="Time">2:04.89 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Smart Sting </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eurico Da Silva</td>
      <td data-title="Trainer">Roger Attfield</td>
      <td data-title="Time">2:03.77 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Masked Maiden </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Omar Moreno</td>
      <td data-title="Trainer">Mark Casse</td>
      <td data-title="Time">2:04.96 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Pachattack </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Chantal Sutherland</td>
      <td data-title="Trainer">Gerard Butler</td>
      <td data-title="Time">2:02.18 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Serenading </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Patrick Husbands</td>
      <td data-title="Trainer">Josie Carroll</td>
      <td data-title="Time">2:06.19 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Tell It as It Is </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Emma-Jayne Wilson</td>
      <td data-title="Trainer">James Smith</td>
      <td data-title="Time">2:04.99 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Like A Gem </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Emma-Jayne Wilson</td>
      <td data-title="Trainer">Daniel J. Vella</td>
      <td data-title="Time">2:03.51 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Howaboutrightnow </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Emma-Jayne Wilson</td>
      <td data-title="Trainer">Mark E. Casse</td>
      <td data-title="Time">2:07.66 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Ballroom Deputy </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Dino Luciani</td>
      <td data-title="Trainer">Josie Carroll</td>
      <td data-title="Time">2:05.77 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">One For Rose</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Emile Ramsammy</td>
      <td data-title="Trainer">Sid C. Attard</td>
      <td data-title="Time">2:04.87 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">One For Rose</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Emile Ramsammy</td>
      <td data-title="Trainer">Sid C. Attard</td>
      <td data-title="Time">2:03.50 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Lady Shari </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Constant Montpellier</td>
      <td data-title="Trainer">David Cotey</td>
      <td data-title="Time">2:06.22 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Catch The Ring </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Robert Landry</td>
      <td data-title="Trainer">Mark Frostad</td>
      <td data-title="Time">2:03.62 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">On A Soapbox </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Gary Boulanger</td>
      <td data-title="Trainer">Niall O'Callaghan</td>
      <td data-title="Time">2:05.94 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">With Flair </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">David Clark</td>
      <td data-title="Trainer">Mark Frostad</td>
      <td data-title="Time">2:05.54 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">No Foul Play </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Steve Bahen</td>
      <td data-title="Trainer">Margaret Spencer</td>
      <td data-title="Time">2:06.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Blue and Red </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Todd Kabel</td>
      <td data-title="Trainer">Sid C. Attard</td>
      <td data-title="Time">2:04.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Wings of Erin</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jim McAleney</td>
      <td data-title="Trainer">Thomas O'Keefe</td>
      <td data-title="Time">2:09.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Wings of Erin</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">K. Willey </td>
      <td data-title="Trainer">Thomas O'Keefe</td>
      <td data-title="Time">2:02.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Smiles With A Fist</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Dave Penna</td>
      <td data-title="Trainer">Michelle Bonte</td>
      <td data-title="Time">2:05.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Dance For Donna</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Robert Landry</td>
      <td data-title="Trainer">Sherry Noakes</td>
      <td data-title="Time">2:07.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Adorned</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Robert Landry</td>
      <td data-title="Trainer">Macdonald Benson</td>
      <td data-title="Time">2:05.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Musical Respite</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Daniel J. David</td>
      <td data-title="Trainer">R. Wright </td>
      <td data-title="Time">2:06.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Star Standing</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">P. Johnson </td>
      <td data-title="Trainer">Peter M. Vestal</td>
      <td data-title="Time">2:05.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">One For Bert</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jim McAleney</td>
      <td data-title="Trainer">John Cardella</td>
      <td data-title="Time">2:07.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Arcroyal</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Don Seymour</td>
      <td data-title="Trainer">Roger Attfield</td>
      <td data-title="Time">2:09.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Triple Wow</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Don Seymour</td>
      <td data-title="Trainer">Roger Attfield</td>
      <td data-title="Time">2:08.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Gypsy Sunshine</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Irwin Driedger</td>
      <td data-title="Trainer">Gerry Belanger</td>
      <td data-title="Time">2:08.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">In My Cap</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Irwin Driedger</td>
      <td data-title="Trainer">James E. Day</td>
      <td data-title="Time">2:07.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Sintrillium</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Brian Swatuk</td>
      <td data-title="Trainer">Conrad Cohen</td>
      <td data-title="Time">2:08.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Sintrillium</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Brian Swatuk</td>
      <td data-title="Trainer">Conrad Cohen</td>
      <td data-title="Time">2:10.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Eternal Search</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Brian Swatuk</td>
      <td data-title="Trainer">Edward Mann</td>
      <td data-title="Time">2:09.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Quantra</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">George HoSang</td>
      <td data-title="Trainer">Jerry G. Lavigne</td>
      <td data-title="Time">2:07.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Orphanella</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">J. Paul Souter</td>
      <td data-title="Trainer">A. Wick </td>
      <td data-title="Time">2:05.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Glorious Song</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">John LeBlanc</td>
      <td data-title="Trainer">Fred H. Loschke</td>
      <td data-title="Time">2:08.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Male Strike</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jim Walford</td>
      <td data-title="Trainer">John Cardella</td>
      <td data-title="Time">2:08.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Bye Bye Paris</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jeffrey Fell</td>
      <td data-title="Trainer">Donnie Walker</td>
      <td data-title="Time">2:05.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Bye Bye Paris</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jeffrey Fell</td>
      <td data-title="Trainer">Donnie Walker</td>
      <td data-title="Time">2:03.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Lovely Sunrise</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Brian Swatuk</td>
      <td data-title="Trainer">Donnie Walker</td>
      <td data-title="Time">2:06.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Lovely Sunrise</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">John LeBlanc</td>
      <td data-title="Trainer">Donnie Walker</td>
      <td data-title="Time">2:04.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Musketeer Miss</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">John Bell</td>
      <td data-title="Trainer">Pat Remillard</td>
      <td data-title="Time">2:09.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Lauries Dancer</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Sandy Hawley</td>
      <td data-title="Trainer">James C. Bentley</td>
      <td data-title="Time">2:07.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Page Me Doll</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">D. Thomas </td>
      <td data-title="Trainer">Glenn Magnusson</td>
      <td data-title="Time">2:05.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Not Too Shy</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Sandy Hawley</td>
      <td data-title="Trainer">Donnie Walker</td>
      <td data-title="Time">2:10.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Not Too Shy</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Brian Swatuk</td>
      <td data-title="Trainer">Donnie Walker</td>
      <td data-title="Time">2:05.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Hometown News</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Richard Grubb</td>
      <td data-title="Trainer">Gil Rowntree</td>
      <td data-title="Time">2:07.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Hinemoa</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Avelino Gomez</td>
      <td data-title="Trainer">Paul Cooper</td>
      <td data-title="Time">2:06.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Hinemoa</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Wayne Harris</td>
      <td data-title="Trainer">Paul Cooper</td>
      <td data-title="Time">2:06.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Lady Victoria</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jim Fitzsimmons</td>
      <td data-title="Trainer">R. Richards </td>
      <td data-title="Time">1:45.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Ciboulette</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Eric Walsh</td>
      <td data-title="Trainer">Duke Campbell</td>
      <td data-title="Time">1:39.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1963 </td>
      <td data-title="Winner">Court Royal</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jim Fitzsimmons</td>
      <td data-title="Trainer">N. Julius </td>
      <td data-title="Time">1:40.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">Golden Turkey</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">H. Bolin </td>
      <td data-title="Trainer">Wilfrid L. Sayles </td>
      <td data-title="Time">1:41.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1961 </td>
      <td data-title="Winner">Golden Turkey</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">B. DeSpirito </td>
      <td data-title="Trainer">Wilfrid L. Sayles </td>
      <td data-title="Time">1:38.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1960 </td>
      <td data-title="Winner">Skinny Minny</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Al Coy</td>
      <td data-title="Trainer">John Passero</td>
      <td data-title="Time">1:38.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1959 </td>
      <td data-title="Winner">Wonder Where</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Al Coy</td>
      <td data-title="Trainer">Yonnie Starr</td>
      <td data-title="Time">1:44.60 </td>
    </tr>
  </tbody>
</table>

</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}