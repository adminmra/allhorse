<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" >
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Signalman </td>
      <td data-title="Jockey">Brian Joseph Hernandez, Jr. </td>
      <td data-title="Trainer">Kenneth G. McPeek </td>
      <td data-title="Time">1:45.29 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Enticed </td>
      <td data-title="Jockey">Junior Alvarado </td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Time">1:44.42 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">McCraken</td>
      <td data-title="Jockey">Brian Joseph Hernandez, Jr.</td>
      <td data-title="Trainer">Ian R. Wilkes</td>
      <td data-title="Time">1:44.15 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Airoforce</td>
      <td data-title="Jockey">Julien Leparoux</td>
      <td data-title="Trainer">Mark E. Casse</td>
      <td data-title="Time">1:45.48 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">El Kabeir</td>
      <td data-title="Jockey">Calvin H. Borel</td>
      <td data-title="Trainer">John P. Terranova, II</td>
      <td data-title="Time">1:44.82 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Tapiture</td>
      <td data-title="Jockey">Jesus Lopez Castanon</td>
      <td data-title="Trainer">Chuck Peery</td>
      <td data-title="Time">1:43.51 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Uncaptured</td>
      <td data-title="Jockey">Miguel Mena</td>
      <td data-title="Trainer">Mark E. Casse</td>
      <td data-title="Time">1:44.97 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Gemologist</td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">1:44.46 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Santiva</td>
      <td data-title="Jockey">Shaun Bridgmohan</td>
      <td data-title="Trainer">Eddie Kenneally</td>
      <td data-title="Time">1:45.31 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Super Saver</td>
      <td data-title="Jockey">Calvin Borel</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">1:42.83 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Beethoven </td>
      <td data-title="Jockey">Calvin Borel</td>
      <td data-title="Trainer">John T. Ward, Jr.</td>
      <td data-title="Time">1:44.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Anak Nakal </td>
      <td data-title="Jockey">Julien Leparoux</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Time">1:43.16 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Tiz Wonderful </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Scott Blasi</td>
      <td data-title="Time">1:42.84 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Private Vow</td>
      <td data-title="Jockey">Shaun Bridgmohan</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Time">1:45.80 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Greater Good </td>
      <td data-title="Jockey">John McKee</td>
      <td data-title="Trainer">Robert E. Holthus</td>
      <td data-title="Time">1:45.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">The Cliff's Edge </td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Time">1:45.50 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Soto </td>
      <td data-title="Jockey">Larry Melancon</td>
      <td data-title="Trainer">Michael W. Dickinson</td>
      <td data-title="Time">1:44.67 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Repent </td>
      <td data-title="Jockey">Tony D'Amico</td>
      <td data-title="Trainer">Kenneth G. McPeek</td>
      <td data-title="Time">1:44.42 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Dollar Bill </td>
      <td data-title="Jockey">Calvin Borel</td>
      <td data-title="Trainer">Dallas Stewart</td>
      <td data-title="Time">1:47.18 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Captain Steve</td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:43.14 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Exploit </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:44.16 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Cape Town </td>
      <td data-title="Jockey">Willie Martinez</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:43.97 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Concerto </td>
      <td data-title="Jockey">Carlos Marquez, Jr.</td>
      <td data-title="Trainer">John Tammaro III</td>
      <td data-title="Time">1:46.91 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Ide </td>
      <td data-title="Jockey">Craig Perret</td>
      <td data-title="Trainer">Peter M. Vestal</td>
      <td data-title="Time">1:44.31 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Jambalaya Jazz </td>
      <td data-title="Jockey">Sam Maple</td>
      <td data-title="Trainer">John T. Ward, Jr.</td>
      <td data-title="Time">1:46.46 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">War Deputy </td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Anthony L. Reinstedler</td>
      <td data-title="Time">1:46.75 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Wild Gale </td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">Michael J. Doyle</td>
      <td data-title="Time">1:45.64 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Dance Floor</td>
      <td data-title="Jockey">Chris Antley</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:45.21 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Richman </td>
      <td data-title="Jockey">Pat Day</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Time">1:45.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Grand Canyon</td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:44.60 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}