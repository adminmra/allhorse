<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Autumn Stakes Past Winners">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Persian King </td>
      <td data-title="Jockey">Pierre-Charles Boudot</td>
      <td data-title="Trainer">Andre Fabre</td>
      <td data-title="Time">1:37.35 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Ghaiyyath </td>
      <td data-title="Jockey">William Buick</td>
      <td data-title="Trainer">Charlie Appleby</td>
      <td data-title="Time">1:35.92 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Best Solution </td>
      <td data-title="Jockey">William Carson </td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Time">1:37.15 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Gifted Master </td>
      <td data-title="Jockey">William Buick</td>
      <td data-title="Trainer">Hugo Palmer </td>
      <td data-title="Time">1:39.24 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Commemorative </td>
      <td data-title="Jockey">James Doyle</td>
      <td data-title="Trainer">Charles Hills </td>
      <td data-title="Time">1:37.91 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Kingston Hill</td>
      <td data-title="Jockey">Andrea Atzeni</td>
      <td data-title="Trainer">Roger Varian</td>
      <td data-title="Time">1:39.07 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Trading Leather</td>
      <td data-title="Jockey">Kevin Manning</td>
      <td data-title="Trainer">Jim Bolger</td>
      <td data-title="Time">1:37.83 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Rockinante </td>
      <td data-title="Jockey">Richard Hughes</td>
      <td data-title="Trainer">Richard Hannon Sr.</td>
      <td data-title="Time">1:37.71 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Abjer </td>
      <td data-title="Jockey">Richard Hills</td>
      <td data-title="Trainer">Clive Brittain</td>
      <td data-title="Time">1:44.84 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Morana </td>
      <td data-title="Jockey">Alan Munro</td>
      <td data-title="Trainer">Peter Chapple-Hyam</td>
      <td data-title="Time">1:43.51 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Kite Wood </td>
      <td data-title="Jockey">Philip Robinson</td>
      <td data-title="Trainer">Michael Jarvis</td>
      <td data-title="Time">1:42.42 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Ibn Khaldun</td>
      <td data-title="Jockey">Frankie Dettori</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Time">1:44.24 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Caldra </td>
      <td data-title="Jockey">Declan McDonogh</td>
      <td data-title="Trainer">Sylvester Kirk </td>
      <td data-title="Time">1:43.56 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Blitzkrieg[d]</td>
      <td data-title="Jockey">Seb Sanders</td>
      <td data-title="Trainer">Vince Smith </td>
      <td data-title="Time">1:40.90 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Fantastic View </td>
      <td data-title="Jockey">Pat Dobbs</td>
      <td data-title="Trainer">Richard Hannon Sr.</td>
      <td data-title="Time">1:45.55 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Big Bad Bob </td>
      <td data-title="Jockey">Pat Eddery</td>
      <td data-title="Trainer">John Dunlop</td>
      <td data-title="Time">1:42.52 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Fight Your Corner </td>
      <td data-title="Jockey">Kevin Darley</td>
      <td data-title="Trainer">Mark Johnston</td>
      <td data-title="Time">1:44.59 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Nayef</td>
      <td data-title="Jockey">Richard Hills</td>
      <td data-title="Trainer">Marcus Tregoning</td>
      <td data-title="Time">1:49.79 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">French Fellow[b]</td>
      <td data-title="Jockey">Pat Eddery</td>
      <td data-title="Trainer">Tim Easterby</td>
      <td data-title="Time">1:46.86 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Daliapour</td>
      <td data-title="Jockey">Olivier Peslier</td>
      <td data-title="Trainer">Luca Cumani</td>
      <td data-title="Time">1:48.32 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Dr Fong</td>
      <td data-title="Jockey">Kieren Fallon</td>
      <td data-title="Trainer">Henry Cecil</td>
      <td data-title="Time">1:49.31 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">High Roller </td>
      <td data-title="Jockey">Pat Eddery</td>
      <td data-title="Trainer">Henry Cecil</td>
      <td data-title="Time">1:44.91 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Beauchamp King</td>
      <td data-title="Jockey">John Reid</td>
      <td data-title="Trainer">John Dunlop</td>
      <td data-title="Time">1:48.16 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Presenting </td>
      <td data-title="Jockey">Frankie Dettori</td>
      <td data-title="Trainer">John Gosden</td>
      <td data-title="Time">1:41.86 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner"><em>no race</em>[a]</td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Taos </td>
      <td data-title="Jockey">Steve Cauthen</td>
      <td data-title="Trainer">John Gosden</td>
      <td data-title="Time">1:43.34 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Ninja Dancer </td>
      <td data-title="Jockey">Bruce Raymond </td>
      <td data-title="Trainer">Julie Cecil </td>
      <td data-title="Time">1:45.28 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Sea Level </td>
      <td data-title="Jockey">Steve Cauthen</td>
      <td data-title="Trainer">Barry Hills</td>
      <td data-title="Time">1:42.08 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Noble Patriarch </td>
      <td data-title="Jockey">Willie Carson</td>
      <td data-title="Trainer">John Dunlop</td>
      <td data-title="Time">1:43.91 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Nashwan</td>
      <td data-title="Jockey">Willie Carson</td>
      <td data-title="Trainer">Dick Hern</td>
      <td data-title="Time">1:47.29 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner"><em>no race</em></td>
      <td data-title="Jockey">&nbsp;</td>
      <td data-title="Trainer">&nbsp;</td>
      <td data-title="Time">&nbsp;</td>
    </tr>
  </tbody>
  </table>

</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}