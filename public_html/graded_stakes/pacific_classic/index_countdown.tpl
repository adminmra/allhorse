
{php}
$startDate = strtotime("17 March 2015 12:00:00");
$endDate  = strtotime("22 August 2015 22:00:00");
{/php}

<div class="countdown margin-bottom-30">
<div class="headline"><h2>The Pacific Classic</h2></div>

<div class="panel">
	<div class="race">{* <span>Race:</span> *} <span>The $1 Million <a href="/pacific-classic">Pacific Classic</a> is Del Mar's richest race.</span></div>
	<div class="info"> <p><span>What we like:</span> Watch many of the country's top horses compete at the classic distance of 1 1/4 miles in the 25th running of Del Mar's signature event.</p><p>The Pacific Classic will anchor a stakes triple header program that includes the 7-furlong Pat O'Brien Stakes and the Del Mar Mile on turf, both Gr. II events for 3-year-olds and up.</p></div>
</div>

<div class="clock">
  <div class="item clock_days">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>      
     <div class="topLayer"><canvas id="canvas_days" width="188" height="188"> </canvas></div>      
      <div class="text">
        <p class="val">0</p>
        <p class="time type_days">Days</p>
      </div>
  </div>

  <div class="item clock_hours">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>
      <div class="topLayer"> <canvas id="canvas_hours" width="188" height="188"> </canvas></div>
      <div class="text">
      	<p class="val">0</p>
        <p class="time type_hours">Hours</p>
      </div>
  </div>  

  <div class="item clock_minutes">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>
      <div class="topLayer"><canvas id="canvas_minutes" width="188" height="188"> </canvas></div>
      <div class="text">
        <p class="val">0</p>
        <p class="time type_minutes">Minutes</p>
      </div>
  </div>
  
  <div class="item clock_seconds">
    <div class="bgLayer"><canvas class="bgClock" width="188" height="188"> </canvas></div>
      <div class="topLayer"><canvas id="canvas_seconds" width="188" height="188"> </canvas></div>     
      <div class="text">
        <p class="val">0</p>
        <p class="time type_seconds">Seconds</p>
      </div>
  </div>
</div>

<br /><br /><br /><br /><br /><!-- delete these if you use/uncomment the button below -->
<!--<div class="blockfooter"><a class="btn btn-primary" href="/travers-stakes" target="_blank" rel="nofollow">Learn More <i class="fa fa-angle-right"></i></a></div>
</div><!-- end/countdown -->


<!-- Initialize the countdown -->
{literal}
<script type="text/javascript">
$(document).ready(function(){	
JBCountDown({ secondsColor : "#ffdc50", secondsGlow  : "none", minutesColor : "#9cdb7d", minutesGlow  : "none", hoursColor : "#378cff", hoursGlow    : "none",  daysColor    : "#ff6565", daysGlow     : "none", startDate   : "{/literal}{php} echo $startDate; {/php}{literal}", endDate : "{/literal}{php} echo $endDate; {/php}{literal}", now : "{/literal}{php} echo strtotime('now'); {/php}{literal}" });
 });
</script>
{/literal}


