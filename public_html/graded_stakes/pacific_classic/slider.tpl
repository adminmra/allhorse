<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/graded-stakes/pacific-classic/pacific-classic-off-track-betting.jpg" alt="Pacific Classic  Off Track Betting - OTB" />
    <figure class="rsCaption" data-move-effect="left" data-speed="200">2014 Pacific Classic Winner Shared Belief</figure>
   
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/graded-stakes/pacific-classic/pacific-classic-horse-betting.jpg" alt="Pacific Classic  Horse Betting" />
    <figure class="rsCaption"> The beautiful Del Mar racetrack</figure>
   </div>

   
  </div>
 </div>
