<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th  width="70px">Age</th>
        <th>Jockey</th>
        <th>Trainer</th>                
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Arles (FR)</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Nik Juarez </td>
      <td data-title="Trainer">Graham Motion</td>
      <td data-title="Time">2:28.65 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Evidently </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Antonio Gallardo </td>
      <td data-title="Trainer">Roy S. Lerman </td>
      <td data-title="Time">2:34.50 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Goldy Espony</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Kendrick Carmouche</td>
      <td data-title="Trainer">Chad C. Brown</td>
      <td data-title="Time">2:31.10 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Aigue Marine</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Christophe Clement</td>
      <td data-title="Time">2:35.37 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Inimitable Romanee </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Junior Alvarado</td>
      <td data-title="Trainer">Christophe Clement</td>
      <td data-title="Time">2:34.28 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Starformer </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Time">2:33.13 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Hit It Rich </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Shug McGaughey</td>
      <td data-title="Time">2:30.39 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Mekong Melody </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Roger L. Attfield</td>
      <td data-title="Time">2:30.74 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Criticism </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Thomas Albertrani</td>
      <td data-title="Time">2:34.98 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Criticism </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Thomas Albertrani</td>
      <td data-title="Time">2:29.76 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Dalvina </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Cornelio Velásquez</td>
      <td data-title="Trainer">Edward A. L. Dunlop</td>
      <td data-title="Time">2:34.35 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Safari Queen </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Chris DeCarlo</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">2:30.93 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Olaya </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">H. Graham Motion</td>
      <td data-title="Time">2:30.20 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Eleusis </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">Jean-Claude Rouget</td>
      <td data-title="Time">2:31.40 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Spice Island </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Victor Carrero</td>
      <td data-title="Trainer">John Pregman, Jr.</td>
      <td data-title="Time">2:32.40 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Uriah </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Norberto Arroyo, Jr.</td>
      <td data-title="Trainer">Harro Remmert</td>
      <td data-title="Time">2:42.40 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Queue </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jose Espinoza</td>
      <td data-title="Trainer">Vincent Blengs</td>
      <td data-title="Time">2:29.20 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Moonlady </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Chris DeCarlo</td>
      <td data-title="Trainer">Harro Remmert</td>
      <td data-title="Time">2:17.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Midnight Line </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Neil D. Drysdale</td>
      <td data-title="Time">2:29.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Coretta</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">Christophe Clement</td>
      <td data-title="Time">2:29.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Yokama </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Time">2:31.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Sweetzie </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jorge F. Chavez</td>
      <td data-title="Trainer">Rita Nash</td>
      <td data-title="Time">2:16.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Ampulla </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Shane Sellers</td>
      <td data-title="Trainer">Patrick B. Byrne</td>
      <td data-title="Time">2:30.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Yenda </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">2:37.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Market Booster </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Mike Luzzi</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">2:31.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Trampoli </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Christophe Clement</td>
      <td data-title="Time">2:31.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Villandry </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Andre Fabre</td>
      <td data-title="Time">2:29.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Shaima </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Lanfranco Dettori</td>
      <td data-title="Trainer">Luca Cumani</td>
      <td data-title="Time">2:31.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Rigamajig </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jorge F. Chavez</td>
      <td data-title="Trainer">Richard A. DeStasio</td>
      <td data-title="Time">2:29.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Peinture Bleue </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">Andre Fabre</td>
      <td data-title="Time">2:29.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Warfie </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Herb McCauley</td>
      <td data-title="Trainer">Nick Zito</td>
      <td data-title="Time">2:14.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Dancing All Night </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">Claude R. McGaughey III</td>
      <td data-title="Time">2:34.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Stardusk </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jean Cruguet</td>
      <td data-title="Trainer">Philip G. Johnson</td>
      <td data-title="Time">2:30.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Dismasted </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jean-Luc Samyn</td>
      <td data-title="Trainer">Philip G. Johnson</td>
      <td data-title="Time">2:30.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Videogenic </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jean Cruguet</td>
      <td data-title="Trainer">Gasper S. Moschera</td>
      <td data-title="Time">2:29.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Heron Cove </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jean Cruguet</td>
      <td data-title="Trainer">Kay Jensen</td>
      <td data-title="Time">2:32.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Hush Dear </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jean-Luc Samyn</td>
      <td data-title="Trainer">J. Elliott Burch</td>
      <td data-title="Time">2:34.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Hush Dear </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eric Beitia</td>
      <td data-title="Trainer">J. Elliott Burch</td>
      <td data-title="Time">2:31.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Euphrosyne </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Richard Migliore</td>
      <td data-title="Trainer">Stephen A. DiMauro</td>
      <td data-title="Time">2:33.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">The Very One</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">Stephen A. DiMauro</td>
      <td data-title="Time">2:35.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Flitalong </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Ramon I. Encinas</td>
      <td data-title="Trainer">Angel Penna, Sr.</td>
      <td data-title="Time">2:31.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Terpsichorist </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">Woody Stephens</td>
      <td data-title="Time">2:34.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Pearl Necklace</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Ruben Hernandez</td>
      <td data-title="Trainer">Roger Laurin</td>
      <td data-title="Time">1:43.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Javamine </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">MacKenzie Miller</td>
      <td data-title="Time">1:41.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Slip Screen </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">George Intelisano Jr.</td>
      <td data-title="Trainer">Lucien Laurin</td>
      <td data-title="Time">1:42.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">D. O. Lady </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Miguel A. Rivera</td>
      <td data-title="Trainer">Vincent J. Cincotta</td>
      <td data-title="Time">1:43.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Lie Low </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">MacKenzie Miller</td>
      <td data-title="Time">1:42.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Tuerta </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jacinto Vasquez</td>
      <td data-title="Trainer">Frank Y. Whiteley, Jr.</td>
      <td data-title="Time">1:43.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Primsie </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Frank Catrone</td>
      <td data-title="Time">1:37.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Twixt </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Robert Woodhouse</td>
      <td data-title="Trainer">Katharine Voss</td>
      <td data-title="Time">1:39.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Rudo Bird </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">Angel Penna, Sr.</td>
      <td data-title="Time">2:03.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Larceny Kid </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Chuck Baltazar</td>
      <td data-title="Trainer">MacKenzie Miller</td>
      <td data-title="Time">2:02.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Mongo's Pride </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Carlos Marquez</td>
      <td data-title="Trainer">Don Combs</td>
      <td data-title="Time">2:01.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">The University </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">John M. Gaver, Sr.</td>
      <td data-title="Time">2:17.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Red Reality </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">MacKenzie Miller</td>
      <td data-title="Time">2:16.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Ruth's Rullah </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">John P. Campo</td>
      <td data-title="Time">2:14.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Flit-To </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Manuel Ycaza</td>
      <td data-title="Trainer">James P. Conway</td>
      <td data-title="Time">2:15.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Munden Point </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Bobby Ussery</td>
      <td data-title="Trainer">Ira Hanford</td>
      <td data-title="Time">1:54.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1967 </td>
      <td data-title="Winner">Assagai</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Braulio Baeza</td>
      <td data-title="Trainer">MacKenzie Miller</td>
      <td data-title="Time">1:54.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Paoluccio </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Heliodoro Gustines</td>
      <td data-title="Trainer">John M. Gaver, Sr.</td>
      <td data-title="Time">1:58.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1966 </td>
      <td data-title="Winner">Rego </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Hedley Woodhouse</td>
      <td data-title="Trainer">James P. Conway</td>
      <td data-title="Time">1:59.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1965 </td>
      <td data-title="Winner">Parka </td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">Walter Blum</td>
      <td data-title="Trainer">Warren A. Croll, Jr.</td>
      <td data-title="Time">1:54.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1964 </td>
      <td data-title="Winner">Parka </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Walter Blum</td>
      <td data-title="Trainer">Warren A. Croll, Jr.</td>
      <td data-title="Time">1:54.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1963 </td>
      <td data-title="Winner">David K. </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Burley Parke</td>
      <td data-title="Time">1:54.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">The Axe II</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">John M. Gaver, Sr.</td>
      <td data-title="Time">2:15.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1962 </td>
      <td data-title="Winner">Irish Dandy </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">John Ruane</td>
      <td data-title="Trainer"><em>not found</em></td>
      <td data-title="Time">2:15.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1961 </td>
      <td data-title="Winner">Wise Ship </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Heliodoro Gustines</td>
      <td data-title="Trainer">Jake Byer</td>
      <td data-title="Time">1:58.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1960 </td>
      <td data-title="Winner">El Espectador </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Braulio Baeza</td>
      <td data-title="Trainer">Jack Weipert</td>
      <td data-title="Time">2:16.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1959 </td>
      <td data-title="Winner">Tudor Era </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Bill Hartack</td>
      <td data-title="Trainer">Sidney Jacobs</td>
      <td data-title="Time">1:54.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1959 </td>
      <td data-title="Winner">One Eyed King </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Woody Stevens</td>
      <td data-title="Time">1:54.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1958 </td>
      <td data-title="Winner">Beau Diable </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Hedley Woodhouse</td>
      <td data-title="Trainer">George P. Odom</td>
      <td data-title="Time">2:43.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1957 </td>
      <td data-title="Winner">Promethean </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">John Ruane</td>
      <td data-title="Trainer">George P. Odom</td>
      <td data-title="Time">2:44.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1956 </td>
      <td data-title="Winner">Third Brother </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Ismael Valenzuela</td>
      <td data-title="Trainer">Casey Hayes</td>
      <td data-title="Time">2:44.00</td>
    </tr>
  </tbody>
</table>

</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}