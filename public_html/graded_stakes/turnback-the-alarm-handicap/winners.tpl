<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th width="70px">Age</th>        
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Teresa Z </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Trevor McCarthy</td>
      <td data-title="Trainer">Anthony R. Margotta, Jr</td>
      <td data-title="Time">1:51.68 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Eskenformoney </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Time">1:43.12 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Lewis Bay </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Irad Ortiz, Jr. </td>
      <td data-title="Trainer">Chad Brown</td>
      <td data-title="Time">1:42.09 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">America </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Christopher P. DeCarlo </td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Time">1:41.96 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Dame Dorothy </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">1:43.50 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Stanwyck </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">John Shirreffs</td>
      <td data-title="Time">1:42.62 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Afleeting Lady </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">Dale L. Romans</td>
      <td data-title="Time">1:42.17 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Arena Elvira </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Junior Alvarado</td>
      <td data-title="Trainer">Bill Mott</td>
      <td data-title="Time">1:51.59 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Our Khrysty </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Ramon Dominguez</td>
      <td data-title="Trainer">Todd Beattie</td>
      <td data-title="Time">1:43.21 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Unbridled Belle </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Ramon Dominguez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Time">1:50.20 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Altesse </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Claude R. McGaughey III</td>
      <td data-title="Time">1:50.02 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Sugar Shake </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:50.02 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Miss Shop </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Raul Rojas</td>
      <td data-title="Trainer">H. Allen Jerkens</td>
      <td data-title="Time">1:50.93 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Indian Vale </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Time">1:49.83 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Personal Legend </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:51.27 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Pocus Hocus </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">James A. Jerkens</td>
      <td data-title="Time">1:50.67 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Svea Dahl </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Richard Migliore</td>
      <td data-title="Trainer">Bruce N. Levine</td>
      <td data-title="Time">1:50.42 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Rochelle's Terms </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Robbie Davis</td>
      <td data-title="Trainer">Howard M. Tesher</td>
      <td data-title="Time">1:51.19 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Atelier </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Claude R. McGaughey III</td>
      <td data-title="Time">1:48.95 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Belle Cherie </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Philip G. Johnson</td>
      <td data-title="Time">1:50.03 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Snit </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Barclay Tagg</td>
      <td data-title="Time">1:51.30 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Mil Kilates </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Joe Bravo</td>
      <td data-title="Trainer">Alfredo Callejas</td>
      <td data-title="Time">1:49.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Shoop </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Gasper Moschera</td>
      <td data-title="Time">1:51.35 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Incinerate </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Filiberto Leon</td>
      <td data-title="Trainer">H. Allen Jerkens</td>
      <td data-title="Time">1:48.89 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}