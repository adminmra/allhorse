<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th width="70px">Age</th>        
        <th>Jockey</th>
        <th>Trainer</th>      
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td class="Year">2018 </td>
      <td class="Winner">Accelerate</td>
      <td class="Age">5 </td>
      <td class="Jockey">Victor Espinoza</td>
      <td class="Trainer">John W. Sadler</td>
      <td class="Time">1:50.58 </td>
    </tr>
    <tr>
      <td class="Year">2017 </td>
      <td class="Winner">Midnight Storm </td>
      <td class="Age">5 </td>
      <td class="Jockey">Rafael Bajarano</td>
      <td class="Trainer">Philip D'Amato</td>
      <td class="Time">1:40.65 </td>
    </tr>
    <tr>
      <td class="Year">2016 </td>
      <td class="Winner">California Chrome</td>
      <td class="Age">5 </td>
      <td class="Jockey">Victor Espinoza</td>
      <td class="Trainer">Art Sherman</td>
      <td class="Time">1:43.39 </td>
    </tr>
    <tr>
      <td class="Year">2015 </td>
      <td class="Winner">Hoppertunity</td>
      <td class="Age">4 </td>
      <td class="Jockey">Martin Garcia</td>
      <td class="Trainer">Bob Baffert</td>
      <td class="Time">1:41.69 </td>
    </tr>
    <tr>
      <td class="Year">2014 </td>
      <td class="Winner">Blueskiesnrainbows</td>
      <td class="Age">5 </td>
      <td class="Jockey">Martin A. Pedroza</td>
      <td class="Trainer">Jerry Hollendorfer</td>
      <td class="Time">1:43.17 </td>
    </tr>
    <tr>
      <td class="Year">2013 </td>
      <td class="Winner">Coil</td>
      <td class="Age">5 </td>
      <td class="Jockey">Martin Garcia</td>
      <td class="Trainer">Bob Baffert</td>
      <td class="Time">1:42.52 </td>
    </tr>
    <tr>
      <td class="Year">2012 </td>
      <td class="Winner">Uh Oh Bango</td>
      <td class="Age">5 </td>
      <td class="Jockey">Mike E. Smith</td>
      <td class="Trainer">R. Kory Owens</td>
      <td class="Time">1:41.44 </td>
    </tr>
    <tr>
      <td class="Year">2011 </td>
      <td class="Winner">Aggie Engineer </td>
      <td class="Age">6 </td>
      <td class="Jockey">Joe Talamo</td>
      <td class="Trainer">Patrick Gallagher</td>
      <td class="Time">1:40.94 </td>
    </tr>
    <tr>
      <td class="Year">2010 </td>
      <td class="Winner">Neko Bay </td>
      <td class="Age">7 </td>
      <td class="Jockey">Mike E. Smith</td>
      <td class="Trainer">John Shirreffs</td>
      <td class="Time">1:42.95 </td>
    </tr>
    <tr>
      <td class="Year">2009 </td>
      <td class="Winner">Cowboy Cal </td>
      <td class="Age">4 </td>
      <td class="Jockey">John Velazquez</td>
      <td class="Trainer">Todd A. Pletcher</td>
      <td class="Time">1:41.26 </td>
    </tr>
    <tr>
      <td class="Year">2008 </td>
      <td class="Winner">Zappa </td>
      <td class="Age">6 </td>
      <td class="Jockey">Joel Rosario</td>
      <td class="Trainer">John W. Sadler</td>
      <td class="Time">1:39.58 </td>
    </tr>
    <tr>
      <td class="Year">2007 </td>
      <td class="Winner">Dixie Meister </td>
      <td class="Age">5 </td>
      <td class="Jockey">David R. Flores</td>
      <td class="Trainer">Julio C. Canani</td>
      <td class="Time">1:43.18 </td>
    </tr>
    <tr>
      <td class="Year">2006 </td>
      <td class="Winner">High Limit </td>
      <td class="Age">4 </td>
      <td class="Jockey">Pat Valenzuela</td>
      <td class="Trainer">Robert J. Frankel</td>
      <td class="Time">1:43.64 </td>
    </tr>
    <tr>
      <td class="Year">2005 </td>
      <td class="Winner">Congrats </td>
      <td class="Age">5 </td>
      <td class="Jockey">Tyler Baze</td>
      <td class="Trainer">Richard E. Mandella</td>
      <td class="Time">1:41.97 </td>
    </tr>
    <tr>
      <td class="Year">2004 </td>
      <td class="Winner">Star Cross </td>
      <td class="Age">7 </td>
      <td class="Jockey">Victor Espinoza</td>
      <td class="Trainer">Darrell Vienna</td>
      <td class="Time">1:42.22 </td>
    </tr>
    <tr>
      <td class="Year">2003 </td>
      <td class="Winner">Congaree</td>
      <td class="Age">5 </td>
      <td class="Jockey">Jerry D. Bailey</td>
      <td class="Trainer">Bob Baffert</td>
      <td class="Time">1:41.04 </td>
    </tr>
    <tr>
      <td class="Year">2002 </td>
      <td class="Winner">Wooden Phone </td>
      <td class="Age">5 </td>
      <td class="Jockey">David R. Flores</td>
      <td class="Trainer">Bob Baffert</td>
      <td class="Time">1:41.83 </td>
    </tr>
    <tr>
      <td class="Year">2001 </td>
      <td class="Winner">Freedom Crest </td>
      <td class="Age">5 </td>
      <td class="Jockey">Gary Stevens</td>
      <td class="Trainer">Richard Baltas</td>
      <td class="Time">1:41.94 </td>
    </tr>
    <tr>
      <td class="Year">2000 </td>
      <td class="Winner">Dixie Dot Com </td>
      <td class="Age">5 </td>
      <td class="Jockey">Pat Valenzuela</td>
      <td class="Trainer">William Morey</td>
      <td class="Time">1:40.95 </td>
    </tr>
    <tr>
      <td class="Year">1999 </td>
      <td class="Winner">Silver Charm</td>
      <td class="Age">5 </td>
      <td class="Jockey">Gary Stevens</td>
      <td class="Trainer">Bob Baffert</td>
      <td class="Time">1:41.78 </td>
    </tr>
    <tr>
      <td class="Year">1998 </td>
      <td class="Winner">Hal's Pal </td>
      <td class="Age">5 </td>
      <td class="Jockey">Brice Blanc</td>
      <td class="Trainer">Ben D. A. Cecil</td>
      <td class="Time">1:41.89 </td>
    </tr>
    <tr>
      <td class="Year">1997 </td>
      <td class="Winner">Kingdom Found </td>
      <td class="Age">7 </td>
      <td class="Jockey">Gary Stevens</td>
      <td class="Trainer">Rafael Becerra</td>
      <td class="Time">1:40.74 </td>
    </tr>
    <tr>
      <td class="Year">1996 </td>
      <td class="Winner">Alphabet Soup</td>
      <td class="Age">5 </td>
      <td class="Jockey">Chris Antley</td>
      <td class="Trainer">David Hofmans</td>
      <td class="Time">1:41.66 </td>
    </tr>
    <tr>
      <td class="Year">1995 </td>
      <td class="Winner">Del Mar Dennis</td>
      <td class="Age">5 </td>
      <td class="Jockey">Alex Solis</td>
      <td class="Trainer">J. Paco Gonzalez</td>
      <td class="Time">1:41.23 </td>
    </tr>
    <tr>
      <td class="Year">1994 </td>
      <td class="Winner">Hill Pass </td>
      <td class="Age">5 </td>
      <td class="Jockey">Chris McCarron</td>
      <td class="Trainer">Jack Van Berg</td>
      <td class="Time">1:41.00 </td>
    </tr>
    <tr>
      <td class="Year">1993 </td>
      <td class="Winner">Jovial </td>
      <td class="Age">6 </td>
      <td class="Jockey">Mickey Walls</td>
      <td class="Trainer">Bruce L. Jackson</td>
      <td class="Time">1:41.94 </td>
    </tr>
    <tr>
      <td class="Year">1992 </td>
      <td class="Winner">Twilight Agenda</td>
      <td class="Age">6 </td>
      <td class="Jockey">Kent Desormeaux</td>
      <td class="Trainer">D. Wayne Lukas</td>
      <td class="Time">1:42.32 </td>
    </tr>
    <tr>
      <td class="Year">1991 </td>
      <td class="Winner">Farma Way</td>
      <td class="Age">4 </td>
      <td class="Jockey">Gary Stevens</td>
      <td class="Trainer">D. Wayne Lukas</td>
      <td class="Time">1:40.80 </td>
    </tr>
    <tr>
      <td class="Year">1990 </td>
      <td class="Winner">Criminal Type</td>
      <td class="Age">5 </td>
      <td class="Jockey">Chris McCarron</td>
      <td class="Trainer">D. Wayne Lukas</td>
      <td class="Time">1:42.40 </td>
    </tr>
    <tr>
      <td class="Year">1989 </td>
      <td class="Winner">On The Line </td>
      <td class="Age">5 </td>
      <td class="Jockey">Gary Stevens</td>
      <td class="Trainer">D. Wayne Lukas</td>
      <td class="Time">1:41.00 </td>
    </tr>
    <tr>
      <td class="Year">1988 </td>
      <td class="Winner">Super Diamond </td>
      <td class="Age">8 </td>
      <td class="Jockey">Laffit Pincay, Jr.</td>
      <td class="Trainer">Edwin J. Gregson</td>
      <td class="Time">1:43.00 </td>
    </tr>
    <tr>
      <td class="Year">1987 </td>
      <td class="Winner">Epidaurus </td>
      <td class="Age">5 </td>
      <td class="Jockey">Gary Baze</td>
      <td class="Trainer">Charles E. Whittingham</td>
      <td class="Time">1:42.40 </td>
    </tr>
    <tr>
      <td class="Year">1986 </td>
      <td class="Winner">Precisionist</td>
      <td class="Age">5 </td>
      <td class="Jockey">Chris McCarron</td>
      <td class="Trainer">L. Ross Fenstermaker</td>
      <td class="Time">1:41.20 </td>
    </tr>
    <tr>
      <td class="Year">1985 </td>
      <td class="Winner">Hula Blaze </td>
      <td class="Age">5 </td>
      <td class="Jockey">Pat Valenzuela</td>
      <td class="Trainer">Noble Threewitt</td>
      <td class="Time">1:42.00 </td>
    </tr>
    <tr>
      <td class="Year">1984 </td>
      <td class="Winner">Danebo </td>
      <td class="Age">5 </td>
      <td class="Jockey">Laffit Pincay, Jr.</td>
      <td class="Trainer">Laz Barrera</td>
      <td class="Time">1:41.80 </td>
    </tr>
    <tr>
      <td class="Year">1983 </td>
      <td class="Winner">Regal Falcon </td>
      <td class="Age">5 </td>
      <td class="Jockey">Eddie Delahoussaye</td>
      <td class="Trainer">Richard E. Mandella</td>
      <td class="Time">1:43.20 </td>
    </tr>
    <tr>
      <td class="Year">1982 </td>
      <td class="Winner">Five Star Flight </td>
      <td class="Age">4 </td>
      <td class="Jockey">Laffit Pincay, Jr.</td>
      <td class="Trainer">Benjamin W. Perkins, Sr.</td>
      <td class="Time">1:40.80 </td>
    </tr>
    <tr>
      <td class="Year">1981 </td>
      <td class="Winner">Flying Paster</td>
      <td class="Age">5 </td>
      <td class="Jockey">Chris McCarron</td>
      <td class="Trainer">Gordon C. Campbell</td>
      <td class="Time">1:41.20 </td>
    </tr>
    <tr>
      <td class="Year">1980 </td>
      <td class="Winner">Valdez </td>
      <td class="Age">4 </td>
      <td class="Jockey">Laffit Pincay, Jr.</td>
      <td class="Trainer">Laz Barrera</td>
      <td class="Time">1:40.20 </td>
    </tr>
    <tr>
      <td class="Year">1979 </td>
      <td class="Winner">Mr. Redoy </td>
      <td class="Age">5 </td>
      <td class="Jockey">Angel Cordero, Jr.</td>
      <td class="Trainer">A. Thomas Doyle</td>
      <td class="Time">1:42.20 </td>
    </tr>
    <tr>
      <td class="Year">1978 </td>
      <td class="Winner">Ancient Title</td>
      <td class="Age">8 </td>
      <td class="Jockey">Darrel McHargue</td>
      <td class="Trainer">Keith L. Stucki, Sr.</td>
      <td class="Time">1:40.20 </td>
    </tr>
    <tr>
      <td class="Year">1977 </td>
      <td class="Winner">Uniformity </td>
      <td class="Age">5 </td>
      <td class="Jockey">Fernando Toro</td>
      <td class="Trainer">Willard L. Proctor</td>
      <td class="Time">1:41.00 </td>
    </tr>
    <tr>
      <td class="Year">1976 </td>
      <td class="Winner">Lightning Mandate </td>
      <td class="Age">5 </td>
      <td class="Jockey">Sandy Hawley</td>
      <td class="Trainer">Gary F. Jones</td>
      <td class="Time">1:48.40 </td>
    </tr>
    <tr>
      <td class="Year">1975 </td>
      <td class="Winner">Okavango </td>
      <td class="Age">5 </td>
      <td class="Jockey">Fernando Toro</td>
      <td class="Trainer">David A. Whiteley</td>
      <td class="Time">1:41.40 </td>
    </tr>
    <tr>
      <td class="Year">1974 </td>
      <td class="Winner">Tri Jet </td>
      <td class="Age">5 </td>
      <td class="Jockey">Bill Shoemaker</td>
      <td class="Trainer">Charles R. Parke</td>
      <td class="Time">1:41.40 </td>
    </tr>
    <tr>
      <td class="Year">1973 </td>
      <td class="Winner">Single Agent </td>
      <td class="Age">5 </td>
      <td class="Jockey">Jerry Lambert</td>
      <td class="Trainer">Wayne B. Stucki</td>
      <td class="Time">1:41.80 </td>
    </tr>
    <tr>
      <td class="Year">1972 </td>
      <td class="Winner">Western Welcome </td>
      <td class="Age">5 </td>
      <td class="Jockey">Laffit Pincay, Jr.</td>
      <td class="Trainer">Junior W. Nicholson</td>
      <td class="Time">1:41.20 </td>
    </tr>
    <tr>
      <td class="Year">1971 </td>
      <td class="Winner">Ack Ack</td>
      <td class="Age">5 </td>
      <td class="Jockey">Bill Shoemaker</td>
      <td class="Trainer">Charles E. Whittingham</td>
      <td class="Time">1:41.40 </td>
    </tr>
    <tr>
      <td class="Year">1970 </td>
      <td class="Winner">Nodouble</td>
      <td class="Age">5 </td>
      <td class="Jockey">Jorge Tejeira</td>
      <td class="Trainer">J. Bert Sonnier</td>
      <td class="Time">1:40.40 </td>
    </tr>
    <tr>
      <td class="Year">1969 </td>
      <td class="Winner">Kings Favor </td>
      <td class="Age">6 </td>
      <td class="Jockey">Jack Leonard</td>
      <td class="Trainer">Darrell Cannon</td>
      <td class="Time">1:45.60 </td>
    </tr>
    <tr>
      <td class="Year">1968 </td>
      <td class="Winner">Kings Favor </td>
      <td class="Age">5 </td>
      <td class="Jockey">Johnny Sellers</td>
      <td class="Trainer">Darrell Cannon</td>
      <td class="Time">1:44.20 </td>
    </tr>
    <tr>
      <td class="Year">1967 </td>
      <td class="Winner">Pretense </td>
      <td class="Age">4 </td>
      <td class="Jockey">Bill Shoemaker</td>
      <td class="Trainer">Charles E. Whittingham</td>
      <td class="Time">1:43.00 </td>
    </tr>
    <tr>
      <td class="Year">1966 </td>
      <td class="Winner">Native Diver</td>
      <td class="Age">7 </td>
      <td class="Jockey">Jerry Lambert</td>
      <td class="Trainer">Buster Millerick</td>
      <td class="Time">1:41.00 </td>
    </tr>
    <tr>
      <td class="Year">1965 </td>
      <td class="Winner">Candy Spots</td>
      <td class="Age">5 </td>
      <td class="Jockey">Bill Shoemaker</td>
      <td class="Trainer">Mesh Tenney</td>
      <td class="Time">1:42.20 </td>
    </tr>
    <tr>
      <td class="Year">1964 </td>
      <td class="Winner">Olden Times</td>
      <td class="Age">6 </td>
      <td class="Jockey">Bill Shoemaker</td>
      <td class="Trainer">Mesh Tenney</td>
      <td class="Time">1:44.40 </td>
    </tr>
    <tr>
      <td class="Year">1963 </td>
      <td class="Winner">Olden Times</td>
      <td class="Age">5 </td>
      <td class="Jockey">Bill Shoemaker</td>
      <td class="Trainer">Mesh Tenney</td>
      <td class="Time">1:42.20 </td>
    </tr>
    <tr>
      <td class="Year">1962 </td>
      <td class="Winner">Micarlo </td>
      <td class="Age">6 </td>
      <td class="Jockey">Angel Valenzuela</td>
      <td class="Trainer">Farrell W. Jones</td>
      <td class="Time">1:47.80 </td>
    </tr>
    <tr>
      <td class="Year">1961 </td>
      <td class="Winner">New Policy</td>
      <td class="Age">4 </td>
      <td class="Jockey">Ismael Valenzuela</td>
      <td class="Trainer">John H. Adams</td>
      <td class="Time">1:41.60 </td>
    </tr>
    <tr>
      <td class="Year">1960 </td>
      <td class="Winner">Fleet Nasrullah </td>
      <td class="Age">5 </td>
      <td class="Jockey">John Longden</td>
      <td class="Trainer">James I. Nazworthy</td>
      <td class="Time">1:41.40 </td>
    </tr>
    <tr>
      <td class="Year">1959 </td>
      <td class="Winner">Tempest II </td>
      <td class="Age">5 </td>
      <td class="Jockey">Bill Shoemaker</td>
      <td class="Trainer">William Molter</td>
      <td class="Time">1:42.60 </td>
    </tr>
    <tr>
      <td class="Year">1958 </td>
      <td class="Winner">Terrang</td>
      <td class="Age">5 </td>
      <td class="Jockey">William Boland</td>
      <td class="Trainer">Carl A. Roles</td>
      <td class="Time">1:41.40 </td>
    </tr>
    <tr>
      <td class="Year">1957 </td>
      <td class="Winner">Battle Dance </td>
      <td class="Age">5 </td>
      <td class="Jockey">George Taniguchi</td>
      <td class="Trainer">Joe Boyce</td>
      <td class="Time">1:42.20 </td>
    </tr>
    <tr>
      <td class="Year">1956 </td>
      <td class="Winner">Bobby Brocato</td>
      <td class="Age">5 </td>
      <td class="Jockey">George Taniguchi</td>
      <td class="Trainer">William Molter</td>
      <td class="Time">1:42.60 </td>
    </tr>
    <tr>
      <td class="Year">1955 </td>
      <td class="Winner">Rejected</td>
      <td class="Age">5 </td>
      <td class="Jockey">Bill Shoemaker</td>
      <td class="Trainer">William J. Hirsch</td>
      <td class="Time">2:04.60 </td>
    </tr>
    <tr>
      <td class="Year">1954 </td>
      <td class="Winner">Phil D. </td>
      <td class="Age">6 </td>
      <td class="Jockey">Merlin Volzke</td>
      <td class="Trainer">T. M. Holt </td>
      <td class="Time">1:41.60 </td>
    </tr>
    <tr>
      <td class="Year">1953 </td>
      <td class="Winner">Moonrush </td>
      <td class="Age">7 </td>
      <td class="Jockey">Ralph Neves</td>
      <td class="Trainer">Willie F. Alvarado</td>
      <td class="Time">1:43.40 </td>
    </tr>
    <tr>
      <td class="Year">1952 </td>
      <td class="Winner">Be Fleet </td>
      <td class="Age">5 </td>
      <td class="Jockey">Johnny Longden</td>
      <td class="Trainer">William Molter</td>
      <td class="Time">1:44.00 </td>
    </tr>
    <tr>
      <td class="Year">1951 </td>
      <td class="Winner">Moonrush </td>
      <td class="Age">5 </td>
      <td class="Jockey">Fred A. Smith</td>
      <td class="Trainer">Willie F. Alvarado</td>
      <td class="Time">1:42.40 </td>
    </tr>
    <tr>
      <td class="Year">1950 </td>
      <td class="Winner">Solidarity </td>
      <td class="Age">5 </td>
      <td class="Jockey">Ralph Neves</td>
      <td class="Trainer">Carl A. Roles</td>
      <td class="Time">1:43.80 </td>
    </tr>
    <tr>
      <td class="Year">1949 </td>
      <td class="Winner">Shim Malone </td>
      <td class="Age">5 </td>
      <td class="Jockey">Ralph Neves</td>
      <td class="Trainer">Maurice W. Breshnen</td>
      <td class="Time">1:45.60 </td>
    </tr>
    <tr>
      <td class="Year">1948 </td>
      <td class="Winner">Olhaverry </td>
      <td class="Age">9 </td>
      <td class="Jockey">Mel Peterson</td>
      <td class="Trainer">Anthony E. Silver</td>
      <td class="Time">1:44.00 </td>
    </tr>
    <tr>
      <td class="Year">1947 </td>
      <td class="Winner">Lets Dance </td>
      <td class="Age">5 </td>
      <td class="Jockey">John Gilbert</td>
      <td class="Trainer">Joseph B. Rosen</td>
      <td class="Time">1:43.60 </td>
    </tr>
    <tr>
      <td class="Year">1946 </td>
      <td class="Winner">Lou-Bre </td>
      <td class="Age">5 </td>
      <td class="Jockey">Robert Permane</td>
      <td class="Trainer">Bud Stotler</td>
      <td class="Time">1:42.40 </td>
    </tr>
    <tr>
      <td class="Year">1945 </td>
      <td class="Winner">Thumbs Up </td>
      <td class="Age">6 </td>
      <td class="Jockey">Johnny Longden</td>
      <td class="Trainer">George M. Odom</td>
      <td class="Time">1:44.60 </td>
    </tr>
    <tr>
      <td class="Year">1941 </td>
      <td class="Winner">Mioland</td>
      <td class="Age">4 </td>
      <td class="Jockey">Leon Haas</td>
      <td class="Trainer">Tom Smith</td>
      <td class="Time">1:51.60 </td>
    </tr>
    <tr>
      <td class="Year">1940 </td>
      <td class="Winner">Don Mike </td>
      <td class="Age">6 </td>
      <td class="Jockey">Lester Balaski</td>
      <td class="Trainer">Lindsay C. Howard</td>
      <td class="Time">1:50.00 </td>
    </tr>
    <tr>
      <td class="Year">1939 </td>
      <td class="Winner">Gosum </td>
      <td class="Age">5 </td>
      <td class="Jockey">Alan Gray</td>
      <td class="Trainer">Graceton Philpot</td>
      <td class="Time">1:59.00 </td>
    </tr>
    <tr>
      <td class="Year">1938 </td>
      <td class="Winner">Sun Egret </td>
      <td class="Age">3 </td>
      <td class="Jockey">John H. Adams</td>
      <td class="Trainer">H. Guy Bedwell</td>
      <td class="Time">1:23.40 </td>
    </tr>
    <tr>
      <td class="Year">1937 </td>
      <td class="Winner">Special Agent </td>
      <td class="Age">5 </td>
      <td class="Jockey">Basil James</td>
      <td class="Trainer">Darrell Cannon</td>
      <td class="Time">1:42.80 </td>
    </tr>
    <tr>
      <td class="Year">1936 </td>
      <td class="Winner">Proclivity </td>
      <td class="Age">3 </td>
      <td class="Jockey">Tommy Luther</td>
      <td class="Trainer">Edward L. Fitzgerald</td>
      <td class="Time">1:12.00 </td>
    </tr>
    <tr>
      <td class="Year">1935 </td>
      <td class="Winner">Bluebeard </td>
      <td class="Age">3 </td>
      <td class="Jockey">Harry Richards</td>
      <td class="Trainer">Clyde Phillips</td>
      <td class="Time">1:12.80 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}