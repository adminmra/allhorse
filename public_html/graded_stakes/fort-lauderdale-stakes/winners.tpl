<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"><thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
         <th width="70px">Age</th> 
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Shining Copper </td>
      <td data-title="Age">8 </td>
      <td data-title="Jockey">Jose L. Ortiz</td>
      <td data-title="Trainer">Michael J. Maker</td>
      <td data-title="Time">1:43.44 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Flatlined </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Joe Bravo</td>
      <td data-title="Trainer">Charles L. Dickey</td>
      <td data-title="Time">1:41.29 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Heart to Heart </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Julien Leparoux</td>
      <td data-title="Trainer">Brian A. Lynch</td>
      <td data-title="Time">1:41.71 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Mshawish </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">1:41.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Summer Front </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Joe Bravo</td>
      <td data-title="Trainer">Christophe Clement</td>
      <td data-title="Time">1:42.24 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Mucho Mas Macho </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Juan Leyva</td>
      <td data-title="Trainer">Henry Collazo</td>
      <td data-title="Time">1:40.39 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Silver Medallion </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">1:43.04 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Little Mike</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Joe Bravo</td>
      <td data-title="Trainer">Allen Iwinski</td>
      <td data-title="Time">1:40.52 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Duke of Mischief </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eibar Coa</td>
      <td data-title="Trainer">David Fawkes</td>
      <td data-title="Time">1:37.70 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Kiss the Kid </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Elvis Trujillo</td>
      <td data-title="Trainer">Amy Tarrant</td>
      <td data-title="Time">1:40.59 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}