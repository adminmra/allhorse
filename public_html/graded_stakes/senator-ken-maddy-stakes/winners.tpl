<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th width="70px">Age</th>        
        <th>Jockey</th>
        <th>Trainer</th>     
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Storm the Hill </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Philip D'Amato</td>
      <td data-title="Time">1:12.13 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Belvoir Bay (GB) </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Flavien Prat</td>
      <td data-title="Trainer">Peter Miller </td>
      <td data-title="Time">0:56.63 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Fair Point </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jose Ortiz</td>
      <td data-title="Trainer">Claude R. McGaughey III</td>
      <td data-title="Time">1:11.75 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Baruta (BRZ) </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Richard E. Mandella</td>
      <td data-title="Time">1:12.96 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Amaranth </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Dennis Carr</td>
      <td data-title="Trainer">O. J. Jauregui</td>
      <td data-title="Time">1:16.07 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Pontchatrain</td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Thomas F. Proctor</td>
      <td data-title="Time">1:12.25 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Broken Dreams </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Thomas F. Proctor</td>
      <td data-title="Time">1:11.81 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Broken Dreams </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Thomas F. Proctor</td>
      <td data-title="Time">1:12.23 </td>
    </tr>
    <tr>
      <td data-title="Year">2010‡ </td>
      <td data-title="Winner">Unzip Me </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Martin F. Jones</td>
      <td data-title="Time">1:08.50 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Gotta Have Her </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Tyler Baze</td>
      <td data-title="Trainer">Jenine Sahadi</td>
      <td data-title="Time">1:12.09 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Queen of the Castle </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Howard L. Zucker</td>
      <td data-title="Time">1:12.84 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Dancing Edie </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Craig Dollase</td>
      <td data-title="Time">1:13.30 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Cambiocorsa </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jon Court</td>
      <td data-title="Trainer">Douglas F. O'Neill</td>
      <td data-title="Time">1:12.02 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Elusive Diva </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Mark Glatt</td>
      <td data-title="Time">1:11.56 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Belleski </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">John W. Sadler</td>
      <td data-title="Time">1:12.86 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Belleski </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">John W. Sadler</td>
      <td data-title="Time">1:12.37 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Rolly Polly </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:12.86 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">A La Reine </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Richard E. Mandella</td>
      <td data-title="Time">1:13.27 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Evening Promise </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Kathy Walsh</td>
      <td data-title="Time">1:13.05 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Hula Queen </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Luis Seglin</td>
      <td data-title="Time">1:13.05 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Dance Parade </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Neil D. Drysdale</td>
      <td data-title="Time">1:13.87 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Madame Pandit </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">Ron McAnally</td>
      <td data-title="Time">1:13.82 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Dixie Pearl </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">Richard E. Mandella</td>
      <td data-title="Time">1:12.33 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Denim Yenem </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Ronald W. Ellis</td>
      <td data-title="Time">1:14.92 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Starolamo </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Kim Lloyd</td>
      <td data-title="Time">1:16.07 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Toussaud</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:14.32 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Bel's Starlet </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Richard E. Mandella</td>
      <td data-title="Time">1:11.63 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Bel's Starlet </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Richard E. Mandella</td>
      <td data-title="Time">1:11.89 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Stylish Star </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">Dan L. Hendricks</td>
      <td data-title="Time">1:12.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Warning Zone </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Rafael Meza</td>
      <td data-title="Trainer">Ron McAnally</td>
      <td data-title="Time">1:12.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Jeanne Jones </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Aaron Gryder</td>
      <td data-title="Trainer">Charles Whittingham</td>
      <td data-title="Time">1:14.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Aberuschka </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:14.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Lichi </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Gary Baze</td>
      <td data-title="Trainer">Gary F. Jones</td>
      <td data-title="Time">1:14.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Shywing </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Jerry M. Fanning</td>
      <td data-title="Time">1:14.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Solva </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Rafael Meza</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:14.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Love Smitten</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Edwin J. Gregson</td>
      <td data-title="Time">1:15.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Irish O'Brien </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Joe Steiner</td>
      <td data-title="Trainer">Hector O. Palma</td>
      <td data-title="Time">1:14.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Lina Cavalieri </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:14.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Matching </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Ray Sibille</td>
      <td data-title="Trainer">Steven L. Morguelan</td>
      <td data-title="Time">1:17.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Maple Tree </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">Richard E. Mandella</td>
      <td data-title="Time">1:15.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Jones Time Machine </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:16.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Kilijaro</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Charles Whittingham</td>
      <td data-title="Time">1:13.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Save Wild Life </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Marco Castaneda</td>
      <td data-title="Trainer">Howard M. Tesher</td>
      <td data-title="Time">1:14.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Great Lady M. </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:13.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Palmistry </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Willard L. Proctor</td>
      <td data-title="Time">1:12.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Wishing Well</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Fernando Toro</td>
      <td data-title="Trainer">Gary F. Jones</td>
      <td data-title="Time">1:12.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Happy Holme </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Warren Stute</td>
      <td data-title="Time">1:14.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Country Queen </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Fernando Toro</td>
      <td data-title="Trainer">Randy Winick</td>
      <td data-title="Time">1:14.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Dancing Femme </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Darrel McHargue</td>
      <td data-title="Trainer">A. Thomas Doyle</td>
      <td data-title="Time">1:13.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">If You Prefer </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Roger E. Clapp</td>
      <td data-title="Time">1:13.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Dancing Liz </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">David Bernstein</td>
      <td data-title="Time">1:12.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">Tizna </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Donald Pierce</td>
      <td data-title="Trainer">Henry Moreno</td>
      <td data-title="Time">1:12.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Impressive Style </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Rudy Rosales</td>
      <td data-title="Trainer">Bill McCormick</td>
      <td data-title="Time">1:13.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">New Moon II </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Charles Whittingham</td>
      <td data-title="Time">1:13.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Street Dancer</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Fernando Toro</td>
      <td data-title="Trainer">John G. Canty</td>
      <td data-title="Time">1:13.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Manta </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Howard Grant</td>
      <td data-title="Trainer">Farrell W. Jones</td>
      <td data-title="Time">1:12.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Ack Ack</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Charles Whittingham</td>
      <td data-title="Time">1:13.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Tell </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Charles Whittingham</td>
      <td data-title="Time">1:13.60 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}