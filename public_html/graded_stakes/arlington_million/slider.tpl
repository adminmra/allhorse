<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/graded-stakes/arlington-million/arlington-million-off-track-betting.jpg" alt="Arlington Million  Off Track Betting - OTB" />
    <figure class="rsCaption" data-move-effect="left" data-speed="200">Thoroughbreds 3-y-o & Up</figure>
   
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/graded-stakes/arlington-million/arlington-million-horse-betting.jpg" alt="Arlington Million  Horse Betting" />
    <figure class="rsCaption">Purse: 	$1,000,000</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/graded-stakes/arlington-million/bet-on-arlington-million.jpg" alt="Bet on Arlington Million"  />
    <figure class="rsCaption">2014 Arlington Stakes Winner Hardest Core</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/graded-stakes/arlington-million/arlington-million-horse-racing.jpg" alt="Arlington Million Horse Racing" />
    <figure class="rsCaption">2014 Arlington Stakes Winner Hardest Core</figure>
   </div>
   
  <div class="rsContent">
    <img class="rsImg" src="/img/racetracks/arlington-park/arlington-park-off-track-betting.jpg" alt="Arlington Park  Off Track Betting - OTB" />
    <figure class="rsCaption" >Arlington International Racecourse
Arlington Heights, Illinois
Inaugurated 	1981</figure>
   
   </div>
     <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/arlington-park/arlington-park-horse-betting.jpg" alt="Arlington Park  Horse Betting" />
    <figure class="rsCaption"></figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/racetracks/arlington-park/bet-on-arlington-park.jpg" alt="Bet on Arlington Park"  />
    <figure class="rsCaption"></figure>
   </div>

   
  </div>
 </div>
