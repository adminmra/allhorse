<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" >
    <thead>
      <tr>
        <th width="75px">Year</th>
        <th>Winner</th>
        <th width="75px">Age</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="90px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2018</td>
      <td data-title="Winner">No Dozing</td>
      <td data-title="Age">5</td>
      <td data-title="Jockey">D. Centeno</td>
      <td data-title="Trainer">A. Delacour</td>
      <td data-title="Time">1:23.81</td>
    </tr>  
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Tom's Ready </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">Dallas Stewart </td>
      <td data-title="Time">1:22.06 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Stallwalkin' Dude </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Irad Ortiz, Jr.</td>
      <td data-title="Trainer">David Jacobson </td>
      <td data-title="Time">1:21.66 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Matrooh </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Abel Castellano, Jr.</td>
      <td data-title="Trainer">Chad C. Brown</td>
      <td data-title="Time">1:21.24 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Salutos Amigos </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Cornelio Velasquez</td>
      <td data-title="Trainer">David Jacobson</td>
      <td data-title="Time">1:21.87 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Clearly Now </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Jose Lezcano</td>
      <td data-title="Trainer">Brian A. Lynch</td>
      <td data-title="Time">1:21.52 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Buffum </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Eddie Castro</td>
      <td data-title="Trainer">Thomas Albertrani</td>
      <td data-title="Time">1:21.68 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Calibrachoa </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jose Lezcano</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">1:21.32 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Bribon </td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd Pletcher</td>
      <td data-title="Time">1:22.36 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Le Gran Cru </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jose Lezcano</td>
      <td data-title="Trainer">Allen Jerkens</td>
      <td data-title="Time">1:22.19 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Lucky Island </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Alan Garcia</td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Time">1:09.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Songster </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Edgar Prado</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Time">1:08.80 </td>
    </tr>
    <tr>
      <td data-title="Year">2006 </td>
      <td data-title="Winner">Tiger </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Eibar Coa</td>
      <td data-title="Trainer">James A. Jerkens</td>
      <td data-title="Time">1:08.49 </td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Uncle Camie </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Richard Migliore</td>
      <td data-title="Trainer">Michael E. Hushion</td>
      <td data-title="Time">1:08.67 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Canadian Frontier </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Javier Castellano</td>
      <td data-title="Trainer">Stanley M. Hough</td>
      <td data-title="Time">1:08.97 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Shake You Down </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Mike Luzzi</td>
      <td data-title="Trainer">Scott A. Lake</td>
      <td data-title="Time">1:08.47 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Left Bank</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Todd A. Pletcher</td>
      <td data-title="Time">1:09.30 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Say Florida Sandy </td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">Joe Bravo</td>
      <td data-title="Trainer">Juan Serey</td>
      <td data-title="Time">1:08.67 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Brutally Frank </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Shaun Bridgmohan</td>
      <td data-title="Trainer">Mitchell Friedman</td>
      <td data-title="Time">1:08.64 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 </td>
      <td data-title="Winner">Kelly Kip</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jean-Luc Samyn</td>
      <td data-title="Trainer">H. Allen Jerkens</td>
      <td data-title="Time">1:07.54 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Kelly Kip</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jean-Luc Samyn</td>
      <td data-title="Trainer">H. Allen Jerkens</td>
      <td data-title="Time">1:07.61 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Punch Line </td>
      <td data-title="Age">7 </td>
      <td data-title="Jockey">Robbie Davis</td>
      <td data-title="Trainer">William Turner, Jr.</td>
      <td data-title="Time">1:08.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Lite The Fuse </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Julie Krone</td>
      <td data-title="Trainer">Richard Dutrow, Sr.</td>
      <td data-title="Time">1:09.51 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Rizzi </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Dale Beckner</td>
      <td data-title="Trainer">H. Allen Jerkens</td>
      <td data-title="Time">1:08.91 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Chief Desire </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Michael E. Hushion</td>
      <td data-title="Time">1:08.76 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Slerp </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jose A. Santos</td>
      <td data-title="Trainer">Robert B. Hess, Jr.</td>
      <td data-title="Time">1:09.17 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Jolies Appeal </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Herb McCauley</td>
      <td data-title="Trainer">Gasper Moschera</td>
      <td data-title="Time">1:09.29 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Rousing Past </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Nick Santagata</td>
      <td data-title="Trainer">Carlos F. Martin</td>
      <td data-title="Time">1:09.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Mr. Nickerson </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Chris Antley</td>
      <td data-title="Trainer">Mark J. Reid</td>
      <td data-title="Time">1:09.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Pok Ta Pok </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Richard Migliore</td>
      <td data-title="Trainer">William Turner, Jr.</td>
      <td data-title="Time">1:09.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">King's Swan</td>
      <td data-title="Age">8 </td>
      <td data-title="Jockey">Chris Antley</td>
      <td data-title="Trainer">Richard Dutrow, Sr.</td>
      <td data-title="Time">1:10.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Pine Tree Lane </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">D. Wayne Lukas</td>
      <td data-title="Time">1:09.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Phone Trick</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jorge Velasquez</td>
      <td data-title="Trainer">Richard Mandella</td>
      <td data-title="Time">1:08.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Rocky Marriage </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:08.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Top Avenger </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Antonio Graell</td>
      <td data-title="Trainer">Jack D. Ludwig</td>
      <td data-title="Time">1:09.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Maudlin </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jerry D. Bailey</td>
      <td data-title="Trainer">Jan H. Nerud</td>
      <td data-title="Time">1:11.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Always Run Lucky </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jimmy Miranda</td>
      <td data-title="Trainer">John P. Campo</td>
      <td data-title="Time">1:09.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Dave's Friend </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Anthony Black</td>
      <td data-title="Trainer">Robert L. Beall</td>
      <td data-title="Time">1:09.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Dave's Friend </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Vince Bracciale, Jr.</td>
      <td data-title="Trainer">Robert L. Beall</td>
      <td data-title="Time">1:09.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Star De Naskra </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jeffrey Fell</td>
      <td data-title="Trainer">Richard D. Ferris</td>
      <td data-title="Time">1:09.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Half High </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Angel Santiago</td>
      <td data-title="Trainer">Edward Coletti</td>
      <td data-title="Time">1:09.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Jaipur's Gem </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jean-Luc Samyn</td>
      <td data-title="Trainer">Philip G. Johnson</td>
      <td data-title="Time">1:09.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Chief Tamanaco </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Angel Cordero, Jr.</td>
      <td data-title="Trainer">James Iselin</td>
      <td data-title="Time">1:09.80</td>
    </tr>
  </tbody>
  </table>

</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}