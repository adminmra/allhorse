{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
{/literal}
<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" zcellspacing="0" border="0" title="Florida Derby Past Winners">
    <thead>
     <caption class="table-title"><h2>Florida Derby Betting | Past Winners</h2></caption>  
     <tr>
        <th width="69px">Year</th>
        <th>Winner</th>
        <th>Jockey</th>
        <th>Trainer</th>
        <th>Owner</th>
        <th width="75px">Time</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-title="Year">2019</td>
        <td data-title="Winner">Maximum Security</td>
        <td data-title="Jockey">Luis Saez</td>
        <td data-title="Trainer">Jason Servis</td>
        <td data-title="Owner">Gary and Mary West</td>
        <td data-title="Time">1:48.86</td>
      </tr>
      <tr>
        <td data-title="Year">2018</td>
        <td data-title="Winner">Audible</td>
        <td data-title="Jockey">John R. Velazquez</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Owner">China Horse Club,<br> Head of Plains Partners LLC,<br>Starlight Racing, WinStar Farm</td>
        <td data-title="Time">1:49.48</td>
      </tr>
      <tr>
        <td data-title="Year">2017</td>
        <td data-title="Winner">Always Dreaming</td>
        <td data-title="Jockey">John R. Velazquez</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Owner">Brooklyn Boyz/Te.Viola Racing</td>
        <td data-title="Time">1:47.47</td>
      </tr>
      <tr>
        <td data-title="Year">2016</td>
        <td data-title="Winner">Nyquist</td>
        <td data-title="Jockey">Mario Gutierrez</td>
        <td data-title="Trainer">Doug O'Neill</td>
        <td data-title="Owner">Reddam Racing</td>
        <td data-title="Time">1:49.11</td>
      </tr>
      <tr>
        <td data-title="Year">2015</td>
        <td data-title="Winner">Materiality</td>
        <td data-title="Jockey">John R. Velazquez</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Owner">Alto Racing</td>
        <td data-title="Time">1:52.30</td>
      </tr>
      <tr>
        <td data-title="Year">2014</td>
        <td data-title="Winner">Constitution</td>
        <td data-title="Jockey">Javier Castellano</td>
        <td data-title="Trainer">Todd Pletcher</td>
        <td data-title="Owner">Winstar Farm</td>
        <td data-title="Time">1:49.17</td>
      </tr>
      <tr>
        <td data-title="Year">2013</td>
        <td data-title="Winner">Orb</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">Claude McGaughey III</td>
        <td data-title="Owner">S. Janney &amp; Phipps Stable</td>
        <td data-title="Time">1:50.87</td>
      </tr>
      <tr>
        <td data-title="Year">2012</td>
        <td data-title="Winner">Take Charge Indy</td>
        <td data-title="Jockey">Calvin Borel</td>
        <td data-title="Trainer">Patrick B. Byrne</td>
        <td data-title="Owner">C &amp; M Sandford</td>
        <td data-title="Time">1:48.79</td>
      </tr>
      <tr>
        <td data-title="Year">2011</td>
        <td data-title="Winner">Dialed In</td>
        <td data-title="Jockey">Julien Leparoux</td>
        <td data-title="Trainer">Nick Zito</td>
        <td data-title="Owner">Robert V. LaPenta</td>
        <td data-title="Time">1:50.74</td>
      </tr>
      <tr>
        <td data-title="Year">2010</td>
        <td data-title="Winner">Ice Box</td>
        <td data-title="Jockey">Jose Lezcano</td>
        <td data-title="Trainer">Nick Zito</td>
        <td data-title="Owner">Robert V. LaPenta</td>
        <td data-title="Time">1:49.19</td>
      </tr>
      <tr>
        <td data-title="Year">2009</td>
        <td data-title="Winner">Quality Road</td>
        <td data-title="Jockey">John Velazquez</td>
        <td data-title="Trainer">James A. Jerkens</td>
        <td data-title="Owner">Edward P. Evans</td>
        <td data-title="Time">1:47.72</td>
      </tr>
      <tr>
        <td data-title="Year">2008</td>
        <td data-title="Winner">Big Brown</td>
        <td data-title="Jockey">Kent Desormeaux</td>
        <td data-title="Trainer">Richard E. Dutrow Jr.</td>
        <td data-title="Owner">IEAH Stables/Paul Pompa Jr.</td>
        <td data-title="Time">1:48.16</td>
      </tr>
      <tr>
        <td data-title="Year">2007</td>
        <td data-title="Winner">Scat Daddy</td>
        <td data-title="Jockey">Edgar Prado</td>
        <td data-title="Trainer">Todd A. Pletcher</td>
        <td data-title="Owner">James Scatuorchio/M. Tabor</td>
        <td data-title="Time">1:49.00</td>
      </tr>
      <tr>
        <td data-title="Year">2006</td>
        <td data-title="Winner">Barbaro</td>
        <td data-title="Jockey">Edgar Prado</td>
        <td data-title="Trainer">Michael Matz</td>
        <td data-title="Owner">Lael Stables</td>
        <td data-title="Time">1:49.01</td>
      </tr>
      <tr>
        <td data-title="Year">2005</td>
        <td data-title="Winner">High Fly</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">Nick Zito</td>
        <td data-title="Owner">Live Oak Plantation</td>
        <td data-title="Time">1:49.43</td>
      </tr>
      <tr>
        <td data-title="Year">2004</td>
        <td data-title="Winner">Friends Lake</td>
        <td data-title="Jockey">Richard Migliore</td>
        <td data-title="Trainer">John C. Kimmel</td>
        <td data-title="Owner">Chester &amp; Mary Broman</td>
        <td data-title="Time">1:51.38</td>
      </tr>
      <tr>
        <td data-title="Year">2003</td>
        <td data-title="Winner">Empire Maker</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">Robert Frankel</td>
        <td data-title="Owner">Juddmonte Farms</td>
        <td data-title="Time">1:49.05</td>
      </tr>
      <tr>
        <td data-title="Year">2002</td>
        <td data-title="Winner">Harlan's Holiday</td>
        <td data-title="Jockey">Edgar Prado</td>
        <td data-title="Trainer">Kenneth McPeek</td>
        <td data-title="Owner">Starlight Stable</td>
        <td data-title="Time">1:48.80</td>
      </tr>
      <tr>
        <td data-title="Year">2001</td>
        <td data-title="Winner">Monarchos</td>
        <td data-title="Jockey">Jorge Chavez</td>
        <td data-title="Trainer">John T. Ward, Jr.</td>
        <td data-title="Owner">John C. Oxley</td>
        <td data-title="Time">1:49.95</td>
      </tr>
      <tr>
        <td data-title="Year">2000</td>
        <td data-title="Winner">Hal's Hope</td>
        <td data-title="Jockey">Roger Velez</td>
        <td data-title="Trainer">Harold J. Rose</td>
        <td data-title="Owner">Rose Family Stable</td>
        <td data-title="Time">1:51.49</td>
      </tr>
      <tr>
        <td data-title="Year">1999</td>
        <td data-title="Winner">Vicar</td>
        <td data-title="Jockey">Shane Sellers</td>
        <td data-title="Trainer">Carl Nafzger</td>
        <td data-title="Owner">James B. Tafel</td>
        <td data-title="Time">1:50.83</td>
      </tr>
      <tr>
        <td data-title="Year">1998</td>
        <td data-title="Winner">Cape Town †</td>
        <td data-title="Jockey">Shane Sellers</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Overbrook Farm</td>
        <td data-title="Time">1:49.21</td>
      </tr>
      <tr>
        <td data-title="Year">1997</td>
        <td data-title="Winner">Captain Bodgit</td>
        <td data-title="Jockey">Alex Solis</td>
        <td data-title="Trainer">Gary Capuano</td>
        <td data-title="Owner">Team Valor</td>
        <td data-title="Time">1:50.60</td>
      </tr>
      <tr>
        <td data-title="Year">1996</td>
        <td data-title="Winner">Unbridled's Song</td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">James T. Ryerson</td>
        <td data-title="Owner">Paraneck Stable</td>
        <td data-title="Time">1:47.85</td>
      </tr>
      <tr>
        <td data-title="Year">1995</td>
        <td data-title="Winner">Thunder Gulch</td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">D. Wayne Lukas</td>
        <td data-title="Owner">Michael Tabor</td>
        <td data-title="Time">1:49.70</td>
      </tr>
      <tr>
        <td data-title="Year">1994</td>
        <td data-title="Winner">Holy Bull</td>
        <td data-title="Jockey">Mike Smith</td>
        <td data-title="Trainer">Warren A. Croll, Jr.</td>
        <td data-title="Owner">Warren A. Croll, Jr.</td>
        <td data-title="Time">1:47.66</td>
      </tr>
      <tr>
        <td data-title="Year">1993</td>
        <td data-title="Winner">Bull Inthe Heather</td>
        <td data-title="Jockey">Wigberto Ramos</td>
        <td data-title="Trainer">Howard M. Tesher</td>
        <td data-title="Owner">Arthur Klein</td>
        <td data-title="Time">1:51.38</td>
      </tr>
      <tr>
        <td data-title="Year">1992</td>
        <td data-title="Winner">Technology</td>
        <td data-title="Jockey">Jerry Bailey</td>
        <td data-title="Trainer">Hubert Hine</td>
        <td data-title="Owner">Scott C. Savin</td>
        <td data-title="Time">1:50.72</td>
      </tr>
      <tr>
        <td data-title="Year">1991</td>
        <td data-title="Winner">Fly So Free</td>
        <td data-title="Jockey">Jose Santos</td>
        <td data-title="Trainer">Scotty Schulhofer</td>
        <td data-title="Owner">Tommy Valando</td>
        <td data-title="Time">1:50.44</td>
      </tr>
      <tr>
        <td data-title="Year">1990</td>
        <td data-title="Winner">Unbridled</td>
        <td data-title="Jockey">Pat Day</td>
        <td data-title="Trainer">Carl Nafzger</td>
        <td data-title="Owner">Genter Stable</td>
        <td data-title="Time">1:52.00</td>
      </tr>
      <tr>
        <td data-title="Year">1989</td>
        <td data-title="Winner">Mercedes Won</td>
        <td data-title="Jockey">Earlie Fires</td>
        <td data-title="Trainer">Arnold Fink</td>
        <td data-title="Owner">Christopher Spencer</td>
        <td data-title="Time">1:49.60</td>
      </tr>
      <tr>
        <td data-title="Year">1988</td>
        <td data-title="Winner">Brian's Time</td>
        <td data-title="Jockey">Randy Romero</td>
        <td data-title="Trainer">John M. Veitch</td>
        <td data-title="Owner">James W. Phillips</td>
        <td data-title="Time">1:49.80</td>
      </tr>
      <tr>
        <td data-title="Year">1987</td>
        <td data-title="Winner">Cryptoclearance</td>
        <td data-title="Jockey">Jose Santos</td>
        <td data-title="Trainer">Scotty Schulhofer</td>
        <td data-title="Owner">Phil Teinowitz</td>
        <td data-title="Time">1:49.60</td>
      </tr>
      <tr>
        <td data-title="Year">1986</td>
        <td data-title="Winner">Snow Chief</td>
        <td data-title="Jockey">Alex Solis</td>
        <td data-title="Trainer">Melvin F. Stute</td>
        <td data-title="Owner">Rochelle/Grinstead</td>
        <td data-title="Time">1:51.80</td>
      </tr>
      <tr>
        <td data-title="Year">1985</td>
        <td data-title="Winner">Proud Truth</td>
        <td data-title="Jockey">Jorge Velasquez</td>
        <td data-title="Trainer">John M. Veitch</td>
        <td data-title="Owner">Darby Dan Farm</td>
        <td data-title="Time">1:50.00</td>
      </tr>
      <tr>
        <td data-title="Year">1984</td>
        <td data-title="Winner">Swale</td>
        <td data-title="Jockey">Laffit Pincay, Jr.</td>
        <td data-title="Trainer">Woody Stephens</td>
        <td data-title="Owner">Claiborne Farm</td>
        <td data-title="Time">1:47.60</td>
      </tr>
      <tr>
        <td data-title="Year">1983</td>
        <td data-title="Winner">Croeso</td>
        <td data-title="Jockey">Frank Olivares</td>
        <td data-title="Trainer">Stephen A. DiMauro</td>
        <td data-title="Owner">Cardiff Stud Farm/J. &amp; R. Fowler</td>
        <td data-title="Time">1:49.80</td>
      </tr>
      <tr>
        <td data-title="Year">1982</td>
        <td data-title="Winner">Timely Writer</td>
        <td data-title="Jockey">Jeffrey Fell</td>
        <td data-title="Trainer">Dominic Imprescia</td>
        <td data-title="Owner">Peter &amp; Francis Martin</td>
        <td data-title="Time">1:49.60</td>
      </tr>
      <tr>
        <td data-title="Year">1981</td>
        <td data-title="Winner">Lord Avie</td>
        <td data-title="Jockey">Chris McCarron</td>
        <td data-title="Trainer">Daniel Perlsweig</td>
        <td data-title="Owner">David Simon</td>
        <td data-title="Time">1:50.40</td>
      </tr>
      <tr>
        <td data-title="Year">1980</td>
        <td data-title="Winner">Plugged Nickle</td>
        <td data-title="Jockey">Buck Thornburg</td>
        <td data-title="Trainer">Thomas J. Kelly</td>
        <td data-title="Owner">John M. Schiff</td>
        <td data-title="Time">1:50.20</td>
      </tr>
      <tr>
        <td data-title="Year">1979</td>
        <td data-title="Winner">Spectacular Bid</td>
        <td data-title="Jockey">Ronnie Franklin</td>
        <td data-title="Trainer">Bud Delp</td>
        <td data-title="Owner">Hawksworth Farm</td>
        <td data-title="Time">1:48.80</td>
      </tr>
      <tr>
        <td data-title="Year">1978</td>
        <td data-title="Winner">Alydar</td>
        <td data-title="Jockey">Jorge Velasquez</td>
        <td data-title="Trainer">John M. Veitch</td>
        <td data-title="Owner">Calumet Farm</td>
        <td data-title="Time">1:47.00</td>
      </tr>
      <tr>
        <td data-title="Year">1977‡</td>
        <td data-title="Winner">Ruthie's Native</td>
        <td data-title="Jockey">Craig Perret</td>
        <td data-title="Trainer">Eugene Jacobs</td>
        <td data-title="Owner">Ruth A. Perlmutter</td>
        <td data-title="Time">1:50.20</td>
      </tr>
      <tr>
        <td data-title="Year">1977‡</td>
        <td data-title="Winner">Coined Silver</td>
        <td data-title="Jockey">Buck Thornburg</td>
        <td data-title="Trainer">George T. Poole III</td>
        <td data-title="Owner">C. V. Whitney</td>
        <td data-title="Time">1:48.80</td>
      </tr>
      <tr>
        <td data-title="Year">1976</td>
        <td data-title="Winner">Honest Pleasure</td>
        <td data-title="Jockey">Braulio Baeza</td>
        <td data-title="Trainer">LeRoy Jolley</td>
        <td data-title="Owner">Bertram R. Firestone</td>
        <td data-title="Time">1:47.80</td>
      </tr>
      <tr>
        <td data-title="Year">1975</td>
        <td data-title="Winner">Prince Thou Art</td>
        <td data-title="Jockey">Braulio Baeza</td>
        <td data-title="Trainer">Lou Rondinello</td>
        <td data-title="Owner">Darby Dan Farm</td>
        <td data-title="Time">1:50.40</td>
      </tr>
      <tr>
        <td data-title="Year">1974</td>
        <td data-title="Winner">Judger</td>
        <td data-title="Jockey">Laffit Pincay, Jr.</td>
        <td data-title="Trainer">Woody Stephens</td>
        <td data-title="Owner">Claiborne Farm</td>
        <td data-title="Time">1:49.00</td>
      </tr>
      <tr>
        <td data-title="Year">1973</td>
        <td data-title="Winner">Royal and Regal</td>
        <td data-title="Jockey">Walter Blum</td>
        <td data-title="Trainer">Warren A. Croll, Jr.</td>
        <td data-title="Owner">Aisco Stable</td>
        <td data-title="Time">1:47.40</td>
      </tr>
      <tr>
        <td data-title="Year">1972</td>
        <td data-title="Winner">Upper Case</td>
        <td data-title="Jockey">Ron Turcotte</td>
        <td data-title="Trainer">Lucien Laurin</td>
        <td data-title="Owner">Meadow Stable</td>
        <td data-title="Time">1:50.00</td>
      </tr>
      <tr>
        <td data-title="Year">1971</td>
        <td data-title="Winner">Eastern Fleet</td>
        <td data-title="Jockey">Eddie Maple</td>
        <td data-title="Trainer">Reggie Cornell</td>
        <td data-title="Owner">Calumet Farm</td>
        <td data-title="Time">1:47.40</td>
      </tr>
      <tr>
        <td data-title="Year">1970</td>
        <td data-title="Winner">My Dad George</td>
        <td data-title="Jockey">Ray Broussard</td>
        <td data-title="Trainer">Frank J. McManus</td>
        <td data-title="Owner">Raymond M. Curtis</td>
        <td data-title="Time">1:50.80</td>
      </tr>
      <tr>
        <td data-title="Year">1969</td>
        <td data-title="Winner">Top Knight</td>
        <td data-title="Jockey">Manuel Ycaza</td>
        <td data-title="Trainer">Raymond F. Metcalf</td>
        <td data-title="Owner">Steven B. Wilson</td>
        <td data-title="Time">1:48.40</td>
      </tr>
      <tr>
        <td data-title="Year">1968</td>
        <td data-title="Winner">Forward Pass</td>
        <td data-title="Jockey">Don Brumfield</td>
        <td data-title="Trainer">Henry Forrest</td>
        <td data-title="Owner">Calumet Farm</td>
        <td data-title="Time">1:49.00</td>
      </tr>
      <tr>
        <td data-title="Year">1967</td>
        <td data-title="Winner">In Reality</td>
        <td data-title="Jockey">Earlie Fires</td>
        <td data-title="Trainer">Melvin Calvert</td>
        <td data-title="Owner">Frances A. Genter</td>
        <td data-title="Time">1:50.20</td>
      </tr>
      <tr>
        <td data-title="Year">1966</td>
        <td data-title="Winner">Williamston Kid †</td>
        <td data-title="Jockey">Robert Stevenson</td>
        <td data-title="Trainer">James Bartlett</td>
        <td data-title="Owner">Ternes &amp; Bartlett</td>
        <td data-title="Time">1:50.60</td>
      </tr>
      <tr>
        <td data-title="Year">1965</td>
        <td data-title="Winner">Native Charger</td>
        <td data-title="Jockey">John L. Rotz</td>
        <td data-title="Trainer">Raymond F. Metcalf</td>
        <td data-title="Owner">Warner Stable</td>
        <td data-title="Time">1:51.20</td>
      </tr>
      <tr>
        <td data-title="Year">1964</td>
        <td data-title="Winner">Northern Dancer</td>
        <td data-title="Jockey">Bill Shoemaker</td>
        <td data-title="Trainer">Horatio Luro</td>
        <td data-title="Owner">Windfields Farm</td>
        <td data-title="Time">1:50.80</td>
      </tr>
      <tr>
        <td data-title="Year">1963</td>
        <td data-title="Winner">Candy Spots</td>
        <td data-title="Jockey">Bill Shoemaker</td>
        <td data-title="Trainer">Mesh Tenney</td>
        <td data-title="Owner">Rex C. Ellsworth</td>
        <td data-title="Time">1:50.60</td>
      </tr>
      <tr>
        <td data-title="Year">1962</td>
        <td data-title="Winner">Ridan</td>
        <td data-title="Jockey">Manuel Ycaza</td>
        <td data-title="Trainer">LeRoy Jolley</td>
        <td data-title="Owner">Jolley / Woods / Greer</td>
        <td data-title="Time">1:50.40</td>
      </tr>
      <tr>
        <td data-title="Year">1961</td>
        <td data-title="Winner">Carry Back</td>
        <td data-title="Jockey">Johnny Sellers</td>
        <td data-title="Trainer">Jack A. Price</td>
        <td data-title="Owner">Mrs. Katherine Price</td>
        <td data-title="Time">1:48.80</td>
      </tr>
      <tr>
        <td data-title="Year">1960</td>
        <td data-title="Winner">Bally Ache</td>
        <td data-title="Jockey">Bobby Ussery</td>
        <td data-title="Trainer">Homer Pitt</td>
        <td data-title="Owner">Edgehill Farm</td>
        <td data-title="Time">1:47.60</td>
      </tr>
      <tr>
        <td data-title="Year">1959</td>
        <td data-title="Winner">Easy Spur</td>
        <td data-title="Jockey">Bill Hartack</td>
        <td data-title="Trainer">Paul L. Kelley</td>
        <td data-title="Owner">Spring Hill Farm</td>
        <td data-title="Time">1:47.20</td>
      </tr>
      <tr>
        <td data-title="Year">1958</td>
        <td data-title="Winner">Tim Tam</td>
        <td data-title="Jockey">Bill Hartack</td>
        <td data-title="Trainer">Horace A. Jones</td>
        <td data-title="Owner">Calumet Farm</td>
        <td data-title="Time">1:49.20</td>
      </tr>
      <tr>
        <td data-title="Year">1957</td>
        <td data-title="Winner">Gen. Duke</td>
        <td data-title="Jockey">Bill Hartack</td>
        <td data-title="Trainer">Horace A. Jones</td>
        <td data-title="Owner">Calumet Farm</td>
        <td data-title="Time">1:46.80</td>
      </tr>
      <tr>
        <td data-title="Year">1956</td>
        <td data-title="Winner">Needles</td>
        <td data-title="Jockey">David Erb</td>
        <td data-title="Trainer">Hugh L. Fontaine</td>
        <td data-title="Owner">D &amp; H Stable</td>
        <td data-title="Time">1:48.60</td>
      </tr>
      <tr>
        <td data-title="Year">1955</td>
        <td data-title="Winner">Nashua</td>
        <td data-title="Jockey">Eddie Arcaro</td>
        <td data-title="Trainer">Jim Fitzsimmons</td>
        <td data-title="Owner">Belair Stud</td>
        <td data-title="Time">1:53.20</td>
      </tr>
      <tr>
        <td data-title="Year">1954</td>
        <td data-title="Winner">Correlation</td>
        <td data-title="Jockey">Bill Shoemaker</td>
        <td data-title="Trainer">Noble Threewitt</td>
        <td data-title="Owner">Robert S. Lytle</td>
        <td data-title="Time">1:55.20</td>
      </tr>
      <tr>
        <td data-title="Year">1953</td>
        <td data-title="Winner">Money Broker</td>
        <td data-title="Jockey">Alfred Popara</td>
        <td data-title="Trainer">Vester R. Wright</td>
        <td data-title="Owner">G. &amp; G. Stable</td>
        <td data-title="Time">1:53.80</td>
      </tr>
      <tr>
        <td data-title="Year">1952</td>
        <td data-title="Winner">Sky Ship</td>
        <td data-title="Jockey">Ronnie Nash</td>
        <td data-title="Trainer">Preston M. Burch</td>
        <td data-title="Owner">Brookmeade Stable</td>
        <td data-title="Time">1:50.80</td>
      </tr>
    </tbody>
    <tfoot></tfoot>
  </table>
</div>

{literal}
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}