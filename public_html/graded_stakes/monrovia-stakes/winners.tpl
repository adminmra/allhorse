<div id="no-more-tables">
  <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0">
    <thead>
      <tr>
        <th width="70px">Year</th>
        <th>Winner</th>
        <th width="70px">Age</th>        
        <th>Jockey</th>
        <th>Trainer</th>
        <th width="75px">Time</th>
        </tr>
    </thead>
  <tbody>
    <tr>
      <td data-title="Year">2019 </td>
      <td data-title="Winner">S Y Sky </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Drayden Van Dyke </td>
      <td data-title="Trainer">Philip D'Amato </td>
      <td data-title="Time">0:57.54 </td>
    </tr>
    <tr>
      <td data-title="Year">2018 </td>
      <td data-title="Winner">Belvoir Bay </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">Peter Miller</td>
      <td data-title="Time">1:12.80 </td>
    </tr>
    <tr>
      <td data-title="Year">2017 </td>
      <td data-title="Winner">Illuminant </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Flavien Prat</td>
      <td data-title="Trainer">Michael McCarthy</td>
      <td data-title="Time">1:12.13 </td>
    </tr>
    <tr>
      <td data-title="Year">2016 </td>
      <td data-title="Winner">Prize Exhibit</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Santiago Gonzalez</td>
      <td data-title="Trainer">James M. Cassidy</td>
      <td data-title="Time">1:12.96 </td>
    </tr>
    <tr>
      <td data-title="Year">2015 </td>
      <td data-title="Winner">Shrinking Violet</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Wesley A. Ward</td>
      <td data-title="Time">1:12.56 </td>
    </tr>
    <tr>
      <td data-title="Year">2014 </td>
      <td data-title="Winner">Pontchatrain</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Thomas F. Proctor</td>
      <td data-title="Time">1:11.14 </td>
    </tr>
    <tr>
      <td data-title="Year">2013 </td>
      <td data-title="Winner">Mizdirection</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Michael Puype</td>
      <td data-title="Time">1:11.94 </td>
    </tr>
    <tr>
      <td data-title="Year">2012 </td>
      <td data-title="Winner">Mizdirection</td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Michael Puype</td>
      <td data-title="Time">1:11.64 </td>
    </tr>
    <tr>
      <td data-title="Year">2011 </td>
      <td data-title="Winner">Unzip Me </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Martin F. Jones</td>
      <td data-title="Time">1:12.72 </td>
    </tr>
    <tr>
      <td data-title="Year">2010 </td>
      <td data-title="Winner">Tuscan Evening</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Jerry Hollendorfer</td>
      <td data-title="Time">1:12.56 </td>
    </tr>
    <tr>
      <td data-title="Year">2009 </td>
      <td data-title="Winner">Jibboom </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:13.21 </td>
    </tr>
    <tr>
      <td data-title="Year">2008 </td>
      <td data-title="Winner">Alexandra Rose</td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Rafael Bejarano</td>
      <td data-title="Trainer">Neil D. Drysdale</td>
      <td data-title="Time">1:12.86 </td>
    </tr>
    <tr>
      <td data-title="Year">2007 </td>
      <td data-title="Winner">Society Hostess </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Garrett Gomez</td>
      <td data-title="Trainer">Christophe Clement</td>
      <td data-title="Time">1:12.85 </td>
    </tr>
    <tr>
      <td data-title="Year">2006</td>
      <td data-title="Winner"><em>no race</em></td>
       <td data-title="Age">&nbsp;</td>
       <td data-title="Jockey">&nbsp;</td> 
       <td data-title="Trainer">&nbsp;</td>
       <td data-title="Time">&nbsp;</td>
    </tr>
    <tr>
      <td data-title="Year">2005 </td>
      <td data-title="Winner">Awesome Lady </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Tyler Baze</td>
      <td data-title="Trainer">Gary Stute</td>
      <td data-title="Time">1:16.77 </td>
    </tr>
    <tr>
      <td data-title="Year">2004 </td>
      <td data-title="Winner">Resplendency </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Casey Fusilier</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Time">1:15.34 </td>
    </tr>
    <tr>
      <td data-title="Year">2003 </td>
      <td data-title="Winner">Icantgoforthat </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Tyler Baze</td>
      <td data-title="Trainer">Steve Knapp</td>
      <td data-title="Time">1:13.07 </td>
    </tr>
    <tr>
      <td data-title="Year">2002 </td>
      <td data-title="Winner">Lil Sister Stich </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Douglas F. O'Neill</td>
      <td data-title="Time">1:13.81 </td>
    </tr>
    <tr>
      <td data-title="Year">2001 </td>
      <td data-title="Winner">Paga </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Richard Mandella</td>
      <td data-title="Time">1:15.09 </td>
    </tr>
    <tr>
      <td data-title="Year">2000 </td>
      <td data-title="Winner">Evening Promise </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Kathy Walsh</td>
      <td data-title="Time">1:12.62 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 <em>(Dec.)</em></td>
      <td data-title="Winner">Show Me The Stage </td>
      <td data-title="Age">3 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Eric Guillot</td>
      <td data-title="Time">1:15.14 </td>
    </tr>
    <tr>
      <td data-title="Year">1999 <em>(Jan.)</em></td>
      <td data-title="Winner">Desert Lady </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Wallace Dollase</td>
      <td data-title="Time">1:14.59 </td>
    </tr>
    <tr>
      <td data-title="Year">1998 </td>
      <td data-title="Winner">Madame Pandit </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">Ron McAnally</td>
      <td data-title="Time">1:15.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1997 </td>
      <td data-title="Winner">Grab The Prize </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Darrell Vienna</td>
      <td data-title="Time">1:16.89 </td>
    </tr>
    <tr>
      <td data-title="Year">1996 </td>
      <td data-title="Winner">Klassy Kim </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Goncalino Almeida</td>
      <td data-title="Trainer">Melvin F. Stute</td>
      <td data-title="Time">1:14.48 </td>
    </tr>
    <tr>
      <td data-title="Year">1995 </td>
      <td data-title="Winner">Rabiadella </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Mike Orman</td>
      <td data-title="Time">1:14.92 </td>
    </tr>
    <tr>
      <td data-title="Year">1994 </td>
      <td data-title="Winner">Mamselle Bebette </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Corey Nakatani</td>
      <td data-title="Trainer">Jack Van Berg</td>
      <td data-title="Time">1:15.35 </td>
    </tr>
    <tr>
      <td data-title="Year">1993 </td>
      <td data-title="Winner">Glen Kate </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Corey Black</td>
      <td data-title="Trainer">Bill Shoemaker</td>
      <td data-title="Time">1:12.89 </td>
    </tr>
    <tr>
      <td data-title="Year">1992 </td>
      <td data-title="Winner">Middlefork Rapids </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Terry Knight</td>
      <td data-title="Time">1:12.55 </td>
    </tr>
    <tr>
      <td data-title="Year">1991 </td>
      <td data-title="Winner">Wedding Bouquet </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Kent Desormeaux</td>
      <td data-title="Trainer">Gary F. Jones</td>
      <td data-title="Time">1:13.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1990 </td>
      <td data-title="Winner">Down Again </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Corey Black</td>
      <td data-title="Trainer">Richard L. Cross</td>
      <td data-title="Time">1:13.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1989 </td>
      <td data-title="Winner">Daloma </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Fernando Valenzuela</td>
      <td data-title="Trainer">Charles E. Whittingham</td>
      <td data-title="Time">1:16.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1988 </td>
      <td data-title="Winner">Aberuschka </td>
      <td data-title="Age">6 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:14.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1987 </td>
      <td data-title="Winner">Sari's Heroine </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Pat Valenzuela</td>
      <td data-title="Trainer">Melvin F. Stute</td>
      <td data-title="Time">1:15.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1986 </td>
      <td data-title="Winner">Water Crystals </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Richard E. Mandella</td>
      <td data-title="Time">1:15.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1985 </td>
      <td data-title="Winner">Lina Cavalieri </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Eddie Delahoussaye</td>
      <td data-title="Trainer">Robert J. Frankel</td>
      <td data-title="Time">1:14.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1984 </td>
      <td data-title="Winner">Tangent </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Julio A. Garcia</td>
      <td data-title="Trainer">Wayne Murty</td>
      <td data-title="Time">1:14.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1983 </td>
      <td data-title="Winner">Matching </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Ray Sibille</td>
      <td data-title="Trainer">Steve Morguelan</td>
      <td data-title="Time">1:13.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1982 </td>
      <td data-title="Winner">Cat Girl </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Edwin J. Gregson</td>
      <td data-title="Time">1:14.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1981 </td>
      <td data-title="Winner">Kilijaro</td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Marco Castaneda</td>
      <td data-title="Trainer">Charles E. Whittingham</td>
      <td data-title="Time">1:12.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1980 </td>
      <td data-title="Winner">Fondre </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Frank Olivares</td>
      <td data-title="Trainer">John W. Fulton</td>
      <td data-title="Time">1:15.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1979 </td>
      <td data-title="Winner">Camarado </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Bill Shoemaker</td>
      <td data-title="Trainer">Charles E. Whittingham</td>
      <td data-title="Time">1:15.40 </td>
    </tr>
    <tr>
          <td data-title="Year">1979 </td>
      <td data-title="Winner">Palmistry </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Chris McCarron</td>
      <td data-title="Trainer">Willard L. Proctor</td>
      <td data-title="Time">1:15.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1978 </td>
      <td data-title="Winner">Little Happiness </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Laffit Pincay, Jr.</td>
      <td data-title="Trainer">Laz Barrera</td>
      <td data-title="Time">1:16.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1977 </td>
      <td data-title="Winner">Winter Solstice </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Mark Sellers</td>
      <td data-title="Trainer">Gordon C. Campbell</td>
      <td data-title="Time">1:13.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1976 </td>
      <td data-title="Winner">Winter Solstice </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Jerry Lambert</td>
      <td data-title="Trainer">Gordon C. Campbell</td>
      <td data-title="Time">1:17.60 </td>
    </tr>
    <tr>
      <td data-title="Year">1975 </td>
      <td data-title="Winner">†Special Goddess </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Sandy Hawley</td>
      <td data-title="Trainer">Robert Smith </td>
      <td data-title="Time">1:13.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1974 </td>
      <td data-title="Winner">Viva La Vivi </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Donald Pierce</td>
      <td data-title="Trainer">Harold Hodosh</td>
      <td data-title="Time">1:15.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1973 </td>
      <td data-title="Winner">Tizna </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Fernando Toro</td>
      <td data-title="Trainer">Henry M. Moreno</td>
      <td data-title="Time">1:13.80 </td>
    </tr>
    <tr>
      <td data-title="Year">1972 </td>
      <td data-title="Winner">Mia Mood </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Victor Tejada</td>
      <td data-title="Trainer">John W. Russell</td>
      <td data-title="Time">1:13.00 </td>
    </tr>
    <tr>
      <td data-title="Year">1971 </td>
      <td data-title="Winner">Atomic Wings </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Robby Kilborn</td>
      <td data-title="Trainer">James I. Nazworthy</td>
      <td data-title="Time">1:13.40 </td>
    </tr>
    <tr>
      <td data-title="Year">1970 </td>
      <td data-title="Winner">Beautiful Dream </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Jerry Lambert</td>
      <td data-title="Trainer">Joseph S. Dunn</td>
      <td data-title="Time">1:16.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1969 </td>
      <td data-title="Winner">Morgaise </td>
      <td data-title="Age">4 </td>
      <td data-title="Jockey">Donald Pierce</td>
      <td data-title="Trainer">James W. Maloney</td>
      <td data-title="Time">1:22.20 </td>
    </tr>
    <tr>
      <td data-title="Year">1968 </td>
      <td data-title="Winner">Mellow Marsh </td>
      <td data-title="Age">5 </td>
      <td data-title="Jockey">Mario Valenzuela</td>
      <td data-title="Trainer">Jay Saladin</td>
      <td data-title="Time">1:13.40 </td>
    </tr>
  </tbody>
</table>

</div>
{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
  <script src="/assets/js/jquery.sortElements.js"></script>
  <script src="/assets/js/sorttable-winners.js"></script>
{/literal}