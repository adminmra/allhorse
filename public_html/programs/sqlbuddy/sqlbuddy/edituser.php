<?php	 		 	
/*

SQL Buddy - Web based MySQL administration
http://www.sqlbuddy.com/

edituser.php
- change permissions, password

MIT license

2008 Calvin Lough <http://calv.in>

*/

include "functions.php";

loginCheck();

$conn->selectDB("mysql");

if (isset($_POST['editParts'])) {
	$editParts = $_POST['editParts'];
	
	$editParts = explode("; ", $editParts);
	
	$totalParts = count($editParts);
	$counter = 0;
	
	$firstField = true;
	
	foreach ($editParts as $part) {
		$part = trim($part);
		
		if ($part != "" && $part != ";") {
			
			list($user, $host) = explode("@", $part);
			
			$userSQL = $conn->query("SELECT * FROM `user` WHERE `User`='" . $user . "' AND `Host`='" . $host . "'");
			$dbuserSQL = $conn->query("SELECT * FROM `db` WHERE `User`='" . $user . "' AND `Host`='" . $host . "'");
			
			if ($conn->isResultSet($userSQL)) {
				
				$allPrivs = true;
				
				$dbShowList = array();
				
				if ($conn->isResultSet($dbuserSQL)) {
					
					$accessLevel = "LIMITED";
					
					while ($dbuserRow = $conn->fetchAssoc($dbuserSQL)) {
						$selectedPrivs = array();
						
						$dbShowList[] = $dbuserRow['Db'];
									
						foreach ($dbuserRow as $key=>$value) {
							if (substr($key, -5) == "_priv" && $key != "Grant_priv" && $value == "N") {
								$allPrivs = false;
							}
							
							if ($value == "N")
								$selectedPrivs[$key] = $value;
						}
						
						if (isset($thePrivList)) {
							$thePrivList = array_merge($thePrivList, $selectedPrivs);
						} else {
							$thePrivList = $dbuserRow;
						}
					}
				} else {
					$accessLevel = "GLOBAL";
					
					$userRow = $conn->fetchAssoc($userSQL);
					
					foreach ($userRow as $key=>$value) {
						if (substr($key, -5) == "_priv" && $key != "Grant_priv" && $value == "N") {
							$allPrivs = false;
						}
					}
					
					$thePrivList = $userRow;
				}
				
				echo '<form id="editform' . $counter . '" querypart="' . $part . '" onsubmit="saveUserEdit(\'editform' . $counter . '\'); return false;">';
				echo '<div class="edituser inputbox">';
				echo '<div class="errormessage" style="margin: 0 7px 13px; display: none"></div>';
				echo '<table class="edit" cellspacing="0" cellpadding="0">';
				
				?>
				
				<tr>
					<td class="secondaryheader"><?php	 	 ?>:</td>
					<td><strong><?php	 	 ?></strong></td>
				</tr>
				<tr>
					<td class="secondaryheader"><?php	 	 ?>:</td>
					<td><input type="password" class="text" name="NEWPASS" /></td>
				</tr>
				<?php	 	
				
				$dbList = $conn->listDatabases();
				
				if ($conn->isResultSet($dbList)) {
				
				?>
				<tr>
					<td class="secondaryheader"><?php	 	 ?>:</td>
					<td>
					<label><input type="radio" name="ACCESSLEVEL" value="GLOBAL" id="ACCESSGLOBAL<?php	 	 ?></label><br />
					<label><input type="radio" name="ACCESSLEVEL" value="LIMITED" id="ACCESSSELECTED<?php	 	 ?></label>
					
					<div id="dbaccesspane<?php	 	 ?> class="privpane">
						<table cellpadding="0">
						<?php	 	
						
						while ($dbRow = $conn->fetchArray($dbList)) {
							echo '<tr>';
							echo '<td>';
							echo '<label><input type="checkbox" name="DBLIST[]" value="' . $dbRow[0] . '"';
							
							if (in_array($dbRow[0], $dbShowList))
								echo ' checked="checked"';
							
							echo ' />' . $dbRow[0] . '</label>';
							echo '</td>';
							echo '</tr>';
						}
						
						?>
						</table>
					</div>
					
					</td>
				</tr>
				<?php	 	
				
				}
				
				?>
				<tr>
					<td class="secondaryheader"><?php	 	 ?>:</td>
					<td>
					<label><input type="radio" name="CHOICE" value="ALL" onchange="updatePane('EDITPRIVSELECTED<?php	 	 ?></label><br />
					<label><input type="radio" name="CHOICE" value="SELECTED" id="EDITPRIVSELECTED<?php	 	 ?></label>
					
					<div id="editprivilegepane<?php	 	 ?>>
					<div class="paneheader">
					<?php	 	 ?>
					</div>
					<table cellpadding="0" id="edituserprivs<?php	 	 ?>">
					<tr>
						<td width="50%">
						<label><input type="checkbox" name="PRIVILEGES[]" value="SELECT" <?php	 	 ?></label>
						</td>
						<td width="50%">
						<label><input type="checkbox" name="PRIVILEGES[]" value="INSERT" <?php	 	 ?></label>
						</td>
					</tr>
					<tr>
						<td>
						<label><input type="checkbox" name="PRIVILEGES[]" value="UPDATE" <?php	 	 ?></label>
						</td>
						<td>
						<label><input type="checkbox" name="PRIVILEGES[]" value="DELETE" <?php	 	 ?></label>
						</td>
					</tr>
					<tr>
						<td>
						<label><input type="checkbox" name="PRIVILEGES[]" value="INDEX" <?php	 	 ?></label>
						</td>
						<td>
						<label><input type="checkbox" name="PRIVILEGES[]" value="ALTER" <?php	 	 ?></label>
						</td>
					</tr>
					<tr>
						<td>
						<label><input type="checkbox" name="PRIVILEGES[]" value="CREATE" <?php	 	 ?></label>
						</td>
						<td>
						<label><input type="checkbox" name="PRIVILEGES[]" value="DROP" <?php	 	 ?></label>
						</td>
					</tr>
					<tr>
						<td colspan="2">
						<label><input type="checkbox" name="PRIVILEGES[]" value="CREATE TEMPORARY TABLES" <?php	 	 ?></label>
						</td>
					</tr>
					</table>
					<div id="adminprivlist<?php	 	 ?>">
					<div class="paneheader">
					<?php	 	 ?>
					</div>
					<table cellpadding="0" id="editadminprivs<?php	 	 ?>">
					<tr>
						<td width="50%">
						<label><input type="checkbox" name="PRIVILEGES[]" value="FILE" <?php	 	 ?></label>
						</td>
						<td width="50%">
						<label><input type="checkbox" name="PRIVILEGES[]" value="PROCESS" <?php	 	 ?></label>
						</td>
					</tr>
					<tr>
						<td>
						<label><input type="checkbox" name="PRIVILEGES[]" value="RELOAD" <?php	 	 ?></label>
						</td>
						<td>
						<label><input type="checkbox" name="PRIVILEGES[]" value="SHUTDOWN" <?php	 	 ?></label>
						</td>
					</tr>
					<tr>
						<td>
						<label><input type="checkbox" name="PRIVILEGES[]" value="SUPER" <?php	 	 ?></label>
						</td>
						<td>
						</td>
					</tr>
					</table>
					</div>
					</div>
					
					</td>
				</tr>
				</table>
				
				<table cellpadding="0">
				<tr>
					<td class="secondaryheader"><?php	 	 ?>:</td>
					<td>
					<label><input type="checkbox" name="GRANTOPTION" value="true" <?php	 	 ?></label>
					</td>
				</tr>
				</table>
				
				<div style="margin-top: 10px; height: 22px; padding: 4px 0">
					<input type="submit" class="inputbutton" value="<?php	 	 ?></a>
				</div>
				</div>
				</form>
			
			<?php	 	
			
			} else {
				echo __("User not found!");
			}
			
			$counter++;
		}
	}
	
}

?>