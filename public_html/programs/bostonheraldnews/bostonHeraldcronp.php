<?php	 		 	

/**
 * Author: Pold
 * email: fvillegas at acedevel.com
 * Copyright: usracing.com 2014
 */
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

define("EOF", "<br />\n");
define("NEWSABSPATH", "/home/ah/allhorse/public_html/programs/news");
define("NEWSPUBLICABSPATH", "/home/ah/usracing.com/htdocs/images/news");


require_once NEWSABSPATH . '/dbconnect_rw.inc';
require_once NEWSABSPATH . '/cron/commonFunctions.php';
require_once NEWSABSPATH . '/cron/simplehtmldom_1_5/simple_html_dom.php';

class BostonHeraldScrapper {
	var $debug;
	var $test;
	var $delay;

	function __construct($debug = false, $test = false, $delay = 5) {
		$this->debug = $debug;
		$this->test = $test;
		$this->delay = $delay;
	}

	public function main() {
		$html = file_get_html('http://bostonherald.com/sports/other/horse_racing');
		$issues = array();

		//stories
		$stories = array();
		$mainStoryDefined = false;
		foreach ($html->find('div.node-tease') as $entry) {
			$title = $teaser = $image = $url = "";
			$featured = 0;
			$url = $this->bh_findNset($entry, 'h2.node-title a', 0, 'href', 'htmlentities');
			if ($this->bh_checkURL($url, $prefijoURL)) {
				$url = $prefijoURL . $url;
				$title = $this->bh_findNset($entry, 'h2.node-title', 0, 'plaintext');
				$image = $this->bh_findNset($entry, 'a.tease-thumb img', 0, 'src');
				$teaser = $this->bh_findNset($entry, 'div.body-copy', 0, 'plaintext', 'htmlentities');

				if ($image && !$mainStoryDefined) {
					$mainStoryDefined = true;
					$featured = 1;
				}

				if ($title == "" || $teaser == "")
					continue;

				$stories[] = array("title" => $title, "image" => $image, "teaser" => $teaser, "url" => $url, "featured" => $featured);
			}
			unset($entry);
		}

		$this->randomSortAndProcess($stories, $issues);

		if (count($issues)) {
			$this->bh_reportWebmaster($issues);
		}

		echo EOF . "End of the scrapping script." . EOF;
	}

	private function randomSortAndProcess($entriesArray, &$issues) {
		$prefijoURL = "";
		if (count($entriesArray)) {
			$this->shuffle_assoc($entriesArray);
			foreach ($entriesArray as $item) {
				$this->bh_printEntryBasic($item['title'], $item['image'], $item['teaser'], $item['url']);
				$this->bh_processEntry($item['title'], $item['image'], $item['teaser'], $item['url'], $issues, $item['featured']);
				unset($item);
			}
		}
	}

	private function shuffle_assoc(&$array) {
		$keys = array_keys($array);

		shuffle($keys);

		foreach ($keys as $key) {
			$new[$key] = $array[$key];
		}

		$array = $new;

		return true;
	}

	private function bh_checkURL($url, &$prefijoURL) {
		$prefijoURL = '';
		$url = trim(strtolower($url));
		if (strpos($url, "http://bostonherald.com/") !== FALSE) {
			return true;
		} elseif (strpos($url, "http://") !== FALSE || strpos($url, "https://") !== FALSE) {
			echo "Status: '$url' is an external link, it points to other web address. Skipping ..." . EOF;
			return false;
		} elseif ($url == '') {
			echo "Status: URL is empty. Skipping ..." . EOF;
			return false;
		} else if (substr($url, 0, 1) == '/') {
			$prefijoURL = "http://bostonherald.com";
			return true;
		} else {
			$prefijoURL = "http://bostonherald.com/";
			return true;
		}
	}

	private function bh_findNset($item, $toFind, $index, $property, $params = '') {
		$out = "";
		$cont = 0;
		try {
			if (method_exists($item, "find")) {
				$objFind = $item->find($toFind);
				if ($objFind) {
					foreach ($objFind as $i) {
						eval('$out = $i->' . $property . ';');
						if ($cont >= $index)
							break;
					}
				}
				else
					throw new Exception("I could not find $toFind.");

				switch ($params) {
					case 'htmlentities': //htmlentities
						$out = htmlentities($out);
					case '':default:
						break;
				}
			} else {
				throw new Exception("I could not get the code and instance an object before trying to find $toFind.");
			}
			return trim($out);
		} catch (Exception $e) {
			echo $e->getMessage() . EOF;
		}
	}

	private function bh_processEntry($title, $image, $teaser, $url, &$issues, $featured = 0) {
		if ($title != "" && $teaser != "" && $url != "") {
			$pageuri = toAscii(urldecode(($title)), '');
			$nrecords = $this->bh_getNRecords($pageuri);

			if ($nrecords > 0 && !$this->test) {
				echo "Status: Entry '$pageuri' already scrapped, and already in database. Skipping ..." . EOF;
			} else {
				// New entry, proceed to scrape and store
				$this->bh_proceedEntry($url, $title, $pageuri, $issues, $featured, $teaser, $image);
			}
		} else {
			$possible_reason = "Wrong scrapping, probably sources have changed at http://bostonherald.com";
			$this->bh_reportError($issues, '', "title: " . $title, '', '', $teaser, 'image: ' . $image . " - url: " . $url, '', "", $possible_reason);
		}
	}

	private function bh_proceedEntry($url, $title, $pageuri, &$issues, $featured, $teaser = "", $image = "") {
		sleep($this->delay);
		$datenewentry = $titlenewentry = $bodynewentry = "-1";
		$ans1 = $ans2 = false;
		$articleHtml = file_get_html($url);
		try {
			$titlenewentry = $this->bh_findNset($articleHtml, 'h1.node-title', 0, 'plaintext');
			$bodynewentry = $this->bh_findNset($articleHtml, 'div.field-name-body', 0, 'innertext', 'htmlentities');
			$datenewentry = $this->bh_findNset($articleHtml, 'time.field-type-datestamp', 0, 'plaintext');
			$unixTimeStamp = strtotime($datenewentry);
			$picAuthor = $picDesc = $mainpic = "";
			if ($image) {
				$mainpic = $this->bh_findNset($articleHtml, 'div.file-image div.content img', 0, 'src');
				$picDesc = $this->bh_findNset($articleHtml, 'div.field-name-field-description div.field-item', 0, 'plaintext');
				$picAuthor = "Photo by " . $this->bh_findNset($articleHtml, 'div.field-name-field-authors div.field-item', 0, 'plaintext');
			}

			if ($teaser == 'noteaser') {
				$descriptionteasure = trim(cleaninputtreasure($bodynewentry));
				$teaser = AbbrHtml($descriptionteasure, '300');
			}

			//only store when all the data are correct
			if ($unixTimeStamp != '-1' && $pageuri != '-1' && $titlenewentry != '-1' && $bodynewentry != '-1') {
				$innerhtml = str_get_html($bodynewentry);

				foreach ($innerhtml->find('a') as $anchorItem) {
					$anchorItem->href = null;
				}

				$datenewentry2 = date("F j, Y", $unixTimeStamp);
				$datenewentry3 = date("Y-m-d H:i:s", $unixTimeStamp);

				$newEntryId = $this->bh_getNewEntryId();

				if ($featured) {
					$bodynewentry = trim($innerhtml->innertext);
					$this->bh_createMainPic($newEntryId, $mainpic);
				} else if ($image != "") {
					$bodynewentry = '<div class="newsimagebostonherald"><img class="img-responsive" src="' . $mainpic . '" alt="" /></div>' . trim($innerhtml->innertext);
				} else {
					$bodynewentry = trim($innerhtml->innertext);
				}

				$bodynewentry = '<div class="newsArticleWrapper">' . $bodynewentry . '</div>';

				//download pictures and update them in the source
				$picture = $pictureThumb = "";
				$this->bh_processImages($bodynewentry, $newEntryId, $picture, $pictureThumb);

				$Sql_insert1 = $Sql_insert2 = "";
				$ans1 = $this->bh_putNewsArchive($Sql_insert1, $newEntryId, $title, $teaser, $datenewentry2, $datenewentry3, $pageuri, $picture, $pictureThumb, $featured);
				$ans2 = $this->bh_putNewsArticle($Sql_insert2, $newEntryId, $title, $bodynewentry, $pageuri, $datenewentry2);

				if ($ans1 && $ans2) {
					echo "Status: New entry scrapped and saved to database successfully." . EOF;
				} else if ($this->test) {
					echo "Status: New entry is not being written. Test mode." . EOF;
				} else {
					$possible_reason = "Database Problem, maybe wrong credentials, or Database overloaded.";
					$this->bh_reportError($issues, $pageuri, $titlenewentry, $datenewentry, $datenewentry2, $teaser, $bodynewentry, $Sql_insert1, $Sql_insert2, $possible_reason);
				}
			} else {
				$possible_reason = "Wrong scrapping, maybe sources have changed at $url.";
				$this->bh_reportError($issues, $pageuri, $titlenewentry, $datenewentry, '', $teaser, $bodynewentry, '', '', $possible_reason);
			}
		} catch (Exception $e) {
			$possible_reason = "Wrong scrapping, here is other type of source code at $url. More details: " . $e->getMessage();
			$this->bh_reportError($issues, "", $title, date("Today: Y-m-d H:i:s"), '', $teaser, '', '', '', $possible_reason);
		}
	}

	private function bh_processImages(&$source, $newEntryId, &$picture, &$pictureThumb) {
		$picture = $pictureThumb = "";
		$dom = new DOMDocument();
		$dom->loadHTML($source);

		# remove <!DOCTYPE 
		$dom->removeChild($dom->firstChild);

		# remove <html><body></body></html> 
		$dom->replaceChild($dom->firstChild->firstChild->firstChild, $dom->firstChild);

		$path = NEWSPUBLICABSPATH . "/" . $newEntryId;
		$newSrc = "http://usracing.com/images/news/" . $newEntryId;

		$proceed = TRUE;

		$imgs = $dom->getElementsByTagName("img");
		$cont = 1;
		foreach ($imgs as $img) {
			if ($proceed) {
				if (!file_exists($path))
					mkdir($path);
				$proceed = FALSE;
			}
			$oldSrc = $img->getAttribute('src');
			$pathinfo = pathinfo($oldSrc);
			$Imagefile = $path . "/" . $pathinfo['filename'] . ".jpg";
			$imgcontent = file_get_contents($oldSrc);
			file_put_contents($Imagefile, $imgcontent);
			$newSrc .= "/" . $pathinfo['filename'] . ".jpg";
			$img->setAttribute('src', $newSrc);

			if ($cont == 1) {
				$picture = $newSrc;
				$dest = $path . "/" . $pathinfo['filename'] . "-thumb.jpg";
				$this->bh_createThumb($Imagefile, $dest);
				$pictureThumb = "http://usracing.com/images/news/" . $newEntryId . "/" . $pathinfo['filename'] . "-thumb.jpg";
			}
			++$cont;
		}
		$source = $dom->saveHTML();
	}

	private function bh_createMainPic($newEntryId, $image) {
		if ($image != '') {

			$imgcontent = file_get_contents($image);
			$Imagefile = NEWSABSPATH . "/images/" . $newEntryId . ".jpg";
			file_put_contents($Imagefile, $imgcontent);
			$dest = NEWSABSPATH . "/images/" . $newEntryId . "-otb.jpg";
			$this->bh_createThumb($Imagefile, $dest);

			return true;
		}
		return false;
	}

	private function bh_createThumb($src, $dest) {
		require_once NEWSABSPATH . "/cron/imageresizer.php";
		$sImg = new SimpleImage();
		$sImg->load($src);

		/* $Height = $sImg->getHeight();
		  $width = $sImg->getWidth();
		  $ratio = 150 / $width;
		  $Newheight = $Height * $ratio; */

		$sImg->resizeToWidth(350);
		$sImg->save($dest);
	}

	private function bh_getNewEntryId() {
		$sql_highestId = " SELECT MAX(ContentId) as MAXContentIdART FROM newsarchive ";
		$result_highestId = mysql_query_w($sql_highestId);
		$data_highestId = mysql_fetch_object($result_highestId);
		$newEntryId = $data_highestId->MAXContentIdART + 1;
		return $newEntryId;
	}

	private function bh_getNRecords($pageuri) {
		$sql = sprintf(" SELECT * FROM newsarchive WHERE pageuri = '%s' AND typetosee = 'OTH' ", $pageuri);
		$result = mysql_query_w($sql);
		$nrecords = mysql_num_rows($result);
		return $nrecords;
	}

	private function bh_putNewsArchive(&$Sql_insert, $newEntryId, $title, $teaser, $datenewentry2, $datenewentry3, $pageuri, $picture = "", $pictureThumb = "", $featured = 0) {
		$Sql_insert = " INSERT INTO newsarchive SET 
               ContentId ='" . $newEntryId . "',
			   featured ='" . $featured . "',
               Title= '" . cleaninput($title) . "',
               Teaser ='" . cleaninputtreasure($teaser) . "',
               DateCreated = '" . $datenewentry2 . "',
               timecreate ='" . $datenewentry3 . "',
               `doc-id` ='Horse',
               pageuri ='" . trim($pageuri) . "',
			   picture ='" . $picture . "',
			   pictureThumb ='" . $pictureThumb . "',
               typetosee  ='OTH' ";
		if ($this->debug) {
			echo $Sql_insert . EOF;
		}
		if ($this->test) {
			return false;
		}
		return mysql_query_w($Sql_insert);
	}

	private function bh_putNewsArticle(&$Sql_insert, $newEntryId, $title, $bodynewentry, $pageuri, $datenewentry2) {
		$Sql_insert = " INSERT INTO newsarticle SET 
				ContentIdART ='" . $newEntryId . "',
				TitleART= '" . cleaninput($title) . "',
				HtmlART = '" . cleaninput($bodynewentry) . "',
				url ='" . $pageuri . "',
				DateCreatedART ='" . $datenewentry2 . "' ";
		if ($this->debug) {
			echo $Sql_insert . EOF;
		}
		if ($this->test) {
			return false;
		}
		return mysql_query_w($Sql_insert);
	}

	private function bh_printEntryBasic($title, $image, $teaser, $url) {
		echo EOF . "Title: " . $title . EOF;
		echo "Picture: " . $image . EOF;
		echo "Teaser: " . $teaser . EOF;
		echo "Url: " . $url . EOF;
	}

	private function bh_reportError(&$issues, $pageuri, $titlenewentry, $datenewentry, $datenewentry2, $teaser, $bodynewentry, $sql1, $sql2, $possible_reason) {
		echo "Status: There has ocurred an error, and it was reported to Webmaster." . EOF;
		$issues[] = array("pageuri" => $pageuri, "title" => $titlenewentry, "date1" => $datenewentry, "date2" => $datenewentry2, "teaser" => $teaser, "contents" => $bodynewentry, "sql1" => $sql1, "sql2" => $sql2, "possible_reason" => $possible_reason);
	}

	private function bh_reportWebmaster($issues) {
		$to = "webmaster.villegas@gmail.com";
		$from = "dr@scrapperusracing.com";
		$subject = "Master, there is a problem while scrapping from foxsports.com";

		$issues_msj = '<table border="1" style="border:1px solid black;">';
		foreach ($issues as $issueI) {
			$issues_msj .= '<tr><td>Reason: </td><td>' . $issueI['possible_reason'] . '</td></tr>';
			$issues_msj .= '<tr><td>Date 1: </td><td>' . $issueI['date1'] . '</td></tr>';
			$issues_msj .= '<tr><td>Date 2: </td><td>' . $issueI['date2'] . '</td></tr>';
			$issues_msj .= '<tr><td>Title: </td><td>' . $issueI['title'] . '</td></tr>';
			$issues_msj .= '<tr><td>Page Uri: </td><td>' . $issueI['pageuri'] . '</td></tr>';
			$issues_msj .= '<tr><td>Teaser: </td><td>' . $issueI['teaser'] . '</td></tr>';
			$issues_msj .= '<tr><td>contents: </td><td>' . $issueI['contents'] . '</td></tr>';
			$issues_msj .= '<tr><td>Sql 1: </td><td>' . $issueI['sql1'] . '</td></tr>';
			$issues_msj .= '<tr><td>Sql 2: </td><td>' . $issueI['sql2'] . '</td></tr>';
			$issues_msj .= '<tr><td colspan="2"><center>-seperator-</center></td></tr>';
		}
		$issues_msj .= '</table>';

		$message = "<html><body bgcolor=\"#DCEEFC\">
					<b>" . date("Y-m-d H:i:s") . "</b> <br> 
					Sorry to report <font color=\"red\">ERRORS</font> Master<br> 
					<a href=\"http://allhorse.com/programs/foxnews/foxnewscronp.php\">FoxNews Scrapper</a><br /> 
					<a href=\"http://usracing.com/Feeds/cron.php\">Usracing Feed Processor</a><br />
					<br><br>" . $issues_msj . "
				</body></html>";

		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: $from\r\n";
		//$headers .= "Cc: [email]maa@p-i-s.cXom[/email]"; 
		//$headers .= "Bcc: [email]email@maaking.cXom[/email]"; 

		if (mail($to, $subject, $message, $headers)) {
			echo EOF . "There were some problems, errors were reported to Webmaster." . EOF;
		} else {
			echo "Sending errors to Webmaster failed. Check whether mail destinatary is ok and the mail server is working properly.";
		}
	}

	function __destruct() {
		;
	}

}
//$bh_ins = new BostonHeraldScrapper(true, true, 2);
$bh_ins = new BostonHeraldScrapper();
$bh_ins->main();
