<?php	 		 	

/**
 * @author: Leopold <webmaster.villegas at gmail dot com>
 * 
 * @copyright (c) 2013, Acedevel
 */
//ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//error_reporting(E_ALL);
define("EOF", "<br />\n");
define("SMARTYPATH", "/home/ah/usracing.com/smarty");

require_once ('/home/ah/allhorse/public_html/programs/foxnews/simplehtmldom_1_5/simple_html_dom.php');

class KentuckyDerbyPrepRaces {
	var $test;
	var $minimumRecords;

	function __construct($test = false, $minimumRecords = 10) {
		$this->test = $test;
		$this->minimumRecords = $minimumRecords;
	}

	function main() {
		$urlkd = "http://www.kentuckyderby.com/road/schedule";
		$html = file_get_html($urlkd);
		$currentDay = strtotime(date("Y-m-d"));
		$arrayOlder = array();
		$arrayRecent = array();
		$arrayRecent[] = array("raceDate" => "Date", "title" => "Race", "grade" => "Grade", "raceDistance" => "Race Distance", "track" => "Track", "points" => "Points");
		$records = 0;
		foreach ($html->find(".view-dom-id-2 tbody tr") as $tr) {
			$raceDate = $title = $grade = $raceDistance = $track = $points = "";
			$raceDate = trim($tr->find(".views-field-field-race-date-value", 0)->plaintext);
			$title = trim($tr->find(".views-field-title", 0)->plaintext);
			$grade = trim($tr->find(".views-field-phpcode", 0)->plaintext);
			$raceDistance = trim($tr->find(".views-field-field-race-distance-value", 0)->plaintext);
			$track = trim($tr->find(".views-field-field-track-nid", 0)->plaintext);
			$points = trim($tr->find(".views-field-field-race-points-value", 0)->plaintext);
			if (strtotime($raceDate) < $currentDay)
				$arrayOlder[] = array("raceDate" => $raceDate, "title" => $title, "grade" => $grade, "raceDistance" => $raceDistance, "track" => $track, "points" => $points);
			else
				$arrayRecent[] = array("raceDate" => $raceDate, "title" => $title, "grade" => $grade, "raceDistance" => $raceDistance, "track" => $track, "points" => $points);
			$records++;
		}
		unset($arrayOlder[0]);
		$arrayOlder[] = array("raceDate" => "Date", "title" => "Race", "grade" => "Grade", "raceDistance" => "Race Distance", "track" => "Track", "points" => "Points");
		$arrayOlder = array_reverse($arrayOlder);

		if ($records >= 10) {//if there are more than 10 scrapped records so write the output to the file, otherwise report
			$table1 = $this->kd_drawTable($arrayRecent, 'infoEntries', 'Road to the Roses Kentucky Derby Prep Races', true);
			$table2 = $this->kd_drawTable($arrayOlder, 'infoEntries', 'Old Kentucky Derby Prep Races', false);
			//$table3 = $this->kd_drawTable($arrayRecent, 'infoEntries', 'Road to the Roses', true, 10, "", "<td><strong>", "</strong></td>");
			$table3 = $this->kd_drawTable($arrayRecent, 'infoEntries', 'Road to the Roses', true, 10);
			$output = $table1 . $table2;
			$output .= '<a href="http://www.usracing.com" style="color:white;">Online Horse Betting</a>';
			echo $output . EOF;
			echo $table3 . EOF;
			echo "Process completed successfully.";
			if (!$this->test) {
				file_put_contents('/home/ah/allhorse/public_html/kd/prepraces.php', $output);
				file_put_contents('/home/ah/allhorse/public_html/kd/road-to-roses.php', $table3);
			}
			else
				echo EOF . "This is just a test, not writing." . EOF;
		} else {
			$issues = array('possible_reason' => $records . ' is less than allowed minimum records (' . $this->minimumRecords . ') for writing the file', 'date' => date("Y-m-d H:i:s"), 'contents' => '<pre>' . print_r($arrayRecent, true) . EOF . print_r($arrayOlder, true) . '</pre>');
		}
	}

	function kd_getTrackName(&$track) {
		$track = stripslashes($track);
		$tracknameCleaned = strtolower($track);
		$tracknameCleaned = trim(preg_replace("/[^ \w]+/", "", $tracknameCleaned));
		$linkForTrackname = '';
		if ($tracknameCleaned !== '') {
			$tmp1 = str_replace(" ", "-", $tracknameCleaned);
			$tmp2 = SMARTYPATH . "/templates/content/racetrack/" . $tmp1 . ".tpl";
			$tmp3 = SMARTYPATH . "/templates/content/racetrack/" . $tmp1 . "-race-course.tpl";
			if (file_exists($tmp2))
				$linkForTrackname = "http://usracing.com/" . str_replace(" ", "-", $tmp1);
			else if (file_exists($tmp3))
				$linkForTrackname = "http://usracing.com/" . str_replace(" ", "-", $tmp1) . "-race-course";
		}
		return $linkForTrackname;
	}

	function kd_getRaceName(&$stake) {
		$stake = stripslashes($stake);
		$racenameCleaned = strtolower($stake);
		$racenameCleaned = trim(preg_replace("/[^ \w]+/", "", $racenameCleaned));
		$linkForRacename = '';
		if ($racenameCleaned !== '') {
			$tmp1 = str_replace(" ", "-", $racenameCleaned);
			$tmp2 = SMARTYPATH . "/templates/content/stake/" . $tmp1 . ".tpl";
			$tmp3 = SMARTYPATH . "/templates/content/stake/" . $tmp1 . "-stakes.tpl";
			if (file_exists($tmp2))
				$linkForRacename = "http://usracing.com/" . str_replace(" ", "-", $tmp1);
			else if (file_exists($tmp3))
				$linkForRacename = "http://usracing.com/" . str_replace(" ", "-", $tmp1) . "-stakes";
		}
		return $linkForRacename;
	}

	function kd_chckLink($link, $openTag, $closeTag, $content) {
		if ($link !== '') {
			return $openTag . '<a href="' . $link . '">' . $content . '</a>' . $closeTag;
		} else {
			return $openTag . $content . $closeTag;
		}
	}

	function kd_drawTable($array, $idTable, $titleT, $schema = false, $limit = 1000, $oddclass="odd", $headtag1="<th>", $headtag2="</th>") {
		$output = '<div class="tableSchema table-responsive"><table id="' . $idTable . '"  width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" title="' . $titleT . '" >';
		$preTags = '<tbody><tr>';
		$postTags = '</tr>';
		$openTag = $headtag1;
		$closeTag = $headtag2;
		$eventSchema = ($schema) ? ' itemscope itemtype="http://schema.org/Event" ' : '';
		$i = 1;
		foreach ($array as $item) {
			$raceDate = $title = $grade = $raceDistance = $track = $points = "";
			$raceDate = $item["raceDate"];
			$title = $item["title"];
			$grade = $item["grade"];
			$raceDistance = $item["raceDistance"];
			$track = $item["track"];
			$points = $item["points"];

			$linkForTrackname = $this->kd_getTrackName($track);
			$linkForRacename = $this->kd_getRaceName($title);

			$output .= $preTags;
			if ($i == 1) {
				$output .= $openTag . "Date" . $closeTag;
				$output .= $openTag . "Race" . $closeTag;
				//$output .= $openTag . $grade . $closeTag;
				$output .= $openTag . $raceDistance . $closeTag;
				$output .= $openTag . $track . $closeTag;
				$output .= $openTag . $points . $closeTag;
			} else {
				if ($schema) {
					$output .= $openTag . '<time itemprop="startDate" content="' . date("Y-m-d", strtotime($raceDate)) . '">' . $raceDate . '</time>' . $closeTag;
					if ($linkForRacename != '')
						$output .= '<td itemprop="name"><a itemprop="url" href="' . $linkForRacename . '">' . $title . '</a></td>';
					else
						$output .= '<td itemprop="name">' . $title . '</td>';

					//$output .= $openTag . $grade . $closeTag;
					$output .= '<td itemprop="description">' . $raceDistance . '</td>';

					if ($linkForTrackname != '')
						$output .= '<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="' . $linkForTrackname . '" itemprop="url">' . $track . '</a></span></td>';
					else
						$output .= '<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">' . $track . '</span></td>';

					$output .= $openTag . $points . $closeTag;
				}
				else {
					$output .= $openTag . $raceDate . $closeTag;
					$output .= $this->kd_chckLink($linkForRacename, $openTag, $closeTag, $title);
					//$output .= $openTag . $grade . $closeTag;
					$output .= $openTag . $raceDistance . $closeTag;
					$output .= $this->kd_chckLink($linkForTrackname, $openTag, $closeTag, $track);
					$output .= $openTag . $points . $closeTag;
				}
			}

			$output .= $postTags;
			if ($i > $limit)
				break;

			$preTags = '<tr  ' . $eventSchema . (++$i % 2 == 1 ? ' class="'.$oddclass.'"' : '') . '>';
			$postTags = "</tr>\n";
			$openTag = '<td>';
			$closeTag = '</td>';
		}

		if ($i > 1)
			$output .= '</tbody>';

		$output .= '</table></div>';
		return $output;
	}

	function kd_reportWebmaster($issues) {
		$to = "webmaster.villegas@gmail.com";
		$from = "dr@scrapperusracing.com";
		$subject = "Master, there is a problem while scrapping kentucky derby prep races";

		$issues_msj = '<table border="1" style="border:1px solid black;">';
		foreach ($issues as $issueI) {
			$issues_msj .= '<tr><td>Reason: </td><td>' . $issueI['possible_reason'] . '</td></tr>';
			$issues_msj .= '<tr><td>Date: </td><td>' . $issueI['date'] . '</td></tr>';
			$issues_msj .= '<tr><td>contents: </td><td>' . $issueI['contents'] . '</td></tr>';
			$issues_msj .= '<tr><td colspan="2"><center>-seperator-</center></td></tr>';
		}
		$issues_msj .= '</table>';

		$message = "<html><body bgcolor=\"#DCEEFC\">
					<b>" . date("Y-m-d H:i:s") . "</b> <br> 
					Master sorry for reporting <font color=\"red\">ERRORS</font><br /> 
					<a href=\"http://allhorse.com/programs/kd/kdPrepracesBasic.php?test=1\">Kentucky Derby Prep races Scrapper</a><br /> 
					<br><br>" . $issues_msj . "
				</body></html>";

		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: $from\r\n";
		//$headers .= "Cc: [email]maa@p-i-s.cXom[/email]"; 
		//$headers .= "Bcc: [email]email@maaking.cXom[/email]"; 

		if (mail($to, $subject, $message, $headers)) {
			echo EOF . "There were some problems, errors were reported to Webmaster." . EOF;
		} else {
			echo EOF . "Warning: Sending errors to Webmaster failed. Check whether mail destinatary is ok and the mail server is working properly." . EOF;
		}
	}

	function __destruct() {
		;
	}

}
//instance
$kd_ins = new KentuckyDerbyPrepRaces();
$kd_ins->main();
?>