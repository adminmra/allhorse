<?php	 	

/**
 * Author: Pold
 * email: fvillegas at acedevel.com
 * Copyright: usracing.com 2014
 */
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

define("EOF", "<br />\n");
define("CONTENDERSDESTINYFILE", "/home/ah/allhorse/public_html/kd/contenders.php");
define("NEWSABSPATH", "/home/ah/allhorse/public_html/programs/news");
define("NEWSPUBLICABSPATH", "/home/ah/usracing.com/htdocs/images/news");

require_once ('/home/ah/allhorse/public_html/programs/foxnews/simplehtmldom_1_5/simple_html_dom.php');

class BHContendersScrapper {
	var $debug;

	function __construct($debug = false) {
		$this->debug = $debug;
	}

	public function bh_main() {
		$html = file_get_html('http://www.bloodhorse.com/horse-racing/triple-crown/road-to-the-kentucky-derby');
		$cont = 0;
		$table = "";

		foreach ($html->find('table.DataTable') as $entry) {
			foreach ($entry->find('tr') as $tr) {
				$rank = $horse = $points = $Non_Restricted_Stakes_Earnings = $Graded_Stakes_Earnings = $Breeder = $Trainer = $Owner = "";
				$cont_td = 0;
				foreach ($tr->find("td") as $td) {
					switch ($cont_td) {
						case 0:
							$rank = $td->plaintext;
							break;
						case 1:
							$horse = $td->plaintext;
							break;
						case 3:
							$points = $td->plaintext;
							break;
						case 4:
							$Non_Restricted_Stakes_Earnings = $td->plaintext;
							break;
						case 5:
							$Graded_Stakes_Earnings = $td->plaintext;
							break;
						case 6:
							$Breeder = $td->plaintext;
							break;
						case 7:
							$Trainer = $td->plaintext;
							break;
						case 8:
							$Owner = $td->plaintext;
							break;
					}
					unset($td);
					$cont_td++;
				}

				if ($this->debug)
					echo $rank . " " . $horse . " " . $points . " " . $Non_Restricted_Stakes_Earnings . " " . $Graded_Stakes_Earnings . " " . $Breeder . " " . $Trainer . " " . $Owner . " " . EOF;

				if ($rank == "" || $horse == "" || $points == "" || $Graded_Stakes_Earnings == "" || $Trainer == "")
					continue;

				$table .= '<tr><td>' . $rank . '</td>';
				$table .= '<td>' . $horse . '</td>';
				$table .= '<td>' . $points . '</td>';
				$table .= '<td>' . $Graded_Stakes_Earnings . '</td>';
				$table .= '<td>' . $Trainer . '</td></tr>';

				++$cont;
				unset($tr);
			}
			unset($entry);
			break;
		}

		if ($cont > 0) {
			$output = '<div class="table-responsive"><table  class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Kentucky Derby Contenders">';
			$output .= '<thead><caption>Kentucky Derby Horses</caption><tr><th>Rank</th><th>Horse</th><th>Points</th><th>Earnings</th><th>Trainer</th></tr></thead><tbody>';
			$output .= $table;
			$output .= '<tr><td class="dateUpdated center" colspan="5"><em>Updated '.date("F jS, Y").' - US Racing <a href="http://www.usracing.com/kentucky-derby/contenders">Kentucky Derby Contenders</a></em></td></tr></tbody></table></div>';
			file_put_contents(CONTENDERSDESTINYFILE, $output);
		} else {
			//report error
			$this->bh_reportWebmaster();
		}
		
		echo EOF . "End of script." . EOF;
	}

	private function bh_reportWebmaster() {
		$to = "webmaster.villegas@gmail.com";
		$from = "dr@scrapperusracing.com";
		$subject = "Master, there is a problem while scrapping from bloodhorse.com contenders";

		$issues_msj = '<p>Error while trying to scrape from http://www.bloodhorse.com/horse-racing/triple-crown/road-to-the-kentucky-derby, zero results.</p>';

		$message = "<html><body bgcolor=\"#DCEEFC\">
					<b>" . date("Y-m-d H:i:s") . "</b> <br> 
					Sorry to report <font color=\"red\">ERRORS</font> Master<br> 
					<a href=\"http://allhorse.com/programs/kd/contenderscronp.php\">BloodHorse Scrapper</a><br /> 
					<a href=\"http://usracing.com/Feeds/cron.php\">Usracing Feed Processor</a><br />
					<br><br>" . $issues_msj . "
				</body></html>";

		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: $from\r\n";
		//$headers .= "Cc: [email]maa@p-i-s.cXom[/email]"; 
		//$headers .= "Bcc: [email]email@maaking.cXom[/email]"; 

		if (mail($to, $subject, $message, $headers)) {
			echo EOF . "There were some problems, errors were reported to Webmaster." . EOF;
		} else {
			echo "Sending errors to Webmaster failed. Check whether mail destinatary is ok and the mail server is working properly.";
		}
	}

	function __destruct() {
		;
	}

}
$ins = new BHContendersScrapper(true);
$ins->bh_main();
//
?>