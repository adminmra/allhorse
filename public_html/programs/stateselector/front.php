<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">		
		<meta name="description" content="">
		<meta name="keywords" content="">
		<style>
			body{margin: 0; padding: 0}
			.mainbox{width: 100%; height: 100%; margin: 0; padding: 0px 0 0 0; position: relative; text-align: center}
			.mainbox .box-wrap{width: 500px; margin: 0 auto; position: relative; background: #f4f4f4; position: relative; top: 100px}
			.mainbox .box-wrap .in{padding: 20px}
			.mainbox .box-wrap input, .mainbox .box-wrap select{padding: 5px;}
		</style>
	</head>
	<body>
		<div class="mainbox">
			<div class="box-wrap">
				<h1>BET ON THE BELMONT STAKES, TODAY!</h1>
				<div class="in">
					<h2>Get Started Now!</h2>					
					<div class="form">
					<?php
					$statesArr = array(""=>"Select a State", "NA"=>"Outside the USA", "AL"=>"Alabama", 
								"AK"=>"Alaska", "AZ"=>"Arizona", "AR"=>"Arkansas", "CA"=>"California", "CO"=>"Colorado", "CT"=>"Connecticut", "DE"=>"Delaware", "DC"=>"District Of Columbia", "FL"=>"Florida", "GA"=>"Georgia", "HI"=>"Hawaii", "ID"=>"Idaho", "IL"=>"Illinois", "IN"=>"Indiana", "IA"=>"Iowa", "KS"=>"Kansas", "KY"=>"Kentucky", "LA"=>"Louisiana", "ME"=>"Maine", "MD"=>"Maryland", "MA"=>"Massachusetts", "MI"=>"Michigan", "MN"=>"Minnesota", "MS"=>"Mississippi", "MO"=>"Missouri", "MT"=>"Montana", "NE"=>"Nebraska", "NV"=>"Nevada", "NH"=>"New Hampshire", "NJ"=>"New Jersey", "NM"=>"New Mexico", "NY"=>"New York", "NC"=>"North Carolina", "ND"=>"North Dakota", "OH"=>"Ohio", "OK"=>"Oklahoma", "OR"=>"Oregon", "PA"=>"Pennsylvania", "RI"=>"Rhode Island", "SC"=>"South Carolina", "SD"=>"South Dakota", "TN"=>"Tennessee", "TX"=>"Texas", "UT"=>"Utah", "VT"=>"Vermont", "VA"=>"Virginia", "WA"=>"Washington", "WV"=>"West Virginia", "WI"=>"Wisconsin", "WY"=>"Wyoming");
					?>
						<form method="POST" action="/stateselector/processdata.php" id="formUsers" onsubmit="return validate()">
							<!--p><input type="text" name="fullName" id="fullName" placeholder="Enter your full name"  /></p>
							<p><input type="text" name="emailAddress" id="emailAddress" placeholder="Enter your email address"  /></p-->
							<p><select name="stateCode" id="usResidents" data-validation="required"><?php
									foreach($statesArr as $code=>$state){
										echo sprintf('<option value="%s">%s</option>', $code, $state);
									}
									?></select></p>
							<p><input type="submit" name="send" id="submitForm" value="I Want To Bet" /></p>
						</form>
					</div>
				</div>
			</div>
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
		<script>$.validate({});</script>
	</body>
</html>

