<?php
/*ini_set("display_errors","on");
error_reporting(E_ALL);
*/
if(isset($_REQUEST['IdLeague']) AND strcasecmp(trim($_REQUEST['IdLeague']),"")!==0) { $argv[1] = $_REQUEST['IdLeague']; }
if(isset($_REQUEST['BannerAbTrue'])  AND strcasecmp(trim($_REQUEST['BannerAbTrue']),"")!==0) { $argv[2] = $_REQUEST['BannerAbTrue']; }
if(isset($_REQUEST['BannerAbFalse'])  AND strcasecmp(trim($_REQUEST['BannerAbFalse']),"")!==0) { $argv[3] = $_REQUEST['BannerAbFalse']; }
if(isset($_REQUEST['SectionTitle'])  AND strcasecmp(trim($_REQUEST['SectionTitle']),"")!==0) { $argv[4] = $_REQUEST['SectionTitle']; }

$id_league = NULL;

if(isset($argv[1]) AND strcasecmp($argv[1], intval($argv[1])) == 0)
{
    $id_league = $argv[1];
}

$banner_ab_true = "Off";
$banner_ab_false = "Off";
$banner_ab_false = "Off";
$section_title = "BELMONT STAKES WINNER";
if(isset($argv[2]))
{
    $banner_ab_true = $argv[2];            
}

if(isset($argv[3]))
{    
    $banner_ab_false = $argv[3];        
}
if(isset($argv[4]))
{    
    $section_title = $argv[4];        
}

$parameters_status = "IdLeague = ".$id_league.", bannerAbTrue = ".$banner_ab_true.", bannerAbFalse = ".$banner_ab_false;

/*if(! isset($_REQUEST['IdLeague']))
{
    echo $parameters_status."\n";
}*/

#$string = file_get_contents("odds1.xml"); 
#$string = file_get_contents("odds2.xml"); 
#$string = file_get_contents("http://www.betusracing.ag/odds.xml"); 
$string = file_get_contents("http://ww1.betusracing.ag/odds.xml"); 

$xml = new SimpleXMLElement($string);

$result = $xml->xpath('/xml/league[@IdLeague="'.$id_league.'"]/game[@vtm="'.$section_title.'"]');
//$result = $xml->xpath('/xml/league[@IdLeague="23"]/game');

/*echo "<pre>";
print_r($result);
echo "</pre>";*/

$parent_node = $xml->xpath('/xml/league[@IdLeague="'.$id_league.'"]');

if(isset($parent_node) AND count($parent_node)>0)
{
    $parent_node_attributes = $parent_node[0]->attributes();    
}

//list(, $league_node_attributes) = each($result2);                    
ob_start();

if(count($result)>0)
{
    $table_title = $parent_node_attributes['Description'];
    $array_contenders = array();




    $i=0;
    while(list( ,$game_node) = each($result)) 
    {
        $game_attributes = $game_node->attributes();            
        $array_contenders[$i]['name'] = $game_attributes['htm'];

        list(, $game_children) = each($game_node->children());                    

        if(is_array($game_children))
        {
            while(list( ,$line_node) = each($game_children)) 
            {
                $line_attributes = $line_node->attributes();                    
                $array_contenders[$i]['odds'][] = fractional_odds_calculate($line_attributes);
            }
        }
        else
        {
            $line_attributes = $game_children->attributes();                                                                           
            $array_contenders[$i]['odds'][] = fractional_odds_calculate($line_attributes);
        }
        $i++;           
    }



    ?>  
    <div class="table-responsive">
        <table  class="data table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Belmont Stakes Odds">
            <caption><?php echo $table_title ?></caption>     
            <tbody>
                <tr>   
                    <th>Contenders</th>            
                    <th>American Odds</th>            
                    <th>Fractional Odds</th>            
                </tr>
                <?php
                for($i=0;$i<count($array_contenders);$i++)
                    {?>
                <tr>
                    <td>
                        <?php echo $array_contenders[$i]['name']?>
                    </td>
                    <td>                    
                        <table class="fer" style="width:100%">
                            <?php
                            $array_odds = $array_contenders[$i]['odds'];
                            for($j=0;$j<count($array_odds);$j++)
                                {?>
                            <tr>
                                <td><?php echo $array_odds[$j]['oddsh']?></td>                                
                            </tr>
                            <?php
                        }                        
                        ?>
                    </table>
                </td>
                <td>                    
                    <table class="fer" style="width:100%">
                        <?php
                        $array_odds = $array_contenders[$i]['odds'];
                        for($j=0;$j<count($array_odds);$j++)
                            {?>
                        <tr>                                
                            <td><?php echo $array_odds[$j]['fractional_odd']?></td>
                        </tr>
                        <?php
                    }                        
                    ?>
                </table>
            </td>
        </tr>
        <?php
    }
    ?>      
    <tr>      
        <td class="dateUpdated center" colspan="5"><em>Updated <?php echo date("F j, Y");?>. BUSR - Official <a href="http://www.usracing.com/belmont-stakes/odds">Belmont Stakes Odds</a>. <br>Bet now to lock in your Belmont Stakes future odds prices. </em></td>
    </tr>
</tbody>   
</table>    
<?php
}
else
{
    echo "Please check back shorlty";
}    
?>
</div>

<?php
$str_table = ob_get_clean();

if( isset($_REQUEST['IdLeague']) OR count($result)>0 )
{
    echo $str_table;

    #$ROOT_DIR = "/www/usracing";    
    $ROOT_DIR = "/home/ah";
    
    $PATH_FILE = $ROOT_DIR."/allhorse/public_html/belmont/odds__busr_xml.php";
    file_put_contents($PATH_FILE, $str_table);    
}
else
{
    echo "Please check back shorlty";
}



##################################################################################################
function fractional_odds_calculate(& $line_attributes)
{
    $row = array();
    $row['oddsh'] = $line_attributes['oddsh'];  

    $oodsh = intval($line_attributes['oddsh']);
    if(intval($oodsh)>0)
    {
       $row['fractional_odd'] = float2rat($oodsh/100);     
   }
   else if($oodsh<0)
   {
     $row['fractional_odd'] = float2rat(100/(-$oodsh));
 }
 else
 {
    $row['fractional_odd'] = $line_attributes['oddsh'];   
}
return $row;
}


function float2rat($n, $tolerance = 1.e-6) {
    $h1=1; $h2=0;
    $k1=0; $k2=1;
    $b = 1/$n;
    do {
        $b = 1/$b;
        $a = floor($b);
        $aux = $h1; $h1 = $a*$h1+$h2; $h2 = $aux;
        $aux = $k1; $k1 = $a*$k1+$k2; $k2 = $aux;
        $b = $b-$a;
    } while (abs($n-$h1/$k1) > $n*$tolerance);

    return "$h1/$k1";
}