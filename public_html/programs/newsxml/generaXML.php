<?php
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

define("EOF", "<br />\n");
define("NEWSABSPATH", "/home/ah/allhorse/public_html/programs/news/");
define("OUTPUTABSPATH", "/home/ah/allhorse/public_html/output/newsxml/");
require_once NEWSABSPATH . 'dbconnect_rw.inc';

$index=0;
$multiplo = 10;
$prefix = "news";

$query = "SELECT timecreate FROM newsarchive ORDER BY timecreate desc LIMIT 0,1";
$ans = mysql_query_w($query);
$ans = mysql_fetch_assoc($ans);
echo $ans['timecreate'] . EOF;

$dateFrom = strtotime($ans['timecreate'])-(365*24*60*60);
$dateFrom = date('Y-m-d H:i:s', $dateFrom);
echo $dateFrom . EOF;

$query = sprintf(" SELECT count(pageuri) AS totalc FROM newsarchive WHERE timecreate>='%s' ", $dateFrom);
$ans = mysql_query_w($query);
$ans = mysql_fetch_assoc($ans);
$totalNews = $ans['totalc'];
echo $totalNews . EOF;

$totalPages = (integer)($totalNews/10);
if($totalNews%$multiplo>0)
	$totalPages++;
echo $totalPages . EOF;	

if($totalPages>0)
	array_map('unlink', glob(OUTPUTABSPATH."*")); // WARNING use it carefully

for ( $page = 1 ; $page <= $totalPages ; $page++) {
	$filename = OUTPUTABSPATH . $prefix . $page . ".xml";
	echo "Writting ".$filename.EOF;	
	$query = sprintf(" SELECT * FROM newsarchive WHERE timecreate>='%s' ORDER BY timecreate DESC LIMIT %d, %d ", $dateFrom, $index, $multiplo);
	$results = mysql_query_w($query);
	
	$xml = new DOMDocument();
	$xml_root = $xml->createElement("filexml");

	while ($row = mysql_fetch_assoc($results)) {
		echo $row['ContentId'].EOF;
		echo $row['Teaser'].EOF;
		echo $row['Title'].EOF;
		echo $row['pageuri'].EOF;
		echo $row['DateCreated'].EOF;
		
		$query2 = sprintf("SELECT HtmlArt FROM newsarticle WHERE ContentIdArt=%d", $row['ContentId']);
		$result = mysql_query_w($query2);
		$HtmlArt = mysql_fetch_object($result);
		
		$xml_item = $xml->createElement("item");
		$xml_ID = $xml->createElement("ContentId");
		$xml_Title = $xml->createElement("Title");
		$xml_Teaser = $xml->createElement("Teaser");
		$xml_pageuri = $xml->createElement("pageuri");
		$xml_DateCreated = $xml->createElement("DateCreated");
		$xml_Article = $xml->createElement("Article");
		$cdataNode = $xml->createCDATASection($HtmlArt->HtmlArt);
		
		$xml_ID->nodeValue = $row['ContentId'];
		$xml_Title->nodeValue = $row['Title'];
		$xml_Teaser->nodeValue = $row['Teaser'];
		$xml_pageuri->nodeValue = $row['pageuri'];
		$xml_DateCreated->nodeValue = $row['DateCreated'];
		$xml_Article->appendChild($cdataNode);
		
		$xml_item->appendChild( $xml_ID );
		$xml_item->appendChild( $xml_Title );
		$xml_item->appendChild( $xml_Teaser );
		$xml_item->appendChild( $xml_pageuri );
		$xml_item->appendChild( $xml_DateCreated );
		$xml_item->appendChild( $xml_Article );
		
		$xml_root->appendChild( $xml_item );
	}
	
	$xml->appendChild( $xml_root );
	$xml->save($filename);
	
	$index+=$multiplo;
}
