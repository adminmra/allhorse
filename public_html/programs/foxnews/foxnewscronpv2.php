<?php	 		 	

/**
 * Author: Leopold
 * email: fvillegas@acedevel.com
 * Copyright: usracing.com 2013
 */
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

define("EOF", "<br />\n");
define("NEWSABSPATH", "/home/ah/allhorse/public_html/programs/news");


require_once NEWSABSPATH . '/dbconnect_rw.inc';
require_once NEWSABSPATH . '/cron/commonFunctions.php';
require_once 'simplehtmldom_1_5/simple_html_dom.php';

class FoxSportScrapper {
	var $debug;
	var $test;
	var $delay;

	function __construct($debug = false, $test = false, $delay = 5) {
		$this->debug = $debug;
		$this->test = $test;
		$this->delay = $delay;
	}

	public function main() {
		$html = file_get_html('http://msn.foxsports.com/horseracing/');
		$issues = array();

		//main story
		$prefijoURL = "";
		$olympicCP = $html->find('div#olympicCP', 0);
		$url_main = $olympicCP->find('span.titleHeadline a', 0)->href;
		$teaser_main = trim($olympicCP->find('span.blurb', 0)->plaintext);
		$image_main = $olympicCP->find('div.cpMain img', 0)->src;
		$title_main = trim($olympicCP->find('div.cpMain span.titleHeadline', 0)->plaintext);
		$this->fs_printEntryBasic($title_main, $image_main, $teaser_main, $url_main);
		if ($this->fs_checkURL($url_main, $prefijoURL)){
			$url_main = $prefijoURL . $url_main;
			$this->fs_processEntry($title_main, $image_main, $teaser_main, $url_main, $issues, 1); // last one id for featured
		}

		//sidebar stories
		$sidebar_stories = array();
		foreach ($html->find('div#olympicHeadlines li') as $entry) {
			$title_sidebar = trim($entry->find('a', 0)->plaintext);
			$url_sidebar = $entry->find('a', 0)->href;
			$sidebar_stories[] = array("title"=>$title_sidebar, "image"=>"#", "teaser"=>"noteaser", "url"=>$url_sidebar);
		}
		$this->randomSortAndProcess($sidebar_stories, $issues);

		//body stories
		$body_stories =array();
		foreach ($html->find('ul.writersExclusive li') as $entry) {
			$title = $image = $teaser = $url = '';
			$title = trim($entry->find('span.story-title', 0)->plaintext);
			$url = $entry->find('a', 0)->href;
			$image = $entry->find('img', 0)->src;
			$teaser = trim($entry->find('span.story-description', 0)->plaintext);
			$body_stories[] = array("title"=>$title, "image"=>$image, "teaser"=>$teaser, "url"=>$url);
		}
		$this->randomSortAndProcess($body_stories, $issues);

		if (count($issues)) {
			$this->fs_reportWebmaster($issues);
		}
		
		echo EOF . "End of the scrapping script." . EOF;
	}
	
	private function randomSortAndProcess($entriesArray, &$issues){
      $prefijoURL = "";
      if(count($entriesArray)){
         $this->shuffle_assoc($entriesArray);
         foreach($entriesArray as $item){
            $this->fs_printEntryBasic($item['title'], $item['image'], $item['teaser'], $item['url']);
            if ($this->fs_checkURL($item['url'], $prefijoURL)){
               $url = $prefijoURL . $item['url'];
               $this->fs_processEntry($item['title'], $item['image'], $item['teaser'], $url, $issues);
            }
         }
      }
	}
	
	private function shuffle_assoc(&$array) {
        $keys = array_keys($array);

        shuffle($keys);

        foreach($keys as $key) {
            $new[$key] = $array[$key];
        }

        $array = $new;

        return true;
    }
	
	private function fs_checkURL($url, &$prefijoURL){
      $prefijoURL = '';
		$url = trim(strtolower($url));
		if(strpos($url, "http://msn.foxsports.com/")!== FALSE){
			return true;
		}
		elseif (strpos($url, "http://")!== FALSE || strpos($url, "https://")!== FALSE) {
			echo "Status: '$url' is an external link, it points to other web address. Skipping ..." . EOF;
			return false;
		}
		elseif($url == ''){
			echo "Status: URL is empty. Skipping ..." . EOF;
			return false;
		}
		else if(substr($url, 0, 1)=='/'){
			$prefijoURL = "http://msn.foxsports.com";
			return true;
		}
		else{
			$prefijoURL = "http://msn.foxsports.com/";
			return true;
		}
	}

	private function fs_processEntry($title, $image, $teaser, $url, &$issues, $featured = 0) {
		if ($title != "" && $image != "" && $teaser != "" && $url != "") {
			$pageuri = toAscii(urldecode(($title)), '');
			$nrecords = $this->fs_getNRecords($pageuri);

			if ($nrecords > 0) {
				echo "Status: Entry '$pageuri' already scrapped, and already in database. Skipping ..." . EOF;
			} else {
				// New entry, proceed to scrape and store
				$this->fs_proceedEntry($url, $title, $pageuri, $issues, $featured, $teaser, $image);
			}
		} else {
			$possible_reason = "Wrong scrapping, probably sources have changed at http://msn.foxsports.com/horseracing/";
			$this->fs_reportError($issues, '', "title: " . $title, '', '', $teaser, 'image: ' . $image . " - url: " . $url, '', "", $possible_reason);
		}
	}

	private function fs_proceedEntry($url, $title, $pageuri, &$issues, $featured, $teaser = "", $image = "") {
		sleep($this->delay);
		$unixTimeStamp = $titlenewentry = $bodynewentry = "-1";
		$ans1 = $ans2 = false;
		$htmlnewentry = file_get_html($url);
		try{
         if (method_exists($htmlnewentry,"find")) {
            $find_headline = $htmlnewentry->find('div.fs-article-headline');
            if ($find_headline){
               foreach($find_headline as $fhItem){
                  $titlenewentry = trim($fhItem->plaintext);
                  break;
               }
            }
            else
               throw new Exception("I could not get the article title.");
         }
         else
            throw new Exception("I could not get the code and instance an object from $url.");
         
         $find_timestamp = $htmlnewentry->find('div.story-timestamp');
         if($find_timestamp){
            foreach($find_timestamp as $ftItem){
               $datenewentry = $ftItem->plaintext;
               break;
            }
         }
         else
            throw new Exception("I could not get the date.");
         
         $find_article = $htmlnewentry->find('div.fs-article');
         if($find_article){
            foreach($find_article as $faItem){
               $bodynewentry = trim($faItem->innertext);
               break;
            }
         }
         else
            throw new Exception("I could not get the article body.");
            
         $datenewentry = trim(str_replace('Updated&nbsp;', '', $datenewentry));
         $datenewentry = trim(str_replace(' ET', '', $datenewentry));
         $unixTimeStamp = strtotime($datenewentry);
            
         if ($teaser == 'noteaser') {
            $descriptionteasure = trim(cleaninputtreasure($bodynewentry));
            $teaser = AbbrHtml($descriptionteasure, '150');
         }

         //only store when all the data are correct
         if ($unixTimeStamp != '-1' && $pageuri != '-1' && $titlenewentry != '-1' && $bodynewentry != '-1') {
            $innerhtml = str_get_html($bodynewentry);
            
            $find_top_container = $innerhtml->find("div#story-top-container");
            if($find_top_container){
               foreach($find_top_container as $ftcItem){
                  $ftcItem->outertext = "";
                  break;
               }
            }
            else
               throw new Exception("I could not clean the story top container div.");

            foreach($innerhtml->find("div.parentWrapper") as $divpw){
               $divpw->outertext = "";
            }
            
            foreach($innerhtml->find('a') as $anchorItem){
               $anchorItem->href = null;
            }
            
            if($featured)
               $bodynewentry = trim($innerhtml->innertext);
            else
               $bodynewentry = '<div clas="newsimagefoxsports"><img src="' . $image . '" alt="" /></div>' . trim($innerhtml->innertext);

            $newEntryId = $this->fs_getNewEntryId();

            $datenewentry2 = date("F j, Y", $unixTimeStamp);
            $datenewentry3 = date("Y-m-d H:i:s", $unixTimeStamp);

            if ($this->test) {
               echo "Status: New entry is not being written. Test mode." . EOF;
            } else {
               if ($featured)
                  $this->fs_createMainPic($newEntryId, $image);
               $Sql_insert1 = $Sql_insert2 = "";
               $ans1 = $this->fs_putNewsArchive($Sql_insert1, $newEntryId, $title, $teaser, $datenewentry2, $datenewentry3, $pageuri, $featured);
               $ans2 = $this->fs_putNewsArticle($Sql_insert2, $newEntryId, $title, $bodynewentry, $pageuri, $datenewentry2);

               if ($ans1 && $ans2) {
                  echo "Status: New entry scrapped and saved to database successfully." . EOF;
               } else {
                  $possible_reason = "Database Problem, maybe wrong credentials, or Database overloaded.";
                  $this->fs_reportError($issues, $pageuri, $titlenewentry, $datenewentry, $datenewentry2, $teaser, $bodynewentry, $Sql_insert1, $Sql_insert2, $possible_reason);
               }
            }
         } else {
            $possible_reason = "Wrong scrapping, maybe sources have changed at $url.";
            $this->fs_reportError($issues, $pageuri, $titlenewentry, $datenewentry, '', $teaser, $bodynewentry, '', '', $possible_reason);
         }
		}
		catch(Exception $e){
         $possible_reason = "Wrong scrapping, here is other type of source code at $url. More details: " . $e->getMessage();
         $this->fs_reportError($issues, "", $title, date("Today: Y-m-d H:i:s"), '', $teaser, '', '', '', $possible_reason);
		}
	}

	private function fs_createMainPic($newEntryId, $image) {
		if ($image != '') {
			require_once NEWSABSPATH . "/cron/imageresizer.php";

			$imgcontent = file_get_contents($image);
			$Imagefile = NEWSABSPATH . "/images/" . $newEntryId . ".jpg";
			file_put_contents($Imagefile, $imgcontent);

			$sImg = new SimpleImage();
			$sImg->load($Imagefile);

			/* $Height = $sImg->getHeight();
			  $width = $sImg->getWidth();
			  $ratio = 150 / $width;
			  $Newheight = $Height * $ratio; */

			$sImg->resizeToWidth(350);
			$sImg->save(NEWSABSPATH . "/images/" . $newEntryId . "-otb.jpg");
			return true;
		}
		return false;
	}

	private function fs_getNewEntryId() {
		$sql_highestId = " SELECT MAX(ContentId) as MAXContentIdART FROM newsarchive ";
		$result_highestId = mysql_query_w($sql_highestId);
		$data_highestId = mysql_fetch_object($result_highestId);
		$newEntryId = $data_highestId->MAXContentIdART + 1;
		return $newEntryId;
	}

	private function fs_getNRecords($pageuri) {
		$sql = sprintf(" SELECT * FROM newsarchive WHERE pageuri = '%s' AND typetosee = 'OTH' ", $pageuri);
		$result = mysql_query_w($sql);
		$nrecords = mysql_num_rows($result);
		return $nrecords;
	}

	private function fs_putNewsArchive(&$Sql_insert, $newEntryId, $title, $teaser, $datenewentry2, $datenewentry3, $pageuri, $featured = 0) {
		$Sql_insert = " INSERT INTO newsarchive SET 
               ContentId ='" . $newEntryId . "',
			   featured ='" . $featured . "',
               Title= '" . cleaninput($title) . "',
               Teaser ='" . cleaninputtreasure($teaser) . "',
               DateCreated = '" . $datenewentry2 . "',
               timecreate ='" . $datenewentry3 . "',
               `doc-id` ='Horse',
               pageuri ='" . trim($pageuri) . "',
               typetosee  ='OTH' ";
		$ans = mysql_query_w($Sql_insert);
		if ($this->debug) {
			echo $Sql_insert . EOF;
			var_dump($ans);
		}
		return $ans;
	}

	private function fs_putNewsArticle(&$Sql_insert, $newEntryId, $title, $bodynewentry, $pageuri, $datenewentry2) {
		$Sql_insert = " INSERT INTO newsarticle SET 
				ContentIdART ='" . $newEntryId . "',
				TitleART= '" . cleaninput($title) . "',
				HtmlART = '" . cleaninput($bodynewentry) . "',
				url ='" . $pageuri . "',
				DateCreatedART ='" . $datenewentry2 . "' ";
		$ans = mysql_query_w($Sql_insert);
		if ($this->debug) {
			echo $Sql_insert . EOF;
			var_dump($ans);
		}
		return $ans;
	}

	private function fs_printEntryBasic($title, $image, $teaser, $url) {
		echo EOF . "Title: " . $title . EOF;
		echo "Picture: " . $image . EOF;
		echo "Teaser: " . $teaser . EOF;
		echo "Url: " . $url . EOF;
	}

	private function fs_reportError(&$issues, $pageuri, $titlenewentry, $datenewentry, $datenewentry2, $teaser, $bodynewentry, $sql1, $sql2, $possible_reason) {
		echo "Status: There has ocurred an error, and it was reported to Webmaster." . EOF;
		$issues[] = array("pageuri" => $pageuri, "title" => $titlenewentry, "date1" => $datenewentry, "date2" => $datenewentry2, "teaser" => $teaser, "contents" => $bodynewentry, "sql1" => $sql1, "sql2" => $sql2, "possible_reason" => $possible_reason);
	}

	private function fs_reportWebmaster($issues) {
		$to = "webmaster.villegas@gmail.com";
		$from = "dr@scrapperusracing.com";
		$subject = "Master, there is a problem while scrapping from foxsports.com";

		$issues_msj = '<table border="1" style="border:1px solid black;">';
		foreach ($issues as $issueI) {
			$issues_msj .= '<tr><td>Reason: </td><td>' . $issueI['possible_reason'] . '</td></tr>';
			$issues_msj .= '<tr><td>Date 1: </td><td>' . $issueI['date1'] . '</td></tr>';
			$issues_msj .= '<tr><td>Date 2: </td><td>' . $issueI['date2'] . '</td></tr>';
			$issues_msj .= '<tr><td>Title: </td><td>' . $issueI['title'] . '</td></tr>';
			$issues_msj .= '<tr><td>Page Uri: </td><td>' . $issueI['pageuri'] . '</td></tr>';
			$issues_msj .= '<tr><td>Teaser: </td><td>' . $issueI['teaser'] . '</td></tr>';
			$issues_msj .= '<tr><td>contents: </td><td>' . $issueI['contents'] . '</td></tr>';
			$issues_msj .= '<tr><td>Sql 1: </td><td>' . $issueI['sql1'] . '</td></tr>';
			$issues_msj .= '<tr><td>Sql 2: </td><td>' . $issueI['sql2'] . '</td></tr>';
			$issues_msj .= '<tr><td colspan="2"><center>-seperator-</center></td></tr>';
		}
		$issues_msj .= '</table>';

		$message = "<html><body bgcolor=\"#DCEEFC\">
					<b>" . date("Y-m-d H:i:s") . "</b> <br> 
					Sorry to report <font color=\"red\">ERRORS</font> Master<br> 
					<a href=\"http://allhorse.com/programs/foxnews/foxnewscronp.php\">FoxNews Scrapper</a><br /> 
					<a href=\"http://usracing.com/Feeds/cron.php\">Usracing Feed Processor</a><br />
					<br><br>" . $issues_msj . "
				</body></html>";

		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: $from\r\n";
		//$headers .= "Cc: [email]maa@p-i-s.cXom[/email]"; 
		//$headers .= "Bcc: [email]email@maaking.cXom[/email]"; 

		if (mail($to, $subject, $message, $headers)) {
			echo EOF . "There were some problems, errors were reported to Webmaster." . EOF;
		} else {
			echo "Sending errors to Webmaster failed. Check whether mail destinatary is ok and the mail server is working properly.";
		}
	}

	function __destruct() {
		;
	}

}
$fs_ins = new FoxSportScrapper();
$fs_ins->main();
