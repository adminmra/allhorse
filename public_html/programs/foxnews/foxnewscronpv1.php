<?php	 		 	

/**
 * Author: Leopold
 * email: fvillegas@acedevel.com
 * Copyright: usracing.com 2003
 */
ini_set('display_errors', 1);
ini_set('log_errors', 1);
error_reporting(E_ALL);

define("EOF", "<br />\n");


require_once '/home/ah/allhorse/public_html/programs/news/dbconnect_rw.inc';
require_once '/home/ah/allhorse/public_html/programs/news/cron/commonFunctions.php';
require_once 'simplehtmldom_1_5/simple_html_dom.php';

$html = file_get_html('http://msn.foxsports.com/horseracing/');
$cont = 1;
$ans1 = $ans2 = false;
$issues = array();

foreach ($html->find('ul.writersExclusive li') as $entry) {
	$title = trim($entry->find('span.story-title', 0)->plaintext);
	$image = $entry->find('img', 0)->src;
	$teaser = trim($entry->find('span.story-description', 0)->plaintext);
	$url = "http://msn.foxsports.com" . $entry->find('a', 0)->href;
	echo EOF . $cont . ") Title: " . $title . EOF;
	echo "Picture: " . $image . EOF;
	echo "Teaser: " . $teaser . EOF;
	echo "Url: " . $url . EOF;

	$pageuri = toAscii(urldecode(($title)), '');
	$sql = sprintf(" SELECT * FROM newsarchive WHERE pageuri = '%s' AND typetosee = 'OTH' ", $pageuri);
	$result = mysql_query_w($sql);
	$nrecords = mysql_num_rows($result);

	if ($nrecords > 0) {
		echo "Status: Entry '$pageuri' already scrapped, and already in database. Skipping ..." . EOF;
	} else {
		// New entry, proceed to scrape and store
		$htmlnewentry = file_get_html($url);
		$titlenewentry = trim($htmlnewentry->find('div.fs-article-headline', 0)->plaintext);
		$datenewentry = $htmlnewentry->find('div.story-timestamp', 0)->plaintext;
		$datenewentry = trim(str_replace('Updated&nbsp;', '', $datenewentry));
		$datenewentry = trim(str_replace(' ET', '', $datenewentry));
		$unixTimeStamp = strtotime($datenewentry);
		$bodynewentry = trim($htmlnewentry->find('div.fs-article', 0)->innertext);

		//only store when all the data are correct
		if ($unixTimeStamp > 0 && $pageuri != '' && $titlenewentry != "" && $bodynewentry != '') {
			$innerhtml = str_get_html($bodynewentry);
			$innerhtml->find("div#story-top-container", 0)->outertext = "";
			$innerhtml->find("div.parentWrapper", 0)->outertext = "";
			$bodynewentry = '<div clas="newsimagefoxsports"><img src="' . $image . '" /></div>' . trim($innerhtml->innertext);

			//Getting new Entry id
			$sql_highestId = " SELECT MAX(ContentId) as MAXContentIdART FROM newsarchive ";
			$result_highestId = mysql_query_w($sql_highestId);
			$data_highestId = mysql_fetch_object($result_highestId);
			$newEntryId = $data_highestId->MAXContentIdART + 1;

			$datenewentry2 = date("F j, Y", $unixTimeStamp);
			$datenewentry3 = date("Y-m-d H:i:s", $unixTimeStamp);

			$Sql_insert1 = " INSERT INTO newsarchive SET 
               ContentId ='" . $newEntryId . "',
               Title= '" . cleaninput($title) . "',
               Teaser ='" . cleaninputtreasure($teaser) . "',
               DateCreated = '" . $datenewentry2 . "',
               timecreate ='" . $datenewentry3 . "',
               `doc-id` ='Horse',
               pageuri ='" . trim($pageuri) . "',
               typetosee  ='OTH' ";
			$ans1 = mysql_query_w($Sql_insert1);
				echo $Sql_insert1 . EOF;
//				var_dump($ans);

			$Sql_insert2 = " INSERT INTO newsarticle SET 
                     ContentIdART ='" . $newEntryId . "',
                     TitleART= '" . cleaninput($title) . "',
                     HtmlART = '" . cleaninput($bodynewentry) . "',
                     url ='" . $pageuri . "',
                     DateCreatedART ='" . $datenewentry2 . "' ";
			$ans2 = mysql_query_w($Sql_insert2);
				echo $Sql_insert2 . EOF;
//				var_dump($ans);

			if ($ans1 && $ans2) {
				echo "Status: New entry scrapped and saved to database successfully." . EOF;
			} else {
				echo "Status: There were an error while trying to inserting the new entry to database. Reported to Webmaster. Skipping.";
				$issues[] = array("pageuri" => $pageuri, "title" => $titlenewentry, "date1" => $datenewentry2, "date2" => $datenewentry3, "teaser" => $teaser, "contents" => $bodynewentry, "sql1" => $Sql_insert1, "sql2" => $Sql_insert2, "possible_reason" => "Database Problem, wrong credentials, or Database overloaded.");
			}
		} else {
			// in case of error send email
			echo "Status: Wrong scrapping, reported to Webmaster. Skipping." . EOF;
			$issues[] = array("pageuri" => $pageuri, "title" => $titlenewentry, "date1" => $datenewentry, "date2" => "", "teaser" => $teaser, "contents" => $bodynewentry, "sql1" => "None", "sql2" => "None", "possible_reason" => "Invalid data. Wrong scrapping, it is possible that sources changed.");
		}
	}
	++$cont;
}

if (count($issues)) {
	$to = "webmaster.villegas@gmail.com";
	$from = "drscrapper@usracing.com";
	$subject = "Master, there is a problem while scrapping from foxsports.com";

	$issues_msj = '<table border="1" style="border:1px solid black;">';
	foreach ($issues as $issueI) {
		$issues_msj .= '<tr><td>Reason: <td><td>' . $issueI['possible_reason'] . '</td></tr>';
		$issues_msj .= '<tr><td>Date 1: <td><td>' . $issueI['date1'] . '</td></tr>';
		$issues_msj .= '<tr><td>Date 2: <td><td>' . $issueI['date2'] . '</td></tr>';
		$issues_msj .= '<tr><td>Title: <td><td>' . $issueI['title'] . '</td></tr>';
		$issues_msj .= '<tr><td>Page Uri: <td><td>' . $issueI['pageuri'] . '</td></tr>';
		$issues_msj .= '<tr><td>Teaser: <td><td>' . $issueI['teaser'] . '</td></tr>';
		$issues_msj .= '<tr><td>contents: <td><td>' . $issueI['contents'] . '</td></tr>';
		$issues_msj .= '<tr><td>Sql 1: <td><td>' . $issueI['sql1'] . '</td></tr>';
		$issues_msj .= '<tr><td>Sql 2: <td><td>' . $issueI['sql2'] . '</td></tr>';
		$issues_msj .= '<tr><td colspan="2">-seperator-</td></tr>';
	}
	$issues_msj .= '</table>';

	$message = "<html><body bgcolor=\"#DCEEFC\">
					<b>" . date("Y-m-d H:i:s") . "</b> <br> 
					Sorry to report <font color=\"red\">ERRORS</font> Master<br> 
					<a href=\"http://allhorse.com/programs/foxnews/foxnewscronp.php\">FoxNews Scrapper</a><br /> 
					<a href=\"http://usracing.com/Feeds/cron.php\">Usracing Feed Processor</a><br />
					<br><br>" . $issues_msj . "
				</body></html>";

	$headers = "MIME-Version: 1.0rn";
	$headers .= "Content-type: text/html; charset=iso-8859-1rn";
	$headers .= "From: $from\r\n";
	//$headers .= "Cc: [email]maa@p-i-s.cXom[/email]"; 
	//$headers .= "Bcc: [email]email@maaking.cXom[/email]"; 

	mail($to, $subject, $message, $headers);

	echo "There were some problems, errors were reported to Webmaster." . EOF;
}