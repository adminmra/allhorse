<div>
  <span id="success_message" style="display:none;">
           <div style="text-align:center;">Thanks for signing up!</div>
   </span>
  <form id="contact" data-id="embedded_signup:form" class="ctct-custom-form Form" name="embedded_signup" method="POST" action="//visitor2.constantcontact.com/api/signup">
		<fieldset>
		<?php echo($sourcetitle); ?>
		<!-- The following code must be included to ensure your sign-up form works properly. -->
        <input data-id="ca:input" type="hidden" name="ca" value="408bbb5e-36c1-4bcf-8be6-bf18c202a066">
        <input data-id="list:input" type="hidden" name="list" value="1630770379">
        <input data-id="source:input" type="hidden" name="source" value="EFD">
        <input data-id="required:input" type="hidden" name="required" value="list,email">
        <input data-id="url:input" type="hidden" name="url" value="">

        <p data-id="First Name:p">
		<label data-id="First Name:label" data-name="first_name" for="name"><span class="required">*</span>First Name</label>
		<input data-id="First Name:input" type="text" name="first_name" value="" maxlength="50" id="name" class="txt" />
		</p>
		<p data-id="Email Address:p" >
			<label data-id="Email Address:label" data-name="email" for="email"><span class="ctct-form-required required">*</span> Email Address</label>
			<input data-id="Email Address:input" name="email" type="text" id="email" class="txt" value="" maxlength="80"/>
		</p>
		<button data-enabled="enabled" id="sendemail" class="button Button ctct-button Button--block Button-secondary">Register Me!</button>
		
		</fieldset>
  </form>
  </div>
  <!--<script type="text/javascript" src="http://www.allhorse.com/fancybox/form.js"></script>-->
  <script type='text/javascript'>
   var localizedErrMap = {};
   localizedErrMap['required'] = 		'This field is required.';
   localizedErrMap['ca'] = 			'An unexpected error occurred while attempting to send email.';
   localizedErrMap['email'] = 			'Please enter your email address in name@email.com format.';
   localizedErrMap['birthday'] = 		'Please enter birthday in MM/DD format.';
   localizedErrMap['anniversary'] = 	'Please enter anniversary in MM/DD/YYYY format.';
   localizedErrMap['custom_date'] = 	'Please enter this date in MM/DD/YYYY format.';
   localizedErrMap['list'] = 			'Please select at least one email list.';
   localizedErrMap['generic'] = 		'This field is invalid.';
   localizedErrMap['shared'] = 		'Sorry, we could not complete your sign-up. Please contact us to resolve this.';
   localizedErrMap['state_mismatch'] = 'Mismatched State/Province and Country.';
	localizedErrMap['state_province'] = 'Select a state/province';
   localizedErrMap['selectcountry'] = 	'Select a country';
   var postURL = 'https://visitor2.constantcontact.com/api/signup';
</script>
<script type='text/javascript' src='//static.ctctcdn.com/h/contacts-embedded-signup-assets/1.0.2/js/signup-form.js'></script>
<!--End CTCT Sign-Up Form—->
