<?php	 		 	

ini_set('display_errors', 1);
error_reporting(E_ALL);

define("EOF", "<br />\n");
define("NEWSABSPATH", "/home/ah/allhorse/public_html/programs/news");
define("NEWSPUBLICABSPATH", "/home/ah/usracing.com/htdocs/images/news");


require_once NEWSABSPATH . '/dbconnect_rw.inc';
require_once NEWSABSPATH . '/cron/commonFunctions.php';
require_once NEWSABSPATH . '/cron/simplehtmldom_1_5/simple_html_dom.php';

function cleandata($string) {
   $string = htmlentities($string, null, 'utf-8');
   $string = str_replace('&nbsp;', '', str_replace('�', '', strip_tags(trim($string))));
   //$string = mysql_real_escape_string($string);
   return $string;
}

$homepage = file_get_contents('http://www.breederscup.com/races/breeders-cup-challenge');
preg_match_all("/<table[^>]*class=\"views\\-table cols\\-6\" >(.*?)<\\/table>/si", $homepage, $match);

//print_r($match[0]);
$months['Jan'] = '01';
$months['Feb'] = '02';
$months['Mar'] = '03';
$months['Apr'] = '04';
$months['May'] = '05';
$months['Jun'] = '06';
$months['Jul'] = '07';
$months['Aug'] = '08';
$months['Sep'] = '09';
$months['Oct'] = '10';
$months['Nov'] = '11';
$months['Dec'] = '12';

$queriesArray = array();
foreach ($match[0] as $table) {
   $DOM = new DOMDocument;
   $DOM->loadHTML($table);

   //get all tr and td
   $items = $DOM->getElementsByTagName('tr');
   $tds = $DOM->getElementsByTagName('td');
   $str = "";
   print_r($tds);
   echo "<br/>";

   $processLast = false;
   for ($ii = 0; $ii < $tds->length; $ii++) {
      switch ($ii % 6) {
         case 0:
            if ($ii > 0) {
               $sql = "insert into 2011_breeders_challenge set
                  racedate = '$racedate',
                  race = '$raceName',
                  track = '$track',
                  winner = '$winner' ";
               $queriesArray[] = $sql;
            }
            $date = $tds->item($ii)->nodeValue;
            $datedata = explode(' ', strip_tags(trim($date)));
            $month = $datedata[0];
            $racedate = date("Y") . "-" . $months[$month] . "-" . ($datedata[1]<10?"0":"") . $datedata[1];
            break;
         case 1:
            $raceName = $tds->item($ii)->nodeValue;
            $raceName = cleandata(trim($raceName));
            break;
         case 2:
            $track = $tds->item($ii)->nodeValue;
            $track = cleandata(trim($track));
            break;
         case 3:
            $division = $tds->item($ii)->nodeValue;
            $division = cleandata(trim($division));
            break;
         case 4:
            //replay image
            break;
         case 5:
            $winner = $tds->item($ii)->nodeValue;
            $winner = cleandata(trim($winner));
            $processLast = true;
            break;
      }
   }
   if ($processLast) {
      $sql = "insert into 2011_breeders_challenge set
         racedate = '$racedate',
         race = '$raceName',
         track = '$track',
         winner = '$winner' ";
      $queriesArray[] = $sql;
   }
   
}

if(count($queriesArray)>0){
   mysql_query_w('truncate table 2011_breeders_challenge');
   foreach($queriesArray as $queryI){
      echo "$queryI<br />\n";
      mysql_query_w($queryI);
   }
}

exit;
## set output file to:/home/ah/allhorse/public_html/breeders/challenge_scraped.php