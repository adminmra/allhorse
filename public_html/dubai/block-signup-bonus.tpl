<section class="card">
    <div class="card_wrap">
        <div class="card_half card_content">
            <a href="/promos/cash-bonus-10">
                {* <img src="/img/index-kd/icon-kentucky-derby-betting.png" alt="Bet on Kentucky Derby"
                    class="card_icon"> *}
                <img class="card_icon" src="/img/dubai-world-cup/dubai-countdown-icon.png">
            </a>
            <h2 class="card_heading">Get a Sign Up Bonus now!</h2>
            <p>Make the most out of your first deposit with an outstanding welcome bonus up to $500 cash!
<br><br>But wait, there’s even more…
If your deposit is of $100 or more, you also qualify to receive an additional $150!
<br><br>Start winning at <a href="/signup?ref={$ref}">BUSR</a> now!</p>
<p>        <a href="https://www.betusracing.ag/promos/20-cash-bonus" class="card_btn">More Info</a>
            </p>
        </div>
        <div class="card_half card_hide-mobile">
            <a href="/promos/cash-bonus-20"><img src="/img/dubai-world-cup/signup-offer.jpg" class="card_img" alt='Dubai World Cup Signup Offer '></a>
        </div>
    </div>
</section>