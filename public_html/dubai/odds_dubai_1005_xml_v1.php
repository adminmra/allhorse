	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2020 Dubai World Cup Odds"
	  }
	</script>
	{/literal}      <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" summary="The latest odds for the Dubai World Cup available for wagering now at  BUSR.">
            <caption>2020 Dubai World Cup Odds</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated February 27, 2020.</em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
    <tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td data-title=' '>Cross Counter</td><td data-title='Fractional'>9/4</td><td data-title='American'>+225</td></tr><tr><td data-title=' '>Dee Ex Bee</td><td data-title='Fractional'>9/2</td><td data-title='American'>+450</td></tr><tr><td data-title=' '>Kew Gardens</td><td data-title='Fractional'>4/1</td><td data-title='American'>+400</td></tr><tr><td data-title=' '>Ispolini</td><td data-title='Fractional'>9/2</td><td data-title='American'>+450</td></tr><tr><td data-title=' '>Prince Of Arran</td><td data-title='Fractional'>14/1</td><td data-title='American'>+1400</td></tr><tr><td data-title=' '>Call The Wind</td><td data-title='Fractional'>7/1</td><td data-title='American'>+700</td></tr><tr><td data-title=' '>Dubhe</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Secret Advisor</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Almond Eye</td><td data-title='Fractional'>3/2</td><td data-title='American'>+150</td></tr><tr><td data-title=' '>Benbatl</td><td data-title='Fractional'>5/1</td><td data-title='American'>+500</td></tr><tr><td data-title=' '>Beauty Generation</td><td data-title='Fractional'>8/1</td><td data-title='American'>+800</td></tr><tr><td data-title=' '>Dream Castle</td><td data-title='Fractional'>14/1</td><td data-title='American'>+1400</td></tr><tr><td data-title=' '>Without Parole</td><td data-title='Fractional'>14/1</td><td data-title='American'>+1400</td></tr><tr><td data-title=' '>Deirdre</td><td data-title='Fractional'>10/1</td><td data-title='American'>+1000</td></tr><tr><td data-title=' '>Sistercharlie</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>Got Stormy</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>Lord Glitters</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Almond Eye</td><td data-title='Fractional'>5/1</td><td data-title='American'>+500</td></tr><tr><td data-title=' '>Ghaiyyath</td><td data-title='Fractional'>4/1</td><td data-title='American'>+400</td></tr><tr><td data-title=' '>Old Persian</td><td data-title='Fractional'>7/1</td><td data-title='American'>+700</td></tr><tr><td data-title=' '>Japan</td><td data-title='Fractional'>9/1</td><td data-title='American'>+900</td></tr><tr><td data-title=' '>Sottsass</td><td data-title='Fractional'>10/1</td><td data-title='American'>+1000</td></tr><tr><td data-title=' '>Anthony Van Dyck</td><td data-title='Fractional'>10/1</td><td data-title='American'>+1000</td></tr><tr><td data-title=' '>Curren Bouquetd'or</td><td data-title='Fractional'>11/1</td><td data-title='American'>+1100</td></tr><tr><td data-title=' '>Loves Only You</td><td data-title='Fractional'>12/1</td><td data-title='American'>+1200</td></tr><tr><td data-title=' '>Cross Counter</td><td data-title='Fractional'>14/1</td><td data-title='American'>+1400</td></tr><tr><td data-title=' '>Exultant</td><td data-title='Fractional'>16/1</td><td data-title='American'>+1600</td></tr><tr><td data-title=' '>Square De Luynes</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>North America</td><td data-title='Fractional'>12/1</td><td data-title='American'>+1200</td></tr><tr><td data-title=' '>Mckinzie</td><td data-title='Fractional'>7/2</td><td data-title='American'>+350</td></tr><tr><td data-title=' '>Maximum Security</td><td data-title='Fractional'>4/1</td><td data-title='American'>+400</td></tr><tr><td data-title=' '>Seeking The Soul</td><td data-title='Fractional'>33/1</td><td data-title='American'>+3300</td></tr><tr><td data-title=' '>Game Winner</td><td data-title='Fractional'>12/1</td><td data-title='American'>+1200</td></tr><tr><td data-title=' '>Tacitus</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>New Trails</td><td data-title='Fractional'>66/1</td><td data-title='American'>+6600</td></tr><tr><td data-title=' '>Midnight Bisou</td><td data-title='Fractional'>6/1</td><td data-title='American'>+600</td></tr><tr><td data-title=' '>Gronkowski</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>Roadster</td><td data-title='Fractional'>20/1</td><td data-title='American'>+2000</td></tr><tr><td data-title=' '>Gunnevera</td><td data-title='Fractional'>25/1</td><td data-title='American'>+2500</td></tr><tr><td data-title=' '>Behavioral Bias</td><td data-title='Fractional'>66/1</td><td data-title='American'>+6600</td></tr>            </tbody>
        </table>
    </div>
	{literal}
        <style>
            table.ordenable th{cursor: pointer}
			table.ordenable td{ word-wrap: break-word !important; word-break: break-all !important;}
			table.ordenable{ max-width: 100% !important; }
        </style>
        <script src="/assets/js/jquery.sortElements.js"></script>
        <script src="/assets/js/sorttable.js"></script>
	{/literal}
    
	 