<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "name": "Dubai World Cup 2020 Contenders",
        "keywords": "Dubai World Cup Contenders, Dubai World Cup, Dubai World cup odds, Dubai World cup contenders, Meydan Racecourse, horse racing, bet Dubai World Cup, Dubai 2020"
    }
</script>
<style>
    @media (max-width: 800px) {
        caption {
            display: none;
        }

    }
</style>
<div id="no-more-tables">

    <table id="sortTable" border="0" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered ordenable"  title="Saudi World Cup Odds" summary="The latest odds for Dubai World Cup.  Only available at BUSR.">
        <caption>Dubai World Cup Contenders</caption>
        <thead>
            <tr>
                <th> Horse </th>
                <th> Trainer </th>
                <th> Breeder </th>
                <th> Status </th>
                 </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> Tacitus </td>
                <td data-title="Trainer" style="word-break: keep-all !important;"> W. Mott </td>
                <td data-title="Breeder" style="word-break: keep-all !important;"> Kentucky (USA) by Juddmonte Farms, Inc.</td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
                </tr>
            	 <tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> Mucho Gusto </td>
                <td data-title="Trainer" style="word-break: keep-all !important;"> B. Baffert </td>
                <td data-title="Breeder" style="word-break: keep-all !important;"> Teneri Farm Inc. & Bernardo Alvarez Calderon </td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
                </tr>
			 <tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> Gronkowski </td>
                <td data-title="Trainer" style="word-break: keep-all !important;"> S. Ghadayer </td>
                <td data-title="Breeder" style="word-break: keep-all !important;"> Epic Thoroughbreds LLC </td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
                </tr>
			 <tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> North America </td>
                <td data-title="Trainer" style="word-break: keep-all !important;"> Satish Seemar </td>
                <td data-title="Breeder" style="word-break: keep-all !important;"> Qatar Bloodstock Ltd. </td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
                </tr>
			 <tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> Benbatl </td>
                <td data-title="Trainer" style="word-break: keep-all !important;"> Salem bin Ghadayer </td>
                <td data-title="Breeder" style="word-break: keep-all !important;"> Hidden Point Farm Inc </td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
                </tr>
			 <tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> Axelrod </td>
                <td data-title="Trainer" style="word-break: keep-all !important;"> Saeed bin Suroor </td>
                <td data-title="Breeder" style="word-break: keep-all !important;"> Darley </td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
               </tr>
			 <tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> Sir Winston </td>
                <td data-title="Trainer" style="word-break: keep-all !important;"> Mark E. Casse </td>
                <td data-title="Breeder" style="word-break: keep-all !important;"> Tracy Farmer </td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
                </tr>
				 <tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> Capezzano </td>
                <td data-title="Trainer" style="word-break: keep-all !important;"> Salem bin Ghadayer </td>
                <td data-title="Breeder" style="word-break: keep-all !important;"> Darley </td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
                </tr>
			<tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> Parsimony </td>
                <td data-title="Trainer" style="word-break: keep-all !important;">  Doug F. O'Neill </td>
                <td data-title="Breeder" style="word-break: keep-all !important;"> Pennsylvania (USA) by Maria Montez Haire  </td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
                </tr>
			 <tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> War Story </td>
                <td data-title="Trainer" style="word-break: keep-all !important;"> Elizabeth L. Dobles </td>
                <td data-title="Breeder" style="word-break: keep-all !important;"> Kentucky (USA) by Jack Swain III </td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
                </tr>
			 <tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> Chrysoberyl </td>
                <td data-title="Trainer" style="word-break: keep-all !important;"> Hidetaka Otonashi </td>
                <td data-title="Breeder" style="word-break: keep-all !important;"> Northern Farm  </td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
               </tr>
				 <tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> Master Fencer </td>
                <td data-title="Trainer" style="word-break: keep-all !important;"> K. Tsunoda </td>
                <td data-title="Breeder" style="word-break: keep-all !important;">  Katsumi Yoshizawa  </td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
                </tr>
			 <tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> Matterhorn </td>
                <td data-title="Trainer" style="word-break: keep-all !important;"> S Ghadayer </td>
                <td data-title="Breeder" style="word-break: keep-all !important;"> (IRL) by Barronstown Stud  </td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
                </tr>
			 <tr>
                <td data-title="Horse" style="word-break: keep-all !important;"> Military Law </td>
                <td data-title="Trainer" style="word-break: keep-all !important;"> Musabbeh Al Mheiri </td>
                <td data-title="Breeder" style="word-break: keep-all !important;"> (GB) by Qatar Bloodstock Ltd.  </td>
                <td data-title="Status" style="word-break: keep-all !important;"> Possible </td>
               </tr>
        </tbody>
    </table>
</div>
<script src="/assets/js/jquery.sortElements.js"></script>
        <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
        <script src="/assets/js/sorttable-preprace.js"></script> 