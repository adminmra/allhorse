 <div id="no-more-tables">
<table border="0" cellspacing="0" cellpadding="0"  class="table table-condensed table-striped table-bordered ordenableResult" width="100%"  title="Dubai World Cup Results" summary="Dubai World Cup Results">
  <thead>
    <tr>
      <th>Fin</th>
      <th>PP</th>
      <th>Horse</th>
      <th>Trainer</th><th>Jockey</th>
    </tr>
    </thead>
    <tbody>
    <tr>
      <td data-title="Finish" >1</td>
      <td data-title="PP" >10</td>
      <td data-title="Horse">Mystic Guide</td>
<td data-title="Trainer">S. Suroor</td>
<td data-title="Jockey">C. Soumillon</td>
    </tr>
        <tr>
      <td data-title="Finish" >2</td>
      <td data-title="PP" >3</td>
      <td data-title="Horse">Chuwa Wizard</td>
<td data-title="Trainer">Shinobu Nakanishi</td>
<td data-title="Jockey">Ryuji Okubo</td>
    </tr>
    <tr>
      <td data-title="Finish">3</td>
      <td data-title="PP">8</td>
      <td data-title="Horse">Magny Cours</td>
<td data-title="Trainer">Andre Fabre</td>
<td data-title="Jockey">Godolphin</td>
    </tr>
    <tr>
      
   


<!--

    <tr>
      <td data-title="Finish" >&nbsp;</td>
      <td data-title="PP" >11</td>
      <td data-title="Horse">K T Brave</td>
<td data-title="Trainer">H. Sugiyama</td>
<td data-title="Jockey">J. Moreira</td>
    </tr> -->

  </tbody>
</table>
</div>
{literal}
    <style type="text/css">
        table.ordenableResult th{cursor: pointer}
    </style>
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_results.js"></script>
{/literal}