<section class="testimonials--blue usr-section">
  <div class="container">
    <blockquote class="testimonial-blue" style="display: flex; justify-content: center;">
      <div class="testimonial-blue_content">
        <p class="testimonial-blue_p">We trust US Racing to provide accurate and thoughtful coverage of racing. They are the best source for horse racing odds.</p>
        <span class="testimonial-blue_name">
          - PACE ADVANTAGE<br>
        </span>
      </div>
    </blockquote>
  </div>
</section>