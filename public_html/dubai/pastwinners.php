 <div id="no-more-tables">
<table id="sortTable" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" summary="Winners of the Dubai World Cup."> 
  <thead>
    <tr>
      <th width="70px">Year</th>
      <th>Winner </th>
      <th width="70px">Age</th>
      <th>Jockey </th>
      <th>Trainer </th>
      <th>Owner </th>
      <th width="77px">Time </th>
    </tr>
  </thead>
  <tbody>
  <tr>
      <td data-title="Year">2021</td>
      <td data-title="Winner">Mystic Guide</td>
      <td data-title="Age">5</td>
      <td data-title="Jockey">Luis Saez</td>
      <td data-title="Trainer">Michael Stidham</td>
      <td data-title="Owner">Godolphin</td>
      <td data-title="Time">2:01.62 </td>
    </tr>
    <tr>
      <td data-title="Year">2019</td>
      <td data-title="Winner">Thunder Snow</td>
      <td data-title="Age">5</td>
      <td data-title="Jockey">Christophe Soumillon</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Owner">Godolphin</td>
      <td data-title="Time">2:03.87 </td>
    </tr>
    <tr>
      <td data-title="Year">2018</td>
      <td data-title="Winner">Thunder Snow</td>
      <td data-title="Age">4</td>
      <td data-title="Jockey">Christophe Soumillon</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Owner">Godolphin</td>
      <td data-title="Time">2:01.38 </td>
    </tr>
    <tr>
      <td data-title="Year">2017</td>
      <td data-title="Winner">Arrogate</td>
      <td data-title="Age">4</td>
      <td data-title="Jockey">Mike E. Smith</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Juddmonte Farms</td>
      <td data-title="Time">2:02.53 </td>
    </tr>
    <tr>
      <td data-title="Year">2016</td>
      <td data-title="Winner">California Chrome</td>
      <td data-title="Age">5</td>
      <td data-title="Jockey">Victor Espinoza</td>
      <td data-title="Trainer">Art Sherman</td>
      <td data-title="Owner">California Chrome LLC </td>
      <td data-title="Time">2:01.83 </td>
    </tr>
    <tr>
      <td data-title="Year">2015</td>
      <td data-title="Winner">Prince Bishop</td>
      <td data-title="Age">8</td>
      <td data-title="Jockey">William Buick</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Owner">Hamdan bin Mohammed Al Maktoum</td>
      <td data-title="Time">2:03.24 </td>
    </tr>
    <tr>
      <td data-title="Year">2014</td>
      <td data-title="Winner">African Story</td>
      <td data-title="Age">7</td>
      <td data-title="Jockey">Silvestre de Sousa</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Owner">Godolphin Racing</td>
      <td data-title="Time">2:01.61 </td>
    </tr>
    <tr>
      <td data-title="Year">2013</td>
      <td data-title="Winner">Animal Kingdom</td>
      <td data-title="Age">5</td>
      <td data-title="Jockey">Joel Rosario</td>
      <td data-title="Trainer">Graham Motion</td>
      <td data-title="Owner">Arrowfield Stud &amp; Team Valor</td>
      <td data-title="Time">2:03.21 </td>
    </tr>
    <tr>
      <td data-title="Year">2012</td>
      <td data-title="Winner">Monterosso</td>
      <td data-title="Age">5</td>
      <td data-title="Jockey">Mickael Barzalona</td>
      <td data-title="Trainer">Mahmood al Zarooni</td>
      <td data-title="Owner">Godolphin Racing</td>
      <td data-title="Time">2:02.67 </td>
    </tr>
    <tr>
      <td data-title="Year">2011</td>
      <td data-title="Winner">Victoire Pisa</td>
      <td data-title="Age">4</td>
      <td data-title="Jockey">Mirco Demuro</td>
      <td data-title="Trainer">Katsuhiko Sumii</td>
      <td data-title="Owner">Yoshimi Ichikawa </td>
      <td data-title="Time">2:05.94 </td>
    </tr>
    <tr>
      <td data-title="Year">2010</td>
      <td data-title="Winner">Glória de Campeão</td>
      <td data-title="Age">7</td>
      <td data-title="Jockey">T. J. Pereira</td>
      <td data-title="Trainer">Pascal Bary</td>
      <td data-title="Owner">Stud Estrela Energia </td>
      <td data-title="Time">2:03.83 </td>
    </tr>
    <tr>
      <td data-title="Year">2009</td>
      <td data-title="Winner">Well Armed</td>
      <td data-title="Age">6</td>
      <td data-title="Jockey">Aaron Gryder</td>
      <td data-title="Trainer">Eoin G. Harty</td>
      <td data-title="Owner">WinStar Farm LLC</td>
      <td data-title="Time">2:01.01 </td>
    </tr>
    <tr>
      <td data-title="Year">2008</td>
      <td data-title="Winner">Curlin</td>
      <td data-title="Age">4</td>
      <td data-title="Jockey">Robby Albarado</td>
      <td data-title="Trainer">Steve Asmussen</td>
      <td data-title="Owner">Stonestreet Stables/Midnight Cry Stbl </td>
      <td data-title="Time">2:00.15 </td>
    </tr>
    <tr>
      <td data-title="Year">2007</td>
      <td data-title="Winner">Invasor</td>
      <td data-title="Age">5</td>
      <td data-title="Jockey">Fernando Jara</td>
      <td data-title="Trainer">Kiaran McLaughlin</td>
      <td data-title="Owner">Hamdan Al Maktoum</td>
      <td data-title="Time">1:59:97 </td>
    </tr>
    <tr>
      <td data-title="Year">2006</td>
      <td data-title="Winner">Electrocutionist</td>
      <td data-title="Age">5</td>
      <td data-title="Jockey">Frankie Dettori</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Owner">Godolphin Racing</td>
      <td data-title="Time">2:01.32 </td>
    </tr>
    <tr>
      <td data-title="Year">2005</td>
      <td data-title="Winner">Roses in May</td>
      <td data-title="Age">5</td>
      <td data-title="Jockey">John Velazquez</td>
      <td data-title="Trainer">Dale L. Romans</td>
      <td data-title="Owner">Ken &amp; Sarah Ramsey</td>
      <td data-title="Time">2:02.17 </td>
    </tr>
    <tr>
      <td data-title="Year">2004</td>
      <td data-title="Winner">Pleasantly Perfect</td>
      <td data-title="Age">6</td>
      <td data-title="Jockey">Alex Solis</td>
      <td data-title="Trainer">Richard Mandella</td>
      <td data-title="Owner">Diamond A Racing Corp. </td>
      <td data-title="Time">2:00.24 </td>
    </tr>
    <tr>
      <td data-title="Year">2003</td>
      <td data-title="Winner">Moon Ballad</td>
      <td data-title="Age">4</td>
      <td data-title="Jockey">Frankie Dettori</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Owner">Godolphin Racing</td>
      <td data-title="Time">2:00.48 </td>
    </tr>
    <tr>
      <td data-title="Year">2002</td>
      <td data-title="Winner">Street Cry</td>
      <td data-title="Age">4</td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Owner">Godolphin Racing</td>
      <td data-title="Time">2:01.18 </td>
    </tr>
    <tr>
      <td data-title="Year">2001</td>
      <td data-title="Winner">Captain Steve</td>
      <td data-title="Age">4</td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Michael E. Pegram</td>
      <td data-title="Time">2:00.47 </td>
    </tr>
    <tr>
      <td data-title="Year">2000</td>
      <td data-title="Winner">Dubai Millennium</td>
      <td data-title="Age">4</td>
      <td data-title="Jockey">Frankie Dettori</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Owner">Godolphin Racing</td>
      <td data-title="Time">1:59.50</td>
    </tr>
    <tr>
      <td data-title="Year">1999</td>
      <td data-title="Winner">Almutawakel</td>
      <td data-title="Age">4</td>
      <td data-title="Jockey">Richard Hills</td>
      <td data-title="Trainer">Saeed bin Suroor</td>
      <td data-title="Owner">Hamdan Al Maktoum</td>
      <td data-title="Time">2:00.65 </td>
    </tr>
    <tr>
      <td data-title="Year">1998</td>
      <td data-title="Winner">Silver Charm</td>
      <td data-title="Age">4</td>
      <td data-title="Jockey">Gary Stevens</td>
      <td data-title="Trainer">Bob Baffert</td>
      <td data-title="Owner">Bob &amp; Beverly Lewis</td>
      <td data-title="Time">2:04.29 </td>
    </tr>
    <tr>
      <td data-title="Year">1997</td>
      <td data-title="Winner">Singspiel</td>
      <td data-title="Age">5</td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">Michael Stoute</td>
      <td data-title="Owner">Sheikh Mohammed</td>
      <td data-title="Time">2:01.91 </td>
    </tr>
    <tr>
      <td data-title="Year">1996</td>
      <td data-title="Winner">Cigar</td>
      <td data-title="Age">6</td>
      <td data-title="Jockey">Jerry Bailey</td>
      <td data-title="Trainer">William I. Mott</td>
      <td data-title="Owner">Allen E. Paulson</td>
      <td data-title="Time">2:03.84 </td>
    </tr>
  </tbody>
</table>
</div>
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable-winners.js"></script>
{/literal}