<!-- Used for week of the Dubai WC  -->



{if $ShowDubaiSalesblurb}

The Dubai World Cup is here! What a better way to enjoy the great event then with BUSR. 
When you sign up you receive up to $500 cash bonus when you use the promo code <strong>DUBAI</strong>. Your first deposit gives you up to $500 cash bonus. Take advantage of this great offer for the Dubai World Cup along 
with all the other great horse racing action throughout the year. Who do you think will win the Dubai World Cup? 
Place your bet now! The action doesn't stop with BUSR. Join today so you can place your 
wager on the Dubai World Cup!<br>

{else}

<p>Find the best <b>Dubai World Cup Betting Odds</b> at <a href="/signup?ref={$ref2}">BUSR</a>. Once known as the richest horse racing event in the world, the 25th edition of this sensational event is set to take place at <b>Meydan Racecourse</b> in Dubai, United Arab Emirates, on March 27th, 2021.

<p><a href="/signup?ref={$ref3}">Sign up</a> with <a href="/signup?ref={$ref4}">BUSR</a> and receive up to $500 in cash bonus. Check the current <b>Dubai World Cup Betting Odds</b> and get your action on this and many more horse racing events!</p>
<p><a href="/signup?ref={$ref5}">Join today</a> and grab your bonus, time’s running out!</p>

{/if}





