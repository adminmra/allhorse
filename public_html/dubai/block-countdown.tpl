<section class="countdown-section sm">
  <div class="countdown-top-block">
    <div class="countdown-icon icon-left">
      <img src="/img/dubai-world-cup/dubai-countdown-icon.png">
    </div>

    <div class="countdown-text">
      <span>Countdown to the {include_php file="/home/ah/allhorse/public_html/dubai/running.php"} Dubai World Cup </span>
    </div>

    <div class="countdown-icon icon-left">
      <img src="/img/dubai-world-cup/dubai-countdown-icon.png" style="transform: scaleX(-1);">
    </div>
  </div>

  <div class="countdown-bottom-block">
    <div class="countdown-icon icon-left">
      <img src="/img/dubai-world-cup/dubai-countdown-icon.png">
    </div>

    <div class="usr-countdown">
      <div class="timer">
        <span id="cd-days" class="quantity"></span>
        <span class="text">Days</span>
      </div>
      <div class="timer">
        <span id="cd-hours" class="quantity"></span>
        <span class="text">Hours</span>
      </div>
      <div class="timer">
        <span id="cd-minutes" class="quantity"></span>
        <span class="text">Minutes</span>
      </div>
      <div class="timer last">
        <span id="cd-seconds" class="quantity"></span>
        <span class="text">Seconds</span>
      </div>
    </div>

    <div class="countdown-icon icon-right">
      <img src="/img/dubai-world-cup/dubai-countdown-icon.png" style="transform: scaleX(-1);">
    </div>
  </div>

</section>

{literal}
    <script>
        var countDownDate = new Date("March 26, 2022 12:50:00").getTime();
    </script>
    <script src="/assets/plugins/countdown/countdown.js"></script>    
{/literal}