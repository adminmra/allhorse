<?php
$cdate = date("Y-m-d H:i:s");
if ($cdate > "2019-03-30 23:59:00" && $cdate < "2020-03-28 23:59:00") {
    echo "March 28th, 2020";
} else if ($cdate > "2020-03-28 23:59:00" && $cdate < "2021-03-26 23:59:00") {
    echo "March 27th, 2021";
} else if ($cdate > "2021-03-26 23:59:00" && $cdate < "2022-03-26 23:59:00") {
    echo "March 26th, 2022";
} else if ($cdate > "2022-03-26 23:59:00" && $cdate < "2023-03-25 23:59:00") { 
    echo "March 25th, 2023";
} else if ($cdate > "2023-03-25 23:59:00" && $cdate < "2024-03-30 23:59:00") {
    echo "March 30th, 2024";
} else if ($cdate > "2024-03-27 23:59:00" && $cdate < "2025-03-29 23:59:00") {
    echo "March 29th, 2025";
}
?>