<section class="testimonials--blue usr-section">
  <div class="container">
    <blockquote class="testimonial-blue">
      <figure class="testimonial-blue_figure"><img src="/img/index-pwc/pace-advantage.png" alt="Pace Advantage"
          class="testimonial-blue_img" style="background-color: transparent; width: 120px; height: auto;"></figure>
      <div class="testimonial-blue_content">
        <p class="testimonial-blue_p">We trust US Racing to provide accurate and thoughtful coverage of racing. They are the best source for horse racing odds.</p>
        {* <p class="testimonial-blue_p">For horse racing and blackjack I play at BUSR</p> *}
        <span class="testimonial-blue_name">
          - PACE ADVANTAGE <br>
          {* - Fred Faour - ESPN Radio Host - Houston 97.5FM<br> *}
        </span>
      </div>
    </blockquote>
  </div>
</section>