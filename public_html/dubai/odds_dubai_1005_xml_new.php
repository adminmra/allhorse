    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0"  summary="Horses - Dubai Cup">
            <caption> Dubai World Cup Odds  </caption>
            <tfoot>
              <tr>
                <td class="dateUpdated center" colspan="3">
                  <em id='updateemp'>  - Updated May 3, 2021 14:30:08 </em> <!-- Bet US Racing - Official
                  <a href="//www.usracing.com/odds/us-presidential-election">US Presidential Election Odds</a>. -->
                  <!-- <a href="//www.usracing.com/odds/us-presidential-election">US Presidential Odds</a>, -->
                </td>
              </tr>
            </tfoot>
            <tbody>
               <!-- <tr>
                    <th colspan="3" class="center">
                                        </th>
            </tr> -->
    <tr><th></th><th>Fractional</th><th>American</th></tr><tr><td>Mystic Guide </td><td>9/4</td><td>+225</td></tr><tr><td>Military Law </td><td>4/1</td><td>+400</td></tr><tr><td>Jesus’ Team </td><td>5/1</td><td>+500</td></tr><tr><td>Salute The Soldier  </td><td>7/1</td><td>+700</td></tr><tr><td>Magny Cours   </td><td>10/1</td><td>+1000</td></tr><tr><td>Sleepy Eyes Todd</td><td>12/1</td><td>+1200</td></tr><tr><td>Hypothetical   </td><td>9/1</td><td>+900</td></tr><tr><td>Chuwa Wizard </td><td>12/1</td><td>+1200</td></tr><tr><td>Great Scot    </td><td>14/1</td><td>+1400</td></tr><tr><td>Title Ready    </td><td>22/1</td><td>+2200</td></tr><tr><td>Ajuste Fiscal  </td><td>30/1</td><td>+3000</td></tr><tr><td>Gifts Of Gold</td><td>33/1</td><td>+3300</td></tr><tr><td>Capezzano   </td><td>40/1</td><td>+4000</td></tr><tr><td>Thegreatcollection</td><td>33/1</td><td>+3300</td></tr>            </tbody>
        </table>
    </div>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    