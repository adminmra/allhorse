	{literal}
	<script type="application/ld+json">
	  {
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2020 Dubai World Cup Odds"
	  }
	</script>
	{/literal}   
    <div>
        <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Dubai World Cup Odds" summary="The latest odds for Dubai World Cup.  Only available at Bet US Racing.">
            <caption>2020 Dubai World Cup Odds</caption>
            <tbody>
               <!--
 <tr>
                    <th colspan="3" class="center">
                    Horses - Dubai Cup  - Mar 28                    </th>
            </tr>
-->
    <tr class='head_title'><th><!--Team--></th><th>Fractional</th><th>American</th></tr><tr><td>Cross Counter</td><td>3/1</td><td>+300</td></tr><tr><td>Dee Ex Bee</td><td>8/1</td><td>+800</td></tr><tr><td>Kew Gardens</td><td>4/1</td><td>+400</td></tr><tr><td>Ispolini</td><td>9/2</td><td>+450</td></tr><tr><td>Prince Of Arran</td><td>14/1</td><td>+1400</td></tr><tr><td>Call The Wind</td><td>4/1</td><td>+400</td></tr><tr><td>Dubhe</td><td>16/1</td><td>+1600</td></tr><tr><td>Secret Advisor</td><td>16/1</td><td>+1600</td></tr><tr><td>Spanish Mission</td><td>20/1</td><td>+2000</td></tr><tr><td>Almond Eye</td><td>5/1</td><td>+500</td></tr><tr><td>Glory Vase</td><td>6/1</td><td>+600</td></tr><tr><td>Ghaiyyath</td><td>2/1</td><td>+200</td></tr><tr><td>Old Persian</td><td>7/1</td><td>+700</td></tr><tr><td>United</td><td>8/1</td><td>+800</td></tr><tr><td>Defoe</td><td>8/1</td><td>+800</td></tr><tr><td>Sottsass</td><td>10/1</td><td>+1000</td></tr><tr><td>Anthony Van Dyck</td><td>10/1</td><td>+1000</td></tr><tr><td>Curren Bouquetd'or</td><td>14/1</td><td>+1400</td></tr><tr><td>Loves Only You</td><td>12/1</td><td>+1200</td></tr><tr><td>Cross Counter</td><td>25/1</td><td>+2500</td></tr><tr><td>Loxley</td><td>9/1</td><td>+900</td></tr><tr><td>Square De Luynes</td><td>20/1</td><td>+2000</td></tr><tr><td>Spanish Mission</td><td>33/1</td><td>+3300</td></tr>            </tbody>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3"
                        <em id='updateemp'>  - Updated November 18, 2020 02:18:17 </em>
                        <!-- br>All odds are fixed odds prices. -->
                    </td>
                </tr> 
            </tfoot>
        </table>
    </div>
    {literal}
    <style>
        table.ordenable th{cursor: pointer}
    </style>
    <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
    <script src="//www.usracing.com/assets/js/sorttable.js"></script>
    {/literal}
        