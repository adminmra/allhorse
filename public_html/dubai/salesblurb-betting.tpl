
<p><strong>BUSR</strong> is the leading site where you can bet on the<a href="/signup?ref={$ref}">Dubai World Cup</a>.<br>
The <a href="/signup?ref={$ref}"}>Dubai World Cup</a>, sponsored by Emirates, stands alone at the summit of international horse racing. As horse racing entered a new era with the changeover of centuries, it was the <a href="/signup?ref={$ref}"}>Dubai World Cup</a>, inaugurated in 1996, that paved the way forward.</p>

<p>With the United Arab Emirates and the Middle East, in general, hosting what is now the centerpiece of international racing and recognized as the thoroughbred "World Championship" the <a href="/signup?ref={$ref}"}>Dubai World Cup</a> represents the wheel turning the full circle. </p>

<p>Every thoroughbred in the world today descends from the three Arabian stallions exported from this part of the world - the Darley Arabian, the Byerley Turk and the Godolphin Arabian.</p>