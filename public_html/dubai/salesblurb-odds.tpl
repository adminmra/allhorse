<!-- Used for week of the Dubai WC  -->

{if $ShowDubaiSalesblurb}



The Dubai World Cup is here! What a better way to enjoy the great event then with BUSR. 

When you sign up you receive up to $500 cash bonus when you use the promo code <strong>DUBAI</strong>. Your first deposit gives you up to $500 cash bonus. Take advantage of this great offer for the Dubai World Cup along 

with all the other great horse racing action throughout the year. Who do you think will win the Dubai World Cup? 

Place your bet now! The action doesn't stop with BUSR. Join today so you can place your 

wager on the Dubai World Cup!<br>



{else}

<p>The most remarkable sporting and social event in the UAE will take place on Saturday 28 March 2020, at the Meydan Racecourse in an astonishing 7.5 million m2 complex at Nad Al Sheba, in the dazzling city of Dubai.</p>
<p>Presenting six Grade 1 races and three Grade 2, one of the world's most renowned horse racing events, first introduced in 1996 by Sheikh Mohammed, is celebrating its 25th anniversary. The activity is sponsored by Emirates Airlines with a generous $12 million purse awaiting its competitors.</p>
<p>The event has it all, food, outstanding hospitality and entertainment for all ages including a kid Zone to allow parents for a worry-free experience. An extravagant after-party featuring on-stage musicians and plenty of surprises will give the attendees a taste of Dubai's modernity. </p>
{/if}


