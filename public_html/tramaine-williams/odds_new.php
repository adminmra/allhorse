    <div>
        <table class="data table table-condensed table-striped table-bordered order1" width="100%" cellpadding="0"
            cellspacing="0" border="0" summary="Tramaine Williams Fight">
            <caption>SUPER BANTAM - 10 ROUNDS</caption>
            <tbody>
                <tr>
                    <th>Team</th>
                    <th>Fractional</th>
                    <th>American</th>
                </tr>
                <tr>
                    <td>Angelo Leo</td>
                    <td>11/10</td>
                    <td>+110</td>
                </tr>
                <tr>
                    <td>Tramaine Williams</td>
                    <td>4/6</td>
                    <td>-150</td>
                </tr>
            </tbody>
        </table>
        <table class="data table table-condensed table-striped table-bordered order1" width="100%" cellpadding="0"
            cellspacing="0" border="0" summary="Tramaine Williams Fight">
            <caption>Leo vs Williams - FINAL RESULT</caption>
            <tbody>
                <tr>
                    <th>Team</th>
                    <th>Fractional</th>
                    <th>American</th>
                </tr>
                <tr>
                    <td>Angelo Leo Ko, Tko Or Dq</td>
                    <td>11/1</td>
                    <td>+1100</td>
                </tr>
                <tr>
                    <td>Angelo Leo By Decision</td>
                    <td>6/4</td>
                    <td>+150</td>
                </tr>
                <tr>
                    <td>Tramaine Williams By Ko, Tko Or Dq</td>
                    <td>9/1</td>
                    <td>+900</td>
                </tr>
                <tr>
                    <td>Tramaine Williams By Decision</td>
                    <td>20/21</td>
                    <td>-105</td>
                </tr>
                <tr>
                    <td>Draw</td>
                    <td>14/1</td>
                    <td>+1400</td>
                </tr>
            </tbody>
        </table>
        <table class="data table table-condensed table-striped table-bordered order1" width="100%" cellpadding="0"
            cellspacing="0" border="0" summary="Tramaine Williams Fight">
            <caption>Leo vs Williams - ROUND BETTING</caption>
            <tbody>
                <tr>
                    <th>Team</th>
                    <th>Fractional</th>
                    <th>American</th>
                </tr>
                <tr>
                    <td>Angelo Leo Wins In Round 1</td>
                    <td>80/1</td>
                    <td>+8000</td>
                </tr>
                <tr>
                    <td>Angelo Leo Wins In Round 2</td>
                    <td>66/1</td>
                    <td>+6600</td>
                </tr>
                <tr>
                    <td>Angelo Leo Wins In Round 3</td>
                    <td>50/1</td>
                    <td>+5000</td>
                </tr>
                <tr>
                    <td>Angelo Leo Wins In Round 4</td>
                    <td>50/1</td>
                    <td>+5000</td>
                </tr>
                <tr>
                    <td>Angelo Leo Wins In Round 5</td>
                    <td>40/1</td>
                    <td>+4000</td>
                </tr>
                <tr>
                    <td>Angelo Leo Wins In Round 6</td>
                    <td>40/1</td>
                    <td>+4000</td>
                </tr>
                <tr>
                    <td>Angelo Leo Wins In Round 7</td>
                    <td>40/1</td>
                    <td>+4000</td>
                </tr>
                <tr>
                    <td>Angelo Leo Wins In Round 8</td>
                    <td>50/1</td>
                    <td>+5000</td>
                </tr>
                <tr>
                    <td>Angelo Leo Wins In Round 9</td>
                    <td>50/1</td>
                    <td>+5000</td>
                </tr>
                <tr>
                    <td>Angelo Leo Wins In Round 10</td>
                    <td>66/1</td>
                    <td>+6600</td>
                </tr>
                <tr>
                    <td>Angelo Leo Wins In Round 11</td>
                    <td>80/1</td>
                    <td>+8000</td>
                </tr>
                <tr>
                    <td>Angelo Leo Wins In Round 12</td>
                    <td>100/1</td>
                    <td>+10000</td>
                </tr>
                <tr>
                    <td>Angelo Leo Wins By Decision</td>
                    <td>6/4</td>
                    <td>+150</td>
                </tr>
                <tr>
                    <td>Tramaine Williams Wins In Round 1</td>
                    <td>80/1</td>
                    <td>+8000</td>
                </tr>
                <tr>
                    <td>Tramaine Williams Wins In Round 2</td>
                    <td>66/1</td>
                    <td>+6600</td>
                </tr>
                <tr>
                    <td>Tramaine Williams Wins In Round 3</td>
                    <td>66/1</td>
                    <td>+6600</td>
                </tr>
                <tr>
                    <td>Tramaine Williams Wins In Round 4</td>
                    <td>50/1</td>
                    <td>+5000</td>
                </tr>
                <tr>
                    <td>Tramaine Williams Wins In Round 5</td>
                    <td>50/1</td>
                    <td>+5000</td>
                </tr>
                <tr>
                    <td>Tramaine Williams Wins In Round 6</td>
                    <td>50/1</td>
                    <td>+5000</td>
                </tr>
                <tr>
                    <td>Tramaine Williams Wins In Round 7</td>
                    <td>50/1</td>
                    <td>+5000</td>
                </tr>
                <tr>
                    <td>Tramaine Williams Wins In Round 8</td>
                    <td>50/1</td>
                    <td>+5000</td>
                </tr>
                <tr>
                    <td>Tramaine Williams Wins In Round 9</td>
                    <td>66/1</td>
                    <td>+6600</td>
                </tr>
                <tr>
                    <td>Tramaine Williams Wins In Round 10</td>
                    <td>66/1</td>
                    <td>+6600</td>
                </tr>
                <tr>
                    <td>Tramaine Williams Wins In Round 11</td>
                    <td>80/1</td>
                    <td>+8000</td>
                </tr>
                <tr>
                    <td>Tramaine Williams Wins In Round 12</td>
                    <td>100/1</td>
                    <td>+10000</td>
                </tr>
                <tr>
                    <td>Tramaine Williams Wins By Decision</td>
                    <td>20/21</td>
                    <td>-105</td>
                </tr>
                <tr>
                    <td>Draw Or Technical Draw</td>
                    <td>14/1</td>
                    <td>+1400</td>
                </tr>
            </tbody>
        </table>
        <table class="data table table-condensed table-striped table-bordered order1" width="100%" cellpadding="0"
            cellspacing="0" border="0" summary="Tramaine Williams Fight">
            <caption>Leo vs Williams - Will the Fight Go The Distance?</caption>
            <tbody>
                <tr>
                    <th>Team</th>
                    <th>Fractional</th>
                    <th>American</th>
                </tr>
                <tr>
                    <td>Yes</td>
                    <td>13/100</td>
                    <td>-800</td>
                </tr>
                <tr>
                    <td>No</td>
                    <td>5/1</td>
                    <td>+500</td>
                </tr>
            </tbody>
        </table>
        <table class="data table table-condensed table-striped table-bordered order1" width="100%" cellpadding="0"
            cellspacing="0" border="0" summary="Tramaine Williams Fight">
            <caption>Leo vs Williams - Total Rounds</caption>
            <tbody>
                <tr>
                    <th>Team</th>
                    <th>Fractional</th>
                    <th>American</th>
                </tr>
                <tr>
                    <td>Over 10½</td>
                    <td>11/100</td>
                    <td>-900</td>
                </tr>
                <tr>
                    <td>Under 10½</td>
                    <td>11/2</td>
                    <td>+550</td>
                </tr>
            </tbody>
        </table>
        <table class="data table table-condensed table-striped table-bordered order1" width="100%" cellpadding="0"
            cellspacing="0" border="0" summary="Tramaine Williams Fight">
            <caption>Leo vs Williams - On which round does the bout end</caption>
            <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated August 1, 2020.</em>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <th>Team</th>
                    <th>Fractional</th>
                    <th>American</th>
                </tr>
                <tr>
                    <td>Round 1</td>
                    <td>40/1</td>
                    <td>+4000</td>
                </tr>
                <tr>
                    <td>Round 2</td>
                    <td>33/1</td>
                    <td>+3300</td>
                </tr>
                <tr>
                    <td>Round 3</td>
                    <td>28/1</td>
                    <td>+2800</td>
                </tr>
                <tr>
                    <td>Round 4</td>
                    <td>25/1</td>
                    <td>+2500</td>
                </tr>
                <tr>
                    <td>Round 5</td>
                    <td>22/1</td>
                    <td>+2200</td>
                </tr>
                <tr>
                    <td>Round 6</td>
                    <td>22/1</td>
                    <td>+2200</td>
                </tr>
                <tr>
                    <td>Round 7</td>
                    <td>22/1</td>
                    <td>+2200</td>
                </tr>
                <tr>
                    <td>Round 8</td>
                    <td>25/1</td>
                    <td>+2500</td>
                </tr>
                <tr>
                    <td>Round 9</td>
                    <td>28/1</td>
                    <td>+2800</td>
                </tr>
                <tr>
                    <td>Round 10</td>
                    <td>33/1</td>
                    <td>+3300</td>
                </tr>
                <tr>
                    <td>Round 11</td>
                    <td>40/1</td>
                    <td>+4000</td>
                </tr>
                <tr>
                    <td>Round 12</td>
                    <td>50/1</td>
                    <td>+5000</td>
                </tr>
                <tr>
                    <td>To Go The Distance</td>
                    <td>13/100</td>
                    <td>-800</td>
                </tr>
            </tbody>
        </table>
    </div>